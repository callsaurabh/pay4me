--Query to insert 'Collection Summary Report' menu under Report
INSERT INTO ep_menu (name, label, parent_id, sfguardperm, url, env, sequence) VALUES ('Summary of Collection Report', 'Summary of Collection Report', '78', 'reportadmin', 'report/collectionSummaryReport', 'custom', '5');

--Query to insert 'Merchant Report' menu under Report
INSERT INTO ep_menu (name, label, parent_id, sfguardperm, url, env, sequence) VALUES ('merchant report', 'Merchant Report', '78', 'reportadmin', 'report/merchantReport', 'custom', '16');

--Query to insert 'Bank User Report' menu under Report
INSERT INTO ep_menu (name, label, parent_id, sfguardperm, url, env, sequence) VALUES ('Bank User Report', 'Bank User Report', '78', 'reportadmin', 'report/testBankReport', 'custom', '5');

--Query to insert 'Ewallet User Report' menu under Report
INSERT INTO ep_menu (name, label, parent_id, sfguardperm, url, env, sequence) VALUES ('Ewallet User Report', 'Ewallet User Report', '78', 'superadmin', 'report/ewalletUserReport', 'custom', '5');


--------------------------------------------------
/*
new table added
dated 16 december
by ramandeep
 */
CREATE TABLE `report_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ep_job_id` bigint(20) DEFAULT NULL,
  `bank_id` bigint(20) DEFAULT NULL,
  `bank_branch_id` bigint(20) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  `from_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `parameters` text,
  `report_name` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `frequency` enum('MONTHLY','WEEKLY','CUSTOM','QUARTERLY') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;


/*
Query for rearranging the report menu
(ep_menu table) 
dated 26 dec 2013
*/
----------------------------------------------------------------------------
UPDATE ep_menu SET sequence = CASE
WHEN name = 'Summary Report' THEN '1'
WHEN name = 'Detailed Report' THEN '2'
WHEN name = 'Bank Transaction Report' THEN '3'
WHEN name = 'Bank Branch Transaction Report' THEN '4'
WHEN name = 'eWallet Recharge Report' THEN '5'
WHEN name = 'eWallet Financial Report' THEN '6'
WHEN name = 'Summary of Collection Report' THEN '7'
WHEN name = 'Bank User Report' THEN '8'
WHEN name = 'Pay4me User Report' THEN '9'
WHEN name = 'merchant report' THEN '10'
WHEN name = 'Credit Card Payment Report' THEN '11'
WHEN name = ' eWallet User Detail' THEN '12'
WHEN name = 'Internet Bank Payment Report' THEN '13'
WHEN name = 'Split Payment Report' THEN '14'
WHEN name = 'Mobile Transaction Report' THEN '15'
WHEN name = ' Merchant Account Report' THEN '16'
WHEN name = 'Individual eWallet Account Report' THEN '17'
WHEN name = 'eWallet Host Account Report' THEN '18'
WHEN name = 'Cash Control Report' THEN '19'
WHEN name = 'Settlement Report' THEN '20'
WHEN name = 'NIBSS Charge Report' THEN '21'
WHEN name = 'Bank Virtual Report' THEN '22'
ELSE sequence
END;
---------------------------------------------------------------------------

/*
Query for selecting the report menu
(ep_menu table) 
dated 26 dec 2013
*/

SELECT id,name, sequence FROM ep_menu WHERE name IN('Summary Report','Detailed Report','Bank Transaction Report','Bank Branch Transaction Report','eWallet Recharge Report','eWallet Financial Report','Summary of Collection Report','Bank User Report','Pay4me User Report','merchant report','Credit Card Payment Report',' eWallet User Detail','Internet Bank Payment Report','Split Payment Report','Mobile Transaction Report',' Merchant Account Report','Individual eWallet Account Report','eWallet Host Account Report','Cash Control Report','Settlement Report','NIBSS Charge Report','Bank Virtual Report') order by sequence;

/*
Procedure to remove job
Will be called to cancle job request from UI
*/

drop procedure removeJob;

DELIMITER $$
CREATE  PROCEDURE removeJob
(
	IN JobId char(20),
	IN UserId char(20)
)
BEGIN

IF EXISTS(select 1 from ep_job where id = JobId)
THEN
update ep_job set ep_job.state = 'finished' , last_execution_status = 'pass'  WHERE id = JobId;

delete FROM ep_job_queue WHERE job_id = JobId;

insert into ep_job_execution(job_id,start_time,end_time,exit_code,created_at,updated_at)   values(JobId,NOW(),NOW(),0,NOW(),NOW());

SET @last_ID = LAST_INSERT_ID();

SET @output_text='Deleted by ';
SET @output_text=concat(@output_text,UserId);


insert into ep_job_data(output_type, output_text,job_execution_id )values(1,@output_text,@last_ID );

END IF;
update report_log set deleted_at =NOW() where ep_job_id =  JobId;
END$$
DELIMITER ;


/* openId login changes*/
/* old
ALTER TABLE `user_detail` CHANGE `ewallet_type` `ewallet_type` ENUM( 'ewallet', 'openid', 'both', 'ewallet_pay4me', 'openid_pay4me', 'both_pay4me', 'openid_guest' );
*/
/* New Query for guest user in openid login */
ALTER TABLE `user_detail` CHANGE `ewallet_type` `ewallet_type` ENUM( 'ewallet', 'openid', 'both', 'ewallet_pay4me', 'openid_pay4me', 'both_pay4me', 'guest_user' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL 

/* Addition of column notification status in merchant_request table */

ALTER TABLE merchant_request ADD notification_status_code tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=>success, 1=>pending,2=>failure,3=>sent' after payment_status_code;

UPDATE  merchant SET category_id='1' where id='57';
UPDATE  merchant SET category_id='1' where id='47';
UPDATE  merchant SET category_id='2' where id='5';
UPDATE  merchant SET category_id='2' where id='53';
UPDATE  merchant SET category_id='4' where id='32';
UPDATE  merchant SET category_id='5' where id='42';
UPDATE  merchant SET category_id='5' where id='50';
UPDATE  merchant SET category_id='7' where id='56';

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `merchant` ADD `category_id` INT NOT NULL AFTER `id`; 
INSERT INTO `category` 
(`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL, 'e-Education', '', NOW(), NOW(), NOW()), 
(NULL, 'e-Government', '', NOW(), NOW(), NOW()),
(NULL, 'Non-Governmental Bodies', '', NOW(), NOW(), NOW()),
(NULL, 'Online Bill Presentment', '', NOW(), NOW(), NOW()),
(NULL, 'Online Shopping Mall', '', NOW(), NOW(), NOW()),
(NULL, 'Crowdfunding', '', NOW(), NOW(), NOW()),
(NULL, 'Events Management', '', NOW(), NOW(), NOW()),
(NULL, 'Religious Organizations and Bodies', '', NOW(), NOW(), NOW()),
(NULL, 'Real Estate Management', '', NOW(), NOW(), NOW()), 
(NULL, 'Sports', '', NOW(), NOW(), NOW());


CREATE TABLE `merchant_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `host_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bank_id_idx` (`merchant_id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `merchant_user_ibfk_1` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE CASCADE,
  CONSTRAINT `merchant_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--Disable existing Support Menu
update ep_menu set deleted_at = now() where url = 'support/index';


UPDATE `a_slot` SET `value` = '<h3>How to reach us</h3> <p>Do you want to make an enquiry, a suggestion, or would like to give us feedback on our services? The contact details below can be used to reach us.</p> <p>&nbsp;</p> <p><strong>Pay4Me Services </strong></p> <p># 45B Adebayo Doherty Street,</p> <p>Lekki Phase One,</p> <p>Lagos State, Nigeria</p> <p>&nbsp;</p> <div> <table cellspacing="0" cellpadding="0" border="0"><tbody> <tr> <td>Contact Phone:</td> <td>+234 (1) 271 6943,</td> </tr> <tr> <td>&nbsp;</td> <td>+234 (1) 271 6944</td> </tr> <tr> <td>Support Phone:</td> <td> +234 (1) 454 4506</td></tr><tr> <td>&nbsp;</td> <td>+234 (1) 841 8532 </td><tr> <td>&nbsp;</td> <td>+234 (1) 844 7570 </td> </tr> <tr> <td>Email:</td> <td>support@pay4me.com</td> </tr> <tr> <td>&nbsp;</td> <td>info@pay4me.com</td> </tr> </tbody></table> </div> <p>&nbsp;</p>' 
WHERE `a_slot`.`id` =240;

ALTER TABLE `user_detail` CHANGE `ewallet_type` `ewallet_type` ENUM( 'ewallet', 'openid', 'both', 'ewallet_pay4me', 'openid_pay4me', 'both_pay4me', 'guest_user' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL 



--------------------------------------------------------------------------------------
/*
Query to create table merchant_log.
*/

CREATE TABLE `merchant_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) DEFAULT NULL,
  `log_message` text,
  `transaction_number` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_id_idx` (`request_id`),
  CONSTRAINT `FK_merchant_log` FOREIGN KEY (`request_id`) REFERENCES `merchant_request_log` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1

/*
Query to create table merchant_request_log.
*/

CREATE TABLE `merchant_request_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_text` text,
  `merchant_request_id` bigint(20) DEFAULT NULL,
  `transaction_number` bigint(20) DEFAULT NULL,
  `message_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `merchant_request_log_message_type_idx` (`message_type`),
  KEY `FK_merchant_request_log` (`merchant_request_id`),
  CONSTRAINT `FK_merchant_request_log` FOREIGN KEY (`merchant_request_id`) REFERENCES `merchant_request` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1

/*
Query to create table merchant_response_log.
*/

 CREATE TABLE `merchant_response_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message_text` text,
  `request_id` int(11) NOT NULL,
  `message_type` varchar(255) DEFAULT NULL,
  `transaction_number` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `merchant_response_log_message_type_idx` (`message_type`),
  KEY `request_id_idx` (`request_id`),
  CONSTRAINT `FK_merchant_response_log` FOREIGN KEY (`request_id`) REFERENCES `merchant_request_log` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1



/*
	Query to allow merchant access to merchant wizard section
*/

1) INSERT INTO sf_guard_permission (name,description,created_at,updated_at) VALUES('merchant_wizard','Merchant Wizard',now(),now());

2) INSERT INTO sf_guard_group_permission (group_id,permission_id,created_at,updated_at) VALUES(1,step1,now(),now());

3) INSERT INTO sf_guard_group_permission (group_id,permission_id,created_at,updated_at) VALUES(merchant_group_id,step1,now(),now());

4)UPDTAE ep_menu SET sfguardperm = 'merchant_wizard' WHERE id IN(122,163,164);


/**Merchants Per Banks Report Menu Entry**/
INSERT INTO ep_menu (name,label,parent_id,sfguardperm,url,env) VALUES ('Merchants Per Banks Report', 'Merchants Per Banks Report','78','superadmin','report/listMerchantPerBankReport','custom');





