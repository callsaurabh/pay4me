﻿YML files with path to be change. 

1.apps/frontend/config/app.yml 

pfm_role: 
    admin:              'portal_admin' 
    dev:                'dev' 
    bank_admin:         'bank_admin' 
    bank_country_head: 'bank_country_head' 
    country_report_user: 'country_report_user' 
    bank_branch_user:   'bank_user' 
    report_bank_admin:  'report_bank_admin' 
    report_admin:       'report_admin' 
    report_portal_admin: 'portal_report_admin' 
    bank_e_auditor:     'bank_e_auditor' 
    ewallet_user:       'ewallet_user'
    guest_user:	        'guest_user' 
    support:            'support' 
    portal_support:     'portal_support' 
    bank_branch_report_user:  'bank_branch_report_user' 
    report_admin:  'report_admin'

2.apps/frontend/modules/welcome/config/security.yml 

#module /welcome 
OpenInfoBoxReportAbuse: 
  is_secure: true 
default: 
  is_secure: true 
  credentials: [[bank_admin,bank_country_head,country_report_user,superadmin,reports,bank_branch_user,audit_trail,support,ewallet_user,guest_user,portalsupport]]

3. apps/frontend/modules/bill/config/security.yml

#module / bank 
default: 
  is_secure: true 
  credentials: [[ewallet_user,guest_user,bank_branch_user]] 

4. apps/frontend/modules/admin/config/security.yml

#module / admin 
EWalletUser: 
  is_secure: true 
  credentials: [[ewallet_user,guest_user]]

Database Changes 

Database changes for the new guest user role. 

Query for inserting a new role in sf_guard_group(portal support). 

INSERT INTO sf_guard_group (name, description) VALUES ('guest_user', 'Guest User'); 

Query for inserting new permissions for guest user. 

INSERT INTO sf_guard_permission (name, description) VALUES ('guest_user', 'Guest User for Online transactions'); 

Query for inserting new permissions to groups for guest user. 

INSERT INTO sf_guard_group_permission (group_id, permission_id) VALUES (16,28);

Query for updating the ep-menu table for new guest user role. 

INSERT INTO ep_menu (name, label, parent_id, sfguardperm, url, sequence) VALUES( 'Complete Signup','Complete SignUp', 1, 'guest_user','openId/createNewAccount', 9);


Query for adding ewallet_type in user_detail table.

ALTER TABLE 'user_detail' CHANGE ewallet_type ewallet_type ENUM('ewallet','openid', 'both', 'ewallet_pay4me','openid_pay4me','both_pay4me','guest_user')

permission added for guest user menu
INSERT INTO sf_guard_permission (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'home_profile', 'Home sub menu (Change password, Update security answer, view profile)', NULL, NULL);

INSERT INTO sf_guard_group_permission (`group_id`, `permission_id`, `created_at`, `updated_at`) 
VALUES 
('1', '32', NULL, NULL),
('2', '32', NULL, NULL),
('3', '32', NULL, NULL),
('4', '32', NULL, NULL),
('5', '32', NULL, NULL),
('6', '32', NULL, NULL),
('7', '32', NULL, NULL),
('8', '32', NULL, NULL),
('9', '32', NULL, NULL),
('12', '32', NULL, NULL),
('13', '32', NULL, NULL),
('14', '32', NULL, NULL),
('15', '32', NULL, NULL),
('16', '32', NULL, NULL);

UPDATE ep_menu SET sfguardperm = 'home_profile' WHERE ep_menu.name in('change_pass', 'Update Security Answers') and parent_id = 1;

Files Changed.

1. apps/frontend/modules/sfGuardAuth/actions/actions.class.php

 private function validateUserAssociations($request,$username) {
    // validate if this user belong to any bank or not --> $this->isValidBankUser
    // to do find group name by userr name
    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $userDetails =$payForMeObj->getUserDetailByUsername($username);
    $group_name = $userDetails['group_name'];
    switch($group_name){
      case sfConfig::get('app_pfm_role_bank_branch_user') :
        case sfConfig::get('app_pfm_role_bank_admin') :
        case sfConfig::get('app_pfm_role_bank_country_head') :
        case sfConfig::get('app_pfm_role_country_report_user') :
          case sfConfig::get('app_pfm_role_report_bank_admin') :
            case sfConfig::get('app_pfm_role_bank_branch_report_user') :
              case sfConfig::get('app_pfm_role_bank_e_auditor') :
                return   $this->validateBankUserAssociation($userDetails,$request);
                break;
            case sfConfig::get('app_pfm_role_ewallet_user') :
              if($this->bankname!=""){return false;}
              else
              return true;
              break;
          case sfConfig::get('app_pfm_role_support') :
            if($this->bankname!=""){return false;}
              else if($request->getParameter('payType') && $request->getParameter('payType')=='ewallet'){return false;}
              else
              return true;
              break;
        case sfConfig::get('app_pfm_role_report_admin') :
        case sfConfig::get('app_pfm_role_report_portal_admin') :
          case sfConfig::get('app_pfm_role_admin') :
            case sfConfig::get('app_pfm_role_dev') :
              if($this->bankname!=""){return false;}
              else if($request->getParameter('payType') && $request->getParameter('payType')=='ewallet'){return false;}
              else
              return true;
              break;
              case sfConfig::get('app_pfm_role_guest_user') :
                return true;
                break;
          default:
            return false;
            break;
      }
    }


private function redirectToUserHome($request,$txnId="")
    {

      $guser = $this->getUser();
      $group_name = $guser->getGroupNames();
      //check user is bank teller , if yes redirect to user bank home
      if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name)) {
        //if user is bank branch user(bank teller ) redirect it to bank home
        if($txnId == "") {
          return $this->redirectToBankHome($guser->getUsername());
        }
        else { //this is for NIPOST incase transaction location species the merchant counter
          return $this->redirectToPayment($guser->getUsername(),$txnId);
        }
      }
      else
      if(in_array(sfConfig::get('app_pfm_role_ewallet_user'),$group_name)||
        in_array(sfConfig::get('app_pfm_role_guest_user'),$group_name)) {
        $requestId = $request->getParameter('merchantRequestId');
        $paymentMode = $request->getParameter('payType');
        return $this->redirectToEwalletHome($guser->getUsername(),$requestId,$paymentMode);
      }
      if(in_array(sfConfig::get('app_pfm_role_bank_admin'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_bank_country_head'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_country_report_user'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_admin'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_dev'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_report_admin'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_report_portal_admin'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_report_bank_admin'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_support'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_bank_e_auditor'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_bank_branch_report_user'),$group_name)) {

        //redirect user
        return $this->redirectToAdminHome($guser->getUsername());
      }
      $signinUrl = sfConfig::get('app_sf_guard_plugin_success_signin_url', $guser->getReferer($request->getReferer()));
      return $this->redirect('' != $signinUrl ? $signinUrl : '@homepage');
    }


2. apps/frontend/modules/admin/actions/actions.class.php
function -> executeEWalletUser
|| in_array(sfConfig::get('app_pfm_role_guest_user'), $this->getUser()->getGroupNames())

