<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_ewalletRegistrationActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testTitle()
  {
    $this->open("/pay4me_wp063/web/index.php/signup");
//    $this->click("css=img[alt=\"Register Now\"]");
//    $this->click("name=submit");
    $this->type("id=name", "fname");
    $this->type("id=lname", "lname");
    $this->type("id=username", "ewallet");
    $this->click("id=password");
    $this->type("id=password", "ankur@123");
    $this->type("id=cpassword", "ankur@123");
    $this->click("id=dob_button");
    $this->click("//tbody/tr[2]/td[2]");
    //$this->click("id=dob_button");
    //$this->click("css=tr.daysrow.rowhilite > td.day.selected");
    $this->type("id=dob_date", "03-03-1990");
    $this->type("id=email", "vikas.rajput@tekmindz.com");
    $this->type("id=address", "Sample Address");
    $this->type("id=mobileno", "9540774955");
    $this->type("id=workphone", "9540774955");
    $this->select("id=currency_id", "label=Naira");
    $this->type("id=captcha", "4597");
    $this->click("name=submit");
    $this->type("id=mobileno", "+9540774955");
    $this->type("id=workphone", "+9540774955");
    $this->click("name=submit");
    $this->type("id=password", "ankur@123");
    $this->type("id=cpassword", "ankur@123");
    $this->click("name=submit");
    $this->type("id=captcha", "8787");
    $this->type("id=password", "ankur@123");
    $this->type("id=cpassword", "ankur@123");
    $this->click("name=submit");
    $this->waitForPageToLoad("30000");
  }
}