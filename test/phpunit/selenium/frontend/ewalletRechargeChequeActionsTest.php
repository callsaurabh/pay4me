<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_ewalletRechargeChequeActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testMyTestCase()
  {
    $project_path = explode('/',$_SERVER[PWD]);
    $this->open($project_path[3].'/web/frontend_dev.php/firstbank/login');
    $this->type("name=signin[username]", "bankuser.firstbank");
    $this->click("id=button");
    $this->waitForPageToLoad("30000");
    $this->type("id=signin_password", "admin@123");
    $this->type("id=signin_answer", "a");
    $this->click("id=button");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("_iframe-EmailBox1");
    $this->click("id=info_check");
    $this->click("css=input.greenButton");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("relative=up");
    $this->click("link=Recharge eWallet Using Cheque");
    $this->waitForPageToLoad("30000");
    $this->type("id=ewallet_number", "0035820234");
    $this->type("id=amount", "1");
    $this->type("id=sort_code", "011");
    $this->type("id=check_number", "23423");
    $this->type("id=account_number", "234234");
    $this->click("id=disclaimer_cheque_details_recharge");
    $this->click("id=charge_wallet_btn");
    $this->waitForCondition("selenium.isElementPresent(\"id=total_amount\")", "30000");
    $this->click("id=total_amount");
    $this->click("id=confirm");
    $this->click("css=#confirm_details > #loadArea > div.wrapForm2 > div.divBlock > center > input.formSubmit");
  }
}