<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_eWalletSupportUserLoginActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testMyTestCase()
  {
    $this->open("/pay4me/web/frontend_dev.php/");
    $this->type("id=chkUsername", "Username");
    $this->type("id=textfield1", "support_user");
    $this->type("id=chkPassword", "Password");
    $this->type("id=textfield", "admin@123");
    $this->click("name=button");
    $this->waitForPageToLoad("30000");
    $this->click("link=Logout");
    $this->waitForPageToLoad("30000");
}
}