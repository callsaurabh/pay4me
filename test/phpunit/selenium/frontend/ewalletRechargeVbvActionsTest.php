<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_ewalletRechargeVbvActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testTitle()
  {
    $this->open("/pay4me_wp063/web/index.php/");
    $this->type("id=textfield1", "pahalwan");
    $this->type("id=textfield", "ankur@1234");
    $this->click("name=button");
    $this->waitForPageToLoad("30000");
    $this->type("id=security_questions_answers_answer", "a");
    $this->click("css=input.formSubmit");
    $this->waitForPageToLoad("30000");
    $this->click("link=Recharge Using Visa Card");
    $this->waitForPageToLoad("30000");
    $this->type("id=recharge_amount", "12");
    $this->click("css=input.formSubmit");
    $this->waitForPageToLoad("30000");
    $this->click("id=check_amt");
    $this->click("id=check_service");
    $this->click("id=check_tot");
    $this->click("id=disclaimer");
    $this->click("css=input.formSubmit");
    $this->waitForCondition("selenium.isElementPresent(\"id=vbv\")", "30000");
    $this->selectFrame("vbv");
    $this->click("xpath=(//button[@type='button'])[6]");
    $this->click("xpath=(//button[@type='button'])[3]");
    $this->click("xpath=(//button[@type='button'])[14]");
    $this->click("xpath=(//button[@type='button'])[4]");
    $this->click("xpath=(//button[@type='button'])[4]");
    $this->click("xpath=(//button[@type='button'])[4]");
    $this->click("css=button.keypad-key");
    $this->click("css=button.keypad-key");
    $this->click("css=button.keypad-key");
    $this->click("css=button.keypad-key");
    $this->click("css=button.keypad-key");
    $this->click("xpath=(//button[@type='button'])[6]");
    $this->click("xpath=(//button[@type='button'])[10]");
    $this->click("xpath=(//button[@type='button'])[10]");
    $this->click("css=button.keypad-key");
    $this->click("xpath=(//button[@type='button'])[3]");
    $this->click("xpath=(//button[@type='button'])[8]");
    $this->click("xpath=(//button[@type='button'])[14]");
    $this->click("xpath=(//button[@type='button'])[8]");
    $this->type("name=fio", "vikash");
    $this->select("name=expMon", "label=03");
    $this->select("name=expMon", "label=04");
    $this->select("id=ExpYear", "label=2013");
    $this->click("id=OK");
    $this->waitForPageToLoad("30000");
    $this->type("id=iPIN", "1111");
    $this->click("css=#btnSubmit > img");
  }
}