<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_ewalletRechargeInterswitchActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testMyTestCase()
  {
    $this->open("/pay4me_wp063/web/frontend_dev.php/");
    $this->type("id=textfield1", "pahalwan");
    $this->type("id=textfield", "ankur@123");
    $this->click("name=button");
    $this->waitForPageToLoad("30000");
    $this->type("id=security_questions_answers_answer", "a");
    $this->click("css=input.formSubmit");
    $this->waitForPageToLoad("30000");
    $this->click("link=Recharge Using Interswitch");
    $this->waitForPageToLoad("30000");
    $this->type("id=recharge_amount", "100");
    $this->click("css=input.formSubmit");
    $this->waitForPageToLoad("30000");
    $this->click("id=check_amt");
    $this->click("id=check_service");
    $this->click("id=check_tot");
    $this->click("id=disclaimer");
    $this->click("css=input.formSubmit");
    $this->waitForCondition("selenium.isElementPresent(\"id=PayFrame\")", "30000");
    $this->selectFrame("PayFrame");
    $this->waitForCondition("selenium.isElementPresent(\"id=cardtype\")", "30000");
    $this->select("id=cardtype", "label=Interswitch MagStripe");
    $this->type("id=pp_cardpan", "6278050000000007");
    $this->type("id=pp_cardpan", "6278050000000007");
    $this->select("id=pp_exp_month", "label=Jul");
    $this->select("id=pp_exp_year", "label=2013");
    $this->click("link=0");
    $this->click("link=0");
    $this->click("link=0");
    $this->click("link=0");
    $this->click("id=pay");
    $this->waitForPageToLoad("30000");
    $this->click("link=No Thanks, conclude payment");
    $this->click("id=cancelandcontinue");
    $this->waitForPageToLoad("30000");
  }
}