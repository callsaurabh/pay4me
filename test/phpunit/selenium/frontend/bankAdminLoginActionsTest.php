<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_bankAdminLoginActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testMyTestCase()
  {
    $this->open("/pay4me/web/frontend_dev.php/");
    $this->type("id=chkUsername", "Username");
    $this->type("id=textfield1", "bankadmin.firstbank");
    $this->type("id=chkPassword", "Password");
    $this->type("id=textfield", "admin@123");
    $this->click("name=button");
    $this->waitForPageToLoad("30000");
    $this->click("id=signin_password");
    $this->type("id=signin_password", "admin@123");
    $this->type("id=signin_answer", "a");
    $this->click("id=button");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("_iframe-EmailBox1");
    $this->click("id=info_check");
    $this->click("css=input.greenButton");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("relative=up");
    $this->click("link=Logout");
    $this->waitForPageToLoad("30000");
}
}