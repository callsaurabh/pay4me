<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_eWalletUserLoginActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testMyTestCase()
  {
    $this->open("/pay4me/web/frontend_dev.php/");
    $this->type("id=chkUsername", "Username");
    $this->type("id=textfield1", "ewallet_user");
    $this->type("id=chkPassword", "Password");
    $this->type("id=textfield", "admin@123");
    $this->click("name=button");
    $this->waitForPageToLoad("30000");
    $this->type("id=security_questions_answers_answer", "a");
    $this->click("css=input.formSubmit");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("_iframe-EmailBox");
    $this->click("id=info_check");
    $this->click("css=input.greenButton");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("relative=up");
    $this->click("link=Logout");
    $this->waitForPageToLoad("30000");
}
}