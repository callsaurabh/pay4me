<?php
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class selenium_frontend_ewalletRechargeActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser('*firefox');
    $this->setBrowserUrl('http://localhost/');
  }

  public function testMyTestCase()
  {
    $project_path = explode('/',$_SERVER[PWD]);
    $this->open($project_path[3].'/web/frontend_dev.php/firstbank/login');
    $this->click("css=body");
    $this->type("name=signin[username]", "bankuser.firstbank");
    $this->click("id=button");
    $this->waitForPageToLoad("30000");
    $this->setTimeout(10000);
    $this->type("id=signin_password", "admin@123");
    $this->type("id=signin_answer", "a");
    $this->click("id=button");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("_iframe-EmailBox1");
    $this->click("id=info_check");
    $this->click("css=input.greenButton");
    $this->waitForPageToLoad("30000");
    $this->selectFrame("relative=up");
    $this->click("link=Recharge eWallet Using Cash");
    $this->waitForPageToLoad("30000");
    $this->type("id=ewallet_number", "0035820234");
    $this->type("id=amount", "10");
    $this->click("id=charge_wallet_btn");

//    echo '<script>
//    timeout=5000;
//
//    js_condition = “selenium.browserbot.getCurrentWindow().jQuery.active == 0” </script>';
//
//    $this->waitForCondition(echo '<script>js_condition</script>', 5000);

    $this->waitForCondition("selenium.isElementPresent(\"id=total_amount\")", "30000");
    
    $this->click("id=total_amount");
    $this->click("id=confirm");
    $this->click("css=#confirm_details > #loadArea > div.wrapForm2 > div.divBlock > center > input.formSubmit");
  }
}