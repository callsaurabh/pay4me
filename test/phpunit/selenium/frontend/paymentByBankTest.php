
<?php
/*
 *  to run functional test
 *  ./symfony phpunit:test-functional <application> <controller_name>
 */
require_once dirname(__FILE__).'/../../bootstrap/selenium.php';

class functional_frontend_supportActionsTest extends sfPHPUnitBaseSeleniumTestCase
{
 protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/');
    }

    public function testBankPayment() {
        $param = "<?xml version='1.0' encoding='UTF-8'?> <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>";
        // Iterate through the rows, printing XML nodes for each
        $transnum = rand();
        // ADD TO XML DOCUMENT NODE
        $param .= '<merchant-service id="1">';
        $param .= "<item number='$transnum'>";
        $param .= '<transaction-number>10</transaction-number>';
        $param .= '<name>anurag</name>';
        $param .= '<description>payment for NIS</description>';
        $param .= '<price>11</price> <currency>naira</currency>   ';
        $param .= "<parameters>";
        $param .= '<parameter name="app_id">26</parameter>';
        $param .= '<parameter name="ref_num">82</parameter>';
        $param .= "</parameters>";
        $param .= '</item>';
        $param .= '</merchant-service>';
        $param .= '</order>';

        $url = "http://localhost/pay4me_bank_integration/web/frontend_dev.php/order/payment/payprocess/v1/PID/1211523582";
        $xmlRequest = $param;

        $merchant_code = 1211523582;
        $merchant_key = "daa5a077f2b0493e94590c2d344acaa8";
        $merchant_auth_string = $merchant_code . ":" . $merchant_key;
        $merchantAuth = base64_encode($merchant_auth_string);

        $header[] = "Authorization: Basic $merchantAuth";
        $header[] = "Content-Type: application/xml;charset=UTF-8";
        $header[] = "Accept: application/xml;charset=UTF-8 ";


        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $response = curl_exec($curl);
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($response);
        $rurl = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue;
        $domain = strstr($rurl, '/p');




        $this->open($domain);
        $this->click("id=payOptions_payType_bank");
        $this->click("css=input.button");
        $this->waitForPageToLoad("30000");
        $this->click("name=continue");
        $this->waitForPageToLoad("30000");
        $Login = $this->getValue("id=transaction_number");
        $this->open('/pay4me_bank_integration/web/frontend_dev.php/firstbank/login');
        $this->type("name=signin[username]", "bankuser.firstbank");
        $this->click("id=button");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_password", "admin@123");
        $this->type("id=signin_answer", "a");
        $this->click("id=button");
        $this->waitForPageToLoad("30000");
        $this->selectFrame("_iframe-EmailBox1");
        $this->click("id=info_check");
        $this->click("css=input.greenButton");
        $this->waitForPageToLoad("30000");
        $this->selectFrame("relative=up");
        $this->type("id=search_txnId", $Login);
        $this->click("name=submit");
        $this->waitForPageToLoad("30000");
        $this->click("id=acknowledge_application_charge");
        $this->click("id=acknowledge_bank_charges");
        $this->click("id=acknowledge_service_charge");
        $this->click("id=acknowledge_total_charges");
        $this->click("id=acknowledge_disclaimer");
        $this->click("name=submit");
        $this->waitForPageToLoad("30000");
        }
}