<?php

require_once dirname(__FILE__) . '/../bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);

new sfDatabaseManager($configuration);

class unit_billerTest extends sfPHPUnitBaseTestCase {

    public function testBillerv2() {
        $t = $this->getTest();
        $billerXml1 = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="264">
    <item number="112">
      <transaction-number>86537</transaction-number>
      <name>Kashif</name>
      <description>AMA-IGRA : Business Operating Permit Registration Payment for : Kashif</description>
      <parameters>
        <parameter name="reference_number">201208090112</parameter>
        <parameter name="valuation_number">ASK030075628</parameter>
        <parameter name="amount_due">350</parameter>
      </parameters>
      <transaction-location>biller</transaction-location>
      <payment>
        <currency code="566">
          <payment-modes>
            <payment-mode>1</payment-mode>
          </payment-modes>
          <deduct-at-source></deduct-at-source>';
        $billerXml3 = '</currency>
      </payment>
    </item>
  </merchant-service>
</order>';

        echo "Test Case for V2\n\n";
        
        /* if Price greater then 100000000000 */
        $billerXml2 = '<price>1000000000000</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $billerManger = new billerManager();

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V2 - Price Could not be greater then 100000000000\n\n";
        } else {
            echo "V2 - Success price is less then or equal : 100000000000\n\n";
        }
        $billerXml2 = '<price>-100000000000</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V2 - Price Could not be less then -10000000000\n\n";
        } else {
            echo "V2 - Success price is less then or equal : -10000000000\n\n";
        }

        $billerXml2 = '<price>String</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V2 - Price is not string\n\n";
        } else {
            echo "V2 - Success : Price is numeric\n\n";
        }

        $billerXml2 = '<price></price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('1', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V2 - Not Empty\n\n";
        } else {
            echo "V2 - Price Is empty\n\n";
        }


        $billerXml2 = '<price>1111</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('1', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V2 - Wrong Price\n\n";
        } else {
            echo "V2 - Correct Price\n\n";
        }
    }

    
 public function testBillerv1() {
        $t = $this->getTest();
        $billerXml1 = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns="http://www.pay4me.com/schema/pay4meorder/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v1 biller.xsd">
  <merchant-service id="161">
    <item number="555534">
      <transaction-number>2132131231423123</transaction-number>
      <is-auto-generated></is-auto-generated>
      <name>Tekmindz</name>
      <description>Payment for FCT Water-Board Consumer Bill</description>
      <price>10000000000</price>
      <currency>naira</currency>
      <parameters>
        <parameter name="sda_type">FCT Water Board</parameter>
        <parameter name="identification_num">4047182</parameter>
        <parameter name="outstanding_amount">1000</parameter>
        <parameter name="fullname">Tekmindz</parameter>
        <parameter name="account_number">AC78645</parameter>
        <parameter name="district">Maitama</parameter>
        <parameter name="plotno">465</parameter>
        <parameter name="servtype">OG 78645</parameter>
        <parameter name="last_bill_amount">10000.30</parameter>
        <parameter name="last_bill_date">2012-05-22</parameter>
      </parameters>
    </item>
  </merchant-service>
</order>';
        echo "Test Case for V1\n\n";


        /* if Price greater then 1000000000 */
        $billerXml2 = '<price>10000000000</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $billerManger = new billerManager();

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v1');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V1 - Price Could not be less then 1000000000\n\n";
        } else {
            echo "V1 - Success price is less then or equal : 1000000000\n\n";
        }
        $billerXml2 = '<price>-100000000000</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v1');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V1 - Price Could not be greater then -100000000\n\n";
        } else {
            echo "V1 - Success price is less then or equal : -100000000\n\n";
        }

        $billerXml2 = '<price>String</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v1');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V1 - Price is not string\n\n";
        } else {
            echo "V1 - Success : Price is numeric\n\n";
        }

        $billerXml2 = '<price></price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v1');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V1 - Not Empty\n\n";
        } else {
            echo "V1 - Price Is empty\n\n";
        }


        $billerXml2 = '<price>1111</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V1 - Wrong Price\n\n";
        } else {
            echo "V1 - Correct Price\n\n";
        }


        $billerXml2 = '<price>10.5</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V1 - Not Accept Float value \n\n";
        } else {
            echo "V1 - Correct Price\n\n";
        }


        $billerXml2 = '<price>-78000.00</price>';
        $billerXml = $billerXml1 . $billerXml2 . $billerXml3;

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $t->is('0', $reponse[0]);
        if ($reponse[0] == '0') {
            echo "V1 - Not Accept Float value with negative \n\n";
        } else {
            echo "V1 - Correct Price\n\n";
        }
        
    }

public function testBillerError() {
    echo "\n Test case for V1 v2 Error Xml\n\n";
        $billerXml = '<?xml version="1.0"?>
<error xmlns="http://www.pay4me.com/schema/pay4meorder/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v1 biller.xsd">
  <error-code>505</error-code>
  <error-message>Record not found</error-message>
</error>';

        $billerManger = new billerManager();
        $reponse = $billerManger->validateSearchResponse($billerXml, 'v1');
        $this->assertEquals(0, ($reponse[0]));
        if ($reponse[0] == '0') {
            echo "V1 Error On Errorxml\n\n ";
        } else {
            echo "V1 Success\n\n";
        }

        $billerXml = '<?xml version="1.0"?>
<error xmlns="http://www.pay4me.com/schema/pay4meorder/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v1 biller.xsd">
  <error-code>404</error-code>
  <error-message>Request Not Completed</error-message>
</error>';

        $reponse = $billerManger->validateSearchResponse($billerXml, 'v2');
        $this->assertEquals(0, ($reponse[0]));
        if ($reponse[0] == '0') {
            echo "V2 Error On Errorxml\n\n";
        } else {
            echo "V2 Success\n\n";
        }
    }
    /*public function testpaymentv1(){
         echo "Payment v1\n\n";

         $objv1 = new pay4MeV1Manager();


         $transnum=rand();
         // ADD TO XML DOCUMENT NODE
         $param .= '<merchant-service id="1">';
         $param .= "<item number='$transnum'>";
         $param .= '<transaction-number>10</transaction-number>';
         $param .= '<name>anurag</name>';
         $param .= '<description>payment for NIS</description>';
         $param .= '<price>10.</price> <currency>naira</currency>   ';
         $param .= "<parameters>";
         $param .= '<parameter name="app_id">1</parameter>';
         $param .= '<parameter name="ref_num">2</parameter>';
         $param .= "</parameters>";
         $param .= '</item>';
         $param .= '</merchant-service>';
         $param .= '</order>';

         $request = new sfWebRequest();
         echo "<pre>111";
         print_r($request);
         die;
         $request->setParameter('module', 'order');
         $request->setParameter('action', 'payment');
         $request->setParameter('payprocess', 'payprocess');
         $request->setParameter('PID', '211523582');

         $objv1->processOrder($request);
    }*/
}