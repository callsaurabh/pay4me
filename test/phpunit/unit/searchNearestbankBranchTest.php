<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);

new sfDatabaseManager($configuration);

/* Test Case For [WP049][CR077] */
class unit_searchNearestbankBranchTest extends sfPHPUnitBaseTestCase
{
    protected function _start()
    {
        $this->getTest()->diag('test is starting');
    }

    protected function _end()
    {
        $this->getTest()->diag('test is ending');
    }

    public function testFindAllReleatedBanks()
    {
        $t = $this->getTest();

        $result = false;
        $bankMerchantObj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);

        $merchantId = 1;
        $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($merchantId);
        if(count($bankMerchantListArray)>0)
        $result = true;
        $t->is(true, $result);

        $merchantId = 2;
        $result = false;
        $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($merchantId);
        if(count($bankMerchantListArray)>0)
        $result = true;
        $t->is(true, $result);

        $merchantId = 0;
        $result = false;
        $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($merchantId);
        if(count($bankMerchantListArray)>0)
        $result = true;
        $t->is(false, $result);

        $merchantId = -1;
        $result = false;
        $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($merchantId);
        if(count($bankMerchantListArray)>0)
        $result = true;

        $t->is(false, $result);

    }

    public function testGetMerchant()
    {
        $t = $this->getTest();

        ### For Negative MerchantId ###
        $merchantId = -1;
        $result = false;
        $merchantListArray = Doctrine::getTable('Merchant')->getMerchant($merchantId);
        if(count($merchantListArray)>0)
        $result = true;

        $t->is(false, $result);

        ### For Null MerchantId ###
        $merchantId = 0;
        $merchantListArray = Doctrine::getTable('Merchant')->getMerchant($merchantId);
        $t->is(true, is_array($merchantListArray));

        ### For Existing MerchantId ###
        $merchantId = 1;
        $merchantListArray = Doctrine::getTable('Merchant')->getMerchant($merchantId);
        $t->is(true, is_array($merchantListArray));


        ### For Blank MerchantId ###
        $merchantId = "";
        $merchantListArray = Doctrine::getTable('Merchant')->getMerchant($merchantId);
        $t->is(true, is_array($merchantListArray));

        ### For Non Existing MerchantId ###
        $merchantId = 11111;
        $result = false;
        $merchantListArray = Doctrine::getTable('Merchant')->getMerchant($merchantId);
        if(count($merchantListArray)>0)
        $result = true;

        $t->is(false, $result);

    }

    public function testGetBankInfo()
    {
        $t = $this->getTest();
        $bankObj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BASE);

        ### For True Value  ###
        $bankId = 1;
        $bankInfoArray = $bankObj->getBankInfo($bankId);
        $t->is(true, is_array($bankInfoArray));

        ### For 0 Value ###
        $bankId = 0;
        $bankInfoArray = $bankObj->getBankInfo($bankId);
        $t->is(false, is_array($bankInfoArray));

        ### For -ive value ###
        $bankId = -1;
        $bankInfoArray = $bankObj->getBankInfo($bankId);
        $t->is(false, is_array($bankInfoArray));

        ### For null value ###
        $bankId = null;
        $bankInfoArray = $bankObj->getBankInfo($bankId);
        $t->is(false, is_array($bankInfoArray));

        ### For String value ###
        $bankId = 'test';
        $result = false;
        $bankInfoArray = $bankObj->getBankInfo($bankId);
        $t->is(false, is_array($bankInfoArray));

    }

    public function testGetStateInfo()
    {
        $t = $this->getTest();
        $stateObj = stateServiceFactory::getService(stateServiceFactory::$TYPE_STATE);
      

        ### For 0 Value ###
        $stateId = 0;
        $stateInfoArray = $stateObj->getStateInfo($stateId);
        $t->is(false, is_array($stateInfoArray));

        ### For -ive value ###
        $stateId = -1;
        $stateInfoArray = $stateObj->getStateInfo($stateId);
        $t->is(false, is_array($stateInfoArray));

        ### For null value ###
        $stateId = -1;
        $stateInfoArray = $stateObj->getStateInfo($stateId);
        $t->is(false, is_array($stateInfoArray));

        ### For String value ###
        $stateId = 'test';
        $stateInfoArray = $stateObj->getStateInfo($stateId);
        $t->is(false, is_array($stateInfoArray));

    }

    public function testGetLgaInfo()
    {
        $t = $this->getTest();
        $lgaObj = lgaServiceFactory::getService(lgaServiceFactory::$TYPE_LGA);


        ### For 0 Value ###
        $lgaId = 0;
        $lgaInfoArray = $lgaObj->getLgaInfo($lgaId);
        $t->is(false, is_array($lgaInfoArray));

        ### For -ive value ###
        $lgaId = -1;
        $lgaInfoArray = $lgaObj->getLgaInfo($lgaId);
        $t->is(false, is_array($lgaInfoArray));

        ### For null value ###
        $lgaId = -1;
        $lgaInfoArray = $lgaObj->getLgaInfo($lgaId);
        $t->is(false, is_array($lgaInfoArray));

        ### For String value ###
        $lgaId = 'test';
        $lgaInfoArray = $lgaObj->getLgaInfo($lgaId);
        $t->is(false, is_array($lgaInfoArray));

    }


}