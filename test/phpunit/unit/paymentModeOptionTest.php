<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);

new sfDatabaseManager($configuration);

class unit_paymentModeOptionTest extends sfPHPUnitBaseTestCase
{
  public function testDefault()
  {
    $t = $this->getTest();
    $managerObj = new payForMeManager();

    #Test Case for Merchant Request payment Mode Option#
    $merchantRequestId = 1;
    $result =  $managerObj->getMerchantRequestPaymentMode($merchantRequestId) ;
    $t->is(true, count($result)>0);

    #for false condition

    $merchantRequestId = 1;
    $result =  $managerObj->getMerchantRequestPaymentMode($merchantRequestId) ;
    $t->is(true, count($result)>0);


    #Test Case for Check Payment Mode Option Available in merchant_request_payment_mode Table #

    $merchantRequestId = 35;
    $result =  $managerObj->checkMerhantRequestPaymentModes($merchantRequestId) ;//for true
    $t->is(true, $result);



    $merchantRequestId = 1;
    $result =  $managerObj->checkMerhantRequestPaymentModes($merchantRequestId) ;//for true
    $t->is(true, $result);



 }
/*
 * Test casr for WP058
 * client feedback
 * WP058_UTC_001
 */
 public function testGetServiceType()
 {
     $t = $this->getTest();

     $payment_mode_id='';
     $paymentModeObj = Doctrine::getTable('PaymentModeOption')->getServiceType($payment_mode_id);
     $paymentModeArr = $paymentModeObj->toArray();
     $t->is(true,($paymentModeArr!=''));



     $payment_mode_id=1;
     $paymentModeObj = Doctrine::getTable('PaymentModeOption')->getServiceType($payment_mode_id);
     $paymentModeArr = $paymentModeObj->toArray();
     $t->is(true,($paymentModeArr!=''));


     $payment_mode_id=121;
     $paymentModeObj = Doctrine::getTable('PaymentModeOption')->getServiceType($payment_mode_id);
     $t->is(true,($paymentModeObj==''));

     $payment_mode_id= 'aaa';
     $paymentModeObj = Doctrine::getTable('PaymentModeOption')->getServiceType($payment_mode_id);
     $t->is(NULL,($paymentModeObj));



 }
}