<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_messageQueueRequestTableTest extends sfPHPUnitBaseTestCase {

    public function testGetQueueDetailsById() {

        $t = $this->getTest();

        $id = "";
        $result = Doctrine::getTable('MessageQueueRequest')->getQueueDetailsById($id);
        $t->is(0, count($result));


        $id = "fgfhf";
        $result = Doctrine::getTable('MessageQueueRequest')->getQueueDetailsById($id);
        $t->is(0, count($result));


        $id = "189";
        $result = Doctrine::getTable('MessageQueueRequest')->getQueueDetailsById($id);
        $t->is(1, count($result));
    }

    public function testFindMailRecord() {

        $t = $this->getTest();

        $id = "";
        $result = Doctrine::getTable('MessageQueueRequest')->findMailRecord($id);
        $t->is(false, $result);


        $id = "fgfhf";
        $result = Doctrine::getTable('MessageQueueRequest')->findMailRecord($id);
        $t->is(false, $result);


        $id = "187";
        $result = Doctrine::getTable('MessageQueueRequest')->findMailRecord($id);
        $t->is(1, $result->count());
    }

}