<?php
require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);
new sfDatabaseManager($configuration);
class unit_billTest extends sfPHPUnitBaseTestCase
{

    
    public function  testgetArchiveBills(){

        $t = $this->getTest();

        ### For Null Values
        $userId = "";
        $intMerchantId = "";
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getArchiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());

       
        $userId = 3573;
        $intMerchantId = "";
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getArchiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());

        $userId = 3573;
        $intMerchantId = 1;
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getArchiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());


        $userId = 3573;
        $intMerchantId = 1;
        $intMerchantServiceId = 2;
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getArchiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());
    }

       public function  testgetActiveBills(){

        $t = $this->getTest();

        ### For Null Values
        $userId = "";
        $intMerchantId = "";
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getItemWiseActiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());

        
        $userId = 3573;
        $intMerchantId = "";
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getItemWiseActiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());

        $userId = 3573;
        $intMerchantId = 1;
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getItemWiseActiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());


        $userId = 3573;
        $intMerchantId = 1;
        $intMerchantServiceId = 2;
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getItemWiseActiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());
    }


    public function  testgetPaidBills(){

        $t = $this->getTest();

        ### For Null Values
        $userId = "";
        $intMerchantId = "";
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getPaidBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());

       
        $userId = 3573;
        $intMerchantId = "";
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getPaidBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());

        $userId = 3573;
        $intMerchantId = 1;
        $intMerchantServiceId = "";
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getPaidBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());


        $userId = 3573;
        $intMerchantId = 1;
        $intMerchantServiceId = 2;
        $strFromDate = "";
        $strToDate = "";
        $transaction = "";
        $result = Doctrine::getTable('Bill')->getPaidBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
        $result= $result->execute();
        $t->is(0, $result->count());
    }

}