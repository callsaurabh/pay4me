<?php
require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);
new sfDatabaseManager($configuration);
class unit_checkdraftTest extends sfPHPUnitBaseTestCase
{

    public function  testGetBanksForCheckPayment(){

        $t = $this->getTest();

        ### For Null Values
        $merchantId = "";
        $currency = "";
        $result = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForCheckPayment($merchantId, $currency);
        $t->is(true, $result!='');


        $merchantId = 1;
        $currency = "";
        $result = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForCheckPayment($merchantId, $currency);
        $t->is(true, $result!='');

       
        $merchantId = 1;
        $currency = 2;
        $result = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForCheckPayment($merchantId, $currency);
        $t->is(true, $result!='');

        $merchantId = 'a';
        $currency = 'a';
        $result = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForCheckPayment($merchantId, $currency);
        $t->is(true, $result!='');
    }

       public function  testGetBankListForCheckRecharge(){

        $t = $this->getTest();

        ### For Null Values
       
        $result = Doctrine::getTable('ServiceBankConfiguration')->getBankListForCheckRecharge();
        $t->is('', $result='');

    }
    public function  testGetValidSortCode(){

        $t = $this->getTest();

        ### For Null Values
        $bank_id = "";
        $bank_id = "";
        $result = Doctrine::getTable('BankSortCodeMapper')->validateSortCode($bank_id, $bank_id);
        $t->is(false, $result);

        $bank_id = 1;
        $bank_id = "";
        $result = Doctrine::getTable('BankSortCodeMapper')->validateSortCode($bank_id, $bank_id);
        $t->is(false, $result);


        $bank_id = 1;
        $bank_id = "123";
        $result = Doctrine::getTable('BankSortCodeMapper')->validateSortCode($bank_id, $bank_id);
        $t->is(false, $result);

        $bank_id = 'a';
        $bank_id = "123";
        $result = Doctrine::getTable('BankSortCodeMapper')->validateSortCode($bank_id, $bank_id);
        $t->is(false, $result);


       
    }

}