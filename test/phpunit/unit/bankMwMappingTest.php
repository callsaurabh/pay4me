<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_bankMwMappingTest extends sfPHPUnitBaseTestCase {

    public function testGetRequestDetails() {

        $t = $this->getTest();

        ### For Null Values
        $bank = "";
        $status = '';
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(2, $result->count());// Can be or cannot be grater than zero


        $bank = 1; //Bank Id which is present in Table
        $status = "";
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(1, $result->getFirst()->getBankId());

        $bank = 6; //Bank Id which is not present in Table
        $status = "";
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(0, $result->count());



        $bank = "";
        $status = 0;
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(2, $result->count());// Can be or cannot be grater than zero

        $bank = "";
        $status = 1;
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(2, $result->count());// Can be or cannot be grater than zero

        $bank = "";
        $status = 56; //Status which is not present in Table
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(0, $result->count());


        $bank = 1;
        $status = 1;
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(1, $result->count());// Can be or cannot be grater than zero

        $bank = 1;
        $status = 0;
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(1, $result->count());// Can be or cannot be grater than zero

        $bank = 1;
        $status = 1;
        $result = Doctrine::getTable('BankMwMapping')->getRequestDetails($bank, $status);
        $result = $result->execute();
        $t->is(1, $result->count());// Can be or cannot be grater than zero
    }

    public function testGetMwMappingIdByBankId() {

        $t = $this->getTest();
        $bank_id = "";
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingIdByBankId($bank_id);
        
        $t->is(false, $result);

        $bank_id = 1;
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingIdByBankId($bank_id);
        
        $t->is(1, $result);


        $bank_id = 9;
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingIdByBankId($bank_id);
        
        $t->is(false, $result);
    }

    public function testGetMwMappingStatus() {

        $t = $this->getTest();
        $system_id = "";
        $instance_name = "";
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id, $instance_name);
//        $result = $result->execute();
        $t->is(0, $result);

        $system_id = "8567u78678"; //Invlid system Id
        $instance_name = "";
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id, $instance_name);
//        $result = $result->execute();
        $t->is(0, $result);


        $system_id = "";
        $instance_name = "hlljkljkl"; //Invalid Instance Name
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id, $instance_name);
//        $result = $result->execute();
        $t->is(0, $result);


        $system_id = "8547ca4d43c40eac3edc223bf7d13c41";//Valid $system_id
        $instance_name = "PRODUCTION"; //Valid Instance Name
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id, $instance_name);
//        $result = $result->execute();
        $t->is(1, $result->count());

        $system_id = "98989bf7d13c41";//InValid $system_id
        $instance_name = "PRODUCTION"; //Valid Instance Name
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id, $instance_name);
//        $result = $result->execute();
        $t->is(0, $result);


        $system_id = "8547ca4d43c40eac3edc223bf7d13c41";//Valid $system_id
        $instance_name = "5645t45t5"; //InValid Instance Name
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id, $instance_name);
//        $result = $result->execute();
        $t->is(0, $result);
    }

}