<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_BankMwMappingTest extends sfPHPUnitBaseTestCase {

    public function testGetPassword() {
         //UTC_001
        $t = $this->getTest();

        $bankId = "";
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingIdByBankId($bankId);
        $t->is(false, $result);

        $bankId =21;
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingIdByBankId($bankId);
        $t->is(1, $result);

        $bankId = "fghfghfgh";
        $result = Doctrine::getTable('BankMwMapping')->getMwMappingIdByBankId($bankId);
        $t->is(false, $result);

        
    }

}