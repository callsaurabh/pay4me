<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_transactionBankPostingTest extends sfPHPUnitBaseTestCase {

    public function testUpdateMessageRequest() {

        $t = $this->getTest();

        $id = 4;
        $attemptCount = 0;
        $checkReAttempt = 1;
        $result = Doctrine::getTable('TransactionBankPosting')->updateMessageRequest($id, $attemptCount, $checkReAttempt);

        $t->is(true, $result);



        $id = 76;
        $attemptCount = 2;
        $checkReAttempt = 1;
        $result = Doctrine::getTable('TransactionBankPosting')->updateMessageRequest($id, $attemptCount, $checkReAttempt);
       $t->is(true, $result);

        $id = 121;
        $attemptCount = 2;
        $checkReAttempt = 0;
        $result = Doctrine::getTable('TransactionBankPosting')->updateMessageRequest($id, $attemptCount, $checkReAttempt);
       $t->is(true, $result);
    }

}