<?php

require_once dirname(__FILE__) . '/../bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);

new sfDatabaseManager($configuration);

class unit_BillerTest extends sfPHPUnitBaseTestCase {

    public function testpaymentv1() {
        echo "Payment v1\n";

        $objv1 = new pay4MeV1Manager();


        $paymentXml = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='1'>
                <item number='2114954985'>
                    <transaction-number>10</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description><price>11</price><currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";
        $xmlValidatorObj = new validatePay4meXML();
        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXml);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');

        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            print_r($xmlError);
        }else {
            echo "V1 : it is a prefact XML\n";
        }


        $paymentXmlInvalidPrice = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='1'>
                <item number='2114954985'>
                    <transaction-number>10</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price>10000.51</price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : 1000.51 is invalid Price\n\n";
        }else {
            echo "V1 : 1000.51 is a valid price\n\n";
        }


        $paymentXmlInvalidPrice = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='1'>
                <item number='2114954985'>
                    <transaction-number>10</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price>-10000.51</price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : -1000.51 is a invalid price\n\n";
        }else {
            echo "V1 : -1000.51 is a valid price\n\n";
        }


        

        $paymentXmlInvalidPrice = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='1'>
                <item number='2114954985'>
                    <transaction-number>10</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price>10000.51000</price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : -1000.5100 is a invalid price. because of value after greater that 2\n\n";
        }else {
            echo "V1 : -1000.5100 is a valid price\n\n";
        }




        $paymentXmlInvalidPrice = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='1'>
                <item number='2114954985'>
                    <transaction-number>10</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price>0</price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : -Price can not be zero\n\n";
        }else {
            echo "V1 : -Price can  be zero\n\n";
        }


        $paymentXmlInvalidPrice = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='1'>
                <item number='2114954985'>
                    <transaction-number>10</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price></price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : -Price can not be empty\n\n";
        }else {
            echo "V1 : -Price can  be empty\n\n";
        }

        $paymentXmlTransno = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='1'>
                <item number='1'>
                    <transaction-number>sdadfsdasdfasdf</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price>10000</price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlTransno);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : -Transaction No can not be string\n\n";
        }else {
            echo "V1 : Transaction  can be string\n\n";
        }

        $paymentXmlService = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='161'>
                <item number='161'>
                    <transaction-number>4444</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price>10000</price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";

        
        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlService);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : -Merchant service No can not be string\n\n";
        }else {
            echo "V1 : Merchant service  can be string\n\n";
        }


        $paymentXmlService = "<?xml version='1.0' encoding='UTF-8'?>
        <order   xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1' xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd'>
            <merchant-service id='161'>
                <item number='sdfdfgs'>
                    <transaction-number>4444</transaction-number>
                    <name>anurag</name>
                    <description>payment for NIS</description>
                    <price>10000</price>
                    <currency>naira</currency>
                    <parameters>
                        <parameter name='app_id'>1</parameter>
                        <parameter name='ref_num'>2</parameter>
                    </parameters>
                </item>
            </merchant-service>
        </order>";


        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlService);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : -Item No can not be string\n\n";
        }else {
            echo "V1 : item No  can be string\n\n";
        }


        
    }

     public function testpaymentv2() {
        echo "\nPayment v2\n";

        $objv1 = new pay4MeV1Manager();


        $paymentXml = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="1">
    <item number="11">
      <transaction-number>18446744073709551615</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price>222</price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';
        $xmlValidatorObj = new validatePay4meXML();
        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXml);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');

        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            print_r($xmlError."\n");
            echo "V2 : it is a valid XML\n\n";

        }else {
            echo "V2 : it is a valid XML\n\n";
        }


        $paymentXmlInvalidPrice = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="1">
    <item number="1">
      <transaction-number>18446744073709551615</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price>1000.51</price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V1 : 1000.51 is invalid Price\n\n";
        }else {
            echo "V1 : 1000.51 is a valid price\n\n";
        }


        $paymentXmlInvalidPrice = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="1">
    <item number="1">
      <transaction-number>18446744073709551615</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price>-1000.51</price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V2 : -1000.51 is a invalid price\n\n";
        }else {
            echo "V2 : -1000.51 is a valid price\n\n";
        }




        $paymentXmlInvalidPrice = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="1">
    <item number="1">
      <transaction-number>18446744073709551615</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price></price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V2 : Price can not be empty \n\n";
        }else {
            echo "V2 : Price Can be empty\n\n";
        }



         $paymentXmlInvalidPrice = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="1">
    <item number="1">
      <transaction-number>18446744073709551615</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price>0</price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlInvalidPrice);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V2 : Price can not be zero (0) \n\n";
        }else {
            echo "V2 : Price Can be zero (0)\n\n";
        }


        $paymentXmlTransno = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="1">
    <item number="1">
      <transaction-number>jklkljkl</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price>11</price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';

        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlTransno);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V2 : -Transaction No can not be string\n\n";
        }else {
            echo "V2 : Transaction  can be string\n\n";
        }



        

        $paymentXmlService = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="asdasd">
    <item number="1">
      <transaction-number>18446744073709551615</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price></price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';


        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlService);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V2 : -Merchant service No can not be string\n\n";
        }else {
            echo "V2 : Merchant service  can be string\n\n";
        }


        $paymentXmlService = '<?xml version="1.0" encoding="utf-8"?>
<order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.pay4me.com/schema/pay4meorder/v2" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd">
  <merchant-service id="1">
    <item number="klkljklj">
      <transaction-number>18446744073709551615</transaction-number>
      <name>MR dssdsd  dssdsd</name>
      <description>NIS-NIS PASSPORT</description>
      <parameters>
	<parameter name="app_id">21</parameter>
	<parameter name="ref_num">8888</parameter>
      </parameters>
      <payment>
        <currency code="566">
          <deduct-at-source>false</deduct-at-source>
          <price></price>
        </currency>
      </payment>
    </item>
  </merchant-service>
</order>';


        $xdoc = new DomDocument;
        $xdoc->LoadXML($paymentXmlService);
        $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2');
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();
            echo "V2 : -Item No can not be string\n\n";
        }else {
            echo "V2 : item No  can be string\n\n";
        }



    }


}
?>
