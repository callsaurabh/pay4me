<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_interswitchTest extends sfPHPUnitBaseTestCase {

    public function testGetBankuserList() {

        $t = $this->getTest();

        ### For Null Values
        $bkname = '';
        $countryId = '';
        $bankId = '';
        $result = Doctrine::getTable('Bank')->checkBankAlreadyExists($bkname, $countryId, $bankId);
        $t->is(0, $result[0]['COUNT']);


        $bkname = 'a';
        $countryId = 'a';
        $bankId = 'aa';
        $result = Doctrine::getTable('Bank')->checkBankAlreadyExists($bkname, $countryId, $bankId);
        $t->is(0, $result[0]['COUNT']);

        $bkname = 'ass';
        $countryId = 1;
        $bankId = 1;
        $result = Doctrine::getTable('Bank')->checkBankAlreadyExists($bkname, $countryId, $bankId);
        $t->is(0, $result[0]['COUNT']);
    }
//
    public function testgetBankList() {
        $t = $this->getTest();

        ### For Null Values
        $result = Doctrine::getTable('Bank')->getBankList();
        $result = $result->toArray();

        $t->is(true, $result!='');
    }

    public function testisValidBank() {
        $t = $this->getTest();

        ### For Null Values
        $bankCode='';
        $bankname='';
        $result = Doctrine::getTable('Bank')->isValidBank($bankCode, $bankname);
        $t->is(0, $result);

        $bankCode='';
        $bankname='xxxx';
        $result = Doctrine::getTable('Bank')->isValidBank($bankCode, $bankname);
        $t->is(0, $result);
    }
public function testGetTransactionDetails(){
     $t = $this->getTest();
     $transactionId='';
     $bankId='';
     $currencyId='';
     $result = Doctrine::getTable('Transaction')->getTransactionDetails($transactionId, $bankId='', $currencyId='');
     $t->is('', $result='');

     $transactionId='';
     $bankId=1;
     $currencyId='';
     $result = Doctrine::getTable('Transaction')->getTransactionDetails($transactionId, $bankId='', $currencyId='');
     $t->is('', $result='');

     $transactionId='';
     $bankId='a';
     $currencyId='';
     $result = Doctrine::getTable('Transaction')->getTransactionDetails($transactionId, $bankId='', $currencyId='');
     $t->is('', $result='');

     $transactionId=1234;
     $bankId=1;
     $currencyId=1;
     $result = Doctrine::getTable('Transaction')->getTransactionDetails($transactionId, $bankId='', $currencyId='');
     $t->is('', $result='');

     $transactionId=1234;
     $bankId=1;
     $currencyId='a';
     $result = Doctrine::getTable('Transaction')->getTransactionDetails($transactionId, $bankId='', $currencyId='');
     $t->is('', $result='');
}
public function testGetP4meMerchantAccountForRecharge(){
     $t = $this->getTest();
     $p4m_merchant_d='';
     $result = Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountForRecharge($p4m_merchant_d);
     $t->is('', $result='');

     $p4m_merchant_d=1;
     $result = Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountForRecharge($p4m_merchant_d);
     $t->is('', $result='');

     $p4m_merchant_d='a';
     $result = Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountForRecharge($p4m_merchant_d);
     $t->is('', $result='');
}
public function testGetP4meMerchantAccountFromTransactionNumber(){
     $t = $this->getTest();
     $transactionNum='';
     $result = Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountFromTransactionNumber($transactionNum);
     $t->is('', $result='');

     $transactionNum='a';
     $result = Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountFromTransactionNumber($transactionNum);
     $t->is('', $result='');

     $transactionNum=1234;
     $result = Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountFromTransactionNumber($transactionNum);
     $t->is('', $result='');
}
}