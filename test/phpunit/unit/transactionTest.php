<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);

new sfDatabaseManager($configuration);

class unit_transactionTest extends sfPHPUnitBaseTestCase
{
  
/*
 * Test casr for WP058
 * client feedback
 * WP058_UTC_003
 */
 public function testUpdateTransactionRecord()
 {
     $t = $this->getTest();

     $draftNum='123';
     $sortCode='123';
     $transNum=343333546;
     $paymentModeObj = Doctrine::getTable('Transaction')->updateTransactionRecord($draftNum,$sortCode, $transNum);
     $UpdatedObj = Doctrine::getTable('Transaction')->findByPfmTransactionNumber($transNum);
     $t->is(true,($UpdatedObj->getFirst()->getCheckNumber()==$draftNum &&  $UpdatedObj->getFirst()->getSortCode()==$sortCode) );


     $draftNum='123';
     $sortCode='';
     $transNum=343333546;
     $paymentModeObj = Doctrine::getTable('Transaction')->updateTransactionRecord($draftNum,$sortCode, $transNum);
     $UpdatedObj = Doctrine::getTable('Transaction')->findByPfmTransactionNumber($transNum);
     $t->is(true,($UpdatedObj->getFirst()->getCheckNumber()==$draftNum &&  $UpdatedObj->getFirst()->getSortCode()==$sortCode) );


     $draftNum='';
     $sortCode='123';
     $transNum=343333546;
     $paymentModeObj = Doctrine::getTable('Transaction')->updateTransactionRecord($draftNum,$sortCode, $transNum);
     $UpdatedObj = Doctrine::getTable('Transaction')->findByPfmTransactionNumber($transNum);
     $t->is(true,($UpdatedObj->getFirst()->getCheckNumber()==$draftNum &&  $UpdatedObj->getFirst()->getSortCode()==$sortCode) );






 }
}