<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_BankCredentialTest extends sfPHPUnitBaseTestCase {

    public function testGetPassword() {

        $t = $this->getTest();

        $bankId = "";
        $user_name = "";


        $result = Doctrine::getTable('BankCredential')->getPassword($bankId, $user_name);
        $t->is(0, $result->count());

        $bankId = 1;
        $user_name = "";
        $result = Doctrine::getTable('BankCredential')->getPassword($bankId, $user_name);
        $t->is(0, $result->count());

        $bankId = "";
        $user_name = "pay4mepublisher"; //valid
        $result = Doctrine::getTable('BankCredential')->getPassword($bankId, $user_name);
        $t->is(0, $result->count());


        $bankId = "";
        $user_name = "fghfghfgh"; //invalid
        $result = Doctrine::getTable('BankCredential')->getPassword($bankId, $user_name);
        $t->is(0, $result->count());

        $bankId = 1;
        $user_name = "fghfghfgh"; //invalid
        $result = Doctrine::getTable('BankCredential')->getPassword($bankId, $user_name);
        $t->is(0, $result->count());

        $bankId = 1;
        $user_name = "pay4mepublisher";
        $result = Doctrine::getTable('BankCredential')->getPassword($bankId, $user_name);
        $t->is(1, $result->count());
    }

}