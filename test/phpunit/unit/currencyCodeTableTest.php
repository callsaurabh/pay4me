<?php


//wp051
require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_currencyCodeTableTest extends sfPHPUnitBaseTestCase {

    public function testGetSelectedCurrency() {

        $t = $this->getTest();
        $country_id = '';
        $result = Doctrine::getTable('CurrencyCode')->getSelectedCurrency($country_id);
        $t->is(1, $result);

        $country_id = 227;
        $result = Doctrine::getTable('CurrencyCode')->getSelectedCurrency($country_id);
        $t->is(2, $result);

        $country_id = "98uhnuihi";
        $result = Doctrine::getTable('CurrencyCode')->getSelectedCurrency($country_id);
        $t->is(1, $result);

        $country_id = 154;
        $result = Doctrine::getTable('CurrencyCode')->getSelectedCurrency($country_id);
        $t->is(1, $result);
    }

}