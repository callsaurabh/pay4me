<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);
new sfDatabaseManager($configuration);
class unit_splitpaymentreportTest extends sfPHPUnitBaseTestCase
{

    public function  testIsMerchantEwalletUser(){

        $t = $this->getTest();

        ### For Null Values
        $userId = "";
        $groupId = "";
        $result = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId,$groupId);
        $t->is(false, $result);

        ### For 0 values###
        $userId = 0;
        $groupId = 0;
        $result = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId,$groupId);
        $t->is(false, $result);


        ### For String value ###
        $userId = "a";
        $groupId = "a";
        $result = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId,$groupId);
        $t->is(false, $result);

        ### For true value ###
        $userId = "1";
        $groupId = "1";
        $result = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId,$groupId);
        if(is_object($result))
        $result = true;
        else
        $result = false;
        $t->is(true, $result);

        ### For -ive value ###
        $userId = "-1";
        $groupId = "-1";
        $result = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId,$groupId);
        $t->is(null, $result);

        ### For wrong userId and right groupId value ###
        $userId = "q";
        $groupId = "1";
        $result = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId,$groupId);
        $t->is(null, $userAnswerCount);
    }

    public function  testGetSplitDetail(){

        ### for true value ###
        $t = $this->getTest();
        $merchantRequestId = 90;
        $searchResultObj = Doctrine::getTable('PaymentSplitDetails')->getSplitDetail($merchantRequestId)->execute();
        $result = count($searchResultObj->toArray());
        $t->is(false, ($result) ? true: false);

        ### for all false value ###
        $t = $this->getTest();
        $merchantRequestId = 0;
        $searchResultObj = Doctrine::getTable('PaymentSplitDetails')->getSplitDetail($merchantRequestId)->execute();
        $result = count($searchResultObj->toArray());
        $t->is(false, ($result) ? true: false);



        $t = $this->getTest();
        $merchantRequestId = '';
        $searchResultObj = Doctrine::getTable('PaymentSplitDetails')->getSplitDetail($merchantRequestId)->execute();
        $result = count($searchResultObj->toArray());
        $t->is(false, ($result) ? true: false);


        $t = $this->getTest();
        $merchantRequestId = 'aaa';
        $searchResultObj = Doctrine::getTable('PaymentSplitDetails')->getSplitDetail($merchantRequestId)->execute();
        $result = count($searchResultObj->toArray());
        $t->is(false, ($result) ? true: false);


        $t = $this->getTest();
        $merchantRequestId = '-12';
        $searchResultObj = Doctrine::getTable('PaymentSplitDetails')->getSplitDetail($merchantRequestId)->execute();
        $result = count($searchResultObj->toArray());
        $t->is(false, ($result) ? true: false);


    }

    ####### WP047 Test cases for function created during the split of payment through revenue share ##########


    public function  testGetRevenueSplitAccounts(){
        $t = $this->getTest();

        ### for null False value ###
        $accountingObj = new pay4meAccounting();
        $merchant_request_id = '';
        $merchant_service_id = '';
        $pay4meAmount = '';
        $currency = '';
        $to_account_arr = $accountingObj->getRevenueSplitAccounts($merchant_request_id, $merchant_service_id,
            $pay4meAmount, $currency);
        $t->is(false, $to_account_arr);


        ### for 0  value ###
        $merchant_request_id = '0';
        $merchant_service_id = '0';
        $pay4meAmount = '0';
        $currency = '0';
        $to_account_arr = $accountingObj->getRevenueSplitAccounts($merchant_request_id, $merchant_service_id,
            $pay4meAmount, $currency);
        $t->is(false, $to_account_arr);



        ### for -ive  value ###
        $merchant_request_id = '-1';
        $merchant_service_id = '-1';
        $pay4meAmount = '-1';
        $currency = '-1';
        $to_account_arr = $accountingObj->getRevenueSplitAccounts($merchant_request_id, $merchant_service_id,
            $pay4meAmount, $currency);
        $t->is(false, $to_account_arr);


        ### for string value ###
        $merchant_request_id = 'sss';
        $merchant_service_id = 'sss';
        $pay4meAmount = 'sss';
        $currency = 'sss';
        $to_account_arr = $accountingObj->getRevenueSplitAccounts($merchant_request_id, $merchant_service_id,
            $pay4meAmount, $currency);
        $t->is(false, $to_account_arr);





    }


    public function testGetMerchantDetailsByEwalletId()
    {
        $t = $this->getTest();

        //test case for valid merchant user it should nob be zero
        $userId = 2;
        $objMerchantDetail = Doctrine::getTable('Merchant')->getMerchantDetailsByEwalletId($userId);
        if(count($objMerchantDetail)>0)
        $result = true;
        $t->is(true, $result);


        //check functionality for negative value
        $userId = -1;
        $result = false;
        $objMerchantDetail = Doctrine::getTable('Merchant')->getMerchantDetailsByEwalletId($userId);
        if(count($objMerchantDetail)>0)
        $result = true;
        $t->is(true, $result);

        //check for numeric value
        $userId = "ABC";
        $result = false;
        $objMerchantDetail = Doctrine::getTable('Merchant')->getMerchantDetailsByEwalletId($userId);
        if(count($objMerchantDetail)>0)
        $result = true;
        $t->is(true, $result);

    }

    public function testCheckMerchantDetailsForEwallet()
    {
        $t = $this->getTest();

        //result should be true for valide user and merchant which exist
        $merchant_id=1;
        $ewalletUserId = 3365;
        $flag =Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id,$ewalletUserId);
        $t->is(false, $flag);

        //result should be false for negative value for merchant
        $merchant_id=-1;
        $ewalletUserId = 3365;
        $flag =Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id,$ewalletUserId);
        $t->is(false, $flag);
        //result should be false for negative value for merchant
        $merchant_id=1;
        $ewalletUserId = -1;
        $flag =Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id,$ewalletUserId);
        $t->is(false, $flag);

        //result should be false for both  negative value for merchant
        $merchant_id=-1;
        $ewalletUserId = -1;
        $flag =Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id,$ewalletUserId);
        $t->is(false, $flag);

        //result should be false for alphabete value in case of merchant
        $merchant_id="AA";
        $ewalletUserId = 3365;
        $flag =Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id,$ewalletUserId);
        $t->is(false, $flag);

        //result should be false for alphabete value in case of ewallet user id
        $merchant_id="ZZ";
        $ewalletUserId = "ZZ";
        $flag =Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id,$ewalletUserId);
        $t->is(false, $flag);
        //result should be false black value
        $merchant_id='';
        $ewalletUserId ='';
        $flag =Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id,$ewalletUserId);
        $t->is(false, $flag);
    }


    public function testIsBankUser(){
        $t = $this->getTest();

        ### for null  value ###
        $userId = null;
        $result = Doctrine::getTable('Bankuser')->IsBankUser($userId);
        $t->is(false, $result);


        ### for blank value ###
        $userId = '';
        $result = Doctrine::getTable('Bankuser')->IsBankUser($userId);
        $t->is(false, $result);


        ### for 0 value ###
        $userId = 0;
        $result = Doctrine::getTable('Bankuser')->IsBankUser($userId);
        $t->is(false, $result);


        ### for -ive value ###
        $userId = -1;
        $result = Doctrine::getTable('Bankuser')->IsBankUser($userId);
        $t->is(false, $result);


        ### for string  value ###
        $userId = 'a';
        $result = Doctrine::getTable('Bankuser')->IsBankUser($userId);
        $t->is(false, $result);


        ### for true  value ###
        $userId = 9;
        $result = Doctrine::getTable('Bankuser')->IsBankUser($userId);
        $t->is(true, $result);
    }

}