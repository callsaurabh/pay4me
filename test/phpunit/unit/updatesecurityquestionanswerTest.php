<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);
new sfDatabaseManager($configuration);
/* Test Case For [WP047][CR078] */
class unit_updatesecurityquestionanswerTest extends sfPHPUnitBaseTestCase
{
   
  public function testGetAnswerCount()
  {
        $t = $this->getTest();
        ### For UserId not Exist
        $userId = 2;
        $userAnswerCount = Doctrine::getTable('SecurityQuestionsAnswers')->findByUserId($userId)->Count();
        $t->is(true, $userAnswerCount>0);
        ### For Null userId  result should be null###
        $userId='';
        $userAnswerCount = Doctrine::getTable('SecurityQuestionsAnswers')->findByUserId($userId)->Count();
        $t->is(null, $result);
        ### For Negative value result shold be null
        $userId=-1;
        $userAnswerCount = Doctrine::getTable('SecurityQuestionsAnswers')->findByUserId($userId)->Count();
        $t->is(null, $result);
        ### For Existing User count value should be numric
        $userId=50;
        $userAnswerCount = Doctrine::getTable('SecurityQuestionsAnswers')->findByUserId($userId)->Count();
        $t->is(true, is_numeric($userAnswerCount));
  }
  
  public function testUserQuestionsByUserId()
  {
        $t = $this->getTest();
        ### For UserId not Exist
        $userId = 2;
        $arrSecurityQuestionsAnswers = Doctrine::getTable('SecurityQuestionsAnswers')
        ->getUserQuestionsByUserId($userId);
        $this->assertEquals(true, count($arrSecurityQuestionsAnswers)>0);
        ### For UserId  Exist but dont have security question
        $userId = 50;
        $arrSecurityQuestionsAnswers = Doctrine::getTable('SecurityQuestionsAnswers')->getUserQuestionsByUserId($userId);
        $t->is(false, count($arrSecurityQuestionsAnswers)>0);
        $userId = -1;
        $arrSecurityQuestionsAnswers = Doctrine::getTable('SecurityQuestionsAnswers')->getUserQuestionsByUserId($userId);
        $t->is(false, count($arrSecurityQuestionsAnswers)>0);
        ### For User having security question  answer three
        $userId = 'awdxsa';
        $arrSecurityQuestionsAnswers = Doctrine::getTable('SecurityQuestionsAnswers')->getUserQuestionsByUserId($userId);
        $t->is(false, count($arrSecurityQuestionsAnswers)>0);
        ### For blank user 
        $userId = '';
        $arrSecurityQuestionsAnswers = Doctrine::getTable('SecurityQuestionsAnswers')
        ->getUserQuestionsByUserId($userId);
        $this->assertEquals(0, sizeof($arrSecurityQuestionsAnswers));
  }
  
  public function testNumberOfQuestion(){
        $t = $this->getTest();
        ### For get defined number of question yml
        $number = Settings::getNumberOfQuestion(); 
        $t->is(true,  is_numeric($number));
        ### number should be equal to value defined un YML
        $number = Settings::getNumberOfQuestion(); 
        $this->assertEquals(3,$number);
  }
  
  public function testGetRandomQuestion(){
          ### count of random number should be equal
        $arrSecurityQuestionsAnswersFirst = Doctrine::getTable('SecurityQuestion')->getRandomQuestion();
        $arrSecurityQuestionsAnswersSecond = Doctrine::getTable('SecurityQuestion')->getRandomQuestion();
        $this->assertEquals(sizeof($arrSecurityQuestionsAnswersFirst), sizeof($arrSecurityQuestionsAnswersSecond));
        ### Random question number should be three
        $arrSecurityQuestionsAnswers =  Doctrine::getTable('SecurityQuestion')->getRandomQuestion();
        $this->assertEquals(3, sizeof($arrSecurityQuestionsAnswers));
        
        ### For Random number it will give question equal to pass in parameter
        $numberQuestion =2;
        $arrSecurityQuestionsAnswers =  Doctrine::getTable('SecurityQuestion')->getRandomQuestion($numberQuestion);
        $this->assertEquals($numberQuestion, sizeof($arrSecurityQuestionsAnswers));
  }
  
  
}