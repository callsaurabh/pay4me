<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);

new sfDatabaseManager($configuration);
$t = new lime_test(2);
/* Test Case For [WP029][CR046] */

$merchantWizardObj = new merchantWizard();
$t->comment('$merchantWizardObj->getSqlForMerchant($merchantId)');

$return = $merchantWizardObj->getSqlForMerchant(-1);
$t->is(false,$return, '$merchantWizardObj->getSqlForMerchant($merchantId) for -ive  merchant Id');

$return = $merchantWizardObj->getSqlForMerchant(1);
$t->is(true,$return, '$merchantWizardObj->getSqlForMerchant($merchantId) for real  merchant Id');





?>
