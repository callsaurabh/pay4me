<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EtranzactTest
 *
 * @author  Sarita
 */
require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);
$url ='http://demo.etranzact.com/WebConnect/query.jsp'; //sfConfig::get('app_et_query_url'); //.'##########';
new sfDatabaseManager($configuration);
$t = new lime_test(4);


/*Test Case For Null value */
$webPay = new webpay();
$transId = "";
$terminalId = "";
$responseArray = $webPay->getStatusMsg($transId, $terminalId, "");
$t->is(false, $responseArray, 'webpay->getStatusMsg() for null values');


/*Test Case For True Value value */
$transId = "130571957487580";
$terminalId = "0000000001";
$responseArray = $webPay->getStatusMsg($transId, $terminalId, $url);
$t->is(true, count($responseArray), 'webpay->getStatusMsg() for All true values');

/*Test Case For Invalid Transaction No */
$transId = "23423423234";
$terminalId = "0000000001";
$responseArray = $webPay->getStatusMsg($transId, $terminalId, $url);
$t->is(-1, $responseArray['SUCCESS'], 'webpay->getStatusMsg() for Invalid transaction no');

/*Test Case For Invalid terminal Id */
$transId = "23423423234";
$terminalId = "asbf";
$responseArray = $webPay->getStatusMsg($transId, $terminalId, $url);
$t->is(-1, $responseArray['SUCCESS'], 'webpay->getStatusMsg() for Invalid Terminal Id');



