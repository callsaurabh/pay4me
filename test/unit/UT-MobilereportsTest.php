<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);

new sfDatabaseManager($configuration);
$t = new lime_test(5);
/* Test Case For [WP030][CR048] */
$t->comment('EwalletTransactionTrack->getTransactionTrackCountByPlatform($platform,$accountNo,$fromDate,$toDate)');

$return=Doctrine::getTable('EwalletTransactionTrack')->getTransactionTrackCountByPlatform(null,null,null,null);
$t->is(false,$return, 'EwalletTransactionTrack->getTransactionTrackCountByPlatform($platform,$accountNo,$fromDate,$toDate) for null values');

$return=Doctrine::getTable('EwalletTransactionTrack')->getTransactionTrackCountByPlatform('Web');
$t->is(true,$return, 'EwalletTransactionTrack->getTransactionTrackCountByPlatform($platform,$accountNo,$fromDate,$toDate) for Web plat form');

$return=Doctrine::getTable('EwalletTransactionTrack')->getTransactionTrackCountByPlatform('Web','dfgdfgdgf');
$t->is(false,$return, 'EwalletTransactionTrack->getTransactionTrackCountByPlatform($platform,$accountNo,$fromDate,$toDate) for string account no');

$return=Doctrine::getTable('EwalletTransactionTrack')->getTransactionTrackCountByPlatform('Web','','asdasdasd','fgdfgdgf');
$t->is(false,$return, 'EwalletTransactionTrack->getTransactionTrackCountByPlatform($platform,$accountNo,$fromDate,$toDate) for string from date and to date');

$return=Doctrine::getTable('EwalletTransactionTrack')->getTransactionTrackCountByPlatform('Web','','2012-01-01','2012-01-01');
$t->is(false,$return, 'EwalletTransactionTrack->getTransactionTrackCountByPlatform($platform,$accountNo,$fromDate,$toDate) for future date');



?>
