<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InterSwitchTest
 *
 * @author Srikanth Reddy
 */
require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);

new sfDatabaseManager($configuration);
$t = new lime_test(4);
//////////////////////////////////
/* Test Case For [WP031][CR050] */
Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
$updateParamArr = array();
$updateParamArr['status'] = '';
$updateParamArr['amount'] = '';
$updateParamArr['code'] = '';
$updateParamArr['desc'] = '';
$updateParamArr['date'] = '';
$updateParamArr['order_id'] = '';
$return=Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
$t->is(false,$return, 'GatewayOrder->updateGatewayOrder($updateParamArr) for null values');

$updateParamArr = array();
$updateParamArr['status'] = 'Pending';
$updateParamArr['amount'] = 'sdddd';
$updateParamArr['code'] = '00';
$updateParamArr['desc'] = 'Approved by Financial instution';
$updateParamArr['date'] = '2011-03-31';
$updateParamArr['order_id'] = '130250341049054';
$return=Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
$t->is(true,$return, 'GatewayOrder->updateGatewayOrder($updateParamArr) for invalid amount');

$updateParamArr = array();
$updateParamArr['status'] = 'Pending';
$updateParamArr['amount'] = 'sdddd';
$updateParamArr['code'] = '00';
$updateParamArr['desc'] = 'Approved by Financial instution';
$updateParamArr['date'] = '2011';
$updateParamArr['order_id'] = '130250341049054';
$return=Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
$t->is(true,$return, 'GatewayOrder->updateGatewayOrder($updateParamArr) for invalid date');

$updateParamArr = array();
$updateParamArr['status'] = 'Pending';
$updateParamArr['amount'] = 1234;
$updateParamArr['code'] = '00';
$updateParamArr['desc'] = 'Approved by Financial instution';
$updateParamArr['date'] = '2011-03-31';
$updateParamArr['order_id'] = '13025034ddd12224gg1049054';
$return=Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
$t->is(true,$return, 'GatewayOrder->updateGatewayOrder($updateParamArr) for invalid order');


