<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);

new sfDatabaseManager($configuration);
$t = new lime_test(5);
//////////////////////////////////
$t->comment('EwalletPin->setPin($pin,$userId)');
$return=Doctrine::getTable('EwalletPin')->setPin(null,null);
$t->is(false,$return, 'Testing EwalletPin->setPin($pin,$userId) for null values');
$return=Doctrine::getTable('EwalletPin')->setPin();
$t->is(false,$return, 'Testing EwalletPin->setPin($pin,$userId) for no values');
$return=Doctrine::getTable('EwalletPin')->setPin(12);
$t->is(false,$return, 'Testing EwalletPin->setPin($pin,$userId) for wrong values');
$return=Doctrine::getTable('EwalletPin')->setPin(12,'uiyiu');
$t->is(false,$return, 'Testing EwalletPin->setPin($pin,$userId) for string value');
$return=Doctrine::getTable('EwalletPin')->setPin(12,-098);
$t->is(false,$return, 'Testing EwalletPin->setPin($pin,$userId) for negative userid');
//$return=Doctrine::getTable('EwalletPin')->setPin('4a7d1ed414474e4033ac29ccb8653d9b','11');
//$t->is(NULL,$return, 'Testing EwalletPin->setPin($pin,$userId) for true values');


?>
