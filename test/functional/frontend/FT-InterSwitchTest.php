<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InterSwitchTest
 *
 * @author Srikanth Reddy
 */

include(dirname(__FILE__).'/../../bootstrap/functional.php');
/* Test Case For [WP031][CR050] */
$browser = new sfTestFunctional(new sfBrowser());
/* Test Case For [WP027][CR044] */
$browser->
  get('/interswitch_configuration/InterswitchResponse')->

  with('request')->begin()->
    isParameter('module', 'interswitch_configuration')->
    isParameter('action', 'InterswitchResponse')->
  end()->

  with('response')->begin()->
  end()
;



?>
