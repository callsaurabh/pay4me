<?php

/**
 * payment_mode actions.
 *
 * @package    mysfp
 * @subpackage payment_mode
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class payment_modeActions extends sfActions {
  public function executeIndex(sfWebRequest $request) {


    $this->payment_mode_list = Doctrine::getTable('PaymentMode')
        ->createQuery('a');

    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('PaymentMode',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->payment_mode_list);
    $this->pager->setPage($this->page);
    $this->pager->init();
  }

  public function executeShow(sfWebRequest $request) {
    $this->payment_mode = Doctrine::getTable('PaymentMode')->find($request->getParameter('id'));
    $this->forward404Unless($this->payment_mode);
  }

  public function executeNew(sfWebRequest $request) {
    $this->form = new PaymentModeForm();
  }

  public function executeCreate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post'));
    $pmtMode = $request->getPostParameter('payment_mode');
    if(strlen(preg_replace('/\s+/u','', $pmtMode['name'])) == 0)
    {
      $this->form = new PaymentModeForm();
      $this->getUser()->setFlash('error', sprintf('Payment Mode cannot be blank'));
      $this->setTemplate('new');
    }else{
      //echo "haahaa"; die();
      $this->form = new PaymentModeForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('new');      
    }
    
    
  }

  public function executeEdit(sfWebRequest $request) {
    $this->forward404Unless($payment_mode = Doctrine::getTable('PaymentMode')->find($request->getParameter('id')), sprintf('Object payment_mode does not exist (%s).', $request->getParameter('id')));
    $this->form = new PaymentModeForm($payment_mode);
  }

  public function executeUpdate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($payment_mode = Doctrine::getTable('PaymentMode')->find($request->getParameter('id')), sprintf('Object payment_mode does not exist (%s).', $request->getParameter('id')));
    $this->form = new PaymentModeForm($payment_mode);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request) {
    $request->checkCSRFProtection();

    $this->forward404Unless($payment_mode = Doctrine::getTable('PaymentMode')->find($request->getParameter('id')), sprintf('Object payment_mode does not exist (%s).', $request->getParameter('id')));
    $payment_mode->delete();
    $this->getUser()->setFlash('notice', sprintf('Payment Mode deleted successfully'));

    $this->redirect('payment_mode/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
//    $form->bind($request->getParameter($form->getName()));
//    if ($form->isValid()) {
//      $payment_mode = $form->save();
//
//      $this->redirect('payment_mode/edit?id='.$payment_mode->getId());
//    }

    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid()) {
         /*   $payment_mode_params = $request->getParameter('payment_mode');
            $name = $payment_mode_params['name'];
            $id = $payment_mode_params['id'];
            $payment_mode_obj = paymentModeServiceFactory::getService(paymentModeServiceFactory::$TYPE_BASE);
            $already_created = $payment_mode_obj->chkDuplicacy($name,$id);
            if($already_created) {
                $this->getUser()->setFlash('error', sprintf('Payment Mode already exists'));
            // $this->redirect('bank_branch/index');
            }
            else //there is no duplicacy
            {*/
                $merchant = $form->save();
                if(($request->getParameter('action')) == "update") {
                    $this->getUser()->setFlash('notice', sprintf('Payment Mode updated successfully'));
                }
                else if(($request->getParameter('action')) == "create") {
                        $this->getUser()->setFlash('notice', sprintf('Payment Mode added successfully'));
                    }
                $this->redirect('payment_mode/index');

          //  }
        }

  }
}
