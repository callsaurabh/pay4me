<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $payment_mode->getid() ?></td>
    </tr>
    <tr>
      <th>Name:</th>
      <td><?php echo $payment_mode->getname() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $payment_mode->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $payment_mode->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $payment_mode->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $payment_mode->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $payment_mode->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('payment_mode/edit?id='.$payment_mode->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('payment_mode/index') ?>">List</a>
