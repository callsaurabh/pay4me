<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th width="100%" >
                <span class="floatLeft"><?php echo __('Found') ?> <b><?php echo $xml->count[0]; ?></b> <?php echo __('results matching your criteria.') ?></span>
            </th>
        </tr>
    </table>
</div>
<div class="wrapTable" style="width:100%;overflow:auto">
    <table  border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
            <tr class="horizontal">
                <th>S.No.</th>
                <th><?php echo __('From') ?></th>
                <th class="fixed_column_width"><?php echo __('To') ?></th>
                <th class="fixed_column_width" ><?php echo __('Reason') ?></th>
                <th class="fixed_column_width"><?php echo __('Date Time') ?></th>

            </tr>
        </thead>
        <tbody>
            <?php
            if (($xml->count[0]) > 0) {
                $i = 0;
                foreach ($xml->collection->item as $item):
                    $i++;
            ?>
                    <tr class="alternateBgColour">
                        <td><?php echo $i ?></td>
                        <td ><?php echo $item->FromAddress; ?></td>
                        <td ><?php echo $item->ToAddress ?></td>
                        <td class="fixed_column_width"><?php
                    $no_of_characters_to_display = 21;
                    if (strlen((string) $item->Reason) > $no_of_characters_to_display) {
                        $arry = explode(" ", (string) $item->Reason);

                        foreach ($arry as $string) {
                            if (strlen($string) > $no_of_characters_to_display) {
                                $iterate = true;
                                $offset = 0;
                                while ($iterate) {
                                    echo wordwrap(substr($string, $offset, $no_of_characters_to_display), 18, "\n", true). "<br />";
                                    $offset+=$no_of_characters_to_display;

                                    $iterate = ($offset + $no_of_characters_to_display) > strlen($string) ? false : true;
                                }
                            } else {
                                echo wordwrap( $string , 18, "\n", true). " ";
                            }
                        }
                    } else {
                        echo (string) wordwrap( $item->Reason, 18, "\n", true);
                    }
            ?></td>
                   <td class="fixed_column_width"><?php echo str_replace(array('T','Z'), " ", $item->DateTime);  ?></td>
            </tr>
            <?php endforeach; ?>


            <?php } else {
            ?> <table   border="0" cellpadding="0" cellspacing="0"  class="dataTable" >
            <tr><td  align='center'  class="error" style="background-color: white">No Record found</td></tr></table><?php } ?>
        </tbody>
    </table>


</div>

