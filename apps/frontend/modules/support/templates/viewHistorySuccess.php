<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<?php use_helper('Pagination'); ?>
<?php echo ePortal_listinghead('History Detail'); ?>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo ($pager->getNbResults() ? $pager->getFirstIndice() : '0' ) ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
            <tr align = "center" class="horizontal">
                <th width = "1%">S.No.</th>
                <th width = "15%">Response Code </th>
                <th width = "30%">Response Description</th>
                <th width = "10%">Request Time</th>
                <th width = "10%">Response Time</th>
        </thead>
        <tbody>
            <?php
            if ($pager->getNbResults() > 0) {

                $limit = sfConfig::get('app_per_page_records');
                $page = $sf_context->getRequest()->getParameter('page', 0);
                $i = max(($page - 1), 0) * $limit;
                $totalamout = '';
                foreach ($pager->getResults() as $result) {
                    $i++;
            ?>
                    <tr align = "center" class="alternateBgColour">
                        <td><?php echo $i ?></td>
                        <td><?php echo $result->getResponseCode($result->getMessageText()); ?></td>
                        <td><?php echo $result->getResponseMsg($result->getMessageText()); ?></td>
                        <td nowrap><?php echo $result->getMwRequest()->getCreatedAt() ?></td>
                        <td nowrap><?php echo $result->getUpdatedAt() ?></td>
                    </tr>
            <?php }
            } else {
 ?><table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                <tr><td colspan="5" class="error"><?php echo "No Record Found"; ?></td></tr></table>
<?php } ?>
        </tbody>
        <tr><td colspan="5">
                <div class="paging pagingFoot">
<?php echo pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . "?id=" . $requestId)) ?>
        </div>
    </td>
</tr>
</table>
</div>


