<?php echo ePortal_pagehead(" "); 
      //use_helper('Form');
 ?>
<?php echo ePortal_legend('Payment Checkout Tool'); ?>
	<div class="descriptionArea"><p>This tool provide payment status of your query .You can search by filters or parameter .</p></div>
<?php echo ePortal_legend('Select the option from which you want to search'); ?>
	<div class="descriptionArea">
    Search By Filters <input id="searchBy_filters" type="radio" checked="checked" value="filters" name="searchByFilters"/>
    &nbsp; &nbsp; &nbsp; &nbsp;
    Search By Parameter <input id="searchBy_parameters" type="radio" value="params" name="searchByFilters"/>
</div>
<div class="wrapForm2" id = "filters_form">
    <?php echo form_tag($sf_context->getModuleName().'/merchantSupportSearch', array('name'=>'pfm_report_form','id'=>'pfm_report_form', 'onsubmit' => 'return validate_report_index_form(this);')) ?>
    <div>
        <?php
        if ($report_label == "Merchant Support Section") {
            echo ePortal_legend(__('Merchant Support Section'));
        } 
        $arrUserGroup = array();
        $arrUserGroup = array(sfConfig::get('app_pfm_role_admin'),sfConfig::get('app_pfm_role_report_portal_admin'), sfConfig::get('app_pfm_role_report_admin'), sfConfig::get('app_pfm_role_ewallet_user'), sfConfig::get('app_pfm_role_portal_support'));
        echo formRowFormatRaw($form['merchant']->renderLabel(), $form['merchant']->render());
        echo formRowFormatRaw($form['service_type']->renderLabel(), $form['service_type']->render());

        if (in_array($userGroup, $arrUserGroup)) {
        	
            echo formRowFormatRaw($form['payment_mode']->renderLabel(), $form['payment_mode']->render());
            echo "</div>";
            if ($report_label == "detailed") {
                echo "<div id = 'bank_teller' name = 'bank_teller' style = 'display:none'>";
                echo formRowFormatRaw($form['teller_id']->renderLabel(), $form['teller_id']->render());
                echo "</div>";
            }
        } else {
            echo formRowFormatRaw($form['payment_mode']->renderLabel(), $form['payment_mode']->render());
            if ($report_label == "detailed") {
                echo "<div id ='bank_teller' name = 'bank_teller'>";
                echo formRowFormatRaw($form['teller_id']->renderLabel(), $form['teller_id']->render());
                echo "</div>";
            }
        }
        if (settings::isMultiCurrencyOn()) {
            echo formRowFormatRaw($form['currency_id']->renderLabel(), $form['currency_id']->render());
        }
        echo formRowComplete($form['from']->renderLabel(),$form['from']->render(),'','from','err_from','from_row',$form['from']->renderError());
        echo formRowComplete($form['to']->renderLabel(),$form['to']->render(),'','to','err_to','to_row',$form['to']->renderError());
        echo formRowFormatRaw($form['payment_status']->renderLabel(), $form['payment_status']->render());
        echo formRowFormatRaw('', $form['report_label']->render());
        echo $form['_csrf_token']->render();
        ?>
        <div class="divBlock">
            <center>
                <input type="submit" value="Generate Report" class="formSubmit" / >
            </center>
        </div>
   </div>
    </form>
    </div>

<div class = 'wrapForm3' id = "params_form">
<?php echo ePortal_legend('Select the option from which you want to search'); ?>
<div class="descriptionArea">
    Search By Transaction Number <input id="selectBy_transID" type="radio" checked="checked" value="transID" name="selectBy"/>
    &nbsp; &nbsp; &nbsp; &nbsp;
    Search By Parameter <input id="selectBy_param" type="radio" value="param" name="selectBy"/>
    &nbsp; &nbsp; &nbsp; &nbsp;
    Search By Validation Number <input id="selectBy_val" type="radio" value="param" name="selectBy"/>
</div>
<div >
<div  style="display:<?php echo $transformDisplay; ?>;"  id="div_by_transId">
<?php echo ePortal_legend('Search by Transaction Number'); ?>
    <?php echo include_partial('formByTransMerchant', array('formA' => $searchByTrans)) ?>
</div>
<div style="display:<?php echo $paramformDisplay; ?>" id="div_by_params">
<?php echo ePortal_legend('Search by Params'); ?>
    <label> <sup><b><?php echo $msg; ?></b></sup></label>
    <?php include_partial('formByParams', array('form' => $formB, 'merchant_service_id' => $merchant_service_id, 'merchant_id' => $merchant_id)) ?>
    <div style="display:none;" id="div_by_val_no">
		<?php echo ePortal_legend('Search by Validation Number'); ?>
		<?php include_partial('formByValMerchant', array('formVal' => $searchByVal)) ?>
	</div>
</div>
</div>
<script type="text/javaScript">
    
    
function validate_report_index_form() {
	if(document.getElementById('merchant').value ==''){
        alert("Please Select The Merchant");
        return false;
    }

}
    $(document).ready(function() {
		if ($('#searchBy_filters').is(':checked')){
			  $('#params_form').hide();
			  $('#filters_form').show();

		}
		if ($('#searchBy_parameters').is(':checked')){
			  $('#filters_form').hide();
			  $('#params_form').show();

		}
		$('input[type=radio]').live('change', function() { 
			if($('#searchBy_filters').is(':checked')) {
				$('#filters_form').slideDown('slow');
				$('#params_form').slideUp('slow');
			}
			if($('#searchBy_parameters').is(':checked')) {
				$('#params_form').slideDown('slow');
				$('#filters_form').slideUp('slow');
			}
		});
		

		<?php
if(isset($formValid) && $formValid == "valid"){
    ?>
         displayReport();
<?php } ?>

        function displayReport(){
            $.ajax({
                type: 'POST',
                url: 'merchantSupportSearchList',
                data: $("#pfm_report_form").serialize(),
                success: function(data){

                    if(data == "logout"){
                        location.reload();
                    }else{
                        $("#search_results").html(data);
                    }
                },
                error: function() {

                    location.reload();
                }
            });
        }

		
        var bank = $('#banks').val();
        var country = $('#country_id').val();
        var branch = $('#bank_branches').val();

        var url = "<?php echo url_for("report/getBankTeller"); ?>";

        if(bank!=''&& country!='' && branch!='')
            $("#teller_id").load(url, {
                bank: bank,country: country,branch: branch
            },function (data){

        });
        $("#payment_mode").change(function()
        {
            if($("#payment_mode").val()==1)
                $('#bank_teller').show();
            else
                $('#bank_teller').hide();
        });

        $("#bank_branches").change(function()
        {
            getTeller();
        });

        $("#country_id").change(function()
        {
            var url = "<?php echo url_for("report/getSelectedCurrency"); ?>";
            var countryId = $("#country_id").val();

            $.post(url,{country: countryId},function (data){ 
                if(data){
                    $("#currency_id").val(data);
                }
            });
       
            getTeller();
        });

        $("#banks").change(function()
        {
            getTeller();
        });
         var selectVal = "<?php echo ($selected)?$selected:'';?>";

         if(selectVal=="selectBy_val"){
            $('#'+selectVal).attr('checked',true);
            $('#div_by_transId').slideUp('slow');
            $("#div_by_params").slideUp('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_params").hide();
            $("#div_by_transId").hide();
            $("#div_by_val_no").show();
            $("#div_by_val_no").slideDown('slow');
         }else if (selectVal=="selectBy_param"){
             $('#'+selectVal).attr('checked',true);
            $('#div_by_transId').slideUp('slow');
            $("#div_by_params").slideDown('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_params").show('slow');
            $("#div_by_transId").hide();
            $("#div_by_val_no").hide();
            $("#merchant_val_transValNo").val();
         }
         else{
            $('#div_by_params').slideUp('slow')
            $("#div_by_params").hide();
            //       $("#support_params").html("");
            $("#div_by_transId").slideDown('slow');
            $("#div_by_transId").show('slow');
            $("#div_by_val_no").slideUp('slow');
            $("#div_by_val_no").hide();

            $("#support_merchant").val("");
            $("#support_merchant_service_id").val("");
            $("#merchant_val_transValNo").val();
         }


        if($("input[name='selectBy']:checked").val()=='transID' ){

            $('#div_by_params').slideUp('slow')
            $("#div_by_params").hide();
            //$("#support_params").html("");
            $("#div_by_transId").slideDown('slow');
            $("#div_by_transId").show('slow');
            $("#div_by_val_no").slideUp('slow');
            $("#div_by_val_no").hide();

            $("#support_merchant").val("");
            $("#support_merchant_service_id").val("");
            $("#merchant_val_transValNo").val();
        }

        $('#selectBy_transID').click(function(){
            $('#div_by_params').slideUp('slow')
            $("#support_merchant").val("");
            $("#support_merchant_service_id").val("");
            $("#support_params").html("");
            $("#div_by_transId").slideDown('slow');
            $("#div_by_transId").show('slow');
            $("#div_by_params").hide();
            $("#div_by_val_no").hide();
            $("#merchant_val_transValNo").val();
        });

        $('#selectBy_param').click(function(){
            $('#div_by_transId').slideUp('slow');
            $("#div_by_params").slideDown('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_params").show('slow');
            $("#div_by_transId").hide();
            $("#div_by_val_no").hide();
            $("#merchant_val_transValNo").val();
        });

        $("#selectBy_val").click(function(){
            $('#div_by_transId').slideUp('slow');
            $("#div_by_val_no").slideUp('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_val_no").slideDown('slow');
            $("#div_by_params").hide();
            $("#div_by_transId").hide();

        });
    });

    function getTeller() {
        var bank = $('#banks').val();
        var country = $('#country_id').val();
        var branch = $('#bank_branches').val();

        var url = "<?php echo url_for("report/getBankTeller"); ?>";
        $("#teller_id").load(url, {
            bank: bank,country: country,branch: branch
        },function (data){

        });
    }
     window.onload = set_service_type();
 </script>
