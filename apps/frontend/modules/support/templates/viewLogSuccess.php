<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead('Log Detail');?>


<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo ($pager->getNbResults()?$pager->getFirstIndice():'0' ) ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable_1">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
            <tr align = "center" class="horizontal">
            <th width = "1%">S.No.</th>
            <th width = "6%">Level </th>
            <th width = "8%">Logger Name</th>
            <th width = "10%">Location </th>
            <th width = "8%">Thread Name</th>
            <th width = "10%">Date</th>
            <th width = "20%">Log Message</th>

        </thead>
        <tbody>
            <?php

            if($pager->getNbResults()>0) {

                $limit = sfConfig::get('app_per_page_records');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
                $totalamout = '';
                if($bankProtocolVersion=='v2'){ 
                foreach ($pager->getResults() as $result) {

                    $i++;
                    ?>
            <tr align = "center" class="alternateBgColour">
                <td><?php echo $i ?></td>
                <td ><?php  echo $result->getLevel($result->getLogMessage());?></td>
                <td ><?php  echo chunk_split($result->getLoggerName($result->getLogMessage()),15) ?></td>
              <!--  <td><?php //echo  chunk_split($result->getLocation(),20) ?></td>-->
                <td><?php echo  "NA" ?></td>
                <td ><?php echo chunk_split($result->getThreadName($result->getLogMessage()),20)?></td>
                <td ><?php echo chunk_split($result->getTime($result->getLogMessage()),20)?></td>
                <td ><?php echo htmlentities(chunk_split($result->getMessage($result->getLogMessage()),20))?></td>
                <?php  //if($result->getLevel()=='ERROR' ): ?>
                   <!--<td ><?php //echo htmlentities(chunk_split($result->getErrorDetails(),30));?></td>-->
                 <?php  //else: ?>

                 <?php // endif; ?>
            </tr>
            <?php  } ?>
            <?php  }else { ?>
            <?php
foreach ($pager->getResults() as $result) {
                    $str =stripslashes(substr($result->getlogMessage(),2,-2));
                    $str=str_replace('""', '"', $str);
                    $strnew=str_replace('"time_stamp":', '"time_stamp":"', $str);
                    if($str!=$strnew)
                    $str=str_replace('}', '"}', $strnew);
                    $decodedResponse = (array)json_decode($str);
                    $i++;
                    ?>
            <tr align = "center" class="alternateBgColour">
                <td><?php echo $i ?></td>
                <td ><?php  echo $decodedResponse['level'];?></td>
                <td ><?php  echo chunk_split($decodedResponse['logger_name'],15) ?></td>
              <!--  <td><?php //echo  chunk_split($result->getLocation(),20) ?></td>-->
                <td><?php echo $decodedResponse['location']  ?></td>
                <td ><?php echo $decodedResponse['thread_name'] ?></td>
                <td ><?php echo $decodedResponse['time_stamp']?></td>
                <td ><?php echo $decodedResponse['log_message']?></td>
                <?php  //if($result->getLevel()=='ERROR' ): ?>
                   <!--<td ><?php //echo htmlentities(chunk_split($result->getErrorDetails(),30));?></td>-->
                 <?php  //else: ?>

                 <?php // endif; ?>
            </tr>
            <?php  } ?>
            <?php  } ?>
        <?php
    }else {?>
            <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr><td  class="error"><?php echo "No Record Found"; ?></td></tr>
            </table>
            <?php }?>

        </tbody>



        <tr><td colspan="8" align="right">
        <div class="paging pagingFoot">
        <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()."?transactionNo=".$transactionNo.'&bank_id='.$bank_id)) ?>
         <?php  //echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'/'.$url,'log_result')) ?>
        </div></td></tr>

    </table>


</div>

