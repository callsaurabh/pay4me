<?php include_stylesheets_for_form($formA) ?>
<?php include_javascripts_for_form($formA) ?>

<script type="text/javascript">
//function isSpclChar(){
//var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
//        for (var i = 0; i < document.getElementById('merchant_request_txnRef').value.length; i++) {
//                if (iChars.indexOf(document.getElementById('merchant_request_txnRef').value.charAt(i)) != -1) {
//                alert ("The search field has special characters. \nThese are not allowed.\n");
//                return false;
//        }
//                }
//}


 function isSpclChar() {
    var reg1 = /^([A-Za-z0-9])+$/;

    if(reg1.test(document.getElementById('merchant_request_txnRef').value) == true) {
        return true;
    }else{
       alert("The search field is empty or has special characters. \nPlease enter a valid input.\n");
       return false;
    }
    
  }

</script>


<div class="wrapForm2">
<form action="<?php echo url_for('support/index') ?>" method="post"  class="multiForm" id="transform" name="transform">
<?php if (!$formA->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  
  <?php echo $formA ?>
  <div class="divBlock">
      <center>
   &nbsp; <?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('support/index').'\''));?>
          <input type="submit" value="Search" onClick="return isSpclChar();" class="formSubmit" />
  </center></div>
  
</form>
</div>