<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead('Log Detail');?>


<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo ($pager->getNbResults()?$pager->getFirstIndice():'0' ) ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable_1">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
            <tr align = "center" class="horizontal">
            <th width = "1%">S.No.</th>
            <th width = "6%">Level </th>
            <th width = "6%">Logger Name</th>
            <th width = "10%">Location </th>
            <th width = "10%">Module Name</th>
            <th width = "12%">Date</th>
            <th width = "18%">Log Message</th>

        </thead>
        <tbody>
            <?php

            if($pager->getNbResults()>0) {
                $limit = sfConfig::get('app_per_page_records');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
                $totalamout = '';
				foreach ($pager->getResults() as $result) {
                    $str =stripslashes(substr($result->getlogMessage(),2,-2));
                   $str=str_replace('"', '', $str);
                    $decodedResponse = explode(",",$str);
                   // print_r($decodedResponse);die;
                    $i++;
                    ?>
            <tr align = "center" class="alternateBgColour">
                <td><?php echo $i ?></td>
                <td ><?php  echo $decodedResponse['0'];?></td>
                <td ><?php  echo $decodedResponse['1'];?></td>
              <!--  <td><?php //echo  chunk_split($result->getLocation(),20) ?></td>-->
                <td><?php  echo $decodedResponse['2'];?></td>
                <td ><?php echo $decodedResponse['3'];?></td>
                <td ><?php echo $decodedResponse['4'];?></td>
                <td ><?php echo $decodedResponse['5'];?></td>
                <?php  //if($result->getLevel()=='ERROR' ): ?>
                   <!--<td ><?php //echo htmlentities(chunk_split($result->getErrorDetails(),30));?></td>-->
                 <?php  //else: ?>

                 <?php // endif; ?>
            </tr>
            <?php  } ?>
        <?php
    }else {?>
            <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr><td  class="error"><?php echo "No Record Found"; ?></td></tr>
            </table>
            <?php }?>

        </tbody>



        <tr><td colspan="8" align="right">
        <div class="paging pagingFoot">
        <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()."?transactionNo=".$transactionNo)) ?>
         <?php  //echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'/'.$url,'log_result')) ?>
        </div></td></tr>

    </table>


</div>

