<div id="printSlip">
<?php use_helper('ePortal'); ?>




   <?php echo ePortal_pagehead("Payment Receipt ", array('class' => '_form')); ?>




    <div id='acknowledgeSlip' class='wrapForm2 wrapdiv'>

        <?php
        echo ePortal_legend('Your Parameters For Search');
        // echo formRowFormatRaw('Transaction Id',$formData['pfm_transaction_number']);
        if (count($searchParamArrLabel) > 0) {
            foreach ($searchParamArrLabel as $key => $value) {
                if (isset($searchParamArrValues[$value['mapped_to']]) && $searchParamArrValues[$value['mapped_to']] != "")
                    $val = $searchParamArrValues[$value['mapped_to']];
                else {
                    $val = "";
                }
                if ($val != "")
                    echo formRowFormatRaw($value['param_description'], $val);
                // echo $value['param_description']." : ".$searchParamArrValues[$value['mapped_to']]."  ";
            }
            echo formRowFormatRaw("Merchant Name", $searchDetail['MerchantName']);
            echo formRowFormatRaw("Service Name", $searchDetail['ServiceName']);
        }
        ?>

    </div>



    <div id='acknowledgeSlip' class='wrapForm2 wrapdiv'>

        <?php
        /* echo "<pre>";
          //print_r($paymentDetail['paramArr']);
          //print_r($searchParamArrLabel);

          //if(array_diff($paymentDetail['paramArr'],$searchParamArrLabel))
          //{ */
        ?>

        <?php
        if (0 == $TransStatus) {
//         echo ePortal_legend('Result Found For These Params.');
//          foreach($paymentDetail['paramArr'] as  $key=> $value)
//           {
//              if($key=="Amount")
//                echo formRowFormatRaw($key,format_amount($value,1));
//            else
//                echo formRowFormatRaw($key,$value);
//
//           }
        }
        ?>

        <?php
        if (0 == $TransStatus) {

            echo ePortal_legend('Payment Details');
            $msg = "MSG_ERR_" . $paymentDetail['payment_status_code'];
            $statusDesc = pay4meError::$$msg;
            echo formRowFormatRaw("Validation Number", $paymentDetail['validation_number']);
            echo formRowFormatRaw("Transaction Number", $paymentDetail['txnNo']);
            echo formRowFormatRaw("Payment Status", $statusDesc);
            echo formRowFormatRaw("Item Id", $paymentDetail['itemId']);
            echo formRowFormatRaw("Total Amount", format_amount($transResult['paid_amount'], $transResult['currency_id']));
            echo formRowFormatRaw("Payment Date", $transResult['updated_at']);
            echo formRowFormatRaw("Payment Mode", $paymentDetail['paymentMode']);
            if (array_key_exists('check_number', $paymentDetail)) {
                echo formRowFormatRaw("Cheque Number", $paymentDetail['check_number']);
                echo formRowFormatRaw("Payee Account Number", $paymentDetail['account_number']);
            }
            echo formRowFormatRaw("Bank Name", $paymentDetail['BankName']?$paymentDetail['BankName']:'NA');
            echo formRowFormatRaw("Branch Name", $paymentDetail['BranchName']?$paymentDetail['BranchName']:'NA');

            if (isset($paymentDetail['userArr'])) { //if 1.3
                echo ePortal_legend('User Details');
                echo formRowFormatRaw("User Name", $paymentDetail['userArr']['uname']);
                echo formRowFormatRaw("e-mail", $paymentDetail['userArr']['email']);
                echo formRowFormatRaw("Mobile No.", $paymentDetail['userArr']['mobile_no']?$paymentDetail['userArr']['mobile_no']:'-');
            } ////if 1.3  
        } else if (1 == $TransStatus) {
            if (isset($transResult['payment_status_code'])) {
                echo ePortal_legend('Transaction Details');
                $msg = "MSG_ERR_" . $transResult['payment_status_code'];
                $statusDesc = pay4meError::$$msg;
                echo formRowFormatRaw("Payment Status", $statusDesc);
                //  echo formRowFormatRaw("Payment Status",$merchantDetails->statusDesc);
            }
        }
        ?>

    </div>

    <?php
        if (2 == $TransStatus) {
            echo ePortal_highlight('No Result Found For Your Query.', '', array('class' => 'error'));
        }
    ?>
    </div> </div>
        <div class="paging pagingFoot"><center><?php
        echo button_to('Back', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for('support/merchantSupportSearch') . '\''));
    ?>
    <?php if (0 == $TransStatus) { ?>
                 <input type="button" style = "cursor:pointer;" value="<?php echo __("View Payment Receipt") ?>" class="formSubmit" onclick="showPrintReceipt(<?php echo $paymentDetail['txnNo'] ?>)">
    <?php } ?>
    </div>

<script>
    function showPrintReceipt(txnNo){
      var webpath = "<?php echo public_path('/index.php/', true); ?>";
      window.open(webpath+'paymentSystem/printReceipt?txn_no='+txnNo, '', 'width=900, height=700, scrollbars=1, resizable=0');
    }
    </script>