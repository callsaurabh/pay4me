<?php //use_helper('Form') ?>
<script type="text/javascript">
    $(document).ready(function(){
         var selectVal = "<?php echo ($selected)?$selected:'';?>";

         if(selectVal=="selectBy_val"){
            $('#'+selectVal).attr('checked',true);
            $('#div_by_transId').slideUp('slow');
            $("#div_by_params").slideUp('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_params").hide();
            $("#div_by_transId").hide();
            $("#div_by_val_no").show();
            $("#div_by_val_no").slideDown('slow');
         }else if (selectVal=="selectBy_param"){
             $('#'+selectVal).attr('checked',true);
            $('#div_by_transId').slideUp('slow');
            $("#div_by_params").slideDown('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_params").show('slow');
            $("#div_by_transId").hide();
            $("#div_by_val_no").hide();
            $("#merchant_val_transValNo").val();
         }
         else{
            $('#div_by_params').slideUp('slow')
            $("#div_by_params").hide();
            //       $("#support_params").html("");
            $("#div_by_transId").slideDown('slow');
            $("#div_by_transId").show('slow');
            $("#div_by_val_no").slideUp('slow');
            $("#div_by_val_no").hide();

            $("#support_merchant").val("");
            $("#support_merchant_service_id").val("");
            $("#merchant_val_transValNo").val();
         }


        if($("input[name='selectBy']:checked").val()=='transID' ){

            $('#div_by_params').slideUp('slow')
            $("#div_by_params").hide();
            //       $("#support_params").html("");
            $("#div_by_transId").slideDown('slow');
            $("#div_by_transId").show('slow');
            $("#div_by_val_no").slideUp('slow');
            $("#div_by_val_no").hide();

            $("#support_merchant").val("");
            $("#support_merchant_service_id").val("");
            $("#merchant_val_transValNo").val();
        }

        $('#selectBy_transID').click(function(){
            $('#div_by_params').slideUp('slow')
            $("#support_merchant").val("");
            $("#support_merchant_service_id").val("");
            $("#support_params").html("");
            $("#div_by_transId").slideDown('slow');
            $("#div_by_transId").show('slow');
            $("#div_by_params").hide();
            $("#div_by_val_no").hide();
            $("#merchant_val_transValNo").val();
        });

        $('#selectBy_param').click(function(){
            $('#div_by_transId').slideUp('slow');
            $("#div_by_params").slideDown('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_params").show('slow');
            $("#div_by_transId").hide();
            $("#div_by_val_no").hide();
            $("#merchant_val_transValNo").val();
        });

        $("#selectBy_val").click(function(){
            $('#div_by_transId').slideUp('slow');
            $("#div_by_val_no").slideUp('slow');
            $("#merchant_request_txnRef").val("");
            $("#div_by_val_no").slideDown('slow');
            $("#div_by_params").hide();
            $("#div_by_transId").hide();

        });
    });

</script>
<?php
echo ePortal_pagehead(" ", array('class' => '_form'));
if ($path_info == '') {
    $transRadioVal = true;
    $transformDisplay = "block";
} else {
    $transRadioVal = false;
    $transformDisplay = "none";
}
$paramRadioVal = false;
$paramformDisplay = "none";
if (1 == $searchByTrans->isBound()) {
    $transRadioVal = true;
    $transformDisplay = "block";
}
if (1 == $form->isBound()) {
    $paramRadioVal = true;
    $paramformDisplay = "block";
}
?>
<?php echo ePortal_legend('Payment Checkout Tool'); ?>
<div class="descriptionArea"><p>This tool provide payment status of your query .You can search from transaction no or parameter .</p></div>
<?php echo ePortal_legend('Select the option from which you want to search'); ?>
<div class="descriptionArea">
    Search By Transaction Number <input id="selectBy_transID" type="radio" checked="checked" value="transID" name="selectBy"/>
    &nbsp; &nbsp; &nbsp; &nbsp;
    Search By Parameter <input id="selectBy_param" type="radio" value="param" name="selectBy"/>
    &nbsp; &nbsp; &nbsp; &nbsp;
    Search By Validation Number <input id="selectBy_val" type="radio" value="param" name="selectBy"/>
</div>
<div  style="display:<?php echo $transformDisplay; ?>;"  id="div_by_transId">
<?php echo ePortal_legend('Search by Transaction Number'); ?>
    <?php include_partial('formByTrans', array('formA' => $searchByTrans)) ?>
</div>
<div style="display:<?php echo $paramformDisplay; ?>" id="div_by_params">
<?php echo ePortal_legend('Search by Params'); ?>
    <label> <sup><b><?php echo $msg; ?></b></sup></label>
    <?php include_partial('form', array('form' => $form, 'merchant_service_id' => $merchant_service_id, 'merchant_id' => $merchant_id)) ?>
    <div style="display:none;" id="div_by_val_no">
    <?php echo ePortal_legend('Search by Validation Number'); ?>
    <?php include_partial('formByVal', array('formVal' => $searchByVal)) ?>
</div>
</div>