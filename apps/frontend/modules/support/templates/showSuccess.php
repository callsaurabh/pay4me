<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $merchant_service->getid() ?></td>
    </tr>
    <tr>
      <th>Merchant:</th>
      <td><?php echo $merchant_service->getmerchant_id() ?></td>
    </tr>
    <tr>
      <th>Name:</th>
      <td><?php echo $merchant_service->getname() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $merchant_service->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $merchant_service->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $merchant_service->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $merchant_service->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $merchant_service->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>
<hr />

<a href="<?php echo url_for('merchant_service/edit?id='.$merchant_service->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('merchant_service/index') ?>">List</a>
