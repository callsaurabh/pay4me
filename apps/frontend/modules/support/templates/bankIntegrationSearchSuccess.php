<?php echo ePortal_pagehead(" "); ?>
<?php  //use_helper('Form') ?>
<script>

$(document).ready(function() {
<?php
if("valid" == $formValid ){
    ?>
         displayReport();
<?php } ?>
		
        function displayReport(){
            $.ajax({
                type: 'POST',
                url: 'bankIntegrationSearchList',
                data: $("#request_report_form").serialize(),
                success: function(data){

                    if(data == "logout"){
                        location.reload();
                    }else{
                        $("#search_results").html(data);
                    }
                },
                error: function() {

                    location.reload();
                }
            });
        }


        if(!$("#search_transaction_type_Payment").is(':checked')){

            $('#Divmerchant').hide();
            $('#transaction_no').val('');
            $('#transaction_no').hide();
            $('#validation_no').show();
            
        }
        //call function on transaction payment bank list
        $("#search_transaction_type_Payment").click(function()
        {
            $("#search_results").html('');
            $('#transaction_no').show();
            $('#Divmerchant').show();
            $('#search_validation_no').val('');
            $("select#search_status").val('');
            $('#validation_no').hide();
            var transactionType =  $('#search_transaction_type_Payment').val();
            var merchant =  $('#search_merchant').val();

            var url = "<?php echo url_for("support/getBankListForTransaction");?>";
            $("#search_bank").load(url,{transactionType:transactionType,merchant:merchant},function (data){
                if(data=='logout'){
                    location.reload();
                }
            });
        });



        //call function on transaction recharge value
        $("#search_transaction_type_Recharge_From_Bank").click(function()
        {

            $("#search_results").html('');
            $('#search_transaction_no').val('');
            $('#transaction_no').hide();
            $('#Divmerchant').hide();
            $('#validation_no').show();
            $("select#search_status").val('');
            $('#merchant').val('');
            var transactionType =  $('#search_transaction_type_Recharge_From_Bank').val();
            var url = "<?php echo url_for("support/getBankListForTransaction");?>";
            $("#search_bank").load(url,{transactionType:transactionType},function (data){

                if(data=='logout'){
                    location.reload();
                }
            });
        });



        //call function on transaction payment bank list
        $("#search_merchant").change(function()
        {

            var merchant =  $('#search_merchant').val();
            var transactionType =  $('input[name=search[transaction_type]]:checked').val();
            var url = "<?php echo url_for("support/getBankListForTransaction");?>";

            if(transactionType =='Payment'){
                $("#search_bank").load(url,{transactionType:transactionType,merchant:merchant},function (data){
                    if(data=='logout'){
                        location.reload();
                    }
                });
            }
        });

    });
function validateBank() {
	if($('#search_bank').val() == ''){
		alert('Please Select Bank');
		return false;
	}
}
</script>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/bankIntegrationSearch','name=request_report_form id=request_report_form');?>
    <?php echo ePortal_legend('Bank Integration Support Section');?>
    <?php echo formRowComplete($form['transaction_type']->renderLabel().'<sup class=cRed>*</sup>',$form['transaction_type']->render(),'','transaction_type','err_transaction_type','transaction_type_row',$form['transaction_type']->renderError());?>
    <?php echo formRowComplete($form['status']->renderLabel(),$form['status']->render(),'','status','err_status','status_row',$form['status']->renderError());?>

    <?php
    echo "<div id = 'Divmerchant' name = 'Divmerchant' >";
    echo formRowComplete($form['merchant']->renderLabel(),$form['merchant']->render(),'','merchant','err_merchant','merchant_row',$form['merchant']->renderError());
    echo "</div>";
    ?>
    <?php echo formRowComplete($form['bank']->renderLabel().'<sup class=cRed>*</sup>',$form['bank']->render(),'','bank','err_bank','bank_row',$form['bank']->renderError());?>

    <?php
    echo "<div id = 'transaction_no' name = 'transaction_no' >";
    echo formRowComplete($form['transaction_no']->renderLabel(),$form['transaction_no']->render());
    echo "</div>";
    ?>
    <?php
    echo "<div id = 'validation_no' name = 'validation_no' style = 'display:none'>";
    echo formRowComplete($form['validation_no']->renderLabel(),$form['validation_no']->render());
    echo "</div>";
    echo formRowComplete($form['from']->renderLabel(),$form['from']->render(),'','from','err_from','from_row',$form['from']->renderError());
    echo formRowComplete($form['to']->renderLabel(),$form['to']->render(),'','to','err_to','to_row',$form['to']->renderError());?>
    <div class="divBlock">
        <center>
            <?php echo $form[$form->getCSRFFieldName()]->render(); ?>
            <input type="Submit" value='Search' class="formSubmit" onclick ="return validateBank();" >
        </center>
    </div>
    </form>
</div>

<div class="load_overflow">
    <span id="search_results"></span>
</div>

