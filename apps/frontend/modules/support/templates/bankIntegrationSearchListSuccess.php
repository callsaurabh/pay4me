<?php
use_helper('Pagination');
echo ePortal_pagehead(" ");
$time = strtotime(date("Y-m-d H:i:s", time() - ($reattemptTime * 60)));
if (!empty($reattemptTime)) {
?>
    <br>
    <div><div id="notice"><span class="cRed">NOTICE:</span>
            Re-attempt option will come after <?php echo $reattemptTime ?> minutes of last attempt.
        </div><br/>

    <?php } ?>

    <?php echo ePortal_listinghead('Bank Integration Support List'); ?>
    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr class="alternateBgColour">
                <th>
                    <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                    <span class="floatRight">Showing <b><?php echo ($pager->getNbResults() ? $pager->getFirstIndice() : '0' ) ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                </th>
            </tr>
        </table>
        <br class="pixbr" />
    </div>
    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable mrgn0" >
            <?php
            if (($pager->getNbResults()) > 0) {
                $limit = sfConfig::get('app_records_per_page');
                $page = $sf_context->getRequest()->getParameter('page', 0);
                $i = max(($page - 1), 0) * $limit;
            ?>
                <thead>
                    <tr class="horizontal">
                    <?php if ($transactionType == 'Payment') {
                    ?>
                        <th>Merchant Name</th>
                        <th>Transaction Number</th>
                    <?php } else {
 ?>

                        <th>Bank Name</th>
                        <th>Validation Number</th>
<?php } ?>
                    <th>Status</th>
                    <th>Total Amount</th>
                    <th>Account Number</th>
                    <th>Transaction Date</th>
                    <th width="10%">Number of Attempt</th>
                    <th width="12%">Action</th>
                </tr>
            </thead>
            <?php ?>
            <?php
                    foreach ($pager->getResults() as $result):
                        $i++;
                        if ($result->getTransactionBankPosting()->getState() == 'not_posted') {
                            $statusList = "Not Processed";
                            $statusCode = 0;
                        } elseif ($result->getTransactionBankPosting()->getState() == 'posted_to_queue') {
                            $statusList = "Under Processing";
                            $statusCode = 1;
                        } elseif ($result->getTransactionBankPosting()->getState() == 'success') {
                            $statusList = "Success";
                            $statusCode = 2;
                        } else {
                            $statusList = "Failure";
                            $statusCode = 3;
                        }
            ?>
                        <tr>
<?php if ($transactionType == 'Payment') { ?>
                            <td align="center" ><?php echo $result->getMerchantName($result->getMessageText()); ?></td>
                            <td align="left"><?php echo $result->getPay4meTransactionNumber($result->getMessageText()); ?></td>
<?php } else { ?>
                            <td align="left"><?php
                            echo $result->getBank($result->getMessageText());
?></td>
                        <td align="left"><?php echo $result->getPay4meTransactionNumber($result->getMessageText()); ?></td>
<?php } ?>
                        <td align="left"><?php echo $statusList; ?></td>
                        <td align="left" ><?php echo format_amount($result->getTotalAmount($result->getMessageText())/100, 1); ?></td>
                        <td align="left"><?php echo $result->getAccountNumber($result->getMessageText()); ?></td>
                        <td align="left"><?php echo $result->getTransactionDate($result->getMessageText()); ?></td>
                        <td align="left" ><?php echo $result->getTransactionBankPosting()->getNoOfAttempts(); ?></td>
                        <td align="left">
                            <?php if (strtotime($result->getTransactionBankPosting()->getUpdatedAt()) < $time) {
 ?>
                        <a href="javascript:;"
                           onclick="reAttempt(<?php echo $result->getId() . "," . $statusCode . ",".$result->getPay4meTransactionNumber($result->getMessageText()); ?>);"
                           title="Re-attempt"><?php echo image_tag('reattempt.gif', array('alt' => 'Re-attempt', 'border' => 0)) ?></a>

                    <?php } ?>
                             <?php
                    if ($transactionType == "Payment") {
                        $transNo = $result->getPay4meTransactionNumber($result->getMessageText());
                    } else {
                        $transNo = $result->getTransactionBankPosting()->getMessageTxnNo();
                    }

                    echo link_to(image_tag('view_response_log.gif'), url_for('support/viewHistory?id=' . $result->getId()), array('popup' => array('popupWindow', 'width=870,height=500,left=150,top=0,scrollbars=yes'), 'title' => 'View Response History'));
                    echo "&nbsp;&nbsp;";
                    echo link_to(image_tag('view_log.gif', array('alt' => 'View Log', 'border' => 0)), url_for('support/middlewareViewLog?transactionNo=' . $transNo.'&bank_id='.$result->getBank($result->getMessageText())), array('popup' => array('popupWindow', 'width=870,height=500,left=150,top=0,scrollbars=yes'), 'title' => 'View Log'));
                    echo "&nbsp;&nbsp;";
                    echo link_to(image_tag('view_credentials.gif', array('alt' => 'View Request XML', 'border' => 0)), url_for('support/BankTransactionXML?id=' . $result->getId().'&type='.$transactionType), array('popup' => array('popupWindow', 'width=910,height=595,left=150,top=0,scrollbars=yes'), 'title' => 'View Request XML'));
              ?>  <?php endforeach; ?>
                <?php
                        $url = "";
                        if ($transactionType != "") {
                            $url .= "transaction_type=" . $transactionType;
                        }
                        if ($transactionNo != "") {
                            $url .= "&" . "transaction_no=" . $transactionNo;
                        }
                        if ($validationNo != "") {
                            $url .= "&" . "validation_no=" . $validationNo;
                        }
                        if ($merchant != "") {
                            $url .= "&" . "merchant=" . $merchant;
                        }
                        if ($bank != "") {
                            $url .= "&" . "bank=" . $bank;
                        }
                        if ($status != "") {
                            $url .= "&" . "status=" . $status;
                        }
                        if ($fromDate != "") {
                            $url .= "&" . "from=" . $fromDate;
                        }
                        if ($toDate != "") {
                            $url .= "&" . "to=" . $toDate;
                        }
                ?>
                    <tfoot>
                        <tr><td colspan="8"><div class="paging pagingFoot">
<?php echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . '?' . $url), 'search_results') ?>
                        </div></td></tr>
            </tfoot>
<?php } else { ?>
                        <tr><td  align='center' class='error' >No Record Found</td></tr>
<?php } ?>
                </table>
            </div>
            <script>
                function reAttempt(requestId,statusId,transactionId){
                    if(statusId==2) {
                        var flg= confirm('This Transaction is already success. Do you really want to re-attempt ?');
                        if(flg!=true){
                            return false;
                        }
                    }
                    var page =<?php echo $page; ?>;
            var str =  $("#request_report_form").serialize()+"&id="+requestId+"&transactionId="+transactionId+"&page="+page;
            $.ajax({
                type: 'POST',
                url: 'bankIntegrationSearchList',
                data: str,
                success: function(data){
                    if(data == "logout"){
                        location.reload();
                    }else{
                        $("#search_results").html(data);
                    }
                },
                error: function() {
                    location.reload();
                }
            });
        }
    </script>
