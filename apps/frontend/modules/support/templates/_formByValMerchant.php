<?php include_stylesheets_for_form($formVal) ?>
<?php include_javascripts_for_form($formVal) ?>
<script type="text/javascript">
 function isSpclCharVal() {
    var reg1 = /^([A-Za-z0-9])+$/;

    if(reg1.test(document.getElementById('merchant_val_transValNo').value) == true) {
        return true;
    }else{
       alert("The search field is empty or has special characters. \nPlease enter a valid input.\n");
       return false;
    }
    
  }
</script>
<div class="wrapForm2">
<form action="<?php echo url_for('support/merchantSupportSearchList') ?>" method="post"  class="multiForm" id="transform" name="transform">
<?php if (!$formVal->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <?php echo $formVal ?>
  <div class="divBlock">
      <center>
   &nbsp; <?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('support/merchantSupportSearch').'\''));?>
          <input type="submit" value="Search" onClick="return isSpclCharVal();" class="formSubmit" />
  </center></div>
</form>
</div>
