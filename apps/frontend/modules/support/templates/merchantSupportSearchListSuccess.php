<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
<div class="load_overflow">
    <span id="search_results"></span>
</div>
<?php if(isset($templatCheck)) { ?>

  <br>
    <div><div id="notice"><span class="cRed">NOTICE:</span>
            Re-attempt option will come after 10 minutes of last attempt.
        </div><br/>

<?php } ?>

<?php if(!isset($templatCheck)){ ?>
<?php echo ePortal_listinghead('Merchant Support Section-->Search by Filters'); ?>
<?php if(($pager->getNbResults())>0) { ?>
<?php } ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
      <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('MerchantSupportCSV', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
    </th>
    </tr>
  </table>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
         <tr class="horizontal">
          <th>Service Type</th>
          <th>Payment Mode</th>
          <th>Transaction Number</th>
          <th>Validation Number</th>
		  <th>Status</th>
		  <th>Amount</th>
		  <th>Transaction Date</th>
		  <th width="12%">Action</th>
        </tr>
   </thead>
  <tbody>
    <?php
    //echo "------".$pager->getNbResults()."------------";die;
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
$originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      //print '<pre>'; print_r($pager->getResults()->toArray());die;
      $i = 0;
      foreach ($pager->getResults(Doctrine::HYDRATE_RECORD) as $result) {
		$pfm_transaction_number = $result->getTransaction()->toArray();
		if(isset($pfm_transaction_number['0']['pfm_transaction_number'])){
			$transaction_number = $pfm_transaction_number['0']['pfm_transaction_number'];
		}
		if(isset($pfm_transaction_number['0']['transaction_number'])){
			$transaction_number = $pfm_transaction_number['0']['transaction_number'];
		}
        //$d = $result->getSfGuardUser()->getUserDetail();
        //$b = $result->getBank()->getBankBranch();
        //$g = $result->getSfGuardUser()->getsfGuardUserGroup();
        $i++;
        $url = 'support/searchfrmtransno/txnRef/TransFormVal/1';
        ?>
        <?php
        $valNo =  $result->getValidationNumber();
        	if(!isset($valNo)){
				$valNo = ' ';
			}
        ?>
    <tr align = "center" class="alternateBgColour">
    	<td align="center" ><?php echo $result->getMerchantService(); ?></td>
                            <td align="left"><?php echo $result->getPaymentModeOption()->getName(); ?></td>
							<td align="left"><?php echo link_to($transaction_number, url_for('support/searchSupportfrmtransno').'/txnRef/'.$transaction_number.'/TransFormVal/1/txnFilter/1', array('popup' => array('popupWindow', 'width=870,height=500,left=150,top=0,scrollbars=yes'), 'title' => 'Transaction Number')); ?></td>
							<td align="left" ><?php echo link_to($valNo, url_for('support/searchSupportValno').'/txnVal/'.$result->getValidationNumber().'/TransFormVal/1/txnFilter/1', array('popup' => array('popupWindow', 'width=870,height=500,left=150,top=0,scrollbars=yes'), 'title' => 'Validation Number')); ?></td>
							<td align="left"><?php
							if($result->getNotificationStatusCode() =='1'){
							  echo 'Pending';
							  }
							elseif($result->getNotificationStatusCode() =='0'){
                echo "Success";
                }
                elseif($result->getNotificationStatusCode() =='2' ||$result->getNotificationStatusCode() =='4'){
                echo "Failure";
                }
                elseif($result->getNotificationStatusCode() =='3'){
                  echo "Success";
                }
                ?></td>
							<td align="left"><?php echo format_amount($result->getPaidAmount(), $result->getCurrencyId()); ?></td>
							<td align="left" ><?php echo $result->getPaidDate(); ?></td>
							<?php  if ($result->getPaymentStatusCode() == '1') { ?>
								<td align="left"> <a href="javascript:;"  onclick="return false"
                           title="Re-attempt"><?php echo image_tag('reattempt_disabled.gif', array('alt' => 'Re-attempt', 'border' => 0)) ?></a>
							
                           <?php } 
                           else { ?>
                           			<td align="left"> <a href="javascript:;"  onclick="reAttemptMerchant(<?php echo $transaction_number; ?>,<?php echo $result->getNotificationStatusCode(); ?>);"
                           title="Re-attempt"><?php echo image_tag('reattempt.gif', array('alt' => 'Re-attempt', 'border' => 0)) ?></a>
                          <?php } 
                          
                          
                           echo link_to(image_tag('view_log.gif', array('alt' => 'View Log', 'border' => 0)), url_for('support/merchantSupportViewLog?transactionNo='.$transaction_number), array('popup' => array('popupWindow', 'width=900,height=500,left=150,top=0,scrollbars=yes'), 'title' => 'View Log'));
                           echo "&nbsp;&nbsp;";
                           echo link_to(image_tag('view_credentials.gif', array('alt' => 'View Request XML', 'border' => 0)), url_for('support/MerchantTransactionXML?transNo='.$transaction_number), array('popup' => array('popupWindow', 'width=910,height=595,left=150,top=0,scrollbars=yes'), 'title' => 'View Request XML'));
                           ?>
                          </td>
      <?php
      }
     Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);
     ?>

    </tr>
    <?php
    }else{ ?><table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
    <?php } ?>

  <tfoot>
  <tr><td colspan="10"><div class="paging pagingFoot floatRight"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td></tr>
   </tfoot></tbody>
</table>
</div>

<div class="paging pagingFoot"><center><?php  echo button_to('Back','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('support/merchantSupportSearch').'\''));?></center></div>
</div>
<?php } ?>
<script>
function reAttempt(requestId,statusId){
    if(statusId==3) {
        var flg= confirm('This Transaction is already success. Do you really want to re-attempt ?');
        if(flg!=true){
            return false;
        }
    }
    var page =<?php echo $page; ?>;
var str = $("#pfm_report_form").serialize()+"&id="+requestId+"&page="+page;;
$.ajax({
type: 'POST',
url: 'merchantSupportSearchList',
data: str,
success: function(data){
    if(data == "logout"){
        location.reload();
    }else{
        $("#search_results").html(data);
    }
},
error: function() {
    location.reload();
}
});
}

function reAttemptMerchant(transactionID,statusId){
	 $("#search_results").hide();
	if(statusId=='3'||statusId=='0') {
        var flg= confirm('This Transaction is already success. Do you really want to re-attempt ?');
        if(flg!=true){
            return false;
        }
    }
    var url_var = 'merchantSupportSearchList'
	$.ajax({
        type: 'POST',
        url: url_var,
        data:
        {
            "transactionID" : transactionID
        },
        success: function(data){
            if(data == "logout"){
                location.reload();
            }else{
                $("#search_results").html(data);
                $("#search_results").show();
            }
        },
        error: function() {

           // location.reload();
        }
    });

}

function viewLogMerchant(transactionID,statusId){
	 $("#search_results").hide();
	if(statusId=='3'||statusId=='0') {
      var flg= confirm('This Transaction is already success. Do you really want to re-attempt ?');
      if(flg!=true){
          return false;
      }
  }
  var url_var = 'merchantSupportSearchList'
	$.ajax({
      type: 'POST',
      url: url_var,
      data:
      {
          "transactionID" : transactionID
      },
      success: function(data){
          if(data == "logout"){
              location.reload();
          }else{
              $("#search_results").html(data);
              $("#search_results").show();
          }
      },
      error: function() {

         // location.reload();
      }
  });

}


function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>

