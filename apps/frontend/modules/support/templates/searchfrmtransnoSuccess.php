<?php //use_helper('Form')   ?>
<div id="printSlip">
<?php echo ePortal_pagehead("Payment Receipt  ", array('class' => '_form')); ?>



<?php if ($transReqStatus) {
?>
    <div class="wrapForm2 wrapdiv"><?php echo ePortal_legend('Search Result from Transaction Number: ' . $SerachedTransNo); ?>
    <?php
    echo "<div>" . formRowFormatRaw("Merchant Name", $searchDetail['MerchantName']) . "</div>";
    echo "<div>" . formRowFormatRaw("Service Name", $searchDetail['ServiceName']) . "</div>";
    ?>
</div>
<?php
    if (isset($paymentDetail['payment_status_code']) && $paymentDetail['payment_status_code'] == 0) {
?>
        <div class="wrapForm2 wrapdiv">
    <?php echo ePortal_legend('Payment Detail '); ?>
    <?php
        if ($SerachedTransNo != $paymentDetail['txnNo']) {
    ?>
    <?php echo ePortal_legend('The payment has been made from transaction Number :' . $paymentDetail['txnNo']); ?>
    <?php
        }
    ?>
        <dl>
            <div class="dsTitle4Fields"><label>Transaction Number <br><sup>(from which payment has been made)</sup></label></div>
            <div class="dsInfo4Fields"><?php echo $paymentDetail['txnNo'] ?><br>&nbsp;</div>
        </dl>
    <?php
        echo formRowFormatRaw("Validation Number", $paymentDetail['validation_number']);
        foreach ($paymentDetail['paramArr'] as $key => $value) {
            if ($key == "Amount")
                echo formRowFormatRaw($key, format_amount($value, 1));
            else
                echo formRowFormatRaw($key, $value);
        }
        $msg = "MSG_ERR_" . $paymentDetail['payment_status_code'];
        $statusDesc = pay4meError::$$msg;

        echo formRowFormatRaw("Payment Status", $statusDesc);
        echo formRowFormatRaw("Item Id", $searchDetail['itemId']);
        echo formRowFormatRaw("Total Amount", format_amount($transResult['paid_amount'], $transResult['currency_id']));
        echo formRowFormatRaw("Payment Date", $transResult['updated_at']);
        echo formRowFormatRaw("Payment Mode", $paymentDetail['paymentMode']);
        if (array_key_exists('check_number', $paymentDetail)) {
            echo formRowFormatRaw("Cheque Number", $paymentDetail['check_number']);
            echo formRowFormatRaw("Payee Account Number", $paymentDetail['account_number']);
        }

        echo formRowFormatRaw("Bank Name", $paymentDetail['BankName']?$paymentDetail['BankName']:'NA');
        echo formRowFormatRaw("Branch Name", $paymentDetail['BranchName']?$paymentDetail['BranchName']:'NA');
        if (isset($paymentDetail['userArr'])) {
    ?></div><div class="wrapForm2 wrapdiv"><?php
            //if 1.3
            echo ePortal_legend('User Detail ');
            echo formRowFormatRaw("User Name", $paymentDetail['userArr']['uname']);
            echo formRowFormatRaw("e-mail", $paymentDetail['userArr']['email']);
            echo formRowFormatRaw("Mobile No.", $paymentDetail['userArr']['mobile_no']?$paymentDetail['userArr']['mobile_no']:'-');
        } ////if 1.3
    ?>
    </div>
<?php
    }  //if 1.1
    else {
?>
        <div id="flash_error" class="error_list">
            <span>No payment has been received till now !</span>
        </div>
<?php
    }
}  //if 1.0
else {
?><?php echo ePortal_legend('Search Result from Transaction Number: ' . $SerachedTransNo); ?>
    <div id="flash_error" class="error_list">
        <span>No Request has been received  till now !</span>
    </div>
<?php
}
?>
  </div>  </div><div class="paging pagingFoot"><center><?php
echo button_to('Back', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for('support/index') . '\''));
?>
<?php if ($transReqStatus && !$transResult['payment_status_code']) {
?>
        <input type="button" style = "cursor:pointer;" value="<?php echo __("View Payment Receipt") ?>" class="formSubmit" onclick="showPrintReceipt(<?php echo $paymentDetail['txnNo'] ?>)">
<?php } ?>
</div>   



<script>
    function showPrintReceipt(txnNo){
      var webpath = "<?php echo public_path('/index.php/', true); ?>";
      window.open(webpath+'paymentSystem/printReceipt?txn_no='+txnNo, '', 'width=900, height=700, scrollbars=1, resizable=0');
    }
    </script>