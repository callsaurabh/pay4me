<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<div class="wrapForm2">
    <?php echo ePortal_listinghead('Search on Failed Mails'); ?>


    <form action="<?php echo url_for('support/searchFailedJobs'); ?>" method="post" id="seach_failed_jobs" >
        <?php
        echo formRowComplete($form['startDate']->renderLabel(), $form['startDate']->render(), '', 'to', 'err_startDate', 'startDate_row', $form['startDate']->renderError());
        echo formRowComplete($form['endDate']->renderLabel(), $form['endDate']->render(), '', 'to', 'err_endDate', 'endDate_row', $form['endDate']->renderError());
        ?><?php echo $form['_csrf_token'] ?>
        <div align="center"><br /><br /><input type="button" value="<?php echo __("Search"); ?>" class="formSubmit" onclick="return validateSearchForm()"/></div>
    </form>
    <br />
    <br />
    <div id="results" align="center">
    </div>
</div>
<script>
    function validateSearchForm()
    {
        err=0;

        if($('#startDate_date').val() == ""){
            $('#err_startDate').html("Please select From Date");
            $("#results").html('');
            err = err+1;

        }
        else{

            if (compare_with_today($('#startDate_date').val())) {

                $('#err_startDate').html("");
            }
            else{
                $('#err_startDate').html("Cannot be Future Date");
                $("#results").html('');
                err = err+1;
            }

        }

        if($('#endDate_date').val() == ""){
            $('#err_endDate').html("Please select To Date");
            $("#results").html('');
            err = err+1;

        }
        else{

            if (compare_with_today($('#endDate_date').val())) {

                $('#err_endDate').html("");
            }
            else{
                $('#err_endDate').html("Cannot be Future Date");
                $("#results").html('');

                err = err+1;
            }

        }
        if($('#startDate_date').val() != "" && $('#endDate_date').val() != "")
        {
            //alert($('#startDate_date').val() != "" && $('#endDate_date').val() != "");

            //return false;
            // var startDate = new Date;
            var entered_date = $('#startDate_date').val().split("-") ;
            var today=new Date();
            today.setHours(0,0,0, 0);
            var   startDate=  new Date(entered_date[0], entered_date[1]-1, entered_date[2], 0, 0, 0, 0);

           
            var entered_date = $('#endDate_date').val().split("-") ;
            var   endDate=  new Date(entered_date[0], entered_date[1]-1, entered_date[2], 0, 0, 0, 0);

            if(startDate>endDate && endDate<today)
            {

                $('#err_endDate').html('To Date should be greater than From Date');
                $("#results").html('');
                err++;
            }
        }
        if(err!=0)
        {
            return false;
        }
        else
        {
            $('#results').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
            $.post('getFailedJobsList',$("#seach_failed_jobs").serialize(), function(data){

                if(data=='logout'){
                    location.reload();
                }
                else {

                    $("#results").html(data);

                }
            });
            //return false;
        }

        //$('#seach_failed_jobs').submit();

    }
    function compare_with_today(date){
        var today = new Date();
        var entered_date = date.split("-") ;

        today.setHours(0, 0, 0, 0);

        var dob = new Date(entered_date[0], entered_date[1]-1, entered_date[2], 0, 0, 0, 0);
        if (dob > today) {
            return false;
        }
        else{
            return true;
        }

    }
</script>



