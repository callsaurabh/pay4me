<?php echo ePortal_pagehead(" "); ?>
<?php  //use_helper('Form') ?>


<div class="wrapForm2">
    <?php echo ePortal_legend('Modify Logging level');?>
    <?php echo form_tag($sf_context->getModuleName().'/setLogginglevel','name=request_report_form id=request_report_form');?>
    <?php echo formRowComplete($form['bank']->renderLabel(),
            $form['bank']->render(),'','bank','error_bank','bank_row',$form['bank']->renderError());?>
    <div id="show_detail" style="display:none">
    <dl id=><div class="dsTitle4Fields"><label for=><label for="bank">Bank Code:</label></label></div>
        <div class="dsInfo4Fields">
    <br /><div id="bank_code"></div></div></dl>

    <dl id=><div class="dsTitle4Fields"><label for=><label for="bank">Bank Key:</label></label></div><div class="dsInfo4Fields">
    <br /><div id="bank_key"></div></div></dl>
    <?php echo formRowComplete($form['level']->renderLabel(),$form['level']->render(),'','level','err_level','level_row',$form['level']->renderError());?>
        
   </div>
   <div class="divBlock">
        <center>
            <?php echo $form[$form->getCSRFFieldName()]->render(); ?>
            <input type = "Button" name="btnButton"  onclick="validateLoggingForm()" value="Modify Log Level" class="formSubmit" >

        </center>
    </div>
    <div class="form_nav_outer">
    <center>
    <?php echo image_tag('../img/ajax-loader.gif', array('id' => 'chargeLoader', 'style' => 'display:none')); ?>
    </center>
   </div>
    </form>
</div>
<div class="load_overflow">

</div>

<script>
    
    function validateLoggingForm(){
        err = 0;
        $('#error_bank').html('');
        $('#error_err_level').html('');
        if($('#loggingLevel_bank').val() == ''){
            $('#error_bank').html('Please select Bank');
            err = err+1;
        }
        if($('#loggingLevel_level').val() == '' && $('#loggingLevel_bank').val() != ''){
            err = err+1;
            $('#err_level').html('Please Select Logging Level');
         }
         if(err == 0){
             document.request_report_form.submit();
         }else{
           return false;  
         }
    }
  
    $(document).ready(function() {
        //call function on transaction recharge value
        var url_bi = "<?php echo url_for("support/findbiVersion");?>"; // Bug:36224 [02-01-2013]
        var url = "<?php echo url_for("support/fetchbankDetail");?>";
        var bank =  $('#loggingLevel_bank').val();//alert(bank)
        if(bank) {
             $.post(url_bi,{bank:bank},function (data){
                   if(data == 'v2'){
                      $("#chargeLoader").show();
                      $(".divBlock").hide();
                   }
                });
             $.post(url,{bank:bank},function (data){
                    dataArray = data.split('$$$');
                    if(dataArray[0]=="sucess"){
                        $("#show_detail").show();
                        $("#bank_code").html(dataArray[1]);
                        $("#bank_key").html(dataArray[2]);
                        $("select#loggingLevel_level").val(dataArray[3]);
                        if(dataArray[4] == 'v2')
                            $("#loggingLevel_level option[value='FATAL']").remove();
                        else if (!$("#loggingLevel_level option[value='FATAL']").text())
                            $("#loggingLevel_level").append('<option value="FATAL">Fatal</option>');
                    } else {
                        location.reload();
                    }
                    $("#chargeLoader").hide();
                    $(".divBlock").show();

            });
        }

       
        $("#loggingLevel_bank").change(function()
        { 
            var url_bi = "<?php echo url_for("support/findbiVersion");?>"; // Bug:36224 [02-01-2013]
            var bank =  $('#loggingLevel_bank').val();//alert(bank)
            var url = "<?php echo url_for("support/fetchbankDetail");?>";
            $("#err_bank").html('');
            if(bank!="") {
                $.post(url_bi,{bank:bank},function (data){
                   if(data == 'v2'){
                      $("#chargeLoader").show();
                      $(".divBlock").hide();
                   }
                });
                $.post(url,{bank:bank},function (data){
                    dataArray = data.split('$$$');
                    if(dataArray[0]=="sucess"){
                        $("#show_detail").show();
                        $("#bank_code").html(dataArray[1]);
                        $("#bank_key").html(dataArray[2]);
                        $("select#loggingLevel_level").val(dataArray[3]);
                        if(dataArray[4] == 'v2')
                            $("#loggingLevel_level option[value='FATAL']").remove();
                        else if (!$("#loggingLevel_level option[value='FATAL']").text()){
                            $("#loggingLevel_level").append('<option value="FATAL">Fatal</option>');
                            $("select#loggingLevel_level").val(dataArray[3]);
                        }
                      
                    } else {                    
                        location.reload();
                    }
                    $("#chargeLoader").hide();
                    $(".divBlock").show();
            });
                
            }else{
                 $("#show_detail").hide();
            }
            

        });

    });

</script>