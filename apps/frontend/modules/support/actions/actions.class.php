<?php

/**
 * merchant_service actions.
 *
 * @package    mysfp
 * @subpackage merchant_service
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class supportActions extends sfActions {

	public static $MERCHANT_INTEGRATION_SUPPORT  = "/report/merchantSupportSearch";
//    public function executeCommandRequest(sfWebRequest $request) {
//
//        $command = "osgi:list";
//        $logged_in_user = sfContext::getInstance()->getUser()->getGuardUser();
//        $bankId = 1; //We will get Bank Id From Request Parameters
//        $bi_protocolVersion = Doctrine::getTable("Bank")->find($bankId)->getBiProtocolVersion();
//
//        $versionManagerObj = "BankIntegration" . $bi_protocolVersion . "Manager";
//        $mgr_obj = new $versionManagerObj();
//        $mgr_obj->addCommandRequest($bankId, $command);
////Posting message to activemq
//        //We need to set input queuename and output queue name for command messages for a bank
//        /*        $bankIntegraionObject = new bankIntegration();
//          $bankIntegraionObject->QueueProcessing($mgr_obj);
//         */
//    }

    public function executeIndex(sfWebRequest $request) {
        $this->merchant_id = 0; //initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
        $this->merchant_service_id = 0; //same as the reason above
        $this->selected = '';
        $this->form = new SupportForm();
        $this->searchByTrans = new SupportTransID();
        $this->seachByKey = $request->getGetParameter('searchBy');
        if ($request->hasParameter('selected')) {
            $this->selected = $request->getParameter('selected');
        }
        /* For Search by Validation Number */
        $this->searchByVal = new SupportValNo();

        $this->msg = "";
        $this->path_info = '';

        if ($request->isMethod('post') && $this->searchByTrans->getDefault('TransFormVal') == 1) {
            $this->searchByTrans->bind($request->getParameter('merchant_request'));

            if ($this->searchByTrans->isValid())
                $this->redirect('support/searchfrmtransno?' . http_build_query($this->searchByTrans->getValues()));
        }
    }


    /* -- Search by validation number -- */

    public function executeSearchValno(sfWebRequest $request) {

        $this->searchByVal = new SupportValNo();
        ini_set("memory_limit", "50M");

        $getAllVal = $request->getParameterHolder()->getAll();
        $this->valNo = $getAllVal['merchant_val']['transValNo'];
        $resQry = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmValNo($this->valNo);

        if (count($resQry) > 0) {
            $this->result = $resQry;
            $this->transReqStatus = true;

            $resArr = $resQry->fetchArray();
            $this->transResult = $resArr[0];
            $searchDetail = $this->getfullDetail($resArr[0]);
            $this->searchDetail = $searchDetail;

            $itemId = $resArr[0]['merchant_item_id'];
            $paidArr = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmItemno($itemId);

            if (count($paidArr) > 0) {
                $succesDetail = $this->getfullDetail($paidArr[0]);
                $this->paymentDetail = $succesDetail;
            }
        } else {
            $this->transReqStatus = false;
        }

        $this->SerachedValidationNo = $this->valNo;
    }
        /* -- Search by validation number for Merchant Support -- */

    public function executeSearchSupportValno(sfWebRequest $request) {

        $this->searchByVal = new SupportValNo();
        ini_set("memory_limit", "50M");
		$merchant_id = '';
        $getAllVal = $request->getParameterHolder()->getAll();
        if(isset($getAllVal['merchant_val']['transValNo'])){
       	 $this->valNo = $getAllVal['merchant_val']['transValNo'];
        }
        /*if(isset($getAllVal['merchant_val']['txnFilter'])){
        	$this->popup_flag = $getAllVal['merchant_val']['txnFilter'];
        }*/
        if(isset($getAllVal['txnVal'])){
        	$this->valNo = $getAllVal['txnVal'];
        	$this->popup_flag = $getAllVal['txnFilter'];
        	$this->setLayout('layout_popup');
        }
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $userId=$this->getUser()->getGuardUser()->getid();
        $merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
        $resQry = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmValNo($this->valNo,$merchant_id);
        if (count($resQry) > 0) {
            $this->result = $resQry;
            $this->transReqStatus = true;

            $resArr = $resQry->fetchArray();
            $this->transResult = $resArr[0];
            $searchDetail = $this->getfullDetail($resArr[0]);
            $this->searchDetail = $searchDetail;

            $itemId = $resArr[0]['merchant_item_id'];
            $paidArr = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmItemno($itemId);

            if (count($paidArr) > 0) {
                $succesDetail = $this->getfullDetail($paidArr[0]);
                $this->paymentDetail = $succesDetail;
            }
        } else {
            $this->transReqStatus = false;
        }

        $this->SerachedValidationNo = $this->valNo;
    }
   /* public function executeNotificationMerchant(sfWebRequest $request) {
    	$transaction_id = $request->getParameter('transactionID');
    	$job_id = Doctrine::getTable('EpJobParameters')->getJobIdFromParametres($transaction_id);
    	$jobId = $job_id['0']['job_id'];
    	$attempts_no = Doctrine::getTable('EpJob')->getAttemptsFromJobId($jobId);

    	if($attempts_no['0']['execution_attempts'] >= '10') {

    	$jobQueueObj = new EpJobQueue();
    	$jobQueueObj->setJobId($jobId);
    	$date = new DateTime('');
    	$interval = new DateInterval('P1D');
    	$date->sub($interval);
    	$new_date = $date->format('Y-m-d H:i:s');
    	$jobQueueObj->setScheduledStartTime($new_date);
    	$jobQueueObj->save();

    	}else
    	$queue_start_time = Doctrine::getTable('EpJobQueue')->getUpdateJobQueue($jobId);

    }
    public function getBrowser() {
    	if (!$this->browserInstance) {
    		$this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
    				array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false, 'timeout' => sfConfig::get('app_notification_request_timeout')));
    	}

    	return $this->browserInstance;
    }*/

    public function createLog($logdata, $nameFormat, $exceptionMessage="", $parentLogFolder='', $appendTime=true, $append = false) {

    	$pay4meLog = new pay4meLog();
    	if ($exceptionMessage != "") {
    		$logdata = $logdata . "\n" . $exceptionMessage;
    	}
    	$pay4meLog->createLogData($logdata, $nameFormat, $parentLogFolder, $appendTime, $append);
    }

    public function executeParam(sfWebRequest $request) {
        $this->form = new SupportForm();
        $this->searchByTrans = new SupportTransID();
        $this->msg = "";
        $this->merchant_service_id = 0;
        $this->merchant_id = 0;
        $this->path_info = $_SERVER['PATH_INFO'];

        if ($request->isMethod('post')) {
            $Allparam = $request->getPostParameters();

            unset($Allparam['support']);
            $paramArr = $Allparam;
            $inArr = array_merge($request->getParameter('support'), $Allparam);
            if (count($paramArr) == 0) {
                $this->merchant_service_id = $inArr['merchant_service_id'];
                $this->merchant_id = $inArr['merchant'];
                $this->msg = "Please Fill All Details";
            }

            unset($inArr['id']);

            $this->form->bind($request->getParameter('support'));

            if ($this->form->isValid()) {
                $isValid = true;

                $ValidationRulesObj = validationRulesServiceFactory::getService(
                                validationRulesServiceFactory::$TYPE_BASE);
                $merchantData = $ValidationRulesObj->getSearchValidationRules(
                                $inArr['merchant_service_id']);

                $err = 0;
                $search_arr = "";
                foreach ($merchantData as $key => $value) {
                    if (($inArr[$key]) == "" && $value['is_mandatory'] == 1) {
                        $isValid = false;
                    }
                }
                if ($isValid) {
                    $this->getRequest()->setParameter('data', $inArr);
                    $this->forward('support', 'SearchByParams');
                } else {
                    $this->merchant_service_id = $inArr['merchant_service_id'];
                    $this->merchant_id = $inArr['merchant'];
                    $this->msg = "Please Fill All Details";
                }
            }
        }
        $this->setTemplate('index');
    }
    public function executeSupportParam(sfWebRequest $request) {
    	$this->form = new SupportForm();
    	$this->searchByTrans = new SupportTransID();
    	$this->msg = "";
    	$this->merchant_service_id = 0;
    	$this->merchant_id = 0;
    	$this->path_info = $_SERVER['PATH_INFO'];

    	if ($request->isMethod('post')) {
    		$Allparam = $request->getPostParameters();

    		unset($Allparam['support']);
    		$paramArr = $Allparam;
    		$inArr = array_merge($request->getParameter('support'), $Allparam);
    		if (count($paramArr) == 0) {
    			$this->merchant_service_id = $inArr['merchant_service_id'];
    			$this->merchant_id = $inArr['merchant'];
    			$this->msg = "Please Fill All Details";
    		}

    		unset($inArr['id']);

    		$this->form->bind($request->getParameter('support'));

    		if ($this->form->isValid()) {
    			$isValid = true;

    			$ValidationRulesObj = validationRulesServiceFactory::getService(
    					validationRulesServiceFactory::$TYPE_BASE);
    			$merchantData = $ValidationRulesObj->getSearchValidationRules(
    					$inArr['merchant_service_id']);

    			$err = 0;
    			$search_arr = "";
    			foreach ($merchantData as $key => $value) {
    				if (($inArr[$key]) == "" && $value['is_mandatory'] == 1) {
    					$isValid = false;
    				}
    			}
    			if ($isValid) {
    				$this->getRequest()->setParameter('data', $inArr);
    				$this->forward('support', 'SupportSearchByParams');
    			} else {
    				$this->merchant_service_id = $inArr['merchant_service_id'];
    				$this->merchant_id = $inArr['merchant'];
    				$this->msg = "Please Fill All Details";
    			}
    		}
    	}
    	$this->setTemplate('index');
    }

    public function executeSearchfrmtransno(sfWebRequest $request) {
        ini_set("memory_limit", "50M");

        $getAllVal = $request->getParameterHolder()->getAll();
        $resQry = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmTransno($getAllVal['txnRef']);

        if ($resQry->count() > 0) {
            $this->transReqStatus = true;
            $resArr = $resQry->fetchArray();
            $this->transResult = $resArr[0];
            $searchDetail = $this->getfullDetail($resArr[0]);
            $this->searchDetail = $searchDetail;

            $itemId = $resArr[0]['merchant_item_id'];
            $paidArr = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmItemno($itemId);

            if (count($paidArr) > 0) {
                $succesDetail = $this->getfullDetail($paidArr[0]);
                $this->paymentDetail = $succesDetail;
            }
        } else {
            $this->transReqStatus = false;
        }

        $this->SerachedTransNo = $getAllVal['txnRef'];
        $this->setTemplate("searchfrmtransno");
    }

  public function executeSearchSupportfrmtransno(sfWebRequest $request) {
        ini_set("memory_limit", "50M");

        $getAllVal = $request->getParameterHolder()->getAll();
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $userId=$this->getUser()->getGuardUser()->getid();
        $merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
        $resQry = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmTransno($getAllVal['txnRef'],$merchant_id);
        if ($resQry->count() > 0) {
            $this->transReqStatus = true;
            $resArr = $resQry->fetchArray();
            $this->transResult = $resArr[0];
            $searchDetail = $this->getfullDetail($resArr[0]);
            $this->searchDetail = $searchDetail;

            $itemId = $resArr[0]['merchant_item_id'];
            $paidArr = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmItemno($itemId);

            if (count($paidArr) > 0) {
                $succesDetail = $this->getfullDetail($paidArr[0]);
                $this->paymentDetail = $succesDetail;
            }
        } else {
            $this->transReqStatus = false;
        }
        //$this->getRequest()->setParameter('template', 'layout_popup');
        if(isset($getAllVal['txnFilter'])) {
        	$this->setLayout('layout_popup');
        	$this->popup_flag = $getAllVal['txnFilter'];
        }

        $this->SerachedTransNo = $getAllVal['txnRef'];
    }

    public function getfullDetail($getarr) {

        $succseDetail = array();
        $succseDetail['txnNo'] = $getarr['Transaction'][0]['pfm_transaction_number'];
        $succseDetail['payment_status_code'] = $getarr['payment_status_code'];
        $succseDetail['itemId'] = Doctrine::getTable('MerchantItem')->getItemId($getarr['merchant_item_id']);
        $succseDetail['validation_number'] = $getarr['validation_number'];
        $serviceDetail = Doctrine::getTable('MerchantService')->getMerchantServiceName($getarr['merchant_service_id']);
        $succseDetail['ServiceName'] = $serviceDetail[0]['name'];
        $MerchantDetail = Doctrine::getTable('Merchant')->getMerchantDetailsById($getarr['merchant_id']);
        $succseDetail['MerchantName'] = $MerchantDetail[0]['name'];
        $mRDetail = Doctrine::getTable('MerchantRequestDetails')->getDetailsByTransaction($succseDetail['txnNo']);
        if (isset($getarr['check_number']) && !is_null($getarr['check_number'])) {
            $succseDetail['check_number'] = $getarr['check_number'];
            $succseDetail['account_number'] = $getarr['account_number'];
        }
        $VRulesqry = Doctrine::getTable('ValidationRules')->getValidationRules($getarr['merchant_service_id']);
        $VRulesArr = $VRulesqry->toArray();
        $param = array();
        foreach ($VRulesArr as $val) {
            if (array_key_exists($val['mapped_to'], $mRDetail[0]))
                $param[$val['param_description']] = $mRDetail[0][$val['mapped_to']];
        }
        $succseDetail['paramArr'] = $param;

        if (0 == $getarr['payment_status_code']) {

            $payArr = Doctrine::getTable('PaymentModeOption')->getAllPayDetails($getarr['payment_mode_option_id']);
            $p4mhelper = new pfmHelper();
            $name = $p4mhelper->getPMONameByPMId($payArr[0]['id']);
            $succseDetail['paymentMode'] = $name;
            $branchDetail = Doctrine::getTable('Bank')->getBankName($getarr['bank_id']);
            $succseDetail['BankName'] = $branchDetail['bank_name'];
            $succseDetail['BranchName'] = Doctrine::getTable('BankBranch')->getBankBranchNameBYID($getarr['bank_branch_id']);
            if ($getarr['updated_by']) {
                $udetail = Doctrine::getTable('UserDetail')->getNameFrmId($getarr['updated_by']);
                if ($udetail)
                    $succseDetail['userArr'] = $udetail;
            }
        }

//
//        echo "<pre>";
//        print_r($getarr);
//        exit;

        return $succseDetail;
    }

    public function executeMerchantService(sfWebRequest $request) {
        $this->setTemplate(FALSE);
        $merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BANK);
        $str = $merchant_service_obj->getServices($request->getParameter('merchant_id'), $request->getParameter('merchant_service_id'));
        return $this->renderText($str);
    }

    public function executeMerchantServiceParams(sfWebRequest $request) {
        $this->setTemplate(FALSE);
        $validation_rules_obj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BANK);
        $str = $validation_rules_obj->getRulesById($request->getParameter('merchant_service_id'));
        return $this->renderText($str);
    }

    public function executeSearchByParams(sfWebRequest $request) {
        //$searchParamsArr = $request->getParameterHolder()->getAll();
        $searchParamsArr = $request->getParameter('data');

        $this->searchParamArrValues = $searchParamsArr;
        $str = Doctrine::getTable('ValidationRules')->getRulesById($searchParamsArr['merchant_service_id']);
        $this->searchParamArrLabel = $str;
        $resultArr = Doctrine::getTable('MerchantRequestDetails')->searchByParama($searchParamsArr);

        $MerchantDetail = Doctrine::getTable('Merchant')->getMerchantDetailsById($searchParamsArr['merchant']);
        $succseDetail['MerchantName'] = $MerchantDetail[0]['name'];
        $serviceDetail = Doctrine::getTable('MerchantService')->getMerchantServiceName($searchParamsArr['merchant_service_id']);
        $succseDetail['ServiceName'] = $serviceDetail[0]['name'];
        $this->searchDetail = $succseDetail;

        if (count($resultArr) > 0) {
            $this->transResult = $resultArr[0]['MerchantRequest'];


            $itemId = $resultArr[0]['MerchantRequest']['merchant_item_id'];

            $resultPayArr = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmItemno($itemId);

            if (count($resultPayArr) > 0) {
                $succesDetail = $this->getfullDetail($resultPayArr[0]);
                $this->paymentDetail = $succesDetail;
                $resQry = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmTransno($succesDetail['txnNo']);
                $resArr = $resQry->fetchArray();
                $this->transResult = $resArr[0];
                $this->TransStatus = 0;
            } else {
                $this->TransStatus = 1;
            }
        }
//                $this->merchantDetails = $detailsObj->setMerchantRequestDetails($pfmTransNo,$this->transResult['merchant_item_id'],$this->transResult['paid_amount'],'',$this->transResult['payment_mode_option_id'],$this->transResult['bank_id'],$this->transResult['bank_branch_id'],'',$this->transResult['payment_status_code'],$this->transResult['created_at'],$this->transResult['updated_by'],$this->transResult['merchant_id'],$this->transResult['merchant_service_id']);
//           $resultMerItemArr = Doctrine::getTable('MerchantRequest')->searchByMerchantItemId($resultArr[0]['MerchantRequest']['merchant_service_id'],$resultArr[0]['MerchantRequest']['merchant_item_id']);
//            if($resultArr[0]['MerchantRequest']['payment_status_code'] == 1){
//
//                $detailsObj = new supportMerchantRequestDetails();
//                 $transDetail = Doctrine::getTable('Transaction')->getDetailfrmMerchantReqId($resultArr[0]['MerchantRequest']['id']);
//                 if($transDetail)
//                    $pfmTransNo=$transDetail['pfm_transaction_number'];
//                  else
//                    $pfmTransNo='';
//                if(count($resultMerItemArr) > 0){echo $resultArr[0]['MerchantRequest']['id'];
//
//                   $this->transResult = $resultMerItemArr[0];
//                   $this->merchantDetails = $detailsObj->setMerchantRequestDetails($pfmTransNo,$this->transResult['merchant_item_id'],$this->transResult['paid_amount'],$this->transResult['updated_at'],$this->transResult['payment_mode_option_id'],$this->transResult['bank_id'],$this->transResult['bank_branch_id'],'',$this->transResult['payment_status_code'],$this->transResult['created_at'],$this->transResult['updated_by'],$this->transResult['merchant_id'],$this->transResult['merchant_service_id']);
//                   $this->paramsFound = $this->transResult['MerchantRequestDetails'];
//
//                }else{
//                   $this->transResult = $resultArr[0]['MerchantRequest'];
//                   $this->merchantDetails = $detailsObj->setMerchantRequestDetails($pfmTransNo,$this->transResult['merchant_item_id'],$this->transResult['paid_amount'],'',$this->transResult['payment_mode_option_id'],$this->transResult['bank_id'],$this->transResult['bank_branch_id'],'',$this->transResult['payment_status_code'],$this->transResult['created_at'],$this->transResult['updated_by'],$this->transResult['merchant_id'],$this->transResult['merchant_service_id']);
//
//                }
//
//            }else if($resultArr[0]['MerchantRequest']['payment_status_code'] == 0){
//
//                 $transDetail = Doctrine::getTable('Transaction')->getPayDetailfrmMerchantReqId($resultArr[0]['MerchantRequest']['id']);
//                 $this->transResult = $resultArr[0]['MerchantRequest'];
//
//                   $detailsObj = new supportMerchantRequestDetails();
//                   $this->merchantDetails = $detailsObj->setMerchantRequestDetails($transDetail['pfm_transaction_number'],$this->transResult['merchant_item_id'],$this->transResult['paid_amount'],'',$this->transResult['payment_mode_option_id'],$this->transResult['bank_id'],$this->transResult['bank_branch_id'],'',$this->transResult['payment_status_code'],$this->transResult['created_at'],$this->transResult['updated_by'],$this->transResult['merchant_id'],$this->transResult['merchant_service_id']);
//
//
//            }
//
//
//        }
        else {

            $this->transResult = array();
            $this->TransStatus = 2;
        }

        $this->setTemplate('searchFrmParam');
    }

    public function executeSupportSearchByParams(sfWebRequest $request) {
    	//$searchParamsArr = $request->getParameterHolder()->getAll();
    	$searchParamsArr = $request->getParameter('data');

    	$this->searchParamArrValues = $searchParamsArr;
    	$str = Doctrine::getTable('ValidationRules')->getRulesById($searchParamsArr['merchant_service_id']);
    	$this->searchParamArrLabel = $str;
    	$resultArr = Doctrine::getTable('MerchantRequestDetails')->searchByParama($searchParamsArr);

    	$MerchantDetail = Doctrine::getTable('Merchant')->getMerchantDetailsById($searchParamsArr['merchant']);
    	$succseDetail['MerchantName'] = $MerchantDetail[0]['name'];
    	$serviceDetail = Doctrine::getTable('MerchantService')->getMerchantServiceName($searchParamsArr['merchant_service_id']);
    	$succseDetail['ServiceName'] = $serviceDetail[0]['name'];
    	$this->searchDetail = $succseDetail;

    	if (count($resultArr) > 0) {
    		$this->transResult = $resultArr[0]['MerchantRequest'];


    		$itemId = $resultArr[0]['MerchantRequest']['merchant_item_id'];

    		$resultPayArr = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmItemno($itemId);

    		if (count($resultPayArr) > 0) {
    			$succesDetail = $this->getfullDetail($resultPayArr[0]);
    			$this->paymentDetail = $succesDetail;
    			$resQry = Doctrine::getTable('MerchantRequest')->getPaidDetailsFrmTransno($succesDetail['txnNo']);
    			$resArr = $resQry->fetchArray();
    			$this->transResult = $resArr[0];
    			$this->TransStatus = 0;
    		} else {
    			$this->TransStatus = 1;
    		}
    	}
    			else {

    			$this->transResult = array();
    			$this->TransStatus = 2;
    }

    $this->setTemplate('searchSupportFrmParam');
    }
    /*
     * action : bank integration search
     */

    public function executeBankIntegrationSearch(sfWebRequest $request) {
    $merchantId = '';
        $transactionType = "Payment";
        $DataArray = $request->getParameterHolder()->getAll();
        $pfmHelperObj = new pfmHelper();
       	$this->userGroup = $pfmHelperObj->getUserGroup();
       	$user_id = $this->getUser()->getGuardUser()->getId();
        if (!empty($DataArray['search'])) {
            $postDataArray = $DataArray['search'];
            if (!empty($postDataArray['merchant'])) {
                $merchantId = $postDataArray['merchant'];
            }
            if (!empty($postDataArray['transaction_type'])) {
                $transactionType = $postDataArray['transaction_type'];
            }
        }
        $selBankChoices = array('' => '-- Select Bank --');
	if($this->userGroup == 'portal_admin') {	
	        $arrBankList = Doctrine::getTable('ServiceBankConfiguration')->getBankListForTransaction($transactionType, $merchantId);
	        $bankList = array();
	       	if (!empty($arrBankList)) {
	      		foreach ($arrBankList as $value) {
	               		$bankList[$value['id']] = $value['bank_name'];
	       		}
	       	}
	        $this->bankList = $selBankChoices + $bankList;
	}
	else {
		$bankArray = Doctrine::getTable('BankUser')->getBankIdByUserId($user_id);
		if(count($bankArray)== 0) {
			$bankList = array('' => 'Please Select Bank');
		}
		else {
			$bankList[$bankArray['0']['id']] = $bankArray['0']['bank_name'];
		}
		$this->bankList = $bankList;
	}  
			
        $this->form = new BankIntegrationSearchForm(array(), array('bankList' => $this->bankList));
        $this->form->setDefault('transaction_type', 'Payment');
        $this->formValid = "";

        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('search'));
            if ($this->form->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeBankIntegrationSearchList(sfWebRequest $request) {
        $DataArray = $request->getParameterHolder()->getAll();
        if (!empty($DataArray['search']))
            $postDataArray = $DataArray['search'];
        else
            $postDataArray = $DataArray;

        // reattemp message code will come here
        if (!empty($DataArray['id'])) {
            $this->requestID = $DataArray['id'];
            $transactionId = $DataArray['transactionId'];
            $message = "";
            try {
                $bankIntegrationManager = new bankIntegration();
                $flagReAttemptCount = true;
                try {
                	$jobName = 'bankNotificationReattempt';
                	$jobAction = 'middleware/BankNotificationRequestList';
                	$parameters = array('mwRequestId' => $this->requestID);
                	$phpMiddlewareBanksList = sfConfig::get('app_middleware_enabled_banks');
                	$record = Doctrine::getTable('transactionBankPosting')->findBy('MessageTxnNo', $transactionId,Doctrine_Core::HYDRATE_ARRAY); 
                	$bankId = $record['0']['bank_id'];
                	$bankName = Doctrine::getTable('bank')->getBankNameByBankId($bankId);
                	if(!empty($phpMiddlewareBanksList) && in_array($bankName,$phpMiddlewareBanksList )) {
                		$bankIntegrationManager->QueueProcessing($this->requestID, $flagReAttemptCount);
                	}
                	else {
                	// Custom job will set for previous day morning 1PM
                		$startTime = date('Y-m-d', strtotime(' -1 day'))." 01:00:00";
                		$taskId = EpjobsContext::getInstance()->addJob($jobName, $jobAction, $parameters,-1,NULL,$startTime);
                		//$noOfAttempts = $record['0']['no_of_attempts'];
                		//Doctrine::getTable('transactionBankPosting')->updateAttemptCount($transactionId,$noOfAttempts);
                	}
                	$message = "Message posted successfully.";
                    $type = 'notice';
                } catch (Exception $e) {

		
                    $message =  "Due to some internal problem re-attempt failed, please try again.";
                    $type = 'error';
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                $type = 'error';
            }
            $this->getUser()->setFlash($type, $message, false);
        }

        // end reattempt code here
        if (isset($postDataArray['transaction_type'])) {
            if ($postDataArray['transaction_type'] == "Recharge From Bank") {
                $this->transactionType = "Recharge";
            } else {
                $this->transactionType = $postDataArray['transaction_type'];
            }
        } else {
            $this->transactionType = 'Payment';
        }
        if (isset($postDataArray['merchant']) && $this->transactionType == 'Payment')
            $this->merchant = $postDataArray['merchant'];
        else
            $this->merchant = '';
        if (isset($postDataArray['transaction_no']) && $this->transactionType == "Payment")
            $this->transactionNo = $postDataArray['transaction_no'];
        else
            $this->transactionNo = '';
        if (isset($postDataArray['validation_no']) && $this->transactionType == 'Recharge')
            $this->validationNo = $postDataArray['validation_no'];
        else
            $this->validationNo = '';

        if (isset($postDataArray['bank']))
            $this->bank = $postDataArray['bank'];
        else
            $this->bank = '';
        if (isset($postDataArray['status']))
            $this->status = $postDataArray['status'];
        else
            $this->status = '';
        if (isset($postDataArray['from']))
            $this->fromDate = $postDataArray['from'];
        else
            $this->fromDate = '';
        if (isset($postDataArray['to']))
            $this->toDate = $postDataArray['to'];
        else
            $this->toDate = '';
        $this->reattemptTime = Settings::getReatttemptTimeInMin();
        $objSupport = new supportManager();
        $obj = $objSupport->getRequestDetails($this->transactionType, $this->transactionNo,
                        $this->validationNo, $this->merchant, $this->bank, $this->status, $this->fromDate, $this->toDate);
//        echo "<pre>";
//        print_r($obj->execute()->toArray());
//        echo "<pre/>";
//        die();
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('MwRequest', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($obj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    /*
     * function GetBankListForTransaction()
     * get drop down for bank configuration
     *
     */

    public function executeGetBankListForTransaction(sfWebRequest $request) {
    	$pfmHelperObj = new pfmHelper();
    	$this->userGroup = $pfmHelperObj->getUserGroup();
    	$user_id = $this->getUser()->getGuardUser()->getId();
        $transactionType = $request->hasParameter('transactionType') ? $request->getParameter('transactionType') : "Payment";
        if ($transactionType == 'Payment') {
            $merchant = $request->hasParameter('merchant') ? $request->getParameter('merchant') : "";
            if($this->userGroup == 'portal_admin') {
            	$bankList = Doctrine::getTable('ServiceBankConfiguration')->getBankListForTransaction($transactionType, $merchant);
            	$str = '<option value="">-- Select Banks --</option>';
            	if (!empty($bankList)) {
            		foreach ($bankList as $value) {
            			$str .= '<option value="' . $value['id'] . '">' . $value['bank_name'] . '</option>';
            		}
            	}
            }
            
            else {
            	$bankArray = Doctrine::getTable('BankUser')->getBankIdByUserId($user_id);
				if(count($bankArray)== 0) {
					$str .= '<option value=""> Please Select Bank</option>';
				}
				else {
					
					$str .= '<option value="' . $bankArray['0']['id'] . '">' . $bankArray['0']['bank_name'] . '</option>';
					//$str .= '<option value=""> 111</option>';
				}
				
            }
        
        }
        else {
            $merchant="";
        if($this->userGroup == 'portal_admin') {
        	$bankList = Doctrine::getTable('ServiceBankConfiguration')->getBankListForTransaction($transactionType, $merchant);
        	$str = '<option value="">-- Select Banks --</option>';
        	if (!empty($bankList)) {
            	foreach ($bankList as $value) {
                	$str .= '<option value="' . $value['id'] . '">' . $value['bank_name'] . '</option>';
            	}
       		 }
        }
		
        else {
        	$bankArray = Doctrine::getTable('BankUser')->getBankIdByUserId($user_id);
        	$bankList = Doctrine::getTable('ServiceBankConfiguration')->getBankListForTransaction($transactionType, $merchant);
        	$count = 0;
        	foreach($bankList as $key => $value) {
        		if(in_array($bankArray['0']['bank_name'],$value)){
        			$count++;
        		}
        	}
        	if ($count > 0) {
        			$str .= '<option value="' . $value['id'] . '">' . $value['bank_name'] . '</option>';
        	}
        	else {
        		
        		$str .= '<option value=""> Please Select Bank</option>';
        	}
    
        }
        }
        
        return $this->renderText($str);
    }

    public function executeViewHistory(sfWebRequest $request) {
        $this->setLayout('layout_popup');
        $this->requestId = $request->getParameter('id');
        $supportObj = supportServiceFactory::getService();
        $historyObj = $supportObj->getHistoryDetail($this->requestId);

        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('MwResponse', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($historyObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeViewLog(sfWebRequest $request) {
        $this->setLayout('layout_popup');
        $this->transactionNo = $request->getParameter('transactionNo');
        $this->bank_id = $request->getParameter('bank_id');
        $this->bankProtocolVersion = Doctrine::getTable('Bank')->findByBankName($this->bank_id)->getFirst()->getBiProtocolVersion();
        $supportObj = supportServiceFactory::getService();
        $logDetailObj = $supportObj->getLogDetail($this->transactionNo);
        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('MwMessageLog', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($logDetailObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeSetLogginglevel(sfWebRequest $request) {
        $this->form = new setLoggingLevelForm();
        $this->formValid = "";
        if ($request->isMethod('post')) {
            // $this->form = new setLoggingLevelForm('',array('bank_id'=>true));
            $this->form->bind($request->getParameter('loggingLevel'));
            if ($this->form->isValid() && $request->isMethod('post')) {
                $param = $request->getParameter('loggingLevel');
                $loggingLevel = $param['level'];
                $bankId = $param['bank'];
                $bi_protocolVersion = Doctrine::getTable("Bank")->find($bankId)->getBiProtocolVersion();
                $validateV2 = Doctrine::getTable("BankMwMapping")->getMwMappingIdByBankId($bankId);


                if (($bi_protocolVersion == "v2") && (!$validateV2) ) {
                    $this->getUser()->setFlash('error', "This Bank is not mapped with Middleware");
                    $this->redirect('support/setLogginglevel');
                }

                try {

                    $objBank = Doctrine::getTable('Bank')->find($bankId);
                    // if ($loggingLevel!=$objBank->getBiLoggerLevel()) {

                    if ($bi_protocolVersion == "v2")  {

                        $versionManagerObj = "BankIntegration" . $bi_protocolVersion . "Manager";
                        $mgr_obj = new $versionManagerObj();
                        // $command = "log:set " . $loggingLevel;
                        $commandSet = "log:set";
                        $commandSetResult =  $mgr_obj->addCommandRequest($bankId, $commandSet, $loggingLevel);

                        //Posting message to activemq
                        //We need to set input queuename and output queue name for command messages for a bank
                        $bankIntegraionObject = new bankIntegration();
                        $bankIntegraionObject->QueueProcessing($mgr_obj);

                        /*$commandGet = "log:get";
                        $commandGetResult = $mgr_obj->addCommandRequest($bankId, $commandGet, '');
                        $bankIntegraionObject->QueueProcessing($mgr_obj);*/

                        //sleep(1); // execution delayed so as to make karaf insert the response in mw_response table in the mean time.
                        $objBank->setBiLoggerLevel($loggingLevel);
                        $objBank->save();
                        //$MwResponseResult = Doctrine::getTable("MwResponse")->getMwResponseDetails($commandSetResult['id']);

                        /* if(!$MwResponseResult){
                            $this->getUser()->setFlash('notice', 'Middleware is currently down, Logging level will be updated as soon as middleware gets up', true);
                        }else{
                            $this->getUser()->setFlash('notice', 'Logging Level Modified Successfully', true);
                        } */
                        // Bug:36220 [18-12-2012]
                        $this->getUser()->setFlash('notice', 'Logging Level Request is posted Successfully on Activemq', true);
                        // Ends here
                    }else{
                        $objBank->setBiLoggerLevel($loggingLevel);
                        $objBank->save();
                         $this->getUser()->setFlash('notice', 'Logging Level Modified Successfully', true);
                    }

                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
        }
    }

    /*
     * function FindbiVersion()
     * to know bi version of selected bank
     * 02-01-2013
     * Bug: 36224
     */
    public function executeFindbiVersion(sfWebRequest $request) {

        $bankId = $request->getParameter('bank');
        $bankDetail = Doctrine::getTable('Bank')->getBankName($bankId);
        $bi_protocolVersion = Doctrine::getTable("Bank")->find($bankId)->getBiProtocolVersion();
        $this->setTemplate(FALSE);
        return  $this->renderText($bi_protocolVersion);
    }
    public function executeFetchbankDetail(sfWebRequest $request) {
        $bankId = $request->getParameter('bank');
        $bankDetail = Doctrine::getTable('Bank')->getBankName($bankId);
        $bi_protocolVersion = Doctrine::getTable("Bank")->find($bankId)->getBiProtocolVersion();

        if ($bi_protocolVersion == "v2"){
            try {
                $versionManagerObj = "BankIntegration" . $bi_protocolVersion . "Manager";
                $mgr_obj = new $versionManagerObj();
                $bankIntegraionObject = new bankIntegration();

                $commandGet = "log:get";
                $commandGetResult = $mgr_obj->addCommandRequest($bankId, $commandGet, '');


                $resultres = $bankIntegraionObject->QueueProcessing($mgr_obj);

                $bankIntObj = new bankIntegrationHelper();
                $MwResponseResult = $bankIntObj->checkFirstResponse($commandGetResult['id'],sfConfig::get('app_karaf_response_attempts'));

                //sleep(1);
                //$MwResponseResult = Doctrine::getTable("MwResponse")->getMwResponseDetails($commandGetResult['id']);

                if($MwResponseResult){
                    $response = $MwResponseResult['message_text'];
                    $decodedResponse = (array)json_decode($response);

                    $decodedResponse=explode(": ",$decodedResponse['stdout-response']);
                    //$decodedRes=explode("\n",$decodedResponse[1]);
                    $decodedRes = preg_replace("/[\\n\\r]+/", "", $decodedResponse[1]); // Bug:36209 (17-12-2012)

                    //$bankDetail['bi_logger_level'] = $decodedRes[0];
                    $bankDetail['bi_logger_level'] = $decodedRes;
                }
            } catch (Exception $e) {
                    //$this->getUser()->setFlash('error', $e->getMessage());
            }
        }

        return $this->renderText("sucess" . '$$$' . $bankDetail['bank_code'] . '$$$' . $bankDetail['bank_key'] . '$$$' . $bankDetail['bi_logger_level'] . '$$$' . $bankDetail['bi_protocol_version']);
    }

    /* action : generate bankTransactionXML()
     * wp053
     */

    public function executeBankTransactionXML(sfWebRequest $request) {
        $this->setLayout('layout_popup');
        $requestId = $request->getParameter('id');
        $objBI = new bankIntegration();
        $this->xmlDetails = $objBI->generateBankTransactionXML($requestId);
    }
    public function executeMerchantTransactionXML(sfWebRequest $request) {
    	$trnsactionId = $request->getParameter('transNo');
    	$txnRef = Doctrine::getTable('Transaction')->getMerchantRequestIdByTransactionId($trnsactionId);
    	$this->xmlDetailsRequest = Doctrine::getTable('MerchantRequestLog')->getRequestXMlData($txnRef);
    	$this->xmlDetailsResponse = Doctrine::getTable('MerchantResponseLog')->getResponseXMlData($trnsactionId);
    	 
    }

    private function contents($parser, $data) {
        echo $data;
    }

    private function startTag($parser, $data) {
        echo "<b>";
    }

    private function endTag($parser, $data) {
        echo "</b><br />";
    }

    public function executeSearchFailedJobs(sfWebRequest $request) {
        $this->form = new CustomFailedJobsForm();
        if ($request->isMethod('post')) {
            $params = $request->getParameterHolder()->getAll();
            array_pop($params);
            array_pop($params);
            $params['startDate'] = $params['startDate_date'];
            $params['endDate'] = $params['endDate_date'];
            $this->form = new CustomFailedJobsForm($params);

            $this->form->bind($params);
        }
    }

    public function executeGetFailedJobsList(sfWebRequest $request) {
        $params = $request->getParameterHolder()->getAll();
        array_pop($params);
        array_pop($params);
        $params['startDate'] = $params['startDate_date'];
        $params['endDate'] = $params['endDate_date'];
        $this->form = new CustomFailedJobsForm($params);

        $this->form->bind($params);
        if ($this->form->isValid()) {
            $startDate = $params['startDate_date'] ? $params['startDate_date'] . "T00:00:00Z" : null;
            $endDate = $params['endDate_date'] ? $params['endDate_date'] . "T23:59:59Z" : null;
            $epSocketLabsPloginObj = new EpSocketLabsManager(sfConfig::get('app_socketlabs_serverId'), sfConfig::get('app_socketlabs_username'), sfConfig::get('app_socketlabs_password'), $startDate, $endDate, 'xml');
            $result = $epSocketLabsPloginObj->getFailedMessages();
            $this->xml = new SimpleXMLElement($result);
        }
    }
  /*
     * action : merchant support
     * Developed By-Deepak Bhardwaj
     */
public function executeChainServiceType(sfWebRequest $request) {
	//echo $request->getParameter('id');
        $this->serviceType = Doctrine::getTable('MerchantService')->getServiceTypes($request->getParameter('id'));
        //print_r($this->serviceType->toArray());die;
    }
    public function executePaymentMode(sfWebRequest $request) {
        $this->chequeDraftMapped = false;
        $this->isBankUser = $this->getBankUser();
        $this->isAdmin = $this->getAdmin();
        if ($this->isBankUser) {
            $user_bank = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
            $records = Doctrine::getTable('BankMerchant')->findByBankId($user_bank);
            $this->merchant_account = $records->getFirst()->getMerchantAccount();
            $chequeDraftMappedDetails = Doctrine::getTable('BankSortCodeMapper')->findByBankId($user_bank);

            if ($chequeDraftMappedDetails->count()) {
                $this->chequeDraftMapped = true;
            }
        }
        $this->arrPaymentMode = Doctrine::getTable('PaymentModeOption')->getPaymentModeByMerchantService($request->getParameter('id'), $request->getParameter('sId'));

        $this->arrPaymentModeMerchantCounter = Doctrine::getTable('PaymentModeOption')->getPaymentModeForMerchantCounter($request->getParameter('id'), $request->getParameter('sId'));
        $this->setLayout(false);
    }
      private function getBankUser()
    {
        $isbankUser = 0;
        $user=sfContext::getInstance()->getUser();
        $group=$user->getGroupNames();
        if($group[0] == sfConfig::get('app_pfm_role_bank_admin') || $group[0] == sfConfig::get('app_pfm_role_bank_country_head')
                || $group[0] == sfConfig::get('app_pfm_role_country_report_user') ||  $group[0] == sfConfig::get('app_pfm_role_bank_branch_user')
                || $group[0] == sfConfig::get('app_pfm_role_report_bank_admin')  ||  $group[0] == sfConfig::get('app_pfm_role_bank_e_auditor')
              || $group[0] == sfConfig::get('app_pfm_role_bank_branch_report_user')  ){
                          $isbankUser = 1;
                }
                return $isbankUser;
    }
     private function getAdmin()
    {
        $isAdmin = 0;
        $user=sfContext::getInstance()->getUser();
        $group=$user->getGroupNames();
        if($group[0] == sfConfig::get('app_pfm_role_admin') || $group[0] == sfConfig::get('app_pfm_role_report_admin')
                || $group[0] == sfConfig::get('app_pfm_role_report_portal_admin')     ){
                          $isAdmin = 1;
                }
                return $isAdmin;
    }
    public function executeCurrencyCode(sfWebRequest $request) {
        $this->arrCurrency = Doctrine::getTable('CurrencyCode')->getAllDistinctCurrencyService($request->getParameter('id'), $request->getParameter('mId'), $request->getParameter('sId'));
        $this->setLayout(false);
   }
    public function executeMerchantSupportSearch(sfWebRequest $request) {
        $this->merchant_id = 0; //initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
        $this->merchant_service_id = 0; //same as the reason above
        $this->selected = '';
        $this->selectedFilters = '';
        $this->userGroup = "";
        $merchant_id = '';
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();        
        if($this->userGroup == "merchant"){
        	$userId=$this->getUser()->getGuardUser()->getid();
        	$merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
        }
       
        $this->formB = new SupportForm('',array('merchant_id' => $merchant_id));
        $this->searchByTrans = new SupportTransID();
        $this->seachByKey = $request->getGetParameter('searchBy');
        if ($request->hasParameter('selected')) {
            $this->selected = $request->getParameter('selected');
        }
        if ($request->hasParameter('selectedFilters')) {
            $this->selected = $request->getParameter('selectedFilters');
        }
         /*For Search by Validation Number */
        $this->searchByVal = new SupportValNo();

        $this->msg = "";
        $this->path_info = '';
        $this->report_label = 'Merchant Support Section';
        $bank_details = $pfmHelperObj->getUserAssociatedBank();
        if (count($bank_details) > 0) {
            $bank_details_id = $bank_details['id'];
        } else {
            $bank_details_id = 0;
        }
        if(isset($merchant_id)){
        $this->form = new MerchantSupportForm('',array('bank_details_id' => $bank_details_id,'userGroup'=>$this->userGroup,'report_label'=>$this->report_label,'merchant_id'=>$merchant_id));
         }
        else {
        	$this->form = new MerchantSupportForm('',array('bank_details_id' => $bank_details_id,'userGroup'=>$this->userGroup,'report_label'=>$this->report_label));

        }
        if($request->isMethod('post')){
            $this->form->bind($request->getPostParameters());
            if($this->form->isValid()) {
				//$this->formValid = "valid";
              $this->forward($this->moduleName,'merchantSupportSearchList');
            }
        }
    }


    public function executeMerchantSupportSearchList(sfWebRequest $request) {
      $transaction_id = $request->getParameter('transactionID');
      if(isset($transaction_id)){
        try{

          $job_id = Doctrine::getTable('EpJobParameters')->getJobIdFromParametres($transaction_id);
          if(!empty($job_id)){
          $jobId = $job_id['0']['job_id'];
          $ep_job = Doctrine::getTable('EpJob')->getAttemptsFromJobId($jobId);
          $attempts_no = $ep_job['0']['execution_attempts'];
          $last_execution_status = $ep_job['0']['last_execution_status'];
          $state = $ep_job['0']['state'];

          if($state == 'finished') {
            //if($last_execution_status == 'pass'){
            $jobQueueObj = new EpJobQueue();
            $jobQueueObj->setJobId($jobId);
            $new_date = date('Y-m-d H:i:s', strtotime("-1 day", strtotime(date('Y-m-d H:i:s'))));
            $jobQueueObj->setScheduledStartTime($new_date);
            $jobQueueObj->save();
            $queue_start_time = Doctrine::getTable('EpJobQueue')->getUpdateJobQueue($jobId);
            $ep_job_update = Doctrine::getTable('EpJob')->getUpdateJob($jobId);

            //}
          }
          else {
            $queue_start_time = Doctrine::getTable('EpJobQueue')->getUpdateJobQueue($jobId);
          }
          $message = "Notification queued successfully";
          $type = 'notice';
          //$ty;
          }
          else{
          	$message = 'Payment pending for this transaction,Notification cannot be posted.';
          	$type = 'error';
          }
        }

        catch(Exception $e) {
          $message = 'Due to some internal problem re-attempt failed, please try again';
          $type = 'error';
        }
        $this->templatCheck = 'notification';
        $this->getUser()->setFlash($type, $message, false);
      }


      else{
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $this->postDataArray = array();
        if ($request->isMethod('post')) {
          unset($_SESSION['pfm']['reportData']);
          $_SESSION['pfm']['reportData'] = $request->getPostParameters();
          $this->postDataArray = $_SESSION['pfm']['reportData'];

        } else {

          $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        $this->page = 1;
        if ($request->hasParameter('page')) {
          $this->page = $request->getParameter('page');
          $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        $report_details_list = MerchantRequestTable::getMerchantSupportDetails($this->postDataArray);
        $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($report_details_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
      }
    }

	/** CSV Download for Merchant Integration Report **/
	/** Developed By - Deepak Bhardwaj - **/

	public function executeMerchantSupportCSV(sfWebRequest $request) {
		
			ini_set('memory_limit', '1024M');
			ini_set("max_execution_time", "64000");
			$this->postDataArray = $_SESSION['pfm']['reportData'];
			$validation_rules = ValidationRulesTable::getRulesById($this->postDataArray['service_type']);
			//$this->postDataArray['service_type'] = '165';
			$records = MerchantRequestTable::getMerchantIntegrationSupportCSV($this->postDataArray);
			// Creating Headers
			$headerNames = array(0 => array('S.No.','Transaction Id', 'Tellers Id', 'Service Type', 'Application Type'));
			$headerNamesSecond = array('Bank Name', 'Branch Name', 'Branch Code', 'Service Charge' ,'Transaction Charge','Date Of Transaction','Amount');
			$headers = array('transaction_number', 'username', 'merchant_name', 'merchant_service');
			$headersSecond = array('bankname', 'bank_branch_name', 'bank_branch_code','service_charge','payforme_amount','updated_at','paid_amount');
			foreach($validation_rules as $rule){
				array_push($headerNames['0'],$rule['param_description']);
				array_push($headers,$rule['mapped_to']);	
			}
			$headerNames['0'] = array_merge($headerNames['0'],$headerNamesSecond);
			$headers = array_merge($headers,$headersSecond);
			$i = 0;
			$grandTotalAmount = 0;
			$amount = 0;
			while ($row = mysql_fetch_array($records)) {
				for ($index = 0; $index < count($headers); $index++) {
					$recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
				}
				//$amount = $row['paid_amount'];
				$grandTotalAmount += $row['paid_amount'];
				$i++;
			}
			//$grandTotalAmount -= $amount;
			$recordsDetails[$i]['Grand Total'] = number_format($grandTotalAmount, 2);
			if (count($recordsDetails) > 0) {
				$arrList = $this->getMerchantSupportXLSExportList($headerNames, $headers, $recordsDetails);
				$file_array = $this->saveExportListFile($arrList, "/report/merchantSupportSearch/", "MerchantIntegrationSupport");
				$file_array = explode('#$', $file_array);
				$this->fileName = $file_array[0];
				$this->filePath = $file_array[1];
				$this->folder = "MERCHANT_INTEGRATION_SUPPORT";
			} else {
				$this->redirect_url('index.php/support/merchantSupportSearch');
			}$this->setTemplate('csv');
			// $this->setTemplate('bankReportCsv');
			$this->setLayout(false);
		}
		public function getMerchantSupportXLSExportList($arrHeader, $headers, $arrList) {
			$arrTotalList = array();
			$counterVal = count($arrHeader);
			for ($i = 0; $i < $counterVal; $i++) {
				$arrTotalList[$i] = $arrHeader[$i];
			}

			$total = count($arrList) + count($arrTotalList);
			$length = count($arrTotalList);
			$j = $length;
			$counterVal = count($arrList) - 1;
			$counterValHeader = count($headers);
			for ($k = 0; $k < $counterVal; $k++) {
				$arrExPortList[$k][] = $k + 1;
				for ($index = 0; $index < $counterValHeader; $index++) {
					$arrExPortList[$k][] = $arrList[$k][$headers[$index]];
				}
			}

			if ($this->postDataArray['report_label'] == 'Merchant Support Section') {
				$counterVal = count($arrHeader[0]) - 2;
				for ($i = 0; $i < $counterVal; $i++) {
					$arrExPortList[$k][$i] = '';
				}

				$arrExPortList[$k][$i] = 'Grand Total ';
				$arrExPortList[$k][++$i] = $arrList[$k]['Grand Total'];
			}

			$counterVal = count($arrExPortList);
			for ($i = 0; $i < $counterVal; $i++) {
				$arrTotalList[$j + $i] = $arrExPortList[$i];
			}

			return $arrTotalList;


		}
		public function saveExportListFile($arrList, $filePath, $fileName) {
			try {
				$downold_Path = sfConfig::get('sf_upload_dir') . $filePath;
				//echo $downold_Path;die;
				$strFile = $fileName . "_" . date("Ymdhis");
				$filepath = $downold_Path . $strFile . ".csv";
				$final_path = sfConfig::get('sf_upload_dir') . $filePath;
				if (!is_dir($final_path)) {
					mkdir($final_path);
					chmod($final_path, 0755);
				}
				$strFileName = $this->makeCsvFile($arrList, $filepath, ",", "\"");
				$arrFileInfo = pathinfo($strFileName);
				$path = "../../uploads/" . $filePath . $arrFileInfo['basename'];
				return $strFile . "#$" . $path;
				$this->redirect_url($path);

			} catch (Exception $e) {
				echo 'problem found' . $e->getMessage() . "\n";
				die;
			}
		}

		/*     * *****************common functions for save the reports data********************** */

		public function makeCsvFile($dataArray, $filename, $delimiter=",", $enclosure="\"") {
			// Build the string
			//print_r($dataArray);die;
			$fh = fopen($filename, "a");
			// for each array element, which represents a line in the csv file...
			foreach ($dataArray as $line) {
				$string = "";
				// No leading delimiter
				$writeDelimiter = FALSE;

				foreach ($line as $dataElement) {
					// Replaces a double quote with two double quotes
					$dataElement = str_replace("\"", "\"\"", $dataElement);
					$dataElement = str_replace("\n", "", $dataElement);
					$dataElement = str_replace("\r", "", $dataElement);

					// Adds a delimiter before each field (except the first)
					if ($writeDelimiter)
						$string .= $delimiter;

					// Encloses each field with $enclosure and adds it to the string
					$string .= $enclosure . $dataElement . $enclosure;

					// Delimiters are used every time except the first.
					$writeDelimiter = TRUE;
				}
				// Append new line
				$string .= "\n";

				fwrite($fh, $string);
			} // end foreach($dataArray as $line)
			fclose($fh);
			return $filename;
		}

		public function executeDownloadCsv(sfWebRequest $request) {
			$fileName = $request->getParameter('fileName') . '.csv';
			$folder = $request->getParameter('folder');
			#sfLoader::loadHelpers('Asset');
			sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
			//        print $$folder;
			$folder_path = self::$$folder;
			$filePath = addslashes(sfConfig::get('sf_web_dir') . '/uploads' . $folder_path . '/' . $fileName);
			$this->setLayout(false);
			sfConfig::set('sf_web_debug', false);

			// Adding the file to the Response object
			// $this->getResponse()->setHttpHeader("Content-Length: ",filesize($filePath),TRUE);
			//$this->getResponse()->setHttpHeader('Content-type','application/csv',TRUE);
			$this->getResponse()->setHttpHeader('Content-type', 'application/force-download');
			$this->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileName}", TRUE);
			$this->getResponse()->sendHttpHeaders();
			$this->getResponse()->setContent(file_get_contents($filePath));
			return sfView::NONE;
		}



	public function executeMerchantSupportSearchByParams(sfWebRequest $request) {
        $this->merchant_id = 0; //initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
        $this->merchant_service_id = 0; //same as the reason above
        $this->selected = '';
        $this->form = new SupportForm();
        $this->searchByTrans = new SupportTransID();
        $this->seachByKey = $request->getGetParameter('searchBy');
        if ($request->hasParameter('selected')) {
            $this->selected = $request->getParameter('selected');
        }
        /* For Search by Validation Number */
        $this->searchByVal = new SupportValNo();

        $this->msg = "";
        $this->path_info = '';

        if ($request->isMethod('post') && $this->searchByTrans->getDefault('TransFormVal') == 1) {
            $this->searchByTrans->bind($request->getParameter('merchant_request'));

            if ($this->searchByTrans->isValid())
            //$this->formValid = "validForm";
                $this->redirect('support/searchSupportfrmtransno?' . http_build_query($this->searchByTrans->getValues()));

        }
    }
    public function getBrowser() {
    	if (!$this->browserInstance) {
    		$this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
    				array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false, 'timeout' => sfConfig::get('app_notification_request_timeout')));
    	}

    	return $this->browserInstance;
    }
    /**
     * view log for Merchant Integration Support Section
     * @param sfWebRequest $request
     * Developed By :Deepak Bhardwaj
     */
    public function executeMerchantSupportViewLog(sfWebRequest $request) {
    	$this->setLayout('layout_popup');
        $this->transactionNo = $request->getParameter('transactionNo');
    	$logDetailObj = MerchantLogTable::getMerchantSupportLog($this->transactionNo);
    	//$logDetailObj = $supportObj->getLogDetail($this->transactionNo);
    	$this->page = 1;
    	if ($request->hasParameter('page')) {
    		$this->page = $request->getParameter('page');
    	}
    	$this->pager = new sfDoctrinePager('MwMessageLog', sfConfig::get('app_records_per_page'));
    	$this->pager->setQuery($logDetailObj);
    	$this->pager->setPage($this->page);
    	$this->pager->init();
    }
    
    /**
     * Function to create txt file in log folder.
     * @param string $response
     * @param integer $mwRequestId
     * Developed By :-Deepak Bhardwaj
     */
    
    public function executeMiddlewareViewLog(sfWebRequest $request) {
    	$this->transactionNo = $request->getParameter('transactionNo');
    	$this->setLayout('layout_popup');
    	
    	$logDetailObj = MwMessageLogTable::getLogDetail($this->transactionNo);
    	//$logDetailObj = $supportObj->getLogDetail($this->transactionNo);
    	$this->page = 1;
    	if ($request->hasParameter('page')) {
    		$this->page = $request->getParameter('page');
    	}
    	$this->pager = new sfDoctrinePager('MwMessageLog', sfConfig::get('app_records_per_page'));
    	$this->pager->setQuery($logDetailObj);
    	$this->pager->setPage($this->page);
    	$this->pager->init();
    }
    
}
