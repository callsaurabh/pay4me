<?php

/**
 * api actions.
 *
 * @package    mysfp
 * @subpackage api
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class apiActions extends sfActions
{
 
    public $error_parameter = array();

    public function executeUsers(sfWebRequest $request) {


        $this->setLayout(NULL);
        $apiObj = new apiManager;
        $returnVal = $apiObj->processApi($request) ;
        return $this->renderText($returnVal) ;
    }

    public function executeBankProperties(sfWebRequest $request) {
        $logger=sfContext::getInstance()->getLogger();
        try {
            $this->setLayout(NULL);
            //if of ip address sucessfull
            if($this->checkIpAddress()){
                $apiObj = new apiManager;
                $returnVal = $apiObj->renderProperties($request);
                $logger->info("Bank connect ip:: sending properties".$returnVal);
                return $this->renderText($returnVal);
            } else {
            //else return render 0
                return $this->renderText('0');
            }
        }
        catch(Exception $e) {           
            return $this->renderText('0');
        }
    }


   private function checkIpAddress(){
        $logger=sfContext::getInstance()->getLogger();
        $objApiHelper = new APIHelper();
        $clientIpAddress = $objApiHelper->getClientIpAddress();
        $arrDetails = explode(".",$clientIpAddress);
        $iptocheck = $arrDetails[0].".".$arrDetails[1].".".$arrDetails[2];     
        if($iptocheck == sfConfig::get('app_bank_connect_ip')){
            $logger->info("Bank connect ip is configured properly to ".$iptocheck);
            return true;
        }
        $logger->debug("Bank connect ip is not configured properly.It should be ".$iptocheck." instead of ".sfConfig::get('app_bank_connect_ip'));
        return false;
    }


    public function executeMerchant(sfWebRequest $request) {
        $this->setLayout(NULL);
        $apiObj = new apiManager;
        $returnVal = $apiObj->processMerchantApi($request) ;
        return $this->renderText($returnVal) ;
    }

    public function executeQuery(sfWebRequest $request) {
        $this->setLayout(NULL);
        $apiObj = new apiManager;
        $returnVal = $apiObj->processQueryApi($request) ;
        return $this->renderText($returnVal) ;
    }


     public function executeOrder_History(sfWebRequest $request) {

        try{
            $this->setLayout(NULL);
            $version = $request->getParameter('version') ;
            $merchantParamName = sfConfig::get('app_merchant_url_param_name') ;
            if($version==""){
                     $version = 'v1';
            }
           
            if(strtolower($version)=='v1'){
                $pay4MeObj = pay4MeServiceFactory::getService(strtolower($version)) ;
                $returnVal = $pay4MeObj->processHistory($request);
                return $this->renderText($returnVal) ;
            }else{
                //echo "aaaaa";die;
                throw New Exception("INVALID_VERSION");
            }
         } catch (Exception $ex) {

            $msg = $ex->getMessage();
            $logger=sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: ".$msg);
            return $this->renderText(pay4MeUtility::errorhistory($msg,'','',array()));

        }
        

    }

    public function executeEmaildetails(sfWebRequest $request){
        try {
            


        } catch(Exception $e){
            echo("problem found");die;
        }


    }

    public function executeFeatureXMLGeneration(sfWebRequest $request){
        //$this->setLayout(null);
        $bank_code =  $request->getParameter('bank');
        $system_id =  $request->getParameter('system-id');
        $instance_name =  $request->getParameter('instance-name');
        $currentTime  = date('H:i:s');
        //$bankDetails = Doctrine::getTable('Bank')->getBankDetailByBankCode($bank_code);
        if($this->checkIpAddress()){
            $bankmwDetails = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id,$instance_name);

            $bankID = $bankmwDetails->getFirst()->getBankId();
            $bankDetails = Doctrine::getTable('Bank')->find($bankID);

            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;
            $root = $doc->createElement('features');
            $root = $doc->appendChild($root);
            $feature = $doc->createElement("feature");
            $feature->setAttribute("name", ucfirst($bankDetails->getAcronym()));
            $root->appendChild($feature);

            $directory_name_bank = sfConfig::get('sf_web_dir') ."/". sfConfig::get('app_bundles_soft_link_bankspecific') . "/". $bankDetails->getAcronym();
            $bankIntObj = new bankIntegrationHelper();
            $this->fileLinks = $bankIntObj->ReturnSoftLinksArray($directory_name_bank, $bankDetails->getAcronym());
            $Maxversion = 0;
            sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
            foreach($this->fileLinks as $fileKey => $filevalue ){

                $version = explode('/',readLink($filevalue['softLink']));
                $Maxversion = ($version[7]>$Maxversion)?$version[7]:$Maxversion;
                $bundle = $doc->createElement("bundle");
                //$bundle->appendChild($doc->createTextNode("https://www.pay4me.com/system-id/".$system_id."/instance-name/".$instance_name."/".$fileKey));
                $bundle->appendChild($doc->createTextNode(public_path(sfConfig::get('app_bundles_soft_link_bankspecific')."/".$bankDetails->getAcronym()."/".$fileKey,true)));
                $root->appendChild($bundle);
                $feature->appendChild($bundle);
            }
            $feature->setAttribute("version", $Maxversion);

            $xmldata = $doc->saveXML();
            $sysInfo = 'System-id:'.$system_id;
            $this->createLog($sysInfo."\n".$xmldata,'featurexml_'.$instance_name.'_'.$bankDetails->getBankName().'_'.$currentTime);
            header('Content-type: text/xml');
            echo $xmldata;
        }else{
            echo $this->renderText('0');
        }
        die;
    }


    public function createLog($xmldata, $type) {
    $nameFormate = $type;
    $pay4meLog = new pay4meLog();
    $pay4meLog->createLogData($xmldata,$nameFormate,'featurexml', false);
  }

    
}
