<?php

/**
 * nibssSetting actions.
 *
 * @package    mysfp
 * @subpackage nibssSetting
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class nibssSettingActions extends sfActions {
  public function executeIndex(sfWebRequest $request) {
    $this->nibss_settings_list = Doctrine::getTable('NibssSettings')
        ->createQuery('a')
        ->execute();
  }

  public function executeEdit(sfWebRequest $request) {
    $this->forward404Unless($nibss_settings = Doctrine::getTable('NibssSettings')->find($request->getParameter('id')), sprintf('Object nibss_settings does not exist (%s).', $request->getParameter('id')));
    $this->form = new NibssSettingsForm($nibss_settings);
  }

  public function executeUpdate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($nibss_settings = Doctrine::getTable('NibssSettings')->find($request->getParameter('id')), sprintf('Object nibss_settings does not exist (%s).', $request->getParameter('id')));
    $this->form = new NibssSettingsForm($nibss_settings);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid()) {
      $nibss_settings = $form->save();
      $this->getUser()->setFlash('notice', sprintf('Nibss settings updated successfully'));
      $this->redirect('nibssSetting/index');
    }
  }

  public function executeGetMailData() {
    $this->setLayout(null);
    $nibssData = Doctrine::getTable('NibssSettings')->getNibssDetails() ;
    foreach($nibssData as $nibssSetting) {
      $popserver_nibss = sfconfig::get('app_nibss_response_popserver_nibss');
      $port_nibss = sfconfig::get('app_nibss_response_port_nibss');
      $ServerName = "{".$popserver_nibss.":".$port_nibss."/pop3/ssl/novalidate-cert}INBOX";
      $UserName = $nibssSetting['username'] ;
      $PassWord = $nibssSetting['password'] ;
    }


    $mbox = imap_open($ServerName, $UserName,$PassWord) or die("Could not open Mailbox - try again later!");
    print_r(imap_errors());
    //include_once("includes.php") ;

    if ($hdr = imap_check($mbox)) {

      $msgCount = $hdr->Nmsgs;
    //print_r($hdr->Nmsgs) ;exit;
    } else {
    }
    $MN=$msgCount;

    $overview=imap_fetch_overview($mbox,"1:$MN",0);
    $size=sizeof($overview);

    $bgColor = "#F0F0F0" ;
    for($i=$size-1;$i>=0;$i--) {
      $val=$overview[$i];
      $msg=$val->msgno;
      $from=$val->from;
      $date=$val->date;
      $subj=$val->subject;
      $seen=$val->seen;

      $from = preg_replace("/\"/","",$from);

      // MAKE DANISH DATE DISPLAY
      list($dayName,$day,$month,$year,$time) = split(" ",$date);
      $time = substr($time,0,5);
      $date = $day ." ". $month ." ". $year . " ". $time;


      if (strlen($subj) > 60) {
        $subj = substr($subj,0,59) ."...";
      }

      $header = imap_headerinfo($mbox, $msg) ;
      $from = $header->from;
      foreach ($from as $id => $object) {
        $fromname = $object->personal;
        $fromaddress = $object->mailbox . "@" . $object->host;
      }
      //  print $fromaddress;
      if($fromaddress == $nibssSetting['message_to'] ) {
        $this->getAttachements($mbox,$msg) ;
      }

    }

    imap_close($mbox);
    return $this->renderText("Processing of inbox successful");

  }


  //Anurag: Attachement with the mail start
  protected function getAttachements($mbox,$msgno) {

    $struct = imap_fetchstructure($mbox,$msgno);
    $contentParts = count($struct->parts);

    if ($contentParts >= 2) {
      for ($i=2;$i<=$contentParts;$i++) {
        $att[$i-2] = imap_bodystruct($mbox,$msgno,$i);
      }
      for ($k=0;$k<sizeof($att);$k++) {
        if ($att[$k]->parameters[0]->value == "us-ascii" || $att[$k]->parameters[0]->value    == "US-ASCII") {
          if ($att[$k]->parameters[1]->value != "") {
            $selectBoxDisplay[$k] = $att[$k]->parameters[1]->value;
          }
        } elseif ($att[$k]->parameters[0]->value != "iso-8859-1" &&    $att[$k]->parameters[0]->value != "ISO-8859-1") {
          $selectBoxDisplay[$k] = $att[$k]->parameters[0]->value;
        }
      }
    }

    if (sizeof($selectBoxDisplay) > 0) {
      ;
      for ($j=0;$j<sizeof($selectBoxDisplay);$j++) {
       $file = $selectBoxDisplay[$j] ;
        $strFileName = $file ; //$att[$file]->parameters[0]->value;
        $strFileType = strrev(substr(strrev($strFileName),0,4));
        $fileContent = imap_fetchbody($mbox,$msgno,$j+2);
        if ($strFileType == ".xls" || $strFileType == ".csv"  ) {

          $name = sfConfig::get('app_nibss_file_folder').$file ;
          $f=fopen($name,'wb');
          fwrite($f,imap_base64($fileContent),strlen($fileContent));
          fclose($f);



          $receivedFile = explode(' ', trim($file)) ;
          //  print_r($receivedFile);
          $file_prefix = $receivedFile[0];
          $fileStatusType = explode('.', trim($receivedFile[1])) ;
          $mandate_file_id = explode('_', $receivedFile[0]) ;

          // print Doctrine::getTable('PaymentBatch')->getPaymentBatch($mandate_file_id[2],$file_prefix );
          $groupDetail = Doctrine::getTable('PaymentBatch')->getPaymentBatch($mandate_file_id[2],$file_prefix );
          if($groupDetail) // $this->PaymentMandate->find(array('PaymentMandate.id' => $mandate_file_id[2] )))
          {


            if(strtoupper($fileStatusType[0]) == 'INVALID' || strtoupper($fileStatusType[0]) == 'VALID' || strtoupper($fileStatusType[0]) == 'PAYMENT' ) {
              if($strFileType == ".xls" && strtoupper($fileStatusType[0]) == 'PAYMENT') {

                $this->readXLSFile($name, $mandate_file_id[2], $fileStatusType[0]) ;
              }
              elseif($strFileType == ".csv" && (strtoupper($fileStatusType[0]) == 'INVALID' || strtoupper($fileStatusType[0]) == 'VALID'))

                $this->readCSVFile($name, $mandate_file_id[2], $fileStatusType[0]) ;

            }
          }
        }
      }
    }

  }

  public function readXLSFile($fileName, $mandateId, $fileType) {
    $data = new Spreadsheet_Excel_Reader();

    $data->setUTFEncoder('iconv');
    // Set output Encoding.
    $data->setOutputEncoding('CP1251');
    $data->read($fileName);


    error_reporting(E_ALL ^ E_NOTICE);
    if(strtoupper($fileType) == "PAYMENT") {
    //      print "<br><br>0....".$data->sheets[0]['numRows'];
    //      print "<br><br>1....".$data->sheets[1]['numRows'];      exit;
      if($data->sheets[0]['numRows'] > 0) {
        $status = 23;  // 23 PAID

        $status_split = 'paid';
        $con = Doctrine_Manager::connection();

        try {
          $con->beginTransaction();
          for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
            $payment_record_id = $data->sheets[0]['cells'][$i][1] ;
            $amount =  $data->sheets[0]['cells'][$i][4]/100 ;

            //get the master_account_id of from_account

            if(Doctrine::getTable('PaymentRecord')->isPaymentRecord($mandateId, $payment_record_id) ) {

              Doctrine::getTable('PaymentRecord')->updatePaymentRecord($payment_record_id, $status, date("Ymd")) ;
              $paymentRecordObj = Doctrine::getTable('PaymentRecord')->getRecordDetail($payment_record_id);
              //$paymentRecordObj->getAmount();
              $account_name = $data->sheets[0]['cells'][$i][5];
              $account_name= preg_replace('/[^\w\d_ -]/si', '', $account_name);
              //$account_name=htmlentities($account_name,ENT_COMPAT,ISO-8859-15);
               $acct_obj = $paymentRecordObj->getFirst()->getEpMasterAccount();
              if(strpos(sfconfig::get("app_nibss_account_name"),$account_name)  !== false) {
                $nibss_account_id = $acct_obj->getId();
                $amount_nibss = $data->sheets[0]['cells'][$i][4];
              }
              else {
                $virtualacctObj = $acct_obj->getVirtualAccountConfig();
                //              print "<br>".$virtualacctObj->getVirtualAccountId()."<br>";
                if((is_object($virtualacctObj)) && ($virtualacctObj->getVirtualAccountId())) {
                  $credited_acct[$i-1]['amount'] = $data->sheets[0]['cells'][$i][4] ;
                  $credited_acct[$i-1]['virtual_account_id'] = $virtualacctObj->getVirtualAccountId();
                  $credited_acct[$i-1]['account_name'] = $account_name;
                  $credited_acct[$i-1]['physical_account_id'] = $virtualacctObj->getPhysicalAccountId();
                }
              }

            }
          }

          //        print "<pre>";
          //        print_r($credited_acct);exit;
          //update ep_master_account and ep_master_ledger
          $total_amount = 0;
          $attrArray = array();
          if(isset($credited_acct) && (count($credited_acct) > 0)) {
          //  print "in";exit;

            foreach($credited_acct as $acct_detail) {
            //create the object
              $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$RESPONSE_FROM_NIBSS, array('batch_id'=>$mandateId));
              $accountingObj = new pay4meAccounting();
              if(strpos(trim(sfconfig::get("app_pay4me_account_name")),trim($acct_detail['account_name']))  !== false) {
                
                $pay4me_amount = $acct_detail['amount']+$amount_nibss;
                $attr = $accountingObj->getAccountingHolderObj($description, $pay4me_amount, $acct_detail['virtual_account_id'], $isCleared=1, $enter_type=EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
                $attrArray[] = $attr;
              }
              else {
                $attr = $accountingObj->getAccountingHolderObj($description, $acct_detail['amount'], $acct_detail['virtual_account_id'], $isCleared=1, $enter_type=EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
                $attrArray[] = $attr;
              }


              $attr = $accountingObj->getAccountingHolderObj($description, $acct_detail['amount'], $acct_detail['physical_account_id'], $isCleared=1, $enter_type=EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
              $attrArray[] = $attr;

              $total_amount = $total_amount+$acct_detail['amount'];

            }
            //$batchObj = Doctrine::getTable('PaymentBatch')->find($mandateId);
            // $from_acct = $batchObj->getAccountId();
            if($nibss_account_id) {
              $attr = $accountingObj->getAccountingHolderObj($description, $amount_nibss, $nibss_account_id, $isCleared=1, $enter_type=EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
              $attrArray[] = $attr;
            }

//                            print "<pre>";
//                            print_r($attrArray);exit;
            $event = $this->dispatcher->notify(new sfEvent($attrArray, 'epUpdateAccounts'));
          }
          $con->commit();
        }
        catch (Exception $e) {
          $con->rollback();
          throw $e;
        }
      }
      if($data->sheets[1]['numRows'] > 0) {
        $status = 82;  // 82 UNPAID
        $status_split = 'unpaid';
        for ($i = 1; $i <= $data->sheets[1]['numRows']; $i++) {
          $payment_record_id = $data->sheets[1]['cells'][$i][1] ;
          //  print "<pre>";print_r($payment_record_id);

          if(Doctrine::getTable('PaymentRecord')->isPaymentRecord($mandateId, $payment_record_id ) ) {
            Doctrine::getTable('PaymentRecord')->updatePaymentRecord($payment_record_id, $status, date("Ymd")) ;
          }
        }
      }
    }
  }

  public function readCSVFile($fileName, $mandateId, $fileType) {
    $reader = new CSVReader( new FileReader( $fileName ) );
    $reader->setSeparator( ',' );
    $fileType = strtoupper($fileType);
    if($fileType == "VALID" || $fileType == "INVALID" ) {
      ($fileType == "VALID")? $status = 22: $status = 81 ; // 22 VALID and 81 INVALID
      ($fileType == "VALID")? $status_split = 'verified_by_nibss': $status_split = 'rejected_from_nibss' ; // 22 VALID and 81 INVALID
      $i = 0 ;
      while( false != ( $cell = $reader->next() ) ) {

        if(Doctrine::getTable('PaymentRecord')->isPaymentRecord($mandateId, $cell[0]) ) {
          Doctrine::getTable('PaymentRecord')->updatePaidUnpaid($cell[0], $status, date("Ymd")) ;
        }
        $i++ ;
      }

    }
  }


}
