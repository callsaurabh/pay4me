<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<div class="dlForm">
  <fieldset>
    <?php 
      echo ePortal_legend('NIBSS Setting List');
      if(count($nibss_settings_list) > 0)
      {
        foreach ($nibss_settings_list as $nibss_settings)
        {
          echo formRowFormatRaw('File Name',$nibss_settings->getFilename());
          echo formRowFormatRaw('Message From',$nibss_settings->getMessageFrom());
          echo formRowFormatRaw('Message To',$nibss_settings->getMessageTo());
          echo formRowFormatRaw('Message CC',$nibss_settings->getMessageCc());
          echo formRowFormatRaw('Payor',$nibss_settings->getPayor());
          echo formRowFormatRaw('Message from',$nibss_settings->getMessageFromName());
          echo formRowFormatRaw('Pop Server',$nibss_settings->getPopserver());
          echo formRowFormatRaw('Port',$nibss_settings->getPort());
          echo formRowFormatRaw('User Name',$nibss_settings->getUsername());
    ?>
          <table>
            <tr>
              <td colspan="2">
              <?php  echo button_to('Edit NIBSS Info','',array('onClick'=>'location.href=\''.url_for('nibssSetting/edit?id='.$nibss_settings->getId()).'\''));?>
            </tr>
          </table>
    <?php
        }
      }
      else
      {
        echo "<B>NIBSS settings not found</B>";
      }
    ?>

