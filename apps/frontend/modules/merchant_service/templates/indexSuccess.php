<div id="theMid">
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

 
        <?php echo ePortal_listinghead('Merchant Service List'); ?>
 
  <div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
        </tr>
      </table>
    <br class="pixbr" />
  </div>

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Merchant</th>
      <th>Merchant Services</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
     if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
    ?>
    <tr class="alternateBgColour">
      <td><?php echo $i ?></td>
      <td align="center"><?php echo $result->getMerchantName() ?></td>
      <td align="center"><?php echo $result->getName() ?></td>
     <td align="center"><?php echo link_to(' ', 'merchant_service/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
      <?php echo link_to(' ', 'merchant_service/delete?id='.$result->getId(), array('method' => 'delete', 'confirm' => 'Merchant Service once deleted cannot be created again. Are you sure, you want to delete Merchant Service?', 'class' => 'delInfo', 'title' => 'Delete')) ?></td>
    </tr>
    <?php endforeach; ?>
     <tr><td colspan="5">
<div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

  </div></td></tr>

    <?php }else{ ?>
     <tr><td  align='center' class='error'>No Merchant Services found</td></tr>
    <?php } ?>

  </tbody>
   
</table>
</div>
<?php  echo button_to('Add New','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('merchant_service/new').'\''));?>
</div>
</div>