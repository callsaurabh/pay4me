<?php

/**
 * merchant_service actions.
 *
 * @package    mysfp
 * @subpackage merchant_service
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class merchant_serviceActions extends sfActions {
    public function executeIndex(sfWebRequest $request) {
        $merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BASE);
        $this->merchant_service_list = $merchant_service_obj->getAllRecords();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('MerchantService',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->merchant_service_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeShow(sfWebRequest $request) {
        $this->merchant_service = Doctrine::getTable('MerchantService')->find($request->getParameter('id'));
        $this->forward404Unless($this->merchant_service);
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = new MerchantServiceForm();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));

        $this->form = new MerchantServiceForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($merchant_service = Doctrine::getTable('MerchantService')->find($request->getParameter('id')), sprintf('Object merchant_service does not exist (%s).', $request->getParameter('id')));
        $this->form = new MerchantServiceForm($merchant_service);
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($merchant_service = Doctrine::getTable('MerchantService')->find($request->getParameter('id')), sprintf('Object merchant_service does not exist (%s).', $request->getParameter('id')));
        $this->form = new MerchantServiceForm($merchant_service);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($merchant_service = Doctrine::getTable('MerchantService')->find($request->getParameter('id')), sprintf('Object merchant_service does not exist (%s).', $request->getParameter('id')));
        $merchant_service->delete();
        $this->getUser()->setFlash('notice', sprintf('Merchant Service deleted successfully'));
        $this->redirect('merchant_service/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $merchant_service_params = $request->getParameter('merchant_service');
            $merchant_id = $merchant_service_params['merchant_id'];
            $name = $merchant_service_params['name'];
            $id = $merchant_service_params['id'];
            $merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BASE);
            $already_created = $merchant_service_obj->checkDuplicacy($merchant_id,$name,$id);
            if($already_created) {
                $this->getUser()->setFlash('error', sprintf('This service has already been added for the selected Merchant'));
            // $this->redirect('bank_branch/index');
            }
            else {

                $merchant_service = $form->save();
                if(($request->getParameter('action')) == "update") {
                    $this->getUser()->setFlash('notice', sprintf('Merchant Service updated successfully'));
                }
                else if(($request->getParameter('action')) == "create") {
                        $this->getUser()->setFlash('notice', sprintf('Merchant Service added successfully'));
                    }
                $this->redirect('merchant_service/index');
            }
        }
    }
}
