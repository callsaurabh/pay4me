<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<?php //use_helper('Form'); ?>

<div class="wrapForm2">

    <?php echo ePortal_listinghead(__('eWallet Secure Certificate')); ?>
<?php echo form_tag($sf_context->getModuleName().'/'.$sf_context->getActionName(),array('name'=>'signup','id'=>'signup','method'=>'post','enctype'=>'multipart/form-data', 'onSubmit'=>'return validateSignForm()')); ?>
<?php
    switch($kycStatus)
    {
      case 2:
        
?>

     <div class="descriptionArea">
        
        <?php echo __('Thank you for applying a Pay4Me eWallet Secure Certificate.')?><br><br>

         <?php echo __('Now your account is queued for approval.')?><br>
         <?php echo __('You will be notified when an action is recorded on your application.')?>
        
    </div>

<?php  break; ////////////Key status 2 ////////////
      case 1:
      
?>

     <div class="descriptionArea">
          <?php echo __('Congratulations! Your application for eWallet Secure Certificate has been approved.')?>
          <br>          
          <?php echo __('You can now use your eWallet Transfer facility.')?>

        
    </div>

<?php  break;////////////Key status 1 /////////////
case 3:

?>

     <div class="descriptionArea">
        
            <?php echo __('We regret to inform you that, Your application for eWallet Secure Certificate is disapproved due to')?> <b><?php echo $disapproveReason;?></b>.
            <?php echo __('Please apply for a fresh eWallet account after correcting the reasons mentioned above.')?>

        
    </div>

<?php  break;////////////Key status 3 /////////////
    case 0:
      
?>
    <?php include_partial('kycForm', array('form' => $kycForm,'currency'=>$currency)); ?>

   
<div class="divBlock">
    <center>
         <input type="submit" class="formSubmit" value="Apply for Certificate" name="commit">
    </center>
</div>
 <div class="passPolicy" >
  <h3 style="color:#000 !important; font-size: 13px;">Guidelines for eWallet Secure Certificate</h3>
 
    <li>Please affix most recent colour photograph. In photograph  hat and glasses are not allowed.</li>
    <li>Supporting Documents should be valid and issued by any Government officials.</li>
</div>
<iframe id="uploadIframe" name="uploadIframe" height="10px;" width="10px;" style="display:none;"></iframe>



<?php  break;////////////Key status 0 /////////////
    }
   
?>


</form>

</div>


<script>
    function uploadDocTemp(docNum)
    {
        var docSize = document.getElementById('totsize').value;
        var docName = document.getElementById('tmpDocName').value;

        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/uploadDoc');?>?docSize="+docSize+"&docNum="+docNum+"&docName="+docName;
        document.forms[0].target = "uploadIframe";
        document.forms[0].submit();

    }
    function uploadImageTemp()
    { 
        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/uploadImage');?>";
        document.forms[0].target = "uploadIframe";
        document.forms[0].submit();
    }
    function addMoreDoc()
    {
        var totdoc = document.getElementById("totdoc").value;
        if(totdoc>=3){
            alert('Your can upload maximum three document.');
            return false;
        }

        if($("#totsize").val() >= 200){
            alert('Your uploaded size(200kb including image) has been exceeded.');
            return false;
        }

        ++totdoc;
        var divid = "div_"+totdoc;

        var divTag = document.createElement("div");
        divTag.id = divid;


        divTag.innerHTML = '<dl id="username_row">\n\
                <div class="dsTitle4Fields">\n\
                <label for="upload"><?php echo __('Upload Document')?> '+totdoc+'</label>\n\
                </div>\n\
                <div class="dsInfo4Fields">\n\
                <input id="doc'+totdoc+'"  type="file" value="" onchange=uploadDocTemp('+totdoc+') name="doc'+totdoc+'"/>\n\
                &nbsp;&nbsp;&nbsp;<input id="chkDoc'+totdoc+'" type="checkbox" value="true" name="chkDoc'+totdoc+'" disabled=true"  />\n\
                <div id="errorDoc'+totdoc+'" class="cRed"/>\n\
                </div>\n\
                </dl>';

        document.getElementById("doc_div").appendChild(divTag);
        document.getElementById("totdoc").value = totdoc;
        if(3==totdoc){
            document.getElementById("addDoc").style.display='none';
        }

    }
    function validateSignForm()
    {
        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/'.$sf_context->getActionName());?>";
        document.forms[0].target ='';
        err = 0;
        if(!$('#chkImage').is(':checked'))
        {
            $('#error_img').html("<?php echo __("Please  Upload  Image and check the box")?>");
            err = err+1;
        }
        else
        {
            $('#error_img').html("");
        }

        if( !$('#chkDoc1').is(':checked') && !$('#chkDoc2').is(':checked') && !$('#chkDoc3').is(':checked') )
        {
            $('#err_alldoc').html("<?php echo __("Please  Upload Documents  and check the  box")?>");
            err = err+1;
        }
        else
        {
            $('#err_alldoc').html("");
        }
        if( !$('#confirmUpload').is(':checked')  && !( !$('#chkDoc1').is(':checked') && !$('#chkDoc2').is(':checked') && !$('#chkDoc3').is(':checked') ))
        {
            $('#err_confirmUpload').html("<?php echo __("Please  confirm the uploaded image and documents")?> ");
            err = err+1;
        }
        else
        {
            $('#err_confirmUpload').html("");
        }
            if(err == 0)
            {             
                return true;
            }
            else
                return false;
    }
</script>