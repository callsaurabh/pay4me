<?php //use_helper('Form');?>
<div id="printSlip"> 

<?php echo ePortal_pagehead(__("eWallet Recharge Receipt"),array('id'=>'dynamicHeading')); ?>
<div class="wrapForm2 wrapdiv">
<?php echo form_tag('',array('name'=>'pfm_receipt_form','id'=>'pfm_receipt_form')) ?>

<div id="update_msg"></div>
<?php
if($err){
   print "<div id='flash_error' class='error_list'><span>".$err."</span></div>";
}
if(!$err){
  
?>
  
<?php echo ePortal_legend(__('eWallet Recharge Details'));?>
<?php echo formRowComplete(__('Validation Number'),$validation_number);?>
<?php echo formRowComplete(__('Transaction Identification Number'),$transactionRef);?>
<?php echo formRowComplete(__('Account Name'),$accountObj->getAccountName(),'','name','','name_row'); ?>
<?php echo formRowComplete(__('Account No.'),$accountObj->getAccountNumber(),'','acct_no','','acct_no_row'); ?>
<?php if($serviceCharge) echo formRowComplete(__('Service Charges'),format_amount($serviceCharge,$currencyId,1),'','acct_no','','amount_row'); ?>
<?php echo formRowComplete(__('Recharge Amount'),format_amount($rechargeAmount,$currencyId,1),'','acct_no','','amount_row'); ?>
<?php echo formRowComplete(__('Total Amount'),format_amount($amount,$currencyId,1),'','acct_no','','amount_row'); ?>
<?php if ($isSuperadmin) { ?>
<?php echo formRowComplete(__('Name'),$userDetailObj->getName()." ".$userDetailObj->getLastName(),'','name','','name_row'); ?>
<?php echo formRowComplete(__('Address'),$userDetailObj->getAddress(),'','address','','address_row'); ?>
<?php echo formRowComplete(__('Email'),$userDetailObj->getEmail(),'','email','','email_row'); ?>
<?php echo formRowComplete(__('Mobile No'),$mobileN0 = ($userDetailObj->getMobileNo())?$userDetailObj->getMobileNo():'#','','mobile_no','','mobile_no_row'); } ?>

 <?php echo ePortal_legend('Recharge Status');
    echo formRowComplete(_('Payment Mode'),$gatewayOrderObj->getFirst()->getPaymentModeOption()->getPaymentMode()->getDisplayName(),'','name','','name_row');
//    if($paymentModeOption == 5 || $paymentModeOption == 3 ) {
        echo ($gatewayOrderObj->getFirst()->getPan()?formRowComplete('Card Number',$gatewayOrderObj->getFirst()->getPan()):'');//FS#30027
        echo ($gatewayOrderObj->getFirst()->getApprovalCode()?formRowComplete('Approval Code',$gatewayOrderObj->getFirst()->getApprovalCode()):'');//
 //   }
     if($paymentModeOption == 2) {
    echo formRowComplete(_('Transaction Response Description'),$gatewayOrderObj->getFirst()->getResponseTxt(),'','desc','','desc_row');
    
    }

    echo formRowComplete(_('Status'),$gatewayOrderObj->getFirst()->getStatus(),'','status','','status_row');
    echo formRowComplete(__('Transaction Date'),formatDate($rechargeDate),'','date','','date_row');
    if($checkPaymentModeOption)
    echo formRowComplete('Card Debited On',date("d M, Y",strtotime($gatewayOrderObj->getFirst()->getTransactionDate())));
    
   ?>
    


</form>
</div>
<?php }?>
</div>
 <div class="paging pagingFoot">
   <center>
    <input type="button" style = "cursor:pointer;" value="<?php echo __("Print User Receipt")?>" class="formSubmit" onclick="printSlip('user')">
    
   </center>
  </div>
<script>
  function printSlip(mode){

    defHeading = $('#dynamicHeading').html();
    heading = "";
    if(mode =='user'){
      heading +="User Receipt";
    }else if (mode =='bank'){
      heading +="Bank Receipt";
    }
    $('#dynamicHeading').html(heading);
    html = '';
    html += $('#printSlip').html();
   // alert(html);
    printMe(html,true);
    $('#dynamicHeading').html(defHeading);

  }
</script>
