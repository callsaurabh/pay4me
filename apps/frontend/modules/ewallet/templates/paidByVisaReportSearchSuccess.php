<?php //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<table><tr><td>&nbsp;</td></tr></table>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>





<div class="wrapTable" style="width:99%; overflow-x: auto; margin:auto; padding:2px;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <thead>
      <tr class="horizontal">
        <th width = "2%">S.No.</th>
        <?php $payType = $sf_request->getParameter('paidwithvisa');
        $isPending = $payType['trans_type'];
        $pending = 0;
        $pendingUrl = '&pending='.$isPending;
        if(strtolower($isPending) == 'pending')
        {
//          $pending = 1;
//          $pendingUrl ='&pending='.$pending;
        }?>

        <th width="15%" align="center">Transaction Number</th>
        <?php if(strtolower($isPending) != 'pending') { ?>
        <th width = "20%">Transaction Date</th>
        <?php } ?>
        <th width = "20%">Amount (<?php echo image_tag('/img/naira.gif'); ?>)</th>
        <th width = "30%">Status</th>
        <th width = "13%">View details</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
        $transaction_date = $result->getTranstime();
        $amount = format_amount($result->getPurchasedAmount(), '',1);
        // for currency   ewalletActions::getCurrencyId($result->getCurrency())?ewalletActions::getCurrencyId($result->getCurrency()):
        ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <?php
        if(strtolower($payType['pay_type'])== 'payment'){?>
        <td align="center"><?php echo $result->getAppId();?></td>
        <?php }else{ ?>
        <td align="center"><?php echo $result->getOrderId();?></td>
        <?php } ?>
         <?php if(strtolower($isPending) != 'pending') { ?>
        <td align="center"><?php echo ($pending)? $result->getCreatedAt() : $transaction_date;?></td>
         <?php } ?>
        <td align="right"><?php echo  $amount; ?></td>
        <td align="center"><?php echo  $result->getStatus() ; ?>
        </td>
        <?php
        if(strtolower($payType['pay_type'])== 'payment'){?>
        <td align="center"><?php echo link_to('View', 'ewallet/viewDetail?transId='.$result->getAppId().'&type=payment'.$pendingUrl, array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes') ,
        'target'=>'_blank') ); ?></td>
    <?php }else{ ?>
        <td align="center"><?php echo link_to('View', 'ewallet/viewDetail?transId='.$result->getOrderId().'&type=recharge'.$pendingUrl, array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes') ,
        'target'=>'_blank') ); ?></td>
    <?php } ?>
      </tr>
      <?php endforeach; ?>

      <tr><td colspan="6">
          <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'search_results')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

      </div></td></tr>



        <?php }
      else { ?>
      <tr><td  align='center' class='error' colspan="6">No Record Found</td></tr>
      <?php } ?>

    </tbody>

  </table>
</div>
