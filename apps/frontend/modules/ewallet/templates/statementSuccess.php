<?php //echo ePortal_pagehead(" ",array('class'=>'_form'));  ?>
<?php use_helper('Pagination'); ?>

<table><tr><td>&nbsp;</td></tr></table>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th width="100%" >
                <span class="floatLeft"><?php echo __('Found') ?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.') ?></span>
                <span class="floatRight"><?php echo __('Showing') ?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total') ?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results') ?></span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>



<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <td align="center"> <b><?php echo __('Account No') ?>:</b> <?php echo $acctDetails->getAccountNumber(); ?>
                <?php echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" . __('Account Balance') . ":</b> " . format_amount($acctDetails->getClearBalance(), $currencyId, 1); ?>
                <?php echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" . __('Unclear Amount') . ":</b> " . format_amount($acctDetails->getUnclearAmount(), $currencyId, 1); ?>
            </td>
        </tr>

        <?php if ($pager->getNbResults() > 0) {
 ?><?php
                    foreach ($statementObj->getFirst()->getEpMasterLedger() as $res) {
                        //    echo $res->getAmount();
                        echo "<tr><td align='center'>";
                        if ($res->getEntryType() == "credit") {
                            echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" . __('Total Credits') . ":</b> " . format_amount($res->getAmount(), $currencyId, 1);
                        }
                        else
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" . __('Total Debits') . ": </b>" . format_amount($res->getAmount(), $currencyId, 1);
                    }
                    echo "</td></tr>";
                }
        ?>
            </table>
        </div>

        <div class="wrapTable">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                <thead>
                    <tr class="horizontal">
                        <th width = "2%">S.No.</th>
                        <th><?php echo __('Narration') ?></th>
                        <th><?php echo __('Transaction Date') ?></th>
                        <th><?php echo __('Amount') ?></th>
                    <th><?php echo __('Transaction Type') ?></th>
                </tr>
            </thead>
            <tbody>
            <?php
                if (($pager->getNbResults()) > 0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $i = max(($page - 1), 0) * $limit;
                    foreach ($pager->getResults() as $result):
                        $i++;
                        $transaction_date = explode(" ", $result->getTransactionDate());
                        $amount = format_amount($result->getAmount(), $currencyId, 1);
            ?>

                    <tr class="alternateBgColour">
                        <td align="center"><?php echo $i ?></td>
                <?php
      
                        if ($result->getEwalletRechargeAcctDetailsRecord()->count()) {
                            
                ?>
                            <td align="center"><?php  echo html_entity_decode ($result->getDescription ()); ?></td>
<?php } else { ?>
                            <td align="center"><?php echo html_entity_decode($result->getDescription() ); ?></td>
            <?php } ?>
                            <td align="center"><?php echo $transaction_date['0']; ?></td>
                            <td align="right"><?php echo $amount; ?></td>
                            <td align="center"><?php echo ucwords($result->getEntryType()); ?></td>
                        </tr>
                        <?php endforeach; ?>

            <tr><td colspan="5">
                    <div class="paging pagingFoot">
<?php
                        if (isset($ewallet_account_id))
                            echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . '?from_account=' . $ewallet_account_id), 'search_results');
                        else
                            echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName()), 'search_results');
?>


                    </div></td></tr>



<?php }
                    else { ?><table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
<?php } ?>

        </tbody>

    </table>
</div>
