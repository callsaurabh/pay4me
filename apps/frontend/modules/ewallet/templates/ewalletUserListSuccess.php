<?php //use_helper('DateForm'); ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<div class="wrapForm2">
    <form action="<?php echo url_for('ewallet/ewalletUserList')?>" method="post" name="request_report_form" id="request_report_form">
        <?php echo ePortal_legend(__('eWallet User List')); ?>
        <?php echo formRowComplete(__($form['status_option']->renderLabel()),"<input type='radio' id = 'status_option' checked='checked' name=selectBy value=user_type onclick='unSetMendetory()'>".'&nbsp;&nbsp'.$form['status_option']->render(),'','status_option','err_status_option','status_option_row'); ?>
        <?php echo formRowComplete(__($form['uname']->renderLabel()),"<input type='radio' id = 'uname'  name=selectBy value=user_name onclick='setMendetory(this)'>".'&nbsp;&nbsp'.$form['uname']->render(), '', 'username', 'err_username', 'username_row',$form['uname']->renderError(),'error_listing_newwallet'); ?>
        <?php echo formRowComplete(__($form['account_number']->renderLabel()),"<input type='radio' id = 'account_number'  name=selectBy value=account_number onclick='setMendetory(this)'>".'&nbsp;&nbsp'.$form['account_number']->render(), '', 'account_number', 'err_account_number', 'username_row',$form['account_number']->renderError(),'error_listing_newwallet'); ?>
        <?php  if ($form->isCSRFProtected()) : ?>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php endif; ?>




        <div class="divBlock">
            <center id="multiFormNav">
                <input type="submit" value=<?php echo __("Search"); ?> class="formSubmit" />
                <input type="hidden" name="isfrom" value="yes">
            </center>
        </div>


        <div class="clear"></div>
    </form>
</div>
<br class="pixbr" />
<div class="load_overflow">
    <span id="search_results"></span>
</div>
<script>
    function setMendetory(element){

        $('#ewalletUserList_status_option').attr("disabled", true);
        $('#ewalletUserList_account_number').attr("disabled", true);
        $('#ewalletUserList_uname').attr("disabled", true);
        $('#sp_uname').html('');
        $('#sp_account_number').html('');
        $('#ewalletUserList_uname').val('');
        $('#ewalletUserList_account_number').val('');
        $('#ewalletUserList_'+element.id).removeAttr("disabled");

        $('#sp_'+element.id).html('*');


    }
    function unSetMendetory(){
        $('#ewalletUserList_status_option').removeAttr("disabled");
        $('#ewalletUserList_uname').val('');
        $('#ewalletUserList_uname').attr("disabled", true);
        $('#ewalletUserList_account_number').val('');
        $('#ewalletUserList_account_number').attr("disabled", true);


        $('#sp_uname').html('');
        $('#sp_account_number').html('');
        $("#err_username").html("");
        $("#err_account_number").html("");
    }


    $(document).ready(function(){
        var selectVal = "";
        <?php
        if($selected){
    ?>
         selectVal = "<?php echo $selected;?>";
        
<?php } ?>
       
        if($('#status_option').attr('checked')==true && $('#uname').attr('checked')==false){
            $('#ewalletUserList_status_option').removeAttr("disabled");
            $('#ewalletUserList_uname').attr("disabled", true);
            $('#ewalletUserList_account_number').attr("disabled", true);
        }
        if($('#ewalletUserList_uname').val() || selectVal=='uname'){
            $('#ewalletUserList_status_option').attr("disabled", true);
            $('#ewalletUserList_account_number').attr("disabled", true);
            $('#ewalletUserList_uname').removeAttr("disabled");
            $('#user_type').attr('checked',false);
            $('#uname').attr('checked',true);
            $('#sp_'+selectVal).html('*');

        }

        if($('#ewalletUserList_account_number').val()|| selectVal=='account_number'){
            $('#ewalletUserList_status_option').attr("disabled", true);
            $('#ewalletUserList_uname').attr("disabled", true);
            $('#ewalletUserList_account_number').removeAttr("disabled");
            $('#user_type').attr('checked',false);
            $('#uname').attr('checked',false);
            $('#account_number').attr('checked',true);
            $('#sp_'+selectVal).html('*');

        }

<?php
if("valid" == $formValid ){
    ?>
            displayReport();
    <?php } ?>

            function displayReport(){

                $.post('ewalletUserListing',$("#request_report_form").serialize(), function(data){
                    if(data == "logout"){
                        location.reload();
                    }else{
                        $("#search_results").html(data);
                    }
                });



            }
        });



</script>

