<?php use_stylesheet('master.css');
echo include_stylesheets(); ?>
<?php use_helper('Pagination');  ?>

<div id="loadArea_new">

  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <div class="popupWrapForm2" style="background-color:#EFEFEF;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" style="background-color:#EFEFEF;" >

      <tbody>
        <?php
        $pending = $sf_request->getParameter('pending');
        if(($pager->getNbResults())>0) {
          $limit = 5;
          $i = max(($page-1),0)*$limit ;
          foreach ($pager->getResults() as $result):
          $i++;
          //        $transaction_date = $result->getTranstime();
          $amount = format_amount($result->getAmount(), 1, 1);
          ?>
        <tr>
          <td>
            <?php echo ePortal_legend("Record - $i");
//            if($result->getTransactionDate())
            if(strtolower($result->getStatus()) != 'pending')
                echo formRowFormatRaw('Transaction Date',$result->getUpdatedAt()) ;
            if($result->getStatus()=='success' && $result->getValidationNumber())
            echo formRowFormatRaw('Validation Number',$result->getValidationNumber()) ;
            if($result->getType()=='payment') {
                  echo formRowFormatRaw('Transaction Number',$result->getAppId()) ;
            }

            echo formRowFormatRaw('Transaction Identification Number', $result->getOrderId()) ;
            if($result->getPan())
            echo formRowFormatRaw('Card Number',$result->getPan()) ;
            echo formRowFormatRaw('Amount',$amount) ;
            echo formRowFormatRaw('Status',$result->getStatus()) ;
            echo formRowFormatRaw('Details', ($pending=='pending')? 'This transaction is pending' : html_entity_decode($result->getResponseTxt())) ;
            if($result->getApprovalCode())
            echo formRowFormatRaw('Approval Code',$result->getApprovalCode()) ;
            ?>           
            <?php if($isSuperadmin && strtolower($result->getStatus())=='pending' && $paymentmode==2 && ($type=='payment' || $type=='recharge') && !$successtrans  ): ?>
            <dl><div class="dsTitle4Fields"><label>Query Interswitch</label></div>
                <div class="dsInfo4Fields">
                <?php echo link_to('Query Interswitch','interswitch_configuration/interswitchQueryStatus?orderId='.$result->getOrderId().'&appId='.$result->getAppId(), array('title'=>'Query Interswitch')) ?></div>
            </dl>
            <?php endif; ?>

            <?php if($isSuperadmin && strtolower($result->getStatus())=='pending' && $paymentmode==16 && ($type=='payment' || $type=='recharge') && !$successtrans  ): ?>
            <dl><div class="dsTitle4Fields"><label>Query Internet Bank</label></div>
                <div class="dsInfo4Fields">
                <?php echo link_to('Query Internet Bank','nibss/queryToInternetbank?orderId='.$result->getOrderId().'&paymentmode='.$paymentmode.'&type='.$type, array('title'=>'Query Internet Bank')) ?></div>
            </dl>
            <?php endif; ?>
          </td>
        </tr>
        <?php endforeach; ?>

        <?php } else { ?>
        <tr><td  align='center' class='error'>No Record Found</td></tr>
        <?php } ?>
      </tbody>
    </table>
  </div>

<?php if(($pager->getNbResults())>2) {?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >

  <tr class="alternateBgColour">
    <th width="100%" >
      <div class="paging pagingFoot floatRight" style="background-color:#EFEFEF">
      <?php if($paymentmode!=""): ?>
        <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())."?transId=".$sf_request->getParameter('transId')."&type=".$sf_request->getParameter('type')."&paymentmode=".$sf_request->getParameter('paymentmode')."&pending=".$sf_request->getParameter('pending')) ?>
      <?php else: ?>
        <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())."?transId=".$sf_request->getParameter('transId')."&type=".$sf_request->getParameter('type')."&pending=".$sf_request->getParameter('pending')) ?></div>
      <?php endif; ?>
      </div>
    </th>
  </tr>
</table>
<?php }?>
</div>
<?php if($isSuperadmin && $paymentmode==2 && $query): ?>
<script type="text/javascript">   
    window.opener.updatedetails();
</script>
<?php endif; ?>