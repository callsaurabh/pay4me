Dear User,
<br /><br />
Your eWallet Financial Report has now been generated successfully.
<br /><br />
Please visit Pay4me Portal to download this report using the following steps.
<br /><br />
1. Click on the eWallet Financial Report (Reports)
<br />
2. Click on View Previously Generated Report
<br />
3. Select the report type
<br />
    Quarterly
    Weekly
    Monthly
    Custom

<br /><br />
4. Select the appropriate and corresponding details from the drop down and click on Search.
<br />
5. Click on the Download Link for the generated report.
<br /><br />

Thank you,
<br />
Regards,
<br />
Pay4me Support Team 