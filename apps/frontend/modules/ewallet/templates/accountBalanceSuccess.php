<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_helper('Form'); ?>

 <div class="wrapForm2">
        <?php echo ePortal_legend(__('Account Details')); ?>
    
<?php //echo form_tag('test/save') ?>
  <?php //echo formRowFormatRaw('Account Name',$accountName); ?>
  <?php //echo formRowFormatRaw('Account No.',$wallet_number); ?>
  <?php echo formRowFormatRaw(__('Name'),$walletUserName); ?>
  <?php echo formRowFormatRaw(__('Email'),$emailId); ?>
  <?php echo formRowFormatRaw(__('Mobile No.'),$mobileNo); ?>
  <?php //echo formRowFormatRaw('Account Balance',format_amount($walletBalance,1,1)); ?>
  <?php //echo formRowFormatRaw('Unclear Amount',format_amount($walletUnclearAmount,1,1)); ?>
   <div class="clear"></div>
 </div>
  <br>
  <div class="wrapForm2">
  <table width="100%"  cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
          <tr class="horizontal">
               <th><?php echo __('Account Number')?></th>
               <th><?php echo __('Balance')?></th>
               <th><?php echo __('Statement')?></th>
          </tr>
        </thead>
        <tbody>
       <?php  foreach($arrAccountCollection as $result) {
           $accountObj = $result->getEpMasterAccount();
           ?>
          <tr class="alternateBgColour">
            <td align="left"><?php echo $accountObj->getAccountNumber(); ?></td>
            <td align="left"><?php  echo format_amount($accountObj->getClearBalance(),$result->getCurrencyId(),1); ?></td>
            <td align="left">
                <?php echo link_to(__('View'), url_for('ewallet/search?id='.base64_encode($accountObj->getId())), array('title' => __('View'))) ?> |
                <?php echo link_to(__('Generate eWallet Certificate'), url_for('ewallet/generateCertificate?id='.base64_encode($accountObj->getId())), array('title' => __('Generate eWallet Certificate'))) ?>
            </td>
          </tr>
      <?php
        }
       ?>
       </tbody>
  </table>
  </div>

</div>

