<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<div class="wrapForm2">

    <?php echo form_tag('ewallet/ewalletUserSendMail', array('name' => 'pfm_ewalletUser_email_form', 'id' => 'pfm_ewalletUser_email_form')) ?>


    <dl id='username_row'>
        <div class="dsTitle4Fields"><?php echo $form['subject']->renderLabel(); ?></div>
        <div class="dsInfo4Fields" align="left"> <?php echo $form['subject']->render(); ?> <?php echo $form['subject']->renderError(); ?><br><br><div id="err_subject" class="cRed"></div>
        </div>
    </dl>

    <dl id='username_row'>
        <div class="dsTitle4Fields"><?php echo $form['message']->renderLabel(); ?></div>
        <div class="dsInfo4Fields" align="left"> <?php echo $form['message']->render(); ?><div id="err_message" class="cRed"></div><?php echo $form['message']->renderError(); ?>
        </div>
    </dl>
    <?php echo $form['UserId']; ?>
    <?php echo $form['_csrf_token']; ?>
    <div class="divBlock">
        <center id="multiFormNav">
            <input type="button" onclick="validateEmailForm('1')" value="Submit" class="formSubmit">
            &nbsp;&nbsp;
            <input type="button" onclick="location.href='<?php echo url_for('ewallet/ewalletUserMailingList') ?>'" value="Cancel" class="formCancel">
        </center>
    </div>

</form>

</div>

<script>
    function validateEmailForm(submit_form)
    {
        var err  = 0;
        if($('#subject').val() == "")
        {
            $('#err_subject').html("Please enter Subject");
            err = err+1;
        }
        else
        {
            $('#err_subject').html("");
        }
        var location1 = FCKeditorAPI.GetInstance('message');    //location_info is name of text area.
        var contents = location1.GetXHTML(true);
        if(!contents)
        {
            $('#err_message').html("Please enter Body");
            err = err+1;
        }
        else
        {
            $('#err_message').html("");
        }
    
        if(err == 0)
        {
            if(submit_form == 1)
            {
                $('#pfm_ewalletUser_email_form').submit();
            }
        }
    }
</script>


