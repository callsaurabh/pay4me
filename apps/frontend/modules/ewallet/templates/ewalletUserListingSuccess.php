
<?php  //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<div align="center" id="loader" class="transparent_class"><div style="padding-top:265px;"><?php echo image_tag('loader.gif');?></div></div>
<div id="flash_notice" class="error_list" style="display:none">
</div>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
           <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
          <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
        </th>
      </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable" style="width:857px;overflow:auto">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <thead>
      <tr class="horizontal">
        <th width="2%" align="center">S.No.</th>
        <th align="center"><?php echo __("UserName"); ?></th>
        <th align="center"><?php echo __("Name"); ?></th>
        <th align="center">Email</th>
        <th align="center">Open Id User</th>
        <th align="center">Ewallet Account</th>
        <th align="center">Action(s)</th>
      </tr>
    </thead>
    <tbody>
      <?php

      $username = $uname == '' ? "BLANK" : $uname;
      $status_option = $status_option == '' ? "BLANK" : $status_option;
      $selectBy = $selectBy == '' ? "BLANK" : $selectBy;
      $accountNumber = $accountNumber == '' ? "BLANK" : $accountNumber;
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;

        if($result->getUserDetail()->getFirst()->getEpMasterAccount()){
         $keyval = $result->getUserDetail()->getFirst()->getEpMasterAccount();
        }else{
         $keyval = '';
        }
      ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <td align="center">
        <?php
        if( isset($keyval['account_number']) &&($keyval['account_number']!=""))
        {
            echo link_to($result->getUsername(), url_for('report/ewalletTransDetail?userId='.$result->getId()), array('popup' => array('popupWindow', 'width=890,height=500,left=150,top=0,scrollbars=yes')));
        }
        else
        { echo $result->getUsername();}
        ?>
        </td>
        <td align="center"><?php echo $result->getUserDetail()->getFirst()->getName(); ?></td>
        <td align="center"><?php echo $result->getUserDetail()->getFirst()->getEmail(); ?></td>
       <td align="center"><?php
       $openIdUser=$result['openiduser'];
       if($openIdUser!=""){
           echo $openIdUser;
       }else{
           echo "No";
       }
       ?></td>
       <td align="center"><?php
       $ewalletAccount=$result['EwalletAccount'];
       if($ewalletAccount!=""){
           echo $ewalletAccount;
       }else{
           if($result->getUserDetail()->getFirst()->getEpMasterAccount())
                echo "Yes-Freezed";
           else
                echo "No";
       }
       ?></td>

        <td align="center">

        <?php if($result->getUserDetail()->getFirst()->getFailedAttempt() == $maxToBlock){
          ?>
          <a class="activeInfo" href="#" title="Unblock User" id='act' onclick="activateDeactivate(<?php echo $result->getUserDetail()->getFirst()->getId(); ?>,'activate',<?php echo $result->getId(); ?>);"></a>

          <?php
          //echo link_to(' ', 'report/setEwalletBlockUnblock?status=deactivate&id='.$result->getId(), array('method' => 'get',  'class' => 'deactiveInfo', 'title' => 'Deactivate')) ;
        }else{
            ?>
            <a class="deactiveInfo"  title="Block User" id='dact' onclick="activateDeactivate(<?php echo $result->getUserDetail()->getFirst()->getId(); ?>,'deactivate',<?php echo $result->getId(); ?>);"> </a>
           <?php
          //echo link_to(' ', 'report/setEwalletBlockUnblock?status=activate&id='.$result->getId(), array('method' => 'get',  'class' => 'activeInfo', 'title' => 'Activate')) ;
        }

        if(count($result->getEwalletPin())) {
        if($result->getEwalletPin()->getFirst()->getStatus()=='blocked' && Settings::isEwalletPinActive())
        {
            ?>
          |  <a class="activeInfo" href="#" title="Unblock/Reset User's Pin" id='unBlockPin' onclick="activateDeactivate(<?php echo $result->getUserDetail()->getFirst()->getId(); ?>,'unBlockPin',<?php echo $result->getId(); ?>);"></a>

          <?php
        }
        else
        { ?>
             | <a class="approveInfo" href="#" title="Reset User's Pin" id='resetPin' onclick="activateDeactivate(<?php echo $result->getUserDetail()->getFirst()->getId(); ?>,'unBlockPin',<?php echo $result->getId(); ?>);"></a>

      <?php  }
        }
        if($userGroup == 'portal_admin') {
        echo link_to(' ', 'ewallet/edit?id='.$result->getId(), array('method' => 'post', 'class' => 'editInfo', 'title' => 'Edit'));
        }
        ?>

        </td>

      </tr>
        <?php endforeach; ?>
      <tr>
        <td colspan="7">
          <div class="paging pagingFoot">
           <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?username='.$username.'&status_option='.$status_option.'&selectBy='.$selectBy.'&account_number='.$accountNumber), 'search_results') ?>
          </div>
        </td>
      </tr>
      <?php }
      else { ?>
          <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr><td  align='center' class='error' colspan="7">No Record Found</td></tr>
          </table>
      <?php } ?>
    </tbody>
  </table>
</div>


<script>
    function activateDeactivate(userId,status,sfUid){

        var  showStatus;
        if(status == 'activate')
          showStatus = 'unblocked';

        if(status == 'deactivate')
          showStatus = 'blocked';

        if(status == 'unBlockPin')
          showStatus = 'pin reset';


        var answer = confirm("Do you want to "+showStatus+" this user?")
        if (answer){
           var url = "<?php echo url_for('ewallet/ewalletUserListing'); ?>";
           var page = "<?php echo $page; ?>";
           var status_option = "<?php echo $status_option; ?>";
           var selectBy = "<?php echo $selectBy; ?>";
           var accountNumber = "<?php echo $accountNumber; ?>";
           var username = "<?php echo $username; ?>";
           var stringname = "";
           var stringVal = '';
           if(status_option!='BLANK'&&status_option!='' ){
                stringname = "status_option";
                stringVal = status_option
           }else if(accountNumber!='BLANK' && status_option!='' ){
                 stringname = "account_number";
                 stringVal = accountNumber
           }else if(username!='BLANK' && username!='' ){
                stringname = "uname";
                stringVal = username
           }
            $('#loader').show();
           $.post(url, { id: userId, status: status, page: page,sfUid: sfUid,selectBy: selectBy,name:stringname,nameValue:stringVal},
         function(data){
               if(data=='logout'){
                    location.reload();
              }
               else {
                $("#search_results").html(data);
                $('#loader').hide();
                $("#flash_notice").show();
                $("#flash_notice").html('<span>User '+showStatus+' successfully</span>');

               }

               //$("#listing_div").html(data);

           });


        }

    }

   function refresh(){
     window.location.reload( true );
   }

</script>
