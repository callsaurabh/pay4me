<?php   
echo ePortal_pagehead(" ");
//use_helper('Form')
?>
<div class="wrapForm2">
    <?php echo form_tag('report/indEwalletAccountsSearch',array('name'=>'ewallet_account_form','method'=>'post','id'=>'newewallet_account_form', 'onsubmit' => 'return newewalletAccounts(this);')) ;
    echo ePortal_legend(__('New eWallet User Section'));
    echo $form;
    ?>
    <div class="divBlock">
        <center id="multiFormNav">
        <?php echo button_to(__('Search'),'',array('class' => 'formSubmit','onClick'=>'newewalletAccounts()')); ?>
        </center>
    </div>
</div>
</form>
</div>
<div id="search_results"></div>

<script>
    function newewalletAccounts(){
        err =0;
        if((document.getElementById('from_date').value != '') && (document.getElementById('to_date').value != '')){
            var start_date = document.getElementById('from_date').value;
            var end_date = document.getElementById('to_date').value;
            start_date = new Date(start_date.split('-')[2],start_date.split('-')[1]-1,start_date.split('-')[0]);
            end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

            if(start_date.getTime()>end_date.getTime()) {
                alert("<From date> cannot be greater than <To date>");
                document.getElementById('from_date').focus();
                return false;
            }
        }
        /* ================= [ END ] Date Field Validations ====================  */
        if(err == 0) {

            $.post('newEwalletUserSearch',$("#newewallet_account_form").serialize(), function(data){
                if(data=='logout'){
                    location.reload();
                }
                else {
                    $("#search_results").html(data);
                }
            });

        }
    }
</script>