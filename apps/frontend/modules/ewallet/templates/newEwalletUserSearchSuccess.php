<?php  
    echo ePortal_pagehead(" ",array('class'=>'_form')); 
    use_helper('Pagination');  
    echo ePortal_listinghead(__('New eWallet User Details')); 
?>
<div align="center" id="loader" class="transparent_class"><div style="padding-top:265px;"><?php echo image_tag('loader.gif');?></div></div>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
              <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
              <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>
<div id="search_results">
    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
           <?php
            $from_date = $sf_request->getParameter('from_date') == '' ? "BLANK" : $sf_request->getParameter('from_date');
            $to_date = $sf_request->getParameter('to_date') == '' ? "BLANK" : $sf_request->getParameter('to_date');
            if(($pager->getNbResults())>0) {
                $limit = sfConfig::get('app_records_per_page');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
                ?>
            <thead>
                <tr class="horizontal">
                    <th width = "2%">S.No.
                        <input type="hidden" name="fromDate" id="fromDate" value="<?php echo $from_date;?>">
                        <input type="hidden" name="toDate"   id="toDate" value="<?php echo $to_date;?>">
                    </th>
                    <th><?php echo __("Username"); ?></th>
                    <th width="17%">Image</th>
                    <th>Documents</th>
                    <th><?php echo __("Last Updated Date"); ?></th>
                    <th>Actions</th>
                </tr>
            </thead>
                <?php
                foreach ($pager->getResults() as $result):
                $i++;
                $userFolder = sfConfig::get('sf_upload_dir')."/".$result->getUid();
                if(is_dir($userFolder)){
                $handle = opendir($userFolder);
                                            /* This is the correct way to loop over the directory. */
               $user_file = array();
                while (false !== ($file = readdir($handle))) {
                    $user_file[]= $file;
                }
                closedir($handle);
                }
                ?>
            <tbody>
                <tr class="alternateBgColour">
                    <td align="center"><?php echo $i ?></td>
                    <td align="center"><?php echo $result->getUname();   ?></td>
                    <td align="center" text-align: center>
                        <?php
                        $img = $userFolder."/img.jpeg";
                        if (file_exists($img))
                        {
                            echo image_tag('/uploads/'.$result->getUid().'/img.jpeg',array('size' => '100x100'));?>&nbsp;
                            <?php  echo "<a class='downloadInfo' href='".url_for('ewallet/downloadfile?fileName=img.jpeg&folder='.$result->getUid())."' title='download'  alt='download'  ></a>";
                        }else{
                          echo "----";
                        }
                        ?>

                    </td>
                    <td align="left">
                        <?php
                        $fileCount=1;
                        if(!empty($user_file)){
                            foreach($user_file as $file)
                            {
                                $fullpath = $userFolder."/".$file;
                                if("img.jpeg" != $file && $file != "." && $file != "..")
                                {
                                    echo $fileCount.". ".$file."     "."<a class='downloadInfo' href='".url_for('ewallet/downloadfile?fileName='.$file.'&folder='.$result->getUid())."' title='download'  alt='download'  ></a>";
                                    echo "<br>";
                                    ++$fileCount;
                                }
                            }
                        }else{
                           echo "----"; 
                        }
                        ?>
                    </td>
                    <td><?php echo $result->getUpdatedDate();   ?></td>
                    <td align="left">
                    <?php
                    if($result->getKycStatus()==2)
                    {
                        ?>
                        <a class="approveInfo" href="#" onclick="changeStatus(<?php echo $result->getUid();?>,'approve');" title="approve" alt="approve"></a>
                        &nbsp;
                        <a class="delInfo" href="#" onclick="changeStatus(<?php echo $result->getUid();?>,'denial');" title="disapprove" alt="delete"></a>
                        <?php
                    }else
                    {
                        echo ($result->getKycStatus()==1? 'Approved':'Rejected');
                    }
                    ?>
                    </td>
                </tr>
                    <?php endforeach;    ?>
            </tbody>
                <?php if(($pager->getNbResults())>$limit) {
                ?>
                <tr>
                <td colspan="6">
                    <div class="paging pagingFoot">
                        <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date.'&request_type='.$request_type), 'search_results') ?>

                    </div>
                </td>
            </tr>
            <?php
        }
    }else {
        ?>
            <tr><td colspan="10"  align='center' class='error' >No Record Found</td></tr>
            <?php } ?>
        </table>
    </div>
</div>
<script>
    function changeStatus(userid,state){

        var denial;
        if("denial" == state)
            {
                 jPrompt('Please enter reason of disapproval (Mandatory):', '', 'Disapprove Reason', function(denial) {
                        if( !denial ){
                            jAlert('User can not be disapproved without the reason!');
                        return;
                        }
                       else {  sendReq(userid,state,denial); }
                    }

                );

            }
       else{
           sendReq(userid,state,'');
        }

    }
 function sendReq(userid,state,denial){
            $('#loader').show();
            var activate_url = "<?php echo url_for('ewallet/setUserStatus');?>";
             $.post(activate_url,{user_id:userid,status:state,reason:denial,from_date:$("#fromDate").val(),to_date:$("#toDate").val()}, function(data){
            if(data){
                newewalletAccounts();
                $('#loader').hide();
        }});

    }
</script>