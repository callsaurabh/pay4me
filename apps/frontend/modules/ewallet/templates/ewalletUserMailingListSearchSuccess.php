<?php
    use_helper('Pagination');
    use_helper('Object')
?>
<?php
if ($sf_user->hasFlash('notice')){
  echo "<div id='flash_notice' class='error_list' ><span>".sfContext::getInstance()->getUser()->getFlash('notice')."</span></div>";
}
?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
          <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
      </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable" style="width:857px;overflow:auto">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <thead>
      <tr class="horizontal">
        <th width="2%" align="center">S.No.</th>
        <th align="center">UserName</th>
        <th align="center">Name</th>
        <th align="center">Email</th>
        <th align="center"><input name="select_deselect" type="checkbox"> Select / Deselect</th>
      </tr>
    </thead>
    <tbody>
      <?php

      $username = $sf_request->getParameter('username') == '' ? "BLANK" : $sf_request->getParameter('username');
      $status_option = $sf_request->getParameter('status_option') == '' ? "BLANK" : $sf_request->getParameter('status_option');
      $selectBy = $sf_request->getParameter('selectBy') == '' ? "BLANK" : $sf_request->getParameter('selectBy');
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;

        if($result->getUserDetail()->getFirst()->getEpMasterAccount()){
         $keyval = $result->getUserDetail()->getFirst()->getEpMasterAccount();
        }else{
         $keyval = '';
        }
      ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <td align="center"><?php echo $result->getUsername(); ?></td>
        <td align="center"><?php echo $result->getUserDetail()->getFirst()->getName(); ?></td>
        <td align="center"><?php echo $result->getUserDetail()->getFirst()->getEmail(); ?></td>
        <td align="center">
            <input type="checkbox" class="check_me" style="width: 250px;" value="<?php echo $result->getId(); ?>" id="ewalletUserId_<?php echo $result->getId(); ?>" name="ewalletUserId[]">
       </td>

      </tr>
        <?php endforeach; ?>
      <tr>
        <td colspan="7">
          <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?username='.$username.'&status_option='.$status_option.'&selectBy='.$selectBy), 'listing_div') ?>
          </div>
        </td>
      </tr>
      <tr>
          <td  align='center' class='error' colspan="7">
             <input type="submit" class="formSubmit" value="Send Email" name="commit">
          </td>
      </tr>
      <?php }
      else { ?>
        <tr><td  align='center' class='error' colspan="7">No Record Found</td></tr>
      <?php } ?>
        
    </tbody>
  </table>

</div>

<script>
    
    $(document).ready(function(){
      $("input[name=select_deselect]").click(function(){
         var checked_status = this.checked;
         $("input[class=check_me]").each(function(){
             this.checked = checked_status;
         });
      });
    });

    function validateCheckbox(element) {
        var okToSubmit = false;

        //A. Make sure that the user selects at least one item before they Request an Action
            if( $("input:checkbox:checked").length > 0 ) okToSubmit = true;
        //C. Either Submit or give an Error Message
        var f=document.pfm_ewalletUser_form,x=f.length,temp=[];
        if(okToSubmit) {
            return okToSubmit;
        } else {
            var f=document.pfm_ewalletUser_form;
            if(f.userId.value == '' ){
                alert( "Please select at least one user" );
                return okToSubmit;
            }
        }

}
function Search_Array(ArrayObj, SearchFor){
  var Found = false;
  for (var i = 0; i < ArrayObj.length; i++){
    if (ArrayObj[i] == SearchFor){
      return true;
      var Found = true;
      break;
    }
    else if ((i == (ArrayObj.length - 1)) && (!Found)){
      if (ArrayObj[i] != SearchFor){
        return false;
      }
    }
  }
}
function removeByElement(arrayName,arrayElement)
{
for(var i=0; i<arrayName.length;i++ )
 {
    if(arrayName[i]==arrayElement)
        arrayName.splice(i,1);
  }
}
 $(document).ready(function(){

        var d=document.pfm_ewalletUser_form.elements,w=d.length;
        while(w--)
        {
            if(d[w].type=="checkbox"){
            d[w].onclick=function()
                {
                    var f=document.pfm_ewalletUser_form,x=f.length,temp=[];
                    if(f.userId.value != ''){
                        myString = f.userId.value;
                        myArray = myString.split(",");
                        for(i = 0; i < myArray.length; i ++)
                        {  
                            temp.push(myArray[i]);
                        }
                    }
                    
                    while(x--){
                        if(f[x].type=="checkbox"  && f[x].checked){
                           if(f[x].value != 'on'){
                               temp.push(f[x].value);
                           }                           
                        }
                    }

                    /* Remove Duplicates From Array */
                    var i, arrayLength=temp.length,out=[],obj=[];
                    for (i=0; i < arrayLength; i++) {
                     obj[temp[i]]=0;
                    }
                    for (i in obj) {
                    out.push(i);
                    }
                    temp = out;

                    if(f.userId.value != ''){
                        for (i = 0; i < f.elements.length; i++){
                            if (f.elements[i].getAttribute('type') == 'checkbox' && f.elements[i].value != 'on')
                            {
                                 if(f.elements[i].checked == false){
                                     for (var j = 0; j < temp.length; j++){
                                        if (Search_Array(temp, f.elements[i].value)){
                                            removeByElement(temp, f.elements[i].value);
                                        }
                                     }
                                 }
                           }
                       }
                    }
                    
                    f.userId.value=temp.join(",");
                    ajax_call('strength', '<?php echo url_for('ewallet/showEwalletMailIds',true).'?byPass='?>'+f.userId.value, 'pfm_ewalletUser_form');
                }
            }
        }

    });
</script>
