<?php use_helper('ePortal'); ?>
<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<?php //use_helper('Form')    ?>
<?php echo form_tag('ewallet/ewalletFinancialReportDisplay', array('name' => 'ewallet_financial_report_form', 'id' => 'pfm_report_form')) ?>
<div class="wrapForm2">
    <?php
    echo ePortal_legend(__('eWallet Financial Report'));
    ?>

    <div id="type_div">
        <?php
        echo formRowFormatRaw($form['type']->renderLabel(), $form['type']->render());
        ?>
    </div>
    <div id="frequency_div">
        <?php
        echo formRowFormatRaw($form['frequency']->renderLabel(), $form['frequency']->render());
        ?>
    </div>
    <dl>
        <div class="dsTitle4Fields">
            <label>
                <label for="year">Year</label>
            </label>
        </div>
        <div class="dsInfo4Fields">
            <select id="quarter_list" name="year" >
                <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                <option value="<?php echo date("Y", strtotime("last year")); ?>"><?php echo date("Y", strtotime("last year")); ?></option>
            </select>
        </div>
    </dl>


    <div id="quarterly_div" style="display: none;">
        <?php
        echo formRowFormatRaw($form['quarter']->renderLabel(), $form['quarter']->render());
        ?>
    </div>
    <div id="monthly_div" style="display: none;">
        <?php
        echo formRowFormatRaw($form['month']->renderLabel(), $form['month']->render());
        ?>
    </div>
    <div class="divBlock">
        <center id="multiFormNav">
            <input type="button" name="submit" value="Show Report" class="formSubmit"  onclick=" fetchRecords();">
            &nbsp;<?php echo button_to('Cancel', '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('ewallet/ewalletFinancialReport') . '\'')); ?>
        </center>
    </div>
</div>
</form>
<div id="loaderImg"></div>
<div id ="flash_error" style = "display:none" class="error_list"></div>
<div id="search_results">    
    <?php echo form_tag('report/ewalletRechargeReportDisplay', array('name' => 'report_listing_form', 'id' => 'report_listing_form')) ?>
    <div> 
        <?php
        if (isset($dataById)) {
            ?>
            <h2> </h2>
            <div class="clear"></div>
            <div class=" listHead"> Reports</div>

            <table width="100%" border="0" cellpadding="0" cellspacing="0"
                   class="dataTable">
                <thead>
                    <tr align="center" class="horizontal">
                        <?php
                        if ($type == "Generated") {
                            $pfmHelperObj = new pfmHelper();
                            $this->userGroup = $pfmHelperObj->getUserGroup();
                            if ($this->userGroup == "portal_admin" || $frequency == "CUSTOM") {
                                ?>
                                <th width="3%">
                                    <input type="checkbox"  name="checkAllAuto" id="checkAllAuto"  />
                                </th>
                                <?php
                            }
                        }
                        if ($type == "Pending") {
                            ?>                    
                            <th width="3%">
                                <input type="checkbox"  name="checkAllAuto" id="checkAllAuto"  />
                            </th>
                        <?php } ?>
                        <!--<th width="10%">Bank Name</th>-->
                        <!--<th width="20%">Parameters</th>-->
                        <th width="10%">Status</th>
                        <th width="20%">From Date</th>
                        <th width="20%">To Date</th>
                        <?php
                        if ($type != "Pending") {
                            ?>
                            <th width="17%">Action</th>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php foreach ($dataById as $key => $value) { ?>
                        <tr>
                            <?php
                            if ($type == "Generated") {
                                $pfmHelperObj = new pfmHelper();
                                $this->userGroup = $pfmHelperObj->getUserGroup();
                                if ($this->userGroup == "portal_admin" || $frequency == "CUSTOM") {
                                    ?>
                                    <td>             
                                        <input type="checkbox" name="chk[]"  value="<?php echo $value['id']; ?>" />
                                    </td>
                                    <?php
                                }
                            }
                            ?>
                            <?php
                            if ($type == "Pending") {
                                ?>
                                <td>
                                    <input type="checkbox" name="chk[]"  value="<?php echo $value['ep_job_id']; ?>" />
                                </td>
                                <?php
                            }
                            ?>
<!--                            <td>
                                <?php
//                                if (isset($value['Bank'][0]['bank_name'])) {
//                                    echo $value['Bank'][0]['bank_name'];
//                                } else {
//                                    echo "All Banks";
//                                }
                                ?>
                            </td>-->
<!-- Following code will work if client ask for parameters value in report-->
                            <!--<td>-->
                                        <?php
//                                        if (isset($value['parameters'])) {
//                                            $filters = json_decode($value["parameters"], true);
//                                            if (isset($filters["user_name"]) && $filters["user_name"] != NULL) {
//                                                echo "Search By Bank Teller (Username) = " . $filters["user_name"] . "</br>";
//                                            }
//                                            if (isset($filters["wallet_no"]) && $filters["wallet_no"] != NULL) {
//                                                echo "eWallet Account No = " . $filters["wallet_no"] . "</br>";
//                                            }
//                                            if (isset($filters["banks"]) && $filters["banks"] != NULL) {
//                                                echo "Bank = " . $filters["banks"] . "</br>";
//                                            }
//                                            if (isset($filters["country"]) && $filters["country"] != NULL) {
//                                                echo "Country = " . $filters["country"] . "</br>";
//                                            }
//                                            if (isset($filters["branch"]) && $filters["branch"] != NULL) {
//                                                echo "Branch = " . $filters["branch"] . "</br>";
//                                            }
//                                            if (isset($filters["currency"]) && $filters["currency"] != NULL) {
//                                                echo "Currency = " . $filters["currency"] . "</br>";
//                                            }
//                                            if (isset($filters["entry_type"]) && $filters["entry_type"] != NULL) {
//                                                echo "Entry Type = " . $filters["entry_type"] . "</br>";
//                                            }
//                                            if (isset($filters["payment_mode"]) && $filters["payment_mode"] != NULL) {
//                                                echo "Payment Mode = " . $filters["payment_mode"] . "</br>";
//                                            }
//                                            if (isset($filters["account_no"]) && $filters["account_no"] != NULL) {
//                                                echo "eWallet account No. = " . $filters["account_no"] . "</br>";
//                                            }
//                                        }
//                                        ?>
                            <!--</td>-->
                            <td><?php echo $type; ?></td>
                            <td><?php echo $value['from_date']; ?></td>
                            <td><?php echo $value['to_date']; ?></td>
                            <?php if ($type != "Pending") { ?>
                                <td><?php echo link_to('Download File', 'report/downloadCsv?fileName=' . $value['file_name'] . "&folder=" . $folder); ?></td>
                            <?php } ?>                    
                        </tr>
                    <?php } ?>
                </thead>
            </table>        
            <?php if (count($dataById) == 0) { ?>
                <div class="wrapTable">
                    <table class="dataTable mrgn0" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td class="error" align="center">No Record Found</td>
                        </tr>

                    </table>
                </div>
            <?php } else { ?>

                <div class="divBlock">
                    <center id="multiFormNav">
                        <?php if ($type == "Generated" || $type == "Pending") { ?>
                            <input type="button" name="buton"
                            <?php
                            if ($type == "Generated") {
                                ?>                           
                                       value="Archive" onclick="deleteRecords('Archive', true);"
                                       <?php
                                   }
                                   if ($type == "Pending") {
                                       ?>
                                       value="Cancel Request" onclick="deleteRecords('Pending', true);"
                                       <?php
                                   }
                                   ?>
                                   class="formSubmit" >
                                   <?php
                               }
                               ?>    
                    </center>  
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</form>
</div>

<input type="hidden" name="checked_ids" value="" id="checked_ids" />
<script type="text/javascript">
                function showHide(val) {
                    refreshContainor();
                    if (val == "QUARTERLY") {
                        $('#monthly_div').hide();
                        $('#quarterly_div').show();
                    }
                    if (val == "MONTHLY") {
                        $('#quarterly_div').hide();
                        $('#monthly_div').show();
                    }
                    if (val == "WEEKLY") {
                        $('#quarterly_div').hide();
                        $('#monthly_div').hide();
                        $('#monthly_div').show();
                    }
                    if (val == "CUSTOM") {
                        $('#quarterly_div').hide();
                        $('#monthly_div').hide();
                    }
                }
                function refreshContainor() {
                    $('#search_results').html("");
                }

                function fetchRecords() {
                    $('#checked_ids').val('');
                    $('#flash_error').hide();
                    if ($('#frequency').val() == '') {
                        alert('Please select Frequency.');
                        $('#frequency').focus();
                        return false;
                    }
                    $("#loaderImg").html('<center><?php echo image_tag('../images/ajax-loader.gif'); ?></center>');
                    $.ajax({
                        url: '<?php echo url_for('ewalletFinancialReportDisplay'); ?>',
                        type: 'POST',
                        data: $('#pfm_report_form').serialize(),
                        success: function(select) {
                            if (select == 'logout') {
                                location.reload();
                            }
                            $('#search_results').html(select);
                            $("#loaderImg").html(' ');
                        }
                    });
                }

                function deleteRecords(requestType, refresh) {
                    if ($('#checked_ids').val().length == 0) {
                        $('#flash_notice').hide();
                        $('#flash_error').show();
                        document.getElementById('flash_error').innerHTML = "<span> Please select atleast one row. </span>";
                        return false;
                    }
                    $('#flash_error').hide();
                    $("#loaderImg").html('<center><?php echo image_tag('../images/ajax-loader.gif'); ?></center>');
                    ids = $('#checked_ids').val();
                    if (refresh) {
                        var postData = $('form').serialize() + "&formIds=" + ids + "&request=" + requestType + "&refresh=yes";
                    } else {
                        var postData = $('form').serialize() + "&formIds=" + ids + "&request=" + requestType;
                    }
                    $('#checked_ids').val('');
                    $.ajax({
                        url: '<?php echo url_for('ewalletFinancialReportDisplay'); ?>',
                        type: 'POST',
                        data: postData,
                        success: function(select) {
                            if (select == 'logout') {
                                location.reload();
                            }
                            
                            if (refresh) {
                                var url = '<?php echo url_for("ewalletFinancialReportDisplay"); ?>';
                                $(location).attr('href', url);
                            } else {
                                $('#search_results').html(select);
                                $("#loaderImg").html(' ');
                            }
                        }
                    });
                }

                $('#checkAllAuto').bind('click', function() {
                    $("INPUT[type='checkbox']").attr('checked', $('#checkAllAuto').is(':checked'));

                    // fetch all check box values
                    var currentAllVals = [];
                    $("INPUT[type='checkbox']").each(function()
                    {
                        currentAllVals.push($(this).val());
                    });
                    //removeing on value of CheckAll checkbox
                    currentAllVals.splice(currentAllVals.indexOf('on'), 1);

                    // if they are check then appending them

                    var allVals = [];
                    if ($('#checked_ids').val().length) {
                        allVals = $('#checked_ids').val().split(',');
                    }

                    var newVals = [];
                    var i = 0;
                    if ($('#checkAllAuto').is(':checked')) {
                        //checking all current page value if they exist in hidden field
                        jQuery.grep(currentAllVals, function(el) {
                            if (jQuery.inArray(el, allVals) == -1)
                                newVals.push(el);
                            i++;
                        });
                        allVals = allVals.concat(newVals);
                    }
                    else {
                        // else removing them because might user have selected one value earlier
                        jQuery.each(currentAllVals, function(index, value) {
                            allVals.splice(allVals.indexOf(value), 1);
                        });
                    }
                    $('#checked_ids').val(allVals);
                });

                $("input[name^=chk]").bind('click', function() {
                    var state = $(this).attr('checked');
                    var allVals = [];
                    if ($('#checked_ids').val().length) {
                        allVals = $('#checked_ids').val().split(',');
                    }
                    if (state == false) {
                        $('#checkAllAuto').attr('checked', false);
                        //removing id from hiddenb field            
                        allVals.splice(allVals.indexOf($(this).val()), 1);
                        $('#checked_ids').val(allVals);

                    } else {
                        // adding id to hidden fields
                        allVals.push($(this).val());
                        $('#checked_ids').val(allVals);

                        // code to check/ uncheck -> CheckAll if all check box are selected
                        $("input:checkbox[name^=chk]").each(function() {
                            if ($(this).attr('checked')) {
                                $('#checkAllAuto').attr('checked', true);
                            } else {
                                $('#checkAllAuto').attr('checked', false);
                                return false;
                            }
                        });
                    }
                });

                $(document).ready(function() {
                $('#frequency').empty();
                $("#frequency").append('<option value="CUSTOM">Custom</option>');
                    if ($('#checked_ids').val().length) {
                        // making check box checked
                        var allVals = [];
                        allVals = $('#checked_ids').val().split(',');
                        jQuery.each(allVals, function(index, value) {
                            $('input[value="' + value + '"]').attr("checked", true);
                        });
                        // if all check box are checked then checking checkAll
                        $("input:checkbox[name^=chk]").each(function() {
                            if ($(this).attr('checked')) {
                                $('#checkAllAuto').attr('checked', true);
                            } else {
                                $('#checkAllAuto').attr('checked', false);
                                return false;
                            }
                        });
                    }
                });

</script>
