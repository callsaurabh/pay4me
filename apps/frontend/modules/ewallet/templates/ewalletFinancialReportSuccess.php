<?php   echo ePortal_pagehead(" "); ?>
 <div class="wrapForm2">
  <?php echo form_tag($sf_context->getModuleName().'/ewalletFinancialReport',array('name'=>'search','class'=>'', 'method'=>'post','id'=>'search'));
        echo ePortal_legend('eWallet Financial Report Section');
        echo $form;  ?>
    <div class="divBlock">
        <center id="multiFormNav">
          <input type="submit" value='Generate Report' class="formSubmit" >
       </center>
    </div>

</div>
<div id="search_results"></div>

 <script>
<?php if("valid" == $formValid ){ ?>
    $(document).ready(function() {
        validate_response_form();
    });
<?php } ?>
    function validate_response_form(){
        $('#search_results').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;
        if(err == 0){
            $.post('<?php echo url_for('ewallet/ewalletUserTransactionDetail');?>',$("#search").serialize(), function(data){

                if(data == "logout"){
                    location.reload();
                }else{
                     $("#search_results").html(data);
                }
            });
        }
    }
</script>