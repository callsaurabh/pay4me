<?php use_helper('Pagination');  ?>
<br class="pixbr" />
 <?php echo ePortal_legend('eWallet Financial Report');
 ?>
<div class="wrapTable"  >
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft" ><div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('ewalletFinancialReportCsv', 'csvDiv', 'progBarDiv')">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</div><div id ="progBarDiv"  style="display:none;"></div></span>
      </th>
    </tr>
  </table>
</div>
<br>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
          <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
      </tr>
    </table>
</div>
<br>

<div class="wrapTable" >
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width="2%">S.No.</th>
      <th>Account Number</th>
      <th>Account Name</th>
      <th>Total Credits</th>
      <th>Total Debits</th>
      <th>Current Balance</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $sum=0;
    $totCredit = 0;
    $totDebit = 0;
    $prev_account_number = "";
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_records_per_page');
      $i = max(($page-1),0)*$limit ;
      foreach ($pager->getResults() as $result):
        $flag=0;
        $i++;
        $current_account_number = $result->getAcctnum();//AccountNumber();
        $url = "";
        if($from_date!= "")  {
           $url .= "&from_date=".$from_date;
        }
        if($to_date!=""){
            $url .= "&to_date=".$to_date;
        }
        ?>
          <tr class="alternateBgColour">
              <td align="center"><?php echo $i ?></td>
              <td align="center"><?php echo link_to($current_account_number, url_for('admin/show?id='.$result->getId().'&account_type='.$result->getType().'&showBal=1'.$url), array('title' => 'Narration','popup' => array('popupWindow','width=870,height=500,left=150,top=0,scrollbars=yes')));
              ?>
              </td>
              <td align="center"><?php echo ucWords(strtolower($result->getAccountName())); ?></td>
              <td align="right"><?php 
                      $credit = $result->getCredit() ; echo format_amount($credit,$currency_id,1);
              ?></td>
              <td align="right"><?php $debit = $result->getdebit() ; echo format_amount($debit,$currency_id,1); ?></td>
              <td align="right"><?php echo format_amount($result->getClearBalance(),$currency_id,1);
                 $bal = $credit-$debit;
                 $sum = $sum+$bal;
                 $totCredit = $totCredit+$credit;
                 $totDebit = $totDebit+$debit; ?>
              </td>
        </tr> 
      <?php
      $prev_account_number = 12;
      endforeach; ?>
    <tr>
        <td align="left" colspan="3"><b>Total</b></td>
        <td align="right"><?php echo format_amount($totCredit,$currency_id,1); ?></td>
        <td align="right"><?php echo format_amount($totDebit,$currency_id,1); ?></td>
        <td align="right">&nbsp;</td>
    </tr>
   <tr>
        <td align="left" colspan="3"><b>Grand Total</b></td>
        <td align="right"><?php echo format_amount($grandCredit,$currency_id,1); ?></td>
        <td align="right"><?php echo format_amount($grandDebit,$currency_id,1); ?></td>
        <td align="right">&nbsp;</td>
    </tr>
    <tr><td colspan="6">
        <div class="paging pagingFoot">
           <?php echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'search_results');  ?>
        </div></td></tr>  </tbody>
     <?php }else{  ?>				
           <tr class="alternateBgColour">           			
              <td align="center" colspan="5" style="color: red;">No Result Found</td>
              <td><?php echo format_amount($clearBalance,$currency_id,1); ?></td>
           </tr>
     <?php  } ?>
</table>
</div>
<div class="notebg">
    <b class="note">Note:</b>
  <div class="note-txt">

		<table width="100%" border="1" cellspacing="0" bordercolor="#cecece" cellpadding="5" style="border-collapse:collapse; text-align:justify;">
          <tr>            
            <td width="1132">Current Balance is clear balance of eWallet user account number.</td>
          </tr>          
        </table>
     </div>
</div>

<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
    if(data=='logout'){
        $('#'+targetDivId).html('');
        location.reload();
     } 
  });
}
</script>