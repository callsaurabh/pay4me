<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('merchant/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm multiForm" id="pfm_merchant_form" name="pfm_merchant_form">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  
  <fieldset>
  <?php echo $form ?>
  <div class="XY20">
      <center>

  &nbsp; <?php  echo button_to('Cancel','',array('onClick'=>'location.href=\''.url_for('merchant/index').'\''));?>
          <input type="submit" value="Save" />
  </center></div>
  </fieldset>
</form>
