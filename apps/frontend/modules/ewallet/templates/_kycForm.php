<?php if($currency) {?>
<input type="hidden" name="currency" value=<?php echo $currency ?>>
<?php } ?>
<dl id='username_row'>
    <div class="dsTitle4Fields"><?php echo $form['image']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['image']->render(); ?>
        <?php echo $form['chkImage']->render(); ?>
        <br>
        (Image size should be maximum 20kb)
        <br>
        <div id="error_img" class="cRed"></div>
    </div>

</dl>

<dl id='username_row'>
    <div class="dsTitle4Fields"></div>
    <div class="dsInfo4Fields" align="left"> <img id="file-info" width="100px;" height="120px;" style="display:none;"></img>
        <?php //echo $sf_params->get('file-info');?>
    </div>
</dl>

<div id="doc_div">
    <div id="div_1">
        <dl id="username_row">
            <div class="dsTitle4Fields">
                <?php echo $form['doc1']->renderLabel(); ?>
            </div>
            <div class="dsInfo4Fields">
                <?php echo $form['doc1']->render(); ?>&nbsp;&nbsp;&nbsp;
                <?php echo $form['chkDoc1']->render(); ?><br><br>
                <div class="cRed" id="errorDoc1"></div></div></dl>                    </div>
</div>
<dl id='username_row'>
    <div class="dsTitle4Fields"></div>
    <div class="dsInfo4Fields" align="left"><?php echo __('(Total document size should be maximum 200kb)') ?><div class="cRed" id="err_alldoc"></div></div>
</dl>

<dl id='username_row'>
    <div class="dsTitle4Fields"></div>
    <div class="dsInfo4Fields" align="right"><a href="javascript:void(0);" id="addDoc" onClick="addMoreDoc();"><?php echo __('add More Document') ?></a></div>
</dl>

<div align="left" class="divBlock">

    <h5 class="passPolicy_new">
        &nbsp;&nbsp;<?php echo $form['confirmUpload']->render(); ?> <font color="gray">I accept and confirm that the uploaded image and documents are correct</font>        </h5>

    <div id="err_confirmUpload" style="color: red;" class="passPolicy"></div>
</div>
<?php echo $form['byPass']->render(); ?>
<?php echo $form['tmpImgName']->render(); ?>
<?php echo $form['tmpDocName']->render(); ?>
<?php echo $form['totDocSize']->render(); ?>
<?php echo $form['totdoc']->render(); ?>
<?php echo $form['totsize']->render(); ?>