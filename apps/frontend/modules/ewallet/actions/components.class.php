<?php

class ewalletComponents extends sfComponents
{

  public function executeAccounts(sfWebRequest $request)
  {
//    print "<pre>";
//    print_r($request->getParameterHolder()->getAll());

    $this->url = "";
    $this->target_div = "";
    $this->user_id = "";

    if($this->userId)
        $this->user_id = $this->userId;


    if($this->loadUrl)
        $this->url = $this->loadUrl;


    if($this->dest_div) 
        $this->target_div = $this->dest_div;
     
     $this->form = new UserAccountsForm(array(),array('user_id'=>$this->user_id));
  }

}

?>