<?php

/**
 * merchant actions.
 *
 * @package    mysfp
 * @subpackage merchant
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class ewalletActions extends sfActions {

    public static $eWalletFinancialReport = "/report/eWalletFinancialReport";
    
    public function executeGenerateCertificate(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $walletObj = new EpAccountingManager;
        $userConfig = $this->getUserConfig();

        if ($request->hasParameter('id')) {
            $this->id = $request->getParameter('id');
            $ewallet_account_id = base64_decode($request->getParameter('id'));
        } else {
            $EpId = $userConfig->getFirst()->getMasterAccountId();
            $getDetails = Doctrine::getTable('EpMasterAccount')->find($EpId);
            $ewallet_account_id = $getDetails->getId();
            $this->id = base64_encode($ewallet_account_id);
        }
        $userCollectObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($ewallet_account_id)->getFirst();
        $accountObj = $userCollectObj->getEpMasterAccount();
        $currencyObj = $userCollectObj->getCurrencyCode();

        //getUserDetails
        $userDetails = $this->getUserDetails($userConfig);
        $this->walletUserName = $userDetails['name'];
        $this->mobileNo = $userDetails['mobileno'];
        $this->emailId = $userDetails['emailid'];

        //get getWalletAccNo
//    $this->wallet_number = $this->getWalletAccNo($userConfig);
//    $this->accountName = $walletDetails->getFirst()->getAccountName();
        $this->wallet_number = $accountObj->getAccountNumber();
        $walletDetails = $walletObj->getWalletDetails($this->wallet_number);
        $this->accountName = $accountObj->getAccountName();
        $this->currency = ucwords($currencyObj->getCurrency());
        $this->currencyCode = $currencyObj->getCurrencyCode();
    }

    public function executeAccountBalance(sfWebRequest $request) {

        $this->checkIsSuperAdmin();
        $walletObj = new EpAccountingManager;
        $userConfig = $this->getUserConfig();

        //getUserDetails
        $userDetails = $this->getUserDetails($userConfig);

        $this->walletUserName = $userDetails['name'];
        $this->mobileNo = $userDetails['mobileno'];
        $this->emailId = $userDetails['emailid'];
        //get getWalletAccNo
        $this->wallet_number = $this->getWalletAccNo($userConfig);
        $walletDetails = $walletObj->getWalletDetails($this->wallet_number);
        $this->accountName = $walletDetails->getFirst()->getAccountName();

        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $this->arrAccountCollection = Doctrine::getTable('UserAccountCollection')->getAccounts($userId);


        // $account_id = $this->getWalletAccId($userConfig);
        // $acctObj = $walletObj->getAccount($account_id);
        // $this->walletBalance = $acctObj->getClearBalance();
        // $this->walletUnclearAmount = $acctObj->getUnclearAmount();
        // $this->walletBalance = $walletObj->getBalance($account_id, $uncleared=false);
        //echo "<pre>"; print_r($walletBalance); die();
    }

    public function executePrintCertificate(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $walletObj = new EpAccountingManager;
        $userConfig = $this->getUserConfig();

        if ($request->hasParameter('id')) {
            $this->id = $request->getParameter('id');
            $ewallet_account_id = base64_decode($request->getParameter('id'));
        } else {
            $EpId = $userConfig->getFirst()->getMasterAccountId();
            $getDetails = Doctrine::getTable('EpMasterAccount')->find($EpId);
            $ewallet_account_id = $getDetails->getId();
        }

        $userCollectObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($ewallet_account_id)->getFirst();
        $accountObj = $userCollectObj->getEpMasterAccount();
        $currencyObj = $userCollectObj->getCurrencyCode();
        //getUserDetails
        $userDetails = $this->getUserDetails($userConfig);
        $this->walletUserName = $userDetails['name'];
        $this->mobileNo = $userDetails['mobileno'];
        $this->emailId = $userDetails['emailid'];

        //get getWalletAccNo
        $this->wallet_number = $accountObj->getAccountNumber();
        $walletDetails = $walletObj->getWalletDetails($this->wallet_number);
        $this->accountName = $accountObj->getAccountName();
        $this->currency = ucwords($currencyObj->getCurrency());
        $this->currencyCode = $currencyObj->getCurrencyCode();

        $this->setLayout(false);
    }

    //this function returns the user connection
    private function getUserConfig() {
        $gUser = $this->getUser()->getGuardUser();
        $bUser = $gUser->getUserDetail();

        return $bUser;
    }

    public function executeKyc(sfWebRequest $request) {
        if($request->hasParameter('currency')){
            $this->currency= $request->getParameter('currency');
        }
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $ewalletObj = new ewallet();
        $kycStatus = $ewalletObj->getKycstatus($userId);
        $this->disapproveReason = '';
        $this->kycStatus = $kycStatus;
        $this->kycForm = new CustomKycForm(null, null, null);
        if ($kycStatus == 3) { //check for disapproval/rejection
            $arrReason = Doctrine::getTable('KycDetails')->getDisapproveReason($userId);
            $this->disapproveReason = $arrReason[0]['reason'];
        }

        if (0 == $kycStatus && $request->isMethod('post')) {

            $tmpImgName = $request->getPostParameter('tmpImgName');
            $tmpDocName = $request->getPostParameter('tmpDocName');

            $chkDoc = $request->getPostParameter('chkDoc');
            $chkDoc = unserialize(base64_decode($chkDoc));


            $frmtemp = '/temp/';
            $from_path = sfConfig::get('sf_upload_dir') . $frmtemp;

            $path = '/' . $userId;
            $final_path = sfConfig::get('sf_upload_dir') . $path;
            if (!file_exists($final_path))
                mkdir($final_path);
            chmod($final_path, 0755);
            ///////////////////////////// Image Upload Function /////////////////////////////////////////
            if ("on" == $request->getPostParameter('chkImage') && isset($tmpImgName)) {

                $fromImg = $from_path . $tmpImgName;
                $file_name = "img.jpeg";

                $path_to_upload = $final_path . '/' . $file_name;

                $cmd = 'mv ' . $fromImg . ' ' . $path_to_upload;
                exec($cmd, $output, $return_val);
            } else if (isset($tmpImgName)) {
                $fromImg = $from_path . $tmpImgName;
                @unlink($fromImg);
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////// File Upload Function ////////////////////////////////////////////
            $tmpDocArr = unserialize(base64_decode($tmpDocName));


            for ($i = 1; $i <= 3; ++$i) {
                $chkPar = "chkDoc" . $i;
                $file = "doc" . $i;
                if (("true" == $request->getPostParameter($chkPar) || "on" == $request->getPostParameter($chkPar)) && isset($_FILES[$file])) {

                    $fromDoc = $from_path . $tmpDocArr[$i];
                    $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $tmpDocArr[$i]);
                    $file_name = "doc" . $i . "." . $ext;

                    $path_to_upload = $final_path . '/' . $file_name;

                    $cmd = 'mv ' . $fromDoc . ' ' . $path_to_upload;
                    exec($cmd, $output, $return_val);
                } else if (isset($tmpDocArr[$i])) {
                    $fromDoc = $from_path . $tmpDocArr[$i];
                    @unlink($fromDoc);
                }
            }

            /////////////////////////////////////////////////////////////////////////////////////////////

            $user_detail->getFirst()->setKycStatus(2);
            $user_detail->save();
            $this->kycStatus = 2;
            if($request->hasParameter('currency'))
            {
                $account_name = $user_detail->getFirst()->getName() . " " . $user_detail->getFirst()->getLastName();

                $userAccounObj = new UserAccountCollection;
                $master_account_id = $userAccounObj->addAccount($account_name, $userId, $request->getParameter('currency'));
                $user_details = Doctrine::getTable('UserDetail')->findByUserId($userId);
                if (isset($master_account_id) && ($master_account_id != "")) {
                    $user_details->getFirst()->setMasterAccountId($master_account_id);
                    $user_details->save();
                }
            }
            //kep track of user ewallet detail
            Doctrine::getTable('KycDetails')->saveKycDetails($userId, $this->kycStatus);
            $userDetailsObj = $this->getUser()->getGuardUser();
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_KYCDETAIL, $userDetailsObj->getUsername(), $userDetailsObj->getId()));
            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_TRANSACTION,
                            EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_KYC_APPLY,
                            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_KYC_APPLY, array('username' => $userDetailsObj->getUsername())),
                            $applicationArr);
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        }
    }

    public function executeUploadImage(sfWebRequest $request) {
        $this->getContext()->getConfiguration()->loadHelpers('Asset');
        //sfLoader::loadHelpers('Asset'); //commeted as it has been deprecated in sfy 1.2

        $c = exec('file ' . $_FILES['image']['tmp_name']);
        $data = stripos($c, "JPEG");

        if ($data != "") {

            if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && $_FILES['image']['tmp_name'] != "") {
                $tmpfileName = $_FILES['image']['tmp_name'];
                if ($_FILES['image']['size'] / 1024 > 20) {
                    echo "<script>window.parent.document.getElementById('file-info').src='';</script>";
                    echo "<script>window.parent.document.getElementById('image').value=''</script>";
                    echo "<script>window.parent.document.getElementById('file-info').style.display='none'</script>";
                    echo "<script>window.parent.document.getElementById('chkImage').disabled=true</script>";
                    echo "<script>window.parent.document.getElementById('chkImage').checked=false</script>";
                    echo "<script>alert('The uploaded image is bigger than the allowed image size');</script>";
                    die;
                }
                $pin_serial_no = md5(date('Y_m_d_H:i:s'));

                $path = '/temp';
                $final_path = sfConfig::get('sf_upload_dir') . $path;
                if (is_dir($final_path) == '') {
                    mkdir($final_path, 0777, true);
                    chmod($final_path, 0777);
                }
                $file_name = $pin_serial_no . "_temp.jpeg";
                $path_to_upload = $final_path . '/' . $file_name;
                //        $apath =  $request->getUri().'../../../../uploads/'.$path.'/'.$file_name;

                $upload = move_uploaded_file($tmpfileName, $path_to_upload);
                chmod($path_to_upload, 0755);

                $imgPAth = image_path('/uploads/' . $path . '/' . $file_name);
                echo "<script>window.parent.document.getElementById('tmpImgName').value='" . $file_name . "'</script>";
                echo "<script>window.parent.document.getElementById('file-info').src='" . $imgPAth . "?" . rand('111', '9999') . "'</script>";
                echo "<script>window.parent.document.getElementById('file-info').style.display=''</script>";
                echo "<script>window.parent.document.getElementById('chkImage').disabled=false</script>";
                echo "<script>window.parent.document.getElementById('chkImage').checked=false</script>";
                die;
            }
        } else {
            echo "<script>window.parent.document.getElementById('file-info').src='';</script>";
            echo "<script>window.parent.document.getElementById('file-info').style.display='none'</script>";
            echo "<script>window.parent.document.getElementById('chkImage').disabled=true</script>";
            echo "<script>window.parent.document.getElementById('chkImage').checked=false</script>";
            echo "<script>window.parent.document.getElementById('image').value=''</script>";
            echo "<script>alert('The uploaded image should be jpeg image only');</script>";
            die;
        }
    }

    public function executeUploadDoc(sfWebRequest $request) {

        $docNum = $_REQUEST['docNum'];
        $docSize = $_REQUEST['docSize'];
        $docNameVar = $_REQUEST['docName'];

        $docName = 'doc' . $docNum;
        $msg_err = 'errorDoc' . $docNum;
        $chkDoc = 'chkDoc' . $docNum;

        $fileSize = $_FILES[$docName]['size'];
        $fileName = $_FILES[$docName]['name'];

        $c = exec('file ' . $_FILES[$docName]['tmp_name']);
        $data = stripos($c, "JPEG");

        if ($data != "") {

            if (isset($_FILES[$docName]) && isset($_FILES[$docName]['tmp_name']) && $_FILES[$docName]['tmp_name'] != "") {
                $totSize = $docSize + ($_FILES[$docName]['size'] / 1024);
                if ($totSize > 200) {
                    echo "<script>alert('You have total 200kb limit to load all document');</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').disabled=true</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').checked=false</script>";
                    echo "<script>window.parent.document.getElementById('$docName').value='';</script>";
                    echo "<script>window.parent.document.getElementById('$msg_err').innerHTML='';</script>";
                    die;
                } else {
                    $tmpfileName = $_FILES[$docName]['tmp_name'];
                    $pin_serial_no = md5(date('Y_m_d_H:i:s'));
                    $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $fileName);
                    $file_name = $pin_serial_no . $docNum . "." . $ext;

                    if ("" == $docNameVar)
                        $docNameVar = array();
                    else
                        $docArr=unserialize(base64_decode($docNameVar));

                    $docArr[$docNum] = $file_name;
                    $doctemp = base64_encode(serialize($docArr));

                    $path = '/temp';
                    $final_path = sfConfig::get('sf_upload_dir') . $path;
                    if (is_dir($final_path) == '') {
                        mkdir($final_path, 0777, true);
                        chmod($final_path, 0777);
                    }

                    $path_to_upload = $final_path . '/' . $file_name;

                    $upload = move_uploaded_file($tmpfileName, $path_to_upload);
                    chmod($path_to_upload, 0755);


                    echo "<script>window.parent.document.getElementById('totsize').value=$totSize;</script>";
                    echo "<script>window.parent.document.getElementById('tmpDocName').value='" . $doctemp . "'</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').disabled=false</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').checked=false</script>";
                    echo "<script>window.parent.document.getElementById('$msg_err').style.color='blue';</script>";
                    echo "<script>window.parent.document.getElementById('$msg_err').innerHTML='$fileName';</script>";
                    die;
                }
            }
        } else {

            echo "<script>window.parent.document.getElementById('" . $chkDoc . "').disabled=true</script>";
            echo "<script>window.parent.document.getElementById('" . $chkDoc . "').checked=false</script>";
            echo "<script>window.parent.document.getElementById('$docName').value=''</script>";
            echo "<script>window.parent.document.getElementById('$msg_err').innerHTML='';</script>";
            echo "<script>alert('The uploaded document should be jpeg only');</script>";
            die;
        }
    }

    //function to return the name, email and mobile no
    private function getUserDetails($connec) {
        $walletUserName = $connec->getFirst()->getName() . " " . $connec->getFirst()->getLastName();
        $mobileNo = $connec->getFirst()->getMobileNo();
        $emailId = $connec->getFirst()->getEmail();

        $uDetails = array('name' => $walletUserName, 'mobileno' => $mobileNo, 'emailid' => $emailId);

        return $uDetails;
    }

    //function to get the wallet id
    private function getWalletAccId($connec) {
        $epId = $connec->getFirst()->getMasterAccountId();
        return $epId;
    }

    //function to get the wallet account no
    private function getWalletAccNo($connec) {
        $EpId = $connec->getFirst()->getMasterAccountId();

        $getDetails = Doctrine::getTable('EpMasterAccount')->find($EpId);
        $wallet_number = $getDetails->getAccountNumber();

        return $wallet_number;
    }

    public function executeSearch(sfWebRequest $request) {
        if ($request->getParameter('id')) {
            $ewallet_account_id = base64_decode($request->getParameter('id'));
        } else {
            $user_id = $this->getUser()->getGuardUser()->getId();
            $user_detail = Doctrine::getTable('UserDetail')->findByUserId($user_id);
            $ewallet_account_id = $user_detail->getFirst()->getMasterAccountId();
        }
        $this->form = new EwalletAccountStatementForm();
        $this->formValid = "";

        $this->form->setDefault('account_id', $ewallet_account_id);
        if ($request->isMethod('post')) {

            $this->form->bind($request->getParameter('statement'));
            if ($this->form->isValid()) {
                $this->formValid = "valid";
            }
        }
        $this->checkIsSuperAdmin();
    }

    public function executeStatement(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->form = new EwalletAccountStatementForm();
        $this->form->bind($request->getParameter('statement'));
        $usePager = $request->getParameter('page');
        if ($this->form->isValid() || (isset($usePager) )) {
            $from_date = "";
            $to_date = "";
            $type = "";
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['statement']))
                $postDataArray = $postDataArray['statement'];


            $start_date = $postDataArray['from_date'];
            if ($start_date != "") {
                $dateArray = explode("-", $start_date);
                $from_date = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
            }


            $end_date = $postDataArray['to_date'];
            if ($end_date != "") {
                $dateArray = explode("-", $end_date);
                $to_date = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
            }


            $type = $postDataArray['type'];
            if ($type == "both") {
                $type = "";
            }


            if ($postDataArray['account_id']) {

                $ewallet_account_id = $postDataArray['account_id'];
                $currencyDetail = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($ewallet_account_id);
                $this->currencyId = $currencyDetail->getFirst()->getCurrencyId();

                $masterAcctObj = new EpAccountingManager;
                $this->acctDetails = $masterAcctObj->getAccount($ewallet_account_id);
                $this->statementObj = $masterAcctObj->getStatementDetails($from_date, $to_date, $ewallet_account_id, $type);
                $walletDetailsObj = $masterAcctObj->getTransactionDetails($ewallet_account_id, $from_date, $to_date, $type);

                $this->page = 1;
                if ($request->hasParameter('page')) {
                    $this->page = $request->getParameter('page');
                }

                $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($walletDetailsObj);
                $this->pager->setPage($this->page);
                $this->pager->init();
            }
        } else {

            die;
        }
    }

    public function executeNewEwalletUser() {
        $this->form = new NewEwalletUserForm();
    }

    public function executeNewEwalletUserSearch(sfWebRequest $request) {
        $postDataArray = $request->getParameterHolder()->getAll();
        if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
            $this->fromDate = $postDataArray['from_date'];
        } else {
            $this->fromDate = "";
        }
        if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
            $this->toDate = $postDataArray['to_date'];
        } else {
            $this->toDate = "";
        }
        $this->request_type = $postDataArray['request_type'];
        $userObj = Doctrine::getTable('UserDetail')->getNewEwallet($this->fromDate, $this->toDate, $this->request_type);

        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('UserDetail', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($userObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeDownloadfile(sfWebRequest $request) {
        $fileName = $request->getParameter('fileName');
        $folder = $request->getParameter('folder');
        $filePath = addslashes(sfConfig::get('sf_upload_dir') . '/' . $folder . '/' . $fileName);

        $this->setLayout(false);
        sfConfig::set('sf_web_debug', false);

        // Adding the file to the Response object
        // $this->getResponse()->setHttpHeader("Content-Length: ",filesize($filePath),TRUE);
        // $this->getResponse()->setHttpHeader('Content-type','application/csv',TRUE);
        $this->getResponse()->setHttpHeader('Content-type', 'application/force-download');
        $this->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileName}", TRUE);
        $this->getResponse()->sendHttpHeaders();
        $this->getResponse()->setContent(file_get_contents($filePath));
        return sfView::NONE;
    }

    public function executeSetUserStatus(sfWebRequest $request) {
        $userId = $request->getParameter('user_id');
        $getStatus = $request->getParameter('status');
        $reason = $request->getParameter('reason');
        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
        $pay4me_user = sfConfig::get('app_pay4me_account_type');

            If(($userDetails['0']['ewallet_type']==$pay4me_user['openid_pay4me']) && ($getStatus != 'denial')){

            $updateduser_type= sfConfig::get('app_ewallet_account_type');
            $updateduser_type = $updateduser_type['openid'];
            $this->getUser()->updateUserDetailForDeactivate($userId,$updateduser_type);
            $subject = "eWallet Activation Mail ";
            $partialName = 'active_mail';

            $taskId = EpjobsContext::getInstance()->addJob('SendActivateAccountMail', 'email' . "/sendActivateAccountMail", array('userid' => $userId, 'subject' => $subject, 'partialName' => $partialName));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            $pinObj = new pin();
            $pinObj->getPin($userId, $userDetails['0']['name'], $userDetails['0']['email']);
            }




        if ("denial" == $getStatus)
            $status = 3;
        else
            $status = 1;
        $setStaus = Doctrine::getTable('UserDetail')->updateEwalletStatus($userId, $status);
        //var_dump($setStaus); exit;
        Doctrine::getTable('KycDetails')->saveKycDetails($userId, $status, $reason);

        if ($setStaus) {
            If($userDetails['0']['ewallet_type']==$updateduser_type['openid'] ){
            $subject = "eWallet registration status mail";
            $partialName = 'ewalletStatus';
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $taskId = EpjobsContext::getInstance()->addJob('SendEwalletStatusMail', "ewallet/sendStatusEmail", array('userid' => $userId, 'partialName' => $partialName, 'status' => $status, 'reason' => base64_encode($reason)));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            }
            if ($status == 3) {
                $this->getUser()->setFlash('notice', sprintf('User disapproved successfully'));
                $event_description = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_KYC_REJECT;
                $sub_message_decription = EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_KYC_REJECT;
            } else {
                $this->getUser()->setFlash('notice', sprintf('User approved successfully'));
                $event_description = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_KYC_APPROVE;
                $sub_message_decription = EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_KYC_APPROVE;
            }
            $userDetailsObj = $this->getUser()->getGuardUser();
            $arrDetails = Doctrine::getTable('sfGuardUser')->find($userId);
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_KYCDETAIL, $arrDetails->getUsername(), $arrDetails->getId()));
            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_TRANSACTION,
                            $sub_message_decription,
                            EpAuditEvent::getFomattedMessage($event_description, array('username' => $arrDetails->getUsername())),
                            $applicationArr);
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            $this->forward($this->getModuleName(), 'newEwalletUserSearch');
        }
    }

    public function executeSendStatusEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = 'eWallet Secure Certificate Status'; //$request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $getStatus = $request->getParameter('status');
        $reason = base64_decode($request->getParameter('reason'));
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendStausEmail($userid, $subject, $partialName, $getStatus, $reason);
        return $this->renderText($mailInfo);
    }

    public function executeEwalletUserList(sfWebRequest $request) {
        $this->form = new EwalletUserListForm();
        $this->formValid = "";
        $this->selected = "";
        $getParams = $request->getParameter('ewalletUserList');
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('ewalletUserList'));
            $selected = $request->getParameter('selectBy');
            if ($selected == 'user_name')
                $this->selected = 'uname';
            elseif ($selected == 'user_type')
                $this->selected = 'status_option';
            else
                $this->selected = 'account_number';

            if ($this->form->isValid()) {
                $this->formValid = "valid";
            } else {
                echo $this->form->renderGlobalErrors();
            }
        }
    }

    public function executeEwalletUserListing(sfWebRequest $request) {

        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $this->postDataArray = $request->getParameterHolder()->getAll();
        if (isset($this->postDataArray['ewalletUserList'])) {
            $data = $this->postDataArray['ewalletUserList'];
        } else {
            $data = $this->postDataArray;
        }

        if (isset($data['name']) && isset($data['nameValue'])) {
            $data[$data['name']] = $data['nameValue'];
        }

        if (($request->getParameter('status'))) {
            $status = $request->getParameter('status');
            if ($status == 'activate') {
                $this->chgStatus = 'unblocked';
            } elseif ($status == 'deactivate') {
                $this->chgStatus = 'blocked';
            } elseif ($status == 'unBlockPin') {
                $this->chgStatus = 'pin reset';
            }
            if ($status == 'unBlockPin') {
                $this->setPinStatus($request);
            } else {
                $this->setStatus($request);
            }
        } else {
            $this->chgStatus = '';
        }

        $this->uname = '';
        $this->status_option = '';
        $this->accountNumber = '';
        $this->selectBy = $request->getParameter('selectBy');


        if (isset($data['status_option']) && $data['status_option'] != 'BLANK' && $data['status_option'] != "") {
            $this->status_option = $data['status_option'];
        } else {
            $this->status_option = '';
        }



        if (isset($data['uname']) && $data['uname'] != 'BLANK' && $data['uname'] != "") {
            $this->uname = $data['uname'];
        } else {
            $this->uname = '';
        }


        if (isset($data['account_number']) && $data['account_number'] != 'BLANK' && $data['account_number'] != "") {
            $this->accountNumber = $data['account_number'];
            $userDetail = Doctrine::getTable('EpMasterAccount')->getEwalletAccountDetails($this->accountNumber);
            if ($userDetail) {
                $this->uname = $userDetail->getFirst()->getUserAccountCollection()->getFirst()->getSfGuardUser()->getUsername();
            } else {
                $this->uname = 0;
            }
        }

        $userObj = Doctrine::getTable('sfGuardUser')->getAllEwalletUsers($this->uname, $this->status_option);
        $this->maxToBlock = sfConfig::get('app_number_of_failed_attempts_for_blocked_user');

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardUser', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($userObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function setPinStatus($request) {
        $userId = $request->getParameter('id');
        $status = $request->getParameter('status');
        $sfUid = $request->getParameter('sfUid');
        if ($userId != '') {
            $ewalletDetail = Doctrine::getTable('EwalletPin')->findByUserId(array($sfUid))->getFirst();
            $pin = substr(rand(), 0, 4);
            $pinObj = new pin();
            $ewalletDetail->set('pin_number', $pinObj->getEncryptedPin($pin));
            $ewalletDetail->set('status', 'inactive');
            $ewalletDetail->set('no_of_retries', 0);
            $ewalletDetail->set('forced_pin_change', 1);
            $ewalletDetail->save();
            $partialName = 'ewalletPinResetMail';
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $taskId = EpjobsContext::getInstance()->addJob('SendEwalletPinResetMail', "ewallet_pin/sendPinResetMail", array('pin' => $pin, 'partialName' => $partialName, 'userId' => $userId));
        }

        //        $this->pager = new sfDoctrinePager('sfGuardUser',sfConfig::get('app_records_per_page'));
        //        $this->pager->setQuery($userObj);
        //        $this->pager->setPage($this->page);
        //        $this->pager->init();
    }

    public function setStatus($request) {
        $userId = $request->getParameter('id');
        $status = $request->getParameter('status');
        $sfUid = $request->getParameter('sfUid');
        if ($userId != '') {

            $user = Doctrine::getTable('UserDetail')->find($userId);
            $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($sfUid));
            $ewalletObj = Doctrine::getTable('EwalletPin')->findByUserId($sfUid);
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_EWALLETINFO, $sfGuardUserDetail->getUsername(), $sfGuardUserDetail->getId()));

            if ($status == "activate") {

                $user->setFailedAttempt(0);
                $user->save();
                $pass_word = NULL;
                $activation_url = NULL;
                if($user->getEwalletType() == 'openid_pay4me'){
                    $partialName = 'ewallet_status_pay4me';
                }
                else{
                    # change forcefully ewallet pin status
                    $ewalletObj->getFirst()->set('status', 'inactive');
                    $ewalletObj->getFirst()->set('no_of_retries', 0);
                    $ewalletObj->getFirst()->set('forced_pin_change', 1);
                    $ewalletObj->save();

                    #End Code To Send Mail To The eWallet Owner
                    //resetting password
                    if($user->getEwalletType() != 'openid'){
                        $pass_word = PasswordHelper::generatePassword();
                        $sfGuardUserDetail->setPassword($pass_word);
                    }
                    $sfGuardUserDetail->setIsActive(false);
                    $sfGuardUserDetail->setLastLogin(NULL);
                    $sfGuardUserDetail->save();

                    #Generate Activation Token For Activation Link
                    $hashAlgo = sfConfig::get('app_user_token_hash_algo');
                    if($user->getEwalletType() != 'openid'){
                        $userPwdSalt = $sfGuardUserDetail->getSalt();
                    }else{
                        $userPwdSalt = $sfGuardUserDetail->getId();
                    }
                    $updatedAt = $sfGuardUserDetail->getUpdatedAt();

                    $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);



                    $partialName = 'ewallet_status';
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $activation_url = url_for('signup/activate', true) . '?i=' . $sfUid . '&t=' . $activationToken;
                }
                $subject = "eWallet Unblock Account Mail";
                $taskId = EpjobsContext::getInstance()->addJob('SendEwalletUnblockMail', "signup/sendEmail", array('userid' => $sfUid, 'subject' => $subject, 'partialName' => $partialName, 'activation_url' => $activation_url, 'password' => $pass_word));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                #End Code To Send Mail To The eWallet Owner
                //auditing the event
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_UNBLOCK,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_ACCOUNT_UNBLOCK, array('username' => $sfGuardUserDetail->getUsername())),
                                $applicationArr);

                //$user->setFailedAttempt (0);
                //$status_msg = "Activated";
            }
            if ($status == "deactivate") {
                $user->setFailedAttempt(sfConfig::get('app_number_of_failed_attempts_for_blocked_user'));
                $user->save();
                $status_msg = "Deactivated";
                $sfGuardUserDetail->setIsActive(true);
                $sfGuardUserDetail->save();
                $classObj = new sessionAuditManager();
                $classObj->doLogoutForcefully($user->getId());

                //auditing the event
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_BLOCK,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_ACCOUNT_BLOCK, array('username' => $sfGuardUserDetail->getUsername())),
                                $applicationArr);
            }


            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            //$this->getUser()->setFlash('notice', sprintf('User '.$status_msg.' successfully'),TRUE);
            //$this->redirect('report/ewalletUserList');
        }
    }

    public function checkIsSuperAdmin() {
        if ($this->getUser() && $this->getUser()->isAuthenticated()) {
            $group_name = $this->getUser()->getGroupNames();
            if (in_array(sfConfig::get('app_pfm_role_admin'), $group_name)) {
                $this->redirect('@adminhome');
            }
        }
    }

    public function executeEwalletUserMailingList() {
        $this->status_list = array('include_custom' => 'All', 1 => 'Block', 2 => 'Unblock');
        $this->form = new CustomEwUserMailListForm(array(), array('user_type_options' => $this->status_list), array());
    }

    public function executeEwalletUserMailingListSearch(sfWebRequest $request) {
        $uname = '';
        $status_option = '';

        if ($request->getParameter('selectBy') == 'user_type') {
            if ($request->getParameter('status_option') != 'BLANK') {
                $status_option = $request->getParameter('status_option');
            } else {
                $status_option = '';
            }
            $uname = '';
        } else if ($request->getParameter('selectBy') == 'user_name') {
            if ($request->getParameter('username') != 'BLANK') {
                $uname = $request->getParameter('username');
            } else {
                $uname = '';
            }
            $status_option = '';
        }
        //echo $status_option."---".$uname; die;

        $userObj = Doctrine::getTable('sfGuardUser')->getAllEwalletUsers($uname, $status_option);
        $this->maxToBlock = sfConfig::get('app_number_of_failed_attempts_for_blocked_user');

        // echo "<pre>"; print_r($userObj->fetchArray()); die;

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('sfGuardUser', 5);
        $this->pager->setQuery($userObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeEwalletUserComposeMail(sfWebRequest $request) {

        $recipietns = $request->getParameter('recipietns');

        if ($recipietns!='') {

            $ewalletMailIdArr = explode(',', $recipietns);
            foreach ($ewalletMailIdArr as $ewalletMailId) {
                $udetail = Doctrine::getTable('UserDetail')->getNameEmailId($ewalletMailId);
                $ewalletIds[] = $udetail['user_id'];
            }
            if (count($ewalletIds) > 0) {
                $ewalletUserIdArr = implode(',', $ewalletIds);
            }
            $this->ewalletUserIdArr = $ewalletUserIdArr;
            $this->form = new CustomEwUserComposeMail(array(), array('UserId' => $ewalletUserIdArr));
        }
        else
        {
         $this->getUser()->setFlash('error','Please select at least one user');
         $this->redirect('ewallet/ewalletUserMailingList');
        }
    }

    public function executeEwalletUserSendMail(sfWebRequest $request) {
        $input_array = array('_csrf_token' => $request->getParameter('_csrf_token'), 'subject' => $request->getParameter('subject'), 'message' => $request->getParameter('message'));
        $this->form = new CustomEwUserComposeMail(array($input_array), array('UserId' => $request->getParameter('UserId')));
        $this->form->bind($input_array);
        if ($this->form->isValid()) {
            $userId = $request->getParameter('UserId');
            $subject = $request->getParameter('subject');
            $message = $request->getParameter('message');
            if (count($userId) > 0) {
                //create a job to send the mail to eWallet Users
                $partialName = 'eWalletUserMailSend';
                $taskId = EpjobsContext::getInstance()->addJob('SendMaileWalletUserNotification', $this->moduleName . "/sendEmail", array('userid' => $userId, 'subject' => $subject, 'message' => $message, 'partialName' => $partialName));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            }
            $this->getUser()->setFlash('notice', sprintf("Email send successfully"));
            $this->redirect('ewallet/ewalletUserMailingList');
        } else {

            $this->setTemplate('ewalletUserComposeMail');
        }
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $partialName = $request->getParameter('partialName');
        $userid = $request->getParameter('userid');
        $ewalletUserIdArr = explode(',', $userid);
        $subject = $request->getParameter('subject');
        $message = $request->getParameter('message');

        /*
          $userid[0] = 3382;
          $userid[1]= 3383;
          $subject = 'eWallet Mail';
          $message = 'body';
          $partialName = 'eWalletUserMailSend';
         */

        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendeWalletUsersEmail($ewalletUserIdArr, $subject, $message, $partialName);
        return $this->renderText($mailInfo);
    }

    public function executeShowEwalletMailIds(sfWebRequest $request) {
        $byPass = $request->getParameter('byPass');
        $ewalletUserIdArr = explode(',', $byPass);
        $result = '';
        if (count($ewalletUserIdArr) > 0) {
            foreach ($ewalletUserIdArr as $ewalletUserId) {
                $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($ewalletUserId);
                $recipients[] = $userDetails[0]['UserDetail'][0]['email'];
            }
            $result = implode(',', $recipients);
        }
        $this->result = $result;
    }

    public function executePaidByVisaReport(sfWebRequest $request) {
        $this->err = '';
        $this->form = new PaidByVisaReportForm();
    }

    public function executePaidByVisaReportSearch(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod('post'));
        $this->err = '';
        try {

            if ($request->isMethod('post')) {  //echo "<pre>"; print_r($request->getPostParameters()); die;
                $this->form = new PaidByVisaReportForm();
                $this->processVisaReportSearchForm($request, $this->form);

                //$this->setTemplate('paidByVisaReport');
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    public function executeGetStatusfrmPayMode(sfWebRequest $request) {
        sfView::NONE;
        $PaymentMode = $request->getParameter('paymode');


        $parModel = 'app_paymode_' . $PaymentMode . "_name";
        $html = "";
        $records = Doctrine::getTable('GatewayOrder')->getOrderStatus($PaymentMode);
        foreach ($records as $status)
            if ('' != $status->getStatus())
                $html .="<option value = " . $status->getStatus() . " >" . ucwords($status->getStatus()) . "</option>";

        echo $html;

        die;
    }

    protected function processVisaReportSearchForm(sfWebRequest $request, sfForm $form) {

        $this->err = '';

        $form->bind($request->getParameter('paidwithvisa'));

        if ($form->isValid()) {
            $from_date = "";
            $to_date = "";
            $status = "";
            $formParam = $request->getParameter('paidwithvisa');


            $start_date = $formParam['from'];
            if ($start_date != "") {
//          $dateArray = explode("/",$start_date);
//          $from_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 00:00:00"));
                $from_date = $start_date . ' 00:00:00';
            }



            $end_date = $formParam['to'];
            if ($end_date != "") {
//          $dateArray = explode("/",$end_date);
//          $to_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 23:59:59"));
                $to_date = $end_date . ' 23:59:59';
            }



            $status = $formParam['trans_type'];
            if ($status == "all" || $status == "") {
                $status = "";
            }
            $PaymentMode = $formParam['pay_mode'];
            $PaymentType = $formParam['pay_type'];



            $user_id = $this->getUser()->getGuardUser()->getId();
            $groups = $this->getUser()->getGroupDescription();

            $parModel = 'app_paymode_' . $PaymentMode . "_name";
            if (strtolower($PaymentType) == 'payment')
                $visaRecords = Doctrine::getTable('GatewayOrder')->VisaStatement($from_date, $to_date, $status, $user_id, '', '', $PaymentMode, $PaymentType);
            else
                $visaRecords = Doctrine::getTable('GatewayOrder')->VisaRechargeStatement($from_date, $to_date, $status, $user_id, '', '', $PaymentMode, $PaymentType);

            $pfmHelperObj = new pfmHelper();
            $this->postDataArray = array();
            if ($request->isMethod('post')) {
                unset($_SESSION['pfm']['visaPayment']);
                $_SESSION['pfm']['visaPayment'] = $request->getPostParameters();
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            } else {
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            }

            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }

            $this->pager = new sfDoctrinePager('GatewayOrder', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($visaRecords);
            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {

            foreach ($form->getGlobalErrors() as $name => $error):
                $this->err .= $name . ': ' . $error . "<br>";
            endforeach;
            echo $form;
            die;
        }
    }

    public function executeViewDetail(sfWebRequest $request) {
        $this->setLayout(false);
        $txnId = $request->getParameter('transId');
        $type = $request->getParameter('type');
        $pending = $request->getParameter('pending');
        $userAccObj = new UserAccountManager();
        $this->isSuperadmin = $userAccObj->IsReportAdmin();
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        if ($pending == "all") {
            $pending = "";
        }
        $this->type = "";
        if ($request->hasParameter('type')) {
            $this->type = $request->getParameter('type');
        }
        //status for sucess transaction
        $this->successtrans = false;
        $this->paymentmode = "";
        if ($request->hasParameter('paymentmode')) {
            $this->paymentmode = $request->getParameter('paymentmode');
            if (($this->paymentmode == 2) || ($this->paymentmode == sfConfig::get('app_internet_bank'))) {
                //check sucess transaction for interswitch and internet_bank
                $arrCheck = Doctrine::getTable('GatewayOrder')->getOrderDetails($txnId, 'Success');
                if ($arrCheck) {
                    $this->successtrans = true;
                }
            }
        }
        $this->query = "";
        if ($request->hasParameter('query')) {
            $this->query = (bool) $request->getParameter('query');
        }
        $viewDetailQuery = Doctrine::getTable('GatewayOrder')->viewDetail($type, $txnId, $pending);

        $this->pager = new sfDoctrinePager('GatewayOrder', 5);
        $this->pager->setQuery($viewDetailQuery);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public static function getCurrencyId($currNum) {
        $currencyDetails = Doctrine::getTable('CurrencyCode')->findByCurrencyNum($currNum);
        if (count($currencyDetails) > 0) {
            return $currencyDetails->getFirst()->getId();
        } else {
            return false;
        }
    }

    public function executeAddAccount(sfWebRequest $request) {
        $this->form = new ewalletAddAccountForm();
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('add_account'));
            if ($this->form->isValid()) {
                $postParams = $request->getParameter('add_account');
                $this->AddEwalletAccount($postParams);
            }
        }
    }

    private function AddEwalletAccount($postParams) {
        $userId = $this->getUser()->getGuardUser()->getId();
        $currencyId = $postParams['currency_id'];

        $userdetail = $this->getUser()->getGuardUser()->getUserDetail();

        $account_name = $userdetail->getFirst()->getName() . ' ' . $userdetail->getFirst()->getLastName();

        $currencyDetails = Doctrine::getTable('CurrencyCode')->find($currencyId);
        $currency = ucwords($currencyDetails->getCurrency());
        $currencyCode = $currencyDetails->getCurrencyCode();

        $collectionObj = new UserAccountCollection();

        $accountObj = new UserAccountManager();
        $detail = $accountObj->getAccountForCurrency($currencyId);

        if ($detail) {
            $this->getUser()->setFlash('error', sprintf('Your account already exists for ' . $currency . '(' . $currencyCode . ')'), false);
        } else {
            $account_id = $collectionObj->addAccount($account_name, $userId, $currencyId);
            $masterAccDetail = Doctrine::getTable('EpMasterAccount')->find($account_id);

            $this->getUser()->setFlash('notice', sprintf('New account for ' . $currency . '(' . $currencyCode . ')' . ' added successfully.<br/> The account number is ' . $masterAccDetail->getAccountNumber()), false);
        }
    }

    public function executeRechargeReceipt(sfWebRequest $request) {
        // set recharge receipt layout as pop-up layout
        if ($request->hasParameter('template')) {
            $template = $request->getParameter('template');
            $this->setLayout($template);
        }

        $this->err = 0;
        $userAccObj = new UserAccountManager();
        $this->isSuperadmin = $userAccObj->IsReportAdmin();
        if ($request->hasParameter('validationNo')) {
            $this->validation_number = $request->getParameter('validationNo');
            if ($this->validation_number != "") {
                $this->validation_number = base64_decode($this->validation_number);
                $this->gatewayOrderObj = Doctrine::getTable('GatewayOrder')->findByValidationNumber($this->validation_number);
                $this->amount = $this->gatewayOrderObj->getFirst()->getAmount();
                $this->rechargeDate = $this->gatewayOrderObj->getFirst()->getUpdatedAt();
                $this->transactionRef = $this->gatewayOrderObj->getFirst()->getOrderId();
                $this->paymentModeOption = $this->gatewayOrderObj->getFirst()->getPaymentModeOptionId();
                $this->serviceCharge = $this->gatewayOrderObj->getFirst()->getServicechargeKobo();
                $this->rechargeAmount = ($this->amount - $this->serviceCharge);
                $accountId = $this->gatewayOrderObj->getFirst()->getAppId();
                $acctObj = new EpAccountingManager();
                $detailObj = $acctObj->getAccount($accountId);
                $wallet_number = $detailObj->getAccountNumber();

                $gateorderObj = new gatewayOrderManager();
                $this->checkPaymentModeOption = $gateorderObj->checkPaymentModeOption($this->paymentModeOption);

                $walletDetails = $acctObj->getWalletDetails($wallet_number);
                if ((!is_object($walletDetails)) && $walletDetails == 0) {

                    $this->err = "Invalid Account Number";
                } else {
                    $this->accountObj = $walletDetails->getFirst();
                    $collectionAccountObj = $walletDetails->getFirst()->getUserAccountCollection()->getFirst();
                    $currencyObj = $collectionAccountObj->getCurrencyCode();
                    $this->currencyId = $currencyObj->getId();
                    $this->userDetailObj = $collectionAccountObj->getSfGuardUser()->getUserDetail()->getFirst();
                }
            } else {
                $this->err = "Validation Number not found.";
            }
        } else {
            $this->err = "Validation Number not found.";
        }
    }
    
    public function executeEwalletFinancialReport(sfWebRequest $request) {
    	$this->form = new EwalletUserTransactionForm();
    	$this->formValid = "";
    	if ($request->isMethod('post')) {
    		$this->form->bind($request->getParameter('transaction'));
    		if ($this->form->isValid()) {
    			$this->formValid = "valid";
    		}
    	}
    }

    public function executeEwalletUserTransactionDetail(sfWebRequest $request) {

        $params = $request->getParameterHolder()->getAll();
        $_SESSION['pfm']['EwalletUserTransactionDetail'] = $params['transaction'];
        $param = $params['transaction'];
        $this->from_date = $from_date = $param['from']; //'';
        $this->to_date = $to_date = $param['to']; //'';
        $ewallet_account_id = $param['account_no'];
        $currency_id = $param['currency_id'];
        $this->currency_id = $currency_id;
        $masterAcctObj = new EpAccountingManager;
        $this->clearBalance = Doctrine::getTable('EpMasterAccount')->getClearBalanceForNoTransactions('ewallet', $currency_id,$ewallet_account_id);
        $this->statementObj = Doctrine::getTable('EpMasterLedger')->getStatementIn($from_date, $to_date, $ewallet_account_id, '', 'ewallet', $currency_id);
        $account_balance = $masterAcctObj->getTotalEwalletBalance($from_date, $to_date, $ewallet_account_id, "", 'ewallet', $currency_id);
     
        $grandCredit = 0;
        $grandDebit = 0;
        $this->grandBalance = 0;
        if ($account_balance) {
            foreach ($account_balance as $vals) {
                $entry_type = $vals->getEntryType();
                $var = "grand" . ucfirst($entry_type);
                $$var = $vals->getAmount();
            }
            $this->grandCredit = $grandCredit;
            $this->grandDebit = $grandDebit;
            $this->grandBalance = $grandCredit - $grandDebit;
        }
        
        
//        $accountObj = $masterAcctObj->getTotalClearBalance('ewallet', $currency_id, $ewallet_account_id);
//        if($accountObj) {
//          foreach($accountObj as $acctBalances)
//           $this->total_ewallet_clear_balance = $acctBalances->getTotalBalance();
//        }
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->statementObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
    
    public function executeEwalletFinancialReportCsv(sfWebRequest $request) {
    	ini_set('memory_limit', '1024M');
    	ini_set("max_execution_time", "64000");
    	$this->postDataArray = $_SESSION['pfm']['EwalletUserTransactionDetail'];
    
    	$param = $_SESSION['pfm']['EwalletUserTransactionDetail'];
    	$from_date = $param['from'];
    	$to_date = $param['to'];
    	$ewallet_account_id = $param['account_no'];
    	$currency_id = $param['currency_id'];
    	// Creating Headers
    	$headerNames = array(0 => array('S.No.', 'Account Number', 'Account Name', 'Total Credits', 'Total Debits', 'Current Balance'));
    	$headers = array('AccountNumber', 'AccountName', 'TotalCredit', 'TotalDebit', 'CurrentAccountBalance');
    	$masterAcctObj = new EpAccountingManager;
    	$statementQry = Doctrine::getTable('EpMasterLedger')->getStatementIn($from_date, $to_date, $ewallet_account_id, '', 'ewallet', $currency_id);
    	$statementObj = $statementQry->execute();
    	$clearBalance = Doctrine::getTable('EpMasterAccount')->getClearBalanceForNoTransactions('ewallet', $currency_id,$ewallet_account_id);
    	$i = 0;
    	$totalcAmount = 0;
    	$totaldAmount = 0;
    	$totalCurrentAccountBalance = 0;
    	foreach ($statementObj as $result) {
    		###################################
    		$ledgerObj = $result->getEpMasterLedger();
    		$collectionObj = $result->getUserAccountCollection()->getFirst();
    		$ledger_entries = count($ledgerObj);
    		$resultArr[$i]['AccountNumber'] = $result->getAccountNumber();
    		$resultArr[$i]['AccountName'] = $result->getAccountName();
    		if ($ledger_entries) {
    			$crAmount = 0;
    			$drAmount = 0;
    			foreach ($ledgerObj as $ledger_result):
    			if ($ledger_result->getEntryType() == 'credit') {
    				$crAmount = $ledger_result->getAmount();
    			} else {
    				$drAmount = $ledger_result->getAmount();
    			}
    			endforeach;
    			$cAmount = ($result->getCredit()) / 100;
    			$resultArr[$i]['TotalCredit'] = html_entity_decode(number_format($cAmount, 2, '.', ''));
    			$dcAmount = ($result->getDebit()) / 100;
    			$resultArr[$i]['TotalDebit'] = html_entity_decode(number_format($dcAmount, 2, '.', ''));
    		} else {
    			$cAmount = 0;
    			$dcAmount = 0;
    			$resultArr[$i]['TotalCredit'] = $cAmount;
    			$resultArr[$i]['TotalDebit'] = $dcAmount;
    		}
    		$CurrentAccountBalance = $cAmount - $dcAmount;
    		$totalcAmount +=$cAmount;
    		$totaldAmount +=$dcAmount;
    		$totalCurrentAccountBalance +=$CurrentAccountBalance;
    		$resultArr[$i]['CurrentAccountBalance'] = html_entity_decode(number_format($result->getClearBalance() / 100, 2, '.', ''));
    		$i++;
    		###################################
    	}
    	$resultArr[$i]['AccountNumber'] = '';
    	$resultArr[$i]['AccountName'] = 'Grand Total : ';
    	$resultArr[$i]['TotalCredit'] = html_entity_decode(number_format($totalcAmount, 2, '.', ''));
    	$resultArr[$i]['TotalDebit'] = html_entity_decode(number_format($totaldAmount, 2, '.', ''));
    	$resultArr[$i]['CurrentAccountBalance'] = html_entity_decode(number_format($clearBalance / 100, 2, '.', ''));
    	$recordsDetails = $resultArr;
    	if (count($recordsDetails) > 0) {
    		$arrList = $this->exportUserList($headerNames, $headers, $recordsDetails);
    		$file_array = csvSave::saveExportListFile($arrList, "/report/eWalletFinancialReport/", "eWalletFinancialReport");
    		$file_array = explode('#$', $file_array);
    		$this->fileName = $file_array[0];
    		$this->filePath = $file_array[1];
    		$this->folder = "eWalletFinancialReport";
    		$this->setTemplate('csv');
    	} else {
    		$this->redirect_url('index.php/report/rechargeReport');
    	}
    	$this->setLayout(false);
    }

    public function exportUserList($arrHeader, $headers, $arrList) {
        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $counterVal = count($arrList);
        $counterValHeaders = count($headers);
        for ($k = 0; $k < $counterVal; $k++) {
            if ($k == ($counterVal - 1))
                $arrExPortList[$k][] = '';
            else
                $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeaders; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }
    public function executeEdit(sfWebRequest $request) {
      $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id'));
      $this->username = $sf_guard_user->getUsername();
      $this->name = $sf_guard_user->getUserDetail()->getFirst();
      $this->id = $request->getParameter('id');
      $this->email_address = $sf_guard_user->getUserDetail()->getFirst()->getEmail();
      $this->form = new EditEwalletUserForm($sf_guard_user,array('action'=>'edit','email'=>$this->email_address,'name'=>$this->name,'username'=>$this->username,'id'=>$this->id));
      $this->userId = $request->getParameter('id');
      $this->setTemplate('edit');

    }

    public function executeUpdate(sfWebRequest $request) {

      $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id'));
      $this->form = new EditEwalletUserForm($sf_guard_user,array('action'=>'edit','email'=>$this->email_address,'name'=>$this->name,'username'=>$this->username,'id'=>$this->id));
      $this->processForm($request, $this->form);
      $this->setTemplate('edit');

    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
      $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id'));
      $this->username = $sf_guard_user->getUsername();
      $this->name = $sf_guard_user->getUserDetail()->getFirst();
      $form->bind($request->getParameter($form->getName()));

      if ($form->isValid()) {
        $postDataArray = $request->getParameterHolder()->getAll();
        $email = $postDataArray['ewallet']['email'];
        $sfUser = Doctrine::getTable("sfGuardUser")->find($request->getParameter('id'));
        $userDetailObj =  Doctrine::getTable("UserDetail")->find($sfUser->getUserDetail()->getFirst()->getId());
        $userDetailObj->setEmail($email);
        $userDetailObj->save();

        /*  For event audit trail (ewallet user email update) starts */
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $sfUser->getUsername(), $sfUser->getId()));
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_EWALLET_USER_EMAIL_UPDATE,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_EWALLET_USER_EMAIL_UPDATE, array('username' => $sfUser->getUsername())),
            $applicationArr);

        /* audit ends here */
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        $this->getUser()->setFlash('notice', sprintf('Ewallet User Profile updated successfully'));
        $this->redirect('ewallet/ewalletUserList');

      }

    }

    /*
     * Function to send mail on ewallet recharge report creation
     */
        public function executeSendReportConfermationEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendEwalletReportCreationEmail($userid, $subject, $partialName);
        return $this->renderText($mailInfo);
    }
    
    /* function will display financial report
     * 
     * @author Ramandeep
     */
    public function executeEwalletFinancialReportDisplay(sfWebRequest $request) {
        
        $this->folder = "eWalletFinancialReport";
        $userId = $this->getUser()->getGuardUser()->getid();
        $this->reportId = $request->getParameterHolder()->get('id');

        if ($this->reportId) {
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
            $this->dataById = Doctrine::getTable('ReportLog')->findById($this->reportId)->toArray();
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);

//            $this->dataById[0]['parameters'] = json_encode(Doctrine::getTable('ReportLog')->setcorrespondingName(json_decode($this->dataById[0]['parameters'], true)));

            if (isset($this->dataById[0]['deleted_at'])) {
                $this->type = "Archive";
            } else {
                $this->type = "Generated";
            }
            if (!(isset($this->dataById[0]['file_name']))) {
                $this->type = "Pending";
            }
            if(count($this->dataById) == 0){
              $this->dataById = array();
              $this->form = new ReportSearchForm('',array('type' => $this->type, 'frequency' => 'CUSTOM'));              
          }else{
            if ($this->dataById[0]['file_name'] == '' && $this->dataById[0]['deleted_at'] != '') {
                $this->dataById = NULL;
                $this->form = new ReportSearchForm('',array('type' => $this->type, 'frequency' => 'CUSTOM'));
            } else {
                $this->form = new ReportSearchForm('', array('type' => $this->type, 'frequency' => $this->dataById[0]['frequency']));
            }  
          }            
        } else {
            $this->form = new ReportSearchForm('',array('type' => $this->type, 'frequency' => 'CUSTOM'));
        }

        if ($request->isMethod('post')) {
            
            $pfmHelperObj = new pfmHelper();
            $this->userGroup = $pfmHelperObj->getUserGroup();

            if ($this->userGroup != "portal_admin") {
                $reportIds = Doctrine::getTable('ReportLog')->fetchReport('id', array('user_id' => $userId));

                // convert multidimentional array to single dimentional
                $reportIds = array_map('current', $reportIds);
                // checking that remove or archive request is being placed by creator only
                $diffResultSet = array_intersect($reportIds, $request->getParameter('archive'));

                if (array_diff($request->getParameter('archive'), $diffResultSet)) {
                    $this->getUser()->setFlash('notice', sprintf('You are not authorized for this action'));
                    $this->redirect('report/ewalletRechargeReportDisplay');
                }
            }

            /*
             * Archive Functionality
             */
            //*****************************
            if ($request->getParameterHolder()->get('request') == 'Archive') {
                $jobIds = explode(',', $request->getParameterHolder()->get('formIds'));
                $records = Doctrine::getTable('ReportLog')->deleteReportById($jobIds);
                $message = 'Report Archived successfully.';
                if($request->getParameterHolder()->get('refresh') == "yes"){
                    $this->getUser()->setFlash('notice', sprintf('Report archived successfully.'));
                }
            }
            //*****************************

            /*
             * Pending Functionality
             */
            //*****************************
            if ($request->getParameterHolder()->get('request') == 'Pending') {
                $jobIds = explode(',', $request->getParameterHolder()->get('formIds'));                
                $records = Doctrine::getTable('ReportLog')->removeJob($jobIds, $userId);
                $message = 'Request canceled successfully.';
                if($request->getParameterHolder()->get('refresh') == "yes"){
                    $this->getUser()->setFlash('notice', sprintf('Report canceled successfully.'));
                }
            }
            //*****************************

            $startDate = '';
            $endDate = '';

            if ($request->getParameter('frequency') == 'QUARTERLY') {
                if ($request->getParameter('quarter') == 1) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-01-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-03-31 23:59:59';
                }
                if ($request->getParameter('quarter') == 2) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-04-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-06-30 23:59:59';
                }
                if ($request->getParameter('quarter') == 3) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-07-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-09-30 23:59:59';
                }
                if ($request->getParameter('quarter') == 4) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-10-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-12-31 23:59:59';
                }
            }
            if ($request->getParameter('frequency') == 'MONTHLY' || $request->getParameter('frequency') == 'WEEKLY') {
                $startDate .= $request->getParameter('year');
                $startDate .= '-' . $request->getParameter('month');
                $startDate .= '-01 00:00:00';

                $endDate .= $request->getParameter('year');
                $endDate .= '-' . $request->getParameter('month');
                $thirtyFirst = array('01', '03', '05', '07', '08', '10', '12');

                if (in_array($request->getParameter('month'), $thirtyFirst)) {
                    $endDate .= '-31 23:59:59';
                } elseif ($request->getParameter('month') == '02') {
                    // leap year check
                    if (date('L', mktime(0, 0, 0, 1, 1, $request->getParameter('year')))) {
                        $endDate .= '-29 23:59:59';
                    } else {
                        $endDate .= '-28 23:59:59';
                    }
                } else {
                    $endDate .= '-30 23:59:59';
                }
                if ($request->getParameter('frequency') == 'WEEKLY') {
                    if (date('N', strtotime($startDate)) != "7") {
                        $start = date('N', strtotime($startDate));
                        $startDate = date('Y-m-d H:i:s', strtotime('-' . $start . ' days', strtotime($startDate)));
                    }
                    if (date('N', strtotime($endDate)) != "6") {
                        $end = 6 - date('N', strtotime($endDate));
                        $endDate = date('Y-m-d H:i:s', strtotime('+' . $end . ' days', strtotime($endDate)));
                    }
                }
            }
            if ($request->getParameter('frequency') == 'CUSTOM') {
                $startDate .= $request->getParameter('year');
                $startDate .= '-01-01 00:00:00';

                $endDate .= $request->getParameter('year');
                $endDate .= '-12-31 23:59:59';
            }
            $values = array(
                'from_date' => $startDate,
                'to_date' => $endDate,
                'frequency' => $request->getParameter('frequency'),
                'type' => $request->getParameter('type'),
                'report_name' => "EwalletFinancialReport",
            );

            if ($this->userGroup != "portal_admin") {
                if ($request->getParameter('frequency') == 'CUSTOM') {
                    $values['user_id'] = $userId;
                }
            }

            $records_query = Doctrine::getTable('ReportLog')->fetchReportName($values);
            if (isset($message)) {
                $this->msg = $message;
            }
            if($request->getParameter('type')){
                $this->type = $request->getParameter('type');
            }
            if($request->getParameter('frequency')){
                $this->frequency = $request->getParameter('frequency');
            }
            if($request->getParameter('year')){
                $this->year = $request->getParameter('year');
            }
            $this->report = 'EwalletFinancialReport';
            
            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('ReportLog', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($records_query);
            $this->pager->setPage($this->page);
            $this->pager->init();
            
            $this->setTemplate('reportListing', 'report');
        }
    }
}

