<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Split Types'); ?>
    </fieldset>
</div>

<table width="50%" class="tGrid">
    <thead>
      <tr>
        <th align="left" width="40%">Split Type</th>
      <th>&nbsp;</th>
      </tr>
    </thead>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tbody>
      <?php foreach ($split_type_list as $split_type): ?>
    <tr>
      <td><?php echo $split_type->getSplitName() ?></td>
      <td><a href="<?php echo url_for('splitType/edit?id='.$split_type->getId()) ?>">[EDIT]</a></td>
    </tr>
    <?php endforeach; ?>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
      <td>
          <?php  echo button_to('Create New Split Type','',array('onClick'=>'location.href=\''.url_for('splitType/new').'\''));?>
      </td>
      <td>&nbsp;</td>
    </tr>
    </tbody>
    
    
</table>  
