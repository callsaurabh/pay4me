<?php

/**
 * splitType actions.
 *
 * @package    mysfp
 * @subpackage splitType
 * @author     Vishnu Rai
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class splitTypeActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->split_type_list = Doctrine::getTable('SplitType')
      ->createQuery('a')
      ->execute();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new SplitTypeForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
        
    $this->form = new SplitTypeForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($split_type = Doctrine::getTable('SplitType')->find($request->getParameter('id')), sprintf('Object split_type does not exist (%s).', $request->getParameter('id')));
    $this->form = new SplitTypeForm($split_type);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($split_type = Doctrine::getTable('SplitType')->find($request->getParameter('id')), sprintf('Object split_type does not exist (%s).', $request->getParameter('id')));
    $this->form = new SplitTypeForm($split_type);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }  

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $split_type = $form->save();

      $this->redirect('splitType/index');
    }
  }
}
