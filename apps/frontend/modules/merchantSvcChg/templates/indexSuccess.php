<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Merchant Service Charge List'); ?>
    </fieldset>
</div>
<table class="tGrid">
  <thead>
    <?php if(isset($merchant_service_charges_list) && count($merchant_service_charges_list) > 0){ ?>
    <tr>      
      <th>Merchant service</th>
      <th>Service charge percent</th>
      <th>Upper slab</th>
      <th>Lower slab</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach ($merchant_service_charges_list as $merchant_service_charges):
    ?>
    <tr>      
      <td><?php echo $merchant_service_charges['MerchantService']['MERCHANT_NAME']; ?></td>
      <td><?php echo $merchant_service_charges['service_charge_percent'] ?>%</td>
      <td><?php echo $merchant_service_charges['upper_slab'] ?></td>
      <td><?php echo $merchant_service_charges['lower_slab'] ?></td>
      <td>
        <?php echo link_to(' ', 'merchantSvcChg/edit?id='.$merchant_service_charges['id'], array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
  <?php }else{ ?>
  <tr><td  align='center' class='error' colspan="5">No Merchant Service Charges Found</td></tr>
  <?php } ?>
</table>
  
<?php  echo button_to('Add New','',array('onClick'=>'location.href=\''.url_for('merchantSvcChg/new').'\''));?>