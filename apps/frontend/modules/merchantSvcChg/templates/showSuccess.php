<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $merchant_service_charges->getid() ?></td>
    </tr>
    <tr>
      <th>Merchant service:</th>
      <td><?php echo $merchant_service_charges->getmerchant_service_id() ?></td>
    </tr>
    <tr>
      <th>Service charge percent:</th>
      <td><?php echo $merchant_service_charges->getservice_charge_percent() ?></td>
    </tr>
    <tr>
      <th>Upper slab:</th>
      <td><?php echo $merchant_service_charges->getupper_slab() ?></td>
    </tr>
    <tr>
      <th>Lower slab:</th>
      <td><?php echo $merchant_service_charges->getlower_slab() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $merchant_service_charges->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $merchant_service_charges->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $merchant_service_charges->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $merchant_service_charges->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $merchant_service_charges->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('merchantSvcChg/edit?id='.$merchant_service_charges->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('merchantSvcChg/index') ?>">List</a>
