<?php

/**
 * merchantSvcChg actions.
 *
 * @package    mysfp
 * @subpackage merchantSvcChg
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class merchantSvcChgActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    //get merchant list
    $createObj = new merchantSvcChgManager();
    $this->merchant_service_charges_list = $createObj->getTotalMerchant();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->merchant_service_charges = Doctrine::getTable('MerchantServiceCharges')->find($request->getParameter('id'));
    $this->forward404Unless($this->merchant_service_charges);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new MerchantServiceChargesForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new MerchantServiceChargesForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($merchant_service_charges = Doctrine::getTable('MerchantServiceCharges')->find($request->getParameter('id')), sprintf('Object merchant_service_charges does not exist (%s).', $request->getParameter('id')));
    $this->form = new MerchantServiceChargesForm($merchant_service_charges);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($merchant_service_charges = Doctrine::getTable('MerchantServiceCharges')->find($request->getParameter('id')), sprintf('Object merchant_service_charges does not exist (%s).', $request->getParameter('id')));
    $this->form = new MerchantServiceChargesForm($merchant_service_charges);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($merchant_service_charges = Doctrine::getTable('MerchantServiceCharges')->find($request->getParameter('id')), sprintf('Object merchant_service_charges does not exist (%s).', $request->getParameter('id')));
    $merchant_service_charges->delete();

    $this->redirect('merchantSvcChg/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $merchant_service_charges = $form->save();

      $this->redirect('merchantSvcChg/index');
    }
  }
}
