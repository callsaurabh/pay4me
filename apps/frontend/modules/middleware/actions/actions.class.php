<?php

/**
 * middleware actions.
 *
 * @package    mysfp
 * @subpackage middleware
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class middlewareActions extends sfActions
{
	public static $currentState = null;
	/**
	* Executes index action
	*
	* @param sfRequest $request A request object
	*/
	public function executeIndex(sfWebRequest $request) {
		$this->forward('default', 'module');
	}

	/**
	 * Function for sending bank notification and updating mw_response and transaction_bank_posting table.
	 * @param integer $requestId
	 * @param integer $bankId
	 * @throws Exception
	 */
	public function sendBankNotificationRequest($requestId, $bankId, $level = 'ERROR') {
		if ($requestId) {

			// Generate request Xml.
			$requestXmlObject = $this->getBankRequestXml($requestId, $bankId);
			sfContext::getInstance()->getLogger()->info('MW Bank REQUEST XML:' . $requestXmlObject);

			// Send request to bank and receive response on same.
			$responseXml = middlewareBankIntegrationServiceFactory::getResponse($requestXmlObject, $bankId, $requestId);
			sfContext::getInstance()->getLogger()->info('Bank REPONSE XML:' . $responseXml);

			if($level == 'DEBUG') {
				$message = 'Response From Bank:  '.$responseXml;
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				$this->MiddlewareLog($message,$location,$requestId,$level);
			}

			// Code for creating text file in log folder path is log/bankResponse.
			//$this->setBankResponseLogFile($responseXml,$requestId, $bankId);

			if ($responseXml) {
				// Parse bank response.
				$responseArray = $this->parseBankResponse($responseXml, $requestId, $bankId);
				
				// Update transaction.
				$this->updateTransactionBankResponse($responseArray, $requestId, $bankId);
			} else {
				$message = 'Response XML not generated';
				if($level == 'ERROR') {
					$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
					$this->MiddlewareLog($message,$location,$requestId ,$level);
				}
				Doctrine::getTable('transactionBankPosting')->updateState($requestId,self::$currentState);
				$this->renderText($message);
				$this->getResponse()->setStatusCode(EpJobConstants::$EXECUTION_REPORTED_FAILURE);
			}
		} else {
			// If request id dosen't exist add log message.
			$message = 'Request Id not found';
			if($level == 'ERROR') {
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				$this->MiddlewareLog($message, $location, null, $level);
			}
			Doctrine::getTable('transactionBankPosting')->updateState($requestId,self::$currentState);
			$this->renderText($message);
			$this->getResponse()->setStatusCode(EpJobConstants::$EXECUTION_REPORTED_FAILURE);
		}
	}

	/**
	 * Function to generate the list of unsuccessfull mw_request transactions and call notification function
	 * @param sfWebRequest $request
	 */
	public function executeBankNotificationRequestList(sfWebRequest $request) {
		$this->setLayout(false);
		$this->setTemplate(false);
		$requestId = ($request->getParameter('mwRequestId'))?$request->getParameter('mwRequestId'):'';
		if ($requestId) {
			// Getting current state of the transaction.
			$transactionObj = Doctrine::getTable('transactionBankPosting')->findByLastMwRequestId($requestId);
			self::$currentState = $transactionObj[0]['state'];

			// Updating the state to under process.
			Doctrine::getTable('transactionBankPosting')->updateState($requestId,'posted_to_queue');

			// increasing the no. of attempts.
			$noOfAttempts = $transactionObj[0]['no_of_attempts'];
			$transactionId = $transactionObj[0]['message_txn_no'];
			$tbpObject = Doctrine::getTable('transactionBankPosting')->updateAttemptCount($transactionId,$noOfAttempts);

			$bankId = $this->getBankIdByRequestId($requestId);
			$level = $this->getBankLoggingLevel($bankId);
			if($level == 'DEBUG') {
				$message = 'Bank notification process initiated for Request ID '.$requestId;
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				$this->MiddlewareLog($message,$location,$requestId,$level);
			}
			$this->sendBankNotificationRequest($requestId, $bankId);
		}
		return sfView::NONE;
	}

	/**
	 * Function for getting bank id on the basis of mw request id.
	 * @param unknown $requestId
	 * @return unknown
	 */
	public function getBankIdByRequestId($requestId) {
		$record = Doctrine::getTable('transactionBankPosting')->findBy('LastMwRequestId', $requestId,Doctrine_Core::HYDRATE_ARRAY);
		$bankId = $record[0]['bank_id'];
		return $bankId;
	}

	/**
	 * Function to create txt file in log folder.
	 * @param string $response
	 * @param integer $mwRequestId
	 */
	public function setBankResponseLogFile($response,$mwRequestId, $bankId) {
		$logObj = new pay4meLog();
		$path = $logObj->getLogPath("bankResponse");
		$fileName = "BankResponse-" . $mwRequestId;
		$file_name = $path . "/" . ($fileName . '-' . date('Y_m_d_H:i:s')) . ".txt";
		$fp = fopen($file_name, "w+");
		fwrite($fp,$response);
		fclose($fp);
	}

	/** Function to save message log data in MwMessageLog table.
	 * Developed By:Deepak Bhardwaj
	 **/
	public static function MiddlewareLog($message,$location,$requestId,$level) {
		$logger_name = NULL;
		$log_date = date('Y-m-d H:i:s');
		$module = 'Middleware';
		$thread = NULL;
		$messageArray = array($level,$logger_name,$location,$module,$log_date,$message);
		$message = json_encode($messageArray);
		$Obj = new MwMessageLog();
		$Obj->setLogMessage($message);
		$Obj->setRequestId($requestId);
		$Obj->save();
	}
	
	/** Function to save message log data in MwMessageLog table.
	 * Developed By:Deepak Bhardwaj
	 **/
	public static function getBankLoggingLevel($bankId) {
		$logLevel = Doctrine::getTable('bank')->getLoggingLevel($bankId);
		return $logLevel;
	}

	/**
	 * Function to generate request xml sent to bank.
	 * @param integer $requestId
	 * @return string responseXml
	 */
	public function getBankRequestXml($requestId, $bankId) {
		$level = $this->getBankLoggingLevel($bankId);
		if ($requestId) {
			// Generating xml to send for bank notification.
			$objBI = new bankIntegration();
			$xmldataObj = $objBI->generateBankTransactionXML($requestId);
			if($level == 'DEBUG') {
				$message = 'Generated xml to send for bank notification';
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				$this->MiddlewareLog($message,$location,$requestId,$level);
			}
			return $xmldataObj;
		} else {
			$message = 'Unable to generate xml to send to bank notification';
			if($level == 'ERROR') {
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				$this->MiddlewareLog($message,$location,$requestId,$level);
			}
			Doctrine::getTable('transactionBankPosting')->updateState($requestId,self::$currentState);
			$this->renderText($message);
			$this->getResponse()->setStatusCode(EpJobConstants::$EXECUTION_REPORTED_FAILURE);
		}
	}
	
	/**
	 * Function to get bank resopnse.
	 * @param string $requestXmlObject
	 * @param integer $bankId
	 * @param integer $requestId
	 * @return boolean|unknown
	 */
	public function getBankResponse($requestXmlObject, $bankId,$requestId) {
		ini_set('soap.wsdl_cache_enabled',0);
		ini_set('soap.wsdl_cache_ttl',0);
		if(!extension_loaded("soap")){
			dl("php_soap.dll");
		}
		$authenticationHeader = $this->getAuthHeader($bankId);
		$context = array('http' => array(
			'header'  => $authenticationHeader)
		);
		if(version_compare(PHP_VERSION, '5.3.0') == -1){
			ini_set('user_agent', 'PHP-SOAP/' . PHP_VERSION . "\r\n" . $authenticationHeader);
		}
	
		$wsdl = Doctrine::getTable('BankProperties')->getBankPropertryValue($bankId, 'BANK_POST_URL');
		$level = $this->getBankLoggingLevel($bankId);
		if($level == 'DEBUG') {
			$message = 'Notification Sent to bank post URL :  '.$wsdl;
			$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
			$this->MiddlewareLog($message,$location,$requestId,$level);
		}
		try{
			//$client = new SoapClient("http://localhost/soap_test/books.wsdl");
			//$responseXml = $client->doMyBookSearch($requestXmlObject);

			// Server link for notification.
			$client = new SoapClient($wsdl, array('trace'=> true, 'exceptions' => true));
			$responseXml = $client->invokeMessage($requestXmlObject);
			
		} catch(Exception $e) {
			if($level == 'DEBUG') {
				$message = 'Error on SoapClient URL :  '.$wsdl.'\n';
				$message .= $e->getMessage();
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				$this->MiddlewareLog($message,$location,$requestId,$level);
			}
			return false;
		}
		return $responseXml;
	}

	/**
	 * Function to generate http header.
	 * @param integer $bankId
	 * @return string
	 */
	public function getAuthHeader($bankId) {
		$bank_code = Doctrine::getTable('BankProperties')->getBankPropertryValue($bankId, 'BANK_CODE');
		$bank_key = Doctrine::getTable('BankProperties')->getBankPropertryValue($bankId, 'BANK_KEY');
	
		$bank_auth_string = trim($bank_code) .":" .trim($bank_key);
		$bankAuth = base64_encode(trim($bank_auth_string)) ;
	
		$authenticationHeader = 'Authorization: Basic ' . trim($bankAuth) . "\r\n"
			. "Accept: application/xml; charset=UTF-8";
	
		return $authenticationHeader;
	}
	
	/**
	 * Function for parsing bank response xml.
	 * @param unknown $responseXml
	 * @param unknown $requestId
	 * @param unknown $bankId
	 * @return Ambigous <NULL, multitype:string , unknown>
	 */
	public function parseBankResponse($responseXml, $requestId, $bankId){
		$xml = simplexml_load_string($responseXml);
		$json = json_encode($xml);
		$responseArray = json_decode($json,TRUE);
		$dataArray = array();
		$level = $this->getBankLoggingLevel($bankId);
		foreach ($responseArray as $key => $value) {
			if ($key == 'item') {
				if (isset($value['bank-transaction-number']) && !empty($value['bank-transaction-number'])) {
					$dataArray['data']['bank-transaction-number'] = $value['bank-transaction-number'];
				} else {
					$dataArray['data']['bank-transaction-number'] = NULL;
				}
				if (isset($value['pay4me-transaction-number']) && !empty($value['pay4me-transaction-number'])) {
					$dataArray['data']['pay4me-transaction-number'] = (integer) $value['pay4me-transaction-number'];
				} else {
					$dataArray['data']['pay4me-transaction-number'] = NULL;
				}
			}
			if ($key == 'status') {
				if (isset($value['code'])) {
					$dataArray['state'] = '';
					$dataArray['data']['response-code'] = (integer) $value['code'];
					if($level == 'DEBUG') {
						$message = 'Response Code  :  '.$dataArray['data']['response-code'];
						$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
						$this->MiddlewareLog($message,$location,$requestId,$level);
					}
					// code 0 for Sucess and 9400 is for duplicate entry.
					if ($dataArray['data']['response-code'] == '0'|| $dataArray['data']['response-code'] == '9400') {
						$dataArray['state'] = "success";
					}
				} else {
					$dataArray['data']['response-code'] = NULL;
					if($level == 'ERROR') {
						$message = 'Response code not found';
						$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
						$this->MiddlewareLog($message,$location,$requestId,$level);
					}
				}
				if (isset($value['message']) && !empty($value['message'])) {
					$dataArray['data']['response-message'] = $value['message'];
					if($level == 'DEBUG') {
						$message = 'Response Message  :  '.$dataArray['data']['response-message'];
						$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
						$this->MiddlewareLog($message,$location,$requestId,$level);
					}
				} else {
					$dataArray['data']['response-message'] = NULL;
					if($level == 'ERROR') {
						$message = 'Response message not found';
						$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
						$this->MiddlewareLog($message,$location,$requestId,$level);
					}
				}
			}
		}
		return $dataArray;
	}

	/**
	 * Function to update transaction bank posting record.
	 * @param unknown $responseArray
	 * @param unknown $requestId
	 * @param unknown $bankId
	 * @throws Exception
	 */
	public function updateTransactionBankResponse($responseArray, $requestId, $bankId) {
		// Updating required tables when response is success.
		$level = $this->getBankLoggingLevel($bankId);
		if (!empty($responseArray) && $responseArray['state'] == 'success') {
			$mwResponse = array(
				'message_text' => json_encode($responseArray['data']),
				'request_id' => $requestId,
				'message_type' => 'transaction',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			);
			$transactionBankPosting = array(
				'state' => $responseArray['state'],
				'bank_transaction_no' => $responseArray['data']['bank-transaction-number'],
				'pay4me_transaction_no' => $responseArray['data']['pay4me-transaction-number'],
				'response_code' => $responseArray['data']['response-code'],
				'updated_at' => date('Y-m-d H:i:s'),
			);
			$con = Doctrine_Manager::connection();
			try {
				$con->beginTransaction();
				// Saving Bank response in mw_response table.
				$mwObject = new MwResponse();
				$mwObject->message_text = $mwResponse['message_text'];
				$mwObject->request_id = $mwResponse['request_id'];
				$mwObject->message_type = $mwResponse['message_type'];
				$mwObject->save();
				if(!$mwObject->getId()) {
					$con->rollback();
					throw new Exception('error found');
				}
				//Saving bank response in transaction_bank_posting table.
				$tbpObject = Doctrine::getTable('transactionBankPosting')->updateBankResponse($transactionBankPosting);
				if(!$tbpObject) {
					$con->rollback();
					throw new Exception('error found');
				}
				$con->commit();
			}
			catch (Exception $e) {		
				$message = 'Error Found:  '.$e->getMessage();
				if($level == 'ERROR') {
					$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
					$this->MiddlewareLog($message,$location,$requestId,$level);
				}
				$this->renderText($message);
				$this->getResponse()->setStatusCode(EpJobConstants::$EXECUTION_REPORTED_FAILURE);
				$con->rollback();
			}
			$this->renderText('Successfull');
			//$this->getResponse()->setStatusCode(0);
		} else {
			// If status is other then success add log message.
			$message = 'Request not processed successfully';
			if($level == 'ERROR') {
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				$this->MiddlewareLog($message,$location,$requestId,$level);
			}
			Doctrine::getTable('transactionBankPosting')->updateState($requestId,self::$currentState);
			$this->renderText($message);
			$this->getResponse()->setStatusCode(EpJobConstants::$EXECUTION_REPORTED_FAILURE);
		}
	}
}