<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_helper('Form');
use_helper('Pagination');  ?>
<div id="status"></div>
<div id="theMid">

<?php echo ePortal_legend(__('Search User'));      ?>
<div class="wrapForm2">
<?php echo form_tag($sf_context->getModuleName().'/index','name=search id=search');?>
     <?php include_partial('bankBranchReportUser/searchUser',array('form'=>$form))?>
<div class="clear"></div></form>

</div>

    <?php echo ePortal_legend(__("Bank Branch User List")); ?>

    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr class="alternateBgColour">
                <th width="100%" >
                    <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
                    <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
                </th>
            </tr>
        </table>
        <br class="pixbr" />
    </div>
  <?php
   if(($pager->getNbResults())>0) { ?>
<div class="wrapTable" style="_margin-top:-20px; _margin-bottom:-20px;" >
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft" ><div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('create_bank_branch_user/bankBranchUserCsv', 'csvDiv', 'progBarDiv')">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</div><div id ="progBarDiv"  style="display:none;"></div></span>
      </th>
    </tr>
  </table>
</div>
<br class="pixbr" />


   <?php } ?>

<div class="wrapTable" >
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
            <th width = "2%">S.No.</th>
            <th><?php echo __('Username'); ?></th>
            <th><?php echo __('Bank Branch'); ?></th>
            <th><?php echo __('Branch Code'); ?></th>
            <th><?php echo __('Bank'); ?></th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
            foreach ($pager->getResults() as $result):
                $i++;
                ?>
       <tr class="alternateBgColour">
            <td align="center"><?php echo $i ?></td>
            <td align="center"><?php echo $result->getUserName() ?></td>
            <td align="center"><?php echo $result->getBankBranchName() ?></td>
            <td align="center"><?php echo $result->getBranchCode() ?></td>
            <td align="center"><?php echo $result->getBankName() ?></td>
            <td align="right">


            <?php

            if($result->getUserStatus()==3)
            {
                echo "<font color=red>De-Activated</font>";
            }

            else{
            if($result->getFailedAttempt() == 5){ echo link_to(' ', 'create_bank_admin/unblock?id='.$result->getId().'&redirect_to='.$sf_context->getModuleName(), array('method' => 'get', 'class' => 'unblockInfo', 'title' => 'Unblock User'));echo "&nbsp;"; }
            echo "&nbsp;";
            if($result['is_active']=='1') echo link_to(' ', url_for('create_bank_admin/resetPassword').'?userId='.$result->getId().'&redirect_to='.$sf_context->getModuleName(), array('method' => 'head', 'confirm' => 'Are you sure, you want to reset the password?', 'class' => 'userEditInfo', 'title' => 'Reset Password')) ;
            echo "&nbsp;";
            echo link_to(' ', 'create_bank_branch_user/edit?id='.$result->getId(), array( 'confirm' => __('Are you sure, you want to edit the user?'), 'class' => 'editInfo', 'title' => 'Edit')) ;
            //            echo "&nbsp;";
            //            if($result['is_active']=='1'){
            //              echo link_to(' ', 'create_bank_branch_user/delistUsers?userId='.$result->getId().'&use=0', array('method' => 'head', 'confirm' => 'Are you sure, you want to deactivate the user?', 'class' => 'deactiveInfo', 'title' => 'Deactivate')) ;
            //             }elseif($result['is_active']=='0'){
            //              echo link_to(' ', 'create_bank_branch_user/delistUsers?userId='.$result->getId().'&use=1', array('method' => 'head', 'confirm' => 'Are you sure, you want to activate the user?', 'class' => 'activeInfo', 'title' => 'Activate'));
            //              }
            echo "&nbsp;";

           // echo link_to(' ', 'create_bank_branch_user/delete?userId='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to delete the user?'), 'class' => 'delInfo', 'title' => 'Delete')) ;
            //echo "&nbsp;";
            if($result->getUserStatus() == 1)
            {
                echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=2&user_id='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to suspend the user?'), 'class' => 'deactiveInfo', 'title' => 'Suspend')) ;
            }

            if($result->getUserStatus() == 2)
            {
                echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=1&user_id='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to resume the user?'), 'class' => 'activeInfo', 'title' => 'Resume')) ;
            }

            echo "&nbsp;";
            echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=3&user_id='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to De-Activate the user?'), 'class' => 'deactivatedInfo', 'title' => 'De-Activate')) ;
            }

            ?>




                </tr>
                <?php endforeach; ?>
                <tr><td colspan="6">
                        <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

                </div></td></tr>
                <?php
            }else { ?>

                <tr><td  align='center' class='error' colspan="6"><?php echo __("No Bank Branch Users Found"); ?></td></tr>
                <?php } ?>
            </tbody>
            <tfoot><tr><td colspan="6"></td></tr></tfoot>
        </table>
        <?php echo button_to(__('Add New'),'',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('create_bank_branch_user/new').'\''));?>

</div>
<?php include_partial('create_bank_admin/message')?>
</div>

<script>

    var activate_url = "<?php echo url_for('create_bank_branch_user/setUserStatus');?>";
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}

    //  function set_user_status(id,status,status_val){
    //   //   alert(activate_url);
    //   var answer = confirm("Are you sure you want to "+status_val)
    //   var url =  "";
    //
    //	if (answer){
    //        $.post(activate_url,{user_id:id,status:status}, function(data){alert(data);$('#status').html(data);});
    //    //fetch_bank_branches(activate_url);
    //    }
    //  }
</script>
