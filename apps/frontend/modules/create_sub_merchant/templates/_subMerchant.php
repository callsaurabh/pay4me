<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<div class="wrapForm2">
	<?php
		$heading = ($form->getObject()->isNew() ? 'Create New' : 'Edit');
		echo ePortal_legend(__($heading.' Sub Merchant'));
	?>
	<?php echo  $form->renderGlobalErrors();?>
	<div id='flash_error'  style='display:none; visibility:hidden;height:1px;overflow-y:hidden'></div>
	<form action="<?php echo url_for($sf_context->getModuleName().'/'.($form->getObject()->isNew() ? 'create' : 'modify').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : ''))?>"  id="pfm_create_user_form" name="pfm_create_user_form"   method="post" enctype="multipart/form-data"  >
		<?php echo $form ?>
		<?php echo $form->renderHiddenFields(); ?>
		<input type="hidden" name="userType" value="<?php echo $user_type;?>" />
		<div class="divBlock">
			<center>
				<input type="submit" value=<?php echo __("Save"); ?> class="formSubmit" />
				&nbsp;&nbsp;
				<?php echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/index').'\''));?>
			</center>
		</div>
	</form>
</div>
<script type="text/javaScript">
	$(document).ready(function() {
		$('#merchant').change(function(){
			if ($('#merchant').val()!='') {
				var merchant_id = this.value;
				var merchant_service_id = '';
				var url = "<?php echo url_for('create_sub_merchant/merchantServiceList'); ?>";
				$('#service_type').load(url,{ merchant_id: merchant_id,merchant_service_id:merchant_service_id,byPass:1},
				function (data){
					if(data=='logout'){
						location.reload();
					}
				});
			} else {
				$('#service_type').html("<option value='' selected>Please select Merchant Service</option>") ;
			}
			return false;
		});
	});
</script>