<?php

/**
 * create_sub_merchant actions.
 *
 * @package    mysfp
 * @subpackage create_sub_merchant
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class create_sub_merchantActions extends sfActions
{
 /**
 * Executes index action
 *
 * @param sfRequest $request A request object
 */
	public function executeIndex(sfWebRequest $request)
	{
		$this->checkIsSuperAdmin();
		$userId = $this->getUser()->getGuardUser()->getid();
		$merchant_id = Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
		$sub_merchant_id = Doctrine::getTable("MerchantUser")->getSubMerchantIdFromMerchantId($merchant_id);
        $this->postDataArray = array();
        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $request->getPostParameters();
            $_SESSION['pfm']['reportData']['merchant_id'] = $merchant_id;
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        } else {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData']['merchant_id'] = $merchant_id;
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }

        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_sub_merchant')); //get Group Id; of bank admin
        $groupid = $sfGroupDetails->getFirst()->getId();

        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id, $country_id);
        $this->bank_branch_id = $request->getParameter('bank_branch_id');
        $this->userName = $request->getParameter('user_search');
        $this->form = new searchUserForm($request->getPostParameters(), array('bank_id' => $bank_id, 'country_id' => $country_id, 'value' => $this->userName));
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllUsers($groupid, $bank_id, $this->userName, $this->bank_branch_id, $country_id);
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
		
	}

	public function executeNew(sfWebRequest $request) {
		$this->user_type = "sub_merchant";
		$this->userGroup = "";
		$pfmHelperObj = new pfmHelper();
		$userGroup = $this->userGroup = $pfmHelperObj->getUserGroup();
		if($userGroup == "merchant"){
			$userId = $this->getUser()->getGuardUser()->getid();
			$merchant_id = Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
		}
		$selServiceTypeChoices = array('' => 'Select Merchant Services');
		$this->merchant_service_choices = $selServiceTypeChoices;
		
		if(isset($merchant_id)){
			$this->form = new CreateSubMerchantForm(array(), array('merchant_id' => $merchant_id, 'merchant_service' => $this->merchant_service_choices, 'userGroup' => $this->userGroup));
		}else{
			$serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr();
			$selMerchantChoices = array('' => 'Select Merchant');
			$this->merchant_choices = $selMerchantChoices + $serviceTypeArray;
			$this->form = new CreateSubMerchantForm(array(), array('merchant_service' => $this->merchant_service_choices,'userGroup' => $this->userGroup));
		}
	}

	public function executeCreate(sfWebRequest $request) {
		//$this->redirecturl = 'index';
		$data = $request->getPostParameters();
		$this->user_type = $data['userType'];
		$this->redirecturl = "new";
		$this->setTemplate("new");
		$merchentId = $data['merchant'];
		$merchentServiceId = $data['service_type'];
		$selMerchantChoices = array('' => 'Select Merchant');
		$selServiceTypeChoices = array('' => 'Select Merchant Services');
		$username = $data['username'];
		$serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr();
		$selMerchantChoices = $selMerchantChoices + $serviceTypeArray;

		$selServiceTypeChoices = $merchentServiceId ? ($selServiceTypeChoices + $this->construct_options_array(Doctrine::getTable('MerchantService')->getServiceTypes($merchentId)->toArray())) : $selServiceTypeChoices;
		$this->form = new CreateSubMerchantForm(array(), array('merchant' => $selMerchantChoices, 'merchant_service' => $selServiceTypeChoices));
		$this->form->bind($data);
		$username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
		if ($username_count == 0) {
			if($this->form->isValid()){
				$sfUser = $this->saveDataOfSubMerchant($request, $data);
				$user_id = $sfUser->getId();
				$user_name = $data['name'];
				$pass_word = $this->pWord;
				$role = 'Sub Merchant';
				
				/*Audit Trail code starts*/
				$applicationArr = array(new EpAuditEventAttributeHolder(
					EpAuditEvent::$ATTR_USERINFO, $user_name, $user_id)
				);
				$eventHolder = new pay4meAuditEventHolder(
					EpAuditEvent::$CATEGORY_TRANSACTION,
					EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_CREATE,
					EpAuditEvent::getFomattedMessage(
						EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_CREATE_SUB_MERCHANT_ACCOUNT,
						array('username' => $username)
					),
					$applicationArr
				);
				$this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

				$this->sendMailToSubMerchantUser($user_id,$user_name, $pass_word);
				
				if(sfConfig::get('app_display_password'))
					$this->getUser()->setFlash('notice', sprintf($user_name." created successfully as ".$role."<br> The password is - ".$pass_word));
				else
					$this->getUser()->setFlash('notice', sprintf($user_name." created successfully as ".$role.'<br> An email has been sent to user regarding the same.'));
					$this->redirect('create_sub_merchant/'.$this->redirecturl);
			}
		} else {
			$this->getUser()->setFlash('error', sprintf('Username already exists'));
			$this->setTemplate('new');
		}
	}

	//move data to model
	private function saveDataOfSubMerchant($request, $data) {
		$sfUser = new sfGuardUser();
		$sfUser->setUsername($data['username']);
		$pass_word = PasswordHelper::generatePassword();
		$this->pWord = $pass_word;
		$salt = $this->getUser()->getGuardUser()->getSalt();
		$sfUser->setSalt($salt);
		$sfUser->setPassword($pass_word);
		$sfUser->setIsActive(true);
		$sfUser->save();
		//insert userid and password in EpPasswordPolicy table
		EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(), $sfUser->getPassword());
		$sfUserId = $sfUser->getId();
		$userTypeToCreate = 'app_pfm_role_sub_merchant';
		$sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get($userTypeToCreate));
		
		$sfGroupId = $sfGroupDetails->getFirst()->getId();
	
		$sfGuardUsrGrp = new sfGuardUserGroup();
		$sfGuardUsrGrp->setGroupId($sfGroupId);
		$sfGuardUsrGrp->setUserId($sfUserId);
		$sfGuardUsrGrp->save();
	
		$this->saveUserDetail($data, $sfUserId);
		return $sfUser;
	}

	protected function saveUserDetail($data, $userId) {
		$userDetailObj = new UserDetail();
		$userDetailObj->setUserId($userId);
		if (trim($data['name']) != "")
			$userDetailObj->setName(trim($data['name']));
		if ($data['dob_date']) {
			$date = date('Y-m-d', strtotime($data['dob_date']));
			$userDetailObj->setDob($date);
		}
		if (trim($data['address']) != "")
			$userDetailObj->setAddress(trim($data['address']));
		if (trim($data['email']) != "")
			$userDetailObj->setEmail(trim($data['email']));
		if (trim($data['mobileno']) != "")
			$userDetailObj->setMobileNo(trim($data['mobileno']));
		$userDetailObj->save();
		
		foreach ($data['service_type'] as $key) {
			$subMerchantObj = new SubMerchant();
			$subMerchantObj->setParentId($data['merchant']);
			$subMerchantObj->setChildId($userId);
			$subMerchantObj->setServiceId($key);
			$subMerchantObj->save();
		}
	}
	
	private function sendMailToSubMerchantUser($sfUserId,$user_name, $pass_word) {
		/* Mail firing code starts */
		$subject = "Welcome to Pay4Me";
		$partialName = 'sub_merchant_account_details';
		sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
		$login_url = url_for('@sf_guard_signin', true);
		$taskId = EpjobsContext::getInstance()->addJob(
			'SubMerchantAccountDetails',
			"email/sendAccountDetailEmail", 
				array(
					'userid' => $sfUserId,
					'password' => $pass_word,
					'subject' => $subject,
					'partialName' => $partialName,
					'login_url' => $login_url
				)
			);
		$this->logMessage("sceduled mail job with id: $taskId", 'debug');
	
		/* Mail firing code ends */
	}
	public function executeChkUserAvailability(sfWebRequest $request) {
		$username = $request->getPostParameter('username');
		//chk if username is available
		$username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
		//  $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
		if ($username_count == 0) {
			$this->result = "<div class='cGreen'>Username Available</div>";
		} else {
			$this->result = "<div class='cRed'>Username not Available</div>";
		}
	}
	public function executeMerchantServiceList(sfWebRequest $request) {
		$this->setTemplate(FALSE);
		$merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BANK);
		$str = $merchant_service_obj->getServices($request->getParameter('merchant_id'),$request->getParameter('merchant_service_id'));
		return $this->renderText($str);
	}
	private function construct_options_array($data_arrays) {
	
		$array_to_return = array();
		foreach ($data_arrays as $value) {
			$array_to_return[$value['id']] = $value['name'];
		}
		return $array_to_return;
	}
	public function checkIsSuperAdmin() {
		if ($this->getUser() && $this->getUser()->isAuthenticated()) {
			$group_name = $this->getUser()->getGroupNames();
			if (in_array(sfConfig::get('app_pfm_role_admin'), $group_name)) {
				$this->redirect('@adminhome');
			}
		}
	}
}
