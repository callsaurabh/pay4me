<b>Hello ,</b>
<br /><br />Processing of Payment with Transaction Number <?php echo $transaction_id; ?> was not successful at Bank<br/>
<br/>

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Transaction Number</b>&nbsp;</td>
        <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 60%; text-align: left;"><b>Response Code</b></td>
        <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 10%; text-align: right; padding-right: 1em;">&nbsp;&nbsp;<b>Response Message</b></td>
     </tr>
    <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php echo $transaction_id; ?></td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $response_code ; ?></td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="right"><?php echo $response_message; ?></td>

    </tr>
</table>

<br><br>
Please look out for our newsletter in your email informing you of ways to utilize your account. <br /><br />PAY4ME.....KEEPING IT SIMPLE"<br /><br />
<?php echo $signature;?>

