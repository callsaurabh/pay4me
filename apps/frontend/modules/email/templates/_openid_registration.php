<?php if($html):?>
Dear <?php echo $name ; ?>
<br /><br />
Thank you for being our Guest!
<br /><br />
We would love to have you back and avail yourself to our numerous <a href="<?php echo $merchantServiceUrl; ?>">Merchants's</a> products and services.
<br /><br />
One of the benefits of creating an account with us is to help you manage your transaction history and follow up with merchants in the case of disputes or reprinting of payment receipts.
<br /><br />
You will also get our regular Newsletters informing you of new products, services and discount offers from our <a href="<?php echo $merchantServiceUrl; ?>">Merchants</a> ..will not SPAM you.
<br /><br />
Create an account with today by clicking on the the following link 
<br />
<a href="<?php echo url_for($registration_url); ?>"><?php echo url_for($registration_url); ?></a>
<br /><br />
For Support related issues kindly contact us on +234 (1) 454 4506, +234 (1) 8418532 , +234 (1) 8447570 or via on <a href="mailto:support@pay4me.com">support@pay4me.com</a>.
<br /><br />
<span style="color:red;">
<br />
</span>
<?php echo $signature;?>
<br /><br />
<?php echo image_tag($logo,array('alt'=>'Pay4Me', 'width' => 261, 'height' => 33, 'border' => 0, 'usemap' => '#Map')); ?>

<?php else:?>

Dear <?php echo $name ; ?>
<br /><br />
Thank you for being our Guest!
<br /><br />
We would love to have you back and avail yourself to our numerous <a href="<?php echo $merchantServiceUrl; ?>">Merchants's</a> products and services.
<br /><br />
One of the benefits of creating an account with us is to help you manage your transaction history and follow up with merchants in the case of disputes or reprinting of payment receipts.
<br /><br />
You will also get our regular Newsletters informing you of new products, services and discount offers from our <a href="<?php echo $merchantServiceUrl; ?>">Merchants</a> ..will not SPAM you.
<br /><br />
Create an account with today by clicking on the the following link 
<br />
<a href="<?php echo url_for($registration_url); ?>"><?php echo url_for($registration_url); ?></a>
<br /><br />
For Support related issues kindly contact us on +234 (1) 454 4506, +234 (1) 8418532 , +234 (1) 8447570 or via on <a href="mailto:support@pay4me.com">support@pay4me.com</a>.
<br /><br />
<span style="color:red;">
<br />
</span>
<?php echo $signature;?>
<br /><br />
<?php echo image_tag($logo,array('alt'=>'Pay4Me', 'width' => 261, 'height' => 33, 'border' => 0, 'usemap' => '#Map')); ?>

<?php endif;?>