<?php $rDetails = unserialize(base64_decode($request_details)); ?>
<b>Hello <?php echo ucwords($firstname."  ".$lastname); ?>,</b>
<br /><br />Thank you for making Payment from <b>Pay4Me </b>through <?php echo $rDetails['payment_mode_option']; ?>.<br />


<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
    <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

        <tr>
            <td style="font-size: 83%;">&nbsp;&nbsp;<b>Payment date:</b>&nbsp;<?php  echo $rDetails['paid_date'];?><br>&nbsp;&nbsp;<b>Pay4Me Validation number:</b>&nbsp;<?php echo $rDetails['validation_number']; ?>
          <?php if (array_key_exists('account_number',$rDetails) && ($rDetails['payment_mode_option'] == 'eWallet') ){
  ?>  <br>&nbsp;&nbsp;<b>eWallet Account Number:</b> <?php echo $rDetails['account_number']; } ?></td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
        </tbody></table>
    </td>
  </tr>

  <tr>
    <td>
        <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Payment Status</b>&nbsp;</td>
                <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 60%; text-align: left;"><b>Application Type</b></td>
                <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 20%; text-align: left; padding-right: 1em;"><b>Total Paid Amount</b></td>
                <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 5%;">&nbsp;</td>
             </tr>
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            &nbsp;Successful</td>
                <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 60%;" align="left">
                <span style="color: black;"><?php  echo $rDetails['application_type']; ?></span>
              </td>
                <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 20%; text-align: left; padding-left: 1em;"><?php echo format_amount($rDetails['amount'],$rDetails['currencyId']); ?></td>

                <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 5%;" align="right">&nbsp;</td>
            </tr>
            <tr style="background-color: rgb(229, 236, 249);">
                <td  style="width: 70%;"></td>
                <td  style="font-weight: bold; font-size: 105%; width: 20%; text-align: right; vertical-align: text-top;"></td>
                <td  style="font-weight: bold; font-size: 105%; width: 5%; text-align: right; white-space: nowrap; padding-right: 1em;">Total: <?php echo format_amount($rDetails['amount'],$rDetails['currencyId']); ?></td>
                <td  style="font-weight: bold; font-size: 105%; width: 5%;">&nbsp;</td></tr>
        </table>
    </td>
  </tr>
   <?php if (array_key_exists('pan',$rDetails)){
  ?>
   <tr>
    <td>
    <table width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

        <tr>
            <td style="font-size: 83%;">
                    &nbsp;&nbsp;<b>Card Number:</b>&nbsp;<?php  echo $rDetails['pan'];?>
                  <br>&nbsp; <b>Approval Code:</b>&nbsp;<?php if(!empty($rDetails['approvalcode'])){ echo $rDetails['approvalcode']; }else { echo "--" ;} ?>
            </td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
        </tbody></table>
    </td>
  </tr>

<?php
  }
?>

 </table>

<br><br>
Please look out for our newsletter in your email informing you of ways to utilize your account. <br /><br />PAY4ME.....KEEPING IT SIMPLE"<br /><br />

<?php echo $signature;?>

