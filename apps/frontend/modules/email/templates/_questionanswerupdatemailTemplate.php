<b style="font-weight: bold;">Hello <?php echo $username  ; ?>,</b>
<br /><br />
Security Answers were updated to your Pay4me account.To ensure that your account information remains accurate and secure we notify you whenever this information changes.
This change request was made on <?php echo Date('F j, Y g:i a',strtotime($date))?>.
<br /><br />
Regards,<br/>
<?php echo $signature  ;?>
