
<b>Hello <?php echo ucwords($firstname."  ".$lastname); ?>,</b>
<br /><br />Thank you for using Pay4Me eWallet to eWallet transfer services. <br/>
Amount of <?php echo format_amount($amount,$currency,1); ?> has been successfully transferred to Pay4Me eWallet Account :- <b><?php echo $intDestAccount; ?></b>.<br /><br/>


<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
    <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

        <tr>
            <td style="font-size: 83%;">&nbsp;<b>Transaction Date:</b>&nbsp;<?php  echo $date; ?><br>&nbsp;<b>Transaction Code:</b>&nbsp;<?php echo $intReturnVal; ?><br>&nbsp;<b>Your eWallet Account Number:</b>&nbsp;<?php echo $intSourceAccount; ?></td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
        </tbody></table>
    </td>
  </tr>

 </table>
        <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Transfer Status</b>&nbsp;</td>
                <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 60%; text-align: left;"><b>Description</b></td>

                <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 10%; text-align: right; padding-right: 1em;">&nbsp;&nbsp;<b>Amount</b></td>


             </tr>
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"> &nbsp;Successful</td>
                <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $discription ; ?></td>
                <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="right"><?php echo format_amount($amount,$currency,1); ?></td>
               
            </tr>
            <tr style="background-color: rgb(229, 236, 249);">
                <td  style=""></td>
                <td style="font-size: 105%;  text-align: right; vertical-align: text-top;">Total:</td>
                <td style="font-size: 105%; text-align: right; white-space: nowrap; padding-right: 1em;"><?php echo format_amount($amount,$currency,1); ?></td>
          </tr>
        </table>
   



<br><br>
Please look out for our newsletter in your email informing you of ways to utilize your account. <br /><br />PAY4ME.....KEEPING IT SIMPLE"<br /><br />

<?php echo $signature;?>

