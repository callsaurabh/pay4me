<b style="font-weight: bold;">Your Pay4me Username is: <?php echo $username  ; ?></b>
<br /><br />

The password for your Pay4me Username was recently changed. You don't need to do anything, this message is simply a notification to protect the security of your account.
Your new password may take a few moments to become active. If it doesn't work on your first try, please try it again later.
<br />
<br />
<b style="font-weight: bold;">Didn't change your password?</b><br />
<div style="margin-left:10px">
1. Someone changed your password on <?php echo Date('F j, Y g:i a',strtotime($date))?>. Please sign in to your Pay4me account to confirm you still have access after the change.
<br />2. Reset your password  from any sign-in screen by clicking the Forgot Password  link.
<br />
</div>
<br/><br/>
Regards,<br/>
<?php echo $signature  ;?>
