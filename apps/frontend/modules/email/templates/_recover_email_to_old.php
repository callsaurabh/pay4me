<?php if($html):?>
Dear <?php echo $username ; ?>!
<br /><br />
Your eWallet account is recovered and the email id is updated to <b><?php echo $email ; ?></b><br /><br />
In case, the action is not requested by you, please contact <a href="mailto:support@pay4me.com">support@pay4me.com</a>
<br /><br />

<?php echo $signature;?>

<?php else:?>

Dear <?php echo $username ; ?>!
<br /><br />
Your eWallet account is recovered and the email id is updated to <b><?php echo $email ; ?></b><br /><br />
In case, the action is not requested by you, please contact <a href="mailto:support@pay4me.com">support@pay4me.com</a> <br /><br />

<?php echo $signature;?>

<?php endif;?>

