<div style="font-family: Arial,Sans-Serif; font-size: 83%; ">
    <b>Hi Support Team, </b>
    <br><br>
     We have received feedback of Pay4me. Please find below details of the same.
</div>
<br>
<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Full Name</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $name ; ?></td>
    </tr>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Email Address</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $email ; ?></td>
    </tr>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Merchant</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $merchant ; ?></td>
    </tr>
    <?php if($payment_mode_option){ ?>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Payment Mode</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $payment_mode_option ; ?></td>
    </tr>
    <?php } if($bank){ ?>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Bank</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $bank ; ?></td>
    </tr>
    <?php } if($ewallet_username){?>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">eWallet Username</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $ewallet_username ; ?></td>
    </tr>
    <?php } if($ewallet_account_number){?>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">eWallet Account Number</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $ewallet_account_number ; ?></td>
    </tr>
    <?php } ?>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Common Issue</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $common_issue ; ?></td>
    </tr>
    <?php if($common_issue == 'Others'){?>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Reason</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $reason ; ?></td>
    </tr>
    <?php } ?>
    <tr style="margin-right: 15px; margin-left: 0px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left" width="25%">Additional Comments</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><?php  echo $message ; ?></td>
    </tr>

</table>
<br><br>
<div style="font-family: Arial,Sans-Serif; font-size: 83%;">PAY4ME.....KEEPING IT SIMPLE"<br /><br />
<?php echo $signature;?> <div>

