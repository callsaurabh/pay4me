<b>Hello <?php echo ucwords($firstname . "  " . $lastname); ?>,</b>
<br /><br />Your eWallet account has been successfully recharged  on <b>Pay4Me </b> from <?php echo $bank; ?>.<br /><br />
<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td>
            <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>
                    <tr>
                        <td style="font-size: 83%;">&nbsp;<b>Recharge From <?php echo $paymentModeDisplayName; ?></b> </td>
                    </tr>
                    <tr>
                        <td style="font-size: 83%;">&nbsp;<b>Recharge Date: </b> <?php echo $recharge_date; ?><br>&nbsp;<b>Pay4Me Validation Number:</b> <?php echo $validationNumber; ?></td>
                        <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
                </tbody></table>
        </td>
    </tr>

    <tr>
        <td>
            <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
                <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                    <td style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Recharge Status</b>&nbsp;</td>
                    <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black;  text-align: left;width: 34%;"><b>eWallet Account Number</b></td>
                    <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black;  text-align: left;width: 12%;"><b>Sort Code</b></td>
                    <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black;  text-align: left;width: 12%;"><b><?php echo $paymentModeDisplayName; ?> Number</b></td>
                    <?php if($paymentModeName == sfConfig::get('app_payment_mode_option_Cheque')) { ?>
                    <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black;  text-align: left;width: 12%;"><b>Payee Account Number</b></td>
                    <?php } ?>
                    <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 10%; text-align: left;"><b>Amount</b></td>
                    <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 5%;">&nbsp;</td>
                </tr>
                <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                    <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
                        &nbsp;&nbsp;Successful</td>
                    <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); " align="left">
                        <?php echo $wallet_number; ?></td>
                    <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);  color: black;  text-align: left;">
                        <?php echo $sort_code ?></td>

                    <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); " align="left">
                        <?php echo $check_number; ?></td>
                     <?php if($paymentModeName == sfConfig::get('app_payment_mode_option_Cheque')) { ?>
                    <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); " align="left">
                        <?php echo $account_number; ?></td>
                    <?php  } ?>
                    <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 10%; text-align: left;"  align="left"><?php echo format_amount($amount, $currencyId); ?></td>

                    <td style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 5%;" align="right">&nbsp;</td>
                </tr>
                <tr style="background-color: rgb(229, 236, 249);">
                    <td ></td>
                    <td ></td>
                    <?php if($paymentModeName == sfConfig::get('app_payment_mode_option_Cheque')) { ?><td ></td> <?php } ?>
                    <td  style="font-weight: bold; font-size: 105%; width: 20%; text-align: left; vertical-align: text-top;"></td>
                    <td  style="font-weight: bold; font-size: 105%; width: 20%; text-align: left; vertical-align: text-top;"></td>
                    <td  style="font-weight: bold; font-size: 105%; width: 5%; text-align: right; white-space: nowrap; padding-right: 1em;">Total:</td>
                    <td  style="font-size: 105%;"><?php echo format_amount($amount, $currencyId); ?></td></tr>
            </table>
        </td>
    </tr>


</table>

<br><br>
Please look out for our newsletter in your email informing you of ways to utilize your account. <br /><br />PAY4ME.....KEEPING IT SIMPLE"<br /><br />

<?php echo $signature; ?>

