Your Pay4me Username is: <?php echo substr($username,0,2)."*****"  ; ?>
<br /><br />
(Username partially hidden for your privacy)
<br /><br />
You recently requested a new password to sign in to your Pay4me account. To select a new password, click on the link below:
<br /><a style="color:red;text-decoration: underline;" href="<?php echo $activation_url; ?>">Reset My Password</a>

<?php //echo link_to('Reset My Password', $activation_url, array('method' => 'head', 'class' => 'userEditInfo', 'title' => 'Reset My Password')) ;?>
<br/><br/>
This request was made on <?php echo Date('F j, Y g:i a',strtotime($date))?>.
<br/>
<br/>
For security reasons, the password reset link will expire on <?php echo Date('F j, Y g:i a',strtotime($exp_date))?> or after you reset your password.
<br/><br/>
Regards,<br/>
<?php echo $signature  ;?>
