<b>Hello <?php echo ucwords($firstname."  ".$lastname); ?>,</b>
<br /><br />
Your eWallet account has been successfully recharged on <b>Pay4Me </b>through Internet Bank.<br /><br/>

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
    <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

        <tr>
            <td style="font-size: 83%;">&nbsp;<b>Recharge Date: </b><?php echo $date; ?><br>&nbsp;<b>Pay4Me Validation Number: </b><?php echo $validationNumber; ?></td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
        </tbody></table>
    </td>
  </tr>
 </table>
<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 25%; text-align: left; padding-right: 1em;" >&nbsp;<b>Recharge Status</b>&nbsp;</td>
        <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 60%; text-align: left;"><b>eWallet Account Number</b></td>
        <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;"><b>Payment Details</b></td>
        <td  style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 10%; text-align: left; padding-right: 1em;">&nbsp;&nbsp;<b></b></td>

     </tr>
    <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 25%;" align="left">
            &nbsp;&nbsp;Successful</td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 60%;" align="left">
           <?php echo $wallet_number; ?></td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 25%; text-align: left; padding-right: 1em;">
                        Recharge Amount: </td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 25%; text-align: right; padding-right: 1em;">
       <?php echo  format_amount($amount_recharge,$currencyId,1); ?></td>

         <?php if(!empty($service_charge)) { ?>
    </tr>
    <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 25%;" align="left"></td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 60%;" align="left"></td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 25%; text-align: left; padding-right: 1em;">Service Charge: </td>
        <td  style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 25%; text-align: right; padding-right: 1em;">
               <?php echo  format_amount($service_charge,$currencyId,1); ?> </td>


    </tr> <?php } ?>

    <tr style="background-color: rgb(229, 236, 249);">
        <td  ></td>
        <td  ></td>
        <td style="font-weight: bold; font-size: 105%; width: 20%; text-align: left; vertical-align: text-top;">Total:</td>
        <td style="font-weight: bold; font-size: 105%; width: 5%; text-align: right; white-space: nowrap; padding-right: 1em;"><?php echo  format_amount($total_amount,$currencyId,1);  ?></td>
     </tr>
</table>

  <table width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

       
        </tbody></table>

<br><br>
Please look out for our newsletter in your email informing you of ways to utilize your account. <br /><br />PAY4ME.....KEEPING IT SIMPLE"<br /><br />

<?php echo $signature;?>





