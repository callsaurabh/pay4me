<?php

/**
 * email actions.
 *
 * @package     email
 * @subpackage  email
 * @author      Rajesh Yadav
 * @version
 * @Date        15-03-2011
 * @Work Packet 33 [Inhouse maintainace]
 */
class emailActions extends sfActions
{
    public $paymentModeOption = '';
    public $partialName = '';

    public $mailFrom;
    public $signature;

    
    

  /**
  * function configSettings()
  * @param N/A
  * Purpose : get username and signature for template
  */

    public function configSettings() {
        if(sfConfig::get('app_host') == "production"){
                $this->mailFrom = sfConfig::get('app_email_production_settings_username');
                $this->signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $this->mailFrom= sfConfig::get('app_email_local_settings_username');
                $this->signature = sfConfig::get('app_email_local_settings_signature');
            }
    }


 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }


  /**
  * Executes SendEwalletMail action
  *
  * @param sfRequest $request A request object
  */

   public function executeSendMail(sfWebRequest $request)
   {
       $this->configSettings();

        //set layout default as we using partial
        $this->setLayout(null);

        //getting ep job parameter
        $txnId = $request->getParameter('p4m_transaction_id');
      //  $this->paymentModeOption = $request->getParameter('payment_mode_option');

        $partialVars='';
        $partialVars = $this->getMailDataPayment($txnId);


        $mailingClass = new Mailing();
        //var $partialName using for ewalletmailTemplate
        $partialName = 'paymentmailTemplate';

        $mailingOptions = array();
        $mailInfo = 'Mail sent successfully';

        if($partialVars) {

            $mailingOptions = $partialVars['mailingOptions'];
            $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
            return $this->renderText($mailInfo);
        }
        return $this->renderText("Failure. Could not send email");

  }


 /**
  * function getMailDataPayment()
  * @param $txnId :transaction number
  * return type : partialVars Array type.
  * Purpose : getting partail variable value useed in mail template
  */
  public function getMailDataPayment($txnId){

    $transDetails = Doctrine::getTable('Transaction')->getMerchantRequestDetails($txnId);

    if ($transDetails) {
        $merchantRequestObj = $transDetails->getFirst()->getMerchantRequest();
        $payment_status_code = $merchantRequestObj->getPaymentStatusCode();
        $paidDate =  $this->format_date($merchantRequestObj->getPaidDate());

        $paid_amount = $merchantRequestObj->getPaidAmount();
       


        $validation_number = $merchantRequestObj->getValidationNumber();
        $merchant = $merchantRequestObj->getMerchant()->getName();
        $currency_id = $merchantRequestObj->getCurrencyId();
        $merchant_service_name = $merchantRequestObj->getMerchantService()->getName();
        $paid_by = $merchantRequestObj->getPaidBy();
        $paymentModeOptionId = $merchantRequestObj->getPaymentModeOptionId();
        $paymentModeObj = Doctrine::getTable('PaymentModeOption')->find($paymentModeOptionId);
        $payMode = $paymentModeObj->getName();
         //get account number
        $account_number = $this->getAccountNumber($paid_by,$currency_id);

       
        $paymentModeOption = $paymentModeObj->getPaymentMode()->getDisplayName();

        $sendArr = array('paid_date'=>$paidDate,
                        'application_type'=>$merchant_service_name,
                        'amount'=>$paid_amount,
                        'currencyId'=>$currency_id,
                        'validation_number'=>$validation_number,
                        'payment_mode_option'=>$paymentModeOption,
                        'account_number'=>$account_number);

        $arrUserDetail = $this->getUserDetail($paid_by);
        $mailingOptions['mailTo'] = $arrUserDetail['email'];




        if($payMode == sfConfig::get('app_payment_mode_option_credit_card')) {
              $orderObj = Doctrine::getTable('GatewayOrder')->getOrderDetails($txnId, "success");
              if($orderObj) {
               $sendArr['pan'] = $orderObj->getFirst()->getPan();
               $sendArr['approvalcode'] = $orderObj->getFirst()->getApprovalcode();
               

              }
            }

        $rDetails = base64_encode(serialize($sendArr));

        $partialVars = array();
        if($payment_status_code == 0) {

            $mailingOptions['mailSubject'] = 'Pay4me - Successful Payment Notification';
            $mailingOptions['mailFrom']  = array($this->mailFrom => 'Pay4Me');
            $partialVars= array('firstname'=>$arrUserDetail['name'],'lastname'=>$arrUserDetail['last_name'],'signature'=>$this->signature,'request_details'=>$rDetails,'mailingOptions'=>$mailingOptions);

        }
        return $partialVars;
    }
    return 0;
  }



 /**
  * function getAccountNumber()
  * @param $paid_by : user_id
  * return type : account number
  * Purpose : get account number by userid
  */


  public function getAccountNumber($paid_by,$currency_id) {

        
        $userObj = new UserAccountManager($paid_by);
        $account_id = $userObj->getAccountIdByCurrency($currency_id);
        // Production Issue Bug:37450
        if($account_id){
            $acctObj = new EpAccountingManager();
            $detailObj = $acctObj->getAccount($account_id);
            return $detailObj->getAccountNumber();
        }
        return false;
  }





  public function format_date($date) {
      sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
      return formatDate($date);
  }



  /**
  * Executes sendewallettransferMail action
  *
  * @param sfRequest $request A request object
  */

   public function executeSendewallettransferMail(sfWebRequest $request)
   {

        //set layout default as we using partial
        $this->setLayout(null);
        $this->configSettings();

        $mailingClass = new Mailing();
        //var $partialName using forewallettransfermmailTemplate
        $partialName = 'ewallettransfermmailTemplate';
        $partialRecieveName = 'ewalletrecievemmailTemplate';


        $mailingOptions = array();
        $mailInfo = 'Mail sent successfully';
        //getting ep job parameter
        $intReturnVal = $request->getParameter('returnVal');
      
        $partialVars='';
        $partialVars = $this->getMailDataEwalletTransfer($intReturnVal);

        if($partialVars) {
           // print "<pre>";print_r($partialVars);


            $arrSourceUserDetail = $this->getUserDetail($partialVars['sourceUserId']);
             $arrDestUserDetail = $this->getUserDetail($partialVars['destUserId']);
            

            $mailingOptions = $partialVars['mailingOptions'];
           // $arrSourceUserDetail = $partialVars['sourceUserId'];
            $mailingOptions['mailTo'] = $arrSourceUserDetail['email'];

            $partialVars['firstname']=$arrSourceUserDetail['name'];
            $partialVars['lastname']=$arrSourceUserDetail['last_name'];//print "<pre>";print_r($mailingOptions);
            $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);


            $mailingOptionsDest = $partialVars['mailingOptions'];
          //  $arrDestUserDetail = $partialVars['destUserId'];
            $mailingOptionsDest['mailTo'] = $arrDestUserDetail['email'];

            $partialVars['firstname']=$arrDestUserDetail['name'];
            $partialVars['lastname']=$arrDestUserDetail['last_name'];//print "<pre>";print_r($mailingOptionsDest);exit;
            $mailingClass->sendMail($partialRecieveName,$partialVars,$mailingOptionsDest);

           

            return $this->renderText($mailInfo);
        }
        return $this->renderText("Failure. Could not send email");

  }


  /**
  * function getMailDataEwalletTransfer()
  * @param $intAccount :destination account number
  * @param $intSourceAccoNumber : source account number
  * @param $intAmount : amount to be transfer
  * @param $intUserId : user id
  * return type : partialVars Array type.
  * Purpose : getting partail variable value useed in mail template
  */


  public function getMailDataEwalletTransfer($intReturnVal){

   


    $objEwalletTransactionDetail  = Doctrine::getTable('EwalletTransactionTrack')->findByTransactionCode($intReturnVal);
    $arrDate = $this->format_date($objEwalletTransactionDetail->getFirst()->getUpdatedAt());
    

    $ewalletRequestObj = Doctrine::getTable('EwalletTransaction')->find($objEwalletTransactionDetail->getFirst()->getAppId());
    $intAmount = $ewalletRequestObj->getAmount();
    $discription = $ewalletRequestObj->getDescription();
   // $intUserId = $ewalletRequestObj->getCreatedBy();

   $sourceAcctId = $ewalletRequestObj->getFromAccountId();
   $destAcctId = $ewalletRequestObj->getToAccountId();
    
   
    $intDestAccountNumber =$this->getAccountNumberByAccId($destAcctId);
    $intSourceAccoNumber =$this->getAccountNumberByAccId($sourceAcctId);


    $sourceAcctDetails = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($sourceAcctId);
    $sourceUserId = $sourceAcctDetails->getFirst()->getUserId();


    $destAcctDetails = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($destAcctId);
    $destUserId = $destAcctDetails->getFirst()->getUserId();




    $partialVars = array();


    $mailingOptions['mailSubject'] = 'Pay4me - Ewallet to Ewallet Transfer Notification';
    $mailingOptions['mailFrom']  = array($this->mailFrom => 'Pay4Me');

    $currency = $this->getCurrency($intSourceAccoNumber);
    $img =  $this->getCurrencyImage($currency);

   
    if($intAmount){
    $partialVars= array('signature'=>$this->signature,
                        'intDestAccount' => $intDestAccountNumber,
                        'intSourceAccount' => $intSourceAccoNumber,
                        'intReturnVal' => $intReturnVal,
                        'amount'=>$intAmount,
                        'mailingOptions'=>$mailingOptions,
                        'img'=>$img,
                        'discription'=>$discription,
                        'currency'=>$currency,
                        'date'=>$arrDate,
                        'img'=>$img,
                        'amount'=>$intAmount,
                        'sourceUserId'=>$sourceUserId,
                        'destUserId'=>$destUserId);
//print "<pre>";print_r($partialVars);exit;
     return $partialVars;
    }else{
        return 0;
    }


  }


   /**
  * function getAccountNumberByAccId()
  * @param $intSourceAccoNumber : source account number
  * return type :currencyId
  * Purpose : get currencyId by account number
  */


   public function getAccountNumberByAccId($intAccountId) {

      $objEPMaster=   Doctrine::getTable('EpMasterAccount')->find($intAccountId);
      return $objEPMaster->getAccountNumber();
    }



  /**
  * function getCurrency()
  * @param $intSourceAccoNumber : source account number
  * return type :currencyId
  * Purpose : get currencyId by account number
  */


   public function getCurrency($intSourceAccoNumber) {

       $objEPMasterAccount=    Doctrine::getTable('EpMasterAccount')->findByAccountNumber($intSourceAccoNumber);
       $intAccountId=   $objEPMasterAccount->getFirst()->getId();
       $collectionObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($intAccountId);
       return $collectionObj->getFirst()->getCurrencyId();
    }


/**
* function getCurrencyImage()
* @param $intSourceAccoNumber : source account number
* return type :currencyId
* Purpose : get currencyId by account number
*/


   public function getCurrencyImage($currency_id) {
      $currencyObj = Doctrine::getTable('CurrencyCode')->find($currency_id);
      $currency_name = strtolower($currencyObj->getCurrency());
      $currency_alt =  $currencyObj->getCurrencyCode();
             sfContext::getInstance()->getConfiguration()->loadHelpers(array('Tag'));
       sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));

      return $img = html_entity_decode(image_tag('/img/'.$currency_name.'.gif'));
      // $img = html_entity_decode(image_tag('../img/'.$currency_name.'.gif',array('alt'=>''.$currency_alt.'')));
    }





   /**
  * Executes sendValueCardRechargeMail action
  * recharge via Value Card
  * @param sfRequest $request A request object
  */


 public function executeSendValueCardRechargeMail(sfWebRequest $request) {

    $this->setLayout(null);
    $mailingClass = new Mailing();
    $this->configSettings();
    $partialName = 'valuecardrechargemaillTemplate';

    $mailingOptions = array();
    $mailInfo = 'Mail is sent successfully';

    $validationNumber = $request->getParameter('validation_number');
    $user_id = $request->getParameter('user_id');
    $partialVars='';
    $partialVars = $this->getValueCardRechargeMailData($validationNumber,$user_id);


    if($partialVars)  {

      $mailingOptions = $partialVars['mailingOptions'];
      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
      return $this->renderText($mailInfo);
    }
    return $this->renderText("Failure. Could not send email");
  }



 /**
  * Executes sendInterSwitchRechargeMail action
  * recharge via Interswitch
  * @param sfRequest $request A request object
  */


 public function executeSendInterSwitchRechargeMail(sfWebRequest $request) {

    $this->setLayout(null);
    $mailingClass = new Mailing();   
    $type = $request->getParameter('type');
    $mailingOptions = array();
    $mailInfo = 'Mail is sent successfully';
    $txnId = $request->getParameter('p4m_transaction_id');

   
    $partialName = 'interrechargemailtemplate';
    $order_id = $request->getParameter('order_id');
    $partialVars = pfmHelper::getMailDataRecharge($txnId,$order_id);
    
    if(count($partialVars))
    {
      $mailingOptions = $partialVars['mailingOptions'];
      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
      return $this->renderText($mailInfo);
    }
    return $this->renderText("Failure. Could not send email");
  }
/**
 * function getValueCardRechargeMailData
 * This function returns the data required to send a mail when recharge  is done throgh  successfully.
 * @param : $validationNumber,$intUserId
 * @author : ryadav
 */

   public function getValueCardRechargeMailData($validationNumber,$intUserId){

        $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->findByValidationNumber($validationNumber);
        $total_amount = $gatewayOrderObj->getFirst()->getAmount();
        $service_charge = $gatewayOrderObj->getFirst()->getServicechargeKobo();
        $amount_recharge = $total_amount-$service_charge;
        $rechargeDate = $this->format_date($gatewayOrderObj->getFirst()->getUpdatedAt());
        $accountId = $gatewayOrderObj->getFirst()->getAppId();
        $acctObj = new EpAccountingManager();
        $detailObj = $acctObj->getAccount($accountId);
        $wallet_number = $detailObj->getAccountNumber();
        $walletDetails = $acctObj->getWalletDetails($wallet_number);
        $collectionAccountObj = $walletDetails->getFirst()->getUserAccountCollection()->getFirst();
        $currencyObj = $collectionAccountObj->getCurrencyCode();
        $currencyId   = $currencyObj->getId();

        $pan = $gatewayOrderObj->getFirst()->getPan();
        $approvalcode = $gatewayOrderObj->getFirst()->getApprovalcode();



        $arrUserDetail = $this->getUserDetail($intUserId);
        $mailingOptions['mailTo'] = $arrUserDetail['email'];

        $partialVars = array();


        $mailingOptions['mailSubject'] = 'Pay4me - Successful Recharge Notification';


        $mailingOptions['mailFrom']  = array($this->mailFrom => 'Pay4Me');
       
        $partialVars= array('firstname'=>$arrUserDetail['name'],
                            'lastname'=>$arrUserDetail['last_name'],
                            'validationNumber'=>$validationNumber,
                            'signature'=>$this->signature,
                            'currencyId'=>$currencyId,
                            'total_amount'=>$total_amount,
                            'amount_recharge'=>$amount_recharge,
                            'service_charge'=>$service_charge,
                            'date'=>$rechargeDate,
                            'pan'=>$pan,
                            'approvalcode'=>$approvalcode,
                            'wallet_number'=>$wallet_number,
                            'mailingOptions'=>$mailingOptions);

        return $partialVars;
  }





   /**
  * Executes executeSendbankRechargeMail action
  *
  * @param sfRequest $request A request object
  */


 public function executeSendbankRechargeMail(sfWebRequest $request) {

    $this->configSettings();
    $this->setLayout(null);
    $mailingClass = new Mailing();
   
    

    $mailingOptions = array();
    $mailInfo = 'Mail is sent successfully';

    $validationNumber = $request->getParameter('validation_number');
    $user_id = $request->getParameter('user_id');
    $wallet_number = $request->getParameter('wallet_number');
    $amount = $request->getParameter('amount');
    $currency_id = $request->getParameter('currency_id');
    $bank = $request->getParameter('bank_name');
    $recharge_date = $request->getParameter('recharge_date');
    $pay_mode = $request->getParameter('pay_mode'); // Vikash , [WP055], Bug:36054 (30-10-2012)
    $partialVars='';
    $partialVars = $this->getBankRechargeMailData($validationNumber,$user_id,$wallet_number,$amount,$currency_id,$bank,$recharge_date,$pay_mode);

    if($partialVars)
    {
       $mailingOptions = $partialVars['mailingOptions'];
       $mailingClass->sendMail($partialVars['partial_name'],$partialVars,$mailingOptions);
       return $this->renderText($mailInfo);
    }
    return $this->renderText("Failure. Could not send email");

  }





/**
 * function getBankRechargeMailData()
 * This function returns the data required to send a mail when recharge  is done throgh bank successfully.
 * @param : $validationNumber,$user_id,$wallet_number,$amount,$currencyId,$branch
 * @author : ryadav
 */
   public function getBankRechargeMailData($validationNumber,$user_id,$wallet_number,$amount,$currencyId,$bank,$recharge_date,$pay_mode){


            $partialVars = array();
            $arrUserDetail = $this->getUserDetail($user_id);
            $check_number_details=$this->rechargeByCheckDetails($validationNumber);
            $recharge_date = $this->format_date($recharge_date);
            $mailingOptions['mailTo'] = $arrUserDetail['email'];
           
            if($pay_mode == 'bank_draft')
                $pay_mode = 'Draft';
            else if($pay_mode == 'check')
                $pay_mode = 'Cheque';
            
            $mailingOptions['mailSubject'] = 'Pay4me - Successful Recharge Notification';

            $mailingOptions['mailFrom']  =   array($this->mailFrom => 'Pay4Me');

            $partialVars= array('firstname'=>$arrUserDetail['name'],
                                'lastname'=>$arrUserDetail['last_name'],
                                'validationNumber'=>$validationNumber,
                                'signature'=>$this->signature,
                                'currencyId'=>$currencyId,
                                'amount'=>$amount,
                                'wallet_number'=>$wallet_number,
                                'bank'=>$bank,
                                'recharge_date'=>$recharge_date,
                                'pay_mode'=>$pay_mode,
                                'mailingOptions'=>$mailingOptions);
           
            $partialVars=$check_number_details+$partialVars;
            

        return $partialVars;
  }


  
/**
 * function rechargeByCheckDetails()
 * This function returns the data required to send a mail when recharge  is done throgh check successfully.
 * @param : $validation_no,$storingArray
 * @author : amalhotra
 */
    public function rechargeByCheckDetails($validation_no) {

        $epMastLedger_array = Doctrine::getTable('EpMasterLedger')->getEwalletRechargeAcctDetailsRecordFromValidationNumber($validation_no);
        if (count($epMastLedger_array['EwalletRechargeAcctDetailsRecord']) != 0) {
            $paymentMode = $epMastLedger_array['EwalletRechargeAcctDetailsRecord']['0']['payment_mode_option_id'];
            $paymentModeObj = new pfmHelper();
            $paymentModeDisplayName = $paymentModeObj->getPMONameByPMId($paymentMode);
            $paymentModeName = $paymentModeObj->getPMONameByPMOId($paymentMode);
        }
        if ($paymentModeName == sfConfig::get('app_payment_mode_option_Cheque') || $paymentModeName == sfConfig::get('app_payment_mode_option_bank_draft')) {
            $param = array('partial_name' => 'checkrechargemailTemplate', 'paymentModeName' => $paymentModeName, 'paymentModeDisplayName' => $paymentModeDisplayName, 'check_number' => $epMastLedger_array['EwalletRechargeAcctDetailsRecord'][0]['check_number'], 'sort_code' => $epMastLedger_array['EwalletRechargeAcctDetailsRecord'][0]['sort_code']);
            if ($paymentModeName == sfConfig::get('app_payment_mode_option_Cheque')) {
                $param['account_number'] = $epMastLedger_array['EwalletRechargeAcctDetailsRecord'][0]['account_number'];
            }
            return $param;
        } else {
            return array('partial_name' => 'bankrechargemaillTemplate');
        }
    }
     /**
  * function getUserDetail()
  * @param $user_id : User Id
  * return type :array user detail
  * Purpose : get currencyId by account number
  * Author : ryadav
  */
   public function getUserDetail($user_id) {

        $userDetail =array();
        $userDetail=Doctrine::getTable('UserDetail')->getDetailsUser($user_id);
        return $userDetail[0];

    }

 /**
  * Executes sendEtranzactRechargeMail action
  * recharge via Etranzact
  * @param sfRequest $request A request object
  */


 public function executeSendEtranzactRechargeMail(sfWebRequest $request) {

    $this->setLayout(null);
    $mailingClass = new Mailing();    
    $mailingOptions = array();
    $mailInfo = 'Mail is sent successfully';
    $txnId = $request->getParameter('p4m_transaction_id');


    $partialName = 'etranzactrechargemailtemplate';
    $order_id = $request->getParameter('order_id');
    $partialVars = pfmHelper::getMailDataRecharge($txnId,$order_id);

    if(count($partialVars))
    {
      $mailingOptions = $partialVars['mailingOptions'];
      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
      return $this->renderText($mailInfo);
    }
    return $this->renderText("Failure. Could not send email");
  }

  /**
  * Executes SendMailforBankResponse action
  * recharge via MiddlewareListener
  * @param sfRequest $request A request object
  */
 
 public function executeSendMailforBankResponse(sfWebRequest $request) {

    $this->setLayout(null);
    $mailingClass = new Mailing();
    $mailingOptions = array();
    $mailInfo = 'Mail is sent successfully';

    $signature = '';
    //$mailFrom = '';
    $this->configSettings();

    //response Code
    $response_code = $request->getParameter('response_code');
    //Pay4me Transaction Id
    $transaction_id = $request->getParameter('transaction_id');
    //response message
    $response_message = $request->getParameter('response_message');
    $partialName = 'bankresponsefailuremailTemplate';
    //mailTo mailFrom
    $email = array(sfConfig::get('app_bankresponsefailure_ccv1'),sfConfig::get('app_bankresponsefailure_ccv2'));
    $mailingOptions['mailSubject'] = 'Pay4me - Payment not processed at bank';
    $mailingOptions['mailTo'] = $email;
    $mailingOptions['mailFrom'] = array($this->mailFrom => 'Pay4Me');
    $partialVars = array('mailingOptions'=>$mailingOptions,'response_code'=>$response_code,
    'transaction_id'=>$transaction_id,'signature'=>$signature,'response_message'=>$response_message);

    if(count($partialVars))
    {
      $mailingOptions = $partialVars['mailingOptions'];
      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
      return $this->renderText($mailInfo);
    }
    return $this->renderText("Failure. Could not send email");
  }

  public function executeAddFailureJob(sfWebRequest $request) {
      //print_r($request->getParameterHolder()->getAll());exit;
      $this->setLayout(null);
      $taskUrl = $request->getParameter('taskUrl');
      $taskName = $request->getParameter('taskName');
      $transaction_id = $request->getParameter('transaction_id');
      $response_code = $request->getParameter('response_code');
      $response_message = $request->getParameter('response_message');
      $taskId = EpjobsContext::getInstance()->addJob($taskName,$taskUrl,array('transaction_id' => $transaction_id, 'response_code' => $response_code, 'response_message' => $response_message));
      sfContext::getInstance()->getLogger()->err("sceduled job with id: $taskId");
      return $this->renderText("Scheduled Failure notification with Id".$taskId);
  }




  /**
  * Executes SendActiveMail
  *
  * @param sfRequest $request A request object
  */

   public function executeMessagePosterFailure(sfWebRequest $request)
   {
       // $success = 0;
        $this->configSettings();
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $mailingOptions = array();
        //failure Reason
        $failure_reason = $request->getParameter('failureReason');

        $partialName = 'activemqdownmailTemplate';
        //mailTo mailFrom
        $email = sfConfig::get('app_activemqresponsefailure_cc1');
      
        $mailingOptions['mailSubject'] = 'Pay4me - ActiveMQ Failure Notification';
       $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = array($this->mailFrom => 'Pay4Me');
      
        $partialVars = array('mailingOptions'=>$mailingOptions,'failure_reason'=>$failure_reason,
        'signature'=>$this->signature);
      
      
        if(count($partialVars))
        {
          $mailingOptions = $partialVars['mailingOptions'];          
          if($mailingClass->sendMail($partialName,$partialVars,$mailingOptions)) {              
              return $this->renderText('Mail sent successfully');
           }
        }
        EpjobsContext::getInstance()->setTaskStatusFailed();
        return $this->renderText("Failure. Could not send email");
  }


  /*
   *
   * Execute mail for change password/forgot password
   *
   */

  public function executeChangePasswordMail(sfWebRequest $request){
    $this->configSettings();
    $this->setLayout(null);
    $mailingClass = new Mailing();
    $mailingOptions = array();

    
    //get user name
    $username = $request->getParameter('username');
    $date = $request->getParameter('date');
    $exp_date =  $request->getParameter('expiry');
    $activation_url = $request->getParameter('activation_url');
    $userId = $request->getParameter('userid');
  //  $userId= 3265;
    //get user mail to
     $arrDetails = Doctrine::getTable('UserDetail')->findByUserId($userId);
     $email = $arrDetails->getFirst()->getEmail();

    //get forgot password details
    $mailingOptions['mailSubject'] = $request->getParameter('subject');
    $mailingOptions['mailTo'] = $email;
    $mailingOptions['mailFrom'] = array($this->mailFrom => 'Pay4Me');

    $partialVars = array('mailingOptions'=>$mailingOptions,
    'signature'=>$this->signature,'username'=>$username,'date'=>$date,
    'exp_date'=>$exp_date,'activation_url'=>$activation_url);
    $partialName = 'resetpasswordmailTemplate';

    if(count($partialVars))
    {
      $mailingOptions = $partialVars['mailingOptions'];
      if($mailingClass->sendMail($partialName,$partialVars,$mailingOptions)) {
          return $this->renderText('Mail sent successfully');
       }
    }
    EpjobsContext::getInstance()->setTaskStatusFailed();
    return $this->renderText("Failure. Could not send email");


  }

  /*
   *
   * Execute password confirmation
   *
   */

  public function executeChangePasswordConfirmation(sfWebRequest $request){
    $this->configSettings();
    $this->setLayout(null);
    $mailingClass = new Mailing();
    $mailingOptions = array();

    $userId = $request->getParameter('userid');
    //get user name
    $username = $request->getParameter('username');
    $date = $request->getParameter('date');
    

     $arrDetails = Doctrine::getTable('UserDetail')->findByUserId($userId);
     $email = $arrDetails->getFirst()->getEmail();

    //get forgot password details
    $mailingOptions['mailSubject'] = $request->getParameter('subject');
    $mailingOptions['mailTo'] = $email;
    $mailingOptions['mailFrom'] = array($this->mailFrom => 'Pay4Me');

    $partialVars = array('mailingOptions'=>$mailingOptions,
    'signature'=>$this->signature,'username'=>$username,'date'=>$date);
    $partialName = 'passwordchangedmailTemplate';

    if(count($partialVars))
    {
      $mailingOptions = $partialVars['mailingOptions'];
      if($mailingClass->sendMail($partialName,$partialVars,$mailingOptions)) {
          return $this->renderText('Mail sent successfully');
       }
    }
    EpjobsContext::getInstance()->setTaskStatusFailed();
    return $this->renderText("Failure. Could not send email");
  }
  /*
   *
   * Execute Security question answer update
   *
   */

  public function executeSecurityquestionanswerupdate(sfWebRequest $request){
    $this->configSettings();
    $this->setLayout(null);
    $mailingClass = new Mailing();
    $mailingOptions = array();
    $userId = $request->getParameter('userid');
    //get user name
    $username = $request->getParameter('username');
    $date = $request->getParameter('date');
    $arrDetails = Doctrine::getTable('UserDetail')->findByUserId($userId);
    $email = $arrDetails->getFirst()->getEmail();
    //get forgot password details
    $mailingOptions['mailSubject'] = $request->getParameter('subject');
    $mailingOptions['mailTo'] = $email;
    $mailingOptions['mailFrom'] = array($this->mailFrom => 'Pay4Me');
    $partialVars = array('mailingOptions'=>$mailingOptions,
    'signature'=>$this->signature,'username'=>$username,'date'=>$date);
    $partialName = 'questionanswerupdatemailTemplate';
    if(count($partialVars)){
      $mailingOptions = $partialVars['mailingOptions'];
      if($mailingClass->sendMail($partialName,$partialVars,$mailingOptions)) {
          return $this->renderText('Mail sent successfully');
       }
    }
    EpjobsContext::getInstance()->setTaskStatusFailed();
    return $this->renderText("Failure. Could not send email");
  }

  public function executeSendNibssRechargeMail(sfWebRequest $request) {

    $this->setLayout(null);
    $mailingClass = new Mailing();
    $this->configSettings();
    $partialName = 'nibssrechargemaillTemplate';

    $mailingOptions = array();
    $mailInfo = 'Mail is sent successfully';

    $validationNumber = $request->getParameter('validation_number');
    $partialVars='';
    $partialVars = $this->getNibssRechargeMailData($validationNumber);


    if($partialVars)  {

      $mailingOptions = $partialVars['mailingOptions'];
      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
      return $this->renderText($mailInfo);
    }
    return $this->renderText("Failure. Could not send email");
  }


  public function getNibssRechargeMailData($validationNumber){

        $gatewayOrderObj = Doctrine::getTable('GatewayOrder')->findByValidationNumber($validationNumber);
        $total_amount = $gatewayOrderObj->getFirst()->getAmount();
        $service_charge = $gatewayOrderObj->getFirst()->getServicechargeKobo();
        $amount_recharge = $total_amount-$service_charge;
        $rechargeDate = $this->format_date($gatewayOrderObj->getFirst()->getUpdatedAt());
        $accountId = $gatewayOrderObj->getFirst()->getAppId();
        $acctObj = new EpAccountingManager();
        $detailObj = $acctObj->getAccount($accountId);
        $wallet_number = $detailObj->getAccountNumber();
        $walletDetails = $acctObj->getWalletDetails($wallet_number);
        $collectionAccountObj = $walletDetails->getFirst()->getUserAccountCollection()->getFirst();
        $currencyObj = $collectionAccountObj->getCurrencyCode();
        $currencyId   = $currencyObj->getId();

        $billObj = billServiceFactory::getService();
        $ewalletDetails = $billObj->getUserDetail($collectionAccountObj->getMasterAccountId());
        $mailingOptions['mailTo'] = $ewalletDetails->getFirst()->getEmail();
        $partialVars = array();


        $mailingOptions['mailSubject'] = 'Pay4me - Successful Recharge Notification';


        $mailingOptions['mailFrom']  = array($this->mailFrom => 'Pay4Me');

        $partialVars= array('firstname'=>$ewalletDetails->getFirst()->getName(),
                            'lastname'=>$ewalletDetails->getFirst()->getLastName(),
                            'validationNumber'=>$validationNumber,
                            'signature'=>$this->signature,
                            'currencyId'=>$currencyId,
                            'total_amount'=>$total_amount,
                            'amount_recharge'=>$amount_recharge,
                            'service_charge'=>$service_charge,
                            'date'=>$rechargeDate,
                            'wallet_number'=>$wallet_number,
                            'mailingOptions'=>$mailingOptions);

        return $partialVars;
  }

  /*
   *Mail for Feedback form filled by End User
   */
  public function executeSendEmailFeedback(sfWebRequest $request) {
        $this->setLayout(null);
        $emailData = array();
        $p4mehelperObj = new pfmHelper();
        $emailData['feedback_id'] = $request->getParameter('feedback_id');
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $emailData = $payForMeObj->getFeedbackDetails($emailData['feedback_id']);


        $merchantName = $payForMeObj->getMerchantName($emailData['merchant_id']);
        if($emailData['bank_id'])
            $bankName = $payForMeObj->getBankName($emailData['bank_id']);
        else
            $bankName = '';
        if($emailData['payment_mode_option_id'])
            $paymentName = $p4mehelperObj->getPMONameByPMId($emailData['payment_mode_option_id']);
        else
            $paymentName = '';

        $emailData['merchant'] = $merchantName;
        $emailData['payment_mode_option'] = $paymentName;
        $emailData['bank'] = $bankName;
        $subject = $merchant.' '.$payment_mode_option.' '.$common_issue;
        $partialName = "feedback_email";
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendConfirmationEmailFeedback($emailData, $subject, $partialName);
        return $this->renderText($mailInfo);
  }

     /*
      * Mail for associating eWallet account with openId
      */
    public function executeSendAssociateConfirmationEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $assocEmail = $request->getParameter('assocEmail');
        $openIdType = $request->getParameter('openIdType');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendAssociationEmail($userid, $subject, $partialName,$assocEmail,$openIdType);
        return $this->renderText($mailInfo);
    }

    /*
      * Mail for recovering eWallet account with new openId
      */
    public function executeSendRecoverEwalletAccountEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $prevEmail = $request->getParameter('prevEmail');
        $openIdType = $request->getParameter('openIdType');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendRecoverEmail($userid, $subject, $partialName,$prevEmail,$openIdType);
        return $this->renderText($mailInfo);
    }
/*
 * Mail for confirming as pay4me user
 */
    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendConfirmationEmailToOpenIdpay4me($userid, $subject, $partialName);
        return $this->renderText($mailInfo);
    }
    /*
     * Mail for guest user to continue registration on pay4me.
    */
    public function executeSendRegistrationEmail(sfWebRequest $request) {
    	$this->setLayout(null);
    	$userid = $request->getParameter('userid');
    	$subject = $request->getParameter('subject');
    	$partialName = $request->getParameter('partialName');
    	$registration_url = $request->getParameter('registration_url');
    	$baseUrl = sfContext::getInstance()->getRequest()->getUriPrefix();
    	$merchant_service_url = $baseUrl . '/page/merchantService';
    	$logo = $baseUrl . '/img/new/logo-pay4me.gif';
    	$vars = array(
    		'registrationUrl' => $registration_url,
    		'merchantServiceUrl' => $merchant_service_url,
    		'logo' => $logo
    	);
    	$sendMailObj = new Mailing();
    	$mailInfo = $sendMailObj->sendConfirmationEmailToOpenIdpay4me($userid, $subject, $partialName,$vars);
    	return $this->renderText($mailInfo);
    }
    
   /*
 * Mail for deactivating eWallet account
 */
    public function executeSendDeactivateAccountMail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
         
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendDeactivateAccountEmail($userid, $subject, $partialName);
        return $this->renderText($mailInfo);
    }

    /*
 * Mail for sending security code for reactivating eWallet account
 */
    public function executeSendReactivateSecurityMail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $securityCode = $request->getParameter('securityCode');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendReactivateSecurityEmail($userid, $subject, $partialName,$securityCode);
        return $this->renderText($mailInfo);
    }
   /*
 * Mail for activating eWallet account
 */
    public function executeSendActivateAccountMail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');

        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendActivateAccountEmail($userid, $subject, $partialName);
        return $this->renderText($mailInfo);
    }

/*
 * mail to old mail id in case of Recover Account
 */
    public function executeSendRecoverEmailToOld(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $email = $request->getParameter('email');
        $preEmail = $request->getParameter('preEmail');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');

        $sendMailObj = new Mailing() ;
        $mailInfo = $sendMailObj->sendUpdatedEmailNotification($userid, $email, $preEmail, $subject,  $partialName);
        return $this->renderText($mailInfo);
    }

    /**
     * Function for sending email account login details to sub merchant type user.
     * @param sfWebRequest $request
     */
	public function executeSendAccountDetailEmail(sfWebRequest $request){
		$this->setLayout(null);
		$subject = $request->getParameter('subject');
		$partialName = $request->getParameter('partialName');
		$userid = $request->getParameter('userid');
		$pass_word = $request->getParameter('password');
		$url = $request->getParameter('login_url');
		$vars = array(
			'signature'=>$this->signature,
			'userid'=>$userid,
			'password'=>$pass_word,
			'login_url'=>$url
		);
		$sendMailObj = new Mailing();
		$mailInfo = $sendMailObj->sendAccountDetailEmailSubMerchant($userid, $subject, $partialName, $vars);
		return $this->renderText($mailInfo);
	}
}
