<?php

/**
 * mobilereports actions.
 *
 * @package    pay4me
 * @subpackage monitoringTool WP034
 * @author     Ashutosh Srivastava
 * @version    SVN: $Id: actions.class.php  2011-04-27 10:41:27Z  $
 */
class monitoringToolActions extends sfActions {

//  public function executeMonitorCronJob(sfWebRequest $request) {//
//
//  }
//  public function executeMonitorMemorySpace(sfWebRequest $request) {//Enough Memory space to run project
//
//  }

/**
 * Check payment and recharge amount is not going in wrong account(ewallet or etc).
 *
 * @param none
 * @return Void
 */
  public function executeMonitorAccountAssociation(sfWebRequest $request) {$this->setLayout(null);
      $monitoringToolObj = new monitoringToolManager();
      $monitoringToolObj->setReturnType('Bool');
      try{
                $verifierArr = array();
             // all account which are in SplitAccountConfiguration must be recieving
               $isNotVerified  = $monitoringToolObj->verifySplitAccountConfiguration();
               if($isNotVerified)
                   $verifierArr[] = $monitoringToolObj->verifierFunction;

             // all account which are in ServiceBankConfiguration must be collection
               $isNotVerified  = $monitoringToolObj->verifyServiceBankConfiguration();
               if($isNotVerified)
                   $verifierArr[] = $monitoringToolObj->verifierFunction;

              // all account which are in UserAccountConfiguration must be ewallet
               $isNotVerified  = $monitoringToolObj->verifyUserAccountCollection();
               if($isNotVerified)
                   $verifierArr[] = $monitoringToolObj->verifierFunction;

              // all account which are in FinacialIsnutionAcctConfiguration must be recieving
               $isNotVerified  = $monitoringToolObj->verifyFinancialInstutionAcctConfiguration();
               if($isNotVerified)
                   $verifierArr[] = $monitoringToolObj->verifierFunction;

              // all account which are in VirtualAccountConfig must be recieving
               $isNotVerified  = $monitoringToolObj->verifyVirtualAccountConfig();
               if($isNotVerified)
                   $verifierArr[] = $monitoringToolObj->verifierFunction;

              foreach($verifierArr  as $value)
                {
                     $valArr = array('functionName'=>$value);
                     $this->processStatus($valArr);
                }

          return $this->renderText("Monitor Account Association");
      }
      catch(Exeception $e)
      {
                 throw $e;
      }

  }
/**
 * Sum of all distributed amount should be as defined as in split rules
 *
 * @param none
 * @return Void
 */
  public function executeMonitorSplitingOfAmount(sfWebRequest $request) {
    try{  //
      // For Payment
         // Browse the data from the the MerchantPaymentTransaction where is_processed ==1 by last day
            $trans_date = date('Y-m-d H:i:s',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 23:59:59"));
            $from_date = date('Y-m-d H:i:s',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 00:00:00"));
            $status = 1;
            $MPaymentTransactionArr =  Doctrine::getTable('MerchantPaymentTransaction')->findAllReqByStatus($trans_date,$status,$from_date);
            if($MPaymentTransactionArr)
            {
             // randomly get a validation number from  records
             //   $randNum = array_rand($MPaymentTransactionArr);
                $randNum=0;
                $validationNumber = $MPaymentTransactionArr[$randNum]['validation_number'];
                $jobParamArr = array('functionName'=>'verifyMonitorSplitingOfAmount',
                                      'validationNumber'=>$validationNumber);

                $merchantRequestId = $MPaymentTransactionArr[$randNum]['merchant_request_unique_id'];
                $merchantRequestObj = Doctrine::getTable('MerchantRequest')->find($merchantRequestId);
                $merchantServiceId = $merchantRequestObj->getMerchantServiceId();
                $currenyId = $merchantRequestObj->getCurrencyId();
             // get the records form EpMasterLedger by validation number
                $ledgerObj =  Doctrine::getTable('EpMasterLedger')->findByTransactionNumber($validationNumber);
             // sum of record should be equal to debit and credit for same account  ## cannot implmented on all transaction ##
             // get the records from the SplitEntityConfiguration/SplitAccountConfiguration and get masterAccountId
                $splitArr =  Doctrine::getTable('SplitEntityConfiguration')->getSplitInfo($merchantServiceId, $currenyId);

             // check the amount is going in that masterAccount
                  foreach($splitArr as $splitRow)
                  {
                             $charge = $splitRow['charge'];
                        if($splitRow['ACCT_CONF_ID']>0)
                            {
                                if($splitRow['SplitType']['split_name']=='Flat')
                                {
                                    $actId = $splitRow['SplitAccountConfiguration']['master_account_id'];
                                    $charge = $splitRow['charge'];
                                    $isValidResult = $this->isValidSplitInAccount($ledgerObj,$actId,$charge,$jobParamArr);
                                    $isValidCountinLedger  = $this->isValidCountinLedger($ledgerObj,$actId,$jobParamArr);
                                    if(!$isValidResult || !$isValidCountinLedger)
                                      {
                                           return $this->renderText("Error in Monitor Spliting Amount with Validation Number: $validationNumber");
                                      }
                                }//if for Flat

                             if($splitRow['SplitType']['split_name']=='Percentage')
                                {
                                    $actId = $splitRow['SplitAccountConfiguration']['master_account_id'];
                                    $amount = $splitRow['charge'];
                                    $item_fee = $merchantRequestObj->getItemFee();
                                    $amount_per = ($charge*$item_fee)/100;
                                    $isValidResult = $this->isValidSplitInAccount($ledgerObj,$actId,$amount_per,$jobParamArr);
                                    $isValidCountinLedger  = $this->isValidCountinLedger($ledgerObj,$actId,$jobParamArr);
                                    if(!$isValidResult || !$isValidCountinLedger)
                                      {
                                           return $this->renderText("Error in Monitor Spliting Amount with Validation Number: $validationNumber");
                                      }
                                }//if for Percentage
                             if($splitRow['SplitType']['split_name']=='Pay4Me')
                                {
                                    $actId = $splitRow['SplitAccountConfiguration']['master_account_id'];
                                    $pay4meFee = $merchantRequestObj->getPayformeAmount();
                                    $isValidResult = $this->isValidSplitInAccount($ledgerObj,$actId,$pay4meFee,$jobParamArr);
                                    if(!$isValidResult)
                                      {
                                           return $this->renderText("Error in Monitor Spliting Amount with Validation Number: $validationNumber");
                                      }
                                }//if for Percentage
                            }
                         else
                            {
                                if($splitRow['SplitType']['split_name']=='Financial Institution' && 1==$merchantRequestObj->getPaymentModeOptionId())
                                {
                                    $fi_id = $merchantRequestObj->getBankId();
                                    $fi_amount = $merchantRequestObj->getBankAmount();
                                    $merchant_id = $merchantRequestObj->getMerchantId();
                                    $currency = $merchantRequestObj->getCurrencyId();
                                    $accountObj = Doctrine::getTable('FinancialInstitutionAcctConfig')->getMasterAccount($fi_id,$merchant_id,$currency);
                                    if($accountObj)
                                      {
                                        $finAccountId = $accountObj->getFirst()->getAccountId();
                                        $isValidResult = $this->isValidSplitInAccount($ledgerObj,$finAccountId,$merchantRequestObj->getBankAmount(),$jobParamArr);
                                        if(!$accountObj)
                                        {
                                           return $this->renderText("Error in Monitor Spliting Amount with Validation Number: $validationNumber");
                                        }
                                      }
                                    else
                                    {
                                        $this->processStatus($jobParamArr);
                                    }


                                } // if of 'Financial Institution'
                            } // else of ACCT_CONF_ID
                  }//foreach of Split row

              return $this->renderText("Monitor Spliting Amount");
         }///
         return $this->renderText("Monitor Spliting Amount : No Record Found");
    }
    catch(Exeception $e)
      {
         throw $e;
      }

  }
/**
 * Check in one entry of ledger and split enntry
 *
 * @param $ledgerObj: EpMasterLedger Obj, $accountId: MasterAccountId ,$amount: amount ,$jobParamArr : EpJob Parameter
 * @return bool
 */

  private function isValidSplitInAccount($ledgerObj,$accountId,$amount,$jobParamArr)
  {
      // convert in kobo

      $amount = $amount * 100;
      $amount = number_format($amount, 2, '.', '');
      foreach($ledgerObj as $ledgerRow)
        {
            if($ledgerRow->getMasterAccountId()==$accountId && 'credit' == $ledgerRow->getEntryType())
            {
              if($ledgerRow->getAmount()!=$amount)
              {
                  $this->processStatus($jobParamArr);
                  return false;

              }
            } // check account
       } //foreach of ledgerObj
       return true;
  }
/**
 * count all ledger enetry in for one account
 *
 * @param $ledgerObj: EpMasterLedger Obj, $accountId: MasterAccountId ,$jobParamArr : EpJob Parameter
 * @return bool
 */
 private function isValidCountinLedger($ledgerObj,$accountId,$jobParamArr)
  {
      $count = 0;
      foreach($ledgerObj as $ledgerRow)
        {
            if($ledgerRow->getMasterAccountId()==$accountId && 'credit' == $ledgerRow->getEntryType())
            {
              ++$count;
            }
       } //foreach of ledgerObj
       if($count!=1)
        {
            $this->processStatus($jobParamArr);
            return false;
        }
       return true;
  }
 /**
 * no error/waring will found in the log
 *
 * @param none
 * @return Void
 */
  public function executeMonitorLog(sfWebRequest $request) {
      $logPath = sfConfig::get('sf_log_dir');
      $trans_date = date('M d',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 23:59:59"));
      $cmd = 'tail '.$logPath.'/frontend_prod.log | grep "'.$trans_date.'" | grep "err"';
      exec($cmd, $output, $return_val);

      if(!(is_Array($output) && count($output)==0))
        {
            $jobParamArr = array('functionName'=>'verifyMonitorLog',
                                    'ErrorLog'=>serialize($output));

            $this->processStatus($jobParamArr);
        }
      return $this->renderText("Monitor PROD LOG");

  }
 /**
 * //Search ledger by last day for amount greater than the defined amount then send the email
 *
 * @param none
 * @return Void
 */
  public function executeMonitorMassiveTransaction(sfWebRequest $request) {

   try{  //
      $upperLimit = Settings::getMonitorLedgerPerTxnLimit();
      $transDate = date('Y-m-d',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 23:59:59"));
      $ledgerObj =  Doctrine::getTable('EpMasterLedger')->getOverLimitTransaction($upperLimit, $transDate);
      if($ledgerObj)
      {
            $jobParamArr = array('functionName'=>'verifyMonitorMassiveTransaction',
                                  'EpMasterLedgerId'=>$ledgerObj->getfirst()->getId());

            $this->processStatus($jobParamArr);
      }
       return $this->renderText("Monitor Transaction Limit");

    }
    catch(Exeception $e)
      {
         throw $e;
      }

  }
/**
 *mail and notification are working poperly
 *
 * @param none
 * @return Void
 */
  public function executeMonitorMailAndNotification(sfWebRequest $request) {//mail and notification are working poperly
    // PaymentNofication and MailNofication has been passed if not send email
     // check for PaymentNofication
//     $jobName = 'PaymentNofication';
     $timeInMin = Settings::getMonitorBeforeTimepayForNotificationAndMail();
     $lastJobTime  = date("Y-m-d H:i:s", strtotime('-'.$timeInMin.' minutes'));
     $jobQueueObj =  Doctrine::getTable('EpJobQueue')->getJobByName($jobName,$lastJobTime);
     $pramVar = '';
     foreach($jobQueueObj as $job)
     {
         $pramVar .= $pramVar==''?'':',';
         $pramVar .= $job->getJobId();
     }
     if($pramVar!='')
     {
        $jobParamArr = array('functionName'=>'verifyMonitorPaymentNotification',
                              'JobName'=> $jobName,
                              'EpJobIds'=>$pramVar);

        $this->processStatus($jobParamArr);
     }
      // check for mail
     $jobName = 'MailNotification';
     $lastJobTime  = date("Y-m-d H:i:s", strtotime('-'.$timeInMin.' minutes'));
     $jobQueueObj =  Doctrine::getTable('EpJobQueue')->getJobByName($jobName,$lastJobTime);
     $count=1;
     $subBody='';
     foreach($jobQueueObj as $job)
     {
         $subBody .='<br>'.$count.': '.$job->getJobId();
         $count++;
     }
     $jobName = 'PaymentEmailNotification';
     $jobQueueObj =  Doctrine::getTable('EpJobQueue')->getJobByName($jobName,$lastJobTime);
     foreach($jobQueueObj as $job)
     {
         $subBody .='<br>'.$count.': '.$job->getJobId();
         $count++;
     }
     if($subBody!='')
     {
             $MAIL_BODY= '<center><b>This is monitoring mail.</b></center>
                          <br>
                            At the time of process monitoring, system diagnosed following issue: <br>
                            Problem with exeuting "Mail Notification" job with following Job Ids </b>';
          $headers  = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
           // This two steps to help avoid spam
          $headers .= "Message-ID: <".time()." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
          $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
          $MAIL_BODY .= $subBody;
          $mailTo = Settings::getMonitorMailTo();
          $subject = "Pay4Me Monitor: Mail Notification NOT SEND";
          if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
          $headers .= "From: $signature <$mailFrom>" . "\r\n";
          foreach($mailTo as $mailVal)
          {
           $success = mail($mailVal,$subject,$MAIL_BODY,$headers);
          }
     }
      return $this->renderText("Monitor Mail and Notification");
  }
/**
 * Monitor Ewallet account
 *
 * @param none
 * @return Void
 */
  public function executeMonitorAccountBalanceOfEwallet(sfWebRequest $request) {//negative balance account
    // get account from EpMasterAccount where type is ewallet and amount not bounded from both side
    $negativeAccountArr =  Doctrine::getTable('EpMasterAccount')->getEWalletForNegativeBal();
    if(count($negativeAccountArr)>0)
    {
        $pramVar='';
        foreach($negativeAccountArr as $accountRow)
        {

            $pramVar .= $pramVar==''?'':',';
            $pramVar .= $accountRow['id'];
        }
        $jobParamArr = array('functionName'=>'verifyMonitorAccountBalaceOfEwallet',
                              'EpMasterAccountIds'=>$pramVar);

        $this->processStatus($jobParamArr);

    }

    // All debit cannot be greater than all credit of ewallet account
    $masterLedgerArr =  Doctrine::getTable('EpMasterLedger')->getDebitAndCreditGroupRecord();
    if($masterLedgerArr)
    {
        $pramVar='';
        foreach($masterLedgerArr as $ledgerRow)
        {

            $pramVar .= $pramVar==''?'':',';
            $pramVar .= $ledgerRow['master_account_id'];
        }
        $jobParamArr = array('functionName'=>'verifyMonitorAccountBalaceOfEwallet',
                              'EpMasterAccountIds'=>$pramVar);

        $this->processStatus($jobParamArr);

    }
    return $this->renderText("Monitor Ewallet Account Limit");
  }

 /**
   * Perform primitve action
   * @param $reponse: array
   * @return Void
  */
  private function processStatus($paramArr) {

      $paramArr['partialName'] = 'monitor_email' ;
      $taskId = EpjobsContext::getInstance()->addJob('SendMonitorMail',$this->moduleName."/sendMonitorMail", $paramArr);
      $this->logMessage("sceduled mail job with id: $taskId", 'debug');
      //$this->sendMonitorMail($response[0], $response[1]);
   }

 /**
   * Send the Monitor mail to take primitve action
   * @param $subject: string $body: text $toMail: array()
   * @return Void
  */
  public function executeSendMonitorMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailTo = Settings::getMonitorMailTo();

//        $functionName = 'verify'.$request->getParameter('functionName');
        $functionName = $request->getParameter('functionName');

        $paramArray = $request->getParameterHolder()->getAll();
        $monitoringToolObj = new monitoringToolManager();
        $response = $monitoringToolObj->$functionName($paramArray);
        $subject = $response[0]; //$request->getParameter('functionName');
        $partialName = $request->getParameter('partialName');
        $mailBody = $response[1]; //$request->getParameter('mailBody');
        $sendMailObj = new Mailing() ;
        $mailInfo = $sendMailObj->sendMonitorEmail($mailTo,$subject,$mailBody,$partialName);
        return $this->renderText($mailInfo);
    // get account from EpMasterAccount where type is ewallet and amount not bounded from both side
  }
}