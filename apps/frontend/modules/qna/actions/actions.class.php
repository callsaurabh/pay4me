<?php

/**
 * qna actions.
 *
 * @package    mysfp
 * @subpackage qna
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class qnaActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $this->security_questions_answers_list = Doctrine::getTable('SecurityQuestionsAnswers')
        ->createQuery('a')
        ->execute();

    }

    public function executeShow(sfWebRequest $request)
    {
        $this->security_questions_answers = Doctrine::getTable('SecurityQuestionsAnswers')->find($request->getParameter('id'));
        $this->forward404Unless($this->security_questions_answers);
    }

    public function executeNew(sfWebRequest $request)
    {
        $this->getUser()->clearCredentials();
        //qns crediential is added at run time for identifying ewallet user for signin( no any use of checkqns credential)
        $this->getUser()->setAttribute('checkqns','checkqns');
        $this->form = new QnAForm();
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod('post'));

        $this->form = new QnAForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->forward404Unless($security_questions_answers = Doctrine::getTable('SecurityQuestionsAnswers')->find($request->getParameter('id')), sprintf('Object security_questions_answers does not exist (%s).', $request->getParameter('id')));
        $this->form = new SecurityQuestionsAnswersForm($security_questions_answers);
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($security_questions_answers = Doctrine::getTable('SecurityQuestionsAnswers')->find($request->getParameter('id')), sprintf('Object security_questions_answers does not exist (%s).', $request->getParameter('id')));
        $this->form = new SecurityQuestionsAnswersForm($security_questions_answers);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request)
    {
        $request->checkCSRFProtection();

        $this->forward404Unless($security_questions_answers = Doctrine::getTable('SecurityQuestionsAnswers')->find($request->getParameter('id')), sprintf('Object security_questions_answers does not exist (%s).', $request->getParameter('id')));
        $security_questions_answers->delete();

        $this->redirect('qna/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()));
        $data = $request->getParameter($form->getName());
        $answer = $data['answer'];

//         $usrId = $this->getUser()->getGuardUser()->getId();
         $usrName = $this->getUser()->getGuardUser()->getUsername();
        if ($form->isValid()){
            $this->getUser()->getAttributeHolder()->remove('checkqns');
            $this->getUser()->addCredentialForEwallet($this->getUser()->getAllPermissionNames());
            $this->auditEwalletUser($usrName, true);
            $openIdDetails = sfContext::getInstance()->getUser()->getAttribute('openId')?sfContext::getInstance()->getUser()->getAttribute('openId'):array();
            if(array_key_exists("recoverAccount",$openIdDetails) && ($openIdDetails['recoverAccount'])){
                $recoverRes = $this->recoverAccountProcess();
            }
            if(sfContext::getInstance()->getUser()->getAttribute('merchantRequestId')){
               $request->setParameter('merchantRequestId',$this->getUser()->getAttribute('merchantRequestId'));
               $request->setParameter('payType',$this->getUser()->getAttribute('payType'));
               $this->forward('sfGuardAuth','signin');
            }else{
                $sfGaurdObj = Doctrine::getTable('sfGuardUser')->find($this->getUser()->getGuardUser()->getId());
                $userDetailObj = $sfGaurdObj->getUserDetail()->getFirst();
                if($userDetailObj->getFailedAttempt()>0){
                    $userDetailObj->setFailedAttempt(0);
                    $sfGaurdObj->save();
                }
                return $this->redirect('@eWallethome');
            }
        }
         $this->auditEwalletUser($usrName,false,$answer);

    }
    
    public function executeRecoverAccountProcessWithoutQuestion(sfWebRequest $request){
        $this->recoverAccountProcess();
        $this->forward('sfGuardAuth', 'signin');
    }

    public function recoverAccountProcess(){
        $openIdDetails = sfContext::getInstance()->getUser()->getAttribute('openId');
        $chkUserExisis = Doctrine::getTable('UserDetail')->findByUserId($this->getUser()->getGuardUser()->getId());
        $ewalletType = $chkUserExisis->getFirst()->getEwalletType();
        $prevEmail = $chkUserExisis->getFirst()->getEmail();
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        if($prevEmail == $this->getUser()->getGuardUser()->getUserName()){
            $updateUserName = $payForMeObj->updateUserName($chkUserExisis->getFirst()->getUserId(),$openIdDetails['email']);
        }
        $updateEwalletType = $payForMeObj->updateEwalletType($chkUserExisis->getFirst()->getUserId(),$ewalletType,$openIdDetails['openIdAuth'],$openIdDetails['email']);

        // if email was not registered for which open Id server verified details (Recover eWallet Account, 2 mails)
        $subject = "eWallet Recover Confirmation Mail";
        $partialName = 'recover_details';
        $taskId = EpjobsContext::getInstance()->addJob('SendRecoverEwalletAccountMail', 'email' . "/sendRecoverEwalletAccountEmail", array('userid' => $chkUserExisis->getFirst()->getUserId(), 'subject' => $subject, 'partialName' => $partialName, 'prevEmail' => $prevEmail, 'openIdType' => $openIdDetails['openIdType']));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');

        # Begin code to send an email to his old email address notifying this action (that he has updated his email address on pay4me wallet)

        $subject = "eWallet Notification Mail" ;
        $partialName = 'recover_email_to_old';
        $taskId = EpjobsContext::getInstance()->addJob('SendMailUpdateEmailAddressNotification',"email/sendRecoverEmailToOld", array('userid'=>$chkUserExisis->getFirst()->getUserId(), 'email'=>$openIdDetails['email'], 'preEmail'=>$prevEmail,'subject'=>$subject,'partialName'=>$partialName));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');
        // Log successfull create new account via Openid Authentication
        $this->logEwalletRecoveOpenIdSuccessAuditDetails($chkUserExisis->getFirst()->getUserId(),$this->getUser()->getGuardUser()->getUsername(),$prevEmail,$openIdDetails['email']);
            
    }

    /*
     * Function to log audit for successful create new account via OpenId authentication
     */
    public function logEwalletRecoveOpenIdSuccessAuditDetails($id,$uname,$prevEmail,$newEmail){
      //Log audit Details
      //$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_RECOVER_ACCOUNT_WITH_OPENID,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_RECOVER_ACCOUNT_WITH_OPENID, array('username'=>$uname,'previousEmail'=>$prevEmail , 'newEmail'=>$newEmail)),
        null);
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
   
    private function auditEwalletUser($uname, $flag,$answer="")
    {
      if($flag) {
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGIN;
        $subcategory = EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN;
      } else{
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_SEC_ANSWER;
        $subcategory = EpAuditEvent::$SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER;
      }
              $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_SECURITY,
                            $subcategory,
                            EpAuditEvent::getFomattedMessage($msg, array('username'=>$uname,'answer'=>md5($answer))),
                            null);
                        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
}
