<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $security_questions_answers->getid() ?></td>
    </tr>
    <tr>
      <th>User:</th>
      <td><?php echo $security_questions_answers->getuser_id() ?></td>
    </tr>
    <tr>
      <th>Security question:</th>
      <td><?php echo $security_questions_answers->getsecurity_question_id() ?></td>
    </tr>
    <tr>
      <th>Answer:</th>
      <td><?php echo $security_questions_answers->getanswer() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $security_questions_answers->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $security_questions_answers->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $security_questions_answers->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $security_questions_answers->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $security_questions_answers->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('qna/edit?id='.$security_questions_answers->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('qna/index') ?>">List</a>
