<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<script type="text/javascript">
    $(document).ready(function(){
       $("#security_questions_answers_answer").val('');

    });

</script>

<?php
$buttonValue = "Proceed";
$openIdDetails = sfContext::getInstance()->getUser()->getAttribute('openId');
if(isset($openIdDetails['recoverAccount']))
    $buttonValue = "Recover";
?>
<div class="wrapForm2">
<form action="<?php echo url_for('qna/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post"  <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

 
  <?php echo $form ?>
  <div class="divBlock">
            <center>
          <input type="submit" value="<?php echo $buttonValue; ?>" class="formSubmit" />
  </center></div>
  
</form>
</div>
</div>