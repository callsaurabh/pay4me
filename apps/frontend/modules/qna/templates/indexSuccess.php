<h1>Qna List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>User</th>
      <th>Security question</th>
      <th>Answer</th>
      <th>Created at</th>
      <th>Updated at</th>
      <th>Deleted</th>
      <th>Created by</th>
      <th>Updated by</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($security_questions_answers_list as $security_questions_answers): ?>
    <tr>
      <td><a href="<?php echo url_for('qna/show?id='.$security_questions_answers->getId()) ?>"><?php echo $security_questions_answers->getId() ?></a></td>
      <td><?php echo $security_questions_answers->getUserId() ?></td>
      <td><?php echo $security_questions_answers->getSecurityQuestionId() ?></td>
      <td><?php echo $security_questions_answers->getAnswer() ?></td>
      <td><?php echo $security_questions_answers->getCreatedAt() ?></td>
      <td><?php echo $security_questions_answers->getUpdatedAt() ?></td>
      <td><?php echo $security_questions_answers->getDeleted() ?></td>
      <td><?php echo $security_questions_answers->getCreatedBy() ?></td>
      <td><?php echo $security_questions_answers->getUpdatedBy() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('qna/new') ?>">New</a>
