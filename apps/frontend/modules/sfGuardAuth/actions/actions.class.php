<?php
require_once(sfConfig::get('sf_plugins_dir').'/sfDoctrineGuardPlugin/modules/sfGuardAuth/lib/BasesfGuardAuthActions.class.php');
class sfGuardAuthActions extends BasesfGuardAuthActions {

  ##########################
  #### method call at signin time
  ##########################
  public function executeSignin($request) {
     //check is request type is ajax,
    //if yes request type not supported , return not supported
    if($this->checkRequestType($request))
    return  sfView::NONE;
    # if authinticated redirect to respective routes
    # parameters: user context object
    if($this->checkIsAllreadyLogin($request))
    return true;
    $openIdDetails = $this->getUser()->getAttribute('openId');

    $this->getFormObj($request);

    $this->validateUser($request);


    $broute = $this->getBankRouteName($request);
    if ($broute) {
      if($request->getParameter('bankName')){
        $this->bankname = $request->getParameter('bankName');
        $templateName = 'bankLogin';
      }else{
        //for showing image
        $this->accountType = 'eWallet';
        $templateName = 'eWalletLogin';

      }
      //$this->logMessage("Now redirecting to route $broute");
      $this->setTemplate($templateName);
      $this->setLayout(false);
      return;
    }
    $this->redirect('@homepage');
  }

  public function executeAssociateOrCreateNewAccountOrRecover($request){
      $this->openIdDetails = $this->getUser()->getAttribute('openId');

      $this->merchantRequestId=$request->getParameter('merchantRequestId');
      $this->payType=$request->getParameter('payType');

$this->setLayout('pay4me');
//      $this->setLayout(false);
  }
  ##########################
  ####check request for create form obj
  ##########################
  private function getFormObj($request){

    # if bankUser Show BankLogin Form else go usual.
    //used for usernametext box readonly attribute
    $usernameReadonly = true;
    if($request->hasParameter('bankName') || $request->hasParameter('payType') || count($this->getUser()->getAttribute('openId') )>0) {
      if($request->getParameter('bankName')) {
        # if bankUser Show BankLogin Form
        $class = 'BankLoginForm';
      }
      else if($request->getParameter('payType')!=sfConfig::get('app_payment_mode_option_bank') || count($this->getUser()->getAttribute('openId'))>0 ) {
        # if bankUser Show BankLogin Form
        $class = 'EWalletLoginForm';

//        //check if ewallet user comming from merchant application
//        if($request->getParameter('merchantRequestId')){
//             $usernameReadonly = false;
//        }
      }}else{
      #Default class
      $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
    }

    $this->form = new $class(array('readonly'=>$usernameReadonly),array(),false);

  }
  ##########################
  #### if request is ajax type, then return false(not supported)
  ##########################
  private function checkRequestType($request){
    if ($request->isXmlHttpRequest()) {
      $this->getResponse()->setHeaderOnly(true);
      $this->getResponse()->setStatusCode(401);
      return true;
    }
  }

  #########################
  #### check for bank teller for showing reset password button on login time
  #########################
  private function showResetPasswordButton($request)
  {
    if($request->isMethod('post'))
    {
      $postData = $request->getParameter('signin');
      $uname = $postData['username'];
      $this->isBankUser  = $this->isBankUser($uname);
    }
  }

  private function getPasswordReset($request)
  {
    $postData = $request->getParameter('signin');
    $uname = $postData['username'];
    $bname = $request->getParameter('bankName');
    $this->mailingNotification('user_request_reset_password',array('bankname'=>$bname,'username'=>$uname,'subject'=>'Pay4Me - User Request password reset'));
    $this->getUser()->setFlash('error', 'Request has been sent to your bank admin.');
    $this->redirect($request->getReferer());
  }

  private function isUserNameEmpty($bname,$request)
  {
    $postData = $request->getParameter('signin');
    $uname = $postData['username'];
    if((isset($postData['username']) && empty($postData['username']))||
            (isset($postData['password']) && empty($postData['password']))) {
      $msg = "";
      //in case of bank login
      if(empty($postData['username']))
      $msg = 'Username can\'t be empty';
      if(empty($postData['password']))
      $msg = (empty($msg))?"Password can't be empty":$msg."<br/>Password can't be empty";
      $this->getUser()->setFlash('error', $msg);
      if($bname != "") {
        return ;//$this->redirect('@bank_login?bankName='.$bname);
      }
      else {
        $this->redirect($request->getReferer());
      }
    }
  }

  ##########################
  #### check is user login first time
  ##########################
  private function checkFirstLogin($uname){
    $group_name = $this->getUser()->getGroupNames();
    if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name) ||  in_array(sfConfig::get('app_pfm_role_ewallet_user'),$group_name) ||in_array(sfConfig::get('app_pfm_role_bank_admin'),$group_name)
      ||in_array(sfConfig::get('app_pfm_role_bank_country_head'),$group_name) ||in_array(sfConfig::get('app_pfm_role_country_report_user'),$group_name)|| in_array(sfConfig::get('app_pfm_role_report_bank_admin'),$group_name) || in_array(sfConfig::get('app_pfm_role_bank_e_auditor'),$group_name) || in_array(sfConfig::get('app_pfm_role_support'),$group_name)
      || in_array(sfConfig::get('app_pfm_role_sub_merchant'),$group_name) || in_array(sfConfig::get('app_pfm_role_report_admin'),$group_name)|| in_array(sfConfig::get('app_pfm_role_bank_branch_report_user'),$group_name)){
      if($this->getUser()->isFirstLogin($this->getUser()->getGuardUser())) {
        //First time user redirect to change password
        $signinUrl = '@change_first_password';
        $this->getUser()->clearCredentials();
        //create a log; that user has logged in for the first time
        $this->getUser()->log_first_login();
        //Audit trail log first login
        $this->logFirstLoginSuccessAuditDetails($uname,$this->getUser()->getGuardUser()->getId());
        return $this->redirect($signinUrl);
      }
    }
    return ;
  }

  ##########################
  ####Redirect Bank Branch user to bank home
  ##########################
  private function redirectToBankHome($uname){
    // Log successfull login Audit Trail Details
    $this->logLoginSuccessAuditDetails($uname, $this->getUser()->getGuardUser()->getId());
    //create a log; successfull login of a bank user
    $this->getUser()->log_successful_login();
    return $this->redirect('@bankhome');
  }

  ##########################
  ####Redirect Bank Branch user to Payment Page
  ##########################
  private function redirectToPayment($uname,$txnId){
    // Log successfull login Audit Trail Details
    $this->logLoginSuccessAuditDetails($uname, $this->getUser()->getGuardUser()->getId());
    $this->redirect('paymentSystem/search?txnId='.$txnId);
  }

  ##########################
  ####Redirect Ewallet user to Ewallet home
  ##########################
  private function redirectToEwalletHome($uname,$requestid,$paymentMode){
    // Log successfull login Audit Trail Details
    $this->logLoginSuccessAuditDetails($uname, $this->getUser()->getGuardUser()->getId());
    //create a log; successfull login of a bank user
    $this->getUser()->log_successful_login();
    $this->redirect('@payRedirect?requestId='.$requestid.'&paymentMode='.$paymentMode);


     /*   if($paymentMode == "ewallet") {//print "gfgfgf";exit;
          return $this->redirect('@eWallethome?requestId='.$requestid);
        }
        else if($paymentMode == sfConfig::get('app_payment_mode_option_credit_card')) {//print "ccc";exit;
          return $this->redirect('@creditCardhome?requestId='.$requestid);
        }*/
  }

  ##########################
  #### Redirect Admin role user to admin home
  ##########################
  private function redirectToAdminHome($uname){
    //$this->logMessage("Found a valid user");
    //create a log; successfull login
    $this->getUser()->log_successful_login();
    // Log successfull login Audit Trail Details
    $this->logLoginSuccessAuditDetails($uname, $this->getUser()->getGuardUser()->getId());
    return $this->redirect('@adminhome');
  }

  ##########################
  #### Update valid User info on invalid sigin
  #### valid User(user is attached with portal)
  ##########################
  private function updateInformationForInvalidLogin($uname,$bname,$request,$error=""){
    //set password and answer to null as it was a failed attempt to login
    $data = $request->getParameter($this->form->getName());
    $data['answer'] = (isset($data['answer']))?$data['answer']:'';
    $data['password'] =  $data['password'];
    $this->logInvalidPasswordAndSECAnswerAuditDetails($uname,$error,$data['answer']);
    $this->form->bind($data);

    ##start block user functionality
    //if username is exist in database but password mismatched or ip mismatched then block user
    //if user not able to login with in max_failure_attempts attempt(continuously)
    $errors = "";
    $authenticateObj = new userAuthentication();
    $authenticateObj->setUsername($uname);
    $returnVal = $authenticateObj->updateFailedAttempt($uname);
    if($returnVal =='user_blocked') {
      $errors = "This username is blocked. Please contact the administrator.";
      //only send in case of bank teller(bank user) to bank admin.
      // mail send only if user is a bank user(bank teller)
      // $this->mailingNotification('user_blocked',array('bankname'=>$bname,'username'=>$uname,'subject'=>'Pay4Me - User Block Notification'));
    }
    else if($returnVal !='continue') {
      $errors = "you did $returnVal failed attempts out of ".sfConfig::get('app_number_of_failed_attempts_for_blocked_user').".";
    }

      foreach($this->form as $elm) {

          if(!stristr($elm->renderError(),'Required')){
              $errors .= $elm->renderError();
          }

      }
    $data['answer'] = '';
    $data['password'] =  '';
    $this->form->bind($data);

    $this->getUser()->setFlash('error', $errors,false);
    if($request->hasParameter('bankName') || $request->hasParameter('payType') || count($this->getUser()->getAttribute('openId'))>0) {
      if($request->getParameter('bankName')!="") {    //if the user is a bank user
        $this->setTemplate('bankLogin');
      }else
      if( ($request->getParameter('payType')!=sfConfig::get('app_payment_mode_option_bank')) || count($this->getUser()->getAttribute('openId'))>0) {    //if the user is a eWallet user
        $this->setTemplate('eWalletLogin');
      }}
    else { //if the user is admin
      $this->forward('page','index');
    }
  }

  private function checkIsAllreadyLogin($request){
    //get user context object
    $user = $this->getUser();
    if ($user->isAuthenticated()) {
      ###   $username = $user->getGuardUser()->getUsername();
      //validate user group with depending entity
      ####  if($this->validateUserAssociations($request,$username)){
      #already logedd in redirect to respective user group home pages
      $this->redirectToUserHome($request);
      return true;
      ####  }
      ####else
      ####return false;
    }else
    return false;
  }

  public function setValuesByRequest($request){
    $this->bankname = '';
    $this->bankuser = false;
    $postData = $request->getParameter('signin');
    if($request->getParameter('bankName')){
      $this->bankname = $request->getParameter('bankName');
    }else if(isset($postData['username']) && $this->isUserRoleIsBank($postData['username'])){
        //get bank name
        $bankObj = Doctrine::getTable('Bank')->getBankNameByUserName($postData['username']);
        if($bankObj!=false){
            $bankname = $bankObj->getFirst()->getAcronym();
            if($bankname!=""){
               $this->bankname = $bankname;
               $request->setParameter('bankName',$bankname);
               $this->getFormObj($request);
               $this->bankuser = true;
               //$this->forward('page','loginStepOne');
               //$this->redirect('@bank_login?bankName='.$bankname);
            }
        }
    } else if($request->getParameter('payType')!=sfConfig::get('app_payment_mode_option_bank')){
        $this->setFormHiddenValues($request);
    }
  }
  # not a valid  username
  public function notAUser($request) {
    if($this->getUser()->getFlash('error'))
    $this->getUser()->setFlash('error', $this->getUser()->getFlash('error'),true);
    else if($this->getUser()->getFlash('notice'))
    $this->getUser()->setFlash('notice', $this->getUser()->getFlash('notice'),true);
    else
    $this->getUser()->setFlash('error', 'Invalid Username',true);
    //set parameter request for ewallet user, reasion is peserve reqiestId value
    if($request->getParameter('payType')!=sfConfig::get('app_payment_mode_option_bank')){
      $request->setParameter('merchantRequestId',  $request->getParameter('merchantRequestId'));
      $request->setParameter('payType',  $request->getParameter('payType'));
      $this->forward('sfGuardAuth','signInOnlinePayment');
      return;
    }else
    $this->redirect($request->getReferer());
  }
  public function validateUser($request){
  	 
    if ($request->isMethod('post')) {
    	$postData = $request->getParameter('signin');
      $this->setValuesByRequest($request);
      $this->isUserNameEmpty($this->bankname,$request);
      $uname = $postData['username'];
      //check user is valid.
      if(!$this->checkValidUser($uname,$request)) {
          $this->notAUser($request);
      }

      if( isset($postData['userId']) && $postData['userId']){
          //check user is valid authenticated ewallet user for openId association for which open Id server has sent details.
          if(!$this->checkOpenIdValidUser($postData,$request)) {
              $this->notAUser($request);
          }
      }

      $openIdDetails = $this->getUser()->getAttribute('openId');
      if(isset($openIdDetails['assocEmail']) &&  ($openIdDetails['assocEmail'])){
          if(!$this->checkOpenIdAssociateValidUser($uname,$request)) {
              $this->notAUser($request);
          }
      }
      $authenticateObj = new userAuthentication();
      $authenticateObj->setUsername($uname);
      $val = $authenticateObj->checkBankUser();
      if(!$val) {
        $this->notAUser($request);
      }
      else {
        if($val!=1) {
          if($val==2) {
            $this->signoutOnError( "This bank user is Suspended,<br/>Please contact to administrator.");
            $this->logUserSuspendAuditDetails($uname);
            $this->redirect($request->getReferer());
            return false;
          }
          if($val==3){
            $this->signoutOnError( "This bank user is De-Activated,<br/>Please contact to administrator");
            $this->logUserDeactivateAuditDetails($uname);
            $this->redirect($request->getReferer());
            return false;
          }
          $this->signoutOnError( "You are not authorized to Login. Please contact to administrator.");
          return false;
        }
      }

      // check user is allready login with limited sessions, if yes redirect to homepage with error msg.
      if($authenticateObj->getUserSessionStatus()==false) {
          $errors =  "User with username - '".$uname."' is already logged in from another terminal";
        //  $errors = "Someone already logged in by username '".$uname."'.";
          $this->getUser()->setFlash('error', $errors);
          $this->redirect('@homepage');
          return false;
      //return false;
      }
      //check numeber of failed attempts. if number of attempts  exceed by failed attempt limit then redirect to home bage with error
      if($authenticateObj->redirectOnFailedAttempt($request->getReferer())==false)
      {
          $errors = "This username is blocked. Please contact the administrator.";
          $this->getUser()->setFlash('error', $errors);
          $this->redirect($request->getReferer());
      }
      //check password is set
      if(isset($postData['password']) && !$this->bankuser) {
        $this->form->bind($request->getParameter('signin'));
        if ($this->form->isValid()) {
          // verify if this user belongs to the bank in question
          $values   = $this->form->getValues();
          $remember = isset($values['remember']) ? $values['remember'] : false;
          //WP051
          $this->getUser()->setFirstLogin($values['user']);
          $this->getUser()->signin($values['user'], $remember);
          # check user login is first time or not is first time redirect user to change password
          $this->checkFirstLogin($uname);
          return  $this->redirectSuccessfullyToGroupHome($request);
        } else {
        	// Log unsuccessfull login Audit Trail Details
          // block user functionality
          // redirect user to sigin page
          $errormsg = $this->form->renderGlobalErrors();
          $this->updateInformationForInvalidLogin($uname,$this->bankname,$request,$errormsg);
        }
      }
    } else {
      if(count($this->getUser()->getAttribute('openId'))>0){
          $formVal =  array();
          $userDetails = $request->getParameter('signin');
          $formVal['userId'] = $userDetails['user_id'];
          $this->formHiddenVal = $formVal;
      }
      $this->getUser()->setReferer($request->getReferer());
      $module = sfConfig::get('sf_login_module');

      if ($this->getModuleName() != $module) {
        return $this->redirect($module.'/'.sfConfig::get('sf_login_action'));
      }
      $this->getResponse()->setStatusCode(401);
    }
  }


  private function redirectSuccessfullyToGroupHome($request){

    if(($this->chkEwalletUser() && (!$request->hasParameter('merchantRequestId')) && (count($this->getUser()->getAttribute('openId'))==0)) ){
      $this->redirectToQnA($request);
    }
    $txnId="";
    if($request->hasParameter('txnId')){
      $txnId = $request->getParameter('txnId');
    }
    if(count($this->getUser()->getAttribute('openId'))>0){
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $userArr = $request->getParameter('signin');
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($userArr['username']);
        $openIdDetails = $this->getUser()->getAttribute('openId');
        $ewalletType = $userDetails[0]['UserDetail'][0]['ewallet_type'];
        $request->setParameter('merchantRequestId',  $this->getUser()->getAttribute('merchantRequestId'));
        $request->setParameter('payType',  $this->getUser()->getAttribute('payType'));
        $updateEwalletType = $payForMeObj->updateEwalletType($userDetails[0]['id'],($ewalletType == 'ewallet')?'both':'both_pay4me',$openIdDetails['openIdAuth'],$openIdDetails['email']);


        if( isset($openIdDetails['assocEmail']) && $openIdDetails['assocEmail']){
            // if email was not registered for which open Id server verified details (2 mails)
            $subject = "eWallet Association Confirmation Mail";
            $partialName = 'association_details';
            $taskId = EpjobsContext::getInstance()->addJob('SendAssociationMail', 'email' . "/sendAssociateConfirmationEmail", array('userid' => $userDetails[0]['id'], 'subject' => $subject, 'partialName' => $partialName, 'assocEmail' => $openIdDetails['assocEmail'], 'openIdType' => $openIdDetails['openIdType']));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

            # Begin code to send an email to his old email address notifying this action (that he has updated his email address on pay4me wallet)

            $subject = "eWallet Notification Mail" ;
            $partialName = 'association_details_to_old_email';
            $taskId = EpjobsContext::getInstance()->addJob('SendMailUpdateEmailAddressNotification',"userAdmin/sendUpdateEmail", array('userid'=>$userDetails[0]['id'], 'email'=>$openIdDetails['email'], 'preEmail'=>$userDetails[0]['UserDetail'][0]['email'],'subject'=>$subject,'partialName'=>$partialName,'openIdType' => $openIdDetails['openIdType']));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            // Log successfull create new account via Openid Authentication
            $this->logOpenidAssociateOpenidSuccessAuditDetails($userArr['username'], $userDetails[0]['id'],$userDetails[0]['UserDetail'][0]['email'],$openIdDetails['email']);

        }else{
            // if email was registered for which open Id server verified details (1 mail)
            $subject = "eWallet Association Confirmation Mail";
            $partialName = 'association_details';
            $taskId = EpjobsContext::getInstance()->addJob('SendAssociationMail', 'email' . "/sendAssociateConfirmationEmail", array('userid' => $userDetails[0]['id'], 'subject' => $subject, 'partialName' => $partialName, 'assocEmail' => NULL , 'openIdType' => $openIdDetails['openIdType']));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            // Log successfull create new account via Openid Authentication
            $this->logOpenidAssociateOpenidSuccessAuditDetails($userArr['username'], $userDetails[0]['id'],$openIdDetails['email']);

        }

    }
    return $this->redirectToUserHome($request,$txnId);
  }


   /*
   * Function to log audit for successful Association of ewallet account with Openid authentication
   */
    public function logOpenidAssociateOpenidSuccessAuditDetails($uname,$id,$preEmail,$newEmail){
      //Log audit Details
      //$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
      if($newEmail){
          $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_ASSOCIATE_ACCOUNT_WITH_OPENID,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_ASSOCIATE_ACCOUNT_WITH_OPENID, array('username'=>$uname , 'previousEmail'=>$preEmail , 'newEmail'=>$newEmail)),
            null);
      }else{
          $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_ASSOCIATE_ACCOUNT_WITH_OPENID,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_ASSOCIATE_ACCOUNT_WITH_OPENID_EMAIL_EXISTS, array('username'=>$uname , 'previousEmail'=>$preEmail)),
            null);
      }
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

  private function validateUserAssociations($request,$username) {
    // validate if this user belong to any bank or not --> $this->isValidBankUser
    // to do find group name by userr name
    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $userDetails =$payForMeObj->getUserDetailByUsername($username);
    $group_name = $userDetails['group_name'];
    switch($group_name){
      case sfConfig::get('app_pfm_role_bank_branch_user') :
        case sfConfig::get('app_pfm_role_bank_admin') :
        case sfConfig::get('app_pfm_role_bank_country_head') :
        case sfConfig::get('app_pfm_role_country_report_user') :
          case sfConfig::get('app_pfm_role_report_bank_admin') :
            case sfConfig::get('app_pfm_role_bank_branch_report_user') :
              case sfConfig::get('app_pfm_role_bank_e_auditor') :
                return   $this->validateBankUserAssociation($userDetails,$request);
                break;
            case sfConfig::get('app_pfm_role_ewallet_user') :
              if($this->bankname!=""){return false;}
              else
              return true;
              break;
              case sfConfig::get('app_pfm_role_guest_user') :
              	if($this->bankname!=""){return false;}
              	else
              		return true;
              	break;
              
          case sfConfig::get('app_pfm_role_support') :
            if($this->bankname!=""){return false;}
              else if($request->getParameter('payType') && $request->getParameter('payType')=='ewallet'){return false;}
              else
              return true;
              break;
        case sfConfig::get('app_pfm_role_report_admin') :
        case sfConfig::get('app_pfm_role_report_portal_admin') :
          case sfConfig::get('app_pfm_role_admin') :
            case sfConfig::get('app_pfm_role_dev') :
              if($this->bankname!=""){return false;}
              else if($request->getParameter('payType') && $request->getParameter('payType')=='ewallet'){return false;}
              else
              return true;
              break;
            case sfConfig::get('app_pfm_role_portal_support') :
              return true;
              break;
            case sfConfig::get('app_pfm_role_merchant') :
              return true;
              break;
            case sfConfig::get('app_pfm_role_sub_merchant') :
            	return true;
            	break;
            default:
            return false;
            break;
      }
    }
    private function validateBankUserAssociation($userDetails,$request) {
      if($request)
      if($this->isValidBankUser($request,$userDetails['bank_acronym'])==false){
        //call siginout method with template name
        $this->signoutOnError( "Sorry, you are not authorized to Login", 'bankLogin');
        return false;
      }
      // validate if the bank and/or branch is active or not
      //check bank is active or not
      if(!$this->isBankActive($userDetails)) {
        //              // NOT Found a valid bank user or the user belongs to inactive bank
        $this->signoutOnError('Sorry, your bank is Inactive','bankLogin');
        return false;
      }
      $group_name[] = $userDetails['group_name'];
      //check brach is active or not
      if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name)){
        if(!$this->isBranchActive($userDetails)) {
          // NOT Found a valid bank user or the user belongs to inactive bank branch
          $this->signoutOnError('Sorry, your bank branch is Inactive','bankLogin');
          return false;
        }
      }
      return true;
    }

    protected function signoutOnError($errMsg, $templateName='') {
      if($this->getUser()->getGuardUser()){
        $EpDBSessionDetailObj =  new EpDBSessionDetail();
        $EpDBSessionDetailObj->doCleanUpSessionByUser();
      }
      $this->getUser()->log_logout();
      $this->getUser()->signout();
      $this->getUser()->setFlash('error', $errMsg);
      if($templateName){
        $this->setTemplate($templateName);
        $this->setLayout(false);
      }
      return ;
    }

    /**
    * signout method
    * Parameters : Request object
    **/
    public function executeSignout($request) {
      //call audittrail security logout
      sfContext::getInstance()->getUser()->getAttributeHolder()->remove('menu');
      $this->getUser()->getAttributeHolder()->remove('openId'); // to unset openId session variables
      $this->getUser()->getAttributeHolder()->remove('merchantRequestId'); // to unset merchantRequestId session variable
      $this->getUser()->getAttributeHolder()->remove('payType'); // to unset payType session variable
      $bankName = $this->getUser()->getAssociatedBankAcronym();
      $groupName =  $this->getUser()->getGroupNames();



      if($this->getUser()  && $this->getUser()->isAuthenticated()) {
        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_SECURITY,
          EpAuditEvent::$SUBCATEGORY_SECURITY_LOGOUT,
          EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGOUT, array()),
          null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
      }
      $this->getUser()->signOutUser();
      if ($bankName && !in_array(sfConfig::get('app_pfm_role_ewallet_user'),$groupName)) {
        return $this->redirect('@bank_login?bankName='.$bankName);
      }
      if( $this->getUser()->getFlash('noticeEmailUpdate') == "yes"){
        $this->getUser()->setFlash('notice', sprintf("Email Address Updated Successfully!  An email containing an 'Activation Link' has been sent to your specified email address. Click on the link in the email, to activate your account."));
      }
//      if (in_array(sfConfig::get('app_pfm_role_ewallet_user'),$groupName)){
//        return $this->redirect('@ewallet_signin?payType=ewallet&payMode=ewallet');
//      }
      $signout_url = sfConfig::get('app_sf_guard_plugin_success_signout_url', $request->getReferer());
      $this->redirect('' != $signout_url ? $signout_url : '@homepage');
    }


  /**
   * This function validates if the current user belongs to
   * bank which's present in the parameter
   */
    protected function isValidBankUser($request,$dbBnkName) {
      $bankName = $request->getParameter('bankName');
      //$this->logMessage("Found user's DB bank: $dbBnkName - going to compare with $bankName");
      if(empty($bankName)) {
        if(empty($dbBnkName)) {
          return true;
        }
      }
      if ($dbBnkName == $bankName ) {
        return true;
      }
      return false;
    }

    protected function isBankActive($userDetails) {
      //  verify if the bank is active or not
      $bnk_status = 0;
      $bnk_id = $userDetails['bank_id'];
      if($bnk_id != "") {
        //the bank is deleted
        $bnk_status = $userDetails['bank_status'];
      }
      if($bnk_status == 0) {
        //$this->logMessage(" -- User belongs to Inactive or the branch is deleted");
        return false;
      }
      else {
        //$this->logMessage(" -- User belongs to Active");
        return true;
      }
    }
    protected function isBranchActive($userDetails) {
      //  verify if the bank is active or not
      $bnk_branch_status = 0;
      $bnk_branch_id = $userDetails['bank_branch_id'];
      if($bnk_branch_id != "") {//the bank_branch is deleted
        $bnk_branch_status = $userDetails['bank_branch_status'];
      }
      if($bnk_branch_status == 0) {
        //$this->logMessage(" -- User belongs to Inactive branch or the branch is deleted");
        return false;
      }
      else {
        //$this->logMessage(" -- User belongs to Active branch");
        return true;
      }
    }

    protected function getBankRouteName($request, $bnk = null) {
      if(count($this->getUser()->getAttribute('openId'))>0){
          return '@ewallet_login';
      }
      else if((($request->getParameter('payType')!='' && ($request->getParameter('payType')!=sfConfig::get('app_payment_mode_option_bank'))))  ){ // Bug:37533
        return '@ewallet_login';
      }
      else{
        if (!$bnk) {
          $bnk = $request->getParameter('bankName');
        }
        if (empty($bnk)) {
          return null;
        }
        return '@bank_login?bankName='.$bnk;
      }
    }

    protected function checkIpAddress($userId, $username,$remoteIp) {
      //To do find
      $ipAdd = Doctrine::getTable('BankBranch')->getIpAddForBankUser($userId);

      if(!count($ipAdd) || !$ipAdd[0]['ipAddress']) {
        return true;
      }
      $ipAdd = explode(',', $ipAdd[0]['ipAddress']);
      if($this->validIpAdd($ipAdd,$remoteIp)) {
        return true;
      }
      $this->logInvalidIPAddressAuditDetails($username,$userId);
      $this->signoutOnError( "Invalid System", 'bankLogin');
      return false;
    }

    protected function validIpAdd($ipRangeFromDb,$remoteIp) {
      if(count($ipRangeFromDb)) {
        foreach($ipRangeFromDb as $rangeFromDb) {
          if (IpValidator::IsIpInRange($remoteIp, $rangeFromDb)) {
            return true;
          }
        }
      }
      return false;
    }

    public function getIpAddress() {
      return (empty($_SERVER['HTTP_CLIENT_IP'])?(empty($_SERVER['HTTP_X_FORWARDED_FOR'])?
          $_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_X_FORWARDED_FOR']):$_SERVER['HTTP_CLIENT_IP']);
    }
    public function checkEwalletStatus($uname) {
      $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($uname);
      // print $userDetails[0]['UserDetail'][0]['user_status'];exit;
      if(4 == $userDetails[0]['UserDetail'][0]['user_status'])
      {
        $this->getUser()->setFlash('error', 'User is not approved. ',true);
        return false;
      }
      if(5 == $userDetails[0]['UserDetail'][0]['user_status'])
      {
        $this->getUser()->setFlash('error', 'User is Disapproved. ',true);
        return false;
      }
      return true;
    }
    public function isActiveUser($activeVal,$uname,$request) {
      $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
      $userDetails =$payForMeObj->getUserDetailByUsername($uname);
      if(empty($activeVal)){
        $this->getUser()->setFlash('error', 'User is not activated. ',true);
        //set parameter request for ewallet user, reasion is peserve reqiestId value
        if($request->getParameter('payType')!=sfConfig::get('app_payment_mode_option_bank')){

          if(!$this->checkEwalletStatus($uname)) {
            return false;
          }

          $request->setParameter('merchantRequestId',  $request->getParameter('merchantRequestId'));
          $request->setParameter('payType',  $request->getParameter('payType'));
          $this->forward('sfGuardAuth','signInOnlinePayment');
        }else
        $this->redirect($request->getReferer());
        return false;
      }
      else
      {
        if('ewallet_user' == $userDetails['group_name']){
          if(!$this->checkEwalletStatus($uname)) {
            $request->setParameter('merchantRequestId',  $request->getParameter('merchantRequestId'));
            $request->setParameter('payType',  'ewallet');
            $this->forward('sfGuardAuth','signInOnlinePayment');
          }
          else
          { return true; }

        }
        return true;
      }
    }



    public function checkBankUser($uname,$request) {

      if(empty($uname)) return false;
      //print_r('user: '.$uname.' : bank: '.$bname);
      # if first timeUser Skip Question validation
      $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($uname);


      $val = $userDetails[0]['UserDetail'][0]['user_status'];
      if($val) {
        if($val==2) {
          $this->signoutOnError( "This bank user is Suspended,<br/>Please contact to bank admin");
          $this->logUserSuspendAuditDetails($uname);
          return false;
        }
        if($val==3){
          $this->signoutOnError( "This bank user is De-Activated,<br/>Please contact to bank admin");
          $this->logUserDeactivateAuditDetails($uname);
          return false;
        }

        return true;
      }
      return false;

    }


    public function checkValidUser($uname,$request) {
      if(empty($uname)) return false;
      //print_r('user: '.$uname.' : bank: '.$bname);
      # if first timeUser Skip Question validation
      $q = Doctrine_Query::create()->select('*')
      ->from('sfGuardUser')
      ->andWhere('username=?',$uname)
      ->execute()
      ->toArray(Doctrine::HYDRATE_ARRAY);
      $val = count($q);
      if($val) {
        if($this->isActiveUser($q[0]['is_active'],$uname,$request)==false){
          return;
        }
        // Check bank user IP Address
        if(!$this->checkIpAddress($q[0]['id'], $uname,$this->getIpAddress())) {
          return;
        }
        if(!$this->validateUserAssociations($request,$uname)) {
          return;
        }
        return true;
      }
    /*Audit trail log....incorrect username*/
      $this->logIncorrectUsernameAuditDetails($uname);
      return false;
    }

    /*
     * function to check if username is the same for which openId credentials have been verified by opnenId server at the time of Association
     */
    public function checkOpenIdValidUser($userData,$request) {

      if(empty($userData['userId'])) return true;
      $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
      $validuser = $payForMeObj->OpenIdValidUser($userData);
      if(!$validuser) {
        $emailData = $this->getUser()->getAttribute('openId');
        $errors =  "You are not authorized to associate with '".$emailData['email']."'";
        $this->getUser()->setFlash('error', $errors);
        $this->logIncorrectUsernameAuditDetails($userData['username']);
        return false;
      }else{
          $this->getUser()->setFlash('error', NULL);
      }
      return true;
    }


    /*
     * function to check if username openid/both type. It should not be of openid/both type if email id does not exist.
     */
    public function checkOpenIdAssociateValidUser($uname,$request) {

      $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
      $ewalletType = $payForMeObj->OpenIdAssociateValidUser($uname);
      if($ewalletType != 'ewallet' && $ewalletType != 'ewallet_pay4me') {
        $emailData = $this->getUser()->getAttribute('openId');
        $errors =  "This user is not authorized for association";
        $this->getUser()->setFlash('error', $errors);
        $this->logIncorrectUsernameAuditDetails($uname);
        return false;
      }else{
          $this->getUser()->setFlash('error', NULL);
      }
      return true;
    }

    //check no of attempt for the day, if >max_failure_attempts redirect to signin page with proper message
    function updateFailedAttempt($uname) {
      $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($uname);
      $countFailedAttempt = 0;
      if($numberOfContinuousAttemptObj!=false)
      {
        $countFailedAttempt = $numberOfContinuousAttemptObj->getFailedAttempt();
        if($countFailedAttempt==sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
          //return to logout message
          //return false if number of attempts are =max_failure_attempts, false represent the user as block user for 24 hours
          return 'user_blocked';
        }
        else {
          if($numberOfContinuousAttemptObj) {
            //update failed attempts for the day
            $numberOfContinuousAttemptObj->setFailedAttempt(($countFailedAttempt+1));
            $numberOfContinuousAttemptObj->save();
            if($numberOfContinuousAttemptObj->getFailedAttempt() ==sfConfig::get('app_number_of_failed_attempts_for_blocked_user')){
              // Audit trail code
              //Log audit Details
              $this->logUserBlockAuditDetails($uname,$numberOfContinuousAttemptObj->getUserId());
              return 'user_blocked';
            }
            return $numberOfContinuousAttemptObj->getFailedAttempt();
          }
          else {
            return 'continue';
          }
        }
      }
      else
      return 'continue';
    }

    //redirect user to login page if blocked
    function redirectOnFailedAttempt($requestReferer,$uname) {
      $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($uname);
      if($numberOfContinuousAttemptObj!=false)
      {
        $countFailedAttempt = $numberOfContinuousAttemptObj->getFailedAttempt();
        // TODO - have max failure attempt in app.yml
        if($countFailedAttempt ==sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
          $errors = "This username is blocked. Please contact the administrator. ";
          $this->getUser()->setFlash('error', $errors);
          $this->redirect($requestReferer);
          return false;
        }
        return true;
      }
      else{
        $errors = "Insufficient User detail.";
        $this->getUser()->setFlash('error', $errors);
        $this->redirect($requestReferer);
        return false;
      }
    }
    public function mailingNotification($partialTemplate,$userValArr){
      $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($userValArr['username']);
      $groupDetails = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userDetails[0]['id']);
      if($groupDetails!=false && sfConfig::get('app_pfm_role_bank_branch_user')==$groupDetails['sfGuardGroup']['name']) {
        $bankAdminDetail = Doctrine::getTable('BankUser')->getBankAdminDetails($userDetails[0]['bank_id']);
        //To Do : a bank may has more then one bank admin so there is number of mail id's , then to whom we will send mail notification
        $mailTo = $bankAdminDetail['sfGuardUser']['UserDetail'][0]['email'];
        //$mailTo = "sunildutttiwari@gmail.com";
        $signature = sfConfig::get('app_email_signature');
        $subject = $userValArr['subject'];
        $mailToGroup = array() ;
        $partialName =$partialTemplate ;
        $partialVars = array('name'=>$userDetails[0]['UserDetail'][0]['name'],'username'=>$userValArr['username'],'signature' => $signature) ;
        $mailingOptions = array('mailFrom' => array(sfConfig::get('app_email_username') => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
        $sendMailObj = new Mailing() ;
        $mailReturned = $sendMailObj->sendMail($partialName, $partialVars, $mailingOptions) ;
        if($mailReturned) {
          return true ;
        }else {
          return false ;
        }
      }
    }
    public function isBankUser($userName){
      $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($userName);
      if(count($userDetails)>0){
        $groupDetails = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userDetails[0]['id']);
        if($groupDetails!=false && sfConfig::get('app_pfm_role_bank_branch_user')==$groupDetails['sfGuardGroup']['name']) { return true; }
        else{ return false; }
      }else{return false; }
    }

    public function isUserRoleIsBank($userName){
      $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($userName);
      if(count($userDetails)>0){
        $groupDetails = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userDetails[0]['id']);
        $arrRoles = array(
                        sfConfig::get('app_pfm_role_bank_branch_user')=>
                            sfConfig::get('app_pfm_role_bank_branch_user'),
                        sfConfig::get('app_pfm_role_bank_branch_report_user')=>
                            sfConfig::get('app_pfm_role_bank_branch_report_user'),
                        sfConfig::get('app_pfm_role_bank_country_head')=>
                            sfConfig::get('app_pfm_role_bank_country_head'),
                        sfConfig::get('app_pfm_role_country_report_user')=>
                            sfConfig::get('app_pfm_role_country_report_user'),
                        sfConfig::get('app_pfm_role_report_bank_admin')=>
                            sfConfig::get('app_pfm_role_report_bank_admin'),
                        sfConfig::get('app_pfm_role_report_admin')=>
                            sfConfig::get('app_pfm_role_report_admin'),
                        sfConfig::get('app_pfm_role_bank_e_auditor')=>
                            sfConfig::get('app_pfm_role_bank_e_auditor'),
                        sfConfig::get('app_pfm_role_bank_admin')=>
                            sfConfig::get('app_pfm_role_bank_admin')
                        );
        if($groupDetails!=false &&
                isset($arrRoles[$groupDetails['sfGuardGroup']['name']])) {
            return true;
        } else {
            return false;
        }
      } else {
          return false;
      }
    }

    public function logUserBlockAuditDetails($uname,$id)
    {
      //Log audit Details
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_TRANSACTION,
        EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_BLOCKED,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_BLOCKED, array('username'=>$uname)),
        null,
        $id, $uname
      );
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logFirstLoginSuccessAuditDetails($uname,$id)
    {
      //Log audit Details
      //$applicationArr = array(new EpAuditEventAttributeHolder('User Details',$uname,$id));
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRST_LOGIN, array('username'=>$uname)),
        null);
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logIncorrectUsernameAuditDetails($uname){
      //Log audit Details
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_INCORRECT_USERNAME,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_INCORRECT_USERNAME, array('username'=>$uname)),
        null
      );
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logUserSuspendAuditDetails($uname){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($uname);
        $applicationArr = NULL;
        $uid = NULL;

        if(count($userDetails[0]['BankUser'])){
            $bank_id = $userDetails[0]['BankUser'][0]['bank_id'];
            $uid = $userDetails[0]['id'];
            $bank_name = $userDetails[0]['bank_name'];
            if($bank_id!=""){
                $branch_name = "";
            }
            $branch_id = $userDetails[0]['BankUser'][0]['branch_id'];
            if($branch_id!=""){
                $branch_name = $userDetails[0]['bank_branch_name'];
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) ,new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME,$branch_name,$bank_id));
            }else{
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) );}
        }else{
            $applicationArr = NULL;
        }

        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_SUSPENDED_USER,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_SUSPENDED_USER_LOGIN_ATTEMPT, array('username'=>$uname)),
            $applicationArr,
            $uid,
            $uname
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logUserDeactivateAuditDetails($uname){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($uname);
        $applicationArr = NULL;
        $uid = NULL;

        if(count($userDetails[0]['BankUser'])){
            $bank_id = $userDetails[0]['BankUser'][0]['bank_id'];
            $uid = $userDetails[0]['id'];
            $bank_name = $userDetails[0]['bank_name'];
            if($bank_id!=""){
                $branch_name = "";
            }
            $branch_id = $userDetails[0]['BankUser'][0]['branch_id'];
            if($branch_id!=""){
                $branch_name = $userDetails[0]['bank_branch_name'];
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) ,new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME,$branch_name,$bank_id));
            }else{
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) );}
        }else{
            $applicationArr = NULL;
        }
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_DEACTIVATED_USER,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_DEACTIVATED_USER_LOGIN_ATTEMPT, array('username'=>$uname)),
            $applicationArr,
            $userDetails[0]['id'],
            $uname
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logLoginSuccessAuditDetails($uname,$id){
      //Log audit Details
      //$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGIN, array('username'=>$uname)),
        null);
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logInvalidIPAddressAuditDetails($uname,$id){
      //Log audit Details
      //$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_INVALID_IP,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_INVALID_IP, array('username'=>$uname)),
        null);
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logInvalidPasswordAndSECAnswerAuditDetails($uname,$error="",$answer=""){
      //Log audit Details
      //username is correct, get the user bank
      $applicationArr = NULL;
      $uid = NULL;
      $guser = Doctrine::getTable('sfGuardUser')->fetchUserDetails($uname);
      if(count($guser[0]['BankUser'])){
        $bank_id = $guser[0]['BankUser'][0]['bank_id'];
        $uid = $guser[0]['id'];
        $bank_name = $guser[0]['bank_name'];
        if($bank_id!=""){
          $branch_name = "";
        }
        $branch_id = $guser[0]['BankUser'][0]['branch_id'];
        if($branch_id!=""){
          $branch_name = $guser[0]['bank_branch_name'];
        }
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) ,new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME,$branch_name,$bank_id));
      }else{
        $applicationArr = NULL;
      }
      //get the user bank branch

      if($error)
      $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_SEC_ANSWER;
      else
      $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER;


      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER,
        EpAuditEvent::getFomattedMessage($msg, array('username'=>$uname,'answer'=>md5($answer))),
        $applicationArr,
        $uid,
        $uname
      );


      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
    private function redirectToUserHome($request,$txnId="")
    {

      $guser = $this->getUser();
      $group_name = $guser->getGroupNames();
      //check user is bank teller , if yes redirect to user bank home
      if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name)) {
        //if user is bank branch user(bank teller ) redirect it to bank home
        if($txnId == "") {
          return $this->redirectToBankHome($guser->getUsername());
        }
        else { //this is for NIPOST incase transaction location species the merchant counter
          return $this->redirectToPayment($guser->getUsername(),$txnId);
        }
      }
      else
      if(in_array(sfConfig::get('app_pfm_role_ewallet_user'),$group_name) || in_array(sfConfig::get('app_pfm_role_guest_user'),$group_name)) {
        $requestId = $request->getParameter('merchantRequestId');
        $paymentMode = $request->getParameter('payType');
        return $this->redirectToEwalletHome($guser->getUsername(),$requestId,$paymentMode);
      }
      if(in_array(sfConfig::get('app_pfm_role_bank_admin'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_bank_country_head'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_country_report_user'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_admin'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_dev'),$group_name) ||
        in_array(sfConfig::get('app_pfm_role_report_admin'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_report_portal_admin'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_report_bank_admin'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_support'),$group_name)  ||
        in_array(sfConfig::get('app_pfm_role_bank_e_auditor'),$group_name)||
        in_array(sfConfig::get('app_pfm_role_portal_support'),$group_name)||
        in_array(sfConfig::get('app_pfm_role_merchant'),$group_name)||
        in_array(sfConfig::get('app_pfm_role_sub_merchant'),$group_name)||
        in_array(sfConfig::get('app_pfm_role_bank_branch_report_user'),$group_name)) {

        //redirect user
        return $this->redirectToAdminHome($guser->getUsername());
      }
      $signinUrl = sfConfig::get('app_sf_guard_plugin_success_signin_url', $guser->getReferer($request->getReferer()));
      return $this->redirect('' != $signinUrl ? $signinUrl : '@homepage');
    }
    private function getUserSessionStatus($userName){
      $EpDBSessionDetailObj = new EpDBSessionDetail();
      $status = $EpDBSessionDetailObj->isSessionActiveByUsername($userName);
      if($status==false){

        $errors = "Someone already logged in by username '".$userName."'.";
        $this->getUser()->setFlash('error', $errors);
        $this->redirect('@homepage');
        return false;
      }else
      return true;

    }
    private function setFormHiddenValues($request){
      $formVal =  array();
      $formVal['payType'] = $request->getParameter('payType');
      $formVal['merchantRequestId'] = $request->getParameter('merchantRequestId');
      $formVal['payMode'] = $request->getParameter('payMode');
      $formVal['userType'] = $request->getParameter('userType');
      //$formVal['trans_num'] = $request->getParameter('trans_num');
      $this->formHiddenVal = $formVal;
    }

    private function chkEwalletUser(){
      $group_name = $this->getUser()->getGroupNames();
      if(in_array(sfConfig::get('app_pfm_role_ewallet_user'),$group_name)){
        return 1;
      }
      return 0;
    }

    private function redirectToQnA($request){
      //$this->getUser()->removeCredential('ewallet_user');
      $this->redirect('@qna');
    }
    //Called on ewallet come for payment from merchant application
    public function executeSignInOnlinePayment(sfWebRequest $request){
      $this->setLayout(false);
      $this->setTemplate('');
      //  $errMsg = $request->getParameter('errMsg');
      $group_name = $this->getUser()->getGroupNames();
      if ($this->getUser() && $this->getUser()->isAuthenticated()) {
      	if ($group_name && $group_name[0] !='guest_user' && $request->getParameter('userType') == 'existingEwallet') {
      		return $this->renderText('allready_login');
      	} else { 
      		$userName = sfContext::getInstance()->getUser()->getGuardUser()->getUserName();
      		Doctrine::getTable('AppSessions')->clearUserSessionByUsername($userName);
      	}
      }
      $this->getFormObj($request);
      $formVal = array();
      $this->accountType = '';
      $this->setLayout(false);
      if($request->getParameter('bankName')){
        $bankname = $request->getParameter('bankName');
        $this->redirect($bankname . '/login');
      }else if(($request->getParameter('payType') != strtolower(sfConfig::get('app_payment_mode_option_bank'))) && ($request->getParameter('payType')!="")){
        $formVal['payType'] =$request->getParameter('payType');
        $formVal['merchantRequestId'] = $request->getParameter('merchantRequestId');
         $this->payType =$request->getParameter('payType');
         $this->merchantRequestId = $request->getParameter('merchantRequestId');

        // this line is changed as first only username will asked
        $templateName = 'eWalletLoginFirst';
        $this->accountType = 'eWallet';
      }else if(count($this->getUser()->getAttribute('openId'))> 0 ){
         $this->accountType = 'eWallet';
         $templateName = 'eWalletLoginFirst';
      }else{
         $this->redirect('@homepage');
      }
      if ($request->isXmlHttpRequest()){
        $formVal['payMode'] = $request->getParameter('payMode');
      }
      else{
        $formVal['payMode'] = $request->getParameter('payType');
      }
      if ($request->getParameter('userType')!="") {
      	$this->userType = $request->getParameter('userType');
      	$request->setParameter('userType', $request->getParameter('userType'));
      	$formVal['userType'] = $request->getParameter('userType');
      }
      $this->formHiddenVal = $formVal;
      return $this->setTemplate($templateName);
    }
    public function executeRedirectToUserPage(sfWebRequest $request){
      $requestId = $request->getParameter('requestId');
      $payOptions = $request->getParameter('payOptions');
                            /*if($payOptions['payMode'] == "ewallet") {*/
      $this->redirect('@payRedirect?requestId='.$requestId.'&paymentMode='.$payOptions['payMode']);
                          /*  }
                            else if($payOptions['payMode'] == sfConfig::get('app_payment_mode_option_credit_card')) {
                               $this->redirect('@creditCardhome?requestId='.$requestId);
                            }
                            else if($payOptions['payMode'] == sfConfig::get('app_payment_mode_option_interswitch')) {
                               $this->redirect('@interswitchhome?requestId='.$requestId);
                            }*/
    }

    //needed for if in same browser we open two tabs , on of them portal admin is login and from another
    // tab we are going to login with ewallet, by that we got some fatal error ,
    //to resolve this problem we check group, and redirect to this action
    public function executeRedirectEwallet(sfWebRequest $request){
      if($request->getParameter('errMsg'))
      $errMsg = $request->getParameter('errMsg');

      $this->signoutOnError($errMsg);
      if($request->getParameter('requestId'))
      $request->setParameter('merchantRequestId', $request->getParameter('requestId'));

      $payType = $request->getParameter('payType');
      $payMode = $request->getParameter('payMode');
      // $request->setParameter($errMsg, $errMsg);
      $request->setParameter('payType', $payType);
      $request->setParameter('payMode', $payMode);
      $this->forward('sfGuardAuth','signInOnlinePayment');
    }
  }
  ?>
