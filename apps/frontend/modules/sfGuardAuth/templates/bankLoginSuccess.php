<?php use_stylesheet('bankLogin');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Admin:</title>

<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>

<!--[if IE 6]>
    <?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
  </head>
  <body>
<?php //echo ($form->renderGlobalErrors()); ?>
<?php echo include_partial('signinBankForm', array('form'=>$form,'bankname'=>$bankname)); /*,'isBankUser'=>$isBankUser*/?>


  </body>

</html>