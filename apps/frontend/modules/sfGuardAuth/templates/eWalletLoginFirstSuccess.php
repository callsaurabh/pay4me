<?php use_stylesheet('bankLogin'); ?>
<?php include_stylesheets() ?>
<?php //use_helper('Form'); ?>
<?php include_javascripts() ?>
<script>
	function getCenteredCoords(w, h) {
		var xPos = null;
		var yPos = null;
		if (window.ActiveXObject) {
			var xPos = (screen.width/2)-(w/2);
			var yPos = (screen.height/2)-(h/2);
		} else {
			var parentSize = [window.outerWidth, window.outerHeight];
			var parentPos = [window.screenX, window.screenY];
			xPos = parentPos[0] +
			Math.max(0, Math.floor((parentSize[0] - w) / 2));
			yPos = parentPos[1] +
			Math.max(0, Math.floor((parentSize[1] - (h*1.25)) / 2));
		}
		return [xPos, yPos];
	}
	function openOpenIdWindow(url, type){
		var width = '450';
		var height = '500';
		if(type == 'yahoo'){
			width = '500';
			height = '550';
		}else if(type == 'pay4me'){
			width = '440';
			height = '480';
		}
		var w = window.open(url, 'openid_popup', 'width='+width+',height='+height+',scrollbars=yes,resizable=no');
		var coords = getCenteredCoords(450,500);
		w.moveTo(coords[0],coords[1]);
	}
</script>
<?php 
            //PAYM-2032
            include_partial('global/gplusLogin');   
            $authURL = $_SESSION['authURL'];
            //End of PAYM-2032
?>
<br>
<?php if (!isset($formHiddenVal['userType']) || $formHiddenVal['userType']== 'existingEwallet') { ?>
	<?php $br = '';
	if ( isset($formHiddenVal['payType']) && (($formHiddenVal['payType'] == 'interswitch' || $formHiddenVal['payType'] == 'credit_card' || $formHiddenVal['payType'] == 'etranzact'))) {
		$br = "<br/>";
	?>
		<div align="center">
			<div class="cream_box">
				<div align="center" class="cream_box_heading">Why am i being asked to sign into the e-wallet for my card payment?</div>
				<div class="cream_box_text">Registering and signing into the Pay4Me e-wallet to use your card for payment is a security measure to help identify you the card holder performing this transaction. This is an effort to prevent any fraudulent use of card and to make card transactions traceable to the registered e-wallet owner at all times.</div>
			</div>
		</div>
	<?php } ?>
	<?php if(count(sfContext::getInstance()->getUser()->getAttribute('openId'))>0){
		$openIdDetails = sfContext::getInstance()->getUser()->getAttribute('openId');
		if(isset($openIdDetails['assocEmail'])){
			echo $br;
			?>
			<div align="center">
				<div class="cream_box">
					<div align="center" class="cream_box_heading">eWallet Account Association with <?php echo $openIdDetails['openIdType']; ?></div>
					<div class="cream_box_text">Please provide your eWallet Account Username to proceed with Association of your eWallet Account with <?php echo $openIdDetails['openIdType']; ?>.</div>
				</div>
			</div>
		<?php }
	} ?>
	<table align="center" cellpadding="0" cellspacing="0" >
		<tr>
			<td align="justify">
				<div id="wrapper-login" class="wrapLogin">
					<div style="padding-left:0px;">
						<?php echo image_tag('/images/' . $accountType . '.jpg', array('title' => "{$accountType}", 'alt' => "{$accountType}")); ?>
					</div>
					<?php if ($sf_user->hasFlash('notice')) {
						echo "<div id='flash_notice' class='error_list'><span>" . sfContext::getInstance()->getUser()->getFlash('notice') . "</span></div>";
					}
					if ($sf_user->hasFlash('error')) {
						echo "<div id='flash_error'><span>" . sfContext::getInstance()->getUser()->getFlash('error') . "</span></div>";
					}
					?>
					<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form">
						<div>
							<?php $hiddenFields = '';
							$i = 0;
							foreach ($form as $elm) {
								if ($elm->isHidden()) {
									$hiddenFields .= $elm->render();
								}
							}?>
							<div class="loginField">
								<label for="username">Username</label>
								<input type="text" name="signin[username]" id="signin_username" size="34" class="loginFieldInput"  />
								<span class='error' id='username_error'></span>
							</div>
							<?php echo $hiddenFields;
							foreach ($formHiddenVal as $k => $v) {?>
								<input type="hidden" name="<?php echo $k; ?>" id="<?php echo $k; ?>" value="<?php echo $v; ?>"><?php
							} ?>
							<div class="login-ffield">
								<input type="submit" value="Continue" id="button" class="formSubmit" name="button" onclick="return checkInput();"/>&nbsp;&nbsp;
								<br>&nbsp;
								<b><span class="HLogin">New to Pay4Me,&nbsp;
									<?php if(!sfConfig::get('app_disable_old_ewallet_registration')){?>
										<a href="<?php echo url_for('signup/index') ?>" >Register</a>.
									<?php } ?><?php echo link_to('Why Pay4Me?', url_for('signup/whypay4me'), array('popup' => array('popupWindow', 'width=780,height=250,left=140,top=0,scrollbars=yes'))); ?>
								</span></b>
							</div>
						</div>
					</form>
					<div class="loginPoweredby">
						<div id="poweredLogin"><a href="#">
							<?php echo image_tag("/img/poweredby.gif", array('alt' => "Powered by pay4me")); ?></a>
						</div>
					</div>
					<p></p>
					<div class="clearfix"></div>
					<?php if(count(sfContext::getInstance()->getUser()->getAttribute('openId'))==0){
						if(sfConfig::get('app_open_id_account_show')){?>
							<div class="ewallet_openid"><span>OR</span><br/>Users can also sign in with</div>
							<div class="ewallet_openid_img">
								<?php
								$payTypeUser = '';
								if (isset($formHiddenVal['userType'])) {
									$payTypeUser = '&userType=' . $userType ;
								}
								?>
                                                                <?php  //PAYM-2032
                                                                        $queryStringArr = array("merchantRequestId"=> $merchantRequestId, "payType"=>$payType, "userType" => $userType);
                                                                        $states = base64_encode(json_encode($queryStringArr));
                                                                        $authURL = $authURL."&state=".$states;  
                                                                        //END Of PAYM-2032
                                                                ?>
								<?php // echo image_tag("/images/google_ewallet.gif", array('alt' => "Google", 'title' => "Google", 'style' =>"cursor:pointer" , 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=google&merchantRequestId=' . $merchantRequestId . '&payType=' . $payType . $payTypeUser).'","google")'));
								       echo image_tag("/images/google_ewallet.gif", array('alt' => "Google", 'title' => "Google", 'style' =>"cursor:pointer" , 'onClick'=>'openOpenIdWindow("'.$authURL.'","google")'));
								echo '&nbsp;&nbsp;';
								echo image_tag("/images/yahoo_ewallet.gif", array('alt' => "Yahoo", 'title' => "Yahoo", 'style' =>"cursor:pointer", 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=yahoo&merchantRequestId='.$merchantRequestId.'&payType='.$payType . $payTypeUser).'","yahoo")'));
								echo '&nbsp;&nbsp;';
								echo image_tag("/images/fb_ewallet.gif", array('alt' => "Facebook", 'title' => "Facebook", 'style' =>"cursor:pointer", 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=facebook&merchantRequestId='.$merchantRequestId.'&payType='.$payType . $payTypeUser).'","facebook")'));?>
							</div>
						<?php }
					} ?>
				</div>
			</td>
		</tr>
	</table>
<?php }?>

<?php if (isset($formHiddenVal['userType']) && $formHiddenVal['userType'] != 'existingEwallet') {?>
<div class="openid_auth">
	<?php 
        //PAYM-2032
        // echo image_tag("/images/google_ewallet.gif", array('alt' => "Google",'title' => "Google", 'style' =>"cursor:pointer" , 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=google&merchantRequestId='.$merchantRequestId.'&payType='.$payType.'&userType='.$userType).'","google")'));
        $queryStringArr = array("merchantRequestId"=> $merchantRequestId, "payType"=>$payType, "userType" => $userType);
        $state = base64_encode(json_encode($queryStringArr));
        $authURL = $authURL."&state=".$state; 
        //END of PAYM-2032
        echo image_tag("/images/google_ewallet.gif", array('alt' => "Google",'title' => "Google", 'style' =>"cursor:pointer" , 'onClick'=>'openOpenIdWindow("'.$authURL.'","google")'));
	echo '&nbsp;&nbsp;';
	echo image_tag("/images/yahoo_ewallet.gif", array('alt' => "Yahoo",'title' => "Yahoo", 'style' =>"cursor:pointer", 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=yahoo&merchantRequestId='.$merchantRequestId.'&payType='.$payType.'&userType='.$userType).'","yahoo")'));
	echo '&nbsp;&nbsp;';
	echo image_tag("/images/fb_ewallet.gif", array('alt' => "Facebook",'title' => "Facebook", 'style' =>"cursor:pointer", 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=facebook&merchantRequestId='.$merchantRequestId.'&payType='.$payType.'&userType='.$userType).'","facebook")'));?>
</div>
<?php }?>

<script>
function checkInput(){ 

var usrName_errText="<font color='red'>Username can't be empty</font>";
if($.browser.msie)
{
usrName_errText="<br><br><font color='red'>Username can't be empty</font>";

}

if(document.getElementById('signin_username') && document.getElementById('signin_username').value==""){
document.getElementById('username_error').style.visibility = 'visible';
document.getElementById('username_error').innerHTML =   usrName_errText;
return false;
}else{
document.getElementById('username_error').innerHTML ='';
document.getElementById('username_error').style.visibility = 'hidden';
return true;
}
}


</script>
<script type="text/javascript">

if (top.location!= self.location) {
top.location = self.location.href
}

</script>
