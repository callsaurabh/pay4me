<script>
    function getCenteredCoords(w, h) {
        var xPos = null;
        var yPos = null;
        if (window.ActiveXObject) {
            var xPos = (screen.width/2)-(w/2);
            var yPos = (screen.height/2)-(h/2);
        } else {
            var parentSize = [window.outerWidth, window.outerHeight];
            var parentPos = [window.screenX, window.screenY];
            xPos = parentPos[0] +
                Math.max(0, Math.floor((parentSize[0] - w) / 2));
            yPos = parentPos[1] +
                Math.max(0, Math.floor((parentSize[1] - (h*1.25)) / 2));
        }
        return [xPos, yPos];
    }
    function openOpenIdWindow(url, type){
        var width = '450';
        var height = '500';
        if(type == 'yahoo'){
            width = '500';
            height = '550';
        }else if(type == 'pay4me'){
            width = '440';
            height = '480';
        }
        var w = window.open(url, 'openid_popup', 'width='+width+',height='+height+',scrollbars=yes,resizable=no');
        var coords = getCenteredCoords(450,500);
        w.moveTo(coords[0],coords[1]);
    }
    </script>
<?php //use_helper('Form');?>
<br>
<style>
.keypad-popup{width:380px!important;}
</style>

<?php
$br = '';
if(isset($formHiddenVal['payType']) && ($formHiddenVal['payType']=='interswitch' || $formHiddenVal['payType']=='credit_card' || $formHiddenVal['payType']=='etranzact')) { $br = "<br/>"; ?>
 <div align="center">
   <div class="cream_box">
         <div align="center" class="cream_box_heading">Why am i being asked to sign into the e-wallet for my card payment?</div>
         <div class="cream_box_text">Registering and signing into the Pay4Me e-wallet to use your card for payment is a security measure to help identify you the card holder performing this transaction. This is an effort to prevent any fraudulent use of card and to make card transactions traceable to the registered e-wallet owner at all times.</div>
     </div></div>
<?php } ?>

<?php
$buttonValue = 'Login';
        if(count(sfContext::getInstance()->getUser()->getAttribute('openId'))>0){
            $openIdDetails = sfContext::getInstance()->getUser()->getAttribute('openId');
            if(isset($openIdDetails['assocEmail']))
                $messageShow = "Please login with your eWallet account credentials to associate it with ".$openIdDetails['openIdType'].".";
            else
                $messageShow = "You are already registered on Pay4me. Please login with your eWallet account credentials to associate it with ".$openIdDetails['openIdType'].".";
            echo $br;
            ?>          
               <div align="center">
   <div class="cream_box">
         <div align="center" class="cream_box_heading">eWallet Account Association with <?php echo $openIdDetails['openIdType']; ?></div>
         <div class="cream_box_text"><?php echo $messageShow; ?></div>
     </div></div>
         <?php   }
            ?>
<table align="center" cellpadding="0" cellspacing="0" >
  <tr>
    <td align="justify">
      <div id="wrapper-login" class="wrapLogin">
        <div style="padding-left:0px;">
        
          <?php echo image_tag('/images/'.$accountType.'.jpg',array('title'=>"{$accountType}", 'alt'=>"{$accountType}"));?>
        </div>
       
        <?php
        if($sf_user->hasFlash('notice')){
          echo "<div id='flash_notice' class='error_list_user'><span>".sfContext::getInstance()->getUser()->getFlash('notice')."</span></div>";
        }
        if($sf_user->hasFlash('error')){
          echo "<div id='flash_error' class='error_list_user' ><span>".sfContext::getInstance()->getUser()->getFlash('error')."</span></div>";
        }
        ?>

        <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form">
        
          <div>
         
            <?php


            $hiddenFields ='';
            $i = 0;
            foreach($form as $elm)
            {
              if($elm->isHidden())
              {
                $hiddenFields .= $elm->render();
              }else{
                echo "<div class='loginField'>";
                //  print_r($elm->getWidget()->getAttributes());
                 $hasAttributeClass = $elm->getWidget()->getAttribute('class');
                echo $elm->renderLabel().' '.$elm->render(array('class'=>'loginFieldInput '.$hasAttributeClass, 'style' => '*padding:0px; _padding:0px;')) ;

                if($elm->hasError()) {
                  echo "<span class='error'>";
                  $er = $elm->getError();
                  echo "</span>";

                }
                echo "<span class='error-sign' id='".++$i."'></span>";
              }

            }
            if(count(sfContext::getInstance()->getUser()->getAttribute('openId'))>0){
                $buttonValue = 'Associate';
               echo '<br><br><input type="checkbox" name="openid_authenticate" id="openid_authenticate" tite="Openid Authenticate"> I authorize pay4me to authenticate my pay4me account using '.$openIdDetails['openIdType'].' credentials.';
                  echo "<span class='error' id = 'openid_auth_erro' style='display:none;'>";
                  echo "</span>";
            }
            echo "</div>";
            echo $hiddenFields ;
            if(isset($formHiddenVal)){
                foreach($formHiddenVal as $k=>$v){
                  ?><input type="hidden" name="<?php echo $k;?>" id="<?php echo $k;?>" value="<?php echo $v;?>"><?php
                  //echo input_hidden_tag($k,$v);
                }
            }
            
            ?>
            <div class="login-ffield">
             <div class="login_btn_ewallet"> <input type="submit" value="<?php echo $buttonValue; ?>" id="button" class="formSubmit" name="button" onclick="return checkInput();"/>&nbsp;&nbsp; <?php  /*if ($isBankUser){*/ ?><!-- <input type="submit" value="Request Password Reset" id="button" class="formSubmit" name="request_password_reset"/> --><?php  /*}*/ ?></div>
			  <div class="frgt_pass"><a href="<?php echo url_for('@forgotPassword')?>" target="_blank">Forgot Password</a></div>
			  <div class="clearfix"></div>
    
              <br>
              <!--// [CR047]-->
              <b><span class="HLogin">New to Pay4Me,&nbsp;
              <?php if(!sfConfig::get('app_disable_old_ewallet_registration')){?>
              <a href="<?php echo url_for('signup/index')?>" >Register.</a>
              <?php } ?><?php echo link_to('Why Pay4Me?',url_for('signup/whypay4me'), array('popup' => array('popupWindow','width=780,height=250,left=140,top=0,scrollbars=yes') )); ?></span>
              </b>
            </div>
          </div>

        </form>
        <div class="loginPoweredby">
          <div id="poweredLogin"><a href="#">
          <?php echo image_tag("/img/poweredby.gif",array('alt'=>"Powered by pay4me")); ?></a></div>
        </div>
<p></p>
                <div class="clearfix"></div>
                <?php if(count(sfContext::getInstance()->getUser()->getAttribute('openId'))==0){
                if(sfConfig::get('app_open_id_account_show')){
                    ?>
                <div class="ewallet_openid"><span>OR</span><br/>Users can also sign in with</div>
                <div class="ewallet_openid_img"><?php
                echo image_tag("/images/google_ewallet.gif", array('alt' => "Google",'title' => "Google", 'style' =>"cursor:pointer" , 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=google').'","google")'));
                echo '&nbsp;&nbsp;&nbsp;';
                echo image_tag("/images/yahoo_ewallet.gif", array('alt' => "Yahoo",'title' => "Yahoo", 'style' =>"cursor:pointer" , 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=yahoo').'","yahoo")'));
                echo '&nbsp;&nbsp;&nbsp;';
                echo image_tag("/images/fb_ewallet.gif", array('alt' => "Facebook",'title' => "Facebook", 'style' =>"cursor:pointer" , 'onClick'=>'openOpenIdWindow("'.url_for('openId/authlogin?openid_identifier=facebook').'","facebook")')); ?>

</div>
<?php }} ?>
      </div>
    </td>
  </tr>
</table>


<script>
  function checkInput(){
      var err = 0;
    if(document.getElementById('flash_error') && document.getElementById('flash_error').value!="")
      document.getElementById('flash_error').innerHTML = '';
    if(document.getElementById('signin_password') && document.getElementById('signin_password').value==""){
      document.getElementById('2').style.visibility = 'visible';
      document.getElementById('2').innerHTML =   "<font color='red'>Password can't be empty</font>";
      err++;
    }else{
      document.getElementById('2').innerHTML ='';
      document.getElementById('2').style.visibility = 'hidden';
    }
    if(document.getElementById('signin_username') && document.getElementById('signin_username').value==""){
      document.getElementById('1').style.visibility = 'visible';
      document.getElementById('1').innerHTML =   "<font color='red'>Username can't be empty</font>";
      err++;
    }else{
      document.getElementById('1').innerHTML ='';
      document.getElementById('1').style.visibility = 'hidden';
    }
    if(document.getElementById('openid_authenticate')){
        if(document.getElementById('openid_authenticate').checked==false){
            document.getElementById('openid_auth_erro').style.display = '';
            document.getElementById('openid_auth_erro').innerHTML =   "<font color='red'>Please accept the authorization</font>";
            err++;
        }
        else{
            document.getElementById('openid_auth_erro').innerHTML ='';
            document.getElementById('openid_auth_erro').style.display = 'none';
        }
    }    
    if(err>0)
        return false;
    else
        return true;
  }
</script>

<script>
 $(document).ready(function()
 {
   // Bug: 16850
   // 1. if we select payemnt mode option visa/interswith ,then due to message text coming up on the page, virtual keyboard postion disturbed, so calculating dynamic offsets of password field and placing keyboard just below this new position
  
   var appname = window.navigator.appName;
   var addheight = 0;
   if(appname == 'Microsoft Internet Explorer'){
       addheight = 18;
   }else{
       addheight = 28;
   }
    enablePasswordKeypad('signin_password','<br><div style="padding-top:7px;">','</div>',null,addheight);
    
    // Bug: 16850
    // 1. if we select payemnt mode option visa/interswith ,then due to message text coming up on the page, virtual keyboard postion disturbed, so calculating dynamic offsets of password field and placing keyboard just below this new position
   
 });

</script>
