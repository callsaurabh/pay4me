<?php use_stylesheet('bankLogin'); ?>
<?php include_stylesheets() ?>
<?php include_javascripts() ?>

<div class="innerWrapper">
    <div align="center">
        <div class="cream_box">
            <div align="center" class="cream_box_heading">Email id "<?php echo $openIdDetails['email']; ?>" is not registered on Pay4me</div>
            <div class="cream_box_text">Kindly choose one of the below options to proceed further.</div>
        </div>        
    </div>

    <table align="center" cellpadding="0" cellspacing="0" >
        <tr>
            <td align="justify">
                <div id="wrapper-login" class="wrapLogin_new">
<!--                    <div style="padding-left:0px;">

                        <?php //echo image_tag('/images/'.$accountType.'.jpg',array('title'=>"{$accountType}", 'alt'=>"{$accountType}"));?>
                    </div>-->
                    <form action="<?php echo url_for('openId/associateOrCreateNewOpenAccountOrRecover') ?>" method="post" class="form">
                        <div>
                            <input type="hidden" name="type" id="type">


                            <input type="hidden" name="merchantRequestId" value="<?php echo isset($merchantRequestId) ? $merchantRequestId : ''; ?>">
                            <input type="hidden" name="payType" value="<?php echo isset($payType) ? $payType : ''; ?>">

                            <div class="ewallet-login-wrap">
                                <?php if (!sfConfig::get('app_disable_ewallet_registration')) { ?>
                                    <div class="ewallet-box" align="center"><h2>Create new Account</h2> <p style="text-align:center;"> Create account with pay4me and start making payments online.<br/><br/> <input type="submit" value="Create" id="button" class="formSubmit" name="button" onclick="$('#type').val('createNewAccount');"/> <br></p>  </div>

                                <?php } else {// patch code to disable ewallet registration [WP076] ?>
                                    <div class="ewallet-box" align="center"><h2>Create new Account</h2> <p style="text-align:center;"> <font color='red'>New ewallet signup has been temporarily suspended and will be resumed shortly. We regret the inconvenience caused and expect your cooperation.</font><br/><br/>  <br></p>  </div>
                                <?php } ?>
                                <div class="ewallet-box"  align="center"> <h2>Associate with existing account</h2> <p style="text-align:center;"> Map with your existing eWallet account(s).<br/> <br/><input type="submit" value="Associate" id="button" class="formSubmit" name="button" onclick="$('#type').val('ewalletAssociation');"/>  <br> </p> </div>
                                <div class="ewallet-box"  align="center"> <h2>Recover eWallet account</h2> <p style="text-align:center;"> Recover your eWallet account(s) mapped with Google/Yahoo/Facebook account. <br/><br/> <input type="submit" value="Recover" id="button" class="formSubmit" name="button" onclick="$('#type').val('recoverEwalletAccount');"/>  <br></p>  </div>
                            </div>


                            <!--div class="login-ffield-associate">
                                         <div class="login_btn_associate" align="center"> <input type="submit" value="Create new Account" id="button" class="formSubmit" name="button" onclick="$('#type').val('createNewAccount');"/></div>
                                                    <div class="pos-center">  <center>OR</center></div>
                                          <div class="login_btn_associate"  align="center"> <input type="submit" value="Associate with existing Account" id="button" class="formSubmit" name="button" onclick="$('#type').val('ewalletAssociation');"/></div>
                                                     <div class="pos-center">  <center>OR</center></div>
                                          <div class="login_btn_associate"  align="center"> <input type="submit" value="Recover eWallet Account" id="button" class="formSubmit" name="button" onclick="$('#type').val('recoverEwalletAccount');"/></div>
                                                      <div class="clearfix"></div>
                            
                            
                                          <br>           
                                        </div-->
                        </div>

                    </form>
                            <div class="loginPoweredby">
                              <div id="poweredLogin">
                                  <a href="#">
                    <?php echo image_tag("/img/poweredby.gif",array('alt'=>"Powered by pay4me")); ?>
                                  </a>
                              </div>
                            </div>
                    <!--
                    
                            <p> </p>-->
                </div>
            </td>
        </tr>
    </table>
</div>

