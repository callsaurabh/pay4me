<?php //use_helper('Form');?>
<div id="wrapper-login" class="wrapLogin">
    <div class="loginLogo">
        <?php echo image_tag('/images/'.$bankname.'.jpg',array('title'=>"{$bankname}", 'alt'=>"{$bankname}",'width'=>'250px','height'=>'50px'));?>
    </div>

    <?php
    //echo sfContext::getInstance()->getUser()->getFlash('error');
    if($sf_user->hasFlash('error')){
        echo "<div id='flash_error' class='error_list_user'><span>".sfContext::getInstance()->getUser()->getFlash('error')."</span></div><br />";
    }    
    ?>

    <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form">
        <div>
            <?php


            $hiddenFields ='';
            $i = 0;
            foreach($form as $elm)
            {
                if($elm->isHidden())
                {
                    $hiddenFields .= $elm->render();
                }else{
                    echo "<div class='loginField' >";
                  //  print_r($elm->getWidget()->getAttributes());
                    $hasAttributeClass = $elm->getWidget()->getAttribute('class');
                    echo $elm->renderLabel().' '.$elm->render(array('class'=>'loginFieldInput '.$hasAttributeClass)) ;
//                    if($elm->hasError()) {
//                        echo "<span class='error' >";
//                        $er = $elm->getError();
////                        if ($er instanceof sfValidatorErrorSchema){
////                            echo $er[0];
////                        } else {
////                            echo $er;
////                        }
//                        echo "</span>";
//                    }
                    //echo $elm->renderRow() ;
                        echo "<span class='error-sign' id='".++$i."'></span></div>";
                }

            }
            echo $hiddenFields ;
            ?><input type="hidden" name="bankName"  id="bankName" value="<?php echo $bankname;?>"><?php
             //echo input_hidden_tag('bankName',$bankname);
            if($sf_request->hasParameter('txnId')){
              ?><input type="hidden" name="txnId" id="txnId" value="<?php echo $sf_request->getParameter('txnId');?>"><?php
              //echo input_hidden_tag('txnId',$sf_request->getParameter('txnId'));
            }

            ?>
            <div class="login-ffield">
                <input type="submit" value="Login" id="button" class="formSubmit" name="button" onclick="return checkInput();"/>&nbsp;&nbsp; <?php  /*if ($isBankUser){*/ ?><!-- <input type="submit" value="Request Password Reset" id="button" class="formSubmit" name="request_password_reset"/> --><?php  /*}*/ ?>
                <div class="frgt_pass"><a href="<?php echo url_for('@forgotPassword')?>" target="_blank">Forgot Password</a></div>
                <div class="clearfix"></div>
            </div>
        </div>

    </form>



         

    <?php /*

                <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form" id="form" name="form">
                    <div/>
                    <div class="loginField">
                        <label for="username">Username</label>
                        <input type="text" name="signin[username]" size="34" class="loginFieldInput"  />
                    </div>
                    <div class="loginField">
                        <label for="username2">Password</label>
                        <input type="password" name="signin[password]" size="34" class="loginFieldInput"  />
                        <input type="hidden" name="bankname" value="<?php echo $bankname;?>">

                    </div>
                    <div class="login-ffield">
                        <input type="submit" value="Login" id="button" class="formSubmit" name="button"/>
                    </div>
                </form>
                <?php } */ ?>
    <div class="loginPoweredby">
        <div id="poweredLogin"><a href="#">
        <?php echo image_tag("/img/poweredby.gif",array('alt'=>"Powered by pay4me")); ?></a></div>
    </div>
    <p> </p>
</div>
<script>
    function checkInput(){
        if(document.getElementById('flash_error') && document.getElementById('flash_error').value!="")
            document.getElementById('flash_error').innerHTML = '';
        if(document.getElementById('signin_password') && document.getElementById('signin_password').value==""){            
            document.getElementById('2').style.visibility = 'visible';
            document.getElementById('2').innerHTML = "Password can't be empty";
            return false;
        }else{
            document.getElementById('2').innerHTML ='';
            document.getElementById('2').style.visibility = 'hidden';
            return true;
        }
    }
</script>


<script>
 $(document).ready(function()
 {
    var appname = window.navigator.appName;
    var addheight = 0;
    if(appname == 'Microsoft Internet Explorer'){
        addheight = 28;
    }else{
        addheight = 30;
    }
    enablePasswordKeypad('signin_password','<br><div style="padding-top:7px;">','</div>',null,addheight);
 });

</script>


