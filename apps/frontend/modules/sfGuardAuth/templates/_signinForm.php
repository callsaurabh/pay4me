    <script>
    function getCenteredCoords(w, h) {
        var xPos = null;
        var yPos = null;
        if (window.ActiveXObject) {
            var xPos = (screen.width/2)-(w/2);
            var yPos = (screen.height/2)-(h/2);
        } else {
            var parentSize = [window.outerWidth, window.outerHeight];
            var parentPos = [window.screenX, window.screenY];
            xPos = parentPos[0] +
                Math.max(0, Math.floor((parentSize[0] - w) / 2));
            yPos = parentPos[1] +
                Math.max(0, Math.floor((parentSize[1] - (h*1.25)) / 2));
        }
        return [xPos, yPos];
    }
    function openOpenIdWindow(url, type){
        var width = '450';
        var height = '500';
        if(type == 'yahoo'){
            width = '500';
            height = '550';
        }else if(type == 'pay4me'){
            width = '440';
            height = '480';
        }
        var w = window.open(url, 'openid_popup', 'width='+width+',height='+height+',scrollbars=yes,resizable=no');
        var coords = getCenteredCoords(450,500);
        w.moveTo(coords[0],coords[1]);
    }
    </script>
<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
<div>
            <div class="redbox_heading">Sign in</div>
          </div>
		  <?php
if($sf_user->isAuthenticated()){
echo $sf_user->getUsername();}else{ ?>

<div id="signinArea">
            Please log in with your assigned Username and Password in the fields below.<br />
            <form id="form" name="form3" method="post" action="">
              <div class="fieldwrap-left">
                <label><input type="text" name="chkUsername" size="34" class="field" id="chkUsername" value="Username" /><input type="text" name="signin[username]" size="34" class="field" id="textfield1" value="" /></label>
              </div>
              <div class="fieldwrap">
                <label><input type="text" name="chkPassword" size="34" class="field" id="chkPassword" value="Password" /><input  type="password" name="signin[password]" size="34" class="field" id="textfield" value="" />
                <input type="hidden" name="bankname" value="<?php echo $bankname;?>"></label>
              </div>
            
              <div class="buttonwrap" id="button">
              <label class="forgot_pass_home">
                                    <a href="<?php echo url_for('@forgotPassword')?>" target="_blank">Forgot Password</a></label>
                <label class="login_btn">

				<input name="button" type="submit" class="submit" id="button" value="Login"/>

                </label>
				
              </div>
               <div class="clearfix"></div>
               <div class="clearfix"></div>
            </form>
          </div>

<?php } ?>



</form>
<script>
    $(document).ready(function()
    {
        //username
        $('#textfield1').hide();
        $('#chkUsername').focus(function() {

            $('#chkUsername').hide();
            $('#textfield1').show().focus();

        });
        $('#textfield1').blur(function() {
            
            if ($('#textfield1').val() == '') {
                $('#chkUsername, #textfield1').toggle();
            }
        });
        //password
        $('#textfield').hide();
        $('#chkPassword').focus(function() {
           // alert('call onfocus');
            $('#chkPassword').hide();
            $('#textfield').show().focus();

        });
        $('#textfield').blur(function() {
            if ((!$('#virtualcheck').is(":checked")) && $('#textfield').val() == '') {
                $('#chkPassword, #textfield').toggle();
            }
        });
        var space_separater = '';
        var addheight = 0;
        var appname = window.navigator.appName;
        if(appname == 'Microsoft Internet Explorer'){
            // space_separater = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; // IE7
            addheight = 29;
        }else{
            // space_separater = '&nbsp;&nbsp;&nbsp;&nbsp;'; // firefox 3.6.28
            addheight = 30;
        }
        enablePasswordKeypadLogin('textfield','<br><div style="padding-top:7px; width:323px; text-align:right;">','</div>',space_separater,addheight);

    });

function enablePasswordKeypadLogin(txtpwd,beforehtml,afterhtml,nbsptext,addheight){
    // to know which id is visible of password field , to calculate actual offset
    if($('#chkPassword').is(':visible'))
        var textbox = jQuery("#chkPassword");
    else
        var textbox = jQuery("#textfield");
    var offset = textbox.offset();
    if(beforehtml==null){ beforehtml="";}
    if(afterhtml==null){ afterhtml="";}
    if(nbsptext==null){ nbsptext="";}
    $('#'+txtpwd).after(beforehtml+'<input type="checkbox" name="virtualcheck" id="virtualcheck" tite="Use Virtual Keyboard"> Use Virtual '+nbsptext+'Keyboard'+afterhtml);
    $('#virtualcheck').change(function() {
        
        if($(this).is(":checked")){
            //alert('is checked');
            if($('#chkPassword').is(':visible')){
               $('#textfield, #chkPassword').toggle();
            }
            $('#'+txtpwd).val('');
            $('#'+txtpwd).keypad({
                showOn: 'focus',
                duration:'fast',
                layout: $.keypad.qwertyLayout,
                randomiseNumeric: true,
                randomiseAlphabetic:true,
                randomiseOther: true,
                keypadOnly:false,
                onClose: function(){   if ($('#textfield').is(':visible') && $('#textfield').val() == '') {  $('#chkPassword, #textfield').toggle(); } }
            });
            $('#'+txtpwd).focus();
            $('#virtualcheck').attr('Checked','Checked');
            $('#'+txtpwd).attr("readonly", "readonly");
            $('.keypad-popup').focus(); // [Wp063] Bug:15579 , 01-03-2012
            $(".keypad-popup").css({"left":(offset.left)+0,"top":(offset.top)+addheight});
        }else{
            var to_confirm=confirm("Virtual keyboard is recommended to protect your password. Are you sure you want to use normal keyboard?");
            if(to_confirm){
                $('#'+txtpwd).val('');
                $('#'+txtpwd).attr("readonly", "");
                $('#'+txtpwd).keypad('destroy');
                if($('#textfield').is(':visible')){
                   $('#textfield, #chkPassword').toggle();
                }
            }else{
                $('#'+txtpwd).val('');
                $('#'+txtpwd).keypad({
                    showOn: 'focus',
                    duration:'fast',
                    layout: $.keypad.qwertyLayout,
                    randomiseNumeric: true,
                    randomiseAlphabetic:true,
                    randomiseOther: true,
                    keypadOnly:false,
                    onClose: function(){   if ($('#textfield').is(':visible') && $('#textfield').val() == '') {  $('#chkPassword, #textfield').toggle(); } }
                });
                $('#'+txtpwd).focus();
                $('#virtualcheck').attr('checked','checked');
                $('#'+txtpwd).attr("readonly", "readonly");
                $('.keypad-popup').focus(); // [Wp063] Bug:15579 , 01-03-2012
                $(".keypad-popup").css({"left":(offset.left)+0,"top":(offset.top)+addheight});
            }
        }
    });
    $("#"+txtpwd).click(function(){
        textbox = jQuery("#"+txtpwd);
        offset = textbox.offset();
        $(".keypad-popup").css({"left":(offset.left)+0,"top":(offset.top)+addheight});
    });
}
</script>