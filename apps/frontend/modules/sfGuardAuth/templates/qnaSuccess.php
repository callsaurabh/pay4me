<?php //use_helper('Form');?>
<?php use_stylesheet('bankLogin');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Admin:</title>

<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>

<!--[if IE 6]>
    <?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
  </head>
  <body>


<div id="wrapper-login" class="wrapLogin">
    <div class="loginLogo">
        <?php echo image_tag('/images/'.$bankname.'.jpg',array('title'=>"{$bankname}", 'alt'=>"{$bankname}",'width'=>'250px','height'=>'50px'));?>
    </div>

    <?php 
    //echo sfContext::getInstance()->getUser()->getFlash('error');
    if($sf_user->hasFlash('error')){
        echo "<div id='flash_error'><span>".sfContext::getInstance()->getUser()->getFlash('error')."</span></div>";
    }
    ?>

    <form action="<?php echo url_for('sfGuardAuth/qna') ?>" method="post" class="form">
        <div>
            <?php


            $hiddenFields ='';

            foreach($qnaForm as $elm)
            {
                if($elm->isHidden())
                {
                    $hiddenFields .= $elm->render();
                }else{
                    echo "<div class='loginField'>";
                  //  print_r($elm->getWidget()->getAttributes());
                    $hasAttributeClass = $elm->getWidget()->getAttribute('class');
                    echo $elm->renderLabel().' '.$elm->render(array('class'=>'loginFieldInput '.$hasAttributeClass)) ;
                    if($elm->hasError()) {
                        echo "<span class='error'>";
                        $er = $elm->getError();
                        if ($er instanceof sfValidatorErrorSchema){
                            echo $er[0];
                        } else {
                            echo $er;
                        }
                        echo "</span>";
                    }
                    //echo $elm->renderRow() ;
                    echo "</div>";
                }

            }
            echo $hiddenFields ;
            ?><input type="hidden" value="<?php echo $bankname;?>"  name="bankName" id="bankName" /><?php
            //echo input_hidden_tag('bankName',$bankname);
            ?>
            <div class="login-ffield">
                <input type="submit" value="Continue" id="button" class="formSubmit" name="button"/>
            </div>
        </div>

    </form>



    <?php /*

                <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form" id="form" name="form">
                    <div/>
                    <div class="loginField">
                        <label for="username">Username</label>
                        <input type="text" name="signin[username]" size="34" class="loginFieldInput"  />
                    </div>
                    <div class="loginField">
                        <label for="username2">Password</label>
                        <input type="password" name="signin[password]" size="34" class="loginFieldInput"  />
                        <input type="hidden" name="bankname" value="<?php echo $bankname;?>">

                    </div>
                    <div class="login-ffield">
                        <input type="submit" value="Login" id="button" class="formSubmit" name="button"/>
                    </div>
                </form>
                <?php } */ ?>
    <div class="loginPoweredby">
        <div id="poweredLogin"><a href="#">
        <?php echo image_tag("/img/poweredby.gif",array('alt'=>"Powered by pay4me")); ?></a></div>
    </div>
    <p> </p>
</div>


  </body>

</html>