<div id="theMid">
<?php use_helper('Pagination');  ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php echo ePortal_listinghead('eWallet Recharge Service Charges List'); ?>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th width="100%" >
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable">

<table  width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <thead>
        <tr class="horizontal">
            <th>Payment Mode Option</th>
            <th>Charge Amount / Percent</th>
            <th>Charge Type</th>
            <th>Upper Slab</th>
            <th>Lower Slab</th>
            <th>Action</th>

        </tr>
    </thead>
    <tbody>

        <?php
        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
            foreach ($pager->getResults() as $ewallet_service_charges):
            $i++;
            ?>

        <tr  class="alternateBgColour">
            <td><?php echo $ewallet_service_charges->getPaymentModeOption()->getPaymentMode()->getDisplayName() ?></td>
            <td><?php echo $ewallet_service_charges->getChargeAmount() ?></td>
            <td><?php echo $ewallet_service_charges->getChargeType() ?></td>
            <td><?php if($ewallet_service_charges->getChargeType()=='percent'){ echo $ewallet_service_charges->getUpperSlab(); } else {echo "--"; } ?></td>
            <td><?php if($ewallet_service_charges->getChargeType()=='percent'){ echo $ewallet_service_charges->getLowerSlab(); } else {echo "--"; } ?></td>
            <td ><?php echo link_to(' ', 'servicecharge/edit?id='.$ewallet_service_charges->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
            <?php echo link_to(' ', 'servicecharge/delete?id='.$ewallet_service_charges->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?', 'class' => 'delInfo', 'title' => 'Delete')) ?></td>
        </tr>

      <?php endforeach; ?>
        
        <tr><td colspan="7">
                <div class="paging pagingFoot">
                    <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid') ?>

        </div></td></tr>
        <?php }else{ ?>
        <tr><td  align='center' class='error' colspan="6">No eWallet service charges List found</td></tr>
        <?php } ?>
    </tbody>
    </tbody>
</table>

<?php  //echo button_to('Add New','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('servicecharge/new').'\''));?>
</div>
</div>


