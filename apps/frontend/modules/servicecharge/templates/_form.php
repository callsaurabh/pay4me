


<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form') ?>
<div class="wrapForm2" >
    <form action="<?php echo url_for('servicecharge/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post"  <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
        <?php if (!$form->getObject()->isNew()): ?>
           <input type="hidden" name="sf_method" value="put" />
        <?php endif; ?>
         <?php   echo ePortal_legend('Edit eWallet Recharge Service Charges'); ?>
         <?php echo $form; ?>
         <div class="divBlock">
            <center>
                <?php echo $form->renderHiddenFields(false) ?>
                        &nbsp;<?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('servicecharge/index').'\''));?>
                        <input type="submit"  class="formSubmit" value="Save" />
            </center>
        </div>

    </form>
</div>



<script language="javascript">

  $(document).ready(function() {
    if($("#ewallet_service_charges_charge_type").val()=='flat'){
        $('#ewallet_service_charges_upper_slab_row').hide();
        $('#ewallet_service_charges_lower_slab_row').hide();
    }

 });



 function showhide(typeValue)
 {
        var chargeType;
        chargeType =typeValue.value;
         $("#ewallet_service_charges_upper_slab").val('');
         $("#ewallet_service_charges_lower_slab").val('');
         $("#ewallet_service_charges_charge_amount").val('');
        if(chargeType =='flat'){

            $('#ewallet_service_charges_upper_slab_row').hide();
            $('#ewallet_service_charges_lower_slab_row').hide();

        }else{
           
            $('#ewallet_service_charges_upper_slab_row').show();
            $('#ewallet_service_charges_lower_slab_row').show();

        }

   }
 </script>


