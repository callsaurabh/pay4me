<?php

/**
 * servicecharge actions.
 *
 * @package    mysfp
 * @subpackage servicecharge
 * @author     ryadav
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 * @WP034 - CR054
 */
class servicechargeActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    

    $this->ewallet_service_chargess = Doctrine::getTable('EwalletServiceCharges')->getAllRecords();
    $this->page = 1;
    if($request->hasParameter('page')) {
        $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('EwalletServiceCharges',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->ewallet_service_chargess);
    $this->pager->setPage($this->page);
    $this->pager->init();

  }

  public function executeNew(sfWebRequest $request)
  {

    $this->form = new EwalletServiceChargesForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new EwalletServiceChargesForm();
   
    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($ewallet_service_charges = Doctrine::getTable('EwalletServiceCharges')->find(array($request->getParameter('id'))), sprintf('Object ewallet_service_charges does not exist (%s).', $request->getParameter('id')));
    $ewallet_service_charges = Doctrine::getTable('EwalletServiceCharges')->find($request->getParameter('id'));
    $payment_mode_option_id = $ewallet_service_charges->getPaymentModeOptionId();
  
    $payment_mode_obj = Doctrine::getTable('PaymentModeOption')->find($payment_mode_option_id);
    $payment_mode_id = $payment_mode_obj->getPaymentModeId();
    $payment_mode = Doctrine::getTable('PaymentMode')->find($payment_mode_id);
    $displayName = $payment_mode->getDisplayName();
   
    $this->form = new EwalletServiceChargesForm($ewallet_service_charges,array('displayName'=>$displayName));
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($ewallet_service_charges = Doctrine::getTable('EwalletServiceCharges')->find(array($request->getParameter('id'))), sprintf('Object ewallet_service_charges does not exist (%s).', $request->getParameter('id')));
    $ewallet_service_charges = Doctrine::getTable('EwalletServiceCharges')->find($request->getParameter('id'));
  
    $payment_mode_option_id = $ewallet_service_charges->getPaymentModeOptionId();
    $payment_mode_obj = Doctrine::getTable('PaymentModeOption')->find($payment_mode_option_id);
    $payment_mode_id = $payment_mode_obj->getPaymentModeId();
    $payment_mode = Doctrine::getTable('PaymentMode')->find($payment_mode_id);
    $displayName = $payment_mode->getDisplayName();
    $this->form = new EwalletServiceChargesForm($ewallet_service_charges,array('displayName'=>$displayName));
    
   
    $this->processForm($request, $this->form);
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($ewallet_service_charges = Doctrine::getTable('EwalletServiceCharges')->find(array($request->getParameter('id'))), sprintf('Object ewallet_service_charges does not exist (%s).', $request->getParameter('id')));
    $ewallet_service_charges->delete();
    $this->getUser()->setFlash('notice', sprintf('eWallet recharge service charge deleted successfully'));
    $this->redirect('servicecharge/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
   
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
             
        $ewallet_service_charges = $form->save();
        if(($request->getParameter('action')) == "update") {
        $this->getUser()->setFlash('notice', sprintf('eWallet recharge service charge updated successfully'));
        }
        else if(($request->getParameter('action')) == "create") {
        $this->getUser()->setFlash('notice', sprintf('eWallet recharge service charge saved successfully'));
        }


        $this->redirect('servicecharge/index');
    }
   
  }
}
