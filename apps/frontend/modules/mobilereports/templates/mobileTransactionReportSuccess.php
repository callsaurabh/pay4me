<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form') ?>

<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/mobileTransactionReport',array('name'=>'search','class'=>'', 'method'=>'post','id'=>'search')) ?>
    
    <?php echo ePortal_legend('Mobile eWallet Transaction Report');?>
     <?php echo $form;?>
    <div class="divBlock">
    <center id="multiFormNav">
     <input type="submit" value='Search' class="formSubmit" >
    </center>
    </div>

    </form>
    
</div>

<div class="clear"></div><br>
<?php if("valid" == $formValid): ?>
<div><div id="notice"><span class="cRed">NOTICE:</span>
    Click on the count to view details
    </div><br/>
    <div class="wrapForm2">
    <?php //echo ePortal_legend('Search Results'); ?>
  <table width="100%"  cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
          <tr class="horizontal">
               <th colspan="1">Search Results</th>
               <th colspan="1">Count</th>
          </tr>
        </thead>
        <tbody>
        <tr class="alternateBgColour">
        <td align="left">Total eWallet Transfers</td>
        <td align="left"><?php if($eWalletTransfer==0): ?><?php echo $eWalletTransfer; ?><?php else: ?>
       <?php echo link_to(" ".$eWalletTransfer."",'mobilereports/mobileTransactionReport',array('title' => 'eWallet to eWallet Transfer Details','onclick'=>'return get_response_list("eWalletTransfer","'.$accountNo.'","'.$from_date.'","'.$to_date.'")'));?>
        <?php endif; ?>
        </td>
        </tr>
         <tr >
        <td width="40%">Total Bill Requests</td>
        <td><?php if($BillRequest==0): ?><?php echo $BillRequest; ?><?php else: ?>
            <?php echo link_to(" ".$BillRequest."",'mobilereports/mobileTransactionReport',array('title' => 'Bill Request Details','onclick'=>'return get_response_list("BillRequest","'.$accountNo.'","'.$from_date.'","'.$to_date.'")'));?>
            <?php endif; ?>
       </td>
        </tr>
         <tr class="alternateBgColour">
        <td>Total Bill Approval</td>
        <td><?php if($BillApprove==0): ?><?php echo $BillApprove; ?><?php else: ?>
        <?php echo link_to(" ".$BillApprove."",'mobilereports/mobileTransactionReport',array('title' => 'Bill Approve Details','onclick'=>'return get_response_list("BillApprove","'.$accountNo.'","'.$from_date.'","'.$to_date.'")'));?>
        <?php endif; ?>
        </td>
        </tr>
         <tr >
        <td>Total Bill Rejected</td>
        <td><?php if($BillReject==0): ?><?php echo $BillReject; ?><?php else: ?>
        <?php echo link_to(" ".$BillReject."",'mobilereports/mobileTransactionReport',array('title' => 'Bill Reject Details','onclick'=>'return get_response_list("BillReject","'.$accountNo.'","'.$from_date.'","'.$to_date.'")'));?>
        <?php endif; ?>
        </td>
        </tr>
        <tr class="alternateBgColour">
        <td>Total Merchant Applications Paid</td>
        <td><?php if($MerchantApplicationsPaid==0): ?><?php echo $MerchantApplicationsPaid; ?><?php else: ?>
        <?php echo link_to(" ".$MerchantApplicationsPaid."",'mobilereports/mobileTransactionReport',array('title' => 'Merchant Transactions Details','onclick'=>'return get_response_list("Merchant","'.$accountNo.'","'.$from_date.'","'.$to_date.'")'));?>
         <?php endif; ?>
         </td>
        </tr>
        </tbody>
    </table>
    
    </div>
</div>

<?php endif; ?>
<div id="result_div"></div>
</div>
 <script>

    function get_response_list(type,account,from,to){        
        $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;        
        $.post(
            '<?php echo url_for('mobilereports/transactionReportDetail');?>',
            {'type':type,'account':account,'from':from,'to':to},
            function(data){                
                if(data == "logout"){
                    location.reload();
                }else{
                     $("#result_div").html(data);
                }
            }
       );
           return false;
    }
</script>