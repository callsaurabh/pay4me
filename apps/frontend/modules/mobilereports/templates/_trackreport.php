 <?php use_helper('ePortal');  ?>
 <thead>
      <tr class="horizontal">
        <th width = "2%">S.No.</th>
        <th>From Account</th>
        <th>To Account</th>
        <th>Transaction Number</th>
        <th>Amount (<?php echo image_tag('../img/naira.gif',array('alt'=>'NGN')); ?>)</th>
        <th>Date of Transaction</th>
        <th>User Name</th>
        <?php if($type!='BillApprove' && $type!='BillReject'): ?>
        <th>Details</th>
        <?php endif; ?>
      </tr>
    </thead>
    <?php
  if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_records_per_page');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
?>
    <?php
      foreach ($pager->getResults() as $result):
      $i++;
    ?>
    <tbody>
      <tr class="alternateBgColour">
          <td align="center"><?php echo $i ?></td>
          <td align="center"><?php echo $result->getFromaccount();   ?></td>
          <td align="center"><?php echo $result->getToaccount();   ?></td>
          <td align="center"><?php echo $result->getTransno();   ?></td>
          <td align="center"><?php echo format_amount($result->getAmount(),'',1);   ?></td>
          <td align="center"><?php echo date('d-m-Y H:m:s',strtotime($result->getRequestedOn()));   ?></td>
          <td align="left"><?php echo $result->getUser();   ?></td>
          <?php if($type!='BillApprove' && $type!='BillReject'): ?>
          <td align="left">
          <?php echo link_to('Details', url_for('mobilereports/viewDetail?id='.$result->getId()), array('popup' => array('popupWindow', 'width=800,height=300,left=150,top=0,scrollbars=yes')));?>
          </td>
          <?php endif; ?>
      </tr>
    <?php endforeach;    ?>
    </tbody>
    <tfoot>
        <tr><td colspan="<?php if($type!='BillApprove' && $type!='BillReject'): ?>8<?php else: ?>7 <?php endif; ?>">
          <div class="paging pagingFoot">
        <?php
        if(($pager->getNbResults())>0) {
             echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/transactionReportDetail?type='.$type.'&account='.$accountNo.'&from='.$from_date.'&to='.$to_date),'result_div');
        }
        ?>
        </div>
        </td>
        </tr>
    </tfoot>
    <?php
    }else { ?>
      <tr><td  align='center' class='error' >No Record Found</td></tr>
      <?php } ?>