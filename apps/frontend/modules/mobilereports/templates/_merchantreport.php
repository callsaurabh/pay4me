<?php use_helper('ePortal');  ?>
<thead>
      <tr class="horizontal">
        <th width = "2%">S.No.</th>
        <th>Transaction Number</th>
        <th>Validation Number</th>
        <th>Amount (<?php echo image_tag('../img/naira.gif',array('alt'=>'NGN')); ?>)</th>
        <th>Date of Transaction</th>
        <th>User Name</th>
      </tr>
    </thead>
    <?php
  if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_records_per_page');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
?>
    <?php
      foreach ($pager->getResults() as $result):
      $i++;
    ?>
    <tbody>
      <tr class="alternateBgColour">
          <td align="center"><?php echo $i ?></td>
          <td align="center"><?php echo $result->getTxnNo();   ?></td>
          <td align="center"><?php echo $result->getValidationNumber(); ?></td>
          <td align="center"><?php echo format_amount($result->getPaidAmount(),'',0);   ?></td>
          <td align="center"><?php echo date('d-m-Y H:m:s',strtotime($result->getPaidDate()));  ?></td>
          <td align="left"><?php echo $result->getUser();   ?></td>
      </tr>
    <?php endforeach;    ?>
    </tbody>
    <tfoot><tr><td colspan="6">
      <div class="paging pagingFoot">
    <?php
    if(($pager->getNbResults())>0) {
         echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/transactionReportDetail?type='.$type.'&account='.$accountNo.'&from='.$from_date.'&to='.$to_date),'result_div');
    }
    ?>
</div>
    </td></tr></tfoot>
    <?php }else { ?>
      <tr><td  align='center' class='error' >No Record Found</td></tr>
    <?php } ?>