<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php echo ePortal_listinghead('Details'); ?>

<div id="wrapTable">
<div class="popupWrapForm2" style="background-color:#EFEFEF;">
    <table width="80%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tbody>
     <?php 
     echo formRowFormatRaw('From Account',$transactionDetails['fromaccount']) ;
     echo formRowFormatRaw('To Account',$transactionDetails['toaccount']) ;
     echo formRowFormatRaw('Transaction Number',$transactionDetails['transno']) ;
     echo formRowFormatRaw('Description',$transactionDetails['description']) ;
     ?>
    </tbody>
    </table>
</div>
</div>