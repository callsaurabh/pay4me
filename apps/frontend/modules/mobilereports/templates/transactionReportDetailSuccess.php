<?php use_helper('Pagination');  ?>
<table><tr><td>&nbsp;</td></tr></table>

<?php echo ePortal_listinghead($headername); ?>

<div class="wrapTable">
    <table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <?php if(($pager->getNbResults())>0) { ?>
                <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                <?php }else{ ?>
                <span class="floatRight">Showing <b>0</b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                <?php } ?>
            </th>
        </tr>
        <?php
        if(($pager->getNbResults())>0) {
            if($accountNo=="")
             $accountNo="BLANK";
            if($from_date=="")
             $from_date="BLANK";
            if($to_date=="")
             $to_date="BLANK";
            ?>
        
        <?php }   ?>
    </table>
    
</div>
<div class="wrapTable" >
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
<?php if($type=='Merchant'){ ?>

<?php echo include_partial('merchantreport', array('pager' => $pager,'type'=>$type,'accountNo'=>$accountNo,'from_date'=>$from_date,'to_date'=>$to_date)); ?>
<?php } else{ ?>
<?php echo include_partial('trackreport', array('pager' => $pager,'type'=>$type,'accountNo'=>$accountNo,'from_date'=>$from_date,'to_date'=>$to_date)); ?>
<?php } ?>
</table>
</div>

