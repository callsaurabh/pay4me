<?php

/**
 * mobilereports actions.
 *
 * @package    mysfp
 * @subpackage mobilereports
 * @author     Srikanth Reddy
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class mobilereportsActions extends sfActions {
  
  public function executeMobileTransactionReport(sfWebRequest $request) {

     // Vikash [WP055] Bug:36081 (07-11-2012)
      if ($request->isMethod('post')) {
            $arrTransactionDetail = $request->getParameter('transaction');
            $to_date = $arrTransactionDetail['to']; // Vikash [WP055] Bug:36081 (07-11-2012)
        } else {
            $to_date = '';
        }

       //get form details
      $this->form = new MobileTransactionForm(array(), array('to_date' => $to_date));
        $this->formValid ="";
        //check for is posted or not
        if($request->isMethod('post')){
             //bind form
             $this->form->bind($request->getParameter('transaction'));
            if ($this->form->isValid())
            {
              //assign form as valid
              $this->formValid = "valid";
              //get all request parameters
              $parameters = $request->getParameterHolder()->getAll();
              
              //get transaction count details
              $this->getTransactiondetails($parameters['transaction']['account_no'],
                    $parameters['transaction']['from'],$parameters['transaction']['to']);
            }
        }

     }
  //function to get all transaction details
  private function getTransactiondetails($accountno='',$from_date='',$to_date=''){
        $to = "";
        //adding time to date for to_date
        if($to_date!="" && $to_date!='BLANK'){
            $to = $to_date." 23:59:59";
        }
      //getting transaction count list
      $arrTransactionCount = Doctrine::getTable('EwalletTransactionTrack')
                ->getTransactionTrackCountByPlatform('Mobile',$accountno,$from_date,$to);
      $this->accountNo = $accountno;
      $this->from_date = $from_date;
      $this->to_date = $to_date;
      //initializationing type values
      $this->BillRequest = 0;
      $this->eWalletTransfer = 0;
      $this->BillApprove = 0;
      $this->BillReject = 0;
      $this->MerchantApplicationsPaid = 0;
      if(count($arrTransactionCount)>0){
          //looping array and assigning value of each parameter
          foreach($arrTransactionCount as $transaction){
              //get Total eWallet Transfers
              if($transaction['type']=='BillRequest')
              $this->BillRequest = $transaction['count'];
              //get Total Bill Requests
              if($transaction['type']=='eWalletTransfer')
              $this->eWalletTransfer = $transaction['count'];
              //get Total Bill Approval
              if($transaction['type']=='BillApprove')
              $this->BillApprove = $transaction['count'];
              //get Total Bill Rejects
              if($transaction['type']=='BillReject')
              $this->BillReject = $transaction['count'];
          }
      }
      //unsetting values
      unset($arrTransactionCount);
      //Total Merchant Applications Paid
      $arrMerchantCount = Doctrine::getTable('MerchantRequest')->
                getMerchantTransactionCountByPlatform('Mobile',$accountno,$from_date,$to);
      $this->MerchantApplicationsPaid = $arrMerchantCount[0]['count'];
      //unsetting merchant values
      unset($arrMerchantCount);
  }
  //transaction detail report
  public function executeTransactionReportDetail(sfWebRequest $request) {
        //getting all parameters
        $parameters = $request->getParameterHolder()->getAll();
        $to = "";
        $this->accountNo = $parameters['account'];
        $this->from_date = $parameters['from'];
        $this->to_date = $parameters['to'];
        //adding time parameter to to_date
        if($parameters['to']!="" && $parameters['to']!='BLANK'){
            $to = $parameters['to']." 23:59:59";
        }
        $this->type = $parameters['type'];
        $type = $parameters['type'];
        //getting label header name
        $this->headername = $this->getHeaderName($type);
        //check whether type is merchant or else
        if($type=="Merchant"){
            //get Merchant Request Transaction details
            $Model = 'MerchantRequest';
            $objTransaction = Doctrine::getTable('MerchantRequest')->
            getMerchantRequestTransaction('Mobile',$parameters['account'],
                $parameters['from'],$to);

        } else {
            //get Ewalet Transcation Track Details
            $Model = 'EwalletTransaction';
            $objTransaction = Doctrine::getTable('EwalletTransaction')->
            getTransactionList('Mobile',$type,$parameters['account'],$parameters['from'],
                        $to);
           
        }
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager($Model,sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($objTransaction);
        // $this->pager->getQuery()->addGroupBy('l.master_account_id');
        $this->pager->setPage($this->page);
        $this->pager->init();
  }

  private function getHeaderName($name){
      switch($name){
          case 'eWalletTransfer':
            return 'eWallet to eWallet Transfer Details';
            break;
          case 'BillRequest':
            return 'Bill Request Details';
            break;
          case 'BillApprove':
            return 'Bill Approve Details';
            break;
          case 'BillReject':
            return 'Bill Reject Details';
            break;
          case 'Merchant':
            return 'Merchant Transactions Details';
            break;
      }

  }
  //transaction detail report
  public function executeViewDetail(sfWebRequest $request) {
      $this->setLayout('layout_popup');
    //getting all parameters
    $parameters = $request->getParameterHolder()->getAll();
    $arrTransactionDetails = Doctrine::getTable('EwalletTransaction')
                    ->getTransactionDetailsById($parameters['id']);
    $this->transactionDetails = $arrTransactionDetails[0];

  }
  
}
