<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<div class="wrapForm2">
<?php
$heading = ($form->getObject()->isNew() ? 'Create New' : 'Edit');

switch($user_type){
    case 'e_auditor':
            echo ePortal_legend(__($heading.' Bank eAuditor Admin'));
            break;
    case 'report_admin':
            echo ePortal_legend(__($heading.' Bank Report Admin'));
            break;
    case 'bank_country_head':
            echo ePortal_legend(__($heading.' Bank Country Head'));
            break;
    case 'country_report_user':
            echo ePortal_legend(__($heading.' Bank Report Country Admin'));
            break;
    default:
            echo ePortal_legend(__($heading.' Bank Admin'));
            break;
}
 ?><?php echo  $form->renderGlobalErrors();?>
 <div id='flash_error'  style='display:none; visibility:hidden;height:1px;overflow-y:hidden'></div>
    <form action="<?php echo url_for($sf_context->getModuleName().'/'.($form->getObject()->isNew() ? 'create' : 'modify').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : ''))?>"  id="pfm_create_user_form" name="pfm_create_user_form"   method="post" enctype="multipart/form-data"  >

  <?php echo $form ?>
  <?php echo $form->renderHiddenFields(); ?>
  <input type="hidden" name="userType" value="<?php echo $user_type;?>" />
  <div class="divBlock">
      <center>
          <input type="submit" value=<?php echo __("Save"); ?> class="formSubmit" />
          &nbsp;&nbsp;
<?php
        switch($user_type){
            case 'e_auditor':
                  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/listBankEAuditor').'\''));
                  break;
            case 'report_admin':
                  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/listReportBankUser').'\''));
                  break;
            case 'bank_country_head':
                  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/listHead').'\''));
                  break;
            case 'country_report_user':
                  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/listReportCountryAdmin').'\''));
                  break;
            default:
                  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/index').'\''));
                  break;
        }

          ?>

  </center></div>
</form>

</div>
