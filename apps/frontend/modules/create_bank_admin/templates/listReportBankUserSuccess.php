<div id='theMid'>
    <?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
    <?php //use_helper('Form');
    use_helper('Pagination');  ?>
    <?php echo ePortal_legend(__('Search User')); ?>
    <div class="wrapForm2">
        <?php echo form_tag($sf_context->getModuleName().'/listReportBankUser','name=search id=search');?>
        <dl id='bank_row'>
            <!-- Earlier Code without using $form

<div class="dsTitle4Fields"><label for="bank">Username</label></div>
<div class="dsInfo4Fields"><?php // echo input_tag('user_search', $userName, 'class=FieldInput');?>
<input type="submit" class='formSubmit' value="Search" onclick="return userSearch();"/>&nbsp;&nbsp;<br /><a href="<?php// echo url_for($sf_context->getModuleName().'/listReportBankUser') ?>"  style="cursor: pointer; color: blue; text-decoration: underline;">Back to User listing</a>
<div id="err_bank_branch" class="cRed"></div>
</div>
            -->
            <div><br/></div>
            <div class="listreport_user_search_row"><?php echo $form->render('user_search'); ?></div>
            <div class="listreport_user_search_row">
                <div id="listreport_user_search_row" class="divBlock">
                    <div class="dsTitle4Fields"></div>
                    <div class="dsInfo4Fields">
                        <input type="submit" class='formSubmit' value=<?php echo __("Search"); ?> onclick="return userSearch();"/>&nbsp;&nbsp;<br /><a  href="<?php echo url_for($sf_context->getModuleName().'/listReportBankUser') ?>"  style="cursor: pointer; color: blue; text-decoration: underline;">Back to User listing</a>
                        <div id="err_bank_branch" class="cRed"></div>
            </div>  </div></div>

        </dl>
        </form>
        <div class="clear"></div>
    </div>
    <br />
    <?php echo ePortal_listinghead(__('Bank Report Admin List')); ?>

    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr class="alternateBgColour">
                <th width="100%" >
                     <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
                     <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
                </th>
            </tr>
        </table>
        <br class="pixbr" />
    </div>

    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <thead>
                <tr class="horizontal">
                    <th width = "2%">S.No.</th>
                    <th><?php echo __('Username'); ?></th>
                    <th><?php echo __('User Role') ?></th>
                    <th><?php echo __('Bank') ?></th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(($pager->getNbResults())>0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $page = $sf_context->getRequest()->getParameter('page',0);
                    $i = max(($page-1),0)*$limit ;
                    foreach ($pager->getResults() as $result):
                    $i++;
                    //                echo "<pre>";
                    //                print_r($result->toArray());
                    //                die;
                    ?>
                <tr class="alternateBgColour">
                    <td align="center"><?php echo $i ?></td>
                    <td align="center"><?php echo $result->getUserName() ?></td>
                    <td align="center"><?php echo $result->getGroupDescription() ?></td>
                    <td align="center"><?php echo $result->getBankName() ?></td>
                    <td align="center"><?php
                    if($result->getUserStatus()==3)
                    {
                        echo "<font class=error>De-Activated</font>";
                    }

                    else{
                        if($result->getFailedAttempt() == 5){ echo link_to(' ', 'create_bank_admin/unblock?id='.$result->getId(), array('method' => 'get', 'class' => 'unblockInfo', 'title' => 'Unblock User')); }
                        else
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                        echo "&nbsp;";
                        echo link_to(' ', 'create_bank_admin/resetPassword?userId='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to reset the password?'), 'class' => 'userEditInfo', 'title' => 'Reset Password')) ;
                        echo "&nbsp;";
                        echo link_to(' ', 'create_bank_admin/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit'));
                        if($result->getUserStatus() == 1)
                        {
                            echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=2&user_id='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to suspend the user?'), 'class' => 'deactiveInfo', 'title' => 'Suspend')) ;
                        }

                        if($result->getUserStatus() == 2)
                        {
                            echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=1&user_id='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to resume the user?'), 'class' => 'activeInfo', 'title' => 'Resume')) ;
                        }

                        echo "&nbsp;";
                        echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=3&user_id='.$result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to De-Activate the user?'), 'class' => 'deactivatedInfo', 'title' => 'De-Activate')) ;

                    }

                        ?>
                        
                </tr>
                <?php endforeach;
                ?>
                <tr><td colspan="5">
                        <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

                </div></td></tr>
                <?php
            }else { ?>

                <tr><td  align='center' class='error' colspan="5">No Bank Admin Found</td></tr>
                <?php } ?>
            </tbody>

        </table>
    </div>
    <?php include_partial('create_bank_admin/message')?>
</div>