<style type="text/css">
<!--
.style3 {color: #007BC1; font-weight: bold; font-size: 12px; }
-->
</style>
<div class="notebg">
    <b class="note">Note:</b>
  <div class="note-txt">
      		
		
		<table width="100%" border="1" cellspacing="0" bordercolor="#cecece" cellpadding="5" style="border-collapse:collapse; text-align:justify;">
          <tr>
            <td width="245" style="color:#007BC1; font-size:12px;"><strong><span class="deactivatedInfo1"></span>&nbsp;De-Activate</strong></td>
            <td width="1132">Should be used under circumstances where user leaves a bank permanently and in future would never work for the same bank on the platform. The Transactions done by user as well all the audits will be available as his/her past activities.</td>
          </tr>
          <tr>
            <td><span class="style3"><span class="userEditInfo1"></span>&nbsp;Reset Password</span></td>
            <td>Should be used under circumstances where user forgot his/her password and user is not able to login. </td>
          </tr>
          <tr>
            <td><span class="style3"><span class="deactiveInfo1"></span>&nbsp;Suspend/&nbsp;<span class="activeInfo1"></span>&nbsp;Resume</span></td>
            <td>Suspend should be used under circumstances where user goes on leave/vacation, Resume can be used when the user rejoins his/her services. </td>
          </tr>
          <tr>
            <td><span class="style3"><span class="unblockInfo1"></span>&nbsp;Unblock User</span></td>
            <td>This button is visible only if the user is currently blocked. Blocked user is able to login only when his/her account is Unblocked. </td>
          </tr>
        </table>
     </div>
</div>
