<?php

/**
 * create_bank_admin actions.
 * 
 * @package    mysfp
 * @subpackage create_bank_admin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class create_bank_adminActions extends sfActions {
    public function executeIndex(sfWebRequest $request) {
        $this->user_type = "bank_admin";
        // print sfConfig::get('app_email_url');exit;
        //    $user_obj = sfGuardAuthServiceFactory::getService(sfGuardAuthServiceFactory::$TYPE_USER);
        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_bank_admin')); //get Group Id; of bank admin
        $groupid = $sfGroupDetails->getFirst()->getId();
        $sfGroupDetails_second = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_report_bank_admin')); //get Group Id; of bank admin
        $groupid_second = $sfGroupDetails_second->getFirst()->getId();
        $sfGroupDetails_third = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_bank_e_auditor')); //get Group Id; ofbank_e_auditor
        $groupid_third = $sfGroupDetails_third->getFirst()->getId();

        $this->userName = $request->getParameter('user_search');
        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BASE);
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllUsersSecond($groupid,'','',$this->userName);
        $this->page = 1;
        $this->form = new listBankAdminForm(array(),array('value'=>$this->userName));
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeShow(sfWebRequest $request) {
        $this->bank_user = Doctrine::getTable('BankUser')->find($request->getParameter('id'));
        $this->forward404Unless($this->bank_user);
    }

    public function executeNew(sfWebRequest $request) {

        $this->user_type = "bank_admin";
        $this->form = new CreateBankAdminForm(array(),array('userType'=>'bank_admin'));
        //        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BANK);
        //        $this->bank_array = $bank_obj->getBankList();
        //    $this->role = array('bank_admin'=>'Bank Admin','report_bank_admin'=>'Report Bank Admin','bank_e_auditor'=>'Bank eAuditor Admin');
    }
  /******************  List Bank Country Head  ***********************/
    public function executeListHead(sfWebRequest $request){
        $this->user_type = "bank_country_head";
        $user = $this->getUser();
        $bank_id = $user->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
        $country = Doctrine::getTable('BankConfiguration')->getAllCountry($bank_id);
        $this->countryArray = '';
        if($country)       
        $this->countryArray = $country;
        else
        $this->countryArray['']   ="--Select Country--";

        $sfGroupDetails_third = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_bank_country_head')); //get Group Id; ofbank_e_auditor
        $groupid_third = $sfGroupDetails_third->getFirst()->getId();
       

        // Earlier code for without form rendering
        // $this->userName = $request->getParameter('user_search');

        $this->countryId = $request->getParameter('country_id');
        $this->userName= $request->getParameter('user_search');
        $this->form = new listBankAdminForm(array(),array('country_id'=>$this->countryId));

        
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllUsers($groupid_third,$bank_id,$this->userName,'',$this->countryId);
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
        $this->setTemplate('index');
    }
  /****************** List Bank Country Head ****************/
   public function executeNewHead(sfWebRequest $request) {
        $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
        $this->user_type = "bank_country_head";
        $this->form = new CreateBankAdminForm(array(),array('userType'=>'bank_country_head','bankId'=>$bank_id));
        $this->setTemplate('new');
    }

 /****************** Create Bank Country Report Admin ****************/
   
   public function executeNewReportCountryAdmin(sfWebRequest $request) {
        $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
        $this->user_type = "country_report_user";
        $this->form = new CreateBankAdminForm(array(),array('userType'=>$this->user_type,'bankId'=>$bank_id));
        $this->setTemplate('new');
   }

   /****************** List Bank Country Report Admin ****************/
   public function executeListReportCountryAdmin(sfWebRequest $request){
        $this->user_type = "country_report_user";
        $user = $this->getUser();
        $bank_id = $user->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
         $country = Doctrine::getTable('BankConfiguration')->getAllCountry($bank_id);
        $this->countryArray = '';
        if($country)
        $this->countryArray = $country;
        else
        $this->countryArray['']   ="--Select Country--";

        $sfGroupDetails_third = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_country_report_user')); //get Group Id; ofbank_e_auditor
        $groupid_third = $sfGroupDetails_third->getFirst()->getId();       
        $this->countryId = $request->getParameter('country_id');
        $this->userName= $request->getParameter('user_search');
        $this->form = new listBankAdminForm(array(),array('country_id'=>$this->countryId));
       
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllUsers($groupid_third,$bank_id,$this->userName,'',$this->countryId);
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
        $this->setTemplate('index');
    }
    public function executeChkUserAvailability(sfWebRequest $request) {

        $postArray = $request->getPostParameters();
        $bank_id = $postArray['sf_guard_user']['bankform']['bank_id'];
        if($bank_id ==''){
            $this->result = "Please select Bank first";
        }else{
            $bankname = Doctrine::getTable('Bank')->find($bank_id);
            if($postArray['sf_guard_user']['username'] ==''){
                $this->result = "Please enter Username";
            }else{
                $username = $postArray['sf_guard_user']['username'].".".$bankname->getAcronym();

                $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);

                if($username_count == 0) {
                      $this->result = "<div class='cGreen'>Username Available</div>";
                }
                else {
                     $this->result = "<div class='cRed'>Username not Available</div>";
                }
            }

        }
    


    }

    public function executeCreate(sfWebRequest $request) {
        $this->redirecturl = 'index';
        $bank_id = '';
        $userType = $request->getPostParameter('userType');
        if($userType=='e_auditor') {
            $this->user_type = $userType;
            $this->redirecturl = "listBankEAuditor";
            
        }else if($userType=='report_admin'){
            $this->user_type = $userType;
            $this->redirecturl = "listReportBankUser";
          }else if($userType=='bank_country_head'){
            $this->user_type = $userType;
            $this->redirecturl = "listHead";
            $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
        }else if($userType=='country_report_user'){
            $this->user_type = $userType;
            $this->redirecturl = "listReportCountryAdmin";
            $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
        }else{
            $this->user_type = "bank_admin";
            $this->redirecturl = "index";
        }
        $this->setTemplate("new");
        $this->form = new CreateBankAdminForm(array(),array('userType'=>$this->user_type,'bankId'=>$bank_id));
        $this->form->bind($request->getParameter($this->form->getName()));
        if($this->form->isValid()){         
            $this->form->save();
            $user_id = $this->form->getObject()->id;
            $bank_id = $this->form->getObject()->getBankUser()->getFirst()->getBankId();
            $country_id = $this->form->getObject()->getBankUser()->getFirst()->getCountryId();
            $sfUser = Doctrine::getTable("sfGuardUser")->find($user_id);



            $bankname = Doctrine::getTable('Bank')->find($bank_id);

            $user_name = $sfUser->username;
            $pass_word = PasswordHelper::generatePassword();
//            $sfUser->setUsername($user_name);
            // $sfUser->setSalt($bank_acronym."_password");
            $sfUser->setPassword($pass_word);
            $sfUser->setIsActive(true);
            $sfUser->save();

            // save email with bank domian and
            // find domain on the basis of bank id and country id
            $getbankDomain = Doctrine::getTable('Bank')->getBankDomain($bank_id,$country_id);
            $bank_domain = $getbankDomain['domain'];

            if($this->user_type != "bank_admin")
                $email =  $sfUser->getUserDetail()->getFirst()->getEmail().$bank_domain;
            else
                $email =  $sfUser->getUserDetail()->getFirst()->getEmail();
            $userDetailobj =  Doctrine::getTable("UserDetail")->find($sfUser->getUserDetail()->getFirst()->getId());

            $userDetailobj->setEmail($email);
            $userDetailobj->save();
            //insert userid and password in EpPasswordPolicy table
            EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(),$sfUser->getPassword());

            //create a User Group

            if($request->getPostParameter('userType')=='e_auditor') {
                $userTypeToCreate = sfConfig::get('app_pfm_role_bank_e_auditor');
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_BANKEAUDITOR_CREATE;
                $role = "BankEAuditor";
            }
            else if($request->getPostParameter('userType')=='report_admin') {
                $userTypeToCreate = sfConfig::get('app_pfm_role_report_bank_admin');
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_REPORT_ADMIN_CREATE;
                $role = "Bank Report Admin";
            }else if($request->getPostParameter('userType')=='bank_country_head') {
                $userTypeToCreate = sfConfig::get('app_pfm_role_bank_country_head');
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_COUNTRY_HEAD_CREATE;
                $role = "Bank Country Head";
            }else if($request->getPostParameter('userType')=='country_report_user') {
                $userTypeToCreate = sfConfig::get('app_pfm_role_country_report_user');
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_REPORT_COUNTRY_ADMIN_CREATE;
                $role = "Bank Report Country Admin";
            }else {
                $userTypeToCreate = sfConfig::get('app_pfm_role_bank_admin');
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_ADMIN_CREATE;
                $role = "Bank Admin";
            }


            $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate); //hard coded as of now; is the bank admin
            $sfGroupId = $sfGroupDetails->getFirst()->getId();

            $sfGuardUsrGrp = new sfGuardUserGroup();
            $sfGuardUsrGrp->setGroupId($sfGroupId);
            $sfGuardUsrGrp->setUserId($user_id);
            $sfGuardUsrGrp->save();

            /*Audit Trail code starts*/
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user_name, $user_id));
            $eventHolder = new pay4meAuditEventHolder(
                EpAuditEvent::$CATEGORY_TRANSACTION,
                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_CREATE,
                EpAuditEvent::getFomattedMessage($msg, array('username'=>$user_name)),
                $applicationArr);

            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            //
            $subject = "Welcome to Pay4Me" ;
            $partialName = 'welcome_email' ;
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $login_url = url_for('@bank_login?bankName='.$bankname->getAcronym(), true);
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$this->moduleName."/sendEmail", array('userid'=>$user_id,'password'=>$pass_word,'subject'=>$subject,'partialName'=>$partialName,'login_url'=>$login_url));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

            if(sfConfig::get('app_display_password'))
            $this->getUser()->setFlash('notice', sprintf($user_name." created successfully as ".$role."<br> The password is - ".$pass_word));
             else
            $this->getUser()->setFlash('notice', sprintf($user_name." created successfully as ".$role.'<br> An email has been sent to user regarding the same.'));
            $this->redirect('create_bank_admin/'.$this->redirecturl);

        }else{    
            //            die("not");
        }


    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $password = $request->getParameter('password');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $login_url = $request->getParameter('login_url');
        $sendMailObj = new Mailing() ;
        $mailInfo = $sendMailObj->sendUserEmail($userid,$password,$subject,$partialName,$login_url);
        return $this->renderText($mailInfo);
    }


    public function executeEdit(sfWebRequest $request) {
        $this->method="edit";
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id')), sprintf('Object bank_admin does not exist (%s).', $request->getParameter('id')));
        $this->user_type = $sf_guard_user->getSfGuardUserGroup()->getFirst()->getSfGuardGroup()->getName();

        $this->userId = $request->getParameter('id');
        $this->username = $sf_guard_user->getUsername();
        $this->bank_id = $sf_guard_user->getBankUser()->getFirst()->getBank()->getId();
        $this->country_id = $sf_guard_user->getBankUser()->getFirst()->getCountryId();;
        $this->bank_acronym = $sf_guard_user->getBankUser()->getFirst()->getBank()->getAcronym();
        //$this->bank_domain = $sf_guard_user->getBankUser()->getFirst()->getBank()->getBankConfiguration()->getFirst()->getDomain();
        $group =  Doctrine::getTable('sfGuardUserGroup')->findByUserId($request->getParameter('id'));//print_r($group);
        $groupid = $group->getFirst()->getGroupId();
        if($groupid == 6){
            $this->redirecturl = "listBankEAuditor";
            $this->user_type = 'e_auditor';
        }
        elseif($groupid == 5){
            $this->redirecturl = "listReportBankUser";
            $this->user_type = 'report_admin';
        }
        elseif($groupid == 2){
            $this->redirecturl = "index";
            $this->user_type = 'bank_admin';
        }
        if($sf_guard_user->getUserDetail() =='') {


        }
        $email_address= $sf_guard_user->getUserDetail()->getFirst()->getEmail();
        if($groupid != 2)
            {
                $email=explode('@',$email_address );
                $email_address = $email[0];
            }
        
        $this->form = new CreateBankAdminForm($sf_guard_user,array('action'=>'edit','bankId'=>$this->bank_id,'email'=>$email_address,'userType'=>$this->user_type,'country_id'=>$this->country_id));

       
        $this->userId = $request->getParameter('id');
        $this->setTemplate('new');
    }

    public function executeModify(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod('post'));
        $this->forward404Unless($sf_guard_user_obj = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id')), sprintf('Object bank_admin does not exist (%s).', $request->getParameter('id')));
        $this->user_type = $sf_guard_user_obj->getSfGuardUserGroup()->getFirst()->getSfGuardGroup()->getName();
        $group =  Doctrine::getTable('sfGuardUserGroup')->findByUserId($request->getParameter('id'));//print_r($group);
        $groupid = $group->getFirst()->getGroupId();
        $email_address= $sf_guard_user_obj->getUserDetail()->getFirst()->getEmail();
        if($groupid != 2)
        {
            $email=explode('@',$email_address );
            $email_address = $email[0];
        }       
       
        $this->bank_id = $sf_guard_user_obj->getBankUser()->getFirst()->getBank()->getId();

        $this->form = new CreateBankAdminForm($sf_guard_user_obj,array('action'=>'edit','bankId'=>$this->bank_id,'userType'=>$this->user_type));
        $this->form->bind($request->getParameter($this->form->getName()));       
       
        switch($groupid){
            case '6':
                 $this->redirecturl = "listBankEAuditor";
                  break;
             case '5':
                 $this->redirecturl = "listReportBankUser";
                  break;
             case '2':
                 $this->redirecturl = "index";
                  break;
             case '12':
                  $this->redirecturl = "listHead";
                  break;
             case '13':
                  $this->redirecturl = "listReportCountryAdmin";
                  break;
            default:
                  $this->redirecturl = "index";
                  break;
        }
        if ($this->form->isValid()){
            $this->form->save();
            if($groupid != 2)
                $email =  $sf_guard_user_obj->getUserDetail()->getFirst()->getEmail().$sf_guard_user_obj->getBankUser()->getFirst()->getBank()->getBankConfiguration()->getFirst()->getDomain();
            else
                $email =  $sf_guard_user_obj->getUserDetail()->getFirst()->getEmail();
            
            $userDetailobj =  Doctrine::getTable("UserDetail")->find($sf_guard_user_obj->getUserDetail()->getFirst()->getId());

            $userDetailobj->setEmail($email);
            $userDetailobj->save();

    /*Audit Trail code starts*/

            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $sf_guard_user_obj->getUsername(), $sf_guard_user_obj->getId()));
            $eventHolder = new pay4meAuditEventHolder(
                EpAuditEvent::$CATEGORY_TRANSACTION,
                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_UPDATE,
                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_USER_UPDATE, array('username' => $sf_guard_user_obj->getUsername())),
                $applicationArr);

            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
      /*Audit Trail code ends*/

            $this->getUser()->setFlash('notice', sprintf('User Profile updated successfully'));
            $this->redirect('create_bank_admin/'.$this->redirecturl);

        }else{
            $this->userId = $request->getParameter('id');
            $this->setTemplate('new');

        }
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($bank_user = Doctrine::getTable('BankUser')->find($request->getParameter('id')), sprintf('Object bank_user does not exist (%s).', $request->getParameter('id')));
        $this->form = new BankUserForm($bank_user);
        $bank_branch_id=$request->getPostParameters('bank_branch_id');
        $this->bank_branch_id=$bank_branch_id['bank_user']['bank_branch_id'];
        $bank_id=$request->getPostParameters('bank_id');
        $this->bank_id=$bank_id['bank_user']['bank_id'];
        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    protected function saveUserDetail($request,$userId) {
        $this->bank_id=$bank_acronym=$request->getPostParameter('bank_id');
        $bankname = Doctrine::getTable('Bank')->findByAcronym($bank_acronym);
        $bank_domain = $bankname->getFirst()->getBankConfiguration()->getfirst()->getDomain();

        $userDetail = new UserDetail();
        $userDetail->setUserId($userId);
        if(trim($request->getPostParameter('name')) != "")
        $userDetail->setName(trim($request->getPostParameter('name')));
        if($request->getPostParameter('dob')) {
            $date = date('Y-m-d', strtotime($request->getPostParameter('dob')));
            $userDetail->setDob($date);
        }
    /*
    if(trim($request->getPostParameter('b_place')) != "")
    $userDetail->setBrithPlace(trim($request->getPostParameter('b_place')));
    */
        if(trim($request->getPostParameter('address')) != "")
        $userDetail->setAddress(trim($request->getPostParameter('address')));
        if(trim($request->getPostParameter('email')) != "")
        $userDetail->setEmail(trim($request->getPostParameter('email').$bank_domain));
        if(trim($request->getPostParameter('mobile_no')) != "")
        $userDetail->setMobileNo(trim($request->getPostParameter('mobile_no')));
        $userDetail->save();
    }

    protected function userDetailValid($request) {
        $msg=array();
        if(trim($request->getPostParameter('name')) == "") {
            $msg[]= 'Please enter name ';
        }
        if(trim($request->getPostParameter('email')) == "") {
            $msg[]= 'Please enter Email<br />';
        }else if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*$", $request->getPostParameter('email'))) {
            $msg[]= 'Please enter Valid Email<br />';

        }
        if(trim($request->getPostParameter('address')) != "") {
            $len = strlen(trim($request->getPostParameter('address')));
            if($len > 255) {
                $msg[]= 'Please enter valid address<br />';
            }
        }
        if(count($msg) > 0) {
            $msg = implode($msg, ', ');
            $this->getUser()->setFlash('error', sprintf($msg));
            return false;
        }
        return true;
    }

    public function executeFetchBankDetails(sfWebRequest $request) {
        if($request->getPostParameter('id')!="") {
            $bankId= $request->getPostParameter('id');
            $countryId = $request->getPostParameter('country_id');
            if($countryId){
                $selected_bank_obj = Doctrine::getTable('Bank')->getBankDomain($bankId,$countryId);
                $bank_domain = $selected_bank_obj['domain'];
                $acronym = $selected_bank_obj['acronym'];
            }else{               
                $selected_bank_obj = Doctrine::getTable('Bank')->findById($request->getPostParameter('id'));               
               
                if(count($selected_bank_obj->getFirst()->getBankConfiguration()->toArray()))                
                $bank_domain = $selected_bank_obj->getFirst()->getBankConfiguration()->getFirst()->getDomain();
                else
                $bank_domain = '';
                $acronym = $selected_bank_obj->getFirst()->getAcronym();
            }
           

            if(in_array(sfConfig::get('app_pfm_role_admin'),$this->getUser()->getGroupNames()))
                $bankString = $acronym.'~'.'BLANK';
            else
                $bankString = $acronym.'~'.$bank_domain;
            return $this->renderText($bankString);
        }

    }    

    public function executeNewReportBankUser(sfWebRequest $request) {
        $this->user_type = "report_admin";
        /* pass userType parameter to validate email on report_admin by ryadav
         */
        $this->form = new CreateBankAdminForm(null,array('userType'=>$this->user_type));
        $this->setTemplate('new');
    }

    public function executeListReportBankUser(sfWebRequest $request){

        $sfGroupDetails_second = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_report_bank_admin')); //get Group Id; of bank admin
        $groupid_second = $sfGroupDetails_second->getFirst()->getId();
        $this->form = new ListReportBankUserForm();

        //         $this->$form->bind($request->getParameter('listreport'));
        //Earlier code
        //$this->userName = $request->getParameter('user_search');
        $this->userName = $request->getParameter('listreport');
        $username = $this->userName['user_search'];

        //echo $request->getParameter('listreport[user_search]');exit;
        // $this->userName = $request->getParameter('listreport[user_search]');
        $params = $request->getParameter('listreport');
        $this->userName = $params['user_search'];
        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BASE);
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllEAuditUsers($groupid_second,$username);
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

  /******************  creat BankEAuditor  ***********************/
    public function executeNewBankEAuditor(sfWebRequest $request) {
        $this->user_type = "e_auditor";
        /* pass userType parameter to validate email on eauditor by ryadav
         */
        $this->form = new CreateBankAdminForm(null,array('userType'=>$this->user_type));
        $this->setTemplate('new');
    }
  /****************** end of create BankEAuditor ****************/

  /******************  List BankEAuditor  ***********************/
    public function executeListBankEAuditor(sfWebRequest $request){
        $sfGroupDetails_third = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_bank_e_auditor')); //get Group Id; ofbank_e_auditor
        $groupid_third = $sfGroupDetails_third->getFirst()->getId();
        $this->form = new ListReportBankUserForm();


        // Earlier code for without form rendering
        // $this->userName = $request->getParameter('user_search');

        $this->userName = $request->getParameter('listreport');
        $username = $this->userName['user_search'];

        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BASE);
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllEAuditUsers($groupid_third,$username);
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
  /****************** end of list BankEAuditor ****************/


    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();
        $this->forward404Unless($user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id')), sprintf('Object message does not exist (%s).', $request->getParameter('id')));
        $user->delete();

        $group =  Doctrine::getTable('sfGuardUserGroup')->findByUserId($request->getParameter('id'));//print_r($group);
        $groupid = $group->getFirst()->getGroupId();        
        switch($groupid){
            case '6':
                 $redirecturl = "listBankEAuditor";
                  break;
             case '5':
                 $redirecturl = "listReportBankUser";
                  break;
             case '2':
                 $redirecturl = "index";
                  break;
             case '12':
                  $redirecturl = "listHead";
                  break;
            case '13':
                  $redirecturl = "listReportCountryAdmin";
                  break;
            default:
                  $redirecturl = "index";
                  break;
        }
    /*Audit Trail code starts*/
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user->getUsername(), $user->getId()));
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_DELETE,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_USER_DELETE, array('username' => $user->getUsername())),
            $applicationArr);

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    /*Audit Trail code ends*/

        $this->getUser()->setFlash('notice','User deleted successfully' , true);
        $this->redirect('create_bank_admin/'.$redirecturl);
    }

    public function executeUnblock(sfWebRequest $request) {
        $user_id = $request->getParameter('id');
        $user_details = Doctrine::getTable('sfGuardUser')->find(array($user_id));
        $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($user_id);
        $group =  Doctrine::getTable('sfGuardUserGroup')->findByUserId($user_id);//print_r($group);
        $groupid = $group->getFirst()->getGroupId();
        switch($groupid){
            case '6':
                 $redirecturl = "listBankEAuditor";
                  break;
             case '5':
                 $redirecturl = "listReportBankUser";
                  break;
             case '2':
                 $redirecturl = "index";
                  break;
             case '12':
                  $redirecturl = "listHead";
                  break;
            case '13':
                  $redirecturl = "listReportCountryAdmin";
                  break;
            default:
                  $redirecturl = "index";
                  break;
        }
       
        // $userDetailId = "";
        if($userDetailObj->count() > 0) {
            $pass_word = PasswordHelper::generatePassword();

         /*Mail firing code starts*/
            $bank_acronym = $user_details->getBankUser()->getFirst()->getBank()->getAcronym();
            $subject = "Your New Password for Pay4Me" ;
            $partialName = 'passwordUnblock_email' ;
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $login_url = url_for('@bank_login?bankName='.$bank_acronym, true);
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$this->moduleName."/sendEmail", array('userid'=>$user_id,'password'=>$pass_word,'subject'=>$subject,'partialName'=>$partialName,'login_url'=>$login_url));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            $user_details->getUserDetail()->getFirst()->setFailedAttempt(0);
            $user_details->setPassword($pass_word);
            $user_details->setLastLogin(NULL);
            $user_details->setIsActive(true);
            $user_details->save();
            if($request->hasParameter('redirect_to')){
                if($request->getParameter('redirect_to') == "create_bank_branch_user"){
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_UNBLOCKED;
                }elseif($request->getParameter('redirect_to') == "bank_user"){
                    $msg =  EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_UNBLOCKED;
                }
            }
            else{
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_USER_UNBLOCKED;
            }
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user_details->getUsername(), $user_details->getId()));
            $eventHolder = new pay4meAuditEventHolder(
                EpAuditEvent::$CATEGORY_TRANSACTION,
                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_UNBLOCKED,
                EpAuditEvent::getFomattedMessage($msg, array('username' => $user_details->getUsername())),
                $applicationArr);
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            /*Audit Trail code ends*/

            if(sfConfig::get('app_display_password'))
                $this->getUser()->setFlash('notice','User - '.$user_details->getUsername().' unblocked successfully<br> The New Password is - '.$pass_word , true);
            else
                $this->getUser()->setFlash('notice','User - '.$user_details->getUsername().' unblocked successfully<br> An email has been sent to user regarding the same. ' , true);
            if($request->getParameter('backurl')){
               $redirecturl=$request->getParameter('backurl');
            }
            if($request->getParameter('redirect_to')){
                $this->redirect($request->getParameter('redirect_to').'/'.$redirecturl);
            }
            else{
                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }
        else {
            $this->getUser()->setFlash('notice','OOps!! We do not have the complete details of the User - '.$user_details->getUsername().'. Kindly edit the user details first so that he can be unblocked' , true);
            if($request->getParameter('redirect_to')){
                $this->redirect($request->getParameter('redirect_to').'/index');
            }
            else{
                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }
    }

    public function executeResetPassword(sfWebRequest $request) {
        $userId = $request->getParameter('userId');
        $user_list = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        $group =  Doctrine::getTable('sfGuardUserGroup')->findByUserId($request->getParameter('userId'));//print_r($group);
        $groupid = $group->getFirst()->getGroupId();
//        if($groupid == 6)
//        $redirecturl = "listBankEAuditor";
//        elseif($groupid == 5)
//        $redirecturl = "listReportBankUser";
//        elseif($groupid == 12)
//        $redirecturl = "listHead";
//        elseif($groupid == 2 || $groupid == 3 || $groupid == 9)
//        $redirecturl = "index";
         switch($groupid){
            case '6':
                 $redirecturl = "listBankEAuditor";
                  break;
             case '5':
                 $redirecturl = "listReportBankUser";
                  break;
             case '2':
                 $redirecturl = "index";
                  break;
             case '12':
                  $redirecturl = "listHead";
                  break;
             case '13':
                  $redirecturl = "listReportCountryAdmin";
                  break;
            default:
                  $redirecturl = "index";
                  break;
        }

        $pass_word = PasswordHelper::generatePassword();
        $user_password_update = Doctrine::getTable('sfGuardUser')->updateUserPasswordById($userId,$pass_word);
        $user_update_failedAtttempt = Doctrine::getTable('UserDetail')->updateFailedAttemptsById($userId,0);
        if($user_password_update=='1') {
            $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($userId);
            $bankName =  $sf_guard_user->getBankUser()->getFirst()->getBank()->getAcronym();

        /*Mail firing code starts*/
            $subject = "Your New Password for Pay4Me" ;
            $partialName = 'passwordChange_email' ;
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $login_url = url_for('@bank_login?bankName='.$bankName, true);
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$this->moduleName."/sendEmail", array('userid'=>$userId,'password'=>$pass_word,'subject'=>$subject,'partialName'=>$partialName,'login_url'=>$login_url));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
    /*Mail firing code ends*/

       /* Audit Trail code starts*/
            if($request->hasParameter('redirect_to')){
                if($request->getParameter('redirect_to') == "create_bank_branch_user"){ //for bank_branch user
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_SECURITY_PASSWORD_RESET;
                }else if($request->getParameter('redirect_to') == "bankBranchReportUser"){ //for bank_branch report user
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_REPORT_USER_SECURITY_PASSWORD_RESET;
              }else if($request->getParameter('redirect_to') == "bank_user"){
                    $msg = EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE;
                    $backUrl =$request->getParameter('backurl');
                    if(!empty($backUrl)){
                       $redirecturl= $backUrl;
                    }
                }
            }
            else{ //for bank admin
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_USER_SECURITY_PASSWORD_RESET;
            }

            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user_list[0]['username'], $user_list[0]['id']));
            $eventHolder = new pay4meAuditEventHolder(
                EpAuditEvent::$CATEGORY_SECURITY,
                EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_RESET,
                EpAuditEvent::getFomattedMessage($msg, array('username' => $user_list[0]['username'])),
                $applicationArr);

            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
      /*Audit Trail code ends*/

            $EpDBSessionDetailObj =  new EpDBSessionDetail();
            $EpDBSessionDetailObj->deleteSessionData(array(array('sigin_user_name' => $user_list[0]['username'])));
            if(sfConfig::get('app_display_password'))
                $this->getUser()->setFlash('notice','Password for  '.$user_list[0]["username"].' has been reset<br> The New Password is - '.$pass_word , true);
            else
                $this->getUser()->setFlash('notice','Password for  '.$user_list[0]["username"].' has been reset successfully<br> An email has been sent to user regarding the same.', true);
            if($request->getParameter('redirect_to')){
                $this->redirect($request->getParameter('redirect_to').'/'.$redirecturl);
            }
            else{

                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }else {
            $this->getUser()->setFlash('error','your are not authorized user !' , true);
            if($request->getParameter('redirect_to')){

                $this->redirect($request->getParameter('redirect_to').'/'.$redirecturl);
            }
            else{
                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }
    }
}