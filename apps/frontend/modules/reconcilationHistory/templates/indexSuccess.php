<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_helper('Form'); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead('Merchant Account Reconcilation History'); ?>

<table class="tGrid">
  <tr>
    <td>
    <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
        <tr>
          <td class="horizontal">Merchant Name</td>
          <td ><?php echo $merchantName; ?></td>
        </tr>
        <tr>
          <td class="horizontal">Merchant Service Name</td>
          <td><?php echo $merchantServiceName; ?></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
          <thead>
            <tr class="horizontal">
            <th width = "2%">S.No.</th>
            <th>Swap Date</th>
            <th>Swap Schedule Date</th>            
          </tr>
          </thead>
        <tbody>
          <?php
          if((count($rconHistoryDetails)>0)) {
             $i = 0;
              foreach ($rconHistoryDetails as $result):
                $i++;
          ?>
          <tr>
            <td align="center"><?php echo $i ?></td>
            <td align="center">
              <?php
                echo link_to($result['created_at'], url_for('reconcilationHistory/paymentDetails?batchIds='.$result['batch_ids'].'&scheduleId='.$sf_request->getParameter('scheduleId')), array('popup' => array('popupWindow', 'width=825,height=500,left=150,top=0,scrollbars=1')));
              ?>
            </td>
            <td align="center"><?php echo $result['swap_date'] ?></td>
          </tr>
          <?php endforeach; ?>
          <?php
            }else {
          ?>
          <tr>
            <td align='center' class='error' colspan="5">No History Found For This Schedule.</td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot><tr><td colspan="5"></td></tr></tfoot>
      </table>
      </div>
    </td>
  </tr>
</table>

