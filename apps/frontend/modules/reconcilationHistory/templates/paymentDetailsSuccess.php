<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php // use_helper('Form'); ?>
<?php use_helper('Pagination');  ?>




<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Reconcilation Payment Record Details'); ?>
    </fieldset>
</div>

<div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
    <br class="pixbr" />
</div>

<table class="tGrid">

<tr><td >
<table border=0>
<tr>
    <td >Merchant Name</td>
    <td ><?php echo $merchantName; ?></td>
</tr>
<tr>
    <td>Merchant Service Name</td>
    <td><?php echo $merchantServiceName; ?></td>
</tr>
</table>
</td></tr>


<tr>
<td>

 <?php
        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
            foreach ($pager->getResults() as $result):
                $i++;
//                echo "<pre>";
//                print_r($result->toArray());
//                die;
?>
<tr><td>
Batch Id &nbsp; : &nbsp; <?php echo $result->getId() ?>

</td></tr>
<table class="tGrid">
    <thead>
        <tr>
            <th width = "2%">S.No.</th>
            <th>From Account</th>
            <th>To Account</th>
            <th>To Account Name</th>
            <th>Status</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>

       <?php
        if(count($result->getPaymentRecord())>0){
            $j = 0;
            foreach ($result->getPaymentRecord() as $payDetails):
            $j++;
       ?>
        <tr>
            <td align="center"><?php echo $j ?></td>
            <td align="center"><?php echo $result->getAccountNo() ?></td>
            <td align="center"><?php echo $payDetails->getAccountNo() ?></td>
            <td align="center"><?php echo $payDetails->getAccountName() ?></td>
            <td align="center">
            <?php
            if($payDetails->getStatus() == 1)
            echo "Send to NIBSS";
            if($payDetails->getStatus() == 22)
            echo "Verified by NIBSS";
            if($payDetails->getStatus() == 23)
            echo "Paid";
            if($payDetails->getStatus() == 81)
            echo "Regection From NIBSS";
            if($payDetails->getStatus() == 82)
            echo "Unpaid";
            ?></td>
            <td align="center"><?php echo $payDetails->getAmount() ?></td>
        </tr>
        <?php
        endforeach;
        }
        ?>
         <tr>
         <td colspan="2">
         Total Amount 
         </td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><?php echo $result->getTotalAmount() ?></td>
         </tr>
    </tbody>
    <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>

</td>
</tr>
  <?php endforeach;
          ?>
    <tr><td colspan="5">
<div class="paging pagingFoot"><?php  //echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?batchIds='.$sf_request->getParameter('batchIds').'&scheduleId='.$sf_request->getParameter('scheduleId')), 'theMiddle')
//pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?designation_id='.$sf_request->getParameter('batchIds')))
echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?batchIds='.$sf_request->getParameter('batchIds').'&scheduleId='.$sf_request->getParameter('scheduleId')));
?>

  </div></td></tr>
      <?php
    }else { ?>

        <tr><td  align='center' class='error' colspan="5">No Payment Record Found</td></tr>
        <?php } ?>
</table>
