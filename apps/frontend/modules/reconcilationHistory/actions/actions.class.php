<?php

class reconcilationHistoryActions extends sfActions {

    //     public $merchantName;
    //     public $merchantServiceName;
    //     public $merchantId;
    //     public $merchantServiceId;

    public function executeIndex(sfWebRequest $request){
        $this->setLayout('layout_popup');
        //if($request->isMethod('post')){
        $this->rconHistoryDetails = array();
        $this->merchantServiceName = '';
        $this->merchantName = '';
        $scheduleId = $request->getParameter('scheduleId');
        $reconMerchantDetailsObj = Doctrine::getTable('MerchantAcctReconcilationSchedule')->getReconScheduleDetails($scheduleId);
        if(isset($reconMerchantDetailsObj[0]['merchant_id'])){
            $merchantId = $reconMerchantDetailsObj[0]['merchant_id'];
            $merchantServiceId = $reconMerchantDetailsObj[0]['merchant_service_id'];
            $merchantDetailsObj = Doctrine::getTable('Merchant')->getMerchantDetailsById($merchantId);
            if(isset($merchantDetailsObj[0]['name'])){
                $this->merchantName = $merchantDetailsObj[0]['name'];
                $merchantServiceDetailsObj = Doctrine::getTable('MerchantService')->getMerchantServiceName($merchantServiceId);
                if(isset($merchantServiceDetailsObj[0]['name'])){
                    $this->merchantServiceName = $merchantServiceDetailsObj[0]['name'];
                    $this->rconHistoryDetails = Doctrine::getTable('MerchantAcctReconcilationHistory')->getReconHistoryDetailsName($scheduleId);
                    //}else{
                    //    $this->rconHistoryDetails = array();
                    //}
                }
            }
        }
    }



    public function executePaymentDetails(sfWebRequest $request){

        $this->setLayout('layout_popup');
        $batchIds = $request->getParameter('batchIds');
        $scheduleId = $request->getParameter('scheduleId');
        $this->PaymentDetails = '';
        $this->merchantName = '';
        $this->merchantServiceName ='';
        if($batchIds != ''){
            $batchIdsArr = explode(",",$batchIds);

            $reconMerchantDetailsObj = Doctrine::getTable('MerchantAcctReconcilationSchedule')->getReconScheduleDetails($scheduleId);
            if(isset($reconMerchantDetailsObj[0]['merchant_id'])){
                $merchantId = $reconMerchantDetailsObj[0]['merchant_id'];
                $merchantServiceId = $reconMerchantDetailsObj[0]['merchant_service_id'];
                $merchantDetailsObj = Doctrine::getTable('Merchant')->getMerchantDetailsById($merchantId);
                if(isset($merchantDetailsObj[0]['name'])){
                    $this->merchantName = $merchantDetailsObj[0]['name'];
                    $merchantServiceDetailsObj = Doctrine::getTable('MerchantService')->getMerchantServiceName($merchantServiceId);
                    if(isset($merchantServiceDetailsObj[0]['name'])){
                        $this->merchantServiceName = $merchantServiceDetailsObj[0]['name'];
                        $this->PaymentDetails = Doctrine::getTable('MerchantAcctReconcilationHistory')->getAllPaymentHistory($batchIdsArr);
                    }
                }
            }
            $this->page = 1;
            if($request->hasParameter('page')){
                $this->page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('sfGuardAuth',10);
            $this->pager->setQuery($this->PaymentDetails);
            $this->pager->setPage($this->page);
            $this->pager->init();


        }

    }

}

?>
