<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Etranzact configuration List'); ?>
    </fieldset>
</div>
<div class="paging pagingHead">

    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
    <br class="pixbr" />
  </div>
<table class="tGrid">
  <thead>
    <tr>
      <th width = "2%">S.No.</th>
      <th>Merchant Name</th>
      <th>Merchant service</th>
      <th>Terminal ID</th>
      <th>Transaction url</th>
      <th>Site redirect url</th>
      <th>Description of transaction</th>
    </tr>
  </thead>
  <tbody>
   <?php
     if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_per_page_records');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
     ?>
    <tr>
      <td><?php echo $i ?></td>
      <td align="center"><?php echo $result->getMerchantName() ?></td>
      <td align="center"><?php echo $result->getMerchantServiceName() ?></td>
      <td align="center"><?php echo $result->getTerminalId() ?></td>
      <td align="center"><?php echo $result->getTransactionUrl() ?></td>
      <td align="center"><?php echo $result->getSiteRedirectUrl() ?></td>
      <td align="center"><?php echo link_to(' ', 'etranzact_configuration/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
      <?php echo link_to(' ', 'etranzact_configuration/delete?id='.$result->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?', 'class' => 'delInfo', 'title' => 'Delete')) ?></td>
    </tr>
    <?php endforeach; }else{ ?>
     <tr><td  align='center' class='error' colspan="4">No Etranzact configuration found</td></tr>
    <?php } ?>
  </tbody>
</table>
<a href="<?php echo url_for('etranzact_configuration/new') ?>">New</a>
