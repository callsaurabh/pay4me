<div align="center" id="wrapForm2" class="wrapForm2">
    <div id="recharge"><div><br>
            <div id="notice">
                <span id="clickHere" style="display:none;">
                    <span class="cRed">
                        If you have entered your card details below and have not received any response for the transaction, please
                        <?php echo link_to("Click here",$sf_context->getModuleName().'/queryHistory?orderId='.$retObj['transaction_id'],array("onclick"=>"return checkstatus()")); ?> to check the status.
                    </span>
                </span>
                <br>
                Do not click the Refresh or Back button while your transaction is Processing.
            </div>
            <br>
        </div>
    </div>
</div>
<script type='text/javascript'>
    var t=setTimeout("refreshWin()",<?php echo $timeOut;?>);

    function refreshWin()
    {
        $('#clickHere').show();
    }
    function checkstatus() {
        var answer = confirm("Are you sure you want to Proceed?")
        if (answer) {
            return true;
        }
        return false;
    }
</script>

<div> 
   <iframe width="100%" name="etranzact" height="490px" src ="<?php echo url_for('etranzact_configuration/etranzactRedirect');?>"></iframe>
   <form name='form1' id='form1' method='post' action='<?php echo $retObj['post_url']; ?>' target="etranzact">
    <input type=hidden name = 'TERMINAL_ID' value='<?php echo $retObj['terminal_id']; ?>'>
    <input type=hidden name = 'RESPONSE_URL' value='<?php echo $retObj['response_url']; ?>'>
    <input type=hidden name = 'TRANSACTION_ID' value='<?php echo $retObj['transaction_id']; ?>'>
    <input type=hidden name = 'AMOUNT' value='<?php echo $retObj['amount']; ?>'>
    <input type=hidden name = 'DESCRIPTION' value='<?php echo $retObj['description']; ?>'>
    <input type=hidden name = 'CHECKSUM' value='<?php echo $retObj['check_sum']; ?>'>
    <input type=hidden name = 'LOGO_URL' value='<?php echo $retObj['logo_url']; ?>'>
    </form>
</div>
<script type='text/javascript'>document.forms[0].submit();</script>
