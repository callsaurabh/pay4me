<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script language="Javascript">

$(document).ready(function()
{
  
    var merchant_id = "<?php echo $merchant_id;?>";
    if(merchant_id!="")//in case of edit, bank needs to be selected, in case of new, it will be blank
    {
        document.getElementById("etranzact_configuration_merchant_id").value = merchant_id;
    }
    if(document.getElementById("etranzact_configuration_merchant_id").value != "")
        {
            var merchant_service_id = "<?php echo $merchant_service_id;?>";
            var merchant_id = document.getElementById("etranzact_configuration_merchant_id").value;
            var module = "etranzact_configuration";
            displayMerchantService(merchant_id,module,merchant_service_id,'<?php echo url_for('etranzact_configuration/MerchantService'); ?>');

        }
   $('#etranzact_configuration_merchant_id').change(function(){
      var merchant_id = this.value;
      var module = "etranzact_configuration";
      displayMerchantService(merchant_id,module,"",'<?php echo url_for('etranzact_configuration/MerchantService'); ?>');
      return false;
  }
  );
});
</script>
<form action="<?php echo url_for('etranzact_configuration/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>        
        <td colspan="2">                  
          &nbsp;<?php  echo button_to('Cancel','',array('onClick'=>'location.href=\''.url_for('etranzact_configuration/index').'\''));?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form ?>
    </tbody>
  </table>
</form>
