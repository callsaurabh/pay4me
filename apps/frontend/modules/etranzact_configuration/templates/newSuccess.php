<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Add New Etranzact configuration'); ?>
    </fieldset>
</div>
<?php include_partial('form', array('form' => $form,'merchant_service_id'=>$merchant_service_id,'merchant_id'=>$merchant_id)) ?>
