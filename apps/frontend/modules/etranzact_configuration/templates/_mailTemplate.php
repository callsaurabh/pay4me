<b>Hi <?php echo ucwords($firstname."  ".$lastname); ?>!</b>
<br /><br />Payment Successfully Done on <b>Pay4Me  .<br /><br /></b>
<b>Your Order number is</b>: <?php echo $orderDetailId; ?>

<?php $rDetails = unserialize(base64_decode($request_details));?>
<br /><br />You have successfully done payment on Pay4Me, transfers and other services (published terms of use apply).<br /><br/>

<table width="100%" cellpadding="3" cellspacing="0">
  <tr style="color:#00345A;background-color:#E7F0F7;">
    <td  colspan="4">Order Date - <?php  echo $rDetails[0];?> </td>
  </tr>
  <tr  style="color:#00345A;background-color:#E7F0F7;">
    <td width="25%" ><b>Payment Status</b></td>
    <td width="77%"><b>Item</b></td>
    <td width="23%" align="right"><b>Price</b></td>
  </tr>
  <tr>

    <td ><span >Payment Done! </span></td>
    <td ><span ><?php  echo $rDetails[1];?></span></td>
    <td align="right" ><?php echo format_amount($rDetails[2],$rDetails[3]);?></td>
  </tr>
  <tr style="color:#00345A;background-color:#E7F0F7;font-size:18px;">

    <td >&nbsp;</td>
    <td  align="right" >Total:</td>
    <td align="right"><?php echo format_amount($rDetails[2],$rDetails[3]); ?></td>
  </tr>
 <!-- <tr class="blTxt">
    <td colspan="3" class="txtBold" align="left"><b>Paid with :</b><?php  //echo $rDetails[3]; ?></td>
  </tr>-->
</table>

<br><br>
Please look out for our newsletter in your email informing you of ways to utilize your account. <br /><br />PAY4ME.....KEEPING IT SIMPLE"<br /><br />

<?php echo $signature;?>


