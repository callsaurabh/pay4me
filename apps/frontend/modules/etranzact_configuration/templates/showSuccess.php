<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $etranzact_configuration->getid() ?></td>
    </tr>
    <tr>
      <th>Merchant service:</th>
      <td><?php echo $etranzact_configuration->getmerchant_service_id() ?></td>
    </tr>
    <tr>
      <th>Terminal:</th>
      <td><?php echo $etranzact_configuration->getterminal_id() ?></td>
    </tr>
    <tr>
      <th>Transaction url:</th>
      <td><?php echo $etranzact_configuration->gettransaction_url() ?></td>
    </tr>
    <tr>
      <th>Site redirect url:</th>
      <td><?php echo $etranzact_configuration->getsite_redirect_url() ?></td>
    </tr>
    <tr>
      <th>Description of transaction:</th>
      <td><?php echo $etranzact_configuration->getdescription_of_transaction() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $etranzact_configuration->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $etranzact_configuration->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $etranzact_configuration->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $etranzact_configuration->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $etranzact_configuration->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('etranzact_configuration/edit?id='.$etranzact_configuration->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('etranzact_configuration/index') ?>">List</a>
