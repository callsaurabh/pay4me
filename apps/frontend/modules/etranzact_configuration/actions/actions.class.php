<?php

/**
 * etranzact_configuration actions.
 *
 * @package    mysfp
 * @subpackage etranzact_configuration
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class etranzact_configurationActions extends sfActions
{

  public function executeEtranzactRecharge(sfWebRequest $request) { 
    $transNo = $request->getParameter('transNo');//this is the account id
    $orderId = $request->getParameter('orderId');
    $type = $request->getParameter('type');
    $amount = round($request->getParameter('totalCharges'), 2) ;

    $epEtranzactManager = new EpEtranzactManager();
    $epEtranzactManager->terminalId = sfConfig::get('app_et_terminal_id');

    $host = $request->getUriPrefix() ;
    $url_root = $request->getPathInfoPrefix();
    $response_url = $host.$url_root."/".sfConfig::get('app_et_response_url');
    $epEtranzactManager->responseUrl = $response_url;
    $epEtranzactManager->description = sfConfig::get('app_et_description');
    $check_sum_value = $amount.sfConfig::get('app_et_terminal_id').$orderId.$response_url.sfConfig::get('app_et_secret_key');
    $epEtranzactManager->check_sum = md5($check_sum_value);
    $epEtranzactManager->logoUrl = EtranzactConfig::getLogoURL();
    $epEtranzactManager->postUrl = sfConfig::get('app_et_post_url');
    $epEtranzactManager->amount = $amount;
    $epEtranzactManager->trnxId = $orderId;
    $epEtranzactManager->userId = $this->getUser()->getGuardUser()->getId();
    $retObj = $epEtranzactManager->setRequest();
    $this->timeOut = Settings::getEtranzactTimeInSecForTimeOut()*1000;

    $this->retObj = $retObj->toArray();   
    $this->setTemplate('etranzactForm');
  }

  public function executeIndex(sfWebRequest $request)
  {
    $etranzact_configuration_obj = etranzactConfigurationServiceFactory::getService(etranzactConfigurationServiceFactory::$TYPE_BASE);
    $this->etranzact_configuration_list = $etranzact_configuration_obj->getAllRecords();

    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('EtranzactConfiguration',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->etranzact_configuration_list);
    $this->pager->setPage($this->page);
    $this->pager->init();

  }

  public function executeShow(sfWebRequest $request)
  {
    $this->etranzact_configuration = Doctrine::getTable('EtranzactConfiguration')->find($request->getParameter('id'));
    $this->forward404Unless($this->etranzact_configuration);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->merchant_id=""; //initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
    $this->merchant_service_id=""; //same as the reason above
    $this->form = new EtranzactConfigurationForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    $params=$request->getPostParameters('etranzact_configuration');
    $this->merchant_service_id=$params['etranzact_configuration']['merchant_service_id'];
    $this->merchant_id=$params['etranzact_configuration']['merchant_id'];

    $this->form = new EtranzactConfigurationForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($etranzact_configuration = Doctrine::getTable('EtranzactConfiguration')->find($request->getParameter('id')), sprintf('Object etranzact_configuration does not exist (%s).', $request->getParameter('id')));
    $this->etranzact_configuration = Doctrine::getTable('EtranzactConfiguration')->find($request->getParameter('id'));
    $this->merchant_service_id = $this->etranzact_configuration->merchant_service_id;
    $this->merchant_service = Doctrine::getTable('MerchantService')->find($this->merchant_service_id);
    $this->merchant_id = $this->merchant_service->merchant_id;

    $this->form = new EtranzactConfigurationForm($etranzact_configuration);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($etranzact_configuration = Doctrine::getTable('EtranzactConfiguration')->find($request->getParameter('id')), sprintf('Object etranzact_configuration does not exist (%s).', $request->getParameter('id')));
    $this->form = new EtranzactConfigurationForm($etranzact_configuration);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($etranzact_configuration = Doctrine::getTable('EtranzactConfiguration')->find($request->getParameter('id')), sprintf('Object etranzact_configuration does not exist (%s).', $request->getParameter('id')));
    $etranzact_configuration->delete();

    $this->redirect('etranzact_configuration/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $etranzact_configuration = $form->save();

      $this->redirect('etranzact_configuration/index');
    }
  }

  public function executeMerchantService(sfWebRequest $request) {
    $this->setTemplate(FALSE);
    $merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BANK);
    $str = $merchant_service_obj->getServices($request->getParameter('merchant_id'),$request->getParameter('merchant_service_id'));
    return $this->renderText($str);
  }


  public function executeEtranzactForm(sfWebRequest $request) {
    $this->timeOut = Settings::getEtranzactTimeInSecForTimeOut()*1000;
    $transNo = $request->getParameter('transNo');
    $orderId = $request->getParameter('orderId');    
    $paymentModeOption = $request->getParameter('paymentModeOption');
    $validateTrans = $this->validateTransactionNumber($transNo, $paymentModeOption);
    if($validateTrans) {
      $item_id = $validateTrans['MerchantRequest']['merchant_item_id'];
      $payment_status = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
    if($payment_status == 0) {
        $this->getUser()->setFlash('notice', 'Payment has already been made for this Application', true) ;
        $delGatewayOrders = Doctrine::getTable('GatewayOrder')->deleterecords($transNo,$orderId);//FS#30249
        $merchant_obj = Doctrine::getTable('MerchantRequest')->getMerchantPaidId($item_id);
        $merchant_request_payment_id = $merchant_obj->getFirst()->getId();
        $transaction_number_payment_id = $merchant_obj->getFirst()->getTransaction()->getFirst()->getTransactionNumber();

        return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'etranzact_configuration',
            'action' => 'payOnline','returnVal' =>base64_encode($transaction_number_payment_id),'duplicate_payment_receipt'=>true))."'</script>");
      }else {
         //check  is there any last transaction is in pending status
        $arrDetails = Doctrine::getTable('GatewayOrder')->getLastOrderDetails($transNo,'',1);

        //check last transaction status
        if(count($arrDetails)>0 && strtolower($arrDetails[0]['status'])=='pending'){
              $this->verifyPayment($arrDetails[0]['order_id']);
            //$this->forward('etranzact_configuration', 'status');
        }

        $txnArr = Doctrine::getTable('Transaction')->getDetailByItemId($item_id);
        $appArr = array();
        foreach($txnArr as $txn){
            if($txn['pfm_transaction_number'] != $transNo ){
                $appArr[] = $txn['pfm_transaction_number'];
            }
        }
        if(count($appArr)>0){
            $status = 'pending';
            $gatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByAppId($appArr,$paymentModeOption,$status);
            if($gatewayOrders){
                $lastPendingOrderId = $gatewayOrders->getfirst()->getOrderId();
                $lastPendingAppId = $gatewayOrders->getfirst()->getAppId();
                 $this->verifyPayment($lastPendingOrderId);
                //query etranzact status and update for same
                //$this->forward('etranzact_configuration','status');
            }
        }
        
        $epEtranzactManager = new EpEtranzactManager();
        $epEtranzactManager->terminalId = sfConfig::get('app_et_terminal_id');
        
        $host = $request->getUriPrefix() ;
        $url_root = $request->getPathInfoPrefix();
        $response_url = $host.$url_root."/".sfConfig::get('app_et_response_url');
        $epEtranzactManager->responseUrl = $response_url;
        $epEtranzactManager->description = sfConfig::get('app_et_description');
        $amount = $this->getAmount($transNo);
        $check_sum_value = $amount.sfConfig::get('app_et_terminal_id').$orderId.$response_url.sfConfig::get('app_et_secret_key');
        $epEtranzactManager->check_sum = md5($check_sum_value);
        $epEtranzactManager->logoUrl = EtranzactConfig::getLogoURL();
        $epEtranzactManager->postUrl = sfConfig::get('app_et_post_url');
        $epEtranzactManager->amount = $amount;
        $epEtranzactManager->trnxId = $orderId;
        $epEtranzactManager->userId = $this->getUser()->getGuardUser()->getId();
        $retObj = $epEtranzactManager->setRequest();

        $this->retObj = $retObj->toArray();

        $this->setTemplate('etranzactForm');
      }
    }else {
      return $this->renderText('error');
    }
  }
//  public function executeStatus(sfWebRequest $request)
//  {
//      $orderId = $request->getParameter('orderId');
//      $transNo = $request->getParameter('transNo');
//      $paymentModeOption = $request->getParameter('paymentModeOption');
//      $validateTrans = $this->validateTransactionNumber($transNo, $paymentModeOption);
//      $item_id = $validateTrans['MerchantRequest']['merchant_item_id'];
//
//      $arrDetails = Doctrine::getTable('GatewayOrder')->getLastOrderDetails($transNo,'',1);
//      if(count($arrDetails)>0 && strtolower($arrDetails[0]['status'])=='pending'){
//            $this->transactionId = $arrDetails[0]['order_id'];
//
//       } else {
//        $txnArr = Doctrine::getTable('Transaction')->getDetailByItemId($item_id);
//        $appArr = array();
//        foreach($txnArr as $txn){
//            if($txn['pfm_transaction_number'] != $transNo ){
//                $appArr[] = $txn['pfm_transaction_number'];
//            }
//        }
//        if(count($appArr)>0){
//            $status = 'pending';
//            $gatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByAppId($appArr,$paymentModeOption,$status);
//            if($gatewayOrders){
//                $lastPendingOrderId = $gatewayOrders->getfirst()->getOrderId();
//                $lastPendingAppId = $gatewayOrders->getfirst()->getAppId();
//                //query interswitch status and update for same
//                $this->transactionId = $lastPendingOrderId;
//
//            }
//        }
//      }
//      $this->terminalId = sfConfig::get('app_et_terminal_id');
//      $host = $request->getUriPrefix() ;
//      $url_root = $request->getPathInfoPrefix();
//      $this->queryPostUrl = sfConfig::get('app_et_query_url');
//      $this->responseUrl = $host.$url_root."/".'etranzact_configuration/etranzactVerify';
//  }
//  public function executeEtranzactVerify(sfWebRequest $request){
//        $postData = $_POST;
//        $this->logResponse($postData);
//        $orderArray = $this->getByOrderId($postData['TRANSACTION_ID']);
//        $txnId = $orderArray['app_id'];
//
//
//        $paymentStatusMsg = GatewayStatusMessages::getEtranzactStatusMessage($postData['SUCCESS']);
//        $getData['desc'] = $paymentStatusMsg;
//        $getData['SUCCESS'] = $postData['SUCCESS'];
//        $getData['TRANSACTION_ID'] = $postData['TRANSACTION_ID'];
//        $getData['AMOUNT'] = $postData['TRANS_AMOUNT'];
//
//        $userId = $this->getUser()->getGuardUser()->getId();
//           //get current transaction details
//        $arrDetails = Doctrine::getTable('GatewayOrder')->currentOrderDetails($userId,'');
//        Doctrine::getTable('GatewayOrder')->deleterecords($arrDetails[0]['app_id'],$arrDetails[0]['order_id']);
//       if($postData['SUCCESS'] == 0) {
//            //soft delete current transaction
//           // Doctrine::getTable('GatewayOrder')->deleterecords($txnId,$getData['TRANSACTION_ID']);
//
//           return $this->successResponse($getData, $orderArray);
//
//      }else{
//          //do failure
//
//          $this->updateFailure($getData, $orderArray);
//          $this->redirect('bill/payByCard?transNo='.$arrDetails[0]['app_id']);
//      }
//
//
//
//  }
  protected function verifyPayment($orderId){
    
      $orderArray = $this->getByOrderId($orderId);
     
      $terminalId = sfConfig::get('app_et_terminal_id');

      $getData = $this->getStatusMsg($orderId, $terminalId);

      $userId = $this->getUser()->getGuardUser()->getId();
      //get current transaction details

      

      if($getData['SUCCESS'] == 0) {
                $arrDetails = Doctrine::getTable('GatewayOrder')->currentOrderDetails($userId,'');

            Doctrine::getTable('GatewayOrder')->deleterecords($arrDetails[0]['app_id'],$arrDetails[0]['order_id']);
          return $this->successResponse($getData, $orderArray);

      }else{
          //do failure
          
          return $this->updateFailure($getData, $orderArray);
           
      }



  }
  private function updateFailure($getData,$orderArray)
  {
      $txnId = $orderArray['app_id']; //ep_master_account id in case of Recharge; and transaction_number in case of payment
      $type = $orderArray['type'];
      $getData['status'] = 'failure';

     return $orderId = $this->updateGatewayOrder($getData);

  }
  protected function validateTransactionNumber($transNo, $paymentModeOption) {
    return $merchantRecord = Doctrine::getTable('Transaction')->validateTransactionNumber($transNo, $paymentModeOption);
  }

  protected function getAmount($transNo) {
    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $formData = $payForMeObj->getTransactionRecord($transNo);
    return $formData['total_amount'];
  }

  public function executeEtranzactRedirect(sfWebRequest $request) {
    $this->pageTitle = 'Card Payment';
    return $this->renderText('Please wait...');
  }

//  public function executeEtranzactResponse(sfWebRequest $request) {
//    $getData =  $_POST;
//    $this->logResponse($getData);
//
//    $order_id = $getData['TRANSACTION_ID'];
//    $amount_paid = $getData['AMOUNT'];
//    $status = $getData['SUCCESS'];
//
//    $epEtranzactManager = new EpEtranzactManager();
//    $saveResponse = $epEtranzactManager->saveResponse($getData);
//
//    $orderArray = $this->getByOrderId($order_id);
//
//    if($orderArray) {
//      $txnId = $orderArray['app_id']; //ep_master_account id in case of Recharge; and transaction_number in case of payment
//      $type = $orderArray['type'];
//      $payment_mode_option_id = $orderArray['payment_mode_option_id'];
//
//      $paymentStatusMsg = GatewayStatusMessages::getEtranzactStatusMessage($status);
//      $getData['desc'] = $paymentStatusMsg;
//      $this->getLock($order_id);
//
//      if(strtolower($type) == 'payment') {
//        if($status == '0')
//        {
//          $getData['status'] = 'success';
//          $orderId = $this->updateGatewayOrder($getData);
//
//          $paymentObj =  new Payment($txnId);
//          $arrayVal = $paymentObj->proceedToPay();
//          $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], true) ;
//          if($arrayVal['comments'] == 'error') {
//            return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
//                  'action' => 'PayByCard','transNo' =>$arrayVal['var_value']))."'</script>");
//          }else {
//            $this->addJobForMail($arrayVal['var_value']);
//
//          $gatewayOrderObj = new gatewayOrderManager();
//          $pfmTransactionDetails = $gatewayOrderObj->getValidationNumber($arrayVal['var_value']);
//          $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
//          $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo,$orderId);
//
//            return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'etranzact_configuration',
//                  'action' => 'payOnline','returnVal' =>base64_encode($arrayVal['var_value'])))."'</script>");
//          }
//
//        }else{
//          $getData['status'] = 'failure';
//          $orderId = $this->updateGatewayOrder($getData);
//
//          $this->getUser()->setFlash('error', $paymentStatusMsg, true) ;
//          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
//                  'action' => 'PayByCard','transNo' =>$txnId))."'</script>");
//        }
//      }
//      else if(strtolower($type) == 'recharge' ) {
//        $total_amount_paid = $amount_paid * 100;  //convert it into KOBO
//        if($status == '0')
//        {
//          $service_charge = $orderArray['servicecharge_kobo'];
//          $getData['status'] = 'success';
//          $validationNo = $this->processToRecharge($getData,$orderArray,$txnId, $payment_mode_option_id);
//
//          $validationNo = $this->recharge($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);
//
//          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
//                'action' => 'accountBalance'))."'</script>");
//          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
//                    'action' => 'rechargeReceipt','validationNo'=> base64_encode($validationNo)))."'</script>");
//        }else{
//          $getData['status'] = 'failure';
//          $orderId = $this->updateGatewayOrder($getData);
//
//          $this->getUser()->setFlash('error', $paymentStatusMsg, true) ;
//          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
//                'action' => 'recharge','recharge_gateway'=>'etranzact'))."'</script>");
//        }
//
//      }
//    }
//    return false;
//  }

  public function executeEtranzactResponse(sfWebRequest $request) {
    $getData =  $_POST;
//    print "<pre>";print_r($getData);
    $this->logResponse($getData);
    $order_id = $getData['TRANSACTION_ID'];    
    $status = $getData['SUCCESS'];
    $etranObj = Doctrine::getTable('EpEtranzactRequest')->findByTransactionId($order_id);
    
    $epEtranzactManager = new EpEtranzactManager();
    $orderArray = $this->getByOrderId($order_id);
    $txnId = $orderArray['app_id'];
    $finalChecksum = md5($getData['SUCCESS'].$etranObj->getFirst()->getAmount().$etranObj->getFirst()->getTerminalId().$getData['TRANSACTION_ID'].$etranObj->getFirst()->getResponseUrl().sfConfig::get('app_et_secret_key'));
    $paymentStatusMsg = GatewayStatusMessages::getEtranzactStatusMessage($status);
    if (strtolower($finalChecksum) == strtolower($getData['FINAL_CHECKSUM']))  {

          $saveResponse = $epEtranzactManager->saveResponse($getData);
          
          if ($orderArray) {
              $this->getLock($order_id);
              
              $getData['desc'] = $paymentStatusMsg;
              if ($status == '0') {
                  return $this->successResponse($getData,$orderArray);
              } else {
                  return  $this->failureResponse($getData,$orderArray,$paymentStatusMsg);
              }

            

          }
    } else {//print "checksum does not match";
          $this->createErrorLog($finalChecksum,$getData['FINAL_CHECKSUM'],$order_id);
          
          $gatewayOrder = Doctrine::getTable('GatewayOrder')->findByOrderId($order_id);
         
          // $webpay = new webpay();
          $gatewayOrder->getFirst()->setStatus('failure');
         // $gatewayOrder->getFirst()->setResponseCode($status);
          //$gatewayOrder->getFirst()->setResponseTxt($paymentStatusMsg);
          $transDate = date('Y-m-d H:i:s');
          $gatewayOrder->getFirst()->setTransactionDate($transDate);
          $gatewayOrder->save();

          $this->getUser()->setFlash('error', 'Invalid Payment', true) ;
          if(strtolower($orderArray['type']) == 'recharge') {
              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
                'action' => 'recharge','recharge_gateway'=>'etranzact'))."'</script>");
         
          } if (strtolower($orderArray['type']) == 'payment') {           
                return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
                  'action' => 'PayByCard','transNo' =>$txnId))."'</script>");
          }
        
    }
   
//   $saveResponse = $epEtranzactManager->saveResponse($getData);
//
//    if (!is_string($saveResponse)) {
//        $orderArray = $this->getByOrderId($order_id);
//        if ($orderArray) {
//              $this->getLock($order_id);
//              $paymentStatusMsg = GatewayStatusMessages::getEtranzactStatusMessage($status);
//              $getData['desc'] = $paymentStatusMsg;
//
//              if ($status == '0') {
//                 return $this->successResponse($getData,$orderArray);
//              } else {
//                return  $this->failureResponse($getData,$orderArray,$paymentStatusMsg);
//              }
//        }
//
//    } else{
//        $this->getUser()->setFlash('error', $saveResponse, true) ;
//        return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
//               'action' => 'recharge','recharge_gateway'=>'etranzact'))."'</script>");
//    }    
  }

    /**
  * function successResponse()
  * @param $getData :response Array
  * @param $orderArray :order Array
  * Purpose : to handle succefull response from eTranzact
  */
  protected function successResponse($getData,$orderArray)
  {
     
      $txnId = $orderArray['app_id']; //ep_master_account id in case of Recharge; and transaction_number in case of payment
      $type = $orderArray['type'];
      $payment_mode_option_id = $orderArray['payment_mode_option_id'];
      if (strtolower($type) == 'payment') {

          $getData['status'] = 'success';
          $arrayVal = $this->processToPayment($getData, $txnId);
          $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], true) ;
          if ($arrayVal['comments'] == 'error') {
              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
                  'action' => 'PayByCard','transNo' =>$arrayVal['var_value']))."'</script>");
          }else {

              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'etranzact_configuration',
                  'action' => 'payOnline','returnVal' =>base64_encode($arrayVal['var_value'])))."'</script>");
          } 
      } else if(strtolower($type) == 'recharge') {
          $service_charge = $orderArray['servicecharge_kobo'];
          $getData['status'] = 'success';
          $validationNo = $this->processToRecharge($getData,$orderArray,$txnId, $payment_mode_option_id);
          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
                    'action' => 'rechargeReceipt','validationNo'=> base64_encode($validationNo)))."'</script>");
          
      }
  }

  /* function failureResponse()
  * @param $getData :response Array
  * @param $orderArray :order Array
  * @param $paymentStatusMsg :failure message
  * Purpose : to handle failure response from eTranzact
  */
  protected function failureResponse($getData,$orderArray,$paymentStatusMsg)
  {
      $txnId = $orderArray['app_id']; //ep_master_account id in case of Recharge; and transaction_number in case of payment
      $type = $orderArray['type'];
      $getData['status'] = 'failure';
      $orderId = $this->updateGatewayOrder($getData);

      if (strtolower($type) == 'payment') { 
          $this->getUser()->setFlash('error', $paymentStatusMsg, true) ;
          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
                  'action' => 'PayByCard','transNo' =>$txnId))."'</script>");
      } else if(strtolower($type) == 'recharge') {
          $this->getUser()->setFlash('error', $paymentStatusMsg, true) ;
          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
                'action' => 'recharge','recharge_gateway'=>'etranzact'))."'</script>");

      }
  }


  protected function processToRecharge($response,$orderArray,$txnId, $payment_mode_option_id) {
      $orderArray = $this->getByOrderId($response['TRANSACTION_ID']); 
      if($orderArray['status']!= "success") {
          $orderId = $this->updateGatewayOrder($response);
          $total_amount_paid = $response['AMOUNT']*100;

          $service_charge = $orderArray['servicecharge_kobo'];
          $gatewayOrderObj = new gatewayOrderManager();
          $validationNo = $this->recharge($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);
          $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo,$orderId);

          $this->addJobForEtranzactRechageMail($txnId,$orderId);
          return $validationNo;
      }    
      return $orderArray['validation_number'];

  }

   /**
  * function addJobForEtranzactRechageMail()
  * @param $validation_number :validation number
  * Purpose : add job to send ewallet payment
  */


  protected function addJobForEtranzactRechageMail($txnId,$orderId) {
    $notificationUrl = 'email/sendEtranzactRechargeMail';
    
    $taskId = EpjobsContext::getInstance()->addJob('MailNotification',$notificationUrl, array('p4m_transaction_id' => $txnId,'order_id'=>$orderId));

    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
  }

  public function executePayOnline(sfWebRequest $request) {
    $txn_no = base64_decode($request->getParameter('returnVal'));

    if($request->hasParameter('duplicate_payment_receipt')) {
      $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
    }

    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);

    $this->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
    $this->forward('paymentSystem', 'payOnline');
  }

  public function executeEtranzactFormRedirect(sfWebRequest $request) {
    $this->setLayout(false);
    $order_id = base64_decode($request->getParameter('id'));
    $epEtranzactManager = new EpEtranzactManager();
    $this->retObj = $epEtranzactManager->selectAllFromRequest($order_id);
    $this->setTemplate('etranzactFormRedirect');
  }

  private function logResponse($getData)
  {
    $pay4meLog = new pay4meLog();
    $nameFormate = 'response';
    $pay4meLog->createLogData($getData, $nameFormate, 'etranzactLog');
  }

  protected function getByOrderId($order_id) {
    $orderArr = Doctrine::getTable('GatewayOrder')->getByOrderId($order_id);
    return $orderArr;
  }

  public function executeSendMail(sfWebRequest $request) {
    $this->setLayout(null);
    $mailingClass = new Mailing();
    $partialName = 'mailTemplate';

    $mailingOptions = array();
    $mailInfo = 'Mail is sent successfully';

    $txnId = $request->getParameter('p4m_transaction_id');
    $partialVars = pfmHelper::getMailData($txnId);
    if(count($partialVars))
    {
      $mailingOptions = $partialVars['mailingOptions'];
      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
      return $this->renderText($mailInfo);
    }
  }

  protected function addJobForMail($txnId) {
    $notificationUrl = 'etranzact_configuration/sendMail';
    $taskId = EpjobsContext::getInstance()->addJob('MailNotification',$notificationUrl, array('p4m_transaction_id' => $txnId));

//    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
 }

  private function recharge($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {

    if(settings::getEtranzactClearanceDays()) {
      return $this->rechargeLater($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);
    }
    else {
      return $this->rechargeImmediate($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);

    }

  }

  private function rechargeLater($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {
    $con = Doctrine_Manager::connection();
    try {
      $con->beginTransaction();

      sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
      $days = settings::getEtranzactClearanceDays();
      $recharge_date = getRechargeDate($days);
      $reachargeObj = rechargeAmountAvailabilityServiceFactory::getService();
      $recharge_id = $reachargeObj->createNew($total_amount_paid,$total_amount_paid,$service_charge,$txnId,$recharge_date,$payment_mode_option_id);

      //create a new task
      $taskId = EpjobsContext::getInstance()->addJob('RechargeEwallet', 'recharge_ewallet/rechargeViaCard', array('recharge_id'=>$recharge_id), -1, NULL, $recharge_date );
      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");



      //create credit , unclear, direct entries for collection_acct, eWallet_user_acct and  pay4me collection account
      $userObj = $this->getUser();
      $ewallet_account_id =  $userObj->getUserAccountId();
      $ewallet_number = $userObj->getUserAccountNumber();
      $accountingObj = new pay4meAccounting;
      $is_clear = 0;
      $accountingArr = $accountingObj->getRechargeAccounts($ewallet_number,$ewallet_account_id,$total_amount_paid,$payment_mode_option_id,$is_clear,EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT, $service_charge);
      if(is_array($accountingArr)) {

        //auditing that ewallet has been charged
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_ETRANZACT;
        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_TRANSACTION,
          EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_ETRANZACT,
          EpAuditEvent::getFomattedMessage($msg, array('account_number'=>$ewallet_number)));

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

        $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));

      }
      $con->commit();
      $clearance_days = settings::getEtranzactClearanceDays();
      if($clearance_days == 1) {
        $clearance_days .= " day.";
      }
      else {
        $clearance_days .= " days.";
      }
      $this->getUser()->setFlash('notice', 'Payment Successful. The amount clearance will take '.$clearance_days , true) ;
          /*return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
                'action' => 'accountBalance'))."'</script>");*/
      return $event->getreturnValue();

    }
    catch (Exception $e) {
      $con->rollback();
      throw $e;
    }
  }

  private function rechargeImmediate($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge)
  {
    $con = Doctrine_Manager::connection();
    try {
      $con->beginTransaction();
      $userObj = $this->getUser();
      $ewallet_account_id =  $userObj->getUserAccountId();
      $ewallet_number = $userObj->getUserAccountNumber();
      $accountingObj = new pay4meAccounting;
      $is_clear = 1;
      $accountingArr = $accountingObj->getRechargeAccounts($ewallet_number,$ewallet_account_id,$total_amount_paid,$payment_mode_option_id,$is_clear,EpAccountingConstants::$TRANSACTION_TYPE_DIRECT, $service_charge);
      if(is_array($accountingArr)) {

        //auditing that ewallet has been charged
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_ETRANZACT;
        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_TRANSACTION,
          EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_ETRANZACT,
          EpAuditEvent::getFomattedMessage($msg, array('account_number'=>$ewallet_number)));

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

        $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));

        $collection_account_id = $accountingObj->getCollectionAccount($payment_mode_option_id, NULL, NULL, 'Recharge');
        $managerObj = new payForMeManager();
        $managerObj->updateEwalletAccount($collection_account_id, $txnId, $total_amount_paid,'credit', $service_charge);
      }
      $con->commit();
      $this->getUser()->setFlash('notice', 'Recharge Successful', true) ;
      return $event->getreturnValue();
    }
    catch (Exception $e) {
      $con->rollback();
      throw $e;
    }
  }

  protected function updateGatewayOrder($response)
  { 
    $updateParamArr = array();
    $updateParamArr['status'] = $response['status'];
    if(key_exists('amount', $response))
    $updateParamArr['amount'] = $response['AMOUNT'] * 100;  //convert it into KOBO

    $updateParamArr['code'] = $response['SUCCESS'];
    $updateParamArr['desc'] = $response['desc'];
    $updateParamArr['order_id'] = $response['TRANSACTION_ID'];
    if(key_exists('date', $response))
    $updateParamArr['date'] = $this->convertDate($response['date']);
    else
    $updateParamArr['date'] = date('Y-m-d H:i:s');
    if(key_exists('CARD4', $response) && $response['CARD4']!="")
    $updateParamArr['pan'] = $response['CARD4'];

    $updated = Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
    return $response['TRANSACTION_ID'];
  }
   protected function convertDate($date)
   {
    
    $date4Db = date('Y-m-d H:i:s',strtotime($date));

    return $date4Db;
   }
  function getLock($order_id)
  {
    return $getLock = Doctrine::getTable('GatewayOrder')->getLock($order_id);

  }

  public function executeQueryHistory(sfWebRequest $request)
  {      
      $transactionId = $request->getParameter('orderId');
      $orderArray = $this->getByOrderId($transactionId);
   
    
      $etReqObj = Doctrine::getTable('EpEtranzactrequest')->findByTransactionId($transactionId);
      $terminalId = $etReqObj->getFirst()->getTerminalId();
      
     
      if ($orderArray['status']!= "success") {
          $getData = $this->getStatusMsg($transactionId, $terminalId);
          if($getData['SUCCESS'] == 0) {

              return $this->successResponse($getData, $orderArray);

          }else{

              return $this->failureResponse($getData, $orderArray, $getData['desc']);
          }
      } else  {
          $gatewayObj = Doctrine::getTable('GatewayOrder')->findByOrderId($transactionId);
          if (strtolower($orderArray['type']) == 'payment') {
          
              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'etranzact_configuration',
                  'action' => 'payOnline','returnVal' =>base64_encode($gatewayObj->getFirst()->getAppId())))."'</script>");
          }else if(strtolower($orderArray['type']) == 'recharge') {

            return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
                    'action' => 'rechargeReceipt','validationNo'=> base64_encode($gatewayObj->getFirst()->getValidationNumber())))."'</script>");
          }
      }
     
  }

//  public function executeEtranzactStatus(sfWebRequest $request){
//      $postData =  $_POST;
//      $this->logResponse($postData);
//      $orderArray = $this->getByOrderId($postData['TRANSACTION_ID']);
//      $getData = Array();
//      $paymentStatusMsg = GatewayStatusMessages::getEtranzactStatusMessage($postData['SUCCESS']);
//
//
//      $getData['desc'] = $paymentStatusMsg;
//      $getData['SUCCESS'] = $postData['SUCCESS'];
//      $getData['TRANSACTION_ID'] = $postData['TRANSACTION_ID'];
//      $getData['AMOUNT'] = $postData['TRANS_AMOUNT'];
//      if($postData['SUCCESS'] == 0) {
//
//        return $this->successResponse($getData, $orderArray);
//
//      }else{
//
//          return $this->failureResponse($getData, $orderArray, $paymentStatusMsg);
//      }
//
//    }

 /*
  * function addJobForEtranzactRechageMail()
  * @param $systemFinalCheckSum :system final checksum
  * @param $responseFinalCheckSum :response final checksum 
  * @param $transactionNo :Transaction Id
  * Purpose : create error log during payment
  */
  private function createErrorLog($systemFinalCheckSum, $responseFinalCheckSum, $transactionId)
  {
        $getData = 'Transaction Id : '.$transactionId.'; Response Final CheckSum : '.strtolower($responseFinalCheckSum).'; Backend Final CheckSum : '.strtolower($systemFinalCheckSum);
        $pay4meLog = new pay4meLog();
        $nameFormate = 'error';
        $pay4meLog->createLogData($getData, $nameFormate, 'etranzactErrorLog');
  }

    /*
     * Process the eTranzact record for eTranzact pending          
     */
    public function executeProcessEtranzact(sfWebRequest $request) {
      try {
            $this->setLayout(null);
            ini_set('memory_limit','512M');

            $payMode = $this->getPayModeOptionId();


            $from_trans_date = date('Y-m-d H:i:s',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 00:00:00"));
            $status = 'pending';
            // $from_trans_date if want to process all record then set $from_trans_date = '';
            //       $from_trans_date = '';
            $allGatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByOrderIdAndStatus($payMode,$status,$from_trans_date);

            foreach($allGatewayOrders as $gatewayOrder)
            {
                $orderId =  $gatewayOrder->getOrderId();
                $this->verfiyAndUpdateByOrderId($orderId);
            }

            return $this->renderText("Etranzact processing done");
        }
        catch(Exception $e ) {
            $this->logMessage("Logging the exception from Etranzact Updation Processing..".$e->getMessage().var_dump(number_format(memory_get_peak_usage())));
        }

    }

    protected function getPayModeOptionId() {
        $payModeName  = sfConfig::get('app_payment_mode_option_etranzact');
        $payModeOptions = Doctrine::getTable('PaymentModeOption')->findByName($payModeName);
        $payMode = $payModeOptions->getfirst()->getId();
        return $payMode;
    }

    public function verfiyAndUpdateByOrderId($orderId) {        
        $terminalId = sfConfig::get('app_et_terminal_id');
        $payMode = $this->getPayModeOptionId();
        $status = 'pending';

        $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode,$status);
        // $webpay = new webpay();
        $response = $this->getStatusMsg($orderId, $terminalId);       
        if ($response['SUCCESS'] == 0)
        {
            $response['status']='success';
            $orderArray = $this->getByOrderId($orderId);

            $txnId = $orderArray['app_id'];
            $type = $orderArray['type'];
            $payment_mode_option_id = $gatewayOrder->getPaymentModeOptionId();

            if(strtolower($type) == 'payment') {
                $arrayVal = $this->processToPayment($response,$txnId);
            }
            if(strtolower($type) == 'recharge') {
                $validationNo = $this->processToRecharge($response,$orderArray,$txnId, $payment_mode_option_id);
            }
        } else {


            $gatewayOrder->setStatus('failure');
            $gatewayOrder->setResponseCode($response['SUCCESS']);
            $gatewayOrder->setResponseTxt($response['desc']);
            $transDate = date('Y-m-d H:i:s');
            $gatewayOrder->setTransactionDate($transDate);
            $gatewayOrder->save();
        }
        return true;

 }

  protected function processToPayment($response,$txnId) {
      $orderId = $this->updateGatewayOrder($response);
      $paymentObj =  new Payment($txnId);
      $arrayVal = $paymentObj->proceedToPay();

      if ($arrayVal['comments'] != 'error') {
          $gatewayOrderObj = new gatewayOrderManager();
          $pfmTransactionDetails = $gatewayOrderObj->getValidationNumber($arrayVal['var_value']);
          $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
          $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo,$orderId);
         }
      return $arrayVal;

  }

 protected function getStatusMsg($transId, $terminalId)
  {
      $url = sfConfig::get('app_et_query_url');
      $webPay = new webpay();
      $responseArray = $webPay->getStatusMsg($transId, $terminalId, $url);
      
    
      // loged response from query history
      //$logMessage = "TRANSACTION_ID=".$responseArray['TRANSACTION_ID']."TERMINAL_ID=".$responseArray['TERMINAL_ID']."TRANS_DATE=".$responseArray['TRANS_DATE']."TRANS_AMOUNT=".$responseArray['TRANS_AMOUNT']."CARD4=".$responseArray['CARD4']."SUCCESS=".$responseArray['SUCCESS'];
      //$this->logResponse($logMessage);

      if ($responseArray) {
          $responseCode = trim($responseArray['SUCCESS']);
          $paymentStatusMsg = GatewayStatusMessages::getEtranzactStatusMessage($responseCode);

         $getData['desc'] = $paymentStatusMsg;
         $getData['SUCCESS'] = $responseArray['SUCCESS'];
         $getData['TRANSACTION_ID'] = $responseArray['TRANSACTION_ID'];
         $getData['CARD4'] = $responseArray['CARD4'];

         if($responseCode == 0 && isset($responseArray['SUCCESS'])) {
             $getData['AMOUNT'] = $responseArray['TRANS_AMOUNT'];

             $getData['date'] = $responseArray['TRANS_DATE'];
         } else {
             $etReqObj = Doctrine::getTable('EpEtranzactrequest')->findByTransactionId($transId);

             if (count($etReqObj))
             $getData['AMOUNT'] = $etReqObj->getFirst()->getAmount();

         }
      } else {         
            $getData['desc'] = GatewayStatusMessages::getEtranzactStatusMessage(-1);
            $getData['SUCCESS'] = -1;
            $getData['TRANSACTION_ID'] = $transId;
           
      }
     
      return $getData;
  }

  
 }



