<?php

/**
 * bankintigration actions.
 *
 * @package    mysfp
 * @subpackage bankintigration
 * @author     ryadav
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bankintegrationActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
 


  public function executeMessagePosterToQueue(sfWebRequest $request) {
        $this->setLayout(null);
        try {
           
            $messageRequestId = $request->getParameter('messageRequestId');
            $processId = $this->UpdateQueueFlag($messageRequestId);
            if ($processId)
                return $this->renderText("Message posted to ActiveMQ");
            else
                throw New Exception("Message not inserted in Queue");
        } catch (Exception $e) {
            EpjobsContext::getInstance()->setTaskStatusFailed();
            $this->logMessage("Logging the exception.".$e->getMessage());
            return $this->renderText($e->getMessage());
        }
    }
   /*
    * function for post message on queue
    *e
    */
    public function UpdateQueueFlag($messageRequestId){

        
        //$transactionNumber= $objQueue->getTransactionNumber();
        $processId = null;
        $bankInteObj = new bankIntegration();
        //we are passing this flag for not update count when activemq post through epjob
        //$flagReAttemptCount=TRUE;
        $flagReAttemptCount=FALSE; //vikash [WP055] Bug:36077 (08-11-2012)
        try {
            $processId = $bankInteObj->QueueProcessing($messageRequestId,$flagReAttemptCount);
        }
        catch (Exception $e){
            throw $e;
        }
        return $processId;


    }

/* WP060
    * function for Connection monitoring
    *e
    */
     public function executeConnectionMonitor(sfWebRequest $request){
        $this->setLayout(null);
        $queueDown = $request->hasParameter('queuedown')?$request->getParameter('queuedown'):'';
        $topicDown = $request->hasParameter('topicdown')?$request->getParameter('topicdown'):'';
        if($queueDown){
            $queueDown = rtrim($queueDown, "|");
        }
        if($topicDown){
            $topicDown = rtrim($topicDown, "|");
        }
        $subject = "Connection Monitor" ;
        $partialName = 'connection_monitor_email' ;
        $taskId = EpjobsContext::getInstance()->addJob('SendConnectionMonitorMail',$this->moduleName."/sendEmail", array('queueDown'=>$queueDown,'topicDown'=>$topicDown,'subject'=>$subject,'partialName'=>$partialName));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');

        print_r($taskId);
        die;
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $queueDown = $request->getParameter('queueDown');
        $topicDown = $request->getParameter('topicDown');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $sendMailObj = new Mailing() ;
        $mailInfo = $sendMailObj->sendConnectionMonitorMail($queueDown,$topicDown,$subject,$partialName);
        return $this->renderText($mailInfo);
    }

}
