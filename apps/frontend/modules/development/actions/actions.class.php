<?php

/**
 * development actions.
 *
 * @package    pay4me
 * @subpackage development
 * @author     Ashutosh srivastava
 * @version    SVN: $Id: actions.class.php 12474 2010-08-30 10:41:27Z fabien $
 */
class developmentActions extends sfActions {

    public function executeAllusers(sfWebRequest $request) {
        $this->userName = $request->getParameter('user_search');
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllUsersByName($this->userName,'');
//        echo "<pre>"; print_r($this->user_list->fetchArray());
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardUser',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();

    }

    public function executePin(sfWebRequest $request) {

        $pinObj=new pin();
        $pinObj->getEncryptedPin('');die;
    }

    public function executeResetPassword(sfWebRequest $request) {
        $userId = $request->getParameter('userId');
        $pass_word = $request->getParameter('newPwd');
        
        $user_list = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        $redirecturl = "allusers";

        $user_password_update = Doctrine::getTable('sfGuardUser')->updateUserPasswordById($userId,$pass_word,"NoLastogin");
        $user_update_failedAtttempt = Doctrine::getTable('UserDetail')->updateFailedAttemptsById($userId,0);
        if($user_password_update=='1') {
            $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($userId);
            $bankName =  $sf_guard_user->getBankUser()->getFirst()->getBank()->getAcronym();


            $EpDBSessionDetailObj =  new EpDBSessionDetail();
            $EpDBSessionDetailObj->deleteSessionData(array(array('sigin_user_name' => $user_list[0]['username'])));

            $this->getUser()->setFlash('notice','Password for  '.$user_list[0]["username"].' has been reset<br> The New Password is - '.$pass_word , true);
            if($request->getParameter('redirect_to')){
                $this->redirect($request->getParameter('redirect_to').'/'.$redirecturl);
            }
            else{

                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }else {
            $this->getUser()->setFlash('error','your are not authorized user !' , true);
            if($request->getParameter('redirect_to')){

                $this->redirect($request->getParameter('redirect_to').'/'.$redirecturl);
            }
            else{
                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }
    }
  public function executeResetAnswer(sfWebRequest $request) {
      $userId = $request->getParameter('userId');
      $answer = $request->getParameter('newAns');

      $user_list = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
      $redirecturl = "allusers";

        $user_password_update = Doctrine::getTable('securityQuestionsAnswers')->updateAnswerById($userId,$answer);

        if($user_password_update=='1') {
            $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($userId);
            $bankName =  $sf_guard_user->getBankUser()->getFirst()->getBank()->getAcronym();


            $EpDBSessionDetailObj =  new EpDBSessionDetail();
            $EpDBSessionDetailObj->deleteSessionData(array(array('sigin_user_name' => $user_list[0]['username'])));

            $this->getUser()->setFlash('notice','Password for  '.$user_list[0]["username"].' has been reset<br> The New Password is - '.$pass_word , true);
            if($request->getParameter('redirect_to')){
                $this->redirect($request->getParameter('redirect_to').'/'.$redirecturl);
            }
            else{

                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }else {
            $this->getUser()->setFlash('error','your are not authorized user !' , true);
            if($request->getParameter('redirect_to')){

                $this->redirect($request->getParameter('redirect_to').'/'.$redirecturl);
            }
            else{
                $this->redirect($this->moduleName.'/'.$redirecturl);
            }
        }

  }
  public function executeAddRepeatableJob(sfWebRequest $request) {
       $this->setLayout(null);

       $nextProcess_schedule = mktime(0, 0+settings::getNextDayProcessBatchTimeInMin(), 0, date("m"),  date("d")+1,  date("Y"));
       $date_nextProcess = date('Y-m-d H:i:s',$nextProcess_schedule);
       EpjobsContext::getInstance()->addJob('UpdatePayments', 'paymentSystem/updateAccounts',NULL,'','',$date_nextProcess);

       $nextVbv_schedule = mktime(0, 0 + settings::getNextDayProcessVbvTimeInMin(), 0, date("m"),  date("d")+1,  date("Y"));
       //$date_nextVbv_schedule = date('Y-m-d H:i:s',$nextVbv_schedule);
        $hour = date('H',$nextVbv_schedule);
        $min = date('i',$nextVbv_schedule);

//       $nextTime =  explode(' ',$date_nextVbv_schedule);
//       $nextTimeArr =  explode(':',$nextTime[1]);

       EpjobsContext::getInstance()->addJobForEveryDay('UpdateVbvPending', 'vbv_configuration/processVbv','2020-3-12 12:12:12',null,null,null,null,$min ,$hour);
       EpjobsContext::getInstance()->addJobForEveryDay('UpdateEtranzactPending', 'etranzact_configuration/processEtranzact','2020-3-12 12:12:12',null,null,null,null, 30, 01);
       EpjobsContext::getInstance()->addJobForEveryDay('UpdateInterswitchPending', 'interswitch_configuration/processInterswitch','2020-3-12 12:12:12',null,null,null,null, 30, 01);

       ///////Monitoring JOBS ////////////////////////
         EpjobsContext::getInstance()->addJobForEveryDay('MonitorAccountAssociation', 'monitoringTool/monitorAccountAssociation','2020-3-12 12:12:12',null,null,null,null, 30, 02);
         EpjobsContext::getInstance()->addJobForEveryDay('MonitorSplitingOfAmount', 'monitoringTool/monitorSplitingOfAmount','2020-3-12 12:12:12',null,null,null,null, 30, 02);
         EpjobsContext::getInstance()->addJobForEveryDay('MonitorLog', 'monitoringTool/monitorLog','2020-3-12 12:12:12',null,null,null,null, 30, 02);
         EpjobsContext::getInstance()->addJobForEveryDay('MonitorMassiveTransaction', 'monitoringTool/monitorMassiveTransaction','2020-3-12 12:12:12',null,null,null,null, 30, 02);
         EpjobsContext::getInstance()->addRepetableJob('MonitorMailAndNortification', 'monitoringTool/monitorMailAndNotification','2020-3-12 12:12:12', null, null, null, null, 30, '*/2');
         EpjobsContext::getInstance()->addJobForEveryDay('MonitorAccountBalaceOfEwallet', 'monitoringTool/monitorAccountBalanceOfEwallet','2020-3-12 12:12:12',null,null,null,null, 30, 02);
      ////////////////////////////////////////////////
         
      
       
       return $this->renderText("Jobs added Successfully");
    }
}
