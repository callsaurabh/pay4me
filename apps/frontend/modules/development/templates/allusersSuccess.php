<div id="theMid">
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_helper('Form');
 use_helper('Pagination');  ?>

    
<?php echo ePortal_legend('Search User'); ?>

<div class="wrapForm2">
<?php echo form_tag($sf_context->getModuleName().'/allusers','name=search id=search');?>
  <dl id='bank_row'>
    <div class="dsTitle4Fields"><label for="bank">Username</label></div>
    <div class="dsInfo4Fields"><input type="text" name="user_search" id="user_search" value="<?php echo $userName;?>" class=FieldInput>
      <input type="submit" class='formSubmit' value="Search" onclick="return userSearch();"/>&nbsp;&nbsp;<br /><a href="<?php echo url_for($sf_context->getModuleName().'/allusers') ?>"  style="cursor: pointer; color: blue; text-decoration: underline;">Back to User listing</a>
      <div id="err_bank_branch" class="cRed"></div>
    </div>
  </dl>
 </form>
<div class="clear"></div>
</div>
<br />
<?php echo ePortal_listinghead('User List'); ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
          <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
      </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Username</th>
      <th>User Role</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
      <?php
      if(($pager->getNbResults())>0) {
          $limit = sfConfig::get('app_records_per_page');
          $page = $sf_context->getRequest()->getParameter('page',0);
          $i = max(($page-1),0)*$limit ;
          foreach ($pager->getResults() as $result):
              $i++;
      ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <td align="left"><?php echo $result->getUserName() ?></td>
        <td align="left"><?php //echo $result->getGroupDescription() ?></td>
        <td align="center"><?php 
//            if($result->getFailedAttempt() == 5)
                { echo link_to(' ', $sf_context->getModuleName().'/unblock?id='.$result->getId(), array('method' => 'get', 'class' => 'unblockInfo', 'title' => 'Unblock User')); }
//            else
                echo "&nbsp;&nbsp;&nbsp;&nbsp;";
          echo "&nbsp";//          $sf_context->getModuleName().'/resetPassword?userId='.$result->getId()
          ?>
          <a onclick =changeProfile('<?php echo $result->getId()?>','resetPwd') class ='userEditInfo' title ='Reset Password' style="cursor:pointer"></a>
          &nbsp;
          <a onclick =changeProfile('<?php echo $result->getId()?>','setAns') class ='editInfo' title ='Reset Answer' style="cursor:pointer"></a>
        </td>
      </tr>
      <?php endforeach; ?>
      <tr>
        <td colspan="5">
          <div class="paging pagingFoot">
            <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid') ?>
          </div>
        </td>
      </tr>
      <?php
        }else{
      ?>
      <tr>
        <td  align='center' class='error' colspan="5">No Bank Admin Found</td>
      </tr>
      <?php } ?>
  </tbody>
  
</table>

</div>
</div>
</div>

<script>
    function changeProfile(userid,val){
       if("resetPwd" == val)
            {
                 jPrompt('Please enter password:', '', 'Password', function(newPwd) {
                        if( !newPwd ){
                            jAlert('Password is required to reset the password!');
                        return;
                        }
                        else {  window.location = "<?php echo url_for($sf_context->getModuleName().'/resetPassword?userId='); ?>"+userid+"/newPwd/"+newPwd;
                            }
                    }
                );
            }

       if("setAns" == val)
            {
                 jPrompt('Please enter answer:', '', 'Answer', function(newAns) {
                        if( !newAns ){
                            jAlert('Answer is required to reset the answer!');
                        return;
                        }
                        else {  window.location = "<?php echo url_for($sf_context->getModuleName().'/resetAnswer?userId='); ?>"+userid+"/newAns/"+newAns;
                            }
                    }
                );
            }

    }
</script>