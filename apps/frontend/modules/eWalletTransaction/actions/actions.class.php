<?php

/**
 * broadcast actions.
 *
 * @package    mysfp
 * @subpackage broadcast
 * @author     Pritam Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class eWalletTransactionActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->checkIsSuperAdmin();

        //    $this->page = 1;
        //    if($request->hasParameter('page')) {
        //      $this->page = $request->getParameter('page');
        //    }
        //    $this->pager = new sfDoctrinePager('BroadcastMessage',sfConfig::get('app_records_per_page'));
        //    $this->pager->setQuery($this->broadcast_list);
        //    $this->pager->setPage($this->page);
        //    $this->pager->init();
    }
    public function executeEwalletBillRequest(sfWebRequest $request)
    {
        $this->form = new EwalletBillRequestForm();
        $this->checkIsSuperAdmin();
        $walletObj = new EpAccountingManager;
        $userConfig = $this->getUserConfig();

        //get getWalletAccNo
        $this->wallet_number = $this->getWalletAccNo($userConfig);
        $walletDetails = $walletObj->getWalletDetails($this->wallet_number);
        $this->accountName = $walletDetails->getFirst()->getAccountName();

        $account_id = $this->getWalletAccId($userConfig);

        $acctObj = $walletObj->getAccount($account_id);
        $this->walletBalance = $acctObj->getClearBalance();

        $this->form->setDefault('account_name',  $this->accountName);
        $this->form->setDefault('account_no',  $this->wallet_number);
        $this->form->setDefault('bal_amount',  $this->walletBalance);
        //$this->form = new EwalletMoneyTransferForm();
    }

    public function executeEwalletBillRequestProcess(sfWebRequest $request){

        $this->checkIsSuperAdmin();
        $data = $request->getPostParameters();
        $ewalletTransMangerObj = new walletTransactionManager();
        #sfLoader::loadHelpers('ePortal');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $amt = convertToKobo($data['amount']);
        $returnVal = $ewalletTransMangerObj->doBillAdd($data['account_no'],$data['fromAcc'],$amt,$data['description']);
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        if(is_numeric($returnVal)){
            $this->getUser()->setFlash('notice', __('Your request has been send successfully (account number').' - '.$data['fromAcc'].')');
            $this->redirect('eWalletTransaction/ewalletBillRequest');
        }else{
            $this->getUser()->setFlash('error', __($returnVal));
            $this->forward($this->getModuleName(), 'ewalletBillRequest');
        }
    }

    public function executeEwalletMoneyTransfer(sfWebRequest $request)
    {
        $this->form = new EwalletMonyTransferForm();
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $ewalletObj = new ewallet();
        $this->kycStatus  = $ewalletObj->getKycstatus($userId);    
      

        $this->checkIsSuperAdmin();
        $walletObj = new EpAccountingManager;
        $userConfig = $this->getUserConfig();

        //get getWalletAccNo
        $this->wallet_number = $this->getWalletAccNo($userConfig);
        $walletDetails = $walletObj->getWalletDetails($this->wallet_number);
        $this->accountName = $walletDetails->getFirst()->getAccountName();

     //   $account_id = $this->getWalletAccId($userConfig);

      //  $acctObj = $walletObj->getAccount($account_id);
      //  $this->walletBalance = $acctObj->getClearBalance();

     //   $this->form->setDefault('account_name',  $this->accountName);
      //  $this->form->setDefault('from_account',  $this->wallet_number);
      //  $this->form->setDefault('bal_amount',  $this->walletBalance);
        if($request->isMethod('post')){
           
            $this->form->bind($request->getParameter('monyTransfer')); 
            if($this->form->isValid())
            {
                $this->forward($this->getModuleName(),'moneyTransfer');
            }else{               
               echo $this->form->renderGlobalErrors();
            }
        }

        //    $this->form = new EwalletMoneyTransferForm();
    }

    //this function returns the user connection
    private function getUserConfig() {
        $gUser = $this->getUser()->getGuardUser();
        $bUser = $gUser->getUserDetail();

        return $bUser;
    }

    //function to get the wallet id
    private function getWalletAccId($connec) {
        $epId = $connec->getFirst()->getMasterAccountId();
        return $epId;
    }

    //function to get the wallet account no
    private function getWalletAccNo($connec) {
        $EpId = $connec->getFirst()->getMasterAccountId();
        $getDetails = Doctrine::getTable('EpMasterAccount')->find($EpId);
        $wallet_number = $getDetails->getAccountNumber();

        return $wallet_number;
    }

    public function executeMoneyTransfer(sfWebRequest $request) {      
        $data = $request->getParameter('monyTransfer');
        //$pinObj=new pin();
        // $ewalletDetail=$pinObj->getEwalletDetail();

        if(isset($data['pin'])) {
            $pinObj=new pin();
            $encrypted_pin = $data['pin'];
            $pin_validate = $pinObj->validatePin($encrypted_pin);
            if($pin_validate!='1') {
                $this->getUser()->setFlash('error', $pin_validate);
                 $this->redirect('eWalletTransaction/ewalletMoneyTransfer');
            }
        }

        $min_transfer_amount = sfConfig::get('app_min_ewallet_transfer');
        $ewalletTransMangerObj = new walletTransactionManager();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $amount = convertToKobo($data['trans_amount']);
        $from_account_id = $data['from_account'];
        $accountDetailObj = new EpAccountingManager();
        $result = $accountDetailObj->getAccount($from_account_id);
        $source_account_number = $result->getAccountNumber();
        $returnVal = $ewalletTransMangerObj->doDirectRequestTransfer($data['to_account'],$source_account_number,$amount,$data['description']);
        
        if(ctype_digit($returnVal)) {
            $this->getUser()->setFlash('notice', __('Amount has been transferred successfully to account number').' - '.$data['to_account'].'<br>'.__('The transaction code is').' -'.$returnVal);
            
            $this->addJobForewallettransferMail($data['to_account'],$source_account_number,$amount,$returnVal,$data['description']);
            $this->redirect('eWalletTransaction/ewalletMoneyTransfer');
        }else {
            $this->getUser()->setFlash('error', __($returnVal));
             $this->redirect('eWalletTransaction/ewalletMoneyTransfer');
        }
    }


    /**
  * function addJobForewallettransferMail
  * @param $txnId :transaction number
  * Purpose : add job to send ewallet payment
  */

    public function addJobForewallettransferMail($to_account,$source_account_number,$amount,$returnVal,$data) {

        $notificationUrl = 'email/sendewallettransferMail';
        $taskId = EpjobsContext::getInstance()->addJob('EwalletTransferMailNotification',$notificationUrl, array('returnVal'=>$returnVal));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

    }

    public function executeAccountSearch() {
      
    }

    public function executeEwalletMoneyTransferListing(sfWebRequest $request)
    {
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $kycStatus = $user_detail->getFirst()->getKycStatus();
        $this->kycStatus = $kycStatus;

        $this->checkIsSuperAdmin();
        $saveObj = new walletTransactionManager();
        $fromAccId =  $request->getParameter('from_account');
        $this->fromAccId = $fromAccId;
        $userManager = new UserAccountManager();
        $this->currency = $userManager->getCurrencyForAccount($fromAccId);
        $this->queryDirectTransfer = $saveObj->listDirectTransfer($fromAccId);
        $this->page = 1;
        if($request->hasParameter('page'))
        {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('EwalletTransaction',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->queryDirectTransfer);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeEwalletBillRequestListing(sfWebRequest $request)
    {
        $this->checkIsSuperAdmin();
        $listingType = $request->getParameter('listType');

        $saveObj = new walletTransactionManager();
        $eWalletAccountId = $this->getUser()->getUserAccountId();

        switch ($listingType) {
            case "rejectFromAcc":
                $this->queryDirectTransfer = $saveObj->listRejectTransferFromAcc($eWalletAccountId);
                $this->listingTitle = "eWallet Bill Reject (From Account) Listing";
                $this->listingType = $request->getParameter('listType');
                $this->accType = "from";
                break;
            case "approveFromAcc":
                $this->queryDirectTransfer = $saveObj->listApproveTransferFromAcc($eWalletAccountId);
                $this->listingTitle = "eWallet Bill Approved (From Account) Listing";
                $this->listingType = $request->getParameter('listType');
                $this->accType = "from";
                break;
            case "requestFromAcc":
                $this->queryDirectTransfer = $saveObj->listRequestTransferFromAcc($eWalletAccountId);
                $this->listingTitle = "eWallet Bill Request (From Account) Listing";
                $this->listingType = $request->getParameter('listType');
                $this->accType = "from";
                break;
            case "rejectToAcc":
                $this->queryDirectTransfer = $saveObj->listRejectTransferToAcc($eWalletAccountId);
                $this->listingTitle = "eWallet Bill Reject (To Account) Listing";
                $this->listingType = $request->getParameter('listType');
                $this->accType = "to";
                break;
            case "approveToAcc":
                $this->queryDirectTransfer = $saveObj->listApproveTransferToAcc($eWalletAccountId);
                $this->listingTitle = "eWallet Bill Approved (To Account) Listing";
                $this->listingType = $request->getParameter('listType');
                $this->accType = "to";
                break;
            case "requestToAcc":
                $this->queryDirectTransfer = $saveObj->listRequestTransferToAcc($eWalletAccountId);
                $this->listingTitle = "eWallet Bill Request (To Account) Listing";
                $this->listingType = $request->getParameter('listType');
                $this->accType = "to";
                break;
        }

        $this->page = 1;
        if($request->hasParameter('page'))
        {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('EwalletTransaction',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->queryDirectTransfer);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeEwalletBillRequestApproval(sfWebRequest $request)
    {
        $this->checkIsSuperAdmin();
        $value ="";
        $accountDetailObj = new walletTransactionManager();
        $approvalStatus = $request->getParameter('status');
        $walletTransactionId = $request->getParameter('id');
        $listingType = $request->getParameter('listType');

        $value = $accountDetailObj->doBillRequestAction($walletTransactionId,$approvalStatus);
        if(is_numeric($value) && $approvalStatus=='rejected')
        $value = "Request has been rejected!";
        else if(is_numeric($value) && $approvalStatus=='approved')
        $value = "Request has been approved!";
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $this->getUser()->setFlash('notice', __($value));
        $this->redirect('eWalletTransaction/ewalletBillRequestListing?listType='.$listingType);
    }

    public function checkIsSuperAdmin(){
        if($this->getUser()  && $this->getUser()->isAuthenticated()) {
            $group_name = $this->getUser()->getGroupNames();
            if(in_array(sfConfig::get('app_pfm_role_admin'),$group_name)){
                $this->redirect('@adminhome');
            }
        }
    }
    
    
    public function executeAccountBalance(sfWebRequest $request) {
      $this->setLayout(null);
      if($request->hasParameter('account_id')) {
        $account_id = $request->getParameter('account_id');
        $acctObj = new EpAccountingManager();
        $detailObj = $acctObj->getAccount($account_id);
        $currency = $this->getCurrency($account_id);
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $balance = format_amount($detailObj->getBalance(), $currency, 1);
        return $this->renderText($balance);
      }
      return $this->renderText("Param not available");
    }

    public function executeAccountCurrency(sfWebRequest $request) {
      $this->setLayout(null);
      if($request->hasParameter('account_id')) {
        $account_id = $request->getParameter('account_id');
        $currency = $this->getCurrency($account_id);
        $currency_image = $this->getCurrencyImage($currency);
        return $this->renderText($currency_image);
      }
      return $this->renderText("Param not available");
    }

    public function getCurrency($account_id) {
      $userAccountObj = new UserAccountManager();
      return $currency = $userAccountObj->getCurrencyForAccount($account_id);
    }

    public function getCurrencyImage($currency_id) {
      $currencyObj = Doctrine::getTable('CurrencyCode')->find($currency_id);
      $currency_name = strtolower($currencyObj->getCurrency());
      $currency_alt =  $currencyObj->getCurrencyCode();
             sfContext::getInstance()->getConfiguration()->loadHelpers(array('Tag'));
       sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));

      return $img = html_entity_decode(image_tag('../img/'.$currency_name.'.gif',array('alt'=>''.$currency_alt.'')));
    }
}
