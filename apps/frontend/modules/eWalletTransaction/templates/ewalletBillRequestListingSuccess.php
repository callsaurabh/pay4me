<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


    <?php echo ePortal_listinghead(__($listingTitle)); ?>
 
<div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr class="alternateBgColour">
            <th width="100%" >
              <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
              <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
            </th>
          </tr>
        </table>
      <br class="pixbr" />
</div>

<div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
          <tr class="alternateBgColour">
            <th>S. No</th>
            <th><?php if($accType == "from"){ echo __("From Account"); }else {echo __("To Account");} ?></th>
            <th><?php echo __('Amount')?></th>
            <th>Description</th>
            <th>Date</th>
            <th><?php echo __('Approve/Reject')?></th>
          </tr>
  </thead>
  <tbody>
    <div name="forChk" id="forChk">
      <?php
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;

        ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i; ?></td>
        <td align="center"><?php if($accType == "from"){echo $result->getEpMasterAccountTo()->getAccountNumber();}else{echo $result->getEpMasterAccountFrom()->getAccountNumber();} ?></td>
        <td align="right"><?php echo format_amount($result->getAmount(),1,1) ?></td>
        <td align="center"><?php echo $result->getDescription(); ?></td>
        <td align="center"><?php echo date('Y-m-d',strtotime($result->getRequestedOn())); ?></td>
        <td align="center">
        <?php if($accType == "from"){ ?>
        <?php if($result->getStatus() == "pending"){ ?>
        <?php echo link_to(' ', $sf_context->getModuleName().'/ewalletBillRequestApproval?id='.$result->getId().'&status=approved&listType='.$listingType, array( 'confirm' => __('Are you sure, you want to approve?'), 'class' => 'approveInfo', 'title' => __('Approve'))) ?>
        <?php echo link_to(' ', $sf_context->getModuleName().'/ewalletBillRequestApproval?id='.$result->getId().'&status=rejected&listType='.$listingType, array('confirm' => __('Are you sure, you want to reject?'), 'class' => 'delInfo', 'title' => __('Reject')))         ?>
        <?php }else{ echo strtoupper($result->getStatus()); } 
        }else{
            echo strtoupper($result->getStatus());
        } ?>
        </td>
      </tr>
      <?php endforeach;
      ?>

      <tr><td colspan="6">
          <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMiddle');?></div>
        </td>
      </tr>
      <?php }else{ ?>
      <tr><td align="center" class="error" colspan="6"><b><?php echo __('No Bill Found')?></b</td></tr>
      <?php } ?>
    </div>
  </tbody>
  <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>
</div>