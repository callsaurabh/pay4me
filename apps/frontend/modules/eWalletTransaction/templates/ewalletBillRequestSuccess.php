<?php echo ePortal_pagehead(__("eWallet Bill Request"),array('class'=>'_form')); ?>
<?php //use_helper('Form'); ?>

<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/ewalletBillRequestProcess',array('name'=>'pfm_ewallet_bill_request_form','id'=>'pfm_ewallet_bill_request_form','onSubmit'=>'return validateTransferForm()')) ?>

    <?php echo ePortal_legend(__('Receiver Details')); ?>
    <?php echo formRowFormatRaw(__('Account Name'),$accountName); ?>
    <?php echo formRowFormatRaw(__('Account No.'),$wallet_number); ?>
    <?php echo formRowFormatRaw(__('Account Balance'),format_amount($walletBalance,1,1)); ?>
    <?php echo $form['account_name']->render();?>
    <?php echo $form['account_no']->render();?>
    <?php echo $form['bal_amount']->render();?>
</div>
<div class="wrapForm2">
    <?php echo ePortal_legend(__('Sender Details')); ?>
    <?php //echo $form ?>
    <?php echo formRowComplete($form['fromAcc']->renderLabel().'<font color=red><sup>*</sup></font>','&nbsp;&nbsp&nbsp;'.$form['fromAcc']->render(),'','toAcc','err_toAcc','to_acc'); ?>
    <?php echo formRowComplete($form['amount']->renderLabel().'<font color=red><sup>*</sup></font>',image_tag('../img/naira.gif').$form['amount']->render(),'&nbsp;&nbsp;&nbsp;'.__('Eg:').(' 10000'),'amount','err_amount','divamount'); ?>
    <?php echo formRowComplete($form['description']->renderLabel().'<font color=red><sup>*</sup></font>','&nbsp;&nbsp&nbsp;'.$form['description']->render(),'&nbsp;&nbsp;&nbsp;'.__('(upto 40 characters only)'),'description','err_description','divdis'); ?>

    <div class="divBlock">
        <center id="multiFormNav">
            <input type="submit" name="submit" value="<?php echo __('Request')?>" class="formSubmit">        
        </center>
    </div>


    </form>
</div>
<script language="Javascript">

    function validateTransferForm()
    {

        var regexp = /^[0-9]+$/;
        //var alphaRegx = /^[ \t]+|[ \t]+$/;

        $('#err_toAcc').html('')
        $('#err_amount').html('');
        $('#err_description').html('');
        if($('#fromAcc').val() == '')
        {
            $('#err_toAcc').html("<?php echo __("Requesting Account Number required")?>");
            return false;
        }
        if($('#amount').val() == '')
        {
            $('#err_amount').html("<?php echo __("Amount required")?>");
            return false;
        }else{
            if(!(document.getElementById('amount').value).match(regexp)){
                $('#err_amount').html("<?php echo __("Please enter numeric value")?>");
                return false;
            }
        }
        if(TrimString($('#description').val()) == '')
        {
            $('#err_description').html("<?php echo __("Description required")?>");
            return false;
        }

        if($('#description').val().length > 40)
        {
            $('#err_description').html('upto 40 characters only');
            return false;
        }


        var answer = confirm("<?php echo __("Are you sure you want to proceed?")?>")
        if (answer){
            return true;
        }
        else{
            return false;
        }


    }






    function TrimString(sInString) {
        sInString = sInString.replace( /^\s+/g, "" );// strip leading
        return sInString.replace( /\s+$/g, "" );// strip trailing
    }
</script>