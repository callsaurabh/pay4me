<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_helper('Form'); ?>
<?php
if(1!=$kycStatus)
{
    ?>
<div class="descriptionArea">
<?php echo __('Pay4Me eWallet Secure Certificate is required to using this service.')?>
</div>
<?php

}
else
{
?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/ewalletMoneyTransfer',array('name'=>'transfer','id'=>'transfer')) ?>

    <?php   echo ePortal_legend(__('eWallet to eWallet Transfer')); ?>
    <br>
    <?php
            echo ePortal_legend(__('Sender Details'));
     ?>
    <?php echo simpleRow(__('Account Name'), $accountName); ?>
    <?php echo formRowComplete($form['from_account']->renderLabel(),$form['from_account']->render(array('onchange'=>'fetchBalance(this.value)')),'','from_account','err_fromAcc','from_acc',$form['from_account']->renderError()); ?>
    <?php echo simpleRow(__('Account Balance'), '<span id="bal">'.image_tag('loader.gif').'</span>'); ?>
    <?php
    $pinObj=new pin();
   if($pinObj->isActive())
     echo formRowComplete($form['pin']->renderLabel(),'&nbsp;&nbsp;&nbsp;'.$form['pin']->render(),'','to_pin','err_to_pin','to_pin',$form['pin']->renderError(),'error_listing'); ?>
    <?php echo $form['account_name']->render();?>
    <?php echo $form['bal_amount']->render();?>

</div>
<div class="wrapForm2">
    <?php echo ePortal_legend(__('Receiver Details')); ?>
    <?php echo formRowComplete($form['to_account']->renderLabel(),'&nbsp;&nbsp&nbsp;'.$form['to_account']->render(),'','to_acc','err_to_acc','to_acc',$form['to_account']->renderError(),'error_listing'); ?>
    <?php echo formRowComplete($form['trans_amount']->renderLabel(),'<span id="currency_img"></span>'.$form['trans_amount']->render(),__('Eg:').' 100.98','trans_amount','err_trans_amount','divamount',$form['trans_amount']->renderError(),'error_listing'); ?>
    <?php echo formRowComplete($form['description']->renderLabel(),'&nbsp;&nbsp&nbsp;'.$form['description']->render(),'','description','err_description','divdis',$form['description']->renderError(),'error_listing'); ?>
<?php  if ($form->isCSRFProtected()) : ?>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php endif; ?>


    <div class="divBlock">
        <center id="multiFormNav">
        <input type="submit" name="submit" value="<?php echo __('Transfer')?>" id="submit" onClick="hideBut();" class="formSubmit">
        </center>
    </div>


    </form>
</div>
<?php

}
?>
<script language="Javascript">

 function hideBut()
 {
     $("#transfer").submit();
     $('#submit').hide();
     $("#transfer").serialize();

 }
  var account_id = $('#monyTransfer_from_account').val();
  $(document).ready(function(){fetchBalance(account_id)});

    var min_amount = "<?php echo sfConfig::get('app_min_ewallet_transfer')/100;?>";
    var max_amount = "<?php echo sfConfig::get('app_max_ewallet_transfer')/100;?>";
    var DigitsAfterDecimal = 2;
    function CheckUnit(amount)
    {
        var val = amount;

        if(isNaN(amount) || Number(val) <= 0  || ( amount.indexOf(".") > -1 && (val.length - (val.indexOf(".")+1) >   DigitsAfterDecimal)  ))
        {  ///|| String(Number(val)) != String(val)
            return "Please enter valid amount";
        }
        else
        {
            if(Number(val) < Number(min_amount))
                return " Minimum amount for transfer is "+min_amount+" Naira  ";

            if(Number(val) > Number(max_amount))
                return " Maximum amount for transfer is "+max_amount+" Naira  ";

            return "valid";
        }
    }


    function validateTransferForm()
    {
        $('#err_toAcc').html('')
        $('#err_trans_amount').html('');
        $('#err_description').html('');
        $('#err_pin').html('');
        if($('#pin').val() == '')
        {
            $('#err_pin').html('Pin required');
            return false;
        }
        if($('#to_account').val() == '')
        {
            $('#err_toAcc').html('Receiver account number required');
            return false;
        }
        if($('#trans_amount').val() == '')
        {
            $('#err_trans_amount').html('Please enter amount');
            return false;
        }
        else{
            /*if(!(document.getElementById('trans_amount').value).match(regexp)){
$('#err_trans_amount').html('Please enter valid amount');
return false;
}*/
            var validate_amt = CheckUnit(document.getElementById('trans_amount').value);

            if(validate_amt!="valid") {
                $('#err_trans_amount').html(validate_amt);
                return false;
            }
        }
        if(TrimString($('#description').val()) == '')
        {
            $('#err_description').html('Description required');
            return false;
        }
        if($('#description').val().length > 40)
        {
            $('#err_description').html('upto 40 characters only');
            return false;
        }


        var answer = confirm("Are you sure you want to proceed?")
        if (answer){
            return true;
        }
        else{
            return false;
        }


    }
    function TrimString(sInString) {
        sInString = sInString.replace( /^\s+/g, "" );// strip leading
        return sInString.replace( /\s+$/g, "" );// strip trailing
    }

    function fetchBalance(account_id) {
      var url = '<?php echo url_for('eWalletTransaction/accountBalance'); ?>';
      if(account_id!="") {
        $.post(url,{account_id:account_id}, function(data){
                if(data=='logout'){
                    location.reload();
                }
                else {
                    $("#bal").html(data);}
            },getCurrency(account_id)
        );
      }
    }

    function getCurrency(account_id) {
    var url = '<?php echo url_for('eWalletTransaction/accountCurrency'); ?>';
        $.post(url,{account_id:account_id}, function(data){
                if(data=='logout'){
                    location.reload();
                }
                else {
                    $("#currency_img").html(data);}
            });
    }

    $('#monyTransfer_pin').keypad({duration:'fast',randomiseNumeric: true});

</script>