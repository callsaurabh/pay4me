<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
<?php
       if(1!=$kycStatus)
       {
?>
     <div class="descriptionArea">
        <pre>
          <?php echo __('Pay4Me eWallet Secure Certificate is required to using this service.')?>
        </pre>
    </div>
<?php

       }
       else
       {
?>

    <?php echo ePortal_listinghead(__('eWallet Money Transfer Listing')); ?>

<div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr class="alternateBgColour">
            <th>
              <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('')?>results matching your criteria.</span>
              <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
            </th>
          </tr>
        </table>
        <br class="pixbr" />
</div>


<div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
          <tr class="alternateBgColour">
            <th>S. No</th>
            <th><?php echo __('To Account')?></th>
            <th><?php echo __('Amount')?></th>
            <th><?php echo __('Transaction Code')?></th>
            <th>Description</th>
            <th>Date</th>
          </tr>
  </thead>
  <tbody>
    <div name="forChk" id="forChk">
      <?php
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;

        ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i; ?></td>
        <td align="center"><?php echo  $result->getEpMasterAccountTo()->getAccountNumber();?></td>
        <td align="right"><?php echo format_amount($result->getAmount(),$currency,1);?></td>
        <td align="center"><?php echo $result->getEwalletTransactionTrack()->getFirst()->getTransactionCode(); ?></td>
        <td align="center"><?php echo htmlentities($result->getDescription());?></td>
        <td align="center"><?php echo $result->getRequestedOn();?></td>
      </tr>
      <?php endforeach;
      ?>

      <tr><td colspan="6">
          <div class="paging pagingFoot">
            <?php  //echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMiddle');?>
            <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.'from_account='.$fromAccId),'listing');?>

            </div>
        </td>
      </tr>
      <?php }else{ ?>
     <table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Bill Found"; ?></td></tr></table>
      <?php } ?>
    </div>
  </tbody>
 </table>
</div>

<?php

       }
  ?>