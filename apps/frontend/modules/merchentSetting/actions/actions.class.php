<?php

/**
 * bank_branch actions.
 *
 * @package    mysfp
 * @subpackage bank_branch
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class merchentSettingActions extends sfActions {

    public function executeStopMerchentRequest(sfWebRequest $request) {
        $selMerchantChoices = array('' => 'Select Merchant');
        $selServiceTypeChoices = array('' => 'Select Merchant Services');

        $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr();
        $this->merchant_choices = $selMerchantChoices + $serviceTypeArray;
        $this->merchant_service_choices = $selServiceTypeChoices;
        $this->form = new StopMerchantRequestForm(array(), array('merchant' => $this->merchant_choices, 'merchant_service' => $this->merchant_service_choices));
    }

    public function executeSaveMerchentRequest(sfWebRequest $request) {

        $merchentId = $request->getParameter('merchant');
        $merchentServiceId = $request->getParameter('service_type');
        $sDate = $request->getParameter('startDate');
        $eDate = $request->getParameter('endDate');
        $input_values = array('_csrf_token' => $request->getParameter('_csrf_token'), 'merchant' => $merchentId,
            'endDate' => $eDate, 'startDate' => $sDate
            , 'service_type' => $merchentServiceId);
        $selMerchantChoices = array('' => 'Select Merchant');
        $selServiceTypeChoices = array('' => 'Select Merchant Services');

        $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr();
        $selMerchantChoices = $selMerchantChoices + $serviceTypeArray;

        $selServiceTypeChoices = $merchentServiceId ? ($selServiceTypeChoices + $this->construct_options_array(Doctrine::getTable('MerchantService')->getServiceTypes($merchentId)->toArray())) : $selServiceTypeChoices;

        $this->form = new StopMerchantRequestForm(array(), array('merchant' => $selMerchantChoices, 'merchant_service' => $selServiceTypeChoices));

        $this->form->bind($input_values);
        if ($this->form->isValid()) {
            $saveTrans = paymentScheduleConfigServiceFactory::getService(paymentScheduleConfigServiceFactory::$merchantConfig);
            $transId = $saveTrans->saveMerchantServiceConfiguration($merchentId, $merchentServiceId, $sDate, $eDate);
            if ($transId) {
                $value = "Merchant Configuration transaction has been saved successfully!";
            } else {
                $value = "Due to some problem your transaction has been not save,Please try again!";
            }
            $this->getUser()->setFlash('notice', $value);
            $this->redirect('merchentSetting/stopMerchentRequest');
        } else {
            $this->setTemplate('stopMerchentRequest');
        }
    }

    public function executeStopPaymentMerchentRequest(sfWebRequest $request) {
        $selMerchantChoices = array('' => 'Select Merchant');
        $selServiceTypeChoices = array('' => 'Select Merchant Services');
        $selPaymentModeChoices = array('' => 'Select Payment Mode');
        $selPaymentModeOptionChoices = array('' => 'Select Payment Mode Options');

        $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr();
        $paymentModeArray = Doctrine::getTable('PaymentMode')->getPaymentModeArr();

        $this->merchant_choices = $selMerchantChoices + $serviceTypeArray;
        $this->merchant_service_choices = $selServiceTypeChoices;
        $this->payment_choice = $selPaymentModeChoices + $paymentModeArray;
        $this->payment_mode_options = $selPaymentModeOptionChoices;
        $this->form = new StopPaymentMerchantRequestForm(array(), array('payment_mode' => $this->payment_choice, 'payment_mode_option' => $this->payment_mode_options,
                    'merchant' => $this->merchant_choices, 'merchant_service' => $this->merchant_service_choices));
    }

    public function executeSavePaymentMerchentRequest(sfWebRequest $request) {

        $paymentId = $request->getParameter('payment');
        $paymentModeId = $request->getParameter('payment_mode');
        $merchentId = $request->getParameter('merchant');
        $merchentServiceId = $request->getParameter('service_type');
        $sDate = $request->getParameter('startDate');
        $eDate = $request->getParameter('endDate');
        $input_values = array('_csrf_token' => $request->getParameter('_csrf_token'), 'payment' => $paymentId,
            'payment_mode' => $paymentModeId, 'merchant' => $merchentId
            , 'service_type' => $merchentServiceId,
            'startDate' => $sDate, 'endDate' => $eDate);
        $selMerchantChoices = array('' => 'Select Merchant');
        $selServiceTypeChoices = array('' => 'Select Merchant Services');
        $selPaymentModeChoices = array('' => 'Select Payment Mode');
        $selPaymentModeOptionChoices = array('' => 'Select Payment Mode Options');


        $selPaymentModeOptionChoices = $paymentModeId ? ($selPaymentModeChoices + $this->construct_options_array(Doctrine::getTable('PaymentModeOption')->getServiceType($paymentId)->toArray())) : $selPaymentModeOptionChoices;



        $selServiceTypeChoices = $merchentServiceId ? ($selServiceTypeChoices + $this->construct_options_array(Doctrine::getTable('MerchantService')->getServiceTypes($merchentId)->toArray())) : $selServiceTypeChoices;

        $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr();
        $paymentModeArray = Doctrine::getTable('PaymentMode')->getPaymentModeArr();
        $paymentModeArray = $selPaymentModeChoices + $paymentModeArray;
        $selMerchantChoices = $selMerchantChoices + $serviceTypeArray;



        $this->form = new StopPaymentMerchantRequestForm($input_values, array('payment_mode' => $paymentModeArray, 'payment_mode_option' => $selPaymentModeOptionChoices,
                    'merchant' => $selMerchantChoices, 'merchant_service' => $selServiceTypeChoices, 'service_id' => $merchentServiceId));

        $this->form->bind($input_values);
        if ($this->form->isValid()) {

            $saveTrans = paymentScheduleConfigServiceFactory::getService(paymentScheduleConfigServiceFactory::$merchantConfig);
            $transId = $saveTrans->savePaymentServiceConfiguration($paymentId, $paymentModeId, $merchentId, $merchentServiceId, $sDate, $eDate);
            if ($transId) {
                $value = "Payment Configuration transaction has been saved successfully!";
            } else {
                $value = "Due to some problem your transaction has been not save,Please try again!";
            }
            $this->getUser()->setFlash('notice', $value);
            $this->redirect('merchentSetting/stopPaymentMerchentRequest');
        } else {

            $this->setTemplate('stopPaymentMerchentRequest');
        }
    }

    public function executeChainServiceType(sfWebRequest $request) {

        $this->serviceType = Doctrine::getTable('MerchantService')->getServiceTypes($request->getParameter('id'));
    }

    public function executePaymentServiceType(sfWebRequest $request) {
        $this->paymentType = Doctrine::getTable('PaymentModeOption')->getServiceType($request->getParameter('id'));
    }

    private function construct_options_array($data_arrays) {

        $array_to_return = array();
        foreach ($data_arrays as $value) {
            $array_to_return[$value['id']] = $value['name'];
        }
        return $array_to_return;
    }

}
