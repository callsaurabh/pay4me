<?php echo ePortal_pagehead("Restrict Merchant Request", array('class' => '_form')); ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/saveMerchentRequest', array('name' => 'pfm_merchant_request_form', 'id' => 'pfm_merchant_request_form', 'onSubmit' => 'return validateTransferForm()')) ?>
    <?php echo ePortal_legend('Merchant Details'); ?>
    <?php include_partial('stopMerchantRequest', array('form' => $form)) ?>
    <div class="divBlock">
        <center id="multiFormNav">
            <input type="submit" class="formSubmit" value="Save" name="commit"> </center>
    </div>
    </form>
</div>
<script type="text/javaScript">
   
    $(document).ready(function()
    {
        
        $('#merchant').change(function(){

            if($('#merchant').val()!='')
            {
            var merchant_id = this.value;
            var merchant_service_id = '';
            var url = "<?php echo url_for('service_payment_mode_option/MerchantService'); ?>";
            
            $('#service_type').load(url,{ merchant_id: merchant_id,merchant_service_id:merchant_service_id,byPass:1},
            function (data){
                if(data=='logout'){
                    location.reload();
                }
            });
            }else{
                $('#service_type').html("<option value='' selected>Please select Merchant Service</option>") ;
            }
            return false;
        });
    });

    function validateTransferForm()
    {
        var sDate = '';
        $('#err_merchant').html('');
        $('#err_sdate').html('');
        $('#err_merchant_service').html('');
        err =0;
        if($('#merchant').val() == '')
        {
            err = err+1
            $('#err_merchant').html('Please Select Merchant');

        }
        if($('#service_type').val() == '')
        {
            err = err+1;           
            $('#err_merchant_service').html('Please Select Merchant Service');

        }
        if($('#startDate_year').val() != '' ||  $('#startDate_month').val() != '' ||  $('#startDate_day').val() != '' )
        {
            sDate = validateDate( $('#startDate_year').val(), $('#startDate_month').val(), $('#startDate_day').val(), $('#startDate_hour').val(), $('#startDate_minute').val(), $('#startDate_second').val());
            if(!sDate)
            {
                err=err+1;
                $('#err_sdate').html('Please Select Valid Date Time');
                return false;

            }
        }
        else
        {

            err=err+1;
            $('#err_sdate').html('Please Select Valid Date Time');
            return false;
        }
        if($('#endDate_year').val() != '' ||  $('#endDate_month').val() != '' ||  $('#endDate_day').val() != '' )
        {
            var eDate = validateDate( $('#endDate_year').val(), $('#endDate_month').val(), $('#endDate_day').val(), $('#endDate_hour').val(), $('#endDate_minute').val(), $('#endDate_second').val());
            if(!eDate)
            {
                err=err+1;
                $('#err_edate').html('Please select valid date time');
                return false;
            }
            return calculate(sDate,eDate)
        }
        if(err == 0) {
            return true;
        }else{
            return false;
        }


    }
</script>