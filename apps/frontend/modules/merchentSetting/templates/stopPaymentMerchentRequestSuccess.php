<?php echo ePortal_pagehead("Restrict Merchant Payment Request", array('class' => '_form')); ?>

<?php //use_helper('DateForm'); ?>



<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/savePaymentMerchentRequest', array('name' => 'pfm_merchant_request_form', 'id' => 'pfm_merchant_request_form', 'onSubmit' => 'return validateTransferForm()')) ?>

    <?php echo ePortal_legend('Merchant Payment Details'); ?>
    <?php include_partial('stopPaymentMerchantRequest', array('form' => $form)) ?>
    <?php $include_custom = array('year' => 'year', 'month' => 'month', 'day' => 'day', 'hour' => 'hour', 'minute' => 'minute', 'second' => 'second');
    //echo formRowComplete('Start Date<sup>*</sup>',select_datetime_tag('startDate', $include_custom, array('include_custom'=>$include_custom,'include_second'=>true,'use_short_month' => true,'12hour_time' => false)),'( month / day / year   hour : minute : second)','startDate','err_sdate','divsdt'); ?>
    <?php //echo formRowComplete('End Date',select_datetime_tag('endDate', $include_custom, array('include_custom'=>$include_custom,'include_second'=>true,'use_short_month' => true,'12hour_time' => false)),'( month / day / year   hour : minute : second)','stopDate','err_edate','divedt'); ?>




    <div class="divBlock">
        <center id="multiFormNav">
            <input type="submit" class="formSubmit" value="Save" name="commit">
        </center>
    </div>
</form>
</div>
<script type="text/javaScript">
    //window.onload = set_merchant_service_type();
    // window.onload = set_payment_service_type();


    $(document).ready(function()
    {
        $('#merchant').change(function(){
            var merchant_id = this.value;
            var merchant_service_id = '';
            var url = "<?php echo url_for('service_payment_mode_option/MerchantService'); ?>";
            $('#service_type').load(url,{ merchant_id: merchant_id,merchant_service_id:merchant_service_id,byPass:1},function (data){
                if(data=='logout'){
                    location.reload();
                }
            });
            return false;
        });

        $('#payment').change(function(){
            
            var payment_id = this.value;
            var payment_mode_option_id = '';
            var url = "<?php echo url_for('service_payment_mode_option/PaymentModeOption'); ?>";
            $('#payment_mode').load(url,{ payment_mode_id: payment_id,payment_mode_option_id:payment_mode_option_id,byPass:1},function (data){
                if(data=='logout'){
                    location.reload();
                }
            });
            return false;
        });

    });


    function validateTransferForm()
    {
        var sDate =
            $('#err_payemnt').html('');
        $('#err_payment_mode').html('');
        $('#err_merchant').html('');
        $('#err_service_type').html('');
        $('#err_startDate').html('');
         err =0;
        if($('#payment').val() == '')
        {
            $('#err_payemnt').html('Please Select Payment Mode');
            err = err+1;
            //return false;
        }

        if($('#payment_mode').val() == '')
        {
            err = err+1;
            $('#err_payment_mode').html('Please Select Payment Mode Option');
           // return false;
        }
        if($('#merchant').val() == '')
        {
            err = err+1;
            $('#err_merchant').html('Please Select Merchant');
            //return false;
        }
        if($('#service_type').val() == '')
        {    
            err = err+1;
            $('#err_service_type').html('Please Select Merchant Service');     
            //return false;
        }

        sDate = validateDate( $('#startDate_year').val(), $('#startDate_month').val(), $('#startDate_day').val(), $('#startDate_hour').val(), $('#startDate_minute').val(), $('#startDate_second').val());
        if(!sDate)
        {
            err = err+1;
            $('#err_sdate').html('Please Select Valid Date Time');
            return false;

        }

        if($('#endDate_year').val() != '' ||  $('#endDate_month').val() != '' ||  $('#endDate_day').val() != '' )
        {
            var eDate = validateDate( $('#endDate_year').val(), $('#endDate_month').val(), $('#endDate_day').val(), $('#endDate_hour').val(), $('#endDate_minute').val(), $('#endDate_second').val());
            if(!eDate)
            {
                err = err+1;
                $('#err_edate').html('Please select valid date time');
                return false;
            }
           
            return calculate(sDate,eDate)
        }
        if(err == 0) {
            return true;
        }else{
            return false;
        }

    }

</script>