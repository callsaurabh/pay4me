
<dl id="divpayment">
<div class="dsTitle4Fields"><?php echo $form['payment']->renderLabel(); ?></div>
<div class="dsInfo4Fields"><?php echo $form['payment']->render(); ?>
<div id="err_payemnt" class="error_listing cRed"><?php echo $form['payment']->renderError(); ?></div>
</div>
</dl>
<dl id="divpayment_mode">
<div class="dsTitle4Fields"><?php echo $form['payment_mode']->renderLabel(); ?></div>
<div class="dsInfo4Fields"><?php echo $form['payment_mode']->render(); ?>
<div id="err_payment_mode" class="error_listing cRed"><?php echo $form['payment_mode']->renderError(); ?></div>
</div>
</dl>
<dl id="merchent">
<div class="dsTitle4Fields"><?php echo $form['merchant']->renderLabel(); ?>
</div>
<div class="dsInfo4Fields"><?php echo $form['merchant']->render(); ?>
<div id="err_merchant" class="error_listing cRed"><?php echo $form['merchant']->renderError(); ?></div>
</div>
</dl>
<dl id="divmserv">
<div class="dsTitle4Fields"><?php echo $form['service_type']->renderLabel(); ?>
</div>
<div class="dsInfo4Fields"><?php echo $form['service_type']->render(); ?>
      <div id="err_service_type" class="error_listing cRed"><?php echo $form['service_type']->renderError(); ?></div>
</div>
</dl>
<dl id="divsdt">
<div class="dsTitle4Fields"><?php echo $form['startDate']->renderLabel(); ?>
</div>
<div class="dsInfo4Fields"><?php echo $form['startDate']->render(); ?><br>( month / day / year   hour : minute : second)
<div id="err_sdate" class="error_listing cRed"><?php echo $form['startDate']->renderError(); ?></div>
</div>
</dl>
<dl id="divedt">
<div class="dsTitle4Fields"><?php echo $form['endDate']->renderLabel(); ?></div>
<div class="dsInfo4Fields"><?php echo $form['endDate']->render(); ?><br>( month / day / year   hour : minute : second)
<div id="err_edate" class="error_listing cRed"><?php echo $form['endDate']->renderError(); ?></div>
</div>
</dl>
<?php echo $form['_csrf_token']; ?>