
<dl id="merchent">
    <div class="dsTitle4Fields"><?php echo $form['merchant']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['merchant']->render(); ?><br>
       <div id="err_merchant" class="error_listing cRed"><?php echo $form['merchant']->renderError(); ?></div>
    </div>
</dl>
<dl id="divmserv">
    <div class="dsTitle4Fields"><?php echo $form['service_type']->renderLabel(); ?>
    </div>
    <div class="dsInfo4Fields"><?php echo $form['service_type']->render(); ?>
        <br>
        <div id="err_merchant_service" class="error_listing cRed"><?php echo $form['service_type']->renderError(); ?></div>
    </div>
</dl>

<dl id="divsdt">
    <div class="dsTitle4Fields"><?php echo $form['startDate']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['startDate']->render(); ?><br>( month / day / year   hour : minute : second)<br> 
        <div id="err_sdate" class="error_listing cRed"> <?php echo $form['startDate']->renderError(); ?> </div>
    </div>
</dl>
<dl id="divedt">
    <div class="dsTitle4Fields"><?php echo $form['endDate']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['endDate']->render(); ?><br>( month / day / year   hour : minute : second)<br><br>
          <div id="err_edate" class="error_listing cRed"><?php echo $form['endDate']->renderError(); ?></div>
    </div>
</dl>
<?php echo $form['_csrf_token']; ?>