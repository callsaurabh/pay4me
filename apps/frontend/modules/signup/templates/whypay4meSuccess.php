<?php
//use_stylesheet('reset.css');
//use_stylesheet('default.css');
//use_stylesheet('menu.css');
//use_stylesheet('admin_main.css');
//use_stylesheet('admin_theme.css');
use_stylesheet('main.css');
echo include_stylesheets();
?>
<div id="faqPage" class="cmspage">
    <?php
    echo ePortal_pagehead('Why Pay4Me?');
    ?>

    <div class="block-container">
         <div class="HFeaturesNoImage">
              <p>
               Pay4Me Services protects the online purchaser from fraudulent activities, when you register with Pay4Me all online
               transactions you conduct with Pay4Me registered merchants like “this one”, will be verified with you logging in to your 
               account which will then allow you to proceed to continue purchasing your desired product either with your credit/debit
               card or your Pay4Me eWallet.
               </p>
              <p>&nbsp;</p>
              <p>Registering for Pay4Me has its immediate benefits, you get the bespoke Pay4Me eWallet account that allows you to store value for online transactions. All eWallet transactions are PIN driven for added security.
              </p>
        </div>
        </div>
</div>
