<div class="innerWrapper">
<?php
// use_helper('Form');
use_javascript('jquery-ui.min.js');
use_javascript('general.js');
use_javascript('jquery.keypad.password.js');
use_stylesheet('jquery.keypad.css');
use_helper('sfCryptoCaptcha');
?>

<?php
echo $form->renderGlobalErrors();
include_partial('cms/leftBlock', array('title' => 'Merchant Activation'))
?>
<div id="wrapperInnerRight">
    <div id="innerImg">
        <?php echo image_tag('/img/new/img_about_pay4bill.jpg', array('alt' => 'About Pay4me', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
    </div>


    <?php
    if(!sfConfig::get('app_disable_merchant_registration')){
    echo include_partial('signuplabel', array('applingCountry' => $applingCountry ,'merchantactivation' => 'merchantactivation')); ?>
        <form name="merchantSignup" id="merchantSignup"  method="post" action="<?php echo url_for('signup/saveMerchant')?>" onSubmit="return validate_signup();">

            <div class="feedbackForm">
                <div class="header">Registration Form</div>
                <div class="fieldName" style="display: none;"><?php echo $form['name']->renderLabel(); ?><span class="required"> *</span></div>
                <div class="fieldWrap" style="display: none;">
                    <label><?php echo $form['name']->render(); ?></label>
                    <div class="cRed" id="err_name"><?php echo $form['name']->renderError(); ?></div>
                </div>
                <div class="fieldName">Merchant Name<span class="required"> *</span></div>
                <div class="fieldWrap">
                    <label><?php if(isset($merchantname)){
                      echo $merchantname;
                    }?>
                    </label>
                    <div class="cRed" id="err_name"><?php echo $form['name']->renderError(); ?></div>
                </div>
                <div class="fieldName"><?php echo $form['username']->renderLabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap">
                    <label> <?php echo $form['username']->render(); ?>
                        <div class="cRed" id="err_username"><?php echo $form['username']->renderError(); ?></div>
                        <div class="cGreen" id="sug_username"></div>
                    </label>
                <?php if ($form['username']->hasError()) {
                    echo "</br>";
                } ?><span class="message"><a href="javascript:;" onclick="return checkusernameavailability();">Check Availability</a></span>
            </div>

            <div class="fieldName"><?php echo $form['password']->renderLabel(); ?>  <span class="required">*</span></div>
            <div class="fieldWrap"><p><label><?php echo $form['password']->render(); ?></label>
                <div class="cRed" id="err_password"><?php echo $form['password']->renderError(); ?></div><br/>
                <span class="clearfix"></span>
                </p>
                <div >Password Strength<br/><span id="strength"></span>
                </div>
                <div style="clear:both;"></div>
                <p><span class="desc" >* Minimum 8 characters in length<br> * Must contain atleast 1 special, 1 numeric and 1 alphabetic character</span></p>
            </div>

            <div class="fieldName"><?php echo $form['cpassword']->renderLabel(); ?>   <span class="required">*</span></div>
            <div class="fieldWrap">
                <p>
                    <label><?php echo $form['cpassword']->render(); ?></label>
                <div class="cRed" id="err_cpassword"><?php echo $form['cpassword']->renderError(); ?></div>
                </p>
            </div>

            <div class="fieldName"><?php echo $form['email']->renderLabel(); ?> <span class="required">*</span></div>
            <div class="fieldWrap"><label><?php echo $form['email']->render(); ?></label>
                <div class="cRed" id="err_email"><?php echo $form['email']->renderError(); ?></div>
            </div>
                <div class="fieldName"><?php echo $form['captcha']->renderlabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap">
                    <label><?php echo $form['captcha']->render(); ?><br/><br/>
                    <?php echo captcha_image();
                    echo captcha_reload_button(); ?>
                </label>
                <div class="cRed" id="err_vocde"><?php echo $form['captcha']->renderError(); ?></div>
            </div>
<?php echo $form['byPass']->render(); ?>
                    <div class="fieldName">&nbsp;</div>
                    <div  id="submitdiv">
                        <label>
                    <?php if ($form->isCSRFProtected()) : ?>
                    <?php echo $form['_csrf_token']->render(); ?>
<?php endif; ?>
                        <input type="submit" class="button_activation_merchant" name="submit" value="Continue" >
                    </label>
                </div>
                <div class="fieldWrap" id ="progBarDiv"  style="display:none;"></div>
<?php if ($applingCountry): ?>
                            <div class="fieldWrap"><span class="cRed">Warning: Mercahnt service is not available in your country.</span></div>
<?php endif; ?>
                            <div class="fieldName">&nbsp;</div>
                            <div class="fieldWrap"><span class="message">[*] Fields marked as * are Mandatory.</span></div>
                        </div>

                    </form>
                    <?php }else{?>
                    <div id='flash_error'  class='error_list_user'><span>Merchant Activation has been temporarily suspended and will be resumed shortly. We regret the inconvenience caused and expect your cooperation.</span></div>
                    <?php } ?>
                    <p>&nbsp;</p>
                </div>
</div>

<script  type="text/javascript">
                    function checkusernameavailability() {
                        $('#sug_username').html("");
                        var uri = "<?php echo url_for('signup/ChkUserAvailability', true) . "?byPass=1"; ?>";
                        if($('#username').val() == "") {
                            // $('#err_username').html("Please enter Username");
                            $('#username').focus();
                        }else if(validateUsernamestring($('#username').val())) {
                            //$('#err_username').html("Please enter valid Username");
                        } else {
                            $('.inputTipContainer').removeClass("badInputTipContainer");
                            $('.inputTipContainer').removeClass("goodInputTipContainer");
                            $('.inputTipContainer').addClass("checkInputTipContainer");
                            $('.inputTipContainer').html('Validating');
                            $.post(uri,{username:$('#username').val(),name:$('#name').val(),lname:$('#lname').val()},
                            function(data){
                                if(data.status){
                                    // $("#err_username").html('');
                                } else {
                                    // $("#err_username").html(data.msg);
                                }
                                $('.inputTipContainer').removeClass("checkInputTipContainer");
                                usernameresponse(data);
                            },'json');
                        }
                    }


                    $(document).ready(function(){
                        //$('#username').focus();
                        var url ='<?php echo url_for('signup/calculateStrength'); ?>';
                        calculatePasswordStrength('password', url);
                        $('#password').after(addVirtualCheckBox('<span style="display:block;margin:5px 2px 2px 0">','</span>'));
                        enableMultiPasswordKeypad('password',"password",url);
                    });


                    function validateUsernamestring(str) {
                        var reg = /^[a-z](?=[\w.]{4,19}$)\w*\.?\w*$/i;
                        var subreg = /^[a-z0-9]$/i;
                        var str = new String(str);
                        if(reg.test(str) == false || subreg.test(str.charAt( str.length-1 ))== false) {
                            return true;
                        }
                        return false;
                    }

                    function progBarDiv(referenceDivId, targetDivId,status){
                        if(status){
                            $('#'+targetDivId).html('<?php echo image_tag('../img/ajax-loader.gif'); ?>');
                            $('#'+referenceDivId).css("display","none");
                            $('#'+targetDivId).css("display","inline");
                        } else {
                            $('#'+referenceDivId).css("display","block");
                            $('#'+targetDivId).css("display","none");
                        }

                    }

                    function fillusername(obj){
                        $("#username").val($(obj).text());
                        $('.inputTipContainer').removeClass("badInputTipContainer");
                        $('.inputTipContainer').addClass("goodInputTipContainer");
                        $('.inputTipContainer').html('Username available');

                        return false;

                    }


                    function usernameresponse(data){
                        if(data.status){
                            $('.inputTipContainer').addClass("goodInputTipContainer");
                            $("#sug_username").html('');
                            $('.inputTipContainer').html('Username available');
                        } else {
                            $('.inputTipContainer').addClass("badInputTipContainer");
                            $('.inputTipContainer').html('Username not available');
                            var arraySuggestion = new Array();
                            arraySuggestion = data.suggestions;
                            var suggestion = "<span class='sug_txt'>Suggestions:</span><ul>";
                            for(var i=0;i<arraySuggestion.length;i++){
                                if(arraySuggestion[i]!=""){
                                    var str = arraySuggestion[i];
                                    suggestion += "<li><a href=# onclick = 'return fillusername(this)'>"+str+"</a></li>";
                                }
                            }
                            suggestion += "</ul>";
                            $("#sug_username").html(suggestion);
                        }
                    }


                    function validate_signup(){

                        $('.inputTipContainer').hide();
                        progBarDiv('submitdiv','progBarDiv',1);
                        // document.forms[0].action = "<?php //echo url_for($sf_context->getModuleName() . '/create');      ?>";
                        // document.forms[0].target ='';
                        var err  = 0;
                        if($('#name').val() == "")
                        {
                            $('#err_name').html("Please enter Merchant Name");
                            err = err+1;
                            $('#name').focus();
                        }
                        else
                        {
                            $('#err_name').html("");
                        }
                        if($('#username').val() == "")
                        {
                            $('#err_username').html("Please enter Username");
                            err = err+1;
                            $('#username').focus();
                        }else if(validateUsernamestring($('#username').val())) {
                            $('#err_username').html("Please enter valid Username");
                            err = err+1;
                            $('#username').focus();
                        } else {
                            $('#err_username').html("");
                        }
                        var regalpha = /[a-z]/i;
                        var regnumeric = /[0-9]/;
                        var regspecial = /[\^\$\.\?\*\!\+\:\=\(\)\[\]\{\}\|@#%&_-]/;
                        if(jQuery.trim($('#password').val()) == "")
                        {
                            $('#err_password').html("Please enter Password");
                            err = err+1;
                            $('#password').focus();
                        } else if(document.getElementById('password').value.length < 8){
                            $('#err_password').html("Password should be at least 8 characters minimum.");
                            err = err+1;
                            $('#password').focus();
                        } else if(!regalpha.test(jQuery.trim($('#password').val())) ){
                            $('#err_password').html("Password must contain atleast 1 alphabetic character");
                            err = err+1;
                            $('#password').focus();
                        } else if(!regnumeric.test(jQuery.trim($('#password').val())) ){
                            $('#err_password').html("Password must contain atleast 1 numeric character");
                            err = err+1;
                            $('#password').focus();
                        } else if(!regspecial.test(jQuery.trim($('#password').val())) ){
                            $('#err_password').html("Password must contain atleast 1 special character");
                            err = err+1;
                            $('#password').focus();
                        } else {
                            $('#err_password').html("");
                        }

                        if(jQuery.trim($('#cpassword').val()) == "")
                        {
                            $('#err_cpassword').html("Please Confirm Password");
                            err = err+1;
                            $('#cpassword').focus();
                        }
                        else {
                            $('#err_cpassword').html("");
                        }
                        if($('#password').val() != '' &&  $('#cpassword').val() != '')
                        {
                            if($('#password').val() != $('#cpassword').val())
                            {
                                $('#err_cpassword').html("Your confirmed password does not match the entered password");
                                err = err+1;
                                $('#cpassword').focus();
                            }
                            else
                            {
                                $('#err_cpassword').html("");
                            }
                        }

                        if($('#email').val() == "")
                        {
                            $('#err_email').html("Please enter Email Address");
                            err = err+1;
                            $('#email').focus();
                        }
                        else
                        {
                            $('#err_email').html("");
                        }
                        if($('#email').val() != ""){
                            if(!validateEmail($('#email').val()))
                            {
                                $('#err_email').html("Please enter valid Email");
                                err = err+1;
                                $('#email').focus();
                            }
                            else
                            {
                                $('#err_email').html("");
                            }
                        }
                        if(jQuery.trim($('#captcha').val()) == "")
                        {
                            $('#err_vocde').html("Please enter Verification Code");
                            err = err+1;
                            $('#captcha').focus();
                        }
                        else {
                            $('#err_vocde').html("");
                        }
                        if(err == 0) {
                            $("#byPass").val(1);
                            return true;
                        } else {
                            $('#password').val('');
                            $('#cpassword').val('');
                            $('#captcha').val('');
                            $('#password_strength_bar').html('');
                            progBarDiv('submitdiv','progBarDiv',0);
                            document.getElementById('captcha_img').src = "<?php echo url_for('@sf_captcha') ?>?r=" + Math.random() + "&reload=1";
                            return false;
                        }

                    }

                    function seterrorresponse(msgArray){
                        if(msgArray.name!=""){
                            $('#name').focus();
                        }else if(msgArray.uname!=""){
                            $('#username').focus();
                        }else if(msgArray.password!=""){
                            $('#password').focus();
                        }else if(msgArray.cpassword!=""){
                            $('#cpassword').focus();
                        }else if(msgArray.email!=""){
                            $('#email').focus();
                        }else if(msgArray.captcha!=""){
                            $('#captcha').focus();
                        }
                        $("#err_name").text(msgArray.fname);
                        $("#err_username").text(msgArray.uname);
                        $("#err_password").text(msgArray.password);
                        $("#err_cpassword").text(msgArray.cpassword);
                        $("#err_email").text(msgArray.email);
                        $("#err_vocde").text(msgArray.captcha);
                    }

                    function validateString(str) {
                        var reg = /^([A-Za-z0-9])+([A-Za-z0-9 {1}])$/;
                        if(reg.test(str) == false) {
                            return true;
                        }

                        return false;
                    }

                    $("#username").inputTip({
                        // Text displayed when the input passes the validation
                        goodText: "Username Available",
                        // Text displayed when the input doesn't pass the validation
                        badText: "Username is not valid or not available",
                        // Text displayed as a tip when the input field is focused
                        tipText: "Username must be between 5 to 20 characters. Only letters, numbers, underscores, and one dot (.) are allowed in Username. Username should start with a character and end with character or number.",
                        validateText: function (inputValue, callback) {
                            $('.inputTipContainer').removeClass("badInputTipContainer");
                            $('.inputTipContainer').removeClass("goodInputTipContainer");
                            $('.inputTipContainer').addClass("checkInputTipContainer");
                            $('.inputTipContainer').html('Validating');
                            // Checking if the input field contains text.
                            if(inputValue.length==0){
                                $('.inputTipContainer').removeClass("checkInputTipContainer");
                                $("#sug_username").html('');
                                $("#err_username").html('');
                                callback(0,'Please enter Username');
                            } else if (inputValue.length > 0){
                                if(validateUsernamestring($('#username').val())){
                                    $('.inputTipContainer').removeClass("checkInputTipContainer");
                                    $("#sug_username").html('');
                                    $("#err_username").html('');
                                    callback(0,'Please enter valid Username');
                                } else  {
                                    var uri = "<?php echo url_for('signup/ChkUserAvailability', true) . "?byPass=1"; ?>";
                                    $.post(uri,{username:$('#username').val(),name:$('#name').val(),lname:$('#lname').val()},
                                    function(data){
                                        usernamevalidateresponse(data);
                                        $('.inputTipContainer').removeClass("checkInputTipContainer");
                                        if(Boolean(data.status)){
                                            callback(1,data.msg);
                                        } else {
                                            callback(0,data.msg);
                                        }
                                    },'json');


                                }
                            }
                        },
                        validateInRealTime: false
                    });


                    function validateEmail(email) {
                        var reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

                        if(reg.test(email) == true) {
                            return true;
                        }

                        return false;
                    }

                    function validateUserName(){
                        var err = 0
                        if($('#username').val() == "")
                        {
                            $('#err_username').html("Please enter Username");
                            err = err+1;
                        }
                        else
                        {
                            $('#err_username').html("");
                        }
                        if(err == 0)
                        {
                            ajax_call('err_username', "<?php echo url_for('signup/ChkUserAvailability', true) . "?byPass=1"; ?>", 'signup');
        }
    }



    function usernamevalidateresponse(data){
        if(data.status){
            $("#sug_username").html('');
            $("#err_username").html('');
        } else {
            $("#err_username").html('');
            var arraySuggestion = new Array();
            arraySuggestion = data.suggestions;
            var suggestion = "<span class='sug_txt'>Suggestions:</span><ul>";
            for(var i=0;i<arraySuggestion.length;i++){
                if(arraySuggestion[i]!=""){
                    //               var click = 'onclick=return fillusername("'+arraySuggestion[i]+'")';
                    var str = arraySuggestion[i];
                    suggestion += "<li><a href=# onclick = 'return fillusername(this)'>"+str+"</a></li>";
                }
            }
            suggestion += "</ul>";
            $("#sug_username").html(suggestion);
            //$("#err_username").html('Username not available');
        }
    }



    function updCaptcha(uri)
    {
        url = uri+"?r=" + Math.random() + "&reload=1";
        $('#captchaImg').attr('src', url);
    }
    function load_captcha(url){

        document.getElementById('captcha_img').src=url;
        $('#cap_ref').hide();
        setTimeout('show_reload_button()', 1000);

    }
    function show_reload_button(){


        $('#cap_ref').show();
    }
</script>
