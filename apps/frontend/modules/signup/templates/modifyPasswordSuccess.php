<div class="innerWrapper">
<?php include_javascripts() ?>
<?php use_javascript('general.js') ?>
<?php if($err==''){?>
<br>
<?php echo ePortal_pagehead(__('Welcome back, '.ucfirst($userDisplayName)),array('class'=>'_form')); ?>
<p>You've verified your account details and may now change your password.</p>
<br>
<?php include_partial('formChange', array('form' => $form, 'submitTo'=>$submitTo, 'userId'=>$userId, 'token'=>$token)) ?>
<?php  }else{
echo ePortal_highlight($err,"Access Error",array('class'=>'error_list','id'=>'flash_error'));
}?>
</div>

