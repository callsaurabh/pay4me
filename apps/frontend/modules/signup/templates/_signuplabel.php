<?php
if(isset($merchantactivation)){
  echo ePortal_pagehead_unAuth('Merchant Account Activation');
}
elseif(isset($merchantregistration)){
  echo ePortal_pagehead_unAuth('Merchant Registration');
}
else{
echo ePortal_pagehead_unAuth('Sign up for the e-Wallet service.');
}
?>
<?php if($applingCountry): ?>
<div class="error_list" id="flash_error">
  <span>Warning: e-Wallet service is not available in your country.</span>
</div>
<?php endif; ?>