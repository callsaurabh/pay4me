<?php // use_helper('Form');?>
<div id="signupPage" class="cmspage">
  <?php
  echo ePortal_pagehead('eWallet');
  ?>
  <div class="wrapForm2">
  <?php echo form_tag($sf_context->getModuleName().'/activateAccount',array('name'=>'signup','id'=>'signup', 'onSubmit'=>'return validateUnblockForm("1")')); ?>
    <?php
    echo ePortal_legend('Unblock Logged In Account');
    ?>
    <?php echo formRowComplete('Username<sup>*</sup>','<input type="text" name="username" id="username" value="'.$sf_params->get("username").'" class="FieldInput" maxlength="50">','','username','err_username','username_row'); ?>
    <?php //echo formRowComplete('Username<sup>*</sup>',input_tag('username', $sf_params->get('username'), array('class'=>'FieldInput' ,'maxlength'=>50)),'','username','err_username','username_row'); ?>
    <dl id='username_row'>
        <div class="dsTitle4Fields"></div>
        <div class="dsInfo4Fields">
        <div>
          <div style="float:left">
            Any one of two mentioned below
          </div>
        </div>
        </div>
    </dl>
    <?php echo formRowComplete('Amount of last load','<input type="text" name="loadAmount" id="loadAmount" value="'.$sf_params->get("loadAmount").'" class="FieldInput" maxlength="30">','','name','err_loadAmount','username_row'); ?>
    <?php //echo formRowComplete('Amount of last load',input_tag('loadAmount', $sf_params->get('loadAmount'), 'class=FieldInput maxlength=30'),'','name','err_loadAmount','username_row'); ?>
    <?php echo formRowComplete('Amount of last spent','<input type="text" name="spentAmount" id="spentAmount" value="'.$sf_params->get("spentAmount").'" class="FieldInput" maxlength="30">','','lname','err_spentAmount','username_row'); ?>
    <?php //echo formRowComplete('Amount of last spent',input_tag('spentAmount', $sf_params->get('spentAmount'), 'class=FieldInput maxlength=30'),'','lname','err_spentAmount','username_row'); ?>

    <div class="divBlock">
          <center id="multiFormNav">
             &nbsp;&nbsp;<?//php echo submit_tag('Submit', array('class'=>'formSubmit','value'=>'Unblock Account'));?>
             <input type="Submit" name="Submit" value="Unblock Account"  class="formSubmit" >
          </center>
    </div>

    </form>
    
    </div>
</div>
<script>
  function validateUnblockForm(submit_form)
  {

    var err  = 0;
    if($('#username').val() == "")
    {
      $('#err_username').html("Please enter Username");
      err = err+1;
    }
    else
    {
      $('#err_username').html("");
    }
    if($('#username').val() != ""){
      if(validateString($('#username').val()))
      {
        $('#err_username').html("Please enter Valid Username");
        err = err+1;
      }
      else
      {
        $('#err_username').html("");
      }
    }
    if($('#loadAmount').val() == "" && $('#spentAmount').val() == "")
    {
      if(validateNumber($('#loadAmount').val()) && validateNumber($('#spentAmount').val()))
      {
        $('#err_loadAmount').html("Please enter either Amount of Last Load or Amount of Last Spent");
        err = err+1;
      }
      else
      {
        $('#err_loadAmount').html("");
      }
    }

    if($('#loadAmount').val() != "")
    {
      if(validateNumber($('#loadAmount').val()))
      {
        $('#err_loadAmount').html("Please enter Valid Amount of Last Load");
        err = err+1;
      }
      else
      {
        $('#err_loadAmount').html("");
      }
    }
    if($('#spentAmount').val() != "")
    {
      if(validateNumber($('#spentAmount').val()))
      {
        $('#err_spentAmount').html("Please enter Valid Amount of Last Spent");
         err = err+1;
      }
      else
      {
        $('#err_spentAmount').html("");
      }
    }
    if(err == 0)
    {
      if(submit_form == 1)
      {
        return true;
      }
    }else{
      return false;
    }
    
  }
  function validateString(str) {
    var reg = /^([A-Za-z0-9\.])+$/;

    if(reg.test(str) == false) {
      return true;
    }
    return false;
  }
  function validateNumber(str) {
    var reg = /^((\d+(\.\d*)?)|((\d*\.)?\d+))$/;

    if(reg.test(str) == false) {
      return true;
    }
    return false;
  }
  
</script>
