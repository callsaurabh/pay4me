<div class="innerWrapper">
<?php  //use_helper('Form'); ?>
<style type="text/css">
.error{*position:inherit!important; *bottom:0px!important;}
</style>
<div id="wrapperInnerRight_new">
  <h3><?php echo __("Forgot Password"); ?></h3><br/>                
  <?php if($securityQuestion):  ?>
  <?php echo form_tag($sf_context->getModuleName().'/createForgotPassword',
            array('name'=>'forgot','id'=>'forgot',
                'onSubmit'=>'return validateForgotForm(this)'));
    ?>
  <?php else: ?>
    <?php echo form_tag($sf_context->getModuleName().'/forgotPassword',
            array('name'=>'forgot','id'=>'forgot',
                'onSubmit'=>'return validateForgotForm(this)'));
    ?>
  <?php endif; ?>
     <div class="feedbackForm_new">
         <div class="header">Please enter your account details </div>   
         <?php  echo $form['_csrf_token']; ?>
         <div class="cRed" id="err_csrf"><?php echo $form['_csrf_token']->renderError(); ?> </div>
         <div class="clearfix"></div>
         <?php echo $form['security_question_id']; ?>
         <div class="dsTitle4Fields">
            <?php echo $form['username']->renderLabel(); ?>
         </div>
         <div class="fieldWrap">
            <label>
            <?php echo $form['username'] ?>
            </label>
            <div class="cRed" id="err_name"> <?php echo $form['username']->renderError(); ?></div>
        </div>
         <div class="clearfix"></div>
         <?php if($securityQuestion): ?>
            <div class="dsTitle4Fields">
                <?php echo $form['security_question']->renderLabel(); ?>
             </div>
               <div class="fieldWrap">
                <label>
                <?php echo $form['security_question'] ?>
                </label>
                <div class="cRed" id="err_answer"><?php echo $form['security_question']->renderError(); ?>&nbsp;</div>
            </div>
         <?php else: ?>
            <?php echo $form['security_question'] ?>
         <?php endif; ?>       
            <div class="clearfix"></div>
         <div class="dsTitle4Fields"><label>&nbsp;</label></div>
         
        <div class="fieldWrap">
          <label>
            <?php // echo submit_tag('Submit', array('class'=>'button','value'=>'Continue'));?>
            <input type="submit" class="button" value="Continue" name="commit">
          </label>
            <div class="cRed" id="err_submit"></div>
        </div>
     </div>
    </form>

   
</div>
</div>
<script type="text/javascript">
  function validateForgotForm(form){     
      var error = false;
      if($("#forgot_username").val() ==""){
          error = true;
          $("#err_name").html("Please enter Username");
      }
      
      if($("#forgot_security_question:input").attr("type") =="password"){
          if($("#forgot_security_question").val() ==""){
          error = true;
          $("#err_answer").html("Please enter answer to your Security question");
        }
      }

      if(!error){
        return true;
      } else {
          return false;
      }
  }

  </script>
