<?php // use_helper('Form');?>
<div id="feedbackPage" class="cmspage">

<?php echo ePortal_pagehead('eWallet'); ?>
  <div class="wrapForm2">
    <?php echo form_tag(''); ?>

        <?php   echo ePortal_legend('Your Account Credentials'); ?>
        <div class="descriptionArea">


<pre>
You will receive an email shortly to Activate your account.

Please follow the link from the confirmation email to activate your Pay4Me eWallet account.
Your confirmation email will be sent from <a href="mailto:noreply@pay4me.com">noreply@pay4me.com</a>and should reach you shortly.
If you do not receive your confirmation email within 24 hours.
please check your "Junk Mail" or contact us at <a href="mailto:support@pay4me.com">support@pay4me.com</a> with your just
signed up username.

To ensure that emails from Pay4me reach you,
please add @<a href="http://pay4me.com/">pay4me.com</a> to your 'Safe Senders' list.

</pre>    </div>

</form>
</div>
<div>&nbsp;</div>
</div>

