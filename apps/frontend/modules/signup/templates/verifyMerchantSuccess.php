<?php
// use_helper('Form');
use_javascript('jquery-ui.min.js');
use_javascript('general.js');
use_javascript('jquery.keypad.password.js');
use_stylesheet('jquery.keypad.css');
use_helper('sfCryptoCaptcha');
?>
<div class="innerWrapper">

<div  class="innerWrapper_level_1">
  <div>
      <?php
        echo $form->renderGlobalErrors();
        include_partial('cms/leftBlock', array(
            'title' => 'Activate Merchant'
        ));
      ?>
  </div>
  <div>
      <?php
        include_partial('page/merchantServicesBlock', array());
      ?>
  </div>
</div>
<div style="float: right;">
<div id="wrapperInnerRight">
		<div id="innerImg">
        <?php echo image_tag('/img/new/img_about_pay4bill.jpg', array('alt' => 'About Pay4me', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
    </div>


    <?php echo include_partial('signuplabel', array('merchantactivation' => 'merchantactivation','applingCountry' => "")); ?>
        <form name="signup" id="signup" method="post" action="<?php echo url_for('signup/merchantExist'); ?>"
			onSubmit="return validate_accountactivation();">

			<div class="feedbackForm">
				<div class="header">Account Activation</div>
				<div class="fieldName"><?php echo $form['merchantname']->renderLabel(); ?><span
						class="required"> *</span>
				</div>
				<div class="fieldWrap">
					<label><?php echo $form['merchantname']->render(); ?></label>
					<div class="cRed" id="err_merchantname"><?php echo $form['merchantname']->renderError(); ?></div>
				</div>
				<div class="fieldName"><?php echo $form['merchantcode']->renderLabel(); ?><span
						class="required"> *</span>
				</div>
				<div class="fieldWrap">
					<label><?php echo $form['merchantcode']->render(); ?></label>
					<div class="cRed" id="err_merchantcode"><?php echo $form['merchantcode']->renderError(); ?></div>
				</div>
				<div class="fieldName"><?php echo $form['merchantkey']->renderLabel(); ?><span
						class="required"> *</span>
				</div>
				<div class="fieldWrap">
					<label><?php echo $form['merchantkey']->render(); ?></label>
					<div class="cRed" id="err_merchantkey"><?php echo $form['merchantkey']->renderError(); ?></div>
				</div>
				<div id="submitdiv">
					<label>
                    <?php if ($form->isCSRFProtected()) : ?>
                    <?php echo $form['_csrf_token']->render(); ?>
<?php endif; ?>
                        <input type="submit"
						class="button_merchant_exist" name="submit" value="Next">
<?php  echo button_to('Cancel','',array('class'=>'button_merchant_exist_cancel', 'onClick'=>'location.href=\''.url_for('page/merchantService').'\''));?>
                    </label>
				</div>
</div>
		</form>

		<p>&nbsp;</p>
	</div>

</div>
</div>
<script type="text/javascript">

function validate_accountactivation(){

    var err  = 0;
    if($('#merchantname').val() == "")
    {
        $('#err_merchantname').html("Please enter Merchant Name");
        err = err+1;
        $('#merchantname').focus();
    }
    else
    {
        $('#err_merchantname').html("");
    }

    if($('#merchantcode').val() == "")
    {
        $('#err_merchantcode').html("Please enter Merchant Code");
        err = err+1;
        $('#merchantcode').focus();
    }
    else
    {
        $('#err_merchantcode').html("");
    }

    if($('#merchantkey').val() == "")
    {
        $('#err_merchantkey').html("Please enter Merchant Key");
        err = err+1;
        $('#merchantkey').focus();
    }
    else
    {
        $('#err_merchantkey').html("");
    }
    if(err == 0) {
        $("#byPass").val(1);
        return true;
    } else {
        return false;
    }

}


</script>
