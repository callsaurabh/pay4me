<span class="notifications_question"><?php echo ePortal_pagehead("To simplify the process, the number of Security Questions have been reduced. <br>Kindly choose any three questions to proceed.
 "); ?></span>
<?php //use_helper('Form'); ?>
<?php $userObj = $sf_context->getUser(); ?>
<div class="wrapForm2">
    <form action="" method="post" >    

        <div class ="formHead" >
            <div style="float:right;text-align:right!important;">
                [<span>    
                    <?php echo link_to(__('Logout'), '@sf_guard_signout'); ?></span>]</b>
                <br />
            </div>
            Security Question  
            <?php 
                $userObj = $sf_context->getUser();    
                if($userObj->isAuthenticated()){
             ?>
            </div>
         <?php } ?>
        <?php echo formRowComplete($form['questions']->renderLabel() . '<sup class=cRed>*</sup>', $form['questions']->render(), '', 'questions', 'err_questions', 'err_questions_row', $form['questions']->renderError()); ?>
        <div class="divBlock">
            <center>
                <?php echo $form[$form->getCSRFFieldName()]->render(); ?>               
                <input type="submit" value="Save" class="formSubmit" name="commit" />
            </center>
        </div>
    </form>
</div></div>
