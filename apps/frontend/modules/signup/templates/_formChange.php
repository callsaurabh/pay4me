<style type="text/css">
.error{*position:inherit!important; *bottom:0px!important;}
</style>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<?php //use_helper('Form') ?>
<div class="wrapForm2">
    <?php $post_url = "signup/updatePassword";
    if(isset($submitTo) && ($submitTo!="")) {
        $post_url = $submitTo;
    }
    ?>

<form action="<?php echo url_for($submitTo); ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />

  <?php endif; ?>

  <?php if(isset($userId)) {?>
  <input type="hidden" name="userId" value="<?php echo $userId;?>">
  <input type="hidden" name="token" value="<?php echo $token;?>">
  <?php }?>
  <?php echo $form ;?><br/>
      <div class="divBlock">
      <center>
        <input type="submit" value="<?php echo __("Reset Password")?>" class="button" />
        <?php  //echo button_to('Cancel','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('welcome/index').'\''));?>
        </center></div>

      </div>
	  <br/>
	<div class="email_notification"><?php include_partial('global/passwordPolicy') ?></div>


</form>
</div>
 <script>

$(document).ready(function()
{
   var url ='<?php echo url_for('signup/calculateStrength');?>';
   calculatePasswordStrength('sf_guard_user_password', url);
   $('#sf_guard_user_password').after(addVirtualCheckBox());
   enableMultiPasswordKeypad('sf_guard_user_password','sf_guard_user_password',url);


});
 

</script>