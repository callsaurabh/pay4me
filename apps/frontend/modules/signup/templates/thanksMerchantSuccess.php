<div class="innerWrapper">
<?php // use_helper('Form');?>
<?php  include_partial('cms/leftBlock', array('title' => 'Merchant Activation' )) ?>
<div id="wrapperInnerRight">
      <div id="innerImg">
	  <?php echo image_tag('/img/new/img_about_pay4bill.jpg',array('alt'=>'eWallet Register', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
</div>
      <!--<div>
        <h3>Feedback from you.</h3>


        <p>&nbsp;</p>
      </div>-->
	  <?php echo ePortal_pagehead_unAuth('Merchant');?>

      <p>What do you think about Pay4me? Fill this Feedback form and mail us your comments.</p>
      <p>&nbsp;</p>
	   <?php echo form_tag('cms/feedback',array('name'=>'feedback','id'=>'feedbackForm')); ?>


	  <div>


<p>Thank you for Activating Pay4me Merchant Account.</p>
<p>&nbsp;</p>
<p>You will receive an email shortly confirming your account.</p>
<p>&nbsp;</p>
<p>Please follow the link from the confirmation email to activate your Pay4me Merchant
account.</p><p>&nbsp;</p>
<p>Your confirmation email will be sent from <a href="mailto:noreply@pay4me.com">noreply@pay4me.com</a> and should reach you shortly.</p><p>&nbsp;</p>
<p>If you do not receive your confirmation email within 24 hours.</p><p>&nbsp;</p>
<p>please check your "Junk Mail" or contact us at <a href="mailto:support@pay4me.com">support@pay4me.com</a> with your just
signed up username.</p><p>&nbsp;</p>

<p>To ensure that emails from Pay4me reach you,
please add @<a href="http://pay4me.com/">pay4me.com</a> to your 'Safe Senders' list.</p>

<!--
Thank you for signing up with Pay4Me.

Shortly, you will receive an email confirming your registration.

You must follow the link from your confirmation email to activate your Pay4Me eWallet
account - your confirmation email will be sent from <a href="noreply@pay4me.com">noreply@pay4me.com</a> - it should
reach you safely, but do check your 'Junk Mail' if you do not receive this email,
and add @<a href="http://pay4me.com/">pay4me.com</a> to your 'Safe Senders' list.

If you still don't receive it, please email us at <a href="support@pay4me.com">support@pay4me.com</a>
-->


        <div class="clearfix"></div>
      </div>
      </form>
      <p>&nbsp;</p>
    </div>
    </div>









