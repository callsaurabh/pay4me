<?php

/**
 * signup actions.
 *
 * @package    mysfp
 * @subpackage signup
 * @author     Ganesh S. Pasarkar
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
//sfLoader::LoadHelpers(array('ePortal'));
//$this->getContext()->getConfiguration()->loadHelpers('ePortal');
sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));

class signupActions extends sfActions {


    public function executeIndex(sfWebRequest $request) {
    	// Currently this action is disabled and user is redirected to admin index page.
    	// Changed on 13-12-2013.
    	  $this->forward('admin', 'index');
        //call third party api for ip based country
        $ip_to_location_check = sfConfig::get('app_ip_to_location_check');
        $this->applingCountry = false;
        if ($ip_to_location_check) {
            $this->checkIptoLocation();
        }

        $this->form = new CustomUserSignUpForm();

        $dob_date = $request->getPostParameter('dob_date');
        $this->dob_text = $dob_date != "" ? $dob_date : null;
        if ($request->isMethod('post')) {

            $allParams = $request->getPostParameters();
            $allParams['dob'] = $this->dob_text;
            $this->form->bind($allParams);
            if ($this->form->isValid()) {
                $this->forward($this->moduleName, 'QnASignUp');
            }
        }

    }

    private function checkIptoLocation() {
        $ApiObj = new APIHelper();
        $ip = $ApiObj->getClientIpAddress();
        $country_code = $ApiObj->getCountryCodeByIp($ip);
        $this->logMessage("E-wallet signup user IP address is $ip and Country code is $country_code");
        if ($country_code != "") {
            $boolcheck = $ApiObj->checkCountrycodeforEwallet($country_code);
            if (!$boolcheck) {
                $arrDetails =
                        Doctrine::getTable('Country')->getCountryDetailsByCode($country_code);
                if (isset($arrDetails[0]) && isset($arrDetails[0]['country_code'])
                        && $arrDetails[0]['country_code'] != "") {
                    $this->applingCountry = true;
                }
            }
        }
    }
    /* Function to check whether merchant is valid during activation */
  public function executeMerchantExist(sfWebRequest $request) {
    $this->form = new MerchantExistForm();
    if ($request->isMethod('post')) {
      $allParams = $request->getPostParameters();
      $this->form->bind($allParams);
      if ($this->form->isValid()) {
        if ($this->validateMerchantName($request)) {
          $this->forward("signup", "merchantActivation");
        }
      }
    }
    $this->setTemplate("verifyMerchant");
  }
    /* Function to check if the merchant name is valid */
    public function validateMerchantName(sfWebRequest $request){

      $merchant_name = $request->getPostParameter('merchantname');
      $merchant_code = $request->getPostParameter('merchantcode');
      $merchant_key = $request->getPostParameter('merchantkey');

      $merchantNameExist = Doctrine::getTable('merchant')->chkIsMerchantExists($merchant_name);

      if($merchantNameExist[0]['COUNT'] != 0){
        return $this->validateMerchantCode($merchant_code,$merchant_name,$merchant_key);
      }
      else{
        $this->getUser()->setFlash('error', sprintf('You are not associated with Pay4me as merchant'));
        $this->redirect('signup/merchantExist');
      }
    }
    /* Function to check if the merchant Code is valid */
    public function validateMerchantCode($merchantCode,$merchantName,$merchantKey){

     $validateMerchantCode =  Doctrine::getTable('merchant')->isValidMerchantCode($merchantCode,$merchantName);
     if($validateMerchantCode[0]['COUNT'] != 0){

       return $this->validateMerchantKey($merchantCode,$merchantName,$merchantKey);
     }
     else{

       $this->getUser()->setFlash('error', sprintf('Please enter Valid Merchant Code'));
       $this->redirect('signup/merchantExist');
     }
    }
    /* Function to check if the merchant Key is valid */

    public function validateMerchantKey($merchantCode,$merchantName,$merchantKey){

      $validateMerchantKey =  Doctrine::getTable('merchant')->isValidMerchantKey($merchantCode,$merchantName,$merchantKey);
      if($validateMerchantKey){
        return $this->chkMerchantRegistered($merchantName);
      }
      else{
        $this->getUser()->setFlash('error', sprintf('Please enter Valid Merchant Key'));
        $this->redirect('signup/merchantExist');
      }
    }

/*  Function to check if merchant already activated or not */

    public function chkMerchantRegistered($merchantName){

      $merchant_id = Doctrine::getTable('Merchant')->getMerchantIdByName($merchantName);
      $chkMerchant =  Doctrine::getTable('MerchantUser')->chkMerchantCodeExist($merchant_id[0]['id']);
      if($chkMerchant){
        $this->getUser()->setFlash('error', sprintf('You are Already Registered'));
        $this->redirect('signup/merchantExist');
      }
      else {
       return true;
      }
    }

    /* Function for activation form */

     public function executeMerchantActivation(sfWebRequest $request){

     $this->merchantname = $request->getParameter('merchantname');
     $this->applingCountry = false;
     $this->form = new MerchantUserActivationForm(array('name' =>$this->merchantname)); // for default value in activation form

    }

    public function executeSaveMerchant(sfWebRequest $request){

      if ($request->isMethod('post')) {
        $this->applingCountry = false;
        $allParams = $request->getPostParameters();
        $this->merchantname = $allParams['name'];
        $this->form = new MerchantUserActivationForm(array('name' =>$this->merchantname));
        $this->form->bind($allParams);
        if ($this->form->isValid()) {
          $this->forward($this->moduleName, 'QnASignUpMerchant');
        }
      }
      else{
        $this->redirect('signup/merchantExist');
      }
      $this->setTemplate("merchantActivation");
    }

    public function executeCalculateStrength() {
        $strength = PasswordStrength::calculate(
                        $this->getRequestParameter('password'),
                        $this->getRequestParameter('username'),
                        $this->getRequestParameter('byPass')
        );
        return $this->renderText(PasswordStrength::getBar($strength));
    }

    public function executeCreate($request) {
        $arrDetails = array('error' => 0, 'msg' => array('module' => 'signup', 'action' => 'QnASignUp'));

        $this->details = json_encode($arrDetails);
    }

    private function userDetailValid($request, $captchaverify=true) {

        $msg = array();
        $errorFlg = false;
        if (trim($request->getPostParameter('name')) == "") {
            $msg['fname'] = 'Please enter First Name';
            $errorFlg = true;
        } else {
            $msg['fname'] = '';
        }
        if (trim($request->getPostParameter('lname')) == "") {
            $msg['lname'] = 'Please enter Last Name';
            $errorFlg = true;
        } else {
            $msg['lname'] = '';
        }
        if (trim($request->getPostParameter('username')) == "") {
            $msg['uname'] = 'Please enter Username ';
            $errorFlg = true;
        } elseif (!preg_match('/^[a-z\d\_\.]{5,20}$/i', trim($request->getPostParameter('username')))) {
            $msg['uname'] = 'Please enter a valid Username ';
            $errorFlg = true;
        } else if (!preg_match('/^[a-z\d]$/i', substr(trim($request->getPostParameter('username')), -1))) {
            $msg['uname'] = 'Username cannot end with a underscore or dot (.)';
            $errorFlg = true;
        } else if (!preg_match('/^[a-z]$/i', substr(trim($request->getPostParameter('username')), 0, 1))) {
            $msg['uname'] = 'Username cannot start with a number,underscore or dot (.)';
            $errorFlg = true;
        } else {
            $msg['uname'] = '';
        }
        if (trim($request->getPostParameter('password')) == "") {
            $msg['password'] = 'Please enter Password';
            $errorFlg = true;
        } else {
            $msg['password'] = '';
        }
        if (trim($request->getPostParameter('cpassword')) == "") {
            $msg['cpassword'] = 'Please enter Confirm Password';
            $errorFlg = true;
        } else if (trim($request->getPostParameter('password')) != trim($request->getPostParameter('cpassword'))) {
            $msg[] = 'Your confirmed password does not match the entered password';
            $errorFlg = true;
        } else {
            $msg['cpassword'] = '';
        }
        if (trim($request->getPostParameter('dob')) == "") {
            $msg['dob'] = 'Please enter Date of Birth';
            $errorFlg = true;
        } else {
            $msg['dob'] = '';
        }

        if (trim($request->getPostParameter('email')) == "") {
            $msg['email'] = 'Please enter Email<br />';
            $errorFlg = true;
            //}else if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $request->getPostParameter('email'))) {
        } else if (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $request->getPostParameter('email'))) {
            $msg['email'] = 'Please enter valid Email';
            $errorFlg = true;
        } else {
            $msg['email'] = '';
        }
        if (trim($request->getPostParameter('address')) == "") {
            $msg['address'] = 'Please enter Address';
            $errorFlg = true;
        } else if (trim($request->getPostParameter('address')) != "") {
            $len = strlen(trim($request->getPostParameter('address')));
            if ($len > 255) {
                $msg['address'] = 'Please enter valid address';
                $errorFlg = true;
            } else {
                $msg['address'] = '';
            }
        } else {
            $msg['address'] = '';
        }

        if (trim($request->getPostParameter('mobileno')) == "") {
            $msg['mobileno'] = 'Please enter Mobile No.';
            $errorFlg = true;
        } else if (trim($request->getPostParameter('mobileno')) != "") {
            if (!preg_match("/^([+]{1})([0-9]{10,15})$/", $request->getPostParameter('mobileno'))) {
                $msg['mobileno'] = 'Please enter valid Mobile No.';
                $errorFlg = true;
            } else {
                $msg['mobileno'] = '';
            }
        }

        if (trim($request->getPostParameter('workphone')) != "") {///  ^(\+)(\d){10,14}?$/
            if (!preg_match("/^([+]{1})([0-9]{10,15})$/", $request->getPostParameter('workphone'))) {
                $msg['workphone'] = 'Please enter valid Work Phone.' . $request->getPostParameter('workphone') . "---";
                $errorFlg = true;
            } else {
                $msg['workphone'] = '';
            }
        } else {
            $msg['workphone'] = '';
        }
        if (settings::isMultiCurrencyOn()) {
            if (trim($request->getPostParameter('currency_id')) == 0) {
                $msg['currency_id'] = 'Please select Currency';
                $errorFlg = true;
            }
        }

        if (trim($request->getPostParameter('captcha')) == "" && $captchaverify) {
            $msg['captcha'] = 'Please enter Verification Code';
            $errorFlg = true;
        } else if (trim($request->getPostParameter('captcha')) != "" && $captchaverify) {
            $hashAlgo = sfConfig::get('app_sf_crypto_captcha_hash_algo');
            $hashedPwd = hash($hashAlgo, strtolower($request->getPostParameter('captcha'))); //FS#29346
            $getCaptcha = $this->getUser()->getAttribute('captcha_code', null, 'captcha');
            if ($hashedPwd != $getCaptcha) {
                $msg['captcha'] = 'Verification Code doesn\'t match with entered code';
                $errorFlg = true;
            } else {
                $msg['captcha'] = '';
            }
        }
        if (!$errorFlg) {
            $emailObj = Doctrine::getTable('UserDetail')->chkEmailId(trim($request->getPostParameter('email')), '');
            if ($emailObj > 0) {
                $msg['email'] = 'E-mail Address already exists';
                $errorFlg = true;
            } else {
                $msg['email'] = '';
            }
            if (!EpPasswordPolicyManager::checkPasswordComplexity($request->getPostParameter('password')) && $captchaverify) {
                $msg['password'] = ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.';
                $errorFlg = true;
            } else {
                $msg['password'] = '';
            }
            $username_count = Doctrine::getTable('sfGuardUser')->chk_username(trim($request->getPostParameter('username')));
            if ($username_count != 0) {
                $msg['uname'] = 'Username already exists';
                $errorFlg = true;
            } else {
                $msg['uname'] = '';
            }
        }

        if ($errorFlg) {
            return array('error' => 1, 'msg' => $msg);
        } else {
            return array('error' => 0, 'msg' => array('module' => 'signup', 'action' => 'QnASignUp'));
        }
    }

    private function merchantDetailValid($request, $captchaverify=true) {

      $msg = array();
      $errorFlg = false;
      if (trim($request->getPostParameter('name')) == "") {
        $msg['fname'] = 'Please enter First Name';
        $errorFlg = true;
      } else {
        $msg['fname'] = '';
      }
      if (trim($request->getPostParameter('username')) == "") {
        $msg['uname'] = 'Please enter Username ';
        $errorFlg = true;
      } elseif (!preg_match('/^[a-z\d\_\.]{5,20}$/i', trim($request->getPostParameter('username')))) {
        $msg['uname'] = 'Please enter a valid Username ';
        $errorFlg = true;
      } else if (!preg_match('/^[a-z\d]$/i', substr(trim($request->getPostParameter('username')), -1))) {
        $msg['uname'] = 'Username cannot end with a underscore or dot (.)';
        $errorFlg = true;
      } else if (!preg_match('/^[a-z]$/i', substr(trim($request->getPostParameter('username')), 0, 1))) {
        $msg['uname'] = 'Username cannot start with a number,underscore or dot (.)';
        $errorFlg = true;
      } else {
        $msg['uname'] = '';
      }
      if (trim($request->getPostParameter('password')) == "") {
        $msg['password'] = 'Please enter Password';
        $errorFlg = true;
      } else {
        $msg['password'] = '';
      }
      if (trim($request->getPostParameter('cpassword')) == "") {
        $msg['cpassword'] = 'Please enter Confirm Password';
        $errorFlg = true;
      } else if (trim($request->getPostParameter('password')) != trim($request->getPostParameter('cpassword'))) {
        $msg[] = 'Your confirmed password does not match the entered password';
        $errorFlg = true;
      } else {
        $msg['cpassword'] = '';
      }
      if (trim($request->getPostParameter('email')) == "") {
        $msg['email'] = 'Please enter Email<br />';
        $errorFlg = true;
        //}else if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $request->getPostParameter('email'))) {
      } else if (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $request->getPostParameter('email'))) {
        $msg['email'] = 'Please enter valid Email';
        $errorFlg = true;
      } else {
        $msg['email'] = '';
      }
      if (trim($request->getPostParameter('captcha')) == "" && $captchaverify) {
        $msg['captcha'] = 'Please enter Verification Code';
        $errorFlg = true;
      } else if (trim($request->getPostParameter('captcha')) != "" && $captchaverify) {
        $hashAlgo = sfConfig::get('app_sf_crypto_captcha_hash_algo');
        $hashedPwd = hash($hashAlgo, strtolower($request->getPostParameter('captcha'))); //FS#29346
        $getCaptcha = $this->getUser()->getAttribute('captcha_code', null, 'captcha');
        if ($hashedPwd != $getCaptcha) {
          $msg['captcha'] = 'Verification Code doesn\'t match with entered code';
          $errorFlg = true;
        } else {
          $msg['captcha'] = '';
        }
      }
      if (!$errorFlg) {
        $emailObj = Doctrine::getTable('UserDetail')->chkEmailId(trim($request->getPostParameter('email')), '');
        if ($emailObj > 0) {
          $msg['email'] = 'E-mail Address already exists';
          $errorFlg = true;
        } else {
          $msg['email'] = '';
        }
        if (!EpPasswordPolicyManager::checkPasswordComplexity($request->getPostParameter('password')) && $captchaverify) {
          $msg['password'] = ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.';
          $errorFlg = true;
        } else {
          $msg['password'] = '';
        }
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username(trim($request->getPostParameter('username')));
        if ($username_count != 0) {
          $msg['uname'] = 'Username already exists';
          $errorFlg = true;
        } else {
          $msg['uname'] = '';
        }
      }

      if ($errorFlg) {
        return array('error' => 1, 'msg' => $msg);
      } else {
        return array('error' => 0, 'msg' => array('module' => 'signup', 'action' => 'QnASignUpMerchant'));
      }
    }
    public function executeThanks(sfWebRequest $request) {

    }

    public function executeThanksMerchant(sfWebRequest $request) {

    }
    public function executeUnblockMessege(sfWebRequest $request) {

    }

    protected function saveUserDetail($request, $userId) {
        $userDetail = new UserDetail();
        $userDetail->setUserId($userId);
        if (trim($request->getPostParameter('name')) != "")
            $userDetail->setName(trim($request->getPostParameter('name')));
        if (trim($request->getPostParameter('lname')) != "")
            $userDetail->setLastName(trim($request->getPostParameter('lname')));
        if ($request->getPostParameter('dob_date') != "")
            $date = date('Y-m-d', strtotime($request->getPostParameter('dob_date')));
        $userDetail->setDob($date);
        if (trim($request->getPostParameter('address')) != "")
            $userDetail->setAddress(trim($request->getPostParameter('address')));
        if (trim($request->getPostParameter('email')) != "")
            $userDetail->setEmail(trim($request->getPostParameter('email')));
        if (trim($request->getPostParameter('mobileno')) != "")
            $userDetail->setMobileNo(trim($request->getPostParameter('mobileno')));
        if (trim($request->getPostParameter('workphone')) != "")
            $userDetail->setWorkPhone(trim($request->getPostParameter('workphone')));
        if (trim($request->getPostParameter('sms')) != "")
            $userDetail->setSendSms(trim($request->getPostParameter('sms')));
        $userDetail->setUserStatus(0);
        $userDetail->setEwalletType("ewallet");
        $userDetail->save();
    }

    protected function saveMerchantDetail($request, $userId) {
      $userDetail = new UserDetail();
      $userDetail->setUserId($userId);
      if (trim($request->getPostParameter('name')) != "")
        $userDetail->setName(trim($request->getPostParameter('name')));
      if (trim($request->getPostParameter('email')) != "")
        $userDetail->setEmail(trim($request->getPostParameter('email')));
      if (trim($request->getPostParameter('sms')) != "")
        $userDetail->setSendSms(trim($request->getPostParameter('sms')));
      $userDetail->setUserStatus(0);
      //$userDetail->setEwalletType("ewallet");
      $userDetail->save();
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id')),
                sprintf('Object sf_guard_user does not exist (%s).', $request->getParameter('id')));
        $this->form = new sfGuardUserForm($sf_guard_user);
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id')),
                sprintf('Object sf_guard_user does not exist (%s).', $request->getParameter('id')));
        $this->form = new sfGuardUserForm($sf_guard_user);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $activation_url = $request->getParameter('activation_url');
        if ($request->getParameter('password')) {
            $password = $request->getParameter('password');
        } else {
            $password = '';
        }
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendConfirmationEmail($userid, $subject, $partialName, $activation_url, $password);
        return $this->renderText($mailInfo);
    }

    public function executeActivate(sfWebRequest $request) {
        // $account_number = $this->generateAccountNumber();
        //   print $master_account_id =   $walletObj->createMasterAccount($account_name, $account_number, $type);exit;
        # Initilize Variables
        $userid = $request->getParameter('i');
        $userToken = $request->getParameter('t');
        $currencyCode = $request->getParameter('c');

        $user_details = Doctrine::getTable('sfGuardUser')->find(array($userid));
        $last_login = $user_details->getLastLogin();
        $sfguardUserId = $user_details->getId();
        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userid);
        if($userDetails[0]['ewallet_type'] != 'openid')
            $userPwdSalt = $user_details->getSalt();
        else
            $userPwdSalt = $user_details->getId();
        $updatedAt = $user_details->getUpdatedAt();

        #Generate Activation Token For Activation Link
        $hashAlgo = sfConfig::get('app_user_token_hash_algo');
        //$userPwdSalt = $user_details->getSalt();
        //$updatedAt = $user_details->getUpdatedAt();


        $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);

        $detailObj = $user_details->getUserDetail()->getFirst();
        //check last login
        if ($last_login == "") {
            //check is user having account id if yes it means user is blocked, and came for unblock, else assigin user an account id
            $accNo = $detailObj->getMasterAccountId();
            if (empty($accNo)) {
                $account_name = $detailObj->getName() . " " . $detailObj->getLastName();
                $userAccounObj = new UserAccountCollection;
                if ($currencyCode != '')
                    $currencyId = base64_decode($currencyCode);
                else
                    $currencyId = 1;
                $master_account_id = $userAccounObj->addAccount($account_name, $userid, $currencyId);
                if (isset($master_account_id) && ($master_account_id != "")) {
                    $detailObj->setMasterAccountId($master_account_id);
                }
            }
        }
        if ($userid == $sfguardUserId && $userToken == $activationToken) {
            $user_details->setIsActive(true);

            // $detailObj->setUserStatus(4);
            $detailObj->setUserStatus(1);
            //if user having account id , it means user came for unblock, so we put last login in conditions.
            // by that when user go for login , it will be redirected to change password and security answers
            if (empty($accNo))
                $user_details->setLastLogin(date('Y-m-d H:i:s'));
            $user_details->save();
            if (Settings::isEwalletPinActive()) {
                $pinObj = new pin();
                $userId = $detailObj->getUserId();
                $name = $detailObj->getName();
                $email = $detailObj->getEmail();
                $pinObj->getPin($userId, $name, $email); //userId ,name, email address
            }
            //$this->getUser()->setFlash('notice', sprintf('Your email has been verified. Now your account is queued for approval. You’ll be notified when an action is recorded on your application.'));
            $this->getUser()->setFlash('notice', sprintf('Your account activated successfully.'));
            $this->redirect('@homepage');
        } else {
            $this->redirect('@homepage');
        }
    }

    /* Function for activating merchant account
     * hit when merchant clicks on the link given in the mail during activation */

    public function executeActivateMerchant(sfWebRequest $request) {

        $userid = $request->getParameter('i');
        $userToken = $request->getParameter('t');
        //$currencyCode = $request->getParameter('c');

        $user_details = Doctrine::getTable('sfGuardUser')->find(array($userid));
        $last_login = $user_details->getLastLogin();
        $sfguardUserId = $user_details->getId();
        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userid);
        $userPwdSalt = $user_details->getSalt();
        $updatedAt = $user_details->getUpdatedAt();
        #Generate Activation Token For Activation Link
        $hashAlgo = sfConfig::get('app_user_token_hash_algo');
        //$userPwdSalt = $user_details->getSalt();
        //$updatedAt = $user_details->getUpdatedAt();
        $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);
        $detailObj = $user_details->getUserDetail()->getFirst();

        if ($userid == $sfguardUserId && $userToken == $activationToken) {
            $user_details->setIsActive(true);
            $detailObj->setUserStatus(1);
          //  if (empty($accNo))
            $user_details->setLastLogin(date('Y-m-d H:i:s'));
            $user_details->save();
            $this->getUser()->setFlash('notice', sprintf('Your merchant account activated successfully.'));
            $this->redirect('@homepage');
        } else {
            $this->redirect('@homepage');
        }
    }


    public function generateAccountNumber() {
        do {
            $account_number = $this->getRandomNumber();
            $walletObj = new EpAccountingManager;
            $duplicacy_account_number = $walletObj->chkAccountNumber($account_number);
        } while ($duplicacy_account_number);
        return $account_number;
    }

    public function getRandomNumber() {
        return rand(1, 5) . '' . rand('100', '1000') . '' . rand('100', '10000');
    }

    public function executeChkUserAvailability(sfWebRequest $request) {

        //if is not xmlhttprequest redirect to home page
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('@homepage');
        }
        $name = "";
        $lname = "";
        if (trim($request->getPostParameter('name')) != "")
            $name = $request->getPostParameter('name');
        if (trim($request->getPostParameter('lname')) != "")
            $lname = $request->getPostParameter('lname');
        if (trim($request->getPostParameter('username')) != "")
            $username = $request->getPostParameter('username');
        $username = $request->getPostParameter('username');
        $suggestions = array();
        if ($this->isvalidusername($username)) {
            $result = "Username available";
            $status = true;
        } else {
            $result = "Username not available";
            $suggestions = $this->usernamesuggestion($username, $name, $lname);
            $status = false;
        }
        $this->result = json_encode(array('status' => $status, 'suggestions' => $suggestions, 'msg' => $result));
    }

    private function usernamesuggestion($username, $name, $lname) {
        //username + first letter of name + first letter of last name
        //check first name
        if (!preg_match("/^([a-zA-Z]+)$/", trim($name))) {
            $name = "";
        }
        //check last name
        if (!preg_match("/^([a-zA-Z]+)$/", trim($lname))) {
            $lname = "";
        }
        $suggestionOne = "";
        if ($name != "" && $lname != "") {
            $suggestionOne = $this->getSuggestions($username . substr($name, 0, 1) . substr($lname, 0, 1));
        }
        //username +  digits
        $suggestionTwo = $this->getSuggestions($username . mt_rand(1, 99));
        //username +  lastname
        $suggestionThree = "";
        if ($lname != "") {
            $suggestionThree = $this->getSuggestions($username . $lname);
        }
        //username +  name
        $suggestionFour = "";
        if ($name != "") {
            $suggestionFour = $this->getSuggestions($username . $name);
        }
        $suggestionFive = "";
        $suggestionSix = "";
        $suggestionSeven = "";
        if ($suggestionOne == '' && $suggestionTwo == '' &&
                $suggestionThree == '' && $suggestionFour == '') {
            //first name + last name + random number

            if ($name != "" && $lname != "") {
                $suggestionFive = $this->getSuggestions($name . $lname);
            }
            //username + first letter of name + random number

            if ($name != "") {
                $suggestionSix = $this->getSuggestions($username . substr($name, 0, 1));
            }
            //username + first letter of last name + random number

            if ($lname != "") {
                $suggestionSeven = $this->getSuggestions($username . substr($lname, 0, 1));
            }
        }
        return array($suggestionOne, $suggestionTwo, $suggestionThree,
            $suggestionFour, $suggestionFive, $suggestionSix, $suggestionSeven);
    }

    private function getSuggestions($name) {
        $suggestion = $name;
        $suggestionStatus = true;
        if (!$this->isvalidusername($suggestion)) {
            $suggestionStatus = false;
            for ($i = 0; $i < 10; $i++) {
                $suggestion = $suggestion . mt_rand(1, 99);
                if ($this->isvalidusername($suggestion)) {
                    $suggestionStatus = true;
                    break;
                }
            }
        }
        if (!$suggestionStatus) {
            $suggestion = '';
        }
        return $suggestion;
    }

    private function isvalidusername($username) {
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
        if ($username_count == 0) {
            return true;
        } else {
            return false;
        }
    }
/* Function for Question and Answers while activating a merchant */

    public function executeQnASignUpMerchant(sfWebRequest $request) {

    if ($request->hasParameter('security_questions')) {

            $security_question = $request->getParameter('security_questions');
            if (!array_key_exists('user_id', $security_question)) {                       //Currently not goes in the condition
                $arrDetails = $this->merchantDetailValid($request, false);
                if ($arrDetails['error']) {
                    $this->getUser()->setFlash('error', sprintf('There is some problem with your browser, Please try again'));
                    $this->redirect('signup/index');
                }
            }
        }
      # Initilize Variables
      $UserId = '';
      $this->captcha = '';
      $this->cpassword = '';
      if (trim($request->getPostParameter('name')) != "")
        $this->name = $request->getPostParameter('name');
              if (trim($request->getPostParameter('username')) != "")
              $this->username = $request->getPostParameter('username');
              if ($request->isMethod('put')) {
              if (trim($request->getPostParameter('password')) != "")
                  $this->password = base64_decode($request->getPostParameter('password'));
                  $this->password = base64_encode($this->password);
            }else {
                  if (trim($request->getPostParameter('password')) != "")
                    $this->password = base64_encode($request->getPostParameter('password'));
                  }
                  $this->cpassword = $this->password;
                        if (trim($request->getPostParameter('email')) != "")
                          $this->email = $request->getPostParameter('email');
                         // if (trim($request->getPostParameter('address')) != "")
                          if (trim($request->getPostParameter('captcha')) != "")
                            $this->captcha = $request->getPostParameter('captcha');
                          if (trim($request->getPostParameter('security_questions[user_id]')) != "")
                            $UserId = $request->getPostParameter('security_questions[user_id]');


                          $this->form = new AddQnAForm();


                          if ($request->isMethod('put')) {

                            if ($UserId == '') {
                              if ($this->password != '') {
                                $password = base64_decode($this->password);
                              }
                              #Create New Merchant User
                              $sfUser = new sfGuardUser();
                              $userName = trim($this->username);
                              // $password = PasswordHelper::generatePassword();
                              $sfUser->setUsername($userName);
                              $sfUser->setPassword($password);
                              $sfUser->setIsActive(false);
                              $sfUser->save();
                              //insert userid and password in EpPasswordPolicy table
                              EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(), $sfUser->getPassword());
                              $sfUserId = $sfUser->getId();

                              #create a User Group
                              $userTypeToCreate = sfConfig::get('app_pfm_role_merchant');


                              $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate);
                              $sfGroupId = $sfGroupDetails->getFirst()->getId();
                              $sfGuardUsrGrp = new sfGuardUserGroup();
                              $sfGuardUsrGrp->setGroupId($sfGroupId);
                              $sfGuardUsrGrp->setUserId($sfUserId);
                              $sfGuardUsrGrp->save();

                              #Save Merchant User Detail
                              $this->saveMerchantDetail($request, $sfUserId);

                              $UserId = $sfUserId;

                              //Merchant userId save Code starts here
                              $merchant_id = Doctrine::getTable('Merchant')->getMerchantIdByName($request->getPostParameter('name'));
                              $id = $merchant_id[0]['id'];

                              $merchant_user = new MerchantUser();
                              $merchant_user->setUserId($UserId);
                              $merchant_user->setMerchantId($id);
                              $merchant_user->save();
                            }

                            $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($UserId), sprintf('Object sf_guard_user does not exist (%s).', array($UserId)));
                            $this->form = new AddQnAForm($sf_guard_user);
                            $this->form->bind($request->getParameter($this->form->getName()));

                            if ($this->form->isValid()) {
                            #Begin Code To Send Confirmation Mail To The Merchant Owner
                            #Generate Activation Token For Activation Link

                            $hashAlgo = sfConfig::get('app_user_token_hash_algo');
                            $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($UserId));
      $userPwdSalt = $sfGuardUserDetail->getSalt();
      $updatedAt = $sfGuardUserDetail->getUpdatedAt();
      $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);
      $subject = "Merchant Confirmation Mail";
      $partialName = 'account_detailsmerchant';
      sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
      $activation_url = url_for('signup/activateMerchant', true) . '?i=' . $UserId . '&t=' . $activationToken;
                          $taskId = EpjobsContext::getInstance()->addJob('SendMailConfirmation', $this->moduleName . "/sendEmail", array('userid' => $UserId, 'subject' => $subject, 'partialName' => $partialName, 'activation_url' => $activation_url));
                          $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                          #End Code To Send Confirmation Mail To The Merchant Owner
                          $this->qnaCount = Settings::getNumberOfQuestion();
                              for ($i = 0; $i < $this->qnaCount; $i++) {
                              $questionAns = new SecurityQuestionsAnswers();
                              $questionAns->setUserId($UserId);
                              $questionAns->setSecurityQuestionId($request->getPostParameter('sf_guard_user[qna' . $i . '][security_question_id]'));
                              $questionAns->setAnswer(trim($request->getPostParameter('sf_guard_user[qna' . $i . '][answer]')));
                              $questionAns->save();
      }
     // Log successfull create new account
                    $this->logMerchantCreateNewAccountAuditDetails($UserId,$userName);
                    #End Code To Save SecurityQuestion Answers
                    $this->redirect('signup/thanksMerchant');
                }
            }
            $this->UserId = $UserId;
            $this->setTemplate("QnASignUpMerchant");
        }

    public function executeQnASignUp(sfWebRequest $request) {

        if ($request->hasParameter('security_questions')) {

            $security_question = $request->getParameter('security_questions');
            if (!array_key_exists('user_id', $security_question)) {
                $arrDetails = $this->userDetailValid($request, false);
                if ($arrDetails['error']) {
                    $this->getUser()->setFlash('error', sprintf('There is some problem with your browser, Please try again'));
                    $this->redirect('signup/index');
                }
            }
        }
        # Initilize Variables
        $UserId = '';
        $this->mobileno = '';
        $this->workphone = '';
        $this->sms = '';
        $this->address = '';
        $this->captcha = '';
        $this->cpassword = '';
        $this->currency_id = '';
        if (trim($request->getPostParameter('name')) != "")
            $this->name = $request->getPostParameter('name');
        if (trim($request->getPostParameter('lname')) != "")
            $this->lname = $request->getPostParameter('lname');
        if (trim($request->getPostParameter('username')) != "")
            $this->username = $request->getPostParameter('username');
        if ($request->isMethod('put')) {
            if (trim($request->getPostParameter('password')) != "")
                $this->password = base64_decode($request->getPostParameter('password'));
            $this->password = base64_encode($this->password);
        }else {
            if (trim($request->getPostParameter('password')) != "")
                $this->password = base64_encode($request->getPostParameter('password'));
        }
        $this->cpassword = $this->password;
        if ($request->getPostParameter('dob_date') != "")
            $this->dob_date = $request->getPostParameter('dob_date');
        if (trim($request->getPostParameter('email')) != "")
            $this->email = $request->getPostParameter('email');
        if (trim($request->getPostParameter('address')) != "")
            $this->address = $request->getPostParameter('address');
        if (trim($request->getPostParameter('mobileno')) != "")
            $this->mobileno = $request->getPostParameter('mobileno');
        if (trim($request->getPostParameter('workphone')) != "")
            $this->workphone = $request->getPostParameter('workphone');
        //if(trim($request->getPostParameter('sms')) != "")
        //$this->sms = $request->getPostParameter('sms');
        if (trim($request->getPostParameter('captcha')) != "")
            $this->captcha = $request->getPostParameter('captcha');

        if (trim($request->getPostParameter('security_questions[user_id]')) != "")
            $UserId = $request->getPostParameter('security_questions[user_id]');

        $this->chkImage = $request->getPostParameter('chkImage');
        for ($i = 1; $i <= 3; ++$i) {
            $chkPar = "chkDoc" . $i;
            $chkdoc[$i] = $request->getPostParameter($chkPar);
        }

        $this->chkDoc = base64_encode(serialize($chkdoc));


        $this->tmpImgName = $request->getPostParameter('tmpImgName');
        $this->tmpDocName = base64_encode($request->getPostParameter('tmpDocName'));

        if (settings::isMultiCurrencyOn()) {
            if (trim($request->getPostParameter('currency_id')) != 0)
                $this->currency_id = $request->getPostParameter('currency_id');
        }


        $this->form = new AddQnAForm();


        if ($request->isMethod('put')) {


            if ($UserId == '') {
                if ($this->password != '') {
                    $password = base64_decode($this->password);
                }
                #Create New eWallet User
                $sfUser = new sfGuardUser();
                $userName = trim($this->username);
                // $password = PasswordHelper::generatePassword();
                $sfUser->setUsername($userName);
                $sfUser->setPassword($password);
                $sfUser->setIsActive(false);
                $sfUser->save();
                //insert userid and password in EpPasswordPolicy table
                EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(), $sfUser->getPassword());
                $sfUserId = $sfUser->getId();

                #create a User Group
                $userTypeToCreate = sfConfig::get('app_pfm_role_ewallet_user');

                $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate);
                $sfGroupId = $sfGroupDetails->getFirst()->getId();
                $sfGuardUsrGrp = new sfGuardUserGroup();
                $sfGuardUsrGrp->setGroupId($sfGroupId);
                $sfGuardUsrGrp->setUserId($sfUserId);
                $sfGuardUsrGrp->save();

                #Save User Detail
                $this->saveUserDetail($request, $sfUserId);
                $UserId = $sfUserId;
            }
            $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($UserId), sprintf('Object sf_guard_user does not exist (%s).', array($UserId)));
            $this->form = new AddQnAForm($sf_guard_user);
            $this->form->bind($request->getParameter($this->form->getName()));

            if ($this->form->isValid()) {

                #Begin Code To Send Confirmation Mail To The eWallet Owner
                #Generate Activation Token For Activation Link
                $hashAlgo = sfConfig::get('app_user_token_hash_algo');
                $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($UserId));
                $userPwdSalt = $sfGuardUserDetail->getSalt();
                $updatedAt = $sfGuardUserDetail->getUpdatedAt();

                $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);

                $subject = "eWallet Confirmation Mail";
                $partialName = 'account_details';
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

                if (settings::isMultiCurrencyOn()) {
                    $activation_url = url_for('signup/activate', true) . '?i=' . $UserId . '&t=' . $activationToken . '&c=' . base64_encode($this->currency_id);
                } else {
                    $activation_url = url_for('signup/activate', true) . '?i=' . $UserId . '&t=' . $activationToken;
                }

                $taskId = EpjobsContext::getInstance()->addJob('SendMailConfirmation', $this->moduleName . "/sendEmail", array('userid' => $UserId, 'subject' => $subject, 'partialName' => $partialName, 'activation_url' => $activation_url));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                #End Code To Send Confirmation Mail To The eWallet Owner
                //  $this->form->save();
                #Begin Code To Save SecurityQuestion Answers
                //$listOfQuestions = Doctrine::getTable('SecurityQuestion')->findAll(Doctrine::HYDRATE_ARRAY);
                //$this->qnaCount = count($listOfQuestions);
                $this->qnaCount = Settings::getNumberOfQuestion();


                for ($i = 0; $i < $this->qnaCount; $i++) {
                    $questionAns = new SecurityQuestionsAnswers();
                    $questionAns->setUserId($UserId);
                    $questionAns->setSecurityQuestionId($request->getPostParameter('sf_guard_user[qna' . $i . '][security_question_id]'));
                    $questionAns->setAnswer(trim($request->getPostParameter('sf_guard_user[qna' . $i . '][answer]')));
                    $questionAns->save();
                }
                 // Log successfull create new account
                $this->logEwalletCreateNewAccountAuditDetails($UserId,$userName);
                #End Code To Save SecurityQuestion Answers
                $this->redirect('signup/thanks');
            }
        }
        $this->UserId = $UserId;
        $this->setTemplate("QnASignUp");
    }


    /*
     * Function to log audit for successful create new account
     */
    public function logEwalletCreateNewAccountAuditDetails($id,$userName){
      //Log audit Details
      //$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_CREATE_NEW_ACCOUNT,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_CREATE_ACCOUNT, array('username'=>$userName)),
        null,$id,$userName);
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logMerchantCreateNewAccountAuditDetails($id,$userName){
      //Log audit Details
      //$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
      $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_SECURITY,
          EpAuditEvent::$SUBCATEGORY_SECURITY_CREATE_NEW_MERCHANT_ACCOUNT,
          EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_CREATE_MERCHANT_ACCOUNT, array('username'=>$userName)),
          null,$id,$userName);
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function executeUnblockAccount(sfWebRequest $request) {

    }

    public function executeActivateAccount(sfWebRequest $request) {

        # Initilize Variables
        if (trim($request->getPostParameter('username')) != "")
            $this->username = $request->getPostParameter('username');
        if (trim($request->getPostParameter('loadAmount')) != "")
            $this->loadAmount = $request->getPostParameter('loadAmount');
        if ($request->getPostParameter('spentAmount') != "")
            $this->spentAmount = $request->getPostParameter('spentAmount');

        if ($this->validateUnblockAccount($request)) {

            # Check Whether User Name is already exist or not
            $sfUser = Doctrine::getTable('sfGuardUser')->chkBlockUsername($this->username);

            if (count($sfUser) > 0) {
                if ($sfUser[0]['is_active']) {
                    $sfUserId = $sfUser[0]['id'];

                    $userDetailRecordObj = Doctrine::getTable('UserDetail')->getUserDetailsAndAcId($sfUserId);


                    if (count($userDetailRecordObj) > 0) {
                        if ($userDetailRecordObj[0]['failed_attempt'] >= sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
                            $masterAccountId = $userDetailRecordObj[0]['master_account_id'];
                            $userId = $userDetailRecordObj[0]['id'];

                            #Begin Code To Validate Amount Of Last Load and Amount Of Last Spent
                            #Get Master Account Id Of the User
                            if ($this->loadAmount != '') {
                                $entry_type = 'credit';
                                $accountDetail = Doctrine::getTable('EpMasterLedger')->getLastAmount($sfUserId, $masterAccountId, $entry_type);

                                $amount = $accountDetail['amount'];
                                $loadAmt = ($this->loadAmount) * 100;
                                if ($amount != $loadAmt) {
                                    $this->getUser()->setFlash('error', sprintf('Please enter valid Amount Of Last Load'));
                                    $this->forward('signup', 'unblockAccount');
                                }
                            }
                            if ($this->spentAmount != '') {
                                $entry_type = 'debit';
                                $accountDetail = Doctrine::getTable('EpMasterLedger')->getLastAmount($sfUserId, $masterAccountId, $entry_type);
                                $amount = $accountDetail['amount'];
                                $spentAmt = ($this->spentAmount) * 100;
                                if ($amount != $spentAmt) {
                                    $this->getUser()->setFlash('error', sprintf('Please enter valid Amount Of Last Spent'));
                                    $this->forward('signup', 'unblockAccount');
                                }
                            }




                            #End Code To Validate Amount Of Last Load and Amount Of Last Spent
                            #Begin Code To Send Mail To The eWallet Owner


                            $user = Doctrine::getTable('UserDetail')->find($userId);
                            $user->setFailedAttempt(0);
                            $user->save();

                            #Resetting password
                            $pass_word = PasswordHelper::generatePassword();

                            $sfGuardUser = Doctrine::getTable('sfGuardUser')->find(array($sfUserId));
                            $sfGuardUser->setIsActive(0);
                            $sfGuardUser->setPassword($pass_word);
                            $sfGuardUser->setLastLogin(NULL);
                            $sfGuardUser->save();

                            #Generate Activation Token For Activation Link
                            $hashAlgo = sfConfig::get('app_user_token_hash_algo');
                            $userPwdSalt = $sfGuardUser->getSalt();
                            $updatedAt = $sfGuardUser->getUpdatedAt();

                            $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);


                            $subject = "eWallet Unblock Account Mail";
                            $partialName = 'ewallet_status';
                            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                            $activation_url = url_for('signup/activate', true) . '?i=' . $sfUserId . '&t=' . $activationToken;
                            $taskId = EpjobsContext::getInstance()->addJob('SendEwalletUnblockMail', "signup/sendEmail", array('userid' => $sfUserId, 'subject' => $subject, 'partialName' => $partialName, 'activation_url' => $activation_url, 'password' => $pass_word));
                            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                            #End Code To Send Mail To The eWallet Owner
                            $this->getUser()->setFlash('notice', sprintf('User has been unblocked successfully.'));
                            $this->redirect('signup/unblockMessege');
                        } else {
                            $this->getUser()->setFlash('error', sprintf('Not a blocked user.'));
                            $this->forward('signup', 'unblockAccount');
                        }
                    } else {
                        $this->getUser()->setFlash('error', sprintf('Not a eWallet user.'));
                        $this->forward('signup', 'unblockAccount');
                    }
                } else {
                    $this->getUser()->setFlash('error', sprintf('Not a active user'));
                    $this->forward('signup', 'unblockAccount');
                }
            } else {
                $this->getUser()->setFlash('error', sprintf('Username does not exists'));
                $this->forward('signup', 'unblockAccount');
            }
        }
    }

    protected function validateUnblockAccount($request) {
        $msg = array();
        if (trim($request->getPostParameter('username')) == "") {
            $msg[] = 'Please enter User Name ';
        }
        if (trim($request->getPostParameter('loadAmount')) == "" && trim($request->getPostParameter('spentAmount')) == "") {
            $msg[] = 'Please enter either Amount of Last Load or Amount of Last Spent';
        }

        if (count($msg) > 0) {
            $msg = implode($msg, ', ');
            $this->getUser()->setFlash('error', sprintf($msg));
            return false;
        }
        return true;
    }

    public function executeTest() {

        //        if(Settings::isEwalletPinActive())
        //        {
        //            $pinObj=new pin();
        //            $pinObj->getPin('3370','prachi','prachi.nigam@tekmindz.com');//userId ,name, email address
        //        }
//                    $pinObj=new pin();
//                    $pinObj->isActive();
        $pinObj = new pin();
        $pinObj->sendGracePeriodMail();
    }

    /**
     * @Function   whypay4me
     * @author     Rajesh Yadav
     * @version
     * @purpose : show pop window to show the why pay4me discription [CR047]
      @date : 10/03/2010
     */
    public function executeWhypay4me() {

        $this->setLayout(false);
    }

    /* Forgot Password
     */

    public function executeForgotPassword(sfWebRequest $request) {
        $this->form = new ForgotPasswordGenForm();
        $this->securityQuestion = false;
        if ($request->getMethod() == "POST") {
            $this->form->bind($request->getParameter("forgot"));
            if ($this->form->isValid()) {
                $this->securityQuestion = true;
                //generate form with security question
                $this->form = new ForgotPasswordGenForm("", array('security_question' => true));
            }
        }
    }

    public function executeCreateForgotPassword(sfWebRequest $request) {
        if ($request->getMethod() == "POST") {
            $this->form = new ForgotPasswordGenForm("", array('security_question' => true));
            $this->securityQuestion = true;
            $this->form->bind($request->getParameter("forgot"));
            $arrData = $request->getParameter('forgot');
            if ($this->form->isValid()) {
                //do generation logic here
                $this->generateResetPasswordLink($request);
                $this->redirect("@forgotSuccess?i=" . encrypt($arrData['username']));
            } else {
                $this->setTemplate("forgotPassword");
            }
        } else {
            $this->redirect("signup/forgotPassword");
        }
    }

    public function executeForgot(sfWebRequest $request) {
        $username = decrypt($request->getParameter('i'));

        $this->email = "";
        $this->error = true;
        if (isset($username) && $username != "") {
            $arrDetails = Doctrine::getTable('sfGuardUser')->findByUsername($username);
            $arrayConvert = $arrDetails->toArray();

            if (!empty($arrayConvert)) {
                $arrUserDetails = Doctrine::getTable('UserDetail')->findByUserId($arrDetails->getFirst()->getId());
                $this->email = $arrUserDetails->getFirst()->getEmail();
            } else {
                $this->redirect('errors/error404');
            }
        }
        if (empty($this->email)) {
            $this->redirect('errors/error404');
        }
        $this->setTemplate("createForgotPassword");
    }

    private function generateResetPasswordLink(sfWebRequest $request) {
        $arrRequestDetails = $request->getPostParameter("forgot");

        list($y, $m, $d, $h, $i, $s) = explode("-", date("Y-m-d-H-i-s"));
        $expiry_date = date("Y-m-d H:i:s", mktime($h + 24, $i, $s, $m, $d, $y));

        $arrDetails = Doctrine::getTable('sfGuardUser')->findByUsername($arrRequestDetails['username']);
        //save details to forgot password table
        //update previous data to in active
        Doctrine::getTable('ForgotPassword')->UpdateExpiredLinks($arrDetails->getFirst()->getId());
        $objInsert = Doctrine::getTable('ForgotPassword')->
                        saveForgotPassword($arrDetails->getFirst()->getId(), $expiry_date);
        //generate activation code
        $hashAlgo = sfConfig::get('app_sf_crypto_captcha_hash_algo');
        $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo,
                        $arrDetails->getFirst()->getUsername(), $objInsert->getUpdatedAt());
        //update forgot password
        Doctrine::getTable('ForgotPassword')->UpdateForgotPassword($objInsert->getId(),
                $activationToken);
        //send mail to users email address
        //add job for mail
        $this->sendmailforresetpassword($arrDetails->getFirst()->getUsername(),
                $arrDetails->getFirst()->getId(),
                $activationToken, $expiry_date, $objInsert->getUpdatedAt());

        $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_PASSWORD_RESET_APPLIED;

        $userName = $arrDetails->getFirst()->getUsername();
        $userId = $arrDetails->getFirst()->getId();

        $applicationArr = NULL;
        $guser = Doctrine::getTable('sfGuardUser')->fetchUserDetails($userName);
        if (count($guser[0]['BankUser'])) {
            $bank_id = $guser[0]['BankUser'][0]['bank_id'];
            $bank_name = $guser[0]['bank_name'];
            if ($bank_id != "") {
                $branch_name = "";
            }
            $branch_id = $guser[0]['BankUser'][0]['branch_id'];
            if ($branch_id != "") {
                $branch_name = $guser[0]['bank_branch_name'];
            }
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME, $bank_name, $bank_id), new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME, $branch_name, $bank_id));
        } else {
            $applicationArr = NULL;
        }


        //$applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $userName, $userId));

        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_SECURITY,
                        EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_RESET,
                        EpAuditEvent::getFomattedMessage($msg, array('username' => $userName)),
                        $applicationArr,
                        $userId,
                        $userName
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    private function sendmailforresetpassword($username, $userId, $activationToken, $expiry_date, $date) {

        $subject = "Reset your Pay4me password";
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $activation_url = url_for('@resetPassword', true) .
                '?i=' . $userId . '&t=' . $activationToken;
        $taskId = EpjobsContext::getInstance()->addJob('SendMailResetPassword',
                        "email/changePasswordMail", array('username' => $username,
                    'expiry' => $expiry_date, 'userid' => $userId, 'date' => $date,
                    'subject' => $subject,
                    'activation_url' => $activation_url));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');
    }

    public function executeModifyPassword(sfWebRequest $request) {
        $err = "";
        if ($request->hasParameter("i") && ($request->hasParameter("t"))) {

            $userId = $request->getParameter("i");
            $token = $request->getParameter("t");
            $this->userId = $userId;
            $this->token = $token;

            //does token exist
            $passwordObj = Doctrine::getTable("ForgotPassword")->validateToken($userId, $token);

            if ($passwordObj) {
                $userDetails = Doctrine::getTable('sfGuardUser')->find($userId);
                $this->userDisplayName = $userDetails->getUserDisplayName();
                $this->submitTo = $this->moduleName . "/" . "updatePassword";

                $this->form = new ChangePasswordForm();
            } else {
                $err = "Sorry, this password reset link or code is no longer valid <br>
                For security reasons we have expired the password reset link or code";
            }
        } else {
            $err = "Oops...There seeem to be some problem";
        }
        $this->err = $err;
    }

    public function executeUpdatePassword(sfWebRequest $request) {
        $this->err = "";
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));

        $postArr = $request->getPostParameters();
        $this->userId = $userId = $postArr['userId'];
        $this->token = $token = $postArr['token'];

        $userDetails = Doctrine::getTable('sfGuardUser')->find($userId);
        $this->userDisplayName = $userDetails->getUserDisplayName();
        $username = $userDetails->getUsername();
        $userId = $request->getParameter('userId');
        $this->form = new ChangePasswordForm('', array('user_id' => $request->getParameter('userId')));

        $this->submitTo = $this->moduleName . "/" . "updatePassword";
        if ($this->processResetForm($request, $this->form, false, $userId)) {
            $username = $userDetails->getUsername();
            $applicationArr = NULL;
            $guser = Doctrine::getTable('sfGuardUser')->fetchUserDetails($username);
            if (count($guser[0]['BankUser'])) {
                $bank_id = $guser[0]['BankUser'][0]['bank_id'];
                $bank_name = $guser[0]['bank_name'];
                if ($bank_id != "") {
                    $branch_name = "";
                }
                $branch_id = $guser[0]['BankUser'][0]['branch_id'];
                if ($branch_id != "") {
                    $branch_name = $guser[0]['bank_branch_name'];
                }
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME, $bank_name, $bank_id), new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME, $branch_name, $bank_id));
            } else {
                $applicationArr = NULL;
            }
            //$applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO,$username,$userId));
            $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_PASSWORD_CHANGE;
            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_SECURITY,
                            EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE,
                            EpAuditEvent::getFomattedMessage($event_description, array('username' => $username)),
                            $applicationArr,
                            $userId,
                            $username);
            $this->sendmailforpasswordchangeconfirm($username, $userId, date('Y-m-d H:i:s'));
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

            $this->getUser()->setFlash('notice', "Password Changed Successfully", true);

            $this->redirect("@homepage");
        }

        $this->setTemplate('modifyPassword');
    }

    protected function processResetForm(sfWebRequest $request, sfForm $form, $redirect=true, $userId="") {//exit;
        $form->bind($request->getParameter($form->getName()));

        if ($form->isValid()) {
            $postArr = $request->getPostParameters(); //print_r($postArr);exit;
            if ($userId != "") {
                $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($userId); //print $sf_guard_user->getId();
            } else {
                $this->redirect('signup/modifyPassword');
            }

            if (EpPasswordPolicyManager::checkPasswordComplexity($postArr['sf_guard_user']['password'])) {
                $resVal = EpPasswordPolicyManager::checkPreviousPasswords(
                                $sf_guard_user->getId(),
                                $postArr['sf_guard_user']['password'],
                                $sf_guard_user->getSalt(), $sf_guard_user->getAlgorithm());

                if ($resVal) {

                    $oldPasswordLimit = EpPasswordPolicyManager::getOldPasswordLimit();
                    $this->getUser()->setFlash('error', 'New password can not be same as last ' . $oldPasswordLimit . ' password.', true);
                    $this->redirect('signup/modifyPassword?i=' . $request->getParameter('userId') . "&t=" . $request->getParameter('token'));
                } else {

                    $form->save();
                    $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($userId);
                    EpPasswordPolicyManager::doChangePassword($request->getParameter('userId'), $sf_guard_user->getPassword());
                    Doctrine::getTable("ForgotPassword")->UpdateExpiredLinks($request->getParameter('userId'));
                }
            } else {
                $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.', true);
                $this->redirect('signup/modifyPassword?i=' . $request->getParameter('userId') . "&t=" . $request->getParameter('token'));
            }
            if ($redirect)
                $this->redirect("@homepage");
            return true;
        }
    }

    private function sendmailforpasswordchangeconfirm($username, $userId, $date) {

        $subject = "Your Pay4me password has been changed successfully";
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        $taskId = EpjobsContext::getInstance()->addJob('SendMailResetPassword',
                        "email/changePasswordConfirmation", array('username' => $username,
                    'userid' => $userId, 'date' => $date,
                    'subject' => $subject));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');
    }

    public function executeChoicequestion(sfWebRequest $request) {
        $this->form = new ChoiceQuestionForm();
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('SQ'));
            if ($this->form->isValid()) {
                $arrQuestion = $request->getParameter('SQ');
                if (!empty($arrQuestion['questions'])) {
                    $user_id = $this->getUser()->getGuardUser()->getId();
                    Doctrine::getTable('SecurityQuestionsAnswers')->DeleteAllUserQustion($user_id);
                    Doctrine::getTable('SecurityQuestionsAnswers')->UpdateUserQustion($arrQuestion['questions'], $user_id);
                }
                $this->getUser()->setFlash('notice', sprintf('Questions Updated Successfully'), true);
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = url_for("page/index");
                $this->redirect($url);
            }
        }
    }

}

