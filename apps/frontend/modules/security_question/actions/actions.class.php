<?php

/**
 * security_question actions.
 *
 * @package    mysfp
 * @subpackage security_question
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class security_questionActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
   
    $this->security_question_list = Doctrine::getTable('SecurityQuestion')->getAllRecords() ;

     $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('SecurityQuestion',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->security_question_list);
        $this->pager->setPage($this->page);
        $this->pager->init();


   
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->security_question = Doctrine::getTable('SecurityQuestion')->find($request->getParameter('id'));
    $this->forward404Unless($this->security_question);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new SecurityQuestionForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new SecurityQuestionForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($security_question = Doctrine::getTable('SecurityQuestion')->find($request->getParameter('id')), sprintf('Object security_question does not exist (%s).', $request->getParameter('id')));
    $this->form = new SecurityQuestionForm($security_question);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($security_question = Doctrine::getTable('SecurityQuestion')->find($request->getParameter('id')), sprintf('Object security_question does not exist (%s).', $request->getParameter('id')));
    $this->form = new SecurityQuestionForm($security_question);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($security_question = Doctrine::getTable('SecurityQuestion')->find($request->getParameter('id')), sprintf('Object security_question does not exist (%s).', $request->getParameter('id')));
    $security_question->delete();

    $this->redirect('security_question/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $security_question = $form->save();

      if(($request->getParameter('action')) == "update") {
                $this->getUser()->setFlash('notice', sprintf('Security Question updated successfully'));
            }
            else if(($request->getParameter('action')) == "create") {
                    $this->getUser()->setFlash('notice', sprintf('Security Question added successfully'));
                }

      $this->redirect('security_question/index');
    }
  }
}
