<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('security_question/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm" id="pfm_security_question_form" name="pfm_security_question_form">
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>


    <fieldset>
        <?php echo $form ?>
        <div class="XY20">
            <center>
                &nbsp;<?php  echo button_to('Cancel','',array('onClick'=>'location.href=\''.url_for('security_question/index').'\''));?>
                <input type="submit" value="Save" />
        </center></div>
    </fieldset>

</form>
