<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $security_question->getid() ?></td>
    </tr>
    <tr>
      <th>Questions:</th>
      <td><?php echo $security_question->getquestions() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $security_question->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $security_question->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $security_question->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $security_question->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $security_question->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('security_question/edit?id='.$security_question->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('security_question/index') ?>">List</a>
