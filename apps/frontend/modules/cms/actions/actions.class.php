<?php

/**
 * Cms actions.
 *
 * @package    symfony
 * @subpackage Cms
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class CmsActions extends sfActions {
    public function executeIndex(sfWebRequest $request) {
        $pageName = $request->getParameter('page');
        // echo "<pre>";print_r($pageName) ;
        //$this->getResponse()->setCookie('WelcomeMessage', 'newOne', 0, '/',0 , true);
        if(isset($pageName) && !empty($pageName) && $pageName !='welcome') {
            if($pageName=='feedback') {
                $this->form = new FeedbackForm();
                if($request->isMethod('post')) {
                    $this->processForm($request, $this->form);
                }
            }
            $this->setTemplate($pageName);
        }else {
            $this->redirect404();
        }

    }

    function processForm(sfWebRequest $request, sfForm $form) {
        $this->forward404Unless($request->isMethod('post'));
//die($request->getParameter('feedback[name]'));
        $data = $request->getParameter($form->getName());
        $data['ip'] = $this->getIpAddress();
        $form->bind($data);
        if ($form->isValid()){
            $form->save();
            $this->getUser()->setFlash('notice', 'Submit successful.');
            $this->redirect('cms/feedback');
        }

    }

    /*
   * Function to get ipaddress of the system from where accessing application rather server error
   */
  public function getIpAddress() {
      return (empty($_SERVER['HTTP_CLIENT_IP'])?(empty($_SERVER['HTTP_X_FORWARDED_FOR'])?
          $_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_X_FORWARDED_FOR']):$_SERVER['HTTP_CLIENT_IP']);
    }


}
