<?php  include_partial('leftBlock', array('title' => 'About pay4me' )) ?>

<div id="wrapperInnerRight">
      <div id="innerImg">
	   <?php echo image_tag('/img/new/img_about_pay4me.jpg',array('alt'=>'About pay4me', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
	  </div>
      <!--<div>
        <h3>What is pay4me?     </h3>
      </div>-->
	  <?php echo ePortal_pagehead_unAuth('What is pay4me?');?>
      <p>pay4me<sup>TM</sup> is a Billing Solution that enables companies and institutions present bills to their customers in simplified and transparent manner. The solution allows customers to make payments on the presented bills in a manner that assures that payment is validated and the company and/or institution receives its funds.<br />
      </p>
<p>&nbsp;</p>
      <p>The pay4me<sup>TM</sup> service provides customers with more payment options and allow companies and institutions to focus on core business rather than worry about billing and payment collection.</p>
<div id="slideControl"></div>
      <div class="clearfix"></div>
    </div>