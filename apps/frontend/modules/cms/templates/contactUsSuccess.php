<?php  include_partial('leftBlock', array('title' => 'Contact' )) ?>

<div id="wrapperInnerRight">
      <div id="innerImg">
	   <?php echo image_tag('/img/new/img_about_pay4bill.jpg',array('alt'=>'Contact', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
</div>
      <!--<div>
        <h3>How to reach us</h3>
      </div>-->
	   <?php echo ePortal_pagehead_unAuth('How to reach us');?>
      <p>Do you want to make an enquiry, a suggestion, or would like to give us feedback on our services? The contact details below can be used to reach us.</p>
      <p>&nbsp;</p>
      <p><strong>Pay4Me Services </strong></p>
      <p>64a Fola Osibo Street, </p>
      <p>Lekki Phase One,</p>
      <p>Lagos, Nigeria</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>Phone: +234 012 716 943</p>
      <p>Email: info@pay4me.com</p>
      <div id="slideControl"></div>
      <div class="clearfix"></div>
    </div>