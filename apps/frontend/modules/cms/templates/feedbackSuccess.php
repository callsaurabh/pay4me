<?php //use_helper('Form');?>

<?php
  // $sfU = sfContext::getInstance()->getUser();
//   //echo $sfU->getFlash('notice'); 
//   if ($sfU->hasFlash('notice')){ echo $sfU->getFlash('notice');
//    $html ='<div id="flash_notice" class="error_list" ><span>';
//    $html .= nl2br($sfU->getFlash('notice'));
//    $html .='</span></div>';
//  }
?>


<?php  include_partial('leftBlock', array('title' => 'Feedback' )) ?>
<div id="wrapperInnerRight">
      <div id="innerImg">
	  <?php echo image_tag('/img/new/img_feedback.jpg',array('alt'=>'Feedback', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
</div>
      <!--<div>
        <h3>Feedback from you.</h3>
	   
	   
        <p>&nbsp;</p>
      </div>-->
	  <?php echo ePortal_pagehead_unAuth('Feedback from you');?>
	  
      <p>What do you think about Pay4Me? Fill this Feedback form and mail us your comments.</p>
      <p>&nbsp;</p>
	   <?php echo form_tag('cms/feedback',array('name'=>'feedback','id'=>'feedbackForm')); ?>
      
	  
	  <div class="feedbackForm">
        <div class="header">Feedback Form</div>
        <div class="fieldName">Full Name <span class="required">*</span></div>
        <div class="fieldWrap">
          <label>
            <?php echo $form['name']->render(array('class' => 'txtfield')) ?>
          </label>
		   <div style="float: left;"><?php echo $form['name']->renderError() ?></div>
        </div>
		 
        <div class="fieldName">Email Address <span class="required">*</span></div>
        <div class="fieldWrap">
          <label>
            <?php echo $form['email']->render(array('class' => 'txtfield')) ?>
          </label>
		  	<div style="float: left;"><?php echo $form['email']->renderError() ?></div>
        </div>
	
        <div class="fieldName">Message <span class="required">*</span></div>
        <div class="fieldWrap">
          <label>
            <?php echo $form['message']->render(array('class' => 'txtfield')) ?>
          </label>
		  <div style="float: left;"><?php echo $form['message']->renderError() ?></div>
        </div>
		
        <div class="fieldName">&nbsp;</div>
        <div class="fieldWrap">
		<?php echo $form['_csrf_token']->render()?>
		<?php echo $form['_csrf_token']->renderError()?>
          <label>
            <input name="button" type="submit" class="button" id="button" value="Submit" />
          </label>
        </div>
        <div class="clearfix"></div>
      </div>
      <p>&nbsp;</p>
    </div>



















<!--<div id="feedbackPage" class="cmspage">
    <?php
    //echo ePortal_pagehead('Feedback');
    ?>
    <p class="descriptionArea">What do you think about Pay4Me? Fill this Feedback form and mail us your comments.<br />

    </p>
    <div class="wrapForm2">
    <?php// echo form_tag('cms/feedback',array('name'=>'feedback','id'=>'feedbackForm')); ?>
    
        <?php
        //echo ePortal_legend('Feedback Form');
        //echo $form ?>
        <div class="divBlock">
            <center>                
                <input type="submit" value="Submit" class="formSubmit" />
            </center>
        </div>
    
</form>
</div>
<p class="descriptionArea">Thank you.&nbsp;<br/>&nbsp;</p>
</div>-->

