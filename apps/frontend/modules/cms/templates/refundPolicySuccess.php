<?php //use_helper('Form');?>
<div id="refundPage" class="cmspage">
    <?php
    echo ePortal_pagehead('Refund Policy');
    ?>
    <p class="descriptionArea">Pay4Me Services Limited (“<a class="" href="https://www.pay4me.com">https://www.pay4me.com</a>” or “pay4me”) has a “No Refund” Policy for any travel Visa, International Passports or any other goods and/or services, purchased from any Participating Passport Agency, Embassy/Consulate or web merchant through pay4me.com. pay4me.com accepts no responsibility for the products or services provided by any participating Passport Agency or Embassy/Consulate in connection with the granting of passports or visas, nor for any delays,  mistakes on passports, loss of passports or other materials occasioned by such services or by any delivery services such as FedEx, UPS, or the US or other Postal Service. Damage compensation is not available from pay4me.</p>

    <p class="descriptionArea">pay4me will refund only if there is a mistake made, as solely determined by pay4me, with respect to two or more payment transactions being processed for ONE order. Any other refunds can only be received directly from seller or provider of the applicable goods and/or service per the sellers refund policy.</p>

    <p class="descriptionArea">You may email us a written letter detailing your request and pay4me will review your complaint and decide if a refund is warranted. pay4me reserves the right to grant or deny refunds to anyone for any reason.  You can email refund requests to: <a href="mailto:refunds@pay4me.com">refunds@pay4me.com</a>
    </p>
    <p class="descriptionArea">pay4me reserves the right to modify this Refund Policy at its discretion, or against any customer it believes is abusing this policy. Any such revision or change will be binding and effective immediately after posting of the revised Refund Policy on pay4me.com. You agree to periodically review our Refund Policy, including the current version of our Refund Policy as made available at <?php echo link_to('https://www.pay4me.com/cms/refundPolicy','cms/refundPolicy') ?>.
    </p>
</div>