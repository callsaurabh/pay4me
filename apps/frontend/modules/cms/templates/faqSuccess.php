<?php  include_partial('leftBlock', array('title' => 'Frequently Asked Questions' )) ?>


<div id="wrapperInnerRight">
      <div id="innerImg">
	  <?php echo image_tag('/img/new/img_faq.jpg',array('alt'=>'Frequently Asked Questions', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
	 </div>
      <!--<div>
        <h3>Answers to your questions.</h3>
        </div>-->
		<?php echo ePortal_pagehead_unAuth('Answers to your questions');?>
<div class="faqstxt">
        <div class="faqstxt-wrap"><h1 class="question"><a name="why" id="why"></a>Why use Pay4Me?</h1><br />
            <span class="answer">Pay4Me is a safe and dependable EBPP platform designed to process your payments in real time. You can make multiple online payments at the same time on one platform. </span>
            <p class="answer">Above all, you have direct access to your Payment History!</p>
        </div>
          <div class="faqstxt-wrap">
            <h1 class="question"><a name="whatpay4me" id="whatpay4me"></a>What Can I do with Pay4Me?</h1>
            <ul>
              <li class="answer">Process multiple Service Bill payments at the same time on a single platform online</li>
              <li class="answer">Print Receipts for Service Bills payments</li>
              <li class="answer">Generate and present Bills in real time</li>
              <li class="answer">View and save your Payment Transaction Report</li>
              <li class="answer">Export payment transaction report to Excel spread sheet and save to your local machine</li>
            </ul>
          </div>
          <div class="faqstxt-wrap">
            <h1 class="question"><a name="howtosign" id="howtosign"></a>How can I Sign into Pay4Me?</h1>
            <ul>
              <li class="answer">On the Pay4Me home page, enter your User ID and Password in the User ID and Password fields</li>
              <li class="answer">Click the Sign In button</li>
            </ul>
          </div>
          <div class="faqstxt-wrap">
            <h1 class="question"><a name="selectaservice" id="selectaservice"></a>I want to select a Service</h1>
            <ul class="answer">
              <li>Sign into Pay4Me using your User ID and Password</li>
              <li>Click the Services tab and select a Service from the provided dropdown list</li>
            </ul>
          </div>
          <div class="faqstxt-wrap">
            <h1 class="question">How can I process payment for applicant?</h1>
            <ul>
              <li class="answer">Sign into Pay4Me using your User ID and Password</li>
              <li class="answer">Click the Services tab and select a Service from the provided dropdown list. The Pay4Me Bank Payment page opens.</li>
              <li class="answer">Enter the transaction number in the provided fields, and click the Submit button. The payment page is displayed and amount to be paid is auto-generated.</li>
              <li class="answer">Check the applicable charges check box. Also, click the I Agree … button and click the Payment button</li>
              <li class="answer">Payment processing may take about 4 seconds. A message confirms the successful payment.</li>
              <li class="answer">Click the Print User Receipt button to print receipt and issue to applicant. Click the Print Bank Receipt button to print the bank’s receipt</li>
            </ul>
          </div>
          <div>
            <h1 class="question">I cannot sign into Pay4Me</h1>
            <ul>
              <li class="answer">Ensure that your User ID and Passwords are correctly typed in the login fields</li>
              <li class="answer">Check the Caps Lock tab on your keyboard</li>
            </ul>
          </div>
<p></p>
      </div>
      <div id="slideControl"></div>
      <div class="clearfix"></div>
    </div>



<!--<div id="faqPage" class="cmspage">
    <?php
    //echo ePortal_pagehead('FAQ');
    ?>


    <div class="block-container">
        <div class="HFeatures">
            <a name="1"></a>
            <h1>Why use Pay4Me?</h1>
            <p>Pay4Me is a safe and dependable EBPP platform designed to process your payments in real time. You can make multiple online payments at the same time on one platform.
            </p>
            <p>Above all, you have direct access to your Payment History!</p>
        </div>
        <div class="HFeatures">
            <a name="2"></a>
            <h1>What Can I do with Pay4Me?</h1>
            <p>
            <ul>
                <li>Process multiple Service Bill payments at the same time on a single platform online</li>
                <li>Print Receipts for Service Bills payments</li>
                <li>Generate and present Bills in real time</li>
                <li>View and save your Payment Transaction Report</li>
                <li>Export payment transaction report to Excel spread sheet and save to your local machine</li>
            </ul>
            </p>
        </div>
        <div class="HFeatures">
            <a name="3"></a>
            <h1>How can I Sign into Pay4Me?</h1>
            <ul>
                <li>On the Pay4Me home page, enter your User ID and Password in the User ID and Password fields</li>
                <li>Click the Sign In button</li>
            </ul>
        </div>
        <div class="HFeatures">
            <a name="4"></a>
            <h1>I want to select a Service</h1>
            <ul>
                <li>Sign into Pay4Me using your User ID and Password</li>
                <li>Click the Services tab and select a Service from the provided dropdown list</li>
            </ul>
        </div>
        <div class="HFeatures">
            <a name="5"></a>
            <h1>How can I process payment for applicant?</h1>
            <ul>
                <li>Sign into Pay4Me using your User ID and Password</li>
                <li>Click the Services tab and select a Service from the provided dropdown list. The Pay4Me Bank Payment page opens.</li>
                <li>Enter the transaction number in the provided fields, and click the Submit button. The payment page is displayed and amount to be paid is auto-generated.</li>
                <li>Check the applicable charges check box. Also, click the I Agree … button and click the Payment button</li>
                <li>Payment processing may take about 4 seconds. A message confirms the successful payment.</li>
                <li>Click the Print User Receipt button to print receipt and issue to applicant. Click the Print Bank Receipt button to print the bank’s receipt</li>
            </ul>
        </div>
        <div class="HFeatures">
            <a name="6"></a>
            <h1>I cannot sign into Pay4Me</h1>
            <ul>
                <li>Ensure that your User ID and Passwords are correctly typed in the login fields</li>
                <li>Check the Caps Lock tab on your keyboard</li>
            </ul>
        </div>
        <p>&nbsp; </p><br/>
    </div>

</div>

-->