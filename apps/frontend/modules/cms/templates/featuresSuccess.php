<?php  include_partial('leftBlock', array('title' => 'Features' )) ?>

<div id="wrapperInnerRight">
      <div id="innerImg">
	  
	   <?php echo image_tag('/img/new/img_features.jpg',array('alt'=>'Features', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
	  </div>
     <!-- <div>
        <h3>Features  of pay4me.</h3>
      </div>-->
	   <?php echo ePortal_pagehead_unAuth('Features  of pay4me');?>
	  
      <p><strong>Secure &amp; Convenient Way to Pay</strong><br />
      Pay4Me allows you to pay for services even if you do not have a bank account. All transactions are secured via strong encryption algorithms using a digital certificate issued by a trusted global certification authority.</p>
      <p><br />
        <strong>Use one single, Trusted Platform</strong><br />
      You can make multiple payments at a time using Pay4Me. The platform is trusted by banks, and that trust and assurance is passed onto you the customer.</p>
      <p><br />
        <strong>Keep in touch with your Payment History</strong><br />
      View and save summarized or detailed reports of your payment transaction for audit trail or for safe keeping for review later.</p>
      <p><strong><br />
        Find Services by just clicking a Button</strong><br />
        You do not need to visit many web pages to process payment: initializing and validating payment can be done from ONE location.
      </p>
      <div id="slideControl"></div>
      <div class="clearfix"></div>
    </div>

<!--<div id="featuresPage" class="cmspage">
    <?//php
    echo ePortal_pagehead('Features');
    ?>

    <div class="blockBlueContainer">
        <div class="HFeatures">
            <h1>Secure &amp; Convenient Way to Pay</h1>

            <p> Pay for me allows you to pay for services even if you do not have a bank account. All transactions are secured via strong encryption algorithms using a digital certificate issued by a trusted global certification authority.</p>
        </div>
        <div class="HFeatures">
            <h1>Use one single, Trusted Platform</h1>
            <p>You can make multiple payments at a time using Pay4Me. The platform is trusted by banks, and that trust and assurance is passed onto you the customer.</p>
        </div>
        <div class="HFeatures">
            <h1>Keep in touch with your Payment History</h1>

            <p>View and save summarized or detailed reports of your payment transaction for audit trail or for safe keeping for review later.</p>
        </div>
        <div class="HFeatures">
            <h1>Find Services by just clicking a Button</h1>
            <p>You do not need to visit many web pages to process payment: initializing and validating payment can be done from ONE location.</p>
        </div>
    </div>

</div>-->

