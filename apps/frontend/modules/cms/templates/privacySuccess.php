
<?php //use_helper('Form');?>
<div id="privacyPage" class="cmspage">
    <?php
    echo ePortal_pagehead('Privacy Policy');
    ?>

<p class="descriptionArea"><i>This Privacy Notice was last modified: November 2, 2009</i></p>
<p class="descriptionArea">
The Pay4Me Privacy policy explains the ways we manage your personal information. It must be read in conjunction with the Pay4Me TERMS OF USE and You automatically agree to this policy when you enroll in the Pay4Me Service. </p>
<h2>The Privacy Policy</h2>
<p class="descriptionArea">This Privacy Policy (“Privacy Policy”) applies to Pay4Me, Ltd, its parents and related entities, subsidiaries, whether or not wholly owned (“Pay4Me”) and specifically, it outlines the types of information that Pay4Me gathers about you (“User”) while you are using the www.pay4me.com website or any other website that links to or uses the Pay4Me Service now or in the future (" Pay4Me Website(s)"). This Privacy Policy is incorporated into and is subject to the Pay4Me Terms of Use. Your use of the Pay4Me Website and any personal information you provide on the Pay4Me Website remains subject to the terms of this Privacy Policy and our Terms of Use.</p>
<h2>Enrolment Information</h2>
<p class="descriptionArea">During enrolment to Pay4Me, we require you to enter your personal information. These include your name, debit card number, card verification number, card expiration number, address, telephone number and email addresses. For Providers, we require that you provide your bank account number and may require your personal address, business category and other information concerning your Payment Transaction (‘Information”). This Information is stored with your Pay4Me account.</p>
<h2>Personal Information & Information Use </h2>
<p class="descriptionArea">Personally identifiable Information submitted via the Pay4Me Website is used only for limited purposes.  For example, when you register to request future communications from Pay4Me, the information you submit may be used to respond to you, to create a personal profile to facilitate further requests or inquiries, or to personalize your web site experience.  In addition, the webmasters may, where necessary, use your information for various site related tasks.</p>

<p class="descriptionArea">Such Information will not otherwise be shared with third parties unless so disclosed at the point of collection. Personally identifiable Information may be transmitted internationally to third parties for the purposes identified above. This may include transfer to countries without data protection rules similar to those in effect in your country of residence. By providing information to the Pay4Me Website, you are consenting to such transfers.</p>

<p class="descriptionArea">Where appropriate, personally identifiable Information may be disclosed to law enforcement, regulatory or other government agencies, or third parties where necessary or desirable to comply with legal or regulatory obligations or requests or for the purposes identified above. Pay4Me also reserves the right to disclose personally identifiable Information and/or non-personally-identifiable information that Pay4Me believes, in good faith, is appropriate or necessary to enforce the Terms of Use, take precautions against liability, to investigate and defend itself against any third-party claims or allegations, to assist government enforcement agencies, to protect the security or integrity of the Pay4Me Website, and to protect the rights, property, or personal safety of Pay4Me, our Users or others. In all cases we will treat requests to access information or change information in accordance with applicable legal requirements.</p>

<h2>Third Party Information</h2>
<p class="descriptionArea">We sometimes obtain information about you from third parties to save you from fraud, financial misconducts and also to verify the information which you have already provided. We may obtain such information about Providers from various Business Information Services.</p>
<h2>Transaction Information</h2>
<p class="descriptionArea">We collate information about every Payment Transaction carried out by you using the Pay4Me Service. These include but are not limited to transaction amount, description of the goods or services being provided by the Provider/, names of the Provider and the Customer and the modes of payment employed.</p>
<h2>Pay4Me Usage</h2>
<p class="descriptionArea">We may require information about your Service usage to enable us validate your identity and thereby stall any upcoming fraudulent conduct. Such information will only be used to prevent fraud except with your permission to use for other reasons.</p>
<h2>Cookies</h2>
<p class="descriptionArea">We send cookies to your computer for browser identification every time you access a Pay4Me web page. This enables us to improve the quality of our service by saving your preferences and remarking your trends. If you choose, you can set your browser to reject all cookies or alert you when a cookie is being sent; however if your cookies are deactivated, you may not be able to access Pay4Me.</p>
<h2>Information on Logs</h2>
<p class="descriptionArea">Our servers record information sent by your browser each time you use Pay4Me. These logs may include your IP address, the browser type and language, the date and time of your request, your web requests and cookies that help identify your browser.</p>
<h2>User Communication</h2>
<p class="descriptionArea">We may save emails and other communication sent by you to enable us process your queries and respond to your request and also, improve our services.</p>
<h2>Interface Links</h2>
<p class="descriptionArea">We may present links in a format that will help us identify if they were followed or not. Using this information, we can improve the quality of our search feature. Additionally, we use the information to provide our services to you; carry out auditing, research and analysis for improving the quality of our services; facilitate the technical functionality of our network and also, build new services.</p>
<p class="descriptionArea">Note that we may process your personal information on a Server which is located outside your own country. Sometimes also, we may process personal information on behalf of a third party</p>
<h2>Information Sharing</h2>
<p class="descriptionArea">We shall not use your personal information for commercial purposes or share with individuals outside Pay4Me. </p>
<p class="descriptionArea">However, we may share your personal information with individuals outside Pay4Me on the following situations:
<ol type="a">
    <li><p class="descriptionArea">To prevent or detect fraud or technical issues</p></li>
    <li><p class="descriptionArea">Where we have your consent and User has elected to opt-in for the sharing of any personal information</p></li>
    <li><p class="descriptionArea">(We provide such information to our subsidiaries, affiliated companies or other trusted businesses or persons for the purpose of processing personal information on our behalf. We require that these parties agree to process such information based on our instructions and in compliance with this Privacy Policy and any other appropriate confidentiality and security measures.</p></li>
    <li><p class="descriptionArea">We have a good faith belief that access, use, preservation or disclosure of such information is reasonably necessary to (a) satisfy any applicable law, regulation, legal process or enforceable governmental request, (b) enforce applicable Terms of Service, including investigation of potential violations thereof, (c) detect, prevent, or otherwise address fraud, security or technical issues, or (d) protect against imminent harm to the rights, property or safety of Pay4Me, its users or the public as required or permitted by law.</p></li>
    <li><p class="descriptionArea">We may share aggregated non-personally identifiable information with third parties. For example, we may disclose that a certain percentage of our users have a billing address in a particular geographic area, or that a certain percentage of users use a particular type of credit or debit card. However, if we do share this aggregated information, we do not include personally identifiable Information without your explicit opt-in consent or in the limited circumstances described above.</p> </li>
    <li><p class="descriptionArea">Should Pay4Me become involved in a merger, acquisition, or any form of sale of some or all of their assets, a noticed will be mailed to you before your personal information is transferred and becomes subject to a different privacy policy.</p></li>
</ol>
<h2>Information security</h2>
<p class="descriptionArea">Note that you must keep your User/Private ID and Password confidential and never share with anyone. If you do share these with a third party, they can have access to your account and all personal information.</p>
<h2>In the Event of Merger, Sale, or Bankruptcy</h2>
<p class="descriptionArea">In the event that Pay4Me is acquired by or merged with a third party entity, we reserve the right, in any of these circumstances, to transfer or assign the information we have collected from our Users as part of such merger, acquisition, sale, or other change of control. In the unlikely event of our bankruptcy, insolvency, reorganization, receivership, or assignment for the benefit of creditors, or the application of laws or equitable principles affecting creditors' rights generally, we may not be able to control how your personal information is treated, transferred, or used.</p>
<h2>Changes to Privacy Notice</h2>
<p class="descriptionArea">This Privacy Notice may be revised periodically and this will be reflected by the "effective date" detailed above. Please revisit this page to stay aware of any changes. In general, Pay4Me may only use your personal information in the manner described in the Privacy Policy in effect when we received the personal information you provided. Your continued use of the Pay4Me Websites constitutes your agreement to this Privacy Policy and any future revisions.</p>
<p class="descriptionArea">Please feel free to direct any questions or concerns regarding this Policy or Pay4Me’s treatment of personal information by contacting us through this web site at privacy@pay4me.com When we receive formal written complaints at this address, it is Pay4Me’s policy to contact the complaining User regarding his or her concerns. We will cooperate with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of personal data that cannot be resolved between Pay4Me and an individual. </p>
<h2>Transfer of User Data outside the User Jurisdiction</h2>
<p class="descriptionArea">The Internet is a global environment. In order to provide this website we may need to transfer User Data to locations outside the jurisdiction in which you are viewing this website (the User Jurisdiction) and process User Data outside the User Jurisdiction therefore it must be noted that such transfers and processing of User Data may be in locations outside the User’s Jurisdiction and any data sent or uploaded by you may be accessible in jurisdictions outside the User’s Jurisdiction and that the level of data protection offered in such jurisdictions may be more or less than that offered within the User Jurisdiction.</p>
<p class="descriptionArea">User Data may be controlled and processed by any of the Pay4Me offices some of which may be outside a User Jurisdiction.  The location of our offices may change from time to time and Pay4Me may acquire offices in any number of countries or territories at any time, any one or more of which may act as controllers of and/or process User Data.</p>
<p class="descriptionArea">By continuing to use this website and by providing any personal data (including sensitive personal data) to us via this website or e-mail addresses provided on this website, you are consenting to such transfers, provided that they are in accordance with the purposes set out above and elsewhere in the Terms of Use. Please do not send us any personal data if you do not consent to the transfer of this information to locations outside the User Jurisdiction.</p>

<br/>
</div>

