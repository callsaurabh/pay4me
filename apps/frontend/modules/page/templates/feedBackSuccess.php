<div class="innerWrapper">
<?php use_helper('a') ?>
<?php //use_helper('Form'); ?>
<?php include_partial('leftBlock', array('title' => 'Support')) ?>
<?php
use_javascript('general.js');
?>
<style>
    .cRed {display:none;}
</style>
<div id="wrapperInnerRight">
    <div id="innerImg">
        <?php a_slot('feedback', 'aImage', array('toolbar' => 'basic', 'global' => true, 'width' => 600, 'height' => 242, 'border' => 0)); ?>
    </div>
    <div class="faqstxt">
        <?php a_slot('feedback_text', 'aRichText', array('toolbar' => 'basic', 'global' => true)); ?>
    </div>
    <div class="clear"></div>
    <?php
        if ($sf_user->hasFlash('notice')) {
            echo "<div id='flash_notice' class='error_list' ><span>" . sfContext::getInstance()->getUser()->getFlash('notice') . "</span></div>";
        } ?>
<?php echo form_tag('page/feedBack', array('name' => 'feedback', 'id' => 'feedbackForm', 'onSubmit' => "return validate_feedback();")); ?>
        <div class="feedbackForm">
            <div class="header">Feedback Form</div>
            <div class="fieldName">Full Name <span class="required">*</span></div>
            <div class="fieldWrap">
                <label>
<?php echo $form['name']->render(array('class' => 'txtfield')) ?>
            </label>
            <div class="cRed" id="err_name" style="float: left;"><?php echo $form['name']->renderError() ?></div>
        </div>

        <div class="fieldName">Email Address <span class="required">*</span></div>
        <div class="fieldWrap">
            <label>
<?php echo $form['email']->render(array('class' => 'txtfield')) ?>
            </label>
            <div class="cRed" id="err_email" style="float: left;"><?php echo $form['email']->renderError() ?></div>
        </div>



        <div id="merchant_id" class="clear">
            <div class="fieldName">Merchant <span class="required">*</span></div>
            <div class="fieldWrap"><?php echo $form['merchant_id']->render(array()) ?>
                <div class="cRed" id="err_merchant_id" id="merchant_id"><?php echo $form['merchant_id']->renderError() ?></div>
            </div></div>

        <div id ="payment_mode_option_id" class="clear">
            <div  class="fieldName">Payment Mode</div>
            <div class="fieldWrap"><?php echo $form['payment_mode_option_id']->render(array()) ?> </div>
        </div>


        <div id="bank_id" class="clear">
            <div class="fieldName">Bank <span class="required">*</span></div>
            <div class="fieldWrap"> <?php echo $form['bank_id']->render(array()) ?>

                <div class="cRed" id ="err_bank_id" ><?php echo $form['email']->renderError() ?></div>
            </div></div>
        <div id ="ewallet_username" class="clear">
            <div class="fieldName">eWallet Username <span class="required">*</span></div>
            <div class="fieldWrap"><?php echo $form['ewallet_username']->render(array()) ?>

                <div class="cRed" id ="err_ewallet_username" ><?php echo $form['ewallet_username']->renderError() ?></div>
            </div></div>

        <div id ="ewallet_account_number"  class="clear">
            <div class="fieldName">eWallet Account No. <span id="req_ewallet_account_number" class="required">*</span></div>
            <div class="fieldWrap"><?php echo $form['ewallet_account_number']->render(array()) ?>

                <div class="cRed" id ="err_ewallet_account_number" ><?php echo $form['ewallet_account_number']->renderError() ?></div>
            </div></div>

        <div id ="common_issue" class="clear">

            <div class="fieldName">Common Issue<span class="required">*</span></div>
            <div class="fieldWrap"><?php echo $form['common_issue']->render(array()) ?>
                <div class="cRed" id="err_common_issue" ><?php echo $form['common_issue']->renderError() ?></div>
            </div> </div>



        <div id ="reason" class="clear">
            <div class="fieldName">Reason<span class="required">*</span> </div>
            <div class="fieldWrap"> <?php echo $form['reason']->render(array()) ?>
                <div class="cRed" id ="err_reason" ><?php echo $form['reason']->renderError() ?></div></div>
        </div>


        <div id =" additional_comments" class="clear" >
            <div class="fieldName">Additional Comments<span class="required">*</span> </div>
            <div class="fieldWrap">
<?php echo $form['message']->render(array('class' => 'txtfield')) ?>
                <div class="cRed" id ="err_message" style="float: left;"><?php echo $form['message']->renderError() ?></div>
            </div>

        </div>

        <div class="fieldName">&nbsp;</div>
        <div class="fieldWrap">
            <?php echo $form['_csrf_token']->render() ?>
<?php echo $form['_csrf_token']->renderError() ?>
            <label>
                <input name="button" type="submit" class="button" id="button" value="Submit" />
            </label>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var payment_mode =  '';
        $.post('<?php echo url_for("page/getCurrentIssue") ?>', {'payment_mode_id': payment_mode,'byPass':1}, function (data) {

            $('#feedback_common_issue').html(data);
        });
        $('#merchant_id').show();
        $('#bank_id').hide();
        $('#common_issue').show();
        $('#reason').hide();
        $('#feedback_reason').val('');
        $('#ewallet_username').hide();
        $('#ewallet_account_number').hide();

    });
    $("#payment_mode_option_id").change(function() {
        $('#reason').hide();
        $('#feedback_reason').val('');
        var payment_mode =  $("#feedback_payment_mode_option_id").val();
        $.post('<?php echo url_for("page/getCurrentIssue") ?>', {'payment_mode_id': payment_mode,'byPass':1}, function (data) {

            $('#feedback_common_issue').html(data);
        });
        if(($("#feedback_payment_mode_option_id").val() == 1 ) || ($("#feedback_payment_mode_option_id").val() == 14 ) || ($("#feedback_payment_mode_option_id").val() == 15 )){

            $('#merchant_id').show();
            $('#bank_id').show();
            $('#common_issue').show();
            $('#ewallet_username').hide();
            $('#ewallet_account_number').hide();
            $('#feedback_ewallet_username').val('');
            $('#feedback_ewallet_account_number').val('');

        }else if(($("#feedback_payment_mode_option_id").val() == 2) || ($("#feedback_payment_mode_option_id").val() == 4)|| ($("#feedback_payment_mode_option_id").val() == 5)){

            $('#merchant_id').show();
            $('#bank_id').hide();
            $('#feedback_bank_id').val('');
            $('#common_issue').show();
            $('#ewallet_username').show();
            $('#ewallet_account_number').show();
            $('#req_ewallet_account_number').show();


        }else{
            $('#merchant_id').show();
            $('#bank_id').hide();
            $('#feedback_bank_id').val('');
            $('#common_issue').show();
            $('#reason').hide();
            $('#feedback_reason').val('');
            $('#ewallet_username').hide();
            $('#ewallet_account_number').hide();
            $('#feedback_ewallet_username').val('');
            $('#feedback_ewallet_account_number').val('');
        }
    });
    $("#feedback_common_issue").change(function() {
        if($("#feedback_common_issue").val() == 'Others'){
            $('#reason').show();
        }else{
            $('#reason').hide();
            $('#feedback_reason').val('');
        }
    });

    $("#common_issue").change(function() {
        if(($("#feedback_payment_mode_option_id").val() == 2) || ($("#feedback_payment_mode_option_id").val() == 4)|| ($("#feedback_payment_mode_option_id").val() == 5)){
            if($('#feedback_common_issue').val()=='Forgot Username'){
                $('#ewallet_username').hide();
                $('#req_ewallet_account_number').hide();
                $('#feedback_ewallet_username').val('');
                $('#err_ewallet_account_number').html('');
            }else{
                $('#ewallet_username').show();
                $('#ewallet_account_number').show();
                $('#req_ewallet_account_number').show();
            }
        }
    });
    function validate_feedback(){
        $('.cRed').css("display", "block");
        var err  = 0;
        if($('#feedback_name').val() == "")
        {
            $('#err_name').html("Please enter Full Name");
            err = err+1;
            $('#feedback_name').focus();
        }
        else
        {
            $('#err_name').html("");
        }
        if($('#feedback_name').val() != ""){
            if(validateString($('#feedback_name').val()))
            {
                $('#err_name').html("Please enter valid Full Name");
                err = err+1;
                $('#feedback_name').focus();
            }
            else
            {
                $('#err_name').html("");
            }
        }
        var regalpha = /[a-z]/i;
        var regnumeric = /^\d+$/;
        var regspecial = /[\^\$\.\?\*\!\+\:\=\(\)\[\]\{\}\|@#%&_-]/;

        if($('#feedback_email').val() == "")
        {
            $('#err_email').html("Please enter Email Address");
            err = err+1;
            $('#feedback_email').focus();
        }
        else
        {
            $('#err_email').html("");
        }
        if($('#feedback_email').val() != ""){
            if(!validateEmail($('#feedback_email').val()))
            {
                $('#err_email').html("Please enter valid Email Address");
                err = err+1;
                $('#feedback_email').focus();
            }
            else
            {
                $('#err_email').html("");
            }
        }
        if($('#feedback_merchant_id').val() == "")
        {
            $('#err_merchant_id').html("Please select Merchant");
            err = err+1;
            $('#feedback_merchant_id').focus();
        }
        else
        {
            $('#err_merchant_id').html("");
        }
        if($('#feedback_common_issue').val() == "")
        {
            $('#err_common_issue').html("Please select Common Issue");
            err = err+1;
            $('#feedback_common_issue').focus();
        }
        else
        {
            $('#err_common_issue').html("");
        }


        if(($('#feedback_payment_mode_option_id').val() == 1) || ($('#feedback_payment_mode_option_id').val() == 14) || ($('#feedback_payment_mode_option_id').val() == 15) )
        {
            if($('#feedback_bank_id').val() == "")
            {
                $('#err_bank_id').html("Please select Bank");
                err = err+1;
                $('#feedback_bank_id').focus();
            }else{
                $('#err_bank_id').html("");
            }
        }else{
            $('#err_bank_id').html("");
        }
        if((($('#feedback_payment_mode_option_id').val() == 2) || ($('#feedback_payment_mode_option_id').val() == 4) || ($('#feedback_payment_mode_option_id').val() == 5)) && ($('#feedback_common_issue').val()!='Forgot Username') )
        {
            if($('#feedback_ewallet_username').val() == "")
            {
                $('#err_ewallet_username').html("Please enter eWallet Username");
                err = err+1;
                $('#feedback_ewallet_username').focus();
            }else{
                $('#err_ewallet_username').html("");
            }
            if($('#feedback_ewallet_account_number').val() == "")
            {
                $('#err_ewallet_account_number').html("Please enter eWallet Account Number");
                err = err+1;
                $('#feedback_ewallet_account_number').focus();
            }else{
                if(regnumeric.test($('#feedback_ewallet_account_number').val()) == false) {
                    $('#err_ewallet_account_number').html("Please enter Numeric eWallet Account Number");
                    err = err+1;
                    $('#feedback_ewallet_username').focus();
                }else{
                    $('#err_ewallet_account_number').html("");
                }
            }
        }else{
            $('#err_ewallet_username').html("");
            $('#err_ewallet_account_number').html("");
        }
        if($('#feedback_common_issue').val()=='Forgot Username')
        {

            if($('#feedback_ewallet_account_number').val() != "")
            {
                if(regnumeric.test($('#feedback_ewallet_account_number').val()) == false) {
                    $('#err_ewallet_account_number').html("Please enter Numeric eWallet Account Number");
                    err = err+1;
                    $('#feedback_ewallet_username').focus();
                }else{
                    $('#err_ewallet_account_number').html("");
                }
            }
        }
        if($('#feedback_common_issue').val() == "Others")
        {
            if($('#feedback_reason').val() == ""){
                $('#err_reason').html("Please enter Reason");
                err = err+1;
                $('#feedback_reason').focus();
            }else{
                $('#err_reason').html("");
            }
        }else{
            $('#err_reason').html("");
        }
        if($('#feedback_message').val() == "")
        {
            $('#err_message').html("Please enter Additional Comments");
            err = err+1;
            $('#feedback_message').focus();
        }
        else
        {
            $('#err_message').html("");
        }
        if(err == 0) {
            $("#byPass").val(1);
            return true;
        } else {
            //                            $('#password').val('');
            //                            $('#cpassword').val('');
            //                            $('#captcha').val('');
            //                            $('#password_strength_bar').html('');
            //                            progBarDiv('submitdiv','progBarDiv',0);
            //                            document.getElementById('captcha_img').src = "<?php echo url_for('@sf_captcha') ?>?r=" + Math.random() + "&reload=1";
            return false;
        }

    }

</script>
