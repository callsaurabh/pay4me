<style>
  ul#visaTypesDetails li{margin-bottom:20px;}
  ul#visaTypesDetails ul li{margin-bottom:auto;}
  ol li {margin:auto;}
</style>
<h1>About NIS </h1>

<div id="contactPage" class="XY20">
<h4>History content</h4>
<p>The Nigeria Immigration Service (NIS) has witnessed series of changes since it was extracted from the Nigeria Police Force (NPF) in 1958. The Immigration Department as it was known then was entrusted with the core Immigration duties under the headship of the Chief Federal Immigration Officer (CFIO). The department in its embryo inherited the Immigration Ordinance of 1958 for its operation. At inception the department had a narrow operational scope and maintained a low profile and simple approach in attaining the desired goals and objectives of the government. During this period, only the Visa and Business Sections were set up.
</p>
<p>
  On August 1st, 1963, Immigration Department came of age when it was formally established by an Act of Parliament (Cap 171, Laws of the Federation Nigeria). The head of the Department then was the Director of Immigration. Thus, the first set of Immigration officers were former NPF officers. It became a department under the control and supervision of the Federal Ministry of Internal Affairs (FMIA) as a Civil Service outfit.
</p>

<h4>Mission statement content</h4>
<ol tyle='1'>
  <li>Our resolve is to have an IT driven security outfit that can conveniently
  address the operational challenges of modern migration.</li>

  <li>To give the service a new sense of direction that can make it relevant at
  all times to the world security order and global trend.</li>
</ol>

<h4>Operational Structure</h4>
<p>
  The Service has three (3) Directorates, Eight Zonal offices, Thirty-Six State Commands & Federal Capital Territory and Immigration offices in the 774 local government areas.
</p>
The three Directorates are:
<dl>
  <dt>Finance, Administration & Technical Services(FATS)</dt>
  <dd>The FATS directorate is statutorily charged with the responsibility of policy initiation, formulation, articulation and implementation.
    <br />It is equally responsible for the financial management, recruitment, discipline, postings, deployments and promotion of officers and men of the Service.
  </dd>

  <dt>Operations, Passport, Border Patrol, ECOWAS & African Affairs</dt>
  <dd>
    It is responsible for the provision of various immigration facilities such as:
    <ul>
      <li>Visa</li>
      <li>CERPAC</li>
      <li>Passports</li>
    </ul>
  </dd>

  <dt>Investigation, Inspectorate and Enforcement </dt>
  <dd>
    <p> Investigation, Inspectorate and Enforcement is a Directorate of the Nigeria charged with the following responsibilities:</p>

    <b>Investigation and Aliens Control :</b><br />
    (I) Detection, Screening and investigation of violations and abuses of
    Immigration Laws and regulations

  </dd>
</dl>


<h4>Visa Types</h4>
<p>
  <p>CATEGORIES OF VISAS AVAILABLE TO EXPATRIATES DESIROUS OF TRAVELING TO NIGERIA FOR VARIOUS REASONS & PURPOSES
  </p>
  <p>
    Nigerian Visas are obtained in the Countries where applicants are domiciled or the nearest Nigerian Mission nearest to their Countries of residence.
  </p>
  <ul id='visaTypesDetails' type="1">
    <li><h5> Transit Visa / Entry Permit </h5>
      <b>Who qualifies</b><br />
      - Transiting passengers with confirmed Visa to onward destination other than Nigeria<br />
      - Seamen/pilots <br />
      <p>
        <b>Requirements for the issuance of Transit Visa</b>
        <ul>
          <li>Valid passport with minimum of 6 months validity</li>
          <li>Valid visa to onward  destination outside Nigeria</li>
          <li>Confirmed Airline ticket to final destination</li>
          <li>Evidence of funds</li>
        </ul>
      </p>

    </li>
    <li>
      <h5>Direct Transit Visa (airside) / Entry Permit</h5>
      <p>
        <b>Who qualifies </b><br />
        - Transiting passengers with confirmed Visa to onward destination other  than Nigeria <br />
        - Seamen/ Pilots signing on <br />

        <b>Requirements for the issuance of Direct Transit Visa (airside)/ Entry Permit</b>
        <ul>
          <li>Passport with minimum of 6 months validity</li>
          <li>Valid visa to onward  destination outside Nigeria</li>
          <li>Confirmed Airline ticket to final destination</li>
          <li>Evidence of sufficient funds</li>
        </ul>
        <b>Validity</b> <br />
        - Validity of stay is 48 hours. <br />
        - Not valid for employment
      </p>
    </li>

    <li>
      <h5>Business Visa / Entry Permit</h5>
      <p>
        <b>Who qualifies </b> <br />

        Business men and Investors coming to Nigeria for business discussions
        Obtainable from Nigerian Missions abroad
        <br />
        <b>Requirements for the issuance of Business Visa / Entry Permit</b>
        <ul>
          <li>Evidence of sufficient funds</li>
          <li>A valid return ticket</li>
          <li>Acceptance of IR (Immigration Responsibilities)</li>
          <li>Evidence of sufficient funds to maintain one's self in the country </li>
          <li>A letter of invitation from a company in Nigeria, accepting full immigration responsibility </li>
        </ul>
        <b>Validity</b> <br />
        - Validity of stay is 90 days but extendable
        - Not valid for employment
      </p>

    </li>

    <li>
      <h5>Tourist Visa / Entry Permit</h5>

      <p><strong>Who qualifies</strong> <br />
        - Persons wishing to visit Nigeria for the purpose of tourism<br />
        - Individuals who want to visit family members <br />
        <strong>Requirements for the issuance of Tourist Visa / Entry Permit</strong>
        <ul>
          <li>Valid passport with minimum of 6 months validity</li>
          <li> Valid Visa to onward destination</li>
          <li>Valid Airline return ticket</li>
          <li>Evidence of funds</li>
          <li>Evidence of Hotel reservation</li>
          <li>Acceptance of IR (Immigration Responsibility)</li>
        </ul>
        <strong>Validity</strong><br />
      - Not valid for employment</p>

    </li>

    <li>
      <h5>Diplomatic Visa / Entry Permit</h5>

      <p><strong>Who qualifies </strong>
        <ul>
          <li>Visiting Heads of States and their families</li>
          <li>Top officials of Government and their families</li>
          <li> Diplomats and their families</li>
          <li> Holders of United Nations Diplomatic Passport and Laisser passes</li>
          <li> members of accredited Diplomatic Missions</li>
          <li> members of International organizations</li>
          <li> members of accredited International non governmental organizations  </li>
        </ul>
        <strong>Requirements for the issuance of Official /    Diplomatic Visa / Entry Permit </strong>
        <ul>
          <li>Valid standard, official or diplomatic     passports</li>
          <li>Note Verbale</li>
          <li>Obtainable from Nigerian Missions abroad </li>
        </ul>
      </p>
    </li>

    <li>
      <h5>Subject to Regularization Visa / Entry Permit</h5>

      <p><strong>Who qualifies </strong><br />
        Expatriates employed by individuals, corporate bodies or governments (i.e. to take up employment in Nigeria)<br />
        <strong>Requirements for the issuance of STR Visa / Entry Permit</strong>
        <ul>
          <li>  Valid passport with minimum of 6 months validity</li>
          <li>Letter of employment</li>
          <li>Expatriate quota approval</li>
          <li>Credentials of the applicant</li>
          <li>Duly completed form IMM 22</li>
          <li>Curriculum Vitae or Resume</li>
          <li>For chief executive officers (C.E.O) of corporate organizations, there
          is  need for extract of the minutes of the Board's resolution</li>
          <li>Obtainable only from Nigerian Missions in the countries where
          applicants are domiciled for at least six (6) months</li>
          <li>Receipt of online payment of visa fee.</li>
        </ul>
      </p>

    </li>
    <li>
      <h5>Temporary Work Permit Visa / Entry Permit</h5>

      <p><strong>Who qualifies</strong> <br />
        - Experts invited by corporate bodies to provide specialized skilled services,  such as after sales installation , maintenance, repairs of machines &amp; equipment<br />
        <strong>Requirements for the issuance of Temporary work permit (TWP)</strong>
        <ul>
          <li>  Confirmed Airline Return ticket</li>
          <li>Acceptance of IR (Immigration Responsibility) by inviting organizations or  individual.</li>
          <li>Obtainable only from the office of the  Comptroller General of Immigration in  the Nigeria Immigration Service Headquarters, Abuja.</li>
        </ul>
      </p>

    </li>
  </ul>
  <b><u>Explanatory Note:</u></b> <br />
  IR means acceptance of Immigration Responsibility which includes but not limited to
  <ul>
    <li>Accommodation/feeding</li>
    <li>Transportation</li>
    <li>and if need be the cost of repatriation or deportation</li>
  </ul>
</p>
<h4>Expatriate Quota</h4>

<h4>REVISED    GUIDELINE ON BUSINESS PERMIT AND QUOTA</h4>
<br />
<h5>INTRODUCTION </h5>
The Citizenship and Business Department has responsibility for    administering and enforcing the provisions of the immigration Act, 1963    relate to the establishment of business in Nigeria and the employment of    expatriates. In other Words, the Department is entrusted principally with the    following responsibilities; <br />
<ol type="I">
  <li>Issuance of Business Permit and expatriate Quota positions</li>
  <li>Monitoring the execution of the quota positions granted in order to    ensure effective transfer of technology to Nigerians and eventual    indigenization of the positions occupied by the Expatriates. </li>
</ol>
<h5>GENERAL RULES APPLICABLE TO ALL COMPANIES</h5>
Every enterprise, desirous of obtaining Business Permit and Expatriate    Quota, is to submit an application to that effect to the Federal Ministry of    Internal Affairs on Form T/1 designed for that purpose Companies are,    however, to note that emphasis would be placed on employment of Nigerians to    understudy the foreign experts for the purpose of training them, to enable    the understudies acquire relevant skills for the eventual take-over of the    expatriate quota positions. Renewal of quotas granted will not be automatic    but considered on merit based on submission of the required documents, as    specified in paragraph 3 (C) of this Handbook. <br />
<br />
<h5>GRANT OF BUSINESS PERMIT / EXPATRIATE QUOTA POSITION </h5>
All applications by companies for Business Permit and expatriate Quota    should be accompanied by the following documents:
<ol type="A">
  <li>    BUSINESS PERMIT
    <br />
    For only Joint Venture and wholly Foreign owned Companies.
    <ol type="i">
      <li>  Completed Immigration Form T/1</li>
      <li>Certificate of Incorporation</li>
      <li>Memorandum and Article of Association</li>
      <li>Feasibility Report (should be certified or registered with Corporate    Affairs commission (CAC)</li>
      <li>Corporate Affairs Commission’s Form CAC 2.3 ^ 2.5 of CAC C02 &amp;    C07</li>
      <li>Joint Venture Agreement for partnership venture between Nigerian and    foreigners (original to be presented for sighting)</li>
      <li>Company’s Current Tax Clearance Certificate (Original to be presented    for sighting)</li>
      <li>Lease agreement for C of O for opening premises (original to be    presented for sighting)<br />
      </li>
    </ol>
    <br />
    NOTE: <br />
    For Business permit, the Authorized share capital must not be less than N10    million in respect of each Company.<br />
    <br />
  </li>
  <li>GRANT OF EXPATRIATE QUOTA ONLY
    <br />
    Same documents as in (a) above plus:
    <ol type="I">
      <li> Evidence of imported machinery, such as, Form M, Proforma invoice,    Shipping documents and Clean Certificate of Inspection issued by Government    appointed Pre-shipment Inspection Agents. </li>
      <li>License / Permit / Certificate from relevant Government Agencies /    department / ministries for the operation or execution of project if company    is engaged in oil services, health care services, fishing, mining,    constructions (Work Registration Board), etc (original to be presented for    sighting)</li>
      <li>Evidence of work at hand, its duration and value attached to the    contract(s) if the company is engaged in building, civil engineering,    construction, etc (original to be presented for sighting) </li>
      <li>Proposed annual salaries to be paid to the expatriates to be recruited    indicating designation, names, jobs description and qualifications (CV and    copies of credentials of expatriate to be attached).</li>
    </ol>
  </li>
  <li>RENEWAL OF EXPATRIATES QUOTA <br />
    <ol type="i">
      <li>Completed Immigration Form /T2</li>
      <li>Corporate Tax Clearance Certificate (original to be presented for    sighting)</li>
      <li>Current Tax Clearance certificate of the expatriate (original to be presented    for sighting)</li>
      <li>Expatriate Quota Returns for the three months preceding the date of    approval.</li>
      <li>Detailed Training programme for Nigerians</li>
      <li>List of Nigerians understudying expatriate on prescribed formats    showing date employed, qualification, etc.</li>
      <li>List of Nigerian Senior/Management Staffs showing names,    designations, qualifications, salaries per annum</li>
      <li>Certificate Current Audited Accounts</li>
      <li>Annual income tax Clearance certificate of the Expatriate staff    (original to be presented fro sighting);<br />
        <br />
      </li>
    </ol>
  </li>
  <li>ADDITIONAL EXPATRIATE QUOTA<br />
    Same documents as for renewal of expatriate quota plus:
    <ol>
      <li>Evidence of acquired machinery or expansion of company is engaged in    manufacturing in the form of Clean Certificate of inspection (CCI) issued by    Government appointed Pre shipment inspection Agents</li>
      <li>Evidence of new contract if Company is engaged in    construction/engineering and oil (original to be presented for Sighting)    see  b (ii) for guidance</li>
      <li>Evidence of acquired farm / factory if Company is engaged in    agro-allied business (original to be presented for sighting).<br />
      </li>
    </ol>
  </li>
  <li>RESTORATIONS OF QUOTA<br />
    In applying for restoration, there must be reasons and proof for the    reactivation and revitalizing the company i.e. previously depressed economy,    increased demand for the Company’s product / services and acquisition of new    machinery i.e. Clean Certificate of Inspection (CCI) issued by Government    appointed Pre-shipment inspection AGENTS.<br />
  </li>
  <li>REDESIGNATION OF QUOTA<br />
    In case where a Company has difficulty in filling or owing to exigency of    Operations, it is at liberty to apply for the re-designation of the affected    position and this will be considered, purely on its own merit.<br />
  </li>
  <li>PERMANENT UNTIL REVIEWED QUOTA – (P.U.R)<br />
    The main reason for the granting of P.U.R quota slot for which a    Certificate would be issued, is to enable the foreign subscriber(s)    adequately protect their interest and to give them a sense of greater    commitment. The criteria and documents required for P.U.R are:<br />
    <ol type="i">
      <li>Minimum share capital should be N10 Million </li>
      <li>Appreciable net profit of which not less than N2 million has been paid    as Corporate Tax (original to be presented for sighting)</li>
      <li>Certified and Detailed Audited Account</li>
      <li>Certificate of Incorporation</li>
      <li>Monthly Returns of Expatriate Quota</li>
      <li>Company Organisation Structure</li>
      <li>Individual Income tax Clearance Certificate of the expatriates    (original to be presented for sighting).<br />
        <br />
      </li>

    </ol>
    NOTE: <br />
    Other factors that would also be considered when considering P.U.R request    include: <br />
    <ol type="i">
      <li>political / policy direction of Government.</li>
      <li>company’s area of business to fall within priority sectors of the    economy.</li>
      <li>Evidence that P/U.R would guarantee Technology transfer.</li>
      <li>Company should have large quota portfolio and Corresponding share    holding as an added qualification.</li>
    </ol>
  </li>
  <li>QUOTA FOR INDIGENOUS COMPANY (FULLY-OWNED BY NIGERIANS<br />
    Same documents as in (b) above except Joint Venture Agreement
    <br />
  </li>
  <li>WHOLLY-OWNED FOREIGN COMPANY <br />
    <ol type="i">
      <li>Same documents as in (a) &amp; (b) above except Joint venture Agreement </li>
      <li>Share Capital must not be less than N10 Million. <br />
        <br />
      </li>
    </ol>
  </li>
  <li>AMENDMENT OF BUSINESS PERMIT<br />
    <br />
    <ol type="i">
      <li>Board Resolution on the changes in either the composition of the Board    of directors, location of business or line of activities duly registered with    Corporate Affairs Commission</li>
      <li>Extract of Minute of Board of Directors Meeting showing decision taken    and attendance.</li>
      <li>Evidence of resignation / appointment of Directors (old &amp; new)    where applicable</li>
      <li>Company Current Tax Clearance Certificate (original to be presented    for sighting);<br />
        <br />
        <u>NOTE:</u> <br />
        Board Resolution should indicate names of Directors present at the meeting    and must be fully signed by the Chairman and Company Secretary.<br />
        <br />
      </li>
    </ol>
  </li>
  <li>FEES PAYABLE FOR SERVICES RENDERED<br />
    From time to time, fees payable for services rendered are subject to    review. The currently reviewed fees are as follows:<br />
    <table border="0" cellspacing="0" cellpadding="5" width="100%">
      <tr>
        <th>S/N</th>
        <th>DESCRIPTION</th>
        <th>COST (N)</th>
      </tr>
      <tr>
        <td>1.</td>
        <td>Processing fees</td>
        <td align="right">25,000</td>
      </tr>
      <tr>
        <td>2.</td>
        <td>Grant of Establishment Quota(Per Slot)</td>
        <td align="right">10,000</td>
      </tr>
      <tr>
        <td>3.</td>
        <td>Renewal of Quota Position (Per Slot)</td>
        <td align="right">5,000</td>
      </tr>
      <tr>
        <td>4.</td>
        <td>Additional quota (Per Slot) </td>
        <td align="right">10,000</td>
      </tr>
      <tr>
        <td>5.</td>
        <td>Stay of Action </td>
        <td align="right">5,000</td>
      </tr>
      <tr>
        <td>6.</td>
        <td>Grant of Business permit</td>
        <td align="right">50,000</td>
      </tr>
      <tr>
        <td>7.</td>
        <td>Amendment of Business permit</td>
        <td align="right">25000</td>
      </tr>
      <tr>
        <td>8.</td>
        <td>Regarding of quota (per slot)</td>
        <td align="right">10,000</td>
      </tr>
      <tr>
        <td>9.</td>
        <td>Appeal Processing fee </td>
        <td align="right">10,000</td>
      </tr>
      <tr>
        <td>10.</td>
        <td>Restoration of Lapsed quota</td>
        <td align="right">55,000</td>
      </tr>
      <tr>
        <td>11.</td>
        <td>upgrading of quota to P.V.R (Per Slot)</td>
        <td align="right">$10,000</td>
      </tr>
      <tr>
        <td>12.</td>
        <td>Re-designation of  P.U.R slot    (GM  to MD or MD to GM etc)</td>
        <td align="right">$10,000</td>
      </tr>
      <tr>
        <td>13.</td>
        <td>Detagging / Extension of quota(Per Slot)</td>
        <td align="right">10,000</td>
      </tr>
      <tr>
        <td>14</td>
        <td>Revalidation of Lapsed quota(Per Slot)</td>
        <td align="right">10,000</td>
      </tr>
      <tr>
        <td>15</td>
        <td>Penalty for late submission of Renewal of private license</td>
        <td align="right">150,000</td>
      </tr>
    </table>
  </li>
</ol>



<h4>CERPAC</h4>

<p><strong>What    Is CERPAC ?</strong> <br />
  CERPAC IS THE ACRONYM FOR COMBINED EXPATRIATE RESIDENCE PERMIT AND ALIENS    CARD.<br />
  <br />
  <strong>Who qualifies</strong><br />
  Expatriates resident or working in Nigeria<br />
  <br />
</p>
<ol type="1">
  <li>INTRODUCTION AND REQUIREMENTS<br />
    CERPAC FORMS:<br />
    <ol>
      <li>CR (Concessionary) forms are meant for Missionaries, Students,  on ECOWAS African Nationals and    Cameroonians</li>
      <li>AO forms are for other Nationals and persons with special  Immigrant status.</li>
      <li>AR forms are for    Exempted Persons (Diplomats, Government officials, NGO’s and Niger-wives).
        <ol type="a">
          <li>AR forms are issued at the Immigration Service
          quarters and State Commands all    over the Federation.</li>
          <li>CR &amp; AO forms are obtainable from Afribank Nigeria <font class="font0">Plc.</font></li>
        </ol>
      </li>
    </ol>
    <p><strong> CERPAC GUIDELINES</strong><br />
    </p>
    <ol>
      <ol type="1" start="4">
        <li> The CERPAC form as endorsed is     valid for three (3) months from the date of purchase.</li>
        <li>Two forms are endorsed with this Booklet Guidelines as sample at the    point of collection of the CERPAC pack.
          <ol type="a">
            <li> Please read through both    forms carefully before completing them.</li>
            <li>Use a black ball point pen and    complete the forms in capital letters .</li>
          </ol>
        </li>
      </ol>
    </ol>
  </li>
  <li><b>Please Note the following:</b>
    <ol type="i">
      <li>The envelope containing this CERPAC Pack should be preserved and used    in submitting your forms and documents after completion.</li>
      <li>All fees paid to obtain forms are not refundable.</li>
      <li>In the event of loss of form(s), replacement will be treated as a new    issue and will attract the original fees.</li>
      <li>Any erasures, cancellation, or defacement of any sort shall render the    form(s) void.</li>
      <li>Any form(s) voided as in 2.4 (above) must be returned as a complete set    to the bank of purchase for replacement. Such replacement will attract a    service charge and pack cost of N2,500(Two Thousand, Five Hundred Naira)    payable to the Bank.</li>
    </ol>
  </li>
  <li>
    <ol type="i">
      <li>Three (3) Nos. “2 x 2” passport     photographs on white background should be submitted with the completed    forms.  One of the photographs should    be attached to the bottom right-hand side of the CERPAC form titled “Receipt    and Temporary Card </li>
      <li>You are required to append your signature beside your photograph    attached on the form (as in 3.1 above)</li>
      <li>Enclose also one (1) No. facial portrait measuring 4' x 3' (inch) white    background </li>
    </ol>
  </li>
  <li>All completed forms and documents should be submitted to the Immigration    State Command where your file(s) is (are) located, for Verification and    Clearance.</li>
  <li>At the time of submitting the completed forms and documents, you will be    required to present your original Passport for verification of the documents    presented.</li>
  <li>When your forms and documents are received, verified and endorsed by the    relevant Immigration State Command, an Immigration/CERPAC Stamp will be    applied to your Receipt/Temporary Card. Please ensure that the Receipt/    Temporary Card is authenticated and returned to you. You will also be    required to physically come into a CERPAC office for the capture of your    Digital Personal Identification. <br />
    Special Immigrant Status
    <ol type="i">
      <li>Special Immigrant Status: These are expatriates married to Nigerian    Women </li>
      <li>Formal letter of request from Nigerian wife accepting Immigration    Responsibility.</li>
      <li>Formal letter of request from the husband.</li>
      <li>Photocopies of the first five (5) pages of wife’s ria Standard    passport.</li>
      <li>Photocopy of marriage Certificate.</li>
      <li>Duly completed form IMM22 in triplicate copies with three (3) recent    passport photographs.</li>
      <li>Applicant’s National Passport.</li>
    </ol>
  </li>
  <li>CERPAC FEES
    <table width="100%">
      <tr>
        <th>S.No.</th>
        <th>CATEGORY FORM ISSUED</th>
        <th>CERPAC 1 YR</th>
        <th>CERPAC 2 YRS</th>
      </tr>
      <tr>
        <td>1</td>
        <td>MISSIONARY CR</td>
        <td><div align="right">$200</div></td>
        <td><div align="right">$400</div></td>
      </tr>
      <tr>
        <td>2</td>
        <td>STUDENTS CR </td>
        <td><div align="right">$200</div></td>
        <td><div align="right">$400</div></td>
      </tr>
      <tr>
        <td>3</td>
        <td>NON ECOWAS AFRICAN NATIONALS CR </td>
        <td><div align="right">$200</div></td>
        <td><div align="right">$400</div></td>
      </tr>
      <tr>
        <td>4</td>
        <td>CAMEROONIANS</td>
        <td><div align="right">$240</div></td>
        <td><div align="right">$480</div></td>
      </tr>
      <tr>
        <td>5</td>
        <td>OTHERS AO</td>
        <td><div align="right">$350</div></td>
        <td><div align="right">$700</div></td>
      </tr>
      <tr>
        <td colspan="4">Payment in Naira Equivalent.</td>
      </tr>
    </table>
  </li>
  <li><strong>Exempted Persons</strong>: <br />
  Exempted Persons (Diplomats, Government Officials (GO’s), Niger-wives,    Non Governmental Organization (NGO’s) are to be issued CERPAC Gratis.</li>
  <li><strong>CERPAC validity</strong>:<br />
  The validity of CERPAC is two (2) years. </li>
  <li><strong>Requirements </strong>:<br />
    In addition to the above guidelines, the following are additional    requirements for the issuance of CERPAC in the various categories.
    <ol type="i">
      <li>Regularisation of Stay
        <ol type="a">
          <li>Application letter from the employer requesting Regularization of    stay and accepting Immigration          Responsibility (IR) on behalf of the expatriate.</li>
          <li>Letter of Appointment/Employment</li>
          <li>Acceptance of offer of Appointment/Employment.</li>
          <li>Form IMM22 with three (3) Passport –size photograph.</li>
          <li>Quota Approval</li>
          <li>Vetted Credentials</li>
          <li>Valid Passport with STR (Subject to Regularization) (Employment)    visa and photocopies of relevant pages.</li>
        </ol>
      </li>
      <li>REGULARIZATION OF FOREIGN STUDENTS AT OUR HIGHER  INSTITUTIONS OF LEARNING
        <ul>
          <li>Formal letter of application for regularization from student</li>
          <li>Letter of recommendation from the Institution and acceptance of </li>
        </ul>
        Immigration responsibility<br />
        <ul>
          <li>A copy of his Admission letter</li>
          <li>A copy of letter of acceptance</li>
          <li>Evidence of payment of School fees.</li>
        </ul>
      Photocopy of relevant pages of the applicant's passport including the page    of relevant visa. </li>
      <li><b>EXEMPTED PERSONS</b>
        <ul>
          <li>Diplomats</li>
          <li>Formal letter of request from Embassy</li>
          <li>National Passport of Diplomat with diplomatic visa endorsement and    photocopies of relevant pages.</li>
          <li>Letter of Accreditation from the Ministry of Foreign Affairs.</li>
          <li>Duly completed form IMM22 in triplicates with three (3) recent Passport    photographs.</li>
        </ul>
      </li>
      <li>GOVERNMENT OFFICIALS
        <ul>
          <li>Government Officials (GO’s)</li>
          <li>Formal letter of request from employer accepting Immigration    Responsibility (I.R.)</li>
          <li>Personal letter of request from applicant.</li>
          <li> Letter of offer of employment</li>
          <li>Personal letter of acceptance of offer </li>
          <li>Duly completed form IMM22 in triplicate copies with three (3) recent    Passport photograph.</li>
          <li>National Passport of applicant with GO visa Endorsement and    photocopies of relevant pages of passport. </li>
          <li>Photocopies of vetted credentials</li>
          <li></li>
        </ul>
      </li>
      <li>NIGER-WIVES
        <ul>
          <li>Niger-wives:</li>
          <li>Formal letter of request from Nigerian husband accepting Immigration    Responsibility.</li>
          <li>Formal letter of request from the wife.</li>
          <li>Photocopies of the first five (5) pages of husband’s Nigeria    Standard passport.</li>
          <li>Photocopy of marriage Certificate.</li>
          <li>Duly completed form IMM22 in triplicate copies with three (3) recent    passport photographs.</li>
          <li>Applicant’s National Passport.</li>
        </ul>
      </li>
      <li>NON GOVERNMENTAL ORGANIZATIONS
        <ul>
          <li>Non Governmental Organizations (NGO'S)</li>
          <li>Formal letter of request from the NGO accepting Immigration    Responsibility I.R.</li>
          <li>Personal letter of request from applicant. </li>
          <li>Letter of offer of employment</li>
          <li>Personal letter of acceptance of offer.</li>
          <li>Duly completed form IMM22 in triplicates with three (3) recent    passport photographs.</li>
          <li>National Passport of applicant with STR visa endorsement and    photocopies of relevant pages of passport.</li>
          <li>Letter from National Planning Commission (NPC).</li>
        </ul>
      </li>
      <li>OTHERS: FREE ZONES (EPZ,ETC,PTZ,GTZ)
        <ol>
          <li>Formal letter of request from the Free Zones accepting I.R.</li>
          <li>Personal letter of request from applicant.</li>
          <li>Letter of offer of employment.</li>
          <li>Personal letter of acceptance of offer.</li>
          <li>Duly completed form IMM22 in triplicate with three (3)passport    photographs</li>
          <li>National Passport of applicant with STR visa endorsement and    photocopies of relevant pages of passport.</li>
          <li>Photocopy of Certificate of Grant of Status</li>
        </ol>
      </li>
      <li> RENEWAL/REPLACEMENT OF CERPAC<br />
        <ol>
          <li>Renewal/Replacement</li>
          <li>Application letter from the Employer requesting for    renewal/replacement of Residence Card and accepting I.R. </li>
          <li>Quota approval</li>
          <li>The Expiring Resident Card (Original)</li>
          <li>Valid Passport and photocopies of relevant pages.<br />
          </li>
        </ol>
      </li>
      <li>CHANGE OF EMPLOYMENT
        <ol>
          <li> Application from the new Employer requesting change
          of employment and accepting Immigration Responsibility I.R.</li>
          <li>Letter of Appointment/Employment</li>
          <li>Acceptance of offer of Appointment/Employment.</li>
          <li>Letter of no objection from the previous Employer.</li>
          <li>Quota Approval</li>
          <li>Valid Passport.<br />
          </li>
        </ol>
      </li>
      <li>RE-DESIGNATION
        <ol>
          <li>Application letter from Employer requesting Re-designation and    accepting Immigration Responsibility I.R.</li>
          <li>Quota Approval</li>
          <li>Current Residence Immigration Responsibility (Original)</li>
          <li>Valid Passport and photocopies of relevant pages.<br />
          </li>
        </ol>
      </li>
      <li>DEPENDANTS
        <ol>
          <li>Application  from the    Employer of the Principal Immigrant requesting Regularization or    Renewal.</li>
          <li>Valid Passport (with STR visa) and photocopies or relevant pages in    case of Regularisation.</li>
          <li>Valid Passport of the Principal Immigrant.</li>
          <li>Current CERPAC of the Principal Immigrant.</li>
          <li>Photocopy of Marriage Certificate case of the wife of the Principal    Immigrant)</li>
          <li>Form IMM22 with three (3) Passport photographs (in the case of    Regularisation).<br />
          </li>
        </ol>
      </li>
      <li>ALIENS REGISTRATION
        <ol>
          <li> Application from the Employer indicating acceptance of Immigration    Responsibility I.R. </li>
          <li>Valid Passport and photocopies of relevant pages.</li>
          <li>Letter of Appointment.</li>
          <li>Quota Approval of the Company.</li>
          <li>Photocopy of the Applicant’s credentials</li>
          <li>Duly completed Forms IMMA.21 and E.20 </li>
          <li>Four (4) Passport-size photographs.</li>
          <li>Current CERPAC (where applicable) </li>
          <li>Physical Presence of the applicant.<br />
          </li>
        </ol>
      </li>
    </ol>
  </li>
</ol>
<p><br />

<h5>WHAT IS CERPAC GREEN CARD ?</h5>
<p> The CERPAC GREEN CARD is a bonafide document that allows a non-Nigerian to    reside in Nigeria and carry out an approved activity as specified in the    permit, or to accompany a resident or citizen of Nigeria as a dependant.    Possession of a valid CERPAC GREEN CARD does not exempt the holder from    having a valid entry or re-entry permit/visa. Holders of resident status in    Nigeria who are proceeding on leave or temporary duty outside Nigeria should    apply for re-entry permit/visa prior to their departure from Nigeria.<br />
The validity is Two (2) years.</p>
<h5> WHAT IS CERPAC BROWN CARD?</h5>
<p>Every Alien resident in Nigeria or visiting with the intention to remain in    Nigeria in excess of 56 days and crew members leaving their ship and staying    ashore in excess of 28 days are required by law to register. Unlike the    CERPAC GREEN CARD, the BROWN CARD is, essentially, a movement chart.<br />
  <br />
Under the new CERPAC Scheme, registration is valid for two years, after    which application for revalidation must be made. </p>

<a class="nameAnchor" name='ecowas'>
<h3>ECOWAS</h3></a>

<h5>What is ECOWAS ?</h5>
<p>It is the acronym for Economic Community of West African States which came into being in May 1975.</p>
<p><b>ECOWAS Treaty:</b><br />
To foster the economic integration and shared development  of West African sub-region.<br />
<br />
ECOWAS member States:<br />
- Benin<br />
- Burkina Faso<br />
- Cape Verde<br />
- Cote d'ivoire<br />
- Gambia<br />
- Ghana<br />
- Guinea<br />
- Guinea Bissau<br />
- Liberia<br />
- Mali<br />
- Niger<br />
- Nigeria<br />
- Senegal<br />
- Sierra Leone<br />
- Equatorial Guinea<br />
- Togo<br />
<br />
ECOWAS PROTOCOL:<br />
Free Entry within 90days<br />
<br />
Right of Residence:<br />
- Right of establishment of business
<p><strong>
  Free Entry within 90days
</strong>
<p> <strong>Who qualifies?</strong><br />
All Nationals of Countries that are signatories to the ECOWAS treaty of May
1975.
<p>  <strong>Requirements</strong><br />
- Valid travel documents e.g. Country's Passport or ECOWAS Travel Certificate.<br />
- Admission through approved port of entry.<br />
<br />
<strong>Validity</strong><br />
- 90 days

<h5>Right of Residence</h5>
<strong>Who qualifies ?</strong><br />
All Nationals of Countries that are signatories to the ECOWAS treaty of May 1975.<br />
<br />
<strong>Requirements</strong><br />
- Valid travel documents e.g. Country's Passport or ECOWAS Travel Certificate.<br />
- Admission through approved port of entry.<br />
- Procurement of Residence Card, valid for 5 years subject to passport validity.<br />
<br />
<h5>Right of Establishment of Business</h5>
Who qualifies ?<br />
All Nationals of Countries that are signatories to the ECOWAS treaty of May
1975.<br />
<br />
<h5>Requirements</h5>
<ol>
<li>Valid travel documents e.g. Country's Passport or ECOWAS Travel Certificate.</li>
<li>Admission through approved port of entry.</li>
<li>Procurement of Residence Card, valid for 5 years subject to passport  validity.</li>
<li>Registration of business with the Corporate Affairs Commission in   accordance with Companies &amp;
  Allied matters act of 1990
</li>
</ol>

<h4>Passports Types</h4>

<ul>
  <li>Diplomatic Passport  (Red cover)</li>
  <li><a href='#official_passport'>Official Passport (Blue cover)</a></li>
  <li><a href='#standard_nigerian_passport'> Standard Nigeria Passport (Green cover)</a></li>
  <li> Pilgrims Passport (Color varies from year to year)</li>
  <li><a href='#seamans_book_passport'>Seaman's Book (Certificate of Identity) (Maroon cover)</a></li>
</ul>
<p>  Note: </p>
<ul>
  <li> Acknowledgment slip &amp; payment
  receipt plus 2 recent photographs are applicable in all cases </li>
  <li> Print out copy of duly completed
  application form </li>
  <li> Take printed and signed
    application forms to Passport office / Embassy / High Commission for further
  processing </li>
  <li> A guarantor must attach the
  following documents
    <ol type="a">
      <li> Photocopy of Data page of Nigerian standard Passport </li>
      <li> Driving License or National Identity Card </li>
    </ol>
  </li>
</ul>


<p></p>
<hr />


<a class="nameAnchor" name="official_passport"><h3>Official Passport</h3></a>
<p>REQUIREMENTS FOR OFFICIAL PASPORT
<ol type="1">
 	<li>Letter of introduction from appropriate State Government, Federal
  Government Ministry / Organization.</li>
    <li>Marriage certificate where applicable </li>
    <li>Police report in case of lost passport</li>
    <li>Letter of appointment / last promotion.</li>
    <li>Submit application with supporting documents to passport office /
    Embassy / High commission      </li>
</ol>
  <u>Note:</u> <br />
  <ul>
  <li>Acknowledgment slip &amp; payment receipt plus 2 recent photographs are applicable in all cases</li>
  <li>Print out copy of duly completed application form</li>
  <li>Take printed and signed application forms to Passport office / Embassy / High Commission for further processing</li>
  <li>A guarantor must attach the following documents
    <ol type="a">
      <li>Photocopy of Data page of Nigerian standard Passport </li>
      <li>Driving License or National Identity Card  </li>
    </ol>
  </li>
  </ul>

<a  class="nameAnchor" name='standard_nigerian_passport'><h3>Standard Nigeria Passport </h3></a>
<b>Who qualifies?</b><br />
  All Nigerians by birth , naturalization and registration. This is the most commonly used travel document in Nigeria. <br />

<br /> Requirements for Obtaining a Standard Nigerian Passport
<ol type="1">
<li>  Local Government letter of  identification.</li>
<li> Birth certificate / age declaration.</li>
<li>2 recent colour passport photographs</li>
<li>Guarantor's form sworn to before a commissioner of Oaths / Magistrate  /  High Court Judge</li>
<li>Parents' letter of consent for minors under 16 years</li>
<li>Marriage certificate where applicable</li>
<li>Police report incase of lost passport</li>
<li>Submit application with supporting documents to passport office /  Embassy / High commission <br />
</li>
</ol>
  <u>Note:</u> <br />
  <ul>
  <li>
 Acknowledgment slip &amp; payment receipt plus 2 recent photographs are applicable in all cases </li>
  <li>Print out copy of duly completed
    application form</li>
  <li>Take printed and signed
    application forms to Passport office / Embassy / High Commission for further
    processing</li>
  <li>A guarantor must attach the
    following documents
    <ol type="a">
      <li>Photocopy of Data page of Nigerian standard Passport</li>
      <li>Driving License or National Identity Card  </li>
    </ol>
  </li>
  </ul>
<a  class="nameAnchor"  name="seamans_book_passport"><h3>Seaman's Book Passport </h3></a>
<b>Who qualifies</b><br />
  This is issued to Nigerians who work on ocean going vessels or watercrafts <br />
  <br />
  REQUIREMENTS FOR SEAMAN'S PASSPORT <br />
<ol type="1"><li>  Local Government letter of identification </li>
  <li>Birth certificate / age declaration</li>
  <li>2 recent colour passport photographs</li>
  <li>Submit application with supporting documents to passport office /
    Embassy / High commission <br />
  </li>
</ol>
 
  <u>Note:</u>
  <ul>
  <li>Acknowledgment slip &amp; payment receipt plus 2 recent photographs are applicable in all cases</li>
  <li>Print out copy of duly completed
    application form </li>
  <li>Take printed and signed
    application forms to Passport office / Embassy / High Commission for further
    processing </li>
  <li>A guarantor must attach the
    following documents
    <ol type="a">
      <li>Photocopy of Data page of Nigerian standard Passport </li>
      <li>Driving License or National Identity Card </li>
    </ol>
  </li>
</ul>
<a  class="nameAnchor"  name="ecowas_travel_certificate">
<h3>ECOWAS Travel Certificate</h3></a>
<p><strong>Who qualifies</strong> <br />
  All Nationals of Countries that are signatories to the ECOWAS Treaty<br />
  <br />
  <strong>Validity</strong><br />
  It is valid for travel within the ECOWAS Sub-region<br />
  <br />
The ECOWAS Travel Certificate serves  dual purpose, namely</p>
<dl>
<dt>(a) Passport:</dt>
<dd>It is a recognized travel document valid for journeys within the Sixteen (16)    Countries of ECOWAS </dd>
<dt>(b) Form of Identity:</dt>
<dd> This proves the identity of the holder  a community citizen. It can be held concurrently with the  National Passport. </dd>
</dl>
<br />
  Requirements for Issuance of ECOWAS Travel Certificate</p>
<ol>
  <li>  Duly completed application form </li>
  <li> Three(3) recent coloured (4x4 cm)
    passport photographs </li>
  <li> Evidence of age (birth certificate
    or statutory declaration of age) </li>
  <li> Letter of Introduction from Employer
    (for salaried workers only) </li>
  <li> Letter of confirmation of Nigerian
    Citizenship from applicant's Local    Government Chairman. </li>
</ol>
<p>  Students and Trainee applicants
  shall obtain letters of introduction from   the heads of their institutions
  accepting Immigration Responsibility (IR) <br />
  Any other document which might be
  required by any locality as evidence    of Nigerian Citizenship. <br />
  <br />
  <strong>Validity</strong><br />
  A Travel Certificate shall be valid for two (2) years and shall be
  renewable for a further period of two (2) years.<br />
</p>

</div>
