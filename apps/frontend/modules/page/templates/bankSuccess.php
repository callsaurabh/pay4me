<div class="innerWrapper">
<?php  use_helper('a') ?>
<?php  include_partial('leftBlock', array('title' => 'Bank' )) ?>

<div id="wrapperInnerRight">
      <div id="innerImg">
	  <?php
      a_slot('contact_us', 'aImage',array('toolbar' => 'basic', 'global' => true, 'width' => 600, 'height' => 242, 'border' => 0)); ?>
	 </div>
     <div class="faqstxt">
<?php //a_slot('contact_us_text', 'aRichText',array('toolbar' => 'basic', 'global' => true));

if (count($bankListArray) > 0) {
    ?>
   
<div style="float:right; display:block;padding-bottom:20px;">
    <div class="search_bank">
    <a href='javascript:void(0)' onclick="open_win('<?php echo url_for("paymentProcess/bankBranchSearchIndex");?>')">
    <?php echo __('Search Nearest Bank Branch')?>
    </a>
    </div>
    </div>
 <table width='100%'>
<tr><td><strong> Bank name </strong></td> <td><strong> Bank Logo</strong></td> </tr>
    <?php
            foreach ($bankListArray as $key => $val) {
                echo "<tr>";
                $bankArray[$val['id']] = $val['bank_name']; 

                echo "<td width='40%'>".$val['bank_name']."</td>";
 
                echo "<td>".image_tag('/images/'.$val['acronym'].'.jpg',array('title'=>"{$val['acronym']}", 'alt'=>"{$val['acronym']}",'width'=>'50px','height'=>'50px'))."</td>";
                
                echo "</tr>";
            }
     echo "</table>";
        }
?>
<p></p>
</div>
<div id="slideControl"></div>
      <div class="clearfix"></div>
    </div>
</div>
<script language="JavaScript">
   function open_win(url_add)
   {
       window.open(url_add,'bankBranchSearchIndex', 'width=700,height=500, status=yes, location=yes, scrollbars=yes');
   }

</script>