<?php
use_javascript('jquery.sexyslider.min.js');
use_javascript('cufon-yui.js');
use_javascript('Bell_Centennial_Std_400.font.js');
use_javascript('jquery.keypad.password.js');
use_stylesheet('jquery.keypad.css');
?>
<script type="text/javascript">
<!--slider config-->
  $(document).ready(function() {
		$("#slides").SexySlider({
		width : 610,
		height : 288,
		delay : 4000,
		strips : 25,
		autopause : true,
		navigation: '#navigation',
		control : '#slideControl',
		effect : 'curtain' }
		);
	});
<!---->

    function open_win(url_add)
   {
       window.open(url_add,'bankBranchSearchIndex', 'width=700,height=500, status=yes, location=yes, scrollbars=yes');
   }


</script>
<!--font replacement  config-->
<script type="text/javascript">
Cufon.replace('h3', { fontFamily: 'Bell Centennial Std' });
//If you also include a library, you can add complex selectors like #content h1
</script>
<!-- -->
<?php

if($sf_user->hasFlash('error')){
  echo "<div id='flash_error'  class='error_list_user'><span>".sfContext::getInstance()->getUser()->getFlash('error')."</span></div>";
}
else if ($sf_user->hasFlash('notice')){
  echo "<div id='flash_notice' class='error_list' ><span>".sfContext::getInstance()->getUser()->getFlash('notice')."</span></div>";
}
else if(sfConfig::get('app_disable_ewallet_cards',false)){
    // Patch code to display message of disabling cards/ewallet payment mode options temporarily [WP076]
    echo "<div id='flash_error'  class='error_list_user'><span>
    ".sfConfig::get('app_message_disable_ewallet_cards')."
    </span></div>";
}

    //PAYM-2032
    include_partial('global/gplusLogin');   
    $authURL = $_SESSION['authURL'];
    //End of PAYM-2032
    
?>

<div id="wrapperLeft">
  <div id="slides"> <?php echo image_tag('/img/new/slide_01.jpg',array('alt'=>'', 'width' => 610, 'height' => 288)); ?> <?php echo image_tag('/img/new/slide_02.jpg',array('alt'=>'', 'width' => 610, 'height' => 288)); ?> <?php echo image_tag('/img/new/slide_03.jpg',array('alt'=>'', 'width' => 610, 'height' => 288)); ?> <?php echo image_tag('/img/new/slide_04.jpg',array('alt'=>'', 'width' => 610, 'height' => 288)); ?> </div>
  <div id="slideControl"></div>
  <div class="clearfix"></div>
  <div class="social_bg">
    <?php if(sfConfig::get('app_open_id_account_show')){?>
    <div class="blue_text">Register new user or sign in with :</div>
    <a href="javascript:void(0)" class="facebook"  style ="cursor:pointer" onClick="openOpenIdWindow('<?php echo url_for('openId/authlogin?openid_identifier=facebook'); ?>')">Facebook</a>
<!--    <a href="javascript:void(0)"  class="google" style ="cursor:pointer" onClick="openOpenIdWindow('<?php // echo url_for('openId/authlogin?openid_identifier=google'); ?>')">Google</a>-->
    <!-- PAYM-2032 -->
    <a href="javascript:void(0)"  class="google" style ="cursor:pointer" onClick="openOpenIdWindow('<?php echo $authURL ?>')">Google</a>
    <!-- End of PAYM-2032 -->
    
    <a href="javascript:void(0)" class="yahoo"  style ="cursor:pointer" onClick="openOpenIdWindow('<?php echo url_for('openId/authlogin?openid_identifier=yahoo'); ?>')">Yahoo</a>
    <?php }?>
  </div>

</div>
<div id="wrapperRight">
  <div class="redShowcase">
    <!--    <div class="top"></div>-->
    <div class="middle">
      <?php include_partial('sfGuardAuth/signinForm', array('bankname'=>$bankname,));?>
      <div class="clearfix"></div>
      <div align="center" class="register_btn">
      <?php if(!sfConfig::get('app_disable_old_ewallet_registration')){?>
       <a href='<?php echo url_for('signup/index')?>' target="_blank"> <?php echo image_tag('/img/new/button-register_now.png',array('alt'=>'Register Now', 'width' => 312, 'height' => 31)); ?> </a>
      <?php } ?>

       <div class="social_bg social-icons">
    <?php if(sfConfig::get('app_open_id_account_show')){?>
    <a href="javascript:void(0)" class="facebook"  style ="cursor:pointer" onClick="openOpenIdWindow('<?php echo url_for('openId/authlogin?openid_identifier=facebook'); ?>')">Facebook</a>
    <!--a href="javascript:void(0)"  class="google" style ="cursor:pointer" onClick="openOpenIdWindow('<?php // echo url_for('openId/authlogin?openid_identifier=google'); ?>')">Google</a-->
    <!-- PAYM-2032 -->
    <a href="javascript:void(0)"  class="google" style ="cursor:pointer" onClick="openOpenIdWindow('<?php echo $authURL ?>')">Google</a>
    <!-- End of PAYM-2032 -->
    <a href="javascript:void(0)" class="yahoo"  style ="cursor:pointer" onClick="openOpenIdWindow('<?php echo url_for('openId/authlogin?openid_identifier=yahoo'); ?>')">Yahoo</a>
    <?php }?>
  </div>

      </div>
    </div>
    <div class="search_bank_branch"><a href="javascript:void(0)" class="search_bank" onclick="open_win('<?php echo url_for("paymentProcess/bankBranchSearchIndex");?>')">Search Nearest Bank Branch</a></div>
    <!--        <div class="bottom"></div>-->
  </div>

</div>

<div class="clearfix"></div>
<div id="wrapMid">
  <div class="white_shadow_bg">
      <div class="blue_box"><a href="<?php echo url_for('page/howitWorks'); ?>" class="features_box">How it Works</a></div>
    <div class="orange_box"><a href="<?php echo url_for('page/howtoSignUp'); ?>" class="services_box">How to Sign Up</a></div>
    <div class="red_box"><a href="<?php echo url_for('page/howtoMakePayment'); ?>" class="faq_box">How to Make Payment</a></div>
  </div>
  <div class="clearfix"></div>
  <div class="blue_heading">Participating Banks</div>
  <div class="clearfix"></div>
  <div class="bank_bg">
    <marquee direction="right" scrollamount="3">
    <?php echo image_tag('/img/new/keystone_bank.gif',array('alt'=>'keystonebank', 'width' => 88, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/skye_bank.gif',array('alt'=>'skyebank', 'width' => 69, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/first_bank.gif',array('alt'=>'firstbank', 'width' => 53, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/enterprise_bank.gif',array('alt'=>'EnterpriseBank', 'width' => 87, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/unity_bank.gif',array('alt'=>'UnityBank', 'width' => 65, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/fidelity_bank.gif',array('alt'=>'FidelityBank', 'width' => 107, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/diamond_bank.gif',array('alt'=>'diamondbank', 'width' => 107, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/zenith_bank.gif',array('alt'=>'zenithbank', 'width' => 38, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/union_bank.gif',array('alt'=>'unionbank', 'width' => 123, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/stanbic_bank.gif',array('alt'=>'stanbicbank', 'width' => 168, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/eco_bank.gif',array('alt'=>'ecobank', 'width' => 90, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/access_bank.gif',array('alt'=>'accessbank', 'width' => 133, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/fcmb_bank.gif',array('alt'=>'fcmbbank', 'width' => 64, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/citi_bank.gif',array('alt'=>'citibank', 'width' => 74, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/uba_bank.gif',array('alt'=>'ubabank', 'width' => 74, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/fin_bank.gif',array('alt'=>'finbank', 'width' => 89, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/sterling_bank.gif',array('alt'=>'sterlingbank', 'width' => 113, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/mainstreet_bank.gif',array('alt'=>'mainstreetbank', 'width' => 150, 'height' => 48)); ?>
    <?php echo image_tag('/img/new/wemabank.png',array('alt'=>'wemabank', 'width' => 150, 'height' => 48, 'style' => 'width:95px;height:26px')); ?>
    </marquee>
  </div>
</div>
