<div class="innerWrapper">
<?php  use_helper('a') ?>
<?php  include_partial('leftBlock', array('title' => 'Contact' )) ?>

<div id="wrapperInnerRight">
      <div id="innerImg">
	  <?php
      a_slot('contact_us', 'aImage',array('toolbar' => 'basic', 'global' => true, 'width' => 600, 'height' => 242, 'border' => 0)); ?>
	 </div>
     <div class="faqstxt">
<?php a_slot('contact_us_text', 'aRichText',array('toolbar' => 'basic', 'global' => true)); ?>
<p></p>
</div>
<div id="slideControl"></div>
      <div class="clearfix"></div>
    </div>
</div>