<?php 
echo '<pre>';
//print_r($merchantInfo); die;
echo '</pre>';
	
?>
<div class="innerWrapper">
	<?php  use_helper('a'); ?>
	<div  class="innerWrapper_level_1">
		<div>
			<?php  include_partial('leftBlock', array('title' => 'Merchant' ))?>
		</div>
		<div>
			<?php
			include_partial ( 'merchantServicesBlock', array () );
			?>
	  	</div>
	</div>
	<div style="float: right;display: inline-block;">
		<div id="wrapperInnerRight">
			<div class="merchant_accor">
				<ul>
					
					<?php 
						
					foreach ($merchantInfo as $categoryId => $categoryData)
					{
						
					?>
					<li style="display:inline-block">
						<ul>
							<li><?php echo $categoryData['name'];?></li>
						</ul>
						<div class="content">
						<?php 
						foreach($categoryData['merchant'] as $merchantId => $merchantData)
						{
							$logo_name = getMerchantLogo ( $merchantData ['name'] );
							
							
						?>
							<div class="logo">
              					<?php if (! empty ( $merchantData ['url'] ))
              							 {
              								if ($logo_name === "NA") 
												{
													echo "<a target='_blank' href=" . $merchantData ['url'] . ">NA</a>";
												} else {
														echo "<a target='_blank' href=" . $merchantData ['url']. ">" . $logo_name . "</a>";
												}
              					      	} else 
              					      		{
              					      			echo "NA";
              					      		}		?>
              					</br>
              					<b style="font-weight: bold;"><?php echo $merchantData['name'];?></b>
              					
								<div class="data">
									<ul>
                		
			              			<?php 
			              			
			              			if(isset($merchantData['service']) && is_array($merchantData['service']))
			              			{
										foreach($merchantData['service'] as $serviceId => $serviceData)
										{
									
										?>
											<li><?php echo $serviceData['name']; ?></li>
										<?php 
										}
			              			}
									?>
								</ul>  
				              </div>
            				</div>
						
						<?php 
						}
						?>
						</div>
					</li>
					<?php 
					}
					?>
					
				</ul>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  $(function(){

  	$('.merchant_accor ul li > ul li').first().addClass('down');
  		$('.merchant_accor ul li div.content').filter('.content').hide().eq(0)
					.show();
    $('.merchant_accor ul li > ul li').click(function(){


        	$this = $(this);
            $this.parent().parent().siblings().children('ul').children().removeClass('down');
            $this.parent().parent().siblings().children('div.content').hide(400);
            $this.toggleClass('down').parent().next().slideToggle(400);
        })
  })
</script>