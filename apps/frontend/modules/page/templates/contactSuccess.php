<h1>NIS Contact Address</h1>

<div id="contactPage" class="XY20" style='line-height:20px;'>
  <p><b>Nigeria Immigration Service</b><br />
    Nnamdi Azikiwe Int’l Airport Road,<br />
    Sauka, Abuja.<br />
    P.M.B.    38, Garki, Abuja.<br />

    Phone:+234-9-7806771<br />
    Email:  <?php echo mail_to('info@immigration.gov.ng');?><br />
    Website: <?php echo link_to('www.immigration.gov.ng','http://www.immigration.gov.ng');?><br />
  </p>
</div>
