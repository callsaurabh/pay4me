<h1>Frequently Asked Questions (FAQ)</h1>

<div id="faqList" class="XY20">
  <ol tyle="1">
    <li>
      <div class='faQ'>How do I make an application for a passport?</div>
      <div class="faA">Simply hover the mouse pointer over the <i>Passports</i>  link on the menu bar and Click on the appropriate passport application type.<br />
      Then select an appropriate option form the dropdown list on the <i>'Select Passport Type'</i> page.</div>
    </li>
    <li>
      <div class="faQ">I keep clicking the submit button to submit my application but I can't.</div>
      <div class='faA'>Make sure that all boxes have been filled with the appropriate information check the form fields marked on the error panel displayed to know which fields have been omitted.</div>

    </li>
    <li>
      <div class="faQ">How do I add my photograph to the form?</div>
      <div class='faA'>The portal only accepts images that are 120pixels (width) by 140 pixels (height) in dimension. Make sure the image conforms to this size before uploading. To upload, click on the <b>browse</b>  button then go through your system to where the picture is saved and select it, click on <b>Open</b>. Click on <b>Upload Picture</b> button. If the picture size is ok, it will be displayed on the page.</div>

    </li>
    <li>
      <div class="faQ">How do I check my payment status? </div>
      <div class='faA'>Move the mouse pointer over <b>Passports</b> on the menu bar, and then click on <b>Application home</b> on the menu bar. Move the mouse pointer over the Services Link on the menu bar, on the drop down menu that appears, click on <b>Check Payment Status</b>. Enter your reference number and Application ID issued to you when you submitted your application. Choose the correct option for application type and click on <b>Search Record</b>. </div>

    </li>
    <li>
      <div class="faQ">I missed my interview date, please how do I reschedule another appointment?</div>
      <div class='faA'>Move the mouse pointer over <b>Passports</b> on the menu bar, and then click on <b>Application home</b> on the menu bar.  Move the mouse pointer over the <b>Processing</b> Link on the menu bar, on the drop down menu that appears, click on <b>Interview [reschedule]</b>. Enter your reference number and Application ID and click on <b>Search Record</b>. Scroll down to the bottom of the page and click on the <b>Re-schedule</b> button. Your application is redisplayed with a new interview date.</div>

    </li>
    <li>
      <div class="faQ">I could not print out my receipt and acknowledgement slip when I first applied. Can I still get them back later to print?</div>
      <div class='faA'>Yes. Move the mouse pointer over Passports on the menu bar, and then click on Application home on the menu bar. Move the mouse pointer over the Services Link on the menu bar, on the drop down menu that appears, click on View & Print [receipt]. Enter your reference number and Application ID and click on Search Record.  On the Passport Payment Receipt page scroll down to the bottom of the page and click on the receipt and acknowledgment slip links to view and print.</div>

    </li>
    <li>
      <div class="faQ">I completed my Passport application and also paid online but my payment status still shows not paid. What do I do? </div>
      <div class='faA'>If the immigration portal shows that your payment is not updated and you completed the payment procedure then try the steps below<br />
If your payment method is Interswitch Verve/Master Card Naira - go to Interswitch Quickteller, you can use the service to view payment made with the card. To use the service just follow the instruction prompts on the website. If payment is confirmed then Send an email to <?php echo mail_to('support@socketworksglobal.com');?>  with your Full name, reference number, application ID, payment method used and the Debit Card number (do not send your card PIN please) explaining the situation.
<br />For other payment types please also email the above address explaining the situation.
</div>

    </li>
    <li>
      <div class="faQ">How do I update an incomplete Passport Application?</div>
      <div class='faA'>Move the mouse pointer over Passports on the menu bar, and then click on Application home on the menu bar. Move the mouse pointer over the Processing Link on the menu bar, on the drop down menu that appears, click on [application completion].  Enter your reference number and Application ID and click on Search Record.</div>
    </li>
    <li>
      <div class="faQ">How do I update an incomplete Passport Application?</div>
      <div class='faA'>Move the mouse pointer over Passports on the menu bar, and then click on Application home on the menu bar. Move the mouse pointer over the Processing Link on the menu bar, on the drop down menu that appears, click on [application completion].  Enter your reference number and Application ID and click on Search Record.</div>

    </li>
    <li>
      <div class="faQ">don't know my reference number and application ID because I could not print my acknowledgment slip and/or receipt, how do I get them?</div>
      <div class='faA'>Send an email to <?php echo mail_to('support@socketworksglobal.com');?> with your full name, scanned passport photograph, email address, date of birth.
When making an application, if for any reason you cannot print out your acknowledgment slip and/or receipt then write down your reference number and application ID. You will need it for subsequent access to the portal.
</div>

    </li>
    <li>
      <div class="faQ">Help! I can't confirm my payment status - have multiple applications?</div>
      <div class='faA'>Applicants that have more than one application should check through all their applications to know which one payment was successful for.</div>
    </li>
    <li>
      <div class="faQ">How do i convert/upgrade from normal passport to e-passport ?</div>
      <div class='faA'>Move the mouse pointer over Passports on the menu bar, and then click on MRP-Passport on the menu bar. Move the mouse pointer over the Services Link on the menu bar, on the drop down menu that appears, click on [upgrade to e-passport].  Enter your reference number and Application ID and click on Search Record.</div>

    </li>
    <li>
      <div class="faQ">What are the different Passport types and how do I know which one to select?</div>
      <div class='faA'>There are 5 different passport types. Standard, Diplomatic, Official, Seamans, Pilgrims.
      <ul>
        <li><b>Standard</b> - This passport type is for the general public</li>
        <li><b>Diplomatic</b> - This passport type is for protocol officers in government and Government ambassadors.</li>
        <li><b>Official</b> - This passport type is for Government officials.</li>
        <li><b>Seamans</b> - This Passport Type is for Sailors.</li>
      
      </ul>
      <p><b>Fresh Passport : </b>If applying for a new passport and you have never previously owned one before then you need to fill the form for a Fresh Passport
Move the mouse pointer over Passports on the menu bar, and then click on Fresh Passport on the menu bar. Choose the appropriate passport type you are applying for and click on Start Application button.
For all the passport types except pilgrims' passport, you can have a passport renewal which is done when your current passport has expired.
      </p>
      
      </div>
    </li>
  </ol>
 
</div>
 <div class='helpSupport'>For Support Call: <?php echo sfConfig::get('app_support_contact_number');?>  or Email  <?php echo mail_to('support@socketworksglobal.com');?> </div>