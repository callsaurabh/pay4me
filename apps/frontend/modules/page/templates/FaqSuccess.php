<?php
use_javascript('jquery-1.4.2.min.js');
use_javascript('jquery-ui.min.js');
use_stylesheet('jquery-ui.css');
?>
<head>
    <script type="text/javascript">
        $(function() {
            $("#accordion").accordion({
                active : false,
                collapsible: true,
                autoHeight:false
            });
        });
    </script>
</head>
<body>
    <div class="innerWrapper">
        <?php use_helper('a') ?>
<?php include_partial('leftBlock', array('title' => 'Frequently Asked Questions')) ?>

        <div id="wrapperInnerRight">
            <div id="innerImg">
                <?php a_slot('faq', 'aImage', array('toolbar' => 'basic', 'global' => true, 'width' => 600, 'height' => 242, 'border' => 0)); ?>
            </div>
            <div class="faqstxt">
                <?php a_slot('faq_text', 'aRichText', array('toolbar' => 'basic', 'global' => true)); ?>
                <p></p>
            </div>
            <div id="slideControl"></div>
            <div class="clearfix"></div>
        </div></div>
</body>
