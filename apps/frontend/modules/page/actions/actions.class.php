<?php

/**
 * Pages actions.
 *
 * @package    symfony
 * @subpackage Pages
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class PageActions extends sfActions {
	public function executeIndex(sfWebRequest $request) {
		$this->getUser ()->getAttributeHolder ()->remove ( 'openId' ); // to unset openId session variable
		$this->getUser ()->getAttributeHolder ()->remove ( 'merchantRequestId' ); // to unset merchantRequestId session variable
		$this->getUser ()->getAttributeHolder ()->remove ( 'payType' ); // to unset payType session variable
		$this->bankname = '';
		if ($request->getParameter ( 'bankName' )) {
			$this->bankname = $request->getParameter ( 'bankName' );
		} else if ($this->getUser ()->getAssociatedBankAcronym ()) {
			$this->bankname = $this->getUser ()->getAssociatedBankAcronym ();
		}
		
		$pageName = $request->getParameter ( 'p' );
		
		if (isset ( $pageName ) && ! empty ( $pageName ) && $pageName != 'welcome') {
			$this->setTemplate ( $pageName );
		} else {
			$this->setTemplate ( 'welcome' );
		}
		
		if (! empty ( $this->bankname )) {
			if ($this->getUser ()->isAuthenticated ()) { // if user is already logged in send him to respective index page
				$group_name = $this->getUser ()->getGroupNames ();
				
				if (in_array ( sfConfig::get ( 'app_pfm_role_bank_branch_user' ), $group_name )) {
					$this->logMessage ( "Found a valid bank user" );
					return $this->redirect ( '@bankhome' );
				} else {
					$this->logMessage ( "Found a valid user" );
					return $this->redirect ( '@adminhome' );
				}
			} else {
				
				$this->setTemplate ( 'bankLogin' );
				$this->setLayout ( false );
			}
		}
		if ($this->getUser ()->isAuthenticated ()) {
			if (in_array ( sfConfig::get ( 'app_pfm_role_bank_branch_user' ), $this->getUser ()->getGroupNames () )) {
				return $this->redirect ( '@bankhome' );
			}
			if (in_array ( sfConfig::get ( 'app_pfm_role_ewallet_user' ), $this->getUser ()->getGroupNames () )) {
				// ewwallet users having credientials then redirect to ewalet home else logout user
				try {
					if (count ( $this->getUser ()->listCredentials () ) > 0) {
						return $this->redirect ( '@eWallethome' );
					} else {
						return $this->forward ( 'sfGuardAuth', 'signout' );
					}
				} catch ( Exception $e ) {
					return $this->forward ( 'sfGuardAuth', 'signout' );
				}
			} else {
				return $this->redirect ( '@adminhome' );
			}
		}
	}
	public function executeLoginStepOne(sfWebRequest $request) {
		$this->bankname = '';
		if ($request->getParameter ( 'bankName' )) {
			$this->bankname = $request->getParameter ( 'bankName' );
		} else if ($this->getUser ()->getAssociatedBankAcronym ()) {
			$this->bankname = $this->getUser ()->getAssociatedBankAcronym ();
		}
		
		// if ($request->isMethod('post')) {
		// $this->forward('sfGuardAuth', 'signin');
		// }
		$this->setLayout ( false );
	}
	// public function executeLogin(sfWebRequest $request) {
	// $this->bankname = $request->getParameter('bankName');
	// if($this->bankname) {
	// if($this->getUser()->isAuthenticated()) {//if user is already logged in send him to respective index page
	// $group_name = $this->getUser()->getGroupNames();
	//
	//
	// if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name)) {
	// $this->logMessage("Found a valid bank user");
	// return $this->redirect('@bankhome');
	// }
	// else {
	// $this->logMessage("Found a valid user");
	// return $this->redirect('@change_password');
	// }
	//
	// }else {
	//
	// $this->setTemplate('bankLogin');
	// $this->setLayout(false);
	// }
	// }
	//
	// }
	public function executePrintMe(sfWebRequest $request) {
		$this->setLayout ( 'pay4me_print' );
		$this->setTemplate ( 'printMe' );
	}
	public function executeErrorUser(sfWebRequest $request) {
		$this->setLayout ( 'pay4me' );
	}
	public function executeErrorAdmin(sfWebRequest $request) {
		$this->setLayout ( 'layout_admin' );
	}
	public function executeError404(sfWebRequest $request) {
		$this->setLayout ( 'pay4me' );
	}
	public function executeErrorInterswitch(sfWebRequest $request) {
		$this->setLayout ( 'layout_admin' );
	}
	public function executeAccessError(sfWebRequest $request) {
	}
	public function executeFeedBack(sfWebRequest $request) {
		$this->form = new FeedbackForm ();
		if ($request->isMethod ( 'post' )) {
			$this->processForm ( $request, $this->form );
		}
	}
	public function executeFaq(sfWebRequest $request) {
	}
	public function executeFeatures(sfWebRequest $request) {
	}
	public function executeHowitWorks(sfWebRequest $request) {
	}
	public function executeHowtoSignUp(sfWebRequest $request) {
	}
	public function executeHowtoMakePayment(sfWebRequest $request) {
	}
	public function executeServices(sfWebRequest $request) {
	}
	public function executeMerchantService(sfWebRequest $request) {
		$this->category = $request->getParameter ( 'category' );
		$merchantObj = merchantServiceFactory::getService ( merchantServiceFactory::$TYPE_BASE );
		$this->merchantListArray = $merchantObj->getMerchant ( $this->category );
		//echo "<pre>";
		
		//print_r($this->merchantListArray);
		$merchantInfo = array();
		
		foreach($this->merchantListArray as  $key=>$value) {
			
			if(isset($value['Category']['id']))
			{
				$merchantInfo[$value['Category']['id']]['name'] = $value['Category']['name'];
				
				$merchantInfo[$value['Category']['id']]['merchant'][$value['id']]['id'] = $value['id'];
				$merchantInfo[$value['Category']['id']]['merchant'][$value['id']]['name'] = $value['name'];
				
				
				if(isset($value['MerchantService']))
				{
					foreach($value['MerchantService'] as $serviceKey=>$serviceInfo)
					{
						$merchantInfo[$value['Category']['id']]['merchant'][$value['id']]['service'][$serviceInfo['id']]['id'] = $serviceInfo['id'];
						$merchantInfo[$value['Category']['id']]['merchant'][$value['id']]['service'][$serviceInfo['id']]['name'] = $serviceInfo['name'];
						$merchantInfo[$value['Category']['id']]['merchant'][$value['id']]['url'] = $serviceInfo['merchant_home_page'];
						
					}
					  
					
				}
			}
			
		}
		//print_r($merchantInfo);
		$this->merchantInfo = $merchantInfo;
		
		
		//die;
	}
	public function executePricing(sfWebRequest $request) {
	}
	public function executeAboutUs(sfWebRequest $request) {
	}
	public function executeContactUs(sfWebRequest $request) {
	}
	public function executeTermsOfUse(sfWebRequest $request) {
	}
	/* public function executeMerchant(sfWebRequest $request) {
		$this->category = $request->getParameter ( 'category' );
		$merchantObj = merchantServiceFactory::getService ( merchantServiceFactory::$TYPE_BASE );
		$this->merchantListArray = $merchantObj->getMerchant ();
		 echo "<pre>"; print_r($this->merchantListArray); echo "</pre>";die;
	} */
	public function executeBank(sfWebRequest $request) {
		$bankObj = bankServiceFactory::getService ( bankServiceFactory::$TYPE_BASE );
		$bankqry = $bankObj->getAllRecords ();
		$this->bankListArray = $bankqry->execute ( array (), Doctrine::HYDRATE_ARRAY );
	}
	function processForm(sfWebRequest $request, sfForm $form) {
		$this->forward404Unless ( $request->isMethod ( 'post' ) );
		// die($request->getParameter('feedback[name]'));
		$data = $request->getParameter ( $form->getName () );
		$data ['ip'] = $this->getIpAddress ();
		$form->bind ( $data );
		if ($form->isValid ()) {
			$saveObj = $form->save ();
			$this->addJobForFeedbackMail ( $saveObj->getId () ); // to send mail
			$this->getUser ()->setFlash ( 'notice', 'Your feedback has been submitted successfully.', true );
			$this->redirect ( 'page/feedBack' );
		}
	}
	
	/*
	 * Function to get ipaddress of the system from where accessing application rather server error
	 */
	public function getIpAddress() {
		return (empty ( $_SERVER ['HTTP_CLIENT_IP'] ) ? (empty ( $_SERVER ['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER ['REMOTE_ADDR'] : $_SERVER ['HTTP_X_FORWARDED_FOR']) : $_SERVER ['HTTP_CLIENT_IP']);
	}
	public function executeGetCurrentIssue(sfWebRequest $request) {
		$optionString = '<option value="' . "" . '">' . "Please Select Common Issue" . '</option>' . "\n";
		$p4mehelperObj = new pfmHelper ();
		$byPass = $request->getParameter ( 'byPass' );
		$payment_mode = $request->getParameter ( 'payment_mode_id' );
		$bankId = $p4mehelperObj->getPMOIdByConfName ( 'bank' );
		$bankDraftId = $p4mehelperObj->getPMOIdByConfName ( 'bank_draft' );
		$chequeId = $p4mehelperObj->getPMOIdByConfName ( 'Cheque' );
		$ewalletId = $p4mehelperObj->getPMOIdByConfName ( 'eWallet' );
		$creditCardId = $p4mehelperObj->getPMOIdByConfName ( 'credit_card' );
		$interswitchId = $p4mehelperObj->getPMOIdByConfName ( 'interswitch' );
		if ($payment_mode == $bankId || $payment_mode == $chequeId || $payment_mode == $bankDraftId) {
			$optionString .= '<option value="' . "Bank Payment Rejected" . '">' . "Bank Payment Rejected" . '</option>' . "\n";
			$optionString .= '<option value="' . "List of Participating Banks" . '">' . "List of Participating Banks" . '</option>' . "\n";
			$optionString .= '<option value="' . "Others" . '">' . "Others" . '</option>' . "\n";
		} else if ($payment_mode == $ewalletId || $payment_mode == $interswitchId || $payment_mode == $creditCardId) {
			$optionString .= '<option value="' . "Account Creation" . '">' . "Account Creation" . '</option>' . "\n";
			$optionString .= '<option value="' . "Account" . '">' . "Account" . '</option>' . "\n";
			$optionString .= '<option value="' . "Forgot Username" . '">' . "Forgot Username" . '</option>' . "\n";
			$optionString .= '<option value="' . "Forgot Password" . '">' . "Forgot Password" . '</option>' . "\n";
			$optionString .= '<option value="' . "eWallet Recharge Issues" . '">' . "eWallet Recharge Issues" . '</option>' . "\n";
			$optionString .= '<option value="' . "eWallet to eWallet" . '">' . "eWallet to eWallet" . '</option>' . "\n";
			$optionString .= '<option value="' . "List of Merchants" . '">' . "List of Merchants" . '</option>' . "\n";
			$optionString .= '<option value="' . "Payment not updating" . '">' . "Payment not updating" . '</option>' . "\n";
			$optionString .= '<option value="' . "Steps to Pay With eWallet" . '">' . "Steps to Pay With eWallet" . '</option>' . "\n";
			$optionString .= '<option value="' . "List of Participating Banks" . '">' . "List of Participating Banks" . '</option>' . "\n";
			$optionString .= '<option value="' . "Others" . '">' . "Others" . '</option>' . "\n";
		} else {
			$optionString .= '<option value="' . "Steps to Pay" . '">' . "Steps to Pay" . '</option>' . "\n";
			$optionString .= '<option value="' . "Payment not updating" . '">' . "Payment not updating" . '</option>' . "\n";
			$optionString .= '<option value="' . "Retrieve Validation Code" . '">' . "Retrieve Validation Code" . '</option>' . "\n";
			$optionString .= '<option value="' . "List of Participating Banks" . '">' . "List of Participating Banks" . '</option>' . "\n";
			$optionString .= '<option value="' . "Others" . '">' . "Others" . '</option>' . "\n";
		}
		return $this->renderText ( $optionString );
	}
	protected function addJobForFeedbackMail($feedbackId) {
		$notificationUrl = 'email/sendEmailFeedback';
		$payForMeObj = payForMeServiceFactory::getService ( payForMeServiceFactory::$TYPE_BASE );
		
		$taskId = EpjobsContext::getInstance ()->addJob ( 'MailNotification', $notificationUrl, array (
				'feedback_id' => $feedbackId 
		) );
		sfContext::getInstance ()->getLogger ()->debug ( "sceduled job with id: $taskId" );
	}
}
