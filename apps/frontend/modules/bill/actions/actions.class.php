<?php

/**
 * bill actions.
 *
 * @package    mysfp
 * @subpackage bill
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class billActions extends sfActions
{
    /*
    public function executeIndex(sfWebRequest $request)
    {

    }

  public function executeShow(sfWebRequest $request)
  {
    $this->bill = Doctrine::getTable('Bill')->find($request->getParameter('id'));
    $this->forward404Unless($this->bill);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new BillForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new BillForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($bill = Doctrine::getTable('Bill')->find($request->getParameter('id')), sprintf('Object bill does not exist (%s).', $request->getParameter('id')));
    $this->form = new BillForm($bill);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($bill = Doctrine::getTable('Bill')->find($request->getParameter('id')), sprintf('Object bill does not exist (%s).', $request->getParameter('id')));
    $this->form = new BillForm($bill);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($bill = Doctrine::getTable('Bill')->find($request->getParameter('id')), sprintf('Object bill does not exist (%s).', $request->getParameter('id')));
    $bill->delete();

    $this->redirect('bill/index');
  }
*/
    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid())
        {
            $bill = $form->save();
            $this->redirect('bill/edit?id='.$bill->getId());
        }
    }

    public function executeShow(sfWebRequest $request){
        $this->checkIsSuperAdmin();
        //     echo "Param : <pre>";print_r($request->getParameterHolder());
        if($request->getParameter('trans_num') && $request->getParameter('trans_num')!=""){
            $billObj = billServiceFactory::getService();
            if($billObj->isRequestBelongsToEwalletUser($request->getParameter('trans_num'),'transactionId')){
                $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
                $this->formData = $payForMeObj->getTransactionRecord($request->getParameter('trans_num'));

                    //get Param Description; as per Merchant Service
                $this->billStatus='';
                //                if($request->getParameter('bill_status'))
                //                    $this->billStatus = $request->getParameter('bill_status');
                $this->billStatus = $this->formData['MerchantRequest']['Bill'][0]['status'];
                if($this->billStatus == "paid") {
                  $this->getRequest()->setParameter('txn_no', $request->getParameter('trans_num'));
                  $this->forward("paymentSystem", 'payOnline');
                }

                $payment_option = $this->formData['MerchantRequest']['PaymentModeOption']['name'];
                $this->pan = "";
                $this->approvalcode = "";
                if($payment_option == sfConfig::get('app_payment_mode_option_credit_card')) {
                    $orderObj = Doctrine::getTable('GatewayOrder')->getOrderDetails($request->getParameter('trans_num'), "success");
                     if($orderObj) {
                        $this->pan = $orderObj->getFirst()->getPan();
                        $this->approvalcode = $orderObj->getFirst()->getApprovalcode();
                      }
                }

                // print "<pre>";
                // print_r($this->formData);
                //  print  $this->formData['validation_number'];
                
                //Restriction for making payment by the bank user of the merchant mentioned in app.yml
                if(isset($this->formData['merchant_id'])){
                    if(commonHelper::isMerchantDisableForPayment($this->formData['merchant_id'])){
                       sfContext::getInstance()->getUser()->setFlash('error', sfConfig::get("app_disable_merchant_for_payment_errmsg"), true);
                       $this->redirect($request->getReferer());
                    }
                }
                //End of Restriction for making ..
                
                
                $this->isClubbed = $this->formData['MerchantRequest']['MerchantService']['clubbed'];
                $this->currencyId = $this->formData['MerchantRequest']['currency_id'];
                $paymentModeConfigManager = new paymentModeConfigManager();

                $this->show = $paymentModeConfigManager->isPaymentServiceActive($this->formData['merchant_id'], $this->formData['merchant_service_id'], $this->formData['payment_mode_id'], $this->formData['payment_mode_option_id']);

                //                                echo "Form Data : <pre>";print_r($this->formData);die;
                //get Param Description; as per Merchant Service
           //     $this->billStatus='';
                //                if($request->getParameter('bill_status'))
                //                    $this->billStatus = $request->getParameter('bill_status');
             //   $this->billStatus = $this->formData['MerchantRequest']['Bill'][0]['status'];
                $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
               $this->MerchantData = $ValidationRulesObj->getValidationRules($this->formData['merchant_service_id']);
                if($payment_option == sfConfig::get('app_payment_mode_option_eWallet')) {
                    $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($this->getUser()->getGuardUser()->getId());
                    $this->permittedType = $billObj->permittedEwalletType($userDetailObj->getFirst()->getEwalletType());
                    if($this->permittedType){
                        $walletDetails = $billObj->getEwalletAccountValues($this->currencyId);
                    }
                    $this->errorForEwalletAcount = '';
                    if($this->permittedType==true && $walletDetails==false){
                        $this->errorForEwalletAcount = "Invalid Account Number.";
                    }else{
                        if($this->permittedType==true)
                        {
                            $this->accountObj = $walletDetails->getFirst();
                            $this->userDetailObj = $walletDetails->getFirst()->getUserDetail()->getFirst();                      
                            $this->accountBalance = $billObj->getEwalletAccountBalance($this->accountObj->getId());
                        }
                    }

                }
                else {
                    $this->setTemplate('creditCardPay');
                }
            }else{
                $this->redirect('bill/invalidTransaction');
            }
        }else{
            $this->redirect('bill/invalidTransaction');
        }
    }
    
    public function executePaymentProcessing(sfWebRequest $request){
        $pfmTransactionDetails =  $request->getParameter('pfmTransactionDetails');
        $this->txnNo =  $pfmTransactionDetails['pfm_transaction_number'];
        $payment_mode_option_id = $pfmTransactionDetails['payment_mode_option_id'];
        if($payment_mode_option_id == 1) {
            $this->url = "admin/bankUser";
        }
        else {
            $this->url = "bill/show";
        }
    }

    public function executeInvalidTransaction(sfWebRequest $request){
        $this->checkIsSuperAdmin();
        $this->pageTitle = 'Invalid Transaction Number.';
    }
    public function executeInvalidAttempt(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->pageTitle = 'Invalid Payment';
    }
    public function executeDoPayment(sfWebRequest $request) {
        $data = $request->getPostParameters();
        $userId = $this->getUser()->getGuardUser()->getId();

        if($data['pinDisplayStatus']=='on') {
          $pinObj=new pin();
          $encrypted_pin = $data['pin'];
          $pin_validate = $pinObj->validatePin($encrypted_pin);
          if($pin_validate!='1') {
            $this->getUser()->setFlash('error', $pin_validate);
            return $this->renderText('logout');
          }
        }

        $this->checkIsSuperAdmin();
        $billObj = billServiceFactory::getService();
        $pfm_transaction_number = $request->getParameter('txnId');
        if($billObj->isRequestBelongsToEwalletUser($pfm_transaction_number,'transactionId')){
            $merchant_request_id = $billObj->getRequestIdByTransaction($pfm_transaction_number);

            $billObj->updateBills($merchant_request_id);
            $record = Doctrine::getTable('EwalletPin')->ResetPin($userId);
            $this->forward('paymentSystem', 'pay');
        }
        else{
            $this->redirect('bill/invalidTransaction');
        }

    }


    public function executeArchiveDuplicateBill(sfWebRequest $request){
        $this->checkIsSuperAdmin();
        $billObj = billServiceFactory::getService();
        $merchant_request_array = array();
        $merchantReqId = $request->getParameter('merchant_req_id');
        $merchant_request_array = $merchantReqId;
        $billObj->doArchiveCheckedBills($merchant_request_array);
        $this->redirect('bill/activeBills');

    }
    public function executeGetItemBills(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        //$this->setLayout('');
        //        if ($this->getUser()  && $this->getUser()->isAuthenticated()) {
        $itemId =  $request->getParameter('itemId');
        $pfm_transaction_number =  $request->getParameter('transactionNumber');
        $billObj = billServiceFactory::getService();
        $this->getResults = $billObj->getItemBills($itemId,$pfm_transaction_number);
        //         }
        //         else{
        //            return $this->renderText('logout');
        //         }

        //            echo "Manoj 44: <pre>";print_r($this->getResults);die;
    }


    /*
     * action MerchantService
     * Purpose : to get merchant service based on merchant
     * Date : 16-05-2011
     * WP037 - CR057
     */

    public function executeMerchantService(sfWebRequest $request) {
            $this->setTemplate(FALSE);
            $merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BANK);
            $str = $merchant_service_obj->getServices($request->getParameter('merchant_id'),$request->getParameter('merchant_service_id'));
            return $this->renderText($str);
    }

    /*
     * action ActiveBills
     * Purpose : to make search on active bills
     * Updated Date : 16-05-2011
     * WP037 - CR057
     *
     */

    public function executeActiveBills(sfWebRequest $request)
    {
         $this->checkIsSuperAdmin();

         $searchArray= $request->getPostParameter('bill');
         $merchant =$searchArray['merchant'];
         $this->form = new BillSearchListForm(array(),array('merchant'=>$merchant));
         $this->formValid ="";
         $request_array = array();
         $merchant_request_array = array();

         if($request->getPostParameter('chkbox')){
   
            $billObj = billServiceFactory::getService();
            $request_array = $request->getPostParameter('chkbox');
           

            if(count($request_array)>0){

                foreach($request_array as $key => $val){
                   $merchant_request_array[$key] = $val;
                }
                $billObj->doArchiveCheckedBills($merchant_request_array);
                $merchant_request_array ='';
            }
         }else{

            if($request->isMethod('post')){

                $this->form->bind($request->getParameter('bill'));
                if ($this->form->isValid())
                {
                   $this->formValid = "valid";
                }
            }
         }



    }



   public function executeSearchactiveBills(sfWebRequest $request){

      
        $this->form = new BillSearchListForm();
        $this->formValid ="";
        $this->form->bind($request->getParameter('bill'));
        //check for is posted or not
        if($request->isMethod('post')){
          //bind form
           if($this->form->isValid())
            {
              
                $this->formValid ="valid";
                $billObj = billServiceFactory::getService();
                 $searchArray= $request->getPostParameter('bill');
                //adding time parameter to to_date
                if($searchArray['from']!="" && $searchArray['from']!='BLANK'){
                  $searchArray['from'] = $searchArray['from']." 00:00:00";
                }
                if($searchArray['to']!="" && $searchArray['to']!='BLANK'){
                  $searchArray['to'] = $searchArray['to']." 23:59:59";
                }

                $this->itemwise_active_bill_list = $billObj->getItemWiseActiveBills($searchArray['merchant'],$searchArray['merchant_service_id'],$searchArray['from'],$searchArray['to'],$searchArray['transaction']);
                $this->page = 1;
                if($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                }
                $this->pager = new sfDoctrinePager('Bill',sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($this->itemwise_active_bill_list);
                $this->pager->setPage($this->page);
                $this->pager->init();

         }
         else{
               
                die;
         }
        }
      
    }



   /*
     * action ArchiveBills
     * Purpose : to make search on archive bills
     * Updated Date : 16-05-2011
     * WP037 - CR057
     *
     */

    public function executeArchiveBills(sfWebRequest $request)
    {
        $this->checkIsSuperAdmin();
        $searchArray= $request->getPostParameter('bill');
        $merchant =$searchArray['merchant'];
        $this->form = new BillSearchListForm(array(),array('merchant'=>$merchant));
        $this->formValid ="";

      
        if($request->isMethod('post')){

        $this->form->bind($request->getParameter('bill'));
        if ($this->form->isValid())
        {
           $this->formValid = "valid";
        }
        }
       
    }



   public function executeSearcharchiveBills(sfWebRequest $request){

        //get form details


        $searchArray= $request->getPostParameter('bill');
         //get form details
        $this->form = new BillSearchListForm();
        $this->formValid ="";

         if($request->isMethod('post')){
             //bind form
             $this->form->bind($request->getParameter('bill'));
            if($this->form->isValid())
            {

                $searchArray= $request->getPostParameter('bill');
                //adding time parameter to to_date
                if($searchArray['from']!="" && $searchArray['from']!='BLANK'){
                $searchArray['from'] = $searchArray['from']." 00:00:00";
                }
                if($searchArray['to']!="" && $searchArray['to']!='BLANK'){
                $searchArray['to'] = $searchArray['to']." 23:59:59";
                }

                //assign form as valid
                $this->formValid = "valid";
                //get all request parameters
                $billObj = billServiceFactory::getService();
                $this->itemwise_archive_bill_list = $billObj->getArchiveBills($searchArray['merchant'],$searchArray['merchant_service_id'],$searchArray['from'],$searchArray['to'],$searchArray['transaction']);

                $this->page = 1;
                if($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                }
                $this->pager = new sfDoctrinePager('Bill',sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($this->itemwise_archive_bill_list);
                $this->pager->setPage($this->page);
                $this->pager->init();
            }else{
                
                 die;

            }
        }
    }




   


     /*
     * action PaidBills
     * Purpose : to make search on paid bills
     * Updated Date : 16-05-2011
     * WP037 - CR057
     *
     */
    public function executePaidBills(sfWebRequest $request)
    {


        $this->checkIsSuperAdmin();
        $billsType = 'paid';
        $searchArray= $request->getPostParameter('bill');
        $merchant =$searchArray['merchant'];
        
        $this->form = new BillSearchListForm(array(),array('billsType'=>$billsType,'merchant'=>$merchant));
        $this->formValid ="";


        if($request->isMethod('post')){

        $this->form->bind($request->getParameter('bill'));
        if ($this->form->isValid())
        {
           $this->formValid = "valid";
        }
        }




    }



    public function executeSearchpaidBills(sfWebRequest $request){

        //get form details


        $searchArray= $request->getPostParameter('bill');
        $billsType = 'paid';
          //get form details
        $this->form = new BillSearchListForm(array(),array('billsType'=>$billsType));
        $this->formValid ="";
         if($request->isMethod('post')){
             //bind form
             $this->form->bind($request->getParameter('bill'));
            if ($this->form->isValid())
            {

                if($searchArray['from']!="" && $searchArray['from']!='BLANK'){
                    $searchArray['from'] = $searchArray['from']." 00:00:00";
                }
                if($searchArray['to']!="" && $searchArray['to']!='BLANK'){
                   $searchArray['to'] = $searchArray['to']." 23:59:59";
                }
                //assign form as valid
                $this->formValid = "valid";
                $billObj = billServiceFactory::getService();
                $this->itemwise_paid_bill_list = $billObj->getPaidBills($searchArray['merchant'],$searchArray['merchant_service_id'],$searchArray['from'],$searchArray['to'],$searchArray['validation_no'],$searchArray['transaction']);
                $this->page = 1;
                if($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                }
                $this->pager = new sfDoctrinePager('Bill',sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($this->itemwise_paid_bill_list);
                $this->pager->setPage($this->page);
                $this->pager->init();
        }else{
                
                die;
            }
        }
    }



    public function checkIsSuperAdmin(){
        if($this->getUser()  && $this->getUser()->isAuthenticated()) {
            $group_name = $this->getUser()->getGroupNames();
            if(in_array(sfConfig::get('app_pfm_role_admin'),$group_name)){
                $this->redirect('@adminhome');
            }
        }
    }
    public function executePayByCard(sfWebRequest $request) {
        $this->postArr = array();
        if ($this->hasRequestParameter('transNo')) {

            $this->postArr['transNo'] = $request->getParameter('transNo');
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $pfmTransactionDetails = $payForMeObj->getTransactionRecord($this->postArr['transNo']);
            $this->postArr['payStatus'] = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];
            $this->postArr['name'] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails']  ['name'];
            $this->postArr['totAmt'] = $pfmTransactionDetails['total_amount'];
            $this->postArr['appType'] = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
            $this->postArr['currencyId'] = $pfmTransactionDetails['MerchantRequest']['currency_id'];

            $url = '';
            $paymentModeOption = strtolower($pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name']);
            switch($paymentModeOption) {

                case sfConfig::get('app_payment_mode_option_credit_card') :
                    $url = 'vbv_configuration/VbvForm';
                    break;
                case sfConfig::get('app_payment_mode_option_interswitch') :
                    $url = 'interswitch_configuration/interswitchForm';
                    break;
                case sfConfig::get('app_payment_mode_option_etranzact'):
                    $url = 'etranzact_configuration/etranzactForm';
                    break;
            }
            $this->postArr['paymentModeOption'] = $pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['id'];
            $this->postArr['url'] = $url;

            $this->setTemplate('confirmPayment');
        } else {
            $this->getUser()->setFlash('error', 'Invalid Transaction');
            $this->setTemplate('confirmPayment');

        }
    }

    /*
     * create order for payment
     */
    public function executeCreateOrder(sfWebRequest $request) {

        $this->setTemplate(NULL);
        $app_id = $request->getParameter('transNo');
        $transaction_id='';
        
        $isPresentInGatewayOrder = Doctrine::getTable('GatewayOrder')->findByAppId($app_id);
        $pfm_obj=new pfmHelper();
        $vbv_pay_mode=$pfm_obj->getPMOIdByConfName(sfConfig::get('app_payment_mode_option_credit_card'));
        if($isPresentInGatewayOrder->count()>0 && $isPresentInGatewayOrder->getFirst()->getPaymentModeOptionId()==$vbv_pay_mode){
            $checkOrderObj = new PaymentVerify();
            $response  = $checkOrderObj->checkOrderStatus($isPresentInGatewayOrder->getLast()->getOrderId());
            if($response['orderstatus']=='CREATED'){
                $transaction_id = $isPresentInGatewayOrder->getLast()->getOrderId();
            }
            else if(in_array($response['orderstatus'], sfConfig::get('app_vbv_orderstatus_reinitiate'))){
                $this->forward('bill','handleCreateOrder');
            }
            else if($response['orderstatus'] == 'APPROVED')
            {
                $this->getUser()->setFlash('error', 'Your Payment is in progress. Please contact Visa for further details.', true) ;
                return $this->renderText("<script>window.location = '".$this->generateUrl('default', array('module' => 'bill',
          'action' => 'PayByCard','transNo' =>$app_id))."'</script>");
            }
            else if(in_array($response['orderstatus'],sfConfig::get('app_vbv_orderstatus_error_msg'))){
                $this->getUser()->setFlash('error', 'Your Payment is in progress. ', true) ;
                return $this->renderText("<script>window.location = '".$this->generateUrl('default', array('module' => 'bill',
          'action' => 'PayByCard','transNo' =>$app_id))."'</script>");
            }
        }else{
            $this->forward('bill','handleCreateOrder');
        }
        return $this->renderText($transaction_id);
    }
    public function executeHandleCreateOrder(sfWebRequest $request){
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($request->getParameter('transNo'));

        $type = $request->getParameter('type');
        $paymentModeOption = $pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['id'];
        $currencyId = $pfmTransactionDetails['MerchantRequest']['currency_id'];
        $total_amount = $this->getAmount($request->getParameter('transNo'));
        $userId = $this->getUser()->getGuardUser()->getId();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $serviceCharges=convertToKobo($pfmTransactionDetails['MerchantRequest']['service_charge']);
        $transaction_id = Doctrine::getTable('GatewayOrder')->saveOrder($request->getParameter('transNo'), $type, $paymentModeOption, $userId, $currencyId, $total_amount,$serviceCharges);
        return $this->renderText($transaction_id);
    }

    protected function getAmount($transNo) {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $formData = $payForMeObj->getTransactionRecord($transNo);
        return $formData['total_amount'] * 100;  //conver to cobe
    }
}









