<?php if("valid" == $formValid): 
 if(($pager->getNbResults())>0) { ?>
<div id="nxtPage">
<?php } ?>
<?php //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead(__('Active Bill Listing')); ?>

<form name="pfmActiveBillListForm"  id= "pfmActiveBillListForm" action="activeBills" method="post" >
    <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr class="alternateBgColour">
            <th width="100%" >
              <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
              <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
            </th>
          </tr>
        </table>
    <br class="pixbr" />
    <?php if(($pager->getNbResults())>0) { ?>
        <input type="button" value="Archive Checked Bills" style="cursor:pointer" onClick="return validate(document.pfmActiveBillListForm.chkbox);">
    <?php } ?>
</div>

    <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
          <tr >
             <?php if(($pager->getNbResults())>0) { ?>
                <th width = "12%">
                    <input type="button" name="checked" id="checked" value="Select All" onClick="checkAll(document.pfmActiveBillListForm.chkbox)" style="cursor:pointer">
                    <input type="button" name="unchecked" id="unchecked" value="Deselect All" onClick="uncheckAll(document.pfmActiveBillListForm.chkbox)" style="display:none;cursor:pointer" >
                </th>
              <?php } ?>
                <th width="22%"><?php echo __('Application Type')?></th>
                <th><?php echo __('Transaction Number')?></th>
                <th width="22%"><?php echo __('Amount')?></th>
                <th width="22%"><?php echo __('Action')?></th>
            </tr>
        </thead>
        <tbody>
            <div name="forChk" id="forChk">
                <?php
                if(($pager->getNbResults())>0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $page = $sf_context->getRequest()->getParameter('page',0);
                    $i = max(($page-1),0)*$limit ;
                    foreach ($pager->getResults() as $bill):
                    $i++;
//                    print "<pre>";
//                    print_R($bill->getMerchantRequest()->toArray());exit;
                  $currency_id = $bill->getMerchantRequest()->getCurrencyId();
                    ?>
                <tr >
                    <td style="padding-top:1px;padding-left:10px;vertical-align:top;"><input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $bill->getBillMerchantRequestId() ?>"></td>
                    <td colspan="3">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr onclick="javascript:slideDiv('<?php echo $bill->getMerchantItemId() ?>','<?php echo $bill->getTransNum() ?>','<?php echo $i; ?>')">
                            <td align="center" width="33%;"><a href="#" class="blue"><?php echo $bill->getApplicationType() ?></a></td>
                                <td align="center" width="33%;"><a href="#"><?php echo $bill->getTransNum() ?></a></td>
                                <td align="center" width="33%;"><a href="#"><?php echo format_amount($bill->getAmount(),$currency_id) ?></a></td>
                            </tr>
                            <tr>
                                <td colspan="3"><div id ="<?php echo $i;?>" style="display:none"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding-top:1px;padding-left:4px;vertical-align:top;">
                        <?php echo image_tag('../images/payBtn.gif',array('width'=>60,'height'=>25,'alt'=>'Pay', 'style' => "cursor:pointer", 'onClick' =>'location.href=\''.url_for('bill/show?trans_num='.$bill->getTransNum().'&bill_status='.$bill->getStatus()).'\'')); ?>
                        <?php  //echo button_to('Pay','',array('onClick'=>'location.href=\''.url_for('bill/show?trans_num='.$bill->getTransNum()).'\'')); ?>
                    </td>
                </tr>
                <?php endforeach;
                ?>

                <tr><td colspan="6">
                        <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'nxtPage')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

                </div></td></tr>
                <?php }else{ ?>
                  <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                  <tr class="alternateBgColour"><td  align='center' class='error' >No Record Found</td></tr>
                 </table>
                <?php } ?>
            </div>
        </tbody>
        <tfoot><tr><td colspan="6"></td></tr></tfoot>
    </table>
  </div>
</form>
 <?php if(($pager->getNbResults())>0) { ?>
</div>
<?php 
 }
 endif 
 ?>

<script language="JavaScript">
    function validate(field)
    {
        var flag = true;
        for (i = 0; i < field.length; i++){
            if(field[i].checked == true){
                //            alert(field[i].value);
                flag = false;
            }
        }
        if(field.length == undefined){
          flag = false;
        }
        if(flag == true){
            alert("Please select at least one Bill to Archive");
            return false;
        }else{
            document.forms["pfmActiveBillListForm"].submit();
        }
    }

function checkAll(field)
{
document.getElementById('checked').style.display = 'none';
document.getElementById('unchecked').style.display = 'block';

if(field.length == undefined){
        field.checked = true ;
}else{
    for (i = 0; i < field.length; i++)
        field[i].checked = true ;
}
}

function uncheckAll(field)
{
document.getElementById('unchecked').style.display = 'none';
document.getElementById('checked').style.display = 'block';

if(field.length == undefined){
        field.checked = false ;
}else{
    for (i = 0; i < field.length; i++)
        field[i].checked = false ;
}
}
function slideDiv(itemId,transactionNumber,divId){
   var url = "<?php echo url_for('bill/getItemBills'); ?>";
if($('#'+divId).css("display")=='none'){
    $('#'+divId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    //$('#'+divId).load(url, {itemId: itemId,transactionNumber:transactionNumber});

    $('#'+divId).load(url, {itemId: itemId,transactionNumber:transactionNumber}, function (data){

                        if(data=='logout'){
                            location.reload();
                        }
                });

    $('#'+divId).slideDown();
    $('#'+divId).show('slow');


}else{
    $('#'+divId).slideUp();
}
}
</script>


