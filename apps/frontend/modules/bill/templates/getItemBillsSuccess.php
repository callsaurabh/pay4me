<table class="tGrid">
        <thead>
            <tr>                
                <th>Make Archive</th>
                <th>Application Type</th>
                <th>Transaction No.</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php
if(count($getResults)>0){
foreach ($getResults as $bill):

?>
<tr>
    <td align="center"><?php echo link_to1('Archive', 'bill/archiveDuplicateBill?merchant_req_id='.$bill->getBillMerchantRequestId())?></td>
    <td align="center"><?php echo $bill->getApplicationType() ?></td>
    <td align="center"><?php echo $bill->getTransNum() ?></td>
    <td align="center"><?php echo format_amount($bill->getAmount(), 1) ?></td>
    <td align="center"><?php  echo button_to('Show Detail','',array('style' => "cursor:pointer", 'onClick'=>'location.href=\''.url_for('bill/show?trans_num='.$bill->getTransNum().'&bill_status='.$bill->getStatus()).'\'')); ?></td>
</tr>
<?php endforeach; }else{ echo "<tr> <td align='center' colspan = 5><b>No Duplicate Records Found</b></td></tr>";}?>
        </tbody>
        </table>
