<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form');
if($formData['MerchantRequest']['MerchantRequestDetails']['id']){

  ?>
<div align="center" id="loader" class="transparent_class"><div style="padding-top:265px;"><?php echo image_tag('loader.gif');?></div></div>
<div id="wrapForm2" class='wrapForm2'>
  <?php echo form_tag('bill/payByCard',array('name'=>'pfmBillShowForm','class'=>'dlForm multiForm', 'id'=>'pfmBillShowForm')) ?>


  <?php
  echo ePortal_legend(__('Application Details'));
  echo formRowFormatRaw(__('Transaction Number'),$formData['pfm_transaction_number']);
  if($formData['MerchantRequest']['validation_number'])
  echo formRowFormatRaw(__('Validation Number'),$formData['MerchantRequest']['validation_number']);
  if($MerchantData)
  {
    foreach($MerchantData as $k=>$v)
    {
       if(isset($formData['MerchantRequest']['MerchantRequestDetails'][$k])){  
      echo formRowFormatRaw(__($v),$formData['MerchantRequest']['MerchantRequestDetails'][$k]);
       }
    }

  }
  //  echo formRowFormatRaw('Application Id',$formData['application_id']);
  //  echo formRowFormatRaw('Reference No.',$formData['ref_no']);
  echo formRowFormatRaw(__('Application Type'),$formData['MerchantRequest']['MerchantService']['name']);
  ?>
  <?php
  echo ePortal_legend(__('User Details'));
  echo formRowFormatRaw(__('Name'),ePortal_displayName($formData['MerchantRequest']['MerchantRequestDetails']['name']));
  #echo formRowFormatRaw('Mobile',$formData['mobile']);
  #echo formRowFormatRaw('Email',$formData['email']);
  ?>
  <?php
  echo ePortal_legend(__('Payment Details'));
  if($isClubbed == 'no'){
    echo formRowFormatRaw(__('Application Charges'),format_amount($formData['MerchantRequest']['item_fee'], $currencyId));
    if(!empty($formData['MerchantRequest']['bank_charge'])){
      echo formRowFormatRaw(__('Transaction Charges'),format_amount($formData['MerchantRequest']['bank_charge'], $currencyId));
    }
    if(!empty($formData['MerchantRequest']['service_charge'])){
      echo formRowFormatRaw(__('Service Charges'),format_amount($formData['MerchantRequest']['service_charge'], $currencyId));
    }
  }else{
    echo formRowFormatRaw(__('Application Charges'),format_amount($formData['total_amount'], $currencyId));
  }
  echo formRowFormatRaw(__('Total Payable Amount'),format_amount($formData['total_amount'], $currencyId));
  ?>
  <?php 
if($billStatus == 'paid') {
  echo ePortal_legend(__('Payment Status'));
    echo formRowFormatRaw(__('Payment Mode'),$formData['MerchantRequest']['PaymentModeOption']['PaymentMode']['display_name']);
    if($pan) echo formRowFormatRaw(__('Card Number'),$pan);
   if($pan)  echo formRowFormatRaw(__('Approval Code'),$approvalcode);
    if($formData['payment_status_code'] == 0){echo formRowFormatRaw('Status','Payment Successful');}
    else echo formRowFormatRaw(__('Status'),'Payment not done');
     echo formRowFormatRaw(__('Payment Date:'),formatDate($formData['MerchantRequest']['paid_date']));
}
?>
 
  <input type ="hidden" name="transNo" id="transNo" value ="<?php echo $formData['pfm_transaction_number'];?>"/>
<!--  <div class="pixbr XY20"> -->

 <div style="margin:auto; text-align:center; width:50%;">
    <center id="formNav">
      <?php
      $payment_status = $formData['MerchantRequest']['payment_status_code'];
      if($payment_status == 0  || $billStatus=='archive'){
        echo button_to('Back','',array('style' => "cursor:pointer", 'onClick'=>'history.go(-1)','class'=>'formSubmit'));
      }else {
        if($payment_status==1 || $billStatus=='active'){
          if($show){echo image_tag('../images/payBtn.gif',array('width'=>60,'height'=>30,'alt'=>'Pay', 'style' => "cursor:pointer", 'onClick' =>'submitForm();'));
          }else{?><font size="2" color="red"><b>You are restricted to pay for this Merchant Service</b></font><?php }
        }
      }?>
       </center>
</div>
 
 <!-- </div> -->
</form></div>
<?php }else{
echo  "<h2>;Bill details not found.</h2>";
}
?>

</div>
<script>
  function submitForm()
  {

        javascript:document.pfmBillShowForm.submit();
//    $('#formNav').hide();
//    var transNo = $('#transNo').val();
//    var url = '<?php //echo url_for('vbv_configuration/vbvForm');?>';
//    $('#loader').show();
//
//    $.post(url, {transNo:transNo,type:'pay'}, function(data){
//      if(data=='logout'){
//        location.reload();
//      }
//      $('#wrapForm2').html(data);
//      $('#loader').hide();
//    });

  }
</script>