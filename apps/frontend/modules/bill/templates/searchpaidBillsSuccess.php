
<?php if("valid"== $formValid ): 
 if(($pager->getNbResults())>0) { ?>
<div id="nxtPage">
<?php } ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


<?php echo ePortal_listinghead(__('Paid Bill Listing')); ?>

<form name="pfmPaidBillListForm" action="#" method="post" >
    <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
          <th width="100%" >
            <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
            <span class="floatRight"><?php echo __('Showing')?> <b><?php echo ($pager->getNbResults() ? $pager->getFirstIndice() :'0' )?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
          </th>
        </tr>
      </table>
      <br class="pixbr" />
    </div>

    <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr class="alternateBgColour">
              <th>S. No</th>
              <th><?php echo __('Application Type')?></th>
              <th><?php echo __('Transaction Number')?></th>
              <th><?php echo __('Amount')?></th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
            <div name="forChk" id="forChk">
                <?php
                if(($pager->getNbResults())>0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $page = $sf_context->getRequest()->getParameter('page',0);
                    $i = max(($page-1),0)*$limit ;
                    foreach ($pager->getResults() as $bill):
                    $i++;
                    ?>
                <tr class="alternateBgColour">
                    <td align="center"><?php echo $i; ?></td>
                    <td align="center"><?php echo $bill->getApplicationType() ?></td>
                    <td align="center"><?php echo $bill->getTransNum() ?></td>
                    <td align="center"><?php echo format_amount($bill->getAmount(), $bill->getCurrencyId()) ?></td>
                    <td align="center"><?php  echo button_to(__('Show Details'),'',array('style' => "cursor:pointer", 'onClick'=>'location.href=\''.url_for('bill/show?trans_num='.$bill->getTransNum().'&bill_status='.$bill->getStatus()).'\'','class'=>'formSubmit')); ?></td>
                </tr>
                <?php endforeach;
                ?>

                <tr><td colspan="5">
                        <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'nxtPage')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

                </div></td></tr>
                <?php }else{ ?>
                 <table  border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                  <tr ><td  align='center' class='error' >No Record Found</td></tr>
                 </table>
                <?php } ?>
            </div>
        </tbody>
        <tfoot><tr><td colspan="5"></td></tr></tfoot>
    </table>
    </div>
</form>
 <?php if(($pager->getNbResults())>0) { ?>
</div>
<?php 
 }
 endif 
 ?>

