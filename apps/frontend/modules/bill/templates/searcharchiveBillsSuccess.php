
<?php if("valid"== $formValid ){ 
 if(($pager->getNbResults())>0) { ?>
<div id="nxtPage">
<?php } ?>
<?php use_helper('Pagination');  ?>
 <?php echo ePortal_listinghead(__('Archive Bill Listing')); ?>
    <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr class="alternateBgColour">
            <th width="100%" >
              <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
              <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
            </th>
          </tr>
        </table>
        <br class="pixbr" />
    </div>
    
     <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
          <tr class="alternateBgColour">
              <th>S. No</th>
              <th><?php echo __('Application Type')?></th>
              <th><?php echo __('Transaction Number')?></th>
              <th><?php echo __('Amount')?></th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
            <div name="forChk" id="forChk">
                <?php
                if(($pager->getNbResults())>0) {
                        $limit = sfConfig::get('app_records_per_page');
                        $page = $sf_context->getRequest()->getParameter('page',0);
                        $i = max(($page-1),0)*$limit ;
                        foreach ($pager->getResults() as $bill){
                        $i++;
                        ?>
                    <tr >
                        <td align="center"><?php echo $i; ?></td>
                        <td align="center"><?php echo $bill->getApplicationType() ?></td>
                        <td align="center"><?php echo $bill->getTransNum() ?></td>
                        <td align="center"><?php echo format_amount($bill->getAmount(), $bill->getCurrencyId()) ?></td>
                        <td align="center"><?php  echo button_to(__('Show Details'),'',array('style' => "cursor:pointer", 'onClick'=>'location.href=\''.url_for('bill/show?trans_num='.$bill->getTransNum().'&bill_status='.$bill->getStatus()).'\'','class'=>'formSubmit')); ?></td>
                    </tr>
                    <?php  
                      } ?>
                       <tr><td colspan="5">
                        <div class="paging pagingFoot">
                          <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'nxtPage') ?>
                           </div>
                       </td></tr>
                 <?php } else { ?>
                        <tr class="alternateBgColour"><td   colspan="5"><font color="red">No Record Found</font></td></tr>
                <?php  } ?>
            </div>
        </tbody>
    </table>
  </div>
   
 <?php if(($pager->getNbResults())>0) { ?></div> <?php } ?>
<?php 
 }
 
?>







