<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form');
if($formData['MerchantRequest']['MerchantRequestDetails']['id']){
  if($permittedType)
  $accountBalance = format_amount($accountBalance,NULL,1);
  if(!$permittedType){
    echo  "<h2>eWallet Account needs to be Activated/Re-activated before making payment.</h2>"; }
  else if($accountBalance<$formData['total_amount'] && $billStatus == 'active'){
    echo  "<h2>Insufficient account balance.</h2>"; }
  
  ?>
<div align="center" id="loader" class="transparent_class"><div style="padding-top:265px;"><?php echo image_tag('loader.gif');?></div></div>
<div id="wrapForm2" class='wrapForm2'>
  <?php //echo form_tag('bill/doPayment',array('name'=>'pfmBillShowForm','class'=>'dlForm multiForm', 'id'=>'pfmBillShowForm')) ?>
  <?php
  if($permittedType) echo ePortal_legend(__('User eWallet Account Details'));
  if($errorForEwalletAcount){
    echo formRowFormatRaw('Error message ',$errorForEwalletAcount);
  }else{
    if($permittedType){
    echo formRowFormatRaw(__('Account ID'),ePortal_displayName($accountObj['account_number']));
    echo formRowFormatRaw(__('Account Name'),ePortal_displayName($accountObj['account_name']));
    echo formRowFormatRaw(__('Account Balance'),format_amount($accountBalance, $currencyId));
    $pinObj=new pin();
    if($pinObj->isActive()){
        if($billStatus == 'active'){
            include_partial('ewallet_pin/ewalletTransfer');
        }
       
       ?>
       <input type="hidden" name="hidPinDisplayStatus" id="hidPinDisplayStatus" value="on">
    <?php }else{
    ?>
    <input type="hidden" name="hidPinDisplayStatus" id="hidPinDisplayStatus" value="off">
    <?php
    }
    }
  }?>
  <?php
  echo ePortal_legend('Application Details');
  echo formRowFormatRaw(__('Transaction Number'),$formData['pfm_transaction_number']);
  if($formData['MerchantRequest']['validation_number'])
  echo formRowFormatRaw(__('Validation Number'),$formData['MerchantRequest']['validation_number']);
  if($MerchantData)
  {
    foreach($MerchantData as $k=>$v)
    { 
        if(isset($formData['MerchantRequest']['MerchantRequestDetails'][$k])){
            echo formRowFormatRaw(__($v),$formData['MerchantRequest']['MerchantRequestDetails'][$k]);
        }

    }

  }
  //  echo formRowFormatRaw('Application Id',$formData['application_id']);
  //  echo formRowFormatRaw('Reference No.',$formData['ref_no']);
  echo formRowFormatRaw('Application Type',$formData['MerchantRequest']['MerchantService']['name']);
  ?>
  <?php
  echo ePortal_legend(__('User Details'));
  echo formRowFormatRaw(__('Name'),ePortal_displayName($formData['MerchantRequest']['MerchantRequestDetails']['name']));
  #echo formRowFormatRaw('Mobile',$formData['mobile']);
  #echo formRowFormatRaw('Email',$formData['email']);
  ?>
  <?php
  echo ePortal_legend(__('Payment Details'));
  if($isClubbed == 'no'){
    echo formRowFormatRaw(__('Application Charges'),format_amount($formData['MerchantRequest']['item_fee'], $currencyId));
    if(!empty($formData['MerchantRequest']['bank_charge'])){
      echo formRowFormatRaw(__('Transaction Charges'),format_amount($formData['MerchantRequest']['bank_charge'], $currencyId));
    }
    if(!empty($formData['MerchantRequest']['service_charge'])){
      echo formRowFormatRaw(__('Service Charges'),format_amount($formData['MerchantRequest']['service_charge'], $currencyId));
    }
  }else{
    echo formRowFormatRaw(__('Application Charges'),format_amount($formData['total_amount'], $currencyId));
  }
  echo formRowFormatRaw(__('Total Payable Amount'),format_amount($formData['total_amount'], $currencyId));
  ?>
   <?php
if($billStatus == 'paid'){
  echo ePortal_legend(__('Payment Status'));
    echo formRowFormatRaw(__('Payment Mode'),$formData['MerchantRequest']['PaymentModeOption']['PaymentMode']['display_name']);
    if($pan) echo formRowFormatRaw('Card Number',$pan);
    if($approvalcode)  echo formRowFormatRaw('Approval Code',$approvalcode);
    if($formData['payment_status_code'] == 0){echo formRowFormatRaw('Status','Payment Successful');}
    else echo formRowFormatRaw(__('Status'),'Payment not done');
     echo formRowFormatRaw(__('Payment Date:'),formatDate($formData['MerchantRequest']['paid_date']));
}
?>
  <input type ="hidden" name="txnId" id="txnId" value ="<?php echo $formData['pfm_transaction_number'];?>"/>
  <input type ="hidden" name="url" id="url" value ="<?php echo $sf_context->getModuleName().'/doPayment';?>"/>
  <dl><div class="pixbr XY20">
      <center id="formNav">
      <?php 
      $payment_status = $formData['MerchantRequest']['payment_status_code'];
      if($permittedType && $accountBalance>=$formData['total_amount']){
        if($payment_status == 0  || $billStatus=='archive'){
          echo button_to('Back','',array('style' => "cursor:pointer", 'onClick'=>'history.go(-1)','class'=>'formSubmit'));
        }else {
          if($payment_status==1 || $billStatus=='active'){
            if($show){echo image_tag('../images/payBtn.gif',array('width'=>60,'height'=>30,'alt'=>'Pay', 'style' => "cursor:pointer", 'onClick' =>'return submitForm();'));
            }else{?><font size="2" color="red"><b>You are restricted to pay for this Merchant Service</b></font><?php }
          }
        }
        //<input type="submit" value="Pay" id="button" class="formSubmit" name="button"/>
      }?>
    </center>
  </div></dl>
<!--</form>--></div>
<?php }else{
echo  "<h2>;Bill details not found.</h2>";
}
?>


<script>
  //[WP045][CR066]
  $('#pin').keypad({duration:'fast',randomiseNumeric: true});
  
  function submitForm()
  {
      $('#formNav').hide();
    //    javascript:document.pfmBillShowForm.submit();
    if($('#pin').val() == '')
        {
            $('#err_pin').html('Pin required');
             $('#formNav').show();
            return false;
        }
    
    var txnId = $('#txnId').val();
    var pinDisplayStatus = $('#hidPinDisplayStatus').val();
    if(pinDisplayStatus=='on')
        {
            var pin=$('#pin').val();
        }else{
            var pin='';
        }
    var url = '<?php echo url_for('bill/doPayment');?>';
    $('#loader').show();

    $.post(url, {txnId:txnId,pinDisplayStatus:pinDisplayStatus,pin:pin}, function(data){
     if(data == 'logout'){
          location.reload();
      }else{
        $('#loadArea').html(data);
        $('#loader').hide();
      }
    });
    
        
  }
</script>