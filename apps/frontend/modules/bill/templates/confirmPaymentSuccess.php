<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form');?>
<!--<div id='error' class='error_list' style="color:red"></div>-->
<div align="center" id="loader" class="transparent_class"><div style="padding-top:265px;"><?php echo image_tag('loader.gif');?></div></div>
<?php if (isset($postArr['transNo']) && $postArr['transNo']!="") {?>
<!--<div id="wrapForm2" class='wrapForm2 dlForm multiForm'>-->
<div id="wrapForm2" class='wrapForm2 multiForm'>

<?php
echo ePortal_legend(__('Application Details'));
echo formRowFormatRaw(__('Transaction Number'),$postArr['transNo']);
echo formRowFormatRaw(__('Application Type'),$postArr['appType']);
echo formRowFormatRaw(__('Name'),ePortal_displayName($postArr['name']));
echo formRowFormatRaw(__('Total Payable Amount'),format_amount($postArr['totAmt'], $postArr['currencyId']));
?>
  <input type ="hidden" name="transNo" id="transNo" value ="<?php echo $postArr['transNo'];?>"/>
  <input type ="hidden" name="paymentModeOption" id="paymentModeOption" value ="<?php echo $postArr['paymentModeOption'];?>"/>
  <!-- <div class="pixbr XY20"> -->

  <div style="margin:auto; text-align:left; width:322px;">
    <center id="formNav">
      <?php if($postArr['payStatus'] != 0) echo button_to(__('Continue'),'',array('style' => "cursor:pointer", 'onClick'=>'submitForm()','class'=>'formSubmit')); ?>
    </center>

    <?php if($postArr['paymentModeOption'] == '2') {
  echo image_tag('/img/interswitch-logo.jpg',array('alt'=>'Interswitch', 'border' => 0 ,'class' => 'interswitch'));
    }?>  
  </div>
</div>
<div id="iframe">

</div>
<?php }?>

</div>
<div id="loadjstext"></div>
<script>
  function submitForm()
  {
    $('#formNav').hide();
    $('#flash_error').hide();
    var transNo = $('#transNo').val();
    var paymentModeOption = '<?php echo $postArr['paymentModeOption'];?>';
    var url = '<?php echo url_for('bill/createOrder');?>';
    $('#loader').show();

    $.post(url, {transNo:transNo,type:'payment',paymentModeOption:paymentModeOption}, function(data){
      if(data=='logout'){
        location.reload();
      } else{
          if (data.indexOf('script') > -1){
              $('#loadjstext').append(data);
          }else{
            var payUrl = '<?php echo url_for($postArr['url']);?>';
            $.post(payUrl, {transNo:transNo,orderId:data,paymentModeOption:paymentModeOption}, function(data){
               if(data=='error'){
                $('#error').html('Invalid Transaction Number');
                $('#formNav').show();
              } else{
                $('#iframe').html(data);
              }
              $('#loader').hide();
            });
        }
      }
    });
  }
</script>