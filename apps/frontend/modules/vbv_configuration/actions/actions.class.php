<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actionsclass
 *
 * @author akumar1
 */


class vbv_configurationActions extends sfActions {

  public function executeVbvRecharge(sfWebRequest $request) {

    $transNo = $request->getParameter('transNo');//this is the account id
    $orderId = $request->getParameter('orderId');
    $gatewayorderDetails = Doctrine::getTable('GatewayOrder')->findByOrderId($orderId);

    $userId = $this->getUser()->getGuardUser()->getId();
    $acctNumber = Doctrine::getTable('UserAccountCollection')->findOneBy("user_id",$userId)->getEpMasterAccount()->getAccountNumber();
    $userDetails= Doctrine::getTable('UserDetail')->findByUserId($userId);
    $type = $request->getParameter('type');
    $epVbvManagerObj = new EpVbvManager();
    $epVbvManagerObj->amount = $request->getParameter('totalCharges') ;
    $epVbvManagerObj->orderId = $orderId;
    $epVbvManagerObj->currency = sfConfig::get('app_naira_currency') ;
    $host = $request->getUriPrefix() ;
    $url_root = $request->getPathInfoPrefix();
    $epVbvManagerObj->returnUrlApprove = $host.$url_root."/".sfConfig::get('app_naira_ret_url_approve')."/z/".session_id() ;
    $epVbvManagerObj->returnUrlDecline = $host.$url_root."/".sfConfig::get('app_naira_ret_url_decline')."/z/".session_id() ;
    $epVbvManagerObj->returnUrlCancel = $host.$url_root."/".sfConfig::get('app_naira_ret_url_cancel')."/z/".session_id() ;
//    $epVbvManagerObj->postUrl = sfConfig::get('app_naira_posturl');
    $epVbvManagerObj->language = sfConfig::get('app_vbv_parameter_language');
    $epVbvManagerObj->email = $userDetails->getFirst()->getEmail();
    $epVbvManagerObj->phone = $userDetails->getFirst()->getMobileNo();
    $epVbvManagerObj->userId = $userId;
    $epVbvManagerObj->merid = sfConfig::get('app_vbv_parameter_merchant');
//    $epVbvManagerObj->acqBin = sfConfig::get('app_naira_acqbin');
    $lastOrderDetail = $this->getLastTransNo($userId);
    $this->lastOrderId = false;
    if($lastOrderDetail)
        {
            $lastOrderId= $lastOrderDetail['order_id'];
            $payMode = $this->getCreditCardPayModeId();
            $status = 'pending';
            $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($lastOrderId,$payMode,$status);
         if($gatewayOrder && $gatewayOrder->getType()=='recharge')
             {
                 $this->verfiyAndUpdateByOrderId($lastOrderId);
             }
        }
    $vbVManager = new vbvConfigurationManager();
    $validateTrans='';
    $isValidRecharge = $vbVManager->createXml($validateTrans , $gatewayorderDetails, $userDetails);
    $epVbvManagerObj->description = 'Recharge for ewallet number '.$acctNumber ;
    $epVbvManagerObj->requestUrl = $isValidRecharge['url'];
    $epVbvManagerObj->order_id = $isValidRecharge['order_id'];
    $epVbvManagerObj->session_id = $isValidRecharge['session_id'];
    $epVbvManagerObj->status = $isValidRecharge['request_status'];
    $this->retObj = $epVbvManagerObj->setRequest();
    $capture_data_in_log = array();
    $capture_data_in_log[] = "url=" . $this->retObj->getVbvUrl();
    $capture_data_in_log[] = "session_id=" . $this->retObj->getVbvSessionId();
    $capture_data_in_log[] = "order_id=" . $this->retObj->getVbvOrderId();
    $vbVManager->createLog(implode("|", $capture_data_in_log), 'request_recharge_log_'.$orderId);
    //    $this->saveVbvOrder($this->retObj['order_id'], $transNo, $type);

    //if(isset($isValidRecharge['msg'])){
    if(!($isValidRecharge['order_id']) && !($isValidRecharge['session_id'])){ // added by vikash (24-09-2012) if gateway response is not 00.
          $this->getUser()->setFlash('error', 'Invalid Transaction ', true) ;
          return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
          'action' => 'recharge','recharge_gateway' =>"credit_card"))."'</script>");

      }else{
          $this->setTemplate('vbvForm');
      }
  }

  public function executeVbvForm(sfWebRequest $request) {
      $transNo = $request->getParameter('transNo');
      $orderId = $request->getParameter('orderId');
      $userId = $this->getUser()->getGuardUser()->getId();
      $userDetails= Doctrine::getTable('UserDetail')->findByUserId($userId);
      $host = $request->getUriPrefix() ;
      $url_root = $request->getPathInfoPrefix();
      $paymentModeOption = $request->getParameter('paymentModeOption');
      $validateTrans = $this->validateTransactionNumber($transNo, $paymentModeOption);
      if($validateTrans) {
          $item_id = $validateTrans['MerchantRequest']['merchant_item_id'];
          $payment_status = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
          if($payment_status !=0)
          {
              $txnArr = Doctrine::getTable('Transaction')->getDetailByItemId($item_id);
              $appArr = array();
              foreach($txnArr as $txn)
              {
                  //                   if($txn['pfm_transaction_number'] != $transNo)
                  $appArr[] = $txn['pfm_transaction_number'];
              }
              if(count($appArr)>0)
              {
                  $status = 'pending';
                  $gatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByAppId($appArr,$paymentModeOption,$status,$orderId);
                  if($gatewayOrders)
                  {
                      $lastPendingOrderId = $gatewayOrders->getfirst()->getOrderId();
                  }
              }
          }
          
          ///again check the status
          $payment_status = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);

          if($payment_status == 0) {

              $delGatewayOrders = Doctrine::getTable('GatewayOrder')->deleterecords($transNo,$orderId);
              $this->getUser()->setFlash('notice', 'Payment has already been made for this Application', true) ;

              $merchant_obj = Doctrine::getTable('MerchantRequest')->getMerchantPaidId($item_id);
              $merchant_request_payment_id = $merchant_obj->getFirst()->getId();
              $transaction_number_payment_id = $merchant_obj->getFirst()->getTransaction()->getFirst()->getTransactionNumber();

              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'vbv_configuration',
            'action' => 'payOnline','returnVal' =>base64_encode($transaction_number_payment_id),'duplicate_payment_receipt'=>true))."'</script>");
          }else {
              $vbVManager = new vbvConfigurationManager();
              $gatewayOrderRec = Doctrine::getTable('GatewayOrder')->findByOrderId($orderId);
              //to chk for already initiated payment through vbv
              $this->retObj = Doctrine::getTable('EpVbvRequest')->findByOrderId($orderId);
              $this->retObj = $this->retObj->getFirst();
                  if($this->retObj && $this->retObj->getVbvOrderId()
                      && $this->retObj->getVbvSessionId() ){
                      $this->createFormPostLog($this->retObj);
                      }
              else{
                  //xml send to vbv for payment (create_order)
                  $this->retObj =$this->processVbvRequest($validateTrans,$gatewayOrderRec,$userDetails,$host,$url_root);
              
                  $this->createFormPostLog($this->retObj);
                  $requestData = $this->retObj->toArray();
                  $this->verfiyAndUpdateByOrderId($requestData['order_id']);

              }

              if($this->retObj->getVbvOrderId()!=''){
                      $this->setTemplate('vbvForm');
                  }else{
                      $this->getUser()->setFlash('error', 'Invalid Transaction ', true) ;
                      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
          'action' => 'PayByCard','transNo' =>$validateTrans['pfm_transaction_number']))."'</script>");
                  }
              }
              
      }else {
          return $this->renderText('error');
      }
  }

 public function createFormPostLog($retObj){
      $vbVManager = new vbvConfigurationManager();
      $capture_data_in_log = array();
      $capture_data_in_log[] = "url=" . $retObj->getVbvUrl();
      $capture_data_in_log[] = "session_id=" . $retObj->getVbvSessionId();
      $capture_data_in_log[] = "order_id=" . $retObj->getVbvOrderId();
      $vbVManager->createLog(implode("|", $capture_data_in_log), 'request_payment_log_'.$retObj->getOrderId());
 }
 public function processVbvRequest($validateTrans,$gatewayOrders,$userDetails,$host,$url_root){
     $vbVManager = new vbvConfigurationManager();
     $isValidPayment = $vbVManager->createXml($validateTrans , $gatewayOrders, $userDetails);
     $epVbvManagerObj = new EpVbvManager();
     $epVbvManagerObj->amount = $this->getAmount($gatewayOrders->getFirst()->getAppId());
     $epVbvManagerObj->userId = $userDetails->getFirst()->getUserId() ;
     $epVbvManagerObj->orderId = $gatewayOrders->getFirst()->getOrderId();

     $merchantServiceDetails = Doctrine::getTable('MerchantService')->find($validateTrans['MerchantRequest']['merchant_service_id']);
     $descriptionTxt='Payment for '.$merchantServiceDetails->getName().' for transaction '.$validateTrans['pfm_transaction_number'] ;
     if($validateTrans['MerchantRequest']['CurrencyCode']['currency_num'] == sfConfig::get('app_naira_currency')) {
          $epVbvManagerObj->currency = sfConfig::get('app_naira_currency') ;
          $epVbvManagerObj->merid = sfConfig::get('app_vbv_parameter_merchant');
          $epVbvManagerObj->returnUrlApprove = $host.$url_root."/".sfConfig::get('app_naira_ret_url_approve')."/z/".session_id() ;
          $epVbvManagerObj->returnUrlDecline = $host.$url_root."/".sfConfig::get('app_naira_ret_url_decline')."/z/".session_id() ;
          $epVbvManagerObj->returnUrlCancel = $host.$url_root."/".sfConfig::get('app_naira_ret_url_cancel')."/z/".session_id() ;
//          $epVbvManagerObj->postUrl = sfConfig::get('app_naira_posturl');
          $epVbvManagerObj->requestUrl = $isValidPayment['url'];
          $epVbvManagerObj->order_id = $isValidPayment['order_id'];
          $epVbvManagerObj->session_id = $isValidPayment['session_id'];
          $epVbvManagerObj->status = $isValidPayment['request_status'];
          $epVbvManagerObj->language = sfConfig::get('app_vbv_parameter_language');
          $epVbvManagerObj->email = $userDetails->getFirst()->getEmail();
          $epVbvManagerObj->description = $descriptionTxt;
          $epVbvManagerObj->phone = $userDetails->getFirst()->getMobileNo();
      }
     else if($validateTrans['MerchantRequest']['CurrencyCode']['currency_num'] == sfConfig::get('app_dollar_currency')) {
          $epVbvManagerObj->currency = sfConfig::get('app_dollar_currency') ;
          $epVbvManagerObj->merid = sfConfig::get('app_vbv_parameter_merchant');
          $epVbvManagerObj->acqBin = sfConfig::get('app_dollar_acqbin');
          $epVbvManagerObj->returnUrlApprove = $host.$url_root."/".sfConfig::get('app_dollar_ret_url_approve')."/z/".session_id() ;
          $epVbvManagerObj->returnUrlDecline = $host.$url_root."/".sfConfig::get('app_dollar_ret_url_decline')."/z/".session_id() ;
          $epVbvManagerObj->returnUrlCancel = $host.$url_root."/".sfConfig::get('dollar_ret_url_cancel')."/z/".session_id() ;
//          $epVbvManagerObj->postUrl = sfConfig::get('app_dollar_posturl');
          $epVbvManagerObj->requestUrl = $isValidPayment['url'];
          $epVbvManagerObj->order_id = $isValidPayment['order_id'];
          $epVbvManagerObj->session_id = $isValidPayment['session_id'];
          $epVbvManagerObj->status = $isValidPayment['request_status'];
          $epVbvManagerObj->language = sfConfig::get('app_vbv_parameter_language');
          $epVbvManagerObj->email = $userDetails->getFirst()->getEmail();
          $epVbvManagerObj->description = $descriptionTxt;
          $epVbvManagerObj->phone = $userDetails->getFirst()->getMobileNo();
      }
      return $this->retObj = $epVbvManagerObj->setRequest();
     
 }


 public function executeViewLastDetail(sfWebRequest $request) {
     $this->setLayout(false);
     $orderId = $request->getParameter("orderId");

     $payMode = $this->getCreditCardPayModeId();
     $status = 'pending';
     $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode,$status);
     $verify = false;
     if($gatewayOrder)
     {
        $verify =  $this->verfiyAndUpdateByOrderId($orderId);

      }
      // get object again after updation
      if($verify)
        {
            $gatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode);
            $gatewayOrder = $gatewayOrders->getfirst();
        }

      $this->result = $gatewayOrder;


 }
 public function verfiyAndUpdateByOrderId($orderId) {
         $payMode = $this->getCreditCardPayModeId();
         $status = 'pending';
         $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode,$status);
         if(!$gatewayOrder){
             return false;
         }
         $vbVManager = new vbvConfigurationManager();
         $isValidPayment = $vbVManager->paymentVerify($orderId);


         $responseData = Doctrine::getTable('EpVbvResponse')->findByOrderId($orderId);

         $response['status']='success';

         if($responseData->count()>0){
             $response['purchaseamount'] = $responseData->getFirst()->getPurchaseAmount();
             $response['responsecode'] = $responseData->getFirst()->getResponseCode();
             $response['responsedescription'] = $responseData->getFirst()->getResponseDescription();
             $response['trandatetime']  = $responseData->getFirst()->getTranDateTime();
             $response['approvalcode'] = $responseData->getFirst()->getApprovalCode();
         }
         $response['orderid'] =  $orderId;
         $response['pan'] = $gatewayOrder->getPan();
        if($isValidPayment == '1') {
          $orderArray = $this->getAppIdAndType($orderId);
          $txnId = $orderArray['app_id'];
          $type = $orderArray['type'];
          $payment_mode_option_id = $gatewayOrder->getPaymentModeOptionId();
          if(($responseData->count()>0) && $responseData->getFirst()->getOrderStatus()=='approved'){
              if(strtolower($type) == 'payment') {
                    $arrayVal = $this->processToPayment($response,$txnId);
              }
              if(strtolower($type) == 'recharge') {
                      $validationNo = $this->processToRecharge($response,$orderArray,$txnId, $payment_mode_option_id);

              }
          }
        } else if ($isValidPayment == '2') {
           $gatewayOrder->setStatus('Pending');
           $gatewayOrder->save();

        } else {
           $gatewayOrder->setStatus('failure');
           $gatewayOrder->setTransactionDate(date('Y-m-d H:i:s'));
           $gatewayOrder->save();
        }
          return true;

 }
     /**
     * OSSCUBE
     * Process the vbv record for vbv pending
     *
     * @param string $componentName
     * @return Doctrine_Table
     *
    public function executeProcessVbv(sfWebRequest $request) {
      try {
        $this->setLayout(null);
        ini_set('memory_limit','512M');

        $payMode = $this->getCreditCardPayModeId();


       $from_trans_date = date('Y-m-d H:i:s',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 00:00:00"));
       $status = 'pending';
       // $from_trans_date if want to process all record then set $from_trans_date = '';
//       $from_trans_date = '';
       $allGatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByOrderIdAndStatus($payMode,$status,$from_trans_date);

       foreach($allGatewayOrders as $gatewayOrder)
       {
           $orderId =  $gatewayOrder->getOrderId();
           $this->verfiyAndUpdateByOrderId($orderId);
       }

       return $this->renderText("VBV processing done");
      }
      catch(Exception $e ) {
        $this->logMessage("Logging the exception from VBV Updation Processing..".$e->getMessage().var_dump(number_format(memory_get_peak_usage())));
      }

    }
	*/

     /**
     * OSSCUBE
     * Process the vbv record for vbv pending and create job
     * for each pending transaction
     *
     * @param string $componentName
     * @return Doctrine_Table
     */
	public function executeProcessVbv(sfWebRequest $request) {
      try {
        $this->setLayout(null);
        ini_set('memory_limit','512M');

        $payMode = $this->getCreditCardPayModeId();


       $from_trans_date = date('Y-m-d H:i:s',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 00:00:00"));
       $status = 'pending';
       // $from_trans_date if want to process all record then set $from_trans_date = '';
//       $from_trans_date = '';
       $allGatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByOrderIdAndStatus($payMode,$status,$from_trans_date);
       $pendingTransactions = 0;
       foreach($allGatewayOrders as $gatewayOrder)
       {
           $orderId =  $gatewayOrder->getOrderId();
           // check if a job already exists
           $ifJobCreated = Doctrine::getTable('EpJob')->ifJobCreated($orderId);
           if(!$ifJobCreated){
             $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode,$status);
             if($gatewayOrder){
               // create new job for pending visa payments
               $url = "vbv_configuration/pendingVisaPayments/" . $orderId;
               $taskId = EpjobsContext::getInstance()->addJob('ClearPendingVisaPayments', $url, array('order_id' => $orderId));
               sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
               $pendingTransactions++;
             }
           }
           //$this->verfiyAndUpdateByOrderId($orderId);
       }

       //return $this->renderText("VBV processing done");
       return $this->renderText("$pendingTransactions job(s) created to check/update VBV payment status for pending transactions");
      }
      catch(Exception $e ) {
        $this->logMessage("Logging the exception from VBV Updation Processing..".$e->getMessage().var_dump(number_format(memory_get_peak_usage())));
      }

    }


   /**
   * OSSCUBE
   * Process pending VISA transactions by creating entry
   * into ep_job for each such transaction.
   * 
   */
  public function executePendingVisaPayments(sfWebRequest $request) {
    $orderId = $request->getParameter('order_id');
    $this->verfiyAndUpdateByOrderId($orderId);
//	return sfView::NONE;
    return $this->renderText('SUCCESS');
  }


  public function executeVbvFormRedirect(sfWebRequest $request) {
    $this->setLayout(false);
    $order_id = base64_decode($request->getParameter('id'));
    $epVbvManagerObj = new EpVbvManager();
    $this->retObj = $epVbvManagerObj->selectAllFromRequest($order_id);//die('tek');
    $this->setTemplate('vbvFormRedirect');
  }

  public function executeVbvRedirect(sfWebRequest $request) {
    $this->pageTitle = 'Visa Payment';
    return $this->renderText('Please wait...');
  }

  protected function saveVbvOrder($order_id, $tarnsaNo, $type) {
    $success = Doctrine::getTable('VbvOrder')->saveOrder($order_id, $tarnsaNo, $type);
  }


  public function executeVbvResponseApprove(sfWebRequest $request) {//Approve
    $dontSave = true;
    $response = $this->saveResponse($request,$dontSave);
    $orderId = $response['orderid'];
//    $chkOrderStatus = $this->checkOrderStatus($orderId);

    $checkOrderObj = new PaymentVerify();
    $chkOrderStatus = $checkOrderObj->checkOrderStatus($orderId);



    if($chkOrderStatus['orderstatus'] != 'APPROVED')
    {
        $this->forward($this->getModuleName(), 'vbvResponseDecline');
    }   else     {

        $response = $this->saveResponse($request);
        $orderArray = $this->getAppIdAndType($orderId);

        $response['status'] = 'success';

        if($orderArray) {
          $txnId = $orderArray['app_id'];
          $type = $orderArray['type'];
          $payment_mode_option_id =$orderArray['payment_mode_option_id'];

          if(strtolower($type) == 'payment') {
            $arrayVal = $this->processToPayment($response,$txnId);

            $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], true) ;

            if($arrayVal['comments'] == 'error') {
              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
                  'action' => 'PayByCard','transNo' =>$arrayVal['var_value']))."'</script>");
            }else {
               return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'vbv_configuration',
                  'action' => 'payOnline','returnVal' =>base64_encode($arrayVal['var_value'])))."'</script>");
            }

            return false;
          }
          else if(strtolower($type) == 'recharge') {

            if($this->getStatus($response['orderid']) == 'pending')
            {
              $validationNo = $this->processToRecharge($response,$orderArray,$txnId, $payment_mode_option_id);
              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
                    'action' => 'rechargeReceipt','validationNo'=> base64_encode($validationNo)))."'</script>");
            }else{
//              return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
//                    'action' => 'accountBalance'))."'</script>");
               $this->getUser()->setFlash('error', 'Recharge Unsuccessful,Please try again', true) ;
               return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
                'action' => 'recharge','recharge_gateway'=>'credit_card'))."'</script>");
            }
          }
        }
    } // Is valid payment
    ////////////////////////////////////////////////////////////////////////
  }

  public function executeVbvResponseDecline(sfWebRequest $request) {
    if(!$request->hasParameter("xmlmsg"))
     {
         $this->getUser()->setFlash('error', 'Transaction Decline.', true) ;
        return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
                'action' => 'recharge','recharge_gateway'=>'credit_card'))."'</script>");
     }

    $response = $this->saveResponse($request);
    $orderArray = $this->getAppIdAndType($response['orderid']);
    $response['status'] = 'failure';
    $updated = $this->updateGatewayOrder($response);

    $appId = $orderArray['app_id'];
    $type = $orderArray['type'];

    if(strtolower($type) == 'payment') {
      if(array_key_exists('pan',$response))
      $this->getUser()->setFlash('error', 'Payment Unsuccessful. Please try again ', true) ;
      else
      $this->getUser()->setFlash('error', 'Invalid payment.', true) ;

      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'bill',
          'action' => 'PayByCard','transNo' =>$appId))."'</script>");
    }
    else if(strtolower($type) == 'recharge') {
      if(array_key_exists('pan',$response))
      $this->getUser()->setFlash('error', 'Recharge Unsuccessful,Please try again', true) ;
      else
      $this->getUser()->setFlash('error', 'Invalid payment.', true) ;
      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
                'action' => 'recharge','recharge_gateway'=>'credit_card'))."'</script>");
    }
  }

   public function executeVerifyCurrentStatus(sfWebRequest $request) {

    $orderId = $request->getParameter('orderId');
    $payMode = $this->getCreditCardPayModeId();

         $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode);
         //if it is pending... check
         if($gatewayOrder->getStatus()!='pending')
           {
                $dontPay =  true;
                $retrunArr  = $this->redirectToStatusPage($orderId,$dontPay);

                 return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', $retrunArr)."'</script>");
           }
         else
         {
            $vbVManager = new vbvConfigurationManager();
            $isValidPayment = $vbVManager->paymentVerify($orderId);
            $vbvVerfyObjs = Doctrine::getTable('EpVbvVerify')->findByOrderId($orderId);
             $vbvVerfyObj  =  $vbvVerfyObjs->getfirst();
             $transRes =$vbvVerfyObj->getTransactionstatusresult();
             $transResArr = explode('~',$transRes);

             $response['purchaseamount'] = $transResArr[2] ;
             $response['responsecode'] = $transResArr[0] ;
             $response['responsedescription'] = $transResArr[1] ;
             $response['trandatetime']  = $transResArr[3] ;
             $response['orderid'] =  $orderId;
             $response['pan'] = '' ;
             $response['approvalcode'] = '' ;
            if($isValidPayment)
            {
                $response['status']='success';
                $retrunArr =$this->redirectToStatusPage($orderId,false,$response);
                return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', $retrunArr)."'</script>");
            }
            else
              {
                  $response['status']='failure';
                  $gatewayOrder->setStatus('failure');
                  $gatewayOrder->setResponseCode($response['responsecode']);
                  $gatewayOrder->setResponseTxt ($response['responsedescription']);
                  list ($date, $time) = explode(' ', $response['trandatetime']);
                  if($date == '1/1/1900')
                    $transDate = date('Y-m-d H:i:s');
                  else
                    $transDate = $this->convertDate($response['trandatetime']);
                  $gatewayOrder->setTransactionDate($transDate);
                  $gatewayOrder->save();

                  $dontPay =  true;
                  $retrunArr = $this->redirectToStatusPage($orderId,$dontPay);
                  return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', $retrunArr)."'</script>");
              }
         }


  }

  protected function redirectToStatusPage($orderId,$dontPay=true,$response=array())
  {


      $orderArray = $this->getAppIdAndType($orderId);
      $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId);
      $txnId = $orderArray['app_id'];
      $type = $orderArray['type'];
      $payment_mode_option_id = $gatewayOrder->getPaymentModeOptionId();

      if(!$dontPay)
      {
         $vbvVerfyObjs = Doctrine::getTable('EpVbvVerify')->findByOrderId($orderId);
         $vbvVerfyObj  =  $vbvVerfyObjs->getfirst();
         $transRes =$vbvVerfyObj->getTransactionstatusresult();
         $transResArr = explode('~',$transRes);

         $response['status']='success';
         $response['purchaseamount'] = $transResArr[2] ;
         $response['responsecode'] = $transResArr[0] ;
         $response['responsedescription'] = $transResArr[1] ;
         $response['trandatetime']  = $transResArr[3] ;
         $response['orderid'] =  $orderId;
         $response['pan'] = '' ;
         $response['approvalcode'] = '' ;
      }


      if(strtolower($type) == 'payment') {

        if($dontPay)
        {
            if($gatewayOrder->getStatus() == 'success')
                {
                    $arrayVal['comments'] = 'notice';
                    $arrayVal['comments_val'] = 'Payment Successful';
                    $arrayVal['var_value'] = $txnId;
                }
            if($gatewayOrder->getStatus() == 'failure')
                {
                    $arrayVal['comments'] = 'error';
                    $arrayVal['comments_val'] = 'Payment Unsuccessful. Please try again';
                    $arrayVal['var_value'] = $txnId;
                }

        }
        else
        {
            $arrayVal = $this->processToPayment($response,$txnId);
            $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], true) ;
        }
         $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], true) ;
        if($arrayVal['comments'] == 'error') {
            $returnArr['module'] = 'bill';
            $returnArr['action'] = 'PayByCard';
            $returnArr['transNo'] = $arrayVal['var_value'];
            return  $returnArr;
          }else {
              $returnArr['module'] = 'vbv_configuration';
            $returnArr['action'] = 'payOnline';
            $returnArr['returnVal'] = base64_encode($arrayVal['var_value']);
            return  $returnArr;
              }

          return false;
      }//payment if

      if(strtolower($type) == 'recharge') {

             if($gatewayOrder->getStatus() == 'pending')
                 {
                     if(!$dontPay)
                        {
                            $validationNo = $this->processToRecharge($response,$orderArray,$txnId, $payment_mode_option_id);
                            $returnArr['module'] = 'ewallet';
                            $returnArr['action'] = 'rechargeReceipt';
                            $returnArr['validationNo'] = base64_encode($validationNo);
                            return  $returnArr;
                        }
                      else
                      {#30061
                            $this->getUser()->setFlash('error', 'Recharge Unsuccessful,Please try again', true) ;
                            $returnArr['module'] = 'recharge_ewallet';
                            $returnArr['action'] = 'recharge';
                            $returnArr['recharge_gateway'] = 'credit_card';
                            return  $returnArr;

                            //$validationNo = $gatewayOrder->getValidationNumber();
                      }

               }else{
                   if($gatewayOrder->getStatus() == 'failure')
                    {
                        $this->getUser()->setFlash('error', 'Recharge Unsuccessful,Please try again', true) ;
                        $returnArr['module'] = 'recharge_ewallet';
                        $returnArr['action'] = 'recharge';
                        $returnArr['recharge_gateway'] = 'credit_card';
                        return  $returnArr;
                    }
                    else
                    {#30061
                        $validationNo = $gatewayOrder->getValidationNumber();
                        $this->getUser()->setFlash('notice', 'Recharge Successful', true) ;
                        $returnArr['module'] = 'ewallet';
                        $returnArr['action'] = 'rechargeReceipt';
                        $returnArr['validationNo'] = base64_encode($validationNo);
                        return  $returnArr;
                    }

                }

         }

  }
  protected function saveResponse($request,$dontSave=false) {
    $xmlmsg = strtolower($request->getParameter("xmlmsg"));

    $xdoc = new DOMDocument;
    $isloaded = $xdoc->loadXML($xmlmsg);
    $a = $xdoc->saveXML();
    $setResponse = array();

    $p = xml_parser_create();
    xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
    xml_parse_into_struct($p, $xmlmsg, $vals, $index);
    xml_parser_free($p);

    foreach($vals as $k=>$v) {
      $key = $v['tag'];


      if(array_key_exists('value',$v)) {
        $value = $v['value'];
      }
      else {
        $value="";
      }
      if($key == 'orderid') {
              $orderArr = Doctrine::getTable('EpVbvRequest')->getOrderId($value);
              $setResponse[$key] = $orderArr['order_id'];
              $gatewayOrderDetails = Doctrine::getTable('Gatewayorder')->findByOrderId($orderArr['order_id']);
              if($gatewayOrderDetails->getFirst()->getType()=='recharge'){
                  $type = 'response_recharge';
              }else{
                  $type = 'response_payment';
              }
          }else {
              $setResponse[$key] = $value;
          }
      }

    $setResponse['msgdate'] = $xdoc->getElementsByTagName('message')->item(0)->getAttribute('date');


    /// Do not save xml and in database if caller defined
    if(!$dontSave)
    {
        $this->createLog($xmlmsg, $type, $setResponse['orderid']);

        $epVbvManager = new EpVbvManager();
        $retObj = $epVbvManager->setResponse($setResponse, $xmlmsg);
    }

    return $setResponse;
  }
  protected function processToPayment($response,$txnId) {
      $orderId = $this->updateGatewayOrder($response);
      $paymentObj =  new Payment($txnId);
      $arrayVal = $paymentObj->proceedToPay();

      if($arrayVal['comments'] != 'error'){
         // $this->addJobForMail($arrayVal['var_value']);

          $gatewayOrderObj = new gatewayOrderManager();
          $pfmTransactionDetails = $gatewayOrderObj->getValidationNumber($arrayVal['var_value']);
          $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
          $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo,$orderId);
         }
      return $arrayVal;

  }
    protected function convertDate($msgdate)
      {
            list ($date, $time) = explode(' ', $msgdate);
            if(strstr($date,'/')!==false){
                list ($dd, $mm, $yyyy) = explode('/', $date);
            }
            if(strstr($date,'-')!==false){
                list ($yyyy,$mm,$dd) = explode('-', $date);
            }
            $newDate = $yyyy.'-'.$mm.'-'.$dd.' '.$time;
            $date4Db = date('Y-m-d H:i:s',strtotime($newDate));           

            return $date4Db;
      }
  protected function processToRecharge($response,$orderArray,$txnId, $payment_mode_option_id) {

      $orderId = $this->updateGatewayOrder($response);
      $total_amount_paid = $response['purchaseamount'];

      $service_charge = $orderArray['servicecharge_kobo'];
      $gatewayOrderObj = new gatewayOrderManager();
      $validationNo = $this->recharge($txnId, $total_amount_paid, $payment_mode_option_id,$service_charge);
      $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo,$orderId);
       // [WP033][Inhouse maintainace]
      // add job mail to send on valu card recharge

      // OSSCUBE : fetch user ID for the process to be passed as argument to create job.
      $orderArr = Doctrine::getTable('GatewayOrder')->getByOrderId($orderId);
      $userId = $orderArr['user_id'];

      $this->addJobForValuCardRechageMail($validationNo, $userId);
      return $validationNo;

  }
  protected function getAppIdAndType($order_id) {
    $orderArr = Doctrine::getTable('GatewayOrder')->getByOrderId($order_id);
    return $orderArr;
  }
  protected function createLog($xmldata, $type, $orderId) {
    $nameFormate = $type.'_'.$orderId;
    $pay4meLog = new pay4meLog();
    $pay4meLog->createLogData($xmldata,$nameFormate,'vbvlog');
  }
  protected function getMerchantRecord($appId) {
    $merchantRecord = Doctrine::getTable('Transaction')->getMerchantRecord($appId);
    return $merchantRecord;
  }
  protected function validateTransactionNumber($transNo, $paymentModeOption) {
    return $merchantRecord = Doctrine::getTable('Transaction')->validateTransactionNumber($transNo, $paymentModeOption);
  }

  protected function getAmount($transNo) {
    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $formData = $payForMeObj->getTransactionRecord($transNo);
    return $formData['total_amount'];
  }
  protected function getLastTransNo($userId) {
     return $transNo = Doctrine::getTable('EpVbvRequest')->getLastTransNo($userId);
  }
  protected function getCreditCardPayModeId() {
     $payModeName  = sfConfig::get('app_payment_mode_option_credit_card');
     $payModeOptions = Doctrine::getTable('PaymentModeOption')->findByName($payModeName);
     $payMode = $payModeOptions->getfirst()->getId();
     return $payMode;
  }
  protected function isFirstVbvRequest($transNo) {
    return $merchantRecord = Doctrine::getTable('EpVbvRequest')->isFirstVbvRequest($transNo);
  }

  public function executePayOnline(sfWebRequest $request) {

    $txn_no = base64_decode($request->getParameter('returnVal'));
    //print_r($request->getParameterHolder()->getAll());exit;
    if($request->hasParameter('duplicate_payment_receipt')) {
      $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
    }

    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);


    $this->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
    $this->forward('paymentSystem', 'payOnline');
  }

  public function setTemplateForReceipt() {
    $billObj = billServiceFactory::getService();
    $walletDetails = $billObj->getEwalletAccountValues();
    $this->errorForEwalletAcount = '';
    if($walletDetails==false) {
      $this->errorForEwalletAcount = "Invalid Account Number";
    }
    else {
      $this->accountObj = $walletDetails->getFirst();
      $this->userDetailObj = $walletDetails->getFirst()->getUserDetail()->getFirst();
      $this->accountBalance =$billObj->getEwalletAccountBalance($this->accountObj->getId());
    }
   // untested code
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
    if ($userDetails[0]['ewallet_type'] == 'guest_user') {
    	Doctrine::getTable('AppSessions')->clearUserSessionByUsername($user_details[0]['email']);
    }
    $this->setLayout('layout_admin');
    $this->setTemplate('vbvPayReceipt');

  }


  /**
  * function addJobForMail()
  * @param   $txnId: transaction_id
  * Purpose : add job to send ewallet payment
  */

   protected function addJobForMail($txnId) {
    //$notificationUrl = 'vbv_configuration/sendMail';
    $notificationUrl = 'email/sendMail';
    $user = $this->getUser();
    $userId =$user->getGuardUser()->getId();
    $taskId = EpjobsContext::getInstance()->addJob('MailNotification',$notificationUrl, array('p4m_transaction_id' => $txnId,'user_id'=>$userId));

    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
  }


  /**
  * function addJobForValuCardRechageMail()
  * @param $validation_number :validation number
  * @param $userId :user Id (added by OSSCUBE)
  * Purpose : add job to send ewallet payment
  */


  protected function addJobForValuCardRechageMail($validation_number, $userId) {
    //$notificationUrl = 'vbv_configuration/sendMail';
    $notificationUrl = 'email/sendValueCardRechargeMail';

    //$user = $this->getUser();
    //$userId =$user->getGuardUser()->getId();
    $taskId = EpjobsContext::getInstance()->addJob('MailNotification',$notificationUrl, array('validation_number' => $validation_number,'user_id'=>$userId));
    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
  }


  //public function executeSendMail(sfWebRequest $request) {
    //    $this->setLayout(null);
    //    $mailingClass = new Mailing();
    //    $partialName = 'mailTemplate';
    //
    //    $mailingOptions = array();
    //    $mailInfo = 'Mail is sent successfully';
    //
    //    $txnId = $request->getParameter('p4m_transaction_id');
    //    $partialVars = pfmHelper::getMailData($txnId);
    //    if(count($partialVars))
    //    {
    //      $mailingOptions = $partialVars['mailingOptions'];
    //      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
    //      return $this->renderText($mailInfo);
    //    }
//  }


  private function recharge($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {

    if(Settings::getVbvClearanceDays()) {
      return $this->rechargeLater($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);
    }
    else {
      return $this->rechargeImmediate($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);
    }
  }

  private function rechargeLater($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {
    $con = Doctrine_Manager::connection();
    try {
      $con->beginTransaction();

      sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
      $days = settings::getVbvClearanceDays();
      $recharge_date = getRechargeDate($days);
      $reachargeObj = rechargeAmountAvailabilityServiceFactory::getService();
      $recharge_id = $reachargeObj->createNew($total_amount_paid,$total_amount_paid,$service_charge,$txnId,$recharge_date,$payment_mode_option_id);

      //create a new task
      $taskId = EpjobsContext::getInstance()->addJob('RechargeEwallet', 'recharge_ewallet/rechargeViaCard', array('recharge_id'=>$recharge_id), -1, NULL, $recharge_date );
      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

      //create credit , unclear, direct entries for collection_acct, eWallet_user_acct and  pay4me collection account
      //$userObj = $this->getUser();
      //$ewallet_account_id =  $userObj->getUserAccountId();
      $acctObj = new EpAccountingManager();
      $detailObj = $acctObj->getAccount($txnId);
      $ewallet_number = $detailObj->getAccountNumber();
      $accountingObj = new pay4meAccounting;
      $is_clear = 0;
      $accountingArr = $accountingObj->getRechargeAccounts($ewallet_number,$txnId,$total_amount_paid,$payment_mode_option_id,$is_clear,EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT, $service_charge);
      if(is_array($accountingArr)) {

        //auditing that ewallet has been charged
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_VISA;
        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_TRANSACTION,
          EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_VISA,
          EpAuditEvent::getFomattedMessage($msg, array('account_number'=>$ewallet_number)));

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

        $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));
      }
      $con->commit();
      $clearance_days = settings::getVbvClearanceDays();
      if($clearance_days == 1) {
        $clearance_days .= " day.";
      }
      else {
        $clearance_days .= " days.";
      }
      $this->getUser()->setFlash('notice', 'Payment Successful. The amount clearance will take '.$clearance_days , true) ;
      return $event->getreturnValue();
    }
    catch (Exception $e) {
      $con->rollback();
      throw $e;
    }
  }

  private function rechargeImmediate($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge)
  {
    $con = Doctrine_Manager::connection();
    try {
      $con->beginTransaction();
      $acctObj = new EpAccountingManager();
      $detailObj = $acctObj->getAccount($txnId);
      $ewallet_number = $detailObj->getAccountNumber();
   /*   $userObj = $this->getUser();
      $ewallet_account_id =  $userObj->getUserAccountId();
      $ewallet_number = $userObj->getUserAccountNumber();*/
      $accountingObj = new pay4meAccounting;
      $is_clear = 1;
      $accountingArr = $accountingObj->getRechargeAccounts($ewallet_number,$txnId,$total_amount_paid,$payment_mode_option_id,$is_clear,EpAccountingConstants::$TRANSACTION_TYPE_DIRECT, $service_charge);
      if(is_array($accountingArr)) {

        //auditing that ewallet has been charged
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_VISA;
        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_TRANSACTION,
          EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_VISA,
          EpAuditEvent::getFomattedMessage($msg, array('account_number'=>$ewallet_number)));

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

        $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));

        $collection_account_id = $accountingObj->getCollectionAccount($payment_mode_option_id, NULL, NULL, 'Recharge');
        $managerObj = new payForMeManager();
        $managerObj->updateEwalletAccount($collection_account_id, $txnId, $total_amount_paid,'credit', $service_charge);
      }
      $con->commit();
      $this->getUser()->setFlash('notice', 'Recharge Successful', true) ;
      return $event->getreturnValue();
    }
    catch (Exception $e) {
      $con->rollback();
      throw $e;
    }
  }

  protected function updateGatewayOrder($response)
  {
  //  print "<pre>";print_r($response);exit;
    $updateParamArr = array();
    $updateParamArr['status'] = $response['status'];
    $updateParamArr['amount'] = $response['purchaseamount'];
    $updateParamArr['code'] = $response['responsecode'];
    $updateParamArr['desc'] = $response['responsedescription'];
    $updateParamArr['date'] = $this->convertDate($response['trandatetime']);
    $updateParamArr['order_id'] = $response['orderid'];
    if(array_key_exists('pan',$response))//FS#30029
    $updateParamArr['pan'] = $response['pan'];
    $updateParamArr['approvalcode'] = $response['approvalcode'];
    $updated = Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr);
    return $response['orderid'];
  }
  function getStatus($order_id)
  {
    return $getLock = Doctrine::getTable('GatewayOrder')->getStatus($order_id);

  }


  function executeV(){
        $this->checkOrderStatus('2475');
        die;
  }

    public function executeVbvQueryStatus(sfWebRequest $request) {
        $this->show = false;
        $this->setLayout('layout_popup');
        $getData = array();
        $txRef = $request->getParameter('orderId');
        $txnId = $request->getParameter('appId');
        //get Gateway order details
        $paymentModObj = Doctrine::getTable('GatewayOrder')->getByOrderId($txRef);
        $paymentModeOption = $paymentModObj['payment_mode_option_id'];
        $type = $paymentModObj['type'];
        $status = $paymentModObj['status'];
        $paymentmode = $paymentModeOption;
        $service_charge = $paymentModObj['servicecharge_kobo'];
        $intRefNo = '';
        if ($type == "payment") {
            $intRefNo = $txnId;
        } else {
            $intRefNo = $txRef;
        }

        if ($status != "success") {
            if ($type == "payment") {
                $this->checkpaymentquerydetails($txnId, $paymentModeOption);
            }

            $epInterswitchManager = new EpVbvManager();
            //get all details from EpInterswitchRequest
            $getResponse = $epInterswitchManager->selectAllFromRequest($txRef);
            //if any records presents
            if (count($getResponse)) {
                //get merchant id
                if (array_key_exists('mer_id', $getResponse[0])) {
                    $mer_id = $getResponse[0]['mer_id'];
                    $order_id = $getResponse[0]['order_id'];
                    $vbv_order_id = $getResponse[0]['vbv_order_id'];
                }

                //get amount in kobo
                $amt = $getResponse[0]['purchase_amount'];
                //Check response has valid amount or not

                //$verification = $this->checkOrderStatus($order_id);

                $checkOrderObj = new PaymentVerify();
                $verification = $checkOrderObj->checkOrderStatus($orderId);

                //get RESPONSE  Status Message
                $paymentStatusMsg = GatewayStatusMessages::getVbvStatusMessage('000');
                //save the response
//                $epInterswitchManager->saveForPending($getResponse[0]['id'], $verification['respCode'], $amt, $paymentStatusMsg);
                //set values to getData variable
                $getData['txnref'] = $txRef;
                $getData['apprAmt'] = $amt;
                $getData['rspcode'] = $verification['respCode'];
                $getData['rspdesc'] = $paymentStatusMsg;
                $getData['date'] = $verification['transDate'];
                //if verification is sucess
                if ($verification['respCode'] == '000') {
                    $getData['status'] = 'success';
                    //  $getData['pan'] = $verification['respPan'];
                    $getData['approvalcode'] = $verification['approvalCode'];
                    $getData['rspdesc'] = 'Approved, balances available';
                    //update Gateway Order

                    $updated = $this->updateGatewayOrder($getData);

                    //if type is payment
                    if ($type == "payment") {
                        $paymentObj = new Payment($txnId);
                        $arrayVal = $paymentObj->proceedToPay();
                        $gatewayOrderObj = new gatewayOrderManager();

                        $pfmTransactionDetails = $gatewayOrderObj->getValidationNumber($arrayVal['var_value']);
                        $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
                        $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $txRef);


                        $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], true);
                        if ($arrayVal['comments'] == 'error') {
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                        'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $txnId, 'type' => $type, 'pending' => 'all')) . "'</script>");
                        } else {
                            //$this->addJobForMail($arrayVal['var_value'],$type, $order_id);

                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                        'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $txnId, 'type' => $type, 'pending' => 'all')) . "'</script>");
                        }
                    } else {

                        //if type is recharge
                        //paymentmode option = 2 and service charges need to capture from top
                        $validationNo = $this->recharge($txnId, $amt, '2', $service_charge);
                        $gatewayOrderObj = new gatewayOrderManager();
                        $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $txRef);
                        $this->addJobForMail($txnId, $type, $txRef);
                        return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                    'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $txRef, 'type' => $type, 'pending' => 'all')) . "'</script>");
                    }
                    //if transaction is not sucessfull
                } else {
                    //update gateway order table
                    $getData['status'] = 'failure';
                    $updated = $this->updateGatewayOrder($getData);
                    $this->getUser()->setFlash('error', $paymentStatusMsg, false);
                    return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $intRefNo, 'type' => $type, 'pending' => 'all')) . "'</script>");
                }
                //if no request detail
            } else {
                $this->getUser()->setFlash('error', 'No Transaction Record', false);
            }
        } else {
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                        'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $intRefNo, 'type' => $type, 'pending' => 'all')) . "'</script>");
        }
        die;
    }

    private function checkpaymentquerydetails($txnId, $paymentModeOption) {
        //validate transaction number if count >0 returns array else returns 0
        $validateTrans = $this->validateTransactionNumber($txnId, $paymentModeOption);
        //if not equal to zero
        if ($validateTrans) {
            //get item id from
            $item_id = $validateTrans['MerchantRequest']['merchant_item_id'];
            //get merchant request payment status
            $payment_status = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
            //if payment status if it is zero payment done else payment not done
            //if payment already done update accordingly
            if ($payment_status == 0) {
                $merchant_obj = Doctrine::getTable('MerchantRequest')->getMerchantPaidId($item_id);
                $merchant_request_payment_id = $merchant_obj->getFirst()->getId();
                $transaction_number_payment_id = $merchant_obj->getFirst()->getTransaction()->getFirst()->getTransactionNumber();
                $this->getRequest()->setParameter('txn_no', $transaction_number_payment_id);
                $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
                $this->forward('paymentSystem', 'payOnline');
            }
        } else {
            $this->getUser()->setFlash('error', 'No Transaction Record', false);
        }
    }

    public function executeVbvResponseCancel(sfWebRequest $request){

        if (!$request->hasParameter("xmlmsg")) {
            $this->getUser()->setFlash('error', 'Transaction Cancel.', true);
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'recharge_ewallet',
                        'action' => 'recharge', 'recharge_gateway' => 'credit_card')) . "'</script>");
        }

        $response = $this->saveResponse($request);

        $orderArray = $this->getAppIdAndType($response['orderid']);

        $response['status'] = 'failure';
        $updated = $this->updateGatewayOrder($response);
        $appId = $orderArray['app_id'];
        $type = $orderArray['type'];

        if (strtolower($type) == 'payment') {
                $this->getUser()->setFlash('error', 'Payment cancelled.', true); // WP059 Bug:35839

            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'bill',
                        'action' => 'PayByCard', 'transNo' => $appId)) . "'</script>");
        }
        else if (strtolower($type) == 'recharge') {
            $this->getUser()->setFlash('error', 'Recharge cancelled.', true);   // WP059 Bug:35839
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'recharge_ewallet',
                        'action' => 'recharge', 'recharge_gateway' => 'credit_card')) . "'</script>");
        }
    }
    public function executeCheck(sfWebRequest $request){
        $orderId = $request->getParameter('orderId');
        $resArr= $this->checkOrderStatus($orderId);
        $this->getUser()->setFlash('error',$resArr['msg'] , true) ;
          if($resArr['type']=='payment'){
        return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => $resArr['module'],
          'action' => $resArr['action'],'transNo' =>$resArr['transNo']))."'</script>");
          }else{
               return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => $resArr['module'],
          'action' => $resArr['action'],'recharge_gateway' =>credit_card))."'</script>");
          }



    }
    public function checkOrderStatus($orderId){
        $responseArr = array();
        $checkOrderObj = new PaymentVerify();
        $payMode = $this->getCreditCardPayModeId();
        $responses = $checkOrderObj->checkOrderStatus($orderId);
        $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode);
        $action='';
        $module='';
        $msg='';
        if($gatewayOrder['type']=='payment'){
                if(in_array($responses['orderstatus'],sfConfig::get('app_vbv_orderstatus_error_msg')) || $responses['orderstatus']=='APPROVED' ){
                    $action='PayByCard';
                    $module='bill';
                    $msg='Your Payment is in progress. Please contact Visa for further details.';
                    $status='error';
                }
                else if(in_array($responses['orderstatus'],sfConfig::get('app_vbv_orderstatus_reinitiate')) || ($responses['orderstatus']=='CREATED')){
                    $action = 'PayByCard';
                    $module = 'bill';
                    $msg='Your Payment is Unsuccessful. Please try again ';
                    $status='error';
                }
        }else
            {

                if(in_array($responses['orderstatus'],sfConfig::get('app_vbv_orderstatus_error_msg')) || $responses['orderstatus']=='APPROVED' ){
                    $action='recharge';
                    $module='recharge_ewallet';
                    $msg='Your Recharge is in progress. Please contact Visa for further details. ';
                    $status='error';
                }
                else if(in_array($responses['orderstatus'],sfConfig::get('app_vbv_orderstatus_reinitiate')) || ($responses['orderstatus']=='CREATED')){
                    $action = 'recharge';
                    $module = 'recharge_ewallet';
                    $msg='Your Recharge is Unsuccessful. Please try again ';
                    $status='error';
                }
            }

           $responseArr['status']=$status;
           $responseArr['action']=$action;
           $responseArr['module']=$module;
           $responseArr['msg']=$msg;
           $responseArr['transNo']=$gatewayOrder['app_id'];
           $responseArr['type']=$gatewayOrder['type'];
        return $responseArr;

    }
}
