<?php //use_helper('Form') ; ?>

<?php

?>
<div id="printSlip" class='wrapForm2'>

  <?php echo ePortal_pagehead("Payment Receipt", array('id'=>'dynamicHeading')); ?>
  <?php echo form_tag('paymentSystem/payOnline',array('name'=>'pfm_nis_details_form','class'=>'dlForm multiForm', 'id'=>'pfm_nis_details_form')) ?>

          <?php
          echo ePortal_legend('User eWallet Account Details');
                if($errorForEwalletAcount){
                     echo formRowFormatRaw('Error message ',$errorForEwalletAcount);
                     }else{
                           echo formRowFormatRaw('Account ID',ePortal_displayName($accountObj['account_number']));
                           echo formRowFormatRaw('Account Name',ePortal_displayName($accountObj['account_name']));
                         //  echo formRowFormatRaw('Account Balance',format_amount(ePortal_displayName($accountBalance),1,1));
                     } ?>
    <?php
    echo ePortal_legend('Application Details');
    echo formRowFormatRaw('Validation Number',$postDataArray['validation_number']);
    echo formRowFormatRaw('Transaction Number',$postDataArray['txnId']);
    if($MerchantData) {
      foreach($MerchantData as $k=>$v) {
        echo formRowFormatRaw($v."",$postDataArray[$k]);
      //    echo input_hidden_tag('appId', $nisServiceDetails['MerchantRequest']['MerchantRequestDetails'][$k]);

      }

    }
    echo formRowFormatRaw('Application Type',$postDataArray['serviceType']);
    ?>
    <?php
    echo ePortal_legend('User Details');
    echo formRowFormatRaw('Name',$postDataArray['name']);
    //        echo formRowFormatRaw('Mobile Number:',$postDataArray['mobNo']);
    //        echo formRowFormatRaw('Email:',$postDataArray['email']);
    ?>
    <?php
    echo ePortal_legend('Payment Charges');
    if($isClubbed == 'no'){
    echo formRowFormatRaw('Application Charges',format_amount($postDataArray['appCharge'],$postDataArray['currency_id']));
    if(!empty($postDataArray['bankCharge'])){
      echo formRowFormatRaw('Transaction Charges',format_amount($postDataArray['bankCharge'],$postDataArray['currency_id']));
    }
    if(!empty($postDataArray['serviceCharge'])){
      echo formRowFormatRaw('Service Charges',format_amount($postDataArray['serviceCharge'],$postDataArray['currency_id']));
    }
    }else{
      echo formRowFormatRaw('Application Charges',format_amount($postDataArray['totalCharge'],$postDataArray['currency_id']));
    }
    echo formRowFormatRaw('Total Payable Amount',format_amount($postDataArray['totalCharge'],$postDataArray['currency_id']));
     
    echo ePortal_legend('Bill Status');   
    if($postDataArray['payment_status_code'] == 0){echo formRowFormatRaw('Status','Payment Successful');}
    else echo formRowFormatRaw('Status','Payment not done');
     echo formRowFormatRaw('Payment Date:',formatDate($postDataArray['payment_date']));
    ?>
  <div class="dsTitle2">
  <center id="formNav">
    <input type="button" style = "cursor:pointer;" class="formSubmit" value="Print User Receipt" onclick="printSlip('user')">
  </center>
  </div>
</form>

</div>




<script>
  function printSlip(mode){

    defHeading = $('#dynamicHeading').html();
    heading = "";
    if(mode =='user'){
      heading +="User Receipt";
    }
    $('#dynamicHeading').html(heading);
    html = '';
    html += $('#printSlip').html();
    printMe(html,true);
    $('#dynamicHeading').html(defHeading);


  }
</script>

<?php if(isset($trans_number) && ($trans_number!="")){
$msg="Payment has already been made for the item against Transaction Number - ".$trans_number;
$cancel_url = url_for('admin/eWalletUser');?>

<SCRIPT language="JavaScript1.2">
  var msg = "<?php echo $msg;?>";
  var cancel_url = "<?php echo $cancel_url;?>";
function displaypop()
{
    alert(msg) ;
    document.getElementById('printSlip').style.visibility="visible";
    document.getElementById('printSlip').style.height="";
}
</SCRIPT>
<body onLoad="javascript: displaypop()" >
</body>
<?php }?>
