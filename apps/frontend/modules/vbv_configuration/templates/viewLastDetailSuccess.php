<?php use_stylesheet('master.css');
echo include_stylesheets(); ?>


<div id="mainPopupWraper">
  <div class="popupWrapForm2" style="background-color:#EFEFEF;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >

      <tbody>
        <?php
          $amount = format_amount($result->getAmount(), 1, 1);
          ?>
        <tr>
          <td>
            <?php echo ePortal_legend("Last Transacriotn Status");
            if($result->getTransactionDate())
            echo formRowFormatRaw('Transaction Date',$result->getTransactionDate()) ;
            if($result->getStatus()=='success' && $result->getValidationNumber())
            echo formRowFormatRaw('Validation Number',$result->getValidationNumber()) ;
            if($result->getType()=='payment') {
                  echo formRowFormatRaw('Transaction Number',$result->getAppId()) ;
            }

            echo formRowFormatRaw('Transaction Identification Number', $result->getOrderId()) ;
            if($result->getPan())
            echo formRowFormatRaw('Card Number',$result->getPan()) ;
            echo formRowFormatRaw('Amount',$amount) ;
            echo formRowFormatRaw('Status',$result->getStatus()) ;
            echo formRowFormatRaw('Details',html_entity_decode($result->getResponseTxt())) ;
            if($result->getApprovalCode())
                echo formRowFormatRaw('Approval Code',$result->getApprovalCode()) ;
            ?>           
          </td>
        </tr>
      </tbody>
    </table>
  </div>

</div>
