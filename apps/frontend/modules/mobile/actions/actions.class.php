<?php

/**
 * mobile actions.
 *
 * @package    mysfp
 * @subpackage mobile
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class mobileActions extends sfActions {

    public function executeAuthenticate(sfWebRequest $request) {
        //to do get mobile object and pass $request object
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseAuthenticateReq($request);
        return $this->returnResponse($response);
    }
    public function executeBalanceEnquiry(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseBalanceEnquiry($request);
        return $this->returnResponse($response);
    }

    public function executeTransferToWallet(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseTransferToWalletReq($request);
        return $this->returnResponse($response);
    }

    public function executeBillRequest(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseNewBillReq($request);
        return $this->returnResponse($response);
    }

    public function executeApprovedBillRequest(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseApprovedBillReq($request);
        return $this->returnResponse($response);
    }

    public function executeBillCountRequest(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseBillCountReq($request);
        return $this->returnResponse($response);
    }

    public function executeBillListRequest(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseBillListReq($request);
        return $this->returnResponse($response);
    }

    public function executePaymentDisAuthorize(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parsePaymentDisAuthorizeReq($request);
        return $this->returnResponse($response);
    }

    public function executeStatementRequest(sfWebRequest $request) {
        $mobObj = new mobileManager($request);
        $response = $mobObj->parseStatementReq($request);
        return $this->returnResponse($response);
    }
    private function returnResponse($response){
        $this->getUser()->signOutUser();
        return $this->renderText($response);
    }

}
