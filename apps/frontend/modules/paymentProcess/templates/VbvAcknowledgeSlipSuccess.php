<?php use_helper('ePortal');?>

<div id="printSlip">
  <?php echo ePortal_pagehead($pageTitle,array('id'=>'dynamicHeading'));
  echo ePortal_highlight('PLEASE TAKE THE PRINTED ACKNOWLEDGEMENT SLIP FOR YOUR FUTURE REFERENCE','',array('class'=>'red noPrint'));
  ?>
  <div id='acknowledgeSlip' class="wrapForm2">
      <?php
      echo ePortal_legend('Application Details');
      echo formRowFormatRaw('Transaction Number',$formData['pfm_transaction_number']);
      if($MerchantData)
      {
        foreach($MerchantData as $k=>$v)
        {
           echo formRowFormatRaw($v,$formData['MerchantRequest']['MerchantRequestDetails'][$k]);

        }

      }
    echo formRowFormatRaw('Application Type',$formData['MerchantRequest']['MerchantService']['name']);
      ?>
      <?php
      echo ePortal_legend('User Details');
      echo formRowFormatRaw('Name',ePortal_displayName($formData['MerchantRequest']['MerchantRequestDetails']['name']));
      ?>
      <?php
      echo ePortal_legend('Payment Details');
      echo formRowFormatRaw('Application Charges',format_amount($formData['MerchantRequest']['item_fee'], 1));
      if(!empty($formData['MerchantRequest']['bank_charge'])){
      echo formRowFormatRaw('Transaction Charges',format_amount($formData['MerchantRequest']['bank_charge'], 1));
      }if(!empty($formData['MerchantRequest']['service_charge'])){
      echo formRowFormatRaw('Service Charges',format_amount($formData['MerchantRequest']['service_charge'], 1));
       }
      echo formRowFormatRaw('Total Payable Amount',format_amount($formData['total_amount'], 1));
      ?>
  
<form name="ackSlip" id="ackSlip" action="<?php if($show) echo url_for('vbv_configuration/vbvForm');?>" method="post">
<div class="pixbr XY20">
  <input type="hidden" name="tarnsaNo" value="<?php echo $formData['pfm_transaction_number']; ?>"/>
  <input type="hidden" name="totalCharges" value="<?php echo $formData['total_amount']; ?>"/>
  <input type="hidden" name="type" value="pay"/>
  <input type="hidden" name="payby" value="visa"/>
  <div class="dsTitle2">
    <center id="multiFormNav">
    <input type="button" style = "cursor:pointer;" class='formSubmit' value="Print Acknowledgement Slip" onclick="printSlip()">&nbsp;&nbsp;
    <?php if($show){?><input type="submit" class='formSubmit' value="Continue to Pay"/>
    <?php }else{?><font size="2" color="red"><b>You are restricted to pay for this Merchant Service</b></font>
    <?php }?>
  </center>
  </div>
</div>
</form>
</div>
</div>
<?php  echo ePortal_highlight('* Please collect your Pay4Me Payment Slip','',array('class'=>'red noPrint'));?>

<script>
  function printSlip(){

    defHeading = $('#dynamicHeading').html();
    heading = "";

    $('#dynamicHeading').html(heading);
    html = '';
    html += $('#printSlip').html();
    
    printMe(html,true);
    $('#dynamicHeading').html(defHeading);
  }
</script>
