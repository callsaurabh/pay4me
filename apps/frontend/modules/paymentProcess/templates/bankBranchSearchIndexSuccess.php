<div class="innerWrapper">
<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form') ?>
<?php include_stylesheets('master.css') ?>
<?php echo form_tag('paymentProcess/bankBranchSearchResults', array('name' => 'pfm_search_bank_branch_form', 'class' => 'dlForm multiForm', 'id' => 'pfm_search_bank_branch_form', 'onsubmit' => 'return validate_search_bank_branch_form(this);')) ?>

<div class="feedbackForm_new">
    <div class="header">Search Nearest Bank Branch</div>
    <?php
    $include_merchant_widget = false;

    if (!$merchantId) {
        $include_merchant_widget = true;
    ?>

    <?php } ?>

    <?php include_partial('bankBranchSearchIndex', array('form' => $custom_BankBranchSearchIndexForm, 'include_merchant_widget' => $include_merchant_widget)); ?>
<input type="hidden" value="<?php echo $paymentMode;?>" name="paymentMode">

    <div style="clear:both;"></div>
    <div class="fieldName">&nbsp;</div>
    <div class="fieldWrap">

        <label>
            <input name="button" type="submit" class="button" id="button" value="Search Branch" />
        </label>
    </div>
    <div class="clearfix"></div>    
</div></div>

<script>
    var stateSelectId = '#state';
    $(document).ready(function(){
        $("#merchant_id").change(function(){
            // var bankSelectId = 'bank';
            var url = "<?php echo url_for('paymentProcess/populateBankDropDown'); ?>";
            var currentValue = $(this).val();
            $('#bank_div').hide();
            $('#bank_loader').show();
            $('#bank_loader').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');           
            $('#bank').load(url, {merchantId: currentValue,byPass:1 },function (data){
                if(data!=""){
                    $('#bank_loader').hide();
                    $('#bank_div').show();
                    $('#bank').html(data);

                }
                if(!currentValue){
                    $('#state').html('<option value="" selected>All States</option>');
                    $('#lga').html('<option value="" selected>All Lgas</option>');

                }
            });
        });

        $("#bank").change(function(){
            // var bankSelectId = 'bank';
            var url = "<?php echo url_for('paymentProcess/populateStatesDropDown'); ?>";
            $('#state_div').hide();
            $('#state_loader').show();
            $('#state_loader').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
            var currentValue = $(this).val();
            $('#state').load(url, {bankId: currentValue,byPass:1 },function (data){
                if(data!=""){
                    $('#state_loader').hide();
                    $('#state_div').show();
                    $('#state').html(data);
                }
                if(!currentValue){
                    $('#lga').html('<option value="" selected>All Lgas</option>');

                }
            });
        });


        $("#state").change(function(){
            // var bankSelectId = 'bank';
            var url = "<?php echo url_for('paymentProcess/populateLgasDropDown'); ?>";

            var currentValue = $(this).val();
            $('#lga_div').hide();
            $('#lga_loader').show();
            $('#lga_loader').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');

            $('#lga').load(url, {stateId: currentValue,byPass:1 },function (data){
                if(data!=""){
                    $('#lga_loader').hide();
                    $('#lga_div').show();
                    $('#lga').html(data);

                }
            });
            //         $.post(url, {'bankId': currentValue}, function(data){
            //             makeHtml(data);
            //         }, 'text');
        });


    });





    function validate_search_bank_branch_form(){
<?php if (!$merchantId) { ?>
                    if(document.getElementById('merchant_id').value == ''){
                        alert('Please select Merchant.');
                        document.getElementById('merchant_id').focus();
                        return false;
                    }
<?php } ?>
               }
</script>