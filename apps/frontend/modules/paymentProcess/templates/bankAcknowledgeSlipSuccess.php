<div class="innerWrapper">
<?php use_helper('ePortal'); ?>
<link href="<?php echo url_for('web/css/print.css') ?>" media="print" type="text/css" rel="stylesheet">
<input type="hidden" id ="transaction_number" Value="<?php echo $formData['pfm_transaction_number'];?>" /> <?php // for selenium tests only ?>
<div id="p4mBank">
    <h2><?php echo __($pageTitle); ?></h2><br/>

    <div class="descriptionArea">
        <P CLASS="cRed"><?php echo __('IN ORDER TO PROCESS YOUR APPLICATION FURTHER, PLEASE COLLECT YOUR VALIDATION NUMBER FROM BANK') ?></p>
        <?php echo ePortal_highlight(__('PLEASE TAKE THE PRINTED ACKNOWLEDGEMENT SLIP TO PARTICIPATING BANK FOR PAYMENT- PAYMENT CANNOT BE ACCEPTED WITHOUT THE ACKNOWLEDGEMENT SLIP'), '', array('class' => 'red noPrint')); ?>
    </div>
    <div id='acknowledgeSlip' class='wrapForm2'>

        <?php
        echo ePortal_legend('Application Details');
        echo formRowFormatRaw('Transaction Number', $formData['pfm_transaction_number']);

        if ($MerchantData) {
            foreach ($MerchantData as $k => $v) {
                if(!empty($formData['MerchantRequest']['MerchantRequestDetails'][$k])){
                    echo formRowFormatRaw(__($v), $formData['MerchantRequest']['MerchantRequestDetails'][$k]);
                }
            }
        }
        echo formRowFormatRaw(__('Application Type'), $formData['MerchantRequest']['MerchantService']['name']);
        ?>

        <?php
        echo ePortal_legend(__('User Details'));
        echo formRowFormatRaw(__('Name'), ePortal_displayName($formData['MerchantRequest']['MerchantRequestDetails']['name']));
        ?>
        <?php
        if ($paymentMode == sfConfig::get('app_payment_mode_option_Cheque')) {
            echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details'));
            echo formRowFormatRaw(__('Bank Name'), $formData['MerchantRequest']['bank_name']);
            echo formRowFormatRaw(__('Bank Sort Code'), $formData['sort_code']);
            echo formRowFormatRaw(__('Cheque Number'), $formData['check_number']);
            echo formRowFormatRaw(__('Payee Account Number'), $formData['account_number']);
            
        } elseif ($paymentMode == sfConfig::get('app_payment_mode_option_bank_draft')) {
            
                echo ePortal_legend(__(pfmHelper::getDraftDisplayName() . ' Details'));
                echo formRowFormatRaw(__('Bank Name'), $formData['MerchantRequest']['bank_name']);
//                echo formRowFormatRaw(__('Bank Sort Code'), $formData['sort_code']);
//                echo formRowFormatRaw(__('Bank Draft Number'), $formData['check_number']);
            }
        ?>
        <?php
            echo ePortal_legend(__('Payment Details'));

            if ($isClubbed == 'no') {

                echo formRowFormatRaw(__('Application Charges'), format_amount($formData['MerchantRequest']['item_fee'], $currencyId));

                if (!empty($formData['MerchantRequest']['bank_charge'])) {
                    echo formRowFormatRaw(__('Transaction Charges'), format_amount($formData['MerchantRequest']['bank_charge'], $currencyId));
                }
                if (!empty($formData['MerchantRequest']['service_charge'])) {
                    echo formRowFormatRaw(__('Service Charges'), format_amount($formData['MerchantRequest']['service_charge'], $currencyId));
                }
            } else {
                echo formRowFormatRaw(__('Application Charges'), format_amount($formData['total_amount'], $currencyId));
            }
            echo formRowFormatRaw('<b>' . __('Total Payable Amount') . '</b>', '<b>' . format_amount($formData['total_amount'], $currencyId)) . '</b>';
            //echo ePortal_highlight('* Please collect your Pay4Me Payment Slip','',array('class'=>'dsTitle2'));
        ?>



            <div style="clear:both;"></div><br/>
            <div class="call-msg-support">
                Please  contact The Pay4Me Support Team to report non-acceptance of your payment at designated banks for prompt attention.<br/>
                * Email: <a href="mailto:support@pay4me.com">support@pay4me.com</a><br/>
                * Phone: <?php echo sfConfig::get('app_support_contact_number');?> 


            </div>
            <div class="dsTitle2">
                <center id="formNav">
                    <button onclick="javascript:printMe('p4mBank')" class="button"><?php echo __('Print Acknowledgement Slip') ?></button>&nbsp;&nbsp;<button onclick="javascript:redirectTo();" class="button"><?php echo __('Back To') ?> <?php echo __($formData['MerchantRequest']['Merchant']['name']); ?></button>
                </center>
            </div></div></div>
<?php echo ePortal_highlight('*' . __('Please collect your Pay4Me Acknowledgement Slip'), '', array('class' => 'red noPrint')); ?>
<?php //echo url_for('pages/printMe');   ?>
    <script>
        function redirectTo()
        {
            window.parent.location = '<?php echo $formData['MerchantRequest']['MerchantService']['merchant_home_page']; ?>';
    }
</script>
</div>
