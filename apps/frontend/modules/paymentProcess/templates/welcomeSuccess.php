<div class="innerWrapper">
	<?php //use_helper('Form');?>
	<div id="selectPaymentMode">
		<?php echo ePortal_pagehead(__($pageTitle)); ?><br/>
		<div class="wrapForm2">
			<?php if(count($pay_mode_options_selection)){ ?>
				<form method="POST" id="select_mode">
					<div id="step1"><?php echo $form ;?></div>
					<?php if($display_visa_help) { ?>
						<div class="divBlock">
							<div class="dsTitle4Fields"></div>
							<div id='continue' class="dsInfo4Fields" style='display:block;'>
								<a href="javascript:;" onClick='window.open("<?php echo url_for("paymentProcess/enableVisa");?>","Visa",
"menubar=no,width=1100,height=360,toolbar=no");'><?php echo __('Instructions to activate Credit/Debit Card for Online Payment');?></a>
							</div>
						</div>
					<?php }?>

					<!--<div style="display:block;">-->
					<?php
					/*  $i=0;
					$mode =array();
					foreach($pay_mode_options_selection as $k=>$v){
					if($i===0) {$mode = $v ; $i++; continue;}
					echo "<div id='mode_{$k}' style='display:none;'>";
					echo ePortal_legend("Select {$mode[$k]} Type");
					echo formRowFormatRaw("Select {$mode[$k]}",select_tag("options_".$k, options_for_select(array_merge(array(''=>'- Please Select -'),$v->getRawValue())), 0));
					
					echo "</div>";
					//            }*/
					?>
					<?php  //echo "<pre>";print_r($pay_mode_options_selection); echo "</pre>"; ?>
					<!--</div>-->
					<div id='continue' class="divBlock" style='display:block; padding-top: 15px; padding-bottom:10px;'>
						<center id="formNav">
							<input type=button onClick='jump()' value=<?php echo __('Continue')?> class="button" />
						</center>
					</div>
				</form>
			<?php }else{?>
				Payment service temporarily deactivated.
			<?php }?>
		</div>
		<?php  //Patch code to display message of disabling cards/ewallet payment mode options temporarily. [WP076]
		if(sfConfig::get('app_disable_ewallet_cards',false)){
		?>
			<div id='flash_error'  class='error_list_user' style='text-align:left;'>
				<span><?php echo sfConfig::get('app_message_disable_ewallet_cards'); ?></span>
			</div>
		<?php } ?>
	</div>
</div>
<div id='selectUserType' class="wrapForm2" style='display:none;height: 130px; margin: 5px 0;'>
	<div class="wrapForm2">
		<div id="step2">
			<div class=" formHead"> Select User Type</div>
			<div id="userOptions_userType_row">
				<div class="dsTitle4Fields">
					<label for="userOptions_userType">I am <sup>*</sup></label>
				</div>
				<div class="dsInfo4Fields" style="width: 500px;">
					<ul class="user_list" style="list-style: none outside none;padding-left: 0;margin:0;">
						<li>
							<input onClick='jump2()' type="radio" id="user_type_ewallet" value="existingEwallet" name="userOptions[userType]">&nbsp;
							<label for="user_type_ewallet">Existing User <span style="font-weight:bold;">(eWallet account holders & Open ID Users)</span></label>
						</li>
						<li>
							<input onClick='jump2()' type="radio" id="user_type_new" value="newUser" name="userOptions[userType]">&nbsp;
							<label for="user_type_new">New User <span style="font-weight:bold;">(Registration Required)</span></label>
						</li>
						<li>
							<input onClick='jump2()' type="radio" id="user_type_guest" value="guestUser" name="userOptions[userType]">&nbsp;
							<label for="user_type_guest">Guest User <span style="font-weight:bold;">(No Registration Required)</span></label>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div id='sigin' style='display:none;' class="X20">
	<?php echo ePortal_legend(" "); ?>
	<div id="wrapper-login" style="text-align: center;"></div>
</div>
</div>
<style>
	.user_list li {
		/*float:left;*/
		padding:5px 5px 5px 0 !important;
		margin-right:10px;
		clear:both;
	}
	.dsInfo4Fields {
		background: none !important;
		border-left: none !important;
	}
</style>
<script>
	modeOption = '';
	function jump(){
		objName = $('#payOptions_payType_row li :checked ').val();
		if(!objName){
			alert('Please select payment mode');
			return false;
		} else {
			if(objName=='bank'){
				$('#payOptions_payMode').val(objName);
				var url = "<?php echo url_for('paymentProcess/bank');?>";
				$("#select_mode").attr("action",url);
				$('#select_mode').submit();
			} else {
				$('#selectUserType').show();
			}
		}
	}
	function jump2() {
		if(modeOption ==''){
			objType = $('#userOptions_userType_row li :checked ').val();
			(objType == 'existingEwallet')? headLine = 'Login with Ewallet Account': headLine = 'Login with OpenId Account';
			$("#sigin .formHead").html(headLine);
			objName = $('#payOptions_payType_row li :checked ').val();
			if(objName=='internet_bank' ){
				$('#payOptions_payMode').val(objName);
				var url = "<?php echo url_for('paymentProcess/internetbankAcknowledgeSlip');?>";
				$("#select_mode").attr("action",url);
				$('#select_mode').submit();
			}
			else if(objName=='cheque' ){
				$('#payOptions_payMode').val(objName);
				var url = "<?php echo url_for('paymentProcess/checkPayment');?>";
				$("#select_mode").attr("action",url);
				$('#select_mode').submit();
			}
			else if(objName=='bankdraft' ){
				$('#payOptions_payMode').val(objName);
				var url = "<?php echo url_for('paymentProcess/bankDraftPayment');?>";
				$("#select_mode").attr("action",url);
				$('#select_mode').submit();
			}
			else if(objName!='bank'){
				$("#wrapper-login").html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
				$('#payOptions_payMode').val(objName);
				$('#payOptions_userType').val(objType);
				var url = "<?php echo url_for('@ewallet_signin'); ?>";
				var payMode = $('#payOptions_payMode').val();
				var merchantRequestId = $('#payOptions_merchantRequestId').val();
				$("#wrapper-login").load(url,{payType:objName, payMode:payMode, merchantRequestId:merchantRequestId, byPass:1, userType:objType
				},function (data){
					if(data=='allready_login'){
						var requestId = $('#payOptions_merchantRequestId').val();
						var allreadyLoginURL = "<?php echo url_for('@ewallet_allready_login');?>?requestId="+requestId;
						$("#select_mode").attr("action",allreadyLoginURL);
						$("#select_mode").submit();
					}
					else{
						$('#sigin').slideDown('slow');
						$('#eWalletSiginIn').slideUp('slow');
					}
				});
				return false;
			}
			else{
				if($('#step1').slideUp()){
					modeOption = objName;
					$('#mode_'+objName).slideDown();
				}
			}
		}else{
			subOptionName = $('#mode_'+modeOption+'  select').val();
			if(!subOptionName){
				alert('Please select an option');
				return false;
			}
			var url = "";
			if(subOptionName == 'visa'){
				url = "<?php echo url_for('paymentProcess/vbvAcknowledge');?>";
			}else{
				url = "<?php echo url_for('paymentProcess/interswitch');?>";
			}
			$('#payOptions_payMode').val(subOptionName);
			$("#select_mode").attr("action",url);
			$('form').submit();
		}
		return ;
	}

	$("input[name='payOptions[payType]']").click(function(){
		$("input[name='userOptions[userType]']").attr('checked',false);
		var paymentOption = $(this).val();
		if(paymentOption=='bank'){
			$("#sigin_ewallet").slideUp('slow');
		} else{
			$('#sigin').hide();
			$('#selectUserType').hide();
			$('#selectUserEmail').hide();
		}
		if (paymentOption == 'ewallet') {
			$('#user_type_guest').attr('disabled',true);
			$('#payOptions_userType').val('');
		}else {
			$('#user_type_guest').attr('disabled',false);
		}
	});
</script>
