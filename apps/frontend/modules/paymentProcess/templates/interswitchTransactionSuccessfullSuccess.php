<?php use_helper('ePortal');?>
<?php echo ePortal_pagehead('Interswitch Transaction Response'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<div id="printMe" class="X20">
  <div class="dlForm">
    <?php
    if($respData['response_code'] =='00'){
      echo ePortal_highlight('','Transaction Successful',array('class'=>'error_list','id'=>'flash_notice'));
    }else{
      echo ePortal_highlight($respData['description'],'Transaction Fail',array('class'=>'error_list','id'=>'flash_error'));

    }
    ?>

    <fieldset>
      <?php


      echo ePortal_legend("transaction Details</p>");
      //echo ePortal_renderFormRow('id',$sFormData['id']);
      //echo  ePortal_renderFormRow('interswitch_request_id',$sFormData['interswitch_request_id']);
      //echo  ePortal_renderFormRow('merchant_service_id',$sFormData['merchant_service_id']);
      echo   ePortal_renderFormRow('Pay For Me Transaction Number',$formData['pfm_transaction_number']);
      //echo   ePortal_renderFormRow('response_code',$sFormData['response_code']);
      //echo   ePortal_renderFormRow('description',$sFormData['description']);
      //echo  ePortal_renderFormRow('payment_reference',$sFormData['payment_reference']);
      //echo   ePortal_renderFormRow('return_reference',$sFormData['return_reference']);
      //echo   ePortal_renderFormRow('card_num',$sFormData['card_num']);
      echo  ePortal_renderFormRow('Total Amount',format_amount($formData['MerchantRequest']['paid_amount'],1));
      //echo    ePortal_renderFormRow('Bank Name',$sFormData['bank_name']);
      echo    ePortal_renderFormRow('Transaction Date',$formData['created_at']);
      //echo   ePortal_renderFormRow('updated_at',$sFormData['updated_at']);


      ?>
    </fieldset>
    <fieldset>
      <?php
      echo ePortal_legend('Application Details');
      //echo formRowFormatRaw('Transaction Id',$formData['transaction_num']);
      if($MerchantData)
                {
                  foreach($MerchantData as $k=>$v)
                  {
                     echo formRowFormatRaw($v.":",$formData['MerchantRequest']['MerchantRequestDetails'][$k]);
                 //    echo input_hidden_tag('appId', $nisServiceDetails['MerchantRequest']['MerchantRequestDetails'][$k]);

                  }

                }

      echo formRowFormatRaw('Application Type',$formData['MerchantRequest']['MerchantService']['name']);
      ?>
    </fieldset>
    <fieldset>
      <?php
      echo ePortal_legend('User Details');
      echo formRowFormatRaw('Full Name',ePortal_displayName($formData['MerchantRequest']['MerchantRequestDetails']['name']));
      #echo formRowFormatRaw('Mobile',$formData['mobile']);
//      echo formRowFormatRaw('Email',$formData['email']);
      ?>
    </fieldset>

    <fieldset>
      <?php
      //echo ePortal_legend('Payment Details');
      //echo formRowFormatRaw('Application Charges',$formData['app_charges']);
      //echo formRowFormatRaw('Bank Charges',$formData['bank_charges']);
      //echo formRowFormatRaw('Total Payable Amount',$formData['app_charges']+$formData['bank_charges']);
      ?>
    </fieldset>
  </div>
  <br/>
</div>

<div class="pixbr XY20">
  <center id="formNav">
        <button onclick="javascript:printMe('printMe')">Print Acknowledgement Slip</button>&nbsp;&nbsp;&nbsp;<button onclick="javascript:redirectTo();">Back To <?php echo $formData['MerchantRequest']['Merchant']['name'];?></button>
  </center>
</div>
<?php //echo url_for('pages/printMe'); ?>

<br/>
<script>
  function redirectTo()
  {
    window.parent.location = '<?php echo $formData['MerchantRequest']['MerchantService']['merchant_home_page'];?>';
  }
</script>