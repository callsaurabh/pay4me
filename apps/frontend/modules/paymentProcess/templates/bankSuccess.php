<?php use_helper('ePortal');
if($err!='') {
   echo ePortal_highlight($err,"Error",array('class'=>'error_list','id'=>'flash_error'));
}
else {?>
<style>
  #SupportedBanklogos {margin:10px;padding:10px;}
  #SupportedBanklogos span { float:left;margin:15px;border:1px solid #fff;background:#f7f7f7;padding:5px;}
  #SupportedBanklogos span img {border:1px solid #ccc;}
</style>
<div class="innerWrapper">
<div id='bankNames'>
  

    <?php if(count($merchantBank)>0){?>
      <div class="formHead"><?php echo __('List of Supported Banks');?></div>
    <?php //echo ePortal_legend("List of Supported Banks"); ?>

    <div style="float:right; display:block;"><div class="search_bank"><A HREF='javascript:void(0)' onclick="open_win('bankBranchSearchIndex/?merchantId=<?php echo $formData['merchant_id'] ?>&currency=<?php echo $formData['currencyId'] ?>')"><?php echo __('Search Nearest Bank Branch')?></A></div></div>
<?php echo "<p class='descriptionArea_new'>".__('Applicant can make payments at any of the below listed banks')."</p>"; ?>
<div class="clearfix"></div>

<?php
    
    echo "<div id='SupportedBanklogos'>";
    foreach($merchantBank as $file){
      echo "<div class='bank_logo'>".image_tag('/images/'.$file.'.jpg',array('title'=>$file, 'alt'=>$file,'width'=>'100px','height'=>'75px'))."</div>";

    }
    echo "</div>";
    ?>
    <div class="pixbr"><br></br></br>
   <?php if($nipostFlag) {  ?>
        <div class="wrapForm2" style="padding-left:27px;">
                            <?php // vikash [WP055] Bug :36003 (19-10-2012) ?>
                            <dl><dt>
                               <b style="font-weight:bold;"> *The payment can be made at any of the following Post offices: </b> <br><br>
                            </dt></dl>
                            <dl><dt>
                                <label>1.Post Office: WUSE</label>
                            </dt><dd>Town : Wuse</dd></dl>
                            <dl><dt>
                                <label>2.Post office: Garki  </label>
                            </dt><dd>Town : Garki</dd></dl>
                            <dl><dt>
                                <label>3.Post Office: Skomolu</label>
                            </dt><dd>Town : Skomolu</dd></dl>
                            <dl><dt>
                                <label>4.Post Office: GPO  Ikeja</label>
                            </dt><dd>Town : Ikaja</dd></dl>
                            <dl><dt>
                                <label>5.Post Office: Apapa</label>
                            </dt><dd>Town : Apapa</dd></dl>
                            <dl><dt>
                                <label>6.Post Office: Marina</label>
                            </dt><dd>Town : SOKOTO</dd></dl>

        </div>
   <?php }   ?>




    </div>
  
  <div class="wrapLogos">
    <center id="formNav">
      <?php
      echo form_tag('paymentProcess/bankAcknowledgeSlip', 'method=post');
      include_component('paymentProcess', 'input',array('inputType'=>'hidden','inputFiled'=>$formData));
      //foreach($formData as $k=>$v){
       // echo input_hidden_tag($k,$v);
     // }
      ?>
      <div style="clear:both;"></div>
      <div class="dsInfo4Fields2">
          <input type="submit" name="continue" value="Continue" class="button">
      <?php

      //echo submit_tag(__('Continue'), array('class'=>'button')); ?>
      </div><br />
    </center>
  </div>
</div>

<?php }else{ ?>
  <p class='XY10 highlight red noPrint'>No bank releated with this merchant.</p>
<?php } ?>
<script language="JavaScript">
    function open_win(url_add)
   {
       window.open(url_add,'bankBranchSearchIndex', 'width=700,height=500, status=yes, location=yes, scrollbars=yes');
   }

</script>
  <?php }?>
  </div>