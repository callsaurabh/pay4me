<?php echo ePortal_pagehead($pageTitle); ?>  

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<title>Requesting for transaction </title>
</head>
<body topmargin="0" leftmargin="0" >

<iframe width="100%" name="interswitch" height="500px" src ="<?php echo url_for('paymentProcess/interswitchRedirect');?>"></iframe>

<form method='POST' target="interswitch">
    <input type="hidden" name="amount" value="<?php echo $totamount;?>"/>
    <input type="hidden" name="cust_id" value="<?php echo $tcust_id;?>"/>
    <input type="hidden" name="cust_name" value="<?php echo $tcust_id_desc;?>"/>
    <input type="hidden" name="cust_name_desc" value="<?php echo $tcust_name_desc;?>"/>
    <input type="hidden" name="product_id" value="<?php echo $tproduct_id;?>"/>
    <input type="hidden" name="currency" value="<?php echo $tcurrency;?>"/>
    <input type="hidden" name="site_redirect_url" value="<?php echo $site_redirect_url;?>">
    <input type="hidden" name="site_name" value="<?php echo $site_name;?>"/>
    <input type="hidden" name="cust_id_desc" value="<?php echo $cust_id_desc;?>"/>

    <input type="hidden" name="txn_ref" value="<?php echo $txn_ref_id;?>"/>


    <input type="hidden" name="pay_item_id" value="<?php echo $tpay_item_id;?>"/>
    <input type="hidden" name="pay_item_name" value="<?php echo $tpay_item_name;?>"/>
    <input type="hidden" name="local_date_time" value="<?php echo date("m/d/y g:i");?>"/>
    <input type="hidden" name="payment_params" value="<?php echo $payment_params;?>"/>
    <input type="hidden" name="xml_data" value="<?php echo $xmlDataToInterswitch;?>"/>
</form>
<script language='javascript'>
var form = document.forms[0];
form.action='<?php echo $interswitchURL; ?>';
form.submit();
</script>
</body>
</html>