<?php echo ePortal_pagehead($pageTitle); ?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<title>Requesting for transaction </title>
</head>
<body topmargin="0" leftmargin="0" >
<iframe width="100%" name="etranzact" height="500px" src ="<?php echo url_for('paymentProcess/etranzactRedirect');?>"></iframe>

<form method='POST' target="etranzact">
    <input type="hidden" name = "AMOUNT" value="<?php echo $amount;?>">
    <input type="hidden" name ="TERMINAL_ID" value="<?php echo $terminalId;?>">
    <input type="hidden" name="RESPONSE_URL" value="<?php echo $redirectURL;?>&app_id=<?php echo $app_id;?>&app_type=<?php echo $app_type;?>">
    <input type="hidden" name = "TRANSACTION_ID" value="<?php echo $transactionID ; ?>" />
    <input type="hidden" name = "DESCRIPTION" value="<?php echo $description;?>">
    <input type="hidden" name = "LOGO_URL" value="<?php echo $logoURL;?>">
    <input type="hidden" name = "ECHODATA" value="<?php echo $echo_data;?>">
</form>

<script language='javascript'>
var form = document.forms[0];
form.action='<?php echo $etranzactURL; ?>';
form.submit();
</script>
</body>
</html>