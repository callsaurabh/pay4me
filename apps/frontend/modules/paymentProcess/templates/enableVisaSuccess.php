
<?php
use_stylesheet('default.css');
use_stylesheet('main.css');
echo include_stylesheets();
?>
<div id="faqPage" class="cmspage">
    <?php
    echo ePortal_pagehead('Enable Visa (Activate Credit/Debit card for online payment)');
    ?>

    <div class="block-container">
        <div class="HFeatures">
            <a name="1"></a>
            <h1>Using your V-Pay card online with Pay4Me</h1>
            <p>Your V-Pay card is a credit or debit card that enables you to pay online with the benefits of the Visa brand.  To protect users, the ability to use these cards online is not activated immediately.  In order to activate your V-Pay card for use online the following steps should be taken:
           
                <ol style="padding-left:18px;">
                  <li>Go to your closest ATM supporting V-PAY</li>
                  <li>Insert your card, and enter your pin</li>
                  <li>Select V-Pay  as your card type (on some ATM machines)</li>
                  <li>Select change pin (usually located on the left side of the screen)</li>
                  <li>Here select i-pin (ensure you do not change your regular ATM pin instead)</li>
                  <li>Here you input a number, this will be treated as your i-pin for online use(twice)
                      <ul style="padding-left:18px;">
                          <li>This pin will be needed for every transaction carried out online</li>
                          <li>This pin is different from your regular ATM pin</li>
                      </ul>
                  </li>
                  <li>Once the pin has been entered twice correctly then the ATM will confirm that your pin is ready for use.</li>
                  <li>You may now use your V-Pay card online</li>
               </ol></p>        
                The i-pin is a security measure to aid you in keeping your bank account safe from fraudulent use.
                The i-pin can be used safely and securely on any online merchant that uses Pay4Me Services ltd as a payment option.
                Using your V-Pay card in tandem with Pay4Me is a convenient way to purchase products and services online.
            </div>
        </div>
</div>
