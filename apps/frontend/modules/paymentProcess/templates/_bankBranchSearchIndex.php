<?php if ($include_merchant_widget) : ?>
    <div class="error" style="position:static">
        Fields marked with (*) are mandatory
    </div>
    <div class="fieldName"><?php echo $form['merchant_id']->renderLabel(); ?>

        <br><br><?php echo $form['merchant_id']->renderError(); ?></div>
    <div class="fieldWrap">
        <label>
        <?php echo $form['merchant_id']->render(); ?>   </label>
</div>
<div style="clear:both;"></div>
<?php else: ?>
<?php echo $form['merchant_id']->render(); ?>
<?php endif; ?>

            <div class="fieldName"><?php echo $form['bank']->renderLabel(); ?></div>
            <div class="fieldWrap"><div id="bank_div"><?php echo $form['bank']->render(); ?>
                </div><div  id="bank_loader" style="display:none"></div>
            </div>
            <div class="clearfix"></div>

            <div class="fieldName"><?php echo $form['state']->renderLabel(); ?></div>
            <div class="fieldWrap"><div id="state_div"><?php echo $form['state']->render(); ?>
                </div><div  id="state_loader" style="display:none"></div>
            </div>
    <div class="clearfix"></div>
            <div class="fieldName"><?php echo $form['lga']->renderLabel(); ?></div>
            <div class="fieldWrap"><div  id="lga_div"><?php echo $form['lga']->render(); ?>
                </div>
                <div id="lga_loader" style="display:none">  </div>

            </div>
    <div class="clearfix"></div>
            <div class="fieldName"><?php echo $form['branch_code']->renderLabel(); ?></div>
            <div class="fieldWrap"><?php echo $form['branch_code']->render(); ?>


            </div>
    <div class="clearfix"></div>
<?php echo $form['search_flag']->render(); ?>
