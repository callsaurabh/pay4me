<div class="innerWrapper">
<?php use_helper('ePortal'); ?>
<link href="<?php echo url_for('web/css/print.css') ?>" media="print" type="text/css" rel="stylesheet">
<div id="p4mBank">
    <h2><?php // echo __($pageTitle); ?></h2><br/>
    <div class="descriptionArea">
        <p CLASS="cRed"><?php echo ePortal_highlight('',__('PLEASE CONFIRM DETAILS'), array('class' => 'red noPrint')); ?></p>
    </div>
    <div id='acknowledgeSlip' class='wrapForm2'>
        <?php
            echo ePortal_legend($data['legend']);
            echo formRowFormatRaw('Bank Name', $data['bank_name']);
            if ($data['payType'] == sfConfig::get('app_payment_mode_option_Cheque')) {
                echo formRowFormatRaw('Sort Code', $data['sort_code']);
                echo formRowFormatRaw('Cheque Number', $data['check_number']);
                echo formRowFormatRaw('Payee Account Number', $data['account_number']);
            }
            if ($data['payType'] == 'Bank Draft') {
//                if($data['sort_code']!='')
//                $sortCode=  $data['sort_code'];
//                else
//                $sortCode=  'NA';
//                echo formRowFormatRaw('Sort Code', $sortCode);
//                if($data['draft_number']!='')
//                $draftNumber=  $data['draft_number'];
//                else
//                $draftNumber=  'NA';
//                echo formRowFormatRaw('Draft Number', $draftNumber);
            }
        ?>
            <div style="clear:both;"></div>
            <div class="dsTitle2">
                <center id="formNav">
                    <form id="reset" action="<?php echo $data['formAction']; ?>" method="post">
                        <input type="hidden" name='payOptions[payType]' value='<?php echo $data['payType']; ?>'>
                        <input type="hidden" name='payOptions[merchantRequestId]' value='<?php echo $merchantRequestId; ?>'>
                        <input type="hidden" name='payOptions[payMode]' value='<?php echo $data['payMode'] ?>'>
                        <!--          params for check payment-->
                    </form>
                    <form id="proceed" action="<?php echo url_for('paymentProcess/checkDraftAcknowledgementSlip'); ?>" method="post">
<?php if ($data['payType'] == sfConfig::get('app_payment_mode_option_Cheque')) { ?><input type="hidden" name='check_number' value='<?php echo $data['check_number'] ?>'>
                                <input type="hidden" name='account_number' value='<?php echo $data['account_number']; ?>'>
                    <?php  } else if ($data['payType'] == 'Bank Draft'){ ?>
                    <input type="hidden" name='draft_number' value='<?php echo $data['draft_number']; ?>'>
                    <?php } ?>
                    <input type="hidden" name='bank_name' value='<?php echo $data['bank_name']; ?>'>
                    <input type="hidden" name='merchantRequestId' value='<?php echo $merchantRequestId; ?>'>
                    <input type="hidden" name='trans_num' value='<?php echo $data['trans_num']; ?>'>
                    <input type="hidden" name='payMode' value='<?php echo $data['payMode']; ?>'>
                    <input type="hidden" name='bank_sortCode' value='<?php echo $data['sort_code']; ?>'>
                </form>
                <div class="dsTitle4Fields">&nbsp;</div>
                <div align="left" class="dsInfo4Fields">
                    <button onclick="reset()" class="button"><?php echo __('Re-enter Payment Details') ?></button>&nbsp;&nbsp;
                    <button onclick="getAcknowledgeMentSlip()" class="button"><?php echo __('Confirm') ?> </button>
                </div>
            </center>
        </div></div></div></div>
<script>
    function reset(){
        $('#reset').submit();
    }function getAcknowledgeMentSlip(){
        $('#proceed').submit();
    }
</script>
