<div class="innerWrapper">
<?php
use_helper('ePortal');
if ($err != '') {
    echo ePortal_highlight($err, "Error", array('class' => 'error_list', 'id' => 'flash_error'));
} else {
?>

    <script>
        $(document).ready(function()
        {
            $('#bankForChkPaymentForm').submit(function(){

                if($('#bank_list').val()==0)
                {
                    $('#err_banks_list').html("Please select Bank");
                    return false;

                }
                else
                {
                    $('#err_banks_list').html("");
                    return true;
                }

            });

        });
        function open_win(url_add)
        {
            window.open(url_add,'bankBranchSearchIndex', 'width=700,height=500, status=yes, location=yes, scrollbars=yes');
        }

    </script>

    <style type="text/css">
        #SupportedBanklogos {margin:10px;padding:10px;}
        #SupportedBanklogos span { float:left;margin:15px;border:1px solid #fff;background:#f7f7f7;padding:5px;}
        #SupportedBanklogos span img {border:1px solid #ccc;}
    </style>

    <div id="bankNames">


    <?php if (count($merchantBank) > 0) {
 ?>
        <div class="formHead"><?php echo __('List of Supported Banks'); ?></div>
<?php //echo ePortal_legend("List of Supported Banks");  ?>

        <div style="float:right; display:block;"><div class="search_bank"><a href='javascript:void(0)' onclick="open_win('bankBranchSearchIndex/?merchantId=<?php echo $formData['merchant_id'] ?>&payment_mode=<?php echo $formData['payType'] ?>&currency=<?php echo $formData['currencyId'] ?>')"><?php echo __('Search Nearest Bank Branch') ?></a></div></div>

<?php
        echo "<p class='descriptionArea_new'>" . __('Please select the bank of your ') . $formData['paymentMode_in_hedaing'] . "</p>";
?>
<div class="clearfix"></div>
        <div class="pixbr">
<?php if ($nipostFlag) { ?>
            <div class="wrapForm2">

                <span > <b>*The payment can be made at any of the following Post offices:</b></span>
                <dl><dt>
                        <label>1.Post Office: WUSE</label>
                    </dt><dd>Town : Wuse</dd></dl>
                <dl><dt>
                        <label>2.Post office: Garki  </label>
                    </dt><dd>Town : Garki</dd></dl>
                <dl><dt>
                        <label>3.Post Office: Skomolu</label>
                    </dt><dd>Town : Skomolu</dd></dl>
                <dl><dt>
                        <label>4.Post Office: GPO  Ikeja</label>
                    </dt><dd>Town : Ikaja</dd></dl>
                <dl><dt>
                        <label>5.Post Office: Apapa</label>
                    </dt><dd>Town : Apapa</dd></dl>
                <dl><dt>
                        <label>6.Post Office: Marina</label>
                    </dt><dd>Town : SOKOTO</dd></dl>

            </div>
<?php } ?>

    </div>

    <div class="wrapLogos">
        <center id="formNav">
            <form action="<?php echo $formData['formAction']; ?>" name="bankForChkPayment" id="bankForChkPaymentForm" method="post">
                <div class="fieldName">
                    <label for="forgot_username">Select Bank <span class="required">*</span></label>         </div>
                <div class="fieldWrap">
                    <select name="bank" id="bank_list">
<?php foreach ($merchantBank as $id => $bank_name) { ?>
                    <option value=<?php echo $id; ?>><?php echo $bank_name; ?></option>
<?php } ?>
                </select>
                <div id="err_banks_list" class="cRed"></div>
            </div>

            <div class="clearfix"></div>

            <div class="fieldName"><label>&nbsp;</label></div>
            <div class="fieldWrap">
            <input type="hidden" name="merchantRequestId" value="<?php echo $formData['merchantRequestId'] ?>" />
                <input type="hidden" name="payOptions[payType]" value="<?php echo $formData['payType'] ?>" />
              
                <div style="clear:both;"></div>
                <div class="dsInfo4Fields2">
                    <input type="submit" name="continue" value="Continue" class="button" />
                    
                </div>
            </div>



        </form><br />
    </center>
</div>
</div>
<?php } else { ?>
<p class="XY10 highlight red noPrint">No Bank available for <?php echo $formData['paymentMode_in_hedaing']; ?> Payment to this merchant</p>
<?php } ?>

<?php } ?>
</div>