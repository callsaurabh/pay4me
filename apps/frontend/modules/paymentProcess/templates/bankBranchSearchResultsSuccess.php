<div class="innerWrapper">
<?php use_helper('Pagination');  ?>


<div class="dlForm">

    <?php
    echo ePortal_legend('Search Criteria');
    ?>


    <div class="feedbackForm float_none">
        <!-- <div class="header">Search Nearest Bank Branch Form</div>-->
        <input type="hidden" name="merchant_id" value="<?php echo $merchantId?>">
        <?php if(!$searchFlag){ ?>
        <div class="fieldName">Merchant <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo $merchantName; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php }?>
        <?php if($bank_name && $bank_name != ""){ ?>
        <div class="fieldName">Bank <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo $bank_name; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php }else{?>
        <div class="fieldName">Bank <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo "--"; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php } ?>

    <?php if($state_name && $state_name != ""){ ?>
        <div class="fieldName">State <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo $state_name; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php }else{?>
        <div class="fieldName">State <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo "--"; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php } ?>


    <?php if($lga_name && $lga_name != ""){ ?>
        <div class="fieldName">LGA <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo $lga_name; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php }else{?>
        <div class="fieldName">LGA <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo "--"; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php } ?>


    <?php if($branch_code && $branch_code != ""){ ?>
        <div class="fieldName">Branch Code <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo $branch_code; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php }else{?>
        <div class="fieldName">Branch Code <span class="required"></span></div>
        <div class="fieldWrap">
            <label>
                <?php echo "--"; ?>
            </label>
        </div>
        <div style="clear:both;"></div>
        <?php } ?>

        <div class="fieldName">&nbsp;</div>
        <div class="fieldWrap">
            <label>
                <?php
                if($searchFlag)
                echo button_to('Back','',array('class'=>'button', 'onClick'=>'location.href=\''.url_for('paymentProcess/bankBranchSearchIndex?merchantId='.$merchantId.'&payment_mode='.$payment_mode).'\''));
                else
                echo button_to('Back','',array('class'=>'button', 'onClick'=>'location.href=\''.url_for('paymentProcess/bankBranchSearchIndex').'\''));?>
            </label>
        </div>
        <div class="clearfix"></div>
    </div>





</div>

<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Bank Branch Listing'); ?>
    </fieldset>
</div>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th width="100%" >
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>
<form name="pfmBankBranchSearchResultForm" id = "pfmBankBranchSearchResultForm" action="#" method="post">
    <table class="dataTable">
        <thead>
            <tr>
                <th>S.No.</th>
                <th>Bank Name</th>
                <th nowrap>Branch Name</th>
                <th nowrap>Branch Address</th>
                <th nowrap>State Name</th>
                <th nowrap>LGA Name</th>
                <th>Last Activity Date</th>
            </tr>
        </thead>
        <tbody>
            <?php


            if(($pager->getNbResults())>0) {
                $limit = sfConfig::get('app_records_per_page');
                $i = max(($page-1),0)*$limit ;
                foreach ($pager->getResults() as $result):
                $i++;
                //echo "<pre>";
                //print_r($result->toArray());
                //die;
               
                ?>
            <div name="forChk" id="forChk">

                <tr>
                    <td align="center"><?php echo $i; ?></td>
                    <td align="center"><?php echo $result->getBank()->getBankName() ?></td>
                    <td align="center"><?php echo $result->getBranchName() ?></td>
                    <td align="center"><?php echo $result->getBranchAdd() ?></td>
                    <td align="center"><?php echo $result->getLgaId()!=0 ? $result->getLga()->getState()->getName() : $result->getStateName() ?>&nbsp;</td>
                    <td align="center"><?php echo $result->getLgaId()!=0 ? $result->getLga()->getName() : $result->getLgaName() ?>&nbsp;</td>
                    <td align="center" nowrap><?php echo ($result->getLastActivity()? $result->getLastActivity():'--'); ?></td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="7">
                        <div class="paging pagingFoot">
                            <?php  //echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?bank='.$bank_id.'&state='.$state_id.'&lga='.$lga_id.'&branch_code='.$branch_code),'a');
                            $selectBy = $sf_request->getParameter('selectBy') == '' ? "BLANK" : $sf_request->getParameter('selectBy');
                            if(!is_array($bank_id))
                            $bank = $bank_id == '' ? "BLANK" : $bank_id;
                            else
                            $bank = 'BLANK';
                            $merchantId = $merchantId == '' ? "BLANK" : $merchantId;
                            $state_id = $state_id == '' ? "BLANK" : $state_id;
                            $lga_id = ($lga_id && !is_array($lga_id)) == '' ? "BLANK" : $lga_id;
                            $branch_code = $branch_code == '' ? "BLANK" : $branch_code;
                            $serachFlag = $searchFlag ;
                            // Vikash [WP058](04-09-2012) bug:35802
                            echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?bank='.$bank.'&merchant_id='.$merchantId.'&paymentMode='.$payment_mode.'&state='.$state_id.'&lga='.$lga_id.'&branch_code='.$branch_code.'&search_flag='.$serachFlag));?>
                    </div></td>
                </tr>
                <?php }
            else { ?>
                <tr><td  align='center' class='error' colspan="7" style="position:static;">No Record Found</td></tr>
                <?php } ?>
            </div>
        </tbody>

    </table>
</form>

</div>