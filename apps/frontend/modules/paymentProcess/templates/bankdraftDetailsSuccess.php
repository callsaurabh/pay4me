<div class="innerWrapper">
<?php use_helper('ePortal');
echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details')); ?>

<div class="wrapLogos">
    <form id="chkDetails" action="<?php echo url_for('paymentProcess/confirmDraftDetails?bank='.$bankId) ?>" method="post" >

        <div class="fieldName">Bank Name</div>
        <div class="fieldWrap">
            <label><?php echo $bank_name; ?></label>
    <!--                    <div class="cRed" id="err_name"><?php //echo $chDetailsForm['sort_code']->renderError();                ?></div>-->
        </div>
        <div class="clearfix"></div>
        <div class="fieldName"><?php echo $chDetailsForm['sort_code']->renderLabel(); ?></div>
        <div class="fieldWrap">
            <label><?php echo $chDetailsForm['sort_code']->render(); ?></label>
             <?php pfmHelper::SortCodeTooltip($payModeType); ?>
            <div class="clearfix"><div class="cRed" id="err_cs_sort_code"></div>
            <div class="cRed" id="err_sort_code"><?php echo $chDetailsForm['sort_code']->renderError(); ?></div></div>
        </div><?php echo $chDetailsForm['bank_id']->render(); ?>
        <div name="sort_code_validated" style="display:none" id="sort_code_validated">
            <div class="clearfix"></div>
            <div class="fieldName"><?php echo $chDetailsForm['draft_number']->renderLabel(); ?></div>
            <div class="fieldWrap">
                <label><?php echo $chDetailsForm['draft_number']->render(); ?></label>
                <div class="cRed" id="err_cs_draft_number"></div>
                <div class="cRed" id="err_draft_number"><?php echo $chDetailsForm['draft_number']->renderError(); ?></div>
            </div>
            
        </div>
        <div class="clearfix"></div>
        <div class="fieldName">&nbsp;</div>
        <div class="fieldWrap">
            <input type="hidden" name="merchantRequestId" value="<?php echo $merchantRequestId; ?>">

            <?php echo $chDetailsForm['_csrf_token']; ?>
            <input type="submit" class="button" name="continue" value="Continue" id="submit_form" style="display:none">
            <input type="button" class="button"
                   id="validateSortCode" name="proceed" value="Continue" >
            <input type="button" class="button"
                    name="proceed" value="Back" onclick="goback()" >
        </div>
    </form>

       <form id="frmback" name="frmback" action="<?php echo url_for('paymentProcess/bankDraftPayment'); ?>" method="post">
                   <input type="hidden" name='payOptions[merchantRequestId]' value='<?php echo $merchantRequestId; ?>'>
                       <!--  <input type="hidden" name='payOptions[payType]' value='<?php //echo $payType; ?>'>

                        <input type="hidden" name='payOptions[payMode]' value='<?php //echo $payMode ?>'>
                       <input type="submit" class="button" name='btnback' id="btnback" value='<?php echo "Back" ?>' >-->
                    </form>
</div>
</div>
<script type="text/javascript">
    function goback(){
        document.frmback.submit();
    }


    $(document).ready(function()
    {

        
            if($('#draft_number').val()!=''){
                $('#sort_code_validated').attr('style','display:inline');
                $('#validateSortCode').attr("style",'display:none');
                $('#submit_form').attr("style",'display:inline');

            }
        
            $('#validateSortCode').click(function(){

                    //                var regEx=new RegExp('[^a-zA-Z0-9]+');
                    if($('#sort_code').val()!=''){
                    if(alphaNumricRegexCheck($('#sort_code').val()) == null){
                        if($('#sort_code').val().length==3)
                        {
                            var t=sortCodeValidation();
                        }
                        else
                        {

                            $('#err_cs_sort_code').html("Please enter at least 3 characters");
                            $('#err_sort_code').html("");
                        }

                    }
                    else
                    {

                        $('#err_cs_sort_code').html("Invalid Sort Code(Only alphanumeric characters accepted)");
                        $('#err_sort_code').html("");

                    }}else{
                    
                        if($('#sort_code_validated').css('display')== "inline"){
                            $('#chkDetails').submit();
                        }
                        $('#sort_code_validated').attr('style','display:inline');
                }
                    
               
                
//                }


            });
            function sortCodeValidation()
            {
                
                //
                var bank_id=$('#bank_id').val();
                var url = '<?php echo url_for("paymentProcess/validateSortCode", true) . "?byPass=1"; ?>';
               
                var sort_code_var=$('#sort_code').val().replace(" ","");
               $.post(url, {bank_id:bank_id,sort_code:sort_code_var}, function(data){
                    if(data==true)
                    {
                        $('#validateSortCode').attr("style",'display:none');
                        $('#submit_form').attr("style",'display:inline');
                        var sortcode_style=$('#sort_code_validated').attr("style");
                        if(sortcode_style.indexOf('display')!=-1 && sortcode_style.indexOf('none')!=-1){

                            $('#err_cs_sort_code').html("");
                            if($('#sort_code')!='')
                            $('#sort_code').attr("readonly","readonly");
                            $('#err_sort_code').html("");
                            $('#err_draft_number').html("");
                            $('#err_cs_draft_number').html("");
//                            $('#err_cs_account_number').html("");

//                            $('#err_account_number').html("");
                            $('#draft_number').val("");
//                            $('#account_number').val("");

                        }


                        $('#err_cs_sort_code').html("");
                        $('#err_sort_code').html("");
                        $('#sort_code_validated').attr('style','display:inline');
                        if($('#draft_number').val()!=''  ){

                            $('#sort_code_validated').attr('style','display:inline');
                            $('#validateSortCode').attr("style",'display:none');
                            $('#submit_form').attr("style",'display:inline');

                        }

                    }
                    else
                    {

                        $('#sort_code_validated').attr('style','display:none');
                        $('#err_cs_sort_code').html("Invalid Sort Code for selected bank");
                        $('#validateSortCode').attr("style",'display:inline');
                        $('#submit_form').attr("style",'display:none');

                        $('#err_sort_code').html("");

                    }


                });
           

            }
            function alphaNumricRegexCheck(text)
            {
                var regEx=new RegExp('[^a-zA-Z0-9]+');
                return regEx.exec(text);


            }
            $('#chkDetails').submit(function(){
                var err=0;
            
            
            
           
                if($('#draft_number').val()=="")
                {
//                    $('#err_cs_check_number').html("Please enter Cheque Number");
//                    $('#err_check_number').html("");
//                    err++;

                }
                else
                {
                    if(alphaNumricRegexCheck($('#draft_number').val()) == null){

                        $('#err_cs_draft_number').html("");
                        $('#err_draft_number').html("");

                    }
                    else
                    {
                        $('#err_cs_draft_number').html("Invalid Bank Draft Number(Only alphanumeric characters accepted)");
                        $('#err_draft_number').html("");
                        err++;
                    }
                    
                }
                
            
                if(err>0){

                    if(alphaNumricRegexCheck($('#sort_code').val()) == null){
                        sortCodeValidation();
                    }
                    else
                    {
                        $('#err_cs_sort_code').html("Invalid Sort Code(Only alphanumeric characters accepted)");
                        $('#err_sort_code').html("");


                    }
                }
//                if($('#sort_code').val()=="")
//                {
//                    $('#err_cs_sort_code').html("Please enter Sort code");
//                    $('#err_sort_code').html("");
//
//                    err++;
//
//                }
                //            else
                //            {
                //                $('#err_cs_sort_code').html("");
                //                $('#err_sort_code').html("");
                //            }
                if(err>0){

                    return false;
                }
            
               
            
            });

        
        });

</script>
