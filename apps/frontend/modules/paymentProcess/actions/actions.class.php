<?php

/**
 * PaymentProcess actions.
 *
 * @package    symfony
 * @subpackage PaymentProcess
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class PaymentProcessActions extends sfActions {

//public $flag = 0;

    public function executeMode(sfWebRequest $request) {
    	$merchantNotification = '1';
        $order = base64_decode($request->getParameter('order'));
        $order_details = explode(":", $order);
        $requestId = $order_details['1']; //get id of merchant_request
        $request_details = Doctrine::getTable('MerchantRequest')->findById($requestId);
        $merchantNotificationStatus = Doctrine::getTable('MerchantRequest')->saveMerchantNotificationStatus($requestId,$merchantNotification);
        //get MerchantId
        $merchantId = $request_details->getFirst()->getMerchantId();
        $merchantServiceId = $request_details->getFirst()->getMerchantServiceId();
        $currencyId = $request_details->getFirst()->getCurrencyId();
        $version = $request_details->getFirst()->getVersion();

        if ($version == "v2" && $request->hasParameter('currency')) {
            $postDataArray = array();

            $currencyId = $request->getParameter('currency');
            $requestObj = Doctrine::getTable('MerchantRequestPaymentDetails')->getCurrenyRequests($requestId, $currencyId);
            if ($requestObj) {
                $postDataArray['item_fee'] = $requestObj->getFirst()->getAmount();
            }



            $postDataArray['id'] = $requestId;
            $postDataArray['currency_id'] = $currencyId;
            Doctrine::getTable('MerchantRequest')->updateMerchantRequest($postDataArray);
        }
        //get the transaction Location
        $transaction_location = $request_details->getFirst()->getTransactionLocation();
        if ($transaction_location == "merchant-counter") {
            $this->processCounterApplication($requestId, $merchantId);
        }
        $managerObj = payForMeServiceFactory::getService($merchantId);

        $checkMerchant = $managerObj->checkMerhantRequestPaymentModes($requestId);
        $payment_mode_options = array();
        if ($checkMerchant) {
            $payment_mode_options = $managerObj->getMerchantRequestPaymentMode($requestId);
        } else {
            $payment_mode_options = $managerObj->getPaymentMode($merchantId, $merchantServiceId, $currencyId);
        }

        $payment_mode_options = $managerObj->checkInterswitchCategory($payment_mode_options, $merchantId);
        /* to check if ewallet/cards payments are allowed or not*/
        if(sfConfig::get('app_disable_ewallet_cards')){
            $payment_mode_options = $this->ewalletPaymentTimeConfig($payment_mode_options);
        }
        
        $this->display_visa_help = false;
        if (array_key_exists(sfConfig::get('app_payment_mode_option_credit_card'), $payment_mode_options[0])) {
            $this->display_visa_help = true;
        }
        $this->pay_mode_options_selection = $payment_mode_options;
        
        if ($payment_mode_options) {
            
            $this->form = new PaymentModeSelectionForm();
            $this->form->getWidget('payType')->setOption('choices', $payment_mode_options[0]);

            $this->form->setDefaults(array(
                'merchantRequestId' => $requestId
            ));
        }
        
        $this->pageTitle = 'Payment Process';
        $this->setTemplate('welcome');
    }

    /*
     * function to compute allowed payment modes(whether ewallet/cards payment mode be there or not) [WP078]
     */
    private function ewalletPaymentTimeConfig($payment_mode_options){
        $isWeekend = false;
        $time=date("H:i:s");
        if(sfConfig::get('app_enable_ewallet_cards_payments_weekend_24hrs')){
            $weekday = date('w');
            $isWeekend = ($weekday == 0 || $weekday == 6) ? true:false;
            if($isWeekend)
              return $payment_mode_options;
        }
        $startTime = sfConfig::get('app_ewallet_payments_start_time');
        $endTime = sfConfig::get('app_ewallet_payments_end_time');
        if($time<$startTime && $time>$endTime){
            $payment_mode_options_rev=array();
            if(isset($payment_mode_options[0]['bank']))
                $payment_mode_options_rev[0]['bank']=$payment_mode_options[0]['bank'];
            if(isset($payment_mode_options[0]['cheque']))
                $payment_mode_options_rev[0]['cheque']=$payment_mode_options[0]['cheque'];
            if(isset($payment_mode_options[0]['bankdraft']))
                $payment_mode_options_rev[0]['bankdraft']=$payment_mode_options[0]['bankdraft'];
            if(isset($payment_mode_options['bank']))
                $payment_mode_options_rev['bank']=$payment_mode_options['bank'];
            if(isset($payment_mode_options['cheque']))
                $payment_mode_options_rev['cheque']=$payment_mode_options['cheque'];
            if(isset($payment_mode_options['bankdraft']))
                $payment_mode_options_rev['bankdraft']=$payment_mode_options['bankdraft'];
            return $payment_mode_options_rev;
        }
        return $payment_mode_options;
    }

    public function executeValidateSortCode(sfWebRequest $request) {

        try {
            if ($request->hasParameter('bank_id') && $request->hasParameter('sort_code')) {
                if ($request->getParameter('bank_id') != "" && $request->getParameter('sort_code') != "") {
                    $validSortCode = Doctrine::getTable('BankSortCodeMapper')->validateSortCode($request->getParameter('bank_id'), $request->getParameter('sort_code'));



                    return $this->renderText($validSortCode);
                } else {


                    throw new Exception("Either Bank or Sort Code is Empty");
                }
            } else {
                throw new Exception("Mandatory Pramaeters not found");
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function executeConfirmChkDetails(sfWebRequest $request) {
        $this->pageTitle = pfmHelper::getChequeDisplayName()." Details";
        $this->chDetailsForm = new CustomCheckDetailsForm(array('bank_id' => $request->getParameter('bank_id')), array('bank_id' => $request->getParameter('bank_id')), array());
        $this->bank_name = Doctrine::getTable('Bank')->find($request->getParameter('bank_id'))->getBankName();
        $this->merchantRequestId = $request->getParameter('merchantRequestId');
        $this->chDetailsForm->bind($request->getPostParameters());
        if ($this->chDetailsForm->isValid()) {
            
            $data = $request->getPostParameters();
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $this->data = array('bank_name' => $this->bank_name, 'payType' => 'Cheque', 'payMode' => 'Cheque', 'trans_num' => $this->generateTransactionNumber(), 'formAction' => url_for("paymentProcess/checkPayment"), 'legend' => pfmHelper::getChequeDisplayName()." Details") + $data;
        } else {
            //foreach ($this->chDetailsForm->getFormFieldSchema() as $name => $formField) {
////
//                echo $formField->getName()."-error-";
//             echo $formField->getError()."<br>"; // renders unformatted
////            }
            $this->setTemplate('checkDetails');
        }
    }

    public function executeConfirmDraftDetails(sfWebRequest $request) {

        $this->pageTitle = pfmHelper::getDraftDisplayName() . ' Details';
//        $this->chDetailsForm = new CustomCheckDetailsForm(array('payType' => 'Bank Draft','bank_id' => $request->getParameter('bank')), array('bank_id' => $request->getParameter('bank')), array());
       
       $this->bank_name = Doctrine::getTable('Bank')->find($request->getParameter('bank'))->getBankName();
        $this->merchantRequestId = $request->getParameter('merchantRequestId');
//        $this->chDetailsForm->bind($request->getPostParameters());
//         if ($this->chDetailsForm->isValid()) {die('safsarf');
        $data = $request->getPostParameters();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        
        $this->data = array('bank_name' => $this->bank_name, 'payType' => 'Bank Draft', 'payMode' => 'bank_draft', 'trans_num' => $this->generateTransactionNumber(), 'formAction' => url_for("paymentProcess/bankDraftPayment"), 'legend' => "Bank Draft Details")+ $data;
      $this->setTemplate('confirmChkDetails');
//         }
//         else{
//          $this->setTemplate('bankdraftDetails');
//         }
        
    }

    public function executeCheckPayment(sfWebRequest $request) {
        $this->payModeType='';
        $detail= $request->getParameter('payOptions');
        if(isset($detail['merchantRequestId']) && $detail['merchantRequestId']!=''){
            $this->merchantRequestId= $detail['merchantRequestId'];

        }else{
            $this->merchantRequestId= $request->getParameter('merchantRequestId');
        }
        if($detail['payType'] && $detail['payType']!='')
        $this->payModeType = $detail['payType'];
        $this->err = '';
       
        
        try {
            if ($request->hasParameter('bank') && $request->getParameter('bank') != 0) {
                $this->chDetailsForm = new CustomCheckDetailsForm(array('bank_id' => $request->getParameter('bank')), array(), array());
                $this->bank_name = Doctrine::getTable('Bank')->find($request->getParameter('bank'))->getBankName();
                $this->merchantRequestId = $request->getParameter('merchantRequestId');
                $template = 'checkDetails';
            } else {
                $template = 'bankSelection';
                if ($request->hasParameter('payOptions')) {
                    foreach ($request->getParameter("payOptions") as $k => $v) {
                        $formdata[$k] = $v;
                    }
                    
                    if (array_key_exists('merchantRequestId', $formdata) && ($formdata['merchantRequestId'] != "")) {
                        $merchantRequestObj = Doctrine::getTable('MerchantRequest')->find($formdata['merchantRequestId']);
                        if ($merchantRequestObj) {
                            $merchantId = $merchantRequestObj->getMerchantId();
                            $formdata['currencyId'] = $currencyId = $merchantRequestObj->getCurrencyId();

                            $merchantReleatedToBank = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForCheckPayment($merchantId, $currencyId);


                            $formdata['merchant_id'] = $merchantId;
                            $merchantBank = array("0" => "--Please Select Bank--");
                            $counterVal = count($merchantReleatedToBank);

                            if ($counterVal != 0) {
                                for ($i = 0; $i < $counterVal; $i++) {
                                    $merchantBank[$merchantReleatedToBank[$i]['Bank']['id']] = $merchantReleatedToBank[$i]['Bank']['bank_name'];
                                }
                                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                                $formdata['formAction'] = url_for("paymentProcess/checkPayment");
                                $formdata['paymentMode_in_hedaing'] = pfmHelper::getChequeDisplayName();
                                $this->formData = $formdata;

                                $this->nipostFlag = false;
                                if (in_array("nipost", $merchantBank))
                                    $this->nipostFlag = true;

                                $this->merchantBank = $merchantBank;


                                $this->pageTitle = 'Supported Banks';
                            }
                            else {
                                throw new Exception("No Bank available for Check Payment for this Merchant");
                            }
                        } else {
                            $this->logMessage("Merchant Request Id has been tampered with!!");
                            throw new Exception("Trying to search with a merchant request that does not exist!!");
                        }
                    } else {
                        $this->logMessage("Hidden variables are being tampered");
                        throw new Exception("Mandatory Parameter merchant request id missing");
                    }
                } else {

                    throw new Exception("Mandatory parameters not passed");
                }
            }
        } catch (Exception $e) {

            $this->err = $e->getMessage();
        }
        $this->pageTitle = 'Supported Banks';
        $this->setTemplate($template);
    }

    public function executeBankDraftPayment(sfWebRequest $request) {
        $detail= $request->getParameter('payOptions');
        if($detail['merchantRequestId']!=''){
            $this->merchantRequestId= $detail['merchantRequestId'];
        }else{
            $this->merchantRequestId= $request->getParameter('merchantRequestId');
        }
        $this->payModeType = $detail['payType'];
        $this->err = '';
        $this->pageTitle = pfmHelper::getDraftDisplayName() . 'Payment';
        try {
            if ($request->hasParameter('bank')) {
                $this->chDetailsForm = new CustomCheckDetailsForm(array('bank_id' => $request->getParameter('bank')), array(), array());
                $this->bank_name = Doctrine::getTable('Bank')->find($request->getParameter('bank'))->getBankName();
                $this->merchantRequestId = $request->getParameter('merchantRequestId');
                $this->bankId = $request->getParameter('bank');
                $template = 'bankdraftDetails';
            } else {
                if ($request->hasParameter('payOptions')) {
                    foreach ($request->getParameter("payOptions") as $k => $v) {
                        $formdata[$k] = $v;
                    }

                    if (array_key_exists('merchantRequestId', $formdata) && ($formdata['merchantRequestId'] != "")) {
                        $merchantRequestObj = Doctrine::getTable('MerchantRequest')->find($formdata['merchantRequestId']);
                        if ($merchantRequestObj) {
                            $merchantId = $merchantRequestObj->getMerchantId();
                            $formdata['currencyId'] = $currencyId = $merchantRequestObj->getCurrencyId();
                            $merchantReleatedToBank = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForDraftPayment($merchantId, $currencyId);
                            $formdata['merchant_id'] = $merchantId;
                            $merchantBank = array("0" => "--Please Select Bank--");
                            $counterVal = count($merchantReleatedToBank);
                            $template = 'bankSelection';
                            if ($counterVal != 0) {
                                for ($i = 0; $i < $counterVal; $i++) {
                                    $merchantBank[$merchantReleatedToBank[$i]['Bank']['id']] = $merchantReleatedToBank[$i]['Bank']['bank_name'];
                                }
                                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

                                $formdata['formAction'] = url_for("paymentProcess/confirmDraftDetails");
//                                $formdata['formAction'] = url_for("paymentProcess/bankDraftPayment");
                                $formdata['paymentMode_in_hedaing'] = pfmHelper::getDraftDisplayName() ;
                                $this->formData = $formdata;

                                $this->nipostFlag = false;
                                if (in_array("nipost", $merchantBank))
                                    $this->nipostFlag = true;

                                $this->merchantBank = $merchantBank;


                                $this->pageTitle = 'Supported Banks';
                            }
                            else {
                                throw new Exception("No Bank available for Bank Draft Payment for this Merchant");
                            }
                        } else {
                            $this->logMessage("Merchant Request Id has been tampered with!!");
                            throw new Exception("Trying to search with a merchant request that does not exist!!");
                        }
                    } else {
                        $this->logMessage("Hidden variables are being tampered");
                        throw new Exception("Mandatory Parameter merchant request id missing");
                    }
                } else {

                    throw new Exception("Mandatory parameters not passed");
                }
            }
        } catch (Exception $e) {

            $this->err = $e->getMessage();
        }

        $this->setTemplate($template);
    }

    public function executeBank(sfWebRequest $request) {
        $this->err = '';
        try {
            if ($request->hasParameter('payOptions')) {
                foreach ($request->getParameter("payOptions") as $k => $v) {
                    $formdata[$k] = $v;
                }
                if (array_key_exists('merchantRequestId', $formdata) && ($formdata['merchantRequestId'] != "")) {
                    $merchantRequestObj = Doctrine::getTable('MerchantRequest')->find($formdata['merchantRequestId']);
                    if ($merchantRequestObj) {
                        $merchantId = $merchantRequestObj->getMerchantId();
                        $formdata['currencyId'] = $currencyId = $merchantRequestObj->getCurrencyId();
                        $merchantReleatedToBank = Doctrine::getTable('BankMerchant')->findAllReleatedBanks($merchantId, $currencyId);
                        $merchantBank = array();
                        $counterVal = count($merchantReleatedToBank);
                        if ($counterVal != 0) {
                            for ($i = 0; $i < $counterVal; $i++) {
                                //   print_r($merchantReleatedToBank[0]['Bank']['acronym']);die;
                                $merchantBank[] = $merchantReleatedToBank[$i]['Bank']['acronym'];
                            }

                            $this->nipostFlag = false;
                            if (in_array("nipost", $merchantBank))
                                $this->nipostFlag = true;

                            $this->merchantBank = $merchantBank;

                            $formdata['trans_num'] = $this->generateTransactionNumber();
                            $this->formData = $formdata;
                            $merchantIdArray = array();
                            $merchantIdArray = array('merchant_id' => $merchantId);
                            $this->formData = array_merge($this->formData, $merchantIdArray);
                            //    echo "Form Data : <pre>";print_r($this->formData);die;

                            $this->pageTitle = 'Supported Banks';
                        }
                        else {
                            throw new Exception("No Bank related to this Merchant");
                        }
                    } else {
                        $this->logMessage("Merchant Request Id has been tampered with!!");
                        throw new Exception("Trying to search with a merchant request that does not exist!!");
                    }
                } else {
                    $this->logMessage("Hidden variables are being tampered");
                    throw new Exception("Mandatory Parameter merchant request id missing");
                }
            } else {
                throw new Exception("Mandatory parameters not passed");
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    //  public function executeBank(sfWebRequest $request) {
    //    foreach($request->getParameter("payOptions") as $k=>$v) {
    //      $formdata[$k] = $v;
    //    }
    //    $merchantRequestObj = Doctrine::getTable('MerchantRequest')->find($formdata['merchantRequestId']);
    //    $merchantId = $merchantRequestObj->getMerchantId();
    //    $merchantReleatedToBank = Doctrine::getTable('BankMerchant')->findAllReleatedBanks($merchantId);
    //
    //    $merchantBank = array();
    //    $counterVal = count($merchantReleatedToBank);
    //    for($i=0;$i<$counterVal; $i++) {
    //    //   print_r($merchantReleatedToBank[0]['Bank']['acronym']);die;
    //      $merchantBank[] = $merchantReleatedToBank[$i]['Bank']['acronym'];
    //    }
    //    $this->nipostFlag = false;
    //    if(in_array("nipost", $merchantBank))
    //      $this->nipostFlag = true;
    //
    //    $this->merchantBank = $merchantBank;
    //
    //    $formdata['trans_num'] = $this->generateTransactionNumber();
    //    $this->formData = $formdata;
    //    $merchantIdArray = array();
    //    $merchantIdArray = array('merchant_id' => $merchantId);
    //    $this->formData = array_merge($this->formData, $merchantIdArray);
    //    //    echo "Form Data : <pre>";print_r($this->formData);die;
    //
    //    $this->pageTitle = 'Supported Banks';
    //  }

    public function generateTransactionNumber() {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        return $payForMeObj->generateTransactionNumber();
    }

    public function executeCheckDraftAcknowledgementSlip(sfWebRequest $request) {


        $formData = $request->getPostParameters();
        if (array_key_exists('merchantRequestId', $formData) && ($formData['merchantRequestId'] != "")) {

            $merchant_service_details = Doctrine::getTable('MerchantRequest')->findById($formData['merchantRequestId']);
            $merchant_service_id = $merchant_service_details->getFirst()->getMerchantServiceId();
            $merchantId = $merchant_service_details->getFirst()->getMerchantId();
            $this->isClubbed = $merchant_service_id = $merchant_service_details->getFirst()->getMerchantService()->getClubbed();
            $this->currencyId = $merchant_service_details->getFirst()->getCurrencyId();

            //  $formData['trans_num'] =$trans_num ;
//              = $formData['payMode'];

            $this->paymentMode = $formData['pay_mode'] = $formData['payMode'];
            $pfm_trans_num = $this->saveTransaction($formData);

            if ($pfm_trans_num != false) {
                $this->updateMerchantRequest($formData['merchantRequestId'], $formData['pay_mode'], $formData['bank_name']);
                $checkNumber = '';
                $accountNumber ='';
                if(isset ($formData['check_number'])){
                    $checkNumber = $formData['check_number'];
                }

                if(isset ($formData['account_number'])){
                    $accountNumber = $formData['account_number'];
                }
                if(isset ($formData['draft_number'])){
                    $checkNumber = $formData['draft_number'];
                }

                //if ($this->paymentMode == 'check') {
                    $this->updateCheckDetailsForMerchantRequest($formData['trans_num'], $checkNumber, $accountNumber,$formData['bank_name'], $formData['bank_sortCode']);
                //}
                //$this->updateTransactionBankId($formData['trans_num'],$formData['bank_name']);
                //$this->formData = payForMeManager::getTransDetails($pfm_trans_num);
                $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
                $this->formData = $payForMeObj->getTransactionRecord($pfm_trans_num);
                //$this->sortCode=Will get sort code aftetr db changes
                $this->pageTitle = 'Acknowledgement Slip';

                //get Param Description; as per Merchant Service
                $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
                $this->MerchantData = $ValidationRulesObj->getValidationRules($this->formData['merchant_service_id']);
                $this->setTemplate('bankAcknowledgeSlip');
//                $this->paymentMode='check';
            } else {
                $this->redirect('paymentProcess/invalidAttempt');
            }
        } else {
            $this->redirect('@accesserror');
        }
    }

    public function executeBankAcknowledgeSlip(sfWebRequest $request) {
        //print "<pre>";print_r($request->getPostParameters());
        //   $trans_num = $request->getParameter('transaction_id');
        $formData = $request->getParameterHolder()->getAll();

        /*
          $gUser = $this->getUser()->getGuardUser();

          if(!is_object($gUser))
          $this->redirect('pages/errorUser');


          $userGroup = $gUser->getGroupNames();

          if($userGroup[0] != "bank_user")
          $this->redirect('pages/errorUser');

          $bUser = $gUser->getBankUser();

          $bankId =  $bUser->getFirst()->getBank()->getId();

          $bankMerchantActive = Doctrine::getTable('BankMerchant')->chkBankMerchantStatus($formData['merchant_id'],$bankId);
          if($bankMerchantActive == 0){
          // $this->getUser()->setFlash('error', "This merchant is de-activated for this bank. !!!", true);
          $this->redirect('pages/errorUser');
          //$this->redirect($request->getReferer());
          }else{ */
        //get Merchant Service Id
        if (array_key_exists('merchantRequestId', $formData) && ($formData['merchantRequestId'] != "")) {
            $merchant_service_details = Doctrine::getTable('MerchantRequest')->findById($formData['merchantRequestId']);
            $merchant_service_id = $merchant_service_details->getFirst()->getMerchantServiceId();
            $this->isClubbed = $merchant_service_id = $merchant_service_details->getFirst()->getMerchantService()->getClubbed();
            $this->currencyId = $merchant_service_details->getFirst()->getCurrencyId();

            //  $formData['trans_num'] =$trans_num ;
            $this->paymentMode = $formData['pay_mode'] = 'bank';

            $pfm_trans_num = $this->saveTransaction($formData);
            if ($pfm_trans_num != false) {

                $this->updateMerchantRequest($formData['merchantRequestId'], $formData['payMode']);

                //$this->formData = payForMeManager::getTransDetails($pfm_trans_num);
                $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
                $this->formData = $payForMeObj->getTransactionRecord($pfm_trans_num);
                //      print "<pre>";
                //print_r($this->formData);
                $this->pageTitle = 'Acknowledgement Slip';

                //get Param Description; as per Merchant Service
                $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
                $this->MerchantData = $ValidationRulesObj->getValidationRules($this->formData['merchant_service_id']);
            } else {
                $this->redirect('paymentProcess/invalidAttempt');
            }
        } else {
            $this->redirect('@accesserror');
        }
        // }
    }
    
    public function executeInternetbankAcknowledgeSlip(sfWebRequest $request) {

        $formData = $request->getParameter("payOptions");

        if (array_key_exists('merchantRequestId', $formData) && ($formData['merchantRequestId'] != "")) {
            $formData['trans_num'] = $this->generateTransactionNumber();
            $merchant_service_details = Doctrine::getTable('MerchantRequest')->findById($formData['merchantRequestId']);
            $merchant_service_id = $merchant_service_details->getFirst()->getMerchantServiceId();
            $this->isClubbed = $merchant_service_id = $merchant_service_details->getFirst()->getMerchantService()->getClubbed();
            $this->currencyId = $merchant_service_details->getFirst()->getCurrencyId();

            $this->paymentMode = $formData['pay_mode'] = 'internet_bank';

            $pfm_trans_num = $this->saveTransaction($formData);
            if ($pfm_trans_num != false) {
                
                $this->updateMerchantRequest($formData['merchantRequestId'], $formData['payMode']);
                $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
                $this->formData = $payForMeObj->getTransactionRecord($pfm_trans_num);
                $this->pageTitle = 'Acknowledgement Slip';

                $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
                $this->MerchantData = $ValidationRulesObj->getValidationRules($this->formData['merchant_service_id']);
            } else {
                $this->redirect('paymentProcess/invalidAttempt');
            }
        } else {
            $this->redirect('@accesserror');
        }
    }

    private function updateMerchantRequest($merchant_request_id, $payment_mode, $bank_name="") {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $payForMeObj->updateMerchantRequest($merchant_request_id, $payment_mode, $add_bank_charges = 1, $bank_name);
    }

    private function updateCheckDetailsForMerchantRequest($merchant_request_id, $check_number, $account_number,$bank_name, $sort_code) {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $payForMeObj->updateCheckDetailsForMerchantRequest($merchant_request_id, $check_number, $account_number,$bank_name, $sort_code);
    }


    private function saveTransaction($formData) {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        return $payForMeObj->saveTransaction($formData);
    }

    public function executeInvalidAttempt(sfWebRequest $request) {
        $this->pageTitle = 'Invalid Payment';
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                            array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    public function executeAllredyPaid(sfWebRequest $request) {
        $getValues = $request->getParameterHolder()->getAll();
        if (isset($getValues['redirect']) && $getValues['redirect'] == true) {
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'paymentProcess', 'action' => 'allredyPaid')) . "'</script>");
        }
        //redirect to a
        $this->redirect('paymentProcess/invalidAttempt');
    }

    public function processCounterApplication($requestId, $merchant_id) {
        $formdata = array();
        $formdata['merchantRequestId'] = $requestId;
        $formdata['pay_mode'] = "merchant-counter";
        $formdata['trans_num'] = $this->generateTransactionNumber();
        $pfm_trans_num = $this->saveTransaction($formdata);
        if ($pfm_trans_num != false) {
            $this->updateMerchantRequest($formdata['merchantRequestId'], $formdata['pay_mode']);
        }
        //get bank associated with the merchant
        $bank = Doctrine::getTable('BankMerchant')->getMechantBank($merchant_id, $merchant_account = 1);
        //
        //chk if the user of the bank related is already logged in
        if (!$this->getUser()->isAuthenticated()) {
            //if user is not loged in
            $bank_acronym = $bank[0]['Bank']['acronym'];
            $this->redirect('@bank_login?bankName=' . $bank_acronym . '&txnId=' . $pfm_trans_num);
        } else { //if user is logged in
            if (sfConfig::has('app_transaction_location_logout_on_transaction')) {
                $logout_on_transaction = sfConfig::get('app_transaction_location_logout_on_transaction');
                if ($logout_on_transaction) {//logout the user
                    $this->getUser()->signout();
                    $bank_acronym = $bank[0]['Bank']['acronym'];
                    $this->redirect('@bank_login?bankName=' . $bank_acronym . '&txnId=' . $pfm_trans_num);
                } else {
                    //chk if user logged in is of valid bank
                    $this->redirect('paymentSystem/search?txnId=' . $pfm_trans_num);
                }
            }
        }
    }

    public function executeVbvAcknowledge(sfWebRequest $request) {
        foreach ($request->getParameter("payOptions") as $k => $v) {
            $formdata[$k] = $v;
        }

        $formdata['trans_num'] = $this->generateTransactionNumber();
        $formData = $formdata;

        $formData['pay_mode'] = 'Visa';
        $pfm_trans_num = $this->saveTransaction($formData);

        if ($pfm_trans_num != false) {
            $formData['payMode'] = 'Visa';
            $formData['merchant_request_id'] = $formData['merchantRequestId']; //echo"<pre>";print_r($formData);echo"<br \>";die;
            $this->updateMerchantRequest($formData['merchant_request_id'], $formData['payMode']);

            $this->redirect('paymentProcess/VbvAcknowledgeSlip?id=' . base64_encode($pfm_trans_num));
        } else {
            $this->redirect('paymentProcess/invalidAttempt');
        }
    }

    public function executeVbvAcknowledgeSlip(sfWebRequest $request) {
        $pfm_trans_num = base64_decode($request->getParameter('id'));
        $payForMeServiceObject = payForMeServiceFactory::getService();
        $this->formData = $payForMeServiceObject->getTransactionRecord($pfm_trans_num);
        $this->pageTitle = 'Acknowledgement Slip';

        //get Param Description; as per Merchant Service
        $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
        $this->MerchantData = $ValidationRulesObj->getValidationRules($this->formData['merchant_service_id']);
        $paymentModeConfigManager = new paymentModeConfigManager();
        $this->show = $paymentModeConfigManager->isPaymentServiceActive($this->formData['merchant_id'], $this->formData['merchant_service_id'], $this->formData['payment_mode_id'], $this->formData['payment_mode_option_id']);

        $this->setTemplate('VbvAcknowledgeSlip');
    }

    public function executeBankBranchSearchIndex(sfWebRequest $request) {

        $merchant_id = 0;
        $bankMerchantListArray = array();
        $this->merchantId = "";
        if ($request->getParameter('merchantId')) {
            $this->merchantId = $request->getParameter('merchantId');
        }
        if ($request->getParameter('currency')) {
            $this->currency = $request->getParameter('currency');
        }
        $selMerchantChoiceArray = array('' => 'Please Select Merchant');
        $selBankChoiceArray = array('' => 'All Banks');
        $selStateChoiceArray = array('' => 'All States');
        $selLgaChoiceArray = array('' => 'All Lgas');

        $merchantListArray = Doctrine::getTable('Merchant')->getMerchant($this->merchantId);
        $merchantArray = array();
        if (count($merchantListArray) > 0) {
            foreach ($merchantListArray as $key => $val) {
                $merchantArray[$val['id']] = $val['name'];
            }
        }

        $bankMerchantObj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);

        if ($request->hasParameter('payment_mode') && $request->getParameter('payment_mode') != "") {
            //for payment_mode = cheque
            if ($request->getParameter('payment_mode') == 'cheque') {
                $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanksForCheckPayment($this->merchantId, $this->currency);
            } else if($request->getParameter('payment_mode') == 'bankdraft'){
                //for payment_mode = bank_draft
                $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanksForDraftPayment($this->merchantId, $this->currency);
            }
            else if($request->getParameter('payment_mode') == 'Bank'){
                //for payment_mode = bank_draft
                $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($this->merchantId, $this->currency);
                $this->paymentMode = "Bank";
            }
            $this->paymentMode = $request->getParameter('payment_mode');
        } else {
            //for payment_mode = bank
            $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($this->merchantId, $this->currency);
            $this->paymentMode = "Bank";
        }

        $bankArray = array();
        if (count($bankMerchantListArray) > 0) {
            foreach ($bankMerchantListArray as $key => $val) {
                $bankArray[$val['Bank']['id']] = $val['Bank']['bank_name'];
            }
        }

        $this->merchant = $selMerchantChoiceArray + $merchantArray;
        if ($this->merchantId)
            $this->bank = $selBankChoiceArray + $bankArray;
        else
            $this->bank = $selBankChoiceArray;
        $this->state = $selStateChoiceArray;
        $this->lga = $selLgaChoiceArray;

        $this->custom_BankBranchSearchIndexForm =
                new CustomBankBranchSearchIndexForm(null,
                        array('bank_choices' => $this->bank, 'merchant' => $this->merchantId,
                            'merchantArray' => $this->merchant), null);
        $this->setLayout('layout_popup_unauth');
    }

    public function executePopulateStatesDropDown(sfWebRequest $request) {
        $bankId = 0;
        $lgaRelatedStateArray = array();
        if ($request->getParameter('bankId')) {
            $bankId = $request->getParameter('bankId');
        }

        $bankBranchObj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BASE);
        $bankRelatedLgaArray = $bankBranchObj->getBankRelatedLgas($bankId);

        $lgaObj = lgaServiceFactory::getService(lgaServiceFactory::$TYPE_LGA);
        $lgaRelatedStateList = $lgaObj->getLgaRelatedStates($bankRelatedLgaArray);

        if (count($lgaRelatedStateList) > 0) {
            foreach ($lgaRelatedStateList as $key => $val) {
                $lgaRelatedStateArray[$val['state_id']] = $val['state_name'];
            }
        }

        $str = "";
        $str.= '<option value="" selected>All States</option>';

        foreach ($lgaRelatedStateArray as $key => $val) {
            $str.= '<option value="' . $key . '">' . $val . '</option>';
        }
        return $this->renderText($str);
    }

    public function executePopulateLgasDropDown(sfWebRequest $request) {
        $stateId = 0;
        if ($request->getParameter('stateId')) {
            $stateId = $request->getParameter('stateId');
        }
        $str = "<option value='' selected>All Lgas</option>";
        if (($stateId != "") && ($stateId > 0)) {
            $this->services = Doctrine::getTable('Lga')->getLgaList($stateId);
            if (count($this->services)) {
                foreach ($this->services as $key => $value) {
                    $str .= "<option value='" . $value['id'] . "'>" . $value['name'] . "</option>";
                }
            }
        }
        return $this->renderText($str);
    }

    public function executeBankBranchSearchResults(sfWebRequest $request) {

        //  echo "<pre>";print_r($request->getParameterHolder()->getAll());die;
        $this->bank_id = $this->state_id = $this->lga_id = $this->branch_code = "";
        $this->searchFlag = $request->getParameter('search_flag');
        $this->merchantId = $request->getParameter('merchant_id');
        $this->state_name = $this->lga_name = $this->branch_code = $this->bank_name = $this->merchantName = "";
        $this->payment_mode = $request->getParameter('paymentMode');
        $this->formData = array();

        $merchantObj = Doctrine::getTable('Merchant')->find($this->merchantId);
        $this->merchantName = $merchantObj->getName();

        $usePager = $request->getParameter('page');
        if ($request->isMethod('post') || $request->getParameter('page')) {
            if ($request->getParameter('bank') && $request->getParameter('bank') != "" && $request->getParameter('bank') != 'BLANK') {
                $this->bank_id = $request->getParameter('bank');
                $bankObj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BASE);
                $bankInfoArray = $bankObj->getBankInfo($this->bank_id);
                $this->bank_name = $bankInfoArray['bank_name'];
                //                $this->formData['bank_name'] = $bank_name;
            }
            if ($request->getParameter('state') && $request->getParameter('state') != "" && $request->getParameter('state') != 'BLANK') {
                $this->state_id = $request->getParameter('state');
                $stateObj = stateServiceFactory::getService(stateServiceFactory::$TYPE_STATE);
                $stateInfoArray = $stateObj->getStateInfo($this->state_id);
                $this->state_name = $stateInfoArray['name'];
                //                $this->formData['state_name'] = $state_name;
            }
            if ($request->getParameter('lga') && $request->getParameter('lga') != "" && $request->getParameter('lga') != 'BLANK') {
                $this->lga_id = $request->getParameter('lga');
                $lgaObj = lgaServiceFactory::getService(lgaServiceFactory::$TYPE_LGA);
                $lgaInfoArray = $lgaObj->getLgaInfo($this->lga_id);
                $this->lga_name = $lgaInfoArray['name'];
                //                $this->formData['lga_name'] = $lga_name;
            }
            if ($request->getParameter('branch_code') && $request->getParameter('branch_code') != 'BLANK') {
                $this->branch_code = $request->getParameter('branch_code');
                $branch_code = $this->branch_code;
            }
        }
        if (!$this->bank_id) {
            $bankMerchantObj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);
            $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanksBankCheque($this->merchantId); // Vikash [WP058](04-09-2012) bug:35802
            $bankArray = array();
            if (count($bankMerchantListArray) > 0) {
                foreach ($bankMerchantListArray as $key => $val) {
                    if($request->getParameter('paymentMode') != 'Bank'){
                        if(count($val['Bank']['SortCode']))  // Vikash [WP058](04-09-2012) bug:35802
                            $bankArray[] = $val['Bank']['id'];
                    }else{
                         $bankArray[] = $val['Bank']['id'];
                    }
                }
            }
            if (count($bankArray))
                $this->bank_id = $bankArray;
        }

        if (!$this->lga_id && $this->state_id) {
            $lgaRelatedStates = Doctrine::getTable('Lga')->getLgaList($this->state_id);
            $lgaArray = array();
            if (count($lgaRelatedStates) > 0) {
                foreach ($lgaRelatedStates as $key => $val) {
                    $lgaArray[] = $val['id'];
                }
            }
            $this->lga_id = $lgaArray;
        }

        $bankBranchObj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BASE);
        $searchResultObj = $bankBranchObj->getBankBranchSearchResults($this->bank_id, $this->state_id, $this->lga_id, $this->branch_code);


        $this->setLayout('layout_popup_unauth');
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }


        $this->pager = new sfDoctrinePager('BankBranch', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($searchResultObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeEnableVisa(sfWebRequest $request) {
        $this->setLayout(false);
    }

    public function executePopulateBankDropDown(sfWebRequest $request) {
        $merchantId = 0;

        if ($request->getParameter('merchantId')) {
            $merchantId = $request->getParameter('merchantId');
        }



        $bankMerchantObj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);
        $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($merchantId);
        $bankArray = array();
        if (count($bankMerchantListArray) > 0) {
            foreach ($bankMerchantListArray as $key => $val) {
                $bankArray[$val['Bank']['id']] = $val['Bank']['bank_name'];
            }
        }

        $str = "";
        $str.= '<option value="" selected>All Banks</option>';

        foreach ($bankArray as $key => $val) {
            $str.= '<option value="' . $key . '">' . $val . '</option>';
        }
        return $this->renderText($str);
    }

	/**
	 * Function to create or auto login user came for payment with card payment option.
	 * @param sfWebRequest $request
	 */
	public function executeCreateOrderUser(sfWebRequest $request) {
		$this->setTemplate(false);
		$this->setLayout(false);
		sfView::NONE;
		$previousUrl = $request->getReferer();
		
		// Set parameters from url.
		$request_id = $request->getParameter('requestId');
		$userPayMode = $request->getParameter('userPayMode');
		$userEmail = $request->getParameter('userEmail');
		$chkUserExisist = Doctrine::getTable('UserDetail')->findByEmail($userEmail);
		
		/* Set session variable for hidding header and menu bar for user came for payment
		 as we are not showing that user is loged in or registerd with pay4me.*/
		$this->getUser()->setAttribute('userPayMode', $userPayMode);
		
		// If user exist in pay4me then make auto login else create user and make auto login.
		if ($chkUserExisist->count()) {
			$userDetails = Doctrine::getTable('sfGuardUser')->find($chkUserExisist->getFirst()->getUserId());
			$userId = $userDetails['id'];
			$userGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
			// If user is protal admin return back to payment mode screen.
			if ($userGroup['sfGuardGroup']['name'] == 'portal_admin') {
				$this->getUser()->setFlash('error', 'User with this email id is allready registered with pay4me with other user type.');
				$this->redirect($previousUrl);
			} else {
				$userDetails->setLastLogin(date('Y-m-d h:m:s'));
				$userDetails->save();
				$this->getUser()->signin($userDetails);
			}
		} else {
			// Create user in sf_guard_user table
			$sfUser = new sfGuardUser();
			$sfUser->setUsername($userEmail);
			$pass_word = PasswordHelper::generatePassword();
			$sfUser->setPassword($pass_word);
			$sfUser->setIsActive(true);
			$sfUser->setLastLogin(date('Y-m-d h:m:s'));
			$sfUser->save();
			EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(), $sfUser->getPassword());

			// Associate new user to guest_user in pay4me.
			$sfUserId = $sfUser->getId();
			$userTypeToCreate = 'app_pfm_role_guest_user';
			$sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get($userTypeToCreate));
			
			$sfGroupId = $sfGroupDetails->getFirst()->getId();
			$sfGuardUsrGrp = new sfGuardUserGroup();
			$sfGuardUsrGrp->setGroupId($sfGroupId);
			$sfGuardUsrGrp->setUserId($sfUserId);
			$sfGuardUsrGrp->save();
			// Create user in user_detail table.
			$merchantArray = Doctrine::getTable('merchantRequestDetails')->findByMerchantRequestId($request_id);
			$data = array('name' => $merchantArray[0]['name'], 'email' => $userEmail);
			$this->saveUserDetail($data, $sfUserId);

			$userDetails = Doctrine::getTable('sfGuardUser')->find($sfUserId);
			$this->getUser()->signin($userDetails);
		}	
		
		// Check request is valid or not
		if ($request_id && $request_id != "" && ctype_digit($request_id)) {
			$billObj = billServiceFactory::getService();
			if ($billObj->isRequestBelongsToEwalletUser($request_id, 'merchantRequestId')) {
				$transactionFlag = $billObj->putBill($request_id, $userPayMode);
			} else {
				$this->redirect('bill/invalidTransaction');
			}
			// Is bill allready paid if paid $transactionFlag=false else $transactionFlag=new transactionid
			if ($transactionFlag) {
				$this->redirect('bill/show?trans_num=' . $transactionFlag);
			} else {
				$this->redirect('bill/invalidAttempt');
			}
		}
	}

	/**
	 * Function to save user details in user_detail table.
	 * @param array $data
	 * @param integer $userId
	 */
	protected function saveUserDetail($data, $userId) {
		$userDetailObj = new UserDetail();
		$userDetailObj->setUserId($userId);
		if (trim($data['name']) != "")
			$userDetailObj->setName(trim($data['name']));
		if (trim($data['email']) != "")
			$userDetailObj->setEmail(trim($data['email']));
		
		$userTypeToCreate = sfConfig::get('app_pfm_role_guest_user');
		if (trim($userTypeToCreate) != '')
			$userDetailObj->setEwalletType(trim($userTypeToCreate));
		
		$userDetailObj->save();
	}
}