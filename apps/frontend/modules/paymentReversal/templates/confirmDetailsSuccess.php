<?php //use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<div class='wrapForm2'>

  <?php echo ePortal_legend('Payment Details');?>
  <?php $currency_id=(($currency_id>0)?$currency_id:1);?>
  <?php echo formRowComplete('eWallet Name',$getDetails->getEpMasterAccount()->getAccountName(),'','amount','','amount_row'); ?>
  <?php echo formRowComplete('eWallet Number',$getDetails->getEpMasterAccount()->getAccountNumber(),'','amount','','amount_row'); ?>
  <?php echo formRowComplete('Amount',format_amount($getDetails->getAmount(), $currency_id, 1),'','amount','','amount_row'); ?>
  <?php echo formRowComplete('Payment Date',$getDetails->getTransactionDate(),'','pdate','','pdate_row'); ?>
  <?php echo formRowComplete('Received By',pfmHelper::getUsernameByUserid($getDetails->getCreatedBy()),'','received_by','','received_by_row'); ?>
  <?php
  $pfmHelperObj=new pfmHelper();
  
  if($checkDraftDetails){?>
        <?php if($checkDraftDetails['payment_mode_option_id']==$pfmHelperObj->getPMOIdByConfName('Cheque')){ ?>
        <?php echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details'));?>
        <?php echo formRowComplete(__('Bank Name'),pfmHelper::getBanknameByUserid($getDetails->getCreatedBy()),'','bank_name','','bank_name_row'); ?>
        <?php echo formRowComplete(__('Bank Sort Code'),$checkDraftDetails['sort_code'],'','bank_sort_code','','bank_sort_code_row'); ?>
        <?php echo formRowComplete(__('Cheque Number'),$checkDraftDetails['check_number'],'','check_no','','check_no_row'); ?>
        <?php echo formRowComplete(__('Payee Account Number'),$checkDraftDetails['account_number'],'','account_no','','account_no_row'); ?>
        <?php } ?>
        <?php if($checkDraftDetails['payment_mode_option_id']==$pfmHelperObj->getPMOIdByConfName('bank_draft')){ ?>
        <?php echo ePortal_legend(__(pfmHelper::getDraftDisplayName() . ' Details'));?>
        <?php echo formRowComplete(__('Bank Name'),pfmHelper::getBanknameByUserid($getDetails->getCreatedBy()),'','bank_name','','bank_name_row'); ?>
        <?php echo formRowComplete(__('Bank Sort Code'),$checkDraftDetails['sort_code'],'','bank_sort_code','','bank_sort_code_row'); ?>
        <?php echo formRowComplete(__('Bank Draft Number'),$checkDraftDetails['check_number'],'','check_no','','check_no_row'); ?>
        <?php } ?>
  <?php }?>
</div>

<div class="divBlock">
  <center>
    <?php echo button_to('Refund','',array('class'=>'formSubmit', 'onClick'=>'validateForm("reverse")')); ?>&nbsp;<?php  echo button_to('Cancel','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('paymentReversal/index').'\''));?>
  </center>
</div>
