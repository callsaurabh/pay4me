<?php echo ePortal_pagehead(" "); ?>
<?php  //use_helper('Form','Validation') ?>
<div id='flash_error' class='error_list'></div> 


<div id='flash_notice' class='error_list'></div>


<div class="wrapForm2">

  <?php
  echo ePortal_legend('Reverse Payment');?>
  <?php echo formRowComplete('Validation No. <sup>*</sup>',''.'<input type="text" id="validation_no" name="validation_no"  >','','validation_no','err_validation_no','validation_no_row'); ?>
  <div class="divBlock">
    <center id="multiFormNav">
      <?php echo button_to('Continue','',array('class'=>'formSubmit', 'id'=>'continue_btn', 'onClick'=>'validateForm("confirm_details")')); ?>&nbsp;<?php  //echo button_to('Cancel','',array('onClick'=>'this.form.reset();'));?>
    </center>
  </div>
</div>
<div id="chargeLoader" align="center"><?php echo image_tag('/img/ajax-loader.gif',array()); ?></div>

<div class="load_overflow"><div id="confirm_details"></div></div>
</div>

<script language="Javascript">

      $(document).ready(function() {

           $('#flash_error').hide();
           $('#flash_notice').hide();

      });

  function validateForm(action){
    $('#confirm_details').html("");
    $('#flash_error').hide();
    $('#flash_notice').hide();
    var err  = 0;
    var validation_no = $("#validation_no").val();
    if(validation_no == ""){
      err = err+1;
      $('#err_validation_no').html("Please enter Validation number");
      $('#err_validation_no').show();
    }
    else{
      $('#err_validation_no').html("");
      $('#err_validation_no').hide();
    }

    //check if validation number is in our db; through ajax
    if(err == 0){
      $('#multiFormNav').hide();

      $('#chargeLoader').show();
      $.post('paymentReversal/verifyValidationNumber',{validation_no:validation_no}, function(data){
        $('#chargeLoader').hide();
        if(data!=""){
          if(data=='logout'){
            location.reload();
          }
          $('#err_validation_no').html(data);
          $('#err_validation_no').show();
          $('#multiFormNav').show();
        }else{
          if(action == 'confirm_details'){
            $('#validation_no').attr('readonly', 'true');
            $.post('paymentReversal/confirmDetails',{validation_no:validation_no}, function(data){
              $('#confirm_details').html(data);
            });
          }
          else if(action  == 'reverse'){
            $('#chargeLoader').show();
            $.post('paymentReversal/reverse', {validation_no:validation_no}, function(data){
              $('#chargeLoader').hide();
              if(data == 'success')
              { $('#flash_error').hide();
                $('#flash_notice').show();
                $('#flash_notice').html('<span>Payment has been reversed successfully</span>');
              }else
              {
                $('#flash_error').show();
                $('#flash_notice').hide();
                $('#flash_error').html(data);
              }
              $("#validation_no").val("");
              $('#multiFormNav').show();
              $('#validation_no').removeAttr("readonly");
            });
          }
        };
      });
    }
  }
</script>
