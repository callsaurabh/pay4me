<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class paymentReversalActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {

    }

    public function executeConfirmDetails(sfWebRequest $request) {
        $validation_no = $request->getParameter('validation_no');
        $epAccountingManagerObj = new EpAccountingManager;
        $detailObj = $epAccountingManagerObj->getAccountDetailsByValidationNumber($validation_no, 'credit');
        foreach ($detailObj as $key => $val) {
            if ($val->getEpMasterAccount()->getType() == 'ewallet') {
                $this->checkDraftDetails=Doctrine::getTable('EwalletRechargeAcctDetails')->getCheckDetailsByValidationNo($val->getPaymentTransactionNumber());
                $this->currency_id=Doctrine::getTable('EpMasterAccount')->getEwalletCurrencyIdByMasterAccountId($val->getMasterAccountId());
                $this->getDetails = $val;
            }
        }
    }

    public function executeVerifyValidationNumber(sfWebRequest $request) {
        $this->setLayout(null);
        $validation_no = $request->getParameter('validation_no');
        $err = "";
        $epAccountingManagerObj = new EpAccountingManager;
        $getDetails = $epAccountingManagerObj->getAccountDetailsByValidationNumber($validation_no, 'credit');

        //echo "<pre>";print_r($getDetails);die;
        if ((!is_object($getDetails)) && $getDetails == 0) {
            $err = "Invalid validation number";
            return $this->renderText($err);
        }

        $paymentReversalManager = new PaymentReversalManager;

        $masterAccountIdArr = array();
        foreach ($getDetails as $key => $details) {
            $masterAccountIdArr[$key] = $details->getMasterAccountId();
            $reversal_amount = $details->getAmount();
            if ($details->getEpMasterAccount()->getType() == 'ewallet') {
                $balance = $details->getEpMasterAccount()->getClearBalance();
            }
        }

        if (count($masterAccountIdArr) > 0) {
            $isEwalletCollectionAccount = $paymentReversalManager->isEwalletCollectionAccount($masterAccountIdArr);
        }

        if (count($masterAccountIdArr) > 0 && !$isEwalletCollectionAccount) {
            $err = "Not an eWallet recharged validation number";
            return $this->renderText($err);
        }

        $isAlreadyReverse = $paymentReversalManager->isAlreadyReverse($validation_no);

        if ((is_object($isAlreadyReverse))) {
            $err = "Payment is already reversed";
            return $this->renderText($err);
        }

        if ($reversal_amount > $balance) {
            $err = "The amount has already been exhausted by the eWallet User";
            return $this->renderText($err);
        }
        return $this->renderText($err);
    }

    public function executeReverse(sfWebRequest $request) {
        $validation_no = $request->getParameter('validation_no');
        $epAccountingManagerObj = new EpAccountingManager;
        $getDetails = $epAccountingManagerObj->getAccountDetailsByValidationNumber($validation_no);
 
        $amount = $getDetails->getLast()->getAmount();
        $wallet_number= $getDetails->getFirst()->getMasterAccountId();
        $currency = '1';
        if($wallet_number) {
            $serviceBankConfigObj = Doctrine::getTable("ServiceBankConfiguration")->findByMasterAccountId($wallet_number);
            $currency = $serviceBankConfigObj->getFirst()->getCurrencyId();
        }
        $credit_done_by = $getDetails->getLast()->getCreatedBy();

        $paymentMode_id = Doctrine::getTable('EwalletRechargeAcctDetails')->getPaymentMOIdByValidationNo($validation_no);

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $amount_formatted = format_amount($amount, $currency, 1);
        $wallet_account_number = $getDetails->getLast()->getEpMasterAccount()->getAccountNumber();
        $wallet_account_id = $getDetails->getLast()->getMasterAccountId();
        $helperObj=new pfmHelper();
        $teller_name = pfmHelper::getUsernameByUserid($getDetails->getLast()->getCreatedBy());
        $pay_mode=$pay_mode_name = 'Cash';
        if ($paymentMode_id) {
            $pay_mode_name=$helperObj->getPMONameByPMId($paymentMode_id);
            switch ($pay_mode_name) {
                case "Cheque":
                    $pay_mode = "Cheque";
                    break;
                case "Bank Draft":
                    $pay_mode = "draft";
                    break;
            }
        }

        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$CHARGED_WALLET_REVERSE, array('amount' => $amount_formatted, 'account_number' => $wallet_account_number, 'teller_name' => $teller_name,'payment_mode_option'=>$pay_mode_name));
        $attrArray = array();

        $collection_account_id = $getDetails->getFirst()->getMasterAccountId();
        $accountingObj = new pay4meAccounting();

        $attr1 = $accountingObj->getAccountingHolderObj($description, $amount, $collection_account_id, $isCleared = 1, $enter_type = EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT, $credit_done_by);
        $attr2 = $accountingObj->getAccountingHolderObj($description, $amount, $wallet_account_id, $isCleared = 1, $enter_type = EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT, $credit_done_by);
        $attrArray[0] = $attr1;
        $attrArray[1] = $attr2;
        $con = Doctrine_Manager::connection();

        try {
            $con->beginTransaction();

            $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts'));

            $reverse_validation_number = $event->getReturnValue();
            $managerObj = new payForMeManager();
            $managerObj->updateEwalletAccount($collection_account_id, $wallet_account_id, $amount, 'debit');
            $paymentReversalManager = new PaymentReversalManager;
            $SaveReverse = $paymentReversalManager->saveReversalValidationNumber($validation_no, $reverse_validation_number);
            $this->updateEwalletAcctDetailsOnReversal($pay_mode, $wallet_account_id, $reverse_validation_number, $validation_no);
            $this->paymentReversalAuditDetails($wallet_account_number, $validation_no, $reverse_validation_number);
            $con->commit();

            return $this->renderText('success');
        } catch (Exception $e) {
            $con->rollback();
            return $this->renderText($e);
            //   throw $e;
        }
    }

    //storing check/draft details
    private function updateEwalletAcctDetailsOnReversal($pay_mode, $ewallet_account_id, $validation_number_new, $validation_no_old) {
        if (($pay_mode == sfConfig::get('app_payment_mode_option_Cheque')) || ($pay_mode == 'draft')) {
            //  sfContext::getInstance()->getLogger()->debug("add recharge acct details for " . $pay_mode . " [ewallet_account_id : $ewallet_account_id, validation_number : $validation_number]", 'debug');
//            $checkNo_andAcctNo_record=Doctrine::getTable('EpMasterLedger')->getCheckNumberAccountNumberDetailsOnRechargeReversal($validation_no_old,"credit");
            //Check/draft Recharge
//                $check_number = $request->getParameter('check_number');
//                $account_number = $request->getParameter('account_number');
            $query = Doctrine_Query::create()
                            ->select('*')
                            ->from('EpMasterLedger')
                            ->where("payment_transaction_number = ?", $validation_number_new)
                            ->andWhere("entry_type = ?", 'debit')
                            ->andWhere("master_account_id = ?", $ewallet_account_id);
            $resLedger = $query->execute();
            if ($resLedger->count() == 1) {
                $ledger_id = $resLedger->getFirst()->getId();
                try {
                    $pfmHelperObj = new pfmHelper();
                    $ERAcctDetails = new EwalletRechargeAcctDetails();
                    if ($pay_mode == sfConfig::get('app_payment_mode_option_Cheque')) {
                        $ewalletRechargeAcctDetails = $this->getCheckNoAcctNoOnReversal($validation_no_old, "credit");
                        $check_id = $pfmHelperObj->getPMOIdByConfName('Cheque');
                        $ERAcctDetails->setPaymentModeOptionId($check_id);

                        $ERAcctDetails->setCheckNumber($ewalletRechargeAcctDetails->getCheckNumber());
                        $ERAcctDetails->setAccountNumber($ewalletRechargeAcctDetails->getAccountNumber());
                        $ERAcctDetails->setSortCode($ewalletRechargeAcctDetails->getSortCode());
                    } else if ($pay_mode == 'draft') {
                        $ewalletRechargeAcctDetails = $this->getCheckNoAcctNoOnReversal($validation_no_old, "credit");
                        $draft_id = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                        $ERAcctDetails->setPaymentModeOptionId($draft_id);
                        $ERAcctDetails->setCheckNumber($ewalletRechargeAcctDetails->getCheckNumber());
                        $ERAcctDetails->setAccountNumber(NULL);
                        $ERAcctDetails->setSortCode($ewalletRechargeAcctDetails->getSortCode());
                    }
                    $ERAcctDetails->setLedgerId($ledger_id);
                    $ERAcctDetails->save();
                } catch (Exception $e) {
                    sfContext::getInstance()->getLogger()->debug("error in " .pfmHelper::getChequeDisplayName()." Recharge" . $e->getTraceAsString());
                }
            }
        }
    }

    private function getCheckNoAcctNoOnReversal($validation_number, $type='credit') {
        $query = Doctrine_Query::create()
                        ->select('*')
                        ->from('EpMasterLedger e')
                        ->leftJoin('e.EwalletRechargeAcctDetailsRecord erad')
                        ->where("e.payment_transaction_number = ?", $validation_number)
                        ->andWhere('e.is_cleared=?', '1')
                        ->andWhere("e.entry_type = ?", $type);


        return $query->execute()->getLast()->getEwalletRechargeAcctDetailsRecord()->getLast();
    }

    public function paymentReversalAuditDetails($account_number, $pvalidation, $rvalidation) {
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_RECHARGED_PAYMENT_REVERSAL,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_RECHARGED_PAYMENT_REVERSAL, array('account_number' => $account_number, 'pvalidation' => $pvalidation, 'rvalidation' => $rvalidation)),
                        null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

}

?>
