<div id="theMid">
  <?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

  <?php echo ePortal_listinghead('Bank Merchant List'); ?>

  <div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
          <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
         </th>
      </tr>
    </table>
    <br class="pixbr" />
  </div>

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Bank</th>
      <th>Merchant</th>
      <th>Currency</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
     <?php
     if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
         $currencyObj = $result->getCurrencyCode();
        $i++;
    ?>
    <tr class="alternateBgColour">
      <td><?php echo $i ?></td>
      <td align="center"><?php echo $result->getBankName() ?></td>
      <td align="center"><?php echo $result->getMerchantName() ?></td>
      <td align="center"><?php echo ucfirst($currencyObj->getCurrency()); ?></td>
      <td align="center"><?php echo link_to(' ', 'bank_merchant/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
      <?php echo link_to(' ', 'bank_merchant/delete?id='.$result->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?', 'class' => 'delInfo', 'title' => 'Delete')) ?>

       <?php if($result->getStatus() == 1)://bank branch is active; enabled
              ?>
        <a href="javascript:;" onClick="set_status_bank_merchant('<?php echo $result->getId();?>','deactivate')" class="deactiveInfo" title="Deactivate" ></a>
            <?php endIf;

            if($result->getStatus() == 0): //bank branch is inactive; disabled
              ?>
        <a href="javascript:;" onClick="set_status_bank_merchant('<?php echo $result->getId();?>','activate')" class="activeInfo" title="Activate" ></a>

            <?php endIf;?>
      </td>
    </tr>
    <?php endforeach; ?>
  <tr><td colspan="5">
<div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

  </div></td></tr>
    <?php }else{ ?>
     <tr><td  align='center' class='error' colspan="5">No Bank Merchant found</td></tr>
    <?php } ?>

  </tbody>
   
</table>
</div>
<?php  echo button_to('Add New','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('bank_merchant/new').'\''));?>
  
</div>
  <script>
    var activate_url = "<?php echo url_for('bank_merchant/index');?>";
    function set_status_bank_merchant(id,status){
    var page = "<?php echo $page;?>";
     $("#theMid").load(activate_url, {status:status, id:id, page:page});

  }

  </script>
