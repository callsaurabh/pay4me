<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $bank_merchant->getid() ?></td>
    </tr>
    <tr>
      <th>Bank:</th>
      <td><?php echo $bank_merchant->getbank_id() ?></td>
    </tr>
    <tr>
      <th>Merchant:</th>
      <td><?php echo $bank_merchant->getmerchant_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $bank_merchant->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $bank_merchant->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $bank_merchant->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $bank_merchant->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $bank_merchant->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('bank_merchant/edit?id='.$bank_merchant->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('bank_merchant/index') ?>">List</a>
