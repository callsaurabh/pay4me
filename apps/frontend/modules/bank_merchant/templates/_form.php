<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<div class="wrapForm2">
  <form action="<?php echo url_for('bank_merchant/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="pfm_bank_form" name="pfm_bank_form">
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>

    <?php echo $form ?>
    <div class="divBlock">
        <center>
            &nbsp;<?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('bank_merchant/index').'\''));?>
            <input type="submit" value="Save" class="formSubmit" />
    </center></div>

  </form>
</div>
</div>