<?php

/**
 * bank_merchant actions.
 *
 * @package    mysfp
 * @subpackage bank_merchant
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class bank_merchantActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    if($request->getParameter('status')) {
      $this->setStatus($request);
    }

    $bank_merchant_obj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);
    $this->bank_merchant_list = $bank_merchant_obj->getAllRecords();

    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('BankMerchant',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->bank_merchant_list);
    $this->pager->setPage($this->page);
    $this->pager->init();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->bank_merchant = Doctrine::getTable('BankMerchant')->find($request->getParameter('id'));
    $this->forward404Unless($this->bank_merchant);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new BankMerchantForm();
    $this->form->setDefault('currency_id',0);
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new BankMerchantForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($bank_merchant = Doctrine::getTable('BankMerchant')->find($request->getParameter('id')), sprintf('Object bank_merchant does not exist (%s).', $request->getParameter('id')));
    $this->form = new BankMerchantForm($bank_merchant);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($bank_merchant = Doctrine::getTable('BankMerchant')->find($request->getParameter('id')), sprintf('Object bank_merchant does not exist (%s).', $request->getParameter('id')));
    $this->form = new BankMerchantForm($bank_merchant);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($bank_merchant = Doctrine::getTable('BankMerchant')->find($request->getParameter('id')), sprintf('Object bank_merchant does not exist (%s).', $request->getParameter('id')));
    $bank_merchant->delete();
    $this->getUser()->setFlash('notice', sprintf('Bank Merchant deleted successfully'));

    $this->redirect('bank_merchant/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $bank_merchant = $form->save();
      if(($request->getParameter('action')) == "update") {
        $this->getUser()->setFlash('notice', sprintf('Bank Merchant updated successfully'));
      }
      else if(($request->getParameter('action')) == "create") {
        $this->getUser()->setFlash('notice', sprintf('Bank Merchant added successfully'));
      }
      $this->redirect('bank_merchant/index');
    }
  }
  public function setStatus(sfWebRequest $request)
  {
    $bankMerchantId = $request->getParameter('id');
    if($request->getParameter('status')!="") {
      $bank_merchant = Doctrine::getTable('BankMerchant')->find(array($request->getParameter('id')));
      $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_MERCHANTINFO,$bank_merchant->getMerchant()->getName(), $bank_merchant->getId()));
      if($request->getParameter('status') == "activate") {
        $bank_merchant->setStatus("1");
        $status_msg = "Activated";

        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_TRANSACTION,
          EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_MERCHANT_ACTIVATE,
          EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_MERCHANT_ACTIVATE, array('merchantname' => $bank_merchant->getMerchant()->getName())),
          $applicationArr);

      }
      if($request->getParameter('status') == "deactivate") {
        $bank_merchant->setStatus("0");
        $status_msg = "Deactivated";

        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_TRANSACTION,
          EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_MERCHANT_DEACTIVATE,
          EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_MERCHANT_DEACTIVATE, array('merchantname' => $bank_merchant->getMerchant()->getName())),
          $applicationArr);

      }
      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
      $bank_merchant->save();
      $this->getUser()->setFlash('notice', sprintf($bank_merchant->getBank()->getBankName().' - '.$bank_merchant->getMerchant()->getName().' association '.$status_msg.' successfully'),FALSE);
    }
  }
}
