<?php

/**
 * interswitch_configuration actions.
 *
 * @package    mysfp
 * @subpackage interswitch_configuration
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class interswitch_configurationActions extends sfActions {

    public function executeTryPayment(sfWebRequest $request) {
//        $this->verifyPayment(133637274148582, 21100);//recharge
//        $this->verifyPayment(133637999141787, 11100);//Payment
    }

    public function executeRequestToInterswitch(sfWebRequest $request) {

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        $capture_data_in_log = array();
        $this->retObj = $obj = Doctrine::getTable('EpInterswitchRequest')->find($request->getParameter('id'));
        $epInterswitchManager = new EpInterswitchManager();
        $this->productId = $epInterswitchManager->productId;
        $capture_data_in_log[] = "productId=" . $this->productId;
        $capture_data_in_log[] = 'amount=' . $obj->getAmountKobo();
        $capture_data_in_log[] = 'currency=' . $obj->getCurrency();

        $this->site_redirect_url = url_for1('@siteRedirectUrlForInterswitch', true);
        $capture_data_in_log[] = "site_redirect_url=" . $this->site_redirect_url;
        $this->site_name = $epInterswitchManager->site_name;
        $capture_data_in_log[] = "site_name=" . $this->site_name;
        $capture_data_in_log[] = "cust_id=" . $obj->getCustId();
        $capture_data_in_log[] = "cust_id_desc=" . $obj->getCustIdDescription();
        $capture_data_in_log[] = 'cust_name=' . $obj->getCustName();
        $capture_data_in_log[] = "cust_name_desc=" . $obj->getCustNameDescription();
        $capture_data_in_log[] = 'txn_ref=' . $obj->getTrnxId();

        $this->payItemId = $request->getParameter('payItemId');
        $capture_data_in_log[] = 'pay_item_id=' . $this->payItemId;
        $this->payItem_name = $request->getParameter('payItemName');
        $capture_data_in_log[] = 'pay_item_name=' . $this->payItem_name;
        $capture_data_in_log[] = 'local_date_time=' . date("d-M-y h:i:s", strtotime($obj->getCreatedAt()));


        $this->logResponse(implode("|", $capture_data_in_log), 'postParameters-' . $this->retObj->getTrnxId());
        $this->setLayout(false);
    }

    private function getPay4meMerchantAccount() {


        $merchantId = Doctrine::getTable('Merchant')->findBy('name', sfConfig::get('app_pay4me_merchant'))->getLast()->getId();
        return Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountForRecharge($merchantId);
    }

    public function executeInterswitchRecharge(sfWebRequest $request) {
        $transNo = $request->getParameter('transNo'); //this is the account id


        $trnxId = $request->getParameter('orderId');
        $type = $request->getParameter('type');
        $interswitchCategoryObj = $this->getPay4meMerchantAccount();
        $name = $this->getUser()->getGuardUser()->getUserDetail()->getLast()->getName() . " " . $this->getUser()->getGuardUser()->getUserDetail()->getLast()->getLastName();

        $epInterswitchManager = new EpInterswitchManager($interswitchCategoryObj->getMerchantAccount(), $interswitchCategoryObj->getName(), "Pay4me", $name);
        $this->payItemId = $interswitchCategoryObj->getMerchantAccount();
        $this->payItemName = $interswitchCategoryObj->getName();
        $epInterswitchManager->amount = round($request->getParameter('totalCharges'), 2);
        $epInterswitchManager->trnxId = $trnxId;
        $epInterswitchManager->currency = sfConfig::get('app_naira_currency');
        $epInterswitchManager->userId = $this->getUser()->getGuardUser()->getId();
        $this->transNo = $transNo;
        $this->retObj = $epInterswitchManager->setRequest();
        $this->type = 'recharge';
        $this->time = sfConfig::get('app_interswitch_time_in_sec_for_timeout') * 1000;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        $this->request_url = url_for('interswitchRequestForm');
        //check if any recharge is available for pending in last transaction
        $arrDetails = Doctrine::getTable('GatewayOrder')->getLastOrderDetails($transNo, '', 1);
        if (count($arrDetails) > 0 && strtolower($arrDetails[0]['status']) == 'pending') {
            //query interswitch status and update for same
            $boolLastStatus = $this->UpdateQueryInterswitchStatus($arrDetails[0]['order_id'], $arrDetails[0]['app_id']);
        }

        $this->setTemplate('interswitchForm');
    }

    public function executeInterswitchForm(sfWebRequest $request) {

        $transNo = $request->getParameter('transNo');
        $orderId = $request->getParameter('orderId');
        $paymentModeOption = $request->getParameter('paymentModeOption');
        $validateTrans = $this->validateTransactionNumberForInterswitch($transNo, $paymentModeOption);
        $this->time = sfConfig::get('app_interswitch_time_in_sec_for_timeout') * 1000;
        if ($validateTrans) {
            $merchantName = $validateTrans['MerchantRequest']['Merchant']['name'];
            $item_id = $validateTrans['MerchantRequest']['merchant_item_id'];
            $payment_status = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
            if ($payment_status == 0) {
                $this->getUser()->setFlash('notice', 'Payment has already been made for this Application', true);

                $merchant_obj = Doctrine::getTable('MerchantRequest')->getMerchantPaidId($item_id);
                $merchant_request_payment_id = $merchant_obj->getFirst()->getId();
                $transaction_number_payment_id = $merchant_obj->getFirst()->getTransaction()->getFirst()->getTransactionNumber();

                return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'paymentSystem',
                            'action' => 'payOnline', 'txn_no' => $transaction_number_payment_id, 'duplicate_payment_receipt' => true)) . "'</script>");
            } else {
                //check previous payments
                //checking for last transaction is done or not
                //check  is there any last transaction is in pending status

                $arrDetails = Doctrine::getTable('GatewayOrder')->getLastOrderDetails($transNo, '', 1);
                //check last transaction status
                if (count($arrDetails) > 0 && strtolower($arrDetails[0]['status']) == 'pending') {
                    //query interswitch status and update for same

                    $boolLastStatus = $this->UpdateQueryInterswitchStatus($arrDetails[0]['order_id'], $arrDetails[0]['app_id']);
                    //if sucess redirect to receipt
                    if ($boolLastStatus) {
                        //do soft delete of GateWayOrderTable
                        Doctrine::getTable('GatewayOrder')->deleterecords($transNo, $orderId);
                        $this->getUser()->setFlash('notice', 'Payment has already been made for this Application', true);
                        $renderText = "<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'paymentSystem',
                                    'action' => 'payOnline', 'txn_no' => $arrDetails[0]['app_id'])) . "'</script>";
                        return $this->renderText($renderText);
                    }
                }

                $txnArr = Doctrine::getTable('Transaction')->getDetailByItemId($item_id);
                $appArr = array();
                foreach ($txnArr as $txn) {
                    if ($txn['pfm_transaction_number'] != $transNo) {
                        $appArr[] = $txn['pfm_transaction_number'];
                    }
                }
                if (count($appArr) > 0) {

                    $status = 'pending';
                    $gatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByAppId($appArr, $paymentModeOption, $status);
                    if ($gatewayOrders) {
                        $lastPendingOrderId = $gatewayOrders->getfirst()->getOrderId();
                        $lastPendingAppId = $gatewayOrders->getfirst()->getAppId();
                        //query interswitch status and update for same
                        $boolLastStatus = $this->UpdateQueryInterswitchStatus($lastPendingOrderId, $lastPendingAppId);
                        //if sucess redirect to receipt
                        if ($boolLastStatus) {
                            //do soft delete of GateWayOrderTable
                            Doctrine::getTable('GatewayOrder')->deleterecords($transNo, $orderId);
                            $this->getUser()->setFlash('notice', 'Payment has already been made for this Application', true);
                            $renderText = "<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'paymentSystem',
                                        'action' => 'payOnline', 'txn_no' => $lastPendingAppId)) . "'</script>";
                            return $this->renderText($renderText);
                        }
                    }
                }
                ///////------------------get Category Id for Merchant From Here------------------

                $interSwitch_merchantAccount = $validateTrans['MerchantRequest']['Merchant']['InterswitchMerchantCategoryMapping'][0]['InterswitchCategory']['merchant_account'];
                $interswitch_pay_item_name = $validateTrans['MerchantRequest']['Merchant']['InterswitchMerchantCategoryMapping'][0]['InterswitchCategory']['name'];

                $name = $this->getUser()->getGuardUser()->getUserDetail()->getLast()->getName() . " " . $this->getUser()->getGuardUser()->getUserDetail()->getLast()->getLastName();
                $epInterswitchManager = new EpInterswitchManager($interSwitch_merchantAccount, $interswitch_pay_item_name, $merchantName, $name);
                $epInterswitchManager->amount = $this->getAmount($transNo);
                $epInterswitchManager->trnxId = $orderId;
                $epInterswitchManager->currency = sfConfig::get('app_naira_currency');
                $epInterswitchManager->userId = $this->getUser()->getGuardUser()->getId();
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $this->request_url = url_for('interswitchRequestForm');
                $this->retObj = $epInterswitchManager->setRequest();


                $this->payItemId = $interSwitch_merchantAccount;
                $this->payItemName = $interswitch_pay_item_name;
                $this->type = 'payment';
                $this->transNo = $transNo;
                $this->paymentModeOption = $paymentModeOption;
                $this->setTemplate('interswitchForm');
            }
        } else {
            return $this->renderText('error');
        }
    }

    public function executeInterswitchStatus(sfWebRequest $request) {

        $transNo = $request->getParameter('appId');
        $orderId = $request->getParameter('orderId');
        $type = $request->getParameter('type');
        $boolLastStatus = $this->UpdateQueryInterswitchStatus($orderId, $transNo);
        if ($boolLastStatus) {
            $status = true;
            if ($type == 'payment') {
                $this->getUser()->setFlash('notice', 'Payment Successful', true);
                $url = $this->generateUrl('default', array('module' => 'paymentSystem', 'action' => 'payOnline', 'txn_no' => $transNo));
            } else {
                $this->getUser()->setFlash('notice', 'Recharge Successful', true);
                //get validation number
                $arrGateWayOrder = Doctrine::getTable('GatewayOrder')->getDetailsByOrderId($orderId);
                $url = $this->generateUrl('default', array('module' => 'ewallet', 'action' => 'rechargeReceipt', 'validationNo' => base64_encode($arrGateWayOrder[0]['validation_number'])));
            }
        } else {
            $status = false;
            if ($type == 'payment') {
                $this->getUser()->setFlash('error', 'Payment Failed', true);
                $url = $this->generateUrl('default', array('module' => 'bill', 'action' => 'PayByCard', 'transNo' => $transNo));
            } else {
                $this->getUser()->setFlash('error', 'Recharge Failed', true);
                $url = $this->generateUrl('default', array('module' => 'recharge_ewallet', 'action' => 'recharge', 'recharge_gateway' => 'interswitch'));
            }
        }
        $renderText = "<script>window.parent.location = '" . $url . "'</script>";
        return $this->renderText($renderText);
        //return $this->renderText(json_encode(array('status'=>$status,'message'=>$message,'url'=>$url)));
    }

    private function UpdateQueryInterswitchStatus($orderId, $transNo) {

        $getData = array();
        //transaction reference no
        $txRef = $orderId;
        //app id
        $txnId = $transNo;
        //get Gateway order details
        $paymentModObj = Doctrine::getTable('GatewayOrder')->getByOrderId($txRef);
        if ($paymentModObj['type'] == 'recharge') {
            $interswitchCategoryObj = $this->getPay4meMerchantAccount();
        } else {

            $interswitchCategoryObj = Doctrine::getTable('InterswitchCategory')->getP4meMerchantAccountFromTransactionNumber($txnId);
        }
        //payment mode options
        $paymentModeOption = $paymentModObj['payment_mode_option_id'];
        //type
        $type = $paymentModObj['type'];
        $status = $paymentModObj['status'];
        $paymentmode = $paymentModeOption;
        $service_charge = $paymentModObj['servicecharge_kobo'];
        if ($status != "success") {
            //create EpInterswitchManager object
            ///////------------------get Category Id for Merchant From Here------------------
            $epInterswitchManager = new EpInterswitchManager($interswitchCategoryObj->getMerchantAccount(), $interswitchCategoryObj->getName());
            //get all details from EpInterswitchRequest
            $getResponse = $epInterswitchManager->selectAllFromRequest($txRef);

            if (count($getResponse)) {
                if (array_key_exists('mert_id', $getResponse[0])) {
                    $mert_id = $getResponse[0]['mert_id'];
                    $cadp_id = $getResponse[0]['cadp_id'];
                }
                //get amount in kobo
                $amt = $getResponse[0]['amount_kobo'];
                //Check response has valid amount or not

                $verification = $this->verifyPayment($txRef, $amt);
//                $verification=     $this->verifyPayment('133715815035361', 793156);
                //get RESPONSE  Status Message
                $paymentStatusMsg = GatewayStatusMessages::getInterswitchStatusMessage($verification['respCode']);
                //save the response
                // $epInterswitchManager->saveForPending($getResponse[0]['id'], $verification['respCode'], $amt, $paymentStatusMsg);
                //set values to getData variable
                $getData['txnref'] = $txRef;
                $getData['apprAmt'] = $amt;
                $getData['rspcode'] = $verification['respCode'];
                $getData['date'] = $verification['transDate'];
                $getData['rspdesc'] = $paymentStatusMsg;
                //update gateway order with data
                if ($verification['respCode'] == '00') {
                    $getData['status'] = 'success';

//                    $getData['pan'] = $verification['respPan'];
                    $getData['approvalcode'] = $verification['approvalCode'];
                    $getData['desc'] = 'Approved by Financial Institution';


                    //update Gateway Order

                    $updated = $this->updateGatewayOrder($getData);

                    //if type is payment
                    if ($type == "payment") {

                        $paymentObj = new Payment($this->getAppIdFromOrderId($txRef));
                        $arrayVal = $paymentObj->proceedToPay();
                        $gatewayOrderObj = new gatewayOrderManager();
                        
                        $pfmTransactionDetails = $gatewayOrderObj->getValidationNumber($arrayVal['var_value']);
                        $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
                      
                        $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $txRef);
                    } else {
                        //if type is recharge
                        //paymentmode option = 2 and service charges needs to capture from top
                        $validationNo = $this->recharge($txnId, $amt, $paymentmode, $service_charge);
                        $gatewayOrderObj = new gatewayOrderManager();
                        $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $txRef);
                        $this->addJobForMail($txnId, $type, $txRef);
                    }
                    return true;
                    //if transaction is not sucessfull
                } else {
                    //update gateway order table
                    $getData['status'] = 'failure';
                    $updated = $this->updateGatewayOrder($getData);
                }
                //if no request detail
            }
        }
//        exit;
        return false;
    }

    private function getAppIdFromOrderId($orderId) {
        return Doctrine::getTable('GateWayOrder')->findBy('order_id', $orderId)->getLast($orderId)->getAppId();
    }

    protected function getPayModeOptionId() {
        $payModeName = sfConfig::get('app_payment_mode_option_interswitch');
        $payModeOptions = Doctrine::getTable('PaymentModeOption')->findByName($payModeName);
        $payMode = $payModeOptions->getfirst()->getId();
        return $payMode;
    }

    /*
     * Process the eTranzact record for Interswitch pending
     */

    public function executeProcessInterswitch(sfWebRequest $request) {
        try {
            $this->setLayout(null);
            ini_set('memory_limit', '512M');

            $payMode = $this->getPayModeOptionId();


            $from_trans_date = date('Y-m-d H:i:s', strtotime(date('Y') . "-" . date('m') . "-" . (date('d') - 1) . " 00:00:00"));
            $status = 'pending';
            // $from_trans_date if want to process all record then set $from_trans_date = '';
            //       $from_trans_date = '';
            $allGatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByOrderIdAndStatus($payMode, $status, $from_trans_date);

            foreach ($allGatewayOrders as $gatewayOrder) {
                $orderId = $gatewayOrder->getOrderId();
                $appId = $gatewayOrder->getAppId();
                $this->UpdateQueryInterswitchStatus($orderId, $appId);
            }

            return $this->renderText("Interswitch processing done");
        } catch (Exception $e) {
            $this->logMessage("Logging the exception from Interswitch Updation Processing.." . $e->getMessage() . var_dump(number_format(memory_get_peak_usage())));
        }
    }

    public function executeInterswitchFormRedirect(sfWebRequest $request) {
        $this->setLayout(false);
        $order_id = base64_decode($request->getParameter('id'));
        $epInterswitchManager = new EpInterswitchManager();
        $this->retObj = $epInterswitchManager->selectAllFromRequest($order_id);
        $this->setTemplate('interswitchFormRedirect');
    }

    public function executeInterswitchResponse(sfWebRequest $request) {

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        //get all get data values
        $getData = $request->getParameterHolder()->getAll();
        //log response
        $logContent = "redirectUrl=" . url_for1('@siteRedirectUrlForInterswitch', true) . "\r\n";
        $logContent.="requestMethod=" . $request->getMethod() . "\r\n";
        $logContent.=urldecode($request->getContent());
        $this->logResponse($logContent, 'response-' . $getData['txnref']);
        //get order Id
        $order_id = $getData['txnref'];
        //get amount paid
        $amount_paid = $getData['apprAmt'];
        //initializing total amount paid
        $total_amount_paid = 0;
        //creating EpInterswitchManager Objects
        //get Gateway order details by txnref
        //here orderarray return 0 if no order found else returns an array
        $orderArray = $this->getByOrderId($order_id);
        //check if order array is 0 return false which means an invalid transaction/response
        if (!$orderArray) {
            return false;
        }
        //get application id
        $txnId = $orderArray['app_id']; //ep_master_account id in case of Recharge; and transaction_number in case of payment
        //get order type
        $type = $orderArray['type'];
        //get payment mode options
        $payment_mode_option_id = $orderArray['payment_mode_option_id'];
        //get status
        $checkStatus = $orderArray['status'];
        //get service change in kobo
        $service_charge = $orderArray['servicecharge_kobo'];
        //unset Gateway order array
        unset($orderArray);
        $interSwitchMgr = new EpInterswitchManager();
        //verify amount paid
        $getReqData = $interSwitchMgr->selectAllFromRequest($getData['txnref']);

        $verification = $this->verifyPayment($order_id, $getReqData[0]['amount_kobo']);

        $transDate = $verification['transDate'];
        $pan = "";
        $approvalcode = "";
        if ($verification['respCode'] == '00') {
            //   $pan = $verification['respPan'];
            $approvalcode = $verification['approvalCode'];
        }
        //get verification amount
        $verification = $this->getAmountVerificationStatus($verification);

        //get payment status
        $paymentStatusMsg = GatewayStatusMessages::getInterswitchStatusMessage($verification['respCode']);
        //process response
        $arrResponse = $this->processresponse($txnId, $order_id, $type, $verification, $checkStatus, $paymentStatusMsg, $getData, $service_charge, $payment_mode_option_id, $transDate, $pan, $approvalcode);
        //check payment type
        if (strtolower($type) == 'payment' || strtolower($type) == 'recharge') {
            //check for payment status
            $this->getUser()->setFlash($arrResponse['flashtype'], $arrResponse['flashMessage'], true);
            return $this->renderText($arrResponse['renderText']);
        } else {
            return false;
        }
    }

    //update Gateway order details
    private function updateGatewayOrderDetails($boollock, $verification, $checkStatus, $getData) {
        if ($boollock) {
            if (strtolower($checkStatus) == 'failure' || $checkStatus == 'pending') {
                $this->updateGatewayOrder($getData);
            }
        }
    }

    //get lock status
    private function getlockstatus($orderId) {
        if ($this->getLock($orderId)) {
            return true;
        }

        return false;
    }

    //get payment response message
    private function getPaymentResponseMessages($txnId, $order_id, $type, $verification, $checkStatus, $paymentStatusMsg, $getData, $arrayVal='') {
        //set verification as response code
        $rspcode = $verification;
        //set description as response description
        $rspdesc = $paymentStatusMsg;
        //initialize variables
        $returnField2 = '';
        $returnVal2 = '';
        //if verification is 00  and $checkStatus is failure or $checkStatus is pending
        if ($verification == '00' && (strtolower($checkStatus) == 'failure' || $checkStatus == 'pending')) {
            //set final status as sucess
            $status = 'success';
            //set response code as 00
            $rspcode = $verification;
            //set response description
            $rspdesc = 'Approved by Financial Institution';
            //set Type of status message to display i.e sucess or error
            if (isset($arrayVal['comments'])) {
                $flashtype = $arrayVal['comments'];
            } else {
                $flashtype = "";
            }
            //set flash message comments
            $flashMessage = $arrayVal['comments_val'];
            if ($arrayVal['comments'] == 'error') {
                //set module
                $module = 'bill';
                //set action
                $action = 'PayByCard';
                //set return field
                $returnField1 = 'transNo';
                //set return value
                //added new check for query and response checks Bug id:FS#29960,FS#29963
                if (is_array($arrayVal['var_value'])) {
                    $returnVal1 = $arrayVal['var_value']['pfm_transaction_number'];
                } else {
                    $returnVal1 = $arrayVal['var_value'];
                }
                $status = 'failure';
                $rspdesc = $arrayVal['comments_val'];
            } else {
                //set module
                $module = 'paymentSystem';
                //set action
                $action = 'payOnline';
                //set return field and value
                $returnField1 = 'txn_no';
                $returnVal1 = $arrayVal['var_value'];
            }
            //for duplicate transaction status
        } else if ($verification == '26' || $verification == '94') {
            $status = 'failure';
            $flashtype = 'error';
            $flashMessage = "Your transaction with Transaction Identification Number: $order_id was not successful <br>Reason: $paymentStatusMsg";
            $module = 'paymentSystem';
            $action = 'payOnline';
            $returnField1 = 'txn_no';
            $returnVal1 = $txnId;
            //set douplicate transaction field and value
            $returnField2 = 'duplicate_payment_receipt';
            $returnVal2 = true;
        } else if ($verification == '00') {
            //if verification is sucess and transaction is already sucess
            $status = 'success';
            $flashtype = 'notice';
            $flashMessage = "Payment Successful";
            $module = 'paymentSystem';
            $action = 'payOnline';
            $returnField1 = 'txn_no';
            $returnVal1 = $txnId;
        } else {
            //if transaction is failure
            $status = 'failure';
            //for X-series codes special message
            if ($verification == "X00" || $verification == "X01" || $verification == "X02" || $verification == "X03" || $verification == "X04" || $verification == "X05") {
                $flashtype = 'error';
                $flashMessage = $paymentStatusMsg;
            } else {
                $flashtype = 'error';
                $flashMessage = "Your transaction with Transaction Identification Number: $order_id was not successful<br>Reason: $paymentStatusMsg";
            }
            $module = 'bill';
            $action = 'PayByCard';
            $returnField1 = 'transNo';
            $returnVal1 = $txnId;
        }

        $renderText = "<script>window.parent.location = '" . $this->generateUrl('default', array('module' => $module,
                    'action' => $action, $returnField1 => $returnVal1, $returnField2 => $returnVal2)) . "'</script>";
        return array('status' => $status, 'rspcode' => $rspcode, 'rspdesc' => $rspdesc, 'flashtype' => $flashtype, 'flashMessage' => $flashMessage, 'renderText' => $renderText);
    }

    //get recharge response messages
    private function getRechargeResponseMessages($txnId, $order_id, $type, $verification, $checkStatus, $paymentStatusMsg, $getData, $validationNo='') {

        //set response code
        $rspcode = $verification;
        //set response description
        $rspdesc = $paymentStatusMsg;
        $returnField1 = '';
        $returnVal1 = '';
        //if verification is 00 and status is failure or pending
        if ($verification == '00' && (strtolower($checkStatus) == 'failure' || $checkStatus == 'pending')) {

            $status = 'success';
            $rspcode = $verification;
            $rspdesc = 'Approved by Financial Institution';
            $flashtype = 'notice';
            $flashMessage = "Recharge Successful";
            //initialize module,action and return fields
            $module = 'ewallet';
            $action = 'rechargeReceipt';
            $returnField1 = 'validationNo';
            $returnVal1 = base64_encode($validationNo);
            //if transaction is already sucess  and return status is 00
        } else if ($verification == '00') {
            $status = 'success';
            //check interswitch clearence days
            if (settings::getInterswitchClearanceDays()) {
                $clearance_days = settings::getInterswitchClearanceDays();
                if ($clearance_days == 1) {
                    $clearance_days .= " day.";
                } else {
                    $clearance_days .= " days.";
                }
                $flashtype = 'notice';
                $flashMessage = 'Recharge Successful. The amount clearance will take ' . $clearance_days;
            } else {
                $flashtype = 'notice';
                $flashMessage = 'Recharge Successful';
            }
            $module = 'ewallet';
            $action = 'accountBalance';
        } else {
            $status = 'failure';
            if ($verification == "X00" || $verification == "X01" || $verification == "X02" || $verification == "X03" || $verification == "X04" || $verification == "X05") {
                $flashtype = 'error';
                $flashMessage = $paymentStatusMsg;
            } else {
                $flashtype = 'error';
                $flashMessage = "Your Transaction with Transaction Identification Number: $order_id was not successful <br>Reason: $paymentStatusMsg";
            }
            $module = 'recharge_ewallet';
            $action = 'recharge';
            $returnField1 = 'recharge_gateway';
            $returnVal1 = 'interswitch';
        }
        //setting values in renderText field
        $renderText = "<script>window.parent.location = '" . $this->generateUrl('default', array('module' => $module,
                    'action' => $action, $returnField1 => $returnVal1)) . "'</script>";

        return array('status' => $status, 'rspcode' => $rspcode, 'rspdesc' => $rspdesc, 'flashtype' => $flashtype, 'flashMessage' => $flashMessage, 'renderText' => $renderText);
    }

    //processing of response
    private function processresponse($txnId, $order_id, $type, $verification, $checkStatus, $paymentStatusMsg, $getData, $service_charge, $payment_mode_option_id, $transDate='', $pan='', $approvalcode='') {


//get the response lock
        $boollock = false;
        $arrayVal = array();
        $boollock = $this->getlockstatus($order_id);
        $gatewayOrderObj = new gatewayOrderManager();
        //if type is payment
        if (strtolower($type) == 'payment') {
            //if transaction is locked
            if ($boollock) {

                //if verification status is 00 and status of interswitch is failurer or Pending
                if ($verification['respCode'] == '00' && (strtolower($checkStatus) == 'failure' || $checkStatus == 'pending')) {

                    $paymentObj = new Payment($txnId);
                    $arrayVal = $paymentObj->proceedToPay();

                    $pfmTransactionDetails = $gatewayOrderObj->getValidationNumber($arrayVal['var_value']);
                    $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
                    $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $order_id);
                    //send mail to user
                    //                if($arrayVal['comments'] != 'error'){
                    //                    $this->addJobForMail($arrayVal['var_value'],$type, $order_id);
                    //                }
                }
            }
            //get payment response message and redirection with values to update gateway order
            $arrResponseText = $this->getPaymentResponseMessages($txnId, $order_id, $type, $verification['respCode'], $checkStatus, $paymentStatusMsg, $getData, $arrayVal);
            //if type is recharge
        } else if (strtolower($type) == 'recharge') {
            //total amount paid
            $total_amount_paid = $verification['amount'];
            //initialize validation no
            $validationNo = "";
            //check locked or not
            if ($boollock) {
                //if locked and verification status is 00 and status of interswitch in table is failurer or Pending
                if ($verification['respCode'] == '00' && (strtolower($checkStatus) == 'failure' || strtolower($checkStatus) == 'pending')) {
                    //rechareg account and return validation number
                    $validationNo = $this->recharge($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);

                    //update ValidationNo on Gateway Order
                    $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $order_id);
                }
            }
            //if  $verification status is 00
            if ($verification['respCode'] == '00') {
                //if clearence days satisfys or $checkStatus is failure or pending
                if ((!settings::getInterswitchClearanceDays()) || (strtolower($checkStatus) == 'failure' || strtolower($checkStatus) == 'pending')) {
                    $this->addJobForMail($txnId, $type, $order_id);
                }
            }
            //get recharge response message and redirection with values to update gateway order
            $arrResponseText = $this->getRechargeResponseMessages($txnId, $order_id, $type, $verification['respCode'], $checkStatus, $paymentStatusMsg, $getData, $validationNo);
        } else {
            return false;
        }
        //assign values in getdata to update Gateway order
        $getData['status'] = $arrResponseText['status'];
        $getData['rspcode'] = $arrResponseText['rspcode'];
        $getData['rspdesc'] = $arrResponseText['rspdesc'];
        $getData['date'] = $transDate;
//        if ($arrResponseText['rspcode'] == '00' && $pan != "") {
//            $getData['pan'] = $pan;
//            $getData['approvalcode'] = $approvalcode;
//        }
        if ($arrResponseText['rspcode'] == '00') {
            $getData['pan'] = $pan;
            $getData['approvalcode'] = $approvalcode;
        }
        //update Gateway Order Details
        $this->updateGatewayOrderDetails($boollock, $verification['respCode'], $checkStatus, $getData);
        //get messages and render text
        return array('flashtype' => $arrResponseText['flashtype'], 'flashMessage' => $arrResponseText['flashMessage'], 'renderText' => $arrResponseText['renderText']);
    }

    //verify amount and set if $verification is W56 or W17 set as W17
    private function getAmountVerificationStatus($verification) {
        $successCodes = array('00', '09', '16', '32', '94');
        if (!in_array($verification['respCode'], $successCodes)) {

            $logContent = "Transaction Identification number=" . $verification['txnref'] . "\r\n";
            $logContent.="Response Code=" . $verification['respCode'] . "\r\n";
            $logContent.="Response Description=" . $verification['desc'] . "\r\n";
            $logContent.="DateTime=" . $verification['transDate'];
            $this->createErrorLog($logContent, $verification['txnref']);
//            if (($verification['respCode'] == "W56") && ($rspcode == "W17")) {
//                $verification['respCode'] = "W17";
//            }
            return $verification;
        }
        return $verification;
    }

    protected function getByOrderId($order_id) {
        $orderArr = Doctrine::getTable('GatewayOrder')->getByOrderId($order_id);
        return $orderArr;
    }

    protected function validateTransactionNumber($transNo, $paymentModeOption) {
        return $merchantRecord = Doctrine::getTable('Transaction')->validateTransactionNumber($transNo, $paymentModeOption);
    }

    protected function validateTransactionNumberForInterswitch($transNo, $paymentModeOption) {
        return $merchantRecord = Doctrine::getTable('Transaction')->validateTransactionNumberForInterswitch($transNo, $paymentModeOption);
    }

    protected function getAmount($transNo) {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $formData = $payForMeObj->getTransactionRecord($transNo);
        return $formData['total_amount'];
    }

    protected function isFirstRequest($trnxId) {
        return $isFirstRequest = Doctrine::getTable('EpInterswitchRequest')->isFirstRequest($trnxId);
    }

    public function addJobForMail($txnId, $type='', $order_id='') {

        $notificationUrl = 'email/sendInterSwitchRechargeMail';

        $taskId = EpjobsContext::getInstance()->addJob('MailNotification', $notificationUrl, array('p4m_transaction_id' => $txnId, 'type' => $type, 'order_id' => $order_id));

        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
    }

    /* public function executeSendMail(sfWebRequest $request) {
      $this->setLayout(null);
      $mailingClass = new Mailing();
      //$type= "recharge";
      $type = $request->getParameter('type');
      $mailingOptions = array();
      $mailInfo = 'Mail is sent successfully';
      $txnId = $request->getParameter('p4m_transaction_id');

      if($type!='recharge'){
      $partialName = 'mailTemplate';
      $partialVars = pfmHelper::getMailData($txnId);

      }else{
      $partialName = 'mailrechargeTemplate';
      $order_id = $request->getParameter('order_id');
      $partialVars = pfmHelper::getMailDataRecharge($txnId,$order_id);
      }

      if(count($partialVars))
      {
      $mailingOptions = $partialVars['mailingOptions'];
      $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
      return $this->renderText($mailInfo);
      }

      }
     * */

    public function executePayOnline(sfWebRequest $request) {
        $txn_no = base64_decode($request->getParameter('returnVal'));

        if ($request->hasParameter('duplicate_payment_receipt')) {
            $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
        }

        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);

        $this->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
        $this->forward('paymentSystem', 'payOnline');
    }

    protected function convertDate($msgdate) {
        /* list ($date, $time) = explode(' ', $msgdate);
          list ( $mm, $dd, $yyyy) = explode('/', $date);
          $newDate = $yyyy.'-'.$mm.'-'.$dd.' '.$time; */
        $date4Db = date('Y-m-d H:i:s', strtotime($msgdate));

        return $date4Db;
    }

    public function executeVerifyApp(sfWebRequest $request) {
        $this->verifyPayment('CADP628051', 'DBP063000000020', '132991887066149', '52500');
        die;
    }

    private function verifyPayment($txRef, $amt) {
        //$epInterswitchManager = new EpInterswitchManager();
        $webpay_inf = new webpay_inf();
        $requestXML = $webpay_inf->getRequestXML($txRef);
        $this->logResponse($requestXML, 'requestXML-' . $txRef);

        $status = $webpay_inf->getStatusMsg($txRef);
        $this->logResponse($status, 'verifyResponse-' . $txRef);
        $webpay_response = $webpay_inf->getTransResponseCode($status);
        // echo $webpay_response;
        $amt_conf = $webpay_inf->getTransResponseAmount($status);
        $transactionDate = "";
        $pan = "";
        $paytxncode = "";
        if ($webpay_response == "00") {
            $transactionDate = $webpay_inf->getTransactionDate($status);
            //   $pan = $webpay_inf->getPan($status);
            //  $paytxncode = $webpay_inf->getPayTxnCode($status);
        }

        $transactionDate = urldecode(html_entity_decode($transactionDate));  // For date format

        if ($transactionDate)
            $transactionDate = $this->convertDate($transactionDate);
        else
            $transactionDate = date('Y-m-d H:i:s');

        if ($webpay_response == "00" && $amt_conf != $amt) {
            $response = 'F00';
            $webpay_response = $response;
        }
        if ($webpay_response == "00" && $amt_conf == $amt) {
            $response = $webpay_response;
        } else {
            $response = $webpay_response;
        }

//        if ($pan != "" && $pan != false) {
//            $responseArr['respPan'] = $pan;
//        } else {
//            $responseArr['respPan'] = "";
//        }
        $responseArr['txnref'] = $txRef;
        $responseArr['approvalCode'] = $response;
        $responseArr['amount'] = $amt_conf;
        $responseArr['respCode'] = $response;
        $responseArr['desc'] = $webpay_inf->getTransResponseDescription($status);
        $responseArr['transDate'] = $transactionDate;
        $responseArr['cardNum'] = $webpay_inf->getTransResponseCardNum($status);
        $responseArr['payRef'] = $webpay_inf->getTransResponsePayRef($status);
        $responseArr['retRef'] = $webpay_inf->getTransResponseRetRef($status);


        $epInterswitchManager = new EpInterswitchManager();
        //save the response data
        $saveResponse = $epInterswitchManager->saveResponse($responseArr);
        return $responseArr;
    }

    private function recharge($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {

        if (settings::getInterswitchClearanceDays()) {
            return $this->rechargeLater($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);
        } else {
            return $this->rechargeImmediate($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge);
        }
    }

    private function rechargeLater($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {
        $con = Doctrine_Manager::connection();
        try {
            $con->beginTransaction();

            sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
            $days = settings::getInterswitchClearanceDays();
            $recharge_date = getRechargeDate($days);
            $reachargeObj = rechargeAmountAvailabilityServiceFactory::getService();
            $recharge_id = $reachargeObj->createNew($total_amount_paid, $total_amount_paid, $service_charge, $txnId, $recharge_date, $payment_mode_option_id);

            //create a new task
            $taskId = EpjobsContext::getInstance()->addJob('RechargeEwallet', 'recharge_ewallet/rechargeViaCard', array('recharge_id' => $recharge_id), -1, NULL, $recharge_date);
            sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");



            //create credit , unclear, direct entries for collection_acct, eWallet_user_acct and  pay4me collection account
            $userObj = $this->getUser();
            $ewallet_account_id = $userObj->getUserAccountId();
            $ewallet_number = $userObj->getUserAccountNumber();
            $accountingObj = new pay4meAccounting;
            $is_clear = 0;
            $accountingArr = $accountingObj->getRechargeAccounts($ewallet_number, $ewallet_account_id, $total_amount_paid, $payment_mode_option_id, $is_clear, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT, $service_charge);
            if (is_array($accountingArr)) {

                //auditing that ewallet has been charged
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERSWITCH;
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERSWITCH,
                                EpAuditEvent::getFomattedMessage($msg, array('account_number' => $ewallet_number)));

                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

                $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));
            }
            $con->commit();
            $clearance_days = settings::getInterswitchClearanceDays();
            if ($clearance_days == 1) {
                $clearance_days .= " day.";
            } else {
                $clearance_days .= " days.";
            }
            $this->getUser()->setFlash('notice', 'Recharge Successful. The amount clearance will take ' . $clearance_days, true);
            return $event->getReturnValue();

            /* return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'ewallet',
              'action' => 'accountBalance'))."'</script>"); */
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    private function rechargeImmediate($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {
        $con = Doctrine_Manager::connection();
        try {
            $con->beginTransaction();
            /*      $userObj = $this->getUser();
              $ewallet_account_id =  $userObj->getUserAccountId();
              $ewallet_number = $userObj->getUserAccountNumber(); */

            $ewallet_account_id = $txnId;
            //alletObj = new EpAccountingManager;
            $accountDetails = EpAccountingManager::getAccount($txnId);
            $ewallet_number = $accountDetails->getAccountNumber();


            $accountingObj = new pay4meAccounting;
            $is_clear = 1;
            $accountingArr = $accountingObj->getRechargeAccounts($ewallet_number, $ewallet_account_id, $total_amount_paid, $payment_mode_option_id, $is_clear, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT, $service_charge);
            if (is_array($accountingArr)) {

                //auditing that ewallet has been charged
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERSWITCH;
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERSWITCH,
                                EpAuditEvent::getFomattedMessage($msg, array('account_number' => $ewallet_number)));

                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

                $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));

                $collection_account_id = $accountingObj->getCollectionAccount($payment_mode_option_id, NULL, NULL, 'Recharge');
                $managerObj = new payForMeManager();
                $managerObj->updateEwalletAccount($collection_account_id, $txnId, $total_amount_paid, 'credit', $service_charge);
            }
            $con->commit();
            $this->getUser()->setFlash('notice', 'Recharge Successful', true);
            return $event->getReturnValue();
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    private function logResponse($getData, $nameForm='') {
        $pay4meLog = new pay4meLog();
        if ($nameForm == '')
            $nameFormate = 'response';
        else
            $nameFormate = $nameForm;
        $pay4meLog->createLogData($getData, $nameFormate, 'interswitchLog');
    }

    protected function updateGatewayOrder($response) {

        $updateParamArr = array();

        $updateParamArr['status'] = $response['status'];
        $updateParamArr['amount'] = $response['apprAmt'];

        $updateParamArr['code'] = $response['rspcode'];
        $updateParamArr['desc'] = $response['rspdesc'];
        if (key_exists('pan', $response)) {
            $updateParamArr['pan'] = $response['pan'];
        }
        if (key_exists('approvalcode', $response)) {
            if ($response['approvalcode'] != "") {
                $updateParamArr['approvalcode'] = $response['approvalcode'];
            }
        }

        if (key_exists('date', $response))
            $updateParamArr['date'] = $response['date'];

        $updateParamArr['order_id'] = $response['txnref'];
        $convertdate = false;
        $updated = Doctrine::getTable('GatewayOrder')->updateGatewayOrder($updateParamArr, $convertdate);
        return $updated;
    }

    function createErrorLog($logContent, $transaction_identification_number) {
        $pay4meLog = new pay4meLog();

        $nameFormate = 'error-' . $transaction_identification_number;

        $pay4meLog->createLogData($logContent, $nameFormate, 'interswitchErrorLog');
    }

    function getLock($order_id) {
        return $getLock = Doctrine::getTable('GatewayOrder')->getLock($order_id);
    }

    private function checkpaymentquerydetails($txnId, $paymentModeOption) {
        //validate transaction number if count >0 returns array else returns 0
        $validateTrans = $this->validateTransactionNumber($txnId, $paymentModeOption);
        //if not equal to zero
        if ($validateTrans) {
            //get item id from
            $item_id = $validateTrans['MerchantRequest']['merchant_item_id'];
            //get merchant request payment status
            $payment_status = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
            //if payment status if it is zero payment done else payment not done
            //if payment already done update accordingly
            if ($payment_status == 0) {
                $merchant_obj = Doctrine::getTable('MerchantRequest')->getMerchantPaidId($item_id);
                $merchant_request_payment_id = $merchant_obj->getFirst()->getId();
                $transaction_number_payment_id = $merchant_obj->getFirst()->getTransaction()->getFirst()->getTransactionNumber();
                $this->getRequest()->setParameter('txn_no', $transaction_number_payment_id);
                $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
                $this->forward('paymentSystem', 'payOnline');
            }
        } else {
            $this->getUser()->setFlash('error', 'No Transaction Record', false);
        }
    }

    //query interswitchstatus
    public function executeInterswitchQueryStatus(sfWebRequest $request) {
        $this->show = false;
        $this->setLayout('layout_popup');
        $getData = array();
        $txRef = $request->getParameter('orderId');
        $txnId = $request->getParameter('appId');
        //get Gateway order details
        $paymentModObj = Doctrine::getTable('GatewayOrder')->getByOrderId($txRef);
        $paymentModeOption = $paymentModObj['payment_mode_option_id'];
        $type = $paymentModObj['type'];
        $status = $paymentModObj['status'];
        $paymentmode = $paymentModeOption;
        $service_charge = $paymentModObj['servicecharge_kobo'];
        //[WP31]
        //initialization of reference number
        $intRefNo = '';
        if ($type == "payment") {
            $intRefNo = $txnId;
        } else {
            $intRefNo = $txRef;
        }

        if ($status != "success") {
            if ($type == "payment") {
                //update payment related specific querys
                $this->checkpaymentquerydetails($txnId, $paymentModeOption);
            }

            //create EpInterswitchManager object
            $epInterswitchManager = new EpInterswitchManager();
            //get all details from EpInterswitchRequest
            $getResponse = $epInterswitchManager->selectAllFromRequest($txRef);
            //if any records presents
            if (count($getResponse)) {
                //get merchant id
                if (array_key_exists('mert_id', $getResponse[0])) {
                    $mert_id = $getResponse[0]['mert_id'];
                    $cadp_id = $getResponse[0]['cadp_id'];
                }
//                $mert_id = $getResponse[0]['mert_id'];
//                $cadp_id = $getResponse[0]['cadp_id'];
                //get amount in kobo
                $amt = $getResponse[0]['amount_kobo'];
                //Check response has valid amount or not

                $verification = $this->verifyPayment($txRef, $amt);

                //get RESPONSE  Status Message
                $paymentStatusMsg = GatewayStatusMessages::getInterswitchStatusMessage($verification['respCode']);
                //save the response
//                $epInterswitchManager->saveForPending($getResponse[0]['id'], $verification['respCode'], $amt, $paymentStatusMsg);
                //set values to getData variable
                $getData['txnref'] = $txRef;
                $getData['apprAmt'] = $amt;
                $getData['rspcode'] = $verification['respCode'];
                $getData['rspdesc'] = $paymentStatusMsg;
                $getData['date'] = $verification['transDate'];
                //if verification is sucess
                if ($verification['respCode'] == '00') {
                    $getData['status'] = 'success';
                    //  $getData['pan'] = $verification['respPan'];
                    $getData['approvalcode'] = $verification['approvalCode'];
                    $getData['rspdesc'] = 'Approved by Financial Institution';
                    //update Gateway Order

                    $updated = $this->updateGatewayOrder($getData);

                    //if type is payment
                    if ($type == "payment") {
                     $paymentObj = new Payment($txnId);
                        $arrayVal = $paymentObj->proceedToPay();
                        $gatewayOrderObj = new gatewayOrderManager();

                        $pfmTransactionDetails = $gatewayOrderObj->getValidationNumber($arrayVal['var_value']);
                        $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
                       $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $txRef);


                        $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], true);
                        if ($arrayVal['comments'] == 'error') {
                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                        'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $txnId, 'type' => $type, 'pending' => 'all')) . "'</script>");
                        } else {
                            //$this->addJobForMail($arrayVal['var_value'],$type, $order_id);

                            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                        'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $txnId, 'type' => $type, 'pending' => 'all')) . "'</script>");
                        }
                    } else {

                        //if type is recharge
                        //paymentmode option = 2 and service charges need to capture from top
                        $validationNo = $this->recharge($txnId, $amt, '2', $service_charge);
                        $gatewayOrderObj = new gatewayOrderManager();
                        $gatewayOrderObj->updateGatewayOrderValidationNo($validationNo, $txRef);
                        $this->addJobForMail($txnId, $type, $txRef);
                        return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                    'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $txRef, 'type' => $type, 'pending' => 'all')) . "'</script>");
                    }
                    //if transaction is not sucessfull
                } else {
                    //update gateway order table
                    $getData['status'] = 'failure';
                    $updated = $this->updateGatewayOrder($getData);
                    $this->getUser()->setFlash('error', $paymentStatusMsg, false);
                    return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                                'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $intRefNo, 'type' => $type, 'pending' => 'all')) . "'</script>");
                }
                //if no request detail
            } else {
                $this->getUser()->setFlash('error', 'No Transaction Record', false);
            }
        } else {
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'ewallet',
                        'action' => 'viewDetail', 'query' => true, 'paymentmode' => $paymentmode, 'transId' => $intRefNo, 'type' => $type, 'pending' => 'all')) . "'</script>");
        }
    }

    /* public function PayOnline($transNumber, $isDuplicate=false) {

      $this->payment_receipt_header = "Payment Receipt";
      if($isDuplicate) {
      $this->payment_receipt_header = "Duplicate ".$this->payment_receipt_header;
      }

      $txn_no = $transNumber;
      $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
      $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);

      //get Param Description; as per Merchant Service
      $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
      $this->MerchantData = $ValidationRulesObj->getValidationRules($pfmTransactionDetails['merchant_service_id']);

      //$payForMeNisManagerObj = payForMeNisServiceFactory::getService('nis');
      $postDataArray['txnId'] = $pfmTransactionDetails['pfm_transaction_number'];
      if($this->MerchantData) {
      foreach($this->MerchantData as $key=>$value) {
      $postDataArray[$key] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails'][$key];
      }

      }

      $postDataArray['type'] = "";
      $postDataArray['name'] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails']  ['name'];
      $postDataArray['appCharge'] = $pfmTransactionDetails['MerchantRequest']['item_fee'];
      $postDataArray['bankCharge'] = $pfmTransactionDetails['MerchantRequest']['bank_charge'];
      $postDataArray['serviceCharge'] = $pfmTransactionDetails['MerchantRequest']['service_charge'];
      $postDataArray['totalCharge'] = $pfmTransactionDetails['total_amount'];
      $postDataArray['serviceType'] = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
      $postDataArray['validation_number'] = $pfmTransactionDetails['MerchantRequest']['validation_number'];
      $postDataArray['payment_date'] = $pfmTransactionDetails['MerchantRequest']['paid_date'];
      $postDataArray['payment_status_code'] = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];
      $postDataArray['currency_id'] = $pfmTransactionDetails['MerchantRequest']['currency_id'];
      $this->isClubbed = $pfmTransactionDetails['MerchantRequest']['MerchantService']['clubbed'];

      $this->postDataArray = $postDataArray;
      $this->setTemplateForReceipt();
      }

      public function setTemplateForReceipt() {
      $this->show = true;
      $billObj = billServiceFactory::getService();
      $walletDetails = $billObj->getEwalletAccountValuesByTxn($this->postDataArray['txnId']);
      $this->errorForEwalletAcount = '';
      if($walletDetails==false) {
      $this->errorForEwalletAcount = "Invalid Account Number";
      }
      else {
      $this->accountObj = $walletDetails->getFirst();
      $this->userDetailObj = $walletDetails->getFirst()->getUserDetail()->getFirst();
      }
      $this->setTemplate('interswitchQueryStatus');
      } */
}
