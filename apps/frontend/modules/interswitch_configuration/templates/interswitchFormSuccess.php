<div align="center" class="wrapForm2" id="wrapForm2"><br/>
    <div id="notice"><span class="cRed">NOTICE:</span> Your Transaction Identification Number is <b><?php echo $retObj->getTrnxId(); ?></b>.<br/>Please quote this Transaction Identification Number for your reference.
        <br/><b>Do not click the Refresh or Back button while your transaction is  Processing.</b>
        <div id="viewlink" class="cRed"></div>
    </div><br/>
</div>
<div id="result_div" style="display:none;"></div>
<div  id="error_notice" style="text-align:center;font-weight:bold;">
    <div id="div_error"  class="error"></div>
</div>
<div align="center" title="Secure Payment Page" id="frame">
    <iframe id="PayFrame" name="PayFrame" width="100%" height="720px" frameborder="0" ></iframe>
</div>
<form name="form1" method="post" action="verify.php">
    <input name="txnref" type="hidden" id="txnref" value="<?php echo $retObj->getTrnxId(); ?>">
    <input name="amount" type="hidden" id="amount" value="<?php echo $retObj->getAmountKobo(); ?>">
</form>

<script type="text/javascript">
    function doPayment() 
    {

        document.getElementById("PayFrame").src ='<?php echo url_for($request_url . "?id=" . $retObj->getId() . "&payItemId=" . $payItemId . "&payItemName=" . $payItemName) ?>';

    }
    function checkstatuspage(){
        //call for a web service call and update status accordingly
        var data = '<b>if you have entered your card details below and have not received any response for the transaction, please <?php echo link_to("Click here", "interswitch_configuration/InterswitchStatus?appId=" . $transNo . "&orderId=" . $retObj->getTrnxId() . "&type=" . $type, array("onclick" => "return checkstatus()")); ?> to check the status.</b>';
        var divdata = data;
        $("#viewlink").html(divdata);        
    }
    function checkstatus(){
        var answer = confirm("Are you sure you want to Proceed?.")
        if(answer){
            return true;
        }
        return false;
        
    }
    function pagetimeout(){
        setTimeout("checkstatuspage()",<?php echo $time; ?>)
    }
    $(document).ready(function(){
        doPayment();

    });
</script>
