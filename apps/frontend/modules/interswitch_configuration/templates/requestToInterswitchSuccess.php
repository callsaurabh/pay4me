<html>
    <head><title>Interswitch Verve/Master Card Naira</title></head>
    <body>
        <form name="form" id="interswitch_form" action="<?php echo $retObj->getPostUrl(); ?>"
              method="post">
            <input name="product_id" type="hidden" value="<?php echo $productId; ?>" />

            <input name="amount" type="hidden" value="<?php echo $retObj->getAmountKobo(); ?>" />

            <input name="currency" type="hidden" value="<?php echo $retObj->getCurrency(); ?>" />

            <input name="site_redirect_url" type="hidden" value="<?php echo $site_redirect_url; ?>" />

            <input name="site_name" type="hidden" value="<?php echo $site_name; ?>" />

            <input name="cust_id" type="hidden" value="<?php echo $retObj->getCustId(); ?>" />

            <input name="cust_id_desc" type="hidden" value="<?php echo $retObj->getCustIdDescription(); ?>" />

            <input name="cust_name" type="hidden" value="<?php echo $retObj->getCustName(); ?>" />

            <input name="cust_name_desc" type="hidden" value="<?php echo $retObj->getCustNameDescription(); ?>" />

            <input name="txn_ref" type="hidden" value="<?php echo $retObj->getTrnxId(); ?>" />

            <input name="pay_item_id" type="hidden" value="<?php echo $payItemId; ?>" />

            <input name="pay_item_name" type="hidden" value="<?php echo $payItem_name; ?>" />

            <input name="local_date_time" type="hidden" value="<?php echo date("d-M-y H:i:s"); ?>" />

        </form>
        <script type="text/javascript">
 
            document.forms.form.submit();
        </script>

