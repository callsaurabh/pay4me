<b>Hello <?php echo ucwords($firstname."  ".$lastname); ?>,</b>
<br /><br />Thanks for making Payment for your application using <b>Pay4Me!<br /><br /></b>

<?php $rDetails = unserialize(base64_decode($request_details));?>

<table width="100%" cellpadding="3" cellspacing="0">
   <tr>
    <td colspan="3">
    <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

        <tr>
            <td style="font-size: 83%;">&nbsp;Payment date: <b><?php echo date('F d, Y, h:i:s a', strtotime($rDetails['0']));?></b>
            <br>&nbsp;Payment Validation Number: <b><?php  echo $rDetails[4];?></b></td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
        </tbody></table>
    </td>
  </tr>

  <tr  style="color:#00345A;background-color:#E7F0F7;">
    <td width="25%" ><b>Payment Status</b></td>
    <td width="77%"><b>Item</b></td>
    <td width="23%" align="right"><b>Price</b></td>
  </tr>
  <tr>

    <td ><span >Successful </span></td>
    <td ><span ><?php  echo $rDetails[1];?></span></td>
    <td align="right" ><?php echo format_amount($rDetails[2],$rDetails[3]);?></td>
  </tr>
  <tr style="color:#00345A;background-color:#E7F0F7;font-size:18px;">

    <td >&nbsp;</td>
    <td  align="right" >Total:</td>
    <td align="right"><?php echo format_amount($rDetails[2],$rDetails[3]); ?></td>
  </tr>
 <!-- <tr class="blTxt">
    <td colspan="3" class="txtBold" align="left"><b>Paid with :</b><?php  //echo $rDetails[3]; ?></td>
  </tr>-->
</table>

<br><br>
Please look out for our newsletter in your email informing you of ways to utilize your account. <br /><br />PAY4ME.....KEEPING IT SIMPLE"<br /><br />

<?php echo $signature;?>


