<?php

/**
 * nibss actions.
 *
 * @package    mysfp
 * @subpackage nibss
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class nibssActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
    	sfContext::getInstance()->getResponse()->setContentType('text/xml');
        $xmldata = file_get_contents('php://input');
        $file_name = $this->createLog($xmldata, 'nibss_request');
        //
        $file_name_without_path = explode('/', $file_name);
        $length = count($file_name_without_path);
        $fileName = explode('-', $file_name_without_path[$length - 1]);
        $filelength = count($fileName);
        $logPath = sfConfig::get('sf_log_dir');
        $newfilePath = $logPath . '/' . sfConfig::get('app_internet_bank_log_directory') . '/' . date('Y-m-d') . '/';

        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        sfContext::getInstance()->getResponse()->setContentType('text/xml');
        if ($xdoc->getElementsByTagName('eWalletValidationRequest')->length > 0) {
            rename($file_name, $newfilePath . 'recharge_validation_request-' . $fileName[$filelength - 1]);
            $this->forward('nibss', 'rechargeValidation');
        } else if ($xdoc->getElementsByTagName('CollectionValidationRequest')->length > 0) {
            rename($file_name, $newfilePath . 'payment_validation_request-' . $fileName[$filelength - 1]);
            $this->forward('nibss', 'paymentValidation');
        } else if ($xdoc->getElementsByTagName('CollectionPaymentConfirmationRequest')->length > 0) {
            rename($file_name, $newfilePath . 'payment_confirmation_request-' . $fileName[$filelength - 1]);
            $this->forward('nibss', 'paymentConfirmation');
        } else if ($xdoc->getElementsByTagName('eWalletPaymentConfirmationRequest')->length > 0) {
            rename($file_name, $newfilePath . 'recharge_confirmation_request-' . $fileName[$filelength - 1]);
            $this->forward('nibss', 'rechargeConfirmation');
        } else {
            $text = "XML is not in valid format";
            $NibssObj = new EpNibssManager();
            $Error = $NibssObj->setErrorXML(EpNibssResponseManager::$RESPONSE_FORMAT_ERROR, $text);
            $this->createLog($Error, 'Invalid_XML_Format_error');
            return $this->renderText($Error);
        }
    }

   public function executePaymentValidation(sfWebRequest $request) {
        try { 
            $this->setTemplate(NULL);
            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $xmldata = file_get_contents('php://input');
            $xdoc = new DomDocument;
            $xdoc->LoadXML($xmldata);
            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $payment_validation_request = $xdoc->getElementsByTagName('CollectionValidationRequest');

            $NibssObj = new EpNibssManager();
            $query_req_arr = $NibssObj->fetchQueryRequestPayment($payment_validation_request);
            $query_req_arr['request_type'] = "payment";
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $pfmTransactionDetails = $payForMeObj->getTransactionRecord($query_req_arr['tran_num']);
            $responseArr = $this->validateTransaction($pfmTransactionDetails); //  to validate transaction number and payment mode

            if (!$responseArr['1']) {
                $pfmTransactionDetails = array();
            }
            //echo "<pre/>";
            //print_r($responseArr);die;
            //response_xml
            $paymentValidationResponse = $NibssObj->setQueryResponseXMLPayment($pfmTransactionDetails, $query_req_arr, $responseArr);
            $this->createLog($paymentValidationResponse, 'payment_validation_response');

            return $this->renderText($paymentValidationResponse);
        } catch (Exception $e) {
            $text = "Found an error" . $e->getMessage();
            $Error = $NibssObj->setErrorXML(EpNibssResponseManager::$RESPONSE_DO_NOT_HONOR, $text);
            $this->createLog($Error, 'payment_validation_response_error');
            return $this->renderText($Error);
        }
    }

    public function executePaymentConfirmation(sfWebRequest $request) {
        try {
            $this->setTemplate(false);
            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $xmldata = file_get_contents('php://input');
            $xdoc = new DomDocument;
            $xdoc->LoadXML($xmldata);
            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $payment_confirmation_request = $xdoc->getElementsByTagName('CollectionPaymentConfirmationRequest');
            $NibssObj = new EpNibssManager();
            $query_req_arr = $NibssObj->fetchConfirmationRequestPayment($payment_confirmation_request);
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $pfmTransactionDetails = $payForMeObj->getTransactionRecord($query_req_arr['tran_num']);
			$query_req_arr['app_type'] = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
            //$validationId = $this->payValidate($query_req_arr['tran_num']); 
          	$ValNumtoGen = $NibssObj->ValidateDetails($pfmTransactionDetails,$query_req_arr); // validating req details, such as service type, amounts
            switch ($ValNumtoGen) {
                case EpNibssResponseManager::$RESPONSE_APPROVED :
                    $Response = $this->ValidationNumberGen($query_req_arr);
                    break;
                default:
                    $Error = $NibssObj->setErrorXML($ValNumtoGen['ResponseCode'], $ValNumtoGen['ResponseText']);
                    $this->createLog($Error, 'payment_confirmation_response_error');
                    return $this->renderText($Error);
                    break;
            }
            //Done
           // $Response['Validation_num'] = '1234567';
            if ($Response) {
                $query_req_arr['validation_no'] = $Response['validationNumber'] ? $Response['validationNumber'] : $pfmTransactionDetails['MerchantRequest']['validation_number'];
                // response xml
                $paymentConfirmationResponse = $NibssObj->setConfirmationResponseXMLPayment($query_req_arr);

                $this->createLog($paymentConfirmationResponse, 'payment_confirmation_response');

                $query_req_arr['confirmation_request_id'] = $Response['confirmationReqId'];

                $confirmation_res_obj = $NibssObj->setNibssConfirmationResponse($query_req_arr);
                return $this->renderText($paymentConfirmationResponse);
            } else {
                $text = "Payment Request is not correct";
                $Error = $NibssObj->setErrorXML($Response['ResponseCode'], $Response['ResponseText']);
                $this->createLog($Error, 'payment_confirmation_response_error');
                return $this->renderText($Error);
            }
        } catch (Exception $e) {
            $text = "Found an error" . $e->getMessage();
            $Error = $NibssObj->setErrorXML(EpNibssResponseManager::$RESPONSE_DO_NOT_HONOR, $text);
            $this->createLog($Error, 'payment_confirmation_response_error');
            return $this->renderText($Error);
        }
    }
    
    public function createLog($xmldata, $type) {
        $nameFormate = $type;
        $pay4meLog = new pay4meLog();
        $pay4meLog->createLogData($xmldata, $nameFormate, 'nibsslog');
        return $filename = $pay4meLog->getFileName();
    }

    public function executeRechargeValidation(sfWebRequest $request) { 
        try {
            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $xmldata = file_get_contents('php://input');
            $xdoc = new DomDocument;
            $xdoc->LoadXML($xmldata);

            $ewallet_validation_request = $xdoc->getElementsByTagName('eWalletValidationRequest');
            $epNibssManagerObj = new EpNibssManager();
            $query_req_arr = $epNibssManagerObj->fetchQueryRequest($ewallet_validation_request);

            $resp_code = '';      
            if ($query_req_arr['ewalletAccountNumber'] != '') {
                $billObj = billServiceFactory::getService();
                $ewalletDetails = $billObj->getEwalletDetails($query_req_arr['ewalletAccountNumber']);
                
                $err = 0;
                if (!$ewalletDetails) {
                    $resp_code = EpNibssResponseManager::$RESPONSE_INVALID_ACCOUNT;
                    $err = $err + 1;
                } else {
                    $resp_code = EpNibssResponseManager::$RESPONSE_APPROVED;
                    $acc_name = $ewalletDetails->getFirst()->getAccountName();
                    $query_req_arr['address'] = $ewalletDetails->getFirst()->getUserDetail()->getFirst()->getAddress();
                    $query_req_arr['email'] = $ewalletDetails->getFirst()->getUserDetail()->getFirst()->getEmail();
                    $query_req_arr['mobileNumber'] = $ewalletDetails->getFirst()->getUserDetail()->getFirst()->getMobileNo();
                }
            } else {
                $resp_code = EpNibssResponseManager::$RESPONSE_INVALID_ACCOUNT;
                $err = $err + 1;
            }
            if (!$err) {
                if (!preg_match("/^[0-9.]+$/", $query_req_arr['amount']) || $query_req_arr['amount'] > '1000000000') {
                    $resp_code = EpNibssResponseManager::$RESPONSE_INVALID_AMOUNT;
                } else {
                    $resp_code = EpNibssResponseManager::$RESPONSE_APPROVED;
                }
            }
            $query_req_arr['accountName'] = $acc_name;
            $query_req_arr['responseCode'] = $resp_code;
            $query_req_arr['serviceCharge'] = $query_req_arr['amount'] * sfConfig::get('app_nibss_svc_charge');
            $query_req_arr['totalAmount'] = $query_req_arr['amount'] + $query_req_arr['serviceCharge'];


//            $amt_arr = $epNibssManagerObj->calculateServiceCharge($query_req_arr['amt']);
//            $query_req_arr['service_charge'] = $epNibssManagerObj->numberFormat($amt_arr['service_charge']);
//            $query_req_arr['total_amt'] = $epNibssManagerObj->numberFormat($amt_arr['total_amt']);
            //response_xml
            $query_response_xml = $epNibssManagerObj->setQueryResponseXML($query_req_arr);
            $this->createLog($query_response_xml, 'recharge_validation_response');
            
            
            //$query_res_obj = $epNibssManagerObj->setQueryResponse();
            return $this->renderText($query_response_xml);
        } catch (Exception $e) {
            $text = "Found an error" . $e->getMessage();
            $Error = $epNibssManagerObj->setErrorXML(EpNibssResponseManager::$RESPONSE_DO_NOT_HONOR, $text);
            $this->createLog($Error, 'recharge_validation_response_error');
            return $this->renderText($Error);
        }
    }
	/*
    public function executeRechargeConfirmation(sfWebRequest $request) {
        try {
            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $xmldata = file_get_contents('php://input');
            $xdoc = new DomDocument;
            $xdoc->LoadXML($xmldata);
            
            $ewallet_validation_request = $xdoc->getElementsByTagName('eWalletPaymentConfirmationRequest');
            $epNibssManagerObj = new EpNibssManager();
            $confirmation_req_arr = $epNibssManagerObj->fetchConfirmationRequest($ewallet_validation_request);

            //validation            
            // not wriiten
            // ValidateDetails can be use but have to see it
            
            //$validation_resp = $epNibssManagerObj->ValidateRequest($confirmation_req_arr, 'recharge');
            
            
            
//            if ($validation_resp['ResponseCode'] == EpNibssResponseManager::$RESPONSE_APPROVED) {
                $pfm_obj = new pfmHelper();
                $nibss_pay_mode = $pfm_obj->getPMOIdByConfName(sfConfig::get('app_payment_mode_option_internet_bank'));


                //amt to be saved in DB should be in KOBO
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                $amount_credit = convertToKobo($confirmation_req_arr['amount']);
                $service_charges = convertToKobo($confirmation_req_arr['serviceCharge']);

                //generatie order_id
                $billObj = billServiceFactory::getService();
                $ewalletDetails = $billObj->getEwalletDetails($confirmation_req_arr['ewalletAccountNumber']);
                
                $confirmation_req_arr['order_id'] = $billObj->saveOrderIdInGatewayorder($ewalletDetails->getFirst()->getId(), 'recharge', $nibss_pay_mode,
                                NULL, 1, $amount_credit, $service_charges);

                $confirmation_req_arr['tran_num'] = NULL;
                $confirmation_req_arr['app_type'] = NULL;
                $confirmation_req_arr['app_charge'] = NULL;
                $confirmation_req_arr['tran_charge'] = NULL;
                $epNibssManagerObj = new EpNibssManager();
                $query_response_obj = $epNibssManagerObj->saveConfirmationRequest($confirmation_req_arr);
                $confirmation_req_arr['confirmation_request_id'] = $query_response_obj->getId();
                $validation_no = $this->verfiyAndUpdateByOrderId($confirmation_req_arr['order_id']);
                $confirmation_req_arr['validation_no'] = $validation_no['Validation_num'];
                // by vikash, in case validation number is not generated , to return error xml
		sfContext::getInstance()->getLogger()->info('Bank REPONSE XML:' . json_encode($confirmation_req_arr) . " : vNo : ". $validation_no);
                if ($confirmation_req_arr['validation_no']) {
                    //response xml
                    $confirmation_response_xml = $epNibssManagerObj->setConfirmationResponseXML($confirmation_req_arr);
                    $this->addJobForNibssRechageMail($confirmation_req_arr['validation_no']);
                    $this->createLog($confirmation_response_xml, 'recharge_confirmation_response');
                    $query_req_obj = $epNibssManagerObj->saveConfirmationResponse($confirmation_req_arr);
                    return $this->renderText($confirmation_response_xml);
                } else {
                    $text = "Recharge Request is not correct";
                    $Error = $epNibssManagerObj->setErrorXML(EpNibssResponseManager::$RESPONSE_SYSTEM_MALFUNCTION, $text);
                    $this->createLog($Error, 'recharge_confirmation_response_error');
                    return $this->renderText($Error);
                }
//            } else {
//                $Error = $epNibssManagerObj->setErrorXML($validation_resp['ResponseCode'], $validation_resp['ResponseText']);
//                $this->createLog($Error, 'recharge_confirmation_response_error');
//                return $this->renderText($Error);
//            }
        } catch (Exception $e) {
            $text = "Found an error" . $e->getMessage();
            $Error = $epNibssManagerObj->setErrorXML(EpNibssResponseManager::$RESPONSE_DO_NOT_HONOR, $text);
            $this->createLog($Error, 'recharge_confirmation_response_error');
            return $this->renderText($Error);
        }
    }
	*/

	/*
	public function executeRechargeConfirmation(sfWebRequest $request) {
        try {
		sfContext::getInstance()->getLogger()->info('Response from NIBSS....');

            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $xmldata = file_get_contents('php://input');
            $xdoc = new DomDocument;
            $xdoc->LoadXML($xmldata);
		 sfContext::getInstance()->getLogger()->info($xmldata);
		//eWalletPaymentConfirmationRequest
            $ewallet_validation_request = $xdoc->getElementsByTagName('eWalletPaymentConfirmationRequest');
		 sfContext::getInstance()->getLogger()->info($xdoc->getElementsByTagName('eWalletPaymentConfirmationRequest'));
		sfContext::getInstance()->getLogger()->info($ewallet_validation_request);
            $epNibssManagerObj = new EpNibssManager();
            $confirmation_req_arr = $epNibssManagerObj->fetchConfirmationRequest($ewallet_validation_request);
  		sfContext::getInstance()->getLogger()->info(json_encode($confirmation_req_arr));

            //validation
            //$validation_resp = $epNibssManagerObj->ValidateRequest($confirmation_req_arr, 'recharge');
            //if ($validation_resp['ResponseCode'] == EpNibssResponseManager::$RESPONSE_APPROVED) {
                $pfm_obj = new pfmHelper();
                $nibss_pay_mode = $pfm_obj->getPMOIdByConfName(sfConfig::get('app_payment_mode_option_internet_bank'));


                //amt to be saved in DB should be in KOBO
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                $amount_credit = convertToKobo($confirmation_req_arr['total_amount']);
                $service_charges = convertToKobo($confirmation_req_arr['service_charge']);

                //generatie order_id
                $billObj = billServiceFactory::getService();
                $ewalletDetails = $billObj->getEwalletDetails($confirmation_req_arr['ewalletNumber']);
                $confirmation_req_arr['order_id'] = $billObj->saveOrderIdInGatewayorder($ewalletDetails->getFirst()->getId(), 'recharge', $nibss_pay_mode,
                                NULL, 1, $amount_credit, $service_charges);

                $confirmation_req_arr['tran_num'] = NULL;
                $confirmation_req_arr['app_type'] = NULL;
                $confirmation_req_arr['app_charge'] = NULL;
                $confirmation_req_arr['tran_charge'] = NULL;
                $epNibssManagerObj = new EpNibssManager();
                $query_response_obj = $epNibssManagerObj->setConfirmationRequest($confirmation_req_arr);
                $confirmation_req_arr['confirmation_request_id'] = $query_response_obj->getId();
                $validation_no = $this->verfiyAndUpdateByOrderId($confirmation_req_arr['order_id']);
                $confirmation_req_arr['validation_no'] = $validation_no['Validation_num'];
                // by vikash, in case validation number is not generated , to return error xml
                if ($confirmation_req_arr['validation_no']) {
                    //response xml
                    $confirmation_response_xml = $epNibssManagerObj->setConfirmationResponseXML($confirmation_req_arr);
                    $this->addJobForNibssRechageMail($confirmation_req_arr['validation_no']);
                    $this->createLog($confirmation_response_xml, 'recharge_confirmation_response');
                    $query_req_obj = $epNibssManagerObj->setConfirmationResponse($confirmation_req_arr);
                    return $this->renderText($confirmation_response_xml);
                } else {
                    $text = "Recharge Request is not correct";
                    $Error = $epNibssManagerObj->setErrorXML(EpNibssResponseManager::$RESPONSE_SYSTEM_MALFUNCTION, $text);
                    $this->createLog($Error, 'recharge_confirmation_response_error');
                    return $this->renderText($Error);
                
		}
		/*
            } else {
                $Error = $epNibssManagerObj->setErrorXML($validation_resp['ResponseCode'], $validation_resp['ResponseText']);
                $this->createLog($Error, 'recharge_confirmation_response_error');
                return $this->renderText($Error);
            }
		*
        } catch (Exception $e) {
            $text = "Found an error" . $e->getMessage();
            $Error = $epNibssManagerObj->setErrorXML(EpNibssResponseManager::$RESPONSE_DO_NOT_HONOR, $text);
            $this->createLog($Error, 'recharge_confirmation_response_error');
            return $this->renderText($Error);
        }
    }
	*/

 public function executeRechargeConfirmation(sfWebRequest $request) {
        try {
//	 sfContext::getInstance()->getLogger()->info('NIBSS RESPONSE ......');
//	sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $xmldata = file_get_contents('php://input');
            $xdoc = new DomDocument;
            $xdoc->LoadXML($xmldata);
		sfContext::getInstance()->getResponse()->setContentType('text/xml');
        	sfContext::getInstance()->getLogger()->info($xmldata);    
            $ewallet_validation_request = $xdoc->getElementsByTagName('eWalletPaymentConfirmationRequest');
            $epNibssManagerObj = new EpNibssManager();
            $confirmation_req_arr = $epNibssManagerObj->fetchConfirmationRequest($ewallet_validation_request);
//		sfContext::getInstance()->getLogger()->info($ewallet_validation_request);
//		sfContext::getInstance()->getLogger()->info(json_encode($confirmation_req_arr));
            //validation            
            // not wriiten
            // ValidateDetails can be use but have to see it
            
            //$validation_resp = $epNibssManagerObj->ValidateRequest($confirmation_req_arr, 'recharge');
            
            
            
//            if ($validation_resp['ResponseCode'] == EpNibssResponseManager::$RESPONSE_APPROVED) {
                $pfm_obj = new pfmHelper();
                $nibss_pay_mode = $pfm_obj->getPMOIdByConfName(sfConfig::get('app_payment_mode_option_internet_bank'));


                //amt to be saved in DB should be in KOBO
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                $amount_credit = convertToKobo($confirmation_req_arr['total_amount']);
                $service_charges = convertToKobo($confirmation_req_arr['service_charge']);
                //generatie order_id
                $billObj = billServiceFactory::getService();
                $ewalletDetails = $billObj->getEwalletDetails($confirmation_req_arr['ewallet_aact_num']);
                
                $confirmation_req_arr['order_id'] = $billObj->saveOrderIdInGatewayorder($ewalletDetails->getFirst()->getId(), 'recharge', $nibss_pay_mode,
                                NULL, 1, $amount_credit, $service_charges);

                $confirmation_req_arr['tran_num'] = NULL;
                $confirmation_req_arr['app_type'] = NULL;
                $confirmation_req_arr['app_charge'] = NULL;
                $confirmation_req_arr['tran_charge'] = NULL;
                $epNibssManagerObj = new EpNibssManager();
                $query_response_obj = $epNibssManagerObj->saveConfirmationRequest($confirmation_req_arr);
                $confirmation_req_arr['confirmation_request_id'] = $query_response_obj->getId();
                $validation_no = $this->verfiyAndUpdateByOrderId($confirmation_req_arr['order_id']);
                $confirmation_req_arr['validation_no'] = $validation_no['Validation_num'];
                // by vikash, in case validation number is not generated , to return error xml
                if ($confirmation_req_arr['validation_no']) {
                    //response xml
                    $confirmation_response_xml = $epNibssManagerObj->setConfirmationResponseXML($confirmation_req_arr);
                    $this->addJobForNibssRechageMail($confirmation_req_arr['validation_no']);
                    $this->createLog($confirmation_response_xml, 'recharge_confirmation_response');
                    $query_req_obj = $epNibssManagerObj->saveConfirmationResponse($confirmation_req_arr);
                    return $this->renderText($confirmation_response_xml);
                } else {
                    $text = "Recharge Request is not correct";
                    $Error = $epNibssManagerObj->setErrorXML(EpNibssResponseManager::$RESPONSE_SYSTEM_MALFUNCTION, $text);
                    $this->createLog($Error, 'recharge_confirmation_response_error');
                    return $this->renderText($Error);
                }
//            } else {
//                $Error = $epNibssManagerObj->setErrorXML($validation_resp['ResponseCode'], $validation_resp['ResponseText']);
//                $this->createLog($Error, 'recharge_confirmation_response_error');
//                return $this->renderText($Error);
//            }
        } catch (Exception $e) {
            $text = "Found an error" . $e->getMessage();
            $Error = $epNibssManagerObj->setErrorXML(EpNibssResponseManager::$RESPONSE_DO_NOT_HONOR, $text);
            $this->createLog($Error, 'recharge_confirmation_response_error');
            return $this->renderText($Error);
        }

	}
    private function rechargeImmediate($txnId, $total_amount_paid, $payment_mode_option_id, $service_charge) {
    	

        $con = Doctrine_Manager::connection();
        try {
            $con->beginTransaction();
            $acctObj = new EpAccountingManager();
            $detailObj = $acctObj->getAccount($txnId);
            $ewallet_number = $detailObj->getAccountNumber();
            $accountingObj = new pay4meAccounting;
            $is_clear = 1;
            $accountingArr = $accountingObj->getRechargeAccounts($ewallet_number, $txnId, $total_amount_paid, $payment_mode_option_id, $is_clear, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT, $service_charge);

            if (is_array($accountingArr)) {

                //auditing that ewallet has been charged
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERNET_BANK;
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERNET_BANK,
                                EpAuditEvent::getFomattedMessage($msg, array('account_number' => $ewallet_number)));

                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

                $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));

                $collection_account_id = $accountingObj->getCollectionAccount($payment_mode_option_id, NULL, NULL, 'Recharge');
                $managerObj = new payForMeManager();
                $managerObj->updateEwalletAccount($collection_account_id, $txnId, $total_amount_paid, 'credit', $service_charge);
            }
            $con->commit();
            $this->getUser()->setFlash('notice', 'Recharge Successful', true);
            return $event->getreturnValue();
        } catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    public function internetbankverifyRequest($app_id, $session_id, $order_id, $type) {
        $NibssObj = new EpNibssManager();
        $verifyRequestXML = $NibssObj->setVerifyRequestXML($app_id, $session_id, $type);
        $verify_request = array();
        $this->createLog($verifyRequestXML, 'verify_request');
        if ($type == 'payment')
            $verify_request['tran_num'] = $app_id;
        else if($type == 'recharge'){
            $verify_request['ewallet_acc_num'] = $app_id;
            $verify_request['tran_num'] = NULL;
        }
        $verify_request['session_id'] = $session_id;
        $verify_request['order_id'] = $order_id;
        $confirmation_res_obj = $NibssObj->saveVerifyRequest($verify_request);
        $verfiy_request_id = $confirmation_res_obj->getId();
        $responseResult = $NibssObj->verifyTransaction($verifyRequestXML, $verfiy_request_id, $type);

        //return (is_array($responseResult)?$responseResult['status']:$responseResult);
        return $responseResult;
    }

    public function validateTransaction($pfmTransactionDetails) {
        if (empty($pfmTransactionDetails)) {
            $responseCode = EpNibssResponseManager::$RESPONSE_INVALID_TRANSACTION_NUMBER;
            $responseStatus = false;
        } else {
            if (($pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name']) != sfConfig::get('app_payment_mode_option_internet_bank')) {
                $responseCode = EpNibssResponseManager::$RESPONSE_INVALID_TRANSACTION_NUMBER;
                $responseStatus = false; // BUg:[19413], [02-APR-2013]
            } else {
                if ($pfmTransactionDetails['MerchantRequest']['payment_status_code'] == 0)
                    $responseCode = EpNibssResponseManager::$RESPONSE_DUPLICATE_TRANSACTION;
                else
                    $responseCode = EpNibssResponseManager::$RESPONSE_APPROVED;
                $responseStatus = true;
            }
        }
        $retVal = array("0" => $responseCode,
            "1" => $responseStatus);
        return $retVal;
    }

    /*
     * Repeated Cron for internet bank transactions
     */

    public function executeProcessNibss(sfWebRequest $request) {
        try {
            $this->setLayout(null);
            ini_set('memory_limit', '512M');

            $p4mObject = new pfmHelper();
            $payMode = $p4mObject->getInternetBankPayModeId();


            $from_trans_date = date('Y-m-d H:i:s', strtotime(date('Y') . "-" . date('m') . "-" . (date('d') - 1) . " 00:00:00"));
            $status = 'pending';
            $billObj = billServiceFactory::getService();
            $allGatewayOrders = $billObj->getRecordByOrderIdStatusAndDate($payMode, $status, $from_trans_date);
            foreach ($allGatewayOrders as $gatewayOrder) {
                $orderId = $gatewayOrder->getOrderId();
                $this->verfiyAndUpdateByOrderId($orderId);
            }

            return $this->renderText("Internet Bank processing done");
        } catch (Exception $e) {
            $this->logMessage("Logging the exception from Internet Bank Updation Processing.." . $e->getMessage() . var_dump(number_format(memory_get_peak_usage())));
        }
    }

    /*
     * function for query to internet bank
     */

    public function executeQueryToInternetbank(sfWebRequest $request) {
        $orderId = $request->getParameter('orderId');
        $paymentmode = $request->getParameter('paymentmode');
        $type = $request->getParameter('type');
        $this->verfiyAndUpdateByOrderId($orderId);

        $this->redirect('ewallet/viewDetail?transId=' . $orderId . '&paymentmode=' . $paymentmode . '&type=recharge');
    }

    /*
     * This function is querying the nibss and according to the response fetched recharge/payment is getting processed
     */

    public function verfiyAndUpdateByOrderId($orderId) {
        $validationNo = NULL;
        $gatewayOrderObj = new gatewayOrderManager();
        $p4mObject = new pfmHelper();
        $payMode = $p4mObject->getInternetBankPayModeId();
        $status = 'pending';
        $billObj = billServiceFactory::getService();
        $gatewayOrder = $billObj->getRecordFrmOrderId($orderId, $payMode, $status);
        if (!$gatewayOrder) {
            return false;
        }

        $order_details = $billObj->findByOrderId($orderId);
        $session_id = $order_details->getFirst()->getSessionId();
        if (strtolower($gatewayOrder->getType() == 'payment')) {
            $req_verify = $this->internetbankverifyRequest($gatewayOrder->getAppId(), $session_id, $gatewayOrder->getOrderId(), strtolower($gatewayOrder->getType()));
        } 
        else if(strtolower($gatewayOrder->getType()=='recharge')){
        	$validationNo = $this->rechargeImmediate($gatewayOrder->getAppId(), $gatewayOrder->getAmount(), $payMode, $gatewayOrder->getServicechargeKobo());
        	$req_verify['transactionDate'] = date("Y-m-d h:i:s");
        }
//         else if(strtolower($gatewayOrder->getType()=='recharge')){
//             $acctObj = new EpAccountingManager();
//             $detailObj = $acctObj->getAccount($gatewayOrder->getAppId());
//             $req_verify = $this->internetbankverifyRequest($detailObj->getAccountNumber(), $session_id, $gatewayOrder->getOrderId(), strtolower($gatewayOrder->getType()));
//             $req_verify['transactionDate'] = date("Y-m-d h:i:s");
//         }
        else{
             $req_verify['ResponseCode'] = EpNibssResponseManager::$RESPONSE_DO_NOT_HONOR;
             $req_verify['ResponseText'] = EpNibssResponseManager::$MSG_DO_NOT_HONOR;
        }
//        if ($req_verify['status'] == EpNibssResponseManager::$RESPONSE_APPROVED && $req_verify['ResponseCode'] == EpNibssResponseManager::$RESPONSE_APPROVED) {
        if ($req_verify['status'] == EpNibssResponseManager::$RESPONSE_APPROVED ) {
            if (strtolower($gatewayOrder->getType() == 'recharge')) {

                $validationNo = $this->rechargeImmediate($gatewayOrder->getAppId(), $gatewayOrder->getAmount(), $payMode, $gatewayOrder->getServicechargeKobo());
            }
            if (strtolower($gatewayOrder->getType()) == 'payment') {
                $paymentObj = new Payment($gatewayOrder->getAppId());
                $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
                $arrayVal = $paymentObj->proceedToPay();
                $pfmTransactionDetailsNew = $payForMeObj->getTransactionRecord($gatewayOrder->getAppId());
                $validationNo = $pfmTransactionDetailsNew['MerchantRequest']['validation_number'];
            }
            $status = 'success';
        } else if ($req_verify == EpNibssResponseManager::$RESPONSE_SYSTEM_MALFUNCTION) {
            $status = 'pending';
        } else {
            $status = 'failure';
        }
if(strtolower($gatewayOrder->getType()=='recharge')){
$status = 'success';
}
        // updating gateway_order table with status, validation number, transaction date
        $gatewayOrderObj->updateGatewayOrderValidationNoStatus($validationNo,$status,$orderId,$req_verify['transactionDate']);
        $ResultSet = array('Validation_num' => $validationNo,
            'ResponseCode' => $req_verify['status'],
            'ResponseText' => "Payment Done Sucessfully");
        //return $validationNo;
        return $ResultSet;
    }

    public function verfiyAndUpdateByOrderIdNibss($orderId) {
    	$validationNo = NULL;
    	$gatewayOrderObj = new gatewayOrderManager();
    	$p4mObject = new pfmHelper();
    	$payMode = $p4mObject->getInternetBankPayModeId();
    	$status = 'pending';
    	$billObj = billServiceFactory::getService();
    	$gatewayOrder = $billObj->getRecordFrmOrderId($orderId, $payMode, $status);
    	if (!$gatewayOrder) {
    		return false;
    	}
    
    	$order_details = $billObj->findByOrderId($orderId);
    	$session_id = $order_details->getFirst()->getSessionId();
    	/*if (strtolower($gatewayOrder->getType() == 'payment')) {
    		$req_verify = $this->internetbankverifyRequest($gatewayOrder->getAppId(), $session_id, $gatewayOrder->getOrderId(), strtolower($gatewayOrder->getType()));
    	}
    	//        if ($req_verify['status'] == EpNibssResponseManager::$RESPONSE_APPROVED && $req_verify['ResponseCode'] == EpNibssResponseManager::$RESPONSE_APPROVED) {
    	if ($req_verify['status'] == EpNibssResponseManager::$RESPONSE_APPROVED ) {
    		
    		$status = 'success';
    	} else if ($req_verify == EpNibssResponseManager::$RESPONSE_SYSTEM_MALFUNCTION) {
    		$status = 'pending';
    	} else {
    		$status = 'failure';
    	}*/
    	// updating gateway_order table with status, validation number, transaction date
    	$req_verify = array('transactionDate' => NULL);
    	$gatewayOrderObj->updateGatewayOrderValidationNoStatus($validationNo,$status,$orderId,$req_verify['transactionDate']);
    	$ResultSet = array('Validation_num' => $validationNo,
    			'ResponseCode' => $req_verify['status'],
    			'ResponseText' => "Payment Done Sucessfully");
    	//return $validationNo;
    	return $ResultSet;
    }
    
    
    
    
    
    /*
     * executes nibss mail
     */

    protected function addJobForNibssRechageMail($validation_number) {
        $notificationUrl = 'email/sendNibssRechargeMail';
        $taskId = EpjobsContext::getInstance()->addJob('MailNotification', $notificationUrl, array('validation_number' => $validation_number));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
    }

    public function ValidationNumberGen($query_req_arr) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $amount_credit = convertToKobo($query_req_arr['total_amount']);
        $service_charge = convertToKobo($query_req_arr['serv_charge']);
        // for getting internet bank payment mode id
        $billObj = billServiceFactory::getService();
        $pfm_obj = new pfmHelper();
        $nibss_pay_mode = $pfm_obj->getPMOIdByConfName(sfConfig::get('app_payment_mode_option_internet_bank'));
        $order_id = $billObj->saveOrderIdInGatewayorder($query_req_arr['tran_num'], 'payment', $nibss_pay_mode,
                        NULL, 1, $amount_credit, $service_charge);
        $query_req_arr['order_id'] = $order_id;
        $NibssObj = new EpNibssManager();
        $confirmationReqObj = $NibssObj->setNibssConfirmationRequest($query_req_arr);
        $ResponseSet = $this->verfiyAndUpdateByOrderIdNibss($order_id);
        $paymentObj = new Payment($query_req_arr['tran_num']);
        $arrayVal = $paymentObj->proceedToPay();
        //$this->redirectNotification($arrayVal);
        //print_r($arrayVal);
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($query_req_arr['tran_num']);
        $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];
//         $req_verify = array('transactionDate' => '');
        $status = "success";
        $gatewayOrderObj = new gatewayOrderManager();
        $gatewayOrderObj->updateGatewayOrderValidationNoStatus($validationNo,$status,$order_id);
        $ResponseSet['validationNumber'] = $validationNo;
        $ResponseSet['confirmationReqId'] = $confirmationReqObj->getId();
        return $ResponseSet;
    }

}
