<?php

/**
 * support actions.
 *
 * @package    mysfp
 * @subpackage support
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class EpsupportActions extends sfActions
{
    public $ItemId,$serviceId;

    public function executeSupport(sfWebRequest $request)
    {
        $this->setLayout(NULL) ;
        $SupportObj=new epSupport();
       
        $Xml=$SupportObj->executeSupport($request);

        return $this->renderText($Xml);
        
        
    }


}
