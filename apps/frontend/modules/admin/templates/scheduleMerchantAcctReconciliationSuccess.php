<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form') ?>
<?php echo ePortal_legend('Merchant Account Reconciliation Schedule'); ?>

<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/saveSchedule', 'name=schedule id=schedule'); ?>
    <?php include_partial('merchantReconSchForm', array('form' => $form)) ?>
    <div class="divBlock">
        <center id="multiFormNav">
            <?php echo button_to('Save Schedule', '', array('class' => 'formSubmit', 'onClick' => 'validateRequestForm()')); ?>
        </center>
    </div>
    <div id="reconcile_summary"></div>
    </form>
</div>
<script>



    $(document).ready(function(){
        // $.post('scheduleListing',"", function(data){$("#reconcile_summary").html(data);});
        $('#merchant_id').change(function(){
            var merchant_id = this.value;

            var module = "";
            //  var module = "service_payment_mode_option";
            displayMerchantService(merchant_id,module,"",'<?php echo url_for('admin/MerchantService'); ?>');
            displayPaymentModeOptions('',module,"",'<?php echo url_for('admin/PaymentModeOption'); ?>');
          
            return false;

        });


        $('#merchant_service_id').change(function(){
            var merchant_service = this.value;

            var module = "";
            //  var module = "service_payment_mode_option";
            displayPaymentModeOptions(merchant_service,module,"",'<?php echo url_for('admin/PaymentModeOption'); ?>');
            return false;

        });
    });



    function validateRequestForm(){
        $('#search_results').html("");
        var err=0;
        var merchant_id = $('#merchant_id').val();
        var merchant_service_id = $('#merchant_service_id').val();
        var account_reconcile = $('#reconcile').val();
        var payment_mode_option_id = $('#payment_mode_option_id').val();
    
        if(merchant_id == ""){
            err = err+1;
            $("#err_merchant_id").html("Please select Merchant");
        }
        else {
            $("#err_merchant_id").html("");
        }
        if(merchant_service_id == "") {
            err = err+1;
            $("#err_merchant_service_id").html("Please select Merchant Service");
        }
        else {
            $("#err_merchant_service_id").html("");
        }
        if(payment_mode_option_id == "") {
            err = err+1;
            $("#err_payment_mode_option_id").html("Please select Payment Mode Option");
        }
        else {
            $("#err_payment_mode_option_id").html("");
        }
        if(account_reconcile == "") {
            err = err+1;
            $("#err_reconcile").html("Please select Reconcilation Schedule");
        }
        else {
            $("#err_reconcile").html("");
        }
    
        if(err == 0) {
            $("#schedule").submit();
            //  $.post('saveSchedule',$("#schedule").serialize(), function(data){$("#reconcile_summary").html(data);});
        }
    }
</script>