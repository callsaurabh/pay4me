
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
          <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
      </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <thead>
      <tr class="horizontal">
        <th width="2%">S.No.</th>
        <th align="center">Merchant</th>
        <th align="center">Merchant Service</th>
        <th align="center">Payment Mode Option</th>
        <th align="center">Reconciliation Schedule</th>
        <th align="center">Last Swap Date</th>
        <th align="center">Next Swap Date</th>
        <th align="center">Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
      ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <td align="center"><?php echo $result->getMerchant(); ?></td>
        <td align="center">
        <?php
        if($result->getLastSwap() != ''){
           echo link_to($result->getMerchantService(), url_for('reconcilationHistory/index?scheduleId='.$result->getId()), array('popup' => array('popupWindow1', 'width=825,height=500,left=150,top=0,scrollbars=1')));
        }else{
           echo $result->getMerchantService();
        }
        ?>
        </td>
        <td align="center"><?php echo $result->getPaymentModeOption(); ?></td>
        <td align="center"><?php echo $result->getSchedule(); ?></td>
        <td align="center"><?php echo $result->getLastSwap(); ?></td>
        <td align="center"><?php echo $result->getNextSwap();?></td>
        <td align="center">
           <a href="javascript:;" onClick="edit_swap('<?php echo $result->getId();?>','edit')" title="Edit" >Edit</a> |
           <?php echo link_to('Swap Immediately', url_for('reconcilation/displaySwapDetails?id='.$result->getId()), array('popup' => array('popupWindow', 'width=810,height=800,left=320,top=0,scrollbars=1')))?> |
           <!--<a href="javascript:;" onClick="immedite_swap('<?php // echo $result->getId();?>','edit')" title="Edit" ></a> |-->
           <a href="javascript:;" onClick="immedite_swap('<?php echo $result->getId();?>','edit')" title="Edit" >Suspend all scheduled swaps</a></td>
      </tr>
        <?php endforeach; ?>
      <tr>
        <td colspan="8">
          <div class="paging pagingFoot floatRight"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'search_results', 'theMid') ?>
          </div>
        </td>
      </tr>
     <?php }
      else { ?>
        <tr><td  align='center' class='error' colspan="8">No Record Found</td></tr>
      <?php } ?>
    </tbody>
  </table>
</div>
