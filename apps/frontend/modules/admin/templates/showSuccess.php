<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>

    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Narration</th>
      <th>Transaction Date</th>
      <th>Amount</th>
      <?php

       if($showBal ==1)
       {
       ?>
        <th>Closing Balance</th>
       <?php
       }
      ?>
      <th>Transaction Type</th>
    </tr>
  </thead>
  <tbody>
    <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_records_per_page');
      $i = max(($page-1),0)*$limit ;
      foreach ($pager->getResults() as $result):

        $i++;
        $transaction_date = explode(" ",$result->getTransactionDate());
        $amount = format_amount($result->getAmount(),$currency,1);
        $balance = format_amount($result->getBalance(),$currency,1);
        ?>
    <tr class="alternateBgColour">
      <td align="center"><?php echo $i ?></td>
      <?php
           /*   if ($result->getEwalletRechargeAcctDetailsRecord()->count()) {
                  $paymentModeOptionName = pfmHelper::getPMONameByPMId($result->getEwalletRechargeAcctDetailsRecord()->getFirst()->getPaymentModeOptionId());
      ?>
                  <td align="center"><?php echo html_entity_decode($result->getDescription() . ' via ' . $paymentModeOptionName); ?></td>
<?php } else { */?>
                  <td align="center"><?php echo htmlspecialchars_decode($result->getDescription()); ?></td>
<?php //} ?>
      <td align="center"><?php echo $transaction_date['0'];?></td>
      <td nowrap align="right"><?php echo $amount; ?></td>
            <?php
       if($showBal == 1)
       {
       ?>
        <td align="center" nowrap><?php echo $balance; ?></td>
       <?php
       }
      ?>
      <td align="center"><?php echo ucwords($result->getEntryType()); ?></td>
    </tr>
      <?php endforeach; ?>
<?php
$dates = '' ;
if(isset($from_date))
{
    $dates .= "&from_date=".$from_date;
}
if(isset($to_date))
{
    $dates .= "&to_date=".$to_date;
}
if(isset($account_type))
{
    $dates .= "&account_type=".$account_type;
}
if(isset($showBal))
{
    $dates .= "&showBal=".$showBal;
}
if(isset($payment_option)){
   $dates .= "&payment_option=".$payment_option;
}
?>

    <tr><td colspan="6">
        <div class="paging pagingFoot"><?php  /*echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?id='.$result->getId()), '') ;*/ echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?id='.$wallet_number.$dates))?>
   </div></td></tr>



    <?php }
    else { ?>
    <tr><td  align='center' class='error' colspan="6">No Record Found</td></tr>
    <?php } ?>

  </tbody>
  <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>
</div>