
<dl>
    <div class="dsTitle4Fields"><?php echo $form['merchant_id']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['merchant_id']->render(); ?>
        <div id="err_merchant" class="cRed"></div>
        <div id="li_merchant_id" class="height_row">NOT APPLICABLE</div>
    </div>
</dl>
<dl >
    <div class="dsTitle4Fields"><?php echo $form['merchant_service']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['merchant_service']->render(); ?>
        <div id="err_merchant_service" class="cRed"></div>
        <div id="li_merchant_service" class="height_row">NOT APPLICABLE</div>
    </div>
</dl>
<dl>
    <div class="dsTitle4Fields"><?php echo $form['currency']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['currency']->render(); ?>
        <div id="err_currency" class="cRed"></div>
        <div id="li_currency" class="height_row">NOT APPLICABLE</div>
    </div>
</dl>
<dl >
    <div class="dsTitle4Fields"><?php echo $form['payment_option']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['payment_option']->render(); ?>

        <div id="err_payment_option" class="cRed"></div>
    </div>
</dl>
<dl >
    <div class="dsTitle4Fields"><?php echo $form['bank']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['bank']->render(); ?>
        <div id="err_bank" class="cRed"></div>
        <div id="li_bank" class="height_row">NOT APPLICABLE</div>
    </div>
</dl>

<script type="text/javascript">
    $(document).ready(function() {
        $("#merchant_id").change(function() {
            $.post("<?php echo url_for('admin/merchantService'); ?>", $("#merchant_id").serialize(),function (data) {
                $('#err_merchant').html('');
                $("#payment_option").html("<option value=''>Please select Payment Option</option>");
                $("#merchant_service").html(data);

            });
            //FS#35785 - [WP058]In Collection Account Summary,All configured bank,according to filter,are displaying two time 
//            $.post("<?php //echo url_for('admin/merchantBank'); ?>", $("#merchant_id").serialize(), function (data) {
//
//                $("#bank").html(data);
//
//            });
       $.post("<?php echo url_for('admin/merchantCurrency'); ?>", $("#merchant_id").serialize(), function (data) {

                $("#currency").html(data);

            });
        });
        $("#merchant_service").change(function() {
            $.post("<?php echo url_for('admin/PaymentModeOption'); ?>", $("#merchant_service").serialize(),function (data) {
                $('#err_payment_option').html('');
                $('#err_merchant_service').html('');
                $('#err_bank').html('');
                $('#err_currency').html('');
                $("#payment_option").html(data);

            });
        });
        $("#bank").change(function() {
            $('#err_bank').html('');
        });
        $("#currency").change(function() {
            $("#payment_option").show();
            $.post("<?php echo url_for('admin/PaymentModeOption'); ?>", $("#merchant_service").serialize(),function (data) {
                $('#err_payment_option').html('');
                $('#err_merchant_service').html('');
                $('#err_bank').html('');
                $('#err_currency').html('');
                $("#payment_option").html(data);

            });
            $.post("<?php echo url_for('admin/paymentModeOptionFilteredBanks'); ?>", {mode_id:$("#payment_option").val(),merchant_id:$("#merchant_id").val(),currency:$("#currency").val()},function (data) {
                    $("#bank").html(data);

                });
        });
        $("#payment_option").change(function() {
            $("#currency").show();
            if(($("#payment_option").val() == 4) || ($("#payment_option").val() == 2) || ($("#payment_option").val() == 5) || ($("#payment_option").val() == <?php echo sfConfig::get('app_internet_bank')?>))
            {
                $("#bank").attr('disabled',true);
                $("#err_bank").html('');
                $("#merchant_id").hide();
                $("#currency").hide();
                $("#bank").hide();
                $("#merchant_service").hide();
                $('#li_merchant_id').show();
                $('#li_bank').show();
                $('#li_currency').show();
                $('#li_merchant_service').show();

                $("#err_merchant").hide();
                $("#err_merchant_service").hide();
                $("#err_currency").hide();
                $("#err_payment_option").hide();
                $("#err_bank").hide();
               
            }
            else if($("#payment_option").val() == 14 || $("#payment_option").val() == 15 || $("#payment_option").val() == 1)
            {
                $.post("<?php echo url_for('admin/paymentModeOptionFilteredBanks'); ?>", {mode_id:$("#payment_option").val(),merchant_id:$("#merchant_id").val(),currency:$("#currency").val()},function (data) {
                    $("#bank").html(data);
                    $("#currency").show();

                });


            }
            if($("#payment_option").val() != 4  && $("#payment_option").val() != 2 &&  $("#payment_option").val() != 5 &&  $("#payment_option").val() != <?php echo sfConfig::get('app_internet_bank')?>  )
            {
                $("#bank").attr('disabled',false);
                //$("#err_bank").html('');
                $('#li_merchant_id').hide();
                $('#li_bank').hide();
                $('#li_currency').hide();
                $('#li_merchant_service').hide();
                $('#merchant_id').show();
                $('#bank').show();
                $('#merchant_service').show();
            }
            $('#err_payment_option').html('');
        });

    });
</script>


