<?php if ($sf_user->hasFlash('lastlogin')): ?>
<br><br>
     <div class="lastlogin">
        <?php echo $sf_user->getFlash('lastlogin') ?>
     </div>
<?php endif; ?>
<?php echo ePortal_pagehead(" "); ?>

<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
?>
<script type="text/javascript">

<?php if($deactivate){ ?>
$(document).ready(function(){
    var confirmText = confirm("Do you want to de-activate your eWallet Account?");
    if (confirmText== true)
    {
        deactivateAccount();
        //return true;
    }else{
        notdeactivateAccount();
        //return false;
    }
})
<?php } ?>
function deactivateAccount()
{
    var url = '<?php echo url_for("openId/Deactivatefinal"); ?>';

     $.ajax({
          type: "POST",
          url: url,
          async:false,
          success: function(){
            alert('Your eWallet Account has been De-Activated successfully for payment and recharge.');
            window.location = "<?php echo url_for('admin/eWalletUser'); ?>";
            }});

}

function notdeactivateAccount()
{
    window.location = "<?php echo url_for('admin/eWalletUser'); ?>";

}
</script>
