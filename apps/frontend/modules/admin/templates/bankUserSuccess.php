<?php echo ePortal_pagehead(" "); ?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
?>

<?php //use_helper('Form');
if ($sf_user->hasFlash('lastlogin')): ?>
     <div class="lastlogin">
        <?php echo $sf_user->getFlash('lastlogin') ?>
     </div>
<?php endif; ?>
<?php echo form_tag('admin/bankUser',array('name'=>'pfm_bank_user_form','id'=>'pfm_bank_user_form')) ?>
<div class="wrapForm2">

<?php
echo ePortal_legend(__('Pay4Me Bank Payment'));?>
<?php echo formRowComplete(__($form['txnId']->renderLabel()),$form['txnId']->render(),'','txnId','err_txnId','txnId_row',__($form['txnId']->renderError()),'error_listing');     ?>
    <div class="divBlock">
        <center>
            <?php  if ($form->isCSRFProtected()) : ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>

        </center>
    </div>
    <div class="divBlock">
        <center>           
            <input type = "submit" name="submit" value="<?php echo __('Submit')?>" class="formSubmit">
        </center>
    </div>
</div>
</form>
</div>
<script type="text/javascript">
    var broad_cast = false;
    function openInformationWindow(){
        var appname = window.navigator.appName;
        var height = '163';
        if(appname == 'Microsoft Internet Explorer'){
            var height = '213';
        }
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'openInfoBox', 'Broadcast  Message', 'width=480px,height='+height+',center=1,border=0, scrolling=yes')
        emailwindow.onclose=function(){
            var theform=this.contentDoc.forms[0]
            var theemail=this.contentDoc.getElementById("info_check")
            if (!theemail.checked){
                alert("Please confirm that you have read the information by seleting the checkbox");
                return false
            }
            else{
                $.post('<?php echo url_for('broadcast/setSessionVariableBroadcast') ?>', {'label1':true}, function (data) {
                  }
              );
                return true;
            }
        }
    }


       function openLegalWindow(){
        var appname = window.navigator.appName;
            var height = '120';
            var width = '490';
            if(appname == 'Microsoft Internet Explorer'){
                var height = '141';
                var width = '473';
            }
        emailwindow=dhtmlmodal.open('EmailBox1', 'iframe', 'openLegalBox', 'Legal Notice', 'width='+width+',height='+height+',center=1,border=0, scrolling=no')
        emailwindow.onclose=function(){
            var theform=this.contentDoc.forms[0]
            var theemail=this.contentDoc.getElementById("info_check")
            if (!theemail.checked){
                alert("Please confirm by selecting the checkbox that this profile is yours and you are accepting the Pay4Me terms of use");
                return false
            }
            else{
                $.post('<?php echo url_for('broadcast/setSessionVariableLegal') ?>', {'label':true}, function (data) {
                        if(broad_cast == true){
                            openInformationWindow();
                        }
                    }
                );
                return true;
            }
        }
    }
   function openReportAbuse(event) {   
     var url = "<?php echo url_for('welcome/openInfoBoxReportAbuse'); ?>";
    emailwindowReportabuse=dhtmlmodal.open('EmailBox', 'iframe',url, 'Report Abuse', 'width=700px,height=300px,center=1,border=0, scrolling=no');
         emailwindowReportabuse.onclose=function(){
            return true;
           }
      return false;     
  }
  $('#reportabuse').click(openReportAbuse);
  

</script>
<?php
if(($broadcast_message!=array() && $broadcast_message!="" && ((!$BroadcastChecked_bank))) && (($legal_message=="") || ($LegalChecked_bank) )){
  echo "<script>$(document).ready(function(){openInformationWindow();});</script>";
}
else if(isset($legal_message) && (!($LegalChecked_bank))){
      if($legal_message!=array() && $legal_message!=""){
          if(($broadcast_message!="") && (!$BroadcastChecked_bank)){
              echo "<script> broad_cast = true;</script>";
          }
      echo "<script>$(document).ready(function(){openLegalWindow();});</script>";
    }
}

?>