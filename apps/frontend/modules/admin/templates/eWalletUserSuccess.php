
<?php echo ePortal_pagehead(" "); ?>

<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
?>
<script type="text/javascript">
  function openInformationWindow(){
    <?php $openinfoUrl = url_for('admin/openInfoBox'); ?>
            var appname = window.navigator.appName;
            var height = '163';
            if(appname == 'Microsoft Internet Explorer'){
                var height = '213';
            }
    emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '<?php echo $openinfoUrl;?>', 'Broadcast Message', 'width=490px,height='+height+',center=1,border=0, scrolling=no')
    emailwindow.onclose=function(){
      var theform=this.contentDoc.forms[0]
      var theemail=this.contentDoc.getElementById("info_check")
      if (!theemail.checked){
        alert("Please confirm that you have read the information by seleting the checkbox");
        return false
      }
      else{
          // Bug:37523
        $.post('<?php echo url_for('broadcast/setSessionVariableBroadcast') ?>', {'label1':true}, function (data) {
                  }
              );
        return true;
      }
    }
  }

</script>
<?php //use_helper('Form');
if($merchantRequestId){
         ?>
<?php echo form_tag('paymentSystem/pay',array('name'=>'pfm_bank_user_form','class'=>'dlForm multiForm', 'id'=>'pfm_bank_user_form')) ?>
<div>
<?php if($accountBalance<$formData['total_amount']){
    echo  "<h2>Insufficient account balance.</h2>";
}?>

          <?php
          echo ePortal_legend('User eWallet Account Details');
                if($errorForEwalletAcount){
                     echo formRowFormatRaw('Error message ',$errorForEwalletAcount);
                     }else{
                           echo formRowFormatRaw('Account ID',ePortal_displayName($accountObj['account_number']));
                           echo formRowFormatRaw('Account Name',ePortal_displayName($accountObj['account_name']));
                           echo formRowFormatRaw('Account Balance',ePortal_displayName($accountBalance));
                         
     }?>
   
      <?php
      echo ePortal_legend('Application Details');
      echo formRowFormatRaw('Transaction Number',$formData['pfm_transaction_number']);
      if($MerchantData)
      {
        foreach($MerchantData as $k=>$v)
        {
           echo formRowFormatRaw($v,$formData['MerchantRequest']['MerchantRequestDetails'][$k]);

        }

      }
    //  echo formRowFormatRaw('Application Id',$formData['application_id']);
    //  echo formRowFormatRaw('Reference No.',$formData['ref_no']);
      echo formRowFormatRaw('Application Type',$formData['MerchantRequest']['MerchantService']['name']);
      ?>
    
      <?php
      echo ePortal_legend('User Details');
      echo formRowFormatRaw('Name',ePortal_displayName($formData['MerchantRequest']['MerchantRequestDetails']['name']));
      #echo formRowFormatRaw('Mobile',$formData['mobile']);
      #echo formRowFormatRaw('Email',$formData['email']);
      ?>
    
      <?php
      echo ePortal_legend('Payment Details');
      echo formRowFormatRaw('Application Charges',$formData['MerchantRequest']['item_fee']);
      if($formData['MerchantRequest']['bank_charge']!=""){
      echo formRowFormatRaw('Transaction Charges',$formData['MerchantRequest']['bank_charge']);
      }
      if($formData['MerchantRequest']['service_charge']!=""){
      echo formRowFormatRaw('Service Charges',$formData['MerchantRequest']['service_charge']);
      }
      echo formRowFormatRaw('Total Payable Amount',$formData['total_amount']);
      ?>
    
    <input type ="hidden" name="txnId" value ="<?php echo $formData['pfm_transaction_number']?>"/>
    <div class="divBlock">
        <center id="formNav">
                <?php  if($accountBalance>=$formData['total_amount']){ ?>
           <input type="submit" value="Pay" id="button" class="formSubmit" name="button"/>
        <?php }?>
        </center>
    </div>
  </div>
  <?php }
if($broadcast_message!=array() && $broadcast_message!="" && !$BroadcastChecked) { // Bug:37523, $BroadcastChecked is added for session on broadcast mesasge
echo "<script>window.onload = openInformationWindow();</script>";
}
?>
<div class="ewallet_proc">
<?php if ($sf_user->hasFlash('lastlogin')){ ?>
    <h3>Last Login Details</h3>
    <p class="text"><?php echo $sf_user->getFlash('lastlogin') ?></p>
  <br>
  <?php } ?>

  
    <h3>Last Transactions</h3>
    <table class="dataTable" width="100%" cellspacing="0" cellpadding="0" >
        <tr class="horizontal">
            <th>S.No.</th>
                 <th>Application Type</th>
                  <th>Transaction Number</th>
                    <th>Amount</th>
                       <th>Action</th>
                         
        </tr>
         <?php
                if(($paidpager->getNbResults())>0) {
                    $limit = 5;
                    $page = $sf_context->getRequest()->getParameter('page',0);
                    $i = max(($page-1),0)*$limit ;
                    foreach ($paidpager->getResults() as $bill):
                    $i++;
                    ?>
        <tr class="oddrow">
            <td><?php echo $i;?></td>
            <td><?php echo $bill->getApplicationType() ?></td>
             <td><?php echo $bill->getTransNum() ?></td>
            <td><?php echo format_amount($bill->getAmount(), $bill->getCurrencyId()) ?></td>
            <td><a href="<?php echo url_for('bill/show?trans_num='.$bill->getTransNum().'&bill_status='.$bill->getStatus());?>">Show Details</a></td>
        </tr>

          <?php endforeach;
                ?>
                <?php }else{ ?>
                  <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                  <tr class="alternateBgColour"><td  align='center' class='error' >No Record Found</td></tr>
                 </table>
                <?php } ?>
    </table>

   
    <p class="text" align="right">
         <?php if($activepager->getNbResults() > 5){?>
        <a href="<?php echo url_for('bill/paidBills');?>"> View more </a><?php }?>
    </p>




    <h3> Pending Transactions</h3>
 <table class="dataTable" width="100%" cellspacing="0" cellpadding="0" >
        <tr class="horizontal">
            <th>S.No.</th>
              <th>Application Type</th>
              <th>Transaction Number</th>
                    <th>Amount</th>
                       <th>Action</th>
        </tr>
      <?php
                if(($activepager->getNbResults())>0) {
                    $limit = 5;
                    $page = $sf_context->getRequest()->getParameter('page',0);
                    $i = max(($page-1),0)*$limit ;
                    foreach ($activepager->getResults() as $bill):
                    $i++;
                    ?>
        <tr class="oddrow">
            <td><?php echo $i;?></td>
            <td><?php echo $bill->getApplicationType() ?></td>
             <td><?php echo $bill->getTransNum() ?></td>
            <td><?php
            echo format_amount($bill->getAmount(),$bill->getMerchantRequest()->getCurrencyId()); ?></td>
            <td>
      <?php echo image_tag('../images/payBtn.gif',array('width'=>60,'height'=>25,'alt'=>'Pay', 'style' => "cursor:pointer", 'onClick' =>'location.href=\''.url_for('bill/show?trans_num='.$bill->getTransNum().'&bill_status='.$bill->getStatus()).'\'')); ?>
              </td>
        </tr>

          <?php endforeach;
                ?>
                <?php }else{ ?>
                  <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                  <tr class="alternateBgColour"><td  align='center' class='error' >No Record Found</td></tr>
                 </table>
                <?php } ?>
    </table>
 
     <p class="text" align="right">
         <?php if($activepager->getNbResults() > 5){?>
         <a href="<?php echo url_for('bill/activeBills');?>"> View more </a><?php }?>
     </p>

     <?php
  $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
  $user_detail = Doctrine::getTable('UserDetail')->findByUserId($user_id);
  if (in_array($user_detail->getFirst()->getEwalletType(), sfConfig::get('app_ewallet_account_type'))) {
  ?>

    <h3>Account Balance</h3>
     <table class="dataTable" width="100%" cellspacing="0" cellpadding="0" >
        <tr class="horizontal">
            
              <th>Account Number</th>
                 <th>Balance</th>
                   <th>Statement</th>
                   

        </tr>
         <?php  foreach($arrAccountCollection as $result) {
           $accountObj = $result->getEpMasterAccount();
           ?>
        <tr class="oddrow">
            <td><?php echo $accountObj->getAccountNumber(); ?></td>
            <td><?php  echo format_amount($accountObj->getClearBalance(),$result->getCurrencyId(),1); ?></td>
            <td>
            
           <?php echo link_to(__('View'), url_for('ewallet/search?id='.base64_encode($accountObj->getId())), array('title' => __('View'))) ?> |
                <?php echo link_to(__('Generate eWallet Certificate'), url_for('ewallet/generateCertificate?id='.base64_encode($accountObj->getId())), array('title' => __('Generate eWallet Certificate'))) ?>
     </td>
        </tr>
          
<?php
        }}
       ?>
           </table>

 



</div>
</div>

<script type="text/javascript">
 function openReportAbuse(event) {   
     var url = "<?php echo url_for('welcome/openInfoBoxReportAbuse'); ?>";
    emailwindowReportabuse=dhtmlmodal.open('EmailBox', 'iframe',url, 'Report Abuse', 'width=700px,height=300px,center=1,border=0, scrolling=no');
         emailwindowReportabuse.onclose=function(){
            return true;
           }
      return false;     
  }
  $('#reportabuse').click(openReportAbuse);
  </script>
  <?php
if(sfContext::getInstance()->getUser()->getAttribute('reactivate')){
    echo "<script> alert ('You have successfully reactivated your eWallet Account on Pay4me.'); </script>";
    sfContext::getInstance()->getUser()->setAttribute('reactivate',false);
}
  ?>
