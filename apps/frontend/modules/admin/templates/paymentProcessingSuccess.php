<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<?php //use_helper('Form') ?>

<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/display', 'name=search id=search'); ?>

    <?php echo ePortal_legend('Account Summary'); ?>


    <dl id='bank_branch_id_dl'>
    <div class="dsTitle4Fields"><?php echo $custom_form_search['account_type']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $custom_form_search['account_type']->render(); ?>
        <div id="err_acct_type" class="cRed"></div>
    </div>


   

    <div id="showdiv"></div>

    <div id="receivediv">
    <?php include_partial('ewallet_receiving_PaymentProcessingForm', array('form' => $custom_form_receiving, 'acc_type' => 'rec')); ?>
        </div>

        <div id="collectiondiv">
    <?php include_partial('collection_PaymentProcessingForm', array('form' => $custom_form_collection)); ?>

        </div>

        <div id="ewalletdiv">
    <?php include_partial('ewallet_receiving_PaymentProcessingForm', array('form' => $custom_form_ewallet, 'acc_type' => 'ewal')); ?>
        </div>

        
    </dl>
    <dl id='bank_branch_id_dl'>
    <div class="dsTitle4Fields"><?php echo $custom_form_search['from_date']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $custom_form_search['from_date']->render();?>
        <div id="err_from_date" class="cRed"></div>
    </div>

</dl>
<dl id='bank_branch_id_dl'>
    <div class="dsTitle4Fields"><?php echo $custom_form_search['to_date']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $custom_form_search['to_date']->render();?>
        <div id="err_to_date" class="cRed"></div>
    </div>

</dl>
    <div class="divBlock">
        <center id="multiFormNav">
            <input class="formSubmit" onclick="validateRequestForm()" value="Search" type="button">
        </center>
    </div>


</form>

</div>
<br />
<div id="search_results"></div>




        <script>


            function validateRequestForm(){
                $('#search_results').html("");
                var err=0;
                var from_date = $('#from_date_date').val();
                var to_date = $('#to_date_date').val();

                if($('#account_type').val() =='')
                {
                    $('#err_acct_type').html('Please select Account Type');
                    return false;
                }
                //for eWallet
                if($('#account_type').val() =='ewallet' && $('#ewal_acc_no').val() =='' && $('#ewal_acc_name').val() =='')
                {

                    $('#err_ewal_no_name').html('Please enter Account No. OR Account Name');
                    return false;
                }
                else
                    {
                        $('#err_ewal_no_name').html('');
                    }
                //for receiving
                //    if($('#account_type').val() =='receiving' && $('#rec_acc_no').val() =='' && $('#rec_acc_name').val() =='')
                //     {
                //        $('#err_rec_acc_no_name').html('Please enter Account No. OR Account Name');
                //        return false;
                //     }
                //for collection
                if($('#account_type').val() =='collection')
                {
                    if($('#merchant_id').val() == '')
                    {
                        $('#err_merchant').html('Please select Merchant');
                        err++;
                        //           return false;
                    }
                    
                    if($('#payment_option').val()!='4' && $('#payment_option').val()!='2' && $('#payment_option').val()!='<?php echo sfConfig::get('app_internet_bank')?>' && $('#payment_option').val()!='5'){
                        if($('#bank').val() == '')
                        {
                            $('#err_bank').html('Please select Bank');
                            err++;
                        }
                    }
                    if($('#merchant_service').val() == '')
                    {
                        $('#err_merchant_service').html('Please select Merchant Service');
                        err++;
                        //           return false;
                    }
                    if($('#currency').val() == '')
                    {
                        $('#err_currency').html('Please select Currency');
                        err++;
                        //           return false;
                    }
                    if($('#payment_option').val() == '')
                    {
                        $('#err_payment_option').html('Please select Payment Option');
                        err++;
                        //           return false;
                    }
                    if(err>0)
                    {
                        return false;
                    }
                }

                if((from_date != "") && (to_date != ""))
                {

                    var from_date_sel = from_date.split("/");
                    var from_date_selected = new Date(from_date_sel[2],from_date_sel[0]-1,from_date_sel[1] );

                    var to_date_sel = to_date.split("/");
                    var to_date_selected = new Date(to_date_sel[2],to_date_sel[0]-1,to_date_sel[1] );


                    var milli_to_date = to_date_selected.getTime();
                    var milli_from_date = from_date_selected.getTime();
                    var diff = milli_to_date - milli_from_date;
                    if(diff<0)
                    {
                        err = err+1;
                        $("#err_from_date").html("From Date should be less than To Date");
                    }
                    else
                    {
                        $("#err_from_date").html("");
                    }
                }

                $('#err_rec_acc_no_name').html('');
                $('#err_ewal_acc_no_name').html('');
                if(err == 0) {
                    $('#err_ewal_no_name').html('');
                    /*var ew_div=$("#ewalletdiv").html();
                    var rec_div=$("#receivediv").html();
                    var collection_div=$("#collectiondiv").html();
                    $("#ewalletdiv").html('');
                    $("#receivediv").html('');
                    $("#collectiondiv").html('');*/
                    $.post('accountSummary',$("#search").serialize(), function(data){
                        if(data=='logout'){
                            location.reload();
                        }
                        else {
                            $("#search_results").html(data);
                            /*$("#ewalletdiv").html(ew_div);
                    $("#receivediv").html(rec_div);
                    $("#collectiondiv").html(collection_div);*/
                        }
                    });
                }

            }
        </script>

<?php //anil added ?>
            <script>
                $(document).ready(function()
                {
                    //Didsplay eWallet div
                    $("#account_type").change(function()
                    {
                        var account_type = $(this).val();
                        $("#search_results").html('');
                        //$("#from_date_date").val("");
                        //$("#to_date_date").val("");
                        $('#search')[0].reset();
                        $(this).val(account_type);
                        if(account_type == '')
                        {
                            $("#showdiv").html("");
                        }
                        if(account_type == 'ewallet')
                        {
                            //$("#showdiv").html($("#ewalletdiv").html());
                            //$("#showdiv").html();
                            $("#ewalletdiv").show();
                            $("#receivediv").hide();
                            $("#collectiondiv").hide();



                            $('#err_acct_type').html('');
                        }
                        if(account_type == 'receiving')
                        {
                            //$("#showdiv").html($("#receivediv").html());
                            $('#err_acct_type').html('');
                            $("#ewalletdiv").hide();
                            $("#receivediv").show();
                            $("#collectiondiv").hide();
                        }
                        if(account_type == 'collection')
                        {
                            $.get("<?php echo url_for('admin/merchantList'); ?>", function (data) {
                                $("#merchant_id").html(data);
                                
                        });
                        $("#ewalletdiv").hide();
                        $("#receivediv").hide();
                        $("#collectiondiv").show();
                //$("#showdiv").html($("#collectiondiv").html());
                $('#err_acct_type').html('');
                
            }
        });
    });

</script>
