<div id="search_results">
<?php //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <thead>
        <tr class="horizontal">
          <th width = "2%">S.No.</th>
          <th>Account Number</th>
          <th>Account Name</th>
          <th>Total Credit</th>
          <th>Total Debit</th>
        </tr>
      </thead>
      <tbody>
      <?php
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $i = max(($page-1),0)*$limit ;

        foreach ($pager->getResults() as $result):

        $ledgerObj = $result->getEpMasterLedger();
        if($account_type!='receiving')
        $collectionObj = $result->getUserAccountCollection()->getFirst();
        $ledger_entries = count($ledgerObj);

        $i++;

        ?>
        <tr class="alternateBgColour">
          <td align="center"><?php echo $i; ?></td>
          <td align="center"><?php echo $result->getAccountNumber();?></td>
          <?php if($ledger_entries){
            $url = "";
          
          if($from_date!= "")  {
            $url .= "&from_date=".$from_date;
          }
           if($to_date!=""){
             $url .= "&to_date=".$to_date;
           }
            ?> 

          <td align="center">
            <?php
            if($account_type=='receiving')
                 echo link_to($result->getAccountName(), url_for('admin/show?id='.$result->getId().'&account_type='.$account_type.$url), array('popup' => array('popupWindow', 'width=870,height=500,left=150,top=0,scrollbars=yes')));
             else
                echo link_to($result->getAccountName(), url_for('admin/show?id='.$result->getId().'&account_type='.$account_type.$url), array('popup' => array('popupWindow', 'width=870,height=500,left=150,top=0,scrollbars=yes')));
             ?>
          </td>
           <?php $crAmount = 0; $drAmount = 0;
          foreach($ledgerObj as $ledger_result):
            if($ledger_result->getEntryType() == 'credit') {$crAmount = $ledger_result->getAmount();}
            else {$drAmount = $ledger_result->getAmount();}
          endforeach; ?>
          <?php
              if($account_type=='ewallet'){
                    $userObj = new UserAccountManager();
                    $currency = $userObj->getCurrencyForAccount($result->getId());
                }
               if($account_type=='receiving'){
                    $finInstActConfigObj = Doctrine::getTable("SplitAccountConfiguration")->findByMasterAccountId($result->getId());
                    $res = $finInstActConfigObj->toArray();//print "<pre>";print_r($res);//exit;
                    if(count($finInstActConfigObj)) {
                    $currency =  $finInstActConfigObj->getFirst()->getCurrencyId();
                    } else {
                        $finInstActConfigObj = Doctrine::getTable("FinancialInstitutionAcctConfig")->findByAccountId($result->getId());
                        $currency =  $finInstActConfigObj->getFirst()->getCurrencyId();
                    }
                    
                }
                  if($account_type!='receiving') { ?>
                      <td align="right"><?php  echo format_amount($cAmount = ($crAmount)? $crAmount : 0,$collectionObj->getCurrencyId(),$currency); ?></td>
                      <td align="right"><?php  echo format_amount($dcAmount = ($drAmount)? $drAmount : 0,$collectionObj->getCurrencyId(),$currency); ?></td>
                  <?php }else { ?>
                      <td align="right"><?php  echo format_amount($cAmount = ($crAmount)? $crAmount : 0,$currency,1); ?></td>
                      <td align="right"><?php  echo format_amount($dcAmount = ($drAmount)? $drAmount : 0,$currency,1); ?></td>
                 <?php } ?>
              <?php }else{?>
                  <td align="center"><?php echo $result->getAccountName();?></td>
                  <td align="right"><?php  echo format_amount(0,1); ?></td>
                  <td align="right"><?php  echo format_amount(0,1); ?></td>
            <?php }?>
        </tr>
        <?php endforeach; ?>

        <tr><td colspan="5">
        <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'search_results')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

    </div></td></tr>
    
      <?php }
    else { ?>
     <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >    
       <tr><td  align='center' class='error' colspan="5" >No Record Found</td></tr>
     </table>
    <?php } ?>
  </tbody>
  <tfoot><tr><td colspan="5"></td></tr></tfoot>
</table>
</div>
</div>