<?php

/**
 * Admin actions.
 *
 * @package    symfony
 * @subpackage admin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class AdminActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $pageName = $request->getParameter('p');
        $pfmHelperObj = new pfmHelper();
        $userGroup = $pfmHelperObj->getUserGroup();
        if (isset($pageName) && !empty($pageName)) {
            $this->setTemplate($pageName);
        } else {
            if ($userGroup == sfConfig::get('app_pfm_role_admin')) {
                $this->setTemplate('admin');
            } else if ($userGroup == sfConfig::get('app_pfm_role_bank_user')) {
                $this->setTemplate('bankUser');
            }
        }
    }

    public function executeUser(sfWebRequest $request) {
        $usr = $this->getUser();
    }

    public function executeBankUser(sfWebRequest $request) {
        $this->LegalChecked_bank= $this->getUser()->getAttribute('LegalChecked', '0'); //legal message session variable
        $this->BroadcastChecked_bank= $this->getUser()->getAttribute('BroadcastChecked', '0'); //Broadcast message session variable
        APIHelper::setFlashLogin();
        if ($request->hasParameter('trans_num')) {//added in case of duplicate integrity constraint; will be forwarded here from bill/paymentProcessing
            $pfm_trans_num = $request->getParameter('trans_num');
            $this->redirect('paymentSystem/search?txnId=' . $pfm_trans_num);
        }

        //prompt message when question updated succesfuuly 10 to 3
        if ($request->getParameter('questionflag')) { //
            $this->getUser()->setFlash('notice', sprintf('Questions Updated Successfully'), false);
        }

        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $this->form = new transactionIdSearchForm();
        if ($request->isMethod('post') && $request->getParameter('search')) {
            
//        if(sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId() == 1){
//            $this->getUser()->setFlash('error', "Sorry! We are experiencing technical difficulties and cannot proceed the transaction. Please try after some time.", true);
//            $this->redirect($request->getReferer());
//        }
            
            $this->form->bind($request->getParameter('search'));
            if ($this->form->isValid()) {
                $pfm_trans_num = $this->form->getValue('txnId');
                $pfmHelper=new pfmHelper();
                $bankDraftId=$pfmHelper->getPMOIdByConfName('bank_draft');
                $paymentModeDetails = Doctrine::getTable('Transaction')->findByPfmTransactionNumber($pfm_trans_num);
                if($paymentModeDetails->Count()!=0){
                    if($paymentModeDetails->getFirst()->getpaymentModeOptionId()==$bankDraftId
                        //                     && ($paymentModeDetails->getFirst()->getCheckNumber()=='' ||$paymentModeDetails->getFirst()->getSortCode()=='' )
                    ){
                        $pfmTransactionNotBank = $payForMeObj->getTransactionRecord($pfm_trans_num);
                        $paymentStatus = $pfmTransactionNotBank["MerchantRequest"]['payment_status_code'];
                        $txnBankId = $pfmTransactionNotBank['bank_id'];
                        $bankId = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
                        if($paymentStatus ==1 && $bankId != $txnBankId){
                            $this->getUser()->setFlash('error', "Your Bank is not eligible for this transaction as applicant had selected different bank for it.", true);
                            $this->redirect($request->getReferer());
                        }else{
                            $request->getParameterHolder()->set('txnNo', $pfm_trans_num);
                            $this->forward('paymentSystem', 'getDraftDetails');
                        }
                    }else{
                        $this->redirect('paymentSystem/search?txnId=' . $pfm_trans_num);

                    }
                }else{
                    $this->getUser()->setFlash('error', "Transaction Id does not exist ", true);
                    $this->redirect($request->getReferer());
                }
            } else {
                // echo $this->form->renderGlobalErrors();
            }
        }
        $this->broadcast_message = false;

        if (!$request->getParameter('name') && $request->getParameter('name') != "STN") {

            $user_id = $this->getUser()->getGuardUser()->getId();
            $userRole = new UserRoleHelper();
            $message = $userRole->fetchBroadCastMessage($user_id);
            if ($message) {
                $this->broadcast_message = $message['0']['message'];
            }

            //$this->broadcast_message = $this->getBroadcastMessage();
        }
        $this->legal_message = false;
        // WP060 - To show legal notice when any user of bank login into account
        if(sfConfig::get('app_legal_notice_show') && !$request->getParameter('name') && $request->getParameter('name') != "STN"){
            $user_id = $this->getUser()->getGuardUser()->getId();
            $userRole = new UserRoleHelper();
            $message = $userRole->fetchLegalMessage($user_id);
            if ($message) {
                $this->legal_message = $message['0']['message'];
            }
        }
        
        if (in_array(sfConfig::get('app_pfm_role_admin'), $this->getUser()->getGroupNames())) {
            $this->redirect('@adminhome');
        }
    }
   
    public function executeEWalletUser(sfWebRequest $request) {
        $this->BroadcastChecked= $this->getUser()->getAttribute('BroadcastChecked', '0'); //Broadcast message session variable ,Bug:37523
        $user_id = $this->getUser()->getGuardUser()->getId();
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($user_id);
        APIHelper::setFlashLogin();
        if (in_array(sfConfig::get('app_pfm_role_ewallet_user'), $this->getUser()->getGroupNames())|| in_array(sfConfig::get('app_pfm_role_guest_user'), $this->getUser()->getGroupNames())) {
            if ($this->getUser() && $this->getUser()->isAuthenticated()) {
                $changePin = false;
                if (in_array($user_detail->getFirst()->getEwalletType(), sfConfig::get('app_ewallet_account_type'))) {
                
                    if (Settings::isEwalletPinActive()) {
                        $user = $this->getUser()->getGuardUser();
                        if($user->getEwalletPin()->count()>0){
                            $forcedChange = $user->getEwalletPin()->getFirst()->getForcedPinChange();
                            $pinStatus = $user->getEwalletPin()->getFirst()->getStatus();

                            $pinNotice = '';
                            if ('1' == $forcedChange && 'inactive' == $pinStatus) {
                                $changePin = true;
                                $pinNotice = '<br>An eWallet secure Pin has been mailed to you.<br>Please change your pin for secure and smooth transactions.';
                            } else if ('0' == $forcedChange && 'inactive' == $pinStatus) {
                                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                                $url = url_for('ewallet_pin/changePin');
                                $pinNotice = '<br>You are advised to use eWallet Secure Pin to make your eWallet transactions more safe and secure
                         Click <a class="navArea" href="' . $url . '">here</a> to generate one ';
                                $this->getUser()->setFlash('notice', 'You are advised to use eWallet Secure Pin to make your eWallet transactions more safe and secure
                         Click <a class="navArea" href="' . $url . '">here</a> to generate one ');
                            }
                        }
                    }
                }

                /* for paid and active transcations on home page */
                $billObj = billServiceFactory::getService();
                $this->itemwise_paid_bill_list = $billObj->getPaidBills(NULL,NULL,NULL,NULL,NULL,NULL,true);
                $this->itemwise_active_bill_list = $billObj->getItemWiseActiveBills();
                $this->page = 1;
                if($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                }
                $this->paidpager = new sfDoctrinePager('Bill',5);
                $this->paidpager->setQuery($this->itemwise_paid_bill_list);
                $this->paidpager->setPage($this->page);
                $this->paidpager->init();


                $this->activepager = new sfDoctrinePager('Bill',5);
                $this->activepager->setQuery($this->itemwise_active_bill_list);
                $this->activepager->setPage($this->page);
                $this->activepager->init();

                 /* code ends for paid and active transcations on home page */

                $getRequestId = $request->getParameter('requestId');
                $userRole = new UserRoleHelper();
                $message = $userRole->fetchBroadCastMessage($user_id,$user_detail->getFirst()->getEwalletType());
                $this->broadcast_message = false;
                if ($message) {
                    $this->broadcast_message = $message['0']['message']; //$this->getBroadcastMessage();
                }
                $this->merchantRequestId = '';
                $this->bankNames = $this->getBankListForRecharge();
                $this->bankNamesForCheckRecharge = $this->getBankListForCheckRecharge();
                $this->bankNamesForDraftRecharge = $this->getBankListForDraftRecharge();

                if (in_array($user_detail->getFirst()->getEwalletType(), sfConfig::get('app_ewallet_account_type'))) {
                    //lines to check the account balance
                    //code to migrate/reduce the number of question
                    //prompt message when question updated succesfuuly 10 to 3



               


        /* to show accnt details for home page */
                $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                 $this->arrAccountCollection = Doctrine::getTable('UserAccountCollection')->getAccounts($userId);
 /* code ends to show accnt details for home page */

                    if ($request->getParameter('questionflag')) { //
                        $this->getUser()->setFlash('notice', sprintf('Questions Updated Successfully'), false);
                    }

                    $userAccountCollectionObj = new UserAccountManager($user_id);

                    if (isset($getRequestId) && $getRequestId != '') {
                        $merchantRequestObj = new MerchantRequest();
                        $currency = $merchantRequestObj->getCurrency($getRequestId);
                        $account_id = $userAccountCollectionObj->getAccountIdByCurrency($currency);
                    } else {
                        $account_id = $user_detail->getFirst()->getMasterAccountId();
                        $currency = $userAccountCollectionObj->getCurrencyForAccount($account_id);
                    }
                    if (!$account_id) {
                        $currencyObj = Doctrine::getTable('CurrencyCode')->find($currency);
                        //   print "<pre>";
                        // print_r($currencyObj->toArray());exit;
                        $currency_display = $currencyObj->getCurrency() . "(" . $currencyObj->getCurrencyCode() . ")";
                        $this->getUser()->setFlash('error', sprintf('Please create a new account for Currency  ' . $currency_display . ' and recharge it from your nearest bank'));
                        $this->redirect('ewallet/addAccount');
                    }
                    $walletObj = new EpAccountingManager;
                    $walletDtailObj = $walletObj->getAccount($account_id);
                    $clBalance = $walletDtailObj->getClearBalance();
                    // $createObj = new adminManager();
                    //$clBalance = $createObj->getAccountBalance($user_detail);

                    if ($clBalance == 0) {
                        $Amt = Doctrine::getTable('EwalletSetting')->getChargeAmountLimit();
                        $Days = Doctrine::getTable('EwalletSetting')->getChargeDaysLimit();

                        //  $chargable_amount =
                        #sfLoader::loadHelpers('ePortal');
                        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                        $current_balance = format_amount('0', $currency, 1);
                        if ($request->hasParameter('paymentMode') &&
                                (
                                $request->getParameter('paymentMode') != sfConfig::get('app_payment_mode_option_credit_card')
                                || $request->getParameter('paymentMode') != sfConfig::get('app_payment_mode_option_interswitch')
                                || $request->getParameter('paymentMode') != sfConfig::get('app_payment_mode_option_etranzact')
                                )) {
                           $this->getUser()->setFlash('notice', sprintf('Your account number is ' . $walletDtailObj->getAccountNumber() . ' and your account balance is ' . $current_balance . '.<br> Please take print out of eWallet Certificate and recharge your account.' . $pinNotice));

    //                        if($changePin) {
    //                         $this->getUser()->setFlash('notice', 'An eWallet secure Pin has been mailed to you.<br>Please change your pin for secure and smooth transactions.');
    //                        $this->redirect('ewallet_pin/changePin');
    //                        }
                        }
                    }

                    //code to check a/c balance ends here
                }
                if ($request->getParameter('requestId') && $request->getParameter('requestId') != "" && ctype_digit($request->getParameter('requestId'))) {
                    $billObj = billServiceFactory::getService();

                    if ($billObj->isRequestBelongsToEwalletUser($request->getParameter('requestId'), 'merchantRequestId')) {
                        $transactionFlag = $billObj->putBill($request->getParameter('requestId'), $request->getParameter('paymentMode'));
                    } else {
                        $this->redirect('bill/invalidTransaction');
                    }


                    if ($changePin) {
                        $this->getUser()->setFlash('notice', 'An eWallet secure Pin has been mailed to you.<br>Please change your pin for secure and smooth transactions.');
                        $this->redirect('ewallet_pin/changePin');
                    }
                    //is bill allready paid if paid $transactionFlag=false else $transactionFlag=new transactionid
                    if ($transactionFlag) {
                        $this->redirect('bill/show?trans_num=' . $transactionFlag);
                    } else {
                        $this->redirect('bill/invalidAttempt');
                    }
                }
                
            } else {
                $this->redirect('@homepage');
            }
        } else {

            //to do redirect to homepage
            if ($request->getParameterHolder()->has('requestId')) {
                $request->setParameter('merchantRequestId', $request->getParameter('requestId'));
            }
            $request->setParameter('payType', 'ewallet');
            $request->setParameter('errMsg', 'Session timeout. Please login again.');
            $request->setParameter('payMode', 'ewallet');
            $this->forward('sfGuardAuth', 'redirectEwallet');
        }
    }

    public function executeOpenInfoBox() {
        $user_id = $this->getUser()->getGuardUser()->getId();
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($user_id);
        $userRole = new UserRoleHelper();
        $message = $userRole->fetchBroadCastMessage($user_id,$user_detail->getFirst()->getEwalletType());

        $this->broadcast_message = false;
        if ($message) {
            $this->broadcast_message = $message['0']['message'];
        }
        $this->setTemplate('openInfoBox');
        $this->setLayout(false);
    }
    // Function to show legal notice [Wp061]
    public function executeOpenLegalBox(sfWebRequest $request) {
        $user_id = $this->getUser()->getGuardUser()->getId();
        $userRole = new UserRoleHelper();
        $message = $userRole->fetchLegalMessage($user_id);
        $this->legal_message = false;
        if ($message) {
            $this->legal_message = $message['0']['message'];
        }
        $this->setTemplate('openLegalBox');
        $this->setLayout(false);
    }
    public function executeCreateSession(sfWebRequest $request) {
        if ($request->getParameter('check') == 1) {
            $this->getUser()->setAttribute('broadcast_msg_read', '1');
            //   print $this->getUser()->getAttribute('broadcast_msg_read');exit;
        }
    }

    //  function getBroadcastMessage()
    //  {
    //    $branchId='';
    //    $bankId='';
    //    $userGroup = "";
    //    $pfmHelperObj = new pfmHelper();
    //    $userGroup = $pfmHelperObj->getUserGroup();
    //    }

    function getBroadcastMessage() {
        $branchId = '';
        $bankId = '';
        $userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $userGroup = $pfmHelperObj->getUserGroup();
        //print_r($userGroup);
        //if user is admin
        if ($this->isValidUserToGetMessage($userGroup) == false)
            return false;

        //if user is bank admin
        if ($userGroup == sfConfig::get('app_pfm_role_bank_admin')) {
            $bankDetails = $pfmHelperObj->getUserAssociatedBankDetails();
            $bankId = $bankDetails['bank_id'];

            //null in case of bank admin
            $branchId = 'NULL';
            $activeMessages = Doctrine::getTable('BroadcastMessage')->getActiveMessageForUser($bankId, $branchId, 'admin');
        } else if ($userGroup == sfConfig::get('app_pfm_role_bank_branch_user')) {
            $bankDetails = $pfmHelperObj->getUserAssociatedBankDetails();
            $bankId = $bankDetails['bank_id'];
            //null in case of bank admin
            $branchId = $bankDetails['bank_branch_id'];

            $activeMessages = Doctrine::getTable('BroadcastMessage')->getActiveMessageForUser($bankId, $branchId);
        } else if ($userGroup == sfConfig::get('app_pfm_role_ewallet_user')) {
            //  $bankDetails  = $pfmHelperObj->getUserAssociatedBankDetails();
            // $bankId = $bankDetails['bank_id'];
            //null in case of bank admin
            // $branchId = $bankDetails['bank_branch_id'];

            $activeMessages = Doctrine::getTable('BroadcastMessage')->getActiveMessageForEwalletUser();
        }
        if ($activeMessages == false)
            return false;
        else
            return $activeMessages;
    }

    function isValidUserToGetMessage($userGroup) {
        if ($userGroup == sfConfig::get('app_pfm_role_bank_admin') || ($userGroup == sfConfig::get('app_pfm_role_bank_branch_user')) || ($userGroup == sfConfig::get('app_pfm_role_ewallet_user')))
            return true;
        else
            return false;
    }

    /*
      public function setTemplateValues($requestId){
      $this->merchantRequestId =$requestId;
      $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
      $transactionNumber =  $payForMeObj->generateTransactionNumber();
      $formdata['trans_num'] = $transactionNumber;
      $formdata['merchantRequestId'] = $this->merchantRequestId;
      $formdata['pay_mode'] = 'ewallet';
      $pfm_trans_num = $payForMeObj->saveTransaction($formdata);
      if($pfm_trans_num!=false){
      $billObj = billServiceFactory::getService();
      $transactionFlag = $billObj->addBill($this->merchantRequestId,$transactionNumber);
      $payForMeObj->updateMerchantRequest($formdata['merchantRequestId'],$formdata['pay_mode']);
      $this->formData = $payForMeObj->getTransactionRecord($pfm_trans_num);
      //get Param Description; as per Merchant Service
      $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
      $this->MerchantData = $ValidationRulesObj->getValidationRules($this->formData['merchant_service_id']);
      $walletDetails = $billObj->getEwalletAccountValues();
      $this->errorForEwalletAcount = '';
      if($walletDetails==false){
      $this->errorForEwalletAcount = "Invalid Ewallet Number.";
      }
      else{
      $this->accountObj = $walletDetails->getFirst();
      $this->userDetailObj = $walletDetails->getFirst()->getUserDetail()->getFirst();
      $this->accountBalance = $billObj->getEwalletAccountBalance($this->accountObj->getId());
      }
      }
      else {
      $this->redirect('paymentProcess/invalidAttempt');
      }
      } */

    public function executePaymentModeOptionFilteredBanks(sfWebRequest $request) {
        $bankMerchantObj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);
        switch ($request->getParameter('mode_id')) {

            case 1:
                $banks = $bankMerchantObj->findAllReleatedBanks($request->getParameter('merchant_id') , $request->getParameter('currency'));
                
                break;
            case 14:
                $banks = $bankMerchantObj->findAllReleatedBanksForCheckPayment($request->getParameter('merchant_id'), $request->getParameter('currency'));

                break;
            case 15:
                $banks = $bankMerchantObj->findAllReleatedBanksForDraftPayment($request->getParameter('merchant_id'), $request->getParameter('currency'));

                break;
        }
        $str = '<option value="">Please select Bank</option>';


        foreach ($banks as $bankMercahntRecord => $data) {

            $str .= '<option value="' . $data['Bank']['id'] . '" >' . $data['Bank']['bank_name'] . '</option>';
        }
        return $this->renderText($str);
    }

    public function executePaymentProcessing(sfWebRequest $request) {


        $this->merchantServiceArray = array('' => 'Please select Merchant Service');
        $this->from_date = "";
        $this->to_date = "";
        $this->custom_form_search = new CustomSearch_PaymentProcessingForm(null, array('acc_type' => array('' => 'Please Select Account Type', 'collection' => 'Collection', 'ewallet' => 'eWallet', 'receiving' => 'Receiving')), null);

        $this->custom_form_ewallet = new CustomPaymentProcessing_ewalletForm(null, array('acc_type' => 'ewallet'), null);
        $this->custom_form_receiving = new CustomPaymentProcessing_ewalletForm(null, array('acc_type' => 'receiving'), null);
        $this->custom_form_collection = new CustomPaymentProcessing_collectionForm(null, null, null);


        //$this->acct_type = array('' => 'Please Select Account Type', 'collection' => 'Collection', 'ewallet' => 'eWallet', 'receiving' => 'Receiving');
    }

    public function executeMerchantService(sfWebRequest $request) {
        $this->setTemplate(FALSE);
        $merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BANK);
        $str = $merchant_service_obj->getServices($request->getParameter('merchant_id'), $request->getParameter('merchant_service_id'));
        return $this->renderText($str);
    }

    public function executeAccountSummary(sfWebRequest $request) {
        $merchant_id = "";
        $merchant_service_id = "";
        $from_date = "";
        $to_date = "";
        $this->account_type = "";
        //variables for ewallet
        $account_no = "";
        $account_name = "";
        $currency = "";

        //anil added
        $masterAcctObj = new EpAccountingManager;
        $this->statementObj;

        if ($request->hasParameter('from_date_date')) {
            $start_date = $request->getParameter('from_date_date');
            if ($start_date != "") {
                $dateArray = explode("/", $start_date);
                if ($request->getParameter('account_type') == 'collection') {

                    $from_date = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[0] . "-" . $dateArray[1] . " 00:00:00"));
                } else {

                    $from_date = date('Y-m-d ', strtotime($dateArray[2] . "-" . $dateArray[0] . "-" . $dateArray[1]));
                }
                // date('Y-m-d',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]));
            }
        }
        if ($request->hasParameter('to_date_date')) {
            $end_date = $request->getParameter('to_date_date');
            if ($end_date != "") {
                $dateArray = explode("/", $end_date);
                if ($request->getParameter('account_type') == 'collection') {

                    $to_date = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[0] . "-" . $dateArray[1] . " 00:00:00"));
                } else {

                    $to_date = date('Y-m-d ', strtotime($dateArray[2] . "-" . $dateArray[0] . "-" . $dateArray[1]));
                }
                //  $this->to_date = $to_date;//date('Y-m-d',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]));
            }
        }
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        if ($request->hasParameter('account_type')) {
            $this->account_type = $request->getParameter('account_type');
        }

        if ($this->account_type == 'ewallet') {


            if ($request->hasParameter('ewal_acc_no')) {
                $account_no = $request->getParameter('ewal_acc_no');
            }
            if ($request->hasParameter('ewal_acc_name')) {
                $account_name = $request->getParameter('ewal_acc_name');
            }
            $currency_id  = '';
            if($account_no){
                $this->ewallet_currency_id = Doctrine::getTable('EpMasterAccount')->getEwalletCurrencyId($account_no);
                if(count($this->ewallet_currency_id->toArray()) > 0){
                    $currency_id  =    $this->ewallet_currency_id->getFirst()->getUserAccountCollection()->getFirst()->getCurrencyId();
                }
            }

            $this->statementObj = $masterAcctObj->getStatementInfo($from_date, $to_date, trim($account_no), trim($account_name), $this->account_type, $currency_id);
        }

        if ($this->account_type == 'receiving') {
            if ($request->hasParameter('rec_acc_no')) {
                $account_no = $request->getParameter('rec_acc_no');
            }
            if ($request->hasParameter('rec_acc_name')) {
                $account_name = $request->getParameter('rec_acc_name');
            }

            $this->statementObj = Doctrine::getTable('EpMasterAccount')->getStatementRecieving($from_date, $to_date, $account_no, $account_name, $this->account_type);
        }
         $currency = $request->getParameter('currency');
        if ($this->account_type == 'collection') {
            $this->payment_option = "";
            $bank = "";
            $merchant_id = "";
            $merchant_service = "";

            $pfmHelperObj = new pfmHelper();
            $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
            $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
            $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');


            if ($request->hasParameter('payment_option') && $request->getParameter('payment_option') != $paymentModebankId && $request->getParameter('payment_option') !=  $paymentModeCheckId && $request->getParameter('payment_option') !=  $paymentModeDraftId) {
                 $this->payment_option = $request->getParameter('payment_option');
            } else {
                  $this->payment_option = $request->getParameter('payment_option');
                if ($request->hasParameter('bank')) {
                    $bank = $request->getParameter('bank');
                }
                if ($request->hasParameter('merchant_id')) {
                    $merchant_id = $request->getParameter('merchant_id');
                }
                if ($request->hasParameter('merchant_service')) {
                    $merchant_service = $request->getParameter('merchant_service');
                }
            }

            $this->statementObj = Doctrine::getTable('ServiceBankConfiguration')->getStatement($this->payment_option, $merchant_id, $bank, $from_date, $to_date, $currency);
            $this->setTemplate('collectionAccountSummery');
        }

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));

        $this->pager->setQuery($this->statementObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeShow(sfWebRequest $request) {
        //  print "<pre>";
        //  print_r($request->getParameterHolder());
        $this->setLayout('layout_popup');
        $this->wallet_number = $request->getParameter('id');
        $this->account_type = $request->getParameter('account_type');
        $masterAcctObj = new EpAccountingManager;
        $this->from_date = NULL;
        $this->to_date = NULL;
        if ($request->getParameter('from_date') != "") {
            $this->from_date = $request->getParameter('from_date') . " 00:00:00";
        }
        if ($request->getParameter('showBal') != "") {
            $this->showBal = $request->getParameter('showBal');
        }
        else
            $this->showBal = 0;
        if ($request->getParameter('to_date') != "") {
            $this->to_date = $request->getParameter('to_date') . " 23:59:59";
        }
//        $paymentModeOptionName = '';
//        if($request->getParameter('payment_option') != ''){
//                $paymentModeOptionName = pfmHelper::getPMONameByPMId($request->getParameter('payment_option'));
//        }

        if($request->getParameter('payment_option')  != ''){
            $this->payment_option = $request->getParameter('payment_option');
        }

        $transactionDetails = Doctrine::getTable('EpMasterLedger')->getTransactionDetailsFinancial($this->wallet_number, $this->from_date, $this->to_date,'',$this->payment_option);
        $this->currency = 1;
        if ($this->account_type == 'ewallet') {
            $userObj = new UserAccountManager();
            $this->currency = $userObj->getCurrencyForAccount($this->wallet_number);
        }

        if ($this->account_type == 'collection') {
            $serviceBankConfigObj = Doctrine::getTable("ServiceBankConfiguration")->findByMasterAccountId($this->wallet_number);
            $this->currency = $serviceBankConfigObj->getFirst()->getCurrencyId();
        }

        if ($this->account_type == 'receiving') {


            $finInstActConfigObj = Doctrine::getTable("SplitAccountConfiguration")->findByMasterAccountId($this->wallet_number);
            $res = $finInstActConfigObj->toArray(); //print "<pre>";print_r($res);//exit;
            if (count($finInstActConfigObj)) {
                $this->currency = $finInstActConfigObj->getFirst()->getCurrencyId();
            } else {
                $finInstActConfigObj = Doctrine::getTable("FinancialInstitutionAcctConfig")->findByAccountId($this->wallet_number);
                $this->currency = $finInstActConfigObj->getFirst()->getCurrencyId();
            }
        }
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));
        //    print "<pre>";
        //  print_r($this->pager->getParameterHolder());
        $this->pager->setQuery($transactionDetails);
        $this->pager->setPage($this->page);
        $this->pager->setParameter('from_date', $this->from_date);
        $this->pager->setParameter('to_date', $this->to_date);
        $this->pager->setParameter('wallet_number', $this->wallet_number);
        $this->pager->init();
    }

    public function executeMerchantList(sfWebRequest $request) {
        $merchantObj = Doctrine::getTable('Merchant')->getAllMerchantsOrderByName();
        $str = '<option value="">Please select Merchant</option>';
        foreach ($merchantObj as $key => $value) {
            $str .= '<option value="' . $value->getId() . '" >' . $value->getName() . '</option>';
        }

        return $this->renderText($str);
    }

    public function executeMerchantBank(sfWebRequest $request) {
        $bankMerchantObj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);
        $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($request->getParameter('merchant_id'));

        $str = '<option value="">Please select Bank</option>';

        foreach ($bankMerchantListArray as $key => $value) {
            $str .= '<option value="' . $value['Bank']['id'] . '" >' . $value['Bank']['bank_name'] . '</option>';
        }
        return $this->renderText($str);
    }
    public function executeMerchantCurrency(sfWebRequest $request) {
        $bankMerchantListArray = Doctrine::getTable('ServiceBankConfiguration')->getMerchantCurrency($request->getParameter('merchant_id'));
        $str = '<option value="">Please select Currency</option>';

        foreach ($bankMerchantListArray as $key => $value) {
            $str .= '<option value="' . $value['currency_id'] . '" >' . $value['CurrencyCode']['currency'] . '</option>';
        }
        return $this->renderText($str);
    }

    public function executePaymentModeOption(sfWebRequest $request) {
        
        $serviceObj = Doctrine::getTable('ServicePaymentModeOption')->getPaymentOptionsForSummary($request->getParameter('merchant_service'));
//        echo "<pre>";
//        print_r($serviceObj->toArray());
//        exit;
        $str = '<option value="">Please select Payment Option</option>';

        foreach ($serviceObj as $key => $value) {
            $name = pfmHelper::getPMONameByPMId($value['payment_mode_option_id']);
            $str .= '<option value="' . $value['PaymentModeOption']['id'] . '" >' . $name . '</option>';
        }
        return $this->renderText($str);
    }

    public function executeScheduleMerchantAcctReconciliation(sfWebRequest $request) {
        $merchantObj = Doctrine::getTable('Merchant')->findAll();
        $this->merchantArray = array();
        $this->merchantArray[''] = 'Please select Merchant';
        foreach ($merchantObj as $value) {
            $this->merchantArray[$value->getId()] = $value->getName();
        }
        $this->merchantServiceArray = array('' => 'Please select Merchant Service');
        $this->paymentModeOptionArray = array('' => 'Please select Payment Mode Option');
        $this->swap_schedule = array('weekly' => 'Weekly', 'monthly' => 'Monthly', 'immediate' => 'Immediate');
        $this->form = new MerchantAcctReconcilationSchForm();
    }

    public function executeSaveSchedule(sfWebRequest $request) {
        if (($request->hasParameter('merchant_id')) && ($request->hasParameter('merchant_service_id')) && ($request->hasParameter('reconcile')) && ($request->hasParameter('payment_mode_option_id'))) {
            $merchant_id = $request->getParameter('merchant_id');
            $merchant_service_id = $request->getParameter('merchant_service_id');
            $payment_mode_option_id = $request->getParameter('payment_mode_option_id');

            //chk if already scheduled
            $count = $this->chkIfAlreadyScheduled($merchant_service_id, $payment_mode_option_id);

            if (!$count) {
                $schedule = $request->getParameter('reconcile');
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                $next_swap = getNextSwap($schedule);
                $reconcileObj = new MerchantAcctReconcilationSchedule();
                $reconcileObj->setMerchantId($merchant_id);
                $reconcileObj->setMerchantServiceId($merchant_service_id);
                $reconcileObj->setPaymentModeOptionId($payment_mode_option_id);
                $reconcileObj->setSchedule($schedule);
                $reconcileObj->setLastSwap(NULL);
                $reconcileObj->setNextSwap($next_swap);
                $reconcileObj->setSwapStatus('0');
                $reconcileObj->save();
                $schedule_id = $reconcileObj->getId();
                // if($schedule == "immediate") {
                $taskId = EpjobsContext::getInstance()->addJob('reconcilation', 'GetSwapAccounts', array('schedule_id' => $schedule_id), -1, NULL, $next_swap);
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
                // }
                $this->getUser()->setFlash('notice', sprintf('Schedule Saved Successfully'), false);
            } else {
                $this->getUser()->setFlash('error', sprintf('Schedule already exists for the selected Merchant and Merchant Service'), false);

                //  echo "";
            }
            $this->forward('admin', 'scheduleListing');
        }
    }

    public function chkIfAlreadyScheduled($merchant_service_id, $payment_mode_option_id) {
        $count = Doctrine::getTable('MerchantAcctReconcilationSchedule')->chkIfAlreadyScheduled($merchant_service_id, $payment_mode_option_id);
        // print $count;exit;
        if ($count) {
            return $count;
        }
        return null;
    }

    public function executeScheduleListing(sfWebRequest $request) {
        $scheduleObj = Doctrine::getTable('MerchantAcctReconcilationSchedule')->createQuery('a');

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('MerchantAcctReconcilationSchedule', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($scheduleObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    function getBankListForRecharge() {
        $bankList = Doctrine::getTable('ServiceBankConfiguration')->getBankListForRecharge();
        return $bankList;
    }

    function getBankListForCheckRecharge() {
        $bankList = Doctrine::getTable('ServiceBankConfiguration')->getBankListForCheckRecharge();
        return $bankList;
    }

    function getBankListForDraftRecharge() {
        $bankList = Doctrine::getTable('ServiceBankConfiguration')->getBankListForDraftRecharge();
        return $bankList;
    }

    public function executeEWalletUserDeactive(sfWebRequest $request) {
        $this->deactivate = $request->getParameter('deactivate');

    }
}
