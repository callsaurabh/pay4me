<?php
// use_helper('Form');
use_javascript('jquery-ui.min.js');
use_javascript('general.js');
?>


<?php
echo $form->renderGlobalErrors();
include_partial('cms/leftBlock', array('title' => 'e-Wallet Registration'))
?> <?php  //  echo  $form; die; ?>
<div id="wrapperInnerRight">
    <div id="innerImg">
        <?php echo image_tag('/img/new/img_about_pay4bill.jpg', array('alt' => 'About Pay4me', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
    </div>

    <?php // echo include_partial('signuplabel', array('applingCountry' => $applingCountry)); ?>
        <form name="signup" id="signup"  method="post" action="<?php echo url_for('openId/submit') ?>" >

            <div class="feedbackForm">
                <div class="header">Registration Form</div>
                <div class="fieldName"><?php echo $form['name']->renderLabel(); ?><span class="required"> *</span></div>
                <div class="fieldWrap">
                    <label><?php echo $form['name']->render(); ?></label>
                    <div class="cRed" id="err_name"><?php echo $form['name']->renderError(); ?></div>
                </div>
                <div class="fieldName"><?php echo $form['lname']->renderLabel(); ?> <span class="required">*</span></div>

                <div class="fieldWrap">
                    <label><?php echo $form['lname']->render(); ?></label>
                    <div class="cRed" id="err_lastname"><?php echo $form['lname']->renderError(); ?></div>
                </div>

                <div class="fieldName"><?php echo $form['email']->renderLabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap"><label><?php echo $form['email']->render(); ?></label>
                    <div class="cRed" id="err_email"><?php echo $form['email']->renderError(); ?></div>
                </div>



                <div class="fieldName"><?php echo $form['mobileno']->renderLabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap">
                    <label><?php echo $form['mobileno']->render(); ?><span class="desc"><br />
                            (format: + [10-14] digit no,eg: +1234567891 )</span></label>
                    <div class="cRed" id="err_mobile_no"><?php echo $form['mobileno']->renderError(); ?></div>
                </div>


            <?php if (settings::isMultiCurrencyOn()) {
            ?>
<!-- PAYM-2032 -->
<!--                <div class="fieldName" ><?php //echo $form['currency_id']->renderLabel(); ?><span class="required">*</span></div>
                <div class="fieldWrap">
                    <label>
                    <?php //echo $form['currency_id']->render(); ?>
                </label>
                <div class="cRed" id="err_modecurrency"><?php //echo $form['currency_id']->renderError(); ?></div>
            </div>-->
<!-- End of PAYM-2032 -->
            <?php } ?>
                <div class="fieldName">&nbsp;</div>


                <div  id="submitdiv">
                    <label>
                    <?php if ($form->isCSRFProtected()) : ?>
                    <?php echo $form['_csrf_token']->render(); ?>
                    <?php endif; ?>
                        <input type="submit" class="formSubmit" name="submit" class="button" value="Continue" >
                    </label>
                </div>
                <div class="fieldWrap" id ="progBarDiv"  style="display:none;"></div>

                <div class="fieldName">&nbsp;</div>
                <div class="fieldWrap"><span  class="message">[*] Fields marked as * are Mandatory.</span></div>
            </div>

        </form>
        <p>&nbsp;</p>
    </div>

    