<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class authModuleActions extends sfActions {

    public function executeOpenIdLogin(sfWebRequest $request) {

    }

    public function executeOpenIdDetails(sfWebRequest $request) {

        $screenName = null;
        $obj = new EpOpenIDExt();
        $identType = $request->getParameter('openid_identifier');
        $appVars = $request->getParameter('appVars');
        $this->getUser()->getAttributeHolder()->remove('appVars');
        $this->getUser()->setAttribute('appVars', $appVars);
        $referer = $request->getReferer();
        if (strpos($referer, 'report/getReceipt') !== false) {            
            $this->getUser()->setAttribute('login_direct', base64_encode($referer));
        }else if (strpos($referer, 'visaArrival/searchFlightDetail') !== false) {
            $this->getUser()->setAttribute('login_direct', base64_encode($referer));
        }

        if ($request->getParameter('srcname') != "") {
            $screenName = $request->getParameter('srcname');
        }
        if (!isset($_GET['openid_mode'])) {
            EpOpenIDExt::getOpenidProvider($identType, $screenName);
        }

        try {
            if (isset($_GET['openid_mode']) && $_GET['openid_mode'] != "cancel") {
                //if(isset($_GET['openid_mode'])) {

                $userDetails = EpOpenIDExt::getOpenidProviderDetails($_GET, $identType);
                $this->getUser()->setAttribute('data', $userDetails);
                //if($identType != 'pay4me') {
?>
                <script>
                    window.opener.location = 'authModule/authUserDetails';
                    self.close();
                </script>
<?php

                exit;
                //}
                // $this->redirect('authModule/preAuthentication');
                //$this->redirect('authModule/authUserDetails');
            }
            if (isset($_GET['openid_mode']) && $_GET['openid_mode'] == "cancel") {
                //if($identType != 'pay4me') {
?>
                <script>
                    self.close();
                </script>
<?php

                exit;
                //}
                //throw new Exception("User has canceled authentication!");
            }
        } catch (Exception $e) {
            // throw new Exception("your authentication has been failed due to some network problem !");
            // $this->getUser()->setFlash('notice','<b><font color=red>'.$e->getMessage().'</font></b>');
            $this->redirect('authModule/openIdLogin');
        }
        exit;
    }

    public function executePreAuthentication(sfWebRequest $request) {
        if ($request->getParameter('email')) {
            $data = $this->getUser()->getAttribute('data');
            if ($request->getParameter('email') == $data[DbtoOpendidAttrMap::$EMAIL_KEY]) {
                $request->setParameter('authStatus', 'pass');
                $this->forward($this->moduleName, 'authUserDetails');
            } else {
//        $this->getUser()->setFlash('notice','<b><font color=red>You are not authorized, Please try again !</font></b>');
                $errorMsgObj = new ErrorMsg();
                $errorMsg = $errorMsgObj->displayErrorMessage("E044", '001000');
                $this->getUser()->setFlash('notice', $errorMsg);
                $this->redirect('authModule/openIdLogin');
            }
        }
    }

    public function executeAuthUserDetails(sfWebRequest $request) { 
        $authStatus = $request->getParameter('authStatus');
        $authStatus = 'pass';
        if ($authStatus == 'pass') {
            $data = $this->getUser()->getAttribute('data');
            if (empty($data)) {
                $this->redirect('welcome/index');
            }
            
            $nameArr =array();
            if(isset($data[DbtoOpendidAttrMap::$FULL_NAME_KEY])){
                $nameArr = explode(" ", $data[DbtoOpendidAttrMap::$FULL_NAME_KEY]);
            }
            if (count($nameArr) > 1) {
                $this->lname = $nameArr[count($nameArr) - 1];
                unset($nameArr[count($nameArr) - 1]);
                $this->fname = implode(" ", $nameArr);
            } else {
                $this->fname = $data[DbtoOpendidAttrMap::$FIRST_NAME_KEY];
                $this->lname = $data[DbtoOpendidAttrMap::$LAST_NAME_KEY];;
            }
           
            //PAYM-2032
            if(isset($_SESSION['s_paytype']) && isset($_SESSION['s_userType']) && isset($_SESSION['s_merchantRequestId'])) {
               $payType = $_SESSION['s_paytype'];
               $userType = $_SESSION['s_userType'];
               $merchantRequestId = $_SESSION['s_merchantRequestId'];
               
               $this->getUser()->setAttribute('merchantRequestId', $merchantRequestId);
               $this->getUser()->setAttribute('payType', $payType);
               $this->getUser()->setAttribute('userType', $userType);
            }
               $data['fname'] = $this->fname;
               $data['lname'] = $this->lname;
               $data['openIdAuth'] = "https://plus.google.com/".$_SESSION['gplusdata']['id'];
               $this->getUser()->setAttribute('openId', $data);
               $this->redirect('openId/authUserDetails');
               exit;
               // End of PAYM-2032                   
            
            
            $userCount = Doctrine::getTable('sfGuardUser')->chk_username($data[DbtoOpendidAttrMap::$USER_KEY]);
            $appVars = $this->getUser()->getAttribute('appVars', '0');
            if ($userCount != 0) {
                $this->redirect('authModule/integSfUserDetails');
            }
            
            $this->email = $data[DbtoOpendidAttrMap::$EMAIL_KEY];
//                $openIdResArr= array('email'=> $this->email,'name'=>$this->fname, 'lname'=>$this->lname );
            $this->token = $data[DbtoOpendidAttrMap::$USER_KEY];
            $this->getUser()->setAttribute('data',$data);
                       
            $this->executeIndex($request);
        } else {
            $this->getUser()->setFlash('notice', '<b><font color=red>You are not authorized, Please try again !</font></b>');
            $this->redirect('authModule/openIdLogin');
        }
    }

    public function executeIntegSfUserDetails(sfWebRequest $request) {
        $data = $this->getUser()->getAttribute('data');

        $fname = $request->getParameter('fname');
        $lname = $request->getParameter('lname');
        $address = $request->getParameter('address');
        $mobile_frefix = $request->getParameter('mobile_frefix');
        $mobile = $request->getParameter('mobile');
        $mobileNo = $mobile_frefix . $mobile;
        $uname = $data[DbtoOpendidAttrMap::$USER_KEY];
        $email = $request->getParameter('email');

        if ($request->isMethod('post')) {
            unset($_SESSION['address']);
            $_SESSION['address'] = $address;
        } else {
            $_SESSION['address'] = NULL;
        }
        $appVars = $this->getUser()->getAttribute('appVars', '0');
        //checking if mobile number already exists
        if ($mobile != null) {
            $mobile = trim($mobile); //[\]+()-]
            $mobile = str_replace("[", "", $mobile);
            $mobile = str_replace("]", "", $mobile);
            $mobile = str_replace("+", "", $mobile);
            $mobile = str_replace("(", "", $mobile);
            $mobile = str_replace(")", "", $mobile);
            $mobile = str_replace("-", "", $mobile);
            $mobile = str_replace(" ", "", $mobile);
            $mobile = "+" . $mobile;
        }

        //PAYM-2032
        $countMobile = 0; //Doctrine::getTable('UserDetail')->checkUniqueMobileNumber('', $mobile);
        $countEmail = 0; //Doctrine::getTable('UserDetail')->checkUniqueEmail('', $email);
        //End of PAYM-2032
        
        if ($countMobile > 0) {

            $errorMsgObj = new ErrorMsg();
            $errorMsg = $errorMsgObj->displayErrorMessage("E045", '001000');

            $this->getUser()->setFlash('notice', $errorMsg);
            $this->redirect('authModule/authUserDetails');
        }


        if ($countEmail > 0) {

            $errorMsgObj = new ErrorMsg();
            $errorMsg = $errorMsgObj->displayErrorMessage("E055", '001000');

            $this->getUser()->setFlash('notice', $errorMsg);
            $this->redirect('authModule/authUserDetails');
        }

        if (!isset($uname) || is_null($uname)) {
            $userCount = 1;
            if ($this->getUser() && $this->getUser()->isAuthenticated()) {
                $userId = $this->getUser()->getGuardUser()->getId();
            } else {

                return sfContext::getInstance()->getController()->redirect('@adminhome');
            }
        } else {
            $userCount = Doctrine::getTable('sfGuardUser')->chk_username($data[DbtoOpendidAttrMap::$USER_KEY]);

            if ($userCount == 0) {
                $userId = Doctrine::getTable('UserDetail')->createUser($data[DbtoOpendidAttrMap::$USER_KEY], $data[DbtoOpendidAttrMap::$EMAIL_KEY], $fname, $lname, $mobile, $address);
            } else {
                $userDet = Doctrine::getTable('sfGuardUser')->findBy('username', $data[DbtoOpendidAttrMap::$USER_KEY]);
                $userId = $userDet->getFirst()->getId();
            }
        }
        //User sigin functionality

        $userObj = Doctrine::getTable('sfGuardUser')->find($userId);

        //start:: Vineet validate payment rights of user
        //PAYM-2032
//        if ($userObj->getUserDetail()->getPaymentRights() == '1') {
//            $this->redirect("paymentProcess/blockUserMsg");
//        }
        // End of PAYM-2032
        //end:: Vineet validate payment rights of user
        //start: Kuldeep
        if ($userObj->getIsActive() == 0) {
            $errorMsg = 'You are not authorize to login!!';
            $this->getUser()->setFlash('notice', $errorMsg);
            $this->redirect('pages/index');
        }
        //End:Kuldeep
        $this->getUser()->signin($userObj, false,true);

        $this->logOpenIDUserLoginAuditDetails($userObj->getUsername(), $userId);

        $this->getUser()->getAttributeHolder()->remove('data');
        if (!$this->getUser()->getAttributeHolder()->has('loginOnOrderRequest')) {
            $this->getUser()->getAttributeHolder()->remove('requestId');
        }

        //GETTING REQUEST ID SESSION
        $requestId = $this->getUser()->getAttribute('requestId');
        

        if ($requestId) {
            //check if cart empty
            //add application if cart is coming from NIS
            $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($requestId);
            
            //get Application on NIS
            $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($orderRequest->getRetryId());

            $application_type = $appDetails[0]['app_type'];
            if ($application_type == 'passport') {
                $application_type = 'p';
            } else if ($application_type == 'visa') {
                $application_type = 'v';
            } else if ($application_type == 'freezone') {
                $application_type = 'v';
            } else if ($application_type == 'vap') {
                $application_type = 'vap';
            }

            $application_id = $appDetails[0]['id'];

            $errorMsgObj = new ErrorMsg();

            ## If application is already processed for refund or chargeback
            $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($application_id, $application_type);
            if (count($isRefunded) > 0) {
                $errorMsg = $errorMsgObj->displayErrorMessage("E049", '001000', array('isRefunded' => $isRefunded[0]['action']));
                $this->getUser()->setFlash('notice', $errorMsg);
                $this->redirect('cart/list');
            }
            
            $cartObj = new cartManager();
            $arrCartItems = $this->getUser()->getCartItems();
            if (count($arrCartItems) == 0) {
                $addToCart = $cartObj->addToCart($application_type, $application_id);
            } else {
                $flg = false;
                foreach ($arrCartItems as $items) {
                    if ($items->getAppId() != $application_id) {
                        $flg = true;
                    }
                    if ($flg) {
                        $cartValidationObj = new CartValidation();
                        $addToCart = $cartObj->addToCart($application_type, $application_id);
                        if ($addToCart == 'common_opt_not_available') {
                            $this->getUser()->setFlash("notice", sfConfig::get("app_cart_err_message_save"), true);
                            $this->redirect('cart/list');
                        } else if ($addToCart == 'capacity_full') {
                            //start:Kuldeep for check cart capacity
                            $arrCartCapacity = array();
                            $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                            if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                                $arrCartCapacity = $arrCartCapacityLimit;
                            }
                            //end:Kuldeep
                            $msg = $arrCartCapacity['cart_capacity'] . " application";
                            $errorMsg = $errorMsgObj->displayErrorMessage("E047", '001000', array("msg" => $msg));
                            $this->getUser()->setFlash('notice', $errorMsg);
                            $this->redirect('cart/list');
                        } else if ($addToCart == 'amount_capacity_full') {
                            //start:Kuldeep for check cart capacity
                            $arrCartCapacity = array();
                            $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                            if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                                $arrCartCapacity = $arrCartCapacityLimit;
                            }
                            //end:Kuldeep
                            $errorMsg = $errorMsgObj->displayErrorMessage("E048", '001000', array("msg" => $arrCartCapacity['cart_amount_capacity']));
                            $this->getUser()->setFlash("notice", $errorMsg);
                            $this->redirect('cart/list');
                        } else if ($addToCart == "application_edit") {
                            $errorMsg = $errorMsgObj->displayErrorMessage("E058", '001000', array("msg" => ''));
                            $this->getUser()->setFlash("notice", $errorMsg);
                        }else if ($addToCart == "application_rejected") {
                            $errorMsg = $errorMsgObj->displayErrorMessage("E060", '001000', array("msg" => ''));
                            $this->getUser()->setFlash("notice", $errorMsg);
                        } else if($addToCart == "only_vap_allowed"){
                            if(strtolower($appDetails[0]['app_type']) != 'vap'){
                                $msg = ucfirst($appDetails[0]['app_type']).' application can not be added into the cart with Visa on Arrival application.';
                            }else{
                                $msg = 'Visa on Arrival application can not be added into the cart with another type of application such as passport, visa, .. etc.';
                            }
                            $this->getUser()->setFlash('notice', sprintf($msg));
                            $this->redirect('cart/list');
                        }

                        break;
                    }
                }
            }

            //generating bill
            $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($requestId);
            $billObj = new billManager();
            $billObj->addBill($orderRequest->getOrderRequestId());
            if ($appVars) {
                $redirectObj = new NisManager();
                $redirectObj->redirectoNisPage($appVars);
                //$this->redirectoNisPage($appVars);
            }
            
            $this->redirect('paymentGateway/gotoApplicantVault?requestId=' . $requestId);
//      switch(sfConfig::get('app_active_paymentgateway')) {
//        case 'authorizeNet':
//          $this->redirect('paymentProcess/orderConfirmation?requestId='.$requestId);
//          break;
//        case 'vbv':
//          $this->redirect('paymentGateway/gotoApplicantVault?requestId='.$requestId);
//          break;
//        case 'payEasy':
//        case 'grayPay':
//          $vbv_active  = settings::isVbvActive();
//          if($vbv_active) {
//            $this->redirect('paymentGateway/gotoApplicantVault?requestId='.$requestId);
//          }
//          else {
//            $this->redirect('paymentGateway/new?requestId='.$requestId);
//          }
//          break;
//        default:
//          throw new Exception("Unable to handle request !");
//          break;
//      }
        }
        //    $this->redirect('paymentProcess/orderConfirmation?requestId='.$requestId);
        //$this->redirect('vbv_configuration/vbvForm?requestId='.$requestId);
        else {
            if ($appVars) {
                $redirectObj = new NisManager();
                $redirectObj->redirectoNisPage($appVars);
                //$this->redirectoNisPage($appVars);
            }
//      print_r($this->getUser()->getAttributeHolder()->getAll());exit;
//      print $this->getUser()->getAttribute('login_direct');exit;
            if (($this->getUser()->hasAttribute('login_direct')) && ($this->getUser()->getAttribute('login_direct') != '')) {//print "fdfdf";exit;
                $url = base64_decode($this->getUser()->getAttribute('login_direct'));
                $this->getUser()->setAttribute('login_direct', '');
                $this->redirect($url);
                // echo "in";exit;
            }

            $this->redirect('welcome/index');
        }
    }

    public function logOpenIDUserLoginAuditDetails($uname, $id) {
        //Log audit Details
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_SECURITY,
                        EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRST_LOGIN, array('username' => $uname)),
                        null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    /**
     * @Author: Vineet Malhotra
     * @param: null
     * @return: logout/session path
     * @desc: reset session
     */
    public function executeResetSession() {
        echo sfUser::ATTRIBUTE_NAMESPACE;
        $this->settemplate(null);
        die;
    }
  public function executeIndex(sfWebRequest $request)
  {
    $this->setTemplate('index');
    $openIdResArr= array('email'=> $this->email,'name'=>$this->fname, 'lname'=>$this->lname );
    $chkUserExisis = Doctrine::getTable('UserDetail')->findByEmail($openIdResArr['email']);
    if($chkUserExisis->count()){
        $chkUserExistsWithOpenId = Doctrine::getTable('sfGuardUser')->findByUsername($openIdResArr['email']);
        if($chkUserExistsWithOpenId->count()){
            //OpenID User, user should be logged in here
            $this->getUser()->signin($chkUserExistsWithOpenId->getFirst(),false,true);
            $this->redirect('@homepage');
        }else{
            //user is already a ewallet user(not openid), then throw error
            $this->getUser()->setFlash('error', sprintf('User already registered as E-waller User'));
            $this->redirect('@homepage');
        }

    }else{
        $this->form = new OpenIdUserSignUpForm(array(), array('email'=> $openIdResArr['email'],'name'=> $openIdResArr['name'],'lname'=> $openIdResArr['lname'],'mobileNo'=>''));
    }

  }

   public function executeSubmit(sfWebRequest $request )
  {
       $data = $this->getUser()->getAttribute('data');
       $subject= 'eWallet Confirmation Mail';
       $partialName = 'account_details';
       $this->form = new OpenIdUserSignUpForm();
       $tempArray=$request->getPostParameters();
       $resultArray=$tempArray['payOptions'];

        array_pop($tempArray);
        array_pop($tempArray);
        $finalArr= array_merge($resultArray,$tempArray);
        $this->form->bind( $finalArr);
//
        if ($this->form->isValid()){
            
       $sfGuarduserObj = new sfGuardUser();
       $sfGuarduserObj->setUsername($request->getParameter('email'));
       $sfGuarduserObj->setIsActive(true);
       $sfGuarduserObj->setPassword($data[DbtoOpendidAttrMap::$USER_KEY]);
       $sfGuarduserObj->setIsSuperAdmin(0);
       $sfGuarduserObj->save();

        $userTypeToCreate = sfConfig::get('app_pfm_role_ewallet_user');

        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate);
        $sfGroupId = $sfGroupDetails->getFirst()->getId();
        $sfGuardUsrGrp = new sfGuardUserGroup();
        $sfGuardUsrGrp->setGroupId($sfGroupId);
        $sfGuardUsrGrp->setUserId($sfGuarduserObj->id);
        $sfGuardUsrGrp->save();
        
       $userdetailobj = new UserDetail();
       $userdetailobj->setUserId($sfGuarduserObj->id);
       $userdetailobj->setName($request->getParameter('name'));
       $userdetailobj->setLastName($request->getParameter('lname'));
       $userdetailobj->setEmail($request->getParameter('email'));
       $userdetailobj->setMobileNo($request->getParameter('mobileno'));
       $userdetailobj->setSendSms("gsmno");
       $userdetailobj->setUserStatus(1);
       $userdetailobj->setKycStatus(0);
       $userdetailobj->save();



       $taskId = EpjobsContext::getInstance()->addJob('SendMailConfirmation', $this->moduleName . "/sendEmail", array('userid' => $sfGuarduserObj->id,'uname'=>$userdetailobj->name, 'subject' => $subject, 'partialName' => $partialName, ));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                $pinObj = new pin();
                $pinObj->getPin($sfGuarduserObj->id, $userdetailobj->name, $userdetailobj->email);
       $this->getUser()->setFlash('notice', sprintf('User Added'));
       $this->getUser()->signin($sfGuarduserObj,false);
       $this->redirect('@homepage');
        }else{
            $this->setTemplate('index');
        }
   }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $uName = $request->getParameter('uname');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendConfirmationEmailToOpenIdUser($userid, $subject, $partialName, $uName);
        return $this->renderText($mailInfo);
    }
    
    //PAYM-2032
    public function executeGooglePlusAuth(sfWebRequest $request)
    {           
        require_once 'src/apiClient.php';
        require_once 'src/contrib/apiPlusService.php';
        require_once 'src/contrib/apiOauth2Service.php';
        
        session_start();

        /* On click on cancel button*/
        if($_GET['error'] == 'access_denied'){
            return $this->renderText('<script>self.close();</script>');
        }
        /* End of click on cancel button*/
        
        
        $client = new apiClient();
        $client->setApplicationName("pay4me");
        $client->setScopes(array('https://www.googleapis.com/auth/plus.me','https://www.googleapis.com/auth/userinfo.email'));
        $plus = new apiPlusService($client);
        $oauth2 = new apiOauth2Service($client);
        if (isset($_REQUEST['logout'])) {
          unset($_SESSION['access_token']);
        }
        
        if (isset($_GET['code'])) {
			  $client->authenticate();
			  $_SESSION['access_token'] = $client->getAccessToken();
			  //header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
        }

        if (isset($_SESSION['access_token'])) {
          $client->setAccessToken($_SESSION['access_token']);
        }

        if ($client->getAccessToken()) {
          $me = $plus->people->get('me');

          $optParams = array('maxResults' => 100);
          $activities = $plus->activities->listActivities('me', 'public',$optParams);          
          // The access token may have been updated lazily.
          $_SESSION['access_token'] = $client->getAccessToken();
        } else {
          $authUrl = $client->createAuthUrl();
          $_SESSION['authURL'] = $authUrl;
        }
        $_SESSION['gplusdata']=$me;
        $userData = $oauth2->userinfo->get();
        //print_r($userData); die;
        $email = $userData['email'];
        /*$userDetails = Doctrine::getTable('userDetail')->findByEmail($email)->toArray();
        if(isset($userDetails) && !empty($userDetails))
        {
            $userID = $userDetails[0]['user_id'];
            $sfUserDetails = Doctrine::getTable('sfGuardUser')->find($userID)->toArray();
            $userName = $sfUserDetails['username'];
        }*/
        
        $userDataObj =  Doctrine::getTable('UserDetail')->getUserDetails($email);                    
        //echo '<pre>';print_r($userData); die;
        $userName = $userDataObj[0]['sfGuardUser']['username'];
        if(!empty($userName))
        {
            $data['openid_identity'] = $userName;
        } else {
            $data['openid_identity'] = $email;
        }
        $data['openid_ext1_value_namePerson_first'] = $userData['given_name'];
        $data['openid_ext1_value_namePerson_last'] = $userData['family_name'];
        $data['openid_ext1_value_contact_email'] = $email;
        
        $mp = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$googleConfig)->getDbDetails($data);
        
        $this->getUser()->setAttribute('data', $mp);
        
        unset($_SESSION['s_paytype']);
        unset($_SESSION['s_userType']);
        unset($_SESSION['s_merchantRequestId']);
        
        $base64 = base64_decode($_REQUEST['state']);
        $requestArray = json_decode($base64);
        
        $_SESSION['s_paytype'] = $requestArray->payType;
        $_SESSION['s_userType'] = $requestArray->userType;
        $_SESSION['s_merchantRequestId'] = $requestArray->merchantRequestId;
        $this->setTemplate('authUserGooglePlus');
    }
    // End of PAYM-2032

}
