<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_helper('Form') ?>
<?php use_helper('ePortal');

?>



<div class="wrapForm2">
    <?php echo  ePortal_legend(__('Search Results For Biller')); ?>
</div>
<br/>
<div class="wrapForm2">

<?php
echo ePortal_legend(__('Biller Identification Details'));
echo formRowFormatRaw(__('Biller Number'),$billerNumber); // WP032
/*Dinesh Kumar
  * To convert the currency in KOBO , Only For Display (fn , Format_amount eportalhelper)
*/
echo formRowComplete(__('Amount'),format_amount($getValues['amount'],$currencyId,$convert_to_kobo));// WP032 FS#29732
?>

</div>
<div class="wrapForm2">

    <?php
    if($errorMsg != '')
    print  "<div id='flash_error' class='error_list'><span>".$errorMsg."</span></div>";
    ?>

    <form action="<?php echo url_for('biller/billerSearchResult') ?>" method="post"  class="dlForm multiForm" id="payment_form" name="payment_form" >

   <?php echo ePortal_legend(__('User Details'));?>
   <?php // WP032
   foreach($rules as $key=>$val){ //'.$getValues[$val].'
       $valRule = '-';
       $hiddenVal = "";

       if(array_key_exists($val,$getValues)) {           
           $valRule = $getValues[$val];
           $hiddenVal = $getValues[$val];
       }
        /*Dinesh Kumar
            * To convert the currency in KOBO , Only For Display (fn , Format_amount eportalhelper)
        */  
       
       echo formRowFormatRaw($key,$valRule);
       ?>
       <input type="hidden" name="<?php echo $val; ?>" id="<?php echo $val; ?>" value="<?php echo $hiddenVal ;?>" >
       <?php }  ?>



        <input type="hidden" name="transaction_num" id="transaction_num" value="<?php echo $getValues['transaction_num'];?>">
        <input type="hidden" name="item_num" id="item_num" value="<?php echo $getValues['item_num'];?>">
        <input type="hidden" name="merchant_svc_id" id="merchant_svc_id" value="<?php echo $getValues['merSvcId'];?>">
        <input type="hidden" name="name" id="name" value="<?php echo $getValues['name'];?>">
        <input type="hidden" name="currency" id="name" value="<?php echo $getValues['currency'];?>">
        <input type="hidden" name="version" id="version" value="<?php echo $getValues['version'];?>">
        <input type="hidden" name="currency_id" id="currency_id" value="<?php echo $currencyId;?>">
        <input type="hidden" name="payment_mode" id="payment_mode" value="<?php echo $payment_mode; ?>"> <!--Change Payment MODe -->
        <input type="hidden" name="check_number" id="check_number" value="<?php echo $checkNum; ?>">
        <input type="hidden" name="account_number" id="account_number" value="<?php echo $accountNum; ?>">
        <input type="hidden" name="draft_number" id="draft_number" value="<?php echo $checkNum; ?>">
        <input type="hidden" name="sort_code" id="sort_code" value="<?php echo $sortCode; ?>">
       
        
        <?php echo formRowComplete(__('Amount Paid').'<sup>*</sup>', getCurrencyImage($currencyId).'<input type="text" name="amount_paid" id="amount_paid" value="" maxlength=10 >','Eg: 1000000.50','','err_amount_paid',''); ?>
        <?php echo formRowComplete(__('Confirm Amount Paid').'<sup>*</sup>', getCurrencyImage($currencyId).'<input type="text" name="confirm_amount_paid" id="confirm_amount_paid" value="" maxlength=10 >','','','err_confirm_amount_paid',''); ?>



        <div class="divBlock">
            <center>
                &nbsp; <?php  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('biller/index?billerService='.$getValues['merSvcId']).'\''));?>
                <input type="button" value="<?php echo __('Pay')?>" class="formSubmit" onClick="validateUserForm(1);" />
            </center>
        </div>


    </form>

</div>



<script>
     
    function validateUserForm(submit_form)
    {
        var err = 0;
        //var reg = /^([0-9])+$/;
         //var reg = /^[-]?\d*\.?\d(1,2)*$/;
         var reg =/^[+]?\d{1,10}(\.\d{1,2})?$/;
       
        if($('#amount_paid').val() == "")
        {
            $('#err_amount_paid').html("Please enter Amount Paid");
            err = err+1;
        }else if ($('#amount_paid').val() < 1){
           $('#err_amount_paid').html("Please enter valid Amount Paid");
            err = err+1

        }

        else if( $('#amount_paid').val() != ""   && reg.test($('#amount_paid').val()) == false ) {
            $('#err_amount_paid').html("Please enter valid Amount Paid");
            err = err+1

        } else {
            $('#err_amount_paid').html("");
        }
        if($('#confirm_amount_paid').val() == "" )
        {
            $('#err_confirm_amount_paid').html("Please enter Confirm Amount Paid");//FS#29731
            err = err+1;
        }
         else if($('#confirm_amount_paid').val() < 1 )
        {
            $('#err_confirm_amount_paid').html("Please enter valid  Confirm Amount Paid");//FS#29731
            err = err+1;
        }
        else
        {
            if($('#amount_paid').val() != $('#confirm_amount_paid').val())
            {
                $('#err_confirm_amount_paid').html("Amount Paid and Confirm Amount Paid are not same");//FS#29731,FS#29733
                err = err+1;
            }else{
                $('#err_confirm_amount_paid').html("");
            }
        }
        if(err == 0)
        {
            if(submit_form == 1)
            {
                //check for server side validations
                $.getJSON("<?php echo url_for('biller/amountvalidation'); ?>?paidamount="+$('#amount_paid').val()+"&confirmamount="+$('#confirm_amount_paid').val(),function(data){
        
                if(data == "logout"){
                            location.reload();
                        }else{

                             if(data.error==true){
                                 $('#err_amount_paid').html(data.errorstr);
                                 return false;
                             } else {
                                  $('#err_amount_paid').html("");
                                  $('#payment_form').submit();
                             }

                        }
                });



            }
        }
    }

   


    function validateName(str) {
        var reg1 = /^([A-Za-z0-9])+$/;

        if(reg1.test(str) == true) {
            return true;
        }
        return false;
    }
</script>
