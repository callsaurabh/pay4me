<?php use_helper('ePortal');?>

<div id="p4mBank">
  <?php echo ePortal_pagehead($pageTitle);
  //echo ePortal_highlight('PLEASE TAKE THE PRINTED PAYMENT SLIP TO PARTICIPATING BANK FOR PAYMENT- PAYMENT CANNOT BE ACCEPTED WITHOUT THE PAYMENT SLIP','',array('class'=>'red noPrint'));
  ?>
  <div id='acknowledgeSlip' class='wrapForm2'>
    
      <?php
      echo ePortal_legend('Application Details');
      echo formRowFormatRaw('Transaction Number',$formData['pfm_transaction_number']);
      if($formData['MerchantRequest']['validation_number'] != '')
      echo formRowFormatRaw('Validation Number',$formData['MerchantRequest']['validation_number']);
      if($MerchantData)
      { 
        foreach($MerchantData as $k=>$v)
        {
           if(strtolower($v) == 'amount'){
             echo formRowFormatRaw($v,format_amount($formData['MerchantRequest']['MerchantRequestDetails'][$k],true,0));
           }else{
             echo formRowFormatRaw($v,$formData['MerchantRequest']['MerchantRequestDetails'][$k]);
           }
           
        }

      }
    //  echo formRowFormatRaw('Application Id',$formData['application_id']);
    //  echo formRowFormatRaw('Reference No.',$formData['ref_no']);
      echo formRowFormatRaw('Application Type',$formData['MerchantRequest']['MerchantService']['name']);
      ?>
   
   
      <?php
      echo ePortal_legend('User Details');
      echo formRowFormatRaw('Name',$formData['MerchantRequest']['MerchantService']['name']);
      #echo formRowFormatRaw('Mobile',$formData['mobile']);
      #echo formRowFormatRaw('Email',$formData['email']);
      ?>
   

    
      <?php
      echo ePortal_legend('Payment Details');
      echo formRowFormatRaw('Application Charges',format_amount($formData['MerchantRequest']['item_fee'],true,0));
      if($formData['MerchantRequest']['bank_charge']!=""){
        echo formRowFormatRaw('Transaction Charges',format_amount($formData['MerchantRequest']['bank_charge'],true,0));
      }
      if($formData['MerchantRequest']['service_charge']!=""){
        echo formRowFormatRaw('Service Charges',format_amount($formData['MerchantRequest']['service_charge'],true,0));
      }
      echo formRowFormatRaw('Total Payable Amount',format_amount($formData['total_amount'],true,0));


      echo ePortal_legend('Payment Status');

      if($formData['MerchantRequest']['payment_status_code'] == 0){
       echo formRowFormatRaw('Payment Status:','Payment Successful');
      }
      echo formRowFormatRaw('Payment Date:',formatDate($formData['MerchantRequest']['paid_date']));
      echo ePortal_highlight('* Please collect your Pay4Me Payment Slip','',array('class'=>'dsTitle2'));
      ?>
    
  </div>

  

<div class="divBlock noPrint">
  <center id="formNav">
    <button onclick="javascript:printMe('p4mBank')" class='formSubmit'>Print Acknowledgement Slip</button>&nbsp;&nbsp;
    <?php  echo button_to('Back','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('biller/index?billerService='.$formData['MerchantRequest']['Merchant']['name']).'\''));?>
  </center>
</div>
</div>
