    <?php use_helper('ePortal'); ?>
<link href="<?php echo url_for('web/css/print.css') ?>" media="print" type="text/css" rel="stylesheet">
<div id="p4mBank" class="check_heading">
    
    <div class="">
        <div CLASS="cRed"><?php echo ePortal_highlight('',__('PLEASE CONFIRM DETAILS'), array('class' => 'red noPrint')); ?></div>
    </div>
    <div id="loadArea">
<h2> </h2>
<div class="clear"></div>
    <div id='acknowledgeSlip' class='wrapForm2'>
    <?php use_helper('ePortal');
            $pfmHelper=new pfmHelper();
            $bankDraftId=$pfmHelper->getPMOIdByConfName('bank_draft');
            if($payment_mode == $bankDraftId ) {
                $payment_mode_text = pfmHelper::getDraftDisplayName();
            }else {
                $payment_mode_text = pfmHelper::getChequeDisplayName();
            }
            echo ePortal_legend(__($payment_mode_text.' Details'));  ?>
        <?php
            echo ePortal_legend('');
            echo formRowFormatRaw('Bank Name', $bank_name);
            
                echo formRowFormatRaw('Sort Code', $sortCode);
    
                if($payment_mode == $bankDraftId ) {
                    echo formRowFormatRaw('Bank Draft Number', $draftNum);
                }else {
                    echo formRowFormatRaw('Cheque Number', $checkNum);
                    echo formRowFormatRaw('Payee Account Number', $accountNum);
                }
        ?>
            <div style="clear:both;"></div>
            <div class="">
                <center id="formNav">
                    <form id="reset" action="<?php echo url_for('biller/checkPayment') ?>" method="post">
                        <input type="hidden" name='billerService' value='<?php echo $billerService; ?>'>
                        <input type="hidden" name='billerNo' value='<?php echo $billerNum; ?>'>
                        <input type="hidden" name='payType' value='<?php echo $payment_mode; ?>'>
                        <input type="hidden" name='biller_request_id' value='<?php echo $biller_request_id; ?>'>
                        <input type="hidden" name='payOptions[merchantRequestId]' value='<?php echo $merchantRequestId; ?>'>
                        <input type="hidden" name='payOptions[payMode]' value='<?php echo $data['payMode'] ?>'>
                        <!--          params for check payment-->
                    </form>
                    <form id="confirm" action="<?php echo url_for('biller/confirmChkDetails?billerchk=1') ?>" method="post">
                        <input type="hidden" name='billerNo' value='<?php echo $billerNum; ?>'>
                        <input type="hidden" name='biller_request_id' value='<?php echo $biller_request_id; ?>'>
                        <input type="hidden" name='billerService' value='<?php echo $billerService; ?>'>
                        <input type="hidden" name='sort_code' value='<?php echo $sortCode; ?>'>
                        <input type="hidden" name='check_number' value='<?php echo $checkNum; ?>'>
                        <input type="hidden" name='account_number' value='<?php echo $accountNum; ?>'>
                        <input type="hidden" name='bankName' value='<?php echo $bank_name; ?>'>
                        <input type="hidden" name='payment_mode' value='<?php echo $payment_mode; ?>'>
                        <input type="hidden" name='draft_number' value='<?php echo $draftNum; ?>'>

                      
                       
                        <!--          params for check payment-->
                    </form>
                <dl id="">
                <div class="dsTitle4Fields">&nbsp;</div>
                <div align="left" class="dsInfo4Fields">
                    <button onclick="reset()" class="formSubmit"><?php echo __('Re-enter Payment Details') ?></button>&nbsp;&nbsp;
                    <button onclick="confirm()" class="formSubmit"><?php echo __('Confirm') ?> </button>
                </div>
                </dl>
                <div class="clear"></div>
            </center>
        </div></div></div>
        <script>
    function reset(){
        $('#reset').submit();
    }function confirm(){
        $('#confirm').submit();
    }
    
</script></div>
