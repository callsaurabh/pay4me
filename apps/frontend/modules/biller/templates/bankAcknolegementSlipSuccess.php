<?php //use_helper('Form') ?>
<?php use_helper('ePortal');?>

<script language="Javascript">
    function checkAll()
    {
        if(document.getElementById('disclaimer').checked == false || document.getElementById('application_charges').checked == false || document.getElementById('bank_charges').checked == false || document.getElementById('service_charges').checked == false || document.getElementById('total_charges').checked == false)
        {
            alert('Please select All Charges, Total Amount and Disclaimer');
            return false;
        }else{
            $('#pfm_acknolege_form').submit();
            return true;
        }
        //document.forms['pfm_acknolege_form'].submit();
        
    }
</script>



  <?php echo ePortal_pagehead($pageTitle);
  //echo ePortal_highlight('PLEASE TAKE THE PRINTED PAYMENT SLIP TO PARTICIPATING BANK FOR PAYMENT- PAYMENT CANNOT BE ACCEPTED WITHOUT THE PAYMENT SLIP','',array('class'=>'red noPrint'));
  ?>
  <?php echo form_tag('biller/bankAcknolegementSlip',' class="dlForm multiForm" id="pfm_acknolege_form" name="pfm_acknolege_form"') ?>
  <div class='wrapForm2'>
    
      <?php
      echo ePortal_legend('Applicant Details');
      echo formRowFormatRaw('Transaction Number',$formData['pfm_transaction_number']);
      if($MerchantData)
      {
        foreach($MerchantData as $k=>$v)
        {
           if(strtolower($v) == 'amount'){
             echo formRowFormatRaw($v,format_amount($formData['MerchantRequest']['MerchantRequestDetails'][$k],true,0));
           }else{
             echo formRowFormatRaw($v,$formData['MerchantRequest']['MerchantRequestDetails'][$k]);
           }

        }

      }
      echo formRowFormatRaw('Application Type',$formData['MerchantRequest']['MerchantService']['name']);
      ?>
    
    
      <?php
      echo ePortal_legend('Service Details');
      echo formRowFormatRaw('Name',$formData['MerchantRequest']['MerchantService']['name']);
      ?>
    

                <?php
                echo ePortal_legend('Payment Charges');
                ?>

                <?php echo formRowFormatRaw("<input type=\"checkbox\" name=\"application_charges\" id=\"application_charges\" value=\"0\" class='noPrint'> Application Charges:",format_amount($formData['MerchantRequest']['item_fee'],true,0));
                 ?><input type="hidden" name="appCharge" id="appCharge" value="<?php echo $formData['MerchantRequest']['item_fee'];?>"><?php
                //echo input_hidden_tag('appCharge', $formData['MerchantRequest']['item_fee']);
                if($formData['MerchantRequest']['bank_charge'] != ""){
                  echo formRowFormatRaw("<input type=\"checkbox\" name=\"bank_charges\" id=\"bank_charges\" value=\"0\" class='noPrint'> Transaction Charges:",format_amount($formData['MerchantRequest']['bank_charge'],true,0));
                  ?><input type="hidden" name="bankCharge" id="bankCharge"  value="<?php echo $formData['MerchantRequest']['bank_charge'];?>"><?php
                  //echo input_hidden_tag('bankCharge', $formData['MerchantRequest']['bank_charge']);
                }
                if($formData['MerchantRequest']['service_charge'] != ""){
                echo formRowFormatRaw("<input type=\"checkbox\" name=\"service_charges\" id=\"service_charges\" value=\"0\" class='noPrint'> Service Charges:",format_amount($formData['MerchantRequest']['service_charge'],true,0));
                ?><input type="hidden" name="serviceCharge" id="serviceCharge"  value="<?php echo $formData['MerchantRequest']['service_charge'];?>"><?php
                //echo input_hidden_tag('serviceCharge', $formData['MerchantRequest']['service_charge']);
                }
                echo formRowFormatRaw("<input type=\"checkbox\" name=\"total_charges\" id=\"total_charges\" value=\"0\" class='noPrint'> Total Payable Amount:",format_amount($formData['total_amount'],true,0));
                ?><input type="hidden" name="totalCharge" id="totalCharge" value="<?php echo $formData['total_amount'];?>"><?php
                //echo input_hidden_tag('totalCharge', $formData['total_amount']);
                ?>
                <div class="dsTitle2"><input type="checkbox" name="disclaimer" id="disclaimer" value="0"> I hereby acknowledge that the information provided above is true and correct.</div>
            
  </div>



<div class="divBlock">
  <center id="formNav">
    <input type="hidden" name="TransactionNo" id="TransactionNo"  value="<?php echo $transactionNum;?>">
   <?php  //echo input_hidden_tag('TransactionNo', $transactionNum) ?>
    <input type="button" class="formSubmit" value="Payment" onClick="checkAll(1);" />
  </center>
</div>
</form>
</div>
