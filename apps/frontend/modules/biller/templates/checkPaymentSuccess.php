<div id="loadArea">
    <h2> </h2>
<div class="clear"></div>
<div class="wrapForm2"><?php use_helper('ePortal');
echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details')); ?>

<div class="wrapLogos">
    <form id="chkDetails" action="<?php echo url_for('biller/confirmChkDetails') ?>" method="post" >

        <div class="dsTitle4Fields">Bank Name</div>
        <div class="dsInfo4Fields">
            <label><?php echo $bank_name; ?></label>
    <!--                    <div class="cRed" id="err_name"><?php //echo $chDetailsForm['sort_code']->renderError();                ?></div>-->
        </div>
        <div class="clear"></div>
        <div class="dsTitle4Fields"><?php echo $chDetailsForm['sort_code']->renderLabel(); ?><span class="required"> *</span>     </div>
        <div class="dsInfo4Fields">
            <label><?php echo $chDetailsForm['sort_code']->render(); ?></label>
            <div class="clearfix"><div class="cRed" id="err_cs_sort_code"></div>
            <div class="cRed" id="err_sort_code"><?php echo $chDetailsForm['sort_code']->renderError(); ?></div></div>
        </div><?php echo $chDetailsForm['bank_id']->render(); ?>
        <div name="sort_code_validated" style="display:none" id="sort_code_validated">
            <div class="clear"></div>
            <div class="dsTitle4Fields"><?php echo $chDetailsForm['check_number']->renderLabel(); ?><span class="required"> *</span></div>
            <div class="dsInfo4Fields">
                <label><?php echo $chDetailsForm['check_number']->render(); ?></label>
                <div class="cRed" id="err_cs_check_number"></div>
                <div class="cRed" id="err_check_number"><?php echo $chDetailsForm['check_number']->renderError(); ?></div>
            </div>
            <div class="clear"></div>
            <div class="dsTitle4Fields"><?php echo $chDetailsForm['account_number']->renderLabel(); ?><span class="required"> *</span></div>
            <div class="dsInfo4Fields">
                <label><?php echo $chDetailsForm['account_number']->render(); ?></label>
                <div class="cRed" id="err_cs_account_number"></div>
                <div class="cRed" id="err_account_number"><?php echo $chDetailsForm['account_number']->renderError(); ?></div>
            </div>
        </div>
        <div class="clear"></div>
        <dl id="">
        <div class="dsTitle4Fields">&nbsp;</div>
        <div class="dsInfo4Fields">
            <input type="hidden" name="payment_mode"  id="payment_mode" value="<?php echo $payment_mode; ?>" />
            <input type="hidden" name="biller_request_id"  id="biller_request_id" value="<?php echo $biller_request_id; ?>" />
            <input type="hidden" name="bankName"  id="bankName" value="<?php echo $bank_name; ?>" />
            <input type="hidden" name="billerService"  id="billerService" value="<?php echo $billerService; ?>" />
            <input type="hidden" name="billerNo"  id="billerNo" value="<?php echo $billerNum; ?>" />
            <?php echo $chDetailsForm['_csrf_token']; ?>
            <input type="submit" class="formSubmit" name="continue" value="Continue" id="submit_form" style="display:none">
            <input type="button" class="formSubmit"
                   id="validateSortCode" name="proceed" value="Continue" >
        </div></dl>
       <div class="descriptionArea">Note: Please enter only first 3 digits of Sort Code.</div>

        <div class="clear"></div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {


            if($('#check_number').val()!='' || $('#account_number').val()!='' ){
                //alert('jgjghg');
                $('#sort_code_validated').attr('style','display:inline');
                $('#validateSortCode').attr("style",'display:none');
                $('#submit_form').attr("style",'display:inline');

            }

            $('#validateSortCode').click(function(){
                if($('#sort_code').val()=="")
                {
                    $('#err_cs_sort_code').html("Please enter Sort Code");
                    $('#err_sort_code').html("");
                    //                return false;
                    //$('#err_cs_sort_code').

                }
                else
                {


                    //                var regEx=new RegExp('[^a-zA-Z0-9]+');
                    if(alphaNumricRegexCheck($('#sort_code').val()) == null){
                        if($('#sort_code').val().length==3)
                        {
                            var t=sortCodeValidation();
                        }
                        else
                        {

                            $('#err_cs_sort_code').html("Please enter 3 digits");
                            $('#err_sort_code').html("");
                        }

                    }
                    else
                    {

                        $('#err_cs_sort_code').html("Invalid Sort Code(Only alphanumeric characters accepted)");
                        $('#err_sort_code').html("");

                    }
                    // alert($('#sort_code').val().length);


                }


            });
            function sortCodeValidation()
            {
                //
                var bank_id=$('#bank_id').val();
                var url = '<?php echo url_for("paymentProcess/validateSortCode", true) ; ?>';

                var sort_code_var=$('#sort_code').val().replace(" ","");
               $.post(url, {bank_id:bank_id,sort_code:sort_code_var}, function(data){
                   if(data=='logout'){
                            location.reload();
                        } else
                    if(data==true)
                    {
                        $('#validateSortCode').attr("style",'display:none');
                        $('#submit_form').attr("style",'display:inline');
                        var sortcode_style=$('#sort_code_validated').attr("style").toLowerCase();
                        if(sortcode_style.indexOf('display')!=-1 && sortcode_style.indexOf('none')!=-1){

                            $('#err_cs_sort_code').html("");
                            $('#sort_code').attr("readonly","readonly");
                            $('#err_sort_code').html("");
                            $('#err_check_number').html("");
                            $('#err_cs_check_number').html("");
                            $('#err_cs_account_number').html("");

                            $('#err_account_number').html("");
                            $('#check_number').val("");
                            $('#account_number').val("");

                        }


                        $('#err_cs_sort_code').html("");
                        $('#err_sort_code').html("");
                        $('#sort_code_validated').attr('style','display:inline');
                        if($('#check_number').val()!='' || $('#account_number').val()!='' ){

                            $('#sort_code_validated').attr('style','display:inline');
                            $('#validateSortCode').attr("style",'display:none');
                            $('#submit_form').attr("style",'display:inline');

                        }

                    }
                    else
                    {

                        $('#sort_code_validated').attr('style','display:none');
                        $('#err_cs_sort_code').html("Invalid Sort Code ");
                        $('#validateSortCode').attr("style",'display:inline');
                        $('#submit_form').attr("style",'display:none');

                        $('#err_sort_code').html("");

                    }


                });


            }
            function alphaNumricRegexCheck(text)
            {
                var regEx=new RegExp('[^a-zA-Z0-9]+');
                return regEx.exec(text);


            }
            $('#chkDetails').submit(function(){
                var err=0;




                if($('#check_number').val()=="")
                {
                    $('#err_cs_check_number').html("Please enter Cheque Number");
                    $('#err_check_number').html("");
                    err++;

                }
                else
                {
                    if(alphaNumricRegexCheck($('#check_number').val()) == null){

                        $('#err_cs_check_number').html("");
                        $('#err_check_number').html("");

                    }
                    else
                    {
                        $('#err_cs_check_number').html("Invalid Cheque Number(Only alphanumeric characters accepted)");
                        $('#err_check_number').html("");
                        err++;
                    }

                }
                if($('#account_number').val()=="")
                {
                    $('#err_cs_account_number').html("Please enter Payee Account Number");
                    $('#err_account_number').html("");
                    err++;

                }
                else
                {
                    if(alphaNumricRegexCheck($('#account_number').val()) == null){
                        $('#err_cs_account_number').html("");
                        $('#err_account_number').html("");
                    }
                    else
                    {
                        $('#err_cs_account_number').html("Invalid Payee Account Number(Only alphanumeric characters accepted)");
                        $('#err_account_number').html("");
                        err++;

                    }
                }

                if(err>0){

                    if(alphaNumricRegexCheck($('#sort_code').val()) == null){
                        sortCodeValidation();
                    }
                    else
                    {
                        $('#err_cs_sort_code').html("Invalid Sort Code(Only alphanumeric characters accepted)");
                        $('#err_sort_code').html("");


                    }
                }
                if($('#sort_code').val()=="")
                {
                    $('#err_cs_sort_code').html("Please enter Sort code");
                    $('#err_sort_code').html("");

                    err++;

                }
                //            else
                //            {
                //                $('#err_cs_sort_code').html("");
                //                $('#err_sort_code').html("");
                //            }
                if(err>0){

                    return false;
                }



            });


        });

</script>
</div>
</div>
