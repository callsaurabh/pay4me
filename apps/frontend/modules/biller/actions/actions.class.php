<?php

class billerActions extends sfActions {


    public function executeIndexOld(sfWebRequest $request) {
        $this->form = new BillerForm();
        $params = $request->getParameter('billerService');
        $billerObj = integrationServiceFactory::getService('biller');

        $this->errorMsg='';
        if(isset($params))
        {
             $this->form->setDefaults(array(
                'billerServiceId' => $params,
                'billerService' => $params
                ));
        }


        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('biller'));
             if ($this->form->isValid() )
             {
                $AllParams = $request->getParameterHolder()->getAll();
                $ifaArr = $AllParams['biller']['ifa_no'];
                $ifaNum = substr($ifaArr, 1, 7);

                $this->ifaNumber = $ifaArr;
//                echo "<pre>";print_r($AllParams);echo $AllParams['biller']['billerServiceId'];die;
                //$this->referenceNumber = $request->getPostParameter('ref_no');
                $billerServiceId = $billerObj->getBillerId($AllParams['biller']['billerServiceId']);
                $svcParams = array('identification_num'=>$ifaArr, 'merchantServiceId' =>$billerServiceId);
                $xmlRequest = $billerObj->getSearchXml($svcParams);
                $notifyResponse = $billerObj->notification($xmlRequest, $request, $svcParams['merchantServiceId']);

                if(is_array($notifyResponse)) {
                    $request->getParameterHolder()->set('data', $notifyResponse);
                    $this->forward('biller','billerSearchResult');
               }else{
                     $this->errorMsg = $notifyResponse;
                    }

         }//valid

        } //post
    }

    /* WP032 Changes in billers for figra*/
    public function executeIndex(sfWebRequest $request) { 
        $billerObj = integrationServiceFactory::getService('biller');
        $pfmHelper=new pfmHelper();
        $bankId=$pfmHelper->getPMOIdByConfName('bank');
        $checkId=$pfmHelper->getPMOIdByConfName('Cheque');
        $bankDraftId=$pfmHelper->getPMOIdByConfName('bank_draft');
        $billerHelper = new billerHelper();
        //When user has selected payment mode
        if($request->getParameter('billerService') && ($request->hasParameter('billerNo')) && ($request->hasParameter('payType'))){
            $this->biller_request_id=$request->getParameter('biller_request_id');
            $billerInfo=$this->getBillerRequestText($this->biller_request_id);
            if (is_array($billerInfo)) {
                $billerInfo['payment_mode'] = $request->getPostParameter('payType');
                $this->payment_mode = $billerInfo['payment_mode'];
                $request->getParameterHolder()->set('data', $billerInfo);
                if($request->getParameter('payType')==$bankId) {
                    $this->forward('biller', 'billerSearchResult');
                }
                else{
                    $request->getParameterHolder()->set('payment_mode', $this->payment_mode);
                    $this->forward('biller', 'checkPayment');
                }
            } else {
                $this->errorMsg = $billerInfo;
            }
        }
        $params = $request->getParameter('billerService');
        $user_bank_id = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
        $merchant = Doctrine::getTable('MerchantService')->find($params);
        $merchantId = $merchant->getMerchantId();
        $billerObj = integrationServiceFactory::getService('biller');
        
        $this->merchantServiceId = $params;
        $merchantServiceObj = Doctrine::getTable('MerchantService')->find($this->merchantServiceId);
        $this->merchantServiceName = $merchantServiceObj->getName();
        $this->form = new BillerForm(array(), array('merchantName' => $this->merchantServiceName,
                    'merchantServiceId' => $this->merchantServiceId));
        $this->errorMsg = '';
        $paymentModeAvailable = Doctrine::getTable('ServicePaymentModeOption')->findByMerchantServiceId($request->getParameter('billerService'));
        if($paymentModeAvailable->count()==0){
            $this->getUser()->setFlash('error', "This Service is not associated with any Payment mode ", false);
            return;
        }
        $bankMerchantActive = Doctrine::getTable('BankMerchant')->chkBankMerchantStatus($merchantId, $user_bank_id);
        if ($bankMerchantActive == 0) {
            $this->getUser()->setFlash('error', "This merchant is de-activated for this bank", false);
            return;
        }

        if ($request->isMethod('post')) { 
            $this->form->bind($request->getParameter('biller'));
            if ($this->form->isValid()) {
                $AllParams = $request->getParameterHolder()->getAll();
                $ifaArr = ltrim($AllParams['biller']['biller_no']);
                //  $ifaNum = substr($ifaArr, 1, 7);

                $this->ifaNumber = $ifaArr;
                $billerServiceId = $AllParams['biller']['billerServiceId'];
                //$notifyResponse['payment_mode'] = $request->getPostParameter('payType');
                //first time calling billing request

                $notifyResponse = $billerHelper->getBillInformation($this->merchantServiceId,$this->ifaNumber);

                if (is_array($notifyResponse)) {
                    $biller_request=new BillerRequest();
                    $biller_request->setRequestText(serialize($notifyResponse));
                    $biller_request->save();
                    $biller_request_id=$biller_request->getId();
                    //payment mode selection
                    $request->getParameterHolder()->set('billerno', $this->ifaNumber);
                    $request->getParameterHolder()->set('merchantservice', $params);
                    $request->getParameterHolder()->set('biller_request_id', $biller_request_id);
                    $this->forward('biller', 'selectPaymentMode');
//                    $request->getParameterHolder()->set('data', $notifyResponse);
//                    $this->forward('biller', 'billerSearchResult');
                } else {
                    $this->errorMsg = $notifyResponse;
                }
            }//valid
            //post
        }
    }
    
    /**
     * WP058 CR098
     * For selection of payment mode for biller payment
     * @param <type> $request
     */
    public function executeSelectPaymentMode(sfWebRequest $request) {
        $this->billerno = $request->getParameterHolder()->get('billerno');
        $this->merchantService = $request->getParameterHolder()->get('merchantservice');
        $this->biller_request_id = $request->getParameterHolder()->get('biller_request_id');
        $this->billForm = new BillerPaymentModeForm(array(), array('merchantService'=>$this->merchantService));
    }

     /**
      * WP058 CR098
      * For taking the values for cheque and draft payment from the teller
      * @param <type> $request
      */
     public function executeCheckPayment(sfWebRequest $request) {
       $user_bank_id = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
       $this->billerService = $request->getParameter('billerService');
       $this->biller_request_id = $request->getParameter('biller_request_id');
       $this->billerNum = $request->getParameter('billerNo');
       $this->bank_name = Doctrine::getTable('Bank')->find($user_bank_id)->getBankName();
       $this->payment_mode = $request->getPostParameter('payType');
       $this->chDetailsForm = new CustomCheckDetailsForm(array('bank_id' => $user_bank_id), array(), array());
       $pfmHelper=new pfmHelper();
       $checkId=$pfmHelper->getPMOIdByConfName('Cheque');
       $bankDraftId=$pfmHelper->getPMOIdByConfName('bank_draft');
       if($request->getPostParameter('payType') == $bankDraftId ){
           $this->setTemplate('draftPayment');
       }else {
           $this->setTemplate('checkPayment');
       }
    }


    /**
     * WP058 CR098
     * Confirming the entered values for cheque and draft payment from the teller
     * @param <type> $request
     */
     public function executeConfirmChkDetails(sfWebRequest $request) {
         $this->pageTitle = pfmHelper::getChequeDisplayName()." Details";
         $this->sortCode = $request->getParameter('sort_code');
         $this->biller_request_id = $request->getParameter('biller_request_id');
         $this->checkNum = $request->getParameter('check_number');
         $this->accountNum = $request->getParameter('account_number');
         $this->bank_name = $request->getParameter('bankName');
         $this->billerService = $request->getParameter('billerService');
         $this->billerNum = $request->getParameter('billerNo');
         $this->payment_mode = $request->getPostParameter('payment_mode');
         $this->draftNum = $request->getParameter('draft_number');
         $billerInfo='';
         $databillerInfo['checkNum'] = $this->checkNum;
         $databillerInfo['accountNum'] = $this->accountNum;
         if($request->hasParameter('billerchk')){
             $billerInfo=$this->getBillerRequestText($this->biller_request_id);
         if (is_array($billerInfo)) {
                $billerInfo=$billerInfo+$databillerInfo;
                $request->getParameterHolder()->set('data', $billerInfo);
                $this->forward('biller', 'billerSearchResult');
         }
         }
         $this->data=$databillerInfo;
         $pfmHelper=new pfmHelper();
         $checkId=$pfmHelper->getPMOIdByConfName('Cheque');
         $bankDraftId=$pfmHelper->getPMOIdByConfName('bank_draft');
//         if($request->getPostParameter('payment_mode') == $bankDraftId ){
//             $this->setTemplate('confirmDraftDetails');
//         }else {
//             $this->setTemplate('confirmChkDetails');
//         }
    }




    private function getBillerRequestText($biller_request_id){
         $billerRec = Doctrine::getTable('BillerRequest')->find($biller_request_id);
         $billerInfo='';
         if($billerRec->getRequestText())
         {
             $billerInfo=unserialize($billerRec->getRequestText());
         }
         return $billerInfo;
    }





    public function executeSearchWithoutIfaNo(sfWebRequest $request){
        $this->checkIsSuperAdmin();
        $params = $request->getParameter('billerService');
        $billerObj = integrationServiceFactory::getService('biller');
        $billerServiceId = $billerObj->getBillerId($params);

        //$svcParams = array('identification_num'=>'', 'ref_num'=>'', 'merchantServiceId' =>$billerServiceId);
        $svcParams = array('identification_num'=>'', 'merchantServiceId' =>$billerServiceId);
        $billerObj = integrationServiceFactory::getService('biller');
        $xmlRequest = $billerObj->getSearchXml($svcParams);

        $notifyResponse = $billerObj->notification($xmlRequest, $request, $svcParams['merchantServiceId']);

        if(is_array($notifyResponse)) {
            $request->getParameterHolder()->set('data', $notifyResponse);
            $this->forward('biller','billerSearchResult');
        }else {
            $this->errorMsg = "Request cannot be fulfilled.";
        }
    }

    public function executeBillerSearchResult(sfWebRequest $request) {
        
        $this->checkIsSuperAdmin();
        $param = $request->getParameterHolder()->get('biller');
        if($param['biller_no']){
        $this->billerNumber = $param['biller_no'];}
        else{
        $this->billerNumber = $request->getParameter('billerNo');}
        $this->getValues = $request->getParameterHolder()->get('data');
        if(isset($this->getValues['merSvcId'])){
            $this->convert_to_kobo = $this->checkVersion($this->getValues['merSvcId']);
        }
        if($this->getValues!="") {
            $_SESSION['orderxml'] =  $this->getValues['orderxml'];
            $currency = $this->getValues['currency'];
        } else {
            $currency = $request->getParameter('currency');
        }
        
        $this->checkNum = $request->getParameter('check_number');
        if($this->checkNum =='') {
            $this->checkNum = $request->getParameter('draft_number');
        }

        $this->accountNum = $request->getParameter('account_number');
        $this->payment_mode = $request->getPostParameter('payment_mode');
        $this->sortCode = $request->getParameter('sort_code');
        $currObj = Doctrine::getTable('CurrencyCode')->findByCurrencyNum($currency);
        $this->currencyId = $currObj->getfirst()->getId();
        //  $this->getValues['orderxml'];


        /* WP032 */
        $merchantServiceId = $this->getValues['merSvcId'];
        $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);

        $this->rules = $ValidationRulesObj->getSearchValidationRulesDetails($merchantServiceId);

        $this->errorMsg = '';

        if ($request->isMethod('post') && $this->getValues == '') {
            $billerObj = integrationServiceFactory::getService('biller');//
            $version = $request->getParameter('version');
          /*  if($version == 'v1')
                        $notificationArr = $billerObj->getOrderNotificationXml($request->getPostParameters());
            else */
                $notificationArr = array(1,$_SESSION['orderxml']);
            if($notificationArr[0] == 1) {

                $merchant_code = '';
                $xdoc = new DomDocument;
                $isLoaded = $xdoc->LoadXML($notificationArr[1]);
                //chk payment status
                $pay4MeXMLOrderObj = pay4meXmlServiceFactory::getRequest($xdoc, $version, $merchant_code) ;

                $item_number = $pay4MeXMLOrderObj->item_number;
                $merchant_service_id = $pay4MeXMLOrderObj->merchant_service_id;
                $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);

                $paymentObj = $billerObj->getPaymentStatus($item_number,$merchant_service_id);

                if($paymentObj){
                    $transactionNum = $paymentObj->getFirst()->getMerchantRequest()->getFirst()->getTransaction()->getFirst()->getPfmTransactionNumber();
                    $this->getRequest()->setParameter('txn_no', $transactionNum);
                    $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
                    $this->getUser()->setFlash('notice', 'Payment has already been made for this item', false) ;
                    $this->forward('paymentSystem', 'payOnline');

                }
                else {
                    $paymentModeId = $request->getPostParameter('payment_mode');
                    $pay4meHelperObj = new pfmHelper();
                    $paymentMode = $pay4meHelperObj->getPMONameByPMOId($paymentModeId);
                    //saving merchant item
                   
                    $merchentRequestId = $pay4MeXMLOrderObj->getDBObjectNew($pay4MeXMLOrderObj) ;
                    
                      //update paym,ent mode option as bank
                     // $updateArray = array('id'=>$merchentRequestId, 'payment_mode_option_id'=>'1');
                   //   Doctrine::getTable('MerchantRequest')->updateMerchantRequest($updateArray);


                    //generating transaction number
                    $transactionNo = $payForMeObj->generateTransactionNumber();

                    $merchant_request_details = $billerObj->getMerchantRequestDetails($merchentRequestId);
                    $merchantId = $merchant_request_details->getFirst()->getMerchantId();
                    $merchantServiceId = $merchant_request_details->getFirst()->getMerchantServiceId();

                    $formData = array('payType' => 'bank', 'merchantRequestId' => $merchentRequestId, 'pay_mode' => $paymentMode, 'trans_num' => $transactionNo, 'merchant_id' => $merchantId );

                    //saving transaction data
                    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
                    $transactionNum = $payForMeObj->saveTransaction($formData);


                }


                if($transactionNum) {
                    $gUser = $this->getUser()->getGuardUser();
                    $bUser = $gUser->getBankUser();
                    $bankName = $bUser->getFirst()->getBank()->getBankName();

                    $pfmHelperObj = new pfmHelper();
                    $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
                    $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                    $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');

                    // WP032
                    //updating merchant request for charges
//                    if ( $version == 'v2' ) {
                        $postDataArray['id'] = $merchentRequestId;
                        $postDataArray['currency_id'] = $this->currencyId;
                        $postDataArray['transaction_location'] = "biller";
                        $postDataArray['item_fee'] = $request->getParameter('amount_paid');
                        Doctrine::getTable('MerchantRequest')->updateMerchantRequest($postDataArray);
//                    }
                    $payForMeObj->updateMerchantRequest($merchentRequestId,$paymentMode);
                    $payForMeObj->updateCheckDetailsForMerchantRequest($transactionNum, $this->checkNum, $this->accountNum,$bankName, $this->sortCode);
                    //                    $pfmTransactionDetails = $payForMeObj->getTransactionRecord($transactionNum);
                    //                    $this->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
                    //                    $this->forward('biller','bankAcknolegementSlip');
                    $merchantNotification = '1';
                    $merchantNotificationStatus = Doctrine::getTable('MerchantRequest')->saveMerchantNotificationStatus($merchentRequestId,$merchantNotification);
                    $this->redirect('paymentSystem/search?txnId='.$transactionNum);

                }
                else {
                    $this->redirect('paymentProcess/invalidAttempt');
                }


            }else {
                $msg = "Not a valid Xml";
            }

//        }
        }

    }


    // Send Notification
    public function executeSendBillerNotification(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->setLayout(null);
        $txnId = $request->getParameter('p4m_transaction_id');
        $this->billerNotification($txnId);
        return $this->renderText("Notification sent Successfully");
    }

    public function billerNotification($txnId) {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId);
        $url = $pfmTransactionDetails['MerchantRequest']['MerchantService']['notification_url'];

        $billerObj = integrationServiceFactory::getService('biller');
        $browser = $billerObj->getBrowser() ;


        $header = $billerObj->settingHeaderInfo($pfmTransactionDetails['merchant_service_id']);
        $transXml =  $billerObj->getPaymentNotificationXml($pfmTransactionDetails,'ifa');    // get the xml

        //$obj = new pay4MeXMLV1();
        //$transXml = $obj->generatePaymentNotificationXMLV1($txnId);
        //$this->createLog($transXml,'PaymentNotification');
        $browser->post($url, $transXml, $header);
        $code = $browser->getResponseCode();
        if ($code != 200 ) {
            $respMsg = $browser->getResponseMessage();
            $respText = $browser->getResponseText();
            throw new Exception("Error in posting notification:".PHP_EOL.$code.' '.$respMsg.PHP_EOL.$respText);
        }
        $this->logMessage('notification send') ;
    }


    public function executeBankAcknolegementSlip(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);

        if($request->isMethod('post') && $request->getPostParameter('TransactionNo')){
            $transactionNum=$request->getPostParameter('TransactionNo');
            $pfmTransactionDetails = $payForMeObj->getTransactionRecord($transactionNum);

            $this->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
           // $this->forward('paymentSystem', 'payOnline');
            $this->forward('biller','bankPaymentSlip');
        }



        $this->pfmTransactionDetails = $request->getParameter('pfmTransactionDetails');
        $transactionNum = $this->pfmTransactionDetails['pfm_transaction_number'];
        $merchantServiceId = $this->pfmTransactionDetails['MerchantRequest']['merchant_service_id'];
        $merchantId = $this->pfmTransactionDetails['MerchantRequest']['merchant_id'];
        $merchentRequestId = $this->pfmTransactionDetails['MerchantRequest']['id'];
        $payment_status = $this->pfmTransactionDetails['MerchantRequest']['payment_status_code'];

        //updating merchant request for charges
        $payForMeObj->updateMerchantRequest($merchentRequestId,'bank');

        $this->pageTitle = 'Acknowledgement Slip';

        //get Param Description; as per Merchant Service
        $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
        $this->MerchantData = $ValidationRulesObj->getValidationRules($merchantServiceId);

        //getting transaction records
        $this->formData = $payForMeObj->getTransactionRecord($transactionNum);


        $this->transactionNum =$transactionNum ;

    }

 public function executeBankPaymentSlip(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $pfmTransactionDetails = $request->getParameter('pfmTransactionDetails');
        $transactionNum = $pfmTransactionDetails['pfm_transaction_number'];
        $merchantServiceId = $pfmTransactionDetails['MerchantRequest']['merchant_service_id'];
        $merchantId = $pfmTransactionDetails['MerchantRequest']['merchant_id'];
        $merchentRequestId = $pfmTransactionDetails['MerchantRequest']['id'];
        $payment_status = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];

        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $billerObj = integrationServiceFactory::getService('biller');

        if($payment_status != 0){

            $billerObj->doPayment($transactionNum,$merchentRequestId,$merchantId);
            $this->getUser()->setFlash('notice', 'Payment successfully done.', false) ;

        }
        else{
            $this->getUser()->setFlash('notice', 'Payment has already been made', false) ;
        }

        $this->pageTitle = 'Payment Slip';

        //get Param Description; as per Merchant Service
        $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
        $this->MerchantData = $ValidationRulesObj->getValidationRules($merchantServiceId);

        //getting transaction records
        $this->formData = $payForMeObj->getTransactionRecord($transactionNum);

    }


    public function executePayAgent(sfWebRequest $request){
      $this->form = new BillerForm();
        $params = $request->getParameter('billerService');


        $this->errorMsg='';
        if(isset($params))
        {
             $this->form->setDefaults(array(
                'billerServiceId' => $params,
                'billerService' => $params
                ));
        }
    }



    public function executePayAgentSubmit(sfWebRequest $request){
        $this->checkIsSuperAdmin();
        $this->err = "";
        $this->notifyResponse = array();
        if ($request->isMethod('post')) {

            $billerObj = integrationServiceFactory::getService('biller');

            $ifaArr = $request->getPostParameter('ifa_no');
            $ifaNum = substr($ifaArr, 1, 7);

            $billerServiceId = $billerObj->getBillerId($request->getPostParameter('billerServiceId'));

            //checking validity of IFA number
            if(ctype_alpha($ifaArr[0]) && ucwords($ifaArr[0]) == $ifaArr[0] && strlen($ifaArr) == 8 && is_numeric($ifaNum)){

             $svcParams = array('identification_num'=>$request->getPostParameter('ifa_no'), 'merchantServiceId' =>$billerServiceId);

                $xmlRequest = $billerObj->getAgentSearchXml($svcParams);
                if($xmlRequest != false){
                 $this->notifyResponse = $billerObj->notification($xmlRequest, $request, $svcParams['merchantServiceId']);

                }else{
                  $this->err = "Request cannot be fulfilled.(xml validation failed)";
                }
            }else{
               $this->err = "Please enter a valid IFA Number.";
            }
        }
    }

    public function checkIsSuperAdmin(){
        if($this->getUser()  && $this->getUser()->isAuthenticated()) {
        $group_name = $this->getUser()->getGroupNames();
        if(in_array(sfConfig::get('app_pfm_role_admin'),$group_name)){
            $this->redirect('@adminhome');
        }
        }
    }

    public function executeAmountvalidation(sfWebRequest $request){
        //get amounts
        $paidamount = $request->getParameter('paidamount');
        $confirmamount = $request->getParameter('confirmamount');
        $error=true;
        $strpaidamount = "";
        if($paidamount==""){
            $strpaidamount = "Please enter amount";
        }elseif(!preg_match ("/^[-+]?\d{1,10}(\.\d{1,2})?$/", $paidamount)){
            $strpaidamount = "Please enter valid amount";
        }elseif(strlen($paidamount) >10){
            $strpaidamount = "Please enter valid amount";
        }elseif($paidamount != $confirmamount){
            $strpaidamount = "Amount and Confirm Amount are not same";
        }else {
            $error=false;
        }
       // $a = array('error'=>$error,'errorstr'=>$strpaidamount);
        //echo $a;
        $resJSON = json_encode(array('error'=>$error,'errorstr'=>$strpaidamount));
        return $this->renderText($resJSON);
        die;
    }

   private function checkVersion ($serviceId){
        $msObj = Doctrine::getTable('MerchantService')->findOneById($serviceId);
        $version = $msObj->getVersion();
        if(strtoupper($version) == 'V1' ||  $version == '' ) {
            return 0;
        }else {
            return 1;
        }
   }

}

?>
