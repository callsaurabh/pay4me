<?php
/**
 * ajax actions.
 * @package    mysfp
 * @subpackage ajax
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ajaxActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  /*
   * function : GetBankCountry
   * Date : 25-11-2011
   */
  public function executeGetBankCountry(sfWebRequest $request) {
   
    $objApi = new UserRoleHelper();
    $arrayGroup = $objApi->getBankUserGroupCountry();
    //get the bank id
    $bankId = $request->getParameter('bank');    
    $bankUserRole = $request->getParameter('bank_user_role');    
    $str = '<option value="">--All Countries--</option>';
    if($bankId != "" && isset($arrayGroup[$bankUserRole])) {
      $countryList = Doctrine::getTable('BankConfiguration')->getAllCountry($bankId);    
      if(!empty($countryList)){
          foreach($countryList as $key=>$val){
              if($key)
              $str .= '<option value="'.$key.'">'.$val.'</option>';
          }
      }
    }
    return $this->renderText($str);
  }
  
  public function executeFind(sfWebRequest $request) {
    try {
      $data=$request->getPostParameter('data');
      if(isset($data) && $data!='')
      {
        $dMg = Doctrine_Query::create()->parseDqlQuery($data);
        $results = $dMg->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $this->renderText(json_encode($results));
      } else {
        return $this->renderText('No data specified');
      }
    } catch (Exception $e) {
      return $this->renderText('Caught exception: ' . $e->getMessage() . "\n");
    }
  }
    
  /*
   * function : GetBankBranches
   * Date : 25-11-2011
   */
   public function executeGetBankBranches(sfWebRequest $request) {
    $bankId = $request->getParameter('bank');
    $countryId = $request->getParameter('country');
    $arrayGroup=array('bank_user'=>'bank_user','bank_branch_report_user'=>'bank_branch_report_user');
    $bankUserRole = $request->getParameter('bank_user_role');    
    $str = '<option value="">--All Bank Branches--</option>';
    if($bankId != "" && isset($arrayGroup[$bankUserRole])) {
            $branchList = Doctrine::getTable('BankBranch')->getAllBranches($bankId,$countryId);
            if(!empty($branchList)){
                foreach($branchList as $value) {
                 $str .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                }
            }
    }
    return $this->renderText($str);
  }
  
}
