<?php

/**
 * Global Setting actions.
 *
 * @package    mysfp
 * @subpackage Global_setting
 * @author     ashutosh srivastava
 * @version    SVN: $Id: actions.class.php 12474 2010-03-03 10:41:27Z fabien $
 */
class GlobalSettingActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {

    $this->form = new GlobalSettingtForm();

    $Amt = Doctrine::getTable('EwalletSetting') ->getChargeAmountLimit();
    $Days= Doctrine::getTable('EwalletSetting') ->getChargeDaysLimit();

    $this->form->setDefaults(array('charge_amount_limit' => $Amt[0]['var_value'],'charge_days_limit' => $Days[0]['var_value']));

     if ($request->isMethod('post'))
     {
            $this->form->bind($request->getParameter('globalsetting'));

            if ($this->form->isValid())
            {
                   $Allparam=$request->getPostParameters();


                   $amtValue=$Allparam['globalsetting']['charge_amount_limit'];
                   $Amt = Doctrine::getTable('EwalletSetting') ->updateChargeAmountLimit($amtValue);


                   $dayValue=$Allparam['globalsetting']['charge_days_limit'];
                   $Amt = Doctrine::getTable('EwalletSetting') ->updateChargeDaysLimit($dayValue);

                    $this->getUser()->setFlash('notice', sprintf('Details have been Updated'));

            }
     }

  }
  public function executeGetMenu(sfWebRequest $request)
  { 
       $this->getContext()->getConfiguration()->loadHelpers('Url');
       $this->getContext()->getConfiguration()->loadHelpers('Tag');
        $menuObj=new menuHelper();
        $serializeMenu=sfContext::getInstance()->getUser()->getAttribute('menu');

//        if(!isset($serializeMenu) ||  ''==$serializeMenu){
//           $menuObj->setMenuSession();
//           $serializeMenu=sfContext::getInstance()->getUser()->getAttribute('menu');
//        }

        $SMPoint = $request->getParameter('SMPoint');
        $totMenu = $request->getParameter('TotMenu');
        $SetPoint=sfContext::getInstance()->getUser()->setAttribute('SMPoint', $SMPoint);
        
        $allMenu = unserialize($serializeMenu);
        $permitedMenu = $menuObj->renderMenuYAMLPerms($allMenu);
        //echo "<pre>";
        //print_r($permitedMenu);die;

        $menuObj->renderMenuYAML($permitedMenu,$SMPoint,8);
        die;
  }
}
