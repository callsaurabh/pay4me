<?php
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php echo ePortal_legend('Details'); ?>

 <table width="100%" class="dtls_table">
     <thead>
    <tr><td style="width:40%">System ID:</td><td><?php echo $bankMappingDetails->getSystemId(); ?>  </td></tr>
    <tr><td>Created Date:</td><td><?php echo $bankMappingDetails->getCreatedAt(); ?>  </td></tr>
  
        </thead>
         </table>


<?php echo ePortal_legend('Please Map Bank with System Id'); ?>

<?php if (!$details) { ?>
        <div class="wrapForm2">
            <form id="bank_form" action="<?php  echo url_for('karafMgmt/info?id=' . $bankMappingDetails->getId()) ?>" method="post" <?php  $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

        <?php
        echo "<div id = 'Divbank' name = 'Divbank' >";
        echo formRowComplete($form['bank']->renderLabel(), $form['bank']->render(), '', 'bank', 'err_bank', 'bank_row', $form['bank']->renderError());
        echo "</div>";
        ?>
     
                  <div class="divBlock">
            <center>
<?php  echo button_to(__('Cancel'),'karafMgmt/index',array('class'=>'formCancel'));?>
 <?php // echo button_to(__('Save'),'',array('class'=>'formSubmit','confirm' => 'Bank once Mapped cannot be updated.', 'post'=>true));?>
                 <input type="submit" value="<?php echo __('Submit')?>" class="formSubmit" onclick="return chkValidation(this)" />
            </center>
        </div>
   </form>
</div>
<?php } ?>
<script>
    function chkValidation(){
        var bank = $('#bank').val();
        if(bank==''){
            $('#err_bank').html('Please select Bank');
            return false;
        }else{
            var response = confirmMapping();
            if(response == true){
                return true;
            }else
                return false;
    }
}
function confirmMapping()
{
    var mapping= confirm("Are you sure about mapping of this System id with selected Bank?");
    if (mapping== true)
    {return true;
    }else
        return false;
}
    </script>
