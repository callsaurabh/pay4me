<div id="search_result" >
<?php
use_helper('Pagination');?>
<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<?php //use_helper('Form')    ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/index', 'name=karaf_form id=karaf_form_form'); ?>
    <?php echo ePortal_legend('Karaf Management Console'); ?>

     <?php
    echo "<div id = 'Divbank' name = 'Divbank' >";
    echo formRowComplete($form['bank']->renderLabel(),$form['bank']->render(),'','bank','err_bank','bank_row',$form['bank']->renderError());
    echo "</div>";
    ?>
    <?php echo formRowComplete($form['status']->renderLabel(), $form['status']->render(), '', 'status', 'err_status', 'status_row', $form['status']->renderError()); ?>

    <div class="divBlock">
        <center>
            <input type="Submit" value='Search' class="formSubmit">
        </center>
    </div>
</form>
</div>


<div class="form_nav_outer">
    <center>
    <?php echo image_tag('../img/ajax-loader.gif', array('id' => 'chargeLoader', 'style' => 'display:none')); ?>
    </center>
   </div>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo ($pager->getNbResults() ? $pager->getFirstIndice() : '0' ) ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <?php
            if (($pager->getNbResults()) > 0) {
                $limit = sfConfig::get('app_records_per_page');
                $page = $sf_context->getRequest()->getParameter('page', 0);
                $i = max(($page - 1), 0) * $limit;
        ?>
                <thead>
                    <tr class="horizontal">
                        <th>System ID</th>
                        <th>Bank Name</th>
                        <th>Status</th>
                        <th>Created Date</th>
                        <th>Associated Date</th>
                    </tr>
                </thead>

        <?php ?>
        <?php
                foreach ($pager->getResults() as $result):
                    $i++;
                    if ($result->getStatus() == '0') {
                        $statusCode = "Inactive";
                    } else {
                        $statusCode = "Active";
                    }
        ?>
                    <tr>
                        <td align="center" ><?php if(is_null($result->getBank())){echo link_to($result->getSystemId(),$sf_context->getModuleName() . '/' .'info?id='.$result->getId()) ;}else{ ?>
                           <?php // echo link_to($result->getSystemId(),$sf_context->getModuleName() . '/' .'bundleList?id='.$result->getId()."&bank_version=".$result->getBank()->getBiProtocolVersion().'&bank='.$result->getBankId()) ; ?>
                            <a href="#" onclick="getRequestId( <?php echo $result->getBankId()  ?>)"><?php echo $result->getSystemId()  ?></a>
                        <?php    } ?></td>
                        <td align="left"><?php if($result->getBank()!='')echo  $result->getBank()->getBankName();

                        else echo"-"; ?></td>

                        <td align="left"><?php echo $statusCode; ?></td>
                        <td align="left"><?php echo $result->getCreatedAt(); ?></td>
                        <td align="left" ><?php
                        if($result->getBankId()!='')
                        echo $result->getUpdatedAt();
                        else
                            echo "NA";
                        ?></td>


            <?php endforeach; ?>

            <?php
                    $url = "";
                     if ($bank != "") {
                        $url .= "bank=" . $bank;
                    }
                     if ($status != "") {
                        $url .= "&status=" . $status;
                    }
            ?>
                <tfoot>
                    <tr><td colspan="8"><div class="paging pagingFoot">
                        <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . '?' . $url), 'search_result') ?>
                    </div></td></tr>
        </tfoot>
        <?php } else {
        ?>
                        <tr><td  align='center' class='error' >No Record Found</td></tr>
        <?php } ?>
    </table>
</div>
</div>
<script>
    function getRequestId( bank)
        {
            $("#chargeLoader").show();
            var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{bank:bank},
            async:false}).responseText;
        if(!data){
            $("#chargeLoader").hide();
            alert('Bank Not enabled for Command Requests');return false;
        }
        else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else {
            setTimeout("checkResponse("+data+", "+bank+")",1000);
        }
    }
}

function checkResponse(id, bank)
{
    var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
    var data=$.ajax({
        url: url,
        type:'post',
        data:{id:id},
        async:false}).responseText;
    if(!data){
        $("#chargeLoader").hide();
        alert('No response from Bank Middleware. Please try after sometime'); return false;
    }else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else{
            getListLoc(bank , data);
        }
    }

}
function getListLoc( bank , listresult)
        {
            var loc =1 ;
            var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{bank:bank, loc:loc},
            async:false}).responseText;
        if(!data){
            $("#chargeLoader").hide();
            alert('Bank Not enabled for Command Requests');return false;
        }
        else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else {
            setTimeout("checkResponseforListLoc("+data+", "+bank+", "+listresult+")",1000);
        }
    }
}
function checkResponseforListLoc(id, bank , listresult)
{
    var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
    var data=$.ajax({
        url: url,
        type:'post',
        data:{id:id},
        async:false}).responseText;
    if(!data){
        $("#chargeLoader").hide();
        alert('No response from Bank Middleware. Please try after sometime'); return false;
    }else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else{
            window.location = "<?php echo url_for('karafMgmt/fetchResponse?id='); ?>"+listresult+"/bank/"+bank+"/listresult/"+data;
        }
    }

}
    </script>
