<?php use_helper('Pagination'); ?>
<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<?php //use_helper('Form')         ?>
<div>

  
<div align="right">
<a  href="#" class="command-link" onclick="sendCustomCommand(<?php echo $bankId ?> )"><?php echo 'Custom Command'; ?></a>
</div>
    <?php echo ePortal_listinghead('Details '); ?>
    <table width="100%" class="dtls_table">

        <thead>
            <tr><td style="width:40%">System Id : </td><td><?php echo $systemInfo->getFirst()->getSystemId() ?></td></tr>
            <tr><td>Bank Name : </td><td><?php echo $bankName ?></td></tr>
            <tr><td>Created Date : </td><td><?php echo $systemInfo->getFirst()->getCreatedAt() ?></td></tr>
            <tr><td>Associated Date : </td><td><?php echo $systemInfo->getFirst()->getUpdatedAt() ?></td>

            </tr>
        </thead>

    </table>
  <div class="form_nav_outer">
    <center>
    <?php echo image_tag('../img/ajax-loader.gif', array('id' => 'chargeLoader', 'style' => 'display:none')); ?>
    </center>
   </div>
    <?php echo ePortal_listinghead('Bundles For ' . $bankName); ?>

</div><br/>

<script>
    var currentLinkName = ($("#enableRefresh").html());
    </script>

<table width="100%">
  <tr>
    <td align="left" width="50%">
       <a href="#" style="display:none;" id="refresh_href" class="command-link" onclick="getRequestIdRefresh( <?php echo $bankId ?>,true,currentLinkName)"><?php echo "Refresh"  ?></a>
    </td>
    <td id="refresh_div" align="right" width="50%">
       <a href="#" id="enableRefresh" class="command-link" onclick="enableRefresh()"></a>
    </td>
  </tr>
</table>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >

        <thead>
            <tr class="horizontal">
                <th>S.No</th>
                <th>Bundle Name</th>
                <th>State</th>
                <th>Action</th>

            </tr>
        </thead>

        <?php
        $i = 0;
        // sort($decodedResponse);
        foreach ($listArr as $result) {
        $bundleDetails = explode(',' ,$result);
        ?>
                <tr>
                    <td align="center" ><?php echo++$i . "."; ?></td>

                    <td align="center" ><?php

                echo $bundleDetails['1'];
        ?></td>
                    <td align="center"><?php echo $bundleDetails['2']; ?></td>
            <td>
                <a href="#" onclick="sendRequest(<?php echo $bundleDetails['0'] ?>, 'Active'  )"><?php echo 'Start'; ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" onclick="sendRequest(<?php echo $bundleDetails['0'] ?>, 'Resolved' )"><?php echo 'Stop'; ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php $allowUpdate = bankIntegrationHelper::getUpdate($bundleDetails , $bankId);
                if($allowUpdate) {
                ?>
                <a  href="#"  title="New Version <?php echo $allowUpdate ?> is available " onclick="sendRequestForUpdate(<?php echo $bundleDetails['0'] ?> )"><?php echo 'Update'; ?></a>&nbsp;
                <?php } ?>
            </td>




        </tr>

        <?php } ?>


    </table>
</div>


<div class="divBlock">
    <center>



         <?php echo button_to(__('Back'), 'karafMgmt/index', array('class' => 'formCancel')); ?>&nbsp;

<!--            <a href="#" onclick="window.open('karafMgmt/customCommand', '', 'width=800, height=400, scrollbars=1, resizable=0')"><?php // echo 'Custom Command' ;   ?></a>-->

    </center>
</div>

<script>
    var bank_id = <?php echo $bankId; ?>;
    var intervalID ;
    var enable_autorefresh = <?php echo $enable_autorefresh; ?>;

    var currentLinkName = ($("#enableRefresh").html());

    $(document).ready(function() {
        var currentLink = '<?php echo $currentLinkLoc; ?>';
        if ( currentLink!=0 ) {
            if(currentLink == 'Enable Auto Refresh'){
                $('#enableRefresh').html('Disable Auto Refresh');
                var refresh  =1;
                $('#refresh_href').hide();
            }
            else{
                $("#enableRefresh").html('Enable Auto Refresh');
                var refresh  =0;
                $('#refresh_href').show();
            }
        }
        else {
            if (enable_autorefresh==1) {
                $('#enableRefresh').html('Disable Auto Refresh');
                 var refresh  =1;
                 $('#refresh_href').hide();
            } else {
                $('#enableRefresh').html('Enable Auto Refresh');
                var refresh  =0;
                $('#refresh_href').show();
            }
        }
           // alert($("#enableRefresh").html());
        if(enable_autorefresh==1 && $("#enableRefresh").html()!='Enable Auto Refresh' ){
            var isLoaderVisible = false;
            var currentLinkName = ($("#enableRefresh").html());
            intervalID = setInterval("getRequestIdRefresh("+bank_id+","+isLoaderVisible+",'"+currentLinkName+"')",<?php echo $timeInterval ?>);

        }
        else{
            
           // alert(refresh)
            var currentLinkName = ($("#enableRefresh").html());
            if(refresh){
                var  timer = setInterval("getRequestIdRefresh("+bank_id+","+isLoaderVisible+",'"+currentLinkName+"')",<?php echo $timeInterval ?>);
            }else{
                window.clearInterval(timer);
                window.clearInterval(intervalID);
                timer = null
                intervalID = null
            }
        }
    });


function enableRefresh(label){
 var currentLinkName = ($("#enableRefresh").html());
    $.post('<?php echo url_for('karafMgmt/setSessionVariable') ?>', {'label':currentLinkName}, function (data) {
        location.reload();
    }
    );
}




    function sendRequest(id , req)
    {
        $("#chargeLoader").show();
        var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id,bank:<?php echo $bankId ?>, req:req},
            async:false}).responseText;

        if(data ){
            if(data =='logout'){
                location.reload();
            }else if(data =='0'){
                $("#chargeLoader").hide();
                alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
            }
            else{
                setTimeout("checkResponse("+data+","+<?php echo $bankId ?>+",'"+req+"',"+id+")",1000);
            }
        }
    }
    function sendCustomCommand(bank)
    {
        var webpath = "<?php echo public_path('/index.php/', true); ?>";
        window.open(webpath+'karafMgmt/customCommand?bank='+bank, '', 'width=800, height=400, scrollbars=1, resizable=0');


    }

    function getRequestId(bank , req , id)
    {
        var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{ bank:<?php echo $bankId ?>,bundleid:id},
            async:false}).responseText;
        if(data){
            if(data =='logout'){
                location.reload();
            }else if(data =='0'){
                $("#chargeLoader").hide();
                alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
            }
            else{
                setTimeout("checkResponseStartStop("+data+", "+<?php echo $bankId ?>+",'"+req+"',"+id+")",1000);
            }
        }
    }

    function checkResponse(id, bank, req, bundleId)
    {

        var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id, req:req,bundleId:bundleId},
            async:false}).responseText;

        if(!data ){
            $("#chargeLoader").hide();
            alert('No response from Bank Middleware. Please try after sometime'); return false;
        }else{
            if( data =='logout'){
                location.reload();
            }
            else if(data == req){
                $("#chargeLoader").hide();
                alert('Bundle is already in '+req+' state');return false;

            }else{

                sendRequestStartStop(bundleId,req);
            }
        }

    }

    function sendRequestForUpdate(id)
    {
        $("#chargeLoader").show();
        var url = '<?php echo url_for("karafMgmt/bundleUpdate"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id,bank:<?php echo $bankId ?>},
            async:false}).responseText;
        if(data){
            if(data =='logout'){
                location.reload();
            }else if(data =='0'){
                $("#chargeLoader").hide();
                alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
            }
            else{
                setTimeout("checkResponseForHeaders("+data+", "+<?php echo $bankId ?>+","+id+")",1000);
            }
        }

    }

    function checkResponseForHeaders(id, bank, bundleId)
    {
        var url = '<?php echo url_for("karafMgmt/CheckResponseForHeaders"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id},
            async:false}).responseText;
        if(! data){
            $("#chargeLoader").hide();
            alert('No response from Bank Middleware. Please try after sometime'); return false;
        }else{
            if(data =='logout'){
                location.reload();
            }else{
                checklistLoc(id , data, bundleId)
            }
        }
    }

    function checklistLoc(id, ver , bundleId)
    {
        var loc =1;
        var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id, ver:ver,loc:loc,bank:<?php echo $bankId ?>},
            async:false}).responseText;
        if(! data){
            $("#chargeLoader").hide();
            alert('No response from Bank Middleware. Please try after sometime'); return false;
        }else{
            if(data =='logout'){
                location.reload();
            }else{
                setTimeout("getResponseForlistLoc("+data+","+<?php echo $bankId ?>+",'"+ver+"',"+bundleId+")",1000);
            }
        }


    }
    function sendRequestStartStop(id , req)
    {
        var result1 ;
        var url = '<?php echo url_for("karafMgmt/bundleStartStop"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id,bank:<?php echo $bankId ?>, req:req},
            async:false}).responseText;
        if(data){
            if(data =='logout'){
                location.reload();
            }else if(data =='0'){
                $("#chargeLoader").hide();
                alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
            }else
                getRequestId(<?php echo $bankId ?>, req , id);
        }
    }
    function checkResponseStartStop(id, bank , req , bundleid)
    {

        var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id},
            async:false}).responseText;
        if(!data){
            $("#chargeLoader").hide();
            alert('No response from Bank Middleware. Please try after sometime'); return false;
        }else{
            if(data =='logout'){
                location.reload();
            }else{
                 getListLoc(bank , data, req ,bundleid );
            }
        }

    }

   function getListLoc( bank , listresult , req , bundleid)
        {
            var loc =1 ;
            var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{bank:bank, loc:loc},
            async:false}).responseText;
        if(!data){
            $("#chargeLoader").hide();
            alert('Bank Not enabled for Command Requests');return false;
        }
        else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else {
            setTimeout("checkResponseforListLoc("+data+", "+bank+", "+listresult+", '"+req+"',"+bundleid+")",1000);
        }
    }
}
function checkResponseforListLoc(id, bank , listresult , req ,bundleid )
{
    var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
    var data=$.ajax({
        url: url,
        type:'post',
        data:{id:id},
        async:false}).responseText;
    if(!data){
        $("#chargeLoader").hide();
        alert('No response from Bank Middleware. Please try after sometime'); return false;
    }else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else{
            window.location = "<?php echo url_for('karafMgmt/fetchResponse?id='); ?>"+listresult+"/req/"+req+"/bank/"+bank+"/bundleid/"+bundleid+"/listresult/"+data;
        }
    }

}

    function getResponseForlistLoc(reqid, bank, ver, bundleId)
    {
        var loc =1 ;
        var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:reqid, ver:ver,bundleId:bundleId, loc:loc, bankId:bank},
            async:false}).responseText;
        if(!data ){
            $("#chargeLoader").hide();
            alert('No response from Bank Middleware. Please try after sometime'); return false;
        }else{

            if(data =='logout'){
                location.reload();
            }else if(data =='0'){
                $("#chargeLoader").hide();
                alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
            }
            else{
                setTimeout("checkResponseForList("+data+","+reqid+","+bundleId+","+<?php echo $bankId ?>+")",1000);
            }

        }

    }

    function checkResponseForList(id,listlocreqid,bundleId, bank)
    {
        var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{id:id, bank:bank},
            async:false}).responseText;
        if(!data ){
            $("#chargeLoader").hide();
            alert('No response from Bank Middleware. Please try after sometime'); return false;
        }else{
            if(data =='logout'){
                location.reload();
            }
             else{
                setTimeout("checkResponseForListLocation("+data+","+<?php echo $bankId ?>+","+listlocreqid+","+bundleId+")",1000);
        }
}
    }
   function checkResponseForListLocation(listrespid, bank , listlocreqid ,bundleid )
{

    var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
    var req = "update";
    var data=$.ajax({
        url: url,
        type:'post',
        data:{id:listlocreqid},
        async:false}).responseText;
    if(!data){
        $("#chargeLoader").hide();
        alert('No response from Bank Middleware. Please try after sometime'); return false;
    }else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else{
            window.location = "<?php echo url_for('karafMgmt/fetchResponse?id='); ?>"+listrespid+"/req/'"+req+"'/bank/"+bank+"/bundleid/"+bundleid+"/listresult/"+data;
        }
    }

}
    function getRequestIdRefresh( bank, isLoaderVisible,currentLinkName)
        {
            
            if(isLoaderVisible)
            $("#chargeLoader").show();
            var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{bank:bank},
            async:false}).responseText;
        if(!data){
            $("#chargeLoader").hide();
            alert('Bank Not enabled for Command Requests');return false;
        }
        else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else {
            setTimeout("checkResponseRefresh("+data+", "+bank+",'"+currentLinkName+"')",1000);
        }
    }
}

function checkResponseRefresh(id, bank ,currentLinkName)
{
    var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
    var data=$.ajax({
        url: url,
        type:'post',
        data:{id:id},
        async:false}).responseText;
    if(!data){
        $("#chargeLoader").hide();
        alert('No response from Bank Middleware. Please try after sometime'); return false;
    }else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else{
            getListLocRefresh(bank , data , currentLinkName);
        }
    }

}
function getListLocRefresh( bank , listresult , currentLinkName)
        {
            var loc =1 ;
            var url = '<?php echo url_for("karafMgmt/bundleList"); ?>';
        var data=$.ajax({
            url: url,
            type:'post',
            data:{bank:bank, loc:loc},
            async:false}).responseText;
        if(!data){
            $("#chargeLoader").hide();
            alert('Bank Not enabled for Command Requests');return false;
        }
        else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else {
            setTimeout("checkResponseforListLocRefresh("+data+", "+bank+", "+listresult+",'"+currentLinkName+"')",1000);
        }
    }
}
function checkResponseforListLocRefresh(id, bank , listresult , currentLinkName)
{
    var url = '<?php echo url_for("karafMgmt/CheckResponse"); ?>';
    var data=$.ajax({
        url: url,
        type:'post',
        data:{id:id},
        async:false}).responseText;
    if(!data){
        $("#chargeLoader").hide();
        alert('No response from Bank Middleware. Please try after sometime'); return false;
    }else{
        if(data =='logout'){

            location.reload();
        }else if(data =='0'){
            $("#chargeLoader").hide();
            alert('Sorry,we could not process your request as Activemq is down. Please contact Administrator');return false;
        }
        else{
            window.location = "<?php echo url_for('karafMgmt/fetchResponse?id='); ?>"+listresult+"/bank/"+bank+"/listresult/"+data+"/currentLinkLoc/"+currentLinkName;
        }
    }

}
</script>