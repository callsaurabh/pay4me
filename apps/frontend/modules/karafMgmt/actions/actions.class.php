<?php

/**
 * karakMgmt actions.
 *
 * @package    mysfp
 * @subpackage karakMgmt
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class karafMgmtActions extends sfActions {

    /**
     * WP060
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $selBankChoices = array('' => '-- Select Bank --');
        //Banklist on the form whose bi enabled is true.
        $bankListObj = Doctrine::getTable('Bank')->getBIEnabledBankList();
        $bankListArr = $bankListObj->toArray();
        $EnablednakArr = array();
        if (count($bankListArr) > 0) {
            foreach ($bankListArr as $k => $v) {
                $EnablednakArr[$v['id']] = $v['bank_name'];
            }
        }
        $bankList = $selBankChoices + $EnablednakArr;
        $this->bank = '';
        $this->status = '';
        if ($request->getParameter('bank')) {
            $this->bank = $request->getParameter('bank');
        }
        if ($request->getParameter('status') != '') {
            $this->status = $request->getParameter('status');
        }
        $this->loc = 0;
        //checkform is used to distinguish from which action it is called
        $this->form = new karafMgmtForm(array(), array('bankList' => $bankList, 'bank' => $this->bank, 'status' => $this->status, 'checkForm' => '0'));
        //By default listing
        $obj = Doctrine::getTable('BankMwMapping')->getRequestDetails($this->bank, $this->status);
        //pager
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('BankMwMapping', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($obj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    /*
     * WP060
     * Executes info action
     *
     * @param sfRequest $request A request object
     */

    public function executeInfo(sfWebRequest $request) {
        $this->details = 0;
        $selBankChoices = array('' => '-- Select Bank --');
        $bankListObj = Doctrine::getTable('Bank')->getBIEnabledBankList();
        $bankListArr = $bankListObj->toArray();
        $EnablednakArr = array();
        if (count($bankListArr) > 0) {
            foreach ($bankListArr as $k => $v) {
                $EnablednakArr[$v['id']] = $v['bank_name'];
            }
        }
        $bankList = $selBankChoices + $EnablednakArr;
        $bankMappingId = $request->getParameter('id');
        $this->bankMappingDetails = Doctrine::getTable('BankMwMapping')->find($bankMappingId);
        //$this->details is set to 1 in case association is set
        if ($this->bankMappingDetails->getBankId() != '')
            $this->details = 1;
        else {
            $this->form = new karafMgmtForm(array(), array('bankList' => $bankList, 'checkForm' => '1'));
            if ($request->getParameter('bank')) {
                $bankId = $request->getParameter('bank');
                $bankdetails = Doctrine::gettable('Bank')->find($bankId);
                $chkDuplicate = Doctrine::getTable('BankMwMapping')->findByBankId($bankId);
                if ($chkDuplicate->count()) {
                    $this->getUser()->setFlash('notice', 'Selected bank is already maaped with another System ID', true);
                    $this->redirect('karafMgmt/info?id=' . $bankMappingId);
                } else {
                    //Query to update the association with the bank
                    Doctrine::getTable('BankMwMapping')->updateRequest($bankId, $bankMappingId);
                    $this->getUser()->setFlash('notice', 'System ID ' . $this->bankMappingDetails->getSystemId() . ' is associated with ' . $bankdetails->getBankName(), true);

                    $this->redirect('karafMgmt/index');
                }
            }
        }
    }

    /**
     * WP060
     * executes bundleList action
     */
    public function executeBundleList(sfWebRequest $request) {

        $bankDetails = Doctrine::getTable('Bank')->find($request->getParameter('bank'));
        $this->bank = $bankDetails->getBankName();
        $bi_protocolVersion = $bankDetails->getBiProtocolVersion();

        $className = "BankIntegration" . $bi_protocolVersion . "Manager";
        $manager = new $className();
        try {
            if ($request->hasParameter('loc'))
                $param = '-l';
            else
                $param = '';
            $command = "osgi:list";
            $addcmdrequestResult = $manager->addCommandRequest($request->getParameter('bank'), $command, $param);
            $bankIntegraionObject = new bankIntegration();
            $bankIntegraionObject->QueueProcessing($manager);

            if ($bi_protocolVersion == 'v1') {
                return $this->renderText(false);
            }

            //$result = bankIntegrationHelper::getResult($className , $request->getParameter('bank') , $command , $param);
        } catch (Exception $e) {
            return $this->renderText($e->getCode());
        }
        return $this->renderText($manager->getRequestObject()->getId());
    }

    /**
     *
     * WP060
     * executes Bundlestart stop action
     * @param sfWebRequest $request
     * @return <int>
     */
    public function executeBundleStartStop(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $bankDetails = Doctrine::getTable('Bank')->find($request->getParameter('bank'));
        $this->bankname = $bankDetails->getBankName();
        $bi_protocolVersion = $bankDetails->getBiProtocolVersion();


        $className = "BankIntegration" . $bi_protocolVersion . "Manager";
        $manager = new $className();
        try {
            if ($request->getParameter('req') == 'Active')
                $command = "osgi:start";
            else
                $command = "osgi:stop";
            $addcmdrequestResult = $manager->addCommandRequest($request->getParameter('bank'), $command, $id);
            $bankIntegraionObject = new bankIntegration();
            $bankIntegraionObject->QueueProcessing($manager);
        } catch (Exception $e) {
            return $this->renderText($e->getCode());
        }
        return $this->renderText($manager->getRequestObject()->getId());
    }

    /**
     * WP060
     * executes check response function
     * @param sfWebRequest $request
     * @return <int>
     */
    public function executeCheckResponse(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $count = sfConfig::get('app_karaf_response_attempts');

        $bankIntObj = new bankIntegrationHelper();
        $commandResponseDetails = $bankIntObj->checkFirstResponse($id, $count);

//        $commandResponseDetails = Doctrine::getTable('MwResponse')->findByRequestId($id);
        if ($commandResponseDetails && $commandResponseDetails->count()) {
            if ($request->hasparameter('req')) {
//                $commandResponseDetails = Doctrine::getTable('MwResponse')->find($commandResponseDetails->getId());
                $response = $commandResponseDetails->getMessageText();
                $decodedResponse = (array) json_decode($response);
                $this->decodedResponse = explode("\n", $decodedResponse['stdout-response']);
                foreach ($this->decodedResponse as $key => $res) {
                    if (trim(substr($res, 1, 4)) == $request->getParameter('bundleId')) {
                        $bundle = $key;
                        break;
                    }
                }
                if (trim(substr($this->decodedResponse[$bundle], 8, 11)) == ($request->getParameter('req')))
                    return $this->renderText(trim(substr($this->decodedResponse[$bundle], 8, 11)));
                else
                    return $this->renderText(substr($this->decodedResponse[$bundle], 8, 11));
            }else if ($request->hasparameter('loc')) {
//                $commandResponseDetails = Doctrine::getTable('MwResponse')->find($commandResponseDetails->getFirst()->getId());
                $response = $commandResponseDetails->getMessageText();
                $decodedResponse = (array) json_decode($response);
                $this->decodedResponse = explode("\n", $decodedResponse['stdout-response']);
                foreach ($this->decodedResponse as $key => $res) {
                    if (trim(substr($res, 1, 4)) == $request->getParameter('bundleId')) {
                        $bundle = $key;
                        break;
                    }
                }
                $bundleName = trim(substr(strrchr($this->decodedResponse[$bundle], "/"), 1));
                $bundlestate = trim(substr($this->decodedResponse[$bundle], 8, 11));
                $bankAcronym = Doctrine::getTable('Bank')->find($request->getParameter('bankId'))->getAcronym();
                $bankSpecific = strpos($bundleName, $bankAcronym);
                sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
                $path = public_path('', true);
                if (!$bankSpecific) {
                    $dirname = sfConfig::get('sf_web_dir') . '/' . sfConfig::get('app_bundles_soft_link_common') . '/' . $bundleName;
                    $linkName = $path . sfConfig::get('app_bundles_soft_link_common') . '/' . $bundleName;
                } else {
                    $dirname = sfConfig::get('sf_web_dir') . '/' . sfConfig::get('app_bundles_soft_link_bankspecific') . '/' . $bankAcronym . '/' . $bundleName;
                    $linkName = $path . sfConfig::get('app_bundles_soft_link_bankspecific') . '/' . $bankAcronym . '/' . $bundleName;
                }
                $symLink = readLink($dirname);
                $newVersion = explode('/', $symLink);
                if (!$bankSpecific)
                    $newVersion = $newVersion[5];
                else
                    $newVersion = $newVersion[7];
                if ($request->getParameter('ver') == $newVersion) {
                    return $this->renderText(false);
                } else {
                    $request->setParameter('bundlestate', $bundlestate);
                    $request->setParameter('bundleLocation', $linkName);
                    $this->forward('karafMgmt', 'BundleUpdateVersion');
                }
            }
            //osgi:list
            else {
                return $this->renderText($commandResponseDetails->getId());
            }
        } else {
            return $this->renderText($commandResponseDetails);
        }
    }

    /**
     * WP060
     * executes checkresponse for headers action
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeCheckResponseForHeaders(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $count = sfConfig::get('app_karaf_response_attempts');

        $bankIntObj = new bankIntegrationHelper();
        $commandResponseDetails = $bankIntObj->checkFirstResponse($id, $count);

        if ($commandResponseDetails && $commandResponseDetails->count()) {
            $response = $commandResponseDetails->getMessageText();
            $decodedResponse = (array) json_decode($response);
            $this->decodedResponse = explode("\n", $decodedResponse['stdout-response']);
            foreach ($this->decodedResponse as $key => $val) {
                $i = '';
                if (strstr($val, 'Bundle-Version')) {
                    $i = trim(substr(strrchr($val, "="), 1, 6));
                    break;
                }
            }
            return $this->renderText($i);
        } else {
            return $this->renderText(false);
        }
    }

    /**
     * WP060
     * executeds bundleupdateversion action
     */
    public function executeBundleUpdateVersion(sfWebRequest $request) {
        $id = $request->getParameter('bundleId');
        $bankIntObj = new bankIntegrationHelper();
        $count = sfConfig::get('app_karaf_response_attempts');
        $bankDetails = Doctrine::getTable('Bank')->find($request->getParameter('bankId'));
        $bi_protocolVersion = $bankDetails->getBiProtocolVersion();
        $className = "BankIntegration" . $bi_protocolVersion . "Manager";
        $manager = new $className();
        try {
            if ($request->getParameter('bundlestate') == 'Active') {
                $command = "osgi:stop";
                $param = $id;
                $addcmdrequestResult = $manager->addCommandRequest($request->getParameter('bankId'), $command, $param);
                $bankIntegraionObject = new bankIntegration();
                $bankIntegraionObject->QueueProcessing($manager);
                $requestId = $manager->getRequestObject()->getId();
                $commandResponseDetails = $bankIntObj->checkFirstResponse($requestId, $count);
                if ($commandResponseDetails && $commandResponseDetails->count()) {
                    $requestId = $this->bundleUpdate($id, $request->getParameter('bundleLocation'), $request->getParameter('bankId'),$bi_protocolVersion);
                    $commandResponseDetails = $bankIntObj->checkFirstResponse($requestId, $count);
                    if ($commandResponseDetails && $commandResponseDetails->count()) {
                        $command = "osgi:start";
                        $param = $id;
                        $addcmdrequestResult = $manager->addCommandRequest($request->getParameter('bankId'), $command, $param);
                        $bankIntegraionObject = new bankIntegration();
                        $bankIntegraionObject->QueueProcessing($manager);
                        $requestId = $manager->getRequestObject()->getId();
                        $commandResponseDetails = $bankIntObj->checkFirstResponse($requestId, $count);
                    }
                } else {
                    return $this->renderText(false);
                }
            } else {
                $requestId = $this->bundleUpdate($id, $request->getParameter('bundleLocation'), $request->getParameter('bankId'),$bi_protocolVersion);
                $commandResponseDetails = $bankIntObj->checkFirstResponse($requestId, $count);
            }
            if ($commandResponseDetails && $commandResponseDetails->count()) {
                $command = "osgi:list";
                $param = '';
                $addcmdrequestResult = $manager->addCommandRequest($request->getParameter('bankId'), $command, $param);
                $bankIntegraionObject = new bankIntegration();
                $bankIntegraionObject->QueueProcessing($manager);
            } else {
                return $this->renderText(false);
            }
        } catch (Exception $e) {
            return $this->renderText($e->getCode());
        }
        return $this->renderText($manager->getRequestObject()->getId());
    }

    /**
     * WP060
     * executes check response action
     * @param sfWebRequest $request
     */
    public function executeFetchResponse(sfWebRequest $request) {
        $this->msg = false;
        $id = $request->getParameter('id');
        $this->bankId = $request->getParameter('bank');
        $this->enable_autorefresh =  sfConfig::get('app_enable_auto_refresh');
        $this->timeInterval =  sfConfig::get('app_time_interval_for_auto_refresh');
        $this->currentLinkLoc= $this->getUser()->getAttribute('currentLinkLoc', '0');
        $this->systemInfo = Doctrine::getTable('BankMwMapping')->findByBankId($this->bankId);
        $this->bankName = Doctrine::getTable('Bank')->find($request->getParameter('bank'))->getBankName();
        $commandResponseDetails = Doctrine::getTable('MwResponse')->find($id);
        $response = $commandResponseDetails->getMessageText();
        $decodedResponse = (array) json_decode($response);
        //orgi:list
        $this->decodedResponse = explode("\n", $decodedResponse['stdout-response']);
        $arr1 = array();
        $i = 0;
        foreach ($this->decodedResponse as $result) {
            $res = stristr($result, "]");
            if ($res) {
                $ArrayKey = trim(substr($result, 1, 4));
                $ArrayValue = trim(substr($result, 1, 4)).','.trim(substr(strrchr($res, "]"), 1)).','.trim(substr($result, 8, 11));
                $bundlePos = strpos(trim(substr(strrchr($res, "]"), 1)), '(');
                if (trim(substr(strrchr($res, "]"), 1)) != in_array(trim(substr(strrchr($res, "]"), 1,$bundlePos)), sfConfig::get('app_bundle_name')))
                    $arr1[$ArrayKey] = $ArrayValue;
            }
        }
        if ($request->hasParameter('listresult')) {
            $commandResponseDetails = Doctrine::getTable('MwResponse')->find($request->getParameter('listresult'));
            $response = $commandResponseDetails->getMessageText();
            $decodedResponse = (array) json_decode($response);
            //orgi:list -l
            $this->symbolicName = explode("\n", $decodedResponse['stdout-response']);
            foreach ($this->symbolicName as $result) {
                $res = stristr($result, "]");
                if ($res) {
                    $ArrayKey = trim(substr($result, 1, 4));
                    $ArrayValue = trim(substr(strrchr($result, "/"), 1));
                    if (isset($arr1[$ArrayKey]))
                        $arr1[$ArrayKey] = $arr1[$ArrayKey].','. $ArrayValue;
                }
            }
        }
        $this->listArr = $arr1;
        if ($request->hasParameter('req')) {
            foreach ($this->decodedResponse as $key => $val) {

                if (trim(substr($val, 1, 4)) == $request->getParameter('bundleid')) {
                    $bundlename = trim(substr(strrchr($val, "]"), 1));
                    break;
                }
            }
            $this->msg = true;
            $res = $request->getParameter('req');
            if ($res == 'Resolved') {
                $this->getUser()->setFlash('notice', 'Bundle '.$bundlename.' has been stopped Successfully.', false);
            } else if ($res == 'Active') {
                $this->getUser()->setFlash('notice', 'Bundle '.$bundlename.' has been started Successfully.', false);
            } else {
                $this->getUser()->setFlash('notice', 'Bundle '.$bundlename.' has been updated Successfully.', false);
            }
        }
    }

    /**
     * WP060
     * exectes bundle update action
     * @param sfWebRequest $request
     * @return <int>
     */
    public function executeBundleUpdate(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $bankDetails = Doctrine::getTable('Bank')->find($request->getParameter('bank'));
        $this->bankname = $bankDetails->getBankName();
        $bi_protocolVersion = $bankDetails->getBiProtocolVersion();


        $className = "BankIntegration" . $bi_protocolVersion . "Manager";
        $manager = new $className();
        try {
            $command = "osgi:headers";
            $addcmdrequestResult = $manager->addCommandRequest($request->getParameter('bank'), $command, $id);
            $bankIntegraionObject = new bankIntegration();
            $bankIntegraionObject->QueueProcessing($manager);
        } catch (Exception $e) {
            return $this->renderText($e->getCode());
        }
        return $this->renderText($manager->getRequestObject()->getId());
    }

    /**
     * WP060
     * excetes bundle update action
     * @param sfWebRequest $request
     */
    public function executeCustomCommand(sfWebRequest $request) {
        $this->setLayout('layout_popup');
        $this->result = false;
        $this->serverdown = true;
        $this->resp = true;
        $this->bankId = $request->getParameter('bank');
        if (!$request->hasParameter('result'))
            $this->form = new customCommandForm();
        else {
            $command = $request->getPostParameter('command');
            $bankDetails = Doctrine::getTable('Bank')->find($request->getParameter('bank'));
            $bi_protocolVersion = $bankDetails->getBiProtocolVersion();
            if (strpos($command, ' ')) {
                $params = explode(' ', $command, 2);
                $command = $params['0'];
                $parameters = $params['1'];
            } else {
                $command = $request->getPostParameter('command');
                $parameters = '';
            }
            $className = "BankIntegration" . $bi_protocolVersion . "Manager";
            $manager = new $className();
            $addcmdrequestResult = $manager->addCommandRequest($request->getParameter('bank'), $command, $parameters);
            $bankIntegraionObject = new bankIntegration();
            $bankIntegraionObject->QueueProcessing($manager);
            $count = sfConfig::get('app_karaf_response_attempts');

            $bankIntObj = new bankIntegrationHelper();
            $response = $bankIntObj->checkFirstResponse($addcmdrequestResult['id'], $count);

//            $response = Doctrine::getTable('MwResponse')->findByRequestId($addcmdrequestResult['id']);
            if ($response && $response->count()) {
                $this->result = true;
                $response = $response->getMessageText();
                $decodedResponse = (array) json_decode($response);
                if ($decodedResponse['stdout-response'] == '') {
                    $this->resp = true;
                    $this->decodedResponse = (array) json_decode($response);
                } else {
                    $this->resp = false;
                    $this->decodedResponse = explode("\n", $decodedResponse['stdout-response']);
                }
            } else {
                $this->form = new customCommandForm();
                $this->serverdown = false;
                $this->result = false;
            }
        }
    }

    private function bundleUpdate($id, $loc, $bank_id, $bi_version) {
        $className = "BankIntegration" . $bi_version . "Manager";
        $manager = new $className();
        $command = "osgi:update";
        $param = $id . ' ' . $loc;
        $addcmdrequestResult = $manager->addCommandRequest($bank_id, $command, $param);
        $bankIntegraionObject = new bankIntegration();
        $bankIntegraionObject->QueueProcessing($manager);
        return $manager->getRequestObject()->getId();
    }

     public function executeSetSessionVariable(sfWebRequest $request)
     {
        if($request->hasParameter('label') ){
            $this->getUser()->setAttribute('currentLinkLoc', $request->getParameter('label'));
        }
        $this->currentLinkLoc= $this->getUser()->getAttribute('currentLinkLoc', '0');
        return $this->renderText($this->currentLinkLoc);
     }

}
