<?php

/**
 * epActionAuditEventReports actions.
 *
 * @package    mysfp
 * @subpackage epActionAuditEventReports
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class epActionAuditEventReportsActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->stDate = '';
        $this->enDate = '';
        $this->bank = '';
        $classObj = new auditTrailManager();
        $guardObj = $this->getUser()->getGuardUser();
        $user_group_arr = $guardObj->getGroupNames();
        $this->user_group = $user_group_arr['0'];
        if ($this->user_group == sfConfig::get('app_pfm_role_bank_branch_user')) {
            $this->bank_name = $guardObj->getBankUser()->getFirst()->getBank()->getBankName();
            $this->bank_branch_name = $guardObj->getBankUser()->getFirst()->getBankBranch()->getName();
        }
        $bankList[''] = '-- All Banks --';
        $this->bankList = $classObj->getBankList($bankList);
        $defaultOption = array('' => '-- All Categories --');
        $this->auditObj = EpAuditEvent::getAllCategories($defaultOption);
        if (in_array($this->user_group, $this->roles_array())) {
            $countryHeadSearch = true;
        } else {
            $countryHeadSearch = false;
        }
        $this->custom_form_parent = new CustomEpActionAuditEventReportsForm(null, array('audit_obj' => $this->auditObj, 'country_head' => $countryHeadSearch), null);

        $this->custom_form_child = new CustomChildEpActionAuditForm(null, array('bankList' => $this->bankList), null);
    }

    private function roles_array() {
        return array(sfConfig::get('app_pfm_role_admin') => sfConfig::get('app_pfm_role_admin'), sfConfig::get('app_pfm_role_bank_admin') => sfConfig::get('app_pfm_role_bank_admin'), sfConfig::get('app_pfm_role_report_bank_admin') => sfConfig::get('app_pfm_role_report_bank_admin'), sfConfig::get('app_pfm_role_report_admin') => sfConfig::get('app_pfm_role_report_admin'));
    }

    public function executeGetBankBranches(sfWebRequest $request) {
        //create an object of the class
        $classObj = new auditTrailManager();
        //get the bank id
        $bankId = $request->getParameter('bank');

        // WP029 Modify audit trail for bank country head

        $countryId = $request->getParameter('country');
        $group_name = $this->getUser()->getGroupName();
        if ($group_name == sfConfig::get('app_pfm_role_bank_country_head')) {
            $countryId = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        }

        //based on the id get the branch list
        $str = '<option value="">-- All Branches --</option>';
        if ($bankId != "") {
//        if ($countryId!="") {
            $this->branchList = $classObj->getBranchList($bankId, $countryId);
            $q = $this->branchList;
            foreach ($q as $value) {
                $str .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
            }
//      }
        }

        return $this->renderText($str);
    }

    public function executeGetCountryHead(sfWebRequest $request) {

        //get the bank id
        $bankId = $request->getParameter('bank');
        $countryId = $request->getParameter('country');
        //based on the id get the branch list
        $str = '<option value="">--Please select Username --</option>';

        $headList = Doctrine::getTable('sfGuardUser')->getAllUsers(12, $bankId, "", "", $countryId);
        $headArr = $headList->execute(array(), Doctrine::HYDRATE_ARRAY);

        $q = $headArr;
        foreach ($q as $value) {
            $str .= '<option value="' . $value['id'] . '">' . $value['username'] . '</option>';
        }

        return $this->renderText($str);
    }

    public function executeGetBankCountry(sfWebRequest $request) {
        $group_name = $this->getUser()->getGroupName();
        //create an object of the class
        $classObj = new auditTrailManager();
        //get the bank id
        $bankId = $request->getParameter('bank');
        //based on the id get the country list

        $countryId = "";
        if ($group_name == sfConfig::get('app_pfm_role_bank_country_head')) {
            $countryId = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        }
        $str = '<option value="">-- All Country --</option>';
        if ($bankId != "") {
            $countryList = $classObj->getCountryList($bankId, $countryId);
            if ($countryList) {
                foreach ($countryList as $key => $val) {
                    if ($key)
                        $str .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }
        }
        return $this->renderText($str);
    }

    public function executeGetSubCategory(sfWebRequest $request) {
        //get the category name
        $catName = $request->getParameter('category');
        $subCatList = EpAuditEvent::getAllSubcategory($catName);

        $str = '<option value="">-- All SubCategories --</option>';
        // $subCatList = $this->subCatList;
        //print_r($q); die();

        foreach ($subCatList as $value) {
            $str .= '<option value="' . $value . '">' . $value . '</option>';
        }
        return $this->renderText($str);
    }

    public function executeDisplayReport(sfWebRequest $request) {
        $params = $request->getParameterHolder()->getAll();
        $this->startDate = $params['startDate_date'];
        $this->endDate = $params['endDate_date'];
        $this->userId = '';
        $this->username = '';
        $this->ivalue = '';
        $this->bank = '';
        $this->branch = '';
        $this->category = '';
        $this->subcategory = '';
        $this->selectBy = '';
        //WP049 CR074
        $allowedGroupIds = Array();

        if (isset($params['bank']))
            $this->bank = $params['bank'];
        if (isset($params['branch']))
            $this->branch = $params['branch'];
        if (isset($params['category']))
            $this->category = $params['category'];
        if (isset($params['subCategory']))
            $this->subcategory = $params['subCategory'];

        if (isset($params['userId']))
            $this->userId = $params['userId'];
        if (isset($params['username']))
            $this->username = $params['username'];

        if (isset($params['ivalue']))
            $this->ivalue = $params['ivalue'];


        //either username or bank/bank branch can be choosen, so

        if (isset($params['selectBy']) && $params['selectBy'] == 'bank_country_head' && ($this->category == 'security' || $this->category=='')) {
            $this->bank = $params['bankId'];
            $this->branch = '';
            $this->country = $params['countryId'];
            $this->userId = $params['bankCountryName'];
            $params['bank'] = $params['bankId'];
            $params['userId'] = $params['bankCountryName'];
        }
        if (isset($params['selectBy']))
            $this->selectBy = $params['selectBy'];



        //get the user credendtials
        $user = $this->getUser();
        $groupDetails = $user->getGroups()->toArray();

        $userDetails['id'] = $user->getGuardUser()->getId();
        $userDetails['username'] = $user->getGuardUser()->getUsername();
        $userDetails['groupId'] = $groupDetails[0]['id'];
        $userDetails['groupName'] = $groupDetails[0]['name'];
        if ($userDetails['groupName'] == 'bank_user') {
            $this->userId = $userDetails['id'];
            $params['userId'] = $userDetails['id'];
        }
        if (($userDetails['groupName'] == 'bank_admin')
                || ($userDetails['groupName'] == 'bank_e_auditor')
                || ($userDetails['groupName'] == 'bank_country_head')) {

            $getBankArray = Doctrine::getTable('BankUser')->getBankIdName($userDetails['id']);
            $this->ivalue = $getBankArray[0]['id'];
            $bankName = $getBankArray[0]['bank_name'];
            $params['ivalue'] = $getBankArray[0]['id'];
        }

        if (isset($params['selectBy']) && $params['selectBy'] == 'username') {
            if ($userDetails['groupName'] == 'bank_country_head') {
                $getUser_details = Doctrine::getTable('sfGuardUser')->fetchUserDetails($params['username']);
                $countryId = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();

                if ((isset($getUser_details['0']['bank_id']) && $getUser_details['0']['bank_id'] == $this->ivalue)
                        && (isset($getUser_details['0']['country_id']) && $getUser_details['0']['country_id'] == $countryId)) {
                    $this->username = $params['username'];
                } else {
                    // forcefully wrong parameter to get null resultset
                    $this->userId = $params['username'];
                    $params['username'] = '';
                }
            }
            else
                $this->username = $params['username'];
        }

        $this->postDataArray = array();
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        } else if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $params;
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        } else {
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }

        if (!isset($this->endDate) || $this->endDate == '') {
            $this->endDate = $this->startDate;
        }

        //convert the date into the proper format
        $this->startDate = date('Y-m-d', strtotime("$this->startDate"));
        $this->startDate = $this->startDate . " 00:00:00";

        $this->endDate = date('Y-m-d', strtotime("$this->endDate"));
        $this->endDate = $this->endDate . " 23:59:59";

        $classObj = new auditTrailManager();
        //WP049 CR074 allowed groupids
        if ($classObj->getAllowedGroupIds($userDetails['groupName']))
            $allowedGroupIds = $classObj->getAllowedGroupIds($userDetails['groupName']);
        // $dataResult = $classObj->getDataForReport($request->getParameterHolder()->getAll());
        $dataResult = $classObj->getDataForReport($this->startDate, $this->endDate, $userDetails, $this->bank, $this->branch, $this->category, $this->subcategory, $this->username, $this->selectBy, $this->userId, $this->ivalue, $allowedGroupIds);
//    print "<pre>";
//    print_r($dataResult);exit;
        //this below mentioned code is for executing the sql recd & get the data in paging order
        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('EpActionAuditEvent', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($dataResult);
        $this->pager->setPage($this->getRequestParameter('page', $this->page));
        $this->pager->init();
    }

    public function executeShow(sfWebRequest $request) {
        $this->ep_action_audit_event = Doctrine::getTable('EpActionAuditEvent')->find($request->getParameter('id'));
        $this->forward404Unless($this->ep_action_audit_event);
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = new EpActionAuditEventForm();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));

        $this->form = new EpActionAuditEventForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($ep_action_audit_event = Doctrine::getTable('EpActionAuditEvent')->find($request->getParameter('id')), sprintf('Object ep_action_audit_event does not exist (%s).', $request->getParameter('id')));
        $this->form = new EpActionAuditEventForm($ep_action_audit_event);
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($ep_action_audit_event = Doctrine::getTable('EpActionAuditEvent')->find($request->getParameter('id')), sprintf('Object ep_action_audit_event does not exist (%s).', $request->getParameter('id')));
        $this->form = new EpActionAuditEventForm($ep_action_audit_event);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($ep_action_audit_event = Doctrine::getTable('EpActionAuditEvent')->find($request->getParameter('id')), sprintf('Object ep_action_audit_event does not exist (%s).', $request->getParameter('id')));
        $ep_action_audit_event->delete();

        $this->redirect('epActionAuditEventReports/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $ep_action_audit_event = $form->save();

            $this->redirect('epActionAuditEventReports/edit?id=' . $ep_action_audit_event->getId());
        }
    }

    public function executeDownloadCSV(sfWebRequest $request) {
        //print_r($request->getPostParameters()); die();
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
//print "<pre>";print_r($_SESSION);

        $this->postDataArray = $_SESSION['pfm']['reportData'];

        $headerNames = array(0 => array('S.No.', 'Username', 'Ip Address', 'Category', 'Subcategory', 'Description', 'Created At', 'BankName', 'BranchName'));
        $headers = array("username", "ip_address", "category", "subcategory", "description", "created_at", "bankName", "branchName");
        //WP049 CR074
        $allowedGroupIds = Array();
        // $classObj = new auditTrailManager();
//
//    print "<pre>";
//    print_r($this->postDataArray);exit;

        $user = $this->getUser();
        $groupDetails = $user->getGroups()->toArray();

        $userDetails['id'] = $user->getGuardUser()->getId();
        $userDetails['username'] = $user->getGuardUser()->getUsername();
        $userDetails['groupId'] = $groupDetails[0]['id'];
        $userDetails['groupName'] = $groupDetails[0]['name'];
        $classObj = new auditTrailManager();
        if ($classObj->getAllowedGroupIds($userDetails['groupName']))
            $allowedGroupIds = $classObj->getAllowedGroupIds($userDetails['groupName']);

        $records = Doctrine::getTable('EpActionAuditEvent')->getAuditDataForCSVDownload($this->postDataArray, $userDetails, $allowedGroupIds);
        $i = 0;
        // while($row = mysql_fetch_array($records)) {
        foreach ($records as $row) {
            $counterVal = count($headers);
            for ($index = 0; $index < $counterVal; $index++) {
                //  print "<br>".$headers[$index];
                if (($headers[$index] == "bankName") || ($headers[$index] == "branchName")) {
                    $bank_added = 0;
                    $branch_added = 0;
                    foreach ($row['EpActionAuditEventAttributes'] as $k => $v) {
                        if ($v['name'] == "BankName") {
                            $bank_added = 1;
                            $recordsDetails[$i]['bankName'] = $v['svalue'];
                        }
                        if ($v['name'] == "BranchName") {
                            $branch_added = 1;
                            $recordsDetails[$i]['branchName'] = $v['svalue'];
                        }
                    }
                    if (!$bank_added) {
                        $recordsDetails[$i]['bankName'] = 'N/A';
                    }
                    if (!$branch_added) {
                        $recordsDetails[$i]['branchName'] = 'N/A';
                    }
                } else {
                    $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
                }
            }
           $recordsDetails[$i]['username'] = $recordsDetails[$i]['username']?$recordsDetails[$i]['username']:'N/A';
           $i++;
        }

        /* print "<pre>";
          print_r($recordsDetails);
          exit; */


        if (count($recordsDetails) > 0) {
            $arrList = $this->exportUserList($headerNames, $headers, $recordsDetails);
            $file_array = csvSave::saveExportListFile($arrList, "/report/auditTrail/", "auditTrail");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "audit_trail";
            $this->setTemplate('csv');
        } else {
            $this->redirect_url('index.php/report/rechargeReport');
        }
        $this->setLayout(false);
    }

    public function exportUserList($arrHeader, $headers, $arrList) {
        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $counterVal = count($arrList);
        $counterValHeaders = count($headers);
        for ($k = 0; $k < $counterVal; $k++) {
            $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeaders; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }

}
