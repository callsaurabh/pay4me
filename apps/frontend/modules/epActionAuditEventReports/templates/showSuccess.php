<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $ep_action_audit_event->getid() ?></td>
    </tr>
    <tr>
      <th>User:</th>
      <td><?php echo $ep_action_audit_event->getuser_id() ?></td>
    </tr>
    <tr>
      <th>Username:</th>
      <td><?php echo $ep_action_audit_event->getusername() ?></td>
    </tr>
    <tr>
      <th>Ip address:</th>
      <td><?php echo $ep_action_audit_event->getip_address() ?></td>
    </tr>
    <tr>
      <th>Category:</th>
      <td><?php echo $ep_action_audit_event->getcategory() ?></td>
    </tr>
    <tr>
      <th>Subcategory:</th>
      <td><?php echo $ep_action_audit_event->getsubcategory() ?></td>
    </tr>
    <tr>
      <th>Description:</th>
      <td><?php echo $ep_action_audit_event->getdescription() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $ep_action_audit_event->getcreated_at() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('epActionAuditEventReports/edit?id='.$ep_action_audit_event->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('epActionAuditEventReports/index') ?>">List</a>
