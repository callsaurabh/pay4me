<table><tr><td>&nbsp;</td></tr></table>
<?php use_helper('Pagination'); ?>
<?php echo ePortal_listinghead('Audit Trail'); ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
        <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>

  <?php
   if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft" ><div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('downloadCSV', 'csvDiv', 'progBarDiv')"><?php echo __('Click here to download data in');?> <font color="red"><b><u>CSV</u></b></font> Format</div><div id ="progBarDiv"  style="display:none;"></div></span>
      </th>
    </tr>
  </table>
</div>
<br class="pixbr" />


   <?php } ?>



<div class="wrapTable" style="width:100%;overflow:auto;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <thead>
        <tr class="horizontal">
          <th>S.No.</th>
          <th><?php echo __('Username')?></th>
          <th><?php echo __('Ip address')?></th>
          <th><?php echo __('Category')?></th>
          <th><?php echo __('Subcategory')?></th>
          <th>Description</th>
          <th><?php echo __('Created at')?></th>
          <th>BankName</th>
          <th>BranchName</th>
        </tr>
      </thead>
      <tbody>
        <?php
         if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
            foreach ($pager->getResults() as $result):
            $i++;
        ?>
        <tr class="alternateBgColour">
          <td><?php echo $i ?></td>
          <td><?php echo $result->getUsername()?$result->getUsername():'N/A' ?></td>
          <td><?php echo $result->getIpAddress() ?></td>
          <td><?php echo $result->getCategory() ?></td>
          <td><?php echo $result->getSubcategory() ?></td>
          <td><?php echo $result->getDescription() ?></td>
          <td><?php echo $result->getCreatedAt() ?></td>

          <?php  $z=2; foreach($result->getEpActionAuditEventAttributes() as $k){
            if($k->getName() == "BankName"){
              echo "<td>".$k->getSvalue()."</td>";
              $z--;
            }
            if($k->getName() == "BranchName"){
                if($k->getSvalue()){
                    echo "<td>".$k->getSvalue()."</td>";
                }else{
                    echo "<td>N/A</td>";
                }
              $z--;
            }

           }
           for($j=0;$j<$z;$j++)
               echo "<td>N/A</td>";
           ?>

        </tr>
        <?php endforeach; ?>
        <?php
          $url="";
          if($startDate != ""){ $url .= "startDate=".$startDate."&"; }
          if($endDate != ""){ $url .= "endDate=".$endDate."&"; }
          if($bank != ""){ $url .= "bank=".$bank."&"; }
          if($branch != ""){ $url .= "branch=".$branch."&"; }
          if($category != ""){ $url .= "category=".$category."&"; }
          if($subcategory != ""){ $url .= "subCategory=".$subcategory."&"; }
          if($username != ""){ $url .= "username=".$username."&"; }
          if($userId != ""){ $url .= "userId=".$userId."&"; }
          if($ivalue != ""){ $url .= "ivalue=".$ivalue; }
        ?>
       <!-- <tr>
          <td colspan="10">

          </td>
        </tr>-->
        <?php }else{ ?><tr><td  align='center' class='error' colspan="10"><?php echo __('No Result found')?></td></tr><?php } ?>
      </tbody>
      <tfoot><tr><td colspan="10">
      <?php  if(($pager->getNbResults())>0) { ?>
      <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?'.$url, 'search_results') ?></div>
      <?php }?>

</td></tr></tfoot>
    </table>
  </div>
  

<script>
function progBarDiv(url, referenceDivId, targetDivId){
    var url = "<?php echo url_for("epActionAuditEventReports/");?>"+url;
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}

</script>
