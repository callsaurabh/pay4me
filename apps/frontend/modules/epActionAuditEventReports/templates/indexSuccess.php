<?php //use_helper('Form') ?>

<script>
    $(document).ready(function()
    {
        showHideDiv();
        $("input[name='selectBy']").change(function(){
            clearvalue();
            showHideDiv();
            
            
        });

        $("#bankId").change(function()
        {
            fillCountries();
            fillCountryHead();
        });
        $("#countryId").change(function()
        {
            fillCountryHead();
        });
        
        $("#bank").select(function (){
            $("#bank").change()
        });

        $("#bank").change(function()
        {

          
            var bank =  $('#bank').val();          
            var url = "<?php echo url_for("epActionAuditEventReports/getBankBranches");?>";

            $('#loader').show();
            $("#branch").load(url, { bank: bank,byPass:1},function (data){
                if(data=='logout'){
                    location.reload();
                }
                $('#loader').hide();
            });
        });

        $("#category").select(function (){
            $("#category").change()
        });
        function fillCountries(){
            var bank    =  $('#bankId').val();
            //        var country =  $('#countryId').val();
            var url1 = "<?php echo url_for("epActionAuditEventReports/getBankCountry"); ?>";

            $("#countryId").load(url1, { bank: bank},function (data){
                if(data=='logout'){
                    location.reload();
                }

            });
        }
        $("#category").change(function()
        {

            $("#divsup").html('');
            var category = $(this).val();
            if(category == 'transaction'){
                $("#bank").attr("disabled", true);
                $("#branch").attr("disabled", true);
                $("#bankId").attr("disabled", true);
                $("#countryId").attr("disabled", true);
                $("#bankCountryName").attr("disabled", true);
               

            }
            else{
                if($('input[name=selectBy]:checked').val()=='bank_country_head'){
                    $("#divsup").html('*');
                }
                $("#bank").attr("disabled", false);
                $("#branch").attr("disabled", false);
                $("#bankId").attr("disabled", false);
                $("#countryId").attr("disabled", false);
                $("#bankCountryName").attr("disabled", false);
            }

            //bank = bank.toString();
            var url = "<?php echo url_for("epActionAuditEventReports/getSubCategory");?>";

            $('#loader').show();
            $("#subCategory").load(url, {
                category: category,byPass:1
            },function (data){
                if(data=='logout'){
                    location.reload();
                }
                $('#loader').hide();
            });
        });


    });
    
    function clearvalue(){
        $('#username').val('');
        $('#bank :selected').attr('selected', '');
        $('#bankId :selected').attr('selected', '');
        $('#branch :selected').attr('selected', '');
        $('#countryId :selected').attr('selected', '');
        $('#bankCountryName :selected').attr('selected', '');
    }

    function showHideDiv(){

        var checkedVal = $('input[name=selectBy]:checked').val()
        $("#divsup").html('');
        $("#err_bankCountryName").html("");
        $('#search_results').html("");

        if(checkedVal == 'bank_branch'){
             $("#user_name").hide();
             $("#check1").show();
             $("#check2").show();
             $("#bank_country_head").hide();
          
        }

        if(checkedVal == 'username'){
             $("#user_name").show();
             $("#check1").hide();
             $("#check2").hide();
             $("#bank_country_head").hide();
        }
        if(checkedVal == 'bank_country_head'){
            $("#bank_country_head").show();
            $("#user_name").hide();
            $("#check1").hide();
            $("#check2").hide();
            fillCountryHead();

            if($("#category").val()=='security' || $("#category").val()==''){
               $("#divsup").html('*');
            }
            
        }
        
    }

    function fillCountryHead(){
        var bank    =  $('#bankId').val();
        var country =  $('#countryId').val();
        var url1 = "<?php echo url_for("epActionAuditEventReports/getCountryHead");?>";

        $("#bankCountryName").load(url1, { bank: bank,country:country,byPass:1},function (data){
            if(data=='logout'){
               location.reload();
           }

        });
    }
</script>

<script>
    function validateRequestForm()
    {
        $('#search_results').html("");
        var from_date = $('#startDate_date').val();
        var to_date = $('#endDate_date').val();
        var checkedVal = $('input[name=selectBy]:checked').val()
        var err=0;

        if(from_date == ""){
            err = err+1;
            $("#err_frm_date").html("<?php echo __("Please select Start Date.")?>");
            return false;
        }else{
            $("#err_frm_date").html("");
        }

        if(to_date == ""){
            err = err+1;
            $("#err_to_date").html("<?php echo __("Please  select End Date.")?>");
            return false;
        }else{
            $("#err_to_date").html("");
        }

       if(checkedVal == 'bank_country_head' && ($("#category").val()=='security' || $("#category").val()=='')){
            var bankCountryName = $('#bankCountryName').val();
            if(bankCountryName == "")
            {
                err = err+1;
                $("#err_bankCountryName").html("<?php echo __("Please select Username.") ?>");
            }else{
                 $("#err_bankCountryName").html("");
            }
        }

        if((from_date != "") && (to_date != ""))
        {
            var from_date_sel = from_date.split("/");
            var from_date_selected = new Date(from_date_sel[2],from_date_sel[0]-1,from_date_sel[1] );

            var to_date_sel = to_date.split("/");
            var to_date_selected = new Date(to_date_sel[2],to_date_sel[0]-1,to_date_sel[1] );

            var milli_to_date = to_date_selected.getTime();
            var milli_from_date = from_date_selected.getTime();
            var diff = milli_to_date - milli_from_date;
            if(diff<0)
            {
                err = err+1;
                $("#err_to_date").html("<?php echo __("Start Date should be less than End Date")?>");
                return false;
            }
           
        }

         if(err == 0) {
           
            $("#search_results").html('<center><?php echo image_tag('../images/ajax-loader.gif'); ?></center>')
            var url = "<?php echo url_for("epActionAuditEventReports/displayReport");?>";
            $.post(url,$("#search").serialize(), function(data){
                if(data=='logout'){
                    location.reload();
                }
                else {

                    $("#search_results").html(data);}
            }
        );
        }
    }
</script>
<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<div class="wrapForm2">
    <?php echo ePortal_legend(__('Search Audit Trail')); ?>
    <?php echo form_tag('epActionAuditEventReports/displayReport', 'name=search id=search') ?>

   <?php include_partial('parentIndexForm', array('form' => $custom_form_parent));
   if ($user_group == sfConfig::get('app_pfm_role_bank_branch_user')) {
   ?>

        <dl style="display:none" id="check1">
            <div class="dsTitle4Fields"><?php echo __('Bank') . ':' ?></div>
            <div class="dsInfo4Fields"><?php echo $bank_name; ?></div>
        </dl>
        <dl style="display:none" id="check2">
            <div class="dsTitle4Fields"><?php echo __('Bank Branch:') ?></div>
            <div class="dsInfo4Fields"><?php echo $bank_branch_name; ?></div>
        </dl>
<?php 
}
include_partial('childPartial', array('form' => $custom_form_child)); ?>

   


    <dl><div class="divBlock"><center>
<?php 
    echo button_to(__('Search'), '', array('class' => 'formSubmit', 'onClick' => 'validateRequestForm()')); ?>

            </center></div></dl>
    </form>
</div>
<div id="search_results"></div>


<script language="Javascript">
    var bank= "<?php echo $bank; ?>";
    if(bank != ""){display_options();}

</script> 