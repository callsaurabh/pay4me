<?php //use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>


<div id="update_msg"></div>
<?php if($err){

      print  "<div id='flash_error' class='error_list'><span>".$err."</span></div>";

}
if(!$err){

  ?>
<div class='wrapForm2'>

     <?php echo ePortal_legend(__('Confirm Details'));?>
     <?php $currencyFlag = Settings::isMultiCurrencyOn();
           $checkBox = '';
           if($currencyFlag)
            $checkBox = '<input type="checkbox" name="total_amount" id="total_amount" >';
     ?>

<?php //echo form_tag($sf_context->getModuleName().'/loadWallet',' class="dlForm multiForm" id="pfm_confirmation_form" name="pfm_confirmation_form"');?>

<?php if(isset($previousRechargeDate)){echo formRowComplete(__('Last Recharge Time'),$previousRechargeDate,'','name','','name_row'); ?>
<?php echo formRowComplete(__('Last Recharge Amount'),format_amount($previousRechargeAmount,$currencyId,1),'','name','','name_row'); } ?>

<?php echo formRowComplete(__('Name'),$userDetailObj->getName()." ".$userDetailObj->getLastName(),'','name','','name_row'); ?>
<?php echo formRowComplete(__('Account Number'),$accountObj->getAccountNumber(),'','acct_no','','acct_no_row'); ?>
<?php echo formRowComplete(__('Address'),$userDetailObj->getAddress(),'','address','','address_row'); ?>
<?php echo formRowComplete(__('Email'),$userDetailObj->getEmail(),'','email','','email_row'); ?>
<?php echo formRowComplete(__('Mobile No'),$userDetailObj->getMobileNo(),'','mobile_no','','mobile_no_row'); ?>
<?php echo formRowComplete($checkBox.' '.__('Total Amount'),format_amount($amount,$currencyId,0),'','total_amount','','total_amount_row'); ?>
<?php if($currencyFlag){?>
<div style="font-size: 11px; color: #666;  text-decoration: none;   float: left; width: 540px;padding-top: 10px; padding-bottom: 6px;  background-color: #E5E5E5;  padding-left: 10px; padding-right: 10px;">
  <label for="confirm"><input type="checkbox" name="confirm" id="confirm"> <?php echo __("Please ensure that amount is being collected in")?> <?php echo ucfirst($currency).' ('.$currencyCode.')'?></label>
 </div>
<?php }?>
<div class="divBlock">
            <center>
    <?php echo button_to(__('Pay'),'',array('class'=>'formSubmit', 'onClick'=>'validateSearchSucessForm()')); ?>&nbsp;<?php
    $cancel_url=url_for('recharge_ewallet/search');
    switch($pay_mode){
        case 'cash':$cancel_url=url_for('recharge_ewallet/search');
            break;
        case 'check':$cancel_url=url_for('recharge_ewallet/searchCheck');
            break;
        case 'bank_draft':$cancel_url=url_for('recharge_ewallet/searchDraft');
            break;
    }

    echo button_to(__('Cancel'),'',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for($cancel_url).'\''));?>
  </center>
</div>
    </div>
<script>
    function validateSearchSucessForm(){
        <?php if($currencyFlag){?>
        if((document.getElementById('total_amount').checked==true)&&(document.getElementById('confirm').checked==true)){
          validateSearchForm("make_payment");
        }else{
            alert("<?php echo __('Please select all checkbox')?>");
            return false;
        }
        <?php }else{?>
            validateSearchForm("make_payment");
         <?php }?>
    }

</script>


<?php }?>
