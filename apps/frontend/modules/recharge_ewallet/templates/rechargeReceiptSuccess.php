<?php //use_helper('Form');
$obj = new pfmHelper();
$bank = $obj->getUserAssociatedBank(); // Bank Array()
$bankBranch  = $obj->getUserAssociatedBankBranch(); // Branch_Name
?>

<div id="printSlip">
  <div id="paymentModeReceipt" class="onlyPrint">
    <div class="logo"><?php echo image_tag('/images/'.$bank['acronym'].'.jpg',array('alt'=>"{$bank['bank_name']}, {$bankBranch}"));?></div>
    <div class="label"><?php echo $bank['bank_name'] ; ?><br><?php echo $bankBranch ; ?></div>
  </div>

<?php $pageHead="eWallet Cash Recharge Receipt";
if(isset($PM_check) && $PM_check){
    $pageHead="eWallet ".pfmHelper::getChequeDisplayName()." Recharge Receipt";
}
else if(isset($PM_draft) && $PM_draft){
    $pageHead="eWallet Bank Draft Recharge Receipt";
}

?>
<?php echo ePortal_pagehead(__($pageHead),array('id'=>'dynamicHeading')); ?>
<div class="wrapForm2">
<?php echo form_tag('',array('name'=>'pfm_receipt_form','id'=>'pfm_receipt_form')) ?>

<div id="update_msg"></div>
<?php
if($err){
   print "<div id='flash_error' class='error_list'><span>".$err."</span></div>";
}
if(!$err){
   print  "<div id='flash_notice' class='error_list'><span>".$msg."</span></div>";
?>
  
<?php echo ePortal_legend(__('eWallet Customer Details'));?>
<?php echo formRowComplete(__('Validation Number'),$validation_number);?>
<?php echo formRowComplete(__('Account Name'),$accountObj->getAccountName(),'','name','','name_row'); ?>
<?php echo formRowComplete(__('Account Number'),$accountObj->getAccountNumber(),'','acct_no','','acct_no_row'); ?>
<?php echo formRowComplete(__('Amount'),format_amount($amount,$currencyId),'','acct_no','','amount_row'); ?>
<?php echo formRowComplete(__('Name'),$userDetailObj->getName()." ".$userDetailObj->getLastName(),'','name','','name_row'); ?>
<?php echo formRowComplete(__('Address'),$userDetailObj->getAddress(),'','address','','address_row'); ?>
<?php echo formRowComplete(__('Email'),$userDetailObj->getEmail(),'','email','','email_row'); ?>
<?php echo formRowComplete(__('Mobile No'),$mobileN0 = ($userDetailObj->getMobileNo())?$userDetailObj->getMobileNo():'#','','mobile_no','','mobile_no_row'); ?>
<?php echo formRowComplete(__('Date'),$transactionDetailsObj->getTransactionDate(),'','date','','date_row'); //FS#31853 ?>
<?php if((isset($PM_check) && $PM_check) && $account_number!='' && $check_number!=''){ ?>
<?php echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details'));?>
<?php echo formRowComplete(__('Bank Name'),$bank['bank_name'],'','bank_name','','bank_name_row'); ?>
<?php echo formRowComplete(__('Bank Sort Code'),$sort_code,'','sort_code','','sort_code_row'); ?>
<?php echo formRowComplete(__('Cheque Number'),$check_number,'','check_no','','check_no_row'); ?>
<?php echo formRowComplete(__('Payee Account Number'),$account_number,'','account_no','','account_no_row'); ?>
<?php } ?>
<?php if((isset($PM_draft) && $PM_draft)){ ?>
<?php echo ePortal_legend(__(pfmHelper::getDraftDisplayName() . ' Details'));?>
<?php echo formRowComplete(__('Bank Name'),$bank['bank_name'],'','bank_name','','bank_name_row'); ?>
<?php echo formRowComplete(__('Bank Sort Code'),$sort_code,'','sort_code','','sort_code_row'); ?>
<?php echo formRowComplete(__('Bank Draft Number'),$draft_number,'','draft_no','','draft_no_row'); ?>
<?php } ?>
 <div class="divBlock">
   <center>
    <input type="button" style = "cursor:pointer;" value="<?php echo __("Print User Receipt")?>" class="formSubmit" onclick="printSlip('user')">
    <input type="button" style = "cursor:pointer;" value="<?php echo __("Print Bank Receipt")?>" class="formSubmit" onclick="printSlip('bank')">  
   </center>
  </div>

</form>
</div>
<?php }?>
</div>

<script>
  function printSlip(mode){

    defHeading = $('#dynamicHeading').html();
    heading = "";
    if(mode =='user'){
      heading +="User Receipt";
    }else if (mode =='bank'){
      heading +="Bank Receipt";
    }
    $('#dynamicHeading').html(heading);
    html = '';
    html += $('#printSlip').html();
   // alert(html);
    printMe(html,true);
    $('#dynamicHeading').html(defHeading);


  }
</script>
