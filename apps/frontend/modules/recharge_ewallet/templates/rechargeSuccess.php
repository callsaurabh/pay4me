<?php //use_helper('Form'); ?>
<?php use_helper('ePortal'); ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form'));?>
<div class="wrapForm2">
<?php echo form_tag($this->getModuleName().'/'.$this->getActionName().'?recharge_gateway='.$recharge_gateway,array('name'=>'pfm_ewallet_recharge_form', 'id'=>'pfm_ewallet_recharge_form')) ?>
    <?php
    if(strtolower($recharge_gateway) == 'credit_card')
      echo ePortal_legend(__('Recharge eWallet Using '.ucwords('Visa Card')));
    elseif(strtolower($recharge_gateway) == 'etranzact')
        echo ePortal_legend(__('Recharge eWallet Using '.'eTranzact'));
    else
      echo ePortal_legend(__('Recharge eWallet Using '.ucwords($recharge_gateway)));  ?>
   <?php echo formRowComplete($form['amount']->renderLabel().'<font color=red><sup>*</sup></font>',image_tag('../img/naira.gif').$form['amount']->render(),__('Eg:').' 10000','amount','err_amount','amount_row',$form['amount']->renderError().$form->renderGlobalErrors()); ?>
    <?php  if ($form->isCSRFProtected()) : ?>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php endif; ?>
   <input type="hidden" name="recharge_gateway" id="recharge_gateway" value="<?php echo $recharge_gateway ?>">

    <div class="divBlock">
      <center id="multiFormNav">
      <input type="submit" class="formSubmit" value="<?php echo __('Charge eWallet')?>">
      </center>
    </div>
</form>
</div>
</div>


