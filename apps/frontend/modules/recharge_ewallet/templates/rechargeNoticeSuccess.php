<?php //use_helper('Form');
$obj = new pfmHelper();
$bank = $obj->getUserAssociatedBank(); // Bank Array()
$bankBranch  = $obj->getUserAssociatedBankBranch(); // Branch_Name

?>

<div id="printSlip">
  <div id="paymentModeReceipt" class="onlyPrint">
      <?php if(!$isSuperadmin){ ?>
    <div class="logo"><?php echo image_tag('/images/'.$bank['acronym'].'.jpg',array('alt'=>"{$bank['bank_name']}, {$bankBranch}"));?></div>
    <div class="label"><?php echo $bank['bank_name'] ; ?><br><?php echo $bankBranch ; ?></div>
    <?php }else { ?>
    <div class="logo"><?php echo image_tag('/images/'.$bankAcr.'.jpg',array('alt'=>"{$bankName}, {$branchName}"));?></div>
    <div class="label"><?php echo $bankName ; ?><br><?php echo $branchName ; ?></div>
    <?php } ?>
  </div>

        <?php
    if ($pay_mode_id == '') {
        $pageHead = "eWallet Cash Recharge Receipt";
    } else {
        $paymentModeName = $obj->getPMONameByPMId($pay_mode_id);
        $pageHead = "eWallet " . $paymentModeName . " Recharge Receipt";
    }
    ?>   
<?php echo ePortal_pagehead(__($pageHead),array('id'=>'dynamicHeading')); ?>
<div class="wrapForm2 wrapdiv">
<?php echo form_tag('',array('name'=>'pfm_receipt_form','id'=>'pfm_receipt_form')) ?>

<div id="update_msg"></div>
<?php
if($err){
   print "<div id='flash_error' class='error_list'><span>".$err."</span></div>";
}
if(!$err ){
    if(!$isSuperadmin){
   print  "<div id='flashAlert' class='error_list'><span><strong>You have recharged this eWallet at ".$transactionDetailsObj->getTransactionDate()." by ".format_amount($amount,$currencyId,1)."<br>You can recharge again this account after ".$timeForRecharge." minutes. <br>The account may be recharged immediately by a different teller.</strong><br>You can view/print the collection details below<br /></strong>Bank Name - ".$bank['bank_name'].",<br />Branch Name - ".$bankBranch."</span></div>";
    }
   ?>

<?php echo ePortal_legend('eWallet Customer Details');?>
<?php echo formRowComplete('Validation Number',$validation_number);?>
<?php echo formRowComplete('Account Name',$accountObj->getAccountName(),'','name','','name_row'); ?>
<?php echo formRowComplete('Account No.',$accountObj->getAccountNumber(),'','acct_no','','acct_no_row'); ?>
<?php echo formRowComplete('Amount',format_amount($amount,$currencyId,1),'','acct_no','','amount_row'); ?>
<?php echo formRowComplete('Name',$userDetailObj->getName()." ".$userDetailObj->getLastName(),'','name','','name_row'); ?>
<?php echo formRowComplete('Address',$userDetailObj->getAddress(),'','address','','address_row'); ?>
<?php echo formRowComplete('Email',$userDetailObj->getEmail(),'','email','','email_row'); ?>
<?php echo formRowComplete('Mobile No',$mobileN0 = ($userDetailObj->getMobileNo())?$userDetailObj->getMobileNo():'#','','mobile_no','','mobile_no_row'); ?>
<?php echo formRowComplete('Date',$transactionDetailsObj->getTransactionDate(),'','date','','date_row'); ?>
<?php if($pay_mode_id && $pay_mode_id==$obj->getPMOIdByConfName('Cheque')){ ?>
<?php echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details'));?>
   <?php if($isSuperadmin){
 echo formRowComplete(__('Bank Name'),$bankName,'','bank_name','','bank_name_row'); ?>
<?php } else { ?>
<?php echo formRowComplete(__('Bank Name'),$bank['bank_name'],'','bank_name','','bank_name_row'); ?>
 
<?php } ?>
<?php echo formRowComplete(__('Bank Sort Code'),$check_details['sort_code'],'','sort_code','','sort_code_row'); ?>
<?php echo formRowComplete(__('Cheque Number'),$check_details['check_number'],'','check_no','','check_no_row'); ?>
<?php echo formRowComplete(__('Payee Account Number'),$check_details['account_number'],'','account_no','','account_no_row'); ?>
<?php } ?>
<?php if($pay_mode_id && $pay_mode_id==$obj->getPMOIdByConfName('bank_draft')){ ?>
<?php echo ePortal_legend(__(pfmHelper::getDraftDisplayName() . ' Details'));?>
 <?php if($isSuperadmin){
 echo formRowComplete(__('Bank Name'),$bankName,'','bank_name','','bank_name_row'); ?>
<?php } else { ?>
<?php echo formRowComplete(__('Bank Name'),$bank['bank_name'],'','bank_name','','bank_name_row'); ?>

<?php } ?>
<?php echo formRowComplete(__('Bank Sort Code'),$check_details['sort_code'],'','sort_code','','sort_code_row'); ?>
<?php echo formRowComplete(__('Bank Draft Number'),$check_details['check_number'],'','check_no','','check_no_row'); ?>
<?php } ?>



</form>
</div>

<?php }?>
</div>
 <div class="paging pagingFoot">
   <center>
    <input type="button" style = "cursor:pointer;" value="Print User Receipt" class="formSubmit" onclick="printSlip('user')">
    <input type="button" style = "cursor:pointer;" value="Print Bank Receipt" class="formSubmit" onclick="printSlip('bank')">
   </center>
  </div>
<script>
  function printSlip(mode){

    defHeading = $('#dynamicHeading').html();
    heading = "";
    if(mode =='user'){
      heading +="User Receipt";
    }else if (mode =='bank'){
      heading +="Bank Receipt";
    }
    $('#dynamicHeading').html(heading);
    html = '';
    html += $('#printSlip').html();
   // alert(html);
    printMe(html,true);
    $('#dynamicHeading').html(defHeading);


  }
</script>
