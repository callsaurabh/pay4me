<?php echo ePortal_pagehead(" "); ?>
<?php use_javascript('general.js'); ?>
<?php  //use_helper('Validation') ?>
<?php if($sf_user->hasFlash('error')):?>
<div id='flash_error' class='error_list'></div>
<?php endif; ?>
<div id="notice"><span class="cRed">DO NOT REFRESH WHILE TRANSACTION IN PROGRESS</span><br />
    <?php if(sfConfig::get('app_ewallet_recharge_interval')>0): ?>
    <div align="justify"><span class="cRed">NOTICE:</span> If you have recharged this account within last <b><?php echo sfConfig::get('app_ewallet_recharge_interval');?> minutes</b>. Please check the status of the previous recharge before
    attempting a fresh reload. Your previous attempt to recharge this account might have succeeded. The result of your previous recharge, if it is made within last
<b><?php echo sfConfig::get('app_ewallet_recharge_interval');?> minutes</b>, will be displayed on the next screen. It is your responsibility to collect the money for the collection you have posted on pay4me.</div>
    <?php endif; ?></div><br />
<?php if($chackBankUserFlag){?>

    <?php echo form_tag($sf_context->getModuleName().'/rechargeReceipt',array('name'=>'pfm_ewallet_search_form', 'id'=>'pfm_ewallet_search_form')) ?>
<div class="wrapForm2">

      <?php
      echo ePortal_legend(__('Recharge eWallet Using Cash'));?>
      <?php echo formRowComplete($form['ewallet_number']->renderLabel(),$form['ewallet_number']->render(),'','ewallet_number','err_ewallet_number','ewallet_number_row',$form['ewallet_number']->renderError()); ?>
      <?php echo formRowComplete($form['amount']->renderLabel(),$form['amount']->render(),__('Eg:').' 10000','amount','err_amount','amount_row',$form['amount']->renderError()); ?>
      <input type="hidden" name="pay_mode" id="pay_mode" value="cash"/>
      <?php echo $form['validation_number']->render();?>
    <div class="divBlock">
        <center id="multiFormNav">
            <?php echo button_to(__('Charge eWallet'),'',array('class'=>'formSubmit', 'id'=>'charge_wallet_btn', 'onClick'=>'validateSearchForm("confirm_details")')); ?>&nbsp;<?php  //echo button_to('Cancel','',array('onClick'=>'this.form.reset();'));?>
        </center>
    </div>




</div>
    </form>
<div id="chargeLoader" align="center"><?php echo image_tag('/img/ajax-loader.gif',array()); ?></div>
<?php } else {
echo __("You are not authorized to recharge eWallet");
}?>
<div id="confirm_details"></div>


<script language="Javascript">

    function validateSearchForm(action){
        $('#confirm_details').html("");
        var err  = 0;
        var wallet_no = $("#ewallet_number").val();
        var amt = $("#amount").val();
        if(wallet_no == ""){
            err = err+1;
            $('#err_ewallet_number').html("<?php echo __('Please enter Account number')?>");
        }
        else{
            $('#err_ewallet_number').html("");
        }
        if((wallet_no != "") && (isNaN(wallet_no))){
            err = err+1;
            $('#err_ewallet_number').html("<?php echo __('Please enter valid Account number')?>");
        }
        else if(wallet_no!=""){
             var result= acctNumberValidation();
            if(result!=''){
                err = err+1;
            }
        }
        if(amt == ""){
            err = err+1;
            $('#err_amount').html("<?php echo __('Please enter Amount')?>");
        }
        else{
            $('#err_amount').html("");
        }
        if((amt!="") && (!validateAmount(amt))){
            err = err+1;
            $('#err_amount').html("<?php echo __('Please enter valid amount')?>");
        }
        else if(amt!=""){
            $('#err_amount').html("");
        }

        //check if wallet number is in our db; through ajax
        if(err == 0){
            $('#charge_wallet_btn').hide();

            $('#chargeLoader').show(); 
            $.post('verifyWalletNumber',$('#pfm_ewallet_search_form').serialize(), function(data){
                $('#chargeLoader').hide();
                if(data!=""){$('#err_ewallet_number').html(data);

                    $('#charge_wallet_btn').show();
                }else{
                    if(action == 'confirm_details'){
                        $('#ewallet_number').attr('readonly', 'true');
                        $('#amount').attr('readonly', 'true');
                        $.post('confirmDetails',$('#pfm_ewallet_search_form').serialize(), function(data){
                            $('#confirm_details').html(data);
                        });
                    }
                    else if(action  == 'make_payment'){
                         $('#chargeLoader').show();
                        $.post('confirmDetails', {ewallet_number: wallet_no,amount: amt, pay_mode:'cash',make_payment:true}, function(data){
                            $('#chargeLoader').hide();
                            if(isNaN(data)){$('#flash_error').html(data)}else{$("#validation_number").val("");$("#validation_number").val(data);$('#pfm_ewallet_search_form').submit();}
                            // else if(data==2){$('#flash_error').html("Invalid Account Number")}
                            //  else if(data==3){$('#flash_error').html("Account configuration failed")}
                            // else{};

                        });
                }
                };
            });
        }
    }
    function acctNumberValidation(){
        var wallet_no = $("#ewallet_number").val();
        var url = '<?php echo url_for("recharge_ewallet/verifyWalletNumber"); ?>';
         var data=$.ajax({
            url: url,
            type:'post',
            data:{ewallet_number:wallet_no},
            async:false}).responseText;
            if(data=='logout'){
                location.reload();
            } else{
                if(data)
            {
                $('#err_ewallet_number').html(data);
                return data;
            }
            else
            {
                $('#err_ewallet_number').html("");
                return data;
            }
            }
    }
</script>
