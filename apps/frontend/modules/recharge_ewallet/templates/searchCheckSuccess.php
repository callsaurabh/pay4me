<?php echo ePortal_pagehead(" "); ?>
<?php  //use_helper('Validation') ?>
<?php if($sf_user->hasFlash('error')):?>
<div id='flash_error' class='error_list'></div>
<?php endif; ?>
<div id="notice"><span class="cRed">DO NOT REFRESH WHILE TRANSACTION IN PROGRESS</span><br />
    <?php if(sfConfig::get('app_ewallet_recharge_interval')>0): ?>
    <div align="justify"><span class="cRed">NOTICE:</span> If you have recharged this account within last <b><?php echo sfConfig::get('app_ewallet_recharge_interval');?> minutes</b>. Please check the status of the previous recharge before
        attempting a fresh reload. Your previous attempt to recharge this account might have succeeded. The result of your previous recharge, if it is made within last
    <b><?php echo sfConfig::get('app_ewallet_recharge_interval');?> minutes</b>, will be displayed on the next screen. It is your responsibility to collect the money for the collection you have posted on pay4me.</div>
<?php endif; ?></div><br />
<?php if($chackBankUserFlag){?>

    <?php echo form_tag($sf_context->getModuleName().'/rechargeCheckReceipt',array('name'=>'pfm_ewallet_search_form', 'id'=>'pfm_ewallet_search_form')) ?>
<div class="wrapForm2">

        <?php
        echo ePortal_legend(__('Recharge eWallet Using '.pfmHelper::getChequeDisplayName()));?>
        <?php echo formRowComplete($form['ewallet_number']->renderLabel(),$form['ewallet_number']->render(),'','ewallet_number','err_ewallet_number','ewallet_number_row',$form['ewallet_number']->renderError()); ?>
        <?php echo formRowComplete($form['amount']->renderLabel(),$form['amount']->render(),__('Eg:').' 10000','amount','err_amount_recharge','amount_row',$form['amount']->renderError()); ?>
        <?php echo formRowComplete($form['sort_code']->renderLabel(),$form['sort_code']->render(),'','sort_code','err_sort_code','sort_code_row',$form['sort_code']->renderError()); ?>
        <?php echo formRowComplete($form['check_number']->renderLabel(),$form['check_number']->render(),'','check_number','err_check_number','check_number_row',$form['ewallet_number']->renderError()); ?>
        <?php echo formRowComplete($form['account_number']->renderLabel(),$form['account_number']->render(),'','account_number','err_account_number','ewallet_number_row',$form['ewallet_number']->renderError()); ?>
        <?php echo formRowComplete('','','','disclaimer_cheque_details_recharge','','','')?>
    <input type="hidden" name="pay_mode" id="pay_mode" value="check"/>
    <?php echo $form['validation_number']->render();?>
    <div class="descriptionArea">Note: Please enter only first 3 digits of Sort Code.</div>
    <div>

        <div id="confirm_cheque_details" class="dsTitle2"><?php  echo $form['disclaimer_cheque_details_recharge']->render() ?> <?php echo $form['disclaimer_cheque_details_recharge']->renderLabel() ?></div>
        <div id="err_disclaimer_cheque_details_recharge" class="cRed dsInfo4Fields" style="display:none;"><?php  echo  $form['disclaimer_cheque_details_recharge']->renderError()   ?></div>
    </div>
    <div class="divBlock">
        <center id="multiFormNav">
            <?php echo button_to(__('Charge eWallet'),'',array('class'=>'formSubmit', 'id'=>'charge_wallet_btn', 'onClick'=>'validateSearchForm("confirm_details")')); ?>&nbsp;<?php  //echo button_to('Cancel','',array('onClick'=>'this.form.reset();'));?>
        </center>
    </div>




</div>
<div id="chargeLoader" align="center"><?php echo image_tag('/img/ajax-loader.gif',array()); ?></div>
<?php } else {
echo __("You are not authorized to recharge eWallet");
}?>
<div id="confirm_details"></div>


<script language="Javascript">

     function validateSearchForm(action){
                //        $('#confirm').hide();

                $('#confirm_details').html("");
                var err  = 0;
                var wallet_no = $("#ewallet_number").val();
                var amt = $("#amount").val();
                var check_no = $("#check_number").val();
                var account_no = $("#account_number").val();
                var sort_code = $("#sort_code").val();
                //ewallet

            if(wallet_no == ""){
                    err = err+1;
                    $('#err_ewallet_number').html("<?php echo __('Please enter Account Number') ?>");
            }
            else{
                $('#err_ewallet_number').html("");
            }
            if((wallet_no != "") && (isNaN(wallet_no))){
                err = err+1;
                $('#err_ewallet_number').html("<?php echo __('Please enter valid Account Number') ?>");
            }
            else if(wallet_no!=""){
                var result= acctNumberValidation();
                if(result!=''){
                    err = err+1;
                }
            }

            //amount
            if(amt == ""){
                err = err+1;
                $('#err_amount_recharge').html("<?php echo __('Please enter Amount') ?>");
            }
            else{
                $('#err_amount_recharge').html("");
            }
            if((amt!="") && (!validateAmount(amt))){
                err = err+1;
                $('#err_amount_recharge').html("<?php echo __('Please enter valid amount') ?>");
            }
            else if(amt!=""){
                $('#err_amount_recharge').html("");
            }
            //check
            if(check_no == ""){
                err = err+1;
                $('#err_check_number').html("<?php echo __('Please enter Cheque Number') ?>");
            }
            else{
                $('#err_check_number').html("");
            }
            //check
            if(check_no != "" && validateAlphaNum(check_no)){
                err = err+1;
                $('#err_check_number').html("<?php echo __('Invalid Cheque Number(Only Alphanumeric Characters Accepted)') ?>");
            }
            else if(check_no != ""){
                $('#err_check_number').html("");
            }
            //acct no
            if(account_no == ""){
                err = err+1;
                $('#err_account_number').html("<?php echo __('Please enter Payee Account Number') ?>");
            }
            else{
                $('#err_account_number').html("");
            }

            if(account_no != "" && validateAlphaNum(account_no)){
                err = err+1;
                $('#err_account_number').html("<?php echo __('Invalid Payee Account Number(Only Alphanumeric Characters Accepted)') ?>");
            }
            else if(account_no != ""){
                $('#err_account_number').html("");
            }
            //sort_code
            if(sort_code == ""){
                err = err+1;
                $('#err_sort_code').html("<?php echo __('Please enter Sort Code') ?>");
            }
            else{
                $('#err_sort_code').html("");
            }
            if((sort_code != "") && (isNaN(sort_code))){
                err = err+1;
                $('#err_sort_code').html("<?php echo __('Please enter valid Sort Code') ?>");
            }
            else if(sort_code!="" && alphaNumricRegexCheck($('#sort_code').val()) == null){
                $('#err_sort_code').html("");
                var result = sortCodeValidation();
                if(result!=1){
                    err = err+1;
                }
            }
            //check if wallet number is in our db; through ajax
            if(err == 0){
                if(action == 'confirm_details'){
                    if((document.getElementById('disclaimer_cheque_details_recharge').checked!=true)){
                        err = err+1;
                        alert("<?php echo __('Please select checkbox') ?>");
                        $('#disclaimer_cheque_details_recharge').attr('checked', false);
                        $('#confirm_cheque_details').show();
                        return false;
                    }else{

                        $('#err_disclaimer_cheque_details_recharge').html('');
                        $('#err_disclaimer_cheque_details_recharge').hide();

                    }
                }else{
                    $('#charge_wallet_btn').hide();
                    $('#disclaimer_cheque_details_recharge').hide();
                    $('#confirm_cheque_details').hide();
                }
                $('#chargeLoader').hide();
                $('#charge_wallet_btn').hide(); //FS #35788 charge wallet button is not hiding even if all details are correct.
                if(action == 'confirm_details'){
                    $('#confirm_cheque_details').hide();
                    $('#ewallet_number').attr('readonly', 'true');
                    $('#amount').attr('readonly', 'true');
                    $('#check_number').attr('readonly', 'true');
                    $('#account_number').attr('readonly', 'true');
                    $('#sort_code').attr('readonly', 'true');
                    $.post('confirmDetails',$('#pfm_ewallet_search_form').serialize(), function(data){
                        $('#confirm_details').html(data);
                    });
                }
                else if(action  == 'make_payment'){
                    $('#chargeLoader').show();
                    $.post('confirmDetails', {ewallet_number: wallet_no,check_number:check_no,account_number:account_no,sort_code:sort_code,amount: amt, pay_mode:'<?php echo sfConfig::get('app_payment_mode_option_Cheque') ?>',make_payment:true}, function(data){
                        $('#chargeLoader').hide();
                        if(isNaN(data)){$('#flash_error').html(data)}else{$("#validation_number").val("");$("#validation_number").val(data);$('#pfm_ewallet_search_form').submit();}

                    });
                }
            }else{
                $('#disclaimer_cheque_details_recharge').attr('checked', false);
            }

     }
    function acctNumberValidation(){
        var wallet_no = $("#ewallet_number").val();
        var url = '<?php echo url_for("recharge_ewallet/verifyWalletNumber"); ?>';
         var data=$.ajax({
            url: url,
            type:'post',
            data:{ewallet_number:wallet_no},
            async:false}).responseText;
            if(data=='logout'){
                location.reload();
            } else{
                if(data)
            {
                $('#err_ewallet_number').html(data);
                return data;
            }
            else
            {
                $('#err_ewallet_number').html("");
                return data;
            }
            }
    }
    function sortCodeValidation()
    {
        //
        var bank_id=<?php echo $bank_id; ?>;
        var url = '<?php echo url_for("paymentProcess/validateSortCode", true); ?>';

        var sort_code_var=$('#sort_code').val().replace(" ","");
          var result=$.ajax({
            url: url,
            type:'post',
            data:{bank_id:bank_id,sort_code:sort_code_var},
            async:false}).responseText;
            if(result=='logout'){
                location.reload();
            } else{
                if(result==true)
            {
                $('#err_sort_code').html("");
                return result;
            }
            else
            {
                $('#err_sort_code').html("Invalid Sort Code ");
                return result;
            }
        }
    }
    function alphaNumricRegexCheck(text)
    {
        var regEx=new RegExp('[^a-zA-Z0-9]+');
        return regEx.exec(text);


    }
</script>
