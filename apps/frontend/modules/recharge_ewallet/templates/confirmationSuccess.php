<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<div class="wrapForm2">
    <?php // echo form_tag($sf_context->getModuleName().'/Pay',array('name'=>'pfm_confirm_recharge_form','class'=>'dlForm multiForm', 'id'=>'pfm_confirm_recharge_form')) ?>


    <div id="update_msg"></div>
    <?php
    if ($err) {

        print "<div id='flash_error' class='error_list'><span>" . html_entity_decode($err) . "</span></div>";
    }
    if (!$err) {
 ?>
<?php echo ePortal_legend(__('Account Details')); ?>
        <input type="hidden" name="recharge_gateway" id="recharge_gateway" value="<?php echo $recharge_gateway ?>">
    <?php echo formRowComplete(__('Account Name'), $accountObj->getAccountName(), '', 'name', '', 'name_row'); ?>
    <?php echo formRowComplete(__('Account No.'), $accountObj->getAccountNumber(), '', 'acct_no', '', 'acct_no_row'); ?>
    <?php echo ePortal_legend(__('Recharge Amount Details')); ?>
    <?php echo formRowComplete('<input type="checkbox" name="check_amt" id="check_amt" value="0">' . '&nbsp;' . __('Recharge Amount'), format_amount($amount, 1), '', 'amt', '', 'acct_amount'); ?>
    <?php echo ($serviceCharge) ? formRowComplete('<input type="checkbox" name="check_service" id="check_service" value="0">' . '&nbsp;' . __('Service Charges'), format_amount($serviceCharge, 1), '', 'service', '', 'service') : ''; ?>
<?php echo formRowComplete('<input type="checkbox" name="check_tot" id="check_tot" value="0">' . '&nbsp;' . __('Total Amount'), format_amount($total_amount, 1), '', 'tot', '', 'tot'); ?>


        <input type="hidden" name="amount" id="amount" value="<?php echo $amount ?>">

        <div class="descriptionArea" id="descriptionArea">
            <input type="checkbox" name="disclaimer" id="disclaimer" value="0">  <?php echo __('I hereby acknowledge that the information provided above is true and correct.') ?>
        </div>
        <div class="form_nav_outer">
            <center >

<?php echo image_tag('../images/ajax-loader.gif', array('id' => 'loader1', 'style' => 'display:none')); ?>
            <span id="multiFormNav">
<?php echo button_to(__('Proceed to Pay'), '', array('class' => 'formSubmit', 'onClick' => 'checkAll()')); ?>&nbsp;<?php // echo button_to('Cancel','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('recharge_ewallet/rechargeUsingVisa').'\'')); ?>
            </span>
        </center>
        <?php
        if ($recharge_gateway == sfConfig::get('app_payment_mode_option_interswitch')) {
            echo image_tag('/img/interswitch-logo.jpg', array('alt' => 'Interswitch', 'border' => 0));
        }
        ?>

    </div>

</div>
<div id="recharge"></div>

<?php } ?>


    </div>

    <script language="Javascript">
        function checkAll() {
            var amount = "<?php echo (isset($amount)) ? $amount : 0; ?>";
            var serviceCharge = "<?php echo (isset($serviceCharge)) ? $serviceCharge : 0; ?>";
            var recharge_gateway = "<?php echo (isset($recharge_gateway)) ? $recharge_gateway : 0; ?>";
            if(document.getElementById('check_amt').checked == false)
            {
                alert('Please select Recharge Amount');
                return false;
            }
            if(serviceCharge != 0)
            {
                if(document.getElementById('check_service').checked == false)
                {
                    alert('Please select Service Charges');
                    return false;
                }
            }
            if(document.getElementById('check_tot').checked == false)
            {
                alert('Please select Total Amount');
                return false;
            }
            if(document.getElementById('disclaimer').checked == false)
            {
                alert('Please select Disclaimer');
                return false;
            }
            var url ="<?php echo url_for('recharge_ewallet/Pay') ?>";
        $('#loader1').show();
        $('#multiFormNav').hide();
        if(serviceCharge != 0)
        {
            document.getElementById('check_service').disabled = true;
        }
        document.getElementById('check_amt').disabled = true;
        document.getElementById('check_tot').disabled = true;
        document.getElementById('disclaimer').disabled = true;
    
        $.post(url,{recharge_gateway:recharge_gateway, amount: amount}, function(data){
      
            if(data == "logout"){
                
                location.reload();
            }
            else
            {
                $('#loader1').hide();
                $('#recharge').html(data);
            }

        });
        $('#loader').hide();
        //document.forms['pfm_confirm_recharge_form'].submit();
        //return true;

    }
</script>

