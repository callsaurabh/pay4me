<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class recharge_ewalletActions extends sfActions {

    public function checkBankUser() {
        $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
        $count = Doctrine::getTable('ServiceBankConfiguration')->chkAssociation($bank_id);
        if ($count)
            return true;
        else
            return false;
    }

    //for recharge by cash
    public function executeSearch(sfWebRequest $request) {

        $this->checkIsSuperAdmin();
        $this->chackBankUserFlag = $this->checkBankUser();

        $this->amount = "";
        $this->ewallet_number = "";
        $this->form = new CustomRechargeEwalletSearchForm();
    }

    //for recharge by check
    public function executeSearchCheck(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->bank_id = $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
        if (Doctrine::getTable('BankSortCodeMapper')->isvalidBankForCheckDraft($bank_id)) {
            $this->chackBankUserFlag = $this->checkBankUser();
            $this->amount = "";
            $this->ewallet_number = "";
            $this->form = new CustomRechargeEwalletCheckSearchForm();
        } else {
            throw new Exception("You are not authorized to access this page");
        }
    }

    //for recharge by draft
    public function executeSearchDraft(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->bank_id = $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
        if (Doctrine::getTable('BankSortCodeMapper')->isvalidBankForCheckDraft($bank_id)) {
            $this->chackBankUserFlag = $this->checkBankUser();
            $this->amount = "";
            $this->ewallet_number = "";
            $this->form = new CustomRechargeEwalletDraftSearchForm();
        }
    }

   public function executeConfirmDetails(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
            $this->pay_mode=$pay_mode=$request->getParameter('pay_mode');
        $this->err = '';
      if(sfConfig::get("app_recharge_ewallet_account") == false){    
         $this->err = sfConfig::get("app_recharge_ewallet_errmsg");
        } else {
        try {
            if(($request->hasParameter('ewallet_number')) && ($request->hasParameter('amount')) && $request->getParameter('amount')!='') {
                 $ewallet_number = $request->getParameter('ewallet_number');
                 $amount = $request->getParameter('amount');
                if(($ewallet_number=="") || (!is_int($ewallet_number)) || ($amount=="") && (!is_numeric($amount))) {
                    $walletObj = new EpAccountingManager;
                    $walletDetails = $walletObj->getWalletDetails($ewallet_number);
                    if((!is_object($walletDetails)) && $walletDetails == 0) {
                        throw new Exception(__("Invalid Account Number"));
                    }
                    else {

                        $capping = $this->rechargeCaping($ewallet_number, $amount);

                        if(!$capping) {
                            $collectionAccountObj = $walletDetails->getFirst()->getUserAccountCollection()->getFirst();
                            $currencyObj = $collectionAccountObj->getCurrencyCode();
                            $this->currencyId   = $currencyObj->getId();
                            $this->currency     = $currencyObj->getCurrency();
                            $this->currencyCode = $currencyObj->getCurrencyCode();
                            $this->userDetailObj = $collectionAccountObj->getSfGuardUser()->getUserDetail()->getFirst();

                            $managerObj = new payForMeManager();
                            $collection_account_id = $managerObj->getRechargeCollectionAccount($this->currencyId);
                            if(!$collection_account_id) {
                                         throw new Exception ("Crediting Account details not found!!");
                             }
                                                    //
                            $user_id = $this->userDetailObj->getUserId();
                            $user_status = $this->userDetailObj->getUserStatus();
                             if($user_status == '1') {
                                $this->accountObj = $walletDetails->getFirst();
                                $this->amount = $amount;
                                //check is input ewallet number is recharge by same teller between a time interval(time interval configure in app.yml).
                                //if yes throw error message
                                $ewalletRechargeInterval = sfConfig::get('app_ewallet_recharge_interval') ;
                                $permissionToRechargeResultSet = $walletObj->checkLastTransaction($this->accountObj->getId(),$this->getUser()->getGuardUser()->getId(),$ewalletRechargeInterval);
                                // echo $this->accountObj->getId().','.$this->getUser()->getGuardUser()->getId().','.$ewalletRechargeInterval."<br>";die;
                                $permissionToRecharge=$permissionToRechargeResultSet->count();//0;
                                if(!$permissionToRecharge){
                                    $permissionToRechargeResultRecord = $walletObj->checkLastTransaction($this->accountObj->getId(),0);
                                    $countTransction = $permissionToRechargeResultRecord->count();
                                    if($countTransction){
                                        $this->previousRechargeDate = $permissionToRechargeResultRecord->getLast()->getTransactionDate();
                                        $this->previousRechargeAmount = $permissionToRechargeResultRecord->getLast()->getAmount();
                                    }
                                    if($request->hasParameter('make_payment')) {//exit;
                                        $this->setLayout(null);
                                        $ewallet_account_id = $this->accountObj->getId();
                                        $accountingObj = new pay4meAccounting;
                                        #sfLoader::loadHelpers('ePortal');
                                        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                                        $amount_credit = convertToKobo($this->amount);
  					$pfmHelper=new pfmHelper();
                                        $acc_desc='Cash';
                                        if($pay_mode!='cash'){
                                            $acc_desc=$pfmHelper->getPMONameByConfName($pay_mode);
                                        }
                                        $accountingArr = $accountingObj->getAccountDetails($amount_credit,$ewallet_account_id,$ewallet_number,$this->currencyId,$acc_desc);
					if(is_array($accountingArr)) {
                                            $con = Doctrine_Manager::connection();

                                            try {
                                                $con->beginTransaction();
                                                $payment_transaction_number = $accountingObj->doAccounting($accountingArr);
                                                if($payment_transaction_number) {
                                                    //auditing that ewallet has been charged
                                                    $managerObj = new payForMeManager();
                                                    switch($pay_mode){
                                                        case 'cash':$managerObj->auditRecharge($ewallet_number);
                                                            break;
                                                        case 'Cheque':$managerObj->auditRechargeCheck($ewallet_number);
                                                            break;
                                                        case 'bank_draft':$managerObj->auditRechargeDraft($ewallet_number);
                                                            break;
                                                    }
							//getRechargeCollectionAccount from service_bank_configuration
                                                   $collection_account_id = $managerObj->getRechargeCollectionAccount($this->currencyId);
							 //make consolidation entry for recharge
                                                    if ($collection_account_id) {
                                                        $managerObj->updateEwalletAccount($collection_account_id, $ewallet_account_id, $amount_credit);
                                                    } else {
                                                        throw new Exception(__("Crediting Account details not found!!"));
                                                    }

$this->updateEwalletAcctDetails($pay_mode,$ewallet_account_id,$payment_transaction_number);
                                                    $gUser =  sfContext::getInstance()->getUser()->getGuardUser();
                                                    $biEnabled = $gUser->getBankUser()->getFirst()->getBank()->getBiEnabled();
   if ($biEnabled) {
                                                        $bankInteObj = new bankIntegration($payment_transaction_number, $collection_account_id, $this->currencyId, $amount_credit);
                                                        if ($bankInteObj) {
                                                            $con->commit();
                                                            try {
                                                            	$bankName = $bankInteObj->biVersionObj->getRequestObject()->getTransactionBankPosting()->getBank()->getBankName();
                                                            	$enabledBanksList = sfConfig::get('app_middleware_enabled_banks');
                                                            	if (!empty($enabledBanksList) && in_array($bankName, $enabledBanksList)) {
                                                            		$bankInteObj->QueueProcessing($bankInteObj->biVersionObj);
                                                            	} else {
                                                            		$taskId = EpjobsContext::getInstance()->addJob('SendBankNotification', 'middleware/BankNotificationRequestList', array('mwRequestId' => $bankInteObj->biVersionObj->getRequestObject()->getId()));
                                                            	} /*else {
                                                                	$bankInteObj->QueueProcessing($bankInteObj->biVersionObj);
                                                            	}*/
 	                                                  }
                                                          catch (Exception $e) {
                                                            	if (!empty($enabledBanksList) && in_array($bankName, $enabledBanksList)) {
                                                                	$taskId = EpjobsContext::getInstance()->addJob('MessagePosterToQueue', 'bankintegration/MessagePosterToQueue', array('messageRequestId' => $bankInteObj->biVersionObj->getRequestObject()->getId()));
                                                                	sfContext::getInstance()->getLogger()->debug("sceduled update queue mail job with id: $taskId", 'debug');
                                                                	$taskemail = EpjobsContext::getInstance()->addJob('sendPosterMailFailure', "email/MessagePosterFailure", array('failureReason' => $e->getMessage()));
                                                                	sfContext::getInstance()->getLogger()->debug("scheduled  mail job with id: $taskemail", 'debug');
                                                            	}
                                                            }
                                                        } else {
                                                            $pay4meLog = new pay4meLog();
                                                            $pay4meLog->createLogData("Rollback for validation no - " . $payment_transaction_number . " ", 'Rollback');
                                                            throw New Exception("Due to some internal problem recharge  could  not  be completed");
                                                        }
                                                    } else {
                                                        $con->commit();
                                                    }
return $this->renderText($payment_transaction_number);
 }
                                                else{
                                                    throw New Exception(__("Could not recharge due to some error!! Please contact administrator"));
                                                }
 } catch (Exception $e) {
                                                $con->rollback();
                                                throw $e;
                                            }
 }
                                        else {
                                            throw new Exception (__("Crediting Account details not found!!"));
                                        }

                                    }
  }else{
                                    $enocde = $permissionToRechargeResultSet->getLast()->getPaymentTransactionNumber().':'.$ewallet_number.':'.$permissionToRechargeResultSet->getLast()->getAmount();
                                    return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'recharge_ewallet',
          'action' => 'rechargeNotice','id' => base64_encode($enocde)))."'</script>");

                                }
                            }
                            else {
                                throw new Exception ("eWallet not linked to any user");
                            }
                        }
                        else {
                            throw new Exception ($capping);
  }
                    }
                }
                else {
                    throw new Exception("Invalid values for paramters eWallet Number and Amount!!");
                }

            }
            else {
                throw new Exception("Mandatory Paramters eWallet Number and Amount not found!!");
            }
        }

        catch (Exception $e)  {
            $this->err = $e->getMessage();
        }
   }  
  }

    private function updateEwalletAcctDetails($pay_mode, $ewallet_account_id, $validation_number) {
        if (($pay_mode == sfConfig::get('app_payment_mode_option_Cheque')) || ($pay_mode == 'bank_draft')) {
            sfContext::getInstance()->getLogger()->debug("add recharge acct details for " . $pay_mode . " [ewallet_account_id : $ewallet_account_id, validation_number : $validation_number]", 'debug');
            $request = sfContext::getInstance()->getRequest();
            if (($pay_mode == sfConfig::get('app_payment_mode_option_Cheque') && (($request->hasParameter('check_number')) && ($request->hasParameter('account_number'))
                    && $request->getParameter('check_number') != '' && $request->getParameter('account_number') != '')) || ($pay_mode == 'bank_draft')) {
                //Check/bank_draft Recharge
                $check_number = $request->getParameter('check_number');
                $draft_number = $request->getParameter('draft_number');
                $account_number = $request->getParameter('account_number');
                $sort_code = $request->getParameter('sort_code');
                $query = Doctrine_Query::create()
                                ->select('*')
                                ->from('EpMasterLedger')
                                ->where("payment_transaction_number = ?", $validation_number)
                                ->andWhere("entry_type = ?", 'credit')
                                ->andWhere("master_account_id = ?", $ewallet_account_id);
                $resLedger = $query->execute();
                if ($resLedger->count() == 1) {
                    $ledger_id = $resLedger->getFirst()->getId();
                    try {
                        $pfmHelperObj = new pfmHelper();
                        $ERAcctDetails = new EwalletRechargeAcctDetails();
                        if ($pay_mode == sfConfig::get('app_payment_mode_option_Cheque')) {
                            $check_id = $pfmHelperObj->getPMOIdByConfName('Cheque');
                            $ERAcctDetails->setPaymentModeOptionId($check_id);
                            $ERAcctDetails->setCheckNumber($check_number);
                            $ERAcctDetails->setAccountNumber($account_number);
                        } else if ($pay_mode == 'bank_draft') {
                            $draft_id = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                            $ERAcctDetails->setPaymentModeOptionId($draft_id);
                            $ERAcctDetails->setCheckNumber($draft_number);
                            $ERAcctDetails->setAccountNumber(NULL);
                        }
                        $ERAcctDetails->setSortCode($sort_code);
                        $ERAcctDetails->setLedgerId($ledger_id);
                        $ERAcctDetails->save();
                    } catch (Exception $e) {
                        sfContext::getInstance()->getLogger()->debug("error in " . pfmHelper::getChequeDisplayName() . " Recharge" . $e->getTraceAsString());
                    }
                }
            }
        }
    }

    public function executeConfirmDetails_old(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $this->err = '';
        try {
            if (($request->hasParameter('ewallet_number')) && ($request->hasParameter('amount')) && $request->getParameter('amount') != '') {
                $ewallet_number = $request->getParameter('ewallet_number');
                $amount = $request->getParameter('amount');
                if (($ewallet_number == "") || (!is_int($ewallet_number)) || ($amount == "") && (!is_numeric($amount))) {
                    $walletObj = new EpAccountingManager;
                    $walletDetails = $walletObj->getWalletDetails($ewallet_number);
                    if ((!is_object($walletDetails)) && $walletDetails == 0) {
                        throw new Exception(__("Invalid Account Number"));
                    } else {

                        $capping = $this->rechargeCaping($ewallet_number, $amount);

                        if (!$capping) {
                            $collectionAccountObj = $walletDetails->getFirst()->getUserAccountCollection()->getFirst();
                            $currencyObj = $collectionAccountObj->getCurrencyCode();
                            $this->currencyId = $currencyObj->getId();
                            $this->currency = $currencyObj->getCurrency();
                            $this->currencyCode = $currencyObj->getCurrencyCode();
                            $this->userDetailObj = $collectionAccountObj->getSfGuardUser()->getUserDetail()->getFirst();

                            $managerObj = new payForMeManager();
                            $collection_account_id = $managerObj->getRechargeCollectionAccount($this->currencyId);
                            if (!$collection_account_id) {
                                throw new Exception("Crediting Account details not found!!");
                            }
                            //
                            $user_id = $this->userDetailObj->getUserId();
                            $user_status = $this->userDetailObj->getUserStatus();
                            if ($user_status == '1') {
                                $this->accountObj = $walletDetails->getFirst();
                                $this->amount = $amount;
                                //check is input ewallet number is recharge by same teller between a time interval(time interval configure in app.yml).
                                //if yes throw error message
                                $ewalletRechargeInterval = sfConfig::get('app_ewallet_recharge_interval');
                                $permissionToRechargeResultSet = $walletObj->checkLastTransaction($this->accountObj->getId(), $this->getUser()->getGuardUser()->getId(), $ewalletRechargeInterval);
                                // echo $this->accountObj->getId().','.$this->getUser()->getGuardUser()->getId().','.$ewalletRechargeInterval."<br>";die;
                                $permissionToRecharge = $permissionToRechargeResultSet->count(); //0;
                                if (!$permissionToRecharge) {
                                    $permissionToRechargeResultRecord = $walletObj->checkLastTransaction($this->accountObj->getId(), 0);
                                    $countTransction = $permissionToRechargeResultRecord->count();
                                    if ($countTransction) {
                                        $this->previousRechargeDate = $permissionToRechargeResultRecord->getLast()->getTransactionDate();
                                        $this->previousRechargeAmount = $permissionToRechargeResultRecord->getLast()->getAmount();
                                    }
                                    if ($request->hasParameter('make_payment')) {//exit;
                                        $this->setLayout(null);
                                        $ewallet_account_id = $this->accountObj->getId();
                                        $accountingObj = new pay4meAccounting;
                                        #sfLoader::loadHelpers('ePortal');
                                        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                                        $amount_credit = convertToKobo($this->amount);
                                        $pfmHelper = new pfmHelper();
                                        $acc_desc = 'Cash';
                                        if ($pay_mode != 'cash') {
                                            $acc_desc = $pfmHelper->getPMONameByConfName($pay_mode);
                                        }
                                        $accountingArr = $accountingObj->getAccountDetails($amount_credit, $ewallet_account_id, $ewallet_number, $this->currencyId, $acc_desc);
                                        if (is_array($accountingArr)) {
                                            $con = Doctrine_Manager::connection();

                                            try {
                                                $con->beginTransaction();
                                                $payment_transaction_number = $accountingObj->doAccounting($accountingArr);
                                                if ($payment_transaction_number) {
                                                    //auditing that ewallet has been charged
                                                    $managerObj = new payForMeManager();
                                                    switch ($pay_mode) {
                                                        case 'cash':$managerObj->auditRecharge($ewallet_number);
                                                            break;
                                                        case 'Cheque':$managerObj->auditRechargeCheck($ewallet_number);
                                                            break;
                                                        case 'bank_draft':$managerObj->auditRechargeDraft($ewallet_number);
                                                            break;
                                                    }


                                                    //getRechargeCollectionAccount from service_bank_configuration
                                                    $collection_account_id = $managerObj->getRechargeCollectionAccount($this->currencyId);
                                                    //make consolidation entry for recharge
                                                    if ($collection_account_id) {
                                                        $managerObj->updateEwalletAccount($collection_account_id, $ewallet_account_id, $amount_credit);
                                                    } else {
                                                        throw new Exception(__("Crediting Account details not found!!"));
                                                    }
                                                    $this->updateEwalletAcctDetails($pay_mode, $ewallet_account_id, $payment_transaction_number);
                                                    //  $this->validation_number = $payment_transaction_number;
                                                    // WP040 save detail of recharge in message queue request
                                                    $gUser = sfContext::getInstance()->getUser()->getGuardUser();
                                                    $biEnabled = $gUser->getBankUser()->getFirst()->getBank()->getBiEnabled();
                                                    if ($biEnabled) {
                                                        $bankInteObj = new bankIntegration();
                                                        $messageArray = $bankInteObj->getRechargeArray($payment_transaction_number, $collection_account_id, $this->currencyId, $amount_credit);


                                                        $messagerequestId = $bankInteObj->saveMessage($messageArray);
                                                        if ($messagerequestId) {
                                                            $con->commit();
                                                            try {
                                                                //return $this->renderText($payment_transaction_number);
                                                                $bankInteObj->QueueProcessing($messagerequestId);
                                                            } catch (Exception $e) {

                                                                $taskId = EpjobsContext::getInstance()->addJob('MessagePosterToQueue', 'bankintegration/MessagePosterToQueue', array('messageRequestId' => $messagerequestId));
                                                                sfContext::getInstance()->getLogger()->debug("sceduled update queue mail job with id: $taskId", 'debug');
                                                                $taskemail = EpjobsContext::getInstance()->addJob('sendPosterMailFailure', "email/MessagePosterFailure", array('failureReason' => $e->getMessage()));
                                                                sfContext::getInstance()->getLogger()->debug("scheduled  mail job with id: $taskemail", 'debug');
                                                            }
                                                        } else {
                                                            $pay4meLog = new pay4meLog();
                                                            $pay4meLog->createLogData("Rollback for validation no - " . $payment_transaction_number . " ", 'Rollback');
                                                            throw New Exception("Due to some internal problem recharge  could  not  be completed");
                                                        }
                                                    } else {
                                                        $con->commit();
                                                    }

                                                    //$bankAcronym = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank()->getAcronym();

                                                    return $this->renderText($payment_transaction_number);
                                                } else {
                                                    throw New Exception(__("Could not recharge due to some error!! Please contact administrator"));
                                                }
                                            } catch (Exception $e) {
                                                $con->rollback();
                                                throw $e;
                                            }
                                        } else {
                                            throw new Exception(__("Crediting Account details not found!!"));
                                        }
                                    }
                                } else {
                                    $enocde = $permissionToRechargeResultSet->getLast()->getPaymentTransactionNumber() . ':' . $ewallet_number . ':' . $permissionToRechargeResultSet->getLast()->getAmount();
                                    return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'recharge_ewallet',
                                                'action' => 'rechargeNotice', 'id' => base64_encode($enocde))) . "'</script>");
                                }
                            } else {
                                throw new Exception("eWallet not linked to any user");
                            }
                        } else {
                            throw new Exception($capping);
                        }
                    }
                } else {
                    throw new Exception("Invalid values for paramters eWallet Number and Amount!!");
                }
            } else {
                throw new Exception("Mandatory Paramters eWallet Number and Amount not found!!");
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    public function executeRecharge(sfWebRequest $request) {

        $this->err = "";
        $this->checkIsSuperAdmin();
        try {
            $this->recharge_gateway = $request->getParameter("recharge_gateway");
            if (!in_array($this->recharge_gateway, sfConfig::get('app_recharge_options_gateways'))) {
                throw new Exception("Invalid gateway chosen for recharge");
            }
            $this->amount = "";
        } catch (Exception $e) {
            print $this->err = $e->getMessage();
            exit;
        }

        $this->form = new RechargeForm('', array('recharge_gateway' => $this->recharge_gateway));
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('recharge'));
            if ($this->form->isValid()) {
                $this->forward($this->getModuleName(), 'confirmation');
            }
        }
    }

    public function executeConfirmation(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->err = 0;
        $param = $request->getParameter('recharge');
        $amount = $param['amount'];
        $recharge_gateway = $request->getParameter("recharge_gateway");
        $paymentModeOptionObj = Doctrine::getTable('PaymentModeOption')->findByName($recharge_gateway);
        $payment_mode_option_id = $paymentModeOptionObj->getFirst()->getId();
        $userObj = $this->getUser();
        $ewallet_account_id = $userObj->getUserAccountId();
        $ewallet_number = $userObj->getUserAccountNumber();
        $validateRecharge = $this->rechargeCaping($ewallet_number, $amount);
        
        if(sfConfig::get("app_recharge_ewallet_account") == false){
         $this->err = sfConfig::get("app_recharge_ewallet_errmsg");
        } else {
            if ($validateRecharge) {
                $this->err = $validateRecharge;
            } else if (($amount != "") && ($amount >= 0)) {

                $currency_id = 1;
                $collectionObj = new UserAccountManager();
                $ewallet_account_id = $collectionObj->getAccountIdByCurrency($currency_id);
                if (!$ewallet_account_id) {
                    $currencyObj = Doctrine::getTable('CurrencyCode')->find($currency_id);
                    $currency_display = $currencyObj->getCurrency() . "(" . $currencyObj->getCurrencyCode() . ")";
                    $this->err = 'Please create a new account for Currency  ' . $currency_display . ' and continue to Recharge';
                }

                //   $ewallet_account_id = $this->getUser()->getUserAccountId();
                else {
                    $walletObj = new EpAccountingManager;
                    $this->accountObj = $walletObj->getAccount($ewallet_account_id);
                    $this->amount = $amount;
                    $this->recharge_gateway = $recharge_gateway;
                    $eWalletSettingObj = eWalletSettingServiceFactory::getService();
                    $this->serviceCharge = $eWalletSettingObj->getServiceCharge($payment_mode_option_id, $amount);
                    $this->total_amount = $amount + $this->serviceCharge;
                }
            } else {
                $this->err = "Due to some error your application cannot be processed";
            }
        }
    }

    public function executeChkRechargeLimit(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $err = "";
        $amount = $request->getParameter('amount');
        $ewallet_account_id = $this->getUser()->getUserAccountId();
        if ($ewallet_account_id) {

            $eWalletSettingObj = eWalletSettingServiceFactory::getService();
            $chkRechargeLimit = $eWalletSettingObj->chkRechargeLimit($amount, '', $ewallet_account_id);
            if ($chkRechargeLimit) {
                #sfLoader::loadHelpers('ePortal');
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                $err = "You can recharge only upto " . format_amount($chkRechargeLimit, 1, 1);
            }
        }
        return $this->renderText($err);
    }

    public function executeVerifyWalletNumber(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->setLayout(null);
        $err = "";
        $wallet_number = $request->getParameter('ewallet_number');
        $walletObj = new EpAccountingManager;
        $walletDetails = $walletObj->getWalletDetails($wallet_number);
        if ((!is_object($walletDetails)) && $walletDetails == 0) {
            $err = "Invalid Account Number";
        }else{
            // condition for recharging only if it is ewallet_type = [ewallet, openid or both ] type else show message
            $epMasterobj = Doctrine::getTable('UserDetail')->getUserDetailBetDates($wallet_number);

            $billObj = billServiceFactory::getService();
            $permittedEwalletType = $billObj->permittedEwalletType($epMasterobj->execute()->getFirst()->getEwalletType());
            if(!$permittedEwalletType)
                $err = "This eWallet Account needs to be Activated/Re-activated";
        }
        return $this->renderText($err);
    }

    public function executeRechargeCheckReceipt(sfWebRequest $request) {
        $this->PM_check = true;
        $this->err = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
        if (Doctrine::getTable('BankSortCodeMapper')->isvalidBankForCheckDraft($bank_id)) {
            if ($request->hasParameter('check_number') && $request->hasParameter('account_number')) {
                $this->check_number = $request->getParameter('check_number');
                $this->account_number = $request->getParameter('account_number');
                $this->sort_code = $request->getParameter('sort_code');
                if ($this->check_number != "") {
                    if ($this->account_number != "") {
                        $this->executeRechargeReceipt($request);
                    } else {
                        $this->err = __("Please enter Payee Account Number");
                    }
                } else {
                    $this->err = __("Please enter Cheque number");
                }
            }
            $this->setTemplate('rechargeReceipt');
        }
    }

    public function executeRechargeDraftReceipt(sfWebRequest $request) {
        $this->PM_draft = true;
        $this->err = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
        if (Doctrine::getTable('BankSortCodeMapper')->isvalidBankForCheckDraft($bank_id)) {
            $this->draft_number = $request->getParameter('draft_number');
            $this->sort_code = $request->getParameter('sort_code');
            $this->executeRechargeReceipt($request);
            $this->setTemplate('rechargeReceipt');
        }
    }

    public function executeRechargeReceipt(sfWebRequest $request) {

        //   $this->checkIsSuperAdmin();
        $this->err = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        if ($request->hasParameter('validation_number')) {
            $this->validation_number = $request->getParameter('validation_number');
            if ($this->validation_number != "") {
                if (($request->hasParameter('ewallet_number'))) {
                    $this->amount = $request->getParameter('amount');
                    $this->pay_mode = $request->getParameter('pay_mode'); //Bug:36054
                    $wallet_number = $request->getParameter('ewallet_number');
                    $this->msg = "eWallet - " . $wallet_number . ' ' . __("Recharged Successfully");
                    $walletObj = new EpAccountingManager;
                    $walletDetails = $walletObj->getWalletDetails($wallet_number);
//                    echo "<pre>";
//                    print_r($this->validation_number);
//                    exit;
                    $transactionDetails = $walletObj->getDetailsByValidationNumber($this->validation_number);
                    if ((!is_object($walletDetails)) && $walletDetails == 0) {
                        $this->err = "Invalid Account Number";
                    } else {
                        $this->accountObj = $walletDetails->getFirst();
                        $collectionAccountObj = $walletDetails->getFirst()->getUserAccountCollection()->getFirst();
                        $currencyObj = $collectionAccountObj->getCurrencyCode();
                        $this->currencyId = $currencyObj->getId();
                        $this->userDetailObj = $collectionAccountObj->getSfGuardUser()->getUserDetail()->getFirst();
                        //$this->userDetailObj = $walletDetails->getFirst()->getUserDetail()->getFirst();
                        $userId = $this->userDetailObj->getUserId();
                        $this->transactionDetailsObj = $transactionDetails->getFirst();
                        // [WP033][Inhouse maintainace]
                        // add job for  bank charge
                        $this->addJobForBankRechageMail($this->validation_number, $userId, $this->amount, $wallet_number, $this->currencyId, $this->pay_mode);
                    }
                }
            } else {
                $this->err = __("Validation Number not found. Account not recharged");
            }
        } else {
            $this->err = __("Validation Number not found. Account not recharged");
        }
    }

    /**
     * function addJobForValuCardRechageMail()
     * @param $validation_number :validation number
     * Purpose : add job to send ewallet payment
     */
    public function addJobForBankRechageMail($validation_number, $userId, $amount, $wallet_number, $currencyId, $payMode) {
        //$notificationUrl = 'vbv_configuration/sendMail';
        $notificationUrl = 'email/sendbankRechargeMail';

        $userObj = $this->getUser();

        $bank = $userObj->getGuardUser()->getBankUser()->getFirst()->getBank()->getBankName();
        $recharge_date = date('Y-m-d H:i:s');


        $taskId = EpjobsContext::getInstance()->addJob('RechargeMailNotification', $notificationUrl, array('validation_number' => $validation_number, 'user_id' => $userId, 'amount' => $amount, 'wallet_number' => $wallet_number, 'currency_id' => $currencyId, 'bank_name' => $bank, 'recharge_date' => $recharge_date, 'pay_mode' => $payMode));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
    }

    public function executePay(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $type = "recharge";
        $recharge_gateway = $request->getParameter('recharge_gateway');
        $paymentModeOptionObj = Doctrine::getTable('PaymentModeOption')->findByName($recharge_gateway);
        $paymentModeOptionId = $paymentModeOptionObj->getFirst()->getId();
        $amount = $request->getParameter('amount');
        $eWalletSettingObj = new eWalletSettingManager();
        $serviceCharges = $eWalletSettingObj->getServiceCharge($paymentModeOptionId, $amount);
        $totalCharges = $amount + $serviceCharges;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $currency_id = 1;
        $collectionObj = new UserAccountManager();
        $app_id = $collectionObj->getAccountIdByCurrency($currency_id);
        $order_id = $this->createOrder($paymentModeOptionId, convertToKobo($serviceCharges), convertToKobo($totalCharges), $app_id);
        //$app_id=$this->getUser()->getUserAccountId();
        $this->getRequest()->setParameter('orderId', $order_id);
        $this->getRequest()->setParameter('transNo', $app_id);
        $this->getRequest()->setParameter('type', $type);
        $this->getRequest()->setParameter('payby', $recharge_gateway);
        $this->getRequest()->setParameter('totalCharges', $totalCharges);
        if (strtolower($recharge_gateway) == 'credit_card')
            $recharge_gateway = 'vbv';
        $this->forward($recharge_gateway . '_configuration', $recharge_gateway . 'Recharge');
    }

    public function createOrder($paymentModeOptionId, $serviceCharges, $totalCharges, $app_id) {
        $type = "recharge";
        // $app_id=$this->getUser()->getUserAccountId();
        $userId = $this->getUser()->getGuardUser()->getId();
        return $order_id = Doctrine::getTable('GatewayOrder')->saveOrder($app_id, $type, $paymentModeOptionId, $userId, 1, $totalCharges, $serviceCharges);
    }

    public function executeRechargeViaCard(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->setLayout(null);
        $con = Doctrine_Manager::connection();

        try {
            $con->beginTransaction();
            //make clear, indirect, credit entries into the ewallet and pay4me Visa account
            $recharge_id = $request->getParameter('recharge_id');
            //$amount = $request->getParameter('amount');
            $amountAvailableObj = Doctrine::getTable('RechargeAmountAvailability')->find(array($recharge_id));

            //$result_obj = $amountAvailableObj->getFirst();
            $master_account_id = $amountAvailableObj->getMasterAccountId();
            $total_amount_paid = $amountAvailableObj->getTotalAmountPaid();
            $amount_wallet = $amountAvailableObj->getAmountWallet();
            $payment_mode_option_id = $amountAvailableObj->getPaymentModeOptionId();
            $service_charge = $amountAvailableObj->getServiceCharges();
            $accountingManagerObj = new EpAccountingManager;
            $account_details = $accountingManagerObj->getAccount($master_account_id);
            $account_number = $account_details->getAccountNumber();
            //  $ewallet_id = $this->getUser()->getUserAccountNumber();
            $accountingObj = new pay4meAccounting;
            $isClear = 1;
            $accountingArr = $accountingObj->getRechargeAccounts($account_number, $master_account_id, $total_amount_paid, $payment_mode_option_id, $isClear, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT, $service_charge);
            if (is_array($accountingArr)) {
                $event = $this->dispatcher->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));
            }
            //make an entry into the ewallet_user_account_consolidation table
            $collection_account_id = $accountingObj->getCollectionAccount($payment_mode_option_id, NULL, NULL, 'Recharge');
            $managerObj = new payForMeManager();
            $managerObj->updateEwalletAccount($collection_account_id, $master_account_id, $amount_wallet, 'credit', $service_charge);

            $amountAvailableObj->setIsCredited('1');
            $amountAvailableObj->save();


            $con->commit();
            return $this->renderText("eWallet charged Successfully");
        } catch (Exception $e) {
            $con->rollback();
            return $this->renderText($e);
            //   throw $e;
        }
    }

    //  public function executeWalletTransfer(sfWebRequest $request){
    //    $accountingObj = new pay4meAccounting();
    //    $walletObj = $accountingObj->eWalletTransfer("100", "101", "Wallet Transfer", 100);
    //  }
    function rechargeCaping($ewallet_number, $amount) {
        $this->checkIsSuperAdmin();
        #sfLoader::loadHelpers('ePortal');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $amount = convertToKobo($amount);
        $Amt = Doctrine::getTable('EwalletSetting')->getChargeAmountLimit();
        $Days = Doctrine::getTable('EwalletSetting')->getChargeDaysLimit();

        //$beforeDate=date("Y-m-d",mktime(0,0,0,date("m"),date("d")- $Days[0]['var_value'],date("Y")));
        //$totAmt= Doctrine::getTable('EpMasterLedger') ->getCrdtAmtBetDate($beforeDate,$ewallet_number);
        //
        $totAmt = array();
        $totAmt[0]['sum'] = 0;
        if (($totAmt[0]['sum'] + $amount) > $Amt[0]['var_value']) {//if cheking anount limit start
            if ($totAmt[0]['sum'] >= $Amt[0]['var_value']) {//if cheking anount limit start
                $chargableAmt = $Amt[0]['var_value'] - $totAmt[0]['sum'];
                return 'Your recharge limit of N' . format_amount($Amt[0]['var_value'], 0, 1) . ' has been  exceeded.';
            } else {
                $chargableAmt = $Amt[0]['var_value'] - $totAmt[0]['sum'];
                return 'You can recharge upto N' . format_amount($chargableAmt, 0, 1) . " at one transaction";
                //              'Your recharge limit ('.format_amount($Amt[0]['var_value'],NULL,1).' Naira) will be exceeded with this amount  . You can recharge upto '.$chargableAmt.' naira
            }
        }
        return false;
    }

    public function checkIsSuperAdmin() {
        if ($this->getUser() && $this->getUser()->isAuthenticated()) {
            $group_name = $this->getUser()->getGroupNames();
            if (in_array(sfConfig::get('app_pfm_role_admin'), $group_name)) {
                $this->redirect('@adminhome');
            }
        }
    }

    public function executeRechargeSandbox(sfWebRequest $request) {

        try {
            if (sfConfig::get('sf_environment') == "sandbox") {
                $userObj = $this->getUser();
                $ewallet_account_id = $userObj->getUserAccountId();
                $ewallet_number = $userObj->getUserAccountNumber();
                if ($ewallet_number) {
                    if ($request->hasParameter('amount') && ($request->getParameter('amount') > 0)) {
                        $amount = $request->getPostParameter('amount');
                        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                        $amount = convertToKobo($amount);
                        $accountingObj = new pay4meAccounting;
                        $accountingArr = $accountingObj->getAccountDetails($amount, $ewallet_account_id, $ewallet_number);
                        if (is_array($accountingArr)) {
                            $con = Doctrine_Manager::connection();

                            try {
                                $con->beginTransaction();

                                $payment_transaction_number = $accountingObj->doAccounting($accountingArr);
                                if ($payment_transaction_number) {
                                    //auditing that ewallet has been charged
                                    $managerObj = new payForMeManager();
                                    $managerObj->auditRecharge($ewallet_number);

                                    //getRechargeCollectionAccount from service_bank_configuration
                                    $collection_account_id = $managerObj->getRechargeCollectionAccount();

                                    //make consolidation entry for recharge
                                    if ($collection_account_id) {
                                        $managerObj->updateEwalletAccount($collection_account_id, $ewallet_account_id, $amount);
                                    } else {
                                        throw new Exception("Crediting Account details not found!!");
                                    }
                                    //  $this->validation_number = $payment_transaction_number;
                                    $con->commit();
                                    //                        $this->getRequest()->setParameter('validation_number', $payment_transaction_number);
                                    //                        $this->getRequest()->setParameter('ewallet_number', $ewallet_number);
                                    //                        $this->getRequest()->setParameter('amount', $amount);
                                    //                        $this->forward('recharge_ewallet', 'RechargeReceipt');

                                    return $this->renderText("<font color='green'>eWallet " . $ewallet_number . " Charged Successfully<br> Your transaction Number is :" . $payment_transaction_number . "</font>");
                                } else {
                                    throw New Exception("Could not recharge due to some error!! Please contact administrator");
                                }
                            } catch (Exception $e) {
                                $con->rollback();
                                throw $e;
                            }
                        } else {
                            throw new Exception("Crediting Account details not found!!");
                        }
                    } else {
                        throw new Exception("Trying to recharge with an invalid amount");
                    }
                } else {
                    throw new Exception("Trying to recharge without an eWallet Number!!");
                }
            } else {
                throw new Exception("Unauthorized access");
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function executeRechargeNotice(sfWebRequest $request) {
        //   $this->checkIsSuperAdmin();
        $this->err = 0;
        $this->paymentModeName='';
        //WP063 Print receipt changes
        $userAccObj = new UserAccountManager();
        $this->isSuperadmin = $userAccObj->IsSuperAdmin();
        if ($request->hasParameter('template')) {
            $template = $request->getParameter('template');
            $this->bankName = $request->getParameter('bank_name');
            $bankDetail = Doctrine::getTable('Bank')->getBankId($this->bankName);
            $this->bankAcr = $bankDetail['acronym'];
            $this->branchName = $request->getParameter('branch_name');
            $this->setLayout($template);
        }
        if ($request->hasParameter('id')) {
            $encodeval = $request->getParameter('id');
            $encode = base64_decode($encodeval);
            $encode = explode(':', $encode);
            if (!empty($encode[0]) && !empty($encode[1]) && !empty($encode[2])) {
                $this->validation_number = $encode[0];
                $this->amount = $encode[2];
                $wallet_number = $encode[1];
                $walletObj = new EpAccountingManager;
                $walletDetails = $walletObj->getWalletDetails($wallet_number);
                $transactionDetails = $walletObj->getDetailsByValidationNumber($this->validation_number);
                if ((!is_object($walletDetails)) && $walletDetails == 0) {
                    $this->err = "Invalid Account Number";
                } else {
                    $this->pay_mode_id = Doctrine::getTable('EwalletRechargeAcctDetails')->getPaymentMOIdByValidationNo($this->validation_number);
                    $this->check_details = Doctrine::getTable('EwalletRechargeAcctDetails')->getCheckDetailsByValidationNo($this->validation_number);
                    $this->accountObj = $walletDetails->getFirst();
                    $collectionAccountObj = $walletDetails->getFirst()->getUserAccountCollection()->getFirst();
                    $this->userDetailObj = $collectionAccountObj->getSfGuardUser()->getUserDetail()->getFirst();
                    $currencyObj = $collectionAccountObj->getCurrencyCode();
                    $this->currencyId = $currencyObj->getId();
                    $this->transactionDetailsObj = $transactionDetails->getFirst();
                    $this->timeForRecharge = $this->timeForRecharge($transactionDetails->getFirst()->getTransactionDate());
                }
            } else {
                $this->err = "Validation Number not found. Account not recharged";
            }
        }
    }

    function timeForRecharge($transactionDateTime) {
        $data_ref = date('Y-m-d H:i:s', strtotime($transactionDateTime));
        $current_date = date('Y-m-d H:i:s');
        $to_time = strtotime($current_date);
        $from_time = strtotime($data_ref);
        $remainingMinutes = round(abs($to_time - $from_time) / 60);
        return sfConfig::get('app_ewallet_recharge_interval') - $remainingMinutes;
    }

    function validateRechargeLimit($amount) { //should use rechargeCaping
        $ewallet_account_id = $this->getUser()->getUserAccountId();
        if ($ewallet_account_id) {
            $eWalletSettingObj = eWalletSettingServiceFactory::getService();
            $chkRechargeLimit = $eWalletSettingObj->chkRechargeLimit($amount, '', $ewallet_account_id);
            if ($chkRechargeLimit) {
                #sfLoader::loadHelpers('ePortal');
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                $err = "You can recharge only upto " . format_amount($chkRechargeLimit, 1, 1);
                return $err;
            }
        }
        return false;
    }

}

?>
