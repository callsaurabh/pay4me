<?php

/**
 * broadcast actions.
 *
 * @package    mysfp
 * @subpackage broadcast
 * @author     Pritam Gupta
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class broadcastActions extends sfActions {
  public function executeIndex(sfWebRequest $request) {
    $this->broadcast_list = Doctrine::getTable('BroadcastMessage')->getAllRecords();
    if(($this->getRequestParameter('flag')) == "delete") {
      $this->getUser()->setFlash('notice', sprintf('Message deleted successfully'),false);
    }
    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('BroadcastMessage',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->broadcast_list);
    $this->pager->setPage($this->page);
    $this->pager->init();
  }

  public function executeShow(sfWebRequest $request) {
    $this->broadcast = Doctrine::getTable('BroadcastMessage')->find($request->getParameter('id'));
    $this->forward404Unless($this->broadcast);
  }

  public function executeNew(sfWebRequest $request) {
    $this->form = new BroadcastMessageForm();
  }

  public function executeCreate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post'));
    $this->form = new BroadcastMessageForm();
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request) {
    $this->forward404Unless($broadcast = Doctrine::getTable('BroadcastMessage')->find($request->getParameter('id')), sprintf('Object message does not exist (%s).', $request->getParameter('id')));
    $this->form = new BroadcastMessageForm($broadcast);
  }

  public function executePermission(sfWebRequest $request) {

    $this->message_id=$request->getParameter('id');
    $selected_bank_branch =  Doctrine::getTable('BroadcastMessagePermission')->getSelectedBranchId($this->message_id);

    $selected_bank = array();
    $selected_user_role = array();
    foreach($selected_bank_branch as $sBank){
        if(!in_array($sBank['bank_id'],$selected_bank)){
            $selected_bank[] = $sBank['bank_id'];
        }
        if(!in_array($sBank['specific_to'],$selected_user_role)){
            $selected_user_role[] = $sBank['specific_to'];
        }
     }
    $this->user_role = implode(',', $selected_user_role);
    $this->bank_select_id = implode(',', $selected_bank);

    
    $this->form = new BroadcastMessagePermissionForm(array(),array('bank_id'=>$this->bank_select_id,'user_role'=>$this->user_role));
    $this->forward404Unless($broadcast = Doctrine::getTable('BroadcastMessage')->find($request->getParameter('id')), sprintf('Object message does not exist (%s).', $request->getParameter('id')));
    
    $this->selectedPermission=Doctrine::getTable('BroadcastMessagePermission')->getSelectedBranchId($this->message_id);

    if($request->hasParameter('premission_field')){
        $this->updatePermission($request, $this->form);
    }
  }

  public function executeGetBranchList(sfWebRequest $request)  {
    $str = '';
    if ($request->getParameter('bank_id'))
    {
      $bank_arr=explode(',',$request->getParameter('bank_id'));
      if(in_array('ewallet_users', $bank_arr)){
        $str .= '<option value="ewallet_users" selected >-- For All eWallet Users </option>';
      }
      else {
        $q = Doctrine::getTable('BankBranch')
        ->createQuery('st')
        ->where("status = '1'")
        ->andWhere("deleted_at IS NULL")
        ->orderBy('name')
        ->execute()->toArray();
        
        $branch_array=explode(',',$request->getPostParameter('branch_id'));

        if(in_array('all_users', $branch_array)){
          $str .= '<option value="all_users" selected >-- For All include Bank Admin </option>';
        }else{
            $str .= '<option value="all_users" >-- For All include Bank Admin </option>';
        }

        if(in_array('only_admin', $branch_array)){
            $str .= '<option value="only_admin" selected >-- For Bank Admin </option>';
        }else{
            $str .= '<option value="only_admin" >-- For Bank Admin </option>';
        }

        if(in_array('bank_report_admin', $branch_array)){
            $str .= '<option value="bank_report_admin" selected >-- For All Bank Report Admin </option>';
        }else{
            $str .= '<option value="bank_report_admin">-- For All Bank Report Admin </option>';
        }

        if(in_array('bank_branch_report_user', $branch_array)){
            $str .= '<option value="bank_branch_report_user" selected >-- For All Bank Branch Report User </option>';
        }else {
            $str .= '<option value="bank_branch_report_user">-- For All Bank Branch Report User </option>';
        }

        foreach($q as $key => $value){
          $selected = (in_array($value['id'], $branch_array))? 'selected':'' ;
          $str .= '<option value="'.$value['id'].'"'.$selected.'>'.$value['name'].'</option>';
        }

      }
    }

    if(empty($str)) {
        $str .= '<option value="" >-- Please Select Branch </option>';
    }

    return $this->renderText($str);
  }

  public function updatePermission(sfWebRequest $request, sfForm $form) {

        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $post_value =$request->getPostParameters();
            $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
            $message_id= $request->getParameter('id');
            $bank_array= $post_value['premission_field']['bank_id'];
            $user_role_array= $post_value['premission_field']['specific_to'];

            Doctrine::getTable('BroadcastMessagePermission')->removePreviousMessageForSelectedList($message_id);
            $set = 0;
            foreach($bank_array as $bank){
                /* Here for (0) for all eWallet and All Bank user we are putting the null */                
                if($bank){
                    if($bank == 'all_banks' || $bank == '0' ){ $bank = null; }
                    foreach($user_role_array as $uRole){
                        $message_permission = new BroadcastMessagePermission();
                        $message_permission->setMessageId($message_id);
                        $message_permission->setSpecificTo($uRole);
                        $message_permission->setBankId($bank);
                        $message_permission->save();
                    }
                }else{
                    $bank = null;
                    $ewalletUserType = '';
                    foreach($user_role_array as $uRole){
                        if($uRole!= 'ewallet_user'){
                            $ewalletUserType .= $uRole.',';
                        }else{
                            $ewalletUserType = $uRole;
                        }
                    }
                    $message_permission = new BroadcastMessagePermission();
                    $message_permission->setMessageId($message_id);
                    $message_permission->setSpecificTo($ewalletUserType);
                    $message_permission->setBankId($bank);
                    $message_permission->save();
                }
                $set = 1;
            }
        if($set == 1) {
            $val = 'Broadcast Message permission updated successfully.';
        }else{
            $val = 'Please select Message Permissions properly.';
        }
        $this->getUser()->setFlash('notice', sprintf($val));
        $this->redirect('broadcast/index');
      }
  }

  public function executeUpdate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($broadcast = Doctrine::getTable('BroadcastMessage')->find($request->getParameter('id')), sprintf('Object message does not exist (%s).', $request->getParameter('id')));
    $this->form = new BroadcastMessageForm($broadcast);
    $this->processForm($request, $this->form);
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request) {
    $request->checkCSRFProtection();
    //if assigined someone then cant delete message
    $resultSet = Doctrine::getTable('BroadcastMessage')->checkMessageIsAssigned($request->getParameter('id'));
    if($resultSet==false)
    {
      $this->forward404Unless($broadcast = Doctrine::getTable('BroadcastMessage')->find($request->getParameter('id')), sprintf('Object message does not exist (%s).', $request->getParameter('id')));
      $broadcast->delete();
      $this->redirect('broadcast/index?flag=delete');
    }
    else
    {
      $this->getUser()->setFlash('error',"Message can't be deleted. Message has been assigned to someone." , false);
      $this->forward('broadcast', 'index');
    }
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid()) {

      //    $status=0;
      //    $updateBroadcastList = Doctrine::getTable('BroadcastMessage')->getAllOldRecordsOff($status);
      $merchant_service = $form->save();
      if(($request->getParameter('action')) == "update") {
        $this->getUser()->setFlash('notice', sprintf('Broadcast Message updated successfully'));
      }
      else if(($request->getParameter('action')) == "create") {
        $this->getUser()->setFlash('notice', sprintf('Broadcast Message added successfully'));
      }
      $this->redirect('broadcast/index');
    }
  }

  public function executeActivateMessage(sfWebRequest $request) {
    $messageId = $request->getParameter('messageId');
    $case = $request->getParameter('use');
    $status=0;
    // $updateBroadcastList = Doctrine::getTable('BroadcastMessage')->getAllOldRecordsOff($status);
    if($case != '2') {
    $message_active_update = Doctrine::getTable('BroadcastMessage')->updateMessageActiveById($messageId,$case);
    if($message_active_update=='1'){
      if($case=='0'){
        $this->getUser()->setFlash('notice','Message is set Inactive');
        $this->redirect('broadcast/index');
      }else if($case=='1'){
        $this->getUser()->setFlash('notice','Message is set Active');
        $this->redirect('broadcast/index');
      }
      //      $this->forward('broadcast', 'index');
    }else{
      $this->getUser()->setFlash('error','your are not authorized user !' , false);
      $this->forward('broadcast', 'index');
    }
    }
    else {
        $this->getUser()->setFlash('notice','Message has been expired. Please change the expire date of message as present/future date.');
        $this->redirect('broadcast/index');
    }
  }

  public function executeUserRole(sfWebRequest $request){

        $id = $request->getParameter('checkId');
        $optionId = explode(',', $id);
        $check = false;
        $options = '';
        if(in_array('0', $optionId)){
             $user_role_array['ewallet_user'] = 'All eWallet Users';
             foreach(sfConfig::get('app_ewallet_account_type') as $key=>$value){
                $user_role_array[$key] = $value;
             }
             foreach(sfConfig::get('app_pay4me_account_type') as $key=>$value){
                $user_role_array[$key] = $value;
             }
             if(count($optionId) > 1){
                 $check = true;
             }
        }else {
            $userRole  = new UserRoleHelper();
            $userRoleArray = $userRole->getBankUserList();
            $user_role_array['all_role'] = 'All Roles';
            unset($userRoleArray['']);
            foreach($userRoleArray as $key=>$value){
                $user_role_array[$key] = $value;
            }
        }
        //asort($user_role_array);
        //$options = '';
        foreach($user_role_array as $key=>$urArray){
            $selected=($key=='all_role' || $key=='ewallet_user')?'selected':'';
            $options .='<option value="'.$key.'"'.$selected.'>'.$urArray.'</option>';
        }

        if(!$check){
            echo $options;
        }else{
            echo 1;
        }
        exit;
  }


  // Bug:36490 [06-02-2013] session implemetation on legal message
  public function executeSetSessionVariableLegal(sfWebRequest $request){
     if($request->hasParameter('label') ){
         $this->getUser()->setAttribute('LegalChecked', $request->getParameter('label'));
     }
     return $this->renderText($this->getUser()->getAttribute('LegalChecked', '0'));
  }


  public function executeSetSessionVariableBroadcast(sfWebRequest $request){
     if($request->hasParameter('label1') ){
         $this->getUser()->setAttribute('BroadcastChecked', $request->getParameter('label1'));
     }
     return $this->renderText($this->getUser()->getAttribute('BroadcastChecked', '0'));
  }

  
}
