<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $bank->getid() ?></td>
    </tr>
    <tr>
      <th>Bank name:</th>
      <td><?php echo $bank->getbank_name() ?></td>
    </tr>
    <tr>
      <th>Country:</th>
      <td><?php echo $bank->getcountry_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $bank->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $bank->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $bank->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $bank->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $bank->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('broadcast/edit?id='.$bank->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('broadcast/index') ?>">List</a>
