<div id='theMid'>
  <?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead(__('Broadcast Message List')); ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
          <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
          <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th><?php echo __('Broadcast Message Title'); ?></th>
      <th width="70x"><?php echo __('Status'); ?></th>
      <th width="90px">Action</th>
    </tr>
  </thead>
  <tbody>
     <?php
     if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
    ?>
    <tr class="alternateBgColour">
      <td><?php echo $i ?></td>
      <td align="center"><?php echo $result->getTitle() ?></td>
      <td align="center">
      <?php 
      
//      echo $result->getExp_date();
//      echo date('Y-m-d');
//      
      
      if($result->getIs_active()=='1'){
              $use = '0'; 
              $message = 'Are you sure, you want to deactivate this message?';
              echo link_to(' ', 'broadcast/activateMessage?messageId='.$result->getId().'&use='.$use, array('method' => 'head', 'confirm' => __($message), 'class' => 'deactiveInfo', 'title' => 'De-Activate')) ;
           }elseif($result['is_active']=='0'){
            if($result->getExp_date() >= date('Y-m-d')){ 
                $use = '1'; 
                $message = 'Are you sure, you want to activate this message?';
            }else{
                $message = '';
                $use = '2';
            }
              echo link_to(' ', 'broadcast/activateMessage?messageId='.$result->getId().'&use='.$use, array('method' => 'head', 'confirm' => __($message), 'class' => 'activeInfo', 'title' => 'Activate'));
            }
            if(strtotime($result->getExp_date())< strtotime(date("Y-m-d")))
            {
                echo "&nbsp;<i>Expired</i>";
            }
            ?>
      </td>
      <td align="center">
      <?php echo link_to(' ', 'broadcast/permission?id='.$result->getId(), array('method' => 'get', 'class' => 'userAddInfo', 'title' => 'User Permission')) ?>
      <?php echo link_to(' ', 'broadcast/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
      <?php echo link_to(' ', 'broadcast/delete?id='.$result->getId(), array('method' => 'delete', 'confirm' => __('Are you sure, you want to delete this message?'), 'class' => 'delInfo', 'title' => 'Delete')) ?></td>
    </tr>
    <?php endforeach;?>

        <tr><td colspan="5">
<div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

  </div></td></tr>
    <?php }else{ ?>
     <tr><td  align='center' class='error' colspan="5"><?php echo __('No Broadcast Message found'); ?></td></tr>
    <?php } ?>

  </tbody>

</table>
</div>
  <!--<a href="<?php //echo url_for('broadcast/new') ?>">Add New Broadcast Message</a>-->
  <?php  echo button_to(__('Add New Broadcast Message'),'',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('broadcast/new').'\''));?>
</div>