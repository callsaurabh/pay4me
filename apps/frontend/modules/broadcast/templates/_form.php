<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<div class="wrapForm2">
<form action="<?php echo url_for('broadcast/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm" id="pfm_broadcast_form" name="pfm_broadcast_form">
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>



        <?php echo $form ?>
        <div class="divBlock">
            <center>
                &nbsp;<input type="submit" value=<?php echo __("Save"); ?> class="formSubmit" />
                <?php  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('broadcast/index').'\''));?>
    
        </center></div>


</form>
</div>
