<?php
echo use_helper('I18N');
// echo use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form'));
include_stylesheets_for_form($form);
include_javascripts_for_form($form);
?>
<script  type="text/javascript">
    $(document).ready(function(){

        $("#premission_field_bank_id").change(function(){
            var checkId = $("#premission_field_bank_id").val();
           
            $.ajax({
                url: "<?php echo url_for('broadcast/userRole'); ?>/checkId/"+checkId,
                success: function(oData){
                    if(oData == 1){
                        $("#premission_field_bank_id").attr('selectedIndex','0')
                        alert('You can not select eWallet and Bank at a time');
                    }else {
                        $("#premission_field_specific_to").html(oData);
                    }
                }
            });
        });
        $('#premission_field_specific_to').change(function(){

       
            var selected_values=String($('#premission_field_specific_to').val()).split(",");
 
            if($.inArray('all_role', selected_values) > -1 && selected_values.length>1) {
                $('#premission_field_specific_to').val('all_role');
                alert("<All Roles> doesn't need any other role Selection");
            }



           
            //alert($('#premission_field_specific_to').val());


        });

       
        $("#updatePermission").submit(function(){
            var answer = confirm ('<?php echo __("It will overwrite selected bank broadcast messages settings with new changes. Do you really want to make this change?") ?>');
            if (answer) {
                return true;
            }else{
                return false
            }
        });
    });
</script>
<div class="loadArea">
    <?php echo ePortal_legend(__('Update Broadcast Message')); ?>
    <div id="preloader"  style="display: none;background-color: #EFEFEF;">&nbsp;&nbsp;Loading...</div>
    <div class="wrapForm2">
        <?php echo form_tag('broadcast/permission?id=' . $message_id, 'name="updatePermission" id="updatePermission" class="dlForm"'); ?>
        <?php echo $form ?>
        <div class="divBlock">
            <center>
                &nbsp;<input type="submit" value=<?php echo __("Save"); ?> class="formSubmit" />
                <?php echo button_to(__('Cancel'), '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('broadcast/index') . '\'')); ?>

            </center>
        </div>

        <div class="passPolicy">
            <h3>Note:</h3>
            <ul>
                <li>Only bank or eWallet can be selected for broadcast message.</li>
                <li>eWallet will be applicable to all eWallet Users. Select '-- All eWallet Users-- ' from Bank Name List / eWallet List and select '-- All eWallet Users-- ' from User Role List .</li>
            </ul>
        </div>
    </div>

</div>
<script type="text/javascript">
    /*$(document).ready(function(){
        $("#premission_field_specific_to").change(function(){
            var userRole = $("#premission_field_specific_to").find(':selected').val();
            if(userRole=='ewallet_user'){
               $("#premission_field_bank_id").attr("selectedIndex",'1');
               var selIndex = document.getElementById('premission_field_specific_to').selectedIndex;
               $("#premission_field_specific_to").attr('selectedIndex','')
               if(selIndex == 1){
                    $("#premission_field_specific_to").attr('selectedIndex','1')
               }else{
                    $("#premission_field_specific_to").attr('selectedIndex','0')
               }
            }
        });
    });*/
</script>
