<?php //use_helper('Form');?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
  
  <?php echo ePortal_legend(__('View Profile'));?>
    <div id="bank_user">
    <?php echo formRowComplete(__('Username'),$user_detalis[0]['username'],'','username','','username_row'); ?>
    <?php echo formRowComplete(__('Name'),$name,'','username','','username_row'); ?>
    <?php echo formRowComplete(__('Date of Birth'),$dob,'','username','','username_row'); ?>
    <?php echo formRowComplete(__('Address'),nl2br($address),'','username','','username_row'); ?>
    <?php echo formRowComplete(__('Email'),$email,'','username','','username_row'); ?>
    <?php echo formRowComplete(__('Mobile Number'),$mobile_no,'','username','','username_row'); ?>
    </div>

    <div class="divBlock">
      <center id="multiFormNav">
        <?php echo button_to(__('Edit Profile'),'',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('userAdmin/editProfile').'\'')); ?>
      </center>
    </div>
 
  <div id="search_results"></div>
</div>