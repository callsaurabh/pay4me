<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php use_stylesheet('/css/sfPasswordStrengthStyle.css'); ?>
<script>
 function call()
 {
    $('form').submit(function() {
         $("form .formSubmit").attr('disabled','disabled');
    });
  document.forms[0].title.disabled = false;
  return false;
 }
</script>
<?php if(isset($firstLogin)): ?>
<?php echo ePortal_highlight('In order to continue please change your password and update your profile.','Welcome',array('class'=>'descriptionArea'));?>
<div class="wrapForm2">
<form method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>


<? else : ?>
<form action="<?php echo url_for('userAdmin/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php endif; ?>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
     
    <?php echo $form ?>  
  
    <div class="divBlock">
      <center id="multiFormNav">
              <?php //echo button_to('Cancel','userAdmin/index') ?>
              <input type="submit" class="formSubmit" value="<?php echo ($form->getObject()->isNew() ? 'Create New User' : 'Update User') ?>" onclick="call();"/>
      </center>
    </div>
        
    <?php include_partial('global/passwordPolicy') ?>
    <?php
        /*echo observe_field('sf_guard_user_password', array(
                  'update'   => 'strength',
                  'url'      => 'userAdmin/calculateStrength',
                  'with'     => "$('sf_guard_user_password').serialize() ",
                  'script'   => true,
                  'frequency'=> 0.5,
        ));*/
     ?>

        <script>

  $(document).ready(function()
 {
   var url ='<?php echo url_for('signup/calculateStrength');?>';
   calculatePasswordStrength('sf_guard_user_password', url);
   $('#sf_guard_user_password').after(addVirtualCheckBox('<span style="padding-left:10px;">','</span>'));
   enableMultiPasswordKeypad('sf_guard_user_password','sf_guard_user_password',url);
});

</script>

   <?php echo $form->renderGlobalErrors(); ?>
</form>
</div>
