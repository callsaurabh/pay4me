<?php
//use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<div class="wrapForm2"> <?php echo ePortal_legend("Update Security Answers Section"); ?>
<div id="bank_user">
    <div class="divBlock">
        <?php
        $submitUrl = 'userAdmin/UserPasswordConfirm';
        $openIdDetails = sfContext::getInstance()->getUser()->getAttribute('openId');
        if(count($openIdDetails) > 0){
            $submitUrl = 'userAdmin/UserQuestionAnswerUpdateForOpenid';
        }
        if (!empty($ansCount)) {

            for ($i = 1; $i <= $ansCount; $i++) {
                echo formRowComplete($form['security_question_text_' . $i]->renderLabel(), 
                $form['security_question_text_' . $i]->render());
                echo formRowComplete($form['answer_' . $i]->renderLabel(), '******');
            }
            $linkName = "Change Security Answers";
        } else {
            ?>
            <div class="descriptionArea">
                <br>
                <?php
                echo "You didn't have Save Security Question with Pay4Me Account.";
                $linkName = "Update Security Answers";
                ?>
            </div>
            <?php
        }
        ?>
        <center id="multiFormNav">
          <?php echo button_to($linkName, url_for($submitUrl),
          array('title' => "Change Security Answers",'class'=>'formSubmit')) ?>
        </center>
    </div>
</div>
</div>



