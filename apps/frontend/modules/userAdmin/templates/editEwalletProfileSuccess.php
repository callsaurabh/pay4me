<?php
//use_helper('Form');
use_helper('sfCryptoCaptcha');
echo ePortal_pagehead(" ", array('class' => '_form'));
?>

<div class="wrapForm2">

    <?php echo form_tag($sf_context->getModuleName() . '/update', array('name' => 'pfm_edit_user_form', 'id' => 'pfm_edit_user_form', 'onSubmit' => 'return validateUserForm("1")')); ?>
    <!--<div id='flash_error' class='error_list' style="visibility:hidden;height:1px;overflow-y:hidden"></div>-->
<?php //echo $ewProfileEditForm; ?>
        <?php echo ePortal_legend(__('Edit Profile')); ?>
    <div id="bank_user">
<?php include_partial('editEwalletUser', array('form' => $ewProfileEditForm, 'edit' => true, 'display' => false, 'userId' => $userId)) ?>


       
    </div>


       <dl id='username_row'>
            <div class="dsTitle4Fields"><?php echo $ewProfileEditForm['captcha']->renderLabel();  ?></div>
        <div class="dsInfo4Fields">
           <?php echo $ewProfileEditForm['captcha']->render();  ?><br><br>
            <img alt="Captcha Image" src="" id="captcha_img">
<?php //echo captcha_image();
            echo captcha_reload_button(); ?>

<!--            <span style='margin-left:7px;'> <a href='' onClick='return false' title='Reload image' id='rotate'><img name='captchaImg' id='captchaImg' src='' onClick='this.src="<?php //echo url_for('@sf_captcha')?>?r=" + Math.random() + "&reload=1"'></a></span>&nbsp;-->
     
<?php //  CR045//echo "Verification Code is case sensitive";  ?>
            <div id="err_vocde" class="cRed"></div>
        </div>
    </dl>

    <div class="divBlock">
        <center id="multiFormNav">
<input type="submit" name="commit" value="Update" class="formSubmit">
&nbsp;<?php echo button_to(__('Cancel'), '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('userAdmin/viewEwalletProfile') . '\'')); ?>
        </center>

    </div>
    <br/>
<!--    <div class="feedbackForm">
        <div class="message" style="color:red;text-align:center;font-size:10px;">[#] This functionality is currently not available</div>
    </div>-->
    <br/>
</div>

<div id="search_results"></div>
</form>

<script>
    var url_captcha = "<?php echo url_for('captcha/') ?>" + Math.round(Math.random(0)*1000)+1+'';
    function validateUserForm(submit_form)
    {

      var err  = 0;
        if($('#name').val() == "")
        {
            $('#err_name').html("Please enter First Name");
            err = err+1;
        }
        else
        {
            $('#err_name').html("");
        }
        if($('#name').val() != ""){
            if(validateString($('#name').val()))
            {
                $('#err_name').html("<?php echo __('Please enter Valid First Name') ?>");
                err = err+1;
            }
            else
            {
                $('#err_name').html("");
            }
        }
        if($('#lname').val() == "")
        {
            $('#err_lastname').html("<?php echo __('Please enter Last Name') ?>");
            err = err+1;
        }
        else
        {
            $('#err_lastname').html("");
        }
        if($('#address').val() == "")
        {
            $('#err_address').html("<?php echo __('Please enter Address') ?>");
            err = err+1;
        }
        else
        {
            $('#err_address').html("");
        }
        if($('#lname').val() != ""){
            if(validateString($('#lname').val()))
            {
                $('#err_lastname').html("<?php echo __('Please enter Valid Last Name') ?>");
                err = err+1;
            }
            else
            {
                $('#err_lastname').html("");
            }
        }
        if($('#dob_date').val() == "")
        {
            $('#err_dob').html("<?php echo __('Please enter Date of Birth') ?>");
            err = err+1;
        }
        else
        {
            $('#err_dob').html("");
        }
        if($('#dob_date').val() != ""){

            if (compare_date($('#dob_date').val())) {
                $('#err_dob').html("");
            }
            else{
                $('#err_dob').html("<?php echo __("Date of Birth cannot be future or today's date") ?>");
                err = err+1;
            }
        }
        if($('#email').length > 0){
            if($('#email').val() == "")
            {
                err = err+1;
            }
            else
            {
                $('#err_email').html("");
            }
            if($('#email').val() != ""){
                if(!validateEmail($('#email').val()))
                {
                    $('#err_email').html("<?php echo __("Please enter Valid Email") ?>");
                    err = err+1;
                }
                else
                {
                    $('#err_email').html("");
                }
            }
        }
        if($('#address').val() != "")
        {
            if(validateAddress($('#address').val()))
            {
                $('#err_address').html(invalid_addr_msg);
                err = err+1;
        
            }
            else
            {
                $('#err_address').html("");
            }
        }
        if(jQuery.trim($('#mobileno').val()) == "")
        {
            if(validatePhone($('#mobileno').val()))
            {
                $('#err_mobile_no').html("<?php echo __("Please enter Mobile number") ?>");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }
        if($('#mobileno').val() != "")
        {

    
            if(validatePhone($('#mobileno').val()))
            {
                $('#err_mobile_no').html("<?php echo __("Please enter Valid Mobile number") ?>");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }
   
        //if($('input[name="sms"]:checked').val() == "workphone" || $('input[name="sms"]:checked').val() == "both"){
//            if(jQuery.trim($('#workphone').val()) == "")
//            {
//                $('#err_work_phone').html("<?php //echo __("Please enter Work Phone") ?>");
//                err = err+1;
//            }
//            else
//            {
//                $('#err_work_phone').html("");
//            }
//        }
//        else{
//            $('#err_work_phone').html("");
//
//
//        }
        if(jQuery.trim($('#workphone').val()) != "")
        {
            if(validatePhone($('#workphone').val()))
            {
                $('#err_work_phone').html("<?php echo __("Please enter valid Work Phone") ?>");
                err = err+1;
            }
            else
            {
                $('#err_work_phone').html("");
            }
        }
        if(jQuery.trim($('#captcha').val()) == "")
        {
            $('#err_vocde').html("<?php echo __("Please enter Verification Code") ?>");
            err = err+1;
        }
        else
        {
            $('#err_vocde').html("");
        }

        if(err != 0)
        {
            $('#flash_error').html('');
            $('#captcha').val('');
            document.getElementById('captcha_img').src = "<?php echo url_for('@sf_captcha') ?>?r=" + Math.random() + "&reload=1";
            return false;
            
        }
    }

    function validateString(str) {
        var reg = /^([A-Za-z0-9])+$/;
        if(reg.test(str) == false) {
            return true;
        }
        return false;
    }
    function validateEmail(email) {
        var reg = /^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$/;

        if(reg.test(email) == true) {
            return true;
        }

        return false;
    }
    function cngCaptcha()
    {
        $('#captcha_img').attr('src',url_captcha);
    }
    window.onload = function() {
        cngCaptcha();
    }
</script>