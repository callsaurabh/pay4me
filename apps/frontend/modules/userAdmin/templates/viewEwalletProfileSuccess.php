<?php //use_helper('Form'); ?>
<?php use_helper('ePortal'); ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form'));?>

<?php
//    use_stylesheet('default');
//     if ($sf_user->hasFlash('notice')){
//        echo "<div id='flash_notice' class='error_list' ><span>".sfContext::getInstance()->getUser()->getFlash('notice')."</span></div>";
//    }
?>


<div class="wrapForm2 wrapForm-outer">
  
  <?php echo ePortal_legend(__('View Profile'));?>
    
    
    <?php echo formRowComplete(__('First Name'),$name,'','','','username_row'); ?>
    <?php echo formRowComplete(__('Last Name'),$lastname,'','','','username_row'); ?>
    <?php echo formRowComplete(__('Username'),$user_detalis[0]['username'],'','','','username_row'); ?>
    <?php echo formRowComplete(__('Date of Birth'),$b_place,'','','','username_row'); ?>
    <?php echo formRowComplete(__('Address'),nl2br($address),'','','','username_row'); ?>
    <?php echo formRowComplete(__('Email'),$email,'','','','username_row'); ?>
    <?php 
//      if($send_sms == 'gsmno'){
//         echo formRowComplete(__('Send SMS'),$mobile_no,'','','','username_row');
//      }else if($send_sms == 'workphone'){
//         echo formRowComplete(__('Send SMS'),$work_phone,'','','','username_row');
//      }else if($send_sms == 'both'){
//         echo formRowComplete(__('Send SMS'),$mobile_no.' Work Phone - '.$work_phone,'','','','username_row');
//      }
     ?>
   

   <?php echo formRowComplete(__('Mobile No.'),$mobile_no,'','','','username_row'); ?>

   <?php echo formRowComplete(__('Work Phone'),$work_phone,'','','','username_row'); ?>
    


    <div class="divBlock">
      <center id="multiFormNav">
        <?php echo button_to(__('Edit Profile'),'',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('userAdmin/editEwalletProfile').'\'')); ?>
      </center>
    </div>
 </div>
  
