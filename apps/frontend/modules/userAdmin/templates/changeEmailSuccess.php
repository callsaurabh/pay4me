<?php //use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<div class="wrapForm2">
<?php echo form_tag($sf_context->getModuleName().'/updateEmail',' class="dlForm multiForm" id="pfm_edit_email_form" name="pfm_edit_email_form"');?>

<div id='flash_error' class='error_list' style="visibility:hidden; display:none; height:1px;overflow-y:hidden"></div>

  <?php echo ePortal_legend('Change Email');?>
  
    <?php echo formRowComplete('Email<sup>*</sup>',"<input id='email' type='text' maxlength='50' size='30' value='' name='email'/>",'','email','err_email','username_row'); ?>

    
   <div class="divBlock">
    <center id="multiFormNav">
      <input type="hidden" name="userId" value="<?php echo $userId;?>" />
      <?php echo button_to('Update','',array('class'=>'formSubmit', 'onClick'=>'validateUserForm("1")')); ?>&nbsp;<?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('userAdmin/viewEwalletProfile').'\''));?>
    </center>
  </div>

<div id="search_results"></div>
</form>
</div>
<script>
  function validateUserForm(submit_form)
  {

    var err  = 0;
    if($('#email').val() == "")
    {
      $('#err_email').html("Please enter Email");
      err = err+1;
    }
    else
    {
      $('#err_email').html("");
    }
    if($('#email').val() != ""){
        if(!validateEmail($('#email').val()))
        {
          $('#err_email').html("Please enter Valid Email");
          err = err+1;
        }
        else
        {
          $('#err_email').html("");
        }
    }
    
   if(err == 0)
   {
      if(submit_form == 1)
      {
        $('#pfm_edit_email_form').submit();
      }
    }else{
        return false;
    }
  }
  function validateEmail(email) {
    var reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

    if(reg.test(email) == true) {
      return true;
    }

    return false;
  }
</script>