<?php echo ePortal_pagehead('UserAdmin List',array('class'=>'_form')); ?>


<div class="wrapTable" >
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th>Username</th>           
      <th>Is active</th>
      <th>Super admin</th>
      <th>Last login</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($sf_guard_user_list as $sf_guard_user): ?>
    <tr class="alternateBgColour">
      
      <!--<td><a href="<?php //echo url_for('userAdmin/editUser') ?>"><?php //echo $sf_guard_user->getusername() ?></a></td>-->
      <td><?php echo $sf_guard_user->getusername() ?></td>
      <td><?php echo $sf_guard_user->getis_active() ?></td>
      <td><?php echo $sf_guard_user->getis_super_admin() ?></td>
      <td><?php echo $sf_guard_user->getlast_login() ?></td>
      <td><?php echo $sf_guard_user->getcreated_at() ?></td>
      <td><?php echo $sf_guard_user->getupdated_at() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
  <a href="<?php echo url_for('userAdmin/new') ?>">Add New</a>&nbsp;&nbsp;&nbsp;
  <a href="<?php echo url_for('userAdmin/editUser') ?>">Edit</a>&nbsp;&nbsp;&nbsp;
 <a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a>