<?php //use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_stylesheet('/css/sfPasswordStrengthStyle.css'); ?>
<?php echo form_tag($sf_context->getModuleName().'/updateEwalletPassword',' class="dlForm multiForm" id="pfm_edit_user_form" name="pfm_edit_user_form"');?>
<script>

  $(document).ready(function()
 {
     $('#password').keypress(function(){

                url ='<?php echo url_for('signup/calculateStrength');?>';
                password=$('#password').val();
                $("#strength").load(url, {password: password,username:'',byPass:1 },function (data){
                            if(data=='logout'){
                                $("#strength").html('');
                                location.reload();
                              } });
      });
 });

</script>
 <div class="wrapForm2"> <?php //echo ePortal_legend('Change Password');?>
  <div id="bank_user">
      <?php //include_partial('formChange', array('form' => $form)) ?>
    <?php echo formRowComplete('Current password <font color="red">*</font>','<input type="password" name="oldpassword" maxlength="30" id="oldpassword" >','','name','err_oldpassword','username_row'); ?>
    <?php //echo formRowComplete('Current password <font color="red">*</font>',input_password_tag('oldpassword', '', 'maxlength=30'),'','name','err_oldpassword','username_row'); ?>

    <dl id='username_row'>
    <div class="dsTitle4Fields"><label for="name">New Password <font color="red">*</font></label></div>
    <div class="dsInfo4Fields">
        <div>
          <div style="float:left">
            <?php //echo input_password_tag('password', '', array('maxlength'=>30));?>
              <input type="password" name="password" maxlength="30" id="password" >
            <br/>            
            <div style="float:left">
            Password Strength
            <span id="strength" style="float:left"></span>
          </div>
          <br/>
            <div id="err_password" class="cRed"></div>
          </div>
          
        </div>
       <div class="desc">* Minimum 8 characters in length<br> * Must contain atleast 1 special, 1 numeric and 1 alphabetic character&nbsp;</div
        
      </div>
    </dl>

    <?php //echo formRowComplete('Confirm password <font color="red">*</font>',input_password_tag('cpassword', '', array('maxlength=>30')),'','username','err_cpassword','username_row'); ?>
    <?php echo formRowComplete('Confirm password <font color="red">*</font>','<input type="password" name="cpassword" maxlength="30" id="cpassword" >','','username','err_cpassword','username_row'); ?>

    
  <div class="divBlock">
    <center id="multiFormNav">
      <input type="hidden" name="i" value="<?php echo $i;?>" />
      <input type="hidden" name="t" value="<?php echo $t;?>" />
      <?php echo button_to('Change Password','',array('class'=>'formSubmit', 'onClick'=>'validateUserForm("1")')); ?>
    </center>
  </div>

<div id="search_results"></div>
</div>
<?php include_partial('global/passwordPolicy') ?>


</div>

<script>
  function validateUserForm(submit_form)
  {
    var err  = 0;
    
    if(jQuery.trim($('#oldpassword').val()) == "")
    {
      $('#err_oldpassword').html("Please enter Current Password");
      err = err+1;
    }
    else
    {
      $('#err_oldpassword').html("");
    }
    if($('#oldpassword').val() != "")
    {
        if(document.getElementById('oldpassword').value.length < 8){
            $('#err_oldpassword').html("Current Password should be at least 8 characters minimum.");
            err = err+1;
        }
     }
     
    if(jQuery.trim($('#password').val()) == "")
    {
      $('#err_password').html("Please enter New Password");
      err = err+1;
    }
    else
    {
      $('#err_password').html("");
    }
    if($('#password').val() != "")
    {
        if(document.getElementById('password').value.length < 8){
            $('#err_password').html("New Password should be at least 8 characters minimum.");
            err = err+1;
        }
     }

    if($('#oldpassword').val() != '' &&  $('#password').val() != '')
    {
        if($('#oldpassword').val() == $('#password').val())
        {
          $('#err_password').html("Your new password should not be same as your current password");
          err = err+1;
        }
//        else
//        {
//          $('#err_password').html("");
//        }
    }

    if(jQuery.trim($('#cpassword').val()) == "")
    {
      $('#err_cpassword').html("Please enter Confirm Password");
      err = err+1;
    }
    else
    {
      $('#err_cpassword').html("");
    }
    if($('#password').val() != '' &&  $('#cpassword').val() != '')
    {
        if($('#password').val() != $('#cpassword').val())
        {
          $('#err_cpassword').html("Your confirmed password does not match the entered new password");
          err = err+1;
        }
        else
        {
          $('#err_cpassword').html("");
        }
    }

   if(err == 0)
   {
      if(submit_form == 1)
      {
        $('#pfm_edit_user_form').submit();
      }
    }else{
        return false;
    }
  }
</script>