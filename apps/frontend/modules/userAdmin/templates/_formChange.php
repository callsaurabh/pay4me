<?php //include_stylesheets_for_form($form) ?>
<?php //include_javascripts_for_form($form) ?>
<?php //use_helper('Form') ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
    <form action="<?php echo url_for('userAdmin/savechange'); ?>"
          method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
        <?php if (!$form->getObject()->isNew()): ?>
            <input type="hidden" name="sf_method" value="put" />
        <?php endif; ?>
        <?php echo $form; ?>
            
        <div class="divBlock">
            <center>
                <input type="submit" value="<?php echo __("Change Password") ?>" class="formSubmit" />
            </center>
        </div>
        <?php include_partial('global/passwordPolicy') ?>
    </form>
</div>
<script>
    $(document).ready(function()
    {
       var url ='<?php echo url_for('signup/calculateStrength'); ?>';
       calculatePasswordStrength('sf_guard_user_password', url);
       $('#sf_guard_user_old_password').after(addVirtualCheckBox('<span style="padding-left:10px;">','</span>'));
       enableMultiPasswordKeypad('sf_guard_user_old_password','sf_guard_user_password',url);
    });    
</script>


