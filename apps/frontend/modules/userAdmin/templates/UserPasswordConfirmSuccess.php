<?php // use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form'));
echo form_tag($sf_context->getModuleName().'/UserPasswordConfirm','  id="pfm_edit_user_form" name="pfm_edit_user_form"');?>
 <div class="wrapForm2"> <?php echo ePortal_legend('Password verification');?>
    <dl id="">
         <div class="dsTitle4Fields"><label for="answerpassword_password"><?php echo $form['password']->renderLabel(); ?></label></div>
         <div class="dsInfo4Fields">
             <?php echo $form['password']->render(); ?>
             <div id="err_password"   class="error_listing cRed"><?php echo $form['password']->renderError(); ?></div>
         </div>
     </dl>
  <div class="divBlock">
    <center id="multiFormNav">
          <?php echo $form[$form->getCSRFFieldName()]->render(); ?>
          <input type="Submit" value='Continue' class="formSubmit">
          &nbsp;&nbsp;
         <?php  echo button_to('Cancel','',array('class'=>'formCancel',
          'onClick'=>'location.href=\''.url_for('userAdmin/UserQuestionAnswer').'\''));?>
    </center>
</div>
</form>
<div class="notebg">
    <b class="note">Note:</b>
    <div class="note-txt">
        <font color="red">Fields marked with (*) are mandatory</font>
    </div>
    <div class="note-txt">
        Enter your current login password here to update security answers.
    </div>
</div>
</div>


<script>
 $(document).ready(function()
 {
    var addheight = 20;
    enablePasswordKeypad('answerpassword_password','<br><div style="padding-top:7px;">','</div>',null,addheight);
 });

</script>

