<?php 
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/editProfile',' id="pfm_edit_user_form" name="pfm_edit_user_form"');?>
    <?php echo ePortal_legend(__('Edit Profile'));?>
    <div id="bank_user">
        <?php echo formRowComplete(__('Username') ,$user_detalis[0]['username'],'','username','','username_row'); ?>
        <?php echo formRowComplete($form['name']->renderLabel().'<sup><font color=red>*</font></sup>',
                $form['name']->render(),'','name','err_name','name_row',$form['name']->renderError(),'error_listing');     ?>
        <?php echo formRowComplete($form['dob']->renderLabel().'<sup><font color=red>*</font></sup>',$form['dob']->render(),

               '','dob','dob','dob_row', $form['dob']->renderError(),'error_listing'); ?>
        <?php echo formRowComplete($form['address']->renderLabel(),$form['address']->render(),'','username','err_address','username_row');     ?>
        <?php echo formRowComplete($form['email']->renderLabel().'<sup><font color=red>*</font></sup>',
                $form['email']->render()."<span id='domain_suffix'> $domain</span>"
                ,'','email','err_email','email_row',$form['email']->renderError(),'error_listing');     ?>
        <?php echo formRowComplete($form['mobile_no']->renderLabel().'<br>'.__('+(10 - 14 digits)'),
                $form['mobile_no']->render(),'','username','err_mobile_no','username_row',$form['mobile_no']->renderError(),'error_listing');     ?>
        <?php echo $form['domain']->render()?>
        <?php echo $form['userId']->render()?>
        <input type="hidden" name="domain" value="<?php echo $domain;?>" />
        <input type="hidden" name="userId" value="<?php echo $userId;?>" />
        <?php  if ($form->isCSRFProtected()) : ?>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php endif; ?>
        <div class="divBlock">
            <center>
                <input type = button name="Update" value="<?php echo __('Update')?>" class="formSubmit" onclick="validateUserForm(1)">
                <?php  echo button_to(__('Cancel'),'',array('class'=>'formCancel','onClick'=>'location.href=\''.url_for('userAdmin/viewProfile').'\''));?>
            </center>
        </div> 

        </div>
        </form>
    </div>
</div>
<script>
    function validateUserForm(submit_form)
    {     
        var err  = 0;
         if($('#edit_address').val() != "")
        {
            if(validateAddress($('#edit_address').val()))
            {
                $('#err_address').html("<br><br>"+invalid_addr_msg);
                err = err+1;

            }
            else
            {
                $('#err_address').html("");
            }
        }
         if($('#edit_address').val() != "")
        {
            if(validateAddress($('#edit_address').val()))
            {
                $('#err_address').html("<br><br>"+invalid_addr_msg);
                err = err+1;

            }
            else
            {
                $('#err_address').html("");
            }
        }
        if(err == 0)
        {
            if(submit_form == 1)
            {
                $('#pfm_edit_user_form').submit();
            }
        }
    }
    function validateEmail(email) {
        var reg = /^([A-Za-z0-9_\-\.])+$/;

        if(reg.test(email) == false) {
            return true;
        }

        return false;
    }


</script>