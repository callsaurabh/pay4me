<div class="notebg">
    <b class="note">Note:</b>
     <div class="note-txt">
         <font color="red">Fields marked with (*) are mandatory</font>
    </div>
    <div class="note-txt">
       Make sure your answer is easy for you to remember but hard for others to guess.
    </div>
</div>
