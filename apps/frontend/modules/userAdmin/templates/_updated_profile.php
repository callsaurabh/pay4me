<?php if($html):?>
Dear <?php echo $username ; ?>!
<br /><br />
You've successfully updated profile details with eWallet.<br /><br />
The updated profile details are as follows.<br /><br />
<?php if($firstname != ''){ ?>
    <b>First Name -:</b><?php echo $firstname ; ?>  <br />
<?php } 
if($lastname != ''){
?>
    <b>Last Name -:</b><?php echo $lastname ; ?>  <br />
<?php
}

if($dob != ''){
?>
    <b>Date of Birth  -:</b><?php echo $dob ; ?>  <br />
<?php
}


if($address != ''){
?>
    <b>Address -:</b><?php echo $address ; ?>  <br /><br />
<?php
}
if($send_sms != ''){
?>
    <b>Send SMS  -:</b> <b><?php echo $send_sms ; ?> </b>
    <?php if($mobileno != '')?>
        <?php echo $mobileno ; ?>
    <?php ?>
    <?php if($workphone != '')?>
        <?php echo $workphone ; ?>
    <?php ?>
    <br />

<?php }
if($mobileno != ''){
?>
    <b>Mobile No. -:</b><?php echo $mobileno ; ?>  <br /><br />
<?php
}
if($workphone != ''){
?>
<b>Workphone No. -:</b><?php echo $workphone ; ?>  <br /><br />
<?php } ?>

<br />
Please <a href="<?php echo $url;?>">click here</a> if you have not requested / carried out these change and immediately change your password.
<br /><br />
<span style="color:red;">
<br />
PLEASE BE ADVISED THAT YOUR PAY4ME EWALLET PASSWORD IS PERSONAL TO YOU AND MUST BE KEPT SECRET AND CONFIDENTIAL AT ALL TIMES AND SHOULD NOT BE DISCLOSED TO ANYONE. YOU SHOULD NOT SEND ANY COMMUNICATION, REQUEST OR QUESTION, WHICH INCLUDES YOUR EWALLET PASSWORD, TO ANY THIRD PARTY, INCLUDING PAY4ME STAFF.
<br /><br />
PAY4ME WILL NEVER SEND YOU ANY COMMUNICATION, WHETHER VIA EMAIL OR BY ANY OTHER MEANS, REQUESTING YOUR EWALLET PASSWORD.  IF YOU RECEIVE SUCH A REQUEST PLEASE DO NOT RESPOND AND DISCARD IT IMMEDIATELY AS IT IS FRAUDULENT.
<br /><br />
YOU SHALL BE RESPONSIBLE FOR MAINTAINING THE CONFIDENTIALITY OF YOUR INFORMATION AND PASSWORD. ALL USE OF THIS PASSWORD, WHETHER AUTHORIZED OR NOT, IS YOUR RESPONSIBILITY AND YOU AGREE TO IMMEDIATELY NOTIFY PAY4ME IF YOU DISCOVER THAT YOUR PASSWORD HAS BEEN COMPROMISED.
<br /><br />
PAY4ME SHALL NOT BE RESPONSIBLE OR LIABLE FOR ERRONEOUS TRANSACTIONS MADE BY YOU OR FOR THE MISUSE OF YOUR ACCOUNT ARISING FROM ANY WRONGFUL, INADVERTENT OR OTHER KIND OF DISCLOSURE OF YOUR PASSWORD TO A THIRD PARTY, INCLUDING PAY4ME STAFF. YOU WARRANT THAT YOU SHALL INDEMNIFY AND HOLD PAY4ME AS WELL AS ITS AGENTS AND ASSIGNS, HARMLESS FROM ANY LOSSES, COSTS, SUITS, DAMAGES, LIABILITIES AND EXPENSES RESULTING FROM YOUR FAILURE TO KEEP YOUR PASSWORD CONFIDENTIAL.
<br /><br />
</span>

<?php echo $signature;?>



<?php else:?>

Dear <?php echo $username ; ?>!
<br /><br />
You've successfully updated profile details with eWallet.<br /><br />
The updated profile details are as follows.<br /><br />
<?php if($firstname != ''){ ?>
    <b>First Name -:</b> <?php echo $firstname ; ?>  <br /><br />
<?php }
if($lastname != ''){
?>
    <b>Last Name -:</b> <?php echo $lastname ; ?>  <br /><br />
<?php
}
if($address != ''){
?>
    <b>Address -:</b> <?php echo $address ; ?>  <br /><br />
<?php
}
if($send_sms != ''){
?>
    <b>Send SMS  -:</b> <b><?php echo $send_sms ; ?> </b>
    <?php if($mobileno != '')?>
        <?php echo $mobileno ; ?>
    <?php ?>
    <?php if($workphone != '')?>
        <?php echo $workphone ; ?>
    <?php ?>
    <br />
    
<?php } ?> <br />
Please <a href="<?php echo $url;?>">click here</a> if you have not requested / carried out these change and immediately change your password.
<br /><br />
<span style="color:red;">
<br />
PLEASE BE ADVISED THAT YOUR PAY4ME EWALLET PASSWORD IS PERSONAL TO YOU AND MUST BE KEPT SECRET AND CONFIDENTIAL AT ALL TIMES AND SHOULD NOT BE DISCLOSED TO ANYONE. YOU SHOULD NOT SEND ANY COMMUNICATION, REQUEST OR QUESTION, WHICH INCLUDES YOUR EWALLET PASSWORD, TO ANY THIRD PARTY, INCLUDING PAY4ME STAFF.
<br /><br />
PAY4ME WILL NEVER SEND YOU ANY COMMUNICATION, WHETHER VIA EMAIL OR BY ANY OTHER MEANS, REQUESTING YOUR EWALLET PASSWORD.  IF YOU RECEIVE SUCH A REQUEST PLEASE DO NOT RESPOND AND DISCARD IT IMMEDIATELY AS IT IS FRAUDULENT.
<br /><br />
YOU SHALL BE RESPONSIBLE FOR MAINTAINING THE CONFIDENTIALITY OF YOUR INFORMATION AND PASSWORD. ALL USE OF THIS PASSWORD, WHETHER AUTHORIZED OR NOT, IS YOUR RESPONSIBILITY AND YOU AGREE TO IMMEDIATELY NOTIFY PAY4ME IF YOU DISCOVER THAT YOUR PASSWORD HAS BEEN COMPROMISED.
<br /><br />
PAY4ME SHALL NOT BE RESPONSIBLE OR LIABLE FOR ERRONEOUS TRANSACTIONS MADE BY YOU OR FOR THE MISUSE OF YOUR ACCOUNT ARISING FROM ANY WRONGFUL, INADVERTENT OR OTHER KIND OF DISCLOSURE OF YOUR PASSWORD TO A THIRD PARTY, INCLUDING PAY4ME STAFF. YOU WARRANT THAT YOU SHALL INDEMNIFY AND HOLD PAY4ME AS WELL AS ITS AGENTS AND ASSIGNS, HARMLESS FROM ANY LOSSES, COSTS, SUITS, DAMAGES, LIABILITIES AND EXPENSES RESULTING FROM YOUR FAILURE TO KEEP YOUR PASSWORD CONFIDENTIAL.
<br /><br />
</span>

<?php echo $signature;?><br /><br />

    
<?php endif;?>

