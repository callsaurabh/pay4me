<?php //use_helper('Form'); ?>
<?php echo ePortal_pagehead('Assign Roles to Portal User',array('class'=>'_form')); ?>

<div class="wrapForm2">
<form action="<?php echo url_for('userAdmin/searchUser'); ?>" method="post">
  
    <?php echo ePortal_legend("Search For Username"); ?>
      <dl>
          <div class="dsTitle4Fields"><label>User Name <sup>*</sup>:</label></div>
          <div class="dsInfo4Fields"><?//php echo input_tag('sf_guard_user[username]', '') ?>
          <input type="text" name="sf_guard_user[username]">
          &nbsp;<input type="submit" name="Submit" value="Search User"></div>
      </dl>
  

<?php if(isset($user_role)) { ?>
  
    <?php echo ePortal_legend("User Details"); ?>
    <?php echo formRowFormatRaw('Full Name:',ePortal_displayName($user_role[0]['UserDetails']['title'],$user_role[0]['UserDetails']['first_name'],@$user_role[0]['UserDetails']['middle_name'],$user_role[0]['UserDetails']['last_name'])); ?>
    <?php echo formRowFormatRaw('Username:',$user_role[0]['username']); ?>
    <?php echo formRowFormatRaw('UserId:',$user_role[0]['id']); ?>
    <?php echo formRowFormatRaw('Email:',$user_role[0]['UserDetails']['email']); ?>
</form>
</div>

<div class="wrapForm2">
<form action="<?php echo url_for('userAdmin/updateRoles') ?>" method="post" id="multiForm" >
  <?php echo $form ?>  

<div class="divBlock">
  <center id="multiFormNav">
    <input type="submit" id="multiFormSubmit" value="Assign Roles" class="formSubmit" />
  </center>
 </div>
</form>
</div>
<?php } ?>
