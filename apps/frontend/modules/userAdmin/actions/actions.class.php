<?php

/**
 * userAdmin actions.
 *
 * @package    symfony
 * @subpackage userAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class userAdminActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->sf_guard_user_list = Doctrine::getTable('sfGuardUser')
                        ->createQuery('a')
                        ->execute();
    }

    public function executeChangePassword(sfWebRequest $request) {
        if ($this->getUser()->isFirstLogin($this->getUser()->getGuardUser())) {
            $this->firstLoginUser = 'yes';
        } else {
            $this->firstLoginUser = 'no';
        }

        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());

        $this->setTemplate('change');
    }

    public function executeCalculateStrength() {

        $params = $this->getRequestParameter('sf_guard_user');
        $password = $params['password'];
        $strength = PasswordStrength::calculate($password);
        return $this->renderText(PasswordStrength::getBar($strength));
    }

    //save the new password
    public function executeSavechange(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
        #set last login time
        //$this->form->setDefault('last_login',date('Y-m-d h:i:s'));

        if ($this->getUser()->isFirstLogin($this->getUser()->getGuardUser())) {
            $this->firstLoginUser = 'yes';
            $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRSTTIME_PASSWORD_CHANGE;
        } else {
            $this->firstLoginUser = 'no';
            $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_PASSWORD_CHANGE;
        }

        if ($this->processForm($request, $this->form, false)) {

            $userDetailsObj = $this->getUser()->getGuardUser();
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $userDetailsObj->getUsername(), $userDetailsObj->getId()));

            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_SECURITY,
                            EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE,
                            EpAuditEvent::getFomattedMessage($event_description, array('username' => $userDetailsObj->getUsername())),
                            $applicationArr);

            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

            $this->getUser()->setFlash('notice', "Password Changed Successfully", false);

            $this->forward($this->getModuleName(), 'changePassword');
        }

        $this->setTemplate('change');
    }

    protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true, $userId="") {//exit;
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $postArr = $request->getPostParameters(); //print_r($postArr);exit;
            if ($userId != "") {
                $sf_guard_user = Doctrine::getTable('sfGuardUser')->find($userId); //print $sf_guard_user->getId();
            } else {
                $sf_guard_user = $this->getUser()->getGuardUser();
            }
            //$sf_guard_user = $this->getUser()->getGuardUser();
            if (EpPasswordPolicyManager::checkPasswordComplexity($postArr['sf_guard_user']['password'])) {
                $resVal = EpPasswordPolicyManager::checkPreviousPasswords($sf_guard_user->getId(), $postArr['sf_guard_user']['password'], $sf_guard_user->getSalt(), $sf_guard_user->getAlgorithm());
                if ($resVal) {
                    $oldPasswordLimit = EpPasswordPolicyManager::getOldPasswordLimit();
                    $this->getUser()->setFlash('error', 'New password can not be same as last ' . $oldPasswordLimit . ' password.', true);
                    $this->redirect('@change_password');
                } else {
                    $form->save();
                    $sf_guard_user = $this->getUser()->getGuardUser();

                    EpPasswordPolicyManager::doChangePassword($sf_guard_user->getId(), $sf_guard_user->getPassword());
                }
            } else {
                $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.', true);
                $this->redirect('@change_password');
            }
            if ($redirect)
                $this->forward('userAdmin', 'edit');
            return true;
        }
        //    print "form is not valid";//exit;
        //sfContext::getInstance()->getLogger()->info("Form invalid");
        return false;
    }

    public function executeChangeFirstPassword(sfWebRequest $request) {
        //die('First Change Password');

        $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($user_id)), sprintf('Object sf_guard_user does not exist (%s).', array($user_id)));
        $this->form = new EditFirstProfileForm($sf_guard_user);
        $this->group_description = $this->getUser()->getGroups()->getFirst()->getDescription();
        if ($request->isMethod('put')) {

            $this->form->bind($request->getParameter($this->form->getName()));
            if ($this->form->isValid()) {
                $postArr = $request->getPostParameters();

                if (EpPasswordPolicyManager::checkPasswordComplexity($postArr['sf_guard_user']['password'])) {
                    $resVal = EpPasswordPolicyManager::checkPreviousPasswords($sf_guard_user->getId(), $postArr['sf_guard_user']['password'], $sf_guard_user->getSalt(), $sf_guard_user->getAlgorithm());
                    if ($resVal) {
                        $oldPasswordLimit = EpPasswordPolicyManager::getOldPasswordLimit();
                        $this->getUser()->setFlash('error', 'New password can not be same as last ' . $oldPasswordLimit . ' password.');
                        $this->redirect('@change_first_password');
                    } else {
                        $sf_guard_user = $this->form->save();

                        EpPasswordPolicyManager::doChangePassword($sf_guard_user->getId(), $sf_guard_user->getPassword());

                        $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRSTTIME_PASSWORD_CHANGE;
                        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $sf_guard_user->getUsername(), $sf_guard_user->getId()));

                        $eventHolder = new pay4meAuditEventHolder(
                                        EpAuditEvent::$CATEGORY_SECURITY,
                                        EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE,
                                        EpAuditEvent::getFomattedMessage($event_description, array('username' => $sf_guard_user->getUsername())),
                                        $applicationArr);

                        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

                        # should go to signout and ask user to relogin.
                        $this->getUser()->setFlash('notice', "Profile updated successfully! Kindly login to proceed.", true);
                        $this->forward('sfGuardAuth', 'signout');
                        return true;
                    }
                } else {
                    $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.', true);
                    $this->redirect('@change_first_password');
                }
            }
        }
        $this->form->setDefault('last_login', date('Y-m-d H:i:s'));
        $this->setVar('firstLogin', 'true');
        $this->setTemplate('edit');
    }

    public function executeViewProfile(sfWebRequest $request) {
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $this->user_detalis = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        if (count($this->user_detalis[0]['UserDetail']) > 0) {
            $this->userId = $this->user_detalis[0]['id'];
            $this->name = $this->user_detalis[0]['UserDetail'][0]['name'];
            if (($this->user_detalis[0]['UserDetail'][0]['dob'] != "") && ($this->user_detalis[0]['UserDetail'][0]['dob'] != "1970-01-01")) {
                $this->dob = date('d-m-Y', strtotime($this->user_detalis[0]['UserDetail'][0]['dob']));
            } else {
                $this->dob = "";
            }
            //      $this->brith_place = $this->user_detalis[0]['UserDetail'][0]['brith_place'];
            $this->address = $this->user_detalis[0]['UserDetail'][0]['address'];
            $this->mobile_no = $this->user_detalis[0]['UserDetail'][0]['mobile_no'];
            $this->email = $this->user_detalis[0]['UserDetail'][0]['email'];
        } else {
            $this->email = "";
            $this->userId = "";
            $this->name = "";
            $this->dob = "";
            //    $this->brith_place="";
            $this->address = "";
            $this->mobile_no = "";
        }
        $this->setTemplate('viewProfile');
    }

    public function executeEditProfile(sfWebRequest $request) {

        $this->form = new EditProfileForm();
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $this->user_detalis = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        //$grup = Doctrine::getTable('sfGuardUserGroup')->findGroupById($userId);
        if (count($this->user_detalis[0]['UserDetail']) > 0) {

            $this->bank_id = $this->user_detalis[0]['domain_name'];

            $full_email_split = explode('@', $this->user_detalis[0]['UserDetail'][0]['email']);
            $full_email = $full_email_split[0];
            if ($this->bank_id != '') {
                $domain = $this->bank_id;
            } else {
                $arr = $this->getUser()->getGroupName();
                $group_name = $arr->getName();
                if ($group_name == sfConfig::get('app_pfm_role_admin')) {
                    $domain = "@swglobal.com";
                    $this->bank_id = "@swglobal.com";
                } else {
                    $domain = "@" . $full_email_split[1];
                    $this->bank_id = $full_email_split[1];
                }
            }
            $this->email = $full_email;
            $this->name = $this->user_detalis[0]['UserDetail'][0]['name'];
            if ($this->user_detalis[0]['UserDetail'][0]['dob'] == '1970-01-01') {
                $this->dob = "";
            } else {
                $this->dob = $this->user_detalis[0]['UserDetail'][0]['dob'];
            }
            //      $this->brith_place = $this->user_detalis[0]['UserDetail'][0]['brith_place'];
            $this->address = $this->user_detalis[0]['UserDetail'][0]['address'];
            $this->mobile_no = $this->user_detalis[0]['UserDetail'][0]['mobile_no'];
        } else {
            $this->email = "";
            $this->name = "";
            $this->dob = "";
            //    $this->brith_place="";
            $this->address = "";
            $this->mobile_no = "";
        }

        $this->userId = $this->user_detalis[0]['id'];
        $this->domain = $domain;
        $this->form->setDefault('name', $this->name);
        $this->form->setDefault('address', $this->address);
        $this->form->setDefault('email', $this->email);
        $this->form->setDefault('dob', $this->dob);
        $this->form->setDefault('mobile_no', $this->mobile_no);
        $this->form->setDefault('domain', $this->domain);
        $this->form->setDefault('userId', $this->userId);
        $this->setTemplate('editProfile');
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('edit'));
            if ($this->form->isValid()) {

                $this->forward('userAdmin', 'modify');
            } else {
                echo $this->form->renderGlobalErrors();
            }
        }
    }

    protected function chk_username($username) {
        return $username_count = Doctrine::getTable('sfGuardUser')->createQuery('a')->where('username=?', $username)->count();
    }

//    public function executeModify(sfWebRequest $request) {
//        $this->forward404Unless($request->isMethod('post'));
//        $userId = $request->getPostParameter('userId');
//        $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userId);
//        $userDetailId = "";
//        if($userDetailObj->count()) {
//            $userDetailId = $userDetailObj->getFirst()->getId();
//        }
//        if($userDetailId!= "") {
//            $user_detail = Doctrine::getTable('UserDetail')->find(array($userDetailId));
//        }
//        else {
//            $user_detail = new UserDetail();
//        }
//        $user_detail->setUserId($userId);
//        $user_detail->setName(trim($request->getPostParameter('name')));
//        $user_detail->setDob(date('Y-m-d', strtotime(trim($request->getPostParameter('dob')))));
//        //   $user_detail->setBrithPlace($request->getPostParameter('b_place'));
//        $user_detail->setAddress(trim($request->getPostParameter('address')));
//        $user_detail->setEmail(trim($request->getPostParameter('email')).''.$request->getPostParameter('domain'));
//        $user_detail->setMobileNo(trim($request->getPostParameter('mobile_no')));
//        $user_detail->save();
//
//        $this->getUser()->setFlash('notice', sprintf('User Profile updated successfully'));
//        $this->redirect('userAdmin/viewProfile');
//
//    }
    public function executeModify(sfWebRequest $request) {
        $params = $request->getPostParameter('edit');
        $this->forward404Unless($request->isMethod('post'));
        $userId = $params['userId'];
        $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $userDetailId = "";
        if ($userDetailObj->count()) {
            $userDetailId = $userDetailObj->getFirst()->getId();
        }
        if ($userDetailId != "") {
            $user_detail = Doctrine::getTable('UserDetail')->find(array($userDetailId));
        } else {
            $user_detail = new UserDetail();
        }
        $user_detail->setUserId($userId);
        $user_detail->setName(trim($params['name']));
        $user_detail->setDob(date('Y-m-d', strtotime(trim($params['dob']))));
        //   $user_detail->setBrithPlace($request->getPostParameter('b_place'));
        $user_detail->setAddress(trim($params['address']));
        $user_detail->setEmail(trim($params['email']) . '' . $params['domain']);
        $user_detail->setMobileNo(trim($params['mobile_no']));
        $user_detail->save();
        $this->getUser()->setFlash('notice', sprintf('User Profile updated successfully'));
        $this->logProfileUpdate();
        $this->redirect('userAdmin/viewProfile');
    }

    public function executeViewEwalletProfile(sfWebRequest $request) {
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $this->user_detalis = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        if (count($this->user_detalis[0]['UserDetail']) > 0) {
            $this->userId = $this->user_detalis[0]['id'];
            $this->name = $this->user_detalis[0]['UserDetail'][0]['name'];
            $this->lastname = $this->user_detalis[0]['UserDetail'][0]['last_name'];
            $dob = $this->user_detalis[0]['UserDetail'][0]['dob'];
            if ($dob)
                $this->b_place = date('d-m-Y', strtotime(trim($dob)));
            else
                $this->b_place = '';
            $this->address = $this->user_detalis[0]['UserDetail'][0]['address'];
            $this->email = $this->user_detalis[0]['UserDetail'][0]['email'];
            $this->mobile_no = $this->user_detalis[0]['UserDetail'][0]['mobile_no'];
            $this->work_phone = $this->user_detalis[0]['UserDetail'][0]['work_phone'];
            $this->send_sms = $this->user_detalis[0]['UserDetail'][0]['send_sms'];
        } else {
            $this->userId = '';
            $this->name = '';
            $this->lastname = '';
            $this->address = '';
            $this->email = '';
            $this->mobile_no = '';
            $this->work_phone = '';
            $this->send_sms = '';
        }
        $this->setTemplate('viewEwalletProfile');
    }

    public function executeEditEwalletProfile(sfWebRequest $request) {
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $this->user_detalis = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        $this->userId = $this->user_detalis[0]['id'];

//echo "<pre>";
//print_r($this->user_detalis[0]['username']);
//exit;

        if (count($this->user_detalis[0]['UserDetail']) > 0) {

            if (trim($request->getPostParameter('name')) != "") {
                $this->name = $request->getPostParameter('name');
            } else {
                $this->name = $this->user_detalis[0]['UserDetail'][0]['name'];
            }
            if (trim($request->getPostParameter('lname')) != "") {
                $this->lastname = $request->getPostParameter('lname');
            } else {
                $this->lastname = $this->user_detalis[0]['UserDetail'][0]['last_name'];
            }
            if (trim($request->getPostParameter('dob')) != "") {
                $this->dob = date('Y-m-d', strtotime(trim($request->getPostParameter('dob_date'))));
            } else {
                $this->dob = $this->user_detalis[0]['UserDetail'][0]['dob'];
            }
            if (trim($request->getPostParameter('address')) != "") {
                $this->address = $request->getPostParameter('address');
            } else {
                $this->address = $this->user_detalis[0]['UserDetail'][0]['address'];
            }
            $this->email = $this->user_detalis[0]['UserDetail'][0]['email'];
            if (trim($request->getPostParameter('mobileno')) != "") {
                $this->mobile_no = $request->getPostParameter('mobileno');
            } else {
                $this->mobile_no = $this->user_detalis[0]['UserDetail'][0]['mobile_no'];
            }
            if (trim($request->getPostParameter('workphone')) != "") {
                $this->work_phone = $request->getPostParameter('workphone');
            } else {
                $this->work_phone = $this->user_detalis[0]['UserDetail'][0]['work_phone'];
            }
            if (trim($request->getPostParameter('sms')) != "") {
                $this->send_sms = $request->getPostParameter('sms');
            } else {
                $this->send_sms = $this->user_detalis[0]['UserDetail'][0]['send_sms'];
            }

            $this->ewProfileEditForm = new CustomEwalletEditUserForm(array('username' => $this->user_detalis[0]['username'], 'dob' => $this->dob, 'name' => $this->name, 'lname' => $this->lastname, 'address' => $this->address, 'mobileno' => $this->mobile_no, 'workphone' => $this->work_phone, 'email' => $this->email, 'sms' => $this->send_sms), array('userId' => $this->userId, 'display' => false, 'edit' => true), null);
        } else {
            $this->userId = '';
            $this->name = '';
            $this->lastname = '';
            $this->address = '';
            $this->email = '';
            $this->mobile_no = '';
            $this->work_phone = '';
            $this->send_sms = '';
        }
        $this->setTemplate('editEwalletProfile');
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));
        $userid = $this->getUser()->getGuardUser()->getId(); //$request->getPostParameter('userId');
        $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userid);
        $userDetailId = "";
        if ($userDetailObj->count()) {
            $userDetailId = $userDetailObj->getFirst()->getId();
        }
        if ($userDetailId != "") {
            if (!$this->verifyEditEwalletProfile($request)) {
                $this->forward('userAdmin', 'editEwalletProfile');
            }
            $user_detail = Doctrine::getTable('UserDetail')->find(array($userDetailId));

            $firstname = trim($request->getPostParameter('name'));
            $lname = trim($request->getPostParameter('lname'));
            $dob = trim($request->getPostParameter('dob_date'));
            $address = trim($request->getPostParameter('address'));
            $mobileno = trim($request->getPostParameter('mobileno'));
            $workphone = trim($request->getPostParameter('workphone'));
            // $send_sms = trim($request->getPostParameter('sms'));
            $send_sms = "gsmno";
            #Begin Code To send Notification mail with all the updated porfile data
            /*
              If the user has updated any data then and only then the mail will trigger with all his updated
              personal details.
             */
            $sendMail = false;
            $subject = "eWallet Notification Mail";
            $partialName = 'updated_profile';

            $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($userid));

            #Generate Activation Token For Activation Link
            $hashAlgo = sfConfig::get('app_user_token_hash_algo');
            $userPwdSalt = $sfGuardUserDetail->getSalt();
            $updatedAt = $sfGuardUserDetail->getUpdatedAt();

            $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);

            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $reportUrl = url_for('userAdmin/changeEwalletPassword', true) . '?i=' . $userid . '&t=' . $activationToken;

            if ($firstname == $user_detail->getName()) {
                $mailFname = '';
            } else {
                $sendMail = true;
                $mailFname = $firstname;
            }
            if ($lname == $user_detail->getLastName()) {
                $mailLname = '';
            } else {
                $sendMail = true;
                $mailLname = $lname;
            }
            $newdate = $this->changeDateFormatDDMMYYYYtoYYYYMMDD($dob);
            if ($newdate == $user_detail->getDob()) {
                $mailDob = '';
            } else {
                $sendMail = true;
                $mailDob = $dob;
            }

            if ($address == $user_detail->getAddress()) {
                $mailAddress = '';
            } else {
                $sendMail = true;
                $mailAddress = $address;
            }
            if ($send_sms == $user_detail->getSendSms()) {
                $mailsendSms = '';
            } else {
                $sendMail = true;
                if ($send_sms == 'gsmno') {
                    $mailsendSms = 'GSM No. - ';
                } elseif ($send_sms == 'workphone') {
                    $mailsendSms = 'Work Phone - ';
                } elseif ($send_sms == 'both') {
                    $mailsendSms = 'Both ';
                } else {
                    $mailsendSms = 'None ';
                }
            }
            if ($mobileno == $user_detail->getMobileNo()) {
                $mailmobileno = '';
            } else {
                $sendMail = true;
                $mailmobileno = $mobileno;
            }
            if ($workphone == $user_detail->getWorkPhone()) {
                $mailworkphone = '';
            } else {
                $sendMail = true;
                $mailworkphone = $workphone;
            }
            if ($sendMail) {
                $taskId = EpjobsContext::getInstance()->addJob('SendMailUpdatedProfileNotification', $this->moduleName . "/sendUpdateProfileEmail", array('userid' => $userid, 'subject' => $subject, 'partialName' => $partialName, 'firstname' => $mailFname, 'lastname' => $mailLname, 'address' => $mailAddress, 'send_sms' => $mailsendSms, 'mobileno' => $mailmobileno, 'workphone' => $mailworkphone, 'reportUrl' => $reportUrl, 'dob' => $mailDob));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            }
            #End Code To send Notification mail with all the updated porfile data
        } else {
            $user_detail = new UserDetail();
        }
        $user_detail->setName($firstname);
        $user_detail->setLastName($lname);
        $date = date('Y-m-d', strtotime($dob));
        $user_detail->setDob($date);
        $user_detail->setAddress($address);
        if ($send_sms == 'gsmno') {
            $user_detail->setSendSms($send_sms);
        } elseif ($send_sms == 'workphone') {
            $user_detail->setSendSms($send_sms);
        } elseif ($send_sms == 'both') {
            $user_detail->setSendSms($send_sms);
        } else {
            $user_detail->setSendSms($send_sms);
        }
        $user_detail->setMobileNo($mobileno);
        $user_detail->setWorkPhone($workphone);
        $user_detail->save();

        $this->getUser()->setFlash('notice', sprintf('User Profile updated successfully'));
        $this->logProfileUpdate();
        $this->redirect('userAdmin/viewEwalletProfile');
    }

    private function changeDateFormatDDMMYYYYtoYYYYMMDD($date) {
        $newDate = explode('-', $date);
        $newDate = $newDate['2'] . '-' . $newDate['1'] . '-' . $newDate['0'];
        return $newDate;
    }

    public function executeSendUpdateProfileEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $firstname = $request->getParameter('firstname');
        $lastname = $request->getParameter('lastname');
        $address = $request->getParameter('address');
        $send_sms = $request->getParameter('send_sms');
        $mobileno = $request->getParameter('mobileno');
        $workphone = $request->getParameter('workphone');
        $reportUrl = $request->getParameter('reportUrl');
        $dob = $request->getParameter('dob');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendUpdatedProfileNotificationEmail($userid, $subject, $firstname, $lastname, $address, $send_sms, $mobileno, $workphone, $partialName, $reportUrl, $dob);
        return $this->renderText($mailInfo);
    }

    public function executeChangeEmail(sfWebRequest $request) {

        # Initilize Variables
        $userId = $request->getParameter('userid');
        $emailExist = $request->getParameter('emailexist');

        $user_detalis = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        // Bug:39130
        if (($user_detalis[0]['UserDetail'][0]['ewallet_type'] == 'both') || ($user_detalis[0]['UserDetail'][0]['ewallet_type'] == 'both_pay4me')) {
            if ($emailExist != 1) {
                // to show Google/Yahoo/Facebook instead of OpenId word.
                $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
                $openIdAuth = $payForMeObj->openIdAuthType($user_detalis[0]['UserDetail'][0]['openid_auth']);
                $this->getUser()->setFlash('error', sprintf('On updation of this email, you will not be able to signin via '.$openIdAuth.' authentication.'));
            } else {
                $this->getUser()->setFlash('error', sprintf('E-mail Address already exists'));
            }
        }
        if (count($user_detalis[0]['UserDetail']) > 0) {
            $this->email = $user_detalis[0]['UserDetail'][0]['email'];
        } else {
            $this->email = "";
        }
        $this->userId = $userId;
    }

    public function executeUpdateEmail(sfWebRequest $request) {
        # Initilize Variables
        $userId = $request->getParameter('userId');
        $email = trim($request->getParameter('email'));
        $preEmail = '';

        # Check Whether Email Id is already exist or not
        $emailObj = Doctrine::getTable('UserDetail')->chkEmailId($email);

        if ($emailObj > 0) {
            $this->getUser()->setFlash('error', sprintf('E-mail Address already exists'));
            $this->redirect('userAdmin/changeEmail?userid=' . $userId . '&emailexist=' . true);
        }
        $userDetail = Doctrine::getTable('sfGuardUser')->find(array($userId));
        $detailObj = $userDetail->getUserDetail()->getFirst();

        $preEmail = $detailObj->getEmail();
        ;
        if ($email != $preEmail) {
            # Begin Code to update new email Address and deactivate the account.
            $detailObj->setEmail($email);
            if ((count(sfContext::getInstance()->getUser()->getAttribute('openId')) == 0)) {
                // detaching open id Authentication
                $detailObj->setEwalletType('ewallet');
                $detailObj->setOpenidAuth(NULL);
            }
            $userDetail->setIsActive(false);
            $userDetail->save();
            # End Code to update new email Address and deactivate the account.
            # Begin code to send an email to his old email address notifying this action (that he has updated his email address on pay4me wallet)

            $subject = "eWallet Notification Mail";
            $partialName = 'updated_email';
            $taskId = EpjobsContext::getInstance()->addJob('SendMailUpdateEmailAddressNotification', $this->moduleName . "/sendUpdateEmail", array('userid' => $userId, 'email' => $email, 'preEmail' => $preEmail, 'subject' => $subject, 'partialName' => $partialName));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

            # End code to send an email to his old email address notifying this action (that he has updated his email address on pay4me wallet)
            # Begin code to send an email to his new email address with a confirmation link. only after he clicks the link, the account will be activated with new email address.
            #Begin Code Generate Activation Token For Activation Link
            $hashAlgo = sfConfig::get('app_user_token_hash_algo');
            $userPwdSalt = $userDetail->getSalt();
            $updatedAt = $userDetail->getUpdatedAt();
            $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);
            #End Code Generate Activation Token For Activation Link

            $subject = "eWallet Confirmation Mail";
            $partialName = 'new_email_account_details';
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $activation_url = url_for('signup/activate', true) . '?i=' . $userId . '&t=' . $activationToken;

            $taskId = EpjobsContext::getInstance()->addJob('SendMailNewConfirmation', $this->moduleName . "/sendNewConfirmationEmail", array('userid' => $userId, 'subject' => $subject, 'partialName' => $partialName, 'activation_url' => $activation_url));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

            # End code to send an email to his new email address with a confirmation link. only after he clicks the link, the account will be activated with new email address.
            # User should go to signout and ask user to relogin.
            $this->getUser()->setFlash('noticeEmailUpdate', "yes", false);
            $this->forward('sfGuardAuth', 'signout');
        } else {
            $this->redirect('userAdmin/viewEwalletProfile');
        }
    }

    public function executeSendUpdateEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $email = $request->getParameter('email');
        $preEmail = $request->getParameter('preEmail');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $openIdType = $request->getParameter('openIdType') ? $request->getParameter('openIdType') : '';

        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendUpdatedEmailNotification($userid, $email, $preEmail, $subject, $partialName, $openIdType);
        return $this->renderText($mailInfo);
    }

    public function executeSendNewConfirmationEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $activation_url = $request->getParameter('activation_url');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendConfirmationEmail($userid, $subject, $partialName, $activation_url);
        return $this->renderText($mailInfo);
    }

    public function executeChangeEwalletPassword($request) {

        #Initialize Variables
//        $this->i = $request->getParameter('i');
//        $this->t = $request->getParameter('t');
        $this->i = 3572;
        $this->t = $request->getParameter('t');
        $isAuthenticated = sfContext::getInstance()->getUser()->isAuthenticated();
//        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
        if (!$isAuthenticated) {
            $this->setLayout('layout_popup');
        }
    }

    public function executeUpdateEwalletPassword(sfWebRequest $request) {

        if (!$this->ewalletPasswordValid($request)) {
            $this->redirect('userAdmin/changeEwalletPassword?i=' . $request->getParameter('i') . '&t=' . $request->getParameter('t'));
        }

        $oldpassword = $request->getParameter('oldpassword');
        $password = $request->getParameter('password');
        $cpassword = $request->getParameter('cpassword');
        if (EpPasswordPolicyManager::checkPasswordComplexity($password)) {
            $userid = $request->getParameter('i');
            $userToken = $request->getParameter('t');
            $user_details = Doctrine::getTable('sfGuardUser')->find(array($userid));
            $sfguardUserId = $user_details->getId();
            $userPwdSalt = $user_details->getSalt();
            $updatedAt = $user_details->getUpdatedAt();

            #Generate Activation Token For Activation Link
            $hashAlgo = sfConfig::get('app_user_token_hash_algo');
            $userPwdSalt = $user_details->getSalt();
            $updatedAt = $user_details->getUpdatedAt();

            $resVal = EpPasswordPolicyManager::checkPreviousPasswords($sfguardUserId, $password, $userPwdSalt, $user_details->getAlgorithm());
            if ($resVal) {
                $oldPasswordLimit = EpPasswordPolicyManager::getOldPasswordLimit();
                $this->getUser()->setFlash('error', 'New password can not be same as last ' . $oldPasswordLimit . ' password.', true);
                $this->redirect('userAdmin/changeEwalletPassword?i=' . $request->getParameter('i') . '&t=' . $request->getParameter('t'));
            } else {
                $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);

                if ($userid == $sfguardUserId && $userToken == $activationToken) {
                    $user_details->setPassword($password);
                    $user_details->save();
                    $sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($userid));
                    EpPasswordPolicyManager::doChangePassword($sf_guard_user->getId(), $sf_guard_user->getPassword());
                    $this->getUser()->setFlash('notice', sprintf('Your password updated successfully.'));
                    $this->redirect('@homepage');
                } else {
                    $this->redirect('@homepage');
                }
            }
        } else {
            $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.', true);
            $this->redirect('userAdmin/changeEwalletPassword?i=' . $request->getParameter('i') . '&t=' . $request->getParameter('t'));
        }
    }

    protected function ewalletPasswordValid($request) {



        $user = Doctrine::getTable('sfGuardUser')->findOneById($request->getParameter('i'));
        $msg = array();
        if (trim($request->getPostParameter('oldpassword')) == "") {
            $msg[] = 'Please enter Current Password';
        }
        if (!$user->checkPassword(trim($request->getPostParameter('oldpassword')))) {
            $msg[] = 'Current Password is Incorrect';
        }
        if (trim($request->getPostParameter('password')) == "") {
            $msg[] = 'Please enter New Password';
        }
        if (trim($request->getPostParameter('cpassword')) == "") {
            $msg[] = 'Please enter Confirm Password';
        }
        if (trim($request->getPostParameter('password')) != trim($request->getPostParameter('cpassword'))) {
            $msg[] = 'Your confirmed password does not match the entered password';
        }
        if (trim($request->getPostParameter('oldpassword')) == trim($request->getPostParameter('password'))) {
            $msg[] = 'Your current password should not match the new password';
        }
        if (count($msg) > 0) {
            $msg = implode($msg, ', ');
            $this->getUser()->setFlash('error', sprintf($msg));
            return false;
        }
        return true;
    }

    protected function verifyEditEwalletProfile($request) {
        $msg = array();
        if (trim($request->getPostParameter('name')) == "") {
            $msg[] = 'Please enter First Name';
        }
        if (trim($request->getPostParameter('lname')) == "") {
            $msg[] = 'Please enter Last Name';
        }
        if (trim($request->getPostParameter('username')) == "") {
            $msg[] = 'Please enter User Name ';
        }
        if (trim($request->getPostParameter('address')) != "") {
            $len = strlen(trim($request->getPostParameter('address')));
            if ($len > 255) {
                $msg[] = 'Please enter valid address';
            }
        }
        if (trim($request->getPostParameter('mobileno')) == "") {
            $msg[] = 'Please enter Mobile number';
        }
        if (trim($request->getPostParameter('sms')) == "gsmno" || trim($request->getPostParameter('sms')) == "both") {
            if (trim($request->getPostParameter('mobileno')) == "") {
                $msg[] = 'Please enter Mobile No.';
            }
            if (trim($request->getPostParameter('mobileno')) != "") {
                if (!preg_match("/^([+]{1})([0-9]{10,15})$/i", $request->getPostParameter('mobileno'))) {
                    $msg[] = 'Please enter Valid Mobile No.';
                }
            }
        }
        if (trim($request->getPostParameter('sms')) == "workphone" || trim($request->getPostParameter('sms')) == "both") {
            if (trim($request->getPostParameter('workphone')) == "") {
                $msg[] = 'Please enter Work Phone.';
            }
        }
        if (trim($request->getPostParameter('workphone')) != "") {
            if (!preg_match("/^([+]{1})([0-9]{10,15})$/i", $request->getPostParameter('workphone'))) {
                $msg[] = 'Please enter Valid Work Phone.';
            }
        }
        if (trim($request->getPostParameter('captcha')) == "") {
            $msg[] = 'Please enter Verification Code';
        }


        if (trim(strtolower($request->getPostParameter('captcha'))) != "") {
            $hashAlgo = sfConfig::get('app_sf_crypto_captcha_hash_algo');
            //[wp029][CR045][Bug Id 29399]srt to lower as we make it catpcha case insensitive
            $hashedPwd = hash($hashAlgo, strtolower($request->getPostParameter('captcha')));
            $getCaptcha = $this->getUser()->getAttribute('captcha_code', null, 'captcha');
//             if(trim($request->getPostParameter('captcha')) != $this->getUser()->getAttribute('captcha')) {
            if ($hashedPwd != $getCaptcha) {
                $msg[] = 'Verification Code doesn\'t match with entered code';
            }
        }
        if (count($msg) > 0) {
            $msg = implode($msg, ', ');
            $this->getUser()->setFlash('error', sprintf($msg));
            return false;
        }
        return true;
    }

    /*
     * execute Action : UserQuestionAnswer
     * WP047 - CR078
     * Date : 14-11-2011
     */

    public function executeUserQuestionAnswer(sfWebRequest $request) {
        $userId = $this->getUser()->getGuardUser()->getId();
        $this->ansCount = Doctrine::getTable('SecurityQuestionsAnswers')->getAnswerCount($userId);
        $this->form = new UpdateSecurityAnswerForm(array(), array('userId' => $userId, 'ansCount' => $this->ansCount));
    }

    /*
     * execute Action : UserPasswordConfirm
     * WP047 - CR078
     * Date : 14-11-2011
     */

    public function executeUserPasswordConfirm(sfWebRequest $request) {
        $this->form = new changeAnswerForm();
        $this->formValid = "";
        if ($request->isMethod('post')) {
            $arr = $request->getPostParameters();
            $this->form->bind($request->getParameter('answerpassword'));
            if ($this->form->isValid()) {
                $this->forward('userAdmin', 'UserQuestionAnswerUpdate');
            }
        }
    }

    /*
     * execute Action : UserQuestionAnswerUpdate
     * WP047 - CR078
     * Date : 14-11-2011
     */

    public function executeUserQuestionAnswerUpdate(sfWebRequest $request) {
        if ($request->isMethod('post')) {
            $arrPassword = $request->getPostParameters('answerpassword');
            if (!empty($arrPassword['answerpassword']['password'])) {
                $password = $arrPassword['answerpassword']['password'];
            } else {
                $password = '';
            }
            $userId = $this->getUser()->getGuardUser()->getId();
            $username = $this->getUser();
            if ($username->checkPassword($password) || $request->isMethod('post')) {
                $this->ansCount = Doctrine::getTable('SecurityQuestionsAnswers')->getAnswerCount($userId);
                if (!empty($this->ansCount)) {
                    $this->loopCount = $this->ansCount;
                } else {
                    $this->loopCount = Settings::getNumberOfQuestion();
                }
                $this->form = new UpdateSecurityAnswerForm(array(), array('userId' => $userId, 'ansCount' => $this->ansCount));
                $this->formValid = "";
                if ($request->isMethod('post') && empty($password)) {
                    $this->form->bind($request->getParameter('security_questions_answers'));
                    if ($this->form->isValid()) {
                        $this->formValid = "valid";
                        $this->processFormQuestionAnswerUpdate($request, $this->form);
                    }
                }
            } else {
                $this->redirect('userAdmin/UserQuestionAnswer');
            }
        } else {
            $this->redirect('userAdmin/UserQuestionAnswer');
        }
    }

    /*
     * execute Action : UserQuestionAnswerUpdateForOpenid
     * WP066 - Openid
     * Date : 30-05-2013
     */

    public function executeUserQuestionAnswerUpdateForOpenid(sfWebRequest $request) {
        $this->setTemplate('UserQuestionAnswerUpdate');
        $password = '';
        $userId = $this->getUser()->getGuardUser()->getId();
        $username = $this->getUser();

        $this->ansCount = Doctrine::getTable('SecurityQuestionsAnswers')->getAnswerCount($userId);
        if (!empty($this->ansCount)) {
            $this->loopCount = $this->ansCount;
        } else {
            $this->loopCount = Settings::getNumberOfQuestion();
        }
        $this->form = new UpdateSecurityAnswerForm(array(), array('userId' => $userId, 'ansCount' => $this->ansCount));
        $this->formValid = "";
        if ($request->isMethod('post') && empty($password)) {
            $this->form->bind($request->getParameter('security_questions_answers'));
            if ($this->form->isValid()) {
                $this->formValid = "valid";
                $this->processFormQuestionAnswerUpdate($request, $this->form);
            }
        }
    }

    /*
     * function processFormQuestionAnswerUpdate()
     * param  : sfWebRequest,sfForm
     * WP047 : CR078
     * Date : 15-11-2011
     */

    protected function processFormQuestionAnswerUpdate(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $arrayParameter = $request->getPostParameters('security_questions_answers');
            $userId = $this->getUser()->getGuardUser()->getId();
            $username = $this->getUser();
            $ansCount = Doctrine::getTable('SecurityQuestionsAnswers')->getAnswerCount($userId);
            if (!empty($ansCount)) {
                for ($i = 1; $i <= $ansCount; $i++) {
                    Doctrine::getTable('SecurityQuestionsAnswers')->UpdateUserQustionAnswer(
                            $arrayParameter['security_questions_answers']['security_question_id_' . $i],
                            $userId, $arrayParameter['security_questions_answers']['answer_' . $i]);
                }
            } else {
                for ($i = 1; $i <= Settings::getNumberOfQuestion(); $i++) {
                    $security_question = new SecurityQuestionsAnswers();
                    $security_question->setUserId($userId);
                    $security_question->setSecurityQuestionId($arrayParameter['security_questions_answers']['security_question_id_' . $i]);
                    $security_question->setAnswer($arrayParameter['security_questions_answers']['answer_' . $i]);
                    $security_question->save();
                }
            }
            $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_ANSWER_CHANGE;
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $username, $userId));
            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_SECURITY,
                            EpAuditEvent::$SUBCATEGORY_SECURITY_USER_QUESTION_ANSWER,
                            EpAuditEvent::getFomattedMessage($event_description, array('username' => $username)),
                            $applicationArr);
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            $this->sendmailforupdateSecurityQuestion($username, $userId, date('Y-m-d H:i:s'));
            $this->getUser()->setFlash('notice', sprintf("Security Answers updated successfully"));
        }
        $this->redirect('userAdmin/UserQuestionAnswer');
    }

    /*
     * function sendmailforupdateSecurityQuestion()
     * param  : $username,$userId,$date
     * WP047 : CR078
     * Date : 15-11-2011
     */

    private function sendmailforupdateSecurityQuestion($username, $userId, $date) {
        $subject = "Your Pay4me  account information has changed";
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $taskId = EpjobsContext::getInstance()->addJob('SendMailUpdateSecurityAnswer',
                        "email/securityquestionanswerupdate", array('username' => $username,
                    'userid' => $userId, 'date' => $date,
                    'subject' => $subject));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');
    }

    private function logProfileUpdate() {
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PROFILE_UPDATED;

        $userName = $this->getUser()->getGuardUser()->getUsername();
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_PROFILE_UPDATED,
                        EpAuditEvent::getFomattedMessage($msg, array('username' => $userName)),
                        null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

}
