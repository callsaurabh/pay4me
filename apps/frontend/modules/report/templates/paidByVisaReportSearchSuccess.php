<?php use_helper('Pagination');  ?>
<table><tr><td>&nbsp;</td></tr></table>
 <?php
      if(isset($paymentmode)){
          $paymentmode=$paymentmode;
      } else {
          $paymentmode="";
      }
      $payType = $sf_request->getParameter('paidwithvisa');
      $type = $payType['pay_type'];
      $isPending = $payType['trans_type'];
      $pending = '';
      $pendingUrl = '';
      $width1 = 23;
      $width2 = 8;
      $pendingUrl = '&pending=all';
      if(strtolower($isPending) == 'pending')
      {
        $pending = 1;
        $width1 = 10;
        $width2 = 33;

      }
?>
<?php if(($isSuperadmin)&&($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('paidByVisaReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft"><?php echo __('')?>Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div>
        <div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
      </th>
    </tr>
  </table>
    <br>
   <?php if(strtolower($payType['pay_type']) == 'recharge'){?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <div id = "csvRecharegeDiv" style="cursor:pointer;" 
             onclick="javascript:progBarDiv('paidByVisaReportRechargeCsv', 'csvRecharegeDiv',
             'progBarDivRecharge')"><span  class="floatLeft"><?php echo __('')?>Click here to download data for Recharge through <?php echo $csvMaymentMode; ?> in <font color="red"><b><u>CSV</u></b></font> Format</span></div>
        <div id ="progBarDivRecharge"  style="display:none;"></div><br class="pixbr" />
      </th>
    </tr>
  </table> 
  <?php } ?>
  <br class="pixbr" />
</div>
<?php } ?>

<?php  if(strtolower($payType['pay_type']) == 'payment' && (strtolower($isPending) == 'pending' || strtolower($isPending) == 'all') && $isSuperadmin && $paymentmode==2):?>
<div>
    <div id="notice"><span class="cRed">NOTICE : </span>Please click on View to <b>Query Interswitch</b> for Pending Transactions.<br/>
       <?php if(strtolower($isPending) == 'all'): ?> Please click on Transaction Number to Print <b>Payment Receipt</b> for Successful Transactions.<?php endif; ?>
    </div><br/>
</div>
<?php endif; ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
        <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>


<div class="wrapTable" style="width:99%;overflow-x:auto; margin:auto;padding:2px;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <thead>
     
      <tr class="horizontal">
        <th width = "2%">S.No.</th>
        <?php if ($isSuperadmin) {?>
        <th width = "12%" align="center"><?php echo __('Username')?></th>
        <?php }?>
        <?php if($type == "recharge") { //fixing the review WP031?>
        <th width="17%" align="center"><?php echo __('Transaction Identification Number')?></th> <?php } else {?>
        <th width="17%" align="center"><?php echo __('Transaction Number')?></th>
        <?php }?>
         <?php if(strtolower($isPending) != 'pending') { ?>
        <th width = "17%" align="center"><?php echo __('Transaction Date')?></th>
        <?php } ?>
        <th width = "12%" align="center">Amount (<?php echo image_tag('/img/naira.gif'); ?>)</th>
        <th width = "<?php $width1?>%" align="center"><?php echo __('Status')?></th>
        <th width = "<?php $width2?>%"><?php echo __('View details')?></th>
      </tr>
    </thead>
    <tbody>
      <?php

      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;

        $transaction_date =$result->getTranstime();
        //Earlier $transaction_date = explode(" ",$result->getTranDateTime());
        $amount = format_amount($result->getPurchasedAmount(),'',1);

        ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <?php if ($isSuperadmin) {?>
        <td align="center"><?php echo $result->getUsername(); ?></td>
        <?php } ?>
        <?php if(strtolower($payType['pay_type']) == 'payment'){?>
        <td align="center"><?php
           if($result->getStatus()=="success"&& $isSuperadmin)  {
               echo link_to( $result->getAppId(), 'paymentSystem/PayOnline?txn_no='.$result->getAppId().'&template=layout_popup', array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes,scrollbars=yes') ,
              'target'=>'_blank') );
           }else{
              echo $result->getAppId(); 
           }
            ?></td>
        
        <?php } else{ ?>
        <td align="center">
            <?php
            if ($result->getStatus()=="success"&& $isSuperadmin && $result->getValidationNumber()) {//($result->getValidationNumber() ? base64_encode($result->getValidationNumber()) : 'BLANK')
              echo   link_to( $result->getOrderId(), 'ewallet/rechargeReceipt?validationNo='.base64_encode($result->getValidationNumber()).'&template=layout_popup', array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes,scrollbars=yes') ,
              'target'=>'_blank') );
            } else {
                 echo $result->getOrderId();
            }

            ?>
        </td>
        <?php } ?>
        <?php if(strtolower($isPending) != 'pending') { ?>
        <td align="center"><?php echo $result->getUpdatedAt();//echo ($pending)? $result->getUpdatedAt() : $transaction_date;?></td>
        <?php } ?>
        <td align="right"><?php  echo $amount; ?></td>
        
        <td align="center"><?php //echo ($pending)? $result->getStatus() : html_entity_decode($result->getOrderDesc());
        echo $result->getStatus() ?></td>
        <?php
        if(strtolower($payType['pay_type'])== 'payment'){?>
      <td align="center">
      <?php if($paymentmode!=""): ?>
      <?php echo link_to('View', 'ewallet/viewDetail?transId='.$result->getAppId().'&type=payment'.$pendingUrl.'&paymentmode='.$paymentmode.'&type=payment'.$pendingUrl, array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes,scrollbars=yes') ,
          'target'=>'_blank') ); ?>
      <?php else: ?>
      <?php echo link_to('View', 'ewallet/viewDetail?transId='.$result->getAppId().'&type=payment'.$pendingUrl, array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes,scrollbars=yes') ,
          'target'=>'_blank') ); ?>
      <?php endif; ?>
     
        </td>
        <?php }else{ ?>
       <td align="center"><?php echo link_to('View', 'ewallet/viewDetail?transId='.$result->getOrderId().'&paymentmode='.$paymentmode.'&type=recharge'.$pendingUrl, array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes,scrollbars=yes') ,
          'target'=>'_blank') ); ?>
    <?php if($result->getStatus()=='pending' && $isSuperadmin && $paymentmode==2){?>
          &nbsp;&nbsp;&nbsp; <?php echo link_to('Query Interswitch','interswitch_configuration/interswitchQueryStatus?orderId='.$result->getOrderId().'&appId='.$result->getAppId(), array('popup' => array('View Detail', 'width=900,height=720,left=150,top=100,resizable=yes,scrollbars=yes') ,
          'target'=>'_blank') ) ?>
      <?php } }?>

        </td>
      </tr>
      <?php endforeach; ?>

      <tr><td colspan="8">
          <div class="paging pagingFoot">
          <?php if($paymentmode!=""): ?>
          <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'search_results')   ?>
          <?php else: ?>
        <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'search_results')   ?>
          <?php endif; ?>
          </div>
      </td></tr>

        <?php }
      else { ?><table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
      <?php } ?>

    </tbody>

  </table>
</div>
<script>
    //function to update interswitch report status
    function updatedetails(){       
        $.post('paidByVisaReportSearch?page=<?php echo $page; ?>',$("#search").serialize(), function(data){
                if(data=='logout'){
                    location.reload();
                }
                else {
                    $("#search_results").html(data);                   
                }
            });
    }
  function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
      if(data=='logout'){
        $('#'+targetDivId).html('');
        location.reload();
      } });

  }
</script>