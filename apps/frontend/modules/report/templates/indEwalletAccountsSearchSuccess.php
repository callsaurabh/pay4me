<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


<?php echo ePortal_listinghead('Individual eWallet Account Details'); ?>  

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>


<div id="search_results">
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
        <tr class="horizontal">
            <th width = "2%">S.No.</th>
            <th>Date</th>
            <th>Particulars</th>       
            <th>Credit</th>       
            <th>Debit</th>                   
        </tr>
    </thead>
 <?php
    $from_date = $sf_request->getParameter('from_date') == '' ? "BLANK" : $sf_request->getParameter('from_date');
    $to_date = $sf_request->getParameter('to_date') == '' ? "BLANK" : $sf_request->getParameter('to_date');
    $entry_type = $sf_request->getParameter('entry_type') == '' ? "BLANK" : $sf_request->getParameter('entry_type');
    $totalCredit = '';
    $totalDebit = '';
    if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
?>
<?php
     // $totCredit=0;
     // $totDedit=0;
      $dateArr=explode('##',$dates);
      foreach ($pager->getResults() as $result):
        $i++;
       // print_r($result);
?>
    <tbody>
        <tr class="alternateBgColour">
            <td align="center"><?php echo $i ?></td>
            <td align="center"><?php echo $result->getDate();   ?></td>
            <td align="left"><?php echo html_entity_decode($result->getDescription());   ?></td>
           <?php
             if($result->getAmount() != '')
             {
                 if( "credit"==$result->getEntryType()){
                     $totalCredit+=$result->getAmount();
                 
           ?><td align="right">
           <?php
             echo format_amount($result->getAmount(),NULL,1);
             
            // $totCredit=$totCredit+$result->getAmount();
           ?>
           </td>
           <?php
             }
             else
             {
           ?>
           <td align="center">&nbsp;</td>
           <?php
             }
           ?>
           <?php
             if($result->getEntryType() == "debit" ){
                 $totalDebit+=$result->getAmount();
             
           ?>
           <td align="right">
           <?php
               echo format_amount($result->getAmount(),NULL,1);
              // $totDedit=$totDedit+$result->getAmount();
           ?></td>
           <?php
             }else{
           ?>
           <td align="center">&nbsp;</td>
           <?php
             }
            } // get amount
           ?>
        </tr>
         
         <?php endforeach;    ?>
         <tr>

           <td colspan="3" align="right" >Total</td>
           <td align="right"><?php echo format_amount($totalCredit,NULL,1); ?></td>
           <td align="right"><?php echo format_amount($totalDebit,NULL,1); ?></td>
        </tr>
        <tr>
         
           <td colspan="3" align="right" >Grand Total</td>
           <td align="right"><?php echo format_amount($creditAmt,NULL,1); ?></td>
           <td align="right"><?php echo format_amount($debitAmt,NULL,1); ?></td>
        </tr>
    </tbody>
    <tr>
        <td colspan="5">
          <div class="paging pagingFoot">
           <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date.'&entry_type='.$entry_type.'&account_no='.$sf_request->getParameter('account_no')), 'search_results') ?>
     <?php  //echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date.'&entry_type='.$entry_type.'&account_no='.$sf_request->getParameter('account_no')));?>
          </div>
        </td>
      </tr>

    <tfoot></tfoot>


    <?php
      }else {
    ?><table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
    <?php } ?>
  </table>
</div>
</div>

<script>
  
function slideDiv(acntId,frmdate,todate,entryType,divId){

   var url = "<?php echo url_for('report/getTransDetail'); ?>";
if($('#'+divId).css("display")=='none'){
    $('#'+divId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    //$('#'+divId).load(url, {itemId: itemId,transactionNumber:transactionNumber});

    $('#'+divId).load(url, {from_date: frmdate,to_date:todate,accoutnId:acntId,type:entryType}, function (data){
            if(data=='logout'){
                     location.reload();
                 }
          });

    $('#'+divId).slideDown();
    $('#'+divId).show('slow');


}else{
    $('#'+divId).slideUp();
}
}
</script>