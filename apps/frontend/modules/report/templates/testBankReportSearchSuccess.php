<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


<?php echo ePortal_listinghead('Bank Report'); ?>
<?php if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
      <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('TestBankReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
    </th>
    </tr>
  </table>
 <br class="pixbr" />
</div>
<?php } ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr align = "center" class="horizontal">
      <th width = "1%">Username</th>
      <th width = "18%">Name</th>
      <th width = "12%">Email Address</th>
      <th width = "18%">System Role</th>
      <th width = "18%">Branch Name</th>
      <th width = "16%">Branch Code</th>
      <th width = "18%">User Status</th>
      <th width = "30%">Last Login </th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo "------".$pager->getNbResults()."------------";
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
$originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      //print '<pre>'; print_r($pager->getResults()->toArray());die;
      $i = 0;
      foreach ($pager->getResults(Doctrine::HYDRATE_RECORD) as $result) {
        $d = $result->getSfGuardUser()->getUserDetail();
        $b = $result->getBank()->getBankBranch();
        $g = $result->getSfGuardUser()->getsfGuardUserGroup();
        $i++
        ?>
    <tr align = "center" class="alternateBgColour">
      <td><?php echo $result->getSfGuardUser()->getUserName(); ?></td>
      <td><?php echo $d[0]->getName(); ?></td>
      <td><?php echo $d[0]->getEmail(); ?></td>
      <td><?php echo $g[0]->getsfGuardGroup()->getDescription(); ?></td>
      <td><?php echo $b[$i]->getName(); ?></td>
      <td><?php if ($result->getBankBranch())
			echo $result->getBankBranch()->getBranchCode(); ?></td>
      <td><?php switch($d[0]->getUserStatus()){
      	          case 1: echo "Active";
      	            break;
      	          case 2: echo "Bank User Suspended";
      	            break;
      	          case 3: echo "Bank User Deactivate";
      	            break;
      	          case 4: echo "eWallet wait Admin";
      	            break;
      	          case 5: echo "eWallet Disapprove";
      	            break;
                } ?>
      </td>
      <td><?php echo $result->getSfGuardUser()->getLastLogin(); ?></td>
      <?php
      }
     Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);
     ?>

    </tr>
    <?php
    }else{ ?><table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
    <?php } ?>

  <tfoot>
  <tr><td colspan="10"><div class="paging pagingFoot floatRight"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td></tr>
   </tfoot></tbody>
</table>
</div>

<div class="paging pagingFoot"><center><?php  echo button_to('Cancel','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/testBankReport').'\''));?></center></div>
</div>
<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>
