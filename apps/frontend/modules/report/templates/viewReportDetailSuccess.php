<html>
  <head>
    <?php
    use_stylesheet('master.css');
    echo include_stylesheets();
    //use_stylesheet('reset.css');
    //use_stylesheet('default.css');
    //use_stylesheet('menu.css');
    //use_stylesheet('admin_main.css');
    //use_stylesheet('admin_theme.css');

    ?>
  </head>
  </body>
  <div id="mainPopupWraper">
    <div class="popupWrapForm2">
      <?php
      echo ePortal_legend('Report Details');
      if($reportType == "detailed"){
        echo formRowFormatRaw('Transaction Number',$result->getTransactionId()) ;
      }

      $serviceType = $result->getServiceType() == '' ? 'N/A': $result->getServiceType();
      echo formRowFormatRaw('Service Type',$serviceType);
      $appType = $result->getApplicationType() == '' ? 'N/A': $result->getApplicationType();
      echo formRowFormatRaw('Application Type',$appType);
      $paymentModeName = $result->getPaymentModeOptionId() == '' ? 'N/A': pfmHelper::getPMONameByPMId($result->getPaymentModeOptionId());
      echo formRowFormatRaw('Payment Mode',$paymentModeName);
      $bankName = $result->getBankName() == '' ? 'N/A': $result->getBankName();
      echo formRowFormatRaw('Bank Name',$bankName);
      $branchName = $result->getBankBranchName() == '' ? 'N/A': $result->getBankBranchName();
      echo formRowFormatRaw('Branch Name',$branchName);
      $branchCode = $result->getBankBranchCode() == '' ? 'N/A': $result->getBankBranchCode();
      echo formRowFormatRaw('Branch Code',$branchCode);
      echo formRowFormatRaw('Application Fee',format_amount($result->getItemFee(),$result->getCurrencyId()));

      if($reportType == 'summery') {
        $toServiceCharge = $result->getTotServiceCharge() == '' ? 'N/A': $result->getTotServiceCharge();
        echo formRowFormatRaw('Service Charge',format_amount($toServiceCharge,$result->getCurrencyId()));
        $toTransCharge = $result->getTotBankCharge() == '' ? 'N/A': $result->getTotBankCharge();
        echo formRowFormatRaw('Transaction Charge',format_amount($toTransCharge,$result->getCurrencyId()));
        echo formRowFormatRaw('No. of transactions',$result->getNoOfTransactions());
      }
      if($reportType == 'detailed') {
        $serviceCharge = $result->getServiceCharge() == '' ? 'N/A': $result->getServiceCharge();
        echo formRowFormatRaw('Service Charge',format_amount($serviceCharge,$result->getCurrencyId()));
        $transCharge = $result->getBankCharge() == '' ? 'N/A': $result->getBankCharge();
        echo formRowFormatRaw('Transaction Charge',format_amount($transCharge,$result->getCurrencyId()));
        echo formRowFormatRaw('Date of transaction',$result->getUpdatedAt());
      }
      echo formRowFormatRaw('Amount',format_amount($result->getAmount(),$result->getCurrencyId()));
      ?>
    </div>
  </div>
  </body>
</html>