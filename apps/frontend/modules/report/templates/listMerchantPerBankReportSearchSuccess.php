<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


<?php echo ePortal_listinghead('Merchants Per Banks Report'); ?>
<?php if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
      <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('ListMerchantPerBankReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
    </th>
    </tr>
  </table>
 <br class="pixbr" />
</div>
<?php } ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr align = "center" class="horizontal">      
                    <th width = "15%" style = "border-bottom:1px solid #ccc; border-left:1px solid #ccc; padding:4px; font-size:12px; text-align:left;">Merchant Name</th>
                    <th width = "15%" style = "border-bottom:1px solid #ccc; border-left:1px solid #ccc; padding:4px; font-size:12px; text-align:left;">Merchant Service</th>
                    <th width = "15%" style = "border-bottom:1px solid #ccc; border-left:1px solid #ccc; padding:4px; font-size:12px; text-align:left;">Collection Bank</th>
                    <th width = "15%" style = "border-bottom:1px solid #ccc; border-left:1px solid #ccc; padding:4px; font-size:12px; text-align:left;">Collection Account</th>
                    <th width = "15%" style = "border-bottom:1px solid #ccc; border-left:1px solid #ccc; padding:4px; font-size:12px; text-align:left;">Merchant Bank</th>
                    <th width = "15%" style = "border-bottom:1px solid #ccc; border-left:1px solid #ccc; padding:4px; font-size:12px; text-align:left;">Merchant Account</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo "------".$pager->getNbResults()."------------";die;
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
		$originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
		Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      $i = 0;
      foreach ($pager->getResults() as $result) {
		 // echo "<pre/>";
		  //print_r($result);die;
        ?>
    <tr align = "center" class="alternateBgColour">
      <td><?php if(isset($result['merchantName'])) {
      				echo $result['merchantName'];
      		} 
      ?></td>
      <td><?php if(isset($result['merchantService'])) { 
      			echo $result['merchantService']; 
      	}?></td>
      <td><?php  if(isset($result['collectionBankName'])) {  
      				echo $result['collectionBankName']; 
      		} ?></td>
      <td><?php  if(isset($result['collectionBankAccountNumber'])) {
      				echo $result['collectionBankAccountNumber']; 
      		}?></td>
      <td><?php if(isset($result['merchantBankName'])) { 
      				echo $result['merchantBankName'];
      		} ?></td>
      <td><?php if(isset($result['merchantBankAccountNumber'])) { 
      				echo $result['merchantBankAccountNumber'];
      	} ?></td>
      <?php
      }
     Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);
     ?>

    </tr>
    <?php
    }else{ ?><table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
    <?php } ?>

  <tfoot>
  <tr><td colspan="10"><div class="paging pagingFoot floatRight"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td></tr>
   </tfoot></tbody>
</table>
</div>
<div class="paging pagingFoot"><center><?php  echo button_to('Cancel','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/listMerchantPerBankReport').'\''));?></center></div>
</div>
<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}

</script>
