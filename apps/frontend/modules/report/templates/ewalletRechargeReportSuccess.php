<?php use_helper('ePortal'); ?>
<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<?php //use_helper('Form') ?>
<?php echo form_tag('report/ewalletRechargeReportSearch', array('name' => 'pfm_report_form', 'id' => 'pfm_report_form', 'onsubmit' => 'return checkDateDifference();')) ?>
<div class="wrapForm2">
    <?php
    echo ePortal_legend(__('eWallet Recharge Report'));
    ?>
    <div  id="noTextTransformation" class="flRight" style="font-size:14px;"> <a href="<?php echo url_for("ewalletRechargeReportDisplay"); ?>"> View Previously Generated Reports </a></div>

    <?php
    //include_partial('ewalletRechargeReport', array('form' => $form, 'userGroup' => $userGroup));
    if ($userGroup != sfConfig::get('app_pfm_role_bank_branch_user')) {
        echo formRowFormatRaw($form['user_name']->renderLabel(), $form['user_name']->render());
    }
    echo formRowFormatRaw($form['wallet_no']->renderLabel(), $form['wallet_no']->render());
    include_component('bank', 'list', array('bank' => 'banks','ewalletreport' =>'ewalletreport'));
    echo formRowFormatRaw($form['currency']->renderLabel(), $form['currency']->render());
    echo formRowFormatRaw($form['entry_type']->renderLabel(), $form['entry_type']->render());
    echo formRowFormatRaw($form['payment_mode']->renderLabel(), $form['payment_mode']->render());
    echo formRowComplete($form['from']->renderLabel(), $form['from']->render(), '', 'from', 'err_from_date', 'from', $form['from']->renderError());
    echo formRowComplete($form['to']->renderLabel(), $form['to']->render(), '', 'to', 'err_to_date', 'to', $form['to']->renderError());
    echo $form['_csrf_token']; ?>
    <div class="divBlock">
        <center id="multiFormNav">

            <?php //echo submit_tag(__('Make Report'), array('class' => 'formSubmit')); ?>
            <input type="submit" name="submit" value="Generate Report" class="formSubmit">
        </center>
    </div>
</div>

<script type="text/javascript">
    function getLoadCountry(bank_id){
        $.post('<?php echo url_for("epActionAuditEventReports/getBankCountry") ?>', {'bank': bank_id}, function (data) {
            $('#country_id').html(data);
            return false;
        });
    }
    function getCountry(){
        obj= $("#banks").val();
        getLoadCountry(obj);

    }
    function getbranches(){
        obj= $("#country_id").val();
        bank= $("#banks").val();
        getLoadBankBranches(obj, bank);

    }
    function getLoadBankBranches(country_id , bank_id){
        $.post('<?php echo url_for("epActionAuditEventReports/getBankBranches") ?>', {'bank': bank_id,'country':country_id}, function (data) {
            $('#bank_branches').html(data);
            return false;
        });
    }
    function checkDateDifference(){
        if(!$('#from_date').val()){
            $('#err_to_date').html("From Date is required");
            return false;
        }
        if(!$('#to_date').val()){
            $('#err_to_date').html("To Date is required");
            return false;
        }
        start_date = $('#from_date').val();
        start_date = new Date(start_date.split('-')[2],start_date.split('-')[1]-1,start_date.split('-')[0]);
        end_date = $('#to_date').val();
        end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);
        
        date_diff = (end_date.getTime() - start_date.getTime())/(3600*24000);
        
        //max 4 week
        if(date_diff <= 27){
            return validate_report_index_form(this);
        }else{
 
                $('#err_to_date').html('Custom report request can be placed for maximum 28 days. \n\
    \nTo view more than 28 days reports, go to "View Previously Generated Reports" section.');
            return false;
        }
    }
</script>
