<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>





<?php echo ePortal_listinghead('Merchant Account Record Details'); ?>
    
<?php
if($sf_request->getParameter('entry_type') != "" && $sf_request->getParameter('entry_type') == "debit"){
?>
<div align="center">No Record Found</div>

<?php }else{ ?>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
          <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
      </tr>
    </table>
  <br class="pixbr" />
</div>



<div class="wrapTable" >
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >


 <?php

        if(($pager->getNbResults())>0) {

             $entry_type = $sf_request->getParameter('entry_type') == "" ? "BLANK" : $sf_request->getParameter('entry_type');
             $from_date = $sf_request->getParameter('from_date') == "" ? "BLANK" : $sf_request->getParameter('from_date');
             $to_date = $sf_request->getParameter('to_date') == "" ? "BLANK" : $sf_request->getParameter('to_date');


            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
//            echo "<pre>";
//            print_r($pager->getResults());
//            die;
?>

  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Date</th>
      <th>Particulars</th>
      <th>Credit</th>
   </tr>
    </thead>


    <?php
            foreach ($pager->getResults() as $result):

                $i++;
////                echo "<pre>";
////                print_r($result->toArray());
////                die;
?>


    <tbody>


        <tr class="alternateBgColour">
            <td align="center"><?php echo $i ?></td>
            <td align="center"><?php echo $result->getDate() ?></td>
            <td align="left">Pay4Me Services NigeriaLimited</td>

            <?php
//                foreach($result->getFirst()->getEpMasterLedger() as $res) {
//              if($res->getEntryType() == "credit")
//                    {echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total Credits:</b> ".format_amount($res->getAmount(),1);}
//                else echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Total Debits: </b>".format_amount($res->getAmount(),1);}

            ?>

           <?php
            // foreach ($result->getEpMasterLedger() as $payDetails){


             if($result->getAmount() != '')
             {
?>
                 <td align="center"><?php
                         echo format_amount($result->getAmount(),NULL,1); ?>
                         </td>
                 

           <?php
                 } // get amount
            // }
           ?>

        </tr>



  <?php endforeach;    ?>
  </tbody>
    <tfoot><tr><td colspan="5">
    
    <div class="paging pagingFoot"><?php  //echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?batchIds='.$sf_request->getParameter('batchIds').'&scheduleId='.$sf_request->getParameter('scheduleId')), 'theMiddle')
//pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?designation_id='.$sf_request->getParameter('batchIds')))
echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date.'&entry_type='.$entry_type.'&master_account_id='.$sf_request->getParameter('master_account_id').'&merchant_id='.$sf_request->getParameter('merchant_id')));
?>

  </div>
    
    </td></tr></tfoot>

     <?php
    }else { ?>

        <tr><td  align='center' class='error' >No Record Found</td></tr>
        <?php } ?>

</table>
</div>

<?php } ?>