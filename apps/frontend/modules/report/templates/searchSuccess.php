<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
    <?php
        if($postDataArray['report_label'] == "summery" && $postDataArray['payment_status'] == ''){
            echo ePortal_listinghead('Summary Report -->  All Transactions');
        }else if($postDataArray['report_label'] == "summery" && $postDataArray['payment_status'] == 0){
            echo ePortal_listinghead('Summary Report -->  Successful Transactions');
        }else if($postDataArray['report_label'] == "summery" && $postDataArray['payment_status'] == 1){
            echo ePortal_listinghead('Summary Report -->  Unsuccessful Transactions');
        }

        if($postDataArray['report_label'] == "detailed" && $postDataArray['payment_status'] == ''){
            echo ePortal_listinghead('Detailed Report -->  All Transactions');
        }else if($postDataArray['report_label'] == "detailed" && $postDataArray['payment_status'] == 0){
            echo ePortal_listinghead('Detailed Report -->  Successful Transactions');
        }else if($postDataArray['report_label'] == "detailed" && $postDataArray['payment_status'] == 1){
            echo ePortal_listinghead('Detailed Report -->  Unsuccessful Transactions');
        }
    ?>
 <?php if (!$err) {?>

<?php if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <?php if(($userGroup == 'report_admin' || $userGroup == 'portal_admin') && ($postDataArray['report_label'] == "detailed") && ($postDataArray['payment_status'] == 0)){?>
        <span class="floatLeft" ><div id = "csvDiv1" style="cursor:pointer;" onclick="javascript:progBarDiv('report/branchReportCsv', 'csvDiv1', 'progBarDiv1')">Click here to download Report For All Processors of the bank in  <font color="red"><b><u>CSV</u></b></font> Format</div><div id ="progBarDiv1"  style="display:none;"></div></span>
        <span class="floatLeft">|</span>
<?php } ?>
        <span ><div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('report/csv', 'csvDiv', 'progBarDiv')">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</div><div id ="progBarDiv"  style="display:none;"></div></span>
      </th>
    </tr>
  </table>
</div>
<br class="pixbr" />
<?php } ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div align="center" style="overflow:auto;">

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "1%">S.No.</th>
      <?php if($postDataArray['report_label'] == "detailed"){ ?>
      <th >Transaction Number</th>
      <?php } ?>
      <th >Service Type</th>
      <th >Application Type</th>
      <?php if($postDataArray['payment_status'] == 0) { ?>
      <th >Bank Name</th>
      <th>Branch Name</th>
      <th >Branch Code</th>
      <th >Service Charge (<?php echo getCurrencyImage($postDataArray['currency_id']);?>)</th>
      <th >Transaction Charge (<?php echo getCurrencyImage($postDataArray['currency_id']);?>)</th>
      <?php }  ?>
      <?php if($postDataArray['report_label'] == 'summery') { ?>
      <th >No. of transactions</th>
      <?php }  
      if($postDataArray['report_label'] == 'detailed') { ?>
        <th >Date of transaction</th>
     <?php } ?>
      <th >Amount (<?php echo getCurrencyImage($postDataArray['currency_id']);?>)</th>
    </tr>
  </thead>
  <tbody>
    <?php 
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
      $TotalAmount = '';
      $TotalTransactions = '';
   $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
   Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      foreach ($pager->getResults() as $result) {
        $i++;
        ?>
    <tr class="alternateBgColour" align = "center">
      <td><?php echo $i ?></td>
      <?php if($postDataArray['report_label'] == "detailed"){ ?>
      <td><?php     
      echo (($result->getPaymentStatusCode() == 1) || $isBankUser )? $result->getTransactionId() : link_to(	$result->getTransactionId(), 'report/viewSplitDetail?transId='.$result->getTransactionId().'&id='.$result->getId(), array(
        'popup' => array('View Detail', 'width=900,height=500,left=150,top=50,resizable=yes,') ,'target'=>'_blank') );  ?></td>
      <?php } ?>
      <td><?php echo $result->getServiceType() ?></td>      
      <td>
        <?php 
        if($postDataArray['report_label'] == "detailed")
        { 
         echo link_to(	$result->getApplicationType(), 'report/viewReportDetail?transId='.$result->getTransactionId().'&id='.$result->getId().'&type='.$postDataArray['report_label'], array(
        'popup' => array('View Detail', 'width=700,height=500,left=150,top=50,resizable=yes') ,'target'=>'_blank') );
        }elseif($postDataArray['report_label'] == "summery")
        {
          echo $result->getApplicationType();
        }else{
         echo link_to(	$result->getApplicationType(), 'report/viewReportDetail?id='.$result->getId().'&type='.$postDataArray['report_label'], array(
        'popup' => array('View Detail', 'width=700,height=500,left=150,top=50,resizable=yes'),'target'=>'_blank' ) );
        }
        ?>
      </td>
      <?php if($postDataArray['payment_status'] == 0) { ?>
      <td><?php echo $result->getBankname() ?></td>
      <td><?php echo $result->getBankBranchName() ?></td>
      <td><?php echo $result->getBankBranchCode() ?></td>
      

      
      <?php }  ?>

      <?php
           if($postDataArray['payment_status'] == 0)
           {
           if($postDataArray['report_label'] == 'summery' )
               {
            ?>
                <td><?php echo $result->getTotServiceCharge() ?></td>
                <td><?php echo $result->getTotBankCharge() ?></td>
           <?php
                      }
            if($postDataArray['report_label'] == 'detailed'){
            ?>
            <td><?php echo $result->getServiceCharge() ?></td>
            <td><?php echo $result->getBankCharge() ?></td>
            
           <?php


            }
           }
                      ?>
        <?php if($postDataArray['report_label'] == 'summery') {  ?>
            <td><?php echo $result->getNoOfTransactions() ?></td>
          <?php } 
      if($postDataArray['report_label'] == 'detailed' ) { ?>
             <td><?php echo $result->getUpdatedAt();?></td>
     <?php
      }
     ?>
      <td>
    <?php 
            // Start Added by Iqbal at 13 Nov 2009 fixed bug 14634
            if($postDataArray['report_label'] == "summery" && $postDataArray['payment_status'] == '1') {
             echo "NA";
            } else {
                echo $result->getAmount();
            }
            // End Added by Iqbal at 13 Nov 2009
    ?>
      </td>
    </tr>
        <?php
             $TotalAmount = $TotalAmount + $result->getAmount();
            if($postDataArray['report_label'] == 'summery') {
               $TotalTransactions = $TotalTransactions + $result->getNoOfTransactions();
            }
        }
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute); 
        ?>
              <tr>
                <?php
                    if($postDataArray['report_label'] == 'detailed') {
                 ?>
                    <?php if($postDataArray['payment_status'] == 0) {  ?>

                        <th colspan="10" align="right">Total</th>
                    <?php }else{ ?>
                        <th colspan="5" align="right">Total</th>
                    <?php } ?>
                <?php } ?>
                <?php
                    if($postDataArray['report_label'] == 'summery') {
                 ?>
                    <?php if($postDataArray['payment_status'] == 0) {  ?>
                        <th colspan="8" align="right">Total</th>
                    <?php }else{ ?>
                        <th colspan="3" align="right">Total</th>
                    <?php } ?>
                    <td align="center"><?php  echo $TotalTransactions; ?></td>
              <?php } ?>
                    <td align="center">

                    <?php
                        if($postDataArray['payment_status'] == 0)
                              echo number_format($TotalAmount,2);
                        else
                                 echo "NA";?>
                    </td>
              </tr>
              
              <tr>
                <?php
                    if($postDataArray['report_label'] == 'detailed') {
                 ?>
                    <?php if($postDataArray['payment_status'] == 0) {  ?>
                        
                        <th colspan="10" align="right">Grand Total</th>
                    <?php }else{ ?>
                        <th colspan="5" align="right">Grand Total</th>
                    <?php } ?>
                <?php } ?>
                <?php
                    if($postDataArray['report_label'] == 'summery') {
                 ?>
                    <?php if($postDataArray['payment_status'] == 0) {  ?>
                        <th colspan="8" align="right">Grand Total</th>
                    <?php }else{ ?>
                        <th colspan="3" align="right">Grand Total</th>
                    <?php } ?>
                    <td align="center"><?php  echo $grandTotalTransactions; ?></td>
              <?php } ?>
                    <td colspan="1" align="center">
                    <?php
                        if($postDataArray['payment_status'] == 0)
                              echo number_format($grandTotalAmount,2);
                        else
                                 echo "NA";?>
                    </td>
                        
              </tr>
              <?php } else {?>     
                        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                        <tr><td colspan="11" class="error"><?php echo "No Record Found"; ?></td></tr>
                        </table>
              <?php } ?>

   <tr><td colspan="11">
<div class="paging pagingFoot floatRight"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td></tr>
 </tbody>
</table>
</div>
</div>


<div class="paging pagingFoot"><center><?php  

if($postDataArray['report_label'] == "detailed"){echo button_to('Back','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/index?label=detailed').'\''));}
if($postDataArray['report_label'] == "summery"){echo button_to('Back','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/index?label=summery').'\''));}
    
    ?></center></div>
</div>
<script>
    
function progBarDiv(url, referenceDivId, targetDivId){
    
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>
<?php
} else { echo ' <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
<tr><td  align="center" class="error">'.$err."</td></tr></table>";}   ?>