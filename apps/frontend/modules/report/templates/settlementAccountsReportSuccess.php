<?php  if($pager->getFirstIndice() == 1){ /* do nithing */ }else{ echo ePortal_pagehead(" ",array('class'=>'_form')); } ?>
<?php use_helper('Pagination');  ?>
<br />
<?php echo ePortal_listinghead('NIBSS Charge Details'); ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>


 
<div class="wrapTable" style="width:857px;overflow:auto">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_records_per_page');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
  ?>
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Date</th>
      <th>From Account</th>
      <th>To Account</th>
      <th>Credit</th>
      <th>From Bank</th>
      <th>To Bank</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach ($pager->getResults() as $result):
          $i++;
    ?>
    <tr class="alternateBgColour">
      <td align="center"><?php echo $i ?></td>
      <td align="center"><?php echo date('d-m-Y',strtotime($result->getUpdatedAt()));   ?></td>
      <td align="left"><?php echo $result->getFrmacntname();   ?></td>
      <td align="left"><?php echo $result->getToacntname();   ?></td>
      <td align="right"><?php echo format_amount($result->getToamt(),NULL,1);     ?></td>
      <td align="left"><?php  echo $result->getFromBank();   ?></td>
      <td align="left"><?php  echo $result->getToBankName();   ?></td>
    </tr>
    <?php endforeach;    ?>
  </tbody>
  <tfoot><tr><td colspan="10">
    <div class="paging pagingFoot">
      <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$sf_request->getParameter('from_date').'&to_date='.$sf_request->getParameter('to_date')));?>
    </div>
  </td></tr></tfoot>
  <?php
    }else { ?>
    
        <tr><td  align='center'  ><span class="error"><?php echo "No Record Found"; ?></span></td></tr>
  <?php } ?>
</table>
</div>