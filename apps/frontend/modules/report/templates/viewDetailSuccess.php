<?php use_stylesheet('master.css');
echo include_stylesheets(); ?>
<?php use_helper('Pagination');  ?>

<div>
  <div class="popupWrapForm2" style="background-color:#EFEFEF;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
          <th width="100%" >
            <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
            <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
          </th>
       </tr>
      <tbody>
        <?php
        $pending = $sf_request->getParameter('pending');
        if(($pager->getNbResults())>0) {
          $limit = 2;
          $i = max(($page-1),0)*$limit ;
          foreach ($pager->getResults() as $result):
          $i++;
          //        $transaction_date = $result->getTranstime();
          $amount = format_amount($result->getAmount(), 1, 1);
//          ($pending)? $result->getCreatedAt() : $result->getTransactionDate()
          ?>
        <tr>
          <td>
                <?php echo ePortal_legend("Record - $i");
                if($result->getTransactionDate())
                echo formRowFormatRaw('Transaction Date', $result->getTransactionDate()) ;
                echo formRowFormatRaw('Transaction Number',($result->getType()=='recharge')? $result->getOrderId() :$result->getAppId()) ;
                echo formRowFormatRaw('Username',$result->getUsername()) ;
                if($result->getPan())
                echo formRowFormatRaw('Card Number',$result->getPan()) ;
                echo formRowFormatRaw('Amount',$amount) ;
                if(!$pending)
                echo formRowFormatRaw('Response Code',$result->getResponseCode()) ;
                echo formRowFormatRaw('Status',$result->getStatus()) ;
                echo formRowFormatRaw('Details', ($pending=='failure')? 'This transaction is pending' : html_entity_decode($result->getResponseTxt())) ;
                if($result->getApprovalCode())
                echo formRowFormatRaw('Approval Code',$result->getApprovalCode()) ;
            ?>
          </td>
        </tr>
        <?php endforeach; ?>

        <?php } else { ?>
        <tr><td  align='center' class='error'>No Record Found</td></tr>
        <?php } ?>
      </tbody>
    </table>
  </div>

<?php if(($pager->getNbResults())>2) {?>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    
    <tr class="alternateBgColour">
      <th width="100%" >
        <div class="paging pagingFoot floatRight" style="background-color:#EFEFEF"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())."?transId=".$sf_request->getParameter('transId')."&type=".$sf_request->getParameter('type')."&pending=".$sf_request->getParameter('pending')) ?></div>
      </th>
    </tr>
  </table>
  <?php }?>
</div>