<?php use_helper('Pagination'); ?>
<script language="javascript">
<?php if (isset($msg)) { ?>
        $("#flash_notice").show();
        $("#flash_notice").html('<span> <?php echo $msg; ?> </span>');
    <?php
    unset($msg);
}
?>
</script>
<?php echo form_tag('', array('name' => 'report_listing_form', 'id' => 'report_listing_form')) ?>
<div>    
    <div id="flash_notice" class="error_list" style="display:none;"></div> 

    <h2> </h2>
    <div class="clear"></div>
    <div class=" listHead"> Reports</div>

    <div class="wrapTable">
        <table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr class="alternateBgColour">
                <th>
                    <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                    <?php if (($pager->getNbResults()) > 0) { ?>
                        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                    <?php } else { ?>
                        <span class="floatRight">Showing <b>0</b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                    <?php } ?>
                </th>
            </tr>
        </table>
        <br class="pixbr" />
    </div>

    <div class="wrapTable" style="overflow-x:auto;width:100%;">

        <table width="100%" border="0" cellpadding="0" cellspacing="0"
               class="dataTable">
                   <?php
                   if (($pager->getNbResults()) > 0) {
                       //echo '<table style="width:1920px" border="0" cellpadding="0" cellspacing="0" class="dataTable" >';
                       $limit = sfConfig::get('app_records_per_page');
                       $page = $sf_context->getRequest()->getParameter('page', 0);
                       $i = max(($page - 1), 0) * $limit;
                       ?>
                <thead>
                    <tr align="center" class="horizontal">
                        <?php
                        if ($type == "Generated") {
                            $pfmHelperObj = new pfmHelper();
                            $this->userGroup = $pfmHelperObj->getUserGroup();
                            if ($this->userGroup == "portal_admin" || $frequency == "CUSTOM") {
                                ?>
                                <th width="3%">
                                    <input type="checkbox"  name="checkAllAuto" id="checkAllAuto"  />
                                </th>
                                <?php
                            }
                        }
                        if ($type == "Pending") {
                            ?>                    
                            <th width="3%">
                                <input type="checkbox"  name="checkAllAuto" id="checkAllAuto"  />
                            </th>
                        <?php } 
                            if (isset($report)) {
                                if ($report == 'EwalletRechargeReport') {
                            ?>
                        <th width="10%">Bank Name</th>
                        <?php 
                                }
                                if($report == 'EwalletFinancialReport') {
                                    ?>
                        <th width="10%">Ewallet Account No</th>
                        <?php
                                }
                            }
                            ?>
                        <!--<th width="20%">Parameters</th>-->
                        <th width="10%">Type</th>
                        <th width="20%">From Date</th>
                        <th width="20%">To Date</th>
                        <?php
                        if ($type != "Pending") {
                            ?>
                            <th width="17%">Action</th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($pager->getResults() as $key => $value) {
                        ?>
                        <tr>
                            <?php
                            if ($type == "Generated") {
                                $pfmHelperObj = new pfmHelper();
                                $this->userGroup = $pfmHelperObj->getUserGroup();
                                if ($this->userGroup == "portal_admin" || $frequency == "CUSTOM") {
                                    ?>
                                    <td>             
                                        <input type="checkbox" name="chk[]"  value="<?php echo $value['id']; ?>" />
                                    </td>
                                    <?php
                                }
                            }
                            ?>
                            <?php
                            if ($type == "Pending") {
                                ?>
                                <td>
                                    <input type="checkbox" name="chk[]"  value="<?php echo $value['ep_job_id']; ?>" />
                                </td>
                                <?php
                            }
                            if (isset($report)) {                               
                            ?>
                            <td>
                                <?php
                                 if ($report == 'EwalletRechargeReport') {
                                    if (isset($value['Bank'][0]['bank_name'])) {
                                        echo $value['Bank'][0]['bank_name'];
                                    } else {
                                        echo "All Banks";
                                    }
                                 }
                                if($report == 'EwalletFinancialReport'){
                                    $temp = json_decode($value['parameters'],true);
                                    if( array_key_exists('account_no',$temp ) ){
                                        echo $temp['account_no'];
                                    }
                                }
                                ?>
                            </td>
                            <?php
                             }
                            ?>
<!-- Below code will work if client ask for parameters value in report --> 
                           <!--<td>-->
                            <?php
//                            if (isset($value['parameters'])) {
//                                $filters = json_decode($value["parameters"], true);
//                                if (isset($filters["user_name"]) && $filters["user_name"] != NULL) {
//                                    echo "Search By Bank Teller (Username) = " . $filters["user_name"] . "</br>";
//                                }
//                                if (isset($filters["wallet_no"]) && $filters["wallet_no"] != NULL) {
//                                    echo "eWallet Account No = " . $filters["wallet_no"] . "</br>";
//                                }
//                                if (isset($filters["banks"]) && $filters["banks"] != NULL) {
//                                    echo "Bank = " . $filters["banks"] . "</br>";
//                                }
//                                if (isset($filters["country"]) && $filters["country"] != NULL) {
//                                    echo "Country = " . $filters["country"] . "</br>";
//                                }
//                                if (isset($filters["branch"]) && $filters["branch"] != NULL) {
//                                    echo "Branch = " . $filters["branch"] . "</br>";
//                                }
//                                if (isset($filters["currency"]) && $filters["currency"] != NULL) {
//                                    echo "Currency = " . $filters["currency"] . "</br>";
//                                }
//                                if (isset($filters["entry_type"]) && $filters["entry_type"] != NULL) {
//                                    echo "Entry Type = " . $filters["entry_type"] . "</br>";
//                                }
//                                if (isset($filters["payment_mode"]) && $filters["payment_mode"] != NULL) {
//                                    echo "Payment Mode = " . $filters["payment_mode"] . "</br>";
//                                }
//                                if (isset($filters["account_no"]) && $filters["account_no"] != NULL) {
//                                    echo "eWallet account No. = " . $filters["account_no"] . "</br>";
//                                }
//                            }
                                    ?>
                            <!--</td>-->
                            <td><?php echo $type; ?></td>
                            <td><?php echo $value['from_date']; ?></td>
                            <td><?php echo $value['to_date']; ?></td>
                            <?php if ($type != "Pending") { ?>
                                <td><?php echo link_to('Download File', 'report/downloadCsv?fileName=' . $value['file_name'] . "&folder=" . $folder); ?></td>
                            <?php } ?>                    
                        </tr>
                    <?php } ?>
                </tbody>
            </table>        
        </div>

        <div class="divBlock">
            <center id="multiFormNav">
                    <?php if ($type == "Generated" || $type == "Pending") { ?>
                        <input type="button" name="buton"
                        <?php
                        if ($type == "Generated") {
                            ?>                           
                                   value="Archive" onclick="deleteRecords('Archive',false);"
                                   <?php
                               }
                               if ($type == "Pending") {
                                   ?>
                                   value="Cancel Request" onclick="deleteRecords('Pending',false);"
                                   <?php
                               }
                               ?>
                               class="formSubmit" >
                               <?php
                           }
                           ?>  
                </center>
        </div>
    <?php } ?>

</div>
<?php if (($pager->getNbResults()) > 0) { ?>
    <table border="0" cellpadding="0" cellspacing="0" class="dataTable">
        <tfoot>
            <tr><td colspan="12">
                    <div class="paging pagingFoot">
                        <?php
                        $url = '';
                        if (isset($report)) {
                            if ($report == 'EwalletRechargeReport') {
                                $url .= url_for('ewalletRechargeReportDisplay');
                            }
                            if ($report == 'EwalletFinancialReport') {
                                $url .= url_for('ewalletFinancialReportDisplay');
                            }
                            if (isset($type)) {
                                $url.='?type=' . $type;
                            }
                            if (isset($frequency)) {
                                $url.='&frequency=' . $frequency;
                            }
                            if (isset($banks)) {
                                $url.='&banks=' . $banks;
                            }
                            if (isset($type)) {
                                $url.='&year=' . $year;
                            }
                        }

                        echo ajax_pager_navigation($pager, $url, 'search_results');
                        ?></td>
            </tr>
        </tfoot>
    </table>
<?php } ?>
</form>
<script language="javascript">

    $('#checkAllAuto').bind('click', function() {        
        $("INPUT[type='checkbox']").attr('checked', $('#checkAllAuto').is(':checked'));
        
        // fetch all check box values
        var currentAllVals = [];
        $("INPUT[type='checkbox']").each(function()
        {
            currentAllVals.push($(this).val());
        });
        //removeing on value of CheckAll checkbox
        currentAllVals.splice(currentAllVals.indexOf('on'), 1);
        
        // if they are check then appending them
        
        var allVals = [];
        if($('#checked_ids').val().length) {
            allVals = $('#checked_ids').val().split(',');
        }
        
        var newVals = [];
        var i =0;
        if($('#checkAllAuto').is(':checked')){
            //checking all current page value if they exist in hidden field
            jQuery.grep(currentAllVals, function(el) {
                if (jQuery.inArray(el, allVals) == -1) newVals.push(el);
                i++;
            });
            allVals = allVals.concat(newVals);
        }
        else{
            // else removing them because might user have selected one value earlier
            jQuery.each(currentAllVals, function(index, value) {                
                allVals.splice(allVals.indexOf(value), 1);
            });
        }        
        $('#checked_ids').val(allVals);
    });
     
    $("input[name^=chk]").bind('click', function() {
        var state = $(this).attr('checked');
        var allVals = [];
        if($('#checked_ids').val().length) {
            allVals = $('#checked_ids').val().split(',');
        }
        if (state == false) {
            $('#checkAllAuto').attr('checked', false);
            //removing id from hiddenb field            
            allVals.splice(allVals.indexOf($(this).val()), 1);
            $('#checked_ids').val(allVals);
            
        } else {
            // adding id to hidden fields
            allVals.push($(this).val());
            $('#checked_ids').val(allVals);
            
            // code to check/ uncheck -> CheckAll if all check box are selected
            $("input:checkbox[name^=chk]").each(function() {
                if ($(this).attr('checked')) {
                    $('#checkAllAuto').attr('checked', true);
                } else {
                    $('#checkAllAuto').attr('checked', false);
                    return false;
                }
            });
        }
    });

$(document).ready(function(){
    if($('#checked_ids').val().length) {
        // making check box checked
        var allVals = [];
        allVals = $('#checked_ids').val().split(',');
        jQuery.each(allVals, function(index, value) {                
                    $('input[value="'+value+'"]').attr("checked", true);
                });
         // if all check box are checked then checking checkAll
        $("input:checkbox[name^=chk]").each(function() {
                if ($(this).attr('checked')) {
                    $('#checkAllAuto').attr('checked', true);
                } else {
                    $('#checkAllAuto').attr('checked', false);
                    return false;
                }
        });
    }
}); 

</script>