
<?php echo ePortal_pagehead(" "); ?>


<?php echo form_tag('report/ewalletUserReportSearch',array('name'=>'pfm_user_report_form','id'=>'pfm_user_report_form')) ?>
 <div class="wrapForm2">
        <?php  echo ePortal_legend(__('System/Pay4me Users Report')); ?>
        <?php  echo formRowFormatRaw(__($form['user_type']->renderLabel()),$form['user_type']->render());?>
        <div id = "pay4me_users">
        <?php  echo formRowFormatRaw(__($form['pay4me_user_type']->renderLabel()),$form['pay4me_user_type']->render());?>
       	</div>
        <div id = "system_users">
         <?php  echo formRowFormatRaw(__($form['system_user_type']->renderLabel()),$form['system_user_type']->render());?>
        </div>
    <div class="divBlock">
    <center id="multiFormNav">
        <input type="Submit" value='<?php echo __('Generate Report')?>' class="formSubmit">
    </center>
  </div>
 </div>
</div>
 </form>

<script>
    function dateValidate(){
        if((document.getElementById('bank_branch_report_from_date_date').value != '') && (document.getElementById('bank_branch_report_to_date_date').value != '')){
        var start_date = document.getElementById('bank_branch_report_from_date_date').value;
        var end_date = document.getElementById('bank_branch_report_to_date_date').value;
        start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
        end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

        if(start_date.getTime()>end_date.getTime()) {
            alert("<From date> cannot be greater than <To date>");
            document.getElementById('bank_branch_report_from_date_date').focus();
            return false;
        }
    }

    }
$(document).ready(function(){
	$('#pay4me_users').hide();
	$('#system_users').show();
	
		//$('label[for="pay4me_user_type"]').hide();
		//alert($('label[for="pay4me_user_type"]').parent().html());
		//$('.dsTitle4Fields pay4me_user_type').hide();
    $("#user_type" ).live( "change", function() {
		if(this.value != 'pay4me') {
		$('#pay4me_users').hide();
		$('#system_users').show();
	} else {
		$('#system_users').hide();
		$('#pay4me_users').show();
	}
});
});
    </script>
