<?php   echo ePortal_pagehead(" ");?>
<?php //use_helper('Form') ?>


 <div class="wrapForm2">
 <?php echo ePortal_legend('Individual eWallet Account Report Section');?>
<?php echo form_tag('report/indEwalletAccountsSearch',array('name'=>'ewallet_account_form','method'=>'get','id'=>'ewallet_account_form', 'onsubmit' => 'return ewalletAccounts(this);')) ?>
<?php  echo $form;?>

 <?php //echo submit_tag('Make Report'); ?><div class="divBlock">
    <center id="multiFormNav">
  <?php echo button_to('Generate Report','',array('class' => 'formSubmit','onClick'=>'ewalletAccounts()')); ?></center>
  </div></form></div>
<div id="search_results" class="load_overflow"></div></div>
  
  <script>
      function ewalletAccounts(){
          err =0;
          if(document.getElementById('account_no').value == '' ){
                alert('Please enter ewallet account no.');
                document.getElementById('account_no').focus();
                return false;
            }
           else if(isNaN(document.getElementById('account_no').value) ){
              
                alert('Please enter valid eWallet account no.');
                document.getElementById('account_no').value = '' ;
                document.getElementById('account_no').focus();
                return false;
             }
            /* =================[ START ] Date Field Validations====================  */
//            if(document.getElementById('from_date').value == '' || document.getElementById('to_date').value == ''){
//                alert('Please Select all Date through calander.');
//                document.getElementById('from_date').focus();
//                return false;
//            }


            if((document.getElementById('from_date').value != '') && (document.getElementById('to_date').value != '')){
                var start_date = document.getElementById('from_date').value;
                var end_date = document.getElementById('to_date').value;
                
                start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
                end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

                if(start_date.getTime()>end_date.getTime()) {
                    alert("<From date> cannot be greater than <To date>");
                    document.getElementById('from_date').focus();
                    return false;
                }
            }
        /* ================= [ END ] Date Field Validations ====================  */
            if(err == 0) {
              
                      $.post('indEwalletAccountsSearch',$("#ewallet_account_form").serialize(), function(data){
                      if(data=='logout'){
                                    location.reload();
                             }
                               else {
                                $("#search_results").html(data);
                               }
                        });
                         
                    }
}
  </script>