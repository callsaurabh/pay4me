<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>




<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('NIBSS Advice Report Details'); ?>
    </fieldset>
</div>

<div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
    <br class="pixbr" />
</div>

<table class="tGrid">

<tr>
<td>

 <?php

        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
//            echo "<pre>";
//            print_r($pager->getResults());
//            die;
?>
<table class="tGrid">
    <thead>
        <tr>
            <th width = "2%">S.No.</th>
            <!--<th>Date</th>-->
            <th>Particulars</th>
                <th>Credit</th>
               <th>Debit</th>
        </tr>
    </thead>
<?php
            foreach ($pager->getResults() as $result):

                $i++;
////                echo "<pre>";
////                print_r($result->toArray());
////                die;
?>


    <tbody>


        <tr>
            <td align="center"><?php echo $i ?></td>
            <!--<td align="center">date</td>-->
            <td align="left"><?php echo $result->getAccountName() ?></td>

            <?php
//                foreach($result->getFirst()->getEpMasterLedger() as $res) {
//              if($res->getEntryType() == "credit")
//                    {echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total Credits:</b> ".format_amount($res->getAmount(),1);}
//                else echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Total Debits: </b>".format_amount($res->getAmount(),1);}

            ?>

           <?php
             foreach ($result->getEpMasterLedger() as $payDetails){


             if($payDetails->getAmount() != '')
             {

                 if( "credit"==$payDetails->getEntryType()){
                      ?><td align="center"><?php
                         echo format_amount($payDetails->getAmount(),NULL,1); ?>
                         </td> <td>&nbsp;</td>  <?php
                      }
//                  else{ <td align="center">&nbsp; g</td><?php }
                    ?>



                   <?php
                     if($payDetails->getEntryType() == "debit" ){
                         ?><td align="center"><?php
                           echo format_amount($payDetails->getAmount(),NULL,1) ;
                       ?> </td><td>&nbsp;</td>
                    <?php

                       }

                 } // get amount
             }
           ?>

        </tr>



  <?php endforeach;    ?>
  </tbody>
    <tfoot><tr><td colspan="5"></td></tr></tfoot>
</table>

</td>
</tr>
    <tr><td >

<div class="paging pagingFoot"><?php  //echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?batchIds='.$sf_request->getParameter('batchIds').'&scheduleId='.$sf_request->getParameter('scheduleId')), 'theMiddle')
//pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?designation_id='.$sf_request->getParameter('batchIds')))
echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$sf_request->getParameter('from_date').'&to_date='.$sf_request->getParameter('to_date').'&entry_type='.$sf_request->getParameter('entry_type')));
?>

  </div></td></tr>
      <?php
    }else { ?>

        <tr><td  align='center' class='error' >No Record Found</td></tr>
        <?php } ?>
</table>