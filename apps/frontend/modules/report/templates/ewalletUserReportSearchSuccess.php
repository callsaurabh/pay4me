<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php switch($user_type) {
	case 'merchant' :$report_label = 'Merchant User Report';
		break;
	case 'portal_support' :$report_label = 'Portal Support User Report';
		break;
	case 'financial_user' :$report_label = 'Financial User Report';
		break;
	default: $report_label = 'System/Pay4me Users Report';
		break;
}
?>
<?php echo ePortal_listinghead($report_label); ?>
<?php if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
      <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('EwalletUserReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
    </th>
    </tr>
  </table>
 <br class="pixbr" />
</div>
<?php } ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr align = "center" class="horizontal">
      <th width = "12%">Username</th>
      <th width = "18%">Name</th>
      <th width = "16%">Email Address</th>
      <th width = "18%">Mobile Number</th>
      <th width = "18%">User Status</th>
      <th width = "30%">Last Login </th>
    </tr>
  </thead>
  <tbody>
    <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
$originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      //print '<pre>'; print_r($pager->getResults()->toArray());die;
      //$i = 0;
      
      foreach ($pager->getResults() as $result) {
        ?>
    <tr align = "center" class="alternateBgColour">
      <td><?php echo $result->getUserName(); ?></td>
      <td><?php echo $result->getUserDetail()->getFirst()->getName(); ?></td>
      <td><?php echo $result->getUserDetail()->getFirst()->getEmail(); ?></td>
      <td><?php echo $result->getUserDetail()->getFirst()->getMobileNo(); ?></td>
      <td><?php switch($result->getUserDetail()->getFirst()->getUserStatus()){
      	          case 1: echo "Active";
      	            break;
      	          case 2: echo "Bank User Suspended";
      	            break;
      	          case 3: echo "Bank User Deactivate";
      	            break;
      	          case 4: echo "eWallet wait Admin";
      	            break;
      	          case 5: echo "eWallet Disapprove";
      	            break; 
      	          } ?>
      </td>
      <td><?php echo $result->getLastLogin(); ?></td>
      <?php
      }
      
     Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);
     
     
     ?>

    </tr>
    <?php
    }else{ ?><table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
    <?php } ?>

  <tfoot>
  <tr><td colspan="10"><div class="paging pagingFoot floatRight"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td></tr>
   </tfoot></tbody>
</table>
</div>

<div class="paging pagingFoot"><center><?php  echo button_to('Cancel','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/ewalletUserReport').'\''));?></center></div>
</div>
<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>
