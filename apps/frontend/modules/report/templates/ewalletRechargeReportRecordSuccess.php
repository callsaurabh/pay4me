
<div class="wrapForm2">

	<div class="divBlock">
		<center id="multiFormNav"></center>
	</div>
</div>

<div class="wrapForm2">
    <div id="loadArea">
        <h2> </h2>
        <div class="clear"></div>
	<table width="100%" border="0" cellpadding="0" cellspacing="0"
		class="dataTable">
				
		<?php foreach ($data as $key => $value){ ?>
		<thead>
			<tr>
				<th class="formHead" style="color: white;" colspan="3"><b><?php echo ucfirst(strtolower($key)); ?></th>
			</tr>
			<tr align="center" class="horizontal">

				<th width="30%">Report Name</th>



				<th width="40%">Duration</th>
				<th width="30%">Download - Link</th>
		
		</thead>
		<?php
			if ($value) {
				foreach ( $value as $index => $fileName ) {
					$finalStr = explode ( '_', $fileName );
					
					?>
		<tr>

			<td>
		<?php
					echo $finalStr [0];
					?>
			</td>
			<td>
			
		<?php
					echo $finalStr [1]. " to " . $finalStr [2];
					?>
			</td>
			<td>
			<?php
					echo link_to ( 'Download File', 'report/downloadCsv?fileName=' . $fileName . "&folder=" . $folder );
					?>
			</td>
		</tr>
		
		<?php
				}
			}
		}
		?>
		</table>
        
    </div>
</div>
