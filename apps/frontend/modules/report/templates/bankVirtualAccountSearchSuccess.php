<?php  //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
<br />
<?php echo ePortal_listinghead('Bank Virtual Account Details'); ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>

 
<div class="wrapTable">  
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <?php
        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
      ?>
    <thead>
        <tr>
            <th width = "2%">S.No.</th>
            <th>Date</th>
            <th>Particulars</th>
            <th>Balance</th>
      

        </tr>
    </thead>
<?php
          
            foreach ($pager->getResults() as $result):
           
          
            $i++;

?>
  
        <tr>
            <td align="center"><?php echo $i ?></td>
            <td align="center"><?php echo date('d-m-Y',strtotime($result->getUpdt()));   ?></td>
            <td align="left"><?php echo $result->getAcoountname();   ?></td>
            <td align="left"><?php echo format_amount($result->getClbl(),NULL,1);   ?></td>
        </tr>
  <?php endforeach;    ?>
  
    <tfoot><tr><td colspan="5">
      <div class="paging pagingFoot">
 <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'));?>

</div>
    </td></tr></tfoot>
    <?php
    }else { ?>

        <tr><td  align='center' class='error' >No Record Found</td></tr>
        <?php } ?>
</table>
</div>