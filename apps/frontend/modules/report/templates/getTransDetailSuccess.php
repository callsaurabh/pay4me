<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
<table class="tGrid">
  <thead>
 
    <tr>
      <th width = "2%">S.No.</th>
      <th>Narration</th>
      <th>Transaction Date</th>
      <th>Amount</th>
      <th>Transaction Type</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
      $walletDetails=$walletDetailsObj->fetchArray();
      if(count($walletDetails)>0)
      {
              foreach ($walletDetails as $result):



                $i++;
                $transaction_date = explode(" ",$result['transaction_date']);
                $amount = format_amount($result['amount'],NULL,1);
                ?>
            <tr>
              <td align="center"><?php echo $i ?></td>
              <td align="center"><?php echo html_entity_decode($result['description']);?></td>
              <td align="center"><?php echo $transaction_date['0'];?></td>
              <td align="right"><?php echo $amount; ?></td>
              <td align="center"><?php echo ucwords($result['entry_type']); ?></td>
            </tr>
              <?php endforeach; ?>
    <?php }
    else { ?>
    <tr><td  align='center' class='error' colspan="5">No Record Found</td></tr>
    <?php } ?>

  </tbody>
  <tfoot><tr><td colspan="5"></td></tr></tfoot>
</table>

