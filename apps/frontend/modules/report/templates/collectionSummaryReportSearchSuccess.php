<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


<?php echo ePortal_listinghead('Summary of Collection Report -->  All Transactions'); ?>
<?php if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
      <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('CollectionSummaryReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
    </th>
    </tr>
  </table>
 <br class="pixbr" />
</div>
<?php } ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr align = "center" class="horizontal">
      <th width = "1%">S.No.</th>
      <th width = "25%">List of Merchants</th>
      <th width = "18%">No. of Transaction</th>
      <th width = "18%">Total Paid Amount<br />(<?php echo getCurrencyImage($postDataArray['currency']);?>)</th>
      <th width = "16%"><?php echo $merchantName;?> Amount<br />(<?php echo getCurrencyImage($postDataArray['currency']);?>)</th>
      <th width = "18%">Pay4Me Amount<br />(<?php echo getCurrencyImage($postDataArray['currency']);?>)</th>
      <th width = "30%">Bank Amount (<?php echo getCurrencyImage($postDataArray['currency']);?>)</th>
    </tr>
  </thead>
  <tbody>
    <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
      $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
      Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      $totalAmount = '';
      $totalTransactions ='';
      $totalNisAmount = '';
      $totalPay4meAmount = '';
      $totalBankAmount = '';
      foreach ($pager->getResults() as $result) {
          //print_r($result->merchant_name);die;
        $i++;
        ?>
    <tr align = "center" class="alternateBgColour">
      <td><?php echo $i; ?></td>
      <td><?php echo $result->getMerchantName(); ?></td>
      <td><?php echo $result->getNoOfTransactions(); ?></td>
      <td><?php echo number_format($result->getAmount(),2); ?></td>
      <td><?php echo number_format($result->getMerchantAmount(),2); ?></td>
      <td><?php echo number_format($result->getPayformeAmount(),2); ?></td>
      <td><?php echo number_format($result->getBankAmount(),2); ?></td>
      	
      <?php
      // page total
      $totalAmount = $totalAmount + $result->getAmount();
      $totalTransactions = $totalTransactions + $result->getNoOfTransactions();
      $totalNisAmount = $totalNisAmount + $result->getMerchantAmount();
      $totalPay4meAmount = $totalPay4meAmount + $result->getPayformeAmount();
      $totalBankAmount = $totalBankAmount + $result->getBankAmount();
      }
     Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute); 
     ?>
    </tr>
    <tr align = "center" class="alternateBgColour">
      <th colspan="2" align="right">Total</th>
      <td><?php  echo $totalTransactions; ?></td>
      <td width = "18%"><?php echo number_format($totalAmount,2); ?></td>
      
      <td><?php echo number_format($totalNisAmount,2); ?></td>
      <td><?php echo number_format($totalPay4meAmount,2); ?></td>
      <td><?php echo number_format($totalBankAmount,2); ?></td>
   </tr>
    <tr align = "center" class="alternateBgColour">
      <th colspan="2" align="right">Grand Total</th>
      <td><?php  echo $grandTotalTransactions; ?></td>
      <td width = "18%"><?php echo number_format($grandTotalAmount,2); ?></td>
      
      <td><?php echo number_format($grandNisAmount,2); ?></td>
      <td><?php echo number_format($grandPay4MeAmount,2); ?></td>
      <td><?php echo number_format($grandBankAmount,2); ?></td>
   </tr>
    <?php
    }else{ ?><table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
    <?php } ?>

  <tfoot>
  <tr><td colspan="10"><div class="paging pagingFoot floatRight"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td></tr>
   </tfoot></tbody>
</table>
</div>

<div class="paging pagingFoot"><center><?php  echo button_to('Cancel','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/collectionSummaryReport').'\''));?></center></div>
</div>
<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>
