<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');
$postParam = sfContext::getInstance()->getRequest()->getParameter("avc_report");
$from_dt = $postParam['from_date'];
$to_dt = $postParam['to_date'];

$txt = "";
if(!empty($from_dt) && !empty($to_dt)){
    $txt  = " [ <b>Date Selected:</b> ".$from_dt;
    $txt .= " <b>to</b> ".$to_dt." ]";
    
}


?>


<?php echo ePortal_listinghead('Address Verification Charges Report -->  All Transactions'); ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
      <div id = "csvDiv" style="cursor:pointer" onclick="javascript:progBarDiv('avcReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
    </th>
    </tr>
  </table>
 <br class="pixbr" />
</div>
<?php //} ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
          <span class="floatLeft">Found <b><?php echo count($bank_report_list) ?></b> results matching your criteria.<?php echo $txt?></span>
        <!--span class="floatRight">Showing <b><?php //echo $pager->getFirstIndice() ?></b> - <b><?php //echo $pager->getLastIndice() ?></b> of total  <b><?php //echo $pager->getNbResults(); ?></b>  results</span-->
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr align = "center" class="horizontal">
      <th>S.No.</th>
      <th>Bank</th>
      <th>No. of Transaction</th>
      <th>Amount (<?php echo getCurrencyImage(1);?>)</th>
      
    </tr>
  </thead>
  <tbody>
    <?php 
    $i = 0;
    $totalpaidamount = 0;
    $noofrecords = 0;
    $totalbankamount = 0;
    $totalpayformeamount = 0;
    $total_item_fees = 0;
    $avc_amount = 0;
   if(count($bank_report_list) > 0 ){     
      foreach ($bank_report_list as $result) { 
        $i++;
        ?>
    <tr align = "center" class="alternateBgColour">
      <td><?php echo $i; ?></td>
      <td><?php echo $result['bank_name']; ?></td>
      <td style="text-align:right"><?php echo $result['cnt']; ?></td>
      <td style="text-align:right"><?php echo  number_format($result['avc_amount'],2,".",","); ?></td>         
      <?php
        $avc_amount = $avc_amount + $result['avc_amount'];
        $noofrecords = $noofrecords + $result['cnt'];
     }
     //Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute); 
     ?>
    </tr>
    <tr align = "center" class="alternateBgColour">
      <th>&nbsp;</th>
      <th align="right">Grand Total</th>
      <td style="text-align:right"><?php echo  $noofrecords; ?></td>
      <td width = "18%" style="text-align:right"><?php echo number_format($avc_amount,2,".",","); ?></td>
   </tr>
    <?php
    }else{ ?>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
   <?php } ?>

  <tfoot>
  <tr><td colspan="10"><div class="paging pagingFoot floatRight"><?php //echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td></tr>
   </tfoot></tbody>
</table>
</div>

<div class="paging pagingFoot"><center><?php  echo button_to('Cancel','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/avcReport').'\''));?></center></div>
</div>
<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>