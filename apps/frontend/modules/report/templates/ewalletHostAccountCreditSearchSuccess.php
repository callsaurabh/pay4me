<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php
  if($err == true){
        print  "<div id='flash_error' class='error_list'><span>Please select a Bank (account) to proceed.</span></div>";
  }else{

echo ePortal_legend('eWallet Host Account Report Details');
echo  "<br>";

?>

<?php echo ePortal_listinghead('Credited Amount Details'); ?>
  <div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th>
          <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
      </tr>
    </table>
    <br class="pixbr" />
  </div>
  
  <div class="wrapTable" style="width:857px;overflow:auto">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <?php
      if(($pager->getNbResults())>0) {

          $from_date = $fromDate == "" ? "BLANK" : $fromDate;
          $to_date = $toDate == "" ? "BLANK" : $toDate;

          $limit = sfConfig::get('app_records_per_page');
          $page = $sf_context->getRequest()->getParameter('page',0);
          $i = max(($page-1),0)*$limit ;
    ?>
    <thead>
      <tr class="horizontal">
        <th width = "2%">S.No.</th>
        <th>Date</th>
        <th>Particulars</th>
        <th>Credit</th>
      </tr>
    </thead>
    <?php
      foreach ($pager->getResults() as $result):
        $i++;
    ?>
    <tbody>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <td align="center"><?php echo $result->getDate() ?></td>
        <td align="left"><?php echo "Received from CASH deposit / ".pfmHelper::getChequeDisplayName().' Payments / ' . pfmHelper::getDraftDisplayName() . ' Payments' ?></td>
        <?php
          if($result->getAmount() != '')
          {
        ?>
        <td align="center"><?php echo format_amount($result->getAmount(),NULL,1); ?></td>
        <?php
          } // get amount
        ?>
      </tr>
      <?php endforeach; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="5">
          <div class="paging pagingFoot">
            <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date.'&master_account_id='.$master_account_id), 'search_results1'); ?>
          </div>
        </td>
      </tr>
    </tfoot>
    <?php
    }else { ?>
    <tr><td  align='center'  ><span class="error"><?php echo "No Record Found"; ?></span></td></tr>
    <?php } ?>
  </table>
  </div>


<?php } ?>


