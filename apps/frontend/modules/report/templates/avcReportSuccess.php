<?php echo ePortal_pagehead(" "); ?>


<?php echo form_tag('report/avcReportSearch',array('name'=>'pfm_report_form','id'=>'pfm_report_form','onsubmit' => 'return dateValidate(this);')) ?>
 <div class="wrapForm2">
        <?php echo ePortal_legend(__('Address Verification Charges Report')); ?>
        <?php  echo formRowFormatRaw(__($form['from_date']->renderLabel()),$form['from_date']->render());?>
        <?php  echo formRowFormatRaw(__($form['to_date']->renderLabel()),$form['to_date']->render());?>
        
    <div class="divBlock">
    <center id="multiFormNav">
        <input type="Submit" value='<?php echo __('Generate Report')?>' class="formSubmit">
    </center>
  </div>
 </div>
</div>
 </form>
 
<script>
    function dateValidate(){
        if((document.getElementById('avc_report_from_date_date').value != '') && (document.getElementById('avc_report_to_date_date').value != '')){
        var start_date = document.getElementById('avc_report_from_date_date').value;
        var end_date = document.getElementById('avc_report_to_date_date').value;
        start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
        end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

        if(start_date.getTime()>end_date.getTime()) {
            alert("<From date> cannot be greater than <To date>");
            document.getElementById('avc_report_from_date_date').focus();
            return false;
        }
    }
        
    }
    </script>
