<?php echo ePortal_pagehead(" "); ?>
<?php  //use_helper('Form') ?>

<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/paidByVisaReportSearch','name=search id=search');?>
    <?php echo ePortal_legend(__('Internet Bank Payment Report'));?>
    <?php //echo formRowComplete(__($form['pay_mode']->renderLabel()).'<sup class=cRed>*</sup>',$form['pay_mode']->render(),'','pay_mode','err_pay_mode','pay_mode_row');?>
    <?php echo formRowComplete(__($form['pay_type']->renderLabel()),$form['pay_type']->render(),'','pay_type','err_pay_type','pay_type_row'); ?>
    <?php
          echo "<div id = 'transaction_no' name = 'transaction_no' style = 'display:none'>";
              echo formRowComplete(__($form['transaction_no']->renderLabel()),$form['transaction_no']->render(),'','transaction_no','err_transaction_no','pay_transaction_no');
           echo "</div>";
    ?>
    <?php
          echo "<div id = 'account_no' name = 'account_no' style = 'display:none'>";
             echo formRowComplete(__($form['account_no']->renderLabel()),$form['account_no']->render(),'','account_no','err_account_no','pay_account_no');
           echo "</div>";
    ?>
    <?php echo formRowComplete(__($form['from']->renderLabel()),$form['from']->render(),'','from_date','err_from_date','from_date_row'); ?>
    <?php echo formRowComplete(__($form['to']->renderLabel()),$form['to']->render(),'','to_date','err_to_date','to_date_row'); ?>
    <?php
//    if($isSuperadmin)
//    echo formRowComplete(__($form['username']->renderLabel()),$form['username']->render(),'','username','err_username','to_username'); ?>
    <?php echo formRowComplete(__($form['trans_type']->renderLabel()).'<sup class=cRed>*</sup>',$form['trans_type']->render(),'','trans_type','err_trans_type','trans_type_row'); ?>
    <div class="divBlock">
        <center>
            <?php echo $form[$form->getCSRFFieldName()]->render(); ?>
            <?php echo button_to(__('Search'),'',array('class'=>'formSubmit', 'onClick'=>'validateRequestForm()')); ?>
        </center>
    </div>
    </form>
</div>
<div id="search_results"></div>
</div>
<script>
    function getStatusfrmPayMode(url){

  var paymode = $("#paidwithvisa_pay_mode").val();
 var url = '<?php echo url_for($sf_context->getModuleName().'/getStatusfrmPayMode');?>';
  $.post(url, {paymode:paymode}, function(data){
     if(data == 'logout'){
          location.reload();
      }else{
        $('#paidwithvisa_trans_type').html(data);
      }
    });

}

    function validateRequestForm(){
        $('#search_results').html("");
        $("#err_from_date").html("");
        $("#err_to_date").html("");
        var err=0;
        var from_date = $('#paidwithinternetbank_from_date').val();
        var to_date = $('#paidwithinternetbank_to_date').val();

        var milli_from_date;
        var milli_to_date;

        var today = new Date();
        var milli_today = today.getTime();

        if(from_date != "")
        {
            var from_date_sel = from_date.split("-");
            var from_date_selected = new Date(from_date_sel[0],from_date_sel[1]-1,from_date_sel[2] );
            milli_from_date = from_date_selected.getTime();
            var isFutureDate = milli_today - milli_from_date;
            if(isFutureDate<0)
            {
                $("#err_from_date").html("<?php echo __('From Date should not be a future Date')?>");
                return false;
            }
        }
        if(to_date != "")
        {
            var to_date_sel = to_date.split("-");
            var to_date_selected = new Date(to_date_sel[0],to_date_sel[1]-1,to_date_sel[2] );
            milli_to_date = to_date_selected.getTime();
            var isFutureDate = milli_today - milli_to_date;
            if(isFutureDate<0)
            {
                $("#err_to_date").html("<?php echo __('To Date should not be a future Date')?>");
                return false;
            }
        }
        if((from_date != "") && (to_date != ""))
        {
            var diff = milli_to_date - milli_from_date;

            if(diff<0)
            {
                err = err+1;
                $("#err_from_date").html("<?php echo __('From Date should be less than To Date')?>");
            }
            else
            {
                $("#err_from_date").html("");
            }

        }


        if(err == 0) {
            $.post('paidByInternetBankReportSearch',$("#search").serialize(), function(data){
                if(data=='logout'){
                    location.reload();
                }
                else {
                    
                    $("#search_results").html(data);}
            }
        );
        }
    }
   $(document).ready(function() {
    if ($("#paidwithinternetbank_pay_type").val()=='payment') {

           $('#transaction_no').show();
           $('#account_no').hide();

       } else {
            $('#transaction_no').hide();
            $('#account_no').show();
    }
    $("#paidwithinternetbank_pay_type").change(function()
    {
       if ($("#paidwithinternetbank_pay_type").val()=='payment') {

           $('#transaction_no').show();
           $('#account_no').hide();

       } else {
            $('#transaction_no').hide();
            $('#account_no').show();
       }
  });
   });
  

</script>