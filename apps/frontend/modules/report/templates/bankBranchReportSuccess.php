<?php echo ePortal_pagehead(" "); ?>
<?php // use_helper('Form') ?>
<?php //echo $bank_details_id; ?>
<?php echo form_tag('report/bankBranchReportSearch',array('name'=>'pfm_report_form','id'=>'pfm_report_form', 'onsubmit' => 'return dateValidate(this);')) ?>
 <div class="wrapForm2">

        <?php echo ePortal_legend('Bank Branch Transaction Report'); ?>
       
        <?php  echo formRowFormatRaw($form['merchant']->renderLabel(),$form['merchant']->render());?>
        <?php  echo formRowFormatRaw($form['service_type']->renderLabel(),$form['service_type']->render());?>
        <?php  echo formRowFormatRaw($form['payment_mode']->renderLabel(),$form['payment_mode']->render());?>
        <div id="bankbrach" style="display:none">
            <?php include_component('bank', 'list'); //'not_needed'=>'branch'?>
        </div>
        <?php  echo formRowFormatRaw($form['currency']->renderLabel(),$form['currency']->render());?>
        <?php  echo formRowFormatRaw($form['from_date']->renderLabel(),$form['from_date']->render());?>
        <?php  echo formRowFormatRaw($form['to_date']->renderLabel(),$form['to_date']->render());?>
        <?php  echo formRowFormatRaw('Ignore Disable Bank',$form['disable_bank']->render());?>
        <?php  echo $form['report_label']->render();?>

    <div class="divBlock">
    <center id="multiFormNav">
    <?php //echo submit_tag('Make Report',array('class'=>'formSubmit')); ?>
    <input type="Submit" value='Generate Report' class="formSubmit">
    </center>
    </div>
  </div>

  </form>
</div>
<script>
    function dateValidate(){
        if((document.getElementById('bank_branch_report_from_date_date').value != '') && (document.getElementById('bank_branch_report_to_date_date').value != '')){
        var start_date = document.getElementById('bank_branch_report_from_date_date').value;
        var end_date = document.getElementById('bank_branch_report_to_date_date').value;
        start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
        end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

        if(start_date.getTime()>end_date.getTime()) {
            alert("<From date> cannot be greater than <To date>");
            document.getElementById('bank_branch_report_from_date_date').focus();
            return false;
        }
    }

    }
    </script>