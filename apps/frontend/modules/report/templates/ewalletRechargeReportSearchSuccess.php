<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead('eWallet Recharge Report --> All Transactions');?>

<?php if (!$err) { ?>

    <?php if($pager->getNbResults()>0) { ?>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('eWalletRechargeReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>
<?php  } ?>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
            <tr align = "center" class="horizontal">
            <th width = "1%">S.No.</th>
            <th width = "15%">Bank Name</th>
            <th width = "30%">Branch Name</th>
            <th width = "10%">Branch Code</th>
            <th width = "30%">Username</th>

            <th width = "20%">Entry Type</th>
            <th width = "40%">Account Name</th>
            <th width = "20%">eWallet Account Number</th>
            <th width = "20%">Reachrged at</th>
            <th width = "20%" nowrap>Amount (<?php echo getCurrencyImage($currency_id);?>)</th>
        </thead>
        <tbody>
            <?php

            if($pager->getNbResults()>0) {

                $limit = sfConfig::get('app_per_page_records');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
                $totalamout = '';
                foreach ($pager->getResults() as $result) {
                    $totalamout+=$result['amount'];
                    $i++;

                    ?>
            <tr align = "center" class="alternateBgColour">
                <td><?php echo $i ?></td>
                <td><?php echo $result['bank_name']?></td>
                <td><?php echo $result['branch_name'] ?></td>

                <td><?php echo $result['branch_code'] ?></td>
                <td><?php echo $result['recharged_by']; ?></td>

                <td><?php echo $result['entry_type'] ?></td>

                <td><?php echo $result['account_name'] ?></td>
                 <td align="center"><?php
           if( $isSuperadmin)  {
               if(isset($result['payment_mode']))
               echo   link_to( $result['ewallet_account_number'], 'recharge_ewallet/rechargeNotice?id='.base64_encode($result['validation_number'].':'.$result['ewallet_account_number'].':'.$result['amount']).'&template=layout_popup'.'&bank_name='.$result['bank_name'].'&branch_name='.$result['branch_name'].'&payment_mode='.$result['payment_mode'], array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes,scrollbars=yes') ,
              'target'=>'_blank') );else{
                  echo   link_to( $result['ewallet_account_number'], 'recharge_ewallet/rechargeNotice?id='.base64_encode($result['validation_number'].':'.$result['ewallet_account_number'].':'.$result['amount']).'&template=layout_popup'.'&bank_name='.$result['bank_name'].'&branch_name='.$result['branch_name'], array('popup' => array('View Detail', 'width=700,height=500,left=150,top=100,resizable=yes,scrollbars=yes') ,
              'target'=>'_blank') );
              }
           }else{
            echo $result['ewallet_account_number'];
           }
            ?></td>
                <td><?php echo $result['recharged_at'] ?></td>
                <td align="right"><?php echo format_amount($result['amount'],null,1); ?></td>
            </tr>
            <?php


        }?>
            <tr>
                <td colspan="9" align="right" >Total</td>
                <td align="left"><?php echo format_amount($totalamout,null,1); ?></td>
            </tr>
            <tr>
                <td colspan="9" align="right" >Grand Total</td>
                <td align="left"><?php echo format_amount($grandTotal,null,1); ?></td>
            </tr>
            <?php
        }else {?>
            <tr><td colspan="11" ><span class="error"><?php echo "No Record Found"; ?></span></td></tr>
            <?php }?>

        </tbody>

        <!--tfoot><tr><td colspan="10"></td></tr></tfoot-->


        <tr><td colspan="10">
                <div class="paging pagingFoot"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div>
        </td></tr>
       
    </table>
	
	 <div class="paging pagingFoot"><center><?php


                            echo button_to('Back','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('report/ewalletRechargeReport').'\''));

                            ?></center></div>
</div>
<?php
} else { echo '  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
<tr><td  align="center" class="error">'.$err."</td></tr></table>"; }   ?>
<script>
    function progBarDiv(url, referenceDivId, targetDivId){
        $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        $('#'+referenceDivId).css("display","none");
        $('#'+targetDivId).css("display","inline");
        $('#'+targetDivId).load(url, {byPass:1 },function (data){
            if(data=='logout'){
                $('#'+targetDivId).html('');
                location.reload();
            } });

    }
</script>
