<?php
/**
 * Template to display report result alongwith validated data
 */
?><?php //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<table><tr><td>&nbsp;</td></tr></table>
<?php if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('internationalCurrencyReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>
<?php } ?>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th width="100%" >
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>






<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
            <tr class="horizontal">
                <th width = "2%">S.No.</th>
                <th>UserName</th>
                <th>Transaction Date</th>
                <th>Paid Amount</th>
                <?php

                foreach($split_account as $val) {
                    echo "<th>".$val->getName()."</th>";
                }?>

                <th>Pay4Me Amount</th>
            </tr>
        </thead>
        <tbody>
            <?php
            //    echo $pager->getNbResults(); die;


            if(($pager->getNbResults())>0) {
                $limit = sfConfig::get('app_records_per_page');
                $i = max(($page-1),0)*$limit ;
                $totalPaidAmount =0;
                $totalsum_split =0;
                $swgSplitAmt = 0;
                $merchantSplitAmt = 0;
                $totalP4m=0;
                foreach ($pager->getResults() as $result):

                $i++;

         //      print "<pre>";print_r($result->getSfGuardUser());exit;
//                foreach ($result as $key){
//              echo '<pre>';
//              print_r($result->toArray());exit;
//              echo '</pre>';
//               }
                if($result->getSfGuardUser()){
                    $user_name = $result->getSfGuardUser()->getUsername();
                }
                else{ $user_name = '--' ;}
                $transaction_date = $result->getUpdatedAt();

                $merchant_id = $result->getMerchantId();
                $merchant_service_id = $result->getMerchantServiceId();


                //Earlier $transaction_date = explode(" ",$result->getTranDateTime());
                $amount = format_amount($result->getPaidAmount(),2);
                ?>
            <tr class="alternateBgColour">
                <td align="center"><?php echo $i ?></td>
                <td align="center"><?php  echo $user_name; ?></td>
                <td align="center"><?php echo $transaction_date;?></td>
                <td align="right"><?php echo $amount; ?></td>
                <?php

                $totalPaidAmount = $totalPaidAmount +$result->getPaidAmount();
                // $test = format_amount($totalPaidAmount,2);
                $sum_split = 0;
                // if(is_array($result->getMerchantRequestPaymentDetails()->getFirst())) {print "in";
                foreach ($result->getMerchantRequestPaymentDetails()->getFirst()->getPay4meSplit() as $sp1){
                    // print $sp1->getAmount();

                    if($sp1->getAmount() != '')
                    {
                        $sum_split = $sum_split+$sp1->getAmount();
                        $totalsum_split = $totalsum_split+$sp1->getAmount();

                        if($sp1->getAccountNumber() == 39878178){
                            $swgSplitAmt = $swgSplitAmt + $sp1->getAmount();
                        }else{
                            $merchantSplitAmt = $merchantSplitAmt + $sp1->getAmount();
                        }


                        ?>
                <td align="right"><?php echo format_amount($sp1->getAmount(),2,1) ;?></td>
                <?php
            }

        }
        $sum_split = format_amount($sum_split,'',1);
        $p4m_share = $result->getPaidAmount()-$sum_split;

        ?>

            <td align="right"><?php echo format_amount($p4m_share,2);?></td></tr>
            <?php endforeach; ?>
            <!--  <td align="center"><?php /* $var = $result->getOrderStatus();
                                    if(!count($var))
                                   { echo "- -";}
                                    else
                                     {echo ucwords($var);} */ ?></td> -->
    <?php
    //echo format_amount($totalPaidAmount,2);
    //echo format_amount($totalsum_split,2,1);
    //$var = format_amount($totalPaidAmount,2);
    $splitAmt = format_amount($totalsum_split,'',1);
    ?>
    <?php
    $totalP4m=$totalPaidAmount-$splitAmt;
    //      echo $totalP4m;?>

               <tr  class="alternateBgColour">
               <td colspan="3" align="right" ><?php echo"<b>Total</b>"; ?></td>
                <td align="right"><?php  echo '<b>'.format_amount($totalPaidAmount,2).'</b>';?></td>
                <td align="right"><?php  echo '<b>'.format_amount($merchantSplitAmt,2,1).'</b>';?></td>
                <td align="right"><?php  echo '<b>'.format_amount($swgSplitAmt,2,1).'</b>';?></td>
                <td align="right"><?php  echo '<b>'.format_amount($totalP4m,2).'</b>'; ?></td>
                </tr>


            <?php
         /*     foreach($split_account as $val) {
             ?>
             <tr>
                <td colspan="6" align="right" > <?php echo $val->getName(); ?> Total Split Amount</td>
                <td align="left"><?php
                if($val->getName() == "swglobal"){
                    echo format_amount($swgSplitAmt,2,1);
                }else{
                    echo format_amount($merchantSplitAmt,2,1);
                }

                ?></td>
            </tr>
             <?php

                }
           */ ?>

            <tr>
                <td colspan="6" align="right" ><b>Grand Pay4Me Amount</b></td>
                <td align="right"><?php

                   $pay4meGrand = format_amount($paidAmountGrand)-format_amount($splitAmountGrand,'',1);
                   echo '<b>'.format_amount($pay4meGrand,2).'</b>';
                   ?></td>

                </tr>


            <?php
            $param = '?';


            if($sf_request->getParameter('mid')){
                $param = $param."mid=".$sf_request->getParameter('mid');
            }
            if($sf_request->getParameter('msid')){
                $param = $param."&msid=".$sf_request->getParameter('msid');
            }
            if($sf_request->getParameter('uname')){
                $param = $param."&uname=".$sf_request->getParameter('uname');
            }

            if($sf_request->getParameter('fdate')){
                if(strstr($sf_request->getParameter('fdate'), '/')){
                $fdate = explode('/',$sf_request->getParameter('fdate'));
                $fdate = $fdate[2]."-". $fdate[0]."-". $fdate[1];}
            else{
                $fdate = $sf_request->getParameter('fdate');
            }
                $param = $param."&fdate=".$fdate;
            }
            if($sf_request->getParameter('tdate')){
                 if(strstr($sf_request->getParameter('tdate'), '/')){
                $tdate = explode('/',$sf_request->getParameter('tdate'));
                $tdate = $tdate[2]."-". $tdate[0]."-". $tdate[1];
                 }else{
                    $tdate =  $sf_request->getParameter('tdate');
                 }
                $param = $param."&tdate=".$tdate;


            }
            //echo $param; echo url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().$param);//exit;

            ?>
            <tr><td colspan="7">
                    <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().$param), 'search_results')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

            </div></td></tr>



                <?php }
            else { ?>
            <tr><td  align='center' class='error' colspan="7">No Record Found</td></tr>
            <?php } ?>

        </tbody>

    </table>
</div>
<script>
    function progBarDiv(url, referenceDivId, targetDivId){
        $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        $('#'+referenceDivId).css("display","none");
        $('#'+targetDivId).css("display","inline");
        $('#'+targetDivId).load(url, {byPass:1 },function (data){
            if(data=='logout'){
                $('#'+targetDivId).html('');
                location.reload();
            } });

    }
</script>
