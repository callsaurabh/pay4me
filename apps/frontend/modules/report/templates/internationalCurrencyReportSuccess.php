
<?php /**
 * Template to render data from form
 */ ?>
<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form')  ?>

<div class="wrapForm2">

    <?php //echo $form['_csrf_token'] ?>
    <?php echo form_tag($sf_context->getModuleName() . '/internationalCurrencyReport', 'name=search id=search'); ?>
    <?php echo ePortal_legend('Credit Card Payment Report'); ?>
    <?php echo formRowComplete($form['from_date']->renderLabel(), $form['from_date']->render(), '', 'from_date', 'err_from_date', 'from_date_row', $form['from_date']->renderError()); ?>
    <?php echo formRowComplete($form['to_date']->renderLabel(), $form['to_date']->render(), '', 'to_date', 'err_to_date', 'to_date_row', $form['to_date']->renderError()); ?>

    <?php echo formRowComplete($form['username']->renderLabel(), $form['username']->render(), '', 'username', 'err_username', 'username_row', $form['username']->renderError()); ?>
    <?php echo formRowComplete($form['merchant_id']->renderLabel(), $form['merchant_id']->render(), '', 'merchant_id', 'err_merchant_id', 'merchant_id_row', $form['merchant_id']->renderError(), 'cRed'); ?>
    <?php echo formRowComplete($form['merchant_service_id']->renderLabel(), $form['merchant_service_id']->render(), '', 'merchant_service_id', 'err_merchant_service_id', 'merchant_service_id_row', $form['merchant_service_id']->renderError(), 'cRed'); ?>




    <div class="divBlock">
        <center>
            <?php if ($form->isCSRFProtected()) : ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>
            <?php echo button_to('Search', '', array('class' => 'formSubmit', 'onClick' => 'validateRequestForm()')); ?>
            </center>
        </div>


    <?php echo form_tag($sf_context->getModuleName() . '/internationalCurrencyReportSearch', 'name=searchfrm id=searchfrm'); ?>
                <input type="hidden" name="uname" id="uname" value="<?php echo $uname; ?>" >
                <input type="hidden" name="tdate" id="tdate" value="<?php echo $tdate; ?>" >
                <input type="hidden" name="fdate" id="fdate" value="<?php echo $fdate; ?>" >
                <input type="hidden" name="mid" id="mid" value="<?php echo $mid; ?>" >
                <input type="hidden" name="msid" id="msid" value="<?php echo $msid; ?>" >
                <input type="hidden" name="isvalidPost" id="isvalidPost" value="<?php echo $isvalidPost; ?>" >
                </form>
                </form>

            </div>
            <div id="search_results"></div>

            <script>

                $(document).ready(function()
                {
                    if($('#isvalidPost').val() ==  'valid'){
                        $.post('internationalCurrencyReportSearch',$("#searchfrm").serialize(), function(data){//alert(data);
                            if(data=='logout'){
                                location.reload();
                            }
                            else {
                                $("#search_results").html(data);
                            }
                        }  );
                    }
                    $('#paidwithvisa_merchant_id').change(function(){
                        var merchant_id = this.value;
                        var module = "paidwithvisa";
                        displayMerchantService(merchant_id,module,"",'<?php echo url_for('report/MerchantService'); ?>');
            //            $.post('../../report/MerchantService',{merchant_id:merchant_id});
            return false;
        });
    });

    function validateRequestForm(){
        $('#search_results').html("");
        $("#err_from_date").html("");
        $("#err_to_date").html("");
        $("#err_merchant_id").html("");
        $("#err_merchant-service_id").html("");
        var err=0;
        var from_date = $('#paidwithvisa_from_date_date').val();
        var to_date = $('#paidwithvisa_to_date_date').val();
        var merchant_id = $("#paidwithvisa_merchant_id").val();
        var merchant_service_id = $('#paidwithvisa_merchant_service_id').val();

        var milli_from_date;
        var milli_to_date;

        var today = new Date();
        var milli_today = today.getTime();

        if(merchant_id=="")
        {
            err = err+1;
            $("#err_merchant_id").html("Please Select Merchant");

        }

        if(merchant_service_id=="")
        {err = err+1;
            $("#err_merchant_service_id").html("Please Select Merchant Service");

        }

        if(from_date != "")
        {
            var from_date_sel = from_date.split("/");
            var from_date_selected = new Date(from_date_sel[2],from_date_sel[0]-1,from_date_sel[1] );
            milli_from_date = from_date_selected.getTime();
            var isFutureDate = milli_today - milli_from_date;
            if(isFutureDate<0)
            {
                $("#err_from_date").html("From Date should not be a future Date");
                return false;
            }
        }
        if(to_date != "")
        {
            var to_date_sel = to_date.split("/");
            var to_date_selected = new Date(to_date_sel[2],to_date_sel[0]-1,to_date_sel[1] );
            milli_to_date = to_date_selected.getTime();
            var isFutureDate = milli_today - milli_to_date;
            if(isFutureDate<0)
            {
                $("#err_to_date").html("To Date should not be a future Date");
                return false;
            }
        }
        if((from_date != "") && (to_date != ""))
        {
            var diff = milli_to_date - milli_from_date;

            if(diff<0)
            {
                err = err+1;
                $("#err_from_date").html("From Date should be less than To Date");
            }
            else
            {
                $("#err_from_date").html("");
            }

            //alert(diff);
            //   err_form[elem]="Please enter date";
        }



        if(err == 0) {
            //                $("#search").submit();
            //            }
            $("#search").submit();

            //                $.post('internationalCurrencyReport',$("#search").serialize(), function(data){//alert(data);
            //                    if(data=='logout'){
            //                        location.reload();
            //                    }
            //                    else {
            //                       // $("#search_results").html(data);
            //                        }
            //                }
            //            );
        }
        else
        {
            return false;
        }
    }



</script>