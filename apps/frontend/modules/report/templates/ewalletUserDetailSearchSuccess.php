<?php use_helper('Pagination'); ?>
<table><tr><td>&nbsp;</td></tr></table>
<?php echo ePortal_listinghead('eWallet User Detail'); ?>
<div class="wrapTable">
    <table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <?php if (($pager->getNbResults()) > 0) { ?>
                    <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                <?php } else { ?>
                    <span class="floatRight">Showing <b>0</b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                <?php } ?>
            </th>
        </tr>
        <?php
        if (($pager->getNbResults()) > 0) {

            if ($account_no == "")
                $account_no = "BLANK";
            if ($email == "")
                $email = "BLANK";
            if ($from_date == "")
                $from_date = "BLANK";
            if ($to_date == "")
                $to_date = "BLANK";
            if ($activaion_on == "")
                $activaion_on = "BLANK";
            if ($orderByBalance == "")
                $orderByBalance = "BLANK";
            if ($currency_id == 0)
                $currency_id = "BLANK";
            ?>
            <tr class="alternateBgColour">
                <th align="right">
                    <span ><div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('csvDiv', 'progBarDiv')">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</div><div id ="progBarDiv"  style="display:none;"></div></span>

                </th>
            </tr>
<?php } ?>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable" style="overflow-x:auto;width:100%;">

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
<?php
if (($pager->getNbResults()) > 0) {
    echo '<table style="width:1920px" border="0" cellpadding="0" cellspacing="0" class="dataTable" >';
    $limit = sfConfig::get('app_records_per_page');
    $page = $sf_context->getRequest()->getParameter('page', 0);
    $i = max(($page - 1), 0) * $limit;
    ?>

            <thead>
                <tr  class="horizontal">
                    <th width = "50px">S.No.</th>
                    <th width = "300px">User name</th>
    <?php if ($currency_id == "BLANK") { ?>
                        <th width = "100px">No of Account</th>
                    <?php } else { ?>
                        <th width = "100px">Balance Amount</th>
                    <?php } ?>
                    <th width = "300px">First Name</th>
                    <th width = "300px">Last Name</th>
                    <th width = "400px">Email</th>
                    <th width = "130px">Mobile</th>
                    <th width = "130px">Work Phone</th>
                    <th width = "70px">Registered On</th>
                    <th width = "150px">Activation Status</th>
                    <th width = "70px">Last Login</th>
                </tr>
            </thead>
            <tbody>
    <?php
    foreach ($pager->getResults() as $result):
        $i++;
        ?>
                    <tr>
                        <td align="left"><?php echo $i ?></td>
                        <td align="left">
                    <?php
                    if ($result->getMasterId() != "") {
                        echo link_to($result->getUname(), url_for('report/ewalletTransDetail?userId=' . $result->getUserId()), array('popup' => array('popupWindow', 'width=890,height=500,left=150,top=0,scrollbars=yes')));
                    } else {
                        echo $result->getUname();
                    }
                    ?>
                        </td>

                            <?php if ($currency_id == "BLANK") { ?>
                            <td align="left"><?php echo $result->getsfGuardUser()->getUserAccountCollection()->count(); ?>&nbsp;</td>
                            <?php } else { ?>

                            <?php if ($result->getCbalance() != '') { ?>
                                <td align="right"><?php echo format_amount($result->getCbalance(), NULL, 1); ?>&nbsp;</td>
                            <?php } else { ?>
                                <td align="right"><?php echo '-'; ?>&nbsp;</td>
                            <?php } ?>

                        <?php } ?>


                        <td align="left"><?php echo $result->getName(); ?>&nbsp;</td>
                        <td align="left"><?php echo $result->getLastName(); ?>&nbsp;</td>
                        <td align="left"><?php echo $result->getEmail(); ?>&nbsp;</td>
                        <td align="left"><?php echo $result->getMobileNo(); ?>&nbsp;</td>
                        <td align="left"><?php echo $result->getWorkPhone(); ?>&nbsp;</td>
                        <td align="left"><?php echo date('d-m-y', strtotime($result->getCreatedAt())); ?>&nbsp;</td>
                        <td align="left"><?php if (0 == $result->getIsact()) {
                    echo "Not Activated";
                } else {
                    echo "Activated";
                } ?>&nbsp;</td>
                        <td align="left"><?php
                if ($result->getLastlogtime() == NULL || $result->getLastlogtime() == '') {
                    echo "-";
                } else {
                    echo date('d-m-y', strtotime($result->getLastlogtime()));
                }
                        ?></td>

                    </tr>
                <?php endforeach; ?>
            </tbody>   

            <tfoot><tr><td colspan="11">

                    </td></tr></tfoot>

            <?php } else { ?>
            <tr><td  align='center'  ><span class="error"><?php echo "No Record Found"; ?></span></td></tr>
        <?php } ?>
        </tbody>

</table>
</div><?php
            if (($pager->getNbResults()) > 0) { ?>
<table border="0" cellpadding="0" cellspacing="0" class="dataTable">
<tfoot>
    <tr><td colspan="12">
            <div class="paging pagingFoot">
            <?php
            
                echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . '?account_no=' . $account_no . '&email=' . $email . '&from=' . $from_date . '&to=' . $to_date . '&activation=' . $activaion_on . '&orderByBalance=' . $orderByBalance . '&currency_id=' . $currency_id), 'search_results');
            
            ?></td>
     </tr>
</tfoot>
</table>
<?php } ?>



<script>
    function progBarDiv(referenceDivId, targetDivId){
        $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        $('#'+referenceDivId).css("display","none");
        $('#'+targetDivId).css("display","inline");

        var accountNo = $('#userDetail_account_no').val();
       
        var Email = $('#userDetail_email').val();
        var fromDate = $('#userDetail_from_date').val();
        var toDate = $('#userDetail_to_date').val();
        var activation = $('#userDetail_activation').val();
        var orderByBalance = $('#userDetail_orderByBalance').val();
        var currency_id = $('#userDetail_currency_id').val();
        var url1 = "<?php echo url_for('report/ewalletUserCSV'); ?>";
        $.post(url1, { account_no: accountNo,email: Email,from: fromDate, to: toDate, activation:activation,orderByBalance:orderByBalance,currency_id:currency_id,byPass:1},
        function(data){
            if(data=='logout'){
                $('#'+targetDivId).html('');
                location.reload();
            }else{
                $('#'+targetDivId).html(data);
            }
        });


    }
</script>
