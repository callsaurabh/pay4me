<?php echo ePortal_pagehead(" "); ?>
<?php // use_helper('Form') ?>


<div class='wrapForm2'>

    <?php echo form_tag('report/ewalletHostAccount',array('name'=>'pfm_report_form','method'=>'post','id'=>'pfm_report_form')) ?>
    <?php
    echo ePortal_legend('eWallet Host Account Report Section');

    echo $form;
    ?>

    <div class="divBlock">
        <center id="multiFormNav">
        <input type="submit" name="submit" value="Generate Report"  class="formSubmit">
    </div>
 </div>
<div class="load_overflow">
<span id="search_results1"></span>
<span id="search_results2"></span>
</div>

<script>
<?php
if("valid" == $formValid )
{
    ?>
        $(document).ready(function()
        {
            displayReport();
        });
    <?php
}
?>

    function displayReport(){
        var err = 0;
        if(err == 0){
            $.post('ewalletHostAccountDebitSearch',$("#pfm_report_form").serialize(), function(data){
                if(data == "logout"){
                    location.reload();
                }else{
                    $("#search_results2").html(data);
                }
            });

            $.post('ewalletHostAccountCreditSearch',$("#pfm_report_form").serialize(), function(data){
                if(data == "logout"){
                    location.reload();
                }else{
                    $("#search_results1").html(data);
                }
            });
        }


    }

</script>