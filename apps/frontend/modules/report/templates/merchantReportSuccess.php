<?php echo ePortal_pagehead(" ");
      //use_helper('Form');
 ?>
<div class="wrapForm2">
    <?php echo form_tag('report/merchantReportSearch', array('name'=>'pfm_report_form','id'=>'pfm_report_form', 'onsubmit' => 'return validate_report_index_form(this);')) ?>
    <div>
        <?php
        echo ePortal_legend(__('Merchant Summary Report'));
        $arrUserGroup = array();
        $arrUserGroup = array(sfConfig::get('app_pfm_role_admin'),sfConfig::get('app_pfm_role_report_portal_admin'), sfConfig::get('app_pfm_role_report_admin'), sfConfig::get('app_pfm_role_ewallet_user'), sfConfig::get('app_pfm_role_merchant'));
        echo formRowFormatRaw($form['merchant']->renderLabel(), $form['merchant']->render());
        echo formRowFormatRaw($form['service_type']->renderLabel(), $form['service_type']->render());

        if (in_array($userGroup, $arrUserGroup)) {
            echo formRowFormatRaw($form['payment_mode']->renderLabel(), $form['payment_mode']->render());
            echo "<div id = 'bank_and_branch' name = 'bank_and_branch' style = 'display:none'>";
            include_component('bank', 'list', array('bank' => 'banks', 'country' => 'country_id', 'branch' => 'bank_branches'));
            echo "</div>";
        } else {
            echo formRowFormatRaw($form['payment_mode']->renderLabel(), $form['payment_mode']->render());
            include_component('bank', 'list', array('bank' => 'banks', 'country' => 'country_id', 'branch' => 'bank_branches'));
        }
        if (settings::isMultiCurrencyOn()) {
            echo formRowFormatRaw($form['currency_id']->renderLabel(), $form['currency_id']->render());
        }
        echo formRowComplete($form['from']->renderLabel(), $form['from']->render(),'','from_date','err_from_date','from_date_row');
        echo formRowComplete($form['to']->renderLabel(), $form['to']->render(),'','to_date','err_to_date','to_date_row');
        echo formRowFormatRaw($form['payment_status']->renderLabel(), $form['payment_status']->render());
        echo formRowFormatRaw($form['disable_bank']->renderLabel(), $form['disable_bank']->render());
        echo formRowFormatRaw('', $form['report_label']->render());
        echo $form['_csrf_token']->render();
        ?>
        <div class="divBlock">
            <center>
                <input type="submit" value="Generate Report" class="formSubmit"  >
            </center>
        </div>
    </div>
    </form>
</div>

<script type="text/javaScript">



    $(document).ready(function() {

        var bank = $('#banks').val();
        var country = $('#country_id').val();
        var branch = $('#bank_branches').val();

        var url = "<?php echo url_for("report/getBankTeller"); ?>";

        if(bank!=''&& country!='' && branch!='')
            $("#teller_id").load(url, {
                bank: bank,country: country,branch: branch
            },function (data){

        });
        $("#payment_mode").change(function()
        {
            if($("#payment_mode").val()==1)
                $('#bank_teller').show();
            else
                $('#bank_teller').hide();
        });

        $("#bank_branches").change(function()
        {
            getTeller();
        });

        $("#country_id").change(function()
        {
            var url = "<?php echo url_for("report/getSelectedCurrency"); ?>";
            var countryId = $("#country_id").val();

            $.post(url,{country: countryId},function (data){
                if(data){
                    $("#currency_id").val(data);
                }
            });

            getTeller();
        });

        $("#banks").change(function()
        {
            getTeller();
        });
    });

    function getTeller() {
        var bank = $('#banks').val();
        var country = $('#country_id').val();
        var branch = $('#bank_branches').val();

        var url = "<?php echo url_for("report/getBankTeller"); ?>";
        $("#teller_id").load(url, {
            bank: bank,country: country,branch: branch
        },function (data){

        });
    }
     window.onload = set_service_type();
 </script>
