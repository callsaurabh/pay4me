<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<div class='dlForm'>
  <fieldset>
    <?php
          echo ePortal_legend('Branch Transaction Report -->  All Transactions');
    ?>
  </fieldset>
</div>

<div class="paging pagingHead">
  <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
  <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
  <br class="pixbr" />
</div>
<div align="center" style="width:870px;overflow:auto;">
<table class="tGrid" border = "0px">
  <thead>
    <tr align = "center">
      <th width = "1%">S.No.</th>
      <th width = "40%">Branch Name</th>
      <th width = "10%">No. of transactions</th>
      
    </tr>
  </thead>
  <tbody>
    <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
      foreach ($pager->getResults() as $result) { 
        $i++;
        ?>
    <tr align = "center">
      <td><?php echo $i; ?></td>
      <td><?php echo $result->getBankBranchName(); ?></td>
      <td><?php echo $result->getNoOfTransactions(); ?></td>
      <?php
      }
     ?>
    </tr>
  <?php }else{ ?>
  <tr><td colspan="8"><?php echo "No Record Found"; ?></td></tr>
   <?php } ?>
  </tbody>

</table>
</div>

<div class="paging pagingFoot"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?>
</div>
<div class="paging pagingFoot"><?php  echo button_to('Cancel','',array('onClick'=>'location.href=\''.url_for('report/branchReport').'\''));?></div>