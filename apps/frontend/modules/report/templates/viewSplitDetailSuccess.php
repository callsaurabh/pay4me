<?php use_helper('Pagination');  ?>

<?php echo ePortal_pagehead('Split Details'); ?>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th width="100%" >
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
        <thead>
            <tr  class="horizontal">
                <th>S.No.</th>
                <th>Account Number</th>
                <th nowrap>Account Name</th>
                <th nowrap>Amount (<?php echo getCurrencyImage($currencyId);?>)</th>
            </tr>
        </thead>
        <tbody>
            <?php

            if(($pager->getNbResults())>0) {
                $limit = sfConfig::get('app_records_per_page');
                $i = max(($page-1),0)*$limit ;
                foreach ($pager->getResults() as $result):
                $i++;
                $collectinObj = $result->getEpMasterAccountTo()->getUserAccountCollection();
                ?>


            <tr  class="alternateBgColour">
                <td align="center"><?php echo $i; ?></td>
                <td align="center"><?php echo $result->getEpMasterAccountTo()->getAccountNumber() ?></td>
                <td align="center"><?php echo $result->getEpMasterAccountTo()->getAccountName();//(is_object($collectinObj->getFirst())) ? $collectinObj->getFirst()->getsfGuardUser()->getUsername() : "" ; ?></td>
                <td align="center"><?php echo format_amount($result->getAmount(),'',1) ?></td>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4">
                    <div class="paging pagingFoot">
                        <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?id='.$merchantRequestId));?>
                </div></td>
            </tr>
            <?php }
        else { ?>
            <tr><td  align='center' class='error' colspan="7">No Record Found</td></tr>
            <?php } ?>

        </tbody>

    </table>
</div>
