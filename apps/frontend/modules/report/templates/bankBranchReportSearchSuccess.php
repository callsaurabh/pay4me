<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>


<?php echo ePortal_listinghead('Bank Branch Transaction Report -->  All Transactions'); ?>

<?php if(($pager->getNbResults())>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <div id = "csvDiv1" style="cursor:pointer;" onclick="javascript:progBarDiv('bankBranchReportCsv', 'csvDiv1', 'progBarDiv1')">
        <span class="r" >Click here to download data in  <font color="red"><b><u>CSV</u></b></font> Format</span></div>
        <div id ="progBarDiv1"  style="display:none;"></div>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<?php } ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th>
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>
<div align="center" style="overflow:auto;">
<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr align = "center" class="horizontal">
      <th width = "1%">S.No.</th>
      <th width = "18%">Bank</th>
      <th width = "18%">Branch</th>
      <th width = "18%">Branch Code</th>
      <th width = "12%">Username</th>
      <th width = "16%">No. of Transaction</th>
      <th width = "18%">Total Paid Amount (<?php echo getCurrencyImage($postDataArray['currency']);?>)</th>
      <th width = "16%"><?php echo $merchantName;?> Amount</th>
      <th width = "18%">Pay4Me Amount (<?php echo getCurrencyImage($postDataArray['currency']);?>)</th>
      <th width = "30%">Bank Amount (<?php echo getCurrencyImage($postDataArray['currency']);?>)</th>
    </tr>
  </thead>
  <tbody>
    <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_per_page_records');
      $page = $sf_context->getRequest()->getParameter('page',0);
      $i = max(($page-1),0)*$limit ;
//      print_r($pager->getResults()); die;
$originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false); 
      foreach ($pager->getResults() as $result) {
        $i++;
        ?>
    <tr align = "center">
      <td><?php echo $i; ?></td>
      <td><?php echo $result->getBankname(); ?></td>
      <td width = "18%"><?php echo $result->getBankBranchName(); ?></td>
      <td><?php echo $result->getBranchCode(); ?></td>
      <td><?php echo $result->getSfGuardUser(); ?></td>
      <td><?php echo $result->getNoOfTransactions(); ?></td>
      <td><?php echo $result->getAmount(); ?></td>
      <td><?php echo $result->getMerchantAmount(); ?></td>
      <td><?php echo $result->getPayformeAmount(); ?></td>
      <td><?php echo $result->getBankAmount(); ?></td>
      <?php
      }
Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);
     ?>
    </tr>
    <tr align = "center" class="alternateBgColour">
      <th colspan="5" align="right">Grand Total</th>
      <td><?php  echo $grandTotalTransactions; ?></td>
      <td width = "18%"><?php echo number_format($grandTotalAmount,2); ?></td>

      <td><?php echo number_format($grandNisAmount,2); ?></td>
      <td><?php echo number_format($grandPay4MeAmount,2); ?></td>
      <td><?php echo number_format($grandBankAmount,2); ?></td>
   </tr>
    <?php
    }else{ ?><table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table>
    <?php } ?>

  </tbody>
  <tfoot>
  <tr><td colspan="11"><div class="paging pagingFoot floatRight"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?>
</div></td></tr> </tfoot>
</table>
</div>
</div>


<div class="paging pagingFoot"><center><?php  echo button_to('Cancel','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('report/bankBranchReport').'\''));?></center></div>
</div>
<script>
function progBarDiv(url, referenceDivId, targetDivId)
{
    
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
                             if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              }  $('#'+targetDivId).html(data); });

}
</script>