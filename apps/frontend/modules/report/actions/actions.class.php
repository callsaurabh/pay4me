<?php

/**
 * report actions.
 *
 * @package    mysfp
 * @subpackage report
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class reportActions extends sfActions {

    public static $DETAILED_REPORT = "/report/detailed/bankPaymentList";
    public static $BRANCH_REPORT = "/report/detailed/allBranchBankUserList";
    public static $BANK_BRANCH_TRANSACTION = "/report/allBankBranchTransactionReport";
    public static $BANK_TRANSACTION = '/report/allBankTransactionReport';
    public static $eWallet_RECHARGE_REPORT = '/report/eWalletRechargeReport';
    public static $eWallet_USER_DETAIL = "/report/ewalletUserDetail";
    public static $bank_branch_user = "/report/bankBranchUser";
    public static $eWalletFinancialReport = "/report/eWalletFinancialReport";
    public static $audit_trail = "/report/auditTrail";
    public static $PAIDBY_CARD = "/report/paidByCardReport";
    public static $internationalCurrencyReport = "/report/internationalCurrencyReport";
    public static $Merchant_SQL = "/report/merchantSql/";
    public static $MerchantPdf = "/MerchantPdf/";
    public static $SPLIT_PAYMENT_REPORT = "/report/splitPaymentReport";
    public static $MERCHANT_SUMMARY_REPORT = '/report/MerchantSummaryReport';
    public static $TEST_BANK_REPORT  = "/report/testBankReport";
    public static $MERCHANT_BANK_REPORT  = "/report/listMerchantPerBankReport";
    public static $EWALLET_USER_REPORT  = "/report/ewalletUserReport";
    //public static $MERCHANT_TRANSACTION = '/report/allMerchantTransactionReport';
    public static $Collection_Summary = '/report/allCollectionSummaryReport/';
    public static $AVC_REPORT_FOLDER = '/report/avcReport';
  
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {

        // Check user permission
        if (!$this->userPermission(true)) {
            $this->forward('sfGuardAuth', 'secure');
        }
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $this->report_label = $request->getParameter('label');
        $bank_details = $pfmHelperObj->getUserAssociatedBank();
        if (count($bank_details) > 0) {
            $bank_details_id = $bank_details['id'];
        } else {
            $bank_details_id = 0;
        }
        $this->form = new MerchantReportForm('',array('bank_details_id' => $bank_details_id,'userGroup'=>$this->userGroup,'report_label'=>$this->report_label));
         if($request->isMethod('post')){
            $this->form->bind($request->getPostParameters());
            if($this->form->isValid()) {
              $this->forward($this->moduleName,'search');
            }
        }
    }

    public function executeSearch(sfWebRequest $request) {
        $this->err = "";
        try {
            ini_set('memory_limit', '1024M');
            ini_set("max_execution_time", "64000");
            $this->userGroup = "";
            $pfmHelperObj = new pfmHelper();
            $this->userGroup = $pfmHelperObj->getUserGroup();

            $this->postDataArray = array();
            if ($request->isMethod('post')) {
                unset($_SESSION['pfm']['reportData']);
                $_SESSION['pfm']['reportData'] = $request->getPostParameters();
                $this->postDataArray = $_SESSION['pfm']['reportData'];
            } else {
                $this->postDataArray = $_SESSION['pfm']['reportData'];
            }
            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                $this->postDataArray = $_SESSION['pfm']['reportData'];
            }


            if ($this->userGroup == sfConfig::get('app_pfm_role_bank_admin') || $this->userGroup == sfConfig::get('app_pfm_role_report_bank_admin')
                    || $this->userGroup == sfConfig::get('app_pfm_role_bank_branch_user') || $this->userGroup == sfConfig::get('app_pfm_role_bank_branch_report_user')) {
                //  $bank_name  = $this->getUser()->getAssociatedBankName();
                //  if($bank_name != $this->postDataArray['banks']) {
                $logged_in_user = $this->getUser()->getGuardUser()->getUsername();
                //  sfContext::getInstance()->getLogger()->debug("unauthorized search by user:". $logged_in_user ." changed bank name to:". $this->postDataArray['banks'] );
                //   throw new Exception("You are not authorized to view the search criteria");
                //   }
            }

            if ($this->userGroup == sfConfig::get('app_pfm_role_bank_branch_user') || $this->userGroup == sfConfig::get('app_pfm_role_bank_branch_report_user')) {
                // $branch_name = $this->getUser()->getAssociatedBankBranchName();
                //   if($branch_name != $this->postDataArray['bank_branches']) {
                $username = $this->getUser()->getGuardUser()->getUsername();
                //   sfContext::getInstance()->getLogger()->debug("unauthorized search by user:". $username ." changed branch name to:". $this->postDataArray['bank_branches'] );
                //   throw new Exception("You are not authorized to view the search criteria");
                //}
            }
            $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
            $groupId = Settings::getGroupIdByGroupname(sfConfig::get('app_pfm_role_ewallet_user'));
            $isMerchantEwalletConfigured = Doctrine::getTable('Merchant')->isMerchantEwalletConfigured($userId);
//            echo "<pre>";
//            print_r('->useQueryCache(new Doctrine_Cache_Apc())');
//            exit;
            $isMerchantEwalletUser = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId, $groupId);

            if ($this->postDataArray['merchant'] == "" && $isMerchantEwalletConfigured && $isMerchantEwalletUser) {
                $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', '', $userId, $groupId);
                $merchantArray = array();
                if (count($serviceTypeArray)) {
                    foreach ($serviceTypeArray as $key => $val) {
                        $merchantArray[] = $key;
                    }
                }
                $this->postDataArray['merchant'] = $merchantArray;
            }

            $this->report_details_list = MerchantRequestTable::getReportDetails($this->postDataArray);

            $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
            $reports = $this->report_details_list->execute();
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);

            $grandTotalAmount = '';
            $grandTotalTransactions = '';

            foreach ($reports as $report) {
                $grandTotalAmount = $grandTotalAmount + $report->amount;
                if ($this->postDataArray['report_label'] == 'summery') {
                    $grandTotalTransactions = $grandTotalTransactions + $report->no_of_transactions;
                }
            }
            $this->grandTotalAmount = $grandTotalAmount;
            $this->grandTotalTransactions = $grandTotalTransactions;

            //        $this->payment_status = '';
            //        if($this->postDataArray['payment_status'] == ''){
            //            $this->success_report_details_list = MerchantRequestTable::getReportDetails($this->postDataArray, $this->payment_status = 0)->execute();
            //            $this->unsuccess_report_details_list = MerchantRequestTable::getReportDetails($this->postDataArray, $this->payment_status = 1)->execute();
            //            $this->setTemplate('allSearch');
            //        }else if($this->postDataArray['payment_status'] == 0 || $this->postDataArray['payment_status'] == 1){
            //            $this->report_details_list = MerchantRequestTable::getReportDetails($this->postDataArray, $this->payment_status);
            //        }
            $userAccObj = new UserAccountManager();
            $this->isBankUser = $userAccObj->IsBankUser();
            $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));

            $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
            $this->pager->setQuery($this->report_details_list);
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);

            $this->pager->setPage($this->page);
            $this->pager->init();
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    public function executeCsv(sfWebRequest $request) {

        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");

        $this->postDataArray = $_SESSION['pfm']['reportData'];
        $grandTotalAmount = 0;
        $grandTotalTransactions = 0;


        // Creating Headers
        if ($this->postDataArray['report_label'] == 'summery' && $this->postDataArray['payment_status'] == 0) {
            $headerNames = array(0 => array('S.No.', 'Service Type', 'Application Type', 'Bank Name', 'Branch Name', 'Branch Code', 'Service Charge', 'Transaction Charge', 'No. of Transactions', 'Amount'));
            $headers = array('service_type', 'application_type', 'bank_name', 'bank_branch_name', 'bank_branch_code', 'tot_service_charge', 'tot_bank_charge', 'no_of_transactions', 'amount');
        } else if ($this->postDataArray['report_label'] == 'summery' && $this->postDataArray['payment_status'] == 1) {
            $headerNames = array(0 => array('S.No.', 'Service Type', 'Application Type', 'No of Transactions', 'Amount'));
            $headers = array('service_type', 'application_type', 'no_of_transactions', 'amount');
        }
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();

        if ($this->userGroup == sfConfig::get('app_pfm_role_bank_branch_user')) {

            if ($this->postDataArray['report_label'] == 'detailed' && $this->postDataArray['payment_status'] == 0) {
                $headerNames = array(0 => array('S.No.', 'Transaction Id', 'Service Type', 'Application Type', 'Bank Name', 'Branch Name', 'Branch Code', 'Service Charge', 'Transaction Charge', 'Date Of Transaction', 'Amount'));
                $headers = array('transaction_id', 'service_type', 'application_type', 'bank_name', 'bank_branch_name', 'bank_branch_code', 'service_charge', 'bank_charge', 'updated_at', 'amount');
            } else if ($this->postDataArray['report_label'] == 'detailed' && $this->postDataArray['payment_status'] == 1) {
                $headerNames = array(0 => array('S.No.', 'Transaction Id', 'Service Type', 'Application Type', 'Date Of Transaction', 'Amount'));
                $headers = array('transaction_id', 'service_type', 'application_type', 'updated_at', 'amount');
            }
        } else {
            if ($this->postDataArray['report_label'] == 'detailed' && $this->postDataArray['payment_status'] == 0) {
                $headerNames = array(0 => array('S.No.', 'Transaction Id', 'Tellers Id', 'Service Type', 'Application Type', 'Bank Name', 'Branch Name', 'Branch Code', 'Service Charge', 'Transaction Charge', 'Date Of Transaction', 'Amount'));
                $headers = array('transaction_id', 'TellersId', 'service_type', 'application_type', 'bank_name', 'bank_branch_name', 'bank_branch_code', 'service_charge', 'bank_charge', 'updated_at', 'amount');
            } else if ($this->postDataArray['report_label'] == 'detailed' && $this->postDataArray['payment_status'] == 1) {
                $headerNames = array(0 => array('S.No.', 'Transaction Id', 'Tellers Id', 'Service Type', 'Application Type', 'Date Of Transaction', 'Amount'));
                $headers = array('transaction_id', 'TellersId', 'service_type', 'application_type', 'updated_at', 'amount');
            }
        } /*
          $records = MerchantRequestTable::getCsvReportDetails($this->postDataArray);

          $i=0;
          while($row = mysql_fetch_array($records)) {
          for($index=0; $index<count($headers);$index++){
          $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
          }
          $grandTotalAmount = $grandTotalAmount + $row['amount'];
          if($this->postDataArray['report_label'] == 'summery') {
          $grandTotalTransactions = $grandTotalTransactions + $row['no_of_transactions'];
          }
          $i++;
          } */
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $groupId = Settings::getGroupIdByGroupname(sfConfig::get('app_pfm_role_ewallet_user'));
        $isMerchantEwalletConfigured = Doctrine::getTable('Merchant')->isMerchantEwalletConfigured($userId);
        $isMerchantEwalletUser = Doctrine::getTable('sfGuardUserGroup')->isMerchantEwalletUser($userId, $groupId);

        if ($this->postDataArray['merchant'] == "" && $isMerchantEwalletConfigured && $isMerchantEwalletUser) {
            $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', '', $userId, $groupId);
            $merchantArray = array();
            if (count($serviceTypeArray)) {
                foreach ($serviceTypeArray as $key => $val) {
                    $merchantArray[] = $key;
                }
            }
            $this->postDataArray['merchant'] = $merchantArray;
        }
        $reports = MerchantRequestTable::getReportDetails($this->postDataArray);
        $reports = $reports->execute();
        $i = 0;
        foreach ($reports as $report) {
            for ($index = 0; $index < count($headers); $index++) {
                $recordsDetails[$i][$headers[$index]] = $report[$headers[$index]];
            }
            $grandTotalAmount = $grandTotalAmount + $report->amount;
            if ($this->postDataArray['report_label'] == 'summery') {
                $grandTotalTransactions = $grandTotalTransactions + $report->no_of_transactions;
            }
            $i++;
        }
        $recordsDetails[$i]['Grand Total'] = number_format($grandTotalAmount, 2);
        $recordsDetails[$i]['No of transactions'] = $grandTotalTransactions;
        $csvFileName='';
        if($this->postDataArray['report_label']== 'summery'){
          $csvFileName =  'summery_report';
        }elseif($this->postDataArray['report_label']== 'detailed'){
             $csvFileName =  'detailed_report';
        }else{
             $csvFileName = "BankPaymentList";
        }

        if (count($recordsDetails) > 0) {
            $arrList = $this->getXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, '/report/detailed/bankPaymentList/', $csvFileName);
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "DETAILED_REPORT";
        } else {
            $this->redirect_url('index.php/report/search');
        }

        $this->setLayout(false);
    }

    public function executeChainServiceType(sfWebRequest $request) {
        $this->serviceType = Doctrine::getTable('MerchantService')->getServiceTypes($request->getParameter('id'));
    }

    public function executeChainBranchType(sfWebRequest $request) {
        $this->bank_branches = Doctrine::getTable('BankBranch')->getBranchTypes($request->getParameter('id'));
    }

    //    public function executeChainBankBranchUserType(sfWebRequest $request){
    //        $this->bank_branch_user = Doctrine::getTable('BankUser')->getBankBranchUserTypes($request->getParameter('id'));
    //    }

    public function executeDownloadCsv(sfWebRequest $request) {
        $fileName = $request->getParameter('fileName') . '.csv';
        $folder = $request->getParameter('folder');
        #sfLoader::loadHelpers('Asset');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
        //        print $$folder;
        $folder_path = self::$$folder;
        $filePath = addslashes(sfConfig::get('sf_web_dir') . '/uploads' . $folder_path . '/' . $fileName);
        $this->setLayout(false);
        sfConfig::set('sf_web_debug', false);

        // Adding the file to the Response object
        // $this->getResponse()->setHttpHeader("Content-Length: ",filesize($filePath),TRUE);
        //$this->getResponse()->setHttpHeader('Content-type','application/csv',TRUE);
        $this->getResponse()->setHttpHeader('Content-type', 'application/force-download');
        $this->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileName}", TRUE);
        $this->getResponse()->sendHttpHeaders();
        $this->getResponse()->setContent(file_get_contents($filePath));
        return sfView::NONE;
    }

    public function executeDownloadPdf(sfWebRequest $request) {
        $fileName = $request->getParameter('fileName') . '.pdf';
        $folder = $request->getParameter('folder');
        #sfLoader::loadHelpers('Asset');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
        //        print $$folder;
        $folder_path = self::$$folder;
        $filePath = addslashes(sfConfig::get('sf_web_dir') . '/uploads' . $folder_path . '/' . $fileName);
        $this->setLayout(false);
        sfConfig::set('sf_web_debug', false);

        // Adding the file to the Response object
        // $this->getResponse()->setHttpHeader("Content-Length: ",filesize($filePath),TRUE);
        // $this->getResponse()->setHttpHeader('Content-type','application/csv',TRUE);
        $this->getResponse()->setHttpHeader('Content-type', 'application/force-download');
        $this->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileName}", TRUE);
        $this->getResponse()->sendHttpHeaders();
        $this->getResponse()->setContent(file_get_contents($filePath));
        return sfView::NONE;
    }

    public function executeViewReportDetail(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = array();

        $id = $this->getRequestParameter('id');
        $transId = $this->getRequestParameter('transId');
        $reportType = $this->getRequestParameter('type');

        $results = Doctrine::getTable('MerchantRequest')->getMerchantRequestDetails($id);
        $results[0]['merchant_service_id'];
        $results[0]['bank_branch_id'];

        $postDataArray['transId'] = $transId;
        $postDataArray['id'] = $id;
        $postDataArray['report_label'] = $reportType;
        $postDataArray['merchant_service_id'] = $results[0]['merchant_service_id'];
        $postDataArray['currency_id'] = $results[0]['currency_id'];
        $postDataArray['bank_branch_id'] = $results[0]['bank_branch_id'];
        //   $postDataArray['banks'] = '';

        $qt = Doctrine::getTable('MerchantRequest')->getReportDetails($postDataArray);

        $this->result = $qt->fetchOne();
        $this->reportType = $reportType;

        $this->setLayout(false);
    }

    public function executeBranchReport(sfWebRequest $request) {

        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $this->report_label = $request->getParameter('label');
        $this->form = new branchReportForm(array(),array('userGroup'=>$this->userGroup,'report_label'=>$this->report_label));

    }

    public function executeBranchReportSearch(sfWebRequest $request) {

        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();

        $this->postDataArray = array();
        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $request->getPostParameters();
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        } else {
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        $report_details_list = MerchantRequestTable::getBankBranchReportDetails($this->postDataArray);


        $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($report_details_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeBranchReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = $_SESSION['pfm']['reportData'];
        $grandTotalAmount = 0;

        // Creating Headers
        $headerNames = array(0 => array('S.No.', 'Transaction Reference', 'Business Office Id', 'Tellers Id', 'Applicants Id', 'Applicants Name', 'Transaction Date', 'Amount'));
        $headers = array('TxnRef', 'BusinessOfficeId', 'TellersId', 'ApplicantsId', 'ApplicantsName', 'TransactionDate', 'amount');

        $records = MerchantRequestTable::getAllBranchBankUserReportDetails($this->postDataArray);
        $i = 0;
        while ($row = mysql_fetch_array($records)) {
            for ($index = 0; $index < count($headers); $index++) {
                $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
            }
            $grandTotalAmount = $grandTotalAmount + $row['amount'];
            $i++;
        }
        $recordsDetails[$i]['Grand Total'] = $grandTotalAmount;
        if (count($recordsDetails) > 0) {
            $arrList = $this->getXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, "/report/detailed/allBranchBankUserList/", "AllBranchBankUserList");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "BRANCH_REPORT";
            $this->setTemplate('csv');
        } else {
            $this->redirect_url('index.php/report/search');
        }
        $this->setLayout(false);
    }

    /////////////////////////merchant report//////////////

    public function executeMerchantReport(sfWebRequest $request) {

      $this->userGroup = "";
      $pfmHelperObj = new pfmHelper();
      $userGroup = $this->userGroup = $pfmHelperObj->getUserGroup();
      if($userGroup == "merchant"){
        $userId=$this->getUser()->getGuardUser()->getid();
        $merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
      }

      $bank_details = $pfmHelperObj->getUserAssociatedBank();
      if (count($bank_details) > 0) {
        $bank_details_id = $bank_details['id'];
      } else {
        $bank_details_id = 0;
      }
      if(isset($merchant_id)){
        $this->form = new MerchantReportFormNew('',array('bank_details_id' => $bank_details_id,'userGroup'=>$this->userGroup,'merchant_id' =>$merchant_id));
      }else{
      $this->form = new MerchantReportFormNew('',array('bank_details_id' => $bank_details_id,'userGroup'=>$this->userGroup));
      }
      $this->form->setDefault('report_label', $request->getParameter('label'));
    }

    public function executeMerchantReportSearch(sfWebRequest $request) {

      ini_set('memory_limit', '1024M');
      ini_set("max_execution_time", "64000");
      $this->userGroup = "";
      $pfmHelperObj = new pfmHelper();
      $this->userGroup = $pfmHelperObj->getUserGroup();
      $this->postDataArray = array();
      if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $request->getPostParameters();
            $this->postDataArray = $_SESSION['pfm']['reportData'];
            } else {
                $this->postDataArray = $_SESSION['pfm']['reportData'];
            }
      $this->page = 1;
      if ($request->hasParameter('page')) {
        $this->page = $request->getParameter('page');
        $this->postDataArray = $_SESSION['pfm']['reportData'];
      }

      if ($this->postDataArray['merchant'] != '') {
        $results = Doctrine::getTable('MerchantRequest')->getMerchantName($this->postDataArray['merchant']);
        $this->merchantName = $results[0]['name'];
      } else {
        $this->merchantName = 'All Merchants';
      }


      $this->merchant_report_list = MerchantRequestTable::getAllMerchantReportDetails($this->postDataArray);
      $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
      Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      $reports = $this->merchant_report_list->execute();
      Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);

      $grandTotalAmount = '';
      $grandTotalTransactions = '';
      $grandNisAmount = '';
      $grandPay4MeAmount = '';
      $grandBankAmount = '';

      foreach ($reports as $report) {
        $grandTotalAmount = $grandTotalAmount + $report->amount;
        $grandTotalTransactions = $grandTotalTransactions + $report->no_of_transactions;
        $grandNisAmount = $grandNisAmount + $report->merchant_amount;
        $grandPay4MeAmount = $grandPay4MeAmount + $report->payforme_amount;
        $grandBankAmount = $grandBankAmount + $report->bank_amount;
      }
      $this->grandTotalAmount = $grandTotalAmount;
      $this->grandTotalTransactions = $grandTotalTransactions;
      $this->grandNisAmount = $grandNisAmount;
      $this->grandPay4MeAmount = $grandPay4MeAmount;
      $this->grandBankAmount = $grandBankAmount;



      $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));

      $this->pager->setQuery($this->merchant_report_list);

      $this->pager->setPage($this->page);
      $this->pager->init();
    }

    public function executeMerchantReportCsv(sfWebRequest $request) {

      ini_set('memory_limit', '1024M');
      ini_set("max_execution_time", "64000");
      $grandTotalTransactions = 0;
      $grandTotalAmount = 0;
      $grandNisAmount = 0;
      $grandPay4MeAmount = 0;
      $grandBankAmount = 0;
      $this->postDataArray = $_SESSION['pfm']['reportData'];
      $records = MerchantRequestTable::getCSVallMerchantReportDetails($this->postDataArray);

      if ($this->postDataArray['merchant'] != '') {
        $results = Doctrine::getTable('MerchantRequest')->getMerchantName($this->postDataArray['merchant']);
        $merchantName = $results[0]['name'] . ' Amount';
      } else {
        $merchantName = 'All Merchants Amount';
      }

      // Creating Headers
      $headerNames = array(0 => array('S.No.','Merchant', 'Merchant Service', 'No. of Transaction', 'Total Paid Amount', "$merchantName", 'Pay4Me Amount', 'Bank Amount'));
      $headers = array('merchant_name', 'service_name', 'No_of_transaction', 'Total_ammount','merchant_amount', 'pay4me_ammount', 'bank_ammount');

      $i = 0;
      $j=0; // for removing csv merchant name redundancy

      while ($row = mysql_fetch_array($records)) {
           for ($index = 0; $index < count($headers); $index++) {
             if(($index ==0)&&($j>0)){
               $recordsDetails[$i][$headers[$index]] = '';
             }
          else $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
        }

        $grandTotalAmount = $grandTotalAmount + $row['Total_ammount'];
        $grandTotalTransactions = $grandTotalTransactions + $row['No_of_transaction'];
        $grandNisAmount = $grandNisAmount + $row['merchant_amount'];
        $grandPay4MeAmount = $grandPay4MeAmount + $row['pay4me_ammount'];
        $grandBankAmount = $grandBankAmount + $row['bank_ammount'];
        $i++;
        $j++;
      }

      $recordsDetails[$i]['Grand Total'] = $grandTotalAmount;
      $recordsDetails[$i]['No of transactions'] = $grandTotalTransactions;
      $recordsDetails[$i]['Merchant Amount'] = $grandNisAmount;
      $recordsDetails[$i]['Payforme Amount'] = $grandPay4MeAmount;
      $recordsDetails[$i]['Bank Amount'] = $grandBankAmount;

      if (count($recordsDetails) > 0) {
        $arrList = $this->getXLSExportListMerchant($headerNames, $headers, $recordsDetails);
        $file_array = $this->saveExportListFile($arrList, "/report/MerchantSummaryReport/", "MerchantSummaryReport");
        $file_array = explode('#$', $file_array);
        $this->fileName = $file_array[0];
        $this->filePath = $file_array[1];
        $this->folder = "MERCHANT_SUMMARY_REPORT";
      } else {
        $this->redirect_url('index.php/report/merchantReport');
      }$this->setTemplate('csv');
      // $this->setTemplate('bankReportCsv');
      $this->setLayout(false);
    }

    ///////////////////end of merchant report///////////////////////////

    public function executeTestBankReport(sfWebRequest $request) {
      //$this->form = new TestBankReportForm();
    /*   if (!$this->userPermission(true)) {
            $this->forward('sfGuardAuth', 'secure');
        }
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        echo $this->userGroup.'----->';
        $this->report_label = $request->getParameter('label');
        $bank_details = $pfmHelperObj->getUserAssociatedBank();
        if (count($bank_details) > 0) {
            $bank_details_id = $bank_details['id'];
        } else {
            $bank_details_id = 0;
        }
        $this->form = new BankUserReportForm('',array('bank_details_id' => $bank_details_id,'userGroup'=>$this->userGroup,'report_label'=>$this->report_label));
         if($request->isMethod('post')){
            $this->form->bind($request->getPostParameters());
            if($this->form->isValid()) {
              $this->forward($this->moduleName,'search');
            }
        }
         */
    }
    //test bank repot
 public function executeTestBankReportSearch(sfWebRequest $request) {

        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $this->postDataArray = array();
        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $request->getPostParameters();
            $this->postDataArray = $_SESSION['pfm']['reportData'];

        } else {

            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        $report_details_list = BankUserTable::getCustomBankUsers($this->postDataArray);


        $this->pager = new sfDoctrinePager('BankUser', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($report_details_list);
        $this->pager->setPage($this->page);
        $this->pager->init();

    }
        public function executeTestBankReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = $_SESSION['pfm']['reportData'];
        $bank_id = $_SESSION['pfm']['reportData']['banks'];
        $records = BankUserTable::getCustomCsvBankUsers($bank_id);
        // Creating Headers
        $headerNames = array(0 => array('S.No.','Username', 'Name', 'Email Address', 'System Role', 'Branch Name', 'Branch Code', 'User Status', 'Last Login'));
        $headers = array('username', 'name', 'email', 'description', 'branch_name', 'branch_code', 'user_status','last_login');
        $i = 0;
        while ($row = mysql_fetch_array($records)) {
            for ($index = 0; $index < count($headers); $index++) {
                $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
            }
            $i++;
        }
        if (count($recordsDetails) > 0) {
            $arrList = $this->getCustomXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, "/report/testBankReport/", "BankUserReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "TEST_BANK_REPORT";
        } else {
            $this->redirect_url('index.php/report/testBankReport');
        }$this->setTemplate('csv');
        // $this->setTemplate('bankReportCsv');
        $this->setLayout(false);
    }
    
    public function executeAllBankUsersReport(sfWebRequest $request) {
    	$this->form = new AllBankUsersReportForm();

    	}
    	
    public function executeAllBankUsersReportSearch(sfWebRequest $request) {
    		/*
    		  $this->postDataArray = array();
    		if ($request->isMethod('post')) {
    			unset($_SESSION['pfm']['reportData']);
    			$_SESSION['pfm']['reportData'] = $request->getPostParameters();
    			$this->postDataArray = $_SESSION['pfm']['reportData'];
    		} else {
    			$this->postDataArray = $_SESSION['pfm']['reportData'];
    		}
    		$this->page = 1;
    		if ($request->hasParameter('page')) {
    			$this->page = $request->getParameter('page');
    			$this->postDataArray = $_SESSION['pfm']['reportData'];
    		}
    		$report_details_list = BankUserTable::getCustomBankUsers($this->postDataArray);
    	
    	
    		$this->pager = new sfDoctrinePager('BankUser', sfConfig::get('app_records_per_page'));
    		$this->pager->setQuery($report_details_list);
    		$this->pager->setPage($this->page);
    		$this->pager->init();
    	*/
    	}
    	
    public function executeEwalletUserReport(sfWebRequest $request) {
      $this->form = new EwalletUserReportForm();
    }
    
	/** Implementation for report/ewalletUserReportSearch
	 * Developed by :- Deepak Bhardwaj
	**/
    public function executeEwalletUserReportSearch(sfWebRequest $request) {
        $this->postDataArray = array();
        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $request->getPostParameters();
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        } else {
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        $user_type = $this->postDataArray['user_type'];
        $pay4me_user_type = $this->postDataArray['pay4me_user_type'];
        $system_user_type = $this->postDataArray['system_user_type'];
      if($user_type == "system_user") {
      	$obj = UserDetailTable::getFilteredSystemUsers($system_user_type);
      }
      else {
      	$obj = Doctrine::getTable('SfGuardUser')->getEwalletUserListBySignupType($pay4me_user_type);
      }
      $this->user_type = $user_type;
      $this->page = 1;
      if ($request->hasParameter('page')) {
        $this->page = $request->getParameter('page');
      }
      $this->pager = new sfDoctrinePager('sfGuardUser', sfConfig::get('app_records_per_page'));
      $this->pager->setQuery($obj);
      $this->pager->setPage($this->page);
      $this->pager->init();
    }
  
 public function executeEwalletUserReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = $_SESSION['pfm']['reportData'];
        $user_type = $_SESSION['pfm']['reportData']['user_type'];
        $csv_report_link_title = '';
        $pay4me_user = '';
        $system_user_type = '';
        if($user_type == 'system_user')
        	$system_user_type = $_SESSION['pfm']['reportData']['system_user_type'];
        if($user_type == 'pay4me'){
        	 $pay4me_user = $_SESSION['pfm']['reportData']['pay4me_user_type'];
         	if($pay4me_user == 'open_id')
            	$csv_report_link_title = 'OpenIdUsersReport';
         	else
            	$csv_report_link_title = 'Non-OpenIdUsersReport';
        }
        else {
        	if($system_user_type == 'merchant')
        		$csv_report_link_title = 'MerchantUserReport';
        	else if($system_user_type == 'portal_support')
        		$csv_report_link_title = 'PortalSupportUserReport';
        	else if($system_user_type == 'portal_report_admin')
        		$csv_report_link_title = 'PortalReportAdminUserReport';
        	else if($system_user_type == 'report_admin')
        		$csv_report_link_title = 'ReportAdminUserReport';
        	else if($system_user_type == 'support')
        		$csv_report_link_title = 'SupportUserReport';
        	else if($system_user_type == 'portal_admin')
        		$csv_report_link_title = 'PortalAdminUserReport';
        	else 
        		$csv_report_link_title = 'SystemUsersReport';
        }
        $records = UserDetailTable::getCustomEwalletUserCsv($user_type,$pay4me_user,$system_user_type);
        // Creating Headers
        $headerNames = array(0 => array('S.No.','Username', 'Name', 'Email Address', 'mobile_no', 'User Status', 'Last Login'));
        $headers = array('username', 'name', 'email','mobile_no', 'user_status','last_login');
        $i = 0;
        while ($row = mysql_fetch_array($records)) {
            for ($index = 0; $index < count($headers); $index++) {
                $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
            }
            $i++;
        }
        if (count($recordsDetails) > 0) {
            $arrList = $this->getCustomXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, "/report/ewalletUserReport/", $csv_report_link_title);
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "EWALLET_USER_REPORT";
        } else {
            $this->redirect_url('index.php/report/ewalletUserReport');
        }$this->setTemplate('csv');
        // $this->setTemplate('bankReportCsv');
        $this->setLayout(false);
 	}
    //merchantReport
    public function executeBankReport(sfWebRequest $request) {
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $bank_details = $pfmHelperObj->getUserAssociatedBank();
        if (count($bank_details) > 0) {
            $bank_details_id = $bank_details['id'];
        } else {
            $bank_details_id = 0;
        }
        $this->form = new BankReportForm('', array('bank_details_id' => $bank_details_id));
        $this->form->setDefault('report_label', $request->getParameter('label'));
    }

    public function executeBankReportSearch(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $this->postDataArray = array();
        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $postDataArray = $request->getPostParameters();
            $_SESSION['pfm']['reportData'] = $postDataArray['bank_branch_report'];
            if (isset($postDataArray['bank']))
                $_SESSION['pfm']['reportData']['bank'] = $postDataArray['bank'];
            if (isset($postDataArray['country']))
                $_SESSION['pfm']['reportData']['country'] = $postDataArray['country'];
            if (isset($postDataArray['branch']))
                $_SESSION['pfm']['reportData']['branch'] = $postDataArray['branch'];
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }else {
            $this->postDataArray = $_SESSION['pfm']['reportData'];
            // unset($_SESSION['pfm']['reportData']);
        }
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }

        if ($this->postDataArray['merchant'] != '') {
            $results = Doctrine::getTable('MerchantRequest')->getMerchantName($this->postDataArray['merchant']);
            $this->merchantName = $results[0]['name'];
        } else {
            $this->merchantName = 'All Merchants';
        }


        $this->bank_report_list = MerchantRequestTable::getAllBankReportDetails($this->postDataArray);

        $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
        $reports = $this->bank_report_list->execute();
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);

        $grandTotalAmount = '';
        $grandTotalTransactions = '';
        $grandNisAmount = '';
        $grandPay4MeAmount = '';
        $grandBankAmount = '';

        foreach ($reports as $report) {
            $grandTotalAmount = $grandTotalAmount + $report->amount;
            $grandTotalTransactions = $grandTotalTransactions + $report->no_of_transactions;
            $grandNisAmount = $grandNisAmount + $report->merchant_amount;
            $grandPay4MeAmount = $grandPay4MeAmount + $report->payforme_amount;
            $grandBankAmount = $grandBankAmount + $report->bank_amount;
        }
        $this->grandTotalAmount = $grandTotalAmount;
        $this->grandTotalTransactions = $grandTotalTransactions;
        $this->grandNisAmount = $grandNisAmount;
        $this->grandPay4MeAmount = $grandPay4MeAmount;
        $this->grandBankAmount = $grandBankAmount;



        $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));

        $this->pager->setQuery($this->bank_report_list);

        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeBankBranchReport(sfWebRequest $request) {


        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $bank_details = $pfmHelperObj->getUserAssociatedBank();
        if (count($bank_details) > 0) {
            $bank_details_id = $bank_details['id'];
        } else {
            $bank_details_id = 0;
        }
        $this->form = new BankBranchReportForm('', array('bank_details_id' => $bank_details_id));
        $this->form->setDefault('report_label', $request->getParameter('label'));
    }

    public function executeBankBranchReportSearch(sfWebRequest $request) {

        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();

        $this->postDataArray = array();

        if ($request->isMethod('post')) {

            $postDataArray = $request->getPostParameters();
//            $this->postDataArray =$postDataArray['bank_branch_report'];
            $_SESSION['pfm']['reportData'] = $postDataArray['bank_branch_report'];
            if (isset($postDataArray['bank']))
                $_SESSION['pfm']['reportData']['bank'] = $postDataArray['bank'];
            if (isset($postDataArray['country']))
                $_SESSION['pfm']['reportData']['country'] = $postDataArray['country'];
            if (isset($postDataArray['branch']))
                $_SESSION['pfm']['reportData']['branch'] = $postDataArray['branch'];
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }else {
            $this->postDataArray = $_SESSION['pfm']['reportData'];
            // unset($_SESSION['pfm']['reportData']);
        }


        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }

        if ($this->postDataArray['merchant'] != '') {
            $results = Doctrine::getTable('MerchantRequest')->getMerchantName($this->postDataArray['merchant']);
            $this->merchantName = $results[0]['name'];
        } else {
            $this->merchantName = 'All Merchants';
        }

        $this->bank_report_list = MerchantRequestTable::getAllBankBranchReportDetails($this->postDataArray);

        $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
        $reports = $this->bank_report_list->execute();
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);

        $grandTotalAmount = '';
        $grandTotalTransactions = '';
        $grandNisAmount = '';
        $grandPay4MeAmount = '';
        $grandBankAmount = '';

        foreach ($reports as $report) {
            $grandTotalAmount = $grandTotalAmount + $report->amount;
            $grandTotalTransactions = $grandTotalTransactions + $report->no_of_transactions;
            $grandNisAmount = $grandNisAmount + $report->merchant_amount;
            $grandPay4MeAmount = $grandPay4MeAmount + $report->payforme_amount;
            $grandBankAmount = $grandBankAmount + $report->bank_amount;
        }
        $this->grandTotalAmount = $grandTotalAmount;
        $this->grandTotalTransactions = $grandTotalTransactions;
        $this->grandNisAmount = $grandNisAmount;
        $this->grandPay4MeAmount = $grandPay4MeAmount;
        $this->grandBankAmount = $grandBankAmount;



        $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));

        $this->pager->setQuery($this->bank_report_list);

        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeBankBranchReportCsv(sfWebRequest $request) {

        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");

        $grandTotalAmount = '';
        $grandTotalTransactions = '';
        $grandNisAmount = '';
        $grandPay4MeAmount = '';
        $grandBankAmount = '';

        $this->postDataArray = $_SESSION['pfm']['reportData'];
        $records = MerchantRequestTable::getCSVallBankBranchReportDetails($this->postDataArray);

        if ($this->postDataArray['merchant'] != '') {
            $results = Doctrine::getTable('MerchantRequest')->getMerchantName($this->postDataArray['merchant']);
            $merchantName = $results[0]['name'] . ' Amount';
        } else {
            $merchantName = 'All Merchants Amount';
        }
        /* Start of Code for creating the csv file */
        $filename = "AllBankBranchTransactionReport_" . date("YmdH:m:s");
        $headerNames = $headerNames = array(0 => array('S.No.', 'Bank', 'Branch', 'Username', 'No. of Transaction', 'Total Paid Amount', "$merchantName", 'Pay4Me Amount', 'Bank Amount'));
        $headers = array('bankname', 'bank_branch_name', 'username', 'no_of_transactions', 'amount', 'merchant_amount', 'payforme_amount', 'bank_amount');


        $i = 0;
        while ($row = mysql_fetch_array($records)) {
            for ($index = 0; $index < count($headers); $index++) {
                $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
            }
            $grandTotalAmount = $grandTotalAmount + $row['amount'];
            $grandTotalTransactions = $grandTotalTransactions + $row['no_of_transactions'];
            $grandNisAmount = $grandNisAmount + $row['merchant_amount'];
            $grandPay4MeAmount = $grandPay4MeAmount + $row['payforme_amount'];
            $grandBankAmount = $grandBankAmount + $row['bank_amount'];
            $i++;
        }

        $recordsDetails[$i]['Grand Total'] = $grandTotalAmount;
        $recordsDetails[$i]['No of transactions'] = $grandTotalTransactions;
        $recordsDetails[$i]['Merchant Amount'] = $grandNisAmount;
        $recordsDetails[$i]['Payforme Amount'] = $grandPay4MeAmount;
        $recordsDetails[$i]['Bank Amount'] = $grandBankAmount;

        if (count($recordsDetails) > 0) {
            $arrList = $this->getXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, "/report/allBankBranchTransactionReport/", "AllBankBranchTransactionReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "BANK_BRANCH_TRANSACTION";
        } else {
            $this->redirect_url('index.php/report/bankBranchReport');
        }
        $this->setTemplate('csv');

        $this->setLayout(false);
    }

    public function executeBankReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $grandTotalTransactions = 0;
        $grandTotalAmount = 0;
        $grandNisAmount = 0;
        $grandPay4MeAmount = 0;
        $grandBankAmount = 0;
        $this->postDataArray = $_SESSION['pfm']['reportData'];
        $records = MerchantRequestTable::getCSVallBankReportDetails($this->postDataArray);

        if ($this->postDataArray['merchant'] != '') {
            $results = Doctrine::getTable('MerchantRequest')->getMerchantName($this->postDataArray['merchant']);
            $merchantName = $results[0]['name'] . ' Amount';
        } else {
            $merchantName = 'All Merchants Amount';
        }

        // Creating Headers
        $headerNames = array(0 => array('S.No.', 'Bank', 'Username', 'No. of Transaction', 'Total Paid Amount', "$merchantName", 'Pay4Me Amount', 'Bank Amount'));
        $headers = array('bankname', 'username', 'no_of_transactions', 'amount', 'merchant_amount', 'payforme_amount', 'bank_amount');

        $i = 0;
        while ($row = mysql_fetch_array($records)) {
            for ($index = 0; $index < count($headers); $index++) {
                $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
            }
            $grandTotalAmount = $grandTotalAmount + $row['amount'];
            $grandTotalTransactions = $grandTotalTransactions + $row['no_of_transactions'];
            $grandNisAmount = $grandNisAmount + $row['merchant_amount'];
            $grandPay4MeAmount = $grandPay4MeAmount + $row['payforme_amount'];
            $grandBankAmount = $grandBankAmount + $row['bank_amount'];
            $i++;
        }

        $recordsDetails[$i]['Grand Total'] = $grandTotalAmount;
        $recordsDetails[$i]['No of transactions'] = $grandTotalTransactions;
        $recordsDetails[$i]['Merchant Amount'] = $grandNisAmount;
        $recordsDetails[$i]['Payforme Amount'] = $grandPay4MeAmount;
        $recordsDetails[$i]['Bank Amount'] = $grandBankAmount;

        if (count($recordsDetails) > 0) {
            $arrList = $this->getXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, "/report/allBankTransactionReport/", "AllBankTransactionReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "BANK_TRANSACTION";
        } else {
            $this->redirect_url('index.php/report/bankReport');
        }$this->setTemplate('csv');
        // $this->setTemplate('bankReportCsv');
        $this->setLayout(false);
    }


    public function getExportListHeaderInfo($reportLabel, $paymentStatus) {
        $arrHeader = array();
        if ($reportLabel == 'summery' && $paymentStatus == 0) {
            $arrHeader[0][] = "S.No";
            $arrHeader[0][] = "Service Type";
            $arrHeader[0][] = "Application Type";
            $arrHeader[0][] = "Bank Name";
            $arrHeader[0][] = "Branch Name";
            $arrHeader[0][] = "Branch Code";
            $arrHeader[0][] = "No of Transactions";
            $arrHeader[0][] = "Transaction Charge";
            $arrHeader[0][] = "Amount";
        } else if ($reportLabel == 'summery' && $paymentStatus == 1) {
            $arrHeader[0][] = "S.No";
            $arrHeader[0][] = "Service Type";
            $arrHeader[0][] = "Application Type";
            $arrHeader[0][] = "No of Transactions";
            $arrHeader[0][] = "Amount";
        }
        if ($reportLabel == 'detailed' && $paymentStatus == 0) {
            $arrHeader[0][] = "S.No";
            $arrHeader[0][] = "Transaction Id";
            $arrHeader[0][] = "Service Type";
            $arrHeader[0][] = "Application Type";
            $arrHeader[0][] = "Bank Name";
            $arrHeader[0][] = "Branch Name";
            $arrHeader[0][] = "Branch Code";
            $arrHeader[0][] = "Service Charge";
            $arrHeader[0][] = "Transaction Charge";
            $arrHeader[0][] = "Date of Transaction";
            $arrHeader[0][] = "Amount";
        } else if ($reportLabel == 'detailed' && $paymentStatus == 1) {
            $arrHeader[0][] = "S.No";
            $arrHeader[0][] = "Transaction Id";
            $arrHeader[0][] = "Service Type";
            $arrHeader[0][] = "Application Type";
            $arrHeader[0][] = "Date of Transaction";
            $arrHeader[0][] = "Amount";
        }
        return $arrHeader;
    }

    public function getSimplifiedXLSExportList($arrHeader, $headers, $arrList) {
        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        $counterVal = count($arrList);
        $counterValHeader = count($headers);
        for ($k = 0; $k < $counterVal; $k++) {
            $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeader; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }

    public function getXLSExportList($arrHeader, $headers, $arrList) {


        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        $counterVal = count($arrList) - 1;
        $counterValHeader = count($headers);
        for ($k = 0; $k < $counterVal; $k++) {
            $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeader; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        if ($this->postDataArray['report_label'] == 'summery') {
            $counterVal = count($arrHeader[0]) - 1;
            for ($i = 0; $i < $counterVal; $i++) {
                $arrExPortList[$k][$i] = '';
            }

            $arrExPortList[$k][$i - 2] = 'Grand Total ';
            $arrExPortList[$k][--$i] = $arrList[$k]['No of transactions'];
//            $arrExPortList[$k][++$i] = '';
//            $arrExPortList[$k][++$i] = '';
            if ($this->postDataArray['payment_status'] == 0)
                $arrExPortList[$k][++$i] = $arrList[$k]['Grand Total'];
        }
        if ($this->postDataArray['report_label'] == 'detailed') {
            $counterVal = count($arrHeader[0]) - 2;
            for ($i = 0; $i < $counterVal; $i++) {
                $arrExPortList[$k][$i] = '';
            }

            $arrExPortList[$k][$i] = 'Grand Total ';
            $arrExPortList[$k][++$i] = $arrList[$k]['Grand Total'];
        }
        if ($this->postDataArray['report_label'] == "avc_report") {
            $counterVal = count($arrHeader[0]) - 2;
            for ($i = 0; $i < $counterVal; $i++) {
                $arrExPortList[$k][$i] = '';
            }
            $arrExPortList[$k][1] = 'Grand Total '; 
            $arrExPortList[$k][++$i] = $arrList[$k]['No of transactions'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Grand Total'];
        }
        if ($this->postDataArray['report_label'] != 'summery' && $this->postDataArray['report_label'] != 'detailed' && $this->postDataArray['report_label'] != 'avc_report') {
                $counterVal = count($arrHeader[0]) - 6; 
            for ($i = 0; $i < $counterVal; $i++) {
                $arrExPortList[$k][$i] = '';
            }

            $arrExPortList[$k][$i] = 'Grand Total ';    
            $arrExPortList[$k][++$i] = $arrList[$k]['No of transactions'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Grand Total'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Merchant Amount'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Payforme Amount'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Bank Amount'];
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }
    public function getCustomXLSExportList($arrHeader, $headers, $arrList) {


        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        $counterVal = count($arrList) - 1;
        $counterValHeader = count($headers);
        for ($k = 0; $k <= $counterVal; $k++) {
            $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeader; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        if (isset($this->postDataArray['report_label']) && $this->postDataArray['report_label'] != 'summery' && $this->postDataArray['report_label'] != 'detailed' && $this->postDataArray['report_label'] != 'MerchantPerBankReport') {
            $counterVal = count($arrHeader[0]) - 6;
            for ($i = 0; $i < $counterVal; $i++) {
                $arrExPortList[$k][$i] = '';
            }

            //$arrExPortList[$k][$i] = 'Grand Total ';
            $arrExPortList[$k][++$i] = $arrList[$k]['No of transactions'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Grand Total'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Merchant Amount'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Payforme Amount'];
            $arrExPortList[$k][++$i] = $arrList[$k]['Bank Amount'];
        }
        /*if (isset($this->postDataArray['report_label']) && $this->postDataArray['report_label'] == 'MerchantPerBankReport') {
        	$counterVal = count($arrHeader[0]) - 6;
        	for ($i = 0; $i < $counterVal; $i++) {
        		$arrExPortList[$k][$i] = '';
        	}
        }*/
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }

    /**
     * function will save csv file
     *
     * @access public
     * @param array [$arrList] Contain Date to be written in file
     * @param string [$filePath] Contain file path
     * @param string [$fileName] Contain file name
     * @return Message either report generated successfully or not
     * @author Ramandeep Singh
     */
    public function saveExportListFileEwalletRechargeReport($arrList, $filePath, $fileName) {
        try {
            $downold_Path = sfConfig::get('sf_upload_dir') . $filePath;
            $strFile = $fileName;
            $filepath = $downold_Path . $strFile . ".csv";
            $final_path = sfConfig::get('sf_upload_dir') . $filePath;
            if (!is_dir($final_path)) {
                mkdir($final_path);
                chmod($final_path, 0755);
            }
            $strFileName = $this->makeCsvFile($arrList, $filepath, ",", "\""); $mgs = "";
    		if($strFileName){
    			return "Report Generated Successfully";
    		}else{
    			return "Some Error";
    		}
        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }
    public function saveExportListFile($arrList, $filePath, $fileName) {
        try {
            $downold_Path = sfConfig::get('sf_upload_dir') . $filePath;
            $strFile = $fileName . "_" . date("Ymdhis");
            $filepath = $downold_Path . $strFile . ".csv";
            $final_path = sfConfig::get('sf_upload_dir') . $filePath;
            if (!is_dir($final_path)) {
                mkdir($final_path);
                chmod($final_path, 0755);
            }
            $strFileName = $this->makeCsvFile($arrList, $filepath, ",", "\"");
            $arrFileInfo = pathinfo($strFileName);
            $path = "../../uploads/" . $filePath . $arrFileInfo['basename'];
            return $strFile . "#$" . $path;
                            $this->redirect_url($path);

        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }

    /*     * *****************common functions for save the reports data********************** */

    public function makeCsvFile($dataArray, $filename, $delimiter=",", $enclosure="\"") {
        // Build the string

        $fh = fopen($filename, "a");
        // for each array element, which represents a line in the csv file...
        foreach ($dataArray as $line) {
            $string = "";
            // No leading delimiter
            $writeDelimiter = FALSE;

            foreach ($line as $dataElement) {
                // Replaces a double quote with two double quotes
                $dataElement = str_replace("\"", "\"\"", $dataElement);
                $dataElement = str_replace("\n", "", $dataElement);
                $dataElement = str_replace("\r", "", $dataElement);

                // Adds a delimiter before each field (except the first)
                if ($writeDelimiter)
                    $string .= $delimiter;

                // Encloses each field with $enclosure and adds it to the string
                $string .= $enclosure . $dataElement . $enclosure;

                // Delimiters are used every time except the first.
                $writeDelimiter = TRUE;
            }
            // Append new line
            $string .= "\n";

            fwrite($fh, $string);
        } // end foreach($dataArray as $line)
        fclose($fh);
        return $filename;
    }

    /*     * *****************end the common function********************** */

    public function executeMerchantAccountReport(sfWebRequest $request) {

        $this->form = new MerchantAccountReportForm();

        $this->formValid = "";
        if ($request->isMethod('post')) {

            $this->form->bind($request->getParameter('report'));
            if ($this->form->isValid()) {

                $this->formValid = "valid";
            }
        }
    }

    public function executeMerchantAccountReportSearch(sfWebRequest $request) {

        $this->splitform = new MerchantAccountReportForm();
        $this->splitform->bind($request->getParameter('report'));
        $usePager = $request->getParameter('page');
        if ($this->splitform->isValid() || (isset($usePager) )) {
            $params = $request->getParameterHolder()->getAll();


            if (isset($params['report']))
                $params = $params['report'];

            if ($params['from'] != "" && $params['from'] != "BLANK") {
                $this->fromDate = $params['from'];
                $frmArr = explode("-", $this->fromDate);
                $from_date = $this->fromDate;
            } else {
                $from_date = "";
                $this->fromDate = "";
            }
            if ($params['to'] != "" && $params['to'] != "BLANK") {
                $this->toDate = $params['to'];
                $frmArr = explode("-", $this->toDate);
                $to_date = $this->toDate;
            } else {
                $to_date = "";
                $this->toDate = "";
            }
//            if ($params['entry_type'] != "" && $params['entry_type'] != "BLANK") {
//                $entry_type = $params['entry_type'];
//            } else {
//                $entry_type = "";
//            }

            $this->merchant_id = $params['merchant_id'];

           // $this->entry_type = $entry_type;
            $acctManagerObj = new EpAccountingManager;
            $acctSummaryObj = $acctManagerObj->getVirtualAccQuery($from_date, $to_date, $this->merchant_id, '');

            //echo "<pre>"; print_r($acctSummaryObj); die;

            $this->page = 1;

            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }


            $this->pager = new sfDoctrinePager('EpMasterAccount', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($acctSummaryObj);

            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {

            foreach ($this->splitform->getFormFieldSchema() as $name => $formField) {
                echo "<div id=error_list class=error_list >" . $formField->renderError() . "</div>";
            }
            echo "<div id=error_list class=error_list >" . $this->splitform->renderGlobalErrors() . "</div>";
            die;
        }
        //}
    }

    public function executeGetVirtualAccLedgerDetails(sfWebRequest $request) {
        $this->setLayout('layout_popup');
        $postDataArray = $request->getParameterHolder()->getAll();
        if ($postDataArray['to_date'] != "BLANK") {
            $to_date = $postDataArray['to_date'];
        } else {
            $to_date = "";
        }

        if ($postDataArray['from_date'] != "BLANK") {
            $from_date = $postDataArray['from_date'];
        } else {
            $from_date = "";
        }

        if (isset($postDataArray['master_account_id'])) {
            $master_account_id = $postDataArray['master_account_id'];
        } else {
            $master_account_id = "";
        }

        if ($postDataArray['entry_type'] != "BLANK") {
            $entry_type = $postDataArray['entry_type'];
        } else {
            $entry_type = "";
        }

        $merchant_id = $postDataArray['merchant_id'];

        $this->entry_type = $entry_type;
        $acctManagerObj = new EpAccountingManager;
        $acctSummaryObj = $acctManagerObj->getVirtualAccDetails($from_date, $to_date, $master_account_id, $entry_type, $merchant_id, "Query");


        //       $all_results = $acctSummaryObj->execute();
        //       $creditAmt = 0;
        //       $debitAmt = 0;
        //       foreach($all_results  as $res){
        //          if($res->entry_type == "credit")
        //           $creditAmt=$creditAmt+$res->amount;
        //
        //          if($res->entry_type == "debit")
        //           $debitAmt=$debitAmt+$res->amount;
        //       }
        //
        //       $this->creditAmt=$creditAmt."<br>";
        //       $this->debitAmt=$debitAmt;


        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($acctSummaryObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeGetP4mVirtualAccLedgerDetails(sfWebRequest $request) {
        $this->setLayout('layout_popup');
        $postDataArray = $request->getParameterHolder()->getAll();
        if ($postDataArray['to_date'] != "BLANK") {
            $to_date = $postDataArray['to_date'];
        } else {
            $to_date = "";
        }

        if ($postDataArray['from_date'] != "BLANK") {
            $from_date = $postDataArray['from_date'];
        } else {
            $from_date = "";
        }

        if (isset($postDataArray['master_account_id'])) {
            $master_account_id = $postDataArray['master_account_id'];
        } else {
            $master_account_id = "";
        }

        if ($postDataArray['entry_type'] != "BLANK") {
            $entry_type = $postDataArray['entry_type'];
        } else {
            $entry_type = "";
        }

        $merchant_id = $postDataArray['merchant_id'];

        $this->entry_type = $entry_type;
        $acctManagerObj = new EpAccountingManager;
        $acctSummaryObj = $acctManagerObj->getP4mVirtualAccDetails($from_date, $to_date, $master_account_id, $entry_type, $merchant_id, "Query");
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('PaymentSplitDetails', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($acctSummaryObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeIndEwalletAccounts(sfWebRequest $request) {
        // $this->entry_type_choices = array('' => '-- All Transaction List --', 'debit' => '-- Debit --', 'credit' => '-- credit --');
        $this->form = new IndEwalletAccountsForm();
    }

    public function executeIndEwalletAccountsSearch(sfWebRequest $request) {

        //        if($request->isMethod('get')){
        $postDataArray = $request->getParameterHolder()->getAll();
        //echo "<pre>"; print_r($postDataArray); die;
        if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
            $from_date = $postDataArray['from_date'];
//            $frmArr=explode("-",$from_date);
//            $from_date=$frmArr[2].'-'.$frmArr[1].'-'.$frmArr[0];
        } else {
            $from_date = "";
        }
        if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
            $to_date = $postDataArray['to_date'];
//            $toArr=explode("-",$to_date);
//            $to_date =$toArr[2].'-'.$toArr[1].'-'.$toArr[0];
        } else {
            $to_date = "";
        }
        if ($postDataArray['entry_type'] != "" && $postDataArray['entry_type'] != "BLANK") {
            $entry_type = $postDataArray['entry_type'];
        } else {
            $entry_type = "";
        }
        $account_no = $postDataArray['account_no'];
        ;
        $dates = $from_date . "##" . $to_date;
        $this->dates = $dates;
        $this->entry_type = $entry_type;
        $acct_type = "ewallet";
        $acctManagerObj = new EpAccountingManager;
        $acctSummaryObj = $acctManagerObj->getStatement($from_date, $to_date, $account_no, "", $entry_type, $acct_type);

        $all_results = $acctSummaryObj->execute();
        $creditAmt = 0;
        $debitAmt = 0;
        foreach ($all_results as $res) {
            if ($res->entry_type == 'credit')
                $creditAmt = $creditAmt + $res->amount;

            if ($res->entry_type == 'debit')
                $debitAmt = $debitAmt + $res->amount;
        }
        $this->debitAmt = $debitAmt;
        $this->creditAmt = $creditAmt;

        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }


        $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($acctSummaryObj);
        $this->pager->setPage($this->page);
        $this->pager->init();

        //        }
    }

    public function executeGetTransDetail(sfWebRequest $request) {
        $from_date = "";
        $to_date = "";
        $type = "";
        if ($request->hasParameter('from_date')) {
            $from_date = $request->getParameter('from_date');
        }
        if ($request->hasParameter('to_date')) {
            $to_date = $request->getParameter('to_date');
        }
        if ($request->hasParameter('accoutnId')) {
            $ewallet_account_Id = $request->getParameter('accoutnId');
        }
        if ($request->hasParameter('type')) {
            $type = $request->getParameter('type');
            if ($type == "both") {
                $type = "";
            }
        }
        $masterAcctObj = new EpAccountingManager;
        $walletDetailsObj = $masterAcctObj->getTransactionDetails($ewallet_account_Id, $from_date, $to_date, $type);
        $this->walletDetailsObj = $walletDetailsObj;
        //   $walletDetails=$walletDetailsObj->fetchArray();
    }

    public function executeCashControl(sfWebRequest $request) {
        $acctManagerObj = new EpAccountingManager;
        $acctSummaryObj = $acctManagerObj->getCashControlDetails();
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('EpMasterAccount', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($acctSummaryObj);
        // $this->pager->getQuery()->addGroupBy('l.master_account_id');
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeNibssCharge(sfWebRequest $request) {
        //getAllNibssDetail
        $obj = Doctrine::getTable('PaymentBatch')->getAllNibssDetail();
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PaymentBatch', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($obj);
        // $this->pager->getQuery()->addGroupBy('l.master_account_id');
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeBankVirtualAccount(sfWebRequest $request) {
        $this->form = new BankVirtualAccountForm();
    }

    public function executeBankVirtualAccountSearch(sfWebRequest $request) {
        //        if($request->isMethod('get')){
        $postDataArray = $request->getParameterHolder()->getAll();
        $merchant = $postDataArray['merchant'];

        $obj = Doctrine::getTable('FinancialInstitutionAcctConfig')->getaccountdetailfrmMerchant($merchant);
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('FinancialInstitutionAcctConfig', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($obj);
        $this->pager->setPage($this->page);
        $this->pager->init();

        //        }
    }

    public function executeEwalletHostAccount(sfWebRequest $request) {
        $this->form = new EwalletHostAccountForm();
        $this->formValid = "";
        if ($request->isMethod('post')) {

            $this->form->bind($request->getParameter('report'));
            if ($this->form->isValid()) {

                $this->formValid = "valid";
            }
        }
    }

    public function executeEwalletHostAccountCreditSearch(sfWebRequest $request) {
        $this->form = new EwalletHostAccountForm();
        $this->form->bind($request->getParameter('report'));
        $usePager = $request->getParameter('page');
        if ($this->form->isValid() || (isset($usePager) )) {

            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['report']))
                $postDataArray = $postDataArray['report'];

            $this->err = false;


            if ($postDataArray['master_account_id'] != 0) {

                if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
                    $this->fromDate = $postDataArray['from_date']." 00:00:00";
                } else {
                    $this->fromDate = "";
                }
                if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
                    $this->toDate = $postDataArray['to_date']." 23:59:59";
                } else {
                    $this->toDate = "";
                }

                $this->master_account_id = $postDataArray['master_account_id'];

                $acctManagerdebitObj = new EpAccountingManager;
                //                echo $this->fromDate.'--'.$this->toDate.'--'.$this->master_account_id;

                $acctSummarycreditObj = $acctManagerdebitObj->getEwalletHostDetailsQuery($this->fromDate, $this->toDate, $this->master_account_id, "credit");


                $this->page = 1;

                if ($request->hasParameter('page')) {
                    $this->page = $request->getParameter('page');
                }


                $this->pager = new sfDoctrinePager('EpMasterAccount', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($acctSummarycreditObj);
                $this->pager->setPage($this->page);
                $this->pager->init();
            } else {
                $this->err = true;
            }
        } else {
            $this->form->renderGlobalErrors();
            die;
        }
    }

    public function executeEwalletHostAccountDebitSearch(sfWebRequest $request) {
        $this->form = new EwalletHostAccountForm();
        $this->form->bind($request->getParameter('report'));
        $usePager = $request->getParameter('page');
        if ($this->form->isValid() || (isset($usePager) )) {
            $this->err = false;
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['report']))
                $postDataArray = $postDataArray['report'];

            if ($postDataArray['master_account_id'] != 0) {

                if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
                    $this->fromDate = $postDataArray['from_date']." 00:00:00";
                } else {
                    $this->fromDate = "";
                }
                if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
                    $this->toDate = $postDataArray['to_date']." 23:59:59";
                } else {
                    $this->toDate = "";
                }


                $this->master_account_id = $postDataArray['master_account_id'];


                //$acctManagercreditObj = new EpAccountingManager;
              //  $acctSummarydebitObj = $acctManagercreditObj->getEwalletHostDetailsQuery($this->fromDate, $this->toDate, $this->master_account_id, "debit");
                $ewallerNumberArr = explode("_",$this->master_account_id);
                $master_account_id = $ewallerNumberArr[1];

                $acctSummarydebitObj = Doctrine::getTable('EpMasterAccount')->getEwalletReversalDetails($this->fromDate, $this->toDate, $master_account_id, "debit");


                $this->page = 1;

                if ($request->hasParameter('page')) {
                    $this->page = $request->getParameter('page');
                }


                $this->pager = new sfDoctrinePager('EpMasterAccount', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($acctSummarydebitObj);
                $this->pager->setPage($this->page);
                $this->pager->init();
            } else {
                $this->err = true;
            }
        } else {
            $this->form->renderGlobalErrors();
            die;
        }
    }

    public function executeNibssAdviceReport(sfWebRequest $request) {

        $this->form = new NibssAdviceReportForm();
        //function to show the nibss search page
    }

    public function executeNibssAdviceSearchReport(sfWebRequest $request) {
        if ($request->isMethod('get')) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if ($postDataArray['from_date'] != "") {
                $from_date = $postDataArray['from_date'];
                $frmArr = explode("-", $from_date);
                $from_date = $frmArr[2] . '-' . $frmArr[1] . '-' . $frmArr[0];
            } else {
                $from_date = "";
            }
            if ($postDataArray['to_date'] != "") {
                $to_date = $postDataArray['to_date'];
                $toArr = explode("-", $to_date);
                $to_date = $toArr[2] . '-' . $toArr[1] . '-' . $toArr[0];
            } else {
                $to_date = "";
            }


            $accountNo = 1;

            $acctManagerObj = new EpAccountingManager;
            $acctSummaryObj = $acctManagerObj->getStatementDetails($from_date, $to_date, $accountNo, "", "Query");

            $this->page = 1;

            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }



            $this->pager = new sfDoctrinePager('EpMasterAccount', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($acctSummaryObj);
            // $this->pager->getQuery()->addGroupBy('l.master_account_id');
            $this->pager->setPage($this->page);
            $this->pager->init();
        }
    }

    public function executeEwalletUserDetail(sfWebRequest $request) {

        // $this->activation = array('' => '-- select--','activated' => '-- Activated --', 'notactivated' => '-- Not Activated --');

        $CurrencyObj = new CurrencyCode;
        $this->CurrencyArray = $CurrencyObj->getAllCountrySelect();
        $this->CurrencyArray[0] = '--All Currency--';

        $this->form = new EwaleetUserDetailForm('', array('CurrencyArray' => $this->CurrencyArray));
        $this->formValid = "";
        if ($request->isMethod('post')) {


            $this->form->bind($request->getParameter('userDetail'));
            if ($this->form->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeEwalletUserDetailSearch(sfWebRequest $request) {

        $this->form = new EwaleetUserDetailForm();

        $this->form->bind($request->getParameter('userDetail'));
        $postDataArray = $request->getParameterHolder()->getAll();
        if (isset($postDataArray['userDetail']))
            $postDataArray = $postDataArray['userDetail'];


        if ($this->form->isValid() || (isset($postDataArray['page']) )) {

            //if($request->isMethod('get')){

            if ($postDataArray['account_no'] != "" && $postDataArray['account_no'] != "BLANK") {
                $this->account_no = $postDataArray['account_no'];
            } else {
                $this->account_no = "";
            }

            if ($postDataArray['email'] != "" && $postDataArray['email'] != "BLANK") {
                $this->email = $postDataArray['email'];
            } else {
                $this->email = "";
            }

            if ($postDataArray['from'] != "" && $postDataArray['from'] != "BLANK") {
                $this->from_date = $postDataArray['from'];
                $from = $this->from_date;
            } else {
                $this->from_date = "";
                $from = "";
            }

            if ($postDataArray['to'] != "" && $postDataArray['to'] != "BLANK") {
                $this->to_date = $postDataArray['to'];
                $to = $this->to_date;
            } else {
                $this->to_date = "";
                $to = "";
            }
            if ($postDataArray['activation'] != "" && $postDataArray['activation'] != "BLANK")
                $this->activaion_on = $postDataArray['activation'];
            else
                $this->activaion_on = "";
            if ($postDataArray['orderByBalance'] != "" && $postDataArray['orderByBalance'] != "BLANK")
                $this->orderByBalance = $postDataArray['orderByBalance'];
            else
                $this->orderByBalance = "";

            if ($postDataArray['currency_id'] != 0 && $postDataArray['currency_id'] != "BLANK")
                $this->currency_id = $postDataArray['currency_id'];
            else
                $this->currency_id = "";
            //    $orderByBalance = $postDataArray['orderByBalance'];


            $settlementObj = Doctrine::getTable('UserDetail')->getUserDetailBetDates($this->account_no, $this->email, $from, $to, $this->activaion_on, $this->orderByBalance, $this->currency_id);

            $this->page = 1;

            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('UserDetail', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($settlementObj);
            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {

            $this->form->renderGlobalErrors();
            die;
        }
    }

    public function executeEwalletTransDetail(sfWebRequest $request) {

        $this->setLayout('layout_popup');
        $this->userId = $request->getParameter('userId');
    }

    public function executeEwalletTransactionDetail(sfWebRequest $request) {
        // $this->setLayout('layout_popup');
        $this->setLayout(false);
        $this->ewallet_account_id = $request->getParameter('from_account');
        $masterAcctObj = new EpAccountingManager;
        $userManagerObj = new UserAccountManager();
        $this->currencyId = $userManagerObj->getCurrencyForAccount($this->ewallet_account_id);
        $this->acctDetails = $masterAcctObj->getAccount($this->ewallet_account_id);
        $this->statementObj = $masterAcctObj->getStatementDetails('', '', $this->ewallet_account_id, '');
        $walletDetailsObj = $masterAcctObj->getTransactionDetails($this->ewallet_account_id, '', '', '');

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($walletDetailsObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
        $this->setTemplate('statement', 'ewallet');
    }

    public function executeEwalletUserCSV(sfWebRequest $request) {



        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $postDataArray = $request->getParameterHolder()->getAll();

        if ($postDataArray['account_no'] != "") {
            $account_no = $postDataArray['account_no'];
        } else {
            $account_no = "";
        }

        if ($postDataArray['email'] != "") {
            $email = $postDataArray['email'];
        } else {
            $email = "";
        }
        if ($postDataArray['from'] != "") {
            $from_date = $postDataArray['from'];
//            $frmArr=explode("-",$from_date);
//            $from_date=$frmArr[2].'-'.$frmArr[1].'-'.$frmArr[0];
        } else {
            $from_date = "";
        }

        if ($postDataArray['to'] != "") {
            $to_date = $postDataArray['to'];
//            $toArr=explode("-",$to_date);
//            $to_date =$toArr[2].'-'.$toArr[1].'-'.$toArr[0];
        } else {
            $to_date = "";
        }
        $activaion_on = $postDataArray['activation'];
        $orderByBalance = $postDataArray['orderByBalance'];
        if ($postDataArray['currency_id'] != 0) {
            $currency_id = $postDataArray['currency_id'];
        } else {
            $currency_id = "";
        }



        // Creating Headers
        if (!empty($currency_id)) {
            $headerNames = array(0 => array('S.No.', 'User Name', 'Balance Amount', 'First Name', 'Last Name', 'email', 'Mobile', 'Work phone', 'registered on', 'activation status', 'last login'));
            $headers = array('uname', 'cbalance', 'name', 'last_name', 'email', 'mobile_no', 'work_phone', 'created_at', 'isact', 'lastlogtime');
        } else {
            $headerNames = array(0 => array('S.No.', 'User Name', 'No of Account', 'First Name', 'Last Name', 'email', 'Mobile', 'Work phone', 'registered on', 'activation status', 'last login'));
            $headers = array('uname', 'account_count', 'name', 'last_name', 'email', 'mobile_no', 'work_phone', 'created_at', 'isact', 'lastlogtime');
        }
        // $headers = array('uname', 'cbalance', 'name', 'last_name','email', 'mobile_no', 'work_phone', 'created_at', 'isact', 'lastlogtime') ;
        $records = Doctrine::getTable('UserDetail')->getUserDetailBetDatesCSV($account_no, $email, $from_date, $to_date, $activaion_on, $orderByBalance, $currency_id);


        $i = 0;
        while ($row = mysql_fetch_array($records)) {

            if ($row['isact'] == 1)
                $row['isact'] = 'Activated';
            else
                $row['isact'] = 'Not Activated';
            $counterVal = count($headers);
            for ($index = 0; $index < $counterVal; $index++) {
                if ($headers[$index] == 'account_count') {
                    $recordsDetails[$i][$headers[$index]] = $row['sum'];
                } else {
                    $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
                }
            }
            $i++;
        }




        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, '/report/ewalletUserDetail/', "ewalletUserDetail");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "eWallet_USER_DETAIL";
        } else {
            $this->redirect_url('index.php/reports/ewalletUserDetail');
        }

        $this->setLayout(false);
    }

    public function executeSettlementaccounts(sfWebRequest $request) {
        $this->form = new SettlementaccountsForm();
        $this->formValid = "";
        if ($request->isMethod('post')) {

            $this->form->bind($request->getParameter('report'));
            if ($this->form->isValid()) {
                $this->formValid = "valid";
            }
        }

        //function to show the settlement accounts search page
    }

    public function executeSettlementAccountsReport(sfWebRequest $request) {
        $this->form = new SettlementaccountsForm();
        $this->form->bind($request->getParameter('report'));
        $usePager = $request->getParameter('page');
        if ($this->form->isValid() || (isset($usePager) )) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['report']))
                $postDataArray = $postDataArray['report'];
            //       if($request->isMethod('get')){

            if ($postDataArray['from'] != "") {
                $from_date = $postDataArray['from'];
//                $frmArr=explode("-",$from_date);
//                $from_date=$frmArr[2].'-'.$frmArr[1].'-'.$frmArr[0];
            } else {
                $from_date = "";
            }

            if ($postDataArray['to'] != "") {
                $to_date = $postDataArray['to'];
//                $toArr=explode("-",$to_date);
//                $to_date =$toArr[2].'-'.$toArr[1].'-'.$toArr[0];
            } else {
                $to_date = "";
            }
            $settlementObj = Doctrine::getTable('PaymentRecord')->getSettlemnetdata($from_date, $to_date);

            $this->page = 1;

            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }


            $this->pager = new sfDoctrinePager('PaymentRecord', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($settlementObj);
            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {
            $this->form->renderGlobalErrors();
            die;
        }
    }

    //    }
    public function executeEwalletRechargeReport(sfWebRequest $request) {
        //drop down for multi currency
        if (settings::isMultiCurrencyOn()) {
            $arrAllCurrency = array();
            $arrAllCurrency = Doctrine::getTable('CurrencyCode')->getAllDistinctCurrencyService();
            if (count($arrAllCurrency) > 0) {
                foreach ($arrAllCurrency as $key => $val) {
                    $currencyArray[$val['id']] = ucfirst($val['currency']);
                }
            }
            $this->CurrencyArray = $currencyArray;
        }
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $this->form = new EWalletRechargeReportForm(array(), array('currency' => $this->CurrencyArray));
    }

    /**
     * function will add job into database
     *
     * @access public
     * @param $request Form Post values
     * @return Doesn't return any thing
     * @modifyBy Mayank and Ramandeep Singh
     */

    public function executeEwalletRechargeReportSearch(sfWebRequest $request) {

        if ($request->isMethod('post')) {

            $input_values = $request->getParameterHolder()->getAll();

            $this->form = new EWalletRechargeReportForm($input_values, array('currency' => $this->CurrencyArray));
            $this->form->bind($input_values);

            if (!$this->form->isValid()) {
            	$this->getUser()->setFlash('notice',sprintf('Some Thing Went Wrong'));
            }else{

                if (settings::isMultiCurrencyOn()) {
                    $arrAllCurrency = array();
                    $arrAllCurrency = Doctrine::getTable('CurrencyCode')->getAllDistinctCurrencyService();
                    if (count($arrAllCurrency) > 0) {
                        foreach ($arrAllCurrency as $key => $val) {
                            $currencyArray[$val['id']] = ucfirst($val['currency']);
                        }
                    }
                    $this->CurrencyArray = $currencyArray;
                }$pfmHelperObj = new pfmHelper();
                $this->userGroup = $pfmHelperObj->getUserGroup();

                $this->postDataArray = $request->getPostParameters();
                $userId=$this->getUser()->getGuardUser()->getid();

                $jobName = "EwalletRechargeReport";
                $jobAction = $this->moduleName . "/eWalletRechargeReportCsv";

                $parameters = array(
                    'from_date'=>date('Y-m-d', strtotime($this->postDataArray ['from_date'])),
                    'to_date'=>date('Y-m-d', strtotime($this->postDataArray ['to_date'])),
                    'currency'=>$this->postDataArray['currency'],
                    'entry_type'=>$this->postDataArray['entry_type'],
                    'user_id'=>$userId,
                    'frequency'=>'CUSTOM',
                    'report_name'=>'EwalletRechargeReport',

                );
                if($this->postDataArray['banks'] != "" && $this->postDataArray['banks'] != null){
                    $parameters['file_name']='EwalletRechargeReport-'.$userId.'_CUSTOM_'.$this->postDataArray['banks'];
                    $parameters['banks'] = $this->postDataArray['banks'];
                }
                else{
                    $parameters['file_name']='EwalletRechargeReport-'.$userId.'_CUSTOM_AllBanks';
                }
                if($this->postDataArray['payment_mode'] != "" && $this->postDataArray['payment_mode'] != null){
                    $parameters['payment_mode'] = $this->postDataArray['payment_mode'];
                }
                if($this->postDataArray['country'] != "" && $this->postDataArray['country'] != null){
                    $parameters['country'] = $this->postDataArray['country'];
                }
                if($this->postDataArray['branch'] != "" && $this->postDataArray['branch'] != null){
                    $parameters['branch'] = $this->postDataArray['branch'];
                }
                if($this->postDataArray['wallet_no'] != "" && $this->postDataArray['wallet_no'] != null){
                    $parameters['wallet_no'] = $this->postDataArray['wallet_no'];
                }
                if($this->postDataArray['user_name'] != "" && $this->postDataArray['user_name'] != null){
                    $parameters['user_name'] = $this->postDataArray['user_name'];
                }

                $reportId = Doctrine::getTable('ReportLog')->verifyDublicateReportRequest($parameters);

                if($reportId){
                    $this->getUser()->setFlash('notice',sprintf('You have already place a request with same criteria. <a href="ewalletRechargeReportDisplay/id/'.$reportId.'">Click here</a> to view this report.'));
                }else{
                    // Custom job will set for next day morning 1PM
                    $startTime = date('Y-m-d', strtotime(' +1 day'))." 01:00:00";
                    $taskId = EpjobsContext::getInstance()->addJob($jobName, $jobAction, $parameters,-1,NULL,$startTime);

                    // Custom Job will set for current time only
                    //$taskId = EpjobsContext::getInstance()->addJob($jobName, $jobAction, $parameters);

                    $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                    /*
                     * making entry in report log
                     * So that report can be deleted if process is pending
                     *
                     * ReportLog rest records will be updated on report creation
                     *
                     * Concatinated time in the end because getRechargeDetailsForCsv
                     * function will concatinate time clause in query itself there
                     *
                     * EpJobParam will hold only YMD for date as in table class time is concatinated
                     * while we concatinated it for report table
                     */

                    $parameters['ep_job_id'] = $taskId;
                    $parameters['from_date'] .= " 00:00:00";
                    $parameters['to_date'] .= " 23:59:59";

                    unset($parameters['file_name']);
                    // unset as file name will be updated on report creation dipicting report has been generated as null or blank value dipict pending report
                    $reportId = Doctrine::getTable('ReportLog')->saveReport($parameters);

                    $this->getUser()->setFlash('notice',sprintf('Request placed successfully, you will receive an email while report generated.'));
                }
            }

            $this->redirect('report/ewalletRechargeReport');
        }
    }

    /**
     * function will add job into database
     *
     * @access public
     * @param $request Form Post values
     * @return Doesn't return any thing
     * @modifyBy Ramandeep Singh
     */

    public function executeEWalletRechargeReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");

        $this->postDataArray = array();
        $this->postDataArray = $request;

        // Creating Headers
        $headerNames = array(0 => array('S.No.', 'Bank Name', 'Branch Name', 'Branch Code', 'Validation Number', 'Username', 'Entry Type', 'Account Name', 'eWallet Account Number', 'Recharged At', 'Amount'));
        $headers = array('bank_name', 'branch_name', 'branch_code', 'payment_transaction_number', 'username', 'entry_type', 'account_name', 'account_number', 'created_at', 'amount');

        if( $this->postDataArray['frequency'] == "MONTHLY" ){
            $this->postDataArray['from_date'] = date("Y-m-d", mktime(0, 0, 0, date("m")-1, 1, date("Y")));
            $this->postDataArray['to_date']   = date("Y-m-d", mktime(0, 0, 0, date("m"), 0, date("Y")));
        }
        if( $this->postDataArray['frequency'] == "WEEKLY" ){
            /*TODO when required by client
             *
             * logic can be applied for 1-4 weekly report
             * echo date('N', strtotime('2013-11-03')); echo 'day </br>';
             * $string = "2013-11-03";
             * $timestamp = strtotime($string);
             * echo date("d", $timestamp);
             */

            $today = getdate();
            $start = $today['wday'] + 6;
            $end = $today['wday'];
            if($today['wday'] == 0){
                $start+=7;
                $end+=7;
            }
            $this->postDataArray['from_date'] = date('Y-m-d', strtotime('-'.$start.' days', strtotime(date('Y-m-d'))));
            $this->postDataArray['to_date']   = date('Y-m-d', strtotime('-'.$end.' days', strtotime(date('Y-m-d'))));
        }
        if( $this->postDataArray['frequency'] == "QUARTERLY" ){
            $currentMonth = date("m");
                if($currentMonth >="1" && $currentMonth <="3"){
                    $startDate = date("Y", strtotime("last year"))."-10-1";
                    $endDate = date("Y", strtotime("last year"))."-12-31";
                }
                if($currentMonth >="4" && $currentMonth <="6"){
                    $startDate = date("Y")."-1-1";
                    $endDate = date("Y")."-3-31";
                }
                if($currentMonth >="7" && $currentMonth <="9"){
                    $startDate = date("Y")."-4-1";
                    $endDate = date("Y")."-6-30";
                }
                if($currentMonth >="10" && $currentMonth <="12"){
                    $startDate = date("Y")."-7-1";
                    $endDate = date("Y")."-9-30";
                }
             $this->postDataArray['from_date'] = $startDate;
             $this->postDataArray['to_date']   = $endDate;
        }

        $records = Doctrine::getTable('EwalletUserAcctConsolidation')->getRechargeDetailsForCsv($this->postDataArray);
        $i = 0;
        $grandTotalAmount = '';
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        while ($row = mysql_fetch_array($records)) {
            $counterVal = count($headers);
            for ($index = 0; $index < $counterVal; $index++) {
                $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
            }
            $grandTotalAmount+= format_amount($row['amount'], null, 1);
            $i++;
        }

            $arrList = $this->getRechargeXLSExportList($headerNames, $headers, $recordsDetails);
            $arrList[] = array('', '', '', '', '', '', '', '', '', 'Grand Total', $grandTotalAmount);

            $fileName  = $this->postDataArray['file_name']."_";
            $fileName .= date('dMY', strtotime($this->postDataArray['from_date']))."To";
            $fileName .= date('dMY', strtotime($this->postDataArray['to_date']));
            $fileName = str_replace(" ", "_", $fileName);

            $file_array = csvSave::saveExportListFileNew($arrList, "/report/eWalletRechargeReport/", $fileName);

            // from and to date being updated as they have to be entered in M,Q,W Report
            // althought not required in custom report

            if($this->postDataArray['frequency'] == "CUSTOM") {
                $columnValue = array(
                    'file_name' => $file_array["file_name"],
                );
            }else{
                $columnValue = array(
                    'file_name' => $file_array["file_name"],
                    'from_date' => $this->postDataArray['from_date']." 00:00:00",
                    'to_date'   => $this->postDataArray['to_date']." 23:59:59",
                );
            }

            $where = array(
                'ep_job_id' => $this->postDataArray['ep_job_id'],
            );

            if($this->postDataArray['frequency'] == "CUSTOM"){
                $reportId = Doctrine::getTable('ReportLog')->updateReport($columnValue,$where);
            }
            if($this->postDataArray['frequency'] == "MONTHLY" || $this->postDataArray['frequency'] == "WEEKLY" || $this->postDataArray['frequency'] == "QUARTERLY"){
                $columnValue['ep_job_id'] = $this->postDataArray['ep_job_id'];
                $columnValue['user_id'] = $this->postDataArray['user_id'];
                $columnValue['report_name'] = $this->postDataArray['report_name'];
                $columnValue['frequency'] = $this->postDataArray['frequency'];
                $columnValue['banks'] = $this->postDataArray['banks'];
                
                $reportId = Doctrine::getTable('ReportLog')->saveReport($columnValue);
            }

            /*
             * Copy Report-File To Slave Server
             */
            $credentials = parse_ini_file(sfConfig::get("app_slave_settings_upload"));
            $sftp = new Net_SFTP($credentials['host']);
            if (!$sftp->login($credentials['user'], $credentials['password'])) { exit('Login Failed'); }
            $sourcePath = sfConfig::get('sf_upload_dir').'/report/eWalletRechargeReport/'.$columnValue['file_name'].'.csv';
            //$destPath = '/var/www/pay4me_v1/web/uploads/report/eWalletRechargeReport/'.$columnValue['file_name'].'.csv';
            $destPath = sfConfig::get('sf_upload_dir').'/report/eWalletRechargeReport/'.$columnValue['file_name'].'.csv';
            if (file_exists($sourcePath)) {
                $content = file_get_contents($sourcePath);
                $sftp->put($destPath, $content);
            }

            /*
             * Email confirmation that report has been generated
             * only applicable in case of CUSTOM report
             */
            if( $this->postDataArray['frequency'] == "CUSTOM" ){

                #Begin Code To Send  Mail

                $UserId = $this->postDataArray['user_id'];
                $subject = "eWallet Recharge Report Creation Reminder Mail";
                $partialName = 'ewallet_report_confirmation';
                $taskId = EpjobsContext::getInstance()->addJob('SendEwalletRechargeReportCreationConfirmation', $this->moduleName . "/sendEmail", array('userid' => $UserId, 'subject' => $subject, 'partialName' => $partialName));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                #End Code To Send Mail
            }
            return $this->renderText($file_array["message"]);
    }
    public function getXLSExportListMerchant($arrHeader, $headers, $arrList) {

      $arrTotalList = array();
      $counterVal = count($arrHeader);
      for ($i = 0; $i < $counterVal; $i++) {
        $arrTotalList[$i] = $arrHeader[$i];
      }

      $total = count($arrList) + count($arrTotalList);

      $length = count($arrTotalList);
      //       echo $length;
      //       die;
      $j = $length;
      $counterVal = count($arrList) - 1;
      //echo $counterVal;

      $counterValHeader = count($headers);
      //echo $counterValHeader;
      //die;
      for ($k = 0; $k < $counterVal; $k++) {
        $arrExPortList[$k][] = $k + 1;
        for ($index = 0; $index < $counterValHeader; $index++) {
          $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
        }
      }

      if ($this->postDataArray['report_label'] != 'summery' && $this->postDataArray['report_label'] != 'detailed') {

        $counterVal = count($arrHeader[0]) - 6;
        for ($i = 0; $i < $counterVal; $i++) {
          $arrExPortList[$k][$i] = '';
        }

        $arrExPortList[$k][$i] = 'Grand Total ';
        $arrExPortList[$k][++$i] = $arrList[$k]['No of transactions'];
        $arrExPortList[$k][++$i] = $arrList[$k]['Grand Total'];
        $arrExPortList[$k][++$i] = $arrList[$k]['Merchant Amount'];
        $arrExPortList[$k][++$i] = $arrList[$k]['Payforme Amount'];
        $arrExPortList[$k][++$i] = $arrList[$k]['Bank Amount'];
      }
      $counterVal = count($arrExPortList);
      for ($i = 0; $i < $counterVal; $i++) {
        $arrTotalList[$j + $i] = $arrExPortList[$i];
      }

      return $arrTotalList;
    }

    public function getRechargeXLSExportList($arrHeader, $headers, $arrList) {
        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $counterVal = count($arrList);
        $counterValHeaders = count($headers);
        $grandTotalAmount = '';
        for ($k = 0; $k < $counterVal; $k++) {
            $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeaders; $index++) {
                if ($headers[$index] == 'amount') {

                    $arrExPortList[$k][] = format_amount($arrList[$k][$headers[$index]], null, 1);
                }
                else
                    $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }

    public function executePaidByVisaReport(sfWebRequest $request) {
        $userAccObj = new UserAccountManager();
        $this->isSuperadmin = $userAccObj->IsReportAdmin();
        $this->err = '';
        $this->form = new PaidByVisaReportForm();
    }

    public function executePaidByInternetBankReport(sfWebRequest $request) {
        $userAccObj = new UserAccountManager();
        $this->isSuperadmin = $userAccObj->IsReportAdmin();
        $this->err = '';
        $this->form = new PaidByInternetBankReportForm();
    }

    public function executePaidByVisaReportSearch(sfWebRequest $request) {
        $userAccObj = new UserAccountManager();
        $this->isSuperadmin = $userAccObj->IsReportAdmin();
        $this->forward404Unless($request->isMethod('post'));
        $this->err = '';
        try {
            if ($request->isMethod('post')) {
                $this->form = new PaidByVisaReportForm();
                $this->processVisaReportSearchForm($request, $this->form);
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    public function executePaidByInternetBankReportSearch(sfWebRequest $request) {
        $userAccObj = new UserAccountManager();
        $this->isSuperadmin = $userAccObj->IsReportAdmin();
        $this->forward404Unless($request->isMethod('post'));
        $this->err = '';
        try {
            if ($request->isMethod('post')) {
                $this->form = new PaidByInternetBankReportForm();
                $this->processInternetBankReportSearchForm($request, $this->form);
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    public function executeGetStatusfrmPayMode(sfWebRequest $request) {
        sfView::NONE;
        $PaymentMode = $request->getParameter('paymode');

        $parModel = 'app_paymode_' . $PaymentMode . "_name";
        $html = "";
        $records = Doctrine::getTable('GatewayOrder')->getOrderStatus($PaymentMode);
        if (sfConfig::get($parModel) == 'Interswitch') {
            $html .="<option value ='all'>All</option>";
        }
        foreach ($records as $status) {
            if ('' != $status->getStatus())
                $html .="<option value = " . $status->getStatus() . " >" . ucwords($status->getStatus()) . "</option>";
        }
        echo $html;
        die;
    }

    protected function processVisaReportSearchForm(sfWebRequest $request, sfForm $form) {
        $this->err = '';

        $form->bind($request->getParameter('paidwithvisa'));

        if ($form->isValid()) {
            $from_date = "";
            $to_date = "";
            $status = "";
            $username = "";
            $transactionNo = "";
            $accountNo = "";
            $user_id = "";
            $currency_code = sfConfig::get('app_naira_currency');
            $currency = '1'; //for naira only

            $formParam = $request->getParameter('paidwithvisa');

            if (isset($formParam['username']) && $formParam['username'] != "") {
                $username = $formParam['username'];
            }
            if ($formParam['transaction_no'] != "") {
                $transactionNo = $formParam['transaction_no'];
            }
            if ($formParam['account_no'] != "") {
                $accountNo = $formParam['account_no'];
            }

            $start_date = $formParam['from'];
            if ($start_date != "") {
                //          $dateArray = explode("/",$start_date);
                //          $from_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 00:00:00"));
                $from_date = $start_date . ' 00:00:00';
            }



            $end_date = $formParam['to'];
            if ($end_date != "") {
                //          $dateArray = explode("/",$end_date);
                //          $to_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 23:59:59"));
                $to_date = $end_date . ' 23:59:59';
            }


            $status = $formParam['trans_type'];
            if ($status == "all" || $status == "") {
                $status = "";
            }
            $PaymentMode = $formParam['pay_mode'];
            $PaymentType = $formParam['pay_type'];

            $userAccObj = new UserAccountManager();
            if (!$userAccObj->IsReportAdmin())
                $user_id = $this->getUser()->getGuardUser()->getId();
            $groups = $this->getUser()->getGroupDescription();
            $parModel = 'app_paymode_' . $PaymentMode . "_name";
            $this->paymentmode = $PaymentMode;
            if ($PaymentMode) {
                $objPaymentMode = Doctrine::getTable('PaymentModeOption')->find($PaymentMode);
                $this->csvMaymentMode = $objPaymentMode->getPaymentMode()->getDisplayName();
            }
            //      echo $from_date.'-----'.$to_date.'-----'.$status.'-----'.$user_id="".'-----'.$currency.'-----'.$username.'-----'.$PaymentMode.'-----'.$PaymentType.'------'.$transactionNo;


            if (strtolower($PaymentType) == 'payment')
                $visaRecords = Doctrine::getTable('GatewayOrder')->VisaStatement($from_date, $to_date, $status, $user_id, $currency, $username, $PaymentMode, $PaymentType, $transactionNo);
            else
                $visaRecords = Doctrine::getTable('GatewayOrder')->VisaRechargeStatement($from_date, $to_date, $status, $user_id, $currency, $username, $PaymentMode, $PaymentType, $accountNo);


            $pfmHelperObj = new pfmHelper();
            $this->postDataArray = array();
            if ($request->isMethod('post')) {
                unset($_SESSION['pfm']['visaPayment']);
                $_SESSION['pfm']['visaPayment'] = $request->getPostParameters();
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            } else {
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            }

            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            }

            $this->pager = new sfDoctrinePager('GatewayOrder', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($visaRecords);
            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {

            foreach ($form->getGlobalErrors() as $name => $error):
                $this->err .= $name . ': ' . $error . "<br>";
            endforeach;
            //echo $form;
            die;
        }
    }


    protected function processInternetBankReportSearchForm(sfWebRequest $request, sfForm $form) {
        $this->err = '';

        $form->bind($request->getParameter('paidwithinternetbank'));

        if ($form->isValid()) {
            $from_date = "";
            $to_date = "";
            $status = "";
            $transactionNo = "";
            $accountNo = "";
            $currency_code = sfConfig::get('app_naira_currency');
            $currency = '1'; //for naira only

            $formParam = $request->getParameter('paidwithinternetbank');

            if ($formParam['transaction_no'] != "") {
                $transactionNo = $formParam['transaction_no'];
            }
            if ($formParam['account_no'] != "") {
                $accountNo = $formParam['account_no'];
            }

            $start_date = $formParam['from'];
            if ($start_date != "") {
                $from_date = $start_date . ' 00:00:00';
            }

            $end_date = $formParam['to'];
            if ($end_date != "") {
                $to_date = $end_date . ' 23:59:59';
            }
            $status = $formParam['trans_type'];
            if ($status == "all" || $status == "") {
                $status = "";
            }
            //$PaymentMode = $formParam['pay_mode'];
            $PaymentMode = Doctrine::getTable('PaymentModeOption')->getPaymentModeID(sfConfig::get('app_payment_mode_option_internet_bank'));

            $PaymentMode = $PaymentMode[0]['id'];


            $PaymentType = $formParam['pay_type'];

            $userAccObj = new UserAccountManager();
//            if (!$userAccObj->IsReportAdmin())
//                $user_id = $this->getUser()->getGuardUser()->getId();
//            $groups = $this->getUser()->getGroupDescription();
            $parModel = 'app_paymode_' . $PaymentMode . "_name";
            $this->paymentmode = $PaymentMode;
            if ($PaymentMode) {
                $objPaymentMode = Doctrine::getTable('PaymentModeOption')->find($PaymentMode);

                $this->csvMaymentMode = $objPaymentMode->getPaymentMode()->getDisplayName();
            }
            //      echo $from_date.'-----'.$to_date.'-----'.$status.'-----'.$user_id="".'-----'.$currency.'-----'.$username.'-----'.$PaymentMode.'-----'.$PaymentType.'------'.$transactionNo;


            if (strtolower($PaymentType) == 'payment'){
                $internetbankRecords = Doctrine::getTable('GatewayOrder')->InternetBankStatement($from_date, $to_date, $status, $currency, $PaymentMode, $PaymentType, $transactionNo);
            }
            else
                $internetbankRecords = Doctrine::getTable('InternetBankRechargeReportView')->InternetBankRechargeStatement($from_date, $to_date, $status, $currency, $PaymentMode, $PaymentType, $accountNo);


            $pfmHelperObj = new pfmHelper();
            $this->postDataArray = array();
            if ($request->isMethod('post')) {
                unset($_SESSION['pfm']['internetbankPayment']);
                $_SESSION['pfm']['internetbankPayment'] = $request->getPostParameters();
                $this->postDataArray = $_SESSION['pfm']['internetbankPayment'];
            } else {
                $this->postDataArray = $_SESSION['pfm']['internetbankPayment'];
            }

            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                $this->postDataArray = $_SESSION['pfm']['internetbankPayment'];
            }

            $this->pager = new sfDoctrinePager('GatewayOrder', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($internetbankRecords);
            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {

            foreach ($form->getGlobalErrors() as $name => $error):
                $this->err .= $name . ': ' . $error . "<br>";
            endforeach;
            //echo $form;
            die;
        }
    }

    public function executePaidByVisaReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = $_SESSION['pfm']['visaPayment'];
        $from_date = "";
        $to_date = "";
        $status = "";
        $username = "";
        $transactionNo = "";
        $accountNo = "";
        $currency_code = sfConfig::get('app_naira_currency');
        $currency = '1'; //for naira only
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $formParam = $this->postDataArray['paidwithvisa'];

        if ($formParam['username']) {
            $username = $formParam['username'];
        }
        if ($formParam['transaction_no'] != "") {
            $transactionNo = $formParam['transaction_no'];
        }
        if ($formParam['account_no'] != "") {
            $accountNo = $formParam['account_no'];
        }
        $start_date = $formParam['from'];
        if ($start_date != "") {
            //      $dateArray = explode("/",$start_date);
            //      $from_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 00:00:00"));
            $from_date = $start_date . ' 00:00:00';
        }

        $end_date = $formParam['to'];
        if ($end_date != "") {
            //      $dateArray = explode("/",$end_date);
            //      $to_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 23:59:59"));
            $to_date = $end_date . ' 23:59:59';
        }


        $status = $formParam['trans_type'];
        if ($status == "all" || $status == "") {
            $status = "";
        }
        $PaymentMode = $formParam['pay_mode'];
        $displayPaymentMode = '';
        if ($PaymentMode) {
            $objPaymentMode = Doctrine::getTable('PaymentModeOption')->find($PaymentMode);
            $displayPaymentMode = $objPaymentMode->getPaymentMode()->getDisplayName();
            $displayPaymentMode = preg_replace("/[\/ ]/", "_", $displayPaymentMode);
        }
        $PaymentType = $formParam['pay_type'];
        $user_id = $this->getUser()->getGuardUser()->getId();

        $parModel = 'app_paymode_' . $PaymentMode . "_name";
        if (strtolower($PaymentType) == 'payment')
            $visaRecords = Doctrine::getTable('GatewayOrder')->getCsvVisaStatement($from_date, $to_date, $status, $currency, $username, $PaymentMode, $PaymentType, $transactionNo);
        else
            $visaRecords = Doctrine::getTable('GatewayOrder')->getCsvRechargeStatement($from_date, $to_date, $status, $currency, $username, $PaymentMode, $PaymentType, $accountNo);

        if (strtolower($PaymentType) == 'payment') {
            if ($PaymentMode == 5) {
                $headerNames = array(0 => array('S.No.', 'UserName', 'Transaction Number', 'Narration', 'Transaction Date', "Amount", 'Transaction Status', 'Card Number', 'Approval Code'));
                $headers = array('username', 'app_id', 'order_desc', 'transtime', 'purchased_amount', 'order_status', 'pan', 'approvalcode');
            } else {
                $headerNames = array(0 => array('S.No.', 'UserName', 'Transaction Number', 'Narration', 'Transaction Date', "Amount", 'Transaction Status'));
                $headers = array('username', 'app_id', 'order_desc', 'transtime', 'purchased_amount', 'order_status');
            }
        } else {
            if ($PaymentMode == 5) {
                $headerNames = array(0 => array('S.No.', 'UserName', 'Narration', 'Transaction Date', "Amount", 'Transaction Status', 'Card Number', 'Approval Code'));
                $headers = array('username', 'order_desc', 'transtime', 'purchased_amount', 'order_status', 'pan', 'approvalcode');
            } else {
                $headerNames = array(0 => array('S.No.', 'UserName', 'Narration', 'Transaction Date', "Amount", 'Transaction Status'));
                $headers = array('username', 'order_desc', 'transtime', 'purchased_amount', 'order_status');
            }
        }
        $i = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        while ($row = mysql_fetch_array($visaRecords)) {
            for ($index = 0; $index < count($headers); $index++) {
                //,FS#29963
                if ($headers[$index] == 'order_desc') {
                    if ($row[$headers[$index]] != "") {
                        $row[$headers[$index]] = strip_tags($row[$headers[$index]]);
                    } else {
                        $row[$headers[$index]] = "";
                    }
                }

                if ($headers[$index] == 'purchased_amount') {
                    $recordsDetails[$i][$headers[$index]] = format_amount($row[$headers[$index]], null, 1);
                } else {
                    $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
                }
            }
            $i++;
        }
        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, '/report/paidByCardReport/', "paidBy" . $displayPaymentMode . "Report");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "PAIDBY_CARD";
        } else {
            $this->redirect_url('index.php/report/paidByCardReport');
        }$this->setTemplate('csv');
        // $this->setTemplate('bankRep
    }



    public function executePaidByInternetBankReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = $_SESSION['pfm']['internetbankPayment'];
        $from_date = "";
        $to_date = "";
        $status = "";
        $username = "";
        $transactionNo = "";
        $accountNo = "";
        $currency_code = sfConfig::get('app_naira_currency');
        $currency = '1'; //for naira only
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $formParam = $this->postDataArray['paidwithinternetbank'];

//        if ($formParam['username']) {
//            $username = $formParam['username'];
//        }
        if ($formParam['transaction_no'] != "") {
            $transactionNo = $formParam['transaction_no'];
        }
        if ($formParam['account_no'] != "") {
            $accountNo = $formParam['account_no'];
        }
        $start_date = $formParam['from'];
        if ($start_date != "") {
            //      $dateArray = explode("/",$start_date);
            //      $from_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 00:00:00"));
            $from_date = $start_date . ' 00:00:00';
        }

        $end_date = $formParam['to'];
        if ($end_date != "") {
            //      $dateArray = explode("/",$end_date);
            //      $to_date = date('Y-m-d H:i:s',strtotime($dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." 23:59:59"));
            $to_date = $end_date . ' 23:59:59';
        }


        $status = $formParam['trans_type'];
        if ($status == "all" || $status == "") {
            $status = "";
        }
        //$PaymentMode = $formParam['pay_mode'];
        $PaymentMode = Doctrine::getTable('PaymentModeOption')->getPaymentModeID(sfConfig::get('app_payment_mode_option_internet_bank'));
        $PaymentMode = $PaymentMode[0]['id'];
        $displayPaymentMode = '';
        if ($PaymentMode) {
            $objPaymentMode = Doctrine::getTable('PaymentModeOption')->find($PaymentMode);
            $displayPaymentMode = $objPaymentMode->getPaymentMode()->getDisplayName();
            $displayPaymentMode = preg_replace("/[\/ ]/", "_", $displayPaymentMode);
        }
        $PaymentType = $formParam['pay_type'];
        $user_id = $this->getUser()->getGuardUser()->getId();

        $parModel = 'app_paymode_' . $PaymentMode . "_name";
        if (strtolower($PaymentType) == 'payment')
            $internetbankRecords = Doctrine::getTable('GatewayOrder')->getCsvVisaStatement($from_date, $to_date, $status, $currency, $username, $PaymentMode, $PaymentType, $transactionNo);
        else{
            $internetbankRecords = Doctrine::getTable('InternetBankRechargeReportView')->InternetBankRechargeStatementcsv($from_date, $to_date, $status, $currency, $PaymentMode, $PaymentType, $accountNo);
         //   $internetbankRecords = Doctrine::getTable('GatewayOrder')->getCsvRechargeStatement($from_date, $to_date, $status, $currency, $username, $PaymentMode, $PaymentType, $accountNo);
        }
        if (strtolower($PaymentType) == 'payment') {

            $headerNames = array(0 => array('S.No.', 'Transaction Number', 'Transaction Date', "Amount", 'Transaction Status'));
            $headers = array('app_id', 'transtime', 'purchased_amount', 'order_status');

        } else {
            $headerNames = array(0 => array('S.No.', 'Ewallet Account UserName',  'Transaction Date', "Amount", 'Transaction Status'));
            $headers = array('username', 'transaction_date', 'amount', 'status');

        }
        $i = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        while ($row = mysql_fetch_array($internetbankRecords)) {

            for ($index = 0; $index < count($headers); $index++) {
                //,FS#29963
                if ($headers[$index] == 'order_desc') {
                    if ($row[$headers[$index]] != "") {
                        $row[$headers[$index]] = strip_tags($row[$headers[$index]]);
                    } else {
                        $row[$headers[$index]] = "";
                    }
                }

                if ( ($headers[$index] == 'purchased_amount') || ($headers[$index] == 'amount')) {
                    $recordsDetails[$i][$headers[$index]] = format_amount($row[$headers[$index]], null, 1);
                } else {
                    $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
                }
            }
            $i++;
        }
        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, '/report/paidByCardReport/', "paidBy" . $displayPaymentMode . "Report");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "PAIDBY_CARD";
        } else {
            $this->redirect_url('index.php/report/paidByCardReport');
        }$this->setTemplate('csv');
        // $this->setTemplate('bankRep
    }
    /**
     *  Internation Currency Report for USD
     * @param <type> $request
     */
    public function executeInternationalCurrencyReport(sfWebRequest $request) {
        //        $this->getUser()->setCulture('en');

        $this->err = '';
        $service_array = array();
        if ($request->isMethod('post')) {
            $allParams = $request->getPostParameters();
            if ($allParams['paidwithvisa']['merchant_id'] != '') {

                $service_array = $this->construct_options_array(Doctrine::getTable("MerchantService")->getServiceTypes($allParams['paidwithvisa']['merchant_id'])->toArray());
            }
        }
        $this->form = new InternationalCurrencyReportForm(array(), array('merchant_service_choices' => $service_array));
        $this->uname = '';
        $this->tdate = '';
        $this->fdate = '';
        $this->mid = '';
        $this->msid = '';
        $this->isvalidPost = '';


        if ($request->isMethod('post')) {



            $this->form->bind($request->getParameter('paidwithvisa'));

            if ($this->form->isValid()) {



                $this->uname = $allParams['paidwithvisa']['username'];
                $this->tdate = $allParams['paidwithvisa']['to_date'];
                $this->fdate = $allParams['paidwithvisa']['from_date'];
                $this->mid = $allParams['paidwithvisa']['merchant_id'];
                $this->msid = $allParams['paidwithvisa']['merchant_service_id'];

                $this->isvalidPost = 'valid';
            } else {

            }
        }
    }

    /**
     *  Execute function for InternationalCurrency Report
     * @param <type> $request
     */
    public function executeInternationalCurrencyReportSearch(sfWebRequest $request) {


        $this->forward404Unless($request->isMethod('post'));
        $this->err = '';
        try {

            if ($request->isMethod('post')) {  //echo "<pre>"; print_r($request->getPostParameters()); die;
                // $this->form = new InternationalCurrencyReportForm();
                /* $currencyRecords = Doctrine::getTable('EpVbvResponse')->CurrencyStatement();
                  foreach($currencyRecords as $val) {
                  print "<pre>";
                  print_r($val->toArray());
                  }
                  exit; */
                $this->processInternationalCurrencyReportSearchForm($request);

                //$this->setTemplate('paidByVisaReport');
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    /**
     * This Function process report template and checks for validate data
     * @param <type> $request
     */
    protected function processInternationalCurrencyReportSearchForm(sfWebRequest $request) {

        //    echo "<pre>"; print_r($request->getPostParameters()); die;
        $this->err = '';

        // $form->bind($request->getParameter('paidwithvisa'));
        try {
            ini_set('memory_limit', '1024M');
            ini_set("max_execution_time", "64000");
            $request->getParameter('merchant_service_id');
            $from_date = "";
            $to_date = "";
            $merchant = "";
            $user_id = "";
            $username = "";
            $merchant_service = "";

            $currency_code = sfConfig::get('app_dollar_currency');

            if ($request->hasParameter('uname')) {
                $username = $request->getParameter('uname');
            }

            if ($request->hasParameter('fdate')) {
                $start_date = $request->getParameter('fdate');
                if ($start_date != "") {
                    if (strstr($start_date, '/')) {
                        $dateArray = explode("/", $start_date);
                        $from_date = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[0] . "-" . $dateArray[1] . " 00:00:00"));
                    } else {
                        $from_date = date('Y-m-d H:i:s', strtotime($start_date . " 00:00:00"));
                    }
                }
            }

            if ($request->hasParameter('tdate')) {
                $end_date = $request->getParameter('tdate');
                if ($end_date != "") {
                    if (strstr($end_date, '/')) {
                        $dateArray = explode("/", $end_date);
                        $to_date = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[0] . "-" . $dateArray[1] . " 23:59:59"));
                    } else {
                        $to_date = date('Y-m-d H:i:s', strtotime($end_date . " 23:59:59"));
                    }
                }
            }

            //            echo $to_date;
            $split_amount_grand = 0;
            $paid_amount_grand = 0;
            $merchant = $request->getParameter('mid');

            $merchant_service = $request->getParameter('msid');

            //echo "$from_date"."$to_date"."$merchant"."$merchant_service"."$user_id"."$username";
            //die;
            $this->split_account = Doctrine::getTable('Merchant')->getMerchantAccounts($merchant);

            $currencyRecords = Doctrine::getTable('MerchantRequest')->CurrencyStatement($from_date, $to_date, $merchant, $merchant_service, $user_id = "", $username);

            $recordQuery = $currencyRecords;
            $recordArr = $recordQuery->execute();
            if (count($recordArr) > 0) {

                foreach ($recordArr as $record) {

                    $paid_amount_grand = $paid_amount_grand + $record['paid_amount'];

                    foreach ($record['MerchantRequestPaymentDetails']['0']['Pay4meSplit'] as $gsp) {
                        if (count($gsp) > 0) {

                            $split_amount_grand = $split_amount_grand + $gsp['amount'];
                        }
                    }
                }
            }

            //            echo $paid_amount_grand.' '.$split_amount_grand;
            $this->paidAmountGrand = $paid_amount_grand;
            $this->splitAmountGrand = $split_amount_grand;
            //
            //                                                        echo "<pre>";
            //                                                        print_r($currencyRecords->getSqlQuery());die;



            $pfmHelperObj = new pfmHelper();
            $this->postDataArray = array();
            if ($request->isMethod('post')) {
                unset($_SESSION['pfm']['visaPayment']);
                $_SESSION['pfm']['visaPayment'] = array('fdate' => $from_date, 'tdate' => $to_date, 'uname' => $username, 'mid' => $merchant, 'msid' => $merchant_service, 'user_id' => $user_id);
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            } else {
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            }

            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
                $this->postDataArray = $_SESSION['pfm']['visaPayment'];
            }

            $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($currencyRecords); //echo $this->pager->getNbResults(); die;
            $this->pager->setPage($this->page);
            $this->pager->init();


            //        }else{
            //            echo "hello";
            //            $this->forward('report', 'internationalCurrencyReport');
            //
            //        }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    /**
     *  International Currency Report Csv download
     * @param <type> $request
     */
    public function executeInternationalCurrencyReportCsv(sfWebRequest $request) {
        $this->postDataArray = $_SESSION['pfm']['visaPayment'];
        $from_date = "";
        $to_date = "";
        $merchant = "";
        $merchant_service = "";
        $username = "";
        $paid_amount_grand = 0;
        $split_amount_grand = 0;
        $currency_code = sfConfig::get('app_dollar_currency');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        if ($this->postDataArray['uname']) {
            $username = $this->postDataArray['uname'];
        }
        //print_r($this->postDataArray);die;

        $start_date = $this->postDataArray['fdate'];


        $end_date = $this->postDataArray['tdate'];


        //$formParam = $this->postDataArray['paidwithvisa'];
        $merchant = $this->postDataArray['mid'];

        //$formParam = $this->postDataArray['paidwithvisa'];
        $merchant_service = $this->postDataArray['msid'];

        $user_id = $this->getUser()->getGuardUser()->getId();

        //            echo $end_date." ".$start_date;
        $intcurrency = Doctrine::getTable('MerchantRequest')->getCsvInternationalCurrencyStatement($start_date, $end_date, $merchant, $merchant_service, $user_id = "", $username);


        $recordArr = $intcurrency;
        //         $recordArr = $recordQuery->execute();

        if (count($recordArr) > 0) {

            foreach ($recordArr as $record) {

                $paid_amount_grand = $paid_amount_grand + $record['paid_amount'];

                foreach ($record['MerchantRequestPaymentDetails']['0']['Pay4meSplit'] as $gsp) {
                    if (count($gsp) > 0) {

                        $split_amount_grand = $split_amount_grand + $gsp['amount'];
                    }
                }
            }
        }

        //                ($paid_amount_grand).' '.$split_amount_grand;
        $splitGrand = format_amount($split_amount_grand, '', 2);
        $pay4meGrandAmt = $paid_amount_grand - $splitGrand;
        $p4megrand = format_amount($pay4meGrandAmt, null);

        $headArr = array('S.No.', 'UserName', 'Transaction Date', 'Paid Amount');

        $split_account = Doctrine::getTable('Merchant')->getMerchantAccounts($merchant);
        foreach ($split_account as $val) {
            $var = array_push($headArr, $val->getName());
        }

        $var = array_push($headArr, 'Pay4Me Amount');

        $headerNames = $headArr;
        //echo '<pre>';
        //print_r($headerNames);print count($headerNames);
        //        echo '</pre>';
        //        exit;
        $headersArr = array('username', 'updated_at', 'paid_amount');
        foreach ($split_account as $val) {
            $var = array_push($headersArr, $val->getName());
        }
        $var = array_push($headersArr, 'Pay4Me Amount');



        $headers = $headersArr;

        $i = 0;


        //        echo '<pre>';
        //        print_r($intcurrency);
        //        echo '</pre>';
        //        exit;
        foreach ($intcurrency as $row) {
            // print "<pre>";
            //   print_r($row);
            $t = 0;
            $sum = 0;

            for ($index = 0; $index < count($headerNames) - 1; $index++) {


                if (array_key_exists($headers[$index], $row)) {
                    $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
                } else if (array_key_exists($t, $row['MerchantRequestPaymentDetails']['0']['Pay4meSplit'])) {
                    $sum = $row['MerchantRequestPaymentDetails']['0']['Pay4meSplit'][$t]['amount'] + $sum;

                    $recordsDetails[$i][$headers[$index]] = format_amount($row['MerchantRequestPaymentDetails']['0']['Pay4meSplit'][$t]['amount'], '', 1);
                    $t = $t + 1;
                } else if ($headers[$index] == "Pay4Me Amount") {
                    $sum = format_amount($sum, '', 1);
                    $pay4me_amount = $recordsDetails[$i]['paid_amount'] - $sum;
                    $recordsDetails[$i][$headers[$index]] = $pay4me_amount;
                }
                //}
            }


            $i++;
        }


        //$headerName = array()
        $headerNames = array("0" => $headerNames);
        //$headers =
        $paidGrand = format_amount($paid_amount_grand);
        $totalAmount = $p4megrand;

        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $arrList[] = array('', '', '', '', '', 'Grand Paid Amount', $paidGrand);
            $arrList[] = array('', '', '', '', '', 'Grand Split Amount', $splitGrand);
            $arrList[] = array('', '', '', '', '', 'Grand Pay4Me Amount', $totalAmount);

            $file_array = csvSave::saveExportListFile($arrList, "/report/internationalCurrencyReport/", "internationalCurrencyReport");

            //   $file_array = $this->saveExportListFile($arrList,'/report/internationalCurrencyReport/',"internationalCurrencyReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "internationalCurrencyReport";
            $this->setTemplate('csv');
        } else {
            $this->redirect_url('index.php/report/internationalCurrencyReport');
        }
        $this->setLayout(false);
        // $this->setTemplate('bankRep
    }

    /**
     * Function that displays MerchantService according to particular Merchant
     * @param <type> $request
     * @return <type>
     */
    public function executeMerchantService(sfWebRequest $request) {
        $merchant_id = $request->getParameter('merchant_id');
        if (($merchant_id != "") && ($merchant_id > 0)) {
            $var = Doctrine::getTable("MerchantService")->getServiceTypes($merchant_id);
            $service = $var->toArray(); {
                $str = "<option value='' selected>Please select Merchant Service</option>";
                if (count($service) == 0) {
                    return $str;
                } else {

                    foreach ($service as $key => $value) {
                        $selected = "";
                        $str .= "<option value='" . $value['id'] . "' " . $selected . ">" . $value['name'] . "</option>";
                    }
                    return $this->renderText($str);
                }
            }
            return $this->renderText($str);
        } else {
            $str = "<option value='' selected>Please select Merchant Service</option>";
            return $this->renderText($str);
        }
    }

    public function executeViewDetail(sfWebRequest $request) {
        $this->setLayout(false);
        $txnId = $request->getParameter('transId');
        $type = $request->getParameter('type');
        $pending = $request->getParameter('pending');

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $viewDetailQuery = Doctrine::getTable('GatewayOrder')->viewDetail($type, $txnId, $pending);

        $this->pager = new sfDoctrinePager('GatewayOrder', 2);
        $this->pager->setQuery($viewDetailQuery);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeCurrencyCode(sfWebRequest $request) {
        // $this->bank_branches =   //drop down for country
        //$request->getParameter('mId')
        //$request->getParameter('sId')
        $this->arrCurrency = Doctrine::getTable('CurrencyCode')->getAllDistinctCurrencyService($request->getParameter('id'), $request->getParameter('mId'), $request->getParameter('sId'));
        $this->setLayout(false);
   }

    public function executeContryBranchType(sfWebRequest $request) {


        $this->bank_branches = Doctrine::getTable('BankBranch')->getAllCountryBankBranch($request->getParameter('id'), $request->getParameter('countryId'));
    }

    public function executePaymentMode(sfWebRequest $request) {
        $this->chequeDraftMapped = false;
        $this->isBankUser = $this->getBankUser();
        $this->isAdmin = $this->getAdmin();
        if ($this->isBankUser) {
            $user_bank = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
            $records = Doctrine::getTable('BankMerchant')->findByBankId($user_bank);
            $this->merchant_account = $records->getFirst()->getMerchantAccount();
            $chequeDraftMappedDetails = Doctrine::getTable('BankSortCodeMapper')->findByBankId($user_bank);

            if ($chequeDraftMappedDetails->count()) {
                $this->chequeDraftMapped = true;
            }
        }
        $this->arrPaymentMode = Doctrine::getTable('PaymentModeOption')->getPaymentModeByMerchantService($request->getParameter('id'), $request->getParameter('sId'));

        $this->arrPaymentModeMerchantCounter = Doctrine::getTable('PaymentModeOption')->getPaymentModeForMerchantCounter($request->getParameter('id'), $request->getParameter('sId'));
        $this->setLayout(false);
    }

    public function executeGetBankTeller(sfWebRequest $request) {
        $bankId = $request->getParameter('bank');
        $countryId = $request->getParameter('country');
        $branchId = $request->getParameter('branch');
        $str = '<option value="">-- All Bank Tellers --</option>';
        if ($bankId != "" && $countryId != "" && $branchId != "") {
            $getTellerArray = Doctrine::getTable('BankUser')->getAllBankUsers($bankId, $branchId, $countryId);

            $str = '<option value="">-- All Bank Tellers --</option>';
            if ($getTellerArray) {
                foreach ($getTellerArray as $val) {
                    $str .= '<option value="' . $val['sfGuardUser']['id'] . '">' . $val['sfGuardUser']['username'] . '</option>';
                }
            }
        }


        return $this->renderText($str);
    }

    public function executeViewSplitDetail(sfWebRequest $request) {
        $this->merchantRequestId = "";


        $usePager = $request->getParameter('page');

        if ($request->getParameter('id') && $request->getParameter('id') != "") {
            $this->merchantRequestId = $request->getParameter('id');
        }
        $merchantReqObj = Doctrine::getTable('MerchantRequest')->findById($this->merchantRequestId);
        $this->currencyId = $merchantReqObj->getFirst()->getCurrencyId();
        $searchResultObj = Doctrine::getTable('PaymentSplitDetails')->getSplitDetail($this->merchantRequestId);

        Doctrine::getTable('MerchantService')->getServiceTypes($request->getParameter('id'));

        $this->setLayout('layout_popup');
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }


        $this->pager = new sfDoctrinePager('PaymentSplitDetails', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($searchResultObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    /*
     * sfAction :SplitPaymentReport
     */

    public function executeSplitPaymentReport(sfWebRequest $request) {

        if (!$this->userPermission(false)) {
            $this->forward('sfGuardAuth', 'secure');
        }

        if ($request->isMethod('post')) {
            $arrSplitDetail = $request->getParameter('splitDetail');
            $merchant_id = $arrSplitDetail['merchant_id'];
            $merchant_service_id = $arrSplitDetail['merchant_service_id'];
            $to_date = $arrSplitDetail['to']; // Vikash [WP055] Bug:36081 (07-11-2012)
        } else {
            $merchant_id = '';
            $to_date = '';
        }
        // Vikash [WP055] Bug:36081 (07-11-2012)
        $this->form = new MerchantSplitPaymentReportForm(array(), array('merchant_id' => $merchant_id , 'to_date' => $to_date));
        $this->formValid = "";
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('splitDetail'));
            if ($this->form->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    /*
     * sfAction :SplitPaymentReportSearch
     */

    public function executeSplitPaymentReportSearch(sfWebRequest $request) {

        $this->form = new MerchantSplitPaymentReportForm();
        $this->form->bind($request->getParameter('splitDetail'));
        $this->postDataArray = $request->getParameterHolder()->getAll();
        if (isset($this->postDataArray['splitDetail'])) {
            $this->postDataArray = $this->postDataArray['splitDetail'];
        }
        if (empty($this->postDataArray['page'])) {
            $_SESSION['pfm']['splitData'] = $this->postDataArray;
        }
        if ($this->form->isValid() || (isset($this->postDataArray['page']) )) {
            $this->arrMerchant = array();
            if ($this->postDataArray['merchant_id'] != "" && $this->postDataArray['merchant_id'] != "BLANK") {
                $this->merchant_id = $this->postDataArray['merchant_id'];
                $this->arrMerchant = $this->merchant_id;
            } else {
                $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
                $groupId = Settings::getGroupIdByGroupname(sfConfig::get('app_pfm_role_ewallet_user'));
                $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', '', $userId, $groupId);
                $merchantArray = array();
                if (count($serviceTypeArray)) {
                    foreach ($serviceTypeArray as $key => $val) {
                        $merchantArray[] = $key;
                    }
                    $this->arrMerchant = $merchantArray;
                }
                $this->merchant_id = '';
            }
            if ($this->postDataArray['merchant_service_id'] != "" && $this->postDataArray['merchant_service_id'] != "BLANK") {
                $this->merchant_service_id = $this->postDataArray['merchant_service_id'];
            } else {
                $this->merchant_service_id = '';
            }
            if ($this->postDataArray['from'] != "" && $this->postDataArray['from'] != "BLANK") {
                $this->from_date = $this->postDataArray['from'];
            } else {
                $this->from_date = "";
            }
            if ($this->postDataArray['to'] != "" && $this->postDataArray['to'] != "BLANK") {
                $this->to_date = $this->postDataArray['to'];
            } else {
                $this->to_date = "";
            }
            $queryObj = Doctrine::getTable('PaymentSplitDetails')->getPaymentSplitDetails($this->arrMerchant, $this->merchant_service_id, $this->from_date, $this->to_date);
            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('PaymentSplitDetails', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($queryObj);
            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {
            die;
        }
    }

    /*
     * sfAction : SplitPaymentReportCsv
     */

    public function executeSplitPaymentReportCsv(sfWebRequest $request) {

        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $postDataArray = $_SESSION['pfm']['splitData'];
        $merchant_id = $postDataArray['merchant_id'];
        $merchant_service_id = $postDataArray['merchant_service_id'];
        $strFromDate = $postDataArray['from'];
        $strToDate = $postDataArray['to'];

        if (empty($merchant_id)) {
            $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
            $groupId = Settings::getGroupIdByGroupname(sfConfig::get('app_pfm_role_ewallet_user'));
            $serviceTypeArray = Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', '', $userId, $groupId);
            $merchantList = "";
            if (count($serviceTypeArray)) {
                foreach ($serviceTypeArray as $key => $val) {
                    $merchantList .= $key . ",";
                }
            }
        } else {
            $merchantList = $merchant_id;
        }

        $merchantList = trim($merchantList, ',');
        $records = Doctrine::getTable('PaymentSplitDetails')->getCSVSplitPaymentAccount($merchantList, $merchant_service_id, $strFromDate, $strToDate);
        $filename = "SplitPaymentReport_" . date("YmdH:m:s");
        $headerNames = array(0 => array('S.No.', 'Account Number', 'Account Name', 'Total Amount', 'Currency Code'));
        $headers = array('from_acct_no', 'account_name', 'total_amount', 'currency_code');
        $i = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $recordsDetails = array();
        while ($row = mysql_fetch_array($records)) {
            for ($index = 0; $index < count($headers); $index++) {
                if ($headers[$index] == 'total_amount') {
                    $recordsDetails[$i][$headers[$index]] = format_amount($row[$headers[$index]], null, 1);
                } else {
                    $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
                }
            }
            $i++;
        }
        if (count($recordsDetails) > 0) {
            $ApiObj = new APIHelper();
            $arrList = $ApiObj->getCSVMergeArray($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, "/report/splitPaymentReport/", "SplitPaymentReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "SPLIT_PAYMENT_REPORT";
        } else {
            $this->redirect_url('index.php/report/SplitPaymentReport');
        }
        $this->setTemplate('csv');
        $this->setLayout(false);
    }

    /**
     * Function that displays MerchantService according to particular Merchant
     * @param <type> $request
     * @return <type>
     */
    public function executeMerchantServiceSplit(sfWebRequest $request) {
        $merchant_id = $request->getParameter('merchant_id');
        if (($merchant_id != "") && ($merchant_id > 0)) {
            $pfmHelperObj = new pfmHelper();
            $this->userGroup = $pfmHelperObj->getUserGroup();
            if ($this->userGroup == 'ewallet_user') {
                $user = $this->getUser();
                $ewalletUserId = $user->getGuardUser()->getId();
                $flag = Doctrine::getTable("Merchant")->checkMerchantForEwalletUser($merchant_id, $ewalletUserId);
                if (!$flag) {
                    $str = "<option value='' selected>--All Merchant Services--</option>";
                    return $this->renderText($str);
                }
            }
            $var = Doctrine::getTable("MerchantService")->getServiceTypes($merchant_id);
            $service = $var->toArray();
            $str = "<option value='' selected>--All Merchant Services--</option>";
            if (count($service) == 0) {
                return $this->renderText($str);
            } else {
                foreach ($service as $key => $value) {
                    $selected = "";
                    $str .= "<option value='" . $value['id'] . "' " . $selected . ">" . $value['name'] . "</option>";
                }
                return $this->renderText($str);
            }
        } else {
            $str = "<option value='' selected>--All Merchant Services--</option>";
            return $this->renderText($str);
        }
    }

    private function userPermission($reportType) {
        $userPermission = new menuHelper();
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        if ($reportType) {
            $arrGroups = array(
                sfConfig::get('app_pfm_role_bank_admin') =>
                sfConfig::get('app_pfm_role_bank_admin'),
                sfConfig::get('app_pfm_role_bank_branch_user') =>
                sfConfig::get('app_pfm_role_bank_branch_user'),
                sfConfig::get('app_pfm_role_report_bank_admin') =>
                sfConfig::get('app_pfm_role_report_bank_admin'),
                sfConfig::get('app_pfm_role_bank_country_head') =>
                sfConfig::get('app_pfm_role_bank_country_head'),
                sfConfig::get('app_pfm_role_bank_e_auditor') =>
                sfConfig::get('app_pfm_role_bank_e_auditor'),
                sfConfig::get('app_pfm_role_bank_branch_report_user') =>
                sfConfig::get('app_pfm_role_bank_branch_report_user'),
                sfConfig::get('app_pfm_role_ewallet_user') =>
                sfConfig::get('app_pfm_role_ewallet_user'),
                sfConfig::get('app_pfm_role_report_admin') =>
                sfConfig::get('app_pfm_role_report_admin'),
                sfConfig::get('app_pfm_role_report_portal_admin')=>
                sfConfig::get('app_pfm_role_report_portal_admin'),
                sfConfig::get('app_pfm_role_country_report_user') =>
                sfConfig::get('app_pfm_role_country_report_user'),
                sfConfig::get('app_pfm_role_admin') =>
                sfConfig::get('app_pfm_role_admin'));
        } else {
            $arrGroups = array(
                sfConfig::get('app_pfm_role_ewallet_user') =>
                sfConfig::get('app_pfm_role_ewallet_user'),
                sfConfig::get('app_pfm_role_report_admin') =>
                sfConfig::get('app_pfm_role_report_admin'),
                sfConfig::get('app_pfm_role_report_portal_admin')=>
                sfConfig::get('app_pfm_role_report_portal_admin'),
                sfConfig::get('app_pfm_role_admin') =>
                sfConfig::get('app_pfm_role_admin')
            );
        }
        $groupArray = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
        $presentGroup = $groupArray['sfGuardGroup']['name'];
        if (!$userPermission->checkUserPermissions($arrGroups, $presentGroup, $userId, true)) {
            return true;
        }
    }

    public function executeGetSelectedCurrency(sfWebRequest $request) {
        $countryId = $request->getParameter('country');
        $currencyId = Doctrine::getTable('CurrencyCode')->getSelectedCurrency($countryId);
        return $this->renderText($currencyId);
    }

    /*
     * action :PaidByVisaReportRechargeCsv
     * WP051
     */

    public function executePaidByVisaReportRechargeCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = $_SESSION['pfm']['visaPayment'];
        $from_date = "";
        $to_date = "";
        $status = "";
        $username = "";
        $transactionNo = "";
        $accountNo = "";
        $currency_code = sfConfig::get('app_naira_currency');
        $currency = '1'; //for naira only
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $formParam = $this->postDataArray['paidwithvisa'];

        if ($formParam['username']) {
            $username = $formParam['username'];
        }
        if ($formParam['transaction_no'] != "") {
            $transactionNo = $formParam['transaction_no'];
        }
        if ($formParam['account_no'] != "") {
            $accountNo = $formParam['account_no'];
        }
        $start_date = $formParam['from'];
        if ($start_date != "") {
            $from_date = $start_date . ' 00:00:00';
        }
        $end_date = $formParam['to'];
        if ($end_date != "") {
            $to_date = $end_date . ' 23:59:59';
        }
        $status = $formParam['trans_type'];
        if ($status == "all" || $status == "") {
            $status = "";
        }
        $PaymentMode = $formParam['pay_mode'];
        $displayPaymentMode = '';
        if ($PaymentMode) {
            $objPaymentMode = Doctrine::getTable('PaymentModeOption')->find($PaymentMode);
            $displayPaymentMode = $objPaymentMode->getPaymentMode()->getDisplayName();
            $displayPaymentMode = preg_replace("/[\/ ]/", "_", $displayPaymentMode);
        }
        $PaymentType = $formParam['pay_type'];
        $user_id = $this->getUser()->getGuardUser()->getId();

        $parModel = 'app_paymode_' . $PaymentMode . "_name";
        $visaRecords = Doctrine::getTable('GatewayOrder')->getCsvRechargeStatement($from_date,
                        $to_date, $status, $currency, $username, $PaymentMode, $PaymentType, $accountNo);
        $headerNames = array(0 => array('S.No.', 'Date of Transaction', 'Transaction Id',
                'Validation No', 'Service Type', 'Account No',
                'Account Name', 'Recharge Amount', 'Services Charge',
                'Total Amount')
        );

        $headers = array('transtime', 'order_id', 'validation_number', 'type',
            'account_number', 'account_name', 'actual_amount', 'servicecharge_kobo', 'purchased_amount');
        $i = 0;
        $arrayAmount = array('purchased_amount', 'servicecharge_kobo', 'actual_amount');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        while ($row = mysql_fetch_array($visaRecords)) {

            for ($index = 0; $index < count($headers); $index++) {

                if (in_array($headers[$index], $arrayAmount)) {
                    $recordsDetails[$i][$headers[$index]] = format_amount($row[$headers[$index]], null, 1);
                } else {
                    $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
                }
            }
            $i++;
        }
        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, '/report/paidByCardReport/', "rechargeVia" . $displayPaymentMode . "Report");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "PAIDBY_CARD";
        } else {
            $this->redirect_url('index.php/report/paidByCardReport');
        }
        $this->setTemplate('csv');
    }

    private function construct_options_array($data_arrays) {

        $array_to_return = array();
        foreach ($data_arrays as $value) {
            $array_to_return[$value['id']] = $value['name'];
        }
        return $array_to_return;
    }


    private function getBankUser()
    {
        $isbankUser = 0;
        $user=sfContext::getInstance()->getUser();
        $group=$user->getGroupNames();
        if($group[0] == sfConfig::get('app_pfm_role_bank_admin') || $group[0] == sfConfig::get('app_pfm_role_bank_country_head')
                || $group[0] == sfConfig::get('app_pfm_role_country_report_user') ||  $group[0] == sfConfig::get('app_pfm_role_bank_branch_user')
                || $group[0] == sfConfig::get('app_pfm_role_report_bank_admin')  ||  $group[0] == sfConfig::get('app_pfm_role_bank_e_auditor')
              || $group[0] == sfConfig::get('app_pfm_role_bank_branch_report_user')  ){
                          $isbankUser = 1;
                }
                return $isbankUser;
    }
    private function getAdmin()
    {
        $isAdmin = 0;
        $user=sfContext::getInstance()->getUser();
        $group=$user->getGroupNames();
        if($group[0] == sfConfig::get('app_pfm_role_admin') || $group[0] == sfConfig::get('app_pfm_role_report_admin')
                || $group[0] == sfConfig::get('app_pfm_role_report_portal_admin')     ){
                          $isAdmin = 1;
                }
                return $isAdmin;
    }

    /**
    * Function to get all merchant transaction records.
    * @param sfWebRequest $request
    * @author Amit Maheshwari
    * @date 6-12-2013
    */
    public function executeCollectionSummaryReport(sfWebRequest $request) {
      $this->userGroup = "";
      $pfmHelperObj = new pfmHelper();
      $this->userGroup = $pfmHelperObj->getUserGroup();
      $bank_details = $pfmHelperObj->getUserAssociatedBank();

      if (count($bank_details) > 0) {
        $bank_details_id = $bank_details['id'];
      }
      else {
        $bank_details_id = 0;
      }
      $this->form = new BankReportForm('', array('bank_details_id' => $bank_details_id));
      $this->form->setDefault('report_label', $request->getParameter('label'));
    }

    /**
    * Function to get search form to get all merchant transaction records.
    * @param sfWebRequest $request
    * @author Amit Maheshwari
    * @date 6-12-2013
    */
    public function executeCollectionSummaryReportSearch(sfWebRequest $request) {
      ini_set('memory_limit', '1024M');
      ini_set("max_execution_time", "64000");
      $this->userGroup = "";
      $pfmHelperObj = new pfmHelper();
      $this->userGroup = $pfmHelperObj->getUserGroup();
      $this->postDataArray = array();
      if ($request->isMethod('post')) {
        unset($_SESSION['pfm']['reportData']);
        $postDataArray = $request->getPostParameters();
        $_SESSION['pfm']['reportData'] = $postDataArray['bank_branch_report'];

        if (isset($postDataArray['bank'])) {
          $_SESSION['pfm']['reportData']['bank'] = $postDataArray['bank'];
        }
        if (isset($postDataArray['country'])) {
          $_SESSION['pfm']['reportData']['country'] = $postDataArray['country'];
        }
        if (isset($postDataArray['branch'])) {
          $_SESSION['pfm']['reportData']['branch'] = $postDataArray['branch'];
        }
        $this->postDataArray = $_SESSION['pfm']['reportData'];
      }
      else {
        $this->postDataArray = $_SESSION['pfm']['reportData'];
      }
      $this->page = 1;
      if ($request->hasParameter('page')) {
        $this->page = $request->getParameter('page');
        $this->postDataArray = $_SESSION['pfm']['reportData'];
      }
      $this->merchantName = 'Merchants';
      $this->collection_report_list = MerchantRequestTable::getAllCollectionSummaryReportDetails($this->postDataArray);
      $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
      Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
      $reports = $this->collection_report_list->execute();
      Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);

      $grandTotalAmount = '';
      $grandTotalTransactions = '';
      $grandNisAmount = '';
      $grandPay4MeAmount = '';
      $grandBankAmount = '';
      foreach ($reports as $report) {
        $grandTotalAmount = $grandTotalAmount + $report->amount;
        $grandTotalTransactions = $grandTotalTransactions + $report->no_of_transactions;
        $grandNisAmount = $grandNisAmount + $report->merchant_amount;
        $grandPay4MeAmount = $grandPay4MeAmount + $report->payforme_amount;
        $grandBankAmount = $grandBankAmount + $report->bank_amount;
      }
      $this->grandTotalAmount = $grandTotalAmount;
      $this->grandTotalTransactions = $grandTotalTransactions;
      $this->grandNisAmount = $grandNisAmount;
      $this->grandPay4MeAmount = $grandPay4MeAmount;
      $this->grandBankAmount = $grandBankAmount;

      $this->pager = new sfDoctrinePager('MerchantRequest', sfConfig::get('app_records_per_page'));
      $this->pager->setQuery($this->collection_report_list);
      $this->pager->setPage($this->page);
      $this->pager->init();
    }

    /**
     * Function to generate CSV file for all merchant transaction record.
     * @param sfWebRequest $request
     * @author Amit Maheshwari
     * @date 6-12-2013
     */
    public function executeCollectionSummaryReportCsv(sfWebRequest $request) {
      ini_set('memory_limit', '1024M');
      ini_set("max_execution_time", "64000");
      $grandTotalTransactions = 0;
      $grandTotalAmount = 0;
      $grandNisAmount = 0;
      $grandPay4MeAmount = 0;
      $grandBankAmount = 0;
      $this->postDataArray = $_SESSION['pfm']['reportData'];
      $records = MerchantRequestTable::getCSVallCollectionSummaryReportDetails($this->postDataArray);
      $merchantName = 'Merchants Amount';

      // Creating Headers
      $headerNames = array(
        0 => array('S.No.', 'Merchant Name', 'No. of Transaction', 'Total Paid Amount', "$merchantName", 'Pay4Me Amount', 'Bank Amount'));
      $headers = array('merchant_name', 'no_of_transactions', 'amount', 'merchant_amount', 'payforme_amount', 'bank_amount');

      $i = 0;
      while ($row = mysql_fetch_array($records)) {
        for ($index = 0; $index < count($headers); $index++) {
          $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
        }
        $grandTotalAmount = $grandTotalAmount + $row['amount'];
        $grandTotalTransactions = $grandTotalTransactions + $row['no_of_transactions'];
        $grandNisAmount = $grandNisAmount + $row['merchant_amount'];
        $grandPay4MeAmount = $grandPay4MeAmount + $row['payforme_amount'];
        $grandBankAmount = $grandBankAmount + $row['bank_amount'];
        $i++;
      }

      $recordsDetails[$i]['Grand Total'] = $grandTotalAmount;
      $recordsDetails[$i]['No of transactions'] = $grandTotalTransactions;
      $recordsDetails[$i]['Merchant Amount'] = $grandNisAmount;
      $recordsDetails[$i]['Payforme Amount'] = $grandPay4MeAmount;
      $recordsDetails[$i]['Bank Amount'] = $grandBankAmount;

      if (count($recordsDetails) > 0) {
        $folderPath = self::$Collection_Summary;
        $arrList = $this->getXLSExportList($headerNames, $headers, $recordsDetails);
        $file_array = $this->saveExportListFile($arrList, $folderPath, "SummaryCollectionReport");
        $file_array = explode('#$', $file_array);
        $this->fileName = $file_array[0];
        $this->filePath = $file_array[1];
        $this->folder = "Collection_Summary";//MERCHANT_TRANSACTION";
      }
      else {
        $this->redirect_url('index.php/report/collectionSummaryReport');
      }
      $this->setTemplate('csv');
      $this->setLayout(false);
    }

    /**
     * function will lead to view page from where reports can be download
     *
     * @access public
     * @param nothing
     * @return Doesn't return any thing rather lead to report view page
     * @author Ramandeep
     */

    public function executeEwalletRechargeReportDisplay(sfWebRequest $request){

      $this->folder = "eWallet_RECHARGE_REPORT";
        $userId = $this->getUser()->getGuardUser()->getid();
      $this->reportId = $request->getParameterHolder()->get('id');

      if($this->reportId){
          Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
          $this->dataById = Doctrine::getTable('ReportLog')->findById($this->reportId)->toArray();
          if(isset($this->dataById[0]['bank_id'])){
              $this->dataById[0]['Bank'] = Doctrine::getTable('Bank')->findById($this->dataById[0]['bank_id'])->toArray();
          }
          Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);

//          $this->dataById[0]['parameters'] = json_encode(Doctrine::getTable('ReportLog')->setcorrespondingName(json_decode($this->dataById[0]['parameters'],true)));

          if(isset($this->dataById[0]['deleted_at'])){
              $this->type = "Archive";
          }else{
              $this->type = "Generated";
          }
          if(!(isset($this->dataById[0]['file_name']))){
              $this->type = "Pending";
          }
          if(count($this->dataById) == 0){
              $this->dataById = array();
              $this->form = new ReportSearchForm('',array('type' => $this->type, 'frequency' => 'CUSTOM'));
          }else{
              if($this->dataById[0]['file_name'] == '' && $this->dataById[0]['deleted_at'] != ''){
                $this->dataById = array();
                $this->form = new ReportSearchForm('',array('type' => $this->type, 'frequency' => 'CUSTOM'));
            }else{
                $this->form = new ReportSearchForm('',array('type'=>$this->type,'frequency'=>$this->dataById[0]['frequency']));
            }
          }
      }
      else{
        $this->form = new ReportSearchForm('',array('type' => $this->type, 'frequency' => 'CUSTOM'));
      }

        if ($request->isMethod('post')) {
            $guardObj = sfContext::getInstance()->getUser()->getGuardUser();

            $pfmHelperObj = new pfmHelper();
            $this->userGroup = $pfmHelperObj->getUserGroup();

            /*
             * Being put for future prospective
             */
            $userBankId = NULL;
//              $userBankBranchId = Null;
//              $userCountryId = Null;
            if ($this->userGroup == "bank_admin") {
                $userBankId = $guardObj->getBankUser()->getFirst()->getBankId();

//                      $userBankBranchId = $guardObj->getBankUser()->getFirst()->getBankBranchId();
//                      $userCountryId = "";
            }

            if ($this->userGroup != "portal_admin") {
                if (count($request->getParameter('archive'))) {
                    $reportIds = Doctrine::getTable('ReportLog')->fetchReport('id', array('user_id' => $userId));

                    // convert multidimentional array to single dimentional
                    $reportIds = array_map('current', $reportIds);
                    // checking that remove or archive request is being placed by creator only
                    $diffResultSet = array_intersect($reportIds, $request->getParameter('archive'));

                    if (array_diff($request->getParameter('archive'), $diffResultSet)) {
                        $this->getUser()->setFlash('notice', sprintf('You are not authorized for this action'));
                        $this->redirect('report/ewalletRechargeReportDisplay');
                    }
                }
            }

            /*
             * Archive Functionality
             */
            //*****************************
            if ($request->getParameterHolder()->get('request') == 'Archive') {
                $jobIds = explode(',', $request->getParameterHolder()->get('formIds'));
                $records = Doctrine::getTable('ReportLog')->deleteReportById($jobIds);
                $message = 'Report Archived successfully.';
                if($request->getParameterHolder()->get('refresh') == "yes"){
                    $this->getUser()->setFlash('notice', sprintf('Report archived successfully.'));
                }
            }
            //*****************************

            /*
             * Pending Functionality
             */
            //*****************************
            if($request->getParameterHolder()->get('request') == 'Pending'){
                $jobIds = explode(',', $request->getParameterHolder()->get('formIds'));
                $records = Doctrine::getTable('ReportLog')->removeJob($jobIds, $userId);
                $message = 'Request canceled successfully.';
                if($request->getParameterHolder()->get('refresh') == "yes"){
                    $this->getUser()->setFlash('notice', sprintf('Report canceled successfully.'));
                }
            }
            //*****************************

            $startDate = '';
            $endDate = '';

            if ($request->getParameter('frequency') == 'QUARTERLY') {
                if ($request->getParameter('quarter') == 1) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-01-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-03-31 23:59:59';
                }
                if ($request->getParameter('quarter') == 2) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-04-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-06-30 23:59:59';
                }
                if ($request->getParameter('quarter') == 3) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-07-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-09-30 23:59:59';
                }
                if ($request->getParameter('quarter') == 4) {
                    $startDate .= $request->getParameter('year');
                    $startDate .= '-10-01 00:00:00';

                    $endDate .= $request->getParameter('year');
                    $endDate .= '-12-31 23:59:59';
                }
            }
            if ($request->getParameter('frequency') == 'MONTHLY' || $request->getParameter('frequency') == 'WEEKLY') {
                $startDate .= $request->getParameter('year');
                $startDate .= '-' . $request->getParameter('month');
                $startDate .= '-01 00:00:00';

                $endDate .= $request->getParameter('year');
                $endDate .= '-' . $request->getParameter('month');
                $thirtyFirst = array('01', '03', '05', '07', '08', '10', '12');

                if (in_array($request->getParameter('month'), $thirtyFirst)) {
                    $endDate .= '-31 23:59:59';
                } elseif ($request->getParameter('month') == '02') {
                    // leap year check
                    if (date('L', mktime(0, 0, 0, 1, 1, $request->getParameter('year')))) {
                        $endDate .= '-29 23:59:59';
                    } else {
                        $endDate .= '-28 23:59:59';
                    }
                } else {
                    $endDate .= '-30 23:59:59';
                }
                if ($request->getParameter('frequency') == 'WEEKLY') {
                    if (date('N', strtotime($startDate)) != "1") {
                        $start = date('N', strtotime($startDate)) - 1;
                        $startDate = date('Y-m-d H:i:s', strtotime('-' . $start . ' days', strtotime($startDate)));
                    }
                    if (date('N', strtotime($endDate)) != "7") {
                        $end = 7 - date('N', strtotime($endDate));                        
                        $endDate = date('Y-m-d H:i:s', strtotime('+' . $end . ' days', strtotime($endDate)));
                    }
                }
            }
            if ($request->getParameter('frequency') == 'CUSTOM') {
                $startDate .= $request->getParameter('year');
                $startDate .= '-01-01 00:00:00';

                $endDate .= $request->getParameter('year');
                $endDate .= '-12-31 23:59:59';
            }

            $values = array(
                'from_date' => $startDate,
                'to_date' => $endDate,
                'banks' => $request->getParameter('banks'),
                'frequency' => $request->getParameter('frequency'),
                'type' => $request->getParameter('type'),
                'report_name' => "EwalletRechargeReport",
            );

            if ($this->userGroup != "portal_admin") {
                if (isset($userBankId)) {
                    $values['userBankId'] = $userBankId;
                }
                if ($request->getParameter('frequency') == 'CUSTOM') {
                    $values['user_id'] = $userId;
                }
            }
//            echo "<pre>";
//            print_r($values);
//            die;

            $records_query = Doctrine::getTable('ReportLog')->fetchReportName($values);
            if (isset($message)) {
                $this->msg = $message;
            }

            if($request->getParameter('type')){
                $this->type = $request->getParameter('type');
            }
            if($request->getParameter('frequency')){
                $this->frequency = $request->getParameter('frequency');
            }
            if($request->getParameter('banks')){
                $this->banks = $request->getParameter('banks');
            }else{
                $this->banks = ""; 
                // being set to black as needed in reportListing
                // if not pass then in reportLog it will not bring related bank name along with it
            }
            if($request->getParameter('year')){
                $this->year = $request->getParameter('year');
            }
            $this->report = 'EwalletRechargeReport';

            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('ReportLog', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($records_query);
            $this->pager->setPage($this->page);
            $this->pager->init();
            
            $this->setTemplate('reportListing');
        }
    }

    /**
     * function will set cron job for ewallertrecharge report for all banks
     *
     * @access public
     * @param type Can be either monthly weekly or quarterly
     * @return Message
     * @author Ramandeep Singh
     */
    public function executeSetJobEWalletRechargeReport(sfWebRequest $request){

    	$type = $request->getParameter("type");

    	if($type == "MONTHLY" || $type == "WEEKLY" || $type == "QUARTERLY"){

        $startDate = $request->getParameter("startdate");
    	$endDate =  $request->getParameter("enddate");

        if(!$startDate || !($endDate) || !($type)){
            die("All three parameters are compulsory");
        }
        /*
        * Removing all earlier monthly,weekly and quarterly jobs
        */
        Doctrine::getTable('EpJob')->findAndDeleteJobByFrequency($type,'EwalletRechargeReport');

    		$bankIds = Doctrine::getTable('Bank')->getAllBankIds();

    		foreach ($bankIds as $key => $value){
    			$postDataArray['banks'] = $value['id'];
    			$user = $this->getUser()->getGuardUser()->getid();
    			$postDataArray['user_id'] = $user;

                        /*
                         * $startTime and $endTime
                         * startTime and endTime are the time related
                         * to job i.e. the job will be executed for ehich time interval
                         */

    			$name		= "EwalletRechargeReport";
    			$url 		= $this->moduleName . "/eWalletRechargeReportCsv";
    			$numOfRetry	= -1;

                        /*@author ramandeep
                         *
                         * being set -1 because
                         * PluginEpJob.class.php
                         * file hold function save() which check to set the value from CONSTANT class
                         */
    			$appName	= null;
    			$endTime	= date('Y-m-d H:i:s', strtotime($endDate." 23:59:59"));

                        $postDataArray['report_name'] = "EwalletRechargeReport";
    			if($type == "MONTHLY"){
                            $postDataArray['frequency']='MONTHLY';
                            $startTime	= date('Y-m-d H:i:s', strtotime($startDate." 03:00:00"));
                            $postDataArray['file_name']='EwalletRechargeReport-'.str_replace(" ", "", $value['bank_name'])."_Monthly" ;
                            $minutes	='0';
                            $hours	='3';
                            $dayofmonth	='1';

                            $parameters	=$postDataArray;
                            $taskId = EpjobsContext::getInstance()->addJobForEveryMonth($name,$url,$endTime,$parameters,$numOfRetry,$appName,$startTime,$minutes,$hours,$dayofmonth);
                            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

    			}
    			if($type == "WEEKLY"){
                            $postDataArray['frequency']='WEEKLY';
                            $startTime	= date('Y-m-d H:i:s', strtotime($startDate." 05:00:00"));
                            $postDataArray['file_name']='EwalletRechargeReport-'.str_replace(" ", "", $value['bank_name'])."_Weekly" ;
                            $minutes	='0';
                            $hours	='5';

                            $dayOfMonth	=null;
                            $month	="*";
//                            $dayOfWeek  =""; // being handel in EpjobsContext -> function addJobForEveryWeek

                            $parameters	=$postDataArray;
                            $taskId = EpjobsContext::getInstance()->addJobForEveryWeek($name,$url,$endTime,$parameters,$numOfRetry,$appName,$startTime,$minutes,$hours,$dayOfMonth,$month);
                            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

    			}
    			if($type == "QUARTERLY"){
                            $postDataArray['frequency']='QUARTERLY';
                            $startTime	= date('Y-m-d H:i:s', strtotime($startDate." 05:00:00"));
                            $postDataArray['file_name']='EwalletRechargeReport-'.str_replace(" ", "", $value['bank_name'])."_Quarterly" ;
                            $minutes	='0';
                            $hours	='5';
                            $dayOfMonth	='2';
                            $month	="1,4,7,10";

                            $parameters	=$postDataArray;
                            /*
                             *  calling Month function bec quarterly event is beeing handel from parameter $month
                             */
                            $taskId = EpjobsContext::getInstance()->addJobForEveryMonth($name,$url,$endTime,$parameters,$numOfRetry,$appName,$startTime,$minutes,$hours,$dayOfMonth,$month);
                            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

    			}
//                        being removed as now the entry in 
//                        report table will be made when the report is being getting generated
//                        unset($parameters['file_name']);
//                        $parameters['ep_job_id'] = $taskId;
//                        $reportId = Doctrine::getTable('ReportLog')->saveReport($parameters);

//                        die("one job saved");
    		}
    		die("Jobs are set Successfully");
    	}

    	die("Dont have right to access this page");
    }

    /*
     * Function to send mail on ewallet recharge report creation
     */
        public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendEwalletReportCreationEmail($userid, $subject, $partialName);
        return $this->renderText($mailInfo);
    }

    /*
     * function will set cron job for ewallert financial report
     *
     * @access public
     * @param type Can be either monthly weekly or quarterly
     * @return Message
     * @author Ramandeep Singh
     */
    public function executeSetJobEWalletFinancialReport(sfWebRequest $request){

    	$type = $request->getParameter("type");

    	if($type == "MONTHLY" || $type == "WEEKLY" || $type == "QUARTERLY"){

        $startDate = $request->getParameter("startdate");
    	$endDate =  $request->getParameter("enddate");

        if(!$startDate || !($endDate) || !($type)){
            die("All three parameters are compulsory");
        }
        /*
        * Removing all earlier monthly,weekly and quarterly jobs
        */
        Doctrine::getTable('EpJob')->findAndDeleteJobByFrequency($type,'EwalletFinancialReport');

    			$userId = $this->getUser()->getGuardUser()->getid();
    			$postDataArray['userId'] = $userId;

                        /*
                         * $startTime and $endTime
                         * startTime and endTime are the time related
                         * to job i.e. the job will be executed for ehich time interval
                         */

    			$name		= "EwalletFinancialReport";
    			$url 		= "ewallet/ewalletFinancialReportCsv";
    			$numOfRetry	= -1;

                        /*@author ramandeep
                         *
                         * being set -1 because
                         * PluginEpJob.class.php
                         * file hold function save() which check to set the value from CONSTANT class
                         */
    			$appName	= null;
    			$endTime	= date('Y-m-d H:i:s', strtotime($endDate." 23:59:59"));
                        $postDataArray['currency_id'] = 1;// setting by default to Naira will change according to need
                        $postDataArray['report_name'] = "EwalletFinancialReport";
    			if($type == "MONTHLY"){
                            $postDataArray['frequency']='MONTHLY';
                            $startTime	= date('Y-m-d H:i:s', strtotime($startDate." 03:00:00"));
                            $postDataArray['file_name']='EwalletFinancialReport-'.$userId."_Monthly" ;
                            $minutes	='0';
                            $hours	='3';
                            $dayofmonth	='1';

                            $parameters	=$postDataArray;
                            $taskId = EpjobsContext::getInstance()->addJobForEveryMonth($name,$url,$endTime,$parameters,$numOfRetry,$appName,$startTime,$minutes,$hours,$dayofmonth);
                            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

    			}
    			if($type == "WEEKLY"){
                            $postDataArray['frequency']='WEEKLY';
                            $startTime	= date('Y-m-d H:i:s', strtotime($startDate." 05:00:00"));
                            $postDataArray['file_name']='EwalletFinancialReport-'.$userId."_Weekly" ;
                            $minutes	='0';
                            $hours	='5';

                            $dayOfMonth	=null;
                            $month	="*";
//                            $dayOfWeek  =""; // being handel in EpjobsContext -> function addJobForEveryWeek

                            $parameters	=$postDataArray;
                            $taskId = EpjobsContext::getInstance()->addJobForEveryWeek($name,$url,$endTime,$parameters,$numOfRetry,$appName,$startTime,$minutes,$hours,$dayOfMonth,$month);
                            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

    			}
    			if($type == "QUARTERLY"){
                            $postDataArray['frequency']='QUARTERLY';
                            $startTime	= date('Y-m-d H:i:s', strtotime($startDate." 05:00:00"));
                            $postDataArray['file_name']='EwalletFinancialReport-'.$userId."_Quarterly" ;
                            $minutes	='0';
                            $hours	='5';
                            $dayOfMonth	='2';
                            $month	="1,4,7,10";

                            $parameters	=$postDataArray;
                            /*
                             *  calling Month function bec quarterly event is beeing handel from parameter $month
                             */
                            $taskId = EpjobsContext::getInstance()->addJobForEveryMonth($name,$url,$endTime,$parameters,$numOfRetry,$appName,$startTime,$minutes,$hours,$dayOfMonth,$month);
                            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

    			}
//                        being removed as now the entry in 
//                        report table will be made when the report is being getting generated
//                        unset($parameters['file_name']);
//                        $parameters['ep_job_id'] = $taskId;
//                        $reportId = Doctrine::getTable('ReportLog')->saveReport($parameters);

//                        die("one job saved");

    		die("Jobs are set Successfully");
    	}

    	die("Dont have right to access this page");
    }

    /** Bank User filtered on user roles **/
    public function executeBankUserRoleReport(sfWebRequest $request) {
    	$this->form = new TestBankReportForm();
    	   if (!$this->userPermission(true)) {
    	$this->forward('sfGuardAuth', 'secure');
    	}
    	$this->userGroup = "";
    	$pfmHelperObj = new pfmHelper();
    	$this->userGroup = $pfmHelperObj->getUserGroup();
    	echo $this->userGroup.'----->';
    	$this->report_label = $request->getParameter('label');
    	$bank_details = $pfmHelperObj->getUserAssociatedBank();
    	if (count($bank_details) > 0) {
    	$bank_details_id = $bank_details['id'];
    	} else {
    	$bank_details_id = 0;
    	}
    	$this->form = new BankUserReportForm('',array('bank_details_id' => $bank_details_id,'userGroup'=>$this->userGroup,'report_label'=>$this->report_label));
    	if($request->isMethod('post')){
    	$this->form->bind($request->getPostParameters());
    	if($this->form->isValid()) {
    	$this->forward($this->moduleName,'search');
    	}
    	}

    }
    	//test bank repot
    public function executeBankUserRoleReportSearch(sfWebRequest $request) {

    	$this->userGroup = "";
    	$pfmHelperObj = new pfmHelper();
    	$this->userGroup = $pfmHelperObj->getUserGroup();
    	$this->postDataArray = array();
    	if ($request->isMethod('post')) {
    		unset($_SESSION['pfm']['reportData']);
    		$_SESSION['pfm']['reportData'] = $request->getPostParameters();
    		$this->postDataArray = $_SESSION['pfm']['reportData'];

    	} else {

    		$this->postDataArray = $_SESSION['pfm']['reportData'];
    		}
    		$this->page = 1;
    				if ($request->hasParameter('page')) {
    						$this->page = $request->getParameter('page');
    		$this->postDataArray = $_SESSION['pfm']['reportData'];
    		}
    		$report_details_list = BankUserTable::getCustomBankUsers($this->postDataArray);


    		$this->pager = new sfDoctrinePager('BankUser', sfConfig::get('app_records_per_page'));
    			$this->pager->setQuery($report_details_list);
    			$this->pager->setPage($this->page);
    			$this->pager->init();

    }
    
    /**
     * function will display List of Merchants Per Banks on Platform
     *
     * @access public
     * @param $request Form Post values
     * @return Doesn't return any thing
     * @createdBy Deepak Bhardwaj
     */
    public function executeListMerchantPerBankReport(sfWebRequest $request) {
    	$this->report_label = 'MerchantPerBankReport';
    	$this->form = new MerchantPerBankReportForm('',array('report_label'=>$this->report_label));
    	if($request->isMethod('post')){
    		$this->form->bind($request->getPostParameters());
    		if($this->form->isValid()) {
    			$this->forward($this->moduleName,'search');
    		}
    	}
    }
    
    public function executeListMerchantPerBankReportSearch(sfWebRequest $request) {
    	$merchantId = '';
    	$merchantServiceId ='';
    	if($request->isMethod('post')){
    		unset($_SESSION['pfm']['reportData']);
    		$_SESSION['pfm']['reportData'] = $request->getPostParameters();
    		$this->postDataArray = $_SESSION['pfm']['reportData'];
    	}
    	else {
    	
    		$this->postDataArray = $_SESSION['pfm']['reportData'];
    	}
    	if(isset($this->postDataArray['merchant'])) {
    		$merchantId = $this->postDataArray['merchant'];
    	}
    	if(isset($this->postDataArray['service_type'])) {
    		$merchantServiceId = $this->postDataArray['service_type'];
    	}
    	$this->page = 1;
    	if ($request->hasParameter('page')) {
    		$this->page = $request->getParameter('page');
    	}

            $this->records = $this->getMerchantPerBankReportDetailsArray($merchantId,$merchantServiceId);
            $this->pager = new sfArrayPager(null, 10);
            $this->pager->setResultArray($this->records);
            $this->pager->setPage($this->page);
            
            $this->pager->init();
     
    }
    
    public function executeListMerchantPerBankReportCsv(sfWebRequest $request) {
    	ini_set('memory_limit', '1024M');
    	ini_set("max_execution_time", "64000");
    	$this->postDataArray = $_SESSION['pfm']['reportData'];
    	
    	$merchantServiceId = "";
    	$merchantId = "";
    	$merchantId = $this->postDataArray['merchant'];
    	$merchantServiceId = $this->postDataArray['service_type'];
    	$records = $this->getMerchantPerBankReportDetailsArray($merchantId,$merchantServiceId);
    	//print_r($records);die;
    	// Creating Headers
    	$headerNames = array(0 => array('S.No.','Merchant Name', 'Merchant Service', 'Collection Bank','Collection Account', 'Merchant Bank', 'Merchant Account'));
    	$headers = array('merchantName', 'merchantService', 'collectionBankName', 'collectionBankAccountNumber', 'merchantBankName', 'merchantBankAccountNumber');
    	for($i = 0 ; $i < count($records); $i++) {
    		for ($index = 0; $index < count($headers); $index++) {
    			if(isset($records[$i][$headers[$index]]))
    				$recordsDetails[$i][$headers[$index]] = $records[$i][$headers[$index]];
    			else
    				$recordsDetails[$i][$headers[$index]] = "";
    		}
    		//$i++;
    	}
    	if (count($recordsDetails) > 0) {
    		$arrList = $this->getCustomXLSExportList($headerNames, $headers, $recordsDetails);
    		$file_array = $this->saveExportListFile($arrList, "/report/listMerchantPerBankReport/", "MerchantPerBankReport");
    		$file_array = explode('#$', $file_array);
    		$this->fileName = $file_array[0];
    		$this->filePath = $file_array[1];
    		$this->folder = "MERCHANT_BANK_REPORT";
    	} else {
    		$this->redirect_url('index.php/report/listMerchantPerBankReport');
    	}$this->setTemplate('csv');
    	// $this->setTemplate('bankReportCsv');
    	$this->setLayout(false);
    }
    
  public function getMerchantPerBankReportDetailsArray($merchantId,$merchantServiceId) {
  	$collectionAccount = Doctrine::getTable('Merchant')->getAllMerchantCollectionAccount($merchantId,$merchantServiceId);
  	$merchantAccount = Doctrine::getTable('Merchant')->getAllMerchantAccount($merchantId,$merchantServiceId);
  	$this->records = Array();
  	
  	foreach ($collectionAccount as $merchantName => $merchantValue) {
  		$i = 0;
  		foreach ($merchantValue['ServiceBankConfiguration'] as $serviceBank => $collBankAccountNumber) {
  			$this->records[$merchantValue['id']][$i]['merchantName'] = $merchantValue['name'];
  			$this->records[$merchantValue['id']][$i]['collectionBankName'] = $collBankAccountNumber['Bank']['bank_name'];
  			$this->records[$merchantValue['id']][$i]['collectionBankAccountNumber'] = $collBankAccountNumber['EpMasterAccount']['account_number'];
  	
  			$i++;
  		}
  	}
  	
  	foreach ($merchantAccount as $merchantName => $merchantValue) {
  		$i = 0;
  		foreach ($merchantValue['MerchantService'] as $service => $serviceName) {
  			foreach ($serviceName['SplitEntityConfiguration'] as $merchantBank => $merchantBankAccountNumber) {
  				$this->records[$merchantValue['id']][$i]['merchantName'] = $merchantValue['name'];
  				$this->records[$merchantValue['id']][$i]['merchantService'] = $serviceName['name'];
  				$this->records[$merchantValue['id']][$i]['merchantBankName'] = $merchantBankAccountNumber['SplitAccountConfiguration']['bank_name'];
  				$this->records[$merchantValue['id']][$i]['merchantBankAccountNumber'] = $merchantBankAccountNumber['SplitAccountConfiguration']['account_number'];
  				$i++;
  			}
  		}
  	}
  	return call_user_func_array('array_merge', $this->records);
  }
   
  //merchantReport
    public function executeAvcReport(sfWebRequest $request) {
        $this->userGroup = "";
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        $bank_details = $pfmHelperObj->getUserAssociatedBank();
        if (count($bank_details) > 0) {
            $bank_details_id = $bank_details['id'];
        } else {
            $bank_details_id = 0;
        }
        $this->form = new AvcPassportChargesForm();
        $this->form->setDefault('report_label', $request->getParameter('label'));
    }

    public function executeAvcReportSearch(sfWebRequest $request) {
        
        $arr = $request->getParameterHolder()->get("avc_report");
        $from_date =  $arr['from_date'];
        $to_date =  $arr['to_date'];
        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $request->getPostParameters();
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        } else {
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }
        
        $this->bank_report_list = Doctrine::getTable("AvcPassportCharges")->getAVCReportDetails($from_date, $to_date);
    }
    
    public function executeAvcReportCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $grandTotalTransactions = 0;
        $grandTotalAmount = 0;
        $postDataArray = $_SESSION['pfm']['reportData'];
        $from_date = $postDataArray['avc_report']['from_date'];
        $to_date = $postDataArray['avc_report']['to_date'];
        $records = Doctrine::getTable("AvcPassportCharges")->getAVCReportDetails($from_date, $to_date);

        // Creating Headers
        $headerNames = array(0 => array('S.No.', 'Bank', 'No. of Transaction', 'Amount'));
        $headers = array('bank_name', 'cnt', 'avc_amount');

        $i = 0;
        $j = 0;

        for($j=0; $j<count($records); $j++){
            for ($index = 0; $index < count($headers); $index++) {
                $recordsDetails[$i][$headers[$index]] = $records[$j][$headers[$index]];
            }
            $grandTotalAmount = $grandTotalAmount + $records[$j]['avc_amount'];
            $grandTotalTransactions = $grandTotalTransactions + $records[$j]['cnt'];
            $i++;
        }
        
        $recordsDetails[$i]['Grand Total'] = $grandTotalAmount;
        $recordsDetails[$i]['No of transactions'] = $grandTotalTransactions;
        if (count($recordsDetails) > 0) {
            $this->postDataArray = array("report_label" => "avc_report");
            $arrList = $this->getXLSExportList($headerNames, $headers, $recordsDetails);
            $file_array = $this->saveExportListFile($arrList, "/report/avcReport/", "AVCReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "AVC_REPORT_FOLDER";
        } else {
            $this->redirect_url('index.php/report/avcReport');
        }
        $this->setTemplate('csv');
        $this->setLayout(false);
    }
    
}
