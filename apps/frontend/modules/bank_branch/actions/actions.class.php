<?php

/**
 * bank_branch actions.
 *
 * @package    mysfp
 * @subpackage bank_branch
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class bank_branchActions extends sfActions {
    public function executeSearch(sfWebRequest $request) {

        $this->bank_id="";
        $this->branch_search="";
        $this->page=1;

        if($request->getParameter('bank_id')) {
            $this->bank_id=$request->getParameter('bank_id');
        }

        $this->formValid ="";
        $this->form = new BankBranchListForm(array(),array('id'=>$this->bank_id));
        if($request->isMethod('post')){

            $this->form->bind($request->getParameter('bankBranch'));
            if ($this->form->isValid())
            {

                $this->formValid = "valid";
            }

        }
    }

    public function executeIndex(sfWebRequest $request) {
       
        $this->form = new BankBranchListForm();
        $this->form->bind($request->getParameter('bankBranch'));
        $usePager =  $request->getParameter('page');
        if ($this->form->isValid() || (isset($usePager) ))
        {
            $postDataArray = $request->getParameterHolder()->getAll();
            if(isset($postDataArray['bankBranch']))
            $postDataArray = $postDataArray['bankBranch'];
            $userCoountObj = new UserAccountManager();
            if(!$userCoountObj->IsSuperAdmin())
             {
                $this->country_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
                $this->bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();;
                $this->branch_search = $postDataArray['branch_search'];
             }
             else
             {
                $this->country_id = '';
                $this->bank_id = '';
                $this->branch_search = $postDataArray['branch_search'];
             }
             
            
            $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BASE);
            $this->bank_branch_list = $bankBranch_obj->getAllRecords($this->bank_id, $this->branch_search,$this->country_id);



            $this->page = 1;
            if($request->getGetParameter('page')) {
                $this->page = $request->getGetParameter('page');
            }
            else {
                $this->page = $request->getPostParameter('page');
            }

            $this->pager = new sfDoctrinePager('BankBranch',sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($this->bank_branch_list);
            $this->pager->setPage($this->page);
            $this->pager->init();
        }else{
            echo $this->form->renderGlobalErrors();
            die;
        }

         if(($request->getParameter('action_user') == "delete")) {
            $this->delete($request);
        }
        if(($request->getParameter('action_user') == "set_status")) {
            $this->setStatus($request);
        }
    }

    public function executeShow(sfWebRequest $request) {
        $this->setLayout(false);
        $request->getParameter('id');
        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BASE);
        $bank_branch = $bankBranch_obj->getRecordDetails($request->getParameter('id'));
        $this->bank_branch = $bank_branch->getFirst();
        $lga_id = $this->bank_branch->getLgaId();
        $this->lgaDetail = "";
        if($lga_id){          
            $this->lgaDetail = Doctrine::getTable('Lga')->find($lga_id);
        }        

        $this->forward404Unless($this->bank_branch);
    }

    public function executeNew(sfWebRequest $request) {
        //get group of logged in user
        //     $user_id = $this->getUser()->getGuardUser()->getId();
        //     $user_group_obj = Doctrine::getTable('sfGuardUserGroup')->findByUserId($user_id);
        //     $group_id = $user_group_obj->getFirst()->getGroupId();
        //     $group_object = Doctrine::getTable('sfGuardGroup')->findById($group_id);
        //     $group_name = $group_object->getFirst()->getName();
        //     if($group_name==sfConfig::get('app_pfm_role_bank_admin')){
        //        $bank_id = $this->user_details['0']['bank_id'];
        //        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        //        $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id);
        //      }
        //      else{
        //
        //      }

            $userCoountObj = new UserAccountManager();
            if(!$userCoountObj->IsSuperAdmin())
             {
                $this->country_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        
             }
             else
             {
                $this->country_id = '';
                
             }
        

       // $this->country_id="";//initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
        $this->state_id=""; //same as the reason above
        $this->lga_id=""; //same as the reason above
        $this->form = new BankBranchForm(array(),array('country_id'=>$this->country_id));
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));

        $params=$request->getPostParameters('bank_branch');
        // print_r($params);
        $this->country_id=$params['bank_branch']['country_id'];
        if($this->country_id == 154) {
            $this->state_id=$params['bank_branch']['state_id'];
            $this->lga_id=$params['bank_branch']['lga_id'];
        }
        else {
            $this->state_id="";
            $this->lga_id="";
        }

        $this->bank_id = $params['bank_branch']['bank_id'];
        //    $this->branch_search = $params['branch_search'];
        //    $this->page = $params['page'];
        $userCountObj = new UserAccountManager();
    /*    if(!$userCountObj->IsSuperAdmin())
        {
            $this->country_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();

        }
        else
        {
            $this->country_id = '';

        }*/
       
        $this->form = new BankBranchForm(array(),array('country_id'=>$this->country_id));

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $postDataArray = $request->getParameterHolder()->getAll();
        if(isset($postDataArray['bankBranch']))
        $postDataArray = $postDataArray['bankBranch'];

        $this->forward404Unless($bank_branch = Doctrine::getTable('BankBranch')->find($request->getParameter('id')), sprintf('Object bank_branch does not exist (%s).', $request->getParameter('id')));

        $bank_branch_details = Doctrine::getTable('BankBranch')->find($request->getParameter('id'));
        $this->country_id=$bank_branch_details->getCountryId();
        if($this->country_id == 154) {

            $this->lga_id = $bank_branch_details->getLgaId();

            $lga_details = Doctrine::getTable('Lga')->find($this->lga_id);
            $this->state_id = $lga_details->getStateId();
        }
        else {
            $this->state_id="";
            $this->lga_id="";
        }


        // $state_details = Doctrine::getTable('State')->find($this->state_id);
        // $this->country_id = $state_details->getCountryId();
        $this->bank_id = $bank_branch_details->getBankId();
        $this->branch_search = $postDataArray['branch_search'];
        $this->page = $request->getParameter('page');
        $this->form = new BankBranchForm($bank_branch,array('action'=>'edit','bankId'=>$this->bank_id,'country_id'=>$this->country_id));

    }

    public function executeUpdate(sfWebRequest $request) {
        //print_r($_POST);
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($bank_branch = Doctrine::getTable('BankBranch')->find($request->getParameter('id')), sprintf('Object bank_branch does not exist (%s).', $request->getParameter('id')));

        $params=$request->getPostParameters('bank_branch');     
       
        $this->form = new BankBranchForm($bank_branch,array('action'=>'edit','bankId'=>$params['bank_branch']['bank_id'],'country_id'=>$params['bank_branch']['country_id']));

        //echo "<pre>"; print_r($params); die;

        $this->bank_id = $params['bank_branch']['bank_id'];
        $this->branch_search = $params['branch_search'];
        $this->page = $params['page'];
        $this->country_id=$params['bank_branch']['country_id'];
        if($this->country_id == 154) {
            $this->state_id=$params['bank_branch']['state_id'];
            $this->lga_id=$params['bank_branch']['lga_id'];
        }
        else {
            $this->state_id="";
            $this->lga_id="";
        }


        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function delete(sfWebRequest $request) {
        $this->forward404Unless($bank_branch = Doctrine::getTable('BankBranch')->find($request->getParameter('id')), sprintf('Object bank_branch does not exist (%s).', $request->getParameter('id')));
        $bank_branch->delete();
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHINFO,$bank_branch->getName(),$bank_branch->getId()));
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_BRANCH_DELETE,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_DELETE, array('branchname' => $bank_branch->getName())),
            $applicationArr);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        $classObj = new sessionAuditManager();
        $classObj->doLogoutForcefully($bank_branch->getBankId(),$bank_branch->getId());
        $this->getUser()->setFlash('notice', sprintf('Bank Branch deleted successfully'),FALSE);
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $bank_branch = $form->save();
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHINFO,$bank_branch->getName(),$bank_branch->getId()));
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
            if(($request->getParameter('action')) == "update") {
                $eventHolder = new pay4meAuditEventHolder(
                    EpAuditEvent::$CATEGORY_TRANSACTION,
                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_BRANCH_UPDATE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_UPDATE, array('branchname' => $bank_branch->getName())),
                    $applicationArr);
                $this->getUser()->setFlash('notice', sprintf(__('Bank Branch details updated successfully')),TRUE);
            }
            else if(($request->getParameter('action')) == "create") {

                $eventHolder = new pay4meAuditEventHolder(
                    EpAuditEvent::$CATEGORY_TRANSACTION,
                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_BRANCH_CREATE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_CREATE, array('branchname' => $bank_branch->getName())),
                    $applicationArr);

                $this->getUser()->setFlash('notice', sprintf(__('Bank Branch added successfully')),TRUE);

            }
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            $this->redirect('bank_branch/search?bank_id='.$bank_branch->getBankId());
//            $this->forward('bank_branch','search');
        }
    }

    public function executeGetStates(sfWebRequest $request) {
        $this->setTemplate(FALSE);
        $state_obj = stateServiceFactory::getService(stateServiceFactory::$TYPE_STATE);
        $str = $state_obj->getStateList($request->getParameter('country_id'),$request->getParameter('state_id'));
        return $this->renderText($str);
    }

    public function executeGetLgas(sfWebRequest $request) {
        $this->setTemplate(FALSE);
        $lga_obj = lgaServiceFactory::getService(lgaServiceFactory::$TYPE_LGA);
        $str = $lga_obj->getLgaList($request->getParameter('state_id'),$request->getParameter('lga_id'));
        return $this->renderText($str);
    }

    public function setStatus(sfWebRequest $request) {
        $bankBranchId = $request->getParameter('id');
        if($request->getParameter('status')!="") {
            $bank_branch = Doctrine::getTable('BankBranch')->find(array($request->getParameter('id')));
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHINFO,$bank_branch->getName(),$bank_branch->getId()));
            if($request->getParameter('status') == "activate") {
                $bank_branch->setStatus("1");
                $status_msg = "Activated";

                $eventHolder = new pay4meAuditEventHolder(
                    EpAuditEvent::$CATEGORY_TRANSACTION,
                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_BRANCH_ACTIVATE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_ACTIVATE, array('branchname' => $bank_branch->getName())),
                    $applicationArr);

            }
            if($request->getParameter('status') == "deactivate") {
                $bank_branch->setStatus("0");
                $status_msg = "Deactivated";

                $eventHolder = new pay4meAuditEventHolder(
                    EpAuditEvent::$CATEGORY_TRANSACTION,
                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_BRANCH_DEACTIVATE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_DEACTIVATE, array('branchname' => $bank_branch->getName())),
                    $applicationArr);
                $classObj = new sessionAuditManager();
                $classObj->doLogoutForcefully($bank_branch->getBankId(),$bank_branch->getId());

            }
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            $bank_branch->save();
            $this->getUser()->setFlash('notice', sprintf('Bank Branch '.$status_msg.' successfully'),FALSE);
            //  $this->redirect('bank_branch/index');
        }
    }
     public function executeGetBankCountry(sfWebRequest $request) {
     
        $group_name = $this->getUser()->getGroupName();
      //create an object of the class
        $classObj = new auditTrailManager();
        //get the bank id
        $bankId = $request->getParameter('bank');
        $country = $request->getParameter('country');
        //based on the id get the country list

        $countryId = "";
        if($group_name == sfConfig::get('app_pfm_role_bank_country_head')) {
          $countryId = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        }
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $str = '<option value="">'.__("Please Select Country").'</option>';
        if($bankId != "") {
          $countryList = $classObj->getCountryList($bankId, $countryId);          
          $selected = "";
          if($countryList){
          foreach($countryList as $key=>$val){
              if($country!="")
              {
                   if ($key == $country )
                    {
                        $selected = "selected='selected'";
                    }
                    else {
                           $selected = "";
                    }

                    }

              if($key)
              $str .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
          }
        }
        }
        return $this->renderText($str);
      }
}
