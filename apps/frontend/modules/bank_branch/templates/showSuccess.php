<table border="1" cellpadding="3" cellspacing="3" width="75%" align="center" style="border-color:#bbbbbb; border-collapse:collapse;">
  <tbody>
      <tr><td colspan="2" align="center"><font size="3"><b>Bank Branch Details</b></font></td></tr>
      <tr>
      <td width="50%"><font size="2"><b>Bank Branch:</b></font></td>
      <td><font size="2"><?php echo $bank_branch->getName() ?></font></td>
    </tr>   
    <tr>
      <td><font size="2"><b>Bank:</b></font></td>
      <td><font size="2"><?php echo $bank_branch->getBankName() ?></font></td>
    </tr>
    <tr>
      <td><font size="2"><b>Branch Code:</b></font></td>
      <td><font size="2"><?php echo $bank_branch->getBranchCode() ?></font></td>
    </tr>
    <tr>
      <td><font size="2"><b>Branch Sortcode:</b></font></td>
      <td><font size="2"><?php echo $bank_branch->getBranchSortcode() ?></font></td>
    </tr>
    <tr>
      <td><font size="2"><b>Address:</b></font></td>
      <td><font size="2"><?php echo nl2br($bank_branch->getAddress()) ?></font></td>
    </tr>
    <tr>
      <td><font size="2"><b>Country:</b></font></td>
      <td><font size="2"><?php echo $bank_branch->getCountryName() ?></font></td>
    </tr>
    <tr>
      <td><font size="2"><b>State:</b></font></td>
      <td><font size="2"><?php echo $sname = ($bank_branch->getCountryCode() == 'NG')? $lgaDetail->getState()->getName() :$bank_branch->getStateName() ?></font></td>
    </tr>
    <tr>
      <td><font size="2"><b>Lga:</b></font></td>
      <td><font size="2"><?php echo $lname = ($bank_branch->getCountryCode() == 'NG')? $lgaDetail->getName():$bank_branch->getLgaName() ?></font></td>
    </tr>
  </tbody>
</table>

