<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

 <script language="Javascript">

$(document).ready(function()
{
    var country_id = "<?php echo $country_id;?>";
    var divId = "bank_branch_country_id";
    var postUrl = "<?php echo url_for("bank_branch/getBankCountry");?>";
    $("#bank_branch_bank_id").change(function()
        {
            var bankId = $('#bank_branch_bank_id').val();
            fetchBankCountry(bankId,divId,postUrl,country_id);
        });
    if($('#bank_branch_bank_id').val()){
        var bankId = $('#bank_branch_bank_id').val();
        fetchBankCountry(bankId,divId,postUrl,country_id);
    }
    if(country_id != "")
        {
          if(country_id == 154){
            document.getElementById('bank_branch_state_id').disabled=false;
            document.getElementById('bank_branch_lga_id').disabled=false;
            document.getElementById('bank_branch_state_name').disabled=true;
            document.getElementById('bank_branch_lga_name').disabled=true;
            document.getElementById('bank_branch_state_name').value="";
            document.getElementById('bank_branch_lga_name').value="";
            var state_id = "<?php echo $state_id;?>";
            //var country_id = document.getElementById("bank_branch_country_id").value;
            var module = "bank_branch";
            displayStates(country_id,module,state_id,'<?php echo url_for('bank_branch/getStates'); ?>');
           }
           else{
             document.getElementById('bank_branch_state_id').disabled=true;
            document.getElementById('bank_branch_lga_id').disabled=true;
            document.getElementById('bank_branch_state_name').disabled=false;
            document.getElementById('bank_branch_lga_name').disabled=false;
            document.getElementById('bank_branch_lga_id').value="";
            document.getElementById('bank_branch_state_id').value="";
             
           }

        }
   $('#bank_branch_country_id').change(function(){
      var country_id = this.value;
      if(country_id == 154){
        document.getElementById('bank_branch_state_id').disabled=false;
        document.getElementById('bank_branch_lga_id').disabled=false;
        document.getElementById('bank_branch_state_name').disabled=true;
        document.getElementById('bank_branch_lga_name').disabled=true;
        document.getElementById('bank_branch_state_name').value="";
        document.getElementById('bank_branch_lga_name').value="";
        var module = "bank_branch";
        displayStates(country_id,module,"",'<?php echo url_for('bank_branch/getStates'); ?>');
        return false;
      }
      else{
        
        document.getElementById('bank_branch_state_id').disabled=true;
        document.getElementById('bank_branch_lga_id').disabled=true;
        document.getElementById('bank_branch_state_name').disabled=false;
        document.getElementById('bank_branch_lga_name').disabled=false;
        document.getElementById('bank_branch_lga_id').value="";
        document.getElementById('bank_branch_state_id').value="";
      }
  }
  );


  // alert(document.getElementById("employee_bank_branch_id").value);
    var state_id = "<?php echo $state_id;?>";
     var lga_id = "<?php echo $lga_id;?>";

    if(state_id!="")//in case of edit, bank needs to be selected, in case of new, it will be blank
    {
    
        document.getElementById("bank_branch_state_id").value = state_id;
        displayLgas(state_id,module,lga_id,'<?php echo url_for('bank_branch/getLgas'); ?>');
        
    }
//    if(document.getElementById("bank_branch_state_id").value != "")
//        {
//
//            var lga_id = "<?php //echo $lga_id;?>";
//            var state_id = document.getElementById("bank_branch_state_id").value;
//            var module = "bank_branch";
//            displayLgas(state_id,module,lga_id,'<?php //echo url_for('bank_branch/getLgas'); ?>');
//
//        }
   $('#bank_branch_state_id').change(function(){
      var state_id = this.value;
      var module = "bank_branch";
      displayLgas(state_id,module,"",'<?php echo url_for('bank_branch/getLgas'); ?>');
      return false;
  }
  );

  var lga_id = "<?php echo $lga_id;?>";
  
    if(lga_id!="")//in case of edit, bank needs to be selected, in case of new, it will be blank
    {
        document.getElementById("bank_branch_lga_id").value = lga_id;

    }



});
</script>
<div class="wrapForm2">

<form action="<?php echo url_for('bank_branch/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  id="pfm_bank_branch_form" name="pfm_bank_branch_form">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<input type="hidden" name="bank_id" value="<?php echo $bank_id;?>" />
<input type="hidden" name="branch_search" value="<?php echo $branch_search;?>" />
<input type="hidden" name="page" value="<?php echo $page;?>" />
<?php endif; ?>
  
  
        <?php echo $form ?>
        <div class="divBlock">
            <center>
 &nbsp;<?php  echo button_to(__('Cancel'),'',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('bank_branch/search/?bank_id='.$bank_id).'\''));?>
          <input type="submit" value="<?php echo __('Save')?>" class="formSubmit" />
            </center>
        </div>

</form>
<div class="clear"></div>
</div>
</div>