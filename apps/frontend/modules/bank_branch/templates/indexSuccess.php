<?php //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead(__('Bank Branch List')); ?>



<div id="flash_notice" class="error_list" style="display:none;">
</div>


<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
            <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
            <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
        </th>
      </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th><?php echo __('Bank Branch')?></th>
      <th><?php echo __('Bank Name')?></th>
      <th><?php echo __('Branch Code')?></th>
      <th><?php echo __('Branch SortCode')?></th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_records_per_page');
      $i = max(($page-1),0)*$limit ;
      foreach ($pager->getResults() as $result):
        $i++;
        ?>
    <tr class="alternateBgColour">
      <td align="center"><?php echo $i ?></td>
      <td align="center"><?php echo link_to($result->getName(), url_for('bank_branch/show?id='.$result->getId()), array('popup' => array('popupWindow', 'width=600,height=400,left=320,top=0')))?></td>
      <td align="center"><?php echo $result->getBankName() ?></td>
      <td align="center"><?php echo $result->getBranchCode() ?></td>
      <td align="center"><?php echo $result->getBranchSortcode() ?></td>
      <td align="center"><?php //echo link_to(' ', 'bank_branch/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>


        <a href="#" onClick="edit_bank_branch('<?php echo $result->getId();?>')" class="editInfo" title="<?php echo __('Edit')?>" ></a>

            <?php if($result->getStatus() == 1)://bank branch is active; enabled
              ?>
        <a href="javascript:;" onClick="set_status_bank_branch('<?php echo $result->getId();?>','deactivate')" class="deactiveInfo" title="<?php echo __('Deactivate')?>" ></a>
            <?php endIf;

            if($result->getStatus() == 0): //bank branch is inactive; disabled
              ?>
        <a href="javascript:;" onClick="set_status_bank_branch('<?php echo $result->getId();?>','activate')" class="activeInfo" title="<?php echo __('Activate')?>" ></a>

            <?php endIf;?>
        <!--<a href="javascript:;" onClick="delete_bank_branch('<?php //echo $result->getId();?>')" class="delInfo" title="Delete" ></a>-->

      </td>
    </tr>
      <?php endforeach; ?>

    <tr><td colspan="6">
        <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'search_results')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

        </div></td></tr>



    <?php }
    else { ?>
    <tr><td  align='center' class='error' colspan="6"><?php echo __('No Bank Branch found')?></td></tr>
    <?php } ?>

  </tbody>
  <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>
</div>
<?php  echo button_to(__('Add New'),'',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('bank_branch/new').'\''));?>

<script>
  var edit_url = "<?php echo url_for('bank_branch/edit');?>";
  var activate_url = "<?php echo url_for('bank_branch/index');?>";
  function delete_bank_branch(id){
    var answer = confirm("Are you sure?");
    if(answer){
      var page = "<?php echo $page;?>";
      $("#action_user").val("delete");
      $("#id").val(id);
      $("#page").val(page);
      fetch_bank_branches(activate_url);
    }
    else{
      return false;
    }
  }

  function set_status_bank_branch(id,status){
   var answer = confirm("Are you sure you want to "+status+"?")
   if (answer){
   var url = '<?php echo  url_for($sf_context->getModuleName().'/index')?>';
    var page = "<?php echo $page;?>";
    $("#action_user").val("set_status");
    $("#id").val(id);
    $("#page").val(page);
    $("#status").val(status);
    $.post(url,$("#search").serialize(), function(data){
                if(data == "logout"){
                    location.reload();
                }else{
                   
                    $("#search_results").html(data);
                     $("#flash_notice").show();
                    $("#flash_notice").html('<span>Bank Branch '+status+'d successfully</span>');
                }
            });
    }
  }

  function edit_bank_branch(edit_id){
    var page = "<?php echo $page;?>";
    $("#page").val(page);
    $("#id").val(edit_id);
    document.forms[0].action = edit_url;
    document.forms[0].submit();

  }
</script>
