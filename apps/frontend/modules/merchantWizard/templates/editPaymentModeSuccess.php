<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/editPaymentMode', ' class="multiForm" onsubmit="return validateTransferForm()" id="payment_mode_frm" name="payment_mode_frm  "'); ?>
    <!-- change for bug 28952 remove full stop as typo error -->
    <?php echo ePortal_legend('Choose a Payment Mode Option For your Merchant-Service'); ?>
    <dl id="divcurrency">
        <div class="dsTitle4Fields">
            <label for="currency">Currency<sup>*</sup></label>
        </div>
        <div class="dsInfo4Fields">
            <select id="currency" name="currency" onchange="getPaymentMode()">
                <?php
                if (!empty($selectedCurr)) {
                    foreach ($selectedCurr as $k => $value) {
                ?>
                        <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
                <?php }
                } ?>
            </select>
            <br>
            <br>
            <div id="err_currency" class="cRed"></div>
        </div>
    </dl>

    <dl id="divpayment_mode">
        <div class="dsTitle4Fields"><label for="payment_mode_option">Payment Mode Option<sup>*</sup></label></div>
        <div class="dsInfo4Fields">
            <select id="payment_mode" multiple="multiple" name="payment_mode[]" onchange="return checkInterswitchSelected()">
                <?php
                $show_inter_cat = false;
                $paymentid = pfmHelper::getPMOIdByConfName('interswitch');
                if (in_array($paymentid, $paymentModeArray)) {
                 $show_inter_cat = true;
                }
                if (!empty($payment_mode_options)) {
                    foreach ($payment_mode_options as $key => $payment_mode) {
                ?>
                        <option value="<?php echo $key; ?>"  <?php if (in_array($key, $paymentModeArray)) {
                ?> selected="selected" <?php } ?>>
                    <?php echo $payment_mode; ?></option>
                <?php }
                } ?>
            </select>
            <br>
            <br>
            <div id="err_payment_mode" class="cRed"></div>
        </div>
    </dl>
    <?php
    ?>
                <dl id="divinterswitch_category" <?php if (!$show_inter_cat) { ?>style="display:none""<?php } ?> >
                    <div class="dsTitle4Fields"><label for="category">Interswitch Category<sup>*</sup></label></div>
                    <div class="dsInfo4Fields">
                        <select id="interswitch_category"  name="interswitch_category[]" >
                <?php
                if (!empty($interswitch_category_options)) {
                    foreach ($interswitch_category_options as $key => $category) {
                ?>
                        <option value="<?php echo $key; ?>"  <?php if (($key == $selectedCategory)) {
                ?>
                                    selected="selected"  <?php } ?>>
                    <?php echo $category; ?></option>
                <?php }
                } ?>
            </select>
            <br>
            <br>
            <div id="err_interswitch_category" class="cRed"></div>
        </div>
    </dl>

    <?php //echo formRowComplete('Currency<sup>*</sup>',select_tag('currency', options_for_select($selectedCurr),array('onchange'=>'getPaymentMode()')),'','currency','err_currency','divcurrency');   ?>
    <?php //echo formRowComplete('Payment Mode Option<sup>*</sup>',select_tag('payment_mode', options_for_select($payment_mode_options,$paymentModeArray),array('multiple' => true)),'','payment_mode_option','err_payment_mode','divpayment_mode');  ?>


                <div class="divBlock">
                    <center id="multiFormNav">
                        <div class="passPolicy">
                            <center>
                                <b>  Click on continue button to proceed next step.</b>
                            </center>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $MerchantServiceId ?>">
            <?php echo button_to('Continue', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for($sf_context->getModuleName() . '/editPaymentMode?task=getNext') . '\'')); ?>
                <input type="submit" value="Save" name="save" id="save"  class="formSubmit" />


            </center>
        </div>
        <div class="note" <?php if (!$show_inter_cat) { ?>style="display:none"<?php } ?>>
            <b style="color:red" > Note : Change in interswitch  category will be reflected in all the services of the merchant</b>
        </div>

    </form>
    </div>
    <script>
        function validateTransferForm()
        {


            var foundIS=true;
            $('#payment_mode :selected').each(function(i, selected){
                if(($(selected).text().search("Interswitch")>-1) && ($('#interswitch_category').val() == '') )
                {
                    $('#err_interswitch_category').html('Please Select Interswitch Category ');
                    foundIS=false;
                }
            });
            return foundIS;

        }

        function getPaymentMode()
        {

            var currency =  $('#currency').val();

            var url = "<?php echo url_for("merchantWizard/getUpdatedPaymentMode"); ?>";

        $("#payment_mode").load(url, { currency: currency,byPass:1},function (data){
            if(data=='logout'){
                location.reload();
            }
        });

    }
    function checkInterswitchSelected(){
        //      $('#divinterswitch_category').hide();
        $('.note').hide();
        //     alert(merchant_mapped);
        var foundIS=false;
        $('#payment_mode :selected').each(function(i, selected){
            if($(selected).text().search("Interswitch")>-1 )
            {
                foundIS=true;
                $('#divinterswitch_category').show();
                $('.note').show();
            }
            else
            {
                foundIS=false;
                $('#divinterswitch_category').hide();
            }
        });
        return foundIS;
    }
</script>


