<?php //use_helper('Form') ?>
<?php echo ePortal_pagehead(' '); ?>
<?php echo ePortal_legend('Enter The Validation Rules For Merchant-Service'); ?>
<br>
<div class="wrapForm2">
    <form action="<?php echo url_for('merchantWizard/setvalidrules') ?>" method="post"  class="dlForm multiForm" id="validation_form" name="validation_form">
        <?php
        if ($noOfValidRules > 0) {
            for ($i = 1; $i <= $noOfValidRules; $i++) {
                 echo ePortal_legend('Enter Details For Validation Rule ' . $i); 
                 echo formRowComplete("Parameter Name<sup>*</sup>",'<input type="text" id="validationRules_param_name'.$i.'" 
                     name="validationRules[param_name'.$i.']" class="FieldInput" >', '', '', 'err_param' . $i); 
                 echo formRowComplete("Parameter Description<sup>*</sup>", '<input type="text" id="validationRules_param_desc'.$i.'" 
                     name="validationRules[param_desc'.$i.']" class="FieldInput" >' . "<br><br>", '', '', 'err_desc' . $i); 
                 echo formRowFormatRaw("Is Mandatory?<sup>*</sup>", 
                        '<input id="validationRules_param_mandatory'.$i.'_yes" type="radio" checked="checked" value="yes" name="validationRules[param_mandatory'.$i.']" >'
                        . '&nbsp; Yes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;' 
                        . '<input id="validationRules_param_mandatory'.$i.'_no" type="radio" value="no" name="validationRules[param_mandatory'.$i.']" >'
                        . '&nbsp; No'
                ); 
                echo formRowFormatRaw("Parameter Type<sup>*</sup>", 
                        '<input id="validationRules_param_type'.$i.'_integer" type="radio" checked="checked" value="integer" name="validationRules[param_type'.$i.']" >'
                        .  '&nbsp; Integer &nbsp; &nbsp; &nbsp; &nbsp;' . 
                         '<input id="validationRules_param_type'.$i.'_string" type="radio" value="string" name="validationRules[param_type'.$i.']" >'
                        .'&nbsp; String');
                 ?>
                <?php //echo formRowComplete("Parameter Name<sup>*</sup>", input_tag('validationRules[param_name' . $i . ']', '', array('class' => 'FieldInput')), '', '', 'err_param' . $i); ?>
                <?php //echo formRowComplete("Parameter Description<sup>*</sup>", textarea_tag('validationRules[param_desc' . $i . ']', '', array('class' => 'FieldInput')) . "<br><br><br>", '', '', 'err_desc' . $i); ?>
                <?php //echo formRowFormatRaw("Is Mandatory?<sup>*</sup>", radiobutton_tag('validationRules[param_mandatory' . $i . ']', 'yes', true) . '&nbsp; Yes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;' . radiobutton_tag('validationRules[param_mandatory' . $i . ']', 'no', false) . '&nbsp; No'
                //); ?>
                <?php //echo formRowFormatRaw("Parameter Type<sup>*</sup>", radiobutton_tag('validationRules[param_type' . $i . ']', 'integer', true) . '&nbsp; Integer &nbsp; &nbsp; &nbsp; &nbsp;' . radiobutton_tag('validationRules[param_type' . $i . ']', 'string', false) . '&nbsp; String'
                  //); ?>
                <br><br>

                <?php
            }
        }
        ?>

        <div  class="divBlock">
            <center>
                &nbsp; <?php echo button_to('Cancel', '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('merchantWizard/index') . '\'')); ?>
                <?php //echo input_hidden_tag('validationRules[noOfValidRules]', $noOfValidRules) ?>
                <input type="hidden" name="validationRules[noOfValidRules]" id="validationRules_noOfValidRules" value="<?php echo $noOfValidRules; ?>" >
                <input type="button" value="Save and Continue" class="formSubmit" onclick="validateRules()" />
            </center>
        </div>


    </form>

</div>
<script>
    function validateRules()
    {
        var err  = 0;
        // validationRules_noOfValidRules  validationRules_param_name1
        for(i=1; i<=$('#validationRules_noOfValidRules').val();i++ )
        {
            var paramId = "validationRules_param_name"+i;
            var paramDesc = "validationRules_param_desc"+i ;
            var errorId = "err_param"+i;
            var errorDesc = "err_desc"+i;   
          
            if(!$('#'+paramId).val() )
            {
                $('#'+errorId).html("Please enter parameter");
                err = err+1;
            }
            else
            {
                $('#'+errorId).html("");
            }
            if(!$('#'+paramDesc).val() )
            {
                $('#'+errorDesc).html("Please enter parameter description");
                err = err+1;
            }
            else
            {
                $('#'+errorDesc).html("");
            }
        }    
        if(0==err)
        {
            for(i=1; i<=$('#validationRules_noOfValidRules').val();i++ )
            {
                paramId = "validationRules_param_name"+i;
                paramDescId = "validationRules_param_desc"+i;
                for(j=1; j<=$('#validationRules_noOfValidRules').val();j++ )
                {
                    secParamId = "validationRules_param_name"+j;
                    secParamDescId = "validationRules_param_desc"+j;
                    var errorId = "err_param"+j;
                    var errorDescId = "err_desc"+j;
                    if( $('#'+paramId).val() == $('#'+secParamId).val() && i!=j)
                    {
                        $('#'+errorId).html("Duplicate parameter name");
                        err = err+1;
                    }
                    //Bug id #28826                           
                    if( $('#'+paramDescId).val() == $('#'+secParamDescId).val() && i!=j)
                    {
                        $('#'+errorDescId).html("Duplicate parameter description");
                        err = err+1;
                    }
                }
            }
        }
        if(err == 0)
        {
      
            $('#validation_form').submit();
      
        }
    }


</script>


