<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/updateConsolidationAccSetup', ' class="multiForm" onsubmit="return validateConsolidationForm()" id="consolidation_acc_frm" name="consolidation_acc_frm"'); ?>
    <div id="consolDiv" >
        <div id="div_0">
            <?php
            $count = 1;
            if (!empty($ConsolidationArr)) {
                foreach ($ConsolidationArr as $outerKey => $outerVal) {
                    foreach ($outerVal as $innerKey => $innerVal) {
                        foreach ($innerVal as $key => $val) {
                            ?>
                            <?php
                            echo ePortal_legend('Enter The Receiving (Consolidation) Account Details. for ' . $selectedCurr[$innerKey] . ' and ' . $splitType[$key]);
                            echo formRowComplete('Account Party<sup>*</sup>', '<input type="text" name="accParty' . $count . '" id="accParty' . $count . '" value="' . $val['accParty'] . '" class="FieldInput">', '', 'accParty' . $count . '', 'err_accParty' . $count . '', 'divaccParty' . $count . '');
                            echo formRowComplete('Account Name<sup>*</sup>', '<input type="text" name="accName' . $count . '" id="accName' . $count . '"  value="' . $val['accName'] . '" class="FieldInput">', '', 'accName' . $count . '', 'err_accName' . $count . '', 'divaccName' . $count . '');
                            echo formRowComplete('Account Number<sup>*</sup>', '<input type="text" name="accNo' . $count . '" id="accNo' . $count . '"  value="' . $val['accNumber'] . '" class="FieldInput">', '', 'accNo' . $count . '', 'err_accNo' . $count . '', 'divaccNo' . $count . '');
                            echo formRowComplete('Bank Name<sup>*</sup>', '<input type="text" name="bankName' . $count . '" id="bankName' . $count . '"  value="' . $val['bankName'] . '" class="FieldInput">', '', 'bankName' . $count . '', 'err_bankName' . $count . '', 'divbankName' . $count . '');
                            echo formRowComplete('Sort Code<sup>*</sup>', '<input type="text" name="sortCode' . $count . '" id="sortCode' . $count . '"  value="' . $val['sortcode'] . '" class="FieldInput">', '', 'sortCode' . $count . '', 'err_sortCode' . $count . '', 'divsortCode' . $count . '');
                            echo formRowComplete('Split<sup>*</sup>', '<input type="text" name="split' . $count . '" id="split' . $count . '"  value="' . $val['charge'] . '" class="FieldInput">', '', 'split' . $count . '', 'err_split' . $count . '', 'divsplit' . $count . '');
                            ?>
                            <input type="hidden" name="<?php echo 'splitTypeId' . $count; ?>" id="<?php echo 'splitTypeId' . $count; ?>" value="<?php echo $val['splitTypeId']; ?>" >
                            <input type="hidden" name="<?php echo 'splitEnittyConfigId' . $count; ?>" id="<?php echo 'splitEnittyConfigId' . $count; ?>" value="<?php echo $val['splitEnittyConfigId']; ?>" >
                            <input type="hidden" name="<?php echo 'splitAccConfigId' . $count; ?>" id="<?php echo 'splitAccConfigId' . $count; ?>" value="<?php echo $val['splitAccConfigId']; ?>" >
                            <?php
                            //echo input_hidden_tag('splitTypeId' . $count, $val['splitTypeId']);
                            //echo input_hidden_tag('splitEnittyConfigId' . $count, $val['splitEnittyConfigId']);
                            //echo input_hidden_tag('splitAccConfigId' . $count, $val['splitAccConfigId']);
                            $count++;
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="divBlock">
        <center id="multiFormNav">
            <input type="hidden" name="noOfAc" id="noOfAc" value="<?php echo $count; ?>" >
            <?php echo button_to('Cancel', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for($sf_context->getModuleName() . '/listMerchantWizard') . '\''));  ?>
            <input type="submit" value="Save and Continue" name="save" id="save"  class="formSubmit" >
            <input type="submit" value="Save and Add More Account" name="addmore" id="addmore"  class="formSubmit" >

        </center>
    </div>



</form>

</div> </div>


<script>


    $(document).ready(function(){
        set_consolidation_frm();
    });


    //function used to validate the form
    function validateConsolidationForm(){
        var reg = /^-?\d*\.?\d+([eE]\d*.\d+)?$/;
        var totalPercent = 0;
        var count = $("#noOfAc").val();
        var i;
        var err = 0;
        var run = 0;
        for(i=1;i < count;i++){

      

            if($('#accParty'+i).val() == ''){
                $('#err_accParty'+i).html('Please enter Account Party');
                err++
            }else{
                $('#err_accParty'+i).html('');
            }
            if($('#accName'+i).val() == ''){
                $('#err_accName'+i).html('Please enter Account Name');
                err++
            }else{
                $('#err_accName'+i).html('');
            }

            if($('#accNo'+i).val() == ''){
                $('#err_accNo'+i).html('Please enter Account Number');
                err++;
            }else if(isNaN($('#accNo'+i).val())){
                $('#err_accNo'+i).html('Please enter a valid Account Number');
                err++;
            }else{
                $('#err_accNo'+i).html('');
            }

            if($('#sortCode'+i).val() == ''){
                $('#err_sortCode'+i).html('Please enter Sort Code');
                err++;
            }else{
                $('#err_sortCode'+i).html('');
            }

            if($('#bankName'+i).val() == ''){
                $('#err_bankName'+i).html('Please enter Bank Name');
                err++;
            }else{
                $('#err_bankName'+i).html('');
            }
        

            if($("#splitType").val() == 2){            
                totalPercent = Number(totalPercent) + Number($('#split'+i).val());
            }

            if(totalPercent > 100 && run == 0){
                err++;
                run++;
                alert('You have entered more then 100% for Split. Please Check!');
        
            }
        }

        if(err == 0){
            return true;
        }
        else{
            return false;
        }


    }




    //function used to add dynamic div's
    function addMoreAcc()
    {
        var len = document.getElementById("count").value;
        var len1 = document.getElementById("totalRec").value;

        if($("#splitType").val() == 2){
            var totalPercent = checkPercentEnter(len1);
            if(totalPercent >= 100){
                alert('You have already distributed your 100% Split.');
                return false;
            }
        }

        var value2 = 1;
        var tLoop = eval(len);
        var divid = "div_"+tLoop;
		
        var divTag = document.createElement("div");
        divTag.id = divid;

            
        divTag.innerHTML = '<dl id="'+divid+'"><div class="dsTitle4Fields"><label for="blank"></label></div><div class="dsInfo4Fields"><br/><br/></dl> <dl id="divaccParty"><div class="dsTitle4Fields"><label for="accParty">Account Party<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="accParty'+tLoop+'" class="FieldInput" type="text" value="" name="accParty'+tLoop+'"/><br/><br/><div id="err_accParty'+tLoop+'" class="cRed"/></div></dl>  <dl id="divaccName"><div class="dsTitle4Fields"><label for="accName">Account Name<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="accName'+tLoop+'" class="FieldInput" type="text" value="" name="accName'+tLoop+'"/><br/><br/><div id="err_accName'+tLoop+'" class="cRed"/></div></dl>\n\
    <dl id="divaccNo"><div class="dsTitle4Fields"><label for="accNo">Account Number<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="accNo'+tLoop+'" class="FieldInput" type="text" value="" name="accNo'+tLoop+'"/><br/><br/><div id="err_accNo'+tLoop+'" class="cRed"/></div></dl>   <dl id="divbankName"><div class="dsTitle4Fields"><label for="bankName">Bank Name<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="bankName'+tLoop+'" class="FieldInput" type="text" value="" name="bankName'+tLoop+'" /><br/><br/><div id="err_bankName'+tLoop+'" class="cRed"/></div></dl>   <dl id="divsortCode"><div class="dsTitle4Fields"><label for="sortCode">Sort Code<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="sortCode'+tLoop+'" class="FieldInput" type="text" value="" name="sortCode'+tLoop+'"/><br/><br/><div id="err_sortCode'+tLoop+'" class="cRed"/></div></dl>  <dl id="divsplit"><div class="dsTitle4Fields"><label for="split">Split<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="split'+tLoop+'" class="FieldInput" type="text" value="" name="split'+tLoop+'" maxLength=6/><br/><br/><div id="err_split'+tLoop+'" class="cRed"/></div></dl>   <dl id="remove"><div class="dsTitle4Fields"><label for="remove"></label></div><div class="dsInfo4Fields"><a href="javascript:void(0)" onClick=removeAcc("'+divid+'")>remove</a><br/><br/></dl> '
			

        //document.getElementById('showID').innerHTML = strFile;
        document.getElementById("consolDiv").appendChild(divTag);
        document.getElementById("count").value = eval(len) + eval(value2);
        document.getElementById("totalRec").value = eval(len1) + eval(value2);
		
    }

    //function used to remove the dynamic divs added.
    function removeAcc(val)
    {
        var len = document.getElementById("count").value;
        var value2 = 1;
        document.getElementById("count").value = eval(len) - eval(value2);
        var d1 = document.getElementById("consolDiv");
        var d2 = document.getElementById(val);
        d1.removeChild(d2);
    }


    //function used to chack the percentage enter, wether it is smaller then 100% or not
    function checkPercentEnter(count){
        var i;
        var totalPercent = 0;
        for(i=0;i < count;i++){
            if(document.getElementById('split'+i)){
                totalPercent = Number(totalPercent) + Number($('#split'+i).val());
            }
        }
        return totalPercent;
    }
    //function used to show hide entry form on dropdown change
    function set_consolidation_frm(){
        //clearing values
        //$("#accParty0").val('');
        $("#err_accParty0").html('');
        //$("#accName0").val('');
        $("#err_accName0").html('');
        //$("#accNo0").val('');
        $("#err_accNo0").html('');
        //$("#sortCode0").val('');
        $("#err_sortCode0").html('');
        //$("#split0").val('');
        $("#err_split0").html('');
        $("#err_bankName").html('');
               
    }



</script>
