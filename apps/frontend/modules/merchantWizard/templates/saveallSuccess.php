<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<script>

    $(document).ready(function()
    {

        $('#accountDetailsBut').click(function(){
            //$('#AccountDetail').slideUp('slow')
            //$("#merchantDetail").slideDown('slow');
            $("#AccountDetail").show();
            $("#merchantDetail").hide();
        });
        $('#merchantDetailsBut').click(function(){
            //$('#merchantDetail').slideUp('slow')
            //$("#AccountDetail").slideDown('slow');
            $("#merchantDetail").show();
            $("#AccountDetail").hide();
        });
    });

</script>
<div>&nbsp;</div>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/saveall', ' class="dlForm multiForm" id="save_all_form" name="save_all_form"'); ?>

    <?php //echo button_to('Merchant Details','',array('class'=>'formCancel','onClick'=>'#', 'id'=>'merchantDetailsBut'));?>

    <?php // echo button_to('Account Details','',array('class'=>'formCancel','onClick'=>'#','id'=>'accountDetailsBut'));?>

    <div align="left" name="merchantDetail" id="merchantDetail" style="display:block;">

        <?php
        echo ePortal_legend('Merchant Details');
        ?>

        <?php echo simpleRow("Merchant Name", $merchantDetails[0], 'fullrow'); ?>
        <?php echo simpleRow("Merchant Code", $merchantDetails[1], 'fullrow'); ?>
        <?php echo simpleRow("Merchant Key", $merchantDetails[2], 'fullrow'); ?>
        <?php echo simpleRow("Merchant Auth-Info", $merchantDetails[3], 'fullrow'); ?>



        <?php echo '<br>' . ePortal_legend('Merchant Service Details'); ?>

        <?php echo simpleRow("Merchant Service Name", $merchantServiceDetails['name'], 'fullrow'); ?>
        <?php echo simpleRow("Notification Url", $merchantServiceDetails['notification_url'], 'fullrow'); ?>
        <!-- add url :bug Id 28890 -->
        <?php echo simpleRow("Merchant Home Page Url", $merchantServiceDetails['merchant_home_page'], 'fullrow'); ?>
        <?php echo simpleRow("Version", $merchantServiceDetails['version'], 'fullrow'); ?>


        <?php
        if ($validationRulesDetails) {
            echo '<br>' . ePortal_legend('Validation Rules Details');
            $counterVal = count($validationRulesDetails);
            for ($i = 0; $i < $counterVal; $i++) {
                $CountValidRules = $i + 1;
                echo formRowComplete('<b>Validation Rule ' . $CountValidRules . '</b>', '', '', '', '', '');
        ?>

        <?php echo simpleRow("Param Name", $validationRulesDetails[$i][0], 'fullrow'); ?>
<?php echo simpleRow("Param Description", $validationRulesDetails[$i][1], 'fullrow'); ?>
        <?php echo simpleRow("Is Mandatory?", $validationRulesDetails[$i][2], 'fullrow'); ?>
        <?php echo simpleRow("Param Type", $validationRulesDetails[$i][3], 'fullrow'); ?>

        <?php
            }
        }
        ?>



<?php
        if (is_array($merchantServiceChrgData)) {
            echo '<br>' . ePortal_legend('Merchant Service Charges Details');

            foreach ($merchantServiceChrgData as $merchantSeviceChrgVal) {
                echo formRowComplete('Currency<b> : ' . $currency[$merchantSeviceChrgVal['currency']] . '</b>    Payment mode :<b>' . $paymentMode[$merchantSeviceChrgVal['currency']][$merchantSeviceChrgVal['paymentMode']] . '</b>', '', '', '', '', '');
                if ($merchantSeviceChrgVal['paymentMode'] == 2) {
                    $interswitchcategoryDetails = Doctrine::getTable('InterswitchCategory')->getCategoryName($interswitchCategory['0']);
                    echo simpleRow("Interswitch Category", $interswitchcategoryDetails['0']['name'], 'fullrow');
                } ?>



        <?php echo simpleRow("Merchant Service Charge", $merchantSeviceChrgVal['service_charge_percent'] . '%', 'fullrow'); ?>
<?php echo simpleRow("Upper Slab", $merchantSeviceChrgVal['upper_slab'], 'fullrow'); ?>
<?php echo simpleRow("Lower Slab", $merchantSeviceChrgVal['lower_slab'], 'fullrow'); ?>
<?php
            }
        }
?>



        <?php
        $counterVal = count($transChrgData);

        if ($counterVal > 0 && is_array($transChrgData)) {
            echo '<br>' . ePortal_legend('Transaction Charges Details');

            foreach ($transChrgData as $transChrgVal) { //Bug id  #28838
                echo formRowComplete('Currency<b> : ' . $currency[$transChrgVal['currency']] . '</b>    Payment mode :<b>' . $paymentMode[$transChrgVal['currency']][$transChrgVal['paymentMode']] . '</b>', '', '', '', '', '');
        ?>
        <?php
//Bug Id ##28922 
                if ($transChrgVal['paymentMode'] == 1)
                    echo simpleRow("Financial Institution Charges", $transChrgVal['finInstCharge'], 'fullrow'); ?>
        <?php echo simpleRow("Payforme Charges", $transChrgVal['PayForCharge'], 'fullrow'); ?>

        <?php
            }
        }
        ?>

    </div>

    <div align="left" name="AccountDetail" id="AccountDetail" style="display:none;">
        <?php
        if (count($bankDetails) > 0) {
            echo '<br>' . ePortal_legend('Bank Details');
        ?>
        <?php
            $bankCount = 1;
            foreach ($bankDetails as $bankVal) {
        ?>
<?php echo simpleRow("Bank Name " . $bankCount, $bankVal[1], 'fullrow'); ?>
<?php echo simpleRow("Bank Acronym " . $bankCount, $bankVal[2], 'fullrow'); ?>

        <?php
                $bankCount++;
            }

            echo '<br>' . ePortal_legend('Collection Account Details');
            foreach ($bankDetails as $bankVal) {

            }

            echo '<br>' . ePortal_legend('Recieving  Account Details');
        }

        echo '<br>' . ePortal_legend('Recieving (Consolidation) Account Details');
        ?>

    </div>

        <?php
        echo '<br>' . ePortal_legend('Do you want to save the data?');
        ?>


    <div  class="divBlock">
        <center id="multiFormNav">
        <?php echo button_to('Cancel', '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('merchantWizard/index') . '\'')); ?> &nbsp;
            <input type="hidden" name="save" value="Y">
            <input type="submit" value="Save and Continue"  class="formSubmit" />
        </center>
    </div>


</form>
</div></div>