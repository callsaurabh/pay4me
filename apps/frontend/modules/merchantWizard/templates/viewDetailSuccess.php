<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead('Merchant Service Detail');?>


<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <thead>
            <tr align = "center" class="horizontal">
            <th width = "15%">Merchant Code </th>
            <th width = "30%">Merchant Key</th>
            <th width = "10%" nowrap>Merchant Service Id</th>

        </thead>
        <tbody>
            <?php if($serviceDetailObj) {?>


            <tr align = "center" class="alternateBgColour">

                <td><?php  echo $serviceDetailObj->getFirst()->getMerchant()->getMerchantCode() ;?></td>
                <td><?php echo $serviceDetailObj->getFirst()->getMerchant()->getMerchantKey() ;?></td>

                <td><?php echo $serviceDetailObj->getFirst()->getId() ;?></td>


            </tr>

     <?php
 }else {?>
            <tr><td colspan="3" class="error"><?php echo "No Record Found"; ?></td></tr>
            <?php }?>

        </tbody>

        




    </table>
</div>


