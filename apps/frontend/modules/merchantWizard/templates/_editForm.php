<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<div class="wrapForm2">
<form action="<?php echo url_for('merchantWizard/update') ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="pfm_merchant_service_form" name="pfm_merchant_service_form">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

  <?php echo $form ?>
  <div class="divBlock">
    <center>
   &nbsp; <?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('merchantWizard/listMerchantWizard').'\''));?>
          <input type="submit" value="Save and Continue"  class="formSubmit" />
  </center></div>


</form>
</div>

</div>