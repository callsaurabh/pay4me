<?php //use_helper('Form') ?>
<?php echo ePortal_pagehead(' '); ?>
<?php echo  ePortal_legend('Validation Rules For Merchant-Service');  ?>
<br>
<div class="wrapForm2">

    <form action="<?php echo url_for('merchantWizard/updateValidationRule') ?>" method="post"  class="dlForm multiForm" id="validation_form" name="validation_form">
        <div id='consolDiv'>

            <?php
            $noOfValues = Array();
            if ($noOfValidRules > 0) {
                $validationArray = $merchant_service->toArray();
                for ($i=1;$i<=$noOfValidRules;$i++) {
                    $noOfValues[] = $i ;
                    ?>
            <div id="div_<?php echo $i?>">
                <?php echo  ePortal_legend('Details For Validation Rule '.$i); 
                  echo formRowComplete("Parameter Name<sup>*</sup>",'<input type="text" id="validationRules_param_name'.$i.'" 
                     name="validationRules[param_name'.$i.']" class="FieldInput" value="'.$validationArray[$i-1]["param_name"].'">', '', '', 'err_param' . $i); 
                   echo formRowComplete("Parameter Description<sup>*</sup>", '<input type="text" id="validationRules_param_desc'.$i.'" 
                     name="validationRules[param_desc'.$i.']" class="FieldInput" value="'.$validationArray[$i-1]["param_description"].'" >' . "<br><br>", '', '', 'err_desc' . $i);  
                   ?>
                <dl id="">
                <div class="dsTitle4Fields"><label>Is Mandatory? <sup>*</sup></label></div>
                        <div class="dsInfo4Fields">
                            <input id="<?php echo "validationRules_param_mandatory$i"."_yes"; ?>" type="radio" <?php if($validationArray[$i-1]['is_mandatory']==1) { ?> checked="checked" <?php } ?>  value="yes" name="<?php echo "validationRules[param_mandatory$i]"; ?>">
                            &nbsp; Yes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <input id="<?php echo "validationRules_param_mandatory$i"."_no"; ?>" type="radio" value="no" name="<?php echo "validationRules[param_mandatory$i]"; ?>" <?php if($validationArray[$i-1]['is_mandatory']=='') { ?> checked="checked" <?php } ?> >
                            &nbsp; No
                        </div>
                </dl>
                <dl id="">
                <div class="dsTitle4Fields"><label>Parameter Type <sup>*</sup></label></div>
                        <div class="dsInfo4Fields">
                            <input id="<?php echo "validationRules_param_type$i"."_integer"; ?>" type="radio" 
                                   <?php if($validationArray[$i-1]['param_type']=='integer') { ?> checked="checked" <?php } ?> 
                                    value="integer" name="<?php echo "validationRules[param_type$i]"; ?>">
                            &nbsp; Integer &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <input id="<?php echo "validationRules_param_type$i"."_string"; ?>" type="radio" value="string" name="<?php echo "validationRules[param_type$i]"; ?>" 
                                    <?php if($validationArray[$i-1]['param_type']=='string') { ?> checked="checked" <?php } ?>  >
                            &nbsp; String
                        </div>
                 </dl>
            
                
                
                


                <?php //echo formRowComplete("Parameter Name<sup>*</sup>",input_tag('validationRules[param_name'.$i.']', $validationArray[$i-1]['param_name'],array('class' => 'FieldInput')),'','','err_param'.$i); ?>

                <?php // echo formRowComplete("Parameter Description<sup>*</sup>",textarea_tag('validationRules[param_desc'.$i.']', $validationArray[$i-1]['param_description'],array('class' => 'FieldInput'))."<br><br>",'','','err_desc'.$i); ?>

                <?php //echo formRowFormatRaw("Is Mandatory?<sup>*</sup>",
                      //  radiobutton_tag('validationRules[param_mandatory'.$i.']', 'yes', 
                        //($validationArray[$i-1]['is_mandatory']==1)?true:false).'&nbsp; Yes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;'.radiobutton_tag('validationRules[param_mandatory'.$i.']', 'no', ($validationArray[$i-1]['is_mandatory']=="")?true:false).'&nbsp; No'
                //); 
                ?>

                <?php // echo formRowFormatRaw("Parameter Type<sup>*</sup>",radiobutton_tag('validationRules[param_type'.$i.']', 'integer', ($validationArray[$i-1]['param_type']=='integer')?true:false).'&nbsp; Integer &nbsp; &nbsp; &nbsp; &nbsp;'.radiobutton_tag('validationRules[param_type'.$i.']', 'string', ($validationArray[$i-1]['param_type']=='string')?true:false).'&nbsp; String'
               // ); 
                ?>
                <input type="hidden" name="validation_id<?php echo $i?>" id="validation_id<?php echo $i?>" value="<?php echo $validationArray[$i-1]['id'] ?>">
                <div class="dsTitle4Fields"><label for="remove"></label></div><div class="dsInfo4Fields"><a href="javascript:void(0)" onClick=removeAcc('div_'+<?php echo $i?>,<?php echo $i?>)>remove</a><br/><br/>
            </div>
           
            <br><br>
        </div>

                    <?php
                }
            }
           else
           {
            ?><div  class="divBlock">
                    <center class="formHead">There is no Validation rules for this service.</center>
               </div>
            <?php
           }
            ?>
            <input type="hidden" name="merchant_service_id" id="merchant_service_id" value="<?php echo $MerchantServiceId?>">
            <input type="hidden" name="oldValue" id="oldValue" value="<?php echo $noOfValidRules?>">
            <input type="hidden" name="no_of_values" id="no_of_values" value="<?php echo implode(',',$noOfValues)?>">
            <input type="hidden" name="delIds" id="delIds" value="">
            <input type="hidden" name="count" id="count" value="<?php echo $noOfValidRules?>">
           
        
        </div>
        <div class="dsTitle4Fields"><a href="#" onClick="addMoreAcc();">Add Validation Rule</a></div>
        <div  class="divBlock">
            <center>
                &nbsp; <?php  echo button_to('Cancel','',array('class'=>'formCancel','onClick'=>'location.href=\''.url_for('merchantWizard/listMerchantWizard').'\''));?>
                <input type="hidden" name="validationRules[noOfValidRules]" id="validationRules_noOfValidRules" value="<?php echo $noOfValidRules?>">
                <?php //echo input_hidden_tag('validationRules[noOfValidRules]', $noOfValidRules) ?>
                <input type="button" value="Save and Continue" class="formSubmit" onclick="validateRules()" />
            </center>
        </div>


    </form>

</div>
</div>
<script>
    String.prototype.trim = function() {
	return this.replace(/,$/g,"");
}

     $(document).ready(function(){
     document.getElementById("count").value = <?php echo $noOfValidRules?>
    });

    function validateRules()
    {
        var err  = 0;
        var itrateval = document.getElementById('no_of_values').value;
        var valArra = new Array();
         valArra = itrateval.split(',');
      if(itrateval!='')
       {  
            for(i=0; i<valArra.length;i++ )
            {
               if(valArra[i])
                {
                    var paramId = "validationRules_param_name"+valArra[i];
                    var paramDesc = "validationRules_param_desc"+valArra[i] ;
                    var errorId = "err_param"+valArra[i];
                    var errorDesc = "err_desc"+valArra[i];
                  
                    if(!$('#'+paramId).val() )
                    {
                        $('#'+errorId).html("Please enter parameter");
                        err = err+1;
                    }
                    else
                    {
                        $('#'+errorId).html("");
                    }
                    if(!$('#'+paramDesc).val() )
                    {
                        $('#'+errorDesc).html("Please enter parameter description");
                        err = err+1;
                    }
                    else
                    {
                        $('#'+errorDesc).html("");
                    }
                }
            }
        }
        
        if(0==err)
        {
            for(i=0; i<valArra.length;i++ )
            {
                paramId = "validationRules_param_name"+valArra[i];
                for(j=0; j<valArra.length;j++ )
                {
                    secParamId = "validationRules_param_name"+valArra[j];
                    var errorId = "err_param"+valArra[j];
                    if( $('#'+paramId).val() == $('#'+secParamId).val() && i!=j)
                    {
                        $('#'+errorId).html("Duplicate parameter name");
                        err = err+1;
                    }
                }
            }
        }
          
        if(err == 0)
        {

            $('#validation_form').submit();

        }
    }
    function addMoreAcc()
    {
        var len = document.getElementById("count").value;  
      
        var tLoop = eval(len)+1;
        var divid = "div_"+tLoop;

        var divTag = document.createElement("div");
        divTag.id = divid;





        divTag.innerHTML = '<div class="formHead">Details For Validation Rule '+tLoop+'</div><dl><div class="dsTitle4Fields"><label for="accParty">Parameter Name<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="validationRules_param_name'+tLoop+'" type="text" class="FieldInput"   name="validationRules[param_name'+tLoop+']"/><br/><br/><div id="err_param'+tLoop+'" class="cRed"/></div></dl>  <dl id="divaccName"><div class="dsTitle4Fields"><label for="accName">Parameter Description<sup>*</sup></label></div><div class="dsInfo4Fields"><textarea id="validationRules_param_desc'+tLoop+'" class="FieldInput" type="text"  name="validationRules[param_desc'+tLoop+']"></textarea><br/><br/><br/><br/><div id="err_desc'+tLoop+'" class="cRed"/></div></dl>\n\
<dl><div class="dsTitle4Fields"><label for="accNo">Is Mandatory?<sup>*</sup></label></div><div class="dsInfo4Fields">\n\
<input id="validationRules_param_mandatory'+tLoop+'_yes"  type="radio" value="yes" checked name="validationRules[param_mandatory'+tLoop+']"/>&nbsp; Yes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input id="validationRules_param_mandatory'+tLoop+'_no" type="radio" value="no" name="validationRules[param_mandatory'+tLoop+']"/>&nbsp; No\n\
<div id="err_accNo'+tLoop+'" class="cRed"/></div></dl>   <dl><div class="dsTitle4Fields"><label for="bankName">Parameter Type<sup>*</sup></label></div><div class="dsInfo4Fields"><input name="validationRules[param_type'+tLoop+']"  type="radio" value="integer" checked  id="validationRules_param_type'+tLoop+'_integer" />&nbsp; Integer &nbsp; &nbsp; &nbsp; &nbsp;<input name="validationRules[param_type'+tLoop+']" type="radio" value="string" id="validationRules_param_type'+tLoop+'_string" />&nbsp; String<br/><br/><div id="err_bankName'+tLoop+'" class="cRed"/></div></dl><input type="hidden" name="validation_id'+tLoop+'" value=""><div class="dsTitle4Fields"><label for="remove"></label></div><div class="dsInfo4Fields"><a href="javascript:void(0)" onClick=removeAcc("'+divid+'","'+tLoop+'")>remove</a><br/><br/> '

       


      
        document.getElementById("consolDiv").appendChild(divTag);
        document.getElementById("count").value = eval(len) + 1;
        var allnoofValues= document.getElementById("no_of_values").value;
// Bugid #28295
         if(allnoofValues.search(',')>0)
             document.getElementById("no_of_values").value =document.getElementById("no_of_values").value +',' +tLoop;
        else
            if( allnoofValues!='')
                document.getElementById("no_of_values").value  = document.getElementById("no_of_values").value +',' +tLoop;
            else
                document.getElementById("no_of_values").value =tLoop;
                

    }
function removeAcc(val,removeval)
	{  
		var len = document.getElementById("count").value;
		
        var noofValues = document.getElementById("no_of_values").value;
       
       
        if(document.getElementById("validation_id"+removeval))
           {
               var delId = document.getElementById("validation_id"+removeval).value;
               
               if(document.getElementById("delIds").value!='')
                    document.getElementById("delIds").value =document.getElementById("delIds").value +',' +delId;
              else
                    document.getElementById("delIds").value=delId;
               
           }
        if(noofValues.search(removeval+',')>0)
            noofValues = noofValues.replace(removeval+',','');
        if(noofValues.search(','+removeval)>0)
            noofValues = noofValues.replace(','+removeval,'');
          
        
        
        document.getElementById("no_of_values").value = noofValues;
		document.getElementById("count").value = eval(len);
		var d1 = document.getElementById("consolDiv");
		var d2 = document.getElementById(val);        
		d1.removeChild(d2);

	}

</script>

