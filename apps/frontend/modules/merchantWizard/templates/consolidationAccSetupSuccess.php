<?php //use_helper('Form');
 echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
<?php
    if(isset($redirctTo))
      echo form_tag($sf_context->getModuleName().'/'.$redirctTo,' class="multiForm" onsubmit="return validateConsolidationForm()" id="consolidation_acc_frm" name="consolidation_acc_frm"');
    else
      echo form_tag($sf_context->getModuleName().'/consolidationAccSetup',' class="multiForm" onsubmit="return validateConsolidationForm()" id="consolidation_acc_frm" name="consolidation_acc_frm"');
      ?>
<?php echo ePortal_legend('Choose Currency');  ?>
<?php //echo formRowComplete("Currency<sup>*</sup>",select_tag('currency', options_for_select($selectedCurr)),'','','err_currency'); ?>
  <dl id="divcurrency">
        <div class="dsTitle4Fields">
            <label for="currency">Currency<sup>*</sup></label>
        </div>
        <div class="dsInfo4Fields">
            <select id="currency" name="currency" >
                <?php if (!empty($selectedCurr)) {
                    foreach ($selectedCurr as $k => $value)
                         ?>
                    <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
                <?php } ?>
            </select>
            <br>
            <br>
            <div id="err_currency" class="cRed"></div>
        </div>
      </dl>  
<?php echo ePortal_legend('Choose Account Split Type');  ?>

<?php
$selectBox ='<select name="splitType" id="splitType" style ="width:250px" onchange = "set_consolidation_frm()" >';
if(!empty($splitType)){
foreach($splitType as $key => $value) {
    $selectBox .='<option value='.$key.'>'.$value.'</option>';
}
}
$selectBox .='</select>';
 //echo formRowComplete('Split Type<br><br><br>',select_tag('splitType', options_for_select($splitType),array('style' => 'width:250px', 'onchange' => 'set_consolidation_frm()')),'','splitType','err_splitType','divsplitType');
 echo formRowComplete('Split Type<br><br><br>',$selectBox,'','splitType','err_splitType','divsplitType');
?>


<?php echo ePortal_legend('Enter The Receiving (Consolidation) Account Details.');  ?>

<div id="consolDiv" >
<div id="div_0">
<?php

echo formRowComplete('Account Party<sup>*</sup>','<input type="text" name="accParty0" id="accParty0" class="FieldInput">','','accParty0','err_accParty0','divaccParty0');
echo formRowComplete('Account Name<sup>*</sup>','<input type="text" name="accName0" id="accName0" class="FieldInput">','','accName0','err_accName0','divaccName0');
echo formRowComplete('Account Number<sup>*</sup>','<input type="text" name="accNo0" id="accNo0" class="FieldInput">','','accNo0','err_accNo0','divaccNo0');
echo formRowComplete('Bank Name<sup>*</sup>','<input type="text" name="bankName0" id="bankName0" class="FieldInput">','','bankName0','err_bankName0','divbankName0');
echo formRowComplete('Sort Code<sup>*</sup>','<input type="text" name="sortCode0" id="sortCode0" class="FieldInput">','','sortCode0','err_sortCode0','divsortCode0');
echo formRowComplete('Split<sup>*</sup>','<input type="text" name="split0" id="split0" class="FieldInput">','','split0','err_split0','divsplit0');


?>
</div>
</div>

<p ><a href="#" onClick="addMoreAcc();">Add More Account</a></p>


    <div class="divBlock">
      <center id="multiFormNav">
       <?php  //echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('merchantWizard/index').'\''));?> 
        
        &nbsp;
       <?php //echo input_hidden_tag('count', 1) ?>
       <input type="hidden" name="count" id="count" value="1">
        <?php //echo input_hidden_tag('totalRec', 1) ?>
        <input type="hidden" name="totalRec" id="totalRec" value="1">
       <?php //echo button_to('Continue','',array( 'class'=>'formSubmit', 'onClick'=>'validateConsolidationForm()')); ?>
        &nbsp;
          <?php 
           $getConsolidation = $sf_context->getUser()->getAttribute('consolidationAccDetails','0');
             if($getConsolidation)
                 echo button_to('Continue','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/'.$redirectTemplate).'\''));?>
           <input type="submit" value="Save" name="save" id="Save and Continue"  class="formSubmit" >
   
      </center>
    </div>



</form>

</div> </div>


<script>


  $(document).ready(function(){
     set_consolidation_frm();
    });


//function used to validate the form
function validateConsolidationForm(){
     var reg = /^-?\d*\.?\d+([eE]\d*.\d+)?$/;
     var totalPercent = 0;
     var count = $("#totalRec").val();
     var i;
     var err = 0;
     var run = 0;
     for(i=0;i < count;i++){

       if(document.getElementById('div_'+i)){

         if($('#accParty'+i).val() == ''){
           $('#err_accParty'+i).html('Please enter Account Party');
           err++
         }else{
           $('#err_accParty'+i).html('');
         }

         if($('#accName'+i).val() == ''){
           $('#err_accName'+i).html('Please enter Account Name');
           err++
         }else{
           $('#err_accName'+i).html('');
         }

        if($('#accNo'+i).val() == ''){
           $('#err_accNo'+i).html('Please enter Account Number');
           err++;
         }else if(isNaN($('#accNo'+i).val())){
           $('#err_accNo'+i).html('Please enter a valid Account Number');
           err++;
         }else{
           $('#err_accNo'+i).html('');
         }

         if($('#sortCode'+i).val() == ''){
           $('#err_sortCode'+i).html('Please enter Sort Code');
           err++;
         }else{
           $('#err_sortCode'+i).html('');
         }


             if($('#split'+i).val() == ''){
               $('#err_split'+i).html('Please enter the Split');
               err++;
             }else if(reg.test($('#split'+i).val()) == false ) {
               $('#err_split'+i).html('Please enter a valid Split');
               err++;
             }else{
               $('#err_split'+i).html('');
             }


        if($('#bankName'+i).val() == ''){
           $('#err_bankName'+i).html('Please enter Bank Name');
           err++;
         }else{
           $('#err_bankName'+i).html('');
         }
        

         if($("#splitType").val() == 2){            
               totalPercent = Number(totalPercent) + Number($('#split'+i).val());
         }

         if(totalPercent > 100 && run == 0){
             err++;
             run++;
             alert('You have entered more then 100% for Split. Please Check!');
         }
       }
     }

     if(err == 0){
         return true;
//         $('#consolidation_acc_frm').submit();
     }
     else{
         return false;
     }


 }




    //function used to add dynamic div's
 	function addMoreAcc()
	{
		var len = document.getElementById("count").value;
        var len1 = document.getElementById("totalRec").value;

        if($("#splitType").val() == 2){
           var totalPercent = checkPercentEnter(len1);
           if(totalPercent >= 100){
               alert('You have already distributed your 100% Split.');
               return false;
           }
        }

        var value2 = 1;
		var tLoop = eval(len);
		var divid = "div_"+tLoop;
		
			var divTag = document.createElement("div");
			divTag.id = divid;

            
			divTag.innerHTML = '<dl id="'+divid+'"><div class="dsTitle4Fields"><label for="blank"></label></div><div class="dsInfo4Fields"><br/><br/></dl> <dl id="divaccParty"><div class="dsTitle4Fields"><label for="accParty">Account Party<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="accParty'+tLoop+'" class="FieldInput" type="text" value="" name="accParty'+tLoop+'"/><br/><br/><div id="err_accParty'+tLoop+'" class="cRed"/></div></dl>  <dl id="divaccName"><div class="dsTitle4Fields"><label for="accName">Account Name<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="accName'+tLoop+'" class="FieldInput" type="text" value="" name="accName'+tLoop+'"/><br/><br/><div id="err_accName'+tLoop+'" class="cRed"/></div></dl>\n\
    <dl id="divaccNo"><div class="dsTitle4Fields"><label for="accNo">Account Number<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="accNo'+tLoop+'" class="FieldInput" type="text" value="" name="accNo'+tLoop+'"/><br/><br/><div id="err_accNo'+tLoop+'" class="cRed"/></div></dl>   <dl id="divbankName"><div class="dsTitle4Fields"><label for="bankName">Bank Name<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="bankName'+tLoop+'" class="FieldInput" type="text" value="" name="bankName'+tLoop+'" /><br/><br/><div id="err_bankName'+tLoop+'" class="cRed"/></div></dl>   <dl id="divsortCode"><div class="dsTitle4Fields"><label for="sortCode">Sort Code<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="sortCode'+tLoop+'" class="FieldInput" type="text" value="" name="sortCode'+tLoop+'"/><br/><br/><div id="err_sortCode'+tLoop+'" class="cRed"/></div></dl>  <dl id="divsplit"><div class="dsTitle4Fields"><label for="split">Split<sup>*</sup></label></div><div class="dsInfo4Fields"><input id="split'+tLoop+'" class="FieldInput" type="text" value="" name="split'+tLoop+'" maxLength=6/><br/><br/><div id="err_split'+tLoop+'" class="cRed"/></div></dl>   <dl id="remove"><div class="dsTitle4Fields"><label for="remove"></label></div><div class="dsInfo4Fields"><a href="javascript:void(0)" onClick=removeAcc("'+divid+'")>remove</a><br/><br/></dl> '
			
//            else if($("#splitType").val() == 2){
//             divTag.innerHTML = '<dl id="'+divid+'"><div class="dsTitle4Fields"><label for="blank"></label></div><div class="dsInfo4Fields"><br/><br/></dl> <dl id="divaccParty"><div class="dsTitle4Fields"><label for="accParty">Account Party</label></div><div class="dsInfo4Fields"><input id="accParty'+tLoop+'" class="FieldInput" type="text" value="" name="accParty'+tLoop+'"/><br/><br/><div id="err_accParty'+tLoop+'" class="cRed"/></div></dl>  <dl id="divaccName"><div class="dsTitle4Fields"><label for="accName">Account Name</label></div><div class="dsInfo4Fields"><input id="accName'+tLoop+'" class="FieldInput" type="text" value="" name="accName'+tLoop+'"/><br/><br/><div id="err_accName'+tLoop+'" class="cRed"/></div></dl>\n\
//    <dl id="divaccNo"><div class="dsTitle4Fields"><label for="accNo">Account Number</label></div><div class="dsInfo4Fields"><input id="accNo'+tLoop+'" class="FieldInput" type="text" value="" name="accNo'+tLoop+'"/><br/><br/><div id="err_accNo'+tLoop+'" class="cRed"/></div></dl>   <dl id="divsortCode"><div class="dsTitle4Fields"><label for="sortCode">Sort Code</label></div><div class="dsInfo4Fields"><input id="sortCode'+tLoop+'" class="FieldInput" type="text" value="" name="sortCode'+tLoop+'" /><br/><br/><div id="err_sortCode'+tLoop+'" class="cRed"/></div></dl> <dl id="divsplit"><div class="dsTitle4Fields"><label for="split">Split</label></div><div class="dsInfo4Fields"><input id="split'+tLoop+'" class="FieldInput" type="text" value="" name="split'+tLoop+'" maxLength=3/>%<br/><br/><div id="err_split'+tLoop+'" class="cRed"/></div></dl>  <dl id="remove"><div class="dsTitle4Fields"><label for="remove"></label></div><div class="dsInfo4Fields"><a href="javascript:void(0)" onClick=removeAcc("'+divid+'")>remove</a><br/><br/></dl> '
//            }


            //document.getElementById('showID').innerHTML = strFile;
			document.getElementById("consolDiv").appendChild(divTag);
			document.getElementById("count").value = eval(len) + eval(value2);
            document.getElementById("totalRec").value = eval(len1) + eval(value2);
		
	}

    //function used to remove the dynamic divs added.
	function removeAcc(val)
	{
		var len = document.getElementById("count").value;
		var value2 = 1;
		document.getElementById("count").value = eval(len) - eval(value2);
		var d1 = document.getElementById("consolDiv");
		var d2 = document.getElementById(val);
		d1.removeChild(d2);
	}


    //function used to chack the percentage enter, wether it is smaller then 100% or not
    function checkPercentEnter(count){
        var i;
        var totalPercent = 0;
        for(i=0;i < count;i++){
            if(document.getElementById('split'+i)){
             totalPercent = Number(totalPercent) + Number($('#split'+i).val());
            }
        }

        return totalPercent;
    }


   //function used to show hide entry form on dropdown change
    function set_consolidation_frm(){
       // var totalCount = $("#count").val();
        //var i;
        
        //clearing dynamic div's'
//        for(i=1;i < totalCount;i++){
//          if(document.getElementById('div_'+i)){
//           removeAcc('div_'+i);
//          }
//        }

        //clearing values
        //$("#accParty0").val('');
        $("#err_accParty0").html('');
        //$("#accName0").val('');
        $("#err_accName0").html('');
        //$("#accNo0").val('');
        $("#err_accNo0").html('');
        //$("#sortCode0").val('');
        $("#err_sortCode0").html('');
        //$("#split0").val('');
        $("#err_split0").html('');
        $("#err_bankName").html('');
               
    }



</script>
