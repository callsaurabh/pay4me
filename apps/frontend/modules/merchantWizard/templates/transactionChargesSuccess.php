<?php //use_helper('Form') ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<div>

         <?php echo ePortal_legend('Transaction Charges Detail'); ?>
    <div class="wrapForm2">
        <form action="<?php echo url_for($sf_context->getModuleName().'/transactionCharges') ?>" method="post" class="dlForm multiForm" id="form" name="form">

         <?php  
            include_component('merchantWizard', 'currencyPaymentmode' ,array('getSession'=>'transChargeData','onloadFunction'=>'showHideDiv();') );
            include_partial('MServiceCharges', array('MServiceCharges' => $TransactionCharges )) ?>
            <div class="passPolicy">

<b>  Click on continue button to proceed next step.</b>
  </div>
           <div  class="divBlock">
      <center>
        &nbsp;
          <?php  echo button_to('Continue','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/'.$redirectTemplate).'\''));?>
          <input type="submit" value="Save" name="save" id="save"  class="formSubmit" />
    </center>
  </div>

         </form>
    </div>
</div>
<script>

  $(document).ready(function()
 {

   showHideDiv();
        $("#paymentMode").change(function(){

            showHideDiv();

        });

 });
 
 function showHideDiv(){
   

    var payMode = $("#paymentMode").val();
    
    if(payMode && payMode !=1 && payMode !=14 && payMode !=15)
        $("#transaction_charges_finInstCharge_row").hide();
    else
        $("#transaction_charges_finInstCharge_row").show();
 }
 </script>