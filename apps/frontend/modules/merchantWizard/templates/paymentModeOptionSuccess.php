
<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/paymentModeOption', 'onsubmit="return validateTransferForm() " class="multiForm" id="payment_mode_frm" name="payment_mode_frm  "'); ?>
    <!-- change for bug 28952 remove full stop as typo error -->
    <?php echo ePortal_legend('Choose a Payment Mode Option For your Merchant-Service'); ?>

    <?php //echo formRowComplete('Payment Mode<sup>*</sup>',select_tag('payment', options_for_select($payment_choice),array('style' => 'width:250px', 'onchange' => 'set_payment_service_type()')),'','payemnt','err_payemnt','divpayment'); ?>
    <dl id="divcurrency">
        <div class="dsTitle4Fields">
            <label for="currency">Currency<sup>*</sup></label>
        </div>
        <div class="dsInfo4Fields">
            <select id="currency" name="currency">
                <?php
                if (!empty($selectedCurr)) {
                    foreach ($selectedCurr as $k => $value)

                ?>
                        <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
<?php } ?>
                </select>
                <br>
                <br>
                <div id="err_currency" class="cRed"></div>
            </div>
        </dl>

        <dl id="divpayment_mode">
            <div class="dsTitle4Fields"><label for="payment_mode_option">Payment Mode Option<sup>*</sup></label></div>
            <div class="dsInfo4Fields">
                <select id="payment_mode" multiple="multiple" name="payment_mode[]" onchange="return checkInterswitchSelected(<?php echo $merchantMapped ?>)">
                <?php
                    if (!empty($payment_mode_options)) {
                        foreach ($payment_mode_options as $key => $payment_mode) {
                ?>
                            <option value="<?php echo $key; ?>"  <?php if (empty($key)) {
 ?>selected="selected"  <?php } ?>><?php echo $payment_mode; ?></option>
<?php }
                    } ?>
                </select>
                <br>
                <br>
                <div id="err_payment_mode" class="cRed"></div>
            </div>
        </dl>

        <dl id="divinterswitch_category" style="display:none">
            <div class="dsTitle4Fields"><label for="category">Interswitch Category<sup>*</sup></label></div>
            <div class="dsInfo4Fields">
                <select id="interswitch_category"  name="interswitch_category[]" >
                <?php
                    if (!empty($interswitch_category_options)) {
                        foreach ($interswitch_category_options as $key => $category) {
                ?>
                            <option value="<?php echo $key; ?>"  <?php if ($key==$selectedCategory) { ?>selected="selected"  <?php } ?>><?php echo $category; ?></option>
<?php }
                    } ?>
                </select>
                <br>
                <br>
                <div id="err_interswitch_category" class="cRed"></div>
            </div>
        </dl>


    <?php //echo formRowComplete('Currency<sup>*</sup>',select_tag('currency', options_for_select($selectedCurr)),'','currency','err_currency','divcurrency'); ?>
    <?php //echo formRowComplete('Payment Mode Option<sup>*</sup>',select_tag('payment_mode', options_for_select($payment_mode_options),array('multiple' => true)),'','payment_mode_option','err_payment_mode','divpayment_mode'); ?>

    <?php
                    $paymentModeArr = $sf_context->getUser()->getAttribute('payment_mode_option', 0);
                    if ($paymentModeArr) {
    ?>
                        <div class="passPolicy">
                            <center>
                                <b>  Click on continue button to proceed next step.</b>
                            </center>
                        </div>
<?php
                    }
?>
            <div class="divBlock">
                <center id="multiFormNav">
            <?php
                    if ($paymentModeArr)
                        echo button_to('Continue', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName() . '?task=getNext') . '\''));
            ?>
            <input type="submit" value="Save" name="save" id="save"  class="formSubmit" />
        </center>
    </div>

<div class="note" style="display:none">
    <b style="color:red" > Note : Change in interswitch  category will be reflected in all the services of the merchant</b>
    </div>
</form>
</div>
<script>
    function checkInterswitchSelected(merchant_mapped){
        $('#divinterswitch_category').hide();
        $('.note').hide();
//       alert(merchant_mapped);
        var foundIS=false;
        $('#payment_mode :selected').each(function(i, selected){
            if($(selected).text().search("Interswitch")>-1 )
            {
                foundIS=true;
                $('#divinterswitch_category').show();
                $('.note').show();
            }
        });
        return foundIS;
    }
    function validateTransferForm()
    {
       
        var foundIS=true;;
        if($('#payment_mode').val() == '')
        {

            $('#err_payment_mode').html('Please select payment mode option');
            foundIS=false;
        }
        else{
            $('#payment_mode :selected').each(function(i, selected){
                if(($(selected).text().search("Interswitch")>-1) && ($('#interswitch_category').val() == '') && merchant_mapped==0)
                {
                   // alert('gjghj');
                    $('#err_interswitch_category').html('Please Select Interswitch Category ');
                    foundIS=false;
                }
            });
        }
        return foundIS;
    }
</script>


