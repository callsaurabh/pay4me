<?php //use_helper('Form');
 echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/'.$formAction,' class="dlForm multiForm" id="choose_user_form" onsubmit="return validateUserForm()" name="choose_user_form"');?>
    <div id='flash_error' class='error_list' style='display:none;height:1px;overflow-y:hidden'></div>


    <?php
      echo ePortal_legend('Choose Bank');
  ?>
<?php //echo formRowComplete("Currency<sup>*</sup>",select_tag('currency_id', options_for_select($selectedCurr)),'','','err_currency'); ?>
    <dl id="divcurrency">
        <div class="dsTitle4Fields">
            <label for="currency">Currency<sup>*</sup></label>
        </div>
         <div class="dsInfo4Fields">
            <select id="currency_id" name="currency_id" >
                <?php if (!empty($selectedCurr)) {
                    foreach ($selectedCurr as $k => $value)
                    {   ?>
                    <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
                <?php } } ?>
            </select>
            <br>
            <br>
            <div id="err_currency" class="cRed"></div>
        </div>  
    </dl>
    
    <dl id="">
<div class="dsTitle4Fields">
<label for="">
Bank
<sup>*</sup>
</label>
</div>
<div class="dsInfo4Fields">
<select id="bank_id" multiple="multiple" name="bank_id[]">
     <option>Please select bank </option>
     <?php if (!empty($bank_array)) {
                    foreach ($bank_array as $k => $value) { ?>
                    <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
                <?php } } ?>
     </select>
</select>
<br>
<br>
<div id="err_bank" class="cRed"></div>
</div>
</dl>
    
<?php //echo formRowComplete("Bank<sup>*</sup>",select_tag('bank_id', options_for_select($bank_array, '', array('include_custom'=>'Please select bank')),array('multiple' => true)),'','','err_bank'); ?>
  <?php
   $getAllBankDetails = $sf_context->getUser()->getAttribute('bankDetails','0');
  if($getAllBankDetails || 'updateBanks'==$sf_context->getActionName())
  {
      ?>
       <div class="passPolicy">
<center>
     
    <b>  Click on continue button to proceed next step.</b>
    </center>
      </div>
  <?php
  }
  ?>
    <div class="divBlock">
      <center id="multiFormNav">
       <?php  
       

       if('updateBanks'==$sf_context->getActionName())
             echo button_to('Continue','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/updateCollectionAccountSetup').'\''));
        else
        if($getAllBankDetails)
           {
                   echo button_to('Continue','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/collectionAccountSetup').'\''));
           }
               ?>
         
          <input type="submit" value="Save" name="save" id="save"  class="formSubmit"  />
      </center>
    </div>


</form>
</div>
<script>
    $(document).ready(function()
    {
         getbanks();
        $("#currency_id").select(function (){
                getbanks();
            });

            $("#currency_id").change(function()
            {
                getbanks();

            });

  
    });
  function getbanks()
  {
          var currency =  $('#currency_id').val();

        var url = "<?php echo url_for("merchantWizard/getBanks");?>";

        $("#bank_id").load(url, { currency: currency,byPass:1},function (data){
            if(data=='logout'){
               location.reload();
             }
           });
  }
  function validateUserForm()
  {
    var err  = 0;
    if(!$('#bank_id').val() || $('#bank_id').val() == "")
    { 
      $('#err_bank').html("Please select bank");
      err = err+1;
    }
    else
    {
      $('#err_bank').html("");
    }

    if(err == 0)
    {    
          return true;
    }
    return false;
  }

 
</script>


