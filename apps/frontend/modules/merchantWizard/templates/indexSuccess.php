<?php //use_helper('Form') ?>
<script>

  $(document).ready(function()
 {

     $('#selectBy_addMerc').click(function(){
            $('#div_choose_merc').slideUp('slow')
            $("#div_add_merc").slideDown('slow');
            $("#div_add_merc").show('slow');
            $("#div_choose_merc").hide();
       });
     $('#selectBy_chooseMerc').click(function(){
        $('#div_add_merc').slideUp('slow')
       $("#div_choose_merc").slideDown('slow');
       $("#div_choose_merc").show('slow');
       $("#div_add_merc").hide();
     });



});

</script>
<div class="wrapTable">
<?php echo ePortal_pagehead(" ",array('class'=>'_form'));

 $chooseMercVal=false;
 $chooseformDisplay="none";
 $addMercVal=false;
 $addformDisplay="none";
 if(1==$form->isBound()){
          $chooseMercVal=true;
          $chooseformDisplay="block";
  }
  if(1==$addMercForm->isBound()){
          $addMercVal=true;
          $addformDisplay="block";
  }
 echo  ePortal_legend('Merchant/Merchant Service Create Wizard'); ?>
<div class="descriptionArea" align="left">This wizard provides steps to add merchant/merchant-service. </div>
<?php
    if($merchantExists > 0){
 ?>
  <div  id="div_choose_merc">
        <?php echo ePortal_legend('Choose Merchant'); 
        include_partial('form', array('form' => $form )) ?>
  </div>
<?php
 }else{
   echo  ePortal_legend('Select an option'); 
 ?>
<div class="descriptionArea" align="left">
    Choose from existing merchants 
    <input id="selectBy_chooseMerc" type="radio" value="chooseMerc" name="selectBy" <?php if($chooseMercVal) { ?> checked="checked" <?php } ?>>

<?php if($userGroup != 'merchant') { ?>
     &nbsp; &nbsp; &nbsp; &nbsp;
    Add new merchant
    <input id="selectBy_addMerc" type="radio" value="addMerc" name="selectBy" <?php if($addMercVal) { ?> checked="checked" <?php } ?>>
    <?php }//echo radiobutton_tag('selectBy', 'addMerc', $addMercVal) ?>

</div>

<div  style="display:<?php echo $chooseformDisplay; ?>;"  id="div_choose_merc">
         
                <?php echo ePortal_legend('Choose Merchant'); ?>
            
         <?php  include_partial('form', array('form' => $form )) ?>
</div>
<div style="display:<?php echo $addformDisplay; ?>" id="div_add_merc">
      
                <?php echo ePortal_legend('Add Merchant'); ?>
            
        <?php  include_partial('addMercForm', array('addMercForm' => $addMercForm)) ?>
</div>

<?php } ?>
</div>