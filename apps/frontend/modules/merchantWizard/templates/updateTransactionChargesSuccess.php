<?php //use_helper('Form') ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
  <div>
       <?php echo ePortal_legend('Transaction Charges Detail'); ?>
        <div class="wrapForm2">
        <form action="<?php echo url_for($sf_context->getModuleName().'/updateTransactionCharges') ?>" method="post" class="dlForm multiForm" id="form" name="form">
         <dl id="divcurrency">
        <div class="dsTitle4Fields">
            <label for="currency">Currency<sup>*</sup></label>
        </div>
        <div class="dsInfo4Fields">
            <select id="currency" name="currency">
                <?php if (!empty($selectedCurr)) {
                    foreach ($selectedCurr as $k => $value)
                         ?>
                    <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
                <?php } ?>
            </select>
            <br>
            <br>
            <div id="err_currency" class="cRed"></div>
        </div>
    </dl>  
    <dl id="">
        <div class="dsTitle4Fields">
            <label for="">Payment Mode<sup>*</sup>
            </label>
        </div>
        <div class="dsInfo4Fields">
            <select id="paymentMode" name="paymentMode">
                 <?php if (!empty($paymentMode)) {
                    foreach ($paymentMode as $k => $value)
                         ?>
                    <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
                <?php } ?>
            </select>
            <br>
            <br>
            <div id="err_paymentMode" class="cRed"></div>
        </div>
    </dl>        
            
            
         <?php
            
           // echo formRowComplete("Currency<sup>*</sup>",select_tag('currency', options_for_select($selectedCurr)),'','','err_currency');
            
            
           // echo formRowComplete("Payment Mode<sup>*</sup>",select_tag('paymentMode', options_for_select($paymentMode, '')),'','','err_paymentMode');
            include_partial('MServiceCharges', array('MServiceCharges' => $TransactionCharges )) ?>
           <div  class="divBlock">
      <center>
       <div class="passPolicy">

        <b>  Click on continue button to proceed next step.</b>
  </div>
        &nbsp;
          <?php 
//          echo button_to('Continue','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/listMerchantWizard').'\''));
          echo button_to('Continue','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for($sf_context->getModuleName().'/updateConsolidationAccSetup').'\''));?>
          <input type="submit" value="Save" name="save" id="save"  class="formSubmit" />
    </center>
  </div>

         </form>
    </div>
</div>
<script>
    $(document).ready(function()
    {
        getPaymentMode();
           showHideDiv();

        $("#currency").select(function (){
                getPaymentMode();
            });

            $("#currency").change(function()
            {
                getPaymentMode();

            });

            $("#paymentMode").select(function (){
                 getServiceCharges();
                    showHideDiv();
            });

            $("#paymentMode").change(function()
            {
                getServiceCharges();
                

            });

       });
    function getPaymentMode()
    {

        var currency =  $('#currency').val();

        var url = "<?php echo url_for("merchantWizard/getPaymentModeforEdit");?>";

        $("#paymentMode").load(url, { currency: currency,byPass:1},function (data){
            if(data=='logout'){
               location.reload();
             }
              getServiceCharges();
              
           });

    }
    function getServiceCharges()
    {

        var paymentMode =  $('#paymentMode').val();
        var currency =  $('#currency').val();

        var url = "<?php echo url_for("merchantWizard/resultTransactionCharges");?>";

        $.post(url, { currency: currency, paymentMode: paymentMode,byPass:1},function (data){
          
            if(data=='logout'){
               location.reload();
             }
             else
                { 
                    
                    var dataArr=data.split('###');
                  $("#transaction_charges_PayForCharge").val(dataArr[1]);
                  $("#transaction_charges_finInstCharge").val(dataArr[0]);
                   showHideDiv();
//    //             if(dataArr[0])
                }
           });

    }
  function showHideDiv(){


    var payMode = $("#paymentMode").val();
    <?php
        $pfmHelperObj = new pfmHelper();
        $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
        $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
        $paymentModeBankId = $pfmHelperObj->getPMOIdByConfName('bank');
    ?>

    if(payMode && payMode !=<?php echo $paymentModeCheckId; ?> && payMode !=<?php echo $paymentModeDraftId; ?> && payMode !=<?php echo $paymentModeBankId; ?>)
        $("#transaction_charges_finInstCharge_row").hide();
    else
        $("#transaction_charges_finInstCharge_row").show();
 }
</script>