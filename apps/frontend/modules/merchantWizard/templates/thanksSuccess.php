<?php //use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/index',' class="dlForm multiForm" id="thanks_form" name="thanks_form"');?>
   


    <?php
      echo ePortal_listinghead('Thanks For Using The Wizard. Your details have been saved.').'<br><br>';
     //print_R($xmlDetails[1]);
     $headers='';
     foreach($xmlDetails[1] as $val)
        $headers=$headers.$val.'<br>';
  ?>
  
    <?php echo simpleRow("<b>Pay4Me Redirectional URL</b>",$xmlDetails[0],'fullrow'); ?>
    <?php echo simpleRow("<b>Headers</b>",$headers,'fullrow'); ?>
    <?php echo simpleRow("<b>Sample XML</b>",'<pre>'.htmlentities($xmlDetails[2]).'</pre>','fullrow'); ?>
    <?php //echo simpleRow('' ,button_to('Thank You','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('merchantWizard/index').'\'')),'fullrow'); ?>
    <?php //echo simpleRow('' ,button_to('Preview the Setup','',array('class'=>'formSubmit','onClick'=>'javascript:progBarDiv("merchantPdf", "csvDiv1", "progBarDiv1",'.$merchantId.')')),'fullrow'); ?>
    <?php //echo simpleRow('' ,button_to('Freeze the Setup','',array('class'=>'formSubmit','id'=>'freezeBut','onClick'=>'freezMerchant('.$merchantId.')')),'fullrow'); ?>

    <div class="wrapForm2">
    <center>
    <span id="unFreezed">
            <?php
            echo button_to('Preview The Setup','',array('class'=>'formSubmit','id'=>'csvDiv1','onClick'=>'javascript:progBarDiv("merchantPdf", "csvDiv1", "progBarDiv1",'.$merchantId.')'));?>
            <span id ="progBarDiv1"  style="display:none;"></span>
    </span>
            <?php echo button_to('Add More Service','',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('merchantWizard/index').'\''));?>
            <?php
            if($freezed==0)
                echo button_to('Freeze The Setup','',array('class'=>'formSubmit','id'=>'freezeBut','onClick'=>'freezMerchant('.$merchantId.')'));?>
    <span id="freezed" style="display:none;">
            <?php
            echo button_to('Download Pdf','',array('class'=>'formSubmit','id'=>'csvDiv2','onClick'=>'javascript:progBarDiv("merchantPdf", "csvDiv2", "progBarDiv2",'.$merchantId.')'));?>
            <span id ="progBarDiv2"  style="display:none;"></span>
    </span>
    </center>
    </div>
    <?php echo formRowFormatRaw(" ",' '); ?>
  </div>
  
</form>
<script>
    
 function freezMerchant(merchantId)
    {
      var answer = confirm("Do you want to freeze the merchant?");
     if (answer)
     {
         var url = "<?php echo url_for($sf_context->getModuleName().'/freezedMerchant'); ?>"+"?merchantId="+merchantId;
        $.post(url, {byPass:1 , merchantId:merchantId},function (data){
            if(data){
                $('#freezeBut').hide();
                $('#unFreezed').hide();
                $('#freezed').show();

            } });
        }
    }
 function progBarDiv(url, referenceDivId, targetDivId,merchantId){
    if(url=='merchantPdf')
        var url = "<?php echo url_for($sf_context->getModuleName().'/merchantPdf'); ?>";
    if(url=="merchantSql")
        var url = "<?php echo url_for($sf_context->getModuleName().'/merchantSql'); ?>";
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 , merchantId:merchantId},function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>

