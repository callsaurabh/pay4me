<?php //use_helper('Form');
 echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
<?php 
    if(isset($formAction))
        echo form_tag($sf_context->getModuleName().'/'.$formAction,' class="multiForm" id="collection_acc_frm" name="collection_acc_frm"');
    else
        echo form_tag($sf_context->getModuleName().'/collectionAccountSetup',' class="multiForm" id="collection_acc_frm" name="collection_acc_frm"');
        
        ?>

<?php echo ePortal_legend('Enter The Collection Account Details');  ?>

<?php
//echo $form;
$count = 1;
foreach($banks as  $outerKey=>$outerVal)
{
    foreach ($outerVal as $key=>$val){
//Bugid #28831
     echo formRowComplete('Bank Name: <b>'.$val[1].' </b><br>Currency : <b> '.$selectedCurr[$outerKey].' </b>','','','','','');
     
     if(!isset($val[4]))
        $val[4]= array('account_id'=>0,'account_number'=>'','account_name'=>'','sortcode'=>'');
    echo formRowComplete('Account Number<sup>*</sup>','<input id="accNo'.$count.'" class="FieldInput" type="text" value="'.$val[4]['account_number'].'" name="accNo'.$count.'">','','accNo','err_accNo'.$count,'divaccNo');
    //echo formRowComplete('Account Number<sup>*</sup>',input_tag('accNo'.$count, $sf_params->get('accNo'.$count),array('class' => 'FieldInput','value'=>$val[4]['account_number'])),'','accNo','err_accNo'.$count,'divaccNo');
    echo formRowComplete('Account Name<sup>*</sup>','<input id="accName'.$count.'" class="FieldInput" type="text" value="'.$val[4]['account_name'].'" name="accName'.$count.'">','','accName','err_accName'.$count,'divaccName');
    //echo formRowComplete('Account Name<sup>*</sup>',input_tag('accName'.$count, $sf_params->get('accName'.$count),array('class' => 'FieldInput','value'=>$val[4]['account_name'])),'','accName','err_accName'.$count,'divaccName');
    echo formRowComplete('Sort Code<sup>*</sup>','<input id="sortCode'.$count.'" class="FieldInput" type="text" value="'.$val[4]['sortcode'].'" name="sortCode'.$count.'">','','sortCode','err_sortCode'.$count,'divsortCode');
    //echo formRowComplete('Sort Code<sup>*</sup>',input_tag('sortCode'.$count, $sf_params->get('sortCode'.$count),array('class' => 'FieldInput','value'=>$val[4]['sortcode'])),'','sortCode','err_sortCode'.$count,'divsortCode');
    ?>
      <input type="hidden" name="<?php echo 'bank'.$count; ?>" id="<?php echo 'bank'.$count; ?>" value="<?php echo $val[1]; ?>" >
      <input type="hidden" name="<?php echo 'bankId'.$count; ?>" id="<?php echo 'bankId'.$count; ?>" value="<?php echo $val[0]; ?>" >
      <input type="hidden" name="<?php echo 'currencyId'.$count; ?>" id="<?php echo 'currencyId'.$count; ?>" value="<?php echo $outerKey; ?>" >
      <input type="hidden" name="<?php echo 'account_id'.$count; ?>" id="<?php echo 'account_id'.$count; ?>" value="<?php echo $val[4]['account_id']; ?>" >
    <?php
    //echo input_hidden_tag('bank'.$count, $val[1]);
    //echo input_hidden_tag('bankId'.$count, $val[0]);
    //echo input_hidden_tag('currencyId'.$count, $outerKey);
    //echo input_hidden_tag('account_id'.$count, $val[4]['account_id']);
    $count++;
    }
}
?>

    <div class="divBlock">
      <center id="multiFormNav">
       <?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('merchantWizard/index').'\''));?> &nbsp;
       <?php //echo input_hidden_tag('noOfBanks', $count) ?>
       <input type="hidden" name="noOfBanks" id="noOfBanks" value="<?php echo $count; ?>">
       <?php echo button_to('Save and Continue','',array( 'class'=>'formSubmit', 'onClick'=>'validateCollectionForm('.($count-1).')')); ?>
      </center>
    </div>


</form>
</div></div>


<script>
 function validateCollectionForm(noOfBanks){
     var i;
     var err = 0;
     for(i=1;i<=noOfBanks;i++){

        if($('#accNo'+i).val() == ''){
           $('#err_accNo'+i).html('Please enter Account Number');
           err++;
         }else if(validateString($('#accNo'+i).val())){
           $('#err_accNo'+i).html('Please enter a valid Account Number');
           err++;
         }else{
           $('#err_accNo'+i).html('');
         }


         if($('#accName'+i).val() == ''){
           $('#err_accName'+i).html('Please enter Account Name');
           err++
         }else{
           $('#err_accName'+i).html('');
         }

         if($('#sortCode'+i).val() == ''){
           $('#err_sortCode'+i).html('Please enter Sort Code');
           err++;
         }else{
           $('#err_sortCode'+i).html('');
         }
     }


     if(err == 0){
         $('#collection_acc_frm').submit();
     }

 }
  function validateString(str) {
    var reg = /^([A-Za-z0-9\.])+$/;

    if(reg.test(str) == false) {
      return true;
    }
    return false;
  }
</script>
