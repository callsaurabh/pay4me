<div id="theMid">
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

 <?php echo  ePortal_legend('Merchant/Merchant Service Update Wizard'); ?>

<div class="descriptionArea" align="left">

    This wizard provides steps to update merchant/merchant-service.

</div>
 <?php echo  ePortal_legend('Select Merchant '); ?>
 <?php  include_partial('uform', array('form' => $form )) ?>



        <?php echo ePortal_listinghead('Merchant Wizard List');?>
 <?php if(($pager->getNbResults())>0 && $MerchantId>0) { ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="50%" align="left" >
        <?php if($isFreezed>0)
           {
         ?>
            <span class="floatLeft" ><div id = "csvDiv1" style="cursor:pointer;" onclick="javascript:progBarDiv('merchantPdf', 'csvDiv1', 'progBarDiv1',<?php echo $MerchantId;?>)">Click here to download PDF</div><div id ="progBarDiv1"  style="display:none;"></div></span>
        <?php
           }
           else
           {
           ?><span class="floatLeft" ><div id = "csvDiv1" style="cursor:pointer;" onclick="freezMerchant(<?php echo $MerchantId;?>)">Click here to freeze the merchant</div></span>
           <?php
           }
         ?>
      </th>
      <th width="50%" align="right" style="text-align:right" >
        <span ><div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('merchantSql', 'csvDiv', 'progBarDiv',<?php echo $MerchantId;?>)">Click here to download Merchant Sql </div><div id ="progBarDiv"  style="display:none;"></div></span>
      </th>
    </tr>
  </table>
</div>
<br class="pixbr" />
<?php } ?>
  <div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
        </tr>
      </table>
    <br class="pixbr" />
  </div>

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Merchant</th>
      <th>Merchant Services</th>      
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
     if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
    ?>
    <tr class="alternateBgColour">
      <td><?php echo $i ?></td>
      <td align="center"><?php echo $result->getMerchantName() ?></td>
      <td align="center"><?php echo $result->getName() ?></td>      
     <td align="center"><?php 
            if($result->getMerchant()->getIsFreezed()!=1)
            echo link_to(' ', 'merchantWizard/edit?id='.base64_encode($result->getId()), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit'));
             else
                    echo '<a class="editInfo" href="#"  title="Edit" onclick=updateFreezed('.$result->getId().')> </a>';
                echo "&nbsp;&nbsp;";
             echo link_to('View Detail', url_for('merchantWizard/viewDetail?id='.$result->getId()), array('popup' => array('popupWindow', 'width=870,height=500,left=150,top=0,scrollbars=yes')));
               ?>

      </td>
    </tr>
    <?php endforeach; ?>
     <tr><td colspan="5">
<div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?merchant_id='.$MerchantId), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

  </div></td></tr>

    <?php }else{ ?>
    <!-- FS#29384 -->
     <tr><td  align='center' class='error' colspan="5">No service found for this merchant</td></tr>
    <?php } ?>

  </tbody>
   
</table>
</div>

</div>
</div>

<script>
/// CR046
    function updateFreezed(merchantServiceId)
    {
        var answer = confirm("Do you want to update freezed service?")
        if (answer)
          {
              var url = "<?php echo url_for($sf_context->getModuleName().'/updateFreezedMerchant'); ?>"+"?merchantServiceId="+merchantServiceId;
              window.location = url
//              document.location(url);
          }
        
    }
function freezMerchant(merchantId)
{
     var answer = confirm("Do you want to freeze the merchant?");
     if (answer)
     {
        var url = "<?php echo url_for($sf_context->getModuleName().'/freezedMerchant'); ?>"+"?merchantId="+merchantId;
        $.post(url, {byPass:1 , merchantId:merchantId},function (data){
        if(data){
                $('#csvDiv1').unbind('click');
                //binding new function after freez
                document.getElementById("csvDiv1").onclick = function(){
                    progBarDiv('merchantPdf', 'csvDiv1', 'progBarDiv1',merchantId);
                }                
                $('#csvDiv1').html('Click here to download PDF');
                //appending new div
                $('#csvDiv1').after('<div id ="progBarDiv1"  style="display:none;"></div>');

               // $('#pfm_merchant_service_form').submit();                   
        } });
    }
}
function progBarDiv(url, referenceDivId, targetDivId,merchantId){
    if(url=='merchantPdf')
        var url = "<?php echo url_for($sf_context->getModuleName().'/merchantPdf'); ?>";
    if(url=="merchantSql")
        var url = "<?php echo url_for($sf_context->getModuleName().'/merchantSql'); ?>";
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline"); 
    $('#'+targetDivId).load(url, {byPass:1 , merchantId:merchantId},function (data){
                            if(data=='logout'){
                                $('#'+targetDivId).html('');
                                location.reload();
                              } });

}
</script>