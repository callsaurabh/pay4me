<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class merchantWizardActions extends sfActions {

    public $numArr = array(1 => 'one', 2 => 'two', 3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six', 7 => 'seven', 8 => 'eight', 9 => 'nine', 10 => 'ten');
    public $iparamNum = 0;
    public $sparamNum = 0;
    public $merchantObj = false;
    public $merchantServiceObj = false;
    public $merchantServiceId = false;

    // Main function for crating Merchant Service
    public function executeIndex(sfWebRequest $request){
		
    	$pfmHelperObj = new pfmHelper();
    	$this->userGroup = $pfmHelperObj->getUserGroup();
    	if($this->userGroup == "merchant"){
    		$userId=$this->getUser()->getGuardUser()->getid();
    		$merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
    	}
    	$pfmHelperObj = new pfmHelper();
    	$this->userGroup = $pfmHelperObj->getUserGroup();
    	if($this->userGroup == "merchant"){
    		$userId=$this->getUser()->getGuardUser()->getid();
    		$merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
    	}
        $this->form = new ChooseMerchantForm('',array('merchant_id' => $merchant_id));
        $this->addMercForm = new MerchantForm();
        $this->merchantExists = 0;

        if('ewallet_user'==sfcontext::getInstance()->getUser()->getGroupName()){
            $userId=sfcontext::getInstance()->getUser()->getGuardUser()->getId();
            $merchantCreated = Doctrine::getTable('Merchant')->getMerchantDetailsByEwalletId($userId);

            $this->merchantExists = $merchantCreated->count();
        }


        if ($request->isMethod('post') && $this->form->getDefault('merchantFormVal')==1){
            $this->form->bind($request->getParameter('merchant'));
            $merchant_params = $request->getPostParameters();
            if ($this->form->isValid()) {
                $merchant_params = $request->getParameter('merchant');
                $merchant_id = $merchant_params['merchant_id'];
                $merchantDetails = Doctrine::getTable('Merchant')->getMerchantDetailsById($merchant_id);

                //setting merchant data session
                $merchantDataArr = array($merchantDetails[0]['name'],$merchantDetails[0]['merchant_code'],$merchantDetails[0]['merchant_key'],$merchantDetails[0]['auth_info']);
                $this->getUser()->setAttribute('merchantId', $merchant_id);
                $this->getUser()->setAttribute('merchantData', $merchantDataArr);

                $this->redirect('merchantWizard/createMService');
            }
        }

        //clearing session data
        //$this->getUser()->getAttributeHolder()->clear();
        $this->getUser()->getAttributeHolder()->remove('merchantData');
        $this->getUser()->getAttributeHolder()->remove('merchantId');
        $this->getUser()->getAttributeHolder()->remove('merchantServiceData');
        $this->getUser()->getAttributeHolder()->remove('transChargeData');
        $this->getUser()->getAttributeHolder()->remove('merchantServiceCharges');
        $this->getUser()->getAttributeHolder()->remove('noOfValidRules');
        $this->getUser()->getAttributeHolder()->remove('validationRules');
        $this->getUser()->getAttributeHolder()->remove('totalRulesEntered');
        $this->getUser()->getAttributeHolder()->remove('bankDetails');
        $this->getUser()->getAttributeHolder()->remove('bankReceivingAccDetails');
        $this->getUser()->getAttributeHolder()->remove('collectionAccDetails');
        $this->getUser()->getAttributeHolder()->remove('consolidationAccDetails');
        $this->getUser()->getAttributeHolder()->remove('payment_mode_option');
        $this->getUser()->getAttributeHolder()->remove('merchantServiceId');


    }

      // Main function for crating New Merchant
    public function executeNewmerchant(sfWebRequest $request){
        $this->form = new ChooseMerchantForm();
        $this->addMercForm = new MerchantForm();
        $this->merchantExists = 0;

        if ($request->isMethod('post')){
            $this->addMercForm->bind($request->getParameter('merchant'));
            if ($this->addMercForm->isValid()) {

                $merchant_params = $request->getPostParameters();

                $name = $merchant_params['merchant']['name'];
                $id = $merchant_params['merchant']['id'];
                $categoryId = $merchant_params['merchant']['category_id'];
                
                
                $merchant_obj = merchantServiceFactory::getService(merchantServiceFactory::$TYPE_BASE);
                $already_created = $merchant_obj->getTotalMerchant($name,$id);
                if($already_created) {
                    $this->getUser()->setFlash('error', sprintf('Merchant of same name already exists'));
                }
                else //there is no duplicacy
                {
                    do {
                        $merchant_code = mt_rand();
                        $duplicacy_code = $merchant_obj->chkCodeDuplicacy($merchant_code);
                    } while ($duplicacy_code > 0);


                    $merchant_key = mt_rand();
                    $auth_info = base64_encode($merchant_code .":" .$merchant_key);

                    //setting merchant data session
                    $merchantDataArr = array($name,$merchant_code,$merchant_key,$auth_info, $categoryId);
                    $this->getUser()->getAttributeHolder()->remove('merchantData');
                    $this->getUser()->setAttribute('merchantData', $merchantDataArr);

                    // $this->getUser()->setFlash('notice', sprintf('Merchant added successfully <br> Merchant code is - '.$merchant_code.', Merchant Key is - '.md5($merchant_key)));


                    $this->redirect('merchantWizard/createMService');
                }
            }
        }
        $this->setTemplate('index');
    }

// Main function for crating New Merchant Service
    public function executeCreateMService(sfWebRequest $request)
    {

        //checking direct hit
        $this->checkIsSessionSet(2);

        $this->getUser()->getAttributeHolder()->remove('merchantServiceData');
        $this->addMserviceform = new MServiceWizdForm();
        if ($request->isMethod('post'))
        {
            $this->addMserviceform->bind($request->getParameter('merchant_service'));
            $merchant_service = $request->getPostParameters();
            if ($this->addMserviceform->isValid()) {
                $merchant_service = $request->getParameter('merchant_service');

                $merchantId=$this->getUser()->getAttribute('merchantId');
                $name = $merchant_service['name'];
                $merchant_obj = merchant_serviceServiceFactory::getService(merchantServiceFactory::$TYPE_BASE);
                $already_created = $merchant_obj->checkDuplicacy($merchantId,$name);
                if($already_created) {
                    $this->getUser()->setFlash('error', sprintf('Merchant Service "'.$name.'" already exists'));
                }
                else{
                    if($merchant_service['version'] == '')
                    $merchant_service['version'] = "v1";
                    $this->getUser()->setAttribute('merchantServiceData', $merchant_service);
                    $this->getUser()->setAttribute('merchantServiceDataSQL', $merchant_service);
                    $this->redirect('merchantWizard/noofvalidrules');
                }
            }
        }
        $this->setTemplate('CreateMService');
    }
// Main function for crating getting Transaction Charges
    public function executeTransactionCharges(sfWebRequest $request)
    {
        //checking direct hit
        $this->checkIsSessionSet(6);

//        $this->getUser()->getAttributeHolder()->remove('transChargeData');

        //check for sandbox
        if(sfconfig::get('sf_environment') == "sandbox"){
            $this->createSandBoxTransCharge($PayModeDetail);

            if(in_array(1,$PayModeOptArr)){
                $this->redirect($this->moduleName.'/choosebank');
            }else{
                $this->redirect($this->moduleName.'/consolidationAccSetup');
            }
        }


        $SelectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $SelectedPayMode = $this->getUser()->getAttribute('payment_mode_option', 0);


        $this->TransactionCharges = new TransactionChargesForWiz(array() , array('paymentMode'=>$SelectedPayMode['1'],'type'=>'add'));
        $this->redirectTemplate = 'consolidationAccSetup';
        $pfmHelperObj = new pfmHelper();
        $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
        $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
        $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');
        foreach($SelectedPayMode as $valArr)
        {
         if(array_key_exists($paymentModebankId,$valArr) || array_key_exists($paymentModeCheckId,$valArr) || array_key_exists($paymentModeDraftId,$valArr) ){
                $this->redirectTemplate = 'choosebank';
                break;
            }
        }
        $getCharges = $this->getUser()->getAttribute('transChargeData','0');

        if($this->isAllModeDone($getCharges))
              $this->redirect($this->moduleName.'/'. $this->redirectTemplate);
        if ($request->isMethod('post'))
        {
            //// print_r($request->getParameter('merchant_service_charges'));die;

            $this->TransactionCharges->bind($request->getParameter('transaction_charges'));


            if ($this->TransactionCharges->isValid() && $request->getParameter('save')) {
                $getTransactionCharges = $this->getUser()->getAttribute('transChargeData');

                $transactionChg = $request->getParameter('transaction_charges');
                $paramHolder = $request->getParameterHolder()->getAll();
                $transactionChg['currency']=$paramHolder['currency'];
                $transactionChg['paymentMode']=$paramHolder['paymentMode'];

                $chkStatus=false;
                if($getTransactionCharges)
                    {
                        foreach($getTransactionCharges as $getNeedle)
                        {
                            if($getNeedle['currency']==$transactionChg['currency'] && $getNeedle['paymentMode']==$transactionChg['paymentMode'])
                               {
                                   $chkStatus=true;
                                   break;
                               }

                        }
                    }
                    if($chkStatus)
                        $this->getUser()->setFlash('error', sprintf('Transaction Charges for Currency '.$SelectedCurr[$transactionChg['currency']].' and Payment Mode '.$SelectedPayMode[$transactionChg['currency']][$transactionChg['paymentMode']].' already defined'));
                    else{
                        $this->getUser()->setFlash('notice', sprintf('Transaction Charges has been added for Currency '.$SelectedCurr[$transactionChg['currency']].' and  Payment Mode '.$SelectedPayMode[$transactionChg['currency']][$transactionChg['paymentMode']]));
                        unset($transactionChg['id']);
                        unset($transactionChg['_csrf_token']);
                        $getTransactionCharges[] = $transactionChg;
                        $this->getUser()->setAttribute('transChargeData', $getTransactionCharges);
                        if($this->isAllModeDone($getTransactionCharges))
                              $this->redirect($this->moduleName.'/'. $this->redirectTemplate);
                    }

                    $request->setParameter('save', false);
                    $this->forward($this->moduleName,$this->actionName);//paymentModeOption

            }
        }else
        {
//            $this->getUser()->getAttributeHolder()->remove('transChargeData');

        }


//        if ($request->isMethod('post')){
//
//            $transArray=array();
//            $i=0;
//            foreach($PayModeDetail as $val)
//            {
//                $finName="finInstCharge".$val['id'];
//                $PayName="PayForCharge".$val['id'];
//                if($request->getParameter($finName) || $request->getParameter($PayName))
//                {
//                    $transArray[$i]['PayModName']=$val['name'];
//                    $transArray[$i]['PayModId']=$val['id'];
//                    $transArray[$i]['finInstCharge']=(float)$request->getParameter($finName);
//                    $transArray[$i]['PayForCharge']=(float)$request->getParameter($PayName);
//                    $i++;
//                }
//            }
//
//            $this->getUser()->setAttribute('transChargeData', $transArray);
//
//            if(in_array(1,$PayModeOptArr)){
//                $this->redirect('merchantWizard/choosebank');
//            }else{
//                $this->redirect('merchantWizard/consolidationAccSetup');
//            }
//
//        }

        $this->setTemplate('transactionCharges');
    }



    public function createSandBoxTransCharge($PayModeDetail){
        foreach($PayModeDetail as $val){
            if($val['payment_mode_id'] == 1){
                $transArray[] = array('PayModName' => $val['name'], 'PayModId' => $val['id'], 'finInstCharge' => 100, 'PayForCharge' => 150 );
            }else{
                $transArray[] = array('PayModName' => $val['name'], 'PayModId' => $val['id'], 'finInstCharge' => 0, 'PayForCharge' => 150 );
            }
        }

        $this->getUser()->setAttribute('transChargeData', $transArray);
    }

// Main function for crating New Merchant Service Service

    public function executeMerchantServiceCharges(sfWebRequest $request)
    {

        if(sfconfig::get('sf_environment') == "sandbox"){
            $merchantSvcChg = array('service_charge_percent' => 2, 'upper_slab' => 1000, 'lower_slab' => 100);
            $this->getUser()->setAttribute('merchantServiceCharges', $merchantSvcChg);
            $this->redirect('merchantWizard/paymentModeOption');
        }

        $SelectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $SelectedPayMode = $this->getUser()->getAttribute('payment_mode_option', 0);

        $getCharges = $this->getUser()->getAttribute('merchantServiceCharges','0');

        if($this->isAllModeDone($getCharges))
              $this->redirect($this->moduleName.'/transactionCharges');

        $this->MerchantServiceCharges = new MerchantServiceChargesForWiz(null,array('currency'=>$SelectedCurr ,'paymentMode'=>$SelectedPayMode ));

        if ($request->isMethod('post'))
        {
            //// print_r($request->getParameter('merchant_service_charges'));die;

            $this->MerchantServiceCharges->bind($request->getParameter('merchant_service_charges'));
            $merchantServiceCharge = $request->getPostParameters();

            if ($this->MerchantServiceCharges->isValid() && $request->getParameter('save')) {
                $getMerchantSvcCharges = $this->getUser()->getAttribute('merchantServiceCharges');
                $merchantSvcChg = $request->getParameter('merchant_service_charges');

                $paramHolder = $request->getParameterHolder()->getAll();
                $merchantSvcChg['currency']=$paramHolder['currency'];
                $merchantSvcChg['paymentMode']=$paramHolder['paymentMode'];

                //For removing the lower slab validation in service charges.

                /* if($merchantServiceCharge['merchant_service_charges']['lower_slab']>$merchantServiceCharge['merchant_service_charges']['upper_slab'])
                {
                    $this->getUser()->setFlash('error', sprintf('lower slab must be less than or equal to upper slab'));
                } */
               // else{
                    $chkStatus=false;
                    if($getMerchantSvcCharges)
                        $chkStatus = $this->my_array_search(array('currency'=>$merchantSvcChg['currency'],'paymentMode'=>$merchantSvcChg['paymentMode']),$getMerchantSvcCharges,array('currency','paymentMode'));

                    if($chkStatus)
                        $this->getUser()->setFlash('error', sprintf('Merchant Service Charges for Currency '.$SelectedCurr[$merchantSvcChg['currency']].' and  Payment Mode '.$SelectedPayMode[$merchantSvcChg['currency']][$merchantSvcChg['paymentMode']].' already defined'));
                    else{

                        unset($merchantSvcChg['id']);
                        unset($merchantSvcChg['_csrf_token']);
                        $getMerchantSvcCharges[] = $merchantSvcChg;
                        $this->getUser()->setAttribute('merchantServiceCharges', $getMerchantSvcCharges);
                        $this->getUser()->setFlash('notice', sprintf('Merchant Service Charges has been added for Currency '.$SelectedCurr[$merchantSvcChg['currency']].' and  Payment Mode '.$SelectedPayMode[$merchantSvcChg['currency']][$merchantSvcChg['paymentMode']]));
                        if($this->isAllModeDone($getMerchantSvcCharges))
                            $this->redirect($this->moduleName.'/transactionCharges');
                    }
                    $request->setParameter('save', false);
                    $getMerchantSvcCharges = $this->getUser()->getAttribute('merchantServiceCharges');
                    $this->forward($this->moduleName,$this->actionName);//paymentModeOption
                    // $this->redirect('merchantWizard/transactionCharges');
                //}
            }
        }

    }

    public function isAllModeDone($getCharges){
        if(!is_array($getCharges))
            return false;
        $frwdChk = $getCharges;
        $PayMode = $this->getUser()->getAttribute('payment_mode_option', 0);

        foreach($frwdChk as $val)
          {
             if(isset($PayMode[$val['currency']][$val['paymentMode']]))
                unset($PayMode[$val['currency']][$val['paymentMode']]);
             if(count($PayMode[$val['currency']])==0)
                unset($PayMode[$val['currency']]);
          }
        if(isset($PayMode) && count($PayMode)==0)
            return true;
         else
            return false;

    }

   public function executeGetPaymentMode(sfWebRequest $request)
    {
        $SelectedPayMode = $this->getUser()->getAttribute('payment_mode_option', 0);

        $currency = $request->getParameter('currency');
        $getSession = $request->getParameter('getSession');
        if($getSession!='')
          {  $Session = $this->getUser()->getAttribute($getSession,'0');
               if(is_array($Session))
                {
                    foreach($Session as $sessVal)
                    {
                        if(isset($SelectedPayMode[$sessVal['currency']][$sessVal['paymentMode']]))
                            unset($SelectedPayMode[$sessVal['currency']][$sessVal['paymentMode']]);
                        if(count($SelectedPayMode[$sessVal['currency']])==0)
                            unset($SelectedPayMode[$sessVal['currency']]);
                    }
                }
          }
//print_r($SelectedPayMode);
        $str = '';
        foreach($SelectedPayMode[$currency] as $key=>$value)
            {
//                 if(!(is_array($Session) && isset($Session[$currency][$key])))
                    $str .= '<option value="'.$key.'">'.$value.'</option>';
            }

        return $this->renderText($str);
    }
   public function executeGetUpdatedPaymentMode(sfWebRequest $request)
    {
        $MerchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');
        $paymentModeObj =  Doctrine::getTable('ServicePaymentModeOption')->getPaymentOptions($MerchantServiceId);
        $this->paymentModeArray = Array();
        foreach($paymentModeObj as $val)
        {
           $paymentModeArray[$val->getCurrencyId()][$val->getPaymentModeOptionId()] = $val->getPaymentModeOptionId();

        }

        $paymentModes    = Doctrine::getTable('PaymentModeOption')->getPaymentModeOptionArr();

        $currency = $request->getParameter('currency');
        $str = '';
        foreach($paymentModes as $key=>$value)
         {
            if(isset($paymentModeArray[$currency]) && in_array($key,$paymentModeArray[$currency]))
                $str .= '<option value="'.$key.'" selected=selected>'.$value.'</option>';
             else
                $str .= '<option value="'.$key.'">'.$value.'</option>';

         }

        return $this->renderText($str);
    }
   public function my_array_search($needle, $haystack_array, $searchCriteria){
        if(!is_array($needle) || !is_array($haystack_array) || !is_array($searchCriteria))
        die('$needle and $haystack_array are mandatory for function my_array_search()');
        foreach($haystack_array as $getNeedle)
          {
              $chkStatus=false;
              foreach($searchCriteria as $val)
              {

                if($getNeedle[$val] !=$needle[$val] )
                    $chkStatus=true;
              }
             if(!$chkStatus)
              {
                  return true;
                  break;
               }
          }

         return false;
    }
    public function executePaymentModeOption(sfWebRequest $request) {

        //checking direct hit
        $this->checkIsSessionSet(5);
        $this->merchantMapped = 0;
        //if merchant already exists
         $this->selectedCategory='';
         if ($this->getUser()->getAttribute('merchantId', '0')) {
                $merchanId = $this->getUser()->getAttribute('merchantId', '0');
                $this->selectedCategory='';
                $chkMerchantMapping = Doctrine::getTable('InterswitchMerchantCategoryMapping')->MerchantExists($merchanId);

                //if merchant mapped with interswitch category
                if ($chkMerchantMapping->count()) {
                    $this->merchantMapped = 1;
                    $this->selectedCategory=Doctrine::getTable('InterswitchMerchantCategoryMapping')->findBy('merchant_id',$merchanId)->getLast()->getInterswitchCategoryId();

                }

//                echo "<pre>";
//                print_r($this->selectedCategory);
//                exit;
            }
        $this->selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $paymentModeArr = $this->getUser()->getAttribute('payment_mode_option', 0);

        $this->redirectTemplate = 'merchantServiceCharges';

        //////////////////// Payment Mode Box /////////////////////////////////////
        $paymentModeArray = Doctrine::getTable('PaymentModeOption')->getPaymentModeOptionArr();
        $selPaymentModeOptionChoices = array('' => 'Select Payment Mode Options');
        $this->payment_mode_options = $selPaymentModeOptionChoices + $paymentModeArray;

        $interswitchCategoryArr = Doctrine::getTable('InterswitchCategory')->getAllCAtegories();
        $selInterswitchCategoryOptionChoices = array('' => 'Select Interswitch Category');
        $this->interswitch_category_options = $selInterswitchCategoryOptionChoices + $interswitchCategoryArr;

        //////////////////////////////////////////////////////////////////////////
        if ('getNext' == $request->getParameter('task')) {

            if ($paymentModeArr)
                $this->redirect($this->getModuleName() . '/merchantServiceCharges');
            else {
                $this->getUser()->setFlash('error', sprintf('Payment Mode Option must be added'));
                $this->redirect($this->getModuleName() . '/paymentModeOption');
            }
        }
        if ($request->isMethod('post') && $request->getParameter('save')) {

            if (!$paymentModeArr)
                unset($paymentModeArr);

            $paymentModeOption = $request->getParameter('payment_mode');

            $interswitchcategory = $request->getParameter('interswitch_category');
            if ($this->merchantMapped) {
//                $merchanId = $this->getUser()->getAttribute('merchantId', '0');
//                $chkMerchantMapping = Doctrine::getTable('InterswitchMerchantCategoryMapping')->MerchantExists($merchanId);
                //if merchant mapped with interswitch category
                if ($chkMerchantMapping->count()) {
                    $chkMerchantMapping = $chkMerchantMapping->toArray();
                    $chkMerchantMapping = array('0' => $chkMerchantMapping['0']['interswitch_category_id']);
                    $this->getUser()->setAttribute('category', $chkMerchantMapping);
                    $this->merchantMapped = 1;
                }elseif (($interswitchcategory[0] != '')) {
                    $this->getUser()->setAttribute('category', $interswitchcategory);
                }
            } else
            //Interswitch is seleted as  payment mode option
            if (($interswitchcategory[0] != '')) {

                $this->getUser()->setAttribute('category', $interswitchcategory);
            } else {
                $this->getUser()->setAttribute('category', 0);
            }
            $currency = $request->getParameter('currency');
            if (count($paymentModeOption) > 0) {
                $i = 0;
                foreach ($paymentModeOption as $payMod) {
                    if ($payMod) {
                        $payModObj = Doctrine::getTable('PaymentModeOption')->find($payMod);
                        $getCurrPaymentModeArr[$payModObj->getId()] = $payModObj->getPaymentMode()->getDisplayName();
                        $i++;
                    }
                }
               if (isset($getCurrPaymentModeArr)) {
                    if(in_array(sfConfig::get('app_interswitch'), $getCurrPaymentModeArr) && ($interswitchcategory['0']== '') && $this->merchantMapped==0){
                        $this->getUser()->setFlash('error', sprintf('Please select the Payment Mode Option'));
                    }else{
                         $paymentModeArr[$currency] = $getCurrPaymentModeArr;
                    $this->getUser()->setAttribute('payment_mode_option', $paymentModeArr);

                    $this->getUser()->setFlash('notice', sprintf('Payment Mode Option has been added for ' . $this->selectedCurr[$currency]));
                    }

                } else {
                    $this->getUser()->setFlash('error', sprintf('Please select the Payment Mode Option'));
                }
                $this->getUser()->getAttributeHolder()->remove('merchantServiceCharges');

                if (count($this->selectedCurr) == count($paymentModeArr))
                    $this->redirect($this->getModuleName() . '/merchantServiceCharges');
            }
            else {
                $this->getUser()->setFlash('error', sprintf('Please select the Payment Mode Optihhhon'));
            }
            $request->setParameter('save', false);
        } else {
            $this->getUser()->getAttributeHolder()->remove('payment_mode_option');
        }
        if (isset($paymentModeArr) && $paymentModeArr)
            $this->selectedCurr = array_diff_key($this->selectedCurr, $paymentModeArr);
    }
    public function executeNoofvalidrules(sfWebRequest $request){

        //checking direct hit
        $this->checkIsSessionSet(3);

        $this->getUser()->getAttributeHolder()->remove('noOfValidRules');

        $this->noOfValidRulesForm = new NoOfValidRulesForm();

        if($request->isMethod('post')){
            $this->noOfValidRulesForm->bind($request->getParameter('noOfValidRules'));
            if ($this->noOfValidRulesForm->isValid()) {

                $noOfValidRules_params = $request->getPostParameters();

                $noOfValidRules = $noOfValidRules_params['noOfValidRules']['no_of_rules'];
                $this->getUser()->setAttribute('noOfValidRules', $noOfValidRules);
                if($noOfValidRules)
                    $this->redirect($this->getModuleName().'/setvalidrules');
                else
                    $this->redirect($this->getModuleName().'/chooseCurrnecy');

            }
        }
    }


    public function executeSetvalidrules(sfWebRequest $request){

        //checking direct hit
        $this->checkIsSessionSet(4);

        $this->getUser()->getAttributeHolder()->remove('validationRules');

        $this->noOfValidRules = $this->getUser()->getAttribute('noOfValidRules', '0');
        //$this->notice = $this->getFlash('validRulesnotice');
        if($request->isMethod('post')){
            $validRules_params = $request->getPostParameters();

            if(count($validRules_params) > 0){
                $validationRulesArr = array();
                $totalRulesEntered = 0;
                for($i=1;$i<=$this->noOfValidRules;$i++){
                    if(isset($validRules_params['validationRules']['param_name'.$i]) && $validRules_params['validationRules']['param_name'.$i] != '' &&  isset($validRules_params['validationRules']['param_mandatory'.$i]) && $validRules_params['validationRules']['param_mandatory'.$i] != '' && isset($validRules_params['validationRules']['param_type'.$i]) && $validRules_params['validationRules']['param_type'.$i] != ''){

                        if($validRules_params['validationRules']['param_type'.$i] == "integer"){
                            $this->iparamNum++;
                            $mapped_to = "iparam_".$this->numArr[$this->iparamNum];
                        }

                        if($validRules_params['validationRules']['param_type'.$i] == "string"){
                            $this->sparamNum++;
                            $mapped_to = "sparam_".$this->numArr[$this->sparamNum];
                        }

                        if('' == $validRules_params['validationRules']['param_desc'.$i] || NULL == $validRules_params['validationRules']['param_desc'.$i])
                            $desc = $validRules_params['validationRules']['param_name'.$i];
                         else
                            $desc = $validRules_params['validationRules']['param_desc'.$i];
                        $validationRulesArr[] = array($validRules_params['validationRules']['param_name'.$i], $desc, $validRules_params['validationRules']['param_mandatory'.$i], $validRules_params['validationRules']['param_type'.$i],$mapped_to);
                        $this->getUser()->setAttribute('validationRules', $validationRulesArr);
                        $totalRulesEntered++;
                        $this->getUser()->setAttribute('totalRulesEntered', $totalRulesEntered);

                    }
                }

                if($totalRulesEntered == 0){
                    $this->getUser()->setFlash('notice', sprintf('Please enter atleast a Validation Rule to proceed.'));
                    $this->redirect($this->getModuleName().'/noofvalidrules');
                }else{
                    $this->redirect($this->getModuleName().'/chooseCurrnecy');
                }

            }
        }
    }

    public function executeChooseCurrnecy(sfWebRequest $request){

        //checking direct hit
//        $this->checkIsSessionSet(3);

        $this->getUser()->getAttributeHolder()->remove('MCurrency');

        $this->currencyForm = new CurrencyChoiceForm();

        if($request->isMethod('post')){
            $this->currencyForm->bind($request->getParameter('currencyForm'));
            if ($this->currencyForm->isValid()) {

               $currencyValues = $request->getPostParameters();
               $i=0;
               foreach($currencyValues['currencyForm']['currency'] as $val)
               {
                   $currencyObj = Doctrine::getTable('CurrencyCode')->find($val);
                   $currencyCode[$currencyObj->getId()] = $currencyObj->getCurrency();
                   $i++;
               }
               $this->getUser()->setAttribute('MCurrency', $currencyCode);
               $this->redirect($this->getModuleName().'/paymentModeOption');

            }
        }
    }

    public function executeChoosebank(sfWebRequest $request){
        $this->formAction='choosebank';
        $setBank=false;

        $this->bank_id = "";
        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BANK);
        $this->selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        //fix bug 28830
        $arrPaymentMode = $this->getUser()->getAttribute('payment_mode_option');
        if(!empty($arrPaymentMode)){
            foreach($arrPaymentMode as $key=>$value){
                if($value[key($value)]!='Bank Draft' && $value[key($value)]!='Bank' &&  $value[key($value)]!='Cheque' ){
                    $nestedCurrency[] = $key;
                }
            }
        }
        if(!empty($nestedCurrency)){
            foreach($nestedCurrency as $arrNested){
               unset($this->selectedCurr[$arrNested]);
            }
        }
        $this->bank_array = $bank_obj->getBankList();
        $merchantId = $this->getUser()->getAttribute('merchantId', '0');

        if($request->isMethod('post')){

            if( $request->getParameter('save'))
            {
                $bank_params = $request->getPostParameters();
                $bankArr=$bank_params['bank_id'];

                $count=0;
                $bankDeatails = array();

                $getAllBankDetails = $this->getUser()->getAttribute('bankDetails','0');

                if($getAllBankDetails && array_key_exists($bank_params['currency_id'], $getAllBankDetails))
                    $this->getUser()->setFlash('error', sprintf('Banks have been already added for '.$this->selectedCurr[$bank_params['currency_id']]),false);
                else{

                    $selectedBanks=array();
                    $bankMerchantArr = Doctrine::getTable('BankMerchant')->findAllReleatedBanks($merchantId,$bank_params['currency_id']);
                    foreach($bankMerchantArr as $selbank)
                        $selectedBanks[]=$selbank['Bank']['id'];

                    $bank=array();
                    foreach( $bankArr as $bankIdVal){
                        if(''!=$bankIdVal)
                        {
                            $setBank =  true;

                            $bankObj = Doctrine::getTable('Bank')->getBankObjectByAcronym($bankIdVal);
                            $bankId = $bankObj->getId();
                            if(!in_array($bankId,$selectedBanks))
                            {
                                $bankName = $bankObj->getBankName();
                                $bank[$count][0] = $bankId;
                                $bank[$count][1] = $bankName;
                                $bank[$count][2] = $bankIdVal;
                                $count++;
                            }

                        }

                    }

                    if(!$getAllBankDetails)
                        $getAllBankDetails=array();
                    if(isset($bank))
                        $getAllBankDetails[$bank_params['currency_id']] = $bank;
                    else
                     if(!$setBank)
                      $this->getUser()->setFlash('error', sprintf('Please select bank'));

                }

                $this->getUser()->setAttribute('bankDetails', $getAllBankDetails);
                $request->setParameter('save', false);

                if(count($this->selectedCurr)==count($getAllBankDetails))
                    $this->redirect($this->moduleName.'/collectionAccountSetup');
                if(isset($bank)){
                $this->getUser()->setFlash('notice', sprintf('Banks have been added for '.$this->selectedCurr[$bank_params['currency_id']]),false);
                }
                $this->forward($this->moduleName,$this->actionName);
    //            $this->redirect('merchantWizard/collectionAccountSetup');
            }//if save
        }
        else{
            $this->getUser()->getAttributeHolder()->remove('bankDetails');
        }

        if (isset($getAllBankDetails)) {
            $curr = array_intersect_key($this->selectedCurr,$getAllBankDetails);
            foreach($curr as $currency => $value){
                if(array_key_exists($currency,$this->selectedCurr))
                unset($this->selectedCurr[$currency]);
            }
        }

    }




    public function executeCollectionAccountSetup(sfWebRequest $request){

        //checking direct hit
        $this->checkIsSessionSet(7);
        //$this->checkIsSessionSet(9);

        //removing previous session
        $this->getUser()->getAttributeHolder()->remove('bankReceivingAccDetails');
        $this->getUser()->getAttributeHolder()->remove('collectionAccDetails');

        //bank details session
        $selBanks = $this->getUser()->getAttribute('bankDetails', '0');

        $this->selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $rdirectFlag = true;
        foreach($selBanks as $bankRow)
        {
            if(count($bankRow)>0)
                $rdirectFlag =false;

        }
        if($rdirectFlag)
           $this->redirect($this->moduleName.'/consolidationAccSetup');

//        To check fo rall collection account
//          foreach($selBanks as $val)
//        {
//            if(array_key_exists($val['currency'], $this->selectedCur))
//        }
        //merchant session
        $merchantDetails = $this->getUser()->getAttribute('merchantData', '0');

        //payment-mode-option session
        $PayModeOptArr= $this->getUser()->getAttribute('payment_mode_option', '0');

        //check for sandbox
        if(sfconfig::get('sf_environment') == "sandbox"){
            $this->createSandBoxCollectionAcc($selBanks,$merchantDetails,$PayModeOptArr);
            $this->redirect('merchantWizard/consolidationAccSetup');
        }

        $this->banks = array();

//        foreach($selBanks as $outerVal){
//
//            foreach($outerVal['banks'] as $val){
//                $this->banks[$val[0]] = $val[1];
//            }
//
//
//        }
        $this->banks = $selBanks;
//        $this->form = new collectionAccountSetupForm(array(),array('banks'=>$this->banks));

//        $this->noOfBanks = count($this->banks);

        if($request->isMethod('post')){
            $this->collectionAcc_params = $request->getPostParameters();
            //echo "<pre>"; print_r($this->collectionAcc_params); die;

            $err = 0;
            //validating all values entered
             for($i=1;$i<$this->collectionAcc_params['noOfBanks'];$i++){
                    if($this->collectionAcc_params['accNo'.$i] == '' || $this->collectionAcc_params['accName'.$i] == '' || $this->collectionAcc_params['sortCode'.$i] == ''){
                    $err++;
                }
//            }
           }

            if($err > 0){
                $this->getUser()->setFlash('error', 'Please fill the complete form.', true) ;
            }else{

                $count=1;
                //setting array for collection accounts
//               foreach($this->selectedCurr as $currKey => $currVal)
//                {
                 $bankReceivingAccDetails=array();
                 $collectionAccDetails=array();
                 for($i=1;$i<$this->collectionAcc_params['noOfBanks'];$i++){
                    $collectionAccDetails[$count] = array('currency' => $this->collectionAcc_params['currencyId'.$count], 'accNo' => $this->collectionAcc_params['accNo'.$count], 'accName' => $this->collectionAcc_params['accName'.$count], 'accType' => 'collection', 'sortCode' => $this->collectionAcc_params['sortCode'.$count], 'bankName' => $this->collectionAcc_params['bank'.$count], 'bankId' => $this->collectionAcc_params['bankId'.$count]);
// Bug id #28936
                    if (array_key_exists(1, $PayModeOptArr[$this->collectionAcc_params['currencyId'.$count]])) {
                        $accountNo = $this->createAccountNumber();
                        $accName = $merchantDetails[0]." ".$this->collectionAcc_params['bank'.$i]." bank receiving account. ";
                        $bankReceivingAccDetails[$count] = array('currency' => $this->collectionAcc_params['currencyId'.$count], 'accNo' => $accountNo, 'accName' => $accName, 'accType' => 'receiving','sortCode' => NULL, 'bankName' => $this->collectionAcc_params['bank'.$count], 'bankId' => $this->collectionAcc_params['bankId'.$count]);
                    }
                    $count++;
                 }
//                }

                $this->getUser()->setAttribute('bankReceivingAccDetails', $bankReceivingAccDetails);
                $this->getUser()->setAttribute('collectionAccDetails', $collectionAccDetails);

                $this->redirect($this->moduleName.'/consolidationAccSetup');
            }
        }
    }




    public function createSandBoxCollectionAcc($selBanks,$merchantDetails,$PayModeOptArr){
        for($i=1, $j=count($selBanks);$i<=$j;$i++){
            $bankIdx = $i-1;
            $collAccountNo = $this->createAccountNumber();
            $sortCode = $this->generateSortCode();
            $collAccName = $merchantDetails[0]." ".$selBanks[$bankIdx][1]." bank collection account. ";
            $collectionAccDetails[$i] = array('accNo' => $collAccountNo, 'accName' => $collAccName, 'accType' => 'collection','sortCode' => $sortCode, 'bankName' => $selBanks[$bankIdx][1], 'bankId' => $selBanks[$bankIdx][0]);

            if (in_array(1, $PayModeOptArr)) {
                $recAccountNo = $this->createAccountNumber();
                $recAccName = $merchantDetails[0]." ".$selBanks[$bankIdx][1]." bank receiving account. ";
                $bankReceivingAccDetails[$i] = array('accNo' => $recAccountNo, 'accName' => $recAccName, 'accType' => 'receiving','sortCode' => NULL, 'bankName' => $selBanks[$bankIdx][1], 'bankId' => $selBanks[$bankIdx][0]);
            }

        }

        $this->getUser()->setAttribute('bankReceivingAccDetails', $bankReceivingAccDetails);
        $this->getUser()->setAttribute('collectionAccDetails', $collectionAccDetails);

    }



    public function createAccountNumber(){
        $payformemgrObj = payForMeServiceFactory::getService();
        $account_number = $payformemgrObj->generateAccountNumber();
        return $account_number;
    }



    public function generateSortCode(){
        $payformemgrObj = payForMeServiceFactory::getService();
        $sort_code = $payformemgrObj->getRandomNumber();
        return $sort_code;
    }



    public function executeConsolidationAccSetup(sfWebRequest $request){

        //checking direct hit
        $this->checkIsSessionSet(10);

        //removing previous session
        $selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $this->selectedCurr = $selectedCurr;
        $getAllConsolidationAccDetails = $this->getUser()->getAttribute('consolidationAccDetails');

//        if($getAllConsolidationAccDetails)
//             $this->selectedCurr = array_diff_key($this->selectedCurr, $getAllConsolidationAccDetails);
        if(0 == $this->getUser()->getAttribute('merchantId', '0')){


            //check for sandbox
            if(sfconfig::get('sf_environment') == "sandbox"){
                //merchant session
                $merchantDetails = $this->getUser()->getAttribute('merchantData', '0');

                $accParty =  $merchantDetails[0]." party";
                $accName  =  $merchantDetails[0]." account";
                $accountNo = $this->createAccountNumber();
                $sortCode = $this->generateSortCode();
                $consolidationAccDetails[0] = array('accParty' => $accParty, 'accName' => $accName, 'accType' => 'receiving', 'accNo' => $accountNo, 'sortCode' => $sortCode, 'split' => 100, 'splitTypeId' => 2, 'bankName' => 'ibplc bank');

                $this->getUser()->setAttribute('consolidationAccDetails', $consolidationAccDetails);
                $this->redirect('merchantWizard/saveall');
            }


            $this->splitType = array(2 => 'Percentage',1=>'Flat' );
            $this->redirectTemplate = 'saveall';

            if($request->isMethod('post') ){
                if( $request->getParameter('save'))
                {

                    $consolidationAcc_params = $request->getPostParameters();
//  print_r($consolidationAcc_params);

//                    if($getAllConsolidationAccDetails && array_key_exists($consolidationAcc_params['currency'],$getAllConsolidationAccDetails))
//                        $this->getUser()->setFlash('error', sprintf('Receiving (Consolidation) Account  have been already added for '.$selectedCurr[$consolidationAcc_params['currency']]));
//                    else
                    {


                    $consolidationAccDetails = array();
                    for($i=0;$i<=$consolidationAcc_params['totalRec'];$i++){
                        if(isset($consolidationAcc_params['accParty'.$i])){
                            $consolidationAccDetails[$i] = array('currency'=>$consolidationAcc_params['currency'],'accParty' => $consolidationAcc_params['accParty'.$i], 'accName' => $consolidationAcc_params['accName'.$i], 'accType' => 'receiving', 'accNo' => $consolidationAcc_params['accNo'.$i], 'sortCode' => $consolidationAcc_params['sortCode'.$i], 'split' => $consolidationAcc_params['split'.$i], 'splitTypeId' => $consolidationAcc_params['splitType'], 'bankName' => $consolidationAcc_params['bankName'.$i]);
                        }
                    }

                     $getAllConsolidationAccDetails[$consolidationAcc_params['currency']] = $consolidationAccDetails;

                     $this->getUser()->setAttribute('consolidationAccDetails', $getAllConsolidationAccDetails);
                     if(count($selectedCurr)==count($getAllConsolidationAccDetails))
                        $this->redirect('merchantWizard/saveall');
                     $request->setParameter('save', false);
                     $this->getUser()->setFlash('notice', sprintf('Receiving (Consolidation) Account  have been added for '.$selectedCurr[$consolidationAcc_params['currency']]),false);
                     //$this->forward($this->moduleName,$this->actionName);
                      $this->redirect('merchantWizard/saveall');

                    }//else consolidation
                  }//else save
            }//else post
            else{
                $this->getUser()->getAttributeHolder()->remove('consolidationAccDetails');
            }
        }else{
             $this->redirect('merchantWizard/saveall');
 //                $consolidationAccDetails = "No Detail found";

        }
    }



    public function executeSaveall(sfWebRequest $request){
        //checking direct hit
        $this->checkIsSessionSet(10);

        $this->merchantDetails = $this->getUser()->getAttribute('merchantData', '0');
        $this->merchantServiceDetails = $this->getUser()->getAttribute('merchantServiceData', '0');
        $this->validationRulesDetails = $this->getUser()->getAttribute('validationRules', '0');
        $this->currency = $this->getUser()->getAttribute('MCurrency', 0);
        $this->paymentMode = $this->getUser()->getAttribute('payment_mode_option', 0);
        $this->interswitchCategory = $this->getUser()->getAttribute('category', 0);
        $bankDetails = $this->getUser()->getAttribute('bankDetails', '0');
        if($bankDetails == 0){
            $this->bankDetails = array();
        }else{
            $this->bankDetails = $bankDetails;
        }

        $this->merchantServiceChrgData = $this->getUser()->getAttribute('merchantServiceCharges', '0');
        $this->transChrgData = $this->getUser()->getAttribute('transChargeData', '0');
        $this->collectionAcctDetials =$this->getUser()->getAttribute('collectionAccDetails', '0');
        $this->bankReceivingAccDetails =$this->getUser()->getAttribute('bankReceivingAccDetails', '0');
        $this->consolidationAccDetails =$this->getUser()->getAttribute('consolidationAccDetails', '0');

        if($request->isMethod('post')){
            $save_params = $request->getPostParameters();
            if($save_params['save'] == "Y"){
                $generateSql='';
                //adding new merchant
                $merchantId = $this->getUser()->getAttribute('merchantId', '0');
                $userId=$this->getUser()->getGuardUser()->getId();

                if($merchantId == 0){
                    $merchantobj=new Merchant();
                    $merchantobj->setName($this->merchantDetails[0]);
                    $merchantobj->setMerchant_code($this->merchantDetails[1]) ;
                    $merchantobj->setMerchant_key($this->merchantDetails[2]);
                    $merchantobj->setAuth_info($this->merchantDetails[3]);
                    $merchantobj->setCategory_id($this->merchantDetails[4]);
                    

//                    $merchantobj->setEwallet_user_id($userId);
                    $generateSql.="\n";
                    $generateSql.="INSERT INTO `merchant` (`id`,`category_id`, `name`, `merchant_code`, `merchant_key`, `auth_info`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`)
                                    VALUES (MERCHANT_ID, '".$this->merchantDetails[4]."', '".$this->merchantDetails[0]."', '".$this->merchantDetails[1]."', '".$this->merchantDetails[2]."', '".$this->merchantDetails[3]."', now(), now(), NULL, ".$userId.", ".$userId.");";
                     $bankCount=0;
                    foreach($this->bankDetails as $currnecyKey=>$allbankVal){
                        foreach($allbankVal as $bankVal)
                        {
                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `bank_merchant` (`bank_id`, `merchant_id`, `merchant_account`, `status`,`currency_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`)
                                            VALUES(".$bankVal[0].", MERCHANT_ID, '0', '1', ".$currnecyKey.", now(), now(), NULL, ".$userId.", NULL);";
                            $bankMerchantobj=$merchantobj->BankMerchant[$bankCount];
                            $bankMerchantobj->setBankId($bankVal[0]);
                            $bankMerchantobj->setCurrencyId($currnecyKey);
                            ++$bankCount;
                        }
                    }

                    $merchantServiceObj=$merchantobj->MerchantService[0];

                }

                else
                {
                    $merchantServiceObj=new MerchantService();
                    $merchantServiceObj->setMerchant_id($merchantId);
                    $bank_arr=$this->bankDetails;
                    $bankCount=0;
                     foreach($this->bankDetails as $currnecyKey=>$allbankVal){
                        foreach($allbankVal as $bankVal)
                        {
                            $getBankDetails=Doctrine::getTable('BankMerchant')->chkBankMerchantStatus($merchantId,$bankVal[0],$currnecyKey);
                            if(!$getBankDetails)
                            {
                                $generateSql.="\n";
                                $generateSql.="INSERT INTO `bank_merchant` (`bank_id`, `merchant_id`, `merchant_account`, `status`,    `currency_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`)
                                                VALUES(".$bankVal[0].", MERCHANT_ID, '0', '1', ".$currnecyKey.", now(), now(), NULL, ".$userId.", NULL);";
                                $bankMerchantobj= new BankMerchant();
                                $bankMerchantobj->setMerchant_id($merchantId);
                                $bankMerchantobj->setBank_id($bankVal[0]);
                                $bankMerchantobj->setCurrencyId($currnecyKey);
                                $bankMerchantobj->save();

                            }
                        }
                    }
                }
                //adding merchant service


                $merchantServiceObj->setName($this->merchantServiceDetails['name']) ;
                $merchantServiceObj->setNotification_url($this->merchantServiceDetails['notification_url']);
                $merchantServiceObj->setMerchant_home_page($this->merchantServiceDetails['merchant_home_page']);
                $merchantServiceObj->setVersion($this->merchantServiceDetails['version']);

                $generateSql.="\n";
                $generateSql.="INSERT INTO `merchant_service` (`id`, `merchant_id`, `name`, `notification_url`, `merchant_home_page`, `version`, `clubbed`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`)
                                VALUES (MERCHANT_SERVICE_ID, MERCHANT_ID, '".$this->merchantServiceDetails['name']."', '".$this->merchantServiceDetails['notification_url']."', '".$this->merchantServiceDetails['merchant_home_page']."', '".$this->merchantServiceDetails['version']."', 'no', now(), now(), ".$userId.", NULL, NULL);";
                $mscchrgs=0;
                if($this->merchantServiceChrgData)
                {
                    foreach($this->merchantServiceChrgData as $merchantServiceChrgsVal)
                    {
                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `merchant_service_charges` (`merchant_service_id`, `payment_mode_option_id`, `currency_id`, `service_charge_percent`, `upper_slab`, `lower_slab`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`)
                                        VALUES (MERCHANT_SERVICE_ID, ".$merchantServiceChrgsVal['paymentMode'].", ".$merchantServiceChrgsVal['currency'].", ".$merchantServiceChrgsVal['service_charge_percent'].", ".$merchantServiceChrgsVal['upper_slab'].", ".$merchantServiceChrgsVal['lower_slab'].", now(), now(), ".$userId.", NULL, NULL);";
                        $merchantServiceChargeObj[$mscchrgs]=$merchantServiceObj->MerchantServiceCharges[$mscchrgs];
                        $merchantServiceChargeObj[$mscchrgs]->service_charge_percent  = $merchantServiceChrgsVal['service_charge_percent'];
                        $merchantServiceChargeObj[$mscchrgs]->upper_slab 	           = $merchantServiceChrgsVal['upper_slab'];
                        $merchantServiceChargeObj[$mscchrgs]->lower_slab 	           = $merchantServiceChrgsVal['lower_slab'];
                        $merchantServiceChargeObj[$mscchrgs]->currency_id 	           = $merchantServiceChrgsVal['currency'];
                        $merchantServiceChargeObj[$mscchrgs]->payment_mode_option_id  = $merchantServiceChrgsVal['paymentMode'];
                        $mscchrgs++;
                    }
                }
                $pfmHelperObj = new pfmHelper();
                $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
                $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                $paymentModeBankId = $pfmHelperObj->getPMOIdByConfName('bank');


                $counterVal = count($this->transChrgData);
                if($counterVal>0 && is_array($this->transChrgData))
                {
                    for($i=0, $j=$counterVal;$i<$j;$i++)
                    {
                        //unset($transactionDetailObj);

                        $transactionDetailObj[$i]=$merchantServiceObj->TransactionCharges[$i];

    //                    $transactionDetailObj[$i]->payment_mode_option_id 	 = $this->transChrgData[$i]['PayModId'] ;
                       if($paymentModeBankId==$this->transChrgData[$i]['paymentMode']||$paymentModeDraftId==$this->transChrgData[$i]['paymentMode']||$paymentModeCheckId==$this->transChrgData[$i]['paymentMode'])
                          {
                              $transactionDetailObj[$i]->financial_institution_charges 	 = $this->transChrgData[$i]['finInstCharge'];
                              $finCharges=$this->transChrgData[$i]['finInstCharge'];
                          }
                          else
                             $finCharges=0;
                        $transactionDetailObj[$i]->payforme_charges 	 = $this->transChrgData[$i]['PayForCharge'];
                        $transactionDetailObj[$i]->currency_id 	           = $this->transChrgData[$i]['currency'];
                        $transactionDetailObj[$i]->payment_mode_option_id  = $this->transChrgData[$i]['paymentMode'];
                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `transaction_charges` (`merchant_service_id`, `payment_mode_option_id`,`currency_id`, `financial_institution_charges`, `payforme_charges`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`)
                                        VALUES (MERCHANT_SERVICE_ID, ".$this->transChrgData[$i]['paymentMode'].", ".$this->transChrgData[$i]['currency'].", ".$finCharges.", ".$this->transChrgData[$i]['PayForCharge'].", now(), now(), ".$userId.", NULL, NULL);";

                    }
                }
                $this->merchantServiceObj=$merchantServiceObj;
                if($this->validationRulesDetails)
                {
                    $counterVal = count($this->validationRulesDetails);
                    for($i=0, $j=$counterVal;$i<$j;$i++)
                    {
                        //unset($validationRules);
                        $validationRules[$i]=$this->merchantServiceObj->ValidationRules[$i];
                        // $validationRules->merchant_service_id = $this->merchantServiceId;
                        $validationRules[$i]->param_name 	 =$this->validationRulesDetails[$i][0] ;
                        $validationRules[$i]->param_description 	 =$this->validationRulesDetails[$i][1];
                        if($this->validationRulesDetails[$i][2]=='yes')
                           {
                               $validationRules[$i]->is_mandatory 	 = '1';
                               $mendatory=1;
                           }
                         else
                            $mendatory='NULL';
                        $validationRules[$i]->param_type 	 = $this->validationRulesDetails[$i][3];
                        $validationRules[$i]->mapped_to 	 = $this->validationRulesDetails[$i][4];
                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `validation_rules` (`merchant_service_id`, `param_name`, `param_description`, `is_mandatory`, `param_type`, `mapped_to`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`)
                                        VALUES (MERCHANT_SERVICE_ID, '".$this->validationRulesDetails[$i][0]."', '".$this->validationRulesDetails[$i][1]."', ".$mendatory.", '".$this->validationRulesDetails[$i][3]."', '".$this->validationRulesDetails[$i][4]."', now(), now(), ".$userId.", NULL, NULL);";
                    }
                }

                if($merchantId == 0)
                {

                    $merchantobj->save();
                    $saveMerchantId =$merchantobj->id;
                    if($this->interswitchCategory['0'])
                    Doctrine::getTable('InterswitchMerchantCategoryMapping')->updateRequest($saveMerchantId,$this->interswitchCategory['0'],true);
                }

                else
                {
                    $merchantServiceObj->save();
                    $saveMerchantId =$merchantServiceObj->merchant_id;
                   if($this->interswitchCategory)
                    {
                    Doctrine::getTable('InterswitchMerchantCategoryMapping')->updateRequest($saveMerchantId,$this->interswitchCategory['0'],false);
                    }
                    }
                $merchantServiceId=$merchantServiceObj->id;
                ///////////////////Payment Mode Option //////////////////


                $pfmHelperObj = new pfmHelper();
                $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
                $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                $paymentModeBankId = $pfmHelperObj->getPMOIdByConfName('bank');

                $PayModeOptArr= $this->getUser()->getAttribute('payment_mode_option', '0');
                $bankPay = false;
                foreach($PayModeOptArr as $currnecyKey=>$allPaymodVal){
                  foreach($allPaymodVal as $paymodKey => $paymodVal)
                   {
                    if($paymentModeBankId==$paymodKey || $paymentModeCheckId== $paymodKey|| $paymentModeDraftId==$paymodKey )
                        $bankPay = true;
                    $generateSql.="\n";
                    $generateSql.="INSERT INTO `service_payment_mode_option` ( `merchant_service_id`, `payment_mode_option_id`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`)
                                    VALUES (MERCHANT_SERVICE_ID, ".$paymodKey.", now(), now(), null, ".$userId.", NULL);";
                    $paymentModeVal=new ServicePaymentModeOption();
                    $paymentModeVal->setMerchant_service_id($merchantServiceId);
                    $paymentModeVal->setPayment_mode_option_id($paymodKey);
                    $paymentModeVal->setCurrency_id($currnecyKey);
                    $paymentModeVal->save();
                   }

                }
                ///////////////////Payment Mode Option //////////////////
                ////////////////////////// Set up For  Accounting Start ////////////////////////
                if($bankDetails != 0 && $bankPay ){


                    ///////////////collection account detail ////////////////
                    $collectionActDetail = $this->getUser()->getAttribute('collectionAccDetails', '0');
                    if(is_array($collectionActDetail))
                    foreach($collectionActDetail as $collectionVal){

                        $EpMasterAccountObj=new EpMasterAccount();
                        $EpMasterAccountObj->setAccount_number($collectionVal['accNo']);
                        $EpMasterAccountObj->setAccount_name($collectionVal['accName']);
                        $EpMasterAccountObj->setType($collectionVal['accType']);
                        $EpMasterAccountObj->setSortcode($collectionVal['sortCode']);
                        $EpMasterAccountObj->setBankname($collectionVal['bankName']);
                        $EpMasterAccountObj->save();
                        $EpMasterAccountId=$EpMasterAccountObj->id;

                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `ep_master_account` (`id`,  `account_number`, `account_name`, `sortcode`, `bankname`, `type`, `created_at`, `updated_at`, `created_by`, `updated_by`)
                                        VALUES (".$EpMasterAccountId.", '".$collectionVal['accNo']."', '".$collectionVal['accName']."', '".$collectionVal['sortCode']."', '".$collectionVal['bankName']."', '".$collectionVal['accType']."', now(), now(), ".$userId.", NULL);";

                     foreach($PayModeOptArr as $currnecyKey=>$allPaymodVal){
                        foreach($allPaymodVal as $paymodKey => $paymodVal)
                        {

                         if($paymentModeBankId==$paymodKey || $paymentModeCheckId== $paymodKey|| $paymentModeDraftId==$paymodKey ){
                        $ServiceBankConfigObj=new ServiceBankConfiguration();
                        if($merchantId == 0)
                            $ServiceBankConfigObj->setMerchant_id($saveMerchantId);
                        else
                            $ServiceBankConfigObj->setMerchant_id($merchantId);
                        $ServiceBankConfigObj->setPayment_mode_option_id($paymodKey);
                        $ServiceBankConfigObj->setBank_id($collectionVal['bankId']);
                        $ServiceBankConfigObj->setMaster_account_id($EpMasterAccountId);
                        $ServiceBankConfigObj->setTransfer_type('Payment');
                        $ServiceBankConfigObj->setCurrency_id($collectionVal['currency']);
                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `service_bank_configuration` ( `merchant_id`, `payment_mode_option_id`, `bank_id`, `master_account_id`, `transfer_type`, `currency_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`)
                                        VALUES ( MERCHANT_ID, ".$paymodKey.", ".$collectionVal['bankId'].", ".$EpMasterAccountId.", 'Payment', ".$collectionVal['currency'].", now(), now(), ".$userId.", NULL, NULL);";
                        $ServiceBankConfigObj->save();
                         }
                   }}

                    }
//foreach for collection account
                    ///////////////collection account detail ////////////////

                    ///////////////recieving  account detail ////////////////
                    $recievingActDetail = $this->getUser()->getAttribute('bankReceivingAccDetails', '0');

                   if(is_array($recievingActDetail))
                    foreach($recievingActDetail as $recievingVal){
                        $EpMasterAccountObj=new EpMasterAccount();
                        $EpMasterAccountObj->setAccount_number($recievingVal['accNo']);
                        $EpMasterAccountObj->setAccount_name($recievingVal['accName']);
                        $EpMasterAccountObj->setType($recievingVal['accType']);
                        $EpMasterAccountObj->setSortcode($recievingVal['sortCode']);
                        $EpMasterAccountObj->setBankname($recievingVal['bankName']);
                        $EpMasterAccountObj->save();
                        $EpMasterAccountId=$EpMasterAccountObj->id;
                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `ep_master_account` (`id`,  `account_number`, `account_name`, `sortcode`, `bankname`, `type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
                                        (".$EpMasterAccountId.", '".$recievingVal['accNo']."', '".$recievingVal['accName']."', '".$recievingVal['sortCode']."', '".$recievingVal['bankName']."', '".$collectionVal['accType']."', now(), now(), ".$userId.", NULL);";

                        $FinInstActConfigObj=new FinancialInstitutionAcctConfig();
                        if($merchantId == 0)
                            $FinInstActConfigObj->setMerchant_id($saveMerchantId);
                        else
                            $FinInstActConfigObj->setMerchant_id($merchantId);

                        $FinInstActConfigObj->setBank_id($recievingVal['bankId']);
                        $FinInstActConfigObj->setAccount_id($EpMasterAccountId);
                        $ServiceBankConfigObj->setCurrency_id($recievingVal['currency']);
                        $FinInstActConfigObj->save();
                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `financial_institution_acct_config` (`merchant_id`, `bank_id`, `account_id`, `currency_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
                                        ( MERCHANT_ID, ".$recievingVal['bankId'].", ".$EpMasterAccountId.", ".$recievingVal['currency'].", now(), now(), ".$userId.", NULL);";


                    } //foreach for receiving 0accoutn

                    ///////////////recieving  account detail ////////////////


                } //// if payment mode is Bank


                ///////////////Consiladation  account detail ////////////////
                if($merchantId == 0)
                { ///for new Merchant
                    $consilationActDetail = $this->getUser()->getAttribute('consolidationAccDetails', '0');
                    foreach($consilationActDetail as $AllConslidateVal){
                       foreach($AllConslidateVal as $conslidateVal){
                        if(count($conslidateVal)>0){


                            $EpMasterAccountObj1=new EpMasterAccount();
                            $EpMasterAccountObj1->setAccount_number($conslidateVal['accNo']);
                            $EpMasterAccountObj1->setAccount_name($conslidateVal['accName']);
                            $EpMasterAccountObj1->setType($conslidateVal['accType']);
                            $EpMasterAccountObj1->setSortcode($conslidateVal['sortCode']);
                            $EpMasterAccountObj1->setBankname($conslidateVal['bankName']);

                            $EpMasterAccountObj1->save();
                            $EpMasterAccountId1=$EpMasterAccountObj1->id;

                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `ep_master_account` (`id`,  `account_number`, `account_name`, `sortcode`, `bankname`, `type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
                                        (".$EpMasterAccountId1.", '".$conslidateVal['accNo']."', '".$conslidateVal['accName']."', '".$conslidateVal['sortCode']."', '".$conslidateVal['bankName']."', '".$conslidateVal['accType']."', now(), now(), ".$userId.", now());";

                            $EpMasterAccountObj2=new EpMasterAccount();
                            $EpMasterAccountObj2->setAccount_number($conslidateVal['accNo']);
                            $EpMasterAccountObj2->setAccount_name($conslidateVal['accName']);
                            $EpMasterAccountObj2->setType($conslidateVal['accType']);
                            $EpMasterAccountObj2->setSortcode($conslidateVal['sortCode']);
                            $EpMasterAccountObj2->setBankname($conslidateVal['bankName']);
                            $EpMasterAccountObj2->save();
                            $EpMasterAccountId2=$EpMasterAccountObj2->id;

                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `ep_master_account` (`id`,  `account_number`, `account_name`, `sortcode`, `bankname`, `type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
                                        (".$EpMasterAccountId2.", '".$conslidateVal['accNo']."', '".$conslidateVal['accName']."', '".$conslidateVal['sortCode']."', '".$conslidateVal['bankName']."', '".$conslidateVal['accType']."', now(), now(), ".$userId.", now());";

                            $VirtualActConfigObj=new VirtualAccountConfig();
                            $VirtualActConfigObj->setVirtual_account_id($EpMasterAccountId1);
                            $VirtualActConfigObj->setPhysical_account_id($EpMasterAccountId2);
                            $VirtualActConfigObj->setPayment_flag(0);
                            $VirtualActConfigObj->save();


                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `virtual_account_config` ( `virtual_account_id`, `physical_account_id`, `payment_flag`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
                                            ( ".$EpMasterAccountId1.", ".$EpMasterAccountId2.", '0', now(), now(), ".$userId.", ".$userId.");";

                            $SplitActConfigObj=new SplitAccountConfiguration();
                            $SplitActConfigObj->setAccount_party($conslidateVal['accParty']);
                            $SplitActConfigObj->setAccount_name($conslidateVal['accName']);
                            $SplitActConfigObj->setAccount_number($conslidateVal['accNo']);
                            $SplitActConfigObj->setSortcode($conslidateVal['sortCode']);
                            $SplitActConfigObj->setBank_name($conslidateVal['bankName']);
                            $SplitActConfigObj->setMaster_account_id($EpMasterAccountId1);
                            $SplitActConfigObj->setCurrency_id($conslidateVal['currency']);
                            $SplitActConfigObj->save();
                            $SplitActConfigId=$SplitActConfigObj->id;
//Bugi d #28853
                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `split_account_configuration` (id, `account_party`, `account_name`, `account_number`, `sortcode`, `bank_name`, `master_account_id`, `currency_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
                                            ($SplitActConfigId, '".$conslidateVal['accParty']."', '".$conslidateVal['accName']."', '".$conslidateVal['accNo']."', '".$conslidateVal['sortCode']."', '".$conslidateVal['bankName']."', ".$EpMasterAccountId1.", ".$conslidateVal['currency'].", now(), now(), ".$userId.", NULL, NULL);";



                            $VirtualActConfigObj=new SplitEntityConfiguration();
                            $VirtualActConfigObj->setMerchant_service_id($merchantServiceId);
                            $VirtualActConfigObj->setSplit_type_id($conslidateVal['splitTypeId']);
                            $VirtualActConfigObj->setCharge($conslidateVal['split']);
                            $VirtualActConfigObj->setSplit_account_configuration_id($SplitActConfigId);
                            $VirtualActConfigObj->setMerchant_item_split_column('split_one');
                            $VirtualActConfigObj->save();

                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `split_entity_configuration` (`merchant_service_id`, `split_type_id`, `charge`, `split_account_configuration_id`, `merchant_item_split_column`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
                                            ( MERCHANT_SERVICE_ID, ".$conslidateVal['splitTypeId'].", ".$conslidateVal['split'].", ".$SplitActConfigId.", 'split_one', now(), now(), ".$userId.", NULL, NULL);";


                        } //if count
                       }//for internel
                       /////// For Pay4Me  detail ////////////
                            $VirtualActConfigObj=new SplitEntityConfiguration();
                            $VirtualActConfigObj->setMerchant_service_id($merchantServiceId);
                            $VirtualActConfigObj->setSplit_type_id(3);
                            $VirtualActConfigObj->setCharge(0);
                            $VirtualActConfigObj->setSplit_account_configuration_id(1);
                            $VirtualActConfigObj->setMerchant_item_split_column('split_one');
                            $VirtualActConfigObj->save();
                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `split_entity_configuration` (`merchant_service_id`, `split_type_id`, `charge`, `split_account_configuration_id`, `merchant_item_split_column`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
                                            ( MERCHANT_SERVICE_ID, 3, 0, 1, 'split_one', now(), now(), ".$userId.", NULL, NULL);";
                            /////// For Pay4Me  detail ////////////

                            /////// For Financial Instution  detail ////////////
                            $VirtualActConfigObj=new SplitEntityConfiguration();
                            $VirtualActConfigObj->setMerchant_service_id($merchantServiceId);
                            $VirtualActConfigObj->setSplit_type_id(6);
                            $VirtualActConfigObj->setCharge(0);
                            $VirtualActConfigObj->setMerchant_item_split_column('split_one');
                            $VirtualActConfigObj->save();
                            $generateSql.="\n";
                            $generateSql.="INSERT INTO `split_entity_configuration` (`merchant_service_id`, `split_type_id`, `charge`, `split_account_configuration_id`, `merchant_item_split_column`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
                                            ( MERCHANT_SERVICE_ID, 6, 0, NULL, 'split_one', now(), now(), ".$userId.", NULL, NULL);";
                            /////// For Financial Instution  detail ////////////
                    }  //for

                }    //if New Merchant
                else{
                    $OldService = Doctrine::getTable('MerchantService')->findByMerchantId($merchantId);
                    $AccountConfig = Doctrine::getTable('SplitEntityConfiguration')->findByMerchantServiceId($OldService->getfirst()->getId());

                    foreach($AccountConfig as $resultConfig)
                    {
                        $VirtualActConfigObj=new SplitEntityConfiguration();
                        $VirtualActConfigObj->setMerchant_service_id($merchantServiceId);
                        $VirtualActConfigObj->setSplit_type_id($resultConfig->getSplitTypeId());
                        $VirtualActConfigObj->setCharge($resultConfig->getCharge());
                        $VirtualActConfigObj->setSplit_account_configuration_id($resultConfig->getSplitAccountConfigurationId());
                        $VirtualActConfigObj->setMerchant_item_split_column('split_one');
                        $VirtualActConfigObj->save();
                        $generateSql.="\n";
                        $generateSql.="INSERT INTO `split_entity_configuration` (`merchant_service_id`, `split_type_id`, `charge`, `split_account_configuration_id`, `merchant_item_split_column`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
                                            ( MERCHANT_SERVICE_ID, ".$resultConfig->getSplitTypeId().", ".$resultConfig->getCharge().", ".$resultConfig->getSplitAccountConfigurationId().", 'split_one', now(), now(), ".$userId.", NULL, NULL);";

                    } // for each

                }


                ///////////////Consiladation  account detail ////////////////

                //////////////Logging   ////////////////////////////////////////////////////
                $pay4meLog = new pay4meLog();
                $pay4meLog->createLogData($generateSql,$this->merchantServiceDetails['name'],'MerchantSql');
                $filename = $pay4meLog->getFileName();
                $this->getUser()->setAttribute('fileName', $filename);
                //////////////////////////////////////////////////////////////////////////////
                //
                ////////////////////////// Set up For  Accounting End ////////////////////////
                $this->getUser()->getAttributeHolder()->remove('merchantServiceId');
                $this->getUser()->setAttribute('merchantServiceId', $merchantServiceId);
                //$this->setTemplate('thanks');

                $this->redirect('merchantWizard/thanks');

            }
        }
    }


    public function executeThanks(sfWebRequest $request){

        //checking direct hit
//        $this->checkIsSessionSet(11);

        $merchantServiceId=$this->getUser()->getAttribute('merchantServiceId', '0');
        $serviceObj=Doctrine::getTable('MerchantService')->find($merchantServiceId);
        $this->merchantId = $serviceObj->getMerchant()->getId();
        $merchantServicedetails = $this->getUser()->getAttribute('merchantServiceDataSQL');
        $merchantServiceName = $merchantServicedetails['name'];
        $this->getUser()->setAttribute("merchantServiceNameSQL", $merchantServiceName);
        $this->freezed = $serviceObj->getMerchant()->getIsFreezed();
        /////////////////Generate Sample Order XML Start ////////////////////////////
        $pay4MeXMLV1Obj=new pay4MeXMLV1();
        $xmlDetails=$pay4MeXMLV1Obj->generateSamlpleOrderXml($merchantServiceId);
        //echo "<pre>"; print_r($xmlDetails); die;
        // $request->getParameterHolder()->set('xmlDetails', $xmlDetails);
        /////////////////Generate Sample Order XML End ////////////////////////////

        //$xmlDetails=$request->getParameterHolder()->get('xmlDetails');
        $this->xmlDetails=$xmlDetails;
        //print_r($this->xmlDetails);
        //clearing all session data
        $this->getUser()->getAttributeHolder()->remove('merchantData');
        $this->getUser()->getAttributeHolder()->remove('merchantId');
        $this->getUser()->getAttributeHolder()->remove('merchantServiceData');
        $this->getUser()->getAttributeHolder()->remove('transChargeData');
        $this->getUser()->getAttributeHolder()->remove('merchantServiceCharges');
        $this->getUser()->getAttributeHolder()->remove('noOfValidRules');
        $this->getUser()->getAttributeHolder()->remove('validationRules');
        $this->getUser()->getAttributeHolder()->remove('totalRulesEntered');
        $this->getUser()->getAttributeHolder()->remove('bankDetails');
        $this->getUser()->getAttributeHolder()->remove('bankReceivingAccDetails');
        $this->getUser()->getAttributeHolder()->remove('collectionAccDetails');
        $this->getUser()->getAttributeHolder()->remove('consolidationAccDetails');
        $this->getUser()->getAttributeHolder()->remove('payment_mode_option');
//        $this->getUser()->getAttributeHolder()->remove('merchantServiceId');
    }

    public function executeShowSql(sfWebRequest $request) {
        $this->downloadSql = false;
        if ($this->getUser()->getAttribute("merchantServiceNameSQL")!=''){
            $this->merchantServiceName = $this->getUser()->getAttribute("merchantServiceNameSQL");
            $this->downloadSql = true;
        }
    }

    public function checkIsSessionSet($step){
        $seriesArr = array(1 =>  'merchantData',
            2 =>  'merchantServiceData',
            3 =>  'noOfValidRules',
            4 =>  'validationRules',
            5 =>  'payment_mode_option',
            6 =>  'transChargeData',
            7 =>  'bankDetails',
            8 =>  'collectionAccDetails',
            9 =>  'bankReceivingAccDetails',
            10 => 'consolidationAccDetails',
            11 => 'merchantServiceId');

        $actionArr = array(1 =>  'index',
            2 =>  'createMService',
            3 =>  'noofvalidrules',
            4 =>  'setvalidrules',
            5 =>  'paymentModeOption',
            6 =>  'transactionCharges',
            7 =>  'choosebank',
            8 =>  'collectionAccountSetup',
            9 =>  'collectionAccountSetup',
            10 => 'consolidationAccSetup',
            11 => 'thanks');
if ($step == 10) {
            $payment_mode_options = $this->getUser()->getAttribute('payment_mode_option');

            $payment_mode_ids=current($payment_mode_options);
        }
        $pfmHelperObj = new pfmHelper();
        $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
        $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
        $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');
        for($i=1;$i<=$step-1;++$i){
            if( ((0!=$this->getUser()->getAttribute('merchantId', '0'))
                    && ($i==7 || $i==8 || $i==9 )) || ((0==$this->getUser()->getAttribute('bankDetails', '0'))
                    && ($i==4 )) || ((0==$this->getUser()->getAttribute('noOfValidRules', '0'))
                    && ($i==7 || $i==8 || $i==9 || $i==4)) || ($i==6 ) || ($step == 10 && (array_key_exists($paymentModebankId, $payment_mode_options)
                          ||  array_key_exists($paymentModeCheckId, $payment_mode_options) || array_key_exists($paymentModeDraftId, $payment_mode_options) )
                    ))

//                              if( ((0!=$this->getUser()->getAttribute('merchantId', '0'))
//                    && ($i==7 || $i==8 || $i==9 )) || ((0==$this->getUser()->getAttribute('bankDetails', '0'))
//                    && ($i==4 )) || ((0==$this->getUser()->getAttribute('noOfValidRules', '0'))
//                    && ($i==7 || $i==8 || $i==9 || $i==4)) || ($i==6 ) || ($step == 10 && array_key_exists('1', $payment_mode_options)) )
          {
                continue;
            }
            //echo $i;
            $sessionData = $this->getUser()->getAttribute($seriesArr[$i]);

            if(!isset($sessionData) || $sessionData == ''){

                $this->redirect('merchantWizard/'.$actionArr[$i]);
            }

        }


    }

   public function executeListMerchantWizard(sfWebRequest $request) {
        

        /*
         * bug Id: 28926
         * changed by ryadav
         * array value passed for make merchant search optional
         */
        $pfmHelperObj = new pfmHelper();
        $this->userGroup = $pfmHelperObj->getUserGroup();
        if($this->userGroup == "merchant"){
        	$userId=$this->getUser()->getGuardUser()->getid();
        	$merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
        }
        else {
        	$merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BASE);
        	$this->merchant_service_list = $merchant_service_obj->getAllRecords();
        }
         $this->form = new ChooseMerchantForm(array(),array('merchant_id'=>$merchant_id,'strShowOptionalAll'=>'strShowOptionalAll'));
         $searchMerchantId = 0;
         $is_freezed = 0;
         if ($request->isMethod('post') && $request->getParameter('merchant')){
            $this->form->bind($request->getParameter('merchant'));
            $merchant_params = $request->getPostParameters();
            if ($this->form->isValid()) {
                	$merchant_params = $request->getParameter('merchant');
                	$merchant_id = $merchant_params['merchant_id'];
              
                	if($this->userGroup == "merchant") {
                	$userId=$this->getUser()->getGuardUser()->getid();
                	$merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
                	}
                    $searchMerchantId = $merchant_id;
                    $this->merchant_service_list =Doctrine::gettable('MerchantService')->getServiceObj($merchant_id);
                    $merchantObj =Doctrine::gettable('Merchant')->find($merchant_id);
                    $is_freezed = $merchantObj->getIsFreezed();
                }

            
         }
         else{

                 /*
                 * bug Id: 30033
                 * changed by ryadav
                 * preserve paging merchant serach
                 */

                if($request->getParameter('updatedMerchantId'))
                {//CR046
                    $merchant_id = $request->getParameter('updatedMerchantId');

                }else{
                    $merchant_id = $request->getParameter('merchant_id');
                    if($this->userGroup == "merchant"){
                    	$userId=$this->getUser()->getGuardUser()->getid();
                    	$merchant_id =Doctrine::getTable("MerchantUser")->getMerchantIdFromUserId($userId);
                    }
                }
                $searchMerchantId = $merchant_id;
                $this->merchant_service_list =Doctrine::gettable('MerchantService')->getServiceObj($merchant_id);

                if($request->getParameter('updatedMerchantId')){
                     $merchantObj = Doctrine::gettable('Merchant')->find($merchant_id);
                     $is_freezed = $merchantObj->getIsFreezed();
                    $this->getUser()->setFlash('notice', sprintf('A new  merchant '.$merchantObj->getName().' has been created. Please continue to edit.'));
                }
                 $this->form->setDefaults(array(
                    'merchant_id' => $merchant_id
                    ));


         }
        $this->MerchantId = $searchMerchantId;
        $this->isFreezed = $is_freezed;
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('MerchantService',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->merchant_service_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
  public function executeMerchantSql(sfWebRequest $request) {
      $merchantId = $request->getParameter('merchantId');
      $merchantObj = Doctrine::gettable('Merchant')->find($merchantId);
      $merchantWizardObj = new merchantWizard();
      $merchantSql = $merchantWizardObj->getSqlForMerchant($merchantId);
      $pay4meLogObj=  new pay4meLog();
      $file_array = $pay4meLogObj->saveListFile($merchantSql,'/report/merchantSql/',$merchantObj->getName());
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "Merchant_SQL";
            $this->setTemplate('csv');
    }
  public function executeMerchantDownloadSql(sfWebRequest $request) {
      $fileName = $this->getUser()->getAttribute('fileName');
      $filenameArr = explode('/',$fileName);
      $keys = array_keys($filenameArr);
      $lastKey = $keys[sizeof($filenameArr)-1];
      $logPath =  sfConfig::get('sf_log_dir').'/MerchantSql/'.date('Y-m-d').'/'.$filenameArr[$lastKey];
      $fileNmae = $filenameArr[$lastKey];
                header("Cache-Control: public, must-revalidate");
                header("Pragma: hack");
                header("Content-Type: text/html" );
                header("Content-Type: application/force-download");
                header("Content-Length: " .(string)(filesize($logPath)) );
                header('Content-Disposition: attachment; filename="'.$fileNmae.'"');
                header("Content-Transfer-Encoding: binary\n");
                readfile($logPath);
                exit;
    }
  public function executeMerchantPdf(sfWebRequest $request) {
      $merchantId = $request->getParameter('merchantId');
      $merchantObj = Doctrine::gettable('Merchant')->find($merchantId);
      $merchantWizardObj = new merchantWizard();
      $merchantSql = $merchantWizardObj->createSetupTemplate($merchantId);
      $pay4meLogObj=  new pay4meLog();
      $file_array = $pay4meLogObj->saveListFile($merchantSql,'/report/merchantSql/',$merchantObj->getName());
            $file_array = explode('#$', $file_array);
            $this->fileName = $merchantObj->getName();//$file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "Merchant_SQL";
            $this->pdf=true;
            $this->setTemplate('csv');
    }
    public function executeUpdateFreezedMerchant(sfWebRequest $request) {
//CR046

        $MServiceId = $request->getParameter('merchantServiceId');
        $oldMerchantDetailsObj = Doctrine::gettable('MerchantService')->find($MServiceId);
        $oldMerchantId = $oldMerchantDetailsObj->getMerchantId();
        $detailsObj = Doctrine::gettable('InterswitchMerchantCategoryMapping')->MerchantExists($oldMerchantId);
        if ($detailsObj->count()) {
            $interswitchCategory = $detailsObj->getFirst()->getInterswitchCategoryId();
        }
        $serviceObj = Doctrine::getTable('MerchantService')->find($MServiceId);

        /////call function to copy merchant

        $newMerchantId = $this->mySqlImportMerchant($serviceObj->getMerchant()->getId());
        if ($detailsObj->count()) {
            Doctrine::gettable('InterswitchMerchantCategoryMapping')->updateRequest($newMerchantId, $interswitchCategory, true);
        }

        ////
//        echo 'Updated Successfully';
//       $this->renderText('Updated Successfully');
        $this->redirect($this->getModuleName() . '/listMerchantWizard?updatedMerchantId=' . $newMerchantId);
    }
   public function executeFreezedMerchant(sfWebRequest $request) {
       $merchantId =  $request->getParameter('merchantId');
       $merchantObj = Doctrine::gettable('Merchant')->find($merchantId);
       $merchantObj->setIsFreezed('1');
       $merchantObj->save();
       echo 'Updated Successfully';
       $this->renderText('Updated Successfully');
       die;

   }
   public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($merchant_service = Doctrine::getTable('MerchantService')->find(base64_decode($request->getParameter('id'))), sprintf('Object merchant_service does not exist (%s).', $request->getParameter('id')));
        $this->getUser()->getAttributeHolder()->remove('merchantServiceId');
        $this->getUser()->getAttributeHolder()->remove('merchantId');

       $this->getUser()->setAttribute('merchantServiceId', base64_decode($request->getParameter('id')));
        $this->getUser()->setAttribute('merchantId', $merchant_service->getMerchantId());


        $this->form = new MerchantWizardForm($merchant_service);
        $this->form->setDefault('merchant_name', $merchant_service->getMerchant()->getName());
        $this->form->setDefault('category_id', $merchant_service->getMerchant()->getCategoryId());
    }

   public function executeUpdate(sfWebRequest $request) {
        $merchantServiceArr = $request->getParameter('merchant_wizard');
        $this->forward404Unless($merchant_service = Doctrine::getTable('MerchantService')->find($merchantServiceArr['id']), sprintf('Object merchant_service does not exist (%s).', $merchantServiceArr['id']));


        $this->form = new MerchantWizardForm($merchant_service);
        $this->form->setDefault('merchant_name', $merchant_service->getMerchant()->getName());

        $this->form->bind($request->getParameter($this->form->getName()));
        if ($this->form->isValid()) {
            $merchant_obj = merchantServiceFactory::getService(merchantServiceFactory::$TYPE_BASE);

            //Update merchant Name
            if ($merchantServiceArr['merchant_name']!=$merchant_service->getMerchant()->getName())
            {
                $already_created = $merchant_obj->getTotalMerchant($merchantServiceArr['merchant_name']);
                if ($already_created) {
                    $this->getUser()->setFlash('error', sprintf('Merchant of same name already exists'));
                } else {
                    $merchantObj = $merchant_service->getMerchant();
                    $merchantObj->setName($merchantServiceArr['merchant_name']);
                    
                    $merchantObj->save();
                }
            }
            // Add Update Merchant Category
            if($merchantServiceArr['category_id']!=$merchant_service->getMerchant()->getCategoryId())
            {
            	$merchantObj = $merchant_service->getMerchant();
            	$merchantObj->setCategoryId($merchantServiceArr['category_id']);
            	$merchantObj->save();
            }
            
            $this->form->save();
            $this->forward($this->getModuleName(), 'updateCurrnecy');
//           $this->forward('merchantWizard', 'editValidationRule');

        }

        $this->setTemplate('edit');
    }
    public function executeUpdateCurrnecy(sfWebRequest $request){

        //checking direct hit
//        $this->checkIsSessionSet(3);
        $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId');
        $servicePayModeOpt = Doctrine::getTable('ServicePaymentModeOption')->findByMerchantServiceId($merchantServiceId);

        $currencyArr =  array();
        foreach($servicePayModeOpt as $servicePayModeOptRow)
        {
            $currencyArr[] = $servicePayModeOptRow->getCurrencyId();
        }

        $this->getUser()->getAttributeHolder()->remove('MCurrency');

        $this->currencyForm = new CurrencyChoiceForm(Null, array('selectedCurrency'=>$currencyArr));

        if($request->isMethod('post')){
            $this->currencyForm->bind($request->getParameter('currencyForm'));
            if ($this->currencyForm->isValid()) {

               $currencyValues = $request->getPostParameters();
               $i=0;
               foreach($currencyValues['currencyForm']['currency'] as $val)
               {
                   $currencyObj = Doctrine::getTable('CurrencyCode')->find($val);
                   $currencyCode[$currencyObj->getId()] = $currencyObj->getCurrency();
                   $i++;
               }
               $this->getUser()->setAttribute('MCurrency', $currencyCode);
               $this->forward('merchantWizard', 'editValidationRule');
//               $this->redirect($this->getModuleName().'/paymentModeOption');

            }
        }
        $this->setTemplate('chooseCurrnecy');
    }
    public function executeEditValidationRule(sfWebRequest $request) {
        $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId');
        $this->MerchantServiceId = $merchantServiceId;
        $this->merchant_service = Doctrine::getTable('ValidationRules')->findByMerchantServiceId($this->MerchantServiceId);
        $this->noOfValidRules =  $this->merchant_service->count();

    }

    public function executeUpdateValidationRule(sfWebRequest $request) {

       $this->merchantServiceId = $request->getParameter('merchant_service_id');
        $count =  $request->getParameter('oldValue');
        if($request->getParameter('no_of_values'))
        {
          $noofValues = explode(',',$request->getParameter('no_of_values'));
          $sub = 0;
          $intIntegerCount = 0;
          $intStringCount = 0;
            foreach ($noofValues as $val) {
                $param_name_var = $request->getParameter('validationRules');
                $paramName = $param_name_var['param_name'.$val];
                $paramDesc = $param_name_var['param_desc'.$val];
                $isManadatory = $param_name_var['param_mandatory'.$val]=='yes'?1:null;
                $parameterType = $param_name_var['param_type'.$val];
                //$paramName = $request->getParameter('validationRules[param_name'.$val.']');
                //$paramDesc = $request->getParameter('validationRules[param_desc'.$val.']');
                //$isManadatory = $request->getParameter('validationRules[param_mandatory'.$val.']')=='yes'?1:null;
                //$parameterType = $request->getParameter('validationRules[param_type'.$val.']');
                if($parameterType == "integer"){
                    $intIntegerCount++;
                    $mapped_to = "iparam_".$this->numArr[$intIntegerCount];
                } else if($parameterType == "string"){
                    $intStringCount++;
                    $mapped_to = "sparam_".$this->numArr[$intStringCount];
                }
                if ($val<=$count) {
                    $id = $request->getParameter('validation_id'.$val.'');
                    $validationObj = Doctrine::getTable('ValidationRules')->find(array($id));
                    $validationObj->setParamName($paramName);
                    $validationObj->setParamDescription($paramDesc);
                    $validationObj->setIsMandatory($isManadatory);
                    $validationObj->setParamType($parameterType);
                    $validationObj->setMappedTo($mapped_to);
                } else {
                    $validationObj = new ValidationRules();
                    $validationObj->setMerchantServiceId($this->merchantServiceId);
                    $validationObj->setParamName($paramName);
                    $validationObj->setParamDescription($paramDesc);
                    $validationObj->setIsMandatory($isManadatory);
                    $validationObj->setParamType($parameterType);
                    $validationObj->setMappedTo($mapped_to);
                }
                $validationObj->save();

                $delIds = explode(',',$request->getParameter('delIds'));
                foreach ($delIds as $delId) {
                    $delValidation = Doctrine::getTable('ValidationRules')->find($delId);
                       if(is_object($delValidation))
                          $delValidation->delete();
                }
            }

        }
       $this->redirect('merchantWizard/editPaymentMode');

    }
private function getPaymentModeOptions($merchantServiceId)
{$paymentModeObj = Doctrine::getTable('ServicePaymentModeOption')->getPaymentOptions($merchantServiceId);


foreach ($paymentModeObj as $val) {
            $paymentModeArray[$val->getCurrencyId()][$val->getPaymentModeOptionId()] = $val->getPaymentModeOptionId();
//           $selectedCurr[$val->getCurrencyId()]=$val->getCurrencyCode()->getCurrency();
            $selectedPaymentModes[] = $val->getPaymentModeOptionId();
        }
        $selectedCurr = $this->getUser()->getAttribute('MCurrency');
        if (isset($selectedCurr)) {
//                $this->getUser()->setAttribute('MCurrency', $selectedCurr);
            $this->selectedCurr = $selectedCurr;
            if (isset($paymentModeArray[key($selectedCurr)])){

                return $paymentModeArray[key($selectedCurr)];
            }
            else{
                return  array();}
        }


}
    public function executeEditPaymentMode(sfWebRequest $request) {
        $this->merchantMapped = 0;

        $this->MerchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');
        $this->MerchantId = $this->getUser()->getAttribute('merchantId', '0');
        $chkMerchantMapping = Doctrine::getTable('InterswitchMerchantCategoryMapping')->MerchantExists($this->MerchantId);

            $this->selectedCategory=0;
        if ($chkMerchantMapping->count()) {
            $this->selectedCategory=Doctrine::getTable('InterswitchMerchantCategoryMapping')->findBy('merchant_id',$this->MerchantId)->getLast()->getInterswitchCategoryId();
                   $this->merchantMapped = 1;
        }

        $this->paymentModeArray = $this->getPaymentModeOptions($this->MerchantServiceId);



        $paymentModes = Doctrine::getTable('PaymentModeOption')->getPaymentModeOptionArr();

        $selPaymentModeOptionChoices = array('' => 'Select Payment Mode Options');

        $this->payment_mode_options = $selPaymentModeOptionChoices + $paymentModes;

        $interswitchCategoryArr = Doctrine::getTable('InterswitchCategory')->getAllCAtegories();
        $selInterswitchCategoryOptionChoices = array('' => 'Select Interswitch Category');
        $this->interswitch_category_options = $selInterswitchCategoryOptionChoices + $interswitchCategoryArr;
        if ('getNext' == $request->getParameter('task')) {
                $pfmHelperObj = new pfmHelper();
                $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
                $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                $paymentModeBankId = $pfmHelperObj->getPMOIdByConfName('bank');
            if (in_array($paymentModeBankId, $this->paymentModeArray)
                    || in_array($paymentModeDraftId, $this->paymentModeArray)
                    || in_array($paymentModeCheckId, $this->paymentModeArray) )
                $this->redirect('merchantWizard/updateBanks');
            else
                $this->redirect('merchantWizard/editMerchantServiceCharges');
        }
        if ($request->isMethod('post') && $request->getParameter('save')) {

            $paymentMode = $request->getParameter('payment_mode');
            $currency = $request->getParameter('currency');
            $interswitchcategory = $request->getParameter('interswitch_category');
           if ($interswitchcategory[0] != '') {

                Doctrine::getTable('InterswitchMerchantCategoryMapping')->updateRequest($this->MerchantId, $interswitchcategory['0'], false);
            }
            if (!is_array($paymentMode) || ( count($paymentMode) == 1 && $paymentMode[0] == '')) {
                $paymentMode = array();
                $this->getUser()->setFlash('error', sprintf('Please select Payment Mode'));
            } else {
                $delPaymentMode = array_diff($this->paymentModeArray, $paymentMode);



                foreach ($paymentMode as $val) {

                    if (!array_search($val, $this->paymentModeArray)) {
                        $addObj = new ServicePaymentModeOption();
                        $addObj->setMerchantServiceId($this->MerchantServiceId);
                        $addObj->setPaymentModeOptionId($val);
                        $addObj->setCurrencyId($currency);
                        $addObj->save();

                        /// Bug id  ##28950 -
                        $this->getUser()->setFlash('notice', sprintf('Payment Mode Option has been Updated  for ' . $this->selectedCurr[$currency]));
                    }
                }
                if($interswitchcategory[0] != '')
                {

                    $this->getUser()->setFlash('notice', sprintf('Payment Mode Option has been Updated  for ' . $this->selectedCurr[$currency]));

                }
                if (count($delPaymentMode) > 0) {
                    foreach ($delPaymentMode as $val) {
                        $pmObj = Doctrine::getTable('ServicePaymentModeOption')->findByMerchantServiceId($this->MerchantServiceId);
                        foreach ($pmObj as $ival) {
                            if ($ival->getPaymentModeOptionId() == $val && $ival->getCurrencyId() == $currency) {
                                $delValidation = Doctrine::getTable('ServicePaymentModeOption')->find(array($ival->getId()));
                                $delValidation->delete();
                            }
                        }
                    }
                }
                 $this->paymentModeArray = $this->getPaymentModeOptions($this->MerchantServiceId);
                 if($interswitchcategory[0] != '')
                {
                 $this->selectedCategory=Doctrine::getTable('InterswitchMerchantCategoryMapping')->findBy('merchant_id',$this->MerchantId)->getLast()->getInterswitchCategoryId();
                }


            }
        }
    }

   public function executeGetBanks(sfWebRequest $request)
    {
        $merchantId = $this->getUser()->getAttribute('merchantId', '0');
        $currency = $request->getParameter('currency');
         $bankMerchantArr = Doctrine::getTable('BankMerchant')->findAllReleatedBanks($merchantId,$currency);
         foreach($bankMerchantArr as $bank)
               $selectedBanks[]=$bank['Bank']['id'];

         $allBanks=Doctrine::gettable('bank')->getList();
         $str = '';
         foreach($allBanks as $key=>$value)
         {
            if(isset($selectedBanks) && in_array($value['id'],$selectedBanks))
                $str .= '<option value="'.$value['acronym'].'" selected=selected >'.$value['bank_name'].'</option>';
            else
                $str .= '<option value="'.$value['acronym'].'">'.$value['bank_name'].'</option>';
         }

        return $this->renderText($str);
    }
    private function setbankDetails($selectedCurr )
    {
       foreach($selectedCurr as $currency => $cvalue)
        {
        $merchantId = $this->getUser()->getAttribute('merchantId', '0');
        $bankMerchantArr = Doctrine::getTable('BankMerchant')->findAllReleatedBanks($merchantId,$currency);
                 $i=0;
                 $selectedBanks=array();
                foreach($bankMerchantArr as $bankrow)
                    {
                        $selectedBanks[$i][0]= $bankrow['Bank']['id'];
                        $selectedBanks[$i][1]= $bankrow['Bank']['bank_name'];
                        $selectedBanks[$i][2]= $bankrow['Bank']['acronym'];

                        $i++;
                    }
                $setBanksDetails[$currency] = $selectedBanks;

        }
       $this->getUser()->setAttribute('bankDetails', $setBanksDetails);
    }
    public function executeUpdateBanks(sfWebRequest $request){


//        $this->checkIsSessionSet(7);
        $this->setTemplate('choosebank');
        $this->formAction='updateBanks';
        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BANK);
        $this->selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $this->bank_array = $bank_obj->getBankList();

         $this->setbankDetails($this->selectedCurr);
         $getAllBankDetails = $this->getUser()->getAttribute('bankDetails');

        if($request->isMethod('post')){

            if( $request->getParameter('save'))
            {
                $bank_params = $request->getPostParameters();
                $bankArr=$bank_params['bank_id'];
//                 echo "<pre>";print_r($bankArr);die;
                $count=0;
                $bankDeatails = array();


//                if($getAllBankDetails && array_key_exists($bank_params['currency_id'], $getAllBankDetails))
//                    {
//                        $this->getUser()->setFlash('error', sprintf('Banks have been already updated for '.$this->selectedCurr[$bank_params['currency_id']]));
//
//                    }
//                else{

                    $merchantId = $this->getUser()->getAttribute('merchantId', '0');

                    $selectedBanks=array();
                    $bankMerchantArr = Doctrine::getTable('BankMerchant')->findAllReleatedBanks($merchantId,$bank_params['currency_id']);
                    foreach($bankMerchantArr as $selbank)
                        $selectedBanks[]=$selbank['Bank']['id'];


                    foreach( $bankArr as $bankIdVal){
                        if(''!=$bankIdVal)
                        {
                            $bankObj = Doctrine::getTable('Bank')->getBankObjectByAcronym($bankIdVal);
                            $bankId = $bankObj->getId();
                            $bankName = $bankObj->getBankName();
                            $bank[$count][0] = $bankId;
                            $bank[$count][1] = $bankName;
                            $bank[$count][2] = $bankIdVal;
                            if(!in_array($bankId,$selectedBanks))
                                {
                                $BankMerchantObj = new BankMerchant();
                                $BankMerchantObj->setMerchantId($merchantId);
                                $BankMerchantObj->setCurrencyId($bank_params['currency_id']);
                                $BankMerchantObj->setBankId($bankId);
                                $BankMerchantObj->save();

                                }
                                 $newBankId[]=$bankId;
//                            $bank[3] = $bank_params['currency_id'];

                        }
                        $count++;
                    }
                    if(isset($newBankId))
                    {
                        $deleteBankIds = array_diff($selectedBanks,$newBankId);

                        foreach($deleteBankIds as $deleteBankId)
                        {
                            $delBank = Doctrine::getTable('BankMerchant')->deleteReleatedBank($merchantId,$bank_params['currency_id'],$deleteBankId);
                        }

                    }
//                    $bankDeatails['banks']=$bank;
//                    $bankDeatails['currency']=$bank_params['currency_id'];
                    //////////$getAllBankDetails[$bank_params['currency_id']] = $bank;
//                }
                $this->setbankDetails($this->selectedCurr);
                //////$this->getUser()->setAttribute('bankDetails', $getAllBankDetails);

                $request->setParameter('save', false);
                $this->getUser()->setFlash('notice', sprintf('Banks have been updated for '.$this->selectedCurr[$bank_params['currency_id']]));
                $this->forward($this->moduleName,$this->actionName);
    //            $this->redirect('merchantWizard/collectionAccountSetup');
            }//if save
        }
        else{
//            $this->getUser()->getAttributeHolder()->remove('bankDetails');
        }
    }

        public function executeUpdateCollectionAccountSetup(sfWebRequest $request){

        //checking direct hit
//        $this->checkIsSessionSet(8);
            $this->formAction='updateCollectionAccountSetup';
            $this->setTemplate('collectionAccountSetup');
        //bank details session
       $selBanks = $this->getUser()->getAttribute('bankDetails', '0');

        if($selBanks==0 )
             $this->redirect($this->moduleName.'/editMerchantServiceCharges');
        $this->selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $merchantId = $this->getUser()->getAttribute('merchantId', '0');
        $merchantDetail=Doctrine::gettable('Merchant')->find($merchantId);
         foreach($selBanks as  $outerKey=>$outerVal)
            {
               foreach ($outerVal as $key=>$val)
               {
//                   echo $val[0]."===".$merchantId."@@@".$outerKey;

                    $isAccountSetuped = Doctrine::getTable('ServiceBankConfiguration')->getMasterAcount('', $val[0], $merchantId, 'Payment',$outerKey);
                   if($isAccountSetuped)
                        {

//                            unset($selBanks[$outerKey][$key]);
                            $actId = $isAccountSetuped->getfirst()->getMasterAccountId();
                            $accontObj  = Doctrine::getTable('EpMasterAccount')->find($actId);

                            $selBanks[$outerKey][$key][4]['account_id'] = $actId;
                            $selBanks[$outerKey][$key][4]['account_number'] = $accontObj->getAccountNumber();
                            $selBanks[$outerKey][$key][4]['account_name'] = $accontObj->getAccountName();
                            $selBanks[$outerKey][$key][4]['sortcode'] = $accontObj->getSortcode();
                         }


                }
//
//                if(count($selBanks[$outerKey])==0)
//                   {
//                       unset($selBanks[$outerKey]);
//                      unset($this->selectedCurr[$outerKey]);
//                   }
            }

        if(count($selBanks)==0 )
             $this->redirect($this->moduleName.'/editMerchantServiceCharges');

        $this->banks = array();
        $bankReceivingAccDetails = array();
        $this->banks = $selBanks;

//        $this->form = new collectionAccountSetupForm(array(),array('banks'=>$this->banks));

//        $this->noOfBanks = count($this->banks);

        if($request->isMethod('post')){
            $this->collectionAcc_params = $request->getPostParameters();
            //echo "<pre>"; print_r($this->collectionAcc_params); die;

            $err = 0;
            //validating all values entered
             for($i=1;$i<$this->collectionAcc_params['noOfBanks'];$i++){
                    if($this->collectionAcc_params['accNo'.$i] == '' || $this->collectionAcc_params['accName'.$i] == '' || $this->collectionAcc_params['sortCode'.$i] == ''){
                    $err++;
                }
//            }
           }

            if($err > 0){
                $this->getUser()->setFlash('error', 'Please fill the complete form.', true) ;
            }else{

                $count=1;
                //setting array for collection accounts
//               foreach($this->selectedCurr as $currKey => $currVal)
//                {
                 for($i=1;$i<$this->collectionAcc_params['noOfBanks'];$i++){


                        if($this->collectionAcc_params['account_id'.$count]==0)
                        {
                            $collectionAccDetails[$count] = array('currency' => $this->collectionAcc_params['currencyId'.$count], 'accNo' => $this->collectionAcc_params['accNo'.$count], 'accName' => $this->collectionAcc_params['accName'.$count], 'accType' => 'collection', 'sortCode' => $this->collectionAcc_params['sortCode'.$count], 'bankName' => $this->collectionAcc_params['bank'.$count], 'bankId' => $this->collectionAcc_params['bankId'.$count]);
                            $accountNo = $this->createAccountNumber();
                            $accName = $merchantDetail->getName()." ".$this->collectionAcc_params['bank'.$i]." bank receiving account. ";
                            $bankReceivingAccDetails[$count] = array('currency' => $this->collectionAcc_params['currencyId'.$count], 'accNo' => $accountNo, 'accName' => $accName, 'accType' => 'receiving','sortCode' => NULL, 'bankName' => $this->collectionAcc_params['bank'.$count], 'bankId' => $this->collectionAcc_params['bankId'.$count]);
                        }
                        else
                        $collectionAccDetails[$count] = array('account_id' => $this->collectionAcc_params['account_id'.$count],'currency' => $this->collectionAcc_params['currencyId'.$count], 'accNo' => $this->collectionAcc_params['accNo'.$count], 'accName' => $this->collectionAcc_params['accName'.$count], 'accType' => 'collection', 'sortCode' => $this->collectionAcc_params['sortCode'.$count], 'bankName' => $this->collectionAcc_params['bank'.$count], 'bankId' => $this->collectionAcc_params['bankId'.$count]);

                    $count++;
                 }

                $userId=$this->getUser()->getGuardUser()->getId();
                                     ///////////////collection account detail ////////////////
                if(!empty($collectionAccDetails)){
                     $sevicebankcfg=Doctrine::getTable('ServiceBankConfiguration')->getPMOByMerchantId($merchantId);
                     $merchant_pmo=Doctrine::getTable('Merchant')->getMerchant($merchantId);
                     $merchant_pmo_arr=$merchant_pmo['0']['MerchantService']['0']['ServicePaymentModeOption'];
                     foreach($merchant_pmo_arr as $merchant_pmo_var){
                        $merchant_pmo_arr_in[]= $merchant_pmo_var['payment_mode_option_id'];
                     }
                    $pfmHelperObj = new pfmHelper();
                    $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
                    $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                    $paymentModeBankId = $pfmHelperObj->getPMOIdByConfName('bank');

                    foreach($collectionAccDetails as $collectionVal){

                       if(isset($collectionVal['account_id']))
                       {

                            $EpMasterAccountObj =  Doctrine::getTable('EpMasterAccount')->find($collectionVal['account_id']);
                            $EpMasterAccountObj->setAccount_number($collectionVal['accNo']);
                            $EpMasterAccountObj->setAccount_name($collectionVal['accName']);
                            $EpMasterAccountObj->setType($collectionVal['accType']);
                            $EpMasterAccountObj->setSortcode($collectionVal['sortCode']);
                            $EpMasterAccountObj->setBankname($collectionVal['bankName']);
                            $EpMasterAccountObj->save();
                            //chk if PMO set for merchant is having equal service bank configuration
                            $EpMasterAccountId=$EpMasterAccountObj->id;
                            if(count($merchant_pmo_arr_in)!=count($sevicebankcfg)){
                                foreach($merchant_pmo_arr_in as $pmo){
                                   if ((!in_array($pmo,$sevicebankcfg)) && ($paymentModeBankId==$pmo || $paymentModeCheckId== $pmo|| $paymentModeDraftId==$pmo )){
                                        $ServiceBankConfigObj=new ServiceBankConfiguration();
                                        $ServiceBankConfigObj->setMerchant_id($merchantId);
                                        $ServiceBankConfigObj->setPayment_mode_option_id($pmo);
                                        $ServiceBankConfigObj->setBank_id($collectionVal['bankId']);
                                        $ServiceBankConfigObj->setMaster_account_id($EpMasterAccountId);
                                        $ServiceBankConfigObj->setTransfer_type('Payment');
                                        $ServiceBankConfigObj->setCurrency_id($collectionVal['currency']);
                                        $ServiceBankConfigObj->save();
                                   }
//                                   $serviceConfDiff = array_diff($sevicebankcfg, $merchant_pmo_arr_in);
//                                    if (count($serviceConfDiff) > 0) {
//                                        foreach ($serviceConfDiff as $val) {
//                                            $sbconfigObj = Doctrine::getTable('ServiceBankConfiguration')->findByMerchantId($merchantId);
//                                            foreach ($sbconfigObj as $sval) {
//                                                if ($sval->getPaymentModeOptionId() == $val && $sval->getCurrencyId() == $collectionVal['currency']) {
//                                                    $delSbConfig = Doctrine::getTable('ServiceBankConfiguration')->find(array($sval->getId()));
//                                                    $delSbConfig->hardDelete();
//                                                }
//                                            }
//                                         }
//                                      }
                                    }
                            }
                            //$EpMasterAccountId=$EpMasterAccountObj->id;


                       }
                       else
                       {
                            $sevicebankcfgWithBank=Doctrine::getTable('ServiceBankConfiguration')->getPMOByMerchantId($merchantId,$collectionVal['bankId']);

                            $EpMasterAccountObj=new EpMasterAccount();
                            $EpMasterAccountObj->setAccount_number($collectionVal['accNo']);
                            $EpMasterAccountObj->setAccount_name($collectionVal['accName']);
                            $EpMasterAccountObj->setType($collectionVal['accType']);
                            $EpMasterAccountObj->setSortcode($collectionVal['sortCode']);
                            $EpMasterAccountObj->setBankname($collectionVal['bankName']);
                            $EpMasterAccountObj->save();
                            $EpMasterAccountId=$EpMasterAccountObj->id;
                            foreach($merchant_pmo_arr_in as $pmo){
                               if ((!in_array($pmo,$sevicebankcfgWithBank)) && ($paymentModeBankId==$pmo || $paymentModeCheckId== $pmo|| $paymentModeDraftId==$pmo )){
                                    $ServiceBankConfigObj=new ServiceBankConfiguration();
                                    if($merchantId == 0)
                                        $ServiceBankConfigObj->setMerchant_id($saveMerchantId);
                                    else
                                        $ServiceBankConfigObj->setMerchant_id($merchantId);
                                    $ServiceBankConfigObj->setPayment_mode_option_id($pmo);
                                    $ServiceBankConfigObj->setBank_id($collectionVal['bankId']);
                                    $ServiceBankConfigObj->setMaster_account_id($EpMasterAccountId);
                                    $ServiceBankConfigObj->setTransfer_type('Payment');
                                    $ServiceBankConfigObj->setCurrency_id($collectionVal['currency']);
                                    $ServiceBankConfigObj->save();
                               }
                            }
                       }//else for collection val
                    } //foreach for collection account
                 }

                    foreach($bankReceivingAccDetails as $recievingVal){
                        $EpMasterAccountObj=new EpMasterAccount();
                        $EpMasterAccountObj->setAccount_number($recievingVal['accNo']);
                        $EpMasterAccountObj->setAccount_name($recievingVal['accName']);
                        $EpMasterAccountObj->setType($recievingVal['accType']);
                        $EpMasterAccountObj->setSortcode($recievingVal['sortCode']);
                        $EpMasterAccountObj->setBankname($recievingVal['bankName']);
                        $EpMasterAccountObj->save();
                        $EpMasterAccountId=$EpMasterAccountObj->id;

                        $FinInstActConfigObj=new FinancialInstitutionAcctConfig();
                        if($merchantId == 0)
                            $FinInstActConfigObj->setMerchant_id($saveMerchantId);
                        else
                            $FinInstActConfigObj->setMerchant_id($merchantId);

                        $FinInstActConfigObj->setBank_id($recievingVal['bankId']);
                        $FinInstActConfigObj->setAccount_id($EpMasterAccountId);
                        $FinInstActConfigObj->setCurrency_id($recievingVal['currency']);
                        $FinInstActConfigObj->save();


                    } //foreach for receiving 0accoutn

                 $this->redirect($this->moduleName.'/editMerchantServiceCharges');
            }
        }
    }


   public function executeGetPaymentModeforEdit(sfWebRequest $request)
    {
//        $SelectedPayMode = $this->getUser()->getAttribute('payment_mode_option', 0);

        $currency = $request->getParameter('currency');
        $merchantId = $this->getUser()->getAttribute('merchantId', '0');
        $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');
        $SelectedPayMode = Doctrine::getTable('ServicePaymentModeOption')->getServiceDetails($merchantId,$merchantServiceId,$currency);

        $str = '';
        foreach($SelectedPayMode as $key=>$value)
            $str .= '<option value="'.$value['paymentModeOption'].'">'.$value['payment_mode_display_name'].'</option>';

        return $this->renderText($str);
    }

    public function executeEditMerchantServiceCharges(sfWebRequest $request)
    {

        $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');
        $merchantServiceChargeObj = Doctrine::getTable('MerchantServiceCharges')->findByMerchantServiceId($merchantServiceId);

        $selectedPayMode = $this->getUser()->getAttribute('payment_mode_option', 0);

        $currency =$this->getUser()->getAttribute('MCurrency', 0);
        $this->selectedCurr =  $currency;
        $this->paymentMode=array();
//        $mCharges = Doctrine::getTable('MerchantServiceCharges')->getCharges($merchantServiceId,key($selectedPayMode),key($currency));
//        $chargesArr = array($mCharges[0]['service_charge_percent'],$mCharges[0]['upper_slab'],$mCharges[0]['lower_slab']);

        $this->MerchantServiceCharges = new MerchantServiceChargesForWiz();
        if ($request->isMethod('post'))
        {
            //// print_r($request->getParameter('merchant_service_charges'));die;

            $this->MerchantServiceCharges->bind($request->getParameter('merchant_service_charges'));
            $merchantServiceCharge = $request->getPostParameters();

            if ($this->MerchantServiceCharges->isValid() && $request->getParameter('save')) {
                $getMerchantSvcCharges = $this->getUser()->getAttribute('merchantServiceCharges');
                $merchantSvcChg = $request->getParameter('merchant_service_charges');

                $paramHolder = $request->getParameterHolder()->getAll();
                $merchantSvcChg['currency']=$paramHolder['currency'];
                $merchantSvcChg['paymentMode']=$paramHolder['paymentMode'];

          //For removing the lower slab validation in service charges.
                /* if($merchantServiceCharge['merchant_service_charges']['lower_slab']>$merchantServiceCharge['merchant_service_charges']['upper_slab'])
                {
                    $this->getUser()->setFlash('error', sprintf('lower slab must be less than or equal to upper slab'));
                } */
                //else{
                    $getMerchantSvcCharges[] = $merchantSvcChg;

                        $this->getUser()->setAttribute('merchantServiceCharges', $getMerchantSvcCharges);
                        $merchanserviceObj = Doctrine::getTable('MerchantServiceCharges')->getChargesObj($merchantServiceId,$merchantSvcChg['paymentMode'],$merchantSvcChg['currency']);
                        if(!$merchanserviceObj)
                             $merchanserviceObj = new MerchantServiceCharges();
                         $merchanserviceObj->setMerchantServiceId($merchantServiceId);
                         $merchanserviceObj->setPaymentModeOptionId($merchantSvcChg['paymentMode']);
                         $merchanserviceObj->setCurrencyId($merchantSvcChg['currency']);
                        $merchanserviceObj->setServiceChargePercent($merchantSvcChg['service_charge_percent']);
                        $merchanserviceObj->setUpperSlab($merchantSvcChg['upper_slab']);
                        $merchanserviceObj->setLowerSlab($merchantSvcChg['lower_slab']);
                        $merchanserviceObj->save();
                        $paymentModeOption_name= pfmHelper::getPMONameByPMId($merchantSvcChg['paymentMode']);
                        $currency_name=Doctrine::getTable('CurrencyCode')->find($merchantSvcChg['currency'])->getCurrency();
                     $this->getUser()->setFlash('notice', sprintf('Merchant Service Charges has been updated for Payment Mode '.$paymentModeOption_name.' and Currency '.$currency_name));
                    $request->setParameter('save', false);
                    $getMerchantSvcCharges = $this->getUser()->getAttribute('merchantServiceCharges');
                    $this->forward($this->moduleName,$this->actionName);//paymentModeOption
                    // $this->redirect('merchantWizard/transactionCharges');
                //}
            }
        }


    }

    public function executeResultMerchantServiceCharges(sfWebRequest $request)
    {
         $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');

         $currency = $request->getParameter('currency');
         $paymentMode = $request->getParameter('paymentMode');
        $mCharges = Doctrine::getTable('MerchantServiceCharges')->getCharges($merchantServiceId,$paymentMode,$currency);
        $str = $mCharges[0]['service_charge_percent'].'###'.$mCharges[0]['upper_slab'].'###'.$mCharges[0]['lower_slab'];
        return $this->renderText($str);

    }
    public function executeResultTransactionCharges(sfWebRequest $request)
    {
         $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');

         $currency = $request->getParameter('currency');
         $paymentMode = $request->getParameter('paymentMode');
        $mCharges = Doctrine::getTable('TransactionCharges')->getCharges($merchantServiceId,$paymentMode,$currency);

        $str = $mCharges['financial_institution_charges'].'###'.$mCharges['payforme_charges'];
        return $this->renderText($str);

    }
      public function executeUpdateTransactionCharges(sfWebRequest $request)
    {

        $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');
        $merchantServiceChargeObj = Doctrine::getTable('MerchantServiceCharges')->findByMerchantServiceId($merchantServiceId);

        $selectedPayMode = $this->getUser()->getAttribute('payment_mode_option', 0);

        $currency =$this->getUser()->getAttribute('MCurrency', 0);
        $this->selectedCurr =  $currency;
        $this->paymentMode=array();
//        $mCharges = Doctrine::getTable('MerchantServiceCharges')->getCharges($merchantServiceId,key($selectedPayMode),key($currency));
//        $chargesArr = array($mCharges[0]['service_charge_percent'],$mCharges[0]['upper_slab'],$mCharges[0]['lower_slab']);

        if($request->getPostParameter('paymentMode')) {
            $paymentMode  = $request->getPostParameter('paymentMode');
        }else {
            $paymentMode = Doctrine::getTable('TransactionCharges')->getPaymentMode($merchantServiceId);
        }

        $this->TransactionCharges = new TransactionChargesForWiz(array(),array('paymentMode'=>$paymentMode,'type'=>'edit'));
        if ($request->isMethod('post'))
        {
            //// print_r($request->getParameter('merchant_service_charges'));die;

            $this->TransactionCharges->bind($request->getParameter('transaction_charges'));


            if ($this->TransactionCharges->isValid() && $request->getParameter('save')) {
                $getTransactionCharges = $this->getUser()->getAttribute('transChargeData');

                $transactionChg = $request->getParameter('transaction_charges');
                $paramHolder = $request->getParameterHolder()->getAll();
                $transactionChg['currency']=$paramHolder['currency'];
                $transactionChg['paymentMode']=$paramHolder['paymentMode'];


                    $merchanserviceObj = Doctrine::getTable('TransactionCharges')->getChargesObj($merchantServiceId,$transactionChg['paymentMode'],$transactionChg['currency']);
                        if(!$merchanserviceObj)
                             $merchanserviceObj = new TransactionCharges();
                         $merchanserviceObj->setMerchantServiceId($merchantServiceId);
                         $merchanserviceObj->setPaymentModeOptionId($transactionChg['paymentMode']);
                         $merchanserviceObj->setCurrencyId($transactionChg['currency']);
                         $pfmHelperObj = new pfmHelper();
                         $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
                         $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
                         $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');

                         if($paymentModeCheckId ==$transactionChg['paymentMode'] || $paymentModeDraftId  ==$transactionChg['paymentMode']||$paymentModebankId  ==$transactionChg['paymentMode'])
                            $merchanserviceObj->setFinancialInstitutionCharges($transactionChg['finInstCharge']);
                        $merchanserviceObj->setPayformeCharges($transactionChg['PayForCharge']);

                        $merchanserviceObj->save();
                    $request->setParameter('save', false);
                    $paymentModeOption_name=pfmHelper::getPMONameByPMId($transactionChg['paymentMode']);
                        $currency_name=Doctrine::getTable('CurrencyCode')->find($transactionChg['currency'])->getCurrency();

                       $this->getUser()->setFlash('notice', sprintf('Transaction Charges have been updated for Payment Mode '.$paymentModeOption_name.' and Currency '.$currency_name));

                    $this->forward($this->moduleName,$this->actionName);//paymentModeOption

            }
        }


    }

    public function executeUpdateConsolidationAccSetup(sfWebRequest $request){

        //checking direct hit
        //$this->checkIsSessionSet(10);

        //removing previous session

        $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');
        $selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $this->splitType = array(2 => 'Percentage',1=>'Flat' );
        $ConsolidationArr = array();
        foreach($selectedCurr as $currencykey => $currncyVal)
            {
                foreach($this->splitType as $splitKey => $splitVal)
                    {
                        $splitEntityObjs = Doctrine::getTable('SplitEntityConfiguration')->getDetailForAccountSetup($merchantServiceId,$currencykey,$splitKey);


                        if(count($splitEntityObjs)>0)
                        {

                        $count = 1;

                        foreach($splitEntityObjs as $splitEntityObj) {


                        $currencyObj = Doctrine::getTable('CurrencyCode')->find($currencykey);
                        $currencyArray[$currencyObj->getId()] = $currencyObj->getCurrency();


                        $ConsolidationArr[$count][$currencykey][$splitKey]['accParty']=$splitEntityObj['SplitAccountConfiguration']['account_party'];
                        $ConsolidationArr[$count][$currencykey][$splitKey]['accName']=$splitEntityObj['SplitAccountConfiguration']['account_name'];
                        $ConsolidationArr[$count][$currencykey][$splitKey]['accNumber']=$splitEntityObj['SplitAccountConfiguration']['account_number'];
                        $ConsolidationArr[$count][$currencykey][$splitKey]['bankName']=$splitEntityObj['SplitAccountConfiguration']['bank_name'];
                        $ConsolidationArr[$count][$currencykey][$splitKey]['sortcode']=$splitEntityObj['SplitAccountConfiguration']['sortcode'];
                        $ConsolidationArr[$count][$currencykey][$splitKey]['charge']=$splitEntityObj['charge'];
                        $ConsolidationArr[$count][$currencykey][$splitKey]['splitTypeId']=$splitKey;
                        $ConsolidationArr[$count][$currencykey][$splitKey]['accId']='';
                        $ConsolidationArr[$count][$currencykey][$splitKey]['splitEnittyConfigId']=$splitEntityObj['id'];
                        $ConsolidationArr[$count][$currencykey][$splitKey]['splitAccConfigId']=$splitEntityObj['SplitAccountConfiguration']['id'];

                   $count++;
                   }


                        }
                    }
            }

        $this->ConsolidationArr=$ConsolidationArr;
        $this->selectedCurr=$selectedCurr;
            if($request->isMethod('post') ){
                if( $request->getParameter('save') || $request->getParameter('addmore'))
                {
                    $params =  $request->getPostParameters();
                    for($i=1;$i<$params['noOfAc'];$i++){
                           $splitEntityAccountObj =  Doctrine::getTable('SplitEntityConfiguration')->find($params['splitEnittyConfigId'.$i]);
                            $splitEntityAccountObj->setCharge($params['split'.$i]);
                            $splitEntityAccountObj->save();

                           $splitAccountObj =  Doctrine::getTable('SplitAccountConfiguration')->find($params['splitAccConfigId'.$i]);
                           $accNumber = $splitAccountObj->getAccount_number();
                            $splitAccountObj->setAccount_party($params['accParty'.$i]);
                            $splitAccountObj->setAccount_number($params['accNo'.$i]);
                            $splitAccountObj->setAccount_name($params['accName'.$i]);
                            $splitAccountObj->setSortcode($params['sortCode'.$i]);
                            $splitAccountObj->setBank_name($params['bankName'.$i]);

                            $accNumber = $splitAccountObj->getAccount_number();
                            $splitAccountObj->save();


                           $EpMasterAccountObj =  Doctrine::getTable('EpMasterAccount')->findByAccountNumber($accNumber);

                           foreach($EpMasterAccountObj as $acObj)
                           {
                                $acObj->setAccount_number($params['accNo'.$i]);
                                $acObj->setAccount_name($params['accName'.$i]);
                                $acObj->setSortcode($params['sortCode'.$i]);
                                $acObj->setBankname($params['bankName'.$i]);
                                $acObj->save();
                           }
                    }

                  }//else save
//                $selectedCurrDiff = array_diff_key($selectedCurr, $currencyArray);
                $this->getUser()->setAttribute('AddedCurrencyForConsolidation',$currencyArray);
                if (isset($params['addmore'])) {
                        $this->redirect($this->getModuleName().'/addConsolidationAccSetup');
                    } else {
                        $this->redirect('merchantWizard/listMerchantWizard');
                }

            }//else post
            else{
                $this->getUser()->getAttributeHolder()->remove('consolidationAccDetails');
            }

    }
    public function executeAddConsolidationAccSetup(sfWebRequest $request){
        $this->setTemplate('consolidationAccSetup');
        $selectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
        $merchantServiceId = $this->getUser()->getAttribute('merchantServiceId', '0');
        $this->selectedCurr = $selectedCurr;
        $AddedCurrencyForConsolidation = $this->getUser()->getAttribute('AddedCurrencyForConsolidation');

        $this->redirctTo = "addConsolidationAccSetup";
        if($AddedCurrencyForConsolidation)
             //$this->selectedCurr = array_diff_key($this->selectedCurr, $AddedCurrencyForConsolidation);

            $this->splitType = array(2 => 'Percentage',1=>'Flat' );

            if($request->isMethod('post') ){
                if( $request->getParameter('save'))
                {

                    $consolidationAcc_params = $request->getPostParameters();
//  print_r($consolidationAcc_params);

                    if($AddedCurrencyForConsolidation && in_array($consolidationAcc_params['currency'],$AddedCurrencyForConsolidation))
                        $this->getUser()->setFlash('error', sprintf('Receiving (Consolidation) Account  have been already added for '.$selectedCurr[$consolidationAcc_params['currency']]));
                    else{


                    $consolidationAccDetails = array();
                    for($i=0;$i<=$consolidationAcc_params['totalRec'];$i++){
                        if(isset($consolidationAcc_params['accParty'.$i])){
                            $consolidationAccDetails[$i] = $conTemp = array('currency'=>$consolidationAcc_params['currency'],'accParty' => $consolidationAcc_params['accParty'.$i], 'accName' => $consolidationAcc_params['accName'.$i], 'accType' => 'receiving', 'accNo' => $consolidationAcc_params['accNo'.$i], 'sortCode' => $consolidationAcc_params['sortCode'.$i], 'split' => $consolidationAcc_params['split'.$i], 'splitTypeId' => $consolidationAcc_params['splitType'], 'bankName' => $consolidationAcc_params['bankName'.$i]);
                            $this->addConsolidationAcc($conTemp,$merchantServiceId);
                        }
                    }

//                     $AddedCurrencyForConsolidation[] = $consolidationAcc_params['currency'];
                     $currencyObj = Doctrine::getTable('CurrencyCode')->find($consolidationAcc_params['currency']);
                     $AddedCurrencyForConsolidation[$consolidationAcc_params['currency']] = $currencyObj->getCurrency();
                     $this->getUser()->setAttribute('AddedCurrencyForConsolidation', $AddedCurrencyForConsolidation);
                     if(count($selectedCurr)==count($AddedCurrencyForConsolidation))
                        $this->redirect('merchantWizard/listMerchantWizard');
                     $request->setParameter('save', false);
                     $this->getUser()->setFlash('notice', sprintf('Receiving (Consolidation) Account  have been added for '.$selectedCurr[$consolidationAcc_params['currency']]));
                     $this->forward($this->moduleName,$this->actionName);

                    }//else consolidation
                  }//else save
            }//else post
            else{
//                $this->getUser()->getAttributeHolder()->remove('AddedCurrencyForConsolidation');
            }

    }


private function addConsolidationAcc($conslidateVal,$merchantServiceId)
{
                $EpMasterAccountObj1=new EpMasterAccount();
                $EpMasterAccountObj1->setAccount_number($conslidateVal['accNo']);
                $EpMasterAccountObj1->setAccount_name($conslidateVal['accName']);
                $EpMasterAccountObj1->setType($conslidateVal['accType']);
                $EpMasterAccountObj1->setSortcode($conslidateVal['sortCode']);
                $EpMasterAccountObj1->setBankname($conslidateVal['bankName']);

                $EpMasterAccountObj1->save();
                $EpMasterAccountId1=$EpMasterAccountObj1->id;


                $EpMasterAccountObj2=new EpMasterAccount();
                $EpMasterAccountObj2->setAccount_number($conslidateVal['accNo']);
                $EpMasterAccountObj2->setAccount_name($conslidateVal['accName']);
                $EpMasterAccountObj2->setType($conslidateVal['accType']);
                $EpMasterAccountObj2->setSortcode($conslidateVal['sortCode']);
                $EpMasterAccountObj2->setBankname($conslidateVal['bankName']);
                $EpMasterAccountObj2->save();
                $EpMasterAccountId2=$EpMasterAccountObj2->id;


                $VirtualActConfigObj=new VirtualAccountConfig();
                $VirtualActConfigObj->setVirtual_account_id($EpMasterAccountId1);
                $VirtualActConfigObj->setPhysical_account_id($EpMasterAccountId2);
                $VirtualActConfigObj->setPayment_flag(0);
                $VirtualActConfigObj->save();



                $SplitActConfigObj=new SplitAccountConfiguration();
                $SplitActConfigObj->setAccount_party($conslidateVal['accParty']);
                $SplitActConfigObj->setAccount_name($conslidateVal['accName']);
                $SplitActConfigObj->setAccount_number($conslidateVal['accNo']);
                $SplitActConfigObj->setSortcode($conslidateVal['sortCode']);
                $SplitActConfigObj->setBank_name($conslidateVal['bankName']);
                $SplitActConfigObj->setMaster_account_id($EpMasterAccountId1);
                $SplitActConfigObj->setCurrency_id($conslidateVal['currency']);
                $SplitActConfigObj->save();
                $SplitActConfigId=$SplitActConfigObj->id;

                $VirtualActConfigObj=new SplitEntityConfiguration();
                $VirtualActConfigObj->setMerchant_service_id($merchantServiceId);
                $VirtualActConfigObj->setSplit_type_id($conslidateVal['splitTypeId']);
                $VirtualActConfigObj->setCharge($conslidateVal['split']);
                $VirtualActConfigObj->setSplit_account_configuration_id($SplitActConfigId);
                $VirtualActConfigObj->setMerchant_item_split_column('split_one');
                $VirtualActConfigObj->save();

}
    /*
    * function : executeMySqlImportMerchant()
    * @param : sfWebRequest $request
    * Purpose : import query from sql file and replace with Merchant Constant
    * Date : 14-03-2011
    * Author : R Yadav
    * Updated On :
    *
    *
    */

    private function mySqlImportMerchant($OldMerchantId)
    {

        // $OldMerchantId=1;
        $OldObjMerchant = Doctrine::getTable('Merchant')->find($OldMerchantId);
        $strExistingMerchantName = $OldObjMerchant->getName();

         $pos=strrpos($strExistingMerchantName,'-V');
         if(empty($pos))
         {
            $newMerchantName = $strExistingMerchantName;
            $strMerchantName=$strExistingMerchantName."-V1.0";
            $vcount=1;
         }
         else
         {
             $stArr = explode('-V',$strExistingMerchantName);
             $versionString = substr($strExistingMerchantName,$pos,strlen($strExistingMerchantName));
             $versionNum = explode('-V',$versionString);
             $versionNum =  $versionNum[1];
             $vcount = $versionNum;
             $newMerchantName = substr($strExistingMerchantName,0,$pos);
             $strMerchantName = $newMerchantName."-V".$versionNum;
         }

       $merchant_count = Doctrine::getTable('Merchant')->getTotalMerchant($strMerchantName);

        while($merchant_count!=0){
                $vcount++;
//                echo $vcount."<br>";
                $strMerchantName=$newMerchantName."-V".$vcount.".0";
                $merchant_count = Doctrine::getTable('Merchant')->getTotalMerchant($strMerchantName);
        }
//        $arrAllMerchnat=Doctrine::getTable('Merchant')->getAllMerchantName($strExistingMerchantName);
//        $strMerchantName=   $arrAllMerchnat->getFirst()->getName();
//
//        $pos=strrpos($strMerchantName,'-V');
//
//
//        if(empty($pos)){
//            $strMerchantName=$strMerchantName."-V1.0";
//
//            $merchant_count = Doctrine::getTable('Merchant')->getTotalMerchant($strMerchantName);
//            $vcount=1;
//            while($merchant_count!=0){
//                $vcount++;
//                $strMerchantName=$strMerchantName."-V".$vcount.".0";
//                $merchant_count = Doctrine::getTable('Merchant')->getTotalMerchant($strMerchantName);
//            }
//        }else{
//
//            if(!empty($strMerchantName)){
//                $arrExplode=  explode('-V', $strMerchantName);
//                $strMerchantName =$arrExplode[1]+1;
//
//                $strMerchantName= $arrExplode[0]."-V".number_format($strMerchantName,1);
//            }



//        }
//die;

        $OldService = Doctrine::getTable('MerchantService')->findByMerchantId($OldMerchantId);
        $intTotalService=$OldService->count();


        $objMerchant =Doctrine::getTable('Merchant')->getMerchantAutoIncrementId();
        $objMerchantService =Doctrine::getTable('MerchantService')->getMerchantServiceAutoIncrementId();
        $intMerchantId= $objMerchant[0]['auto']+1;
        $intMerchantServiceId= $objMerchantService[0]['auto']+1;
        $merchantWizardObj= new merchantWizard();
        $getOldSql = $merchantWizardObj->getSqlForMerchant($OldMerchantId);

        //replcae merchant id
        $getNewSql  = str_replace('MERCHANT_ID',$intMerchantId,$getOldSql );
        // replace mercahnt name
        $getNewSql  = str_replace('name="'.$strExistingMerchantName.'"','name="'.$strMerchantName.'"',$getNewSql );

        for($i=1;$i<=$intTotalService;$i++){
            $strMerchantService ='MERCHANT_SERVICE_ID'.$i;
            // replace merchant service name
            $getNewSql  = str_replace($strMerchantService,$intMerchantServiceId,$getNewSql );
            $intMerchantServiceId++;
        }

        //str replacement for ep master account id
        //get ep_mster _account_id last
        // $walletObj = new EpAccountingManager;
        $epMasterAccountId = Doctrine::getTable('SplitAccountConfiguration')->getEpMasterAccountIncrementId();
        $intMerchantAccountId= $epMasterAccountId[0]['auto']+1;

        $countStrEpMaster =substr_count($getNewSql, 'EP_MASTER_ACCOUNT_ID');

        for($intI=1;$intI<=$countStrEpMaster;$intI++){

            $strEmMasterAccount = 'EP_MASTER_ACCOUNT_ID'.$intI.",";
            $pos=strrpos($getNewSql,$strEmMasterAccount);

            if(!empty($pos)){

                $getNewSql  = str_replace($strEmMasterAccount,$intMerchantAccountId.",",$getNewSql );
                $intMerchantAccountId++;
            }
            else {
                break;
            }

        }


        //str replacement for ep master account id
        $epSplitAccountId = Doctrine::getTable('SplitAccountConfiguration')->getSplitAccountIncrementId();
        $inSplitAccountId= $epSplitAccountId[0]['auto']+1;


        $countStrSplit =substr_count($getNewSql,'SPLIT_ACCOUNT_CONIFIGURATION_ID');
        for($intI=1;$intI<=$countStrSplit;$intI++){

            $strSplitAccount = 'SPLIT_ACCOUNT_CONIFIGURATION_ID'.$intI.",";
            $pos=strrpos($getNewSql,$strSplitAccount);

            if(!empty($pos)){

                $getNewSql  = str_replace($strSplitAccount,$inSplitAccountId.",",$getNewSql);
                $inSplitAccountId++;
            }else{
                break;

            }



        }
;
        // Connects to your Database
        $dbArr =DBInfo::getDSNInfo();
        mysql_connect($dbArr['host'],$dbArr['username'],$dbArr['password']) or die(mysql_error());
        mysql_select_db($dbArr['dbname']) or die(mysql_error());


        $sql = explode("; ", $getNewSql);

        foreach ($sql as $key => $val) {
            if($val!='')
            mysql_query($val) or die(mysql_error());
        }

        mysql_close();

        return $intMerchantId;


    }




 public function executeViewDetail(sfWebRequest $request){
     $this->setLayout('layout_popup');
     $serviceId = $request->getParameter('id');
     $this->serviceDetailObj = Doctrine::gettable('MerchantService')->getServiceDetail($serviceId);

 }
}
?>
