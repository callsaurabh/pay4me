<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class merchantWizardComponents extends sfComponents
{
  
  public function executeCurrencyPaymentmode(sfWebRequest $request)
  {
       $SelectedCurr = $this->getUser()->getAttribute('MCurrency', 0);
       $SelectedPayMode = $this->getUser()->getAttribute('payment_mode_option', 0);
     
      
       if($this->getSession)
            {
                $this->getSession=$this->getSession;
                $getSession = $this->getUser()->getAttribute($this->getSession,'0');
                 
                if(is_array($getSession))
                {
                    foreach($getSession as $sessVal)
                    {
                        if(isset($SelectedPayMode[$sessVal['currency']][$sessVal['paymentMode']]))
                            unset($SelectedPayMode[$sessVal['currency']][$sessVal['paymentMode']]);
                        if(count($SelectedPayMode[$sessVal['currency']])==0)
                            unset($SelectedPayMode[$sessVal['currency']]);
                    }
                }
            }
        else
            $this->getSession='';
 
       foreach($SelectedCurr as $key=>$value)
        {
            if(!array_key_exists($key,$SelectedPayMode))
                unset($SelectedCurr[$key]);
        }

        
        
      $this->form = new CurrencyPaymentmodeForm(null,array('currency'=>$SelectedCurr ));
  }

}

?>
