<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $split_entity_configuration->getid() ?></td>
    </tr>
    <tr>
      <th>Merchant service:</th>
      <td><?php echo $split_entity_configuration->getmerchant_service_id() ?></td>
    </tr>
    <tr>
      <th>Split type:</th>
      <td><?php echo $split_entity_configuration->getsplit_type_id() ?></td>
    </tr>
    <tr>
      <th>Charge:</th>
      <td><?php echo $split_entity_configuration->getcharge() ?></td>
    </tr>
    <tr>
      <th>Split account configuration:</th>
      <td><?php echo $split_entity_configuration->getsplit_account_configuration_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $split_entity_configuration->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $split_entity_configuration->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $split_entity_configuration->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $split_entity_configuration->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $split_entity_configuration->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('splitEntityConf/edit?id='.$split_entity_configuration->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('splitEntityConf/index') ?>">List</a>
