<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
 <div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Split Entity Configuration List'); ?>
    </fieldset>
</div>
  <div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
    <br class="pixbr" />
  </div>

<table class="tGrid">
  <thead>
    <tr>
      <th width = "2%">S.No.</th>
      <th>Merchant service</th>
      <th>Split type</th>
      <th>Charge</th>
      <th>Split account configuration</th>
      <th>Merchant Split Column</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
     <?php
     if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_per_page_records');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
    ?>
    <tr>
      <td><?php echo $i ?></td>
      <td align="center"><?php echo $result->getMerchantServiceName() ?></td>
      <td align="center"><?php echo $result->getSplitTypeName() ?></td>
      <td align="center"><?php echo $result->getCharge() ?></td>
      <td align="center"><?php echo $result->getSplitAccountConfigurationName() ?></td>
      <td align="center"><?php echo $result->getMerchantItemSplitColumn() ?></td>
      <td align="center"><?php echo link_to(' ', 'splitEntityConf/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
    </tr>
    <?php endforeach;?>

        <tr><td colspan="7">
<div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMiddle')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

  </div></td></tr>
    <?php }else{ ?>
     <tr><td  align='center' class='error' colspan="6">No Split Entity Configuration found</td></tr>
    <?php } ?>

  </tbody>
   <tfoot><tr><td colspan="7"></td></tr></tfoot>
</table>

<?php  echo button_to('Add New','',array('onClick'=>'location.href=\''.url_for('splitEntityConf/new').'\''));?>
