<?php

/**
 * splitEntityConf actions.
 *
 * @package    mysfp
 * @subpackage splitEntityConf
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class splitEntityConfActions extends sfActions
{
  public function executeIndex(sfWebRequest $request) {
        $split_entity_configuration_obj = splitEntityConfServiceFactory::getService(splitEntityConfServiceFactory::$TYPE_BASE);
        $this->split_entity_configuration_list = $split_entity_configuration_obj->getAllRecords();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('SplitEntityConfiguration',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->split_entity_configuration_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

  public function executeShow(sfWebRequest $request)
  {
    $this->split_entity_configuration = Doctrine::getTable('SplitEntityConfiguration')->find($request->getParameter('id'));
    $this->forward404Unless($this->split_entity_configuration);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new SplitEntityConfigurationForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new SplitEntityConfigurationForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($split_entity_configuration = Doctrine::getTable('SplitEntityConfiguration')->find($request->getParameter('id')), sprintf('Object split_entity_configuration does not exist (%s).', $request->getParameter('id')));
    $this->form = new SplitEntityConfigurationForm($split_entity_configuration);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($split_entity_configuration = Doctrine::getTable('SplitEntityConfiguration')->find($request->getParameter('id')), sprintf('Object split_entity_configuration does not exist (%s).', $request->getParameter('id')));
    $this->form = new SplitEntityConfigurationForm($split_entity_configuration);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($split_entity_configuration = Doctrine::getTable('SplitEntityConfiguration')->find($request->getParameter('id')), sprintf('Object split_entity_configuration does not exist (%s).', $request->getParameter('id')));
    $split_entity_configuration->delete();

    $this->redirect('splitEntityConf/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
        $split_entity_configuration = $form->save();
        if(($request->getParameter('action')) == "update") {
            $this->getUser()->setFlash('notice', sprintf('Split Entity Configuration details updated successfully'));
        }else if(($request->getParameter('action')) == "create") {
                $this->getUser()->setFlash('notice', sprintf('Split Entity Configuration added successfully'));
        }

        $this->redirect('splitEntityConf/index');
    }
  }
}
