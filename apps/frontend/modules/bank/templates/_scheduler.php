<dl id='start_time_row'>
    <div class="dsTitle4Fields"><?php echo $form['start_time']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['start_time']->render(); ?>
        <br><br><div class="cRed" id="err_start_time"></div>
    </div>
</dl>

<dl id='end_time_row'>
    <div class="dsTitle4Fields"><?php echo $form['end_time']->renderLabel(); ?></div>
    <div class="dsInfo4Fields"><?php echo $form['end_time']->render(); ?>
        <br><br><div class="cRed" id="err_end_time"></div></div>
</dl>

<dl id="status_row"><div class="dsTitle4Fields"><?php echo $form['status']->renderLabel(); ?></div>
    <div class="dsInfo4Fields">
        <?php echo $form['status']->render(); ?><br><br><div class="cRed" id="err_status"></div></div></dl>

<?php echo $form['type']->render(); ?>