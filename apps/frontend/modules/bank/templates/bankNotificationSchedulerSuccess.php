<?php //use_helper('DateForm'); ?>
<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<div class="wrapForm2">

    <?php echo ePortal_legend('Bank Notification Scheduler'); ?>

    <?php echo form_tag('bank/submitBankNotificationScheduler', 'id="bank_time_scheduler" name="bank_time_scheduler"'); ?>


    <?php include_partial('scheduler', array('form' => $bank_notific_scheduler_form)) ?>

    <dl>
        <div class="dsTitle4Fields">&nbsp;</div>
        <div class="dsInfo4Fields"><?php echo button_to('Submit', '', array('class' => 'formSubmit', 'onClick' => 'validateUserForm("1")')); ?></div>
    </dl>


    <div class="clear"></div>
</div>

<script>
    function validateUserForm(submit_form)
    {
        var err = 0;
        
        var today=new Date();
        var start = new Date();
        start.setHours($('#start_time_hour').val(),$('#start_time_minute').val(),$('#start_time_second').val(),0);
        var end = new Date();
        end.setHours($('#end_time_hour').val(),$('#end_time_minute').val(),$('#end_time_second').val(),0);
          
        if((start.getHours()<=today.getHours() && start.getMinutes()<=today.getMinutes() &&   start.getSeconds()<=today.getSeconds()))
        {
            $('#err_start_time').html("Cannot select Past/Same Time");
            err++;
        }
        else
        {        
            
           
            $('#err_start_time').html("");
        

        }

        if((end.getHours()<=today.getHours() && end.getMinutes()<=today.getMinutes() &&   end.getSeconds()<today.getSeconds()))
        {


            $('#err_end_time').html("Cannot select Past/Same Time");
            err++;
        }
        else
        {

            $('#err_end_time').html("");

        }
        if(err == 0 )
        {  if(start>end)
            {
                $('#err_end_time').html("End Time cannot be less than Start Time");

                       
            }
            else
            {
                $('#err_end_time').html('');
            }

        }
        if($('#status').val()=='')
        {
            err++;

            $('#err_status').html('Please Select Status');
        }
        else
        {
            $('#err_status').html('');


        }
        if(err == 0)
        {
            if(submit_form == 1)
            {
                $('#bank_time_scheduler').submit();
            }
        }
    }
</script>