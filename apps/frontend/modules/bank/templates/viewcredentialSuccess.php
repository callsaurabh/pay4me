<?php echo ePortal_pagehead(" "); ?>
<?php  //use_helper('Form') ?>
<div class="wrapForm2">
    <form>
        <?php echo ePortal_legend('View Credential Section');?>
        <dl id=><div class="dsTitle4Fields"><label for="bank">Bank Name:</label></div>
            <div class="dsInfo4Fields">
        <div id="bank_name"><?php echo $bankName; ?></div></div></dl>
        <dl id=><div class="dsTitle4Fields"><label for="bank">Bank Code:</label></div>
            <div class="dsInfo4Fields">
        <div id="bank_name"><?php echo $bankCode; ?></div></div></dl>
        <dl id=><div class="dsTitle4Fields"><label for="bank">Bank Key:</label></div>
            <div class="dsInfo4Fields">
        <div id="bank_name"><?php echo $bankKey; ?></div></div></dl>
        <dl id="">
            <div class="dsTitle4Fields">
                    <?php echo $form['user_group']->renderLabel(); ?>
               </div>
            <div class="dsInfo4Fields">
                 <div id="user_group" >
                        <?php echo $form['user_group']->render(); ?>
                 </div>
            </div>
        </dl>
        <dl id=><div class="dsTitle4Fields"><label for=>User Name:</label></div>
            <div class="dsInfo4Fields">
        <div id="user_name" ></div></div></dl>

        <dl id=><div class="dsTitle4Fields"><label for=>Asymmetric Key:</label></div><div class="dsInfo4Fields">
        <div id="asymmetric_key"></div></div></dl>

        <dl id=><div class="dsTitle4Fields"><label for=>Encrypted Credential:<br><br></label></div><div class="dsInfo4Fields">
        <div id="enycrypted_credential"></div></div></dl>
       <br>     
        <div class="divBlock">
            <br>
            <center>
                <?php echo $form->renderHiddenFields(); ?>
                <?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('bank/index').'\''));?>
            </center>
        </div>
        <br>

    </form>
</div>
<script>
    $(document).ready(function() {

        if($("#credentail_user_group").val()!=''){
            var url = "<?php echo url_for("bank/fetchCredentail");?>";
            var bank_id =  $('#credentail_bank_id').val();
            var group_id =  $('#credentail_user_group').val();

            $.post(url,{bank_id:bank_id,group_id:group_id},function (data){
                dataArray = data.split('###');
                if(dataArray[0]=="sucess"){
                    $("#user_name").html(dataArray[1]);
                    $("#asymmetric_key").html(dataArray[2]);
                    $("#enycrypted_credential").html(dataArray[3]);
                }else{
                    location.reload();
                }

            });

        }else{

            $("#user_name").html("--");
            $("#asymmetric_key").html("--");
            $("#enycrypted_credential").html("--");
        }




        $("#credentail_user_group").change(function()
        {
            var url = "<?php echo url_for("bank/fetchCredentail");?>";
            var bank_id =  $('#credentail_bank_id').val();//alert(bank)
            var group_id =  $('#credentail_user_group').val();//alert(bank)
            if(group_id!=''){
                $.post(url,{bank_id:bank_id,group_id:group_id},function (data){
                    dataArray = data.split('###');
                    if(dataArray[0]=="sucess"){


                        $("#user_name").html(dataArray[1]);
                        $("#asymmetric_key").html(dataArray[2]);
                        $("#enycrypted_credential").html(dataArray[3]);
                    }else{
                        location.reload();
                    }


                });
            }else{

                $("#user_name").html("--");
                $("#asymmetric_key").html("--");
                $("#enycrypted_credential").html("--");

            }

        });

    });

</script>