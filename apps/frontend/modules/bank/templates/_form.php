<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php //use_helper('Asset') ?>

<div class="wrapForm2">

<?php //echo ePortal_legend('Add New Bank'); ?>


<form action="<?php echo url_for('bank/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  id="pfm_bank_form" name="pfm_bank_form"  enctype="multipart/form-data">
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>

        <?php echo $form ?>
        <dl id="bank_image1_row">
          <div class="dsTitle4Fields"><label for="bank_domain">Bank Logo<br />(login page Logo)<sup>*</sup></label></div>          
            <div class="dsInfo4Fields">
              <input type="file" id="bank_image1" name="bank[image1]">
              <div class="help"><?php if(($image_name !='') && is_file('images/'.$image_name)){echo image_tag('/images/'.$image_name,$option=Array());} ?>(Please upload '.jpeg or .gif or .jpg' Image only.)<br /></div>
              <div id="img1" class="error"></div>
          </div>          
        </dl>

        <dl id="bank_image2_row">
          <div class="dsTitle4Fields"><label for="bank_domain">Bank Logo<br />(Logo shown at the time of payment)<sup>*</sup></label></div>
          
            <div class="dsInfo4Fields">
              <input type="file" id="bank_image2" name="bank[image2]">
              <div class="help"><!--(Please upload '.jpeg or .gif or .jpg' Image only.)<br /><br />-->
              <?php
         //     $image_path =  _compute_public_path("ico_".$image_name, 'images','','true');
              if(($image_name !='') && is_file('images/ico_'.$image_name)){echo image_tag('/images/ico_'.$image_name,$option=Array());} ?>(Please upload '.jpeg or .gif or .jpg' Image only.)<br />
              </div>
              <div id="img2" class="error"></div>
          </div>
          
        </dl>

        <dl id="bank_image3_row">
          <div class="dsTitle4Fields"><label for="bank_domain">Bank Logo<br />(Bank area header Logo)<sup>*</sup></label></div>
          
            <div class="dsInfo4Fields">
              <input type="file" id="bank_image3" name="bank[image3]">
              <div class="help"><!--(Please upload '.jpeg or .gif or .jpg' Image only.)<br /><br />-->
              <?php
             // $image_path =  _compute_public_path("tm_".$image_name, 'images','','true');
              if(($image_name !='') && is_file('images/tm_'.$image_name)){echo image_tag('/images/tm_'.$image_name,$option=Array()); }?>(Please upload '.jpeg or .gif or .jpg' Image only.)<br />
              </div>
              <div id="img3" class="error"></div>
          </div>
          
        </dl>
        
        <div class="divBlock">
            <center>
                &nbsp;<?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('bank/index').'\''));?>
                <input type="submit" value="Save" class="formSubmit" />
        </center></div>

</form>
<div class="clear"></div>
</div>
</div>