<div id="theMid">
    <?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
    <?php use_helper('Pagination'); ?>



    <?php echo ePortal_listinghead('Bank List'); ?>
    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr class="alternateBgColour">
                <th width="100%" >
                    <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                    <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                </th>
            </tr>
        </table>
        <br class="pixbr" />
    </div>

    <div class="wrapTable" >
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <thead>
                <tr class="horizontal">
                    <th width = "2%">S.No.</th>
                    <th>Bank name</th>
                    <th>Acronym</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (($pager->getNbResults()) > 0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $page = $sf_context->getRequest()->getParameter('page', 0);
                    $i = max(($page - 1), 0) * $limit;
                    foreach ($pager->getResults() as $result):
                        $i++;
                ?>
                        <tr class="alternateBgColour">
                            <td><?php echo $i ?></td>
                            <td align="center"><?php echo $result->getBankName() ?></td>
                            <td align="center"><?php echo $result->getAcronym() ?></td>
                            <td align="center"><?php echo link_to(' ', 'bank/edit?id=' . $result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
                        <?php echo link_to(' ', 'bank/delete?id=' . $result->getId(), array('method' => 'delete', 'confirm' => 'Deleted bank cannot be re-created. Are you sure, you want to delete bank?', 'class' => 'delInfo', 'title' => 'Delete')) ?>
                        <?php
                        if ($result->getStatus() == 1)://bank branch is active; enabled

                            echo link_to(' ', 'bank/setStatus?status=deactivate&id=' . $result->getId(), array('method' => 'get', 'class' => 'deactiveInfo', 'title' => 'Deactivate'));

                        endIf;

                        if ($result->getStatus() == 0): //bank branch is inactive; disabled
                            echo link_to(' ', 'bank/setStatus?status=activate&id=' . $result->getId(), array('method' => 'get', 'class' => 'activeInfo', 'title' => 'Activate'));


                        endIf;
                        if ($result->getBiProtocolVersion() == 'v1'):
                            echo "&nbsp;&nbsp;" . link_to(image_tag('view_credentials.gif', array('align' => "center")), 'bank/viewcredential?id=' . $result->getId(), array('title' => 'View Credentials'));
                        endIf;
                        ?></td>
                </tr>
                <?php endforeach; ?>

                        <tr><td colspan="4">
                                <div class="paging pagingFoot"><?php echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id')))   ?>

                                </div></td></tr>
                <?php }else {
 ?>
                        <tr><td  align='center' class='error' colspan="4">No Bank found</td></tr>
<?php } ?>

                </tbody>
                 <!--tfoot><tr><td colspan="5"></td></tr></tfoot-->
            </table>
        </div>

<?php echo button_to('Add New', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for('bank/new') . '\'')); ?>
</div></div>