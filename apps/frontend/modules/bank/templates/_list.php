<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
    <?php  echo formRowFormatRaw($form[$bankId]->renderLabel(),$form[$bankId]->render());?>
    <?php
        if('country'!=$not_needed)
           echo formRowFormatRaw($form[$countryId]->renderLabel(),$form[$countryId]->render());?>
    <?php 
    if('branch'!=$not_needed &&  'country'!=$not_needed)
        echo formRowFormatRaw($form[$branchId]->renderLabel(),$form[$branchId]->render());
        ?>
<script>
    $(document).ready(function()
    {
        $("#<?php echo $bankId;?>").select(function (){
                $("#"+<?php echo $bankId;?>).change()
            });

            $("#<?php echo $bankId;?>").change(function()
            {
                <?php
                if('country'!=$not_needed)
                {
                  ?>
                var bank = $(this).val();
               if(!bank)
               {
                    var country =  $('#<?php echo $countryId;?>').val();

                    <?php if('branch'!=$not_needed)
                        {
                    ?>
                        var url1 = "<?php echo url_for("epActionAuditEventReports/getBankBranches");?>";

                        $('#loader').show();
                        $("#<?php echo $branchId;?>").load(url1, { bank: bank,country:country,byPass:1},function (data){
                            if(data=='logout'){
                                location.reload();
                            }
                            $('#loader').hide();
                        });
                    <?php
                        }
                    ?>
               }

                var url = "<?php echo url_for("epActionAuditEventReports/getBankCountry");?>";

                $('#loader').show();
                $("#<?php echo $countryId;?>").load(url, { bank: bank,byPass:1},function (data){
                    if(data=='logout'){
                        location.reload();
                    }
                    $('#loader').hide();
                });

                $('#<?php echo $branchId;?> >option').remove();
                options = $('#<?php echo $branchId;?>');
                options.append($("<option />").val("").text("--All Branches--"));
            });


            $("#<?php echo $countryId;?>").change(function()
            {
                <?php if('branch'!=$not_needed)
                {
                    ?>

                    var country = $(this).val();
                    var bank =  $('#<?php echo $bankId;?>').val();

                    //bank = bank.toString();
                    var url = "<?php echo url_for("epActionAuditEventReports/getBankBranches");?>";

                    $('#loader').show();
                    $("#<?php echo $branchId;?>").load(url, { bank: bank,country:country,byPass:1},function (data){
                        if(data=='logout'){
                            location.reload();
                        }
                        $('#loader').hide();
                    });
                 <?php }//not_needed branch?>
              <?php }//not_needed country?>
            });
             
      });

</script>