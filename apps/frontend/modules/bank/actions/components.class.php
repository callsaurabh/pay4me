<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class bankComponents extends sfComponents
{

  public function executeList(sfWebRequest $request)
  {

      if($this->bank)
          $this->bankId = $this->bank;
      else
        $this->bankId ='bank';
      if($this->country)
          $this->countryId = $this->country;
      else
        $this->countryId ='country';

      if($this->branch)
          $this->branchId = $this->branch;
      else
        $this->branchId ='branch';

       $this->not_needed = $this->not_needed;
       if(isset($this->ewalletreport)){
         $this->form = new BankCountryBranchForm(array(),array('bank'=>$this->bankId,'country'=>$this->countryId,'branch'=>$this->branchId,'not_needed'=>$this->not_needed,'ewalletreport'=>'ewalletreport'));
       }
       else
         $this->form = new BankCountryBranchForm(array(),array('bank'=>$this->bankId,'country'=>$this->countryId,'branch'=>$this->branchId,'not_needed'=>$this->not_needed));
  }

}

?>
