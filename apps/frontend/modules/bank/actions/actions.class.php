<?php

/**
 * bank actions.
 *
 * @package    mysfp
 * @subpackage bank
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class bankActions extends sfActions {
    public function executeIndex(sfWebRequest $request) {
        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BASE);
        $this->bank_list = $bank_obj->getAllRecords();
        # Start Added by Iqbal at 13 Nov 2009 fixed bug 14395
        if(($this->getRequestParameter('flag')) == "delete") {
            $this->getUser()->setFlash('notice', sprintf('Bank deleted successfully'));
        }
        # End Added by Iqbal at 13 Nov 2009
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('Bank',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->bank_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeShow(sfWebRequest $request) {
        $this->bank = Doctrine::getTable('Bank')->find($request->getParameter('id'));
        $this->forward404Unless($this->bank);
    }

    public function executeNew(sfWebRequest $request) {

        /*
         * code added by ryadav wp041
         * toa dd key code and auth for query API
         * Date 25-07-2011
         */

        do{
          $bank_code = mt_rand();
          $duplicacy_code= Doctrine::getTable('Bank')->chkCodeDuplicacy($bank_code);
        } while ($duplicacy_code > 0);{

            $bank_key = mt_rand();
            $auth_info = base64_encode($bank_code .":" .$bank_key);

        }
        /* end here */

        $this->form = new BankForm();

         /*
         * code added by ryadav wp041
         * toa dd key code and auth for query API
          * Date 25-07-2011
         */
        $this->form->setDefault('bank_code', $bank_code);
        $this->form->setDefault('bank_key', md5($bank_key));
        $this->form->setDefault('auth_info', $auth_info);
          /* end here */

    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));
        $bankDet = $request->getPostParameter('bank');
        $newObj = new bankManager();

        $getValAcronym = $newObj->checkBankAcronym($bankDet['acronym']);
        $getVal = $newObj->checkBankExists($bankDet['bank_name']);

        if($getVal[0]['COUNT'] == '1'){
            //echo "test1.1"; die();
            $this->getUser()->setFlash('error', sprintf('This Bank has already been deleted by administrator'),false);
            $this->forward('bank', 'new');
            //$this->setTemplate('new');
        }else if ($getValAcronym[0]['COUNT'] == '1'){
            $this->getUser()->setFlash('error', sprintf('This Bank acronym has already been deleted by administrator'),false);
            $this->forward('bank', 'new');

        }else{
            //echo "test1.2"; die();
            $this->form = new BankForm();
            $this->image_name = $bankDet['acronym'].'.jpg';

            $this->processForm($request, $this->form);
            $this->setTemplate('new');
        }

    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($bank = Doctrine::getTable('Bank')->find($request->getParameter('id')), sprintf('Object bank does not exist (%s).', $request->getParameter('id')));

        if($bank['BankConfiguration'][0]['domain']!="") {
            $bank['BankConfiguration'][0]['domain'] = substr($bank['BankConfiguration'][0]['domain'], 1);//remove the first symbol of '@',for display
        }

        $this->form = new BankForm($bank);
        $this->image_name = $bank['acronym'].'.jpg';
        //    $data = $request->getPostParameters();
        //    print print_r($data);
        //
        //    exit;
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($bank = Doctrine::getTable('Bank')->find($request->getParameter('id')), sprintf('Object bank does not exist (%s).', $request->getParameter('id')));
        $newObj = new bankManager();
        $params = $request->getParameter('bank');
        $bankNanme = $params['bank_name'];
       // $countryId = $params['bankConfiguration']['country_id'];
        $acronym   = $params['acronym'];
       // echo $bankNanme.'----'.$countryId.'----'.$acronym;echo "<br/>";
       // echo $request->getParameter('bank[bank_name]').'----'.$request->getParameter('bank[country_id]').'----'.$request->getParameter('bank[acronym]');die;
       // $getVal = $newObj->checkBankExists($request->getParameter('bank[bank_name]'), $request->getParameter('bank[country_id]'));
       // $getValAcronym = $newObj->checkBankAcronym($request->getParameter('bank[acronym]'));
        $getVal = $newObj->checkBankExists($bankNanme);
        $getValAcronym = $newObj->checkBankAcronym($acronym);

        if($getVal[0]['COUNT'] == '1'){
            $this->getUser()->setFlash('error', sprintf('This Bank has already been deleted by administrator'));
            $this->redirect('bank/edit?id='.$request->getParameter('id'));
        }else if ($getValAcronym[0]['COUNT'] == '1'){
            $this->getUser()->setFlash('error', sprintf('This Bank acronym has already been deleted by administrator'));
            $this->redirect('bank/edit?id='.$request->getParameter('id'));
        }else{
            $this->form = new BankForm($bank);
            $this->image_name = $bank['acronym'].'.jpg';
            $this->processForm($request, $this->form);
        }

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($bank = Doctrine::getTable('Bank')->find($request->getParameter('id')), sprintf('Object bank does not exist (%s).', $request->getParameter('id')));
        $bank->delete();

        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKINFO,$bank->getBankName(),$bank->getId()));
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_DELETE,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_DELETE, array('bankname' => $bank->getBankName())),
            $applicationArr);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        $classObj = new sessionAuditManager();
        $classObj->doLogoutForcefully($bank->getId());

        $this->redirect('bank/index?flag=delete');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {

        // $form->set('domain',"@".$bank_details['domain']);

        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {

            $bank_details = $request->getParameter('bank') ;

//            if($bank_details['bankConfiguration']['domain']!="") {
//                $dns_details =  dns_get_record($bank_details['bankConfiguration']['domain']);
//            }
//            if(count($dns_details) == 0) {
//                $this->getUser()->setFlash('error', sprintf('Please enter a valid domain'));
//            }else

             if($this->checkImages($form , $_FILES)){
                $data = $request->getParameter($form->getName());

                //$data['bankConfiguration']['domain'] = '@'.$data['bankConfiguration']['domain'];//concat @ while storing in db
                $form->bind($data);


                //  $form->setDomain();

                $final_path = str_replace('uploads', 'images', sfConfig::get('sf_upload_dir'));
                $file_name1 =  $data['acronym'].".jpg";
                $file_name2 =  "ico_".$data['acronym'].".jpg";
                $file_name3 =  "tm_".$data['acronym'].".jpg";
                $path_to_upload1 = $final_path.'/'.$file_name1;
                $path_to_upload2 = $final_path.'/'.$file_name2;
                $path_to_upload3 = $final_path.'/'.$file_name3;
                $tmpfileName1 = $_FILES['bank']['tmp_name']['image1'];
                $tmpfileName2 = $_FILES['bank']['tmp_name']['image2'];
                $tmpfileName3 = $_FILES['bank']['tmp_name']['image3'];
                if(is_file($tmpfileName1)) {
                    $upload1 = move_uploaded_file($tmpfileName1, $path_to_upload1);
                    chmod($path_to_upload1, 0777);
                }
                if(is_file($tmpfileName2)) {
                    $upload2 = move_uploaded_file($tmpfileName2, $path_to_upload2);
                    chmod($path_to_upload2, 0777);
                }
                if(is_file($tmpfileName3)) {
                    $upload3 = move_uploaded_file($tmpfileName3, $path_to_upload3);
                    chmod($path_to_upload3, 0777);
                }




                $bank = $form->save();

                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKINFO,$bank->getBankName(),$bank->getId()));

                if(($request->getParameter('action')) == "update") {


                    $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_UPDATE,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_UPDATE, array('bankname' => $bank->getBankName())),
                        $applicationArr);

                    $this->getUser()->setFlash('notice', sprintf('Bank details updated successfully'));
                }
                else if(($request->getParameter('action')) == "create") {

                    $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_CREATE,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_CREATE, array('bankname' => $bank->getBankName())),
                        $applicationArr);

                        $this->saveBankCredential($bank->getId(),$bank->getAcronym());

                    $this->getUser()->setFlash('notice', sprintf('Bank added successfully<br>Kindly proceed to set-up <a href="setUpBankConfiguration/new/id/'.$bank->getId().'" style="color:#FFFFFF">bank configuration</a>'));
                }
                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                $this->redirect('bank/index');
            }
        }else{
            echo $form->renderGlobalErrors();
        }
    }

    private function saveBankCredential($bankId, $bankAcronym){

        $bankIntObj = new bankIntegrationHelper();
        $arrayGroup = sfConfig::get('app_bank_credential_group');
        $countGroup = count($arrayGroup);
        for($i=0;$i<$countGroup;$i++){
            $result =Doctrine::getTable('BankCredential')->getBankCredentailDetail($bankId,$arrayGroup[$i]);
            if(!$result){
                $length=12;
                $strength=8;
                $password = $bankIntObj->generatePassword($length,$strength);
                do{
                       $key =substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',5)),0,16);
                       $duplicacyKey= Doctrine::getTable('BankCredential')->chkKeyDuplicacy($key);
                }  while ($duplicacyKey > 0);
                
                Doctrine::getTable('BankCredential')->saveBankCredential($bankId,
                        $arrayGroup[$i],$bankAcronym,$password,$key);
            }
        }
    } 



    public function executeAddBankCredential(sfWebRequest $request) {
        
        $this->setLayout(null);
        $res = Doctrine::getTable('Bank')->getBIEnabledBankList();
        foreach($res as $bankObj) {
            $bankId = $bankObj->getId();
            $bankAcronym = $bankObj->getAcronym();
            $this->saveBankCredential($bankId,$bankAcronym);
        }
        return $this->renderText("Password and user name created successfully");
    }



    public function executeSetStatus(sfWebRequest $request) {
        $bankId = $request->getParameter('id');
        if($request->getParameter('status')!="") {
            $bank = Doctrine::getTable('Bank')->find(array($request->getParameter('id')));
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKINFO,$bank->getBankName(),$bank->getId()));
            if($request->getParameter('status') == "activate") {
                $bank->setStatus("1");
                $status_msg = "Activated";

                $eventHolder = new pay4meAuditEventHolder(
                    EpAuditEvent::$CATEGORY_TRANSACTION,
                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_ACTIVATE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_ACTIVATE, array('bankname' => $bank->getBankName())),
                    $applicationArr);

            }
            if($request->getParameter('status') == "deactivate") {
                $bank->setStatus("0");
                $status_msg = "Deactivated";

                $eventHolder = new pay4meAuditEventHolder(
                    EpAuditEvent::$CATEGORY_TRANSACTION,
                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_BANK_DEACTIVATE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BANK_DEACTIVATE, array('bankname' => $bank->getBankName())),
                    $applicationArr);
                $classObj = new sessionAuditManager();
                $classObj->doLogoutForcefully($bank->getId());

            }
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            $bank->save();
            $this->getUser()->setFlash('notice', sprintf('Bank '.$status_msg.' successfully'),TRUE);
            $this->redirect('bank/index');
            //  $this->redirect('bank_branch/index');
        }
    }
    private function checkImages($form , $_FILES)
    {
        if(($form->getObject()->isNew()=='1')&&($_FILES['bank']['tmp_name']['image1'] == '' || $_FILES['bank']['tmp_name']['image2'] == '' || $_FILES['bank']['tmp_name']['image3'] == ''))
        {
            $this->getUser()->setFlash('error', sprintf("Please upload all the three bank logo images"));
            return false;
        }

        for($i = 1; $i <= 3; $i++)
        {
            if($form->getObject()->isNew()=='1' || $_FILES['bank']['type']['image'.$i])
            {
                $arr = explode('/', $_FILES['bank']['type']['image'.$i]);
                if(strtolower($arr[0]) != 'image')
                {
                    $this->getUser()->setFlash('error', sprintf("Please upload only '.jpeg or .gif or .jpg' for all three logo images"));
                    return false;
                    break;
                }
            }
        }
        return true;
    }



    public function executeBankNotificationScheduler(sfWebRequest $request){
//        $this->time = date('H:i:s') ;
//        $currHr = date('H:i:s') ;
//        $this->end_time = $currHr ;
//        $this->status_list = array(0=>'Active',1=>'De-active');
        $this->bank_notific_scheduler_form=new CustomBankNotificationSchedulerForm();

    }


    public function executeSubmitBankNotificationScheduler(sfWebRequest $request)
    {
        if($request->isMethod('post')){
            ini_set('memory_limit','1024M');
            $bankReport_obj = bankPaymentSchedulerServiceFactory::getService(bankPaymentSchedulerServiceFactory::$TYPE_BASE);

            //get the information and store in the proper format
            //$stTimeArr = $request->getPostParameter('start_time');
            //$enTimeArr = $request->getPostParameter('end_time');
            $notifyStatus = $request->getPostParameter('status');
            $type = $request->getPostParameter('type');

            //get bank id
            $gUser = $this->getUser()->getGuardUser();
            //         $userGroup = $gUser->getGroupNames();

            //    if($userGroup[0] != "bank_user")
            //    $this->redirect('pages/errorUser');

            $bUser = $gUser->getBankUser();
            $bankId =  $bUser->getFirst()->getBank()->getId();


            $bankIntObj = new bankIntegrationHelper();
            $serializedArray = $bankIntObj->getSerialize($type,$request->getPostParameters());

            $updateStatus = $bankReport_obj->updateBankNotifySchedule($bankId,$serializedArray,$notifyStatus,'notify');

            //create the code and key

            //   do
            //        {
            //          $bank_code = mt_rand();
            //          $duplicacy_code = $bankReport_obj->checkDuplicacy($bankId);
            //        }while($duplicacy_code > 0);
            //
            //        $bank_key = mt_rand();
            //        $auth_info = base64_encode($bank_code.$bank_key);
            //        echo $bank_code.'---'.$auth_info; die;
            //
            //        $bnkSch = new BankPaymentScheduler();
            //        $bnkSch->setBankId($bankId);
            //        $bnkSch->setNotificationSchedule($serializedArray);
            //        $bnkSch->setBankKey($auth_info);
            //        $bnkSch->setBankCode($bank_code);
            //        $bnkSch->setNotificationStatus($notifyStatus);
            //        $bnkSch->save();

            $this->getUser()->setFlash('notice', sprintf('Bank notification schedule set successfully'));
            $this->redirect('bank/bankNotificationScheduler');

        }
    }


    public function executeBankPaymentScheduler(sfWebRequest $request){
//        $this->time = date('H:i:s') ;
//        $currHr = date('H:i:s') ;
//        $this->end_time = $currHr ;
//        $this->status_list = array(0=>'Active',1=>'De-active');
        $this->bank_notific_scheduler_form=new CustomBankNotificationSchedulerForm();

    }



    public function executeSubmitBankPaymentScheduler(sfWebRequest $request)
    {
        if($request->isMethod('post')){
            //ini_set('memory_limit','256M');
            $bankReport_obj = bankPaymentSchedulerServiceFactory::getService(bankPaymentSchedulerServiceFactory::$TYPE_BASE);

            $notifyStatus = $request->getPostParameter('status');
            $type = $request->getPostParameter('type');

            //get bank id
            $gUser = $this->getUser()->getGuardUser();
            //         $userGroup = $gUser->getGroupNames();

            //    if($userGroup[0] != "bank_user")
            //    $this->redirect('pages/errorUser');



            $bUser = $gUser->getBankUser();
            $bankId =  $bUser->getFirst()->getBank()->getId();

            $bankIntObj = new bankIntegrationHelper();
            $serializedArray = $bankIntObj->getSerialize($type,$request->getPostParameters());

            $updateStatus = $bankReport_obj->updateBankNotifySchedule($bankId,$serializedArray,$notifyStatus,'payment');

            $this->getUser()->setFlash('notice', sprintf('Bank Payment schedule set successfully'));
            $this->redirect('bank/bankPaymentScheduler');

        }
    }




    public function executeBankNotifyTest(sfWebRequest $request) {

        $xmldata = file_get_contents('php://input') ;

        echo $xmldata; die;


    }


  public function executeAddBankAuthentication(sfWebRequest $request) {
       $this->setLayout(null);
      $sql =  Doctrine_Query::create()->from('Bank')->where('id >= 1');
      //print $sql->getSqlQuery();

      $res = $sql->execute();
      foreach($res as $bankObj) {
          do {

              $bank_code = mt_rand();
             $duplicacy_code = Doctrine::getTable('Bank')->chkCodeDuplicacy($bank_code);
            } while ($duplicacy_code > 0);

            $bank_key = mt_rand();
            $auth_info = base64_encode($bank_code .":" .$bank_key);
//print "<br>".$bankObj->getId();
          $bankObj->setBankCode($bank_code);
          $bankObj->setBankKey(md5($bank_key));
          $bankObj->setAuthInfo($auth_info);
          $bankObj->save();
      }
return $this->renderText("Bank Code and Key created successfully");

  }

    /*
     * action : Viewcredential()
     */
    public function executeViewcredential(sfWebRequest $request) {
       $bankId  = $request->hasParameter('id')? $request->getParameter('id'):"";
       if(!empty($bankId))
       {
           $arrBank = Doctrine::getTable('Bank')->getBankName($bankId);
           if(!empty($arrBank)){
               $this->bankName = $arrBank['bank_name'];
               $this->bankCode = $arrBank['bank_code'];
               $this->bankKey = $arrBank['bank_key'];
               $this->bankId = $arrBank['id'];
               $this->form = new viewCredentialForm();
               $this->form->setDefault('bank_id', $this->bankId);
           }else{

               $this->redirect('bank/index');
           }

       }else{
          
           $this->redirect('bank/index');

       }

    
    }

    
     public function executeFetchCredentail(sfWebRequest $request) {

       $bankId =  $request->hasParameter('bank_id')? $request->getParameter('bank_id'):"";
       $groupId=  $request->hasParameter('group_id')? $request->getParameter('group_id'):"";
      
       $bankArray =array();
       if(!empty($bankId) && !empty($groupId))
       $bankArray = Doctrine::getTable('BankCredential')->getBankCredentailDetail($bankId,$groupId);
       else
       $bankArray[0]='';
       
       if(!empty($bankArray[0])){
            $encObj = new Encryption();
            $this->password = $encObj->generateEncyrypt($bankArray[0]['password'],$bankArray[0]['asymmetric_key']);
            $this->decpassword = $encObj->generateEncyrypt($bankArray[0]['password'],$bankArray[0]['asymmetric_key']);
           
       }else{
           $bankArray[0]['user_name']='--';
           $bankArray[0]['asymmetric_key']='--';
           $this->password = '--';
           
       }
       return $this->renderText('sucess'.'###'.$bankArray[0]['user_name'].'###'.$bankArray[0]['asymmetric_key']."###".$this->password);
  
   }






}
