<?php

/**
 * nis actions.
 *
 * @package    mysfp
 * @subpackage nis
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class paymentSystemActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $payForMeManagerObj = new payForMeManager();
        $this->merchant_name = $request->getParameter('name');
        //print_r("MERCHANR".$merchant_name) ; die();
        if (isset($this->merchant_name)) {
            $merchant_id = $payForMeManagerObj->getMerchantId($this->merchant_name);
        }
        $this->form = new MercahantPaymentForm();

        $this->merchant_service_array = $payForMeManagerObj->getMerchantServices($this->merchant_name);

        $this->type = "";
        $this->txnId = "";
        if ($request->getGetParameters()) {
            foreach ($request->getGetParameters() as $key => $value) {
                $this->$key = $value;
             }
        }
        $this->form->setDefault('txnId', $this->txnId);
        $this->form->setDefault('type', $this->type);
    }

//    public function executeIndex(sfWebRequest $request) {
//        $this->checkIsSuperAdmin();
//        $payForMeManagerObj = new payForMeManager() ;
//        $this->merchant_name = $request->getParameter('name') ;
//        //print_r("MERCHANR".$merchant_name) ; die();
//        if(isset($this->merchant_name)) {
//            $merchant_id = $payForMeManagerObj->getMerchantId($this->merchant_name) ;
//        }
//
//        $this->merchant_service_array = $payForMeManagerObj->getMerchantServices($this->merchant_name) ;
//        $this->type="";
//        $this->txnId="";
//        if($request->getGetParameters()) {
//            foreach($request->getGetParameters() as $key=>$value) {
//                $this->$key = $value;
//            }
//        }
//    }

    public function executeSearch(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        //$this->setLayout(false);
        $txnId = $appId = $refNo = $type = "";
        if ($request->getParameter('txnId')) {//print "ffd";exit;
            $txnId = $request->getParameter('txnId');
            $this->txnId = $txnId;
        }


        $gUser = $this->getUser()->getGuardUser();
        $bUser = $gUser->getBankUser();
        $bankId = $bUser->getFirst()->getBank()->getId();

        if (($request->getParameter('name')) && ($txnId == "")) {
            $merchantName = $request->getParameter('name');
            $merchantServiceId = $request->getParameter('type');
            //             $merchantObj = Doctrine::getTable('Merchant')->findByName($merchantName);
            //             $merchantId = $merchantObj->getFirst()->getId();
            $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
            $merchantData = $ValidationRulesObj->getSearchValidationRules($merchantServiceId);
            $rules = $ValidationRulesObj->getValidationRules($merchantServiceId);
            $err = 0;
            $search_arr = array();
            if (!empty($merchantData)) {
                foreach ($merchantData as $key => $value) {
                    if (($request->getParameter($value['name'])) == "" && $value['is_mandatory'] == 1) {
                        $err = 1;
                        $this->getUser()->setFlash('error', "Please enter " . $rules[$key], true);
                        $this->redirect('paymentSystem/index?' . http_build_query($request->getPostParameters()));
                    } else {
                        if ($request->getParameter($value['name']) != "")
                            $search_arr[$key] = $request->getParameter($value['name']);
                    }
                }
            }
            if ($err == 0) { //if all the values have been entered
                $applicationDetails = MerchantRequestDetailsTable::getDetailsByApplication($search_arr, $merchantServiceId);
                if ((count($applicationDetails) > 0) && isset($applicationDetails['txnId']) && ($applicationDetails['txnId'] != "")) {
                    $txnId = $applicationDetails['txnId']; //gives the transaction number
                } else {
                    $this->getUser()->setFlash('error', "Invalid Search", true);
                    $this->redirect('paymentSystem/index?' . http_build_query($request->getPostParameters()));
                }
            }
        } else if (($request->getParameter('name')) && ($txnId != "")) {
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId, $bankId);

            if (count($pfmTransactionDetails) == 0) {

                $this->getUser()->setFlash('error', "Transaction Id does not exist ", true);
                $this->redirect('paymentSystem/index?' . http_build_query($request->getPostParameters()));
            } else {

                $nisObj = new nisManager();
                $getMerArray = $nisObj->checkMerchantTxnNo($txnId);
                //echo "<pre>"; print_r($getMerArray); die();
                if ($getMerArray[0]['MER_NAME'] != $request->getParameter('name')) {
                    $this->getUser()->setFlash('error', "Transaction Id does not belongs to this Merchant ", true);
                    $this->redirect($request->getReferer());
                }
            }
        }

        //get user bank id


        if ($txnId != "") {
            //  echo "aaaaaa";die;
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            //bug 34207
            $pfmTransactionNotBank = $payForMeObj->getTransactionRecord($txnId);
            
            
            //Restriction for making payment by the bank user of the merchant mentioned in app.yml
            if(isset($pfmTransactionNotBank['merchant_id'])){
               if(commonHelper::isMerchantDisableForPayment($pfmTransactionNotBank['merchant_id'])){
                    sfContext::getInstance()->getUser()->setFlash('error', sfConfig::get("app_disable_merchant_for_payment_errmsg"), true);
                    $this->redirect($request->getReferer());
                 }
            }
            //End of Restriction for making ..
             
            $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId, $bankId);
            $lastTxnNumber = Doctrine::getTable('Transaction')->checkTxnNumber($txnId);
            $txnBankId = Doctrine::getTable('Transaction')->checkBankId($txnId);

            $pfmHelperObj = new pfmHelper();
            $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
            $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
            $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');

            $paymentModeArr = array($paymentModeDraftId,$paymentModeCheckId);
            $this->paymentStatus=1;
            if (count($pfmTransactionNotBank) > 0) {
                if(count($pfmTransactionDetails)>0){
                    $item_id = $pfmTransactionDetails['MerchantRequest']['merchant_item_id'];
                    $this->paymentStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
                }
            }
//                if($pfmTransactionDetails['payment_mode_option_id'])
            if (count($pfmTransactionNotBank) == 0) {

                $this->getUser()->setFlash('error', "Transaction Id does not exist", true);
                $this->redirect($request->getReferer());

            }
            if(count($pfmTransactionDetails)==0){
                $this->getUser()->setFlash('error', "Your Bank is not eligible for this transaction as applicant had selected different bank for it.", true);
                $this->redirect($request->getReferer());
            }

            if($this->paymentStatus===1 && (in_array($pfmTransactionDetails['payment_mode_option_id'],$paymentModeArr) & $bankId != $txnBankId)){
                $this->getUser()->setFlash('error', "Your Bank is not eligible for this transaction as applicant had selected different bank for it.", true);
                $this->redirect($request->getReferer());
            }
//            else if (($pfmTransactionDetails['payment_mode_option_id'] != "1") && ($pfmTransactionDetails['payment_mode_option_id'] != "13" && $pfmTransactionDetails['payment_mode_option_id'] != "14" && $pfmTransactionDetails['payment_mode_option_id'] != "15")) {
//
//
//                $this->getUser()->setFlash('error', "Transaction Id does not exist", true);
//                $this->redirect($request->getReferer());
//
//            }
            else if ($this->paymentStatus===1 && ($pfmTransactionDetails['payment_mode_option_id'] != "1" && $pfmTransactionDetails['payment_mode_option_id'] != "14" && $pfmTransactionDetails['payment_mode_option_id'] != "15" && $pfmTransactionDetails['payment_mode_option_id'] != "13")) {

                $paymentModeDetail = Doctrine::getTable('PaymentMode')->getPaymentMode($pfmTransactionDetails['MerchantRequest']['payment_mode_option_id']);
                $this->getUser()->setFlash('error', "Please advice the customer to pay via " . $paymentModeDetail->getDisplayName() . ".", true);
                $this->redirect($request->getReferer());

            } else if ($this->paymentStatus===1 && ($txnId != $lastTxnNumber['pfmTxnNumber'] && $pfmTransactionDetails['payment_mode_option_id'] != $lastTxnNumber['paymentMode'])){


                $paymentModeDetail = Doctrine::getTable('PaymentMode')->getPaymentMode($pfmTransactionDetails['MerchantRequest']['payment_mode_option_id']);
                $paymentModeArr = array($paymentModeCheckId,$paymentModebankId,$paymentModeDraftId);

                if(in_array($pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'], $paymentModeArr)){
                     $this->getUser()->setFlash('error', "This transaction number is archived, Please ask the customer to pay with new transaction number ".$lastTxnNumber['pfmTxnNumber']."." , true);
                }else {
                     $this->getUser()->setFlash('error', "This transaction number is archived, Please advice customer to pay with new transaction number ".$lastTxnNumber['pfmTxnNumber']." via ". $paymentModeDetail->getDisplayName()."." , true);
                }

                $this->redirect($request->getReferer());

            } else {
                if (isset($txnId)) {
                    unset($_SESSION['pfm']['selected_txnId']);
                    $_SESSION['pfm']['selected_txnId'] = $txnId;
                }

                $bankMerchantActive = Doctrine::getTable('BankMerchant')->chkBankMerchantStatus($pfmTransactionDetails['MerchantRequest']['merchant_id'], $bankId);
                if ($bankMerchantActive == 0) {
                    $this->getUser()->setFlash('error', "This merchant is de-activated for this bank", true);
                    $this->redirect($request->getReferer());
                } else {

                    $item_id = $pfmTransactionDetails['MerchantRequest']['merchant_item_id'];
                    $this->paymentStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
                    // $this->paymentStatus = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];
                    if ($this->paymentStatus != '' && $this->paymentStatus != 0) {
                        //got to detail function

                        $this->executeDetail($pfmTransactionDetails);
                    } else if ($pfmTransactionDetails['MerchantRequest']['bank_id'] != $bankId) {
                        $this->getUser()->setFlash('error', "Payment has already been received for this application", true);
                        $this->redirect($request->getReferer());
                    } else {
                        $this->getRequest()->setParameter('txn_no', $txnId);
                        $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
                        $this->forward('paymentSystem', 'payOnline');
                    }
                }
            }
        }
    }

    public function restrictPayment($bankId) {
        //checking for bank payment scheduler
        $bankPayment_obj = bankPaymentSchedulerServiceFactory::getService(bankPaymentSchedulerServiceFactory::$TYPE_BASE);
        $bankScheduleDetails = $bankPayment_obj->getBankScheduleRecords($bankId);

        if (count($bankScheduleDetails) > 0) {

            $payment_schedule = $bankScheduleDetails[0]['payment_schedule']; //payment_schedule for bank payment
            $payment_status = $bankScheduleDetails[0]['payment_status']; //payment_status for bank payment

            if ($payment_schedule != '') {
                $uns_payment_schedule = unserialize($payment_schedule);
            } else {
                $uns_payment_schedule = '';
            }

            if ($payment_status != '' && $payment_status != 1) {
                if (is_array($uns_payment_schedule)) {

                    if ($uns_payment_schedule['schedule'] == 'all') {
                        //all time notification
                        if ($uns_payment_schedule['all']['start_time'] >= date('H:i:s') || $uns_payment_schedule['all']['end_time'] <= date('H:i:s')) {
                            return true;
                        }
                    } elseif ($uns_payment_schedule['schedule'] == 'day') {
                        //day based notification
                        if (array_key_exists(strtolower(date('l')), $uns_payment_schedule['day'])) {
                            if ($uns_payment_schedule['day'][strtolower(date('l'))]['start_time'] >= date('H:i:s') || $uns_payment_schedule['day'][strtolower(date('l'))]['end_time'] <= date('H:i:s')) {
                                return true;
                            }
                        }
                    } elseif ($uns_payment_schedule['schedule'] == 'date') {
                        //date based notification
                        if (array_key_exists(strtolower(date('j')), $uns_payment_schedule['date'])) {
                            if ($uns_payment_schedule['date'][strtolower(date('j'))]['start_time'] >= date('H:i:s') || $uns_payment_schedule['date'][strtolower(date('j'))]['end_time'] <= date('H:i:s')) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    public function executeDetail($pfmTransactionDetails) {
        $this->form = new AcknowledgeSlipForm();

        $this->checkIsSuperAdmin();
        $gUser = $this->getUser()->getGuardUser();
        $bUser = $gUser->getBankUser();
        $bankId = $bUser->getFirst()->getBank()->getId();

        $bankMerchantActive = Doctrine::getTable('BankMerchant')->chkBankMerchantStatus($pfmTransactionDetails['MerchantRequest']['merchant_id'], $bankId);
        if ($bankMerchantActive == 0) {
            $this->getUser()->setFlash('error', "This merchant is de-activated for this bank. !!!", true);
            $this->redirect($request->getReferer());
        } else {
            //get Param Description; as per Merchant Service
            $this->isClubbed = $pfmTransactionDetails['MerchantRequest']['MerchantService']['clubbed'];
            $this->currency_id = $pfmTransactionDetails['MerchantRequest']['currency_id'];
            $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
            $this->MerchantData = $ValidationRulesObj->getValidationRules($pfmTransactionDetails['merchant_service_id']);
            $this->nisServiceDetails = $pfmTransactionDetails;
            $paymentModeConfigManager = new paymentModeConfigManager();
            $this->show = $paymentModeConfigManager->isPaymentServiceActive($pfmTransactionDetails['merchant_id'], $pfmTransactionDetails['merchant_service_id'], $pfmTransactionDetails['payment_mode_id'], $pfmTransactionDetails['payment_mode_option_id']);
            $this->form->setDefault('txnId', $this->nisServiceDetails['pfm_transaction_number']);
            $this->form->setDefault('name', $this->nisServiceDetails['MerchantRequest']['MerchantRequestDetails']['name']);
            $this->form->setDefault('appCharge', $this->nisServiceDetails['MerchantRequest']['item_fee']);
            $this->form->setDefault('bankCharge', $this->nisServiceDetails['MerchantRequest']['bank_charge']);
            $this->form->setDefault('serviceCharge', $this->nisServiceDetails['MerchantRequest']['service_charge']);
            $this->form->setDefault('totalCharge', $this->nisServiceDetails['total_amount']);
            $this->txnId = $this->nisServiceDetails['pfm_transaction_number'];

            $this->setTemplate('detail');
        }
    }

    public function executeValidate(sfWebRequest $request) {

        $this->form = new AcknowledgeSlipForm();
        $this->form->bind($request->getParameter('acknowledge'));
        if ($request->isMethod('post')) {
            if ($this->form->isValid()) {
                $this->checkIsSuperAdmin();

                $txnId = $this->form->getValue('txnId');

                $enviroment = '';
                if (sfConfig::get('sf_environment') == "sandbox")
                    $enviroment = 'sandbox';
                $paymentSystemObject = paymentSystemServiceFactory::getService($enviroment);

                $paymentObj = new Payment($txnId);
                $arrayVal = $paymentObj->proceedToPay();
                // return false;
                $this->redirectNotification($arrayVal);
                return false;
            }
            else {
                $txnId = $request->getParameter('txnId');
                $this->getUser()->setFlash('error', "Please select all checkboxes", true); //FS#29737
                $this->redirect('paymentSystem/search?txnId=' . $txnId);
                //    $this->redirect($request->getReferer());
            }
        }
    }

    public function executeGetMerchantDetails(sfWebRequest $request) {
        $this->checkIsSuperAdmin();

        $createObj = new nisManager();
        $getData = $request->getParameter('merchant_service_id');
        $this->getAllData = $createObj->getValidationRules($getData);
    }

    public function executePayOnline(sfWebRequest $request) {

        $this->superadmin = false;
        if ($request->hasParameter('template')) {
            $template = $request->getParameter('template');
            $this->setLayout($template);
        }

        if ($request->hasParameter('pfmTransactionDetails')) {
            $txnArray = $request->getParameter('pfmTransactionDetails');
            $txn_no = $txnArray['pfm_transaction_number'];
        }
        if ($request->hasParameter('txn_no')) {
            $txn_no = $request->getParameter('txn_no');
        }
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);
        $item_id = $pfmTransactionDetails['MerchantRequest']['merchant_item_id'];

        //get the merchant_request_id against which the payment has been done
        $merchant_obj = Doctrine::getTable('MerchantRequest')->getMerchantPaidId($item_id);

        //    $merchant_request_payment_id = $merchant_obj->getFirst()->getId();
        if ($merchant_obj->count()) {
            $transaction_number_payment_id = $merchant_obj->getFirst()->getTransaction()->getFirst()->getTransactionNumber();

            //      $this->getRequest()->setParameter('duplicate_payment_receipt', 'true');
            if ($transaction_number_payment_id != $txn_no) {
                $txn_no = $transaction_number_payment_id;
                //$pfmTransactionDetails = $payForMeObj->getTransactionRecord($transaction_number_payment_id);
                // $this->getRequest()->setParameter('trans_number', $transaction_number_payment_id);
            }
        }
        $this->payment_receipt_header = "Payment Receipt";
        if ($request->hasParameter('duplicate_payment_receipt')) {
            $this->payment_receipt_header = "Duplicate " . $this->payment_receipt_header;
        }

        $this->setTemplateForReceipt($txn_no);
    }

    public function setTemplateForReceipt($txn_no) {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);


        //get Param Description; as per Merchant Service
        $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
        $this->MerchantData = $ValidationRulesObj->getValidationRules($pfmTransactionDetails['merchant_service_id']);

        //$payForMeNisManagerObj = payForMeNisServiceFactory::getService('nis');

        $postDataArray['txnId'] = $pfmTransactionDetails['pfm_transaction_number'];
        if ($this->MerchantData) {
            foreach ($this->MerchantData as $key => $value) {
                $postDataArray[$key] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails'][$key];
            }
        }

        $postDataArray['type'] = "";

        $postDataArray['name'] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails'] ['name'];
        $postDataArray['appCharge'] = $pfmTransactionDetails['MerchantRequest']['item_fee'];
        $postDataArray['bankCharge'] = $pfmTransactionDetails['MerchantRequest']['bank_charge'];
        $postDataArray['serviceCharge'] = $pfmTransactionDetails['MerchantRequest']['service_charge'];
        $postDataArray['totalCharge'] = $pfmTransactionDetails['total_amount'];
        $postDataArray['serviceType'] = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
        $pfm_helper_obj = new pfmHelper();
        if ($pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'] == $pfm_helper_obj->getPMOIdByConfName('Cheque')) {
            $postDataArray['check_number'] = $pfmTransactionDetails['check_number'];
            $postDataArray['bank_name'] = $pfmTransactionDetails['MerchantRequest']['bank_name'];
            $postDataArray['account_number'] = $pfmTransactionDetails['account_number'];
            $postDataArray['sort_code'] = $pfmTransactionDetails['sort_code'];
        }
        if ($pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'] == $pfm_helper_obj->getPMOIdByConfName('bank_draft')) {
            $postDataArray['bank_name'] = $pfmTransactionDetails['MerchantRequest']['bank_name'];
            $postDataArray['sort_code'] = $pfmTransactionDetails['sort_code'];
            $postDataArray['draft_number'] = $pfmTransactionDetails['check_number'];
        }
        $postDataArray['paymentModeName'] = Doctrine::getTable('PaymentModeOption')->find($pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'])->getName();
        $postDataArray['validation_number'] = $pfmTransactionDetails['MerchantRequest']['validation_number'];
        $postDataArray['payment_date'] = $pfmTransactionDetails['MerchantRequest']['paid_date'];
        $postDataArray['payment_status_code'] = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];
        $paid_by = ($pfmTransactionDetails['MerchantRequest']['paid_by'])?$pfmTransactionDetails['MerchantRequest']['paid_by']:$pfmTransactionDetails['MerchantRequest']['updated_by'];
        $postDataArray['merchant_name'] = $pfmTransactionDetails['MerchantRequest']['Merchant']['name'];


        $currency_id = $pfmTransactionDetails['MerchantRequest']['currency_id'];
        $postDataArray['currency_id'] = $currency_id;
        $payment_mode_option = $postDataArray['payment_mode_option'] = $pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name'];
        $payment_mode_option_name = $postDataArray['payment_mode_option'] = $pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['PaymentMode']['display_name'];
        $postDataArray['payment_mode'] = $payment_mode_option_name;

        $this->isClubbed = $pfmTransactionDetails['MerchantRequest']['MerchantService']['clubbed'];
        $this->homePage = $pfmTransactionDetails['MerchantRequest']['MerchantService']['merchant_home_page'];
        $this->merchantServiceId = $pfmTransactionDetails['merchant_service_id'];


        $group_name = $this->getUser()->getGuardUser()->getGroupNames();
        if(($postDataArray['payment_mode'] == 'Bank') || ($postDataArray['payment_mode'] == 'Cheque') || ($postDataArray['payment_mode'] == 'Bank Draft') || ($postDataArray['payment_mode'] == 'Internet Bank') || ($postDataArray['payment_mode'] == 'Merchant Counter')){
        //if (in_array(sfConfig::get('app_pfm_role_bank_branch_user'), $group_name) || in_array(sfConfig::get('app_pfm_role_admin'), $group_name )) {
            $this->postDataArray = $postDataArray;
            if($payment_mode_option == sfConfig::get('app_payment_mode_option_internet_bank')){ // nibss receipt template
                $this->setTemplate('payOnlineNibss');
            }else{
                $this->setTemplate('payOnline');
            }
        } else {
            // Bug[32812]
            $userDetails = Doctrine::getTable('UserDetail')->findByUserId($paid_by);
            $this->userType = $userDetails[0]['ewallet_type'];
            if ($userDetails[0]['ewallet_type'] != 'guest_user') {
	            $this->ewalletAccountDetailsShow = strpos($userDetails->getFirst()->getEwalletType(),'pay4me');
	            if(!$this->ewalletAccountDetailsShow){
	                $billObj = billServiceFactory::getService();
	                $walletDetails = $billObj->getEwalletAccountValues($currency_id, $paid_by);
	                $this->errorForEwalletAcount = '';
	                if ($walletDetails == false) {
	                    $this->errorForEwalletAcount = "Invalid Account Number";
	                } else {
	                    $this->accountObj = $walletDetails->getFirst();
	                    $this->userDetailObj = $walletDetails->getFirst()->getUserDetail()->getFirst();
	                    // $this->accountBalance =$billObj->getEwalletAccountBalance($this->accountObj->getId());
	                }
	            }
            }
            $payment_mode_option = strtolower($payment_mode_option);

            $paymentModeOptionId = $pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'];
            $gateorderObj = new gatewayOrderManager();
            $checkPaymentModeOption = $gateorderObj->checkPaymentModeOption($paymentModeOptionId);
            if ($checkPaymentModeOption) {
                $orderObj = Doctrine::getTable('GatewayOrder')->getOrderDetails($txn_no, "success");
                if ($orderObj) {
                    $postDataArray['transaction_date'] = $orderObj->getFirst()->getTransactionDate();
                }
            }
            if (($payment_mode_option == sfConfig::get('app_payment_mode_option_credit_card')) || ($payment_mode_option == sfConfig::get('app_payment_mode_option_interswitch')) || ($payment_mode_option == sfConfig::get('app_payment_mode_option_etranzact'))) {
                $orderObj = Doctrine::getTable('GatewayOrder')->getOrderDetails($txn_no, "success");
                if ($orderObj) {
                    $postDataArray['pan'] = $orderObj->getFirst()->getPan();
                    $postDataArray['approvalcode'] = $orderObj->getFirst()->getApprovalcode();
                }
            }
            $groupName = sfContext::getInstance()->getUser()->getGroupNames();
            if (!empty($groupName) && $groupName[0] == 'guest_user') {
              $EpDBSessionDetailObj =  new EpDBSessionDetail();
              $EpDBSessionDetailObj->doCleanUpSessionByUser();
            }
            $this->postDataArray = $postDataArray;
            $this->setTemplate('eWalletPayOnline');
        }
    }

    public function executeVbvPayOnline(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $txnId = base64_decode($request->getParameter('id'));
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId);

        $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
        $this->MerchantData = $ValidationRulesObj->getValidationRules($pfmTransactionDetails['merchant_service_id']);

        $postDataArray['txnId'] = $pfmTransactionDetails['pfm_transaction_number'];
        if ($this->MerchantData) {
            foreach ($this->MerchantData as $key => $value) {
                $postDataArray[$key] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails'][$key];
            }
        }

        $postDataArray['type'] = "";
        $postDataArray['name'] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails'] ['name'];
        $postDataArray['appCharge'] = $pfmTransactionDetails['MerchantRequest']['item_fee'];
        $postDataArray['bankCharge'] = $pfmTransactionDetails['MerchantRequest']['bank_charge'];
        $postDataArray['serviceCharge'] = $pfmTransactionDetails['MerchantRequest']['service_charge'];
        $postDataArray['totalCharge'] = $pfmTransactionDetails['MerchantRequest']['total_amount'];
        $postDataArray['serviceType'] = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
        $postDataArray['validation_number'] = $pfmTransactionDetails['MerchantRequest']['validation_number'];
        $this->postDataArray = $postDataArray;

        $this->setLayout('pay4me');
        $this->setTemplate('vbvPayOnline');
    }

    public function executePay(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $txnId = $request->getPostParameter('txnId');
        //$this->postDataArray = $request->getPostParameters();
        //$txnId = $this->postDataArray['txnId'];
        //$gUser = $this->getUser()->getGuardUser();
        //$group_name = $gUser->getGroupNames();

        $enviroment = '';
        if (sfConfig::get('sf_environment') == "sandbox")
            $enviroment = 'sandbox';
        $paymentSystemObject = paymentSystemServiceFactory::getService($enviroment);

        $paymentObj = new Payment($txnId);
        $arrayVal = $paymentObj->proceedToPay();

        // [WP033][Inhouse maintainace]
        // add job mail to send on ewallet recharge
        /* if($arrayVal['comments_val']=='Payment Successful' && $arrayVal['var_value']['MerchantRequest']['PaymentModeOption']['name']=='eWallet'){
          $this->addJobForewalletMail($txnId);
          } */

        $this->redirectNotification($arrayVal);
        return false;
    }

    public function chkPaymentStatusForVbv($pfmTransactionDetails) {
        $txnId = $pfmTransactionDetails['pfm_transaction_number'];
        $item_id = $pfmTransactionDetails['MerchantRequest']['merchant_item_id'];
        $this->serviceTypeStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
        if ($this->serviceTypeStatus != '' && $this->serviceTypeStatus != 0) {
            return true;
        } else {
            return false;
        }
    }

    /* public function createLog($logdata,$nameFormat,$exceptionMessage="") {

      $pay4meLog = new pay4meLog();
      if($exceptionMessage!="") {
      $logdata = $logdata."\n".$exceptionMessage;
      }
      $pay4meLog->createLogData($logdata,$nameFormat, 'notificationTime', false, true);
      } */

    public function createLog($logdata, $nameFormat, $exceptionMessage="", $parentLogFolder='', $appendTime=true, $append = false) {

        $pay4meLog = new pay4meLog();
        if ($exceptionMessage != "") {
            $logdata = $logdata . "\n" . $exceptionMessage;
        }
        $pay4meLog->createLogData($logdata, $nameFormat, $parentLogFolder, $appendTime, $append);
    }

    public function executeSendBankNotification(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->setLayout(null);
        sfView::NONE;
        $txnId = $request->getParameter('p4m_transaction_id');
        $response = $this->bankNotification($txnId);
        if ($response == 1) {
            return $this->renderText("Bank Notification sent Successfully");
        } elseif ($response == 2) {
            return $this->renderText("Bank Notification has been rescheduled");
        } else {
            return $this->renderText("Bank Notification not sent");
        }
    }

    public function bankNotification($txnId) {

        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId);
        $bankId = $pfmTransactionDetails['MerchantRequest']['bank_id'];

        $bankPayment_obj = bankPaymentSchedulerServiceFactory::getService(bankPaymentSchedulerServiceFactory::$TYPE_BASE);
        $bankScheduleDetails = $bankPayment_obj->getBankScheduleRecords($bankId);

        if (count($bankScheduleDetails) > 0) {

            $url = $bankScheduleDetails[0]['notification_url']; //url for bank notification
            $notification_schedule = $bankScheduleDetails[0]['notification_schedule']; //notification_schedule for bank notification
            $notification_status = $bankScheduleDetails[0]['notification_status']; //notification_status for bank notification

            if ($notification_schedule != '') {
                $uns_notification_schedule = unserialize($notification_schedule);
            } else {
                $uns_notification_schedule = '';
            }

            if ($notification_status != '' && $notification_status != 1) {
                if (is_array($uns_notification_schedule)) {

                    if ($uns_notification_schedule['schedule'] == 'all') {
                        //all time notification
                        if ($uns_notification_schedule['all']['start_time'] <= date('H:i:s') && $uns_notification_schedule['all']['end_time'] >= date('H:i:s')) {
                            $this->postBankNotification($txnId, $url);
                            return 1;
                        } else {
                            //reschedule the job to send notification
                            $this->recheduleBankNotification($uns_notification_schedule['all']['start_time']);
                            return 2;
                        }
                    } elseif ($uns_notification_schedule['schedule'] == 'day') {
                        //day based notification
                        if (array_key_exists(strtolower(date('l')), $uns_notification_schedule['day'])) {
                            if ($uns_notification_schedule['day'][strtolower(date('l'))]['start_time'] <= date('H:i:s') && $uns_notification_schedule['day'][strtolower(date('l'))]['end_time'] >= date('H:i:s')) {
                                $this->postBankNotification($txnId, $url);
                                return 1;
                            } else {
                                //reschedule the job to send notification
                                $nextDayStamp = strtotime("+1 day");
                                $nextDay = strtolower(date('l', $nextDayStamp));
                                $this->recheduleBankNotification($uns_notification_schedule['day'][$nextDay]['start_time']);
                                return 2;
                            }
                        }
                    } elseif ($uns_notification_schedule['schedule'] == 'date') {
                        //date based notification
                        if (array_key_exists(strtolower(date('j')), $uns_notification_schedule['day'])) {
                            if ($uns_notification_schedule['date'][strtolower(date('j'))]['start_time'] <= date('H:i:s') && $uns_notification_schedule['date'][strtolower(date('j'))]['end_time'] >= date('H:i:s')) {
                                $this->postBankNotification($txnId, $url);
                                return 1;
                            } else {
                                //reschedule the job to send notification
                                $nextDateStamp = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
                                ;
                                $nextDate = date('j', $nextDateStamp);
                                $this->recheduleBankNotification($uns_notification_schedule['date'][$nextDate]['start_time']);
                                return 2;
                            }
                        }
                    }
                }
            }
        }
    }

    protected function postBankNotification($txnId, $url) {
        $browser = $this->getBrowser();
        $header['Content-Type'] = "application/xml;charset=UTF-8";
        $header['Accept'] = "application/xml;charset=UTF-8";
        //xml for bank notification
        $bankXmlObj = new bankNotificationXml();
        $bankXml = $bankXmlObj->generateBankNotificationXML($txnId);
        $this->createLog($bankXml, 'BankNotification');
        $browser->post($url, $bankXml, $header);
        $code = $browser->getResponseCode();
        if ($code != 200) {
            $respMsg = $browser->getResponseMessage();
            $respText = $browser->getResponseText();
            throw new Exception("Error in posting bank notification:" . PHP_EOL . $code . ' ' . $respMsg . PHP_EOL . $respText);
        }
        $this->logMessage('bank notification send');
    }

    protected function recheduleBankNotification($start_time) {
        $tomorrowStamp = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
        $tommorowDate = date('Y-m-d', $tomorrowStamp);
        EpjobsContext::getInstance()->setTaskStatusFailed();
        EpjobsContext::getInstance()->setNextRescheduleTime($tommorowDate . ' ' . $start_time);
    }

    public function executeSendNotification(sfWebRequest $request) {
        try {
            $startTime = time();
            $this->setLayout(null);
            $txnId = $request->getParameter('p4m_transaction_id'); 
            sfContext::getInstance()->getLogger()->info('Txn No: ' . $txnId);
            $this->notification($txnId);
            $endTime = time();
            $execution_time = $endTime - $startTime;
            if ($execution_time > 1) {
                $logString = "Send Notification : Start Time - " . date("Y-M-d-D H:i:s",$startTime) . " End Time - " . date("Y-M-d-D H:i:s",$endTime) . " Execution Time - " . date("Y-M-d-D H:i:s",$execution_time) . " TRANSACTION ID - " . $txnId;
                $this->createLog($logString, 'NotificationTime', '', 'notificationTime', false, true);
                $location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
                $this->merchantLog($logString, $location ,$txnId);
            }
            $this->setLayout(null);
            $message = "Notification sent Successfully";
            $location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
            $this->merchantLog($message, $location ,$txnId);
            return $this->renderText($message);
        } catch (Exception $e) {
            EpjobsContext::getInstance()->setTaskStatusFailed();
            $this->logMessage("Logging Curl Post Error -->" . $e->getMessage());
            $location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
            $this->merchantLog($e->getMessage, $location ,$txnId);
            return $this->renderText("Timeout :" . $e->getMessage());
        }
    }

    public function notification($txnId) {
    	
    try {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId);
        $url = $pfmTransactionDetails['MerchantRequest']['MerchantService']['notification_url'];
        $version = $pfmTransactionDetails['MerchantRequest']['version'];
	$updatedBy  = (isset($pfmTransactionDetails['MerchantRequest']['updated_by']) && $pfmTransactionDetails['MerchantRequest']['updated_by'] != "0") ? $pfmTransactionDetails['MerchantRequest']['updated_by'] : "";
	$paidBy     = ($updatedBy != '') ? $updatedBy : $pfmTransactionDetails['MerchantRequest']['paid_by'];
        $browser = $this->getBrowser();
        $header['Content-Type'] = "application/xml;charset=UTF-8";
        $header['Accept'] = "application/xml;charset=UTF-8";
        $responseClass = "pay4MeXML" . ucfirst($version);
        $obj = new $responseClass();
        $transXml = $obj->generatePaymentNotificationXML($txnId);
        
        /*
        //Merchant Response Log Table enteries 
        $merchantResposeObj = new MerchantResponseLog();
        $merchantResposeObj->setTransactionNumber($txnId);
        $merchantResposeObj->setMessageText($transXml);
        */
        $this->logMessage($url);
        $browser->post($url, $transXml, $header);
       	$message = 'Notification sent to URL '.$url;
       	$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
       	$this->merchantLog($message,$location,$txnId);
       	$code = $browser->getResponseCode();
       	$message = 'Reponse Code from merchant: '.$code;
       	$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
       	$this->merchantLog($message, $location ,$txnId);
       	$notification_response = $browser->getResponseText();
       	
       	sfContext::getInstance()->getLogger()->info('Response from Merchant....' . $notification_response);
       	
       	$merchantNotification = '';
        //to get merchant_request_id
        $result = Doctrine::getTable('Transaction')->getMerchantRequestId($txnId);
        $merchantRequestId = $result['0']['merchant_request_id'];
        $transRef = Doctrine::getTable('MerchantRequest')->getTransRefByMerchantRequestLog($merchantRequestId);
        $requestId = Doctrine::getTable('MerchantRequestLog')->getRequestLogIdBytransRef($transRef);
	//Merchant Response Log Table enteries 
        $merchantResposeObj = new MerchantResponseLog();
        $merchantResposeObj->setTransactionNumber($txnId);
        $merchantResposeObj->setMessageText($transXml);
        $merchantResposeObj->setRequestId($requestId);
        $merchantResposeObj->save();
        if ($code != 200) {
        	$respMsg = $browser->getResponseMessage();
        	//$respText = $browser->getResponseText();
        	//failure case 
        	$merchantNotification = '2';
        	$merchantNotification = Doctrine::getTable('MerchantRequest')->saveMerchantNotificationStatus($merchantRequestId,$merchantNotification, $paidBy);
        	throw new Exception("Error in posting notification:" . PHP_EOL . $code . ' ' . $respMsg . PHP_EOL .$notification_response);
        }
        else{ 
        	$pos = strpos($notification_response, '<script');
        	if ($pos === true) {
        		$notification_response = substr($notification_response, 0, $pos);
        	} 
        	$response_xml_obj = simplexml_load_string($notification_response);
        	if($response_xml_obj){
        		$notification_response = $response_xml_obj->status;
        	}
        	
        	$message = 'Response from Merchant: '.$notification_response;
        	$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
        	$this->merchantLog($message, $location,$txnId);
        	
        		if($notification_response =='success'){
          			$merchantNotification = '0';
        		}
        		else if($notification_response =='failure'){
          			$merchantNotification = '2';
        		}
        		else if($notification_response =='Already Updated'){
          			$merchantNotification = '3';
        		}
        		else {
         			 $merchantNotification = '4';   // if nothing returned
        		}
       			 $merchantNotification = Doctrine::getTable('MerchantRequest')->saveMerchantNotificationStatus($merchantRequestId,$merchantNotification, $paidBy);
        }
        $this->logMessage('notification send');
      } 
        //To save notification status sent from  merchant's end in merchant_request(pay4me) table.
         catch (Exception $e) {
        throw $e;
      }
    }
    
  /** Function to save message log data in merchant_log table.
   * Developed By:Deepak Bhardwaj
   **/  
    public function merchantLog($message,$location,$txnId) {
        $logger_id = Doctrine::getTable('Transaction')->getTransactionCreatedBy($txnId);
    	$pfmHelperObj = new pfmHelper();
    	$logger_name = $pfmHelperObj->getUsernameByUserid($logger_id);
    	$level = 'DEBUG';
    	$log_date = date('Y-m-d H:i:s');
    	$module = 'Support';
    	$messageArray = array($level,$logger_name,$location,$module,$log_date,$message);
    	$message = json_encode($messageArray);
    	$Obj = new MerchantLog();
    	$Obj->setLogMessage($message);
    	$Obj->setTransactionNumber($txnId);
    	$Obj->save();
    }
    
    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                            array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false, 'timeout' => sfConfig::get('app_notification_request_timeout')));
        }

        return $this->browserInstance;
    }

    public function checkIsSuperAdmin() {
        if ($this->getUser()->isAuthenticated()) {
            $group_name = $this->getUser()->getGroupNames();
            if (in_array(sfConfig::get('app_pfm_role_admin'), $group_name)) {
                $this->redirect('@adminhome');
            }
        }
    }

    public function redirectNotification($arrayVal) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $this->getUser()->setFlash($arrayVal['comments'], __($arrayVal['comments_val']), false);
        $this->getRequest()->setParameter($arrayVal['var_name'], $arrayVal['var_value']);
        if (isset($arrayVal['var_name_trans']))
            $this->getRequest()->setParameter($arrayVal['var_name_trans'], $arrayVal['var_value_trans']);

        $this->forward($arrayVal['module'], $arrayVal['action']);
        return;
    }

//    public function executeUpdateAccounts(sfWebRequest $request) {
//      try {
//      //EpjobsContext::getInstance()->addJobForEveryDay('UpdatePayments', 'paymentSystem/updateAccounts',"2020-3-12 12:12:12",null,null,null,null,"26","11");
//        $this->setLayout(null);
//        ///select all unpaid txn id
//
////        $manager = Doctrine_Manager::getInstance();
////        $callBackValue = $manager->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
////        $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
//        do {
//          $this->SplitPayments();
//          $totNotPayments = Doctrine::getTable('MerchantPaymentTransaction')->findCountNotPaidReq();
//         }while($totNotPayments);
////        $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $callBackValue );
//       // Doctrine_Manager::connection()->clean();
//        return $this->renderText("Payment Updated Successfully");
//      }
//      catch(Exception $e ) {
////        $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $callBackValue );
//        $this->logMessage("Logging the exception from Batch Processing..".$e->getMessage());
//      }
//
//    }
    public function executeUpdateAccounts(sfWebRequest $request) {
        try {
            // EpjobsContext::getInstance()->addJobForEveryDay('UpdatePayments', 'paymentSystem/updateAccounts',"2020-3-12 12:12:12",null,null,null,null,"26","11");
            $this->setLayout(null);
            ini_set('memory_limit', '512M');
            $this->SplitPayments();
            $trans_date = date('Y-m-d H:i:s', strtotime(date('Y') . "-" . date('m') . "-" . (date('d') - 1) . " 23:59:59"));
            $totNotPayments = Doctrine::getTable('MerchantPaymentTransaction')->findCountNotPaidReq($trans_date);
            if ($totNotPayments) {

                $next_schedule = mktime(date("H"), date("i") + settings::getSameDayProcessBatchTimeInMin(), date("s"), date("m"), date("d"), date("Y"));
                $date_next = date('Y-m-d H:i:s', $next_schedule);
                EpjobsContext::getInstance()->addJob('UpdatePayments', 'paymentSystem/updateAccounts', NULL, '', '', $date_next);
            } else {
                $next_schedule = mktime(0, 0 + settings::getNextDayProcessBatchTimeInMin(), 0, date("m"), date("d") + 1, date("Y"));
                $date_next = date('Y-m-d H:i:s', $next_schedule);
                EpjobsContext::getInstance()->addJob('UpdatePayments', 'paymentSystem/updateAccounts', NULL, '', '', $date_next);
            }
            Doctrine_Manager::connection()->clear();
            //}while($totNotPayments);
            // $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $callBackValue );
            // Doctrine_Manager::connection()->clean();
            return $this->renderText("Payment Updated Successfully");
        } catch (Exception $e) {
            EpjobsContext::getInstance()->setTaskStatusFailed();
            $this->logMessage("" . $e->getMessage() . var_dump(number_format(memory_get_peak_usage())));
            return $this->renderText("Logging the exception from Batch Processing.." . $e->getMessage() . var_dump(number_format(memory_get_peak_usage())));
            // $this->addJobForMail($e->getMessage());
            // $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $callBackValue );
        }
    }

    public function SplitPayments() {
        //   Doctrine_Manager::connection()->clean();
        $trans_date = date('Y-m-d H:i:s', strtotime(date('Y') . "-" . date('m') . "-" . (date('d') - 1) . " 23:59:59"));
        $con = Doctrine_Manager::connection()->setAttribute(Doctrine_Core::ATTR_AUTO_FREE_QUERY_OBJECTS, true);
        $allRecords = Doctrine::getTable('MerchantPaymentTransaction')->findAllNotPaidReq($trans_date);
        foreach ($allRecords as $rec) {
            try {

                $con->beginTransaction();

                $txnId = $rec->getTransactionNumber();
                $validationNo = $rec->getValidationNumber();

                $manager = Doctrine_Manager::getInstance();
                $callBackValue = $manager->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
                $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
                $paymentObj = new payForMeManager();
                $paymentObj->splitPayment($txnId, $validationNo);


                $rec->setIsProcessed('1');
                $rec->setUpdatedAt(date('Y-m-d H:i:s'));
                $rec->save();
                $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $callBackValue);

                $con->commit();
                $rec->free(true);
                $rec = null;
                unset($rec);
            } catch (Exception $e) {
                $con->rollback();
                throw $e;
            }
        }
        // $allRecords->free(true);
        $allRecords = null;
    }

    protected function addJobForMail($message) {
        $taskId = EpjobsContext::getInstance()->addJob('BatchNotification', "paymentSystem/sendEmail", array('subject' => 'Batch Proccesing Failed', 'reason' => $message, 'partialName' => 'mailTemplate'));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = $request->getParameter('partialName');
        $subject = $request->getParameter('subject');
        $reason = $request->getParameter('reason');
        $mailToGroup = Array();

        $mailArr = Settings::getSenMailInformation();

        $partialVars = array('signature' => $mailArr['signature'], 'reason' => $reason);
        $mailTo = $mailArr['mailTo'];
        $mailingOptions = array('mailFrom' => array($mailArr['mailFrom'] => 'Pay4Me'), 'mailTo' => $mailTo, 'mailToGroup' => $mailToGroup, 'mailSubject' => $subject);

        $mailInfo = $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        return $this->renderText($mailInfo);
    }

    public function executeRedirectHomePage(sfWebRequest $request) {
        $serviceId = $request->getParameter('serviceId');
        $merchantDetail = Doctrine::getTable('MerchantService')->find($serviceId);
        if ($this->getUser() && $this->getUser()->isAuthenticated()) {
            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_SECURITY,
                            EpAuditEvent::$SUBCATEGORY_SECURITY_LOGOUT,
                            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGOUT, array()),
                            null);
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        }
        $this->getUser()->signOutUser();
        $this->homePage = $merchantDetail->getMerchantHomePage();

        $substr = substr($this->homePage, '0', 4);
        if ($substr != "http")
            $this->homePage = "http://" . $this->homePage;
        $this->redirect($this->homePage);
    }
    public function executeGetDraftDetails(sfWebRequest $request){
        $user_bank_id = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();

        $this->bank_name = Doctrine::getTable('Bank')->find($user_bank_id)->getBankName();
        $this->transNum = $request->getparameter('txnNo');
        $this->chDetailsForm = new DraftPaymentForm(array('bank_id'=>$user_bank_id), array(), array());
        $this->setTemplate('draftDetails');
    }
    public function executeProcessDraftDetails(sfWebRequest $request){
        $draftNum = $request->getParameter('draft_number');
        $sortcode = $request->getParameter('sort_code');
        $transNum = $request->getParameter('transactionNum');
        Doctrine::getTable('Transaction')->updateTransactionRecord($draftNum, $sortcode, $transNum);
        $this->redirect('paymentSystem/search?txnId=' .$transNum);
    }
    /*
     * execute printReceipt
     */
    public function executePrintReceipt(sfWebRequest $request){
        $txn_no = $request->getParameter('txn_no');
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);
        $bankDetails = Doctrine::getTable('Bank')->getBankName($pfmTransactionDetails['MerchantRequest']['bank_id']);
        $this->bankAcr = $bankDetails['acronym'];
        $this->bankName = $bankDetails['bank_name'];
        $branchDetails = Doctrine::getTable('BankBranch')->getBankBranchName($pfmTransactionDetails['MerchantRequest']['bank_branch_id']);
        $this->branchname = $branchDetails['name'];
        $this->getRequest()->setParameter('template', 'layout_popup');
        $this->executePayOnline($request);
    }

    public function executeCardRedirectHomePage(sfWebRequest $request) {
      $serviceId = $request->getParameter('serviceId');
      $merchantDetail = Doctrine::getTable('MerchantService')->find($serviceId);
      if ($this->getUser() && $this->getUser()->isAuthenticated()) {
        $eventHolder = new pay4meAuditEventHolder(
          EpAuditEvent::$CATEGORY_SECURITY,
          EpAuditEvent::$SUBCATEGORY_SECURITY_LOGOUT,
          EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGOUT, array()),
          null
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
      }
      $this->homePage = $merchantDetail->getMerchantHomePage();
      $substr = substr($this->homePage, '0', 4);
      if ($substr != "http")
        $this->homePage = "http://" . $this->homePage;
      $this->redirect($this->homePage);
    }
    
}
