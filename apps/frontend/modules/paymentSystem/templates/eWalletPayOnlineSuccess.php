<?php //use_helper('Form'); ?>


<div id="printSlip">

    <?php echo ePortal_pagehead($payment_receipt_header, array('id' => 'dynamicHeading')); ?>
    <?php echo form_tag('paymentSystem/payOnline', array('name' => 'pfm_nis_details_form', 'class' => 'wrapForm2 multiForm wrapdiv', 'id' => 'pfm_nis_details_form')) ?>


    <?php if (!($sf_user->hasAttribute('userPayMode') && $sf_user->getAttribute('userPayMode') == 'credit_card') && (isset($userType) && $userType != 'guest_user')){?>
      <?php if(!$ewalletAccountDetailsShow){
          echo $ewalletAccountDetailsShow;
          echo ePortal_legend('User eWallet Account Details');
          if ($errorForEwalletAcount) {
              echo formRowFormatRaw('Error message ', $errorForEwalletAcount);
          } else {
              echo formRowFormatRaw('Account ID', ePortal_displayName($accountObj['account_number']));
              echo formRowFormatRaw('Account Name', ePortal_displayName($accountObj['account_name']));
              // echo formRowFormatRaw('Account Balance',format_amount(ePortal_displayName($accountBalance),1,1));
          }
      }
    }?>
    <?php
    echo ePortal_legend('Application Details');
    echo formRowFormatRaw('Validation Number', $postDataArray['validation_number']);
    echo formRowFormatRaw('Transaction Number', $postDataArray['txnId']);
    if ($MerchantData) {
        foreach ($MerchantData as $k => $v) {
            if(!empty($postDataArray[$k])){
                echo formRowFormatRaw($v . "", $postDataArray[$k]);
            }
            //    echo input_hidden_tag('appId', $nisServiceDetails['MerchantRequest']['MerchantRequestDetails'][$k]);
        }
    }
    echo formRowFormatRaw('Application Type', $postDataArray['serviceType']);
    ?>
    <?php
    echo ePortal_legend('User Details');
    echo formRowFormatRaw('Name', $postDataArray['name']);
    //        echo formRowFormatRaw('Mobile Number:',$postDataArray['mobNo']);
    //        echo formRowFormatRaw('Email:',$postDataArray['email']);
    ?>
    <?php
    echo ePortal_legend('Payment Charges');
    if ($isClubbed == 'no') {
        echo formRowFormatRaw('Application Charges', format_amount($postDataArray['appCharge'], $postDataArray['currency_id']));
        if (!empty($postDataArray['bankCharge'])) {
            echo formRowFormatRaw('Transaction Charges', format_amount($postDataArray['bankCharge'], $postDataArray['currency_id']));
        }
        if (!empty($postDataArray['serviceCharge'])) {
            echo formRowFormatRaw('Service Charges', format_amount($postDataArray['serviceCharge'], $postDataArray['currency_id']));
        }
    } else {
        echo formRowFormatRaw('Application Charges', format_amount($postDataArray['totalCharge'], $postDataArray['currency_id']));
    }
    echo formRowFormatRaw('Total Payable Amount', format_amount($postDataArray['totalCharge'], $postDataArray['currency_id']));

    echo ePortal_legend('Payment Status');
    echo formRowFormatRaw('Payment Mode', $postDataArray['payment_mode']);
    if (array_key_exists('pan', $postDataArray)) {
        if ($postDataArray['pan'])
            echo formRowFormatRaw('Card Number', $postDataArray['pan']);
    }
    if (array_key_exists('approvalcode', $postDataArray)) {
        if ($postDataArray['approvalcode']
            )echo formRowFormatRaw('Approval Code', $postDataArray['approvalcode']);
    }
    if ($postDataArray['payment_status_code'] == 0) {
        echo formRowFormatRaw('Status', 'Payment Successful');
    }
    else
        echo formRowFormatRaw('Status', 'Payment not done');
    echo formRowFormatRaw('Payment Date', formatDate($postDataArray['payment_date']));

    if (array_key_exists('transaction_date', $postDataArray)) {
        if ($postDataArray['transaction_date'])
            echo formRowFormatRaw('Card Debited On', date("d M, Y",strtotime($postDataArray['transaction_date'])));
    }
    ?>
  
</form>
</div>
 <div class="paging pagingFoot">
        <center id="formNav">
            <input type="button" style = "cursor:pointer;" class="formSubmit" value="Print User Receipt" onclick="printSlip('user')">
            <?php $userObj = $sf_context->getUser(); 
            $group_name = $userObj->getGroupName();
            if ($group_name && $group_name == 'guest_user'){
            	$merchant_url = url_for('paymentSystem/cardRedirectHomePage?serviceId='.$merchantServiceId);
            } else {
				$merchant_url = url_for('paymentSystem/redirectHomePage?serviceId='.$merchantServiceId);	
			}?>
            <input type="button" style = "cursor:pointer;" id="back_to_home_btn" class="formSubmit" value="Back to<?php echo " ".$postDataArray['merchant_name'];?>" onClick='location.href="<?php echo $merchant_url?>"'>

        </center>
    </div>
    




<script>
//    $("#back_to_home_btn").click(function()
//    {
//        //            alert('kkkkkkkkkkkkkkkkkk');
//        var postUrl = "<?php //echo url_for('@sf_guard_signout'); ?>";
//        getLoginStatusAndLogout(postUrl);
//        alert("<?php //echo $homePage; ?>")
//        //window.parent.location ="<?php //echo $homePage; ?>";
//    });
    function printSlip(mode){

        defHeading = $('#dynamicHeading').html();
        heading = "";
        if(mode =='user'){
            heading +="User Receipt";
        }
        $('#dynamicHeading').html(heading);
        html = '';
        html += $('#printSlip').html();
        printMe(html,true);
        $('#dynamicHeading').html(defHeading);


    }
    function getLoginStatusAndLogout(postUrl){
        $("#back_to_home_btn").load(postUrl,function (data){
        
      
            //            location.reload();
     

        });

    }
</script>

<?php
    if (isset($trans_number) && ($trans_number != "")) {
        $msg = "Payment has already been made for the item against Transaction Number - " . $trans_number;
        $cancel_url = url_for('admin/eWalletUser');
?>

        <SCRIPT language="JavaScript1.2">
            var msg = "<?php echo $msg; ?>";
            var cancel_url = "<?php echo $cancel_url; ?>";
            function displaypop()
            {
                alert(msg) ;
                document.getElementById('printSlip').style.visibility="visible";
                document.getElementById('printSlip').style.height="";
            }
        </SCRIPT>
        <body onLoad="javascript: displaypop()" >
        </body>
<?php } ?>
