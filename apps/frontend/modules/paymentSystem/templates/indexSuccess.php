<?php echo ePortal_pagehead(" "); ?>
<?php //use_helper('Form') ?>
<?php echo form_tag('paymentSystem/search',array('name'=>'pfm_nis_details_form', 'id'=>'pfm_nis_details_form', 'onsubmit' => 'return validate_nis_index_form(this)')) ?>
<div class="wrapForm2">

<?php echo ePortal_legend(__('pay4me Bank Payment'));?>
    <input type="hidden" name="name" value="<?php echo $name?>">
    <?php echo formRowFormatRaw($form['txnId']->renderLabel(),$form['txnId']->render());
    echo formRowFormatRaw(' ', "<input type='text' disabled='disabled' value='OR' style='font-weight:bold;color:#056CB6;width:22px;' readonly>");
    echo formRowFormatRaw($form['type']->renderLabel(),$form['type']->render());
    ?>
    <div id="response"></div>
    <div class="divBlock">
        <center>
            <?php echo button_to(__('Create'),'',array('class'=>'formSubmit', 'onClick'=>'validateSearchForm()')); ?>
        </center>
    </div>
</div>
</form>
</div>

<script language="Javascript">
  var type= "<?php echo $type;?>";
  if(type != ""){display_options();}

  function validateSearchForm(){
    var txn_no = $("#txnId").val();
    var application_type = $("#type").val();
    if(txn_no != ""){
       $('#pfm_nis_details_form').submit();
    }
    else if(application_type!=""){
      $('#pfm_nis_details_form').submit();
    }
    else if((txn_no == "") && (application_type == "")){
      alert("<?php echo __("Please enter Transaction Number or Select the Application Type")?>");
    }
  }

 function display_options(){
   var merchant_service_id = document.getElementById("type").value;
    if(merchant_service_id != "")
      {
        var module = "nis";
        getMerchantDetails(merchant_service_id, '<?php echo url_for('paymentSystem/GetMerchantDetails'); ?>');
      }
 }



</script>
