<?php echo ePortal_pagehead("Acknowledgment Receipt"); ?>
<?php //use_helper('Form') ?>
<script type="text/javaScript">
  javascript:window.print();
</script>
<div class='wrapForm2'>
  
    <?php
    echo ePortal_legend('Payment Details');
    echo formRowFormatRaw('Transaction Number:',$nisServiceDetails['txnId']);
    echo formRowFormatRaw('Application Id:',$nisServiceDetails['appId']);
    echo formRowFormatRaw('Reference Number:',$nisServiceDetails['refNo']);
    echo formRowFormatRaw('Application Type:',$nisServiceDetails['type']);
    ?>
  
    <?php
    echo ePortal_legend('User Details');
    echo formRowFormatRaw('First Name:',$nisServiceDetails['fName']);
    echo formRowFormatRaw('Lat Name:',$nisServiceDetails['lName']);
    echo formRowFormatRaw('Mobile Number:',$nisServiceDetails['mobNo']);
    echo formRowFormatRaw('Email:',$nisServiceDetails['email']);
    ?>
  
    <?php
    echo ePortal_legend('Payment Charges');
    echo formRowFormatRaw('Application Charges:',format_amount($nisServiceDetails['appCharge'],1));
    if($nisServiceDetails['bankCharge'] > 0) {

      echo formRowFormatRaw('Bank Service Charges:',format_amount($nisServiceDetails['bankCharge'],1));
    }
    echo formRowFormatRaw('Total Payable Amount:',format_amount($nisServiceDetails['totalCharge'],1));
    ?>
  
</div>