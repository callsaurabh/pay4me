<?php //use_helper('Form') ;
  use_javascript('jquery-1.2.6.js','first');
  use_javascript('jquery-ui.min.js','first');
  include_http_metas();
  include_metas();
  include_title(); ?>
  <link rel="shortcut icon" href="/favicon.ico" />
  <?php include_javascripts();
  include_stylesheets();
  echo stylesheet_tag('ie6');
  ?>


<div id="printSlip" class='wrapForm2'<?php if(isset($trans_number) && ($trans_number!="")){echo "style='visbility:hidden;height:1px;overflow-y:hidden'"; }?>>

  <?php echo ePortal_pagehead("Payment Receipt", array('id'=>'dynamicHeading')); ?>
  <?php echo form_tag('nis/payOnline',array('name'=>'pfm_nis_details_form','class'=>'dlForm multiForm', 'id'=>'pfm_nis_details_form')) ?>


    <?php
    echo ePortal_legend('Application Details');
    echo formRowFormatRaw('Validation Number:',$postDataArray['validation_number']);
    echo formRowFormatRaw('Transaction Number:',$postDataArray['txnId']);
    if($MerchantData) {
      foreach($MerchantData as $k=>$v) {
        echo formRowFormatRaw($v.":",$postDataArray[$k]);
      }
    }
    echo formRowFormatRaw('Application Type:',$postDataArray['serviceType']);
    ?>
    <?php
    echo ePortal_legend('User Details');
    echo formRowFormatRaw('Name:',ePortal_displayName($postDataArray['name']));
    ?>
    <?php
    echo ePortal_legend('Payment Charges');
    echo formRowFormatRaw('Application Charges:',format_amount($postDataArray['appCharge'], 1));
    if($postDataArray['bankCharge'] > 0){
      echo formRowFormatRaw('Transaction Charges:',format_amount($postDataArray['bankCharge'], 1));
    }
    echo formRowFormatRaw('Service Charges:',format_amount($postDataArray['serviceCharge'], 1));
    echo formRowFormatRaw('Total Payable Amount:',format_amount($postDataArray['totalCharge'], 1));
     echo ePortal_legend('Payment Status');
    if($postDataArray['payment_status_code'] == 0){echo formRowFormatRaw('Status:','Payment Successful');
      echo formRowFormatRaw('Payment Date:',formatDate($postDataArray['payment_date']));}
    else{echo formRowFormatRaw('Status','Payment Pending');}
    ?>
   <div class="dsTitle2">
  <center id="formNav">
    <input type="button" style = "cursor:pointer;" class="formSubmit" value="Print User Receipt" onclick="printSlip()">
  </center>
  </div>
</form>

</div>

<script>
  function printSlip(){

    defHeading = $('#dynamicHeading').html();
    heading = "";

    $('#dynamicHeading').html(heading);
    html = '';
    html += $('#printSlip').html();
    printMe(html,true);
    $('#dynamicHeading').html(defHeading);
  }
</script>

