<?php //use_helper('Form')  ?>
<script language="Javascript">
    function checkAll()
    {
        if(document.getElementById('disclaimer').checked == false || document.getElementById('application_charges').checked == false || (($("#bank_charges").length) &&  (document.getElementById('bank_charges').checked == false)) || (($("#service_charges").length) && (document.getElementById('service_charges').checked == false)) || document.getElementById('total_charges').checked == false)
        {
<?php if ($isClubbed == 'no') { ?>
                alert('Please select All Charges, Total Amount and Disclaimer');
<?php } else { ?>
                    alert('Please select Application Charges, Total Amount and Disclaimer');
<?php } ?>
                return false;
            }
            //        document.forms['pfm_nis_details_form'].submit();
            $('#formNav').hide();
            var txnId = $('#txnId').val();
            var url = '<?php echo url_for('paymentSystem/pay'); ?>';
            $('#loader').show();

            $.post(url, {txnId:txnId}, function(data){
                $('#wrapForm2').html(data);
                $('#loader').hide();
            });
            return true;
        }
</script>

<div align="center" id="loader" class="transparent_class"><div style="padding-top:265px;"><?php echo image_tag('loader.gif'); ?></div></div>
<div id="wrapForm2" class='wrapForm2'>

    <?php echo ePortal_pagehead($nisServiceDetails['MerchantRequest']['Merchant']['name'] . '&nbsp;' . __("Acknowledgment Receipt")); ?>
    <div class="Y10 r"><a href='#'class="descriptionArea" onclick="javascript:printMe('printSlip')">Print <?php echo $nisServiceDetails['MerchantRequest']['Merchant']['name'] ?> Acknowledgment Slip</a></div>
    <br class="pixbr"/>
    <div id='printSlip'>
        <div id="ackPaymentSlip">


            <?php
//            echo "<pre>";
//            print_r($nisServiceDetails);
//            exit;
            ?>
            <?php echo form_tag($sf_context->getModuleName() . '/validate', array('name' => 'validate_form', 'id' => 'validate_form')) ?>
            <input type="hidden" value="<?php echo $txnId; ?>" name="txnId">
            <input type="hidden" value="<?php echo $isClubbed; ?>" name="clubbed">
            <div class="spht">
<?php
            echo ePortal_legend(__('Application Details'));
            echo formRowFormatRaw('Transaction Number:', $nisServiceDetails['pfm_transaction_number']);
            echo $form['txnId']->render();
            if ($MerchantData) {
                foreach ($MerchantData as $k => $v) {
                     if(!empty($nisServiceDetails['MerchantRequest']['MerchantRequestDetails'][$k])){
                        echo formRowFormatRaw("$v:", $nisServiceDetails['MerchantRequest']['MerchantRequestDetails'][$k]);
                     }
                }
            }
            echo formRowFormatRaw('Application Type:', $nisServiceDetails['MerchantRequest']['MerchantService']['name']);
            echo $form['type']->render();
?>
                <?php
                echo ePortal_legend(__('User Details'));
                echo formRowFormatRaw('Name:', ePortal_displayName($nisServiceDetails['MerchantRequest']['MerchantRequestDetails']['name']));
                echo $form['name']->render();
                ?>
                <?php
                if ($nisServiceDetails['payment_mode_option_id'] == 14) {
                    $bankDetails = Doctrine::getTable('Bank')->getBankName($nisServiceDetails['bank_id']);
                    echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details'));
                    echo formRowFormatRaw('Bank Name:',$bankDetails['bank_name']);
                    echo formRowFormatRaw('Bank Sort Code:',$nisServiceDetails['sort_code']);
                    echo formRowFormatRaw('Cheque Number:', $nisServiceDetails['check_number']);
                    echo formRowFormatRaw('Payee Account Number:', $nisServiceDetails['account_number']);?>
                    <div class="dsTitle2"><?php echo $form['disclaimer_cheque_details']->render() ?> <?php echo $form['disclaimer_cheque_details']->renderLabel() ?></div>
                <?php }
                ?>
                 <?php
                if ($nisServiceDetails['payment_mode_option_id'] == 15) {
                    echo ePortal_legend(__(pfmHelper::getDraftDisplayName().' Details '));
                    $bankDetails = Doctrine::getTable('Bank')->getBankName($nisServiceDetails['bank_id']);
                    echo formRowFormatRaw('Bank Name:',$bankDetails['bank_name']);
                    echo formRowFormatRaw('Sort Code:',$nisServiceDetails['sort_code']);
                    echo formRowFormatRaw('Bank Draft Number:',$nisServiceDetails['check_number']);?>
                     <div class="dsTitle2"><?php echo $form['disclaimer_draft_details']->render() ?> <?php echo $form['disclaimer_draft_details']->renderLabel() ?></div>
                    <?php }
                ?>
                <?php echo ePortal_legend(__('Payment Charges')); ?>

                <?php
                if ($isClubbed == 'no') {
                    echo formRowFormatRaw($form['application_charge']->render() . '&nbsp;' . $form['application_charge']->renderLabel() . ":", format_amount($nisServiceDetails['MerchantRequest']['item_fee'], $currency_id, 0));


                    echo $form['appCharge']->render();
                    if (!empty($nisServiceDetails['MerchantRequest']['bank_charge'])) {
                        echo formRowFormatRaw($form['bank_charges']->render() . '&nbsp;' . $form['bank_charges']->renderLabel() . ":", format_amount($nisServiceDetails['MerchantRequest']['bank_charge'], $currency_id, 0));

                        echo $form['bankCharge']->render();
                    }

                    if (!empty($nisServiceDetails['MerchantRequest']['service_charge'])) {
                        echo formRowFormatRaw($form['service_charge']->render() . '&nbsp;' . $form['service_charge']->renderLabel() . ":", format_amount($nisServiceDetails['MerchantRequest']['service_charge'], $currency_id, 0));

                        echo $form['serviceCharge']->render();
                    }
                } else {
                    echo formRowFormatRaw($form['application_charge']->render() . '&nbsp;' . $form['application_charge']->renderLabel() . ":", format_amount($nisServiceDetails['total_amount'], $currency_id, 0));

                    echo $form['appCharge']->render();

                    if (!empty($nisServiceDetails['MerchantRequest']['bank_charge']))
                        echo $form['bankCharge']->render();


                    if (!empty($nisServiceDetails['MerchantRequest']['service_charge']))
                        echo $form['serviceCharge']->render();
                }
                echo formRowFormatRaw($form['total_charges']->render() . '&nbsp;' . $form['total_charges']->renderLabel() . ":", format_amount($nisServiceDetails['total_amount'], $currency_id, 0));

                echo $form['totalCharge']->render();
                ?>
                <div class="dsTitle2"><?php echo $form['disclaimer']->render() ?> <?php echo $form['disclaimer']->renderLabel() ?></div>



            </div>
            <div class="divBlock">
                <center>
                <?php if ($form->isCSRFProtected()) : ?>
                <?php echo $form['_csrf_token']->render(); ?>
                <?php endif; ?>

                    </center>
                </div>
<?php if ($show) {//echo button_to('Payment','', array('class'=>'formSubmit', 'onClick'=>'checkAll()')); ?>
                    <input type = submit name="submit" value="<?php echo __('Payment') ?>" class="formSubmit">
                    </form>
<?php
                    } else {
?>
                <font size="2" color="red"><b>You are restricted to pay for this Merchant Service</b></font>
                    <?php } ?>
            <!-- </form>-->
            <div class="clear"></div>
        </div>
    </div>
</div>


