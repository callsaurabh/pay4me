<div id="loadArea">
    <h2> </h2>
    <div class="clear"></div>
    <div class="wrapForm2"><?php use_helper('ePortal');
    echo ePortal_legend(__(pfmHelper::getDraftDisplayName() . ' Details')); ?>
        <div class="wrapLogos">
            <form id="chkDetails" action="<?php echo url_for('paymentSystem/processDraftDetails') ?>" method="post" >

                <div class="dsTitle4Fields">Bank Name</div>
                <div class="dsInfo4Fields">
                    <label><?php echo $bank_name; ?></label>
                </div>
                <div class="clear"></div>
                <div class="dsTitle4Fields"><?php echo $chDetailsForm['sort_code']->renderLabel(); ?><span class="required"> *</span>     </div>
                <div class="dsInfo4Fields">
                    <label><?php echo $chDetailsForm['sort_code']->render(); ?></label>
                    <div class="clearfix"><div class="cRed" id="err_cs_sort_code"></div>
                        <div class="cRed" id="err_sort_code"><?php echo $chDetailsForm['sort_code']->renderError(); ?></div></div>
                </div><?php echo $chDetailsForm['bank_id']->render(); ?>
                <div name="sort_code_validated" style="display:none" id="sort_code_validated">
                    <div class="clear"></div>
                    <div class="dsTitle4Fields"><?php echo $chDetailsForm['draft_number']->renderLabel(); ?><span class="required"> *</span></div>
                    <div class="dsInfo4Fields">
                        <label><?php echo $chDetailsForm['draft_number']->render(); ?></label>
                        <div class="cRed" id="err_cs_draft_number" ></div>
                        <div class="cRed" id="err_draft_number" style="display:none;"><?php echo $chDetailsForm['draft_number']->renderError(); ?></div>
                    </div>
                    <input type="hidden" name="transactionNum" value = <?php echo $transNum ?>>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <dl id="">
                    <div class="dsTitle4Fields">&nbsp;</div>
                    <div class="dsInfo4Fields">
                        <input type="hidden" name="payment_mode"  id="payment_mode" value="<?php echo $payment_mode; ?>" />
                        <?php echo $chDetailsForm['_csrf_token']; ?>
                        <input type="submit" class="formSubmit" name="continue" value="Continue" id="submit_form" style="display:none">
                        <input type="button" class="formSubmit"
                               id="validateSortCode" name="proceed" value="Continue" >
                    </div></dl>
                <div class="clear"></div>
            </form>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        if($('#draft_number').val()!=''){
            //alert('jgjghg');
            $('#sort_code_validated').attr('style','display:inline');
            $('#validateSortCode').attr("style",'display:none');
            $('#submit_form').attr("style",'display:inline');

        }

        $('#validateSortCode').click(function(){
            if($('#sort_code').val()=="")
            {
                $('#err_cs_sort_code').show();
                $('#err_cs_sort_code').html("Please enter Sort Code");
                $('#err_sort_code').html("");
                $('#err_sort_code').hide();
            }
            else
            {
                if(alphaNumricRegexCheck($('#sort_code').val()) == null){
                    if($('#sort_code').val().length==3)
                    {
                        var t=sortCodeValidation();
                    }
                    else
                    {
                        $('#err_cs_sort_code').show();
                        $('#err_cs_sort_code').html("Please enter 3 digits");
                        $('#err_sort_code').html("");
                        $('#err_sort_code').hide();
                    }
                }
                else
                {
                    $('#err_cs_sort_code').show();
                    $('#err_cs_sort_code').html("Invalid Sort Code(Only alphanumeric characters accepted)");
                    $('#err_sort_code').html("");
                    $('#err_sort_code').hide();
                }
            }
        });
        function sortCodeValidation()
        {
          var bank_id=$('#bank_id').val();
          
            var url = '<?php echo url_for("paymentProcess/validateSortCode", true) ; ?>';
            var sort_code_var=$('#sort_code').val().replace(" ","");
            $.post(url, {bank_id:bank_id,sort_code:sort_code_var}, function(data){
                if(data=='logout'){
                            location.reload();
                        } else
                            if(data==true)
                        {
                            $('#validateSortCode').attr("style",'display:none');
                            $('#submit_form').attr("style",'display:inline');
                            var sortcode_style=$('#sort_code_validated').attr("style").toLowerCase();
                            if(sortcode_style.indexOf('display')!=-1 && sortcode_style.indexOf('none')!=-1){
                                $('#err_cs_sort_code').html("");
                                $('#err_cs_sort_code').hide();
                                $('#sort_code').attr("readonly","readonly");
                                $('#err_sort_code').html("");
                                $('#err_sort_code').hide();
                                $('#err_draft_number').html("");
                                $('#err_cs_draft_number').html("");
                                $('#draft_number').val("");

                            }


                            $('#err_cs_sort_code').html("");
                            $('#err_cs_sort_code').hide();
                            $('#err_sort_code').html("");
                            $('#err_sort_code').hide();
                            $('#sort_code_validated').attr('style','display:inline');
                            if($('#draft_number').val()!='' ){

                                $('#sort_code_validated').attr('style','display:inline');
                                $('#validateSortCode').attr("style",'display:none');
                                $('#submit_form').attr("style",'display:inline');

                            }

                        }
                        else
                        {

                            $('#sort_code_validated').attr('style','display:none');
                            $('#err_cs_sort_code').html("Invalid Sort Code ");
                            $('#err_cs_sort_code').show();
                            $('#validateSortCode').attr("style",'display:inline');
                            $('#submit_form').attr("style",'display:none');

                            $('#err_sort_code').html("");
                            $('#err_sort_code').hide();

                        }


                    });


                }
        function alphaNumricRegexCheck(text)
        {
            var regEx=new RegExp('[^a-zA-Z0-9]+');
            return regEx.exec(text);


        }
        $('#chkDetails').submit(function(){
            var err=0;




            if($('#draft_number').val()=="")
            {
                $('#err_cs_draft_number').html("Please enter Bank Draft Number");
                $('#err_draft_number').html("");
                err++;

            }
            else
            {
                if(alphaNumricRegexCheck($('#draft_number').val()) == null){

                    $('#err_cs_draft_number').html("");
                    $('#err_draft_number').html("");

                }
                else
                {
                    $('#err_cs_draft_number').html("Invalid Bank Draft Number(Only alphanumeric characters accepted)");
                    $('#err_draft_number').html("");
                    err++;
                }

            }


            if(err>0){

                if(alphaNumricRegexCheck($('#sort_code').val()) == null){
                    sortCodeValidation();
                }
                else
                {
                    $('#err_cs_sort_code').show();
                    $('#err_cs_sort_code').html("Invalid Sort Code(Only alphanumeric characters accepted)");
                    $('#err_sort_code').html("");
                    $('#err_sort_code').hide();


                }
            }
            if($('#sort_code').val()=="")
            {
                $('#err_cs_sort_code').show();
                $('#err_cs_sort_code').html("Please enter Sort code");
                $('#err_sort_code').html("");
                $('#err_sort_code').hide();

                err++;

            }
            if(err>0){

                return false;
            }
        });
    });
</script>