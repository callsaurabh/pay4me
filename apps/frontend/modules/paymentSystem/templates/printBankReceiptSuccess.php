<?php echo ePortal_pagehead("Bank Receipt"); ?>
<?php //use_helper('Form') ?>
<script type="text/javaScript">
    javascript:window.print();
</script>
<div class='wrapForm2'>
    
        <?php
        echo ePortal_legend('Application Details');
        echo formRowFormatRaw('Validation Number:',$nisServiceDetails['MerchantRequest']['validation_number']);
        echo formRowFormatRaw('Transaction Number:',$nisServiceDetails['pfm_transaction_number']);
        echo formRowFormatRaw('Application Id:',$nisServiceDetails['MerchantRequest']['MerchantRequestDetails']['iparam_one']);
        echo formRowFormatRaw('Reference Number:',$nisServiceDetails['MerchantRequest']['MerchantRequestDetails']['iparam_two']);
        echo formRowFormatRaw('Application Type:',$serviceType);
        ?>
    
        <?php
        echo ePortal_legend('User Details');
        echo formRowFormatRaw('Name:',$nisServiceDetails['MerchantRequest']['MerchantRequestDetails']['name']);
        ?>
    
        <?php
        echo ePortal_legend('Payment Charges');
        echo formRowFormatRaw('Application Charges:',format_amount($nisServiceDetails['MerchantRequest']['item_fee'],1));
        if(!empty($nisServiceDetails['MerchantRequest']['bank_charge'])){
          echo formRowFormatRaw('Transaction Charges:',format_amount($nisServiceDetails['MerchantRequest']['bank_charge'],1));
         }
        if(!empty($nisServiceDetails['MerchantRequest']['service_charge'])){

        echo formRowFormatRaw('Transaction Charges:',format_amount($nisServiceDetails['MerchantRequest']['service_charge'],1));
        }
        echo formRowFormatRaw('Total Payable Amount:',format_amount($nisServiceDetails['total_amount'],1));
        ?>
   
</div>