<?php //use_helper('Form') ;  ?>
<?php use_helper('I18N'); ?>

<div id="printSlip" <?php // if(isset($trans_number) && ($trans_number!="")){echo "style='visbility:hidden;height:1px;overflow-y:hidden'"; } ?>>
    

<?php echo ePortal_pagehead(__($payment_receipt_header), array('id' => 'dynamicHeading')); ?>
    <div class="wrapForm2">
<?php echo form_tag('paymentSystem/payOnline', array('name' => 'pfm_nis_details_form', 'id' => 'pfm_nis_details_form')) ?>



<?php
echo ePortal_legend(__('Application Details'));
echo formRowFormatRaw(__('Validation Number') . ":", $postDataArray['validation_number']);
echo formRowFormatRaw(__('Transaction Number') . ":", $postDataArray['txnId']);
if ($MerchantData) {
    foreach ($MerchantData as $k => $v) {
        echo formRowFormatRaw(__($v) . ":", $postDataArray[$k]);
        //    echo input_hidden_tag('appId', $nisServiceDetails['MerchantRequest']['MerchantRequestDetails'][$k]);
    }
}
echo formRowFormatRaw(__('Application Type') . ":", $postDataArray['serviceType']);
?>

        <?php
        echo ePortal_legend(__('User Details'));
        echo formRowFormatRaw(__('Name') . ":", ePortal_displayName($postDataArray['name']));
        //        echo formRowFormatRaw('Mobile Number:',$postDataArray['mobNo']);
        //        echo formRowFormatRaw('Email:',$postDataArray['email']);
        ?>
        <?php
        if ($postDataArray['paymentModeName']==sfConfig::get('app_payment_mode_option_Cheque')) {
            echo ePortal_legend(__(pfmHelper::getChequeDisplayName().' Details'));
            echo formRowFormatRaw('Bank Name:', $postDataArray['bank_name']);
            echo formRowFormatRaw('Bank Sort Code:', $postDataArray['sort_code']);
            echo formRowFormatRaw('Cheque Number:', $postDataArray['check_number']);
            echo formRowFormatRaw('Payee Account Number:', $postDataArray['account_number']);
        }
        ?>
         <?php

        if ($postDataArray['paymentModeName']==sfConfig::get('app_payment_mode_option_bank_draft')) {
            echo ePortal_legend(__(pfmHelper::getDraftDisplayName() . ' Details'));
            echo formRowFormatRaw('Bank Name:', $postDataArray['bank_name']);
            echo formRowFormatRaw('Sort Code:', $postDataArray['sort_code']);
            echo formRowFormatRaw('Bank Draft Number:', $postDataArray['draft_number']);

        }
        ?>
        <?php
        echo ePortal_legend(__('Payment Charges'));
        if ($isClubbed == 'no') {
            echo formRowFormatRaw(__('Application Charges') . ":", format_amount($postDataArray['appCharge'], $postDataArray['currency_id']));
            if (!empty($postDataArray['bankCharge'])) {
                echo formRowFormatRaw(__('Transaction Charges') . ":", format_amount($postDataArray['bankCharge'], $postDataArray['currency_id']));
            }
            if (!empty($postDataArray['serviceCharge'])) {
                echo formRowFormatRaw(__('Service Charges') . ":", format_amount($postDataArray['serviceCharge'], $postDataArray['currency_id']));
            }
        } else {
            echo formRowFormatRaw(__('Application Charges') . ":", format_amount($postDataArray['totalCharge'], $postDataArray['currency_id']));
        }
        echo formRowFormatRaw(__('Total Payable Amount') . ":", format_amount($postDataArray['totalCharge'], $postDataArray['currency_id']));
        echo ePortal_legend(__('Payment Status'));
        if ($postDataArray['payment_status_code'] == 0) {
            echo formRowFormatRaw(__('Status') . ":", __('Payment Successful'));
            echo formRowFormatRaw(__('Payment Mode') . ":",$postDataArray['payment_mode']);
            echo formRowFormatRaw(__('Payment Date') . ":", formatDate($postDataArray['payment_date']));
        } else {
            echo formRowFormatRaw(__('Status'), __('Payment Pending'));
        }
        ?>

        <div class="divBlock">
            <center  id="formNav">

                <input type="button" style = "cursor:pointer;" value="<?php echo __("Print Receipt") ?>" class="formSubmit" onclick="printSlip('default')">
            </center>
        </div>
        </form>
    </div>
</div>




<script>
    function printSlip(mode){

        defHeading = $('#dynamicHeading').html();
        heading = "";
        if(mode =='user'){
            heading +="User Receipt";
        }else if (mode =='bank'){
            heading +="Bank Receipt";
        }else if (mode =='default'){
            heading +="Receipt";
        }
        $('#dynamicHeading').html(heading);
        html = '';
        html += $('#printSlip').html();
        printMe(html,true);
        $('#dynamicHeading').html(defHeading);


    }
</script>

<?php
        if (isset($trans_number) && ($trans_number != "")) {
            $msg = "Payment has already been made for the item against Transaction Number - " . $trans_number;
            $cancel_url = url_for('admin/bankUser');
?>

            <SCRIPT language="JavaScript1.2">
                var msg = "<?php echo $msg; ?>";
                var cancel_url = "<?php echo $cancel_url; ?>";
                function displaypop()
                {
                    document.getElementById('printSlip').style.visibility="visible";
                    document.getElementById('printSlip').style.height="";
                }
            </SCRIPT>
            <body onLoad="javascript: displaypop()" >
            </body>
<?php } ?>
