<?php

/**
 * bank_user actions.
 *
 * @package    mysfp
 * @subpackage bank_user
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class bank_userActions extends sfActions {
    public function executeIndex(sfWebRequest $request) {
        $bankUser_obj = bank_userServiceFactory::getService(bank_userServiceFactory::$TYPE_BASE);
        $this->bank_user_list = $bankUser_obj->getAllRecords();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('BankUser',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->bank_user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();

    }

    public function executeShow(sfWebRequest $request) {
        $this->bank_user = Doctrine::getTable('BankUser')->find($request->getParameter('id'));
        $this->forward404Unless($this->bank_user);
    }

    public function executeNew(sfWebRequest $request) {
        $this->bank_branch_id="";//initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
        $this->bank_id=""; //same as the reason above
        $this->form = new BankUserForm();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));
        $bank_branch_id=$request->getPostParameters('bank_branch_id');
        $this->bank_branch_id=$bank_branch_id['bank_user']['bank_branch_id'];
        $bank_id=$request->getPostParameters('bank_id');
        $this->bank_id=$bank_id['bank_user']['bank_id'];
        $this->form = new BankUserForm();
        $this->processForm($request, $this->form);
        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($bank_user = Doctrine::getTable('BankUser')->find($request->getParameter('id')), sprintf('Object bank_user does not exist (%s).', $request->getParameter('id')));
        $this->bank_user = Doctrine::getTable('BankUser')->find($request->getParameter('id'));//get the bank_id of the bank_branch
        $this->bank_branch_id = $this->bank_user->bank_branch_id;
        $this->bank_id = $this->bank_user->bank_id;
        $this->form = new BankUserForm($bank_user);
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($bank_user = Doctrine::getTable('BankUser')->find($request->getParameter('id')), sprintf('Object bank_user does not exist (%s).', $request->getParameter('id')));
        $this->form = new BankUserForm($bank_user);
        $bank_branch_id=$request->getPostParameters('bank_branch_id');
        $this->bank_branch_id=$bank_branch_id['bank_user']['bank_branch_id'];
        $bank_id=$request->getPostParameters('bank_id');
        $this->bank_id=$bank_id['bank_user']['bank_id'];
        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($bank_user = Doctrine::getTable('BankUser')->find($request->getParameter('id')), sprintf('Object bank_user does not exist (%s).', $request->getParameter('id')));
        $bank_user->delete();

        $this->redirect('bank_user/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $bank_user = $form->save();

            if(($request->getParameter('action')) == "update") {
                $this->getUser()->setFlash('notice', sprintf('Bank User updated successfully'));
            }
            else if(($request->getParameter('action')) == "create") {
                    $this->getUser()->setFlash('notice', sprintf('Bank User added successfully'));
                }

            $this->redirect('bank_user/index');
        }
    }

    public function executeBankBranch(sfWebRequest $request) {
        $this->setTemplate(FALSE);
        $bank_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        $str = $bank_obj->BankBranch($request->getParameter('bank_id'),$request->getParameter('bank_branch_id'));
        return $this->renderText($str);
    }
    /*
    * action : BankUserList
    * WP047 : CR080
    */
      public function executeBankUserList(sfWebRequest $request) {
         $this->module=$this->moduleName;
         $this->action=$this->actionName;
         $this->bank='';
         $this->country='';
         $this->branch=''; 
         $this->user_role='';
         $this->user_name='';
         $objApi = new UserRoleHelper();
         $arrBankUserRole = $objApi->getBankUserList();
         $this->postDataArray = $request->getParameterHolder()->getAll();
         if(isset($this->postDataArray['bank_user'])){
            $this->postDataArray=$this->postDataArray['bank_user'];
         }
         if(isset($this->postDataArray)){
             if(isset($this->postDataArray['user_name']) && $this->postDataArray['user_name']!=""  && $this->postDataArray['user_name']!="BLANK" ){
               $this->user_name = $this->postDataArray['user_name'];
             }
             if(isset($this->postDataArray['bank']) && $this->postDataArray['bank']!="" && $this->postDataArray['bank']!="BLANK" ){
               $this->bank = $this->postDataArray['bank'];
             }
             if(isset($this->postDataArray['country']) && $this->postDataArray['country']!=""  && $this->postDataArray['country']!="BLANK" ){
               $this->country = $this->postDataArray['country'];
             }
             if(isset($this->postDataArray['branch']) && $this->postDataArray['branch']!=""  && $this->postDataArray['branch']!="BLANK" ){
               $this->branch = $this->postDataArray['branch'];
             }
             if(isset($this->postDataArray['user_role']) && $this->postDataArray['user_role']!=""  && $this->postDataArray['user_role']!="BLANK" ){
               $this->user_role = $this->postDataArray['user_role'];
               $objGroup = Doctrine::getTable('SfGuardGroup')->findByName($this->user_role);
               $this->user_role_id= $objGroup->getFirst()->getId();
             }
          
         }
         $this->form = new ManageBankUserForm(array(),array('bank_user_role'=>$arrBankUserRole,
         'bankId'=>$this->bank,'countryId'=>$this->country,'user_role'=>$this->user_role));
         //set value when we are geting value through paging navigation
         if(isset($this->postDataArray['bank']) && $this->postDataArray['user_role']!="BLANK"){
            $this->form->setDefault('bank', $this->postDataArray['bank']);  
         }
         if(isset($this->postDataArray['country']) && $this->postDataArray['country']!="BLANK"){
             $this->form->setDefault('country', $this->postDataArray['country']);
         }
         if(isset($this->postDataArray['branch']) && $this->postDataArray['branch']!="BLANK"){
            $this->form->setDefault('branch', $this->postDataArray['branch']);
         }
        
         if(isset($this->postDataArray['user_role']) && $this->postDataArray['user_role']!="BLANK"){
            $this->form->setDefault('user_role', $this->postDataArray['user_role']);
         }
       
         if(isset($this->postDataArray['user_name']) && $this->postDataArray['user_name']!="BLANK"){
             $this->form->setDefault('user_name', $this->postDataArray['user_name']);
         }
         $this->validForm=true;
         if($request->isMethod('post')){
             $this->form->bind($request->getParameter('bank_user'));
             if(!$this->form->valid()){
                 $this->validForm=false;
             }
         }
         $queryObj=Doctrine::getTable('BankUser')->getAllBankUserList($this->user_name,
           $this->user_role_id, $this->bank, $this->country,$this->branch);
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PaymentSplitDetails',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($queryObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
}
