<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $bank_user->getid() ?></td>
    </tr>
    <tr>
      <th>User:</th>
      <td><?php echo $bank_user->getuser_id() ?></td>
    </tr>
    <tr>
      <th>Bank:</th>
      <td><?php echo $bank_user->getbank_id() ?></td>
    </tr>
    <tr>
      <th>Bank branch:</th>
      <td><?php echo $bank_user->getbank_branch_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $bank_user->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $bank_user->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $bank_user->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $bank_user->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $bank_user->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('bank_user/edit?id='.$bank_user->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('bank_user/index') ?>">List</a>
