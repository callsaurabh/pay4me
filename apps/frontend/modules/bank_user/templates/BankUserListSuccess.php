<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<div class="wrapForm2">
<?php  echo form_tag($sf_context->getModuleName().'/BankUserList',' class="" 
        id="bank_user_list_form" name="bank_user_list_form"');
        if('portal_admin'==sfcontext::getInstance()->getUser()->getGroupName()) { 
         echo ePortal_legend("Manage All Bank Users Section ");  
         }else{
         echo ePortal_listinghead('View All Bank Users Section');    
         } 
         echo $form;     ?>
       <div class="divBlock">
        <center id="multiFormNav">
             <input type="Submit" value='Search' class="formSubmit">
        </div>
      </div>
</form>
<div class="clear"></div>
<div class="searchresult">
<?php if(isset($validForm))  {
 if('portal_admin'==sfcontext::getInstance()->getUser()->getGroupName()) { 
 echo ePortal_listinghead('Manage All Bank Users');
 }else{
 echo ePortal_listinghead('View All Bank Users');    
 }    ?>  
  <div class="wrapTable">
    <?php
    if ($pager->getNbResults() > 0) {
        if ($user_name == "")
            $user_name = "BLANK";
        if ($user_role == "")
            $user_role = "BLANK";
        if ($bank == "")
            $bank = "BLANK";
        if ($country == "")
            $country = "BLANK";
        if ($branch == "")
            $branch = "BLANK";
        ?>
<?php } ?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour">
            <th>
                <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?>
                    </b> results matching your criteria.</span>
                <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?>
                    </b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b>
                            <?php echo $pager->getNbResults(); ?></b>  results</span>
            </th>
        </tr>
    </table>
    <br class="pixbr" />
</div>
<?php use_helper('Pagination'); ?>
<div class="wrapTable">  
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <thead>
                <tr class="horizontal">
                    <th width = "50px">S.No.</th>
                    <th width = "300px">Username</th>
                    <th width = "300px">User Role</th>
                    <th width = "400px">Bank</th>
                    <?php if('portal_admin'==sfcontext::getInstance()->getUser()->getGroupName()) { ?>
                    <th width = "400px">Action(s)</th>
                    <? } ?>
                </tr>
            </thead>
           <?php
           if (($pager->getNbResults()) > 0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page', 0);
            $i = max(($page - 1), 0) * $limit;
            foreach ($pager->getResults() as $result):
                $i++;
                ?>
                <tr>
                    <td align="left"><?php echo $i ?></td>
                    <td align="left"><?php echo $result->getUsername(); ?></td>
                    <td align="left"><?php echo $result->getDescription(); ?></td>
                    <td align="left"><?php echo $result->getBankname(); ?></td>
                    <?php if('portal_admin'==sfcontext::getInstance()->getUser()->getGroupName()) { ?>
                    <td align="left">
                    <?php
                        if ($result->getUserStatus() == 3) {
                            echo "<font class=error>De-Activated</font>";
                        } else {
                            if ($result->getFailedAttempt() == 5) {
                                echo link_to(' ', 'create_bank_admin/unblock?id='
                               . $result->getUserid().'&redirect_to='.$module
                               .'&backurl='.$action, array('method' => 'get', 'class' => 'unblockInfo',
                               'title' => 'Unblock User'));
                            }
                            echo link_to(' ', 'create_bank_admin/resetPassword?userId='
                                 . $result->getUserid().'&redirect_to='.$module
                                 .'&backurl='.$action, array('method' => 'head', 
                                 'confirm' =>'Are you sure, you want to reset the password?', 
                                  'class' => 'userEditInfo', 'title' => 'Reset Password'));
                            if ($result->getUserStatus() == 1) {
                                echo link_to(' ', 'create_bank_branch_user/setUserStatus?&status=2&user_id='.$result->getUserid()
                                .'&redirect_to='.$module.'&backurl='.$action, array('method' => 'head', 
                                'confirm' => 'Are you sure, you want to suspend the user?', 
                                'class' => 'deactiveInfo', 'title' => 'Suspend'));
                            }
                            if ($result->getUserStatus() == 2) {
                                echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=1&user_id='
                                .$result->getUserid().'&redirect_to='.$module
                                .'&backurl='.$action, array('method' => 'head', 
                                'confirm' => 'Are you sure, you want to resume the user?',
                                'class' => 'activeInfo', 'title' => 'Resume'));
                            }
                            echo "&nbsp;";
                            echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=3&user_id='.$result->getUserid()
                                .'&redirect_to='.$module.'&backurl='.$action, 
                                array('method' => 'head', 'confirm' =>'Are you sure, you want to De-Activate the user?',
                                'class' => 'deactivatedInfo', 'title' => 'De-Activate'));
                        }
                      ?>
                     </td>
                     <? } ?>
                </tr>
    <?php endforeach; ?>
            <tfoot><tr><td colspan= "5">
                        <div class="paging pagingFoot">
            <?php echo pager_navigation($pager, url_for($sf_context->getModuleName() 
                  . '/' . $sf_context->getActionName() . '?user_name=' . $user_name 
                  . '&user_role=' . $user_role . '&bank=' . $bank . '&country=' . $country
                  . '&branch=' .$branch), ''); ?>
                        </div>
                    </td></tr></tfoot>
        <?php }else { ?>

            <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
       <tr><td  align='center' class='error' colspan="5" >No Record found</td></tr>
     </table>
<?php } ?>
    </table>
</div>  
<?php 
if('portal_admin'==sfcontext::getInstance()->getUser()->getGroupName()) { 
    include_partial('create_bank_admin/message');
}
}
?>
</div>
</div>


<script>
$('#bank_user_bank').change(function(){
 var bank = $("#bank_user_bank").val();
 var bank_user_role = $("#bank_user_user_role").val();
 if(bank!='' && bank_user_role!=''){
 var url = '<?php echo url_for('ajax/GetBankCountry');?>';
  $.post(url, {bank:bank,bank_user_role:bank_user_role}, function(data){
     if(data == 'logout'){
          location.reload();
      }else{
        $('#bank_user_country').html(data);
        $('#bank_user_branch').html('<option value="">-- All Bank Branches --</option>');
      }
    });
    }else{
        $('#bank_user_country').html('<option value="">-- All Countries --</option>');
        $('#bank_user_branch').html('<option value="">-- All Bank Branches --</option>');
    }
});
$('#bank_user_country').change(function(){
 var bank = $("#bank_user_bank").val();
 var country = $("#bank_user_country").val();
 var bank_user_role = $("#bank_user_user_role").val();
 if(country!=''){
 var url = '<?php echo url_for('ajax/GetBankBranches');?>';
  $.post(url, {bank:bank,country:country,bank_user_role:bank_user_role}, function(data){
     if(data == 'logout'){
          location.reload();
      }else{
        $('#bank_user_branch').html(data);
      }
    });
    }else{
        $('#bank_user_branch').html('<option value="">-- All Bank Branches --</option>');
    }
});


$('#bank_user_user_role').change(function(){
 var bank = $("#bank_user_bank").val();
 var bank_user_role = $("#bank_user_user_role").val();
 if(bank!='' && bank_user_role!=''){
 var url = '<?php echo url_for('ajax/GetBankCountry');?>';
  $.post(url, {bank:bank,bank_user_role:bank_user_role}, function(data){
     if(data == 'logout'){
          location.reload();
      }else{
        $('#bank_user_country').html(data);
        $('#bank_user_branch').html('<option value="">-- All Bank Branches --</option>');
      }
    });
    }else{
        $('#bank_user_country').html('<option value="">-- All Countries --</option>');
        $('#bank_user_branch').html('<option value="">-- All Bank Branches --</option>');
    }
});
</script>
