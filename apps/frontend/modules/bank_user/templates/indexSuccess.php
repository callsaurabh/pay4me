<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<div class='dlForm'>
    <fieldset>
        <?php echo ePortal_legend('Bank User List'); ?>
    </fieldset>
</div>

<div class="paging pagingHead">
    <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
    <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
    <br class="pixbr" />
</div>

<table class="tGrid">
    <thead>
        <tr>
            <th width = "2%">S.No.</th>
            <th>User</th>
            <th>Bank</th>
            <th>Bank branch</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_per_page_records');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
            foreach ($pager->getResults() as $result):
                $i++;
                ?>
        <tr>
            <td align="center"><?php echo $i ?></td>
            <td align="center"><?php echo $result->getUserName() ?></td>
            <td align="center"><?php echo $result->getBankName() ?></td>
            <td align="center"><?php echo $result->getBranchName() ?></td>
            <td align="center"><?php echo link_to(' ', 'bank_user/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
                    <?php echo link_to(' ', 'bank_user/delete?id='.$result->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?', 'class' => 'delInfo', 'title' => 'Delete')) ?></td>
        </tr>
            <?php endforeach; }else { ?>
        <tr><td  align='center' class='error' colspan="5">No Bank Users found</td></tr>
        <?php } ?>
    </tbody>
    <tfoot><tr><td colspan="5"></td></tr></tfoot>
</table>

<a href="<?php echo url_for('bank_user/new') ?>">Add New</a>
