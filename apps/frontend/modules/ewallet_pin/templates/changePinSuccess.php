<?php //use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_stylesheet('/css/sfPasswordStrengthStyle.css'); ?>
<div class="wrapForm2">
    <form action="<?php echo url_for('ewallet_pin/changePin') ?>" method="post"  id="frm_change_pin" name="frm_change_pin"  enctype="multipart/form-data">
        <?php echo ePortal_legend(__('Generate New Pin'));?>
        <?php echo $form ?>

        <div class="divBlock">
            <center>
                <input type="submit" value="<?php echo __('Save')?>" class="formSubmit" />
        </center></div>
    </form>
    <div class="clear"></div>
</div>

<script type="text/javascript">
$(function () {
	$('#ewallet_pin_old_pin').keypad({duration:'fast',randomiseNumeric: true});
	$('#ewallet_pin_new_pin').keypad({duration:'fast',randomiseNumeric: true});
	$('#ewallet_pin_confirm_pin').keypad({duration:'fast',randomiseNumeric: true});
});
</script>
