<?php

/**
 * ewallet_pin actions.
 *
 * @package    mysfp
 * @subpackage ewallet_pin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ewallet_pinActions extends sfActions {


  protected function processForm(sfWebRequest $request, sfForm $form) {

    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid()) {
      $pinObj=new pin();
      $postParameters=$request->getParameter('ewallet_pin');
      $newPin=$postParameters['new_pin'];
      $ewalletDetail= $pinObj->getEwalletDetail();
      $ewalletDetail->set('pin_number',$pinObj->getEncryptedPin($newPin));
      if($ewalletDetail->getStatus()=='inactive') {
        $ewalletDetail->set('status','active');
        $ewalletDetail->save();
      }else if($ewalletDetail->getStatus()=='active')
      {
        $ewalletDetail->save();
      }
      if($ewalletDetail->getStatus()=='blocked') {
        $this->getUser()->setFlash('error',"Your Pin has been blocked by the admin",true);
      }else {
        $this->getUser()->setFlash('notice',"Pin Changed Successfully",true);
      }


      $this->redirect('ewallet_pin/changePin');
    }
  }

  public function executeSendPinMail(sfWebRequest $request) {
    $subject='Ewallet pin No.';
    $pin=$request->getParameter('pin');
    $partialName=$request->getParameter('partialName');
    $email=$request->getParameter('email');
    $name=$request->getParameter('name');
    $sendMailObj = new Mailing() ;
    $mailInfo = $sendMailObj->sendPinEmail($pin, $subject,$partialName,$email,$name);
    return $this->renderText($mailInfo);
  }
  public function executeChangePin(sfWebRequest $request) {
    $pinObj=new pin();
    $ewalletDetail=$pinObj->getEwalletDetail();
    if($ewalletDetail->getStatus()=='inactive') {
      $value='0000';
    }else {
      $value='';
    }

    $this->form = new changePinForm(null,array('value'=>$value));
    if($request->isMethod(sfRequest::POST)) {
      $this->processForm($request, $this->form);
    }


  }
  public function executeSendPinBlockedMail(sfWebRequest $request) {
    $subject='Ewallet pin Blocked';
    $partialName=$request->getParameter('partialName');
    $userId=$request->getParameter('userId');
    $userDetail=Doctrine::getTable('UserDetail')->findByUserId($userId)->getFirst();
    $email=$userDetail->getEmail();
    $name=$userDetail->getName();
    $sendMailObj = new Mailing() ;
    $mailInfo = $sendMailObj->sendPinBlockedEmail($subject,$partialName,$email,$name);
    return $this->renderText($mailInfo);
  }
  public function executeSendPinResetMail(sfWebRequest $request) {
    $subject='Ewallet pin Reset';
    $partialName=$request->getParameter('partialName');
    $userId=$request->getParameter('userId');
    $pin=$request->getParameter('pin');
    $userDetail=Doctrine::getTable('UserDetail')->find($userId);
    $email=$userDetail->getEmail();
    $name=$userDetail->getName();
    $sendMailObj = new Mailing() ;
    $mailInfo = $sendMailObj->sendPinResetEmail($subject,$partialName,$email,$name,$pin);
    return $this->renderText($mailInfo);
  }
  public function executeSendGracePeriodMail(sfWebRequest $request) {
    $subject='Generate your ewallet pin';
    $partialName=$request->getParameter('partialName');
    $emailListing=Doctrine::getTable('EwalletPin')->getInActiveEmailList();
    $sendMailObj = new Mailing() ;
    $mailInfo = $sendMailObj->sendGracePeriodMail($subject,$partialName,$emailListing);
    return $this->renderText($mailInfo);
  }

}
