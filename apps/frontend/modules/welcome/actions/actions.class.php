<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class welcomeActions extends sfActions {

  public function executeIndex(sfWebRequest $request){
    $this->LegalChecked_welcome = $this->getUser()->getAttribute('LegalChecked', '0'); //legal message session variable
    $this->BroadcastChecked_welcome = $this->getUser()->getAttribute('BroadcastChecked', '0'); //Broadcast message session variable
    //prompt message when question updated succesfuuly 10 to 3
    $pfmHelperObj = new pfmHelper();
    $userGroup = $pfmHelperObj->getUserGroup();

    if($this->isValidUserToWelconeQuestion($userGroup)){

        if($request->getParameter('questionflag')){ //
             $this->getUser()->setFlash('notice', sprintf('Questions Updated Successfully'),false);
        }
    }
     APIHelper::setFlashLogin();
     $user_id = $this->getUser()->getGuardUser()->getId();
     $user_group_obj = Doctrine::getTable('sfGuardUserGroup')->findByUserId($user_id);
     $group_name = $user_group_obj->getFirst()->getsfGuardGroup()->getName();
     // if guest user came to home page redirect him creatnewaccount page with required details.
     if ($group_name == 'guest_user') {
     	$user = Doctrine::getTable('UserDetail')->findByUserId($user_id);
     	$userDetails = array(
     		'fname' => $user->getFirst()->getname(),
     		'lname' => $user->getFirst()->getlastName(),
     		'email' => $user->getFirst()->getemail(),
     		'openIdAuth'=>$user->getFirst()->getopenidAuth(),
     		'openIdType' => '',//$user->getFirst()->getopenidType()
     	);
     	$this->getUser()->setAttribute('openId', $userDetails);
     	$this->redirect('openId/createNewAccount');
     }
     $LegalMsgNotToShowGroup = array(sfConfig::get('app_pfm_role_admin'),
                                     sfConfig::get('app_pfm_role_support'),
                                     sfConfig::get('app_pfm_role_report_admin'),
                                     sfConfig::get('app_pfm_role_merchant'),
                                     sfConfig::get('app_pfm_role_report_portal_admin'));
     $userRole = new UserRoleHelper();
     $message = $userRole->fetchBroadCastMessage($user_id);
     $this->broadcast_message = false;
     if($message){
        $this->broadcast_message = $message['0']['message'];
     }
     $this->legal_message = false;
     if(sfConfig::get('app_legal_notice_show') && (!in_array($group_name, $LegalMsgNotToShowGroup))){
         $message = $userRole->fetchLegalMessage($user_id);
         if ($message) {
            $this->legal_message = $message['0']['message'];
         }
    }
  }

  public function executeOpenInfoBox()
  {
    //$this->broadcast_message = $this->getBroadcastMessage();

    $user_id = $this->getUser()->getGuardUser()->getId();
    $userRole = new UserRoleHelper();
    $message = $userRole->fetchBroadCastMessage($user_id);
    $this->broadcast_message = '';
     if($message){
        $this->broadcast_message = $message['0']['message'];
     }

    $this->setTemplate('openInfoBox');
    $this->setLayout(false);
  }

  public function executeOpenLegalBox() {
        $this->legal_message = false;
        $user_id = $this->getUser()->getGuardUser()->getId();
        $userRole = new UserRoleHelper();
        $message = $userRole->fetchLegalMessage($user_id);
        if ($message) {
            $this->legal_message = $message['0']['message'];
        }
        $this->setTemplate('openLegalBox');
        $this->setLayout(false);
    }

  public function executeOpenInfoBoxReportAbuse()
  {
    $this->setTemplate('openInfoBoxReportAbuse');
    $this->setLayout(false);
  }

  function getBroadcastMessage()
  {
    $branchId='';
    $bankId='';
    $userGroup = "";
    $pfmHelperObj = new pfmHelper();
    $userGroup = $pfmHelperObj->getUserGroup();

    //if user is admin
    if($this->isValidUserToGetMessage($userGroup)==false)
    return false;

    //if user is bank admin
    if($userGroup==sfConfig::get('app_pfm_role_bank_admin'))
    {
      $bankDetails  = $pfmHelperObj->getUserAssociatedBankDetails();
      $bankId = $bankDetails['bank_id'];

      //null in case of bank admin
      $branchId = 'NULL';
      $activeMessages = Doctrine::getTable('BroadcastMessage')->getActiveMessageForUser($bankId,$branchId,'admin');
    }else if($userGroup == sfConfig::get('app_pfm_role_report_bank_admin'))
    {
      $bankDetails  = $pfmHelperObj->getUserAssociatedBankDetails();
      $bankId = $bankDetails['bank_id'];

      //null in case of bank admin
      $branchId = 'NULL';
      $activeMessages = Doctrine::getTable('BroadcastMessage')->getActiveMessageForUser($bankId,$branchId,'report_bank_admin');
    }
    else if($userGroup == sfConfig::get('app_pfm_role_bank_branch_user'))
    {
      $bankDetails  = $pfmHelperObj->getUserAssociatedBankDetails();
      $bankId = $bankDetails['bank_id'];

      //null in case of bank admin
      $branchId = $bankDetails['bank_branch_id'];;
      $activeMessages = Doctrine::getTable('BroadcastMessage')->getActiveMessageForUser($bankId,$branchId);
    }
    else if($userGroup == sfConfig::get('app_pfm_role_bank_branch_report_user'))
    {
      $bankDetails  = $pfmHelperObj->getUserAssociatedBankDetails();
      $bankId = $bankDetails['bank_id'];

      //null in case of bank admin
      $branchId = $bankDetails['bank_branch_id'];;
      $activeMessages = Doctrine::getTable('BroadcastMessage')->getActiveMessageForUser($bankId,$branchId,'bank_branch_report_user');
    }
    if($activeMessages==false)
    return false;
    else
    return $activeMessages;
  }

  function isValidUserToGetMessage($userGroup)
  {
    if($userGroup == sfConfig::get('app_pfm_role_bank_admin') || ($userGroup == sfConfig::get('app_pfm_role_bank_branch_user') || ($userGroup == sfConfig::get('app_pfm_role_report_bank_admin')) || ($userGroup == sfConfig::get('app_pfm_role_report_bank_admin')) ||($userGroup == sfConfig::get('app_pfm_role_bank_branch_report_user'))))
    return true;
    else
    return false;
  }


  function isValidUserToWelconeQuestion($userGroup)
  {
   $arrayQuestionUser = array(sfConfig::get('app_pfm_role_bank_admin'),
       sfConfig::get('app_pfm_role_bank_branch_user'),
       sfConfig::get('app_pfm_role_report_bank_admin'),
       sfConfig::get('app_pfm_role_bank_country_head'),
       sfConfig::get('app_pfm_role_bank_e_auditor'),
       sfConfig::get('app_pfm_role_bank_branch_report_user'),
       sfConfig::get('app_pfm_role_report_admin'),
       sfConfig::get('app_pfm_role_country_report_user'))  ;

       if(in_array($userGroup, $arrayQuestionUser)){
        return true;
       }else{
        return false;
      }
    }
}

?>
