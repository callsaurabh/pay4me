<?php echo ePortal_pagehead(" ",array('class'=>'_form')); 
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
if ($sf_user->hasFlash('lastlogin')): ?>
     <div class="lastlogin">
        <?php //echo $sf_user->getFlash('lastlogin') ?>
        <?php echo $sf_user->getFlash('lastlogin'); echo "&nbsp;".link_to('Report Abuse','welcome/OpenInfoBoxReportAbuse', array('id'=>'reportabuse'));?>
     </div>
<?php endif; ?>


<div class='wrapForm2'>
        <?php echo ePortal_listinghead('Welcome'); ?>
</div>
<div class="Y50">&nbsp;<div class="Y50">&nbsp;</div></div></div>
<script type="text/javascript">
    var broad_cast = false;
  function openReportAbuse(event) {   
    emailwindowReportabuse=dhtmlmodal.open('EmailBox', 'iframe', 'openInfoBoxReportAbuse', 'Report Abuse', 'width=700px,height=300px,center=1,border=0, scrolling=no');
         emailwindowReportabuse.onclose=function(){
            return true;
           }
      return false;     
  }
  $('#reportabuse').click(openReportAbuse);
  
     
  function openInformationWindow(){
    var appname = window.navigator.appName;
    var height = '163';
    if(appname == 'Microsoft Internet Explorer'){
        var height = '213';
    }
    emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'openInfoBox', 'Broadcast Message', 'width=480px,height='+height+',center=1,border=0, scrolling=yes')
    emailwindow.onclose=function(){
      var theform=this.contentDoc.forms[0]
      var theemail=this.contentDoc.getElementById("info_check")
      if (!theemail.checked){
        alert("Please confirm that you have read the information by selecting the checkbox");
        return false
      }
      else{
        $.post('<?php echo url_for('broadcast/setSessionVariableBroadcast') ?>', {'label1':true}, function (data) {
                 }
            );
        return true;
      }
    }
  }

  function openLegalWindow(){
  var appname = window.navigator.appName;
            var height = '120';
            var width = '490';
            if(appname == 'Microsoft Internet Explorer'){
                var height = '141';
                var width = '473';
            }
        emailwindow=dhtmlmodal.open('EmailBox1', 'iframe', 'openLegalBox', 'Legal Notice', 'width='+width+',height='+height+',center=1,border=0, scrolling=no')
        emailwindow.onclose=function(){
            var theform=this.contentDoc.forms[0]
            var theemail=this.contentDoc.getElementById("info_check")
            if (!theemail.checked){
                alert("Please confirm by selecting the checkbox that this profile is yours and you are accepting the Pay4Me terms of use");
                return false
            }
            else{
                $.post('<?php echo url_for('broadcast/setSessionVariableLegal') ?>', {'label':true}, function (data) {
                    if(broad_cast == true)
                    openInformationWindow();
                    }
                );

                return true;
            }
        }
    }
    
</script>
<?php
if(($broadcast_message!=array() && $broadcast_message!="" && ((!$BroadcastChecked_welcome))) && (($legal_message=="") || ($LegalChecked_welcome) )){
  echo "<script>$(document).ready(function(){openInformationWindow();});</script>";
}
else if(isset($legal_message) && (!($LegalChecked_welcome))){
      if($legal_message!=array() && $legal_message!=""){
          if(($broadcast_message!="") && (!$BroadcastChecked_welcome)){
              echo "<script> broad_cast = true;</script>";
          }
      echo "<script>$(document).ready(function(){openLegalWindow();});</script>";
    }
}

?>