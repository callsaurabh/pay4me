<?php use_stylesheet('jquery.alerts.css');
echo include_stylesheets(); ?>
<?php use_stylesheet('dhtmlwindow.css');
echo include_stylesheets(); ?>
<body id='popupContent'>
<div class="popupBody">
  <div  class="popupContent" style="height:250px;*height:266px;">
    If you are concerned about someone using your account without your permission. 
    Please report us at <?php echo mail_to('support@pay4me.com','support@pay4me.com'); ?> 
    and <strong>change your password</strong> immediately.
    <h4>&nbsp;</h4>
    <span class ="headings">Last Login information is updated in following condition:</span>
    <ol>
        <li>Once user login at Pay4Me Portal.</li>
        <li>When user activate his/her account.</li>
    </ol>
    <span class ="headings">Pay4me Guidelines  for User Security:</span>
    <ol>
    <li>Do cross check your last login information available in Pay4me upon every login to ascertain your last login and monitor any unauthorized logins. </li>
    <li>If you identify any unauthorized logins  <strong> change your password </strong> immediately.</li>
    <li>Monitor your transactions regularly.</li>
    <li>Always logout when you exit Pay4me. Do not directly close the browser.</li>
    </ol>
    <div class="flRt">
        <input name="" type="button" value="Close" class="greenButton_new"
        onClick="parent.emailwindowReportabuse.hide()" />
     </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>
<script type="text/javascript">
nW = document.getElementById('popupContent').offsetWidth;
nH = document.getElementById('popupContent').offsetHeight;
//window.parent.emailwindowReportabuse.setSize(nW+20,nH+20);
</script>
