<?php

/**
 * create_bank_branch_user actions.
 *
 * @package    mysfp
 * @subpackage create_bank_branch_user
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class bankBranchReportUserActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        //    $user_obj = sfGuardAuthServiceFactory::getService(sfGuardAuthServiceFactory::$TYPE_USER);
        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_bank_branch_report_user')); //get Group Id; of bank admin
        $groupid = $sfGroupDetails->getFirst()->getId();

        $user = $this->getUser();
        $bank_id = $user->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();

        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id, $country_id);
        $this->bank_branch_id = $request->getParameter('bank_branch_id');
        $this->userName = $request->getParameter('user_search');
        $this->form = new searchUserForm($request->getPostParameters(), array('bank_id' => $bank_id, 'country_id' => $country_id, 'value' => $this->userName));
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllUsers($groupid, $bank_id, $this->userName, $this->bank_branch_id, $country_id);
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeNew(sfWebRequest $request) {
        $this->err = 0;
        $user = $this->getUser();
        $sfGuardUserId = $user->getGuardUser()->getId();


        $bankobj = $user->getGuardUser()->getBankUser()->getFirst()->getBank();
        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $bank_id = $bankobj->getId();
        $this->bankname = $bankobj->getAcronym();
        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);
        if (!$bankConfigObj) {
            $this->err = "Bank domain is not setup, therefore you cannot create any users";
        } else {

            $this->bankdomain = $bankConfigObj->getfirst()->getDomain();
            $this->method = "new";
            $this->bank_branch_id = "";
            $this->username = "";
            //   $this->b_place="";
            $this->address = "";
            $this->email = "";
            $this->mobile_no = "";
            $this->dob = "";
            $this->name = "";

            $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
            $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id, $country_id);
            $this->form = new CustomBankUserForm(null,
                            array('country_id' => $country_id, 'bank_id' => $bank_id,
                                'bank_name' => $this->bankname,
                                'edit' => false, 'display' => false, 'grp' => 'app_pfm_role_bank_branch_report_user'), null);

//            if ($request->isMethod('post')) {
//
//                $sfUser = new sfGuardUser();
//                $this->bank_branch_id = $bankbranchid = $request->getPostParameter('bank_branch_id');
//                $bank_name = $request->getPostParameter('bankname');
//                $this->username = $request->getPostParameter('username');
//
//                if (trim($request->getPostParameter('name')) != "")
//                    $this->name = $request->getPostParameter('name');
//                if (trim($request->getPostParameter('dob')) != "")
//                    $this->dob = date('Y-m-d', strtotime($request->getPostParameter('dob')));
//                if (trim($request->getPostParameter('b_place')) != "")
//                    $this->b_place = $request->getPostParameter('b_place');
//                if (trim($request->getPostParameter('address')) != "")
//                    $this->address = $request->getPostParameter('address');
//                if (trim($request->getPostParameter('email')) != "")
//                    $this->email = $request->getPostParameter('email');
//                if (trim($request->getPostParameter('mobile_no')) != "")
//                    $this->mobile_no = $request->getPostParameter('mobile_no');
//
//
//                $username = $request->getPostParameter('username') . "." . $request->getPostParameter('bankname');
//                $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
//                if ($username_count == 0) {
//                    if (!$this->userDetailValid($request)) {
//                        $this->redirect('bankBranchReportUser/new');
//                    }
//
//                    $sfUser->setUsername($username);
//                    $pass_word = PasswordHelper::generatePassword();
//                    $sfUser->setPassword($pass_word);
//                    $sfUser->setIsActive(true);
//                    $sfUser->save();
//                    //insert userid and password in EpPasswordPolicy table
//                    EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(), $sfUser->getPassword());
//                    $sfUserId = $sfUser->getId();
//
//                    $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_bank_branch_report_user')); //hard coded as of now; is the bank admin
//                    $sfGroupId = $sfGroupDetails->getFirst()->getId();
//
//
//                    $sfGuardUsrGrp = new sfGuardUserGroup();
//                    $sfGuardUsrGrp->setGroupId($sfGroupId);
//                    $sfGuardUsrGrp->setUserId($sfUserId);
//                    $sfGuardUsrGrp->save();
//
//                    //Insert into bank user
//                    $bank_user = new BankUser();
//                    $bank_user->setUserId($sfUserId);
//                    $bank_user->setBankId($bank_id);
//                    $bank_user->setCountryId($country_id);
//                    $bank_user->setBankBranchId($bankbranchid);
//                    $bank_user->save();
//
//                    //Save User Detail
//                    $this->saveUserDetail($request, $sfUserId);
//
//                    /* Audit Trail code starts */
//                    $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $username, $sfUserId));
//                    $eventHolder = new pay4meAuditEventHolder(
//                                    EpAuditEvent::$CATEGORY_TRANSACTION,
//                                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_CREATE,
//                                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_CREATE, array('username' => $username)),
//                                    $applicationArr);
//
//                    $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
//                    /* Audit Trail code ends */
//
//
//                    /* Mail firing code starts */
//                    $subject = "Welcome to Pay4Me";
//                    $partialName = 'create_bank_admin/welcome_email';
//                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
//                    $login_url = url_for('@bank_login?bankName=' . $this->bankname, true);
//                    $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', "create_bank_admin/sendEmail", array('userid' => $sfUserId, 'password' => $pass_word, 'subject' => $subject, 'partialName' => $partialName, 'login_url' => $login_url));
//                    $this->logMessage("sceduled mail job with id: $taskId", 'debug');
//
//                    /* Mail firing code ends */
//
//                    $this->getUser()->setFlash('notice', sprintf("Bank Branch Report User -" . $username . " created successfully<br> The password is - " . $pass_word));
//
//                    //  $this->getUser()->setFlash('notice', sprintf("User -".$user_name." created successfully as ".$role.'<br> The password is - '.$pass_word));
//                    $this->redirect('bankBranchReportUser/index');
//                } else {
//
//                    $this->getUser()->setFlash('error', sprintf('Username already exists'));
//                    $this->setTemplate('new');
//                }
//            }
        }
    }

    public function executeChkUserAvailability(sfWebRequest $request) {


        $username = $request->getPostParameter('username') . "." . $request->getPostParameter('bankname');

        //chk if username is available
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
        //  $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
        if ($username_count == 0) {
            $this->result = "<div class='cGreen'>Username Available</div>";
        } else {
            $this->result = "<div class='cRed'>Username not Available</div>";
        }
    }

    protected function saveUserDetail($request, $userId) {
        $bankobj = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank();
        $country_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $bank_id = $bankobj->getId();

        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);
        $bankdomain = $bankConfigObj->getfirst()->getDomain();

        $userDetail = new UserDetail();
        $userDetail->setUserId($userId);
        if (trim($request->getPostParameter('name')) != "")
            $userDetail->setName(trim($request->getPostParameter('name')));
        if ($request->getPostParameter('dob')) {
            $date = date('Y-m-d', strtotime($request->getPostParameter('dob')));
            $userDetail->setDob($date);
        }
        /*
          if(trim($request->getPostParameter('b_place')) != "")
          $userDetail->setBrithPlace(trim($request->getPostParameter('b_place')));
         */
        if (trim($request->getPostParameter('address')) != "")
            $userDetail->setAddress(trim($request->getPostParameter('address')));
        if (trim($request->getPostParameter('email')) != "")
            $userDetail->setEmail(trim($request->getPostParameter('email')) . $bankdomain);
        if (trim($request->getPostParameter('mobile_no')) != "")
            $userDetail->setMobileNo(trim($request->getPostParameter('mobile_no')));
        $userDetail->save();
    }

    protected function userDetailValid($request) {
        $msg = array();
        if (trim($request->getPostParameter('name')) == "") {
            $msg[] = 'Please enter name ';
        }
        if (trim($request->getPostParameter('email')) == "") {
            $msg[] = 'Please enter Email<br />';
        } else if (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*$^", $request->getPostParameter('email'))) {
            $msg[] = 'Please enter Valid Email<br />';
        }

        if (trim($request->getPostParameter('address')) != "") {
            $len = strlen(trim($request->getPostParameter('address')));
            if ($len > 255) {
                $msg[] = 'Please enter valid address<br />';
            }
        }

        if (count($msg) > 0) {
            $msg = implode($msg, ', ');
            $this->getUser()->setFlash('error', sprintf($msg));
            return false;
        }
        return true;
    }

    public function executeSearch() {
        $user = $this->getUser();
        $this->bank_acronym = $user->getGuardUser()->getBankUser()->getFirst()->getBank()->getAcronym();
        $this->name = "";
    }

    public function executeDisplay(sfWebRequest $request) {
        $this->err = 0;
        $this->bank_acronym = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank()->getAcronym();
        $this->name = $request->getParameter('name');
        $username = $this->name . "." . $this->bank_acronym;
        $this->bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();

        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
        if ($username_count == 0) {
            $this->err = "No user found";
            //   $this->redirect('create_bank_branch_user/search');
        } else {
            $this->user_details = Doctrine::getTable('sfGuardUser')->fetchUserDetails($username);

            $this->buser_form = new CustomBankUserForm(array('username' => $username, 'name' => $this->name, 'address' => $this->user_details['0']['UserDetail'][0]['address'], 'mobile_no' => $this->user_details['0']['UserDetail'][0]['mobile_no'], 'email' => $this->user_details['0']['UserDetail'][0]['email'], 'dob' => $this->user_details['0']['UserDetail']['0']['dob']), array('userid' => $this->user_details[0]['id'], 'bank_id' => $this->bank_id, 'display' => true, 'edit' => false, 'branch_id' => $this->user_details['0']['BankUser'][0]['BankBranch']['id']), null);

            $user_id = $this->user_details['0']['UserDetail']['0']['user_id'];
            $user_group_obj = Doctrine::getTable('sfGuardUserGroup')->findByUserId($user_id);
            $group_id = $user_group_obj->getFirst()->getGroupId();
            $group_object = Doctrine::getTable('sfGuardGroup')->findById($group_id);
            $group_name = $group_object->getFirst()->getName();
            if ($group_name == sfConfig::get('app_pfm_role_bank_branch_user')) {
                $bank_id = $this->user_details['0']['bank_id'];
                $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
                $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id);
            } else {
                $this->err = "The Bank Branch of searched user can not be changed";
            }
        }
    }

    public function executeUpdate(sfWebRequest $request) {

        $this->err = "";
        $userid = $request->getPostParameter('userId');
        
        if ($userid != "") {

            $user_count = Doctrine::getTable('sfGuardUser')->createQuery('a')->where('id=?', $userid)->count();
            if ($user_count == 1) {
                $bank_branch_id = $request->getPostParameter('bank_branch_id');
                $this->user_details = Doctrine::getTable('BankUser')->updateUserBankBranch($userid, $bank_branch_id);

                /* Audit Trail code starts */
                $sfGuardUserObj = Doctrine::getTable('sfGuardUser')->find($userid);
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $sfGuardUserObj->getUsername(), $sfGuardUserObj->getId()));
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_BRANCH_CHANGED,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_BRANCH_CHANGED, array('username' => $sfGuardUserObj->getUsername())),
                                $applicationArr);

                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                /* Audit Trail code ends */

                $this->err = 0;
            } else {
                $this->err = 1;
            }
        } else {
            $this->err = 1;
        }

        if ($this->err) {
            $this->msg = "Due to some error could not save the changes.Please try again!!";
        } else {
            $branchName = Doctrine::getTable('BankBranch')->getBankBranchNameBYID($bank_branch_id);
            $this->msg = "Bank User " . $sfGuardUserObj->getUsername() . " has been linked to " . $branchName . " branch.";
        }
    }

    public function executeDelistUsers(sfWebRequest $request) {
        $userId = $request->getParameter('userId');
        $case = $request->getParameter('use');
        $user_list = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        $user_active_update = Doctrine::getTable('sfGuardUser')->updateUserActiveById($userId, $case);
        if ($user_active_update == '1') {
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user_list[0]["username"], $userId));

            if ($case == '0') {


                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_DEACTIVATE,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DEACTIVATE, array('username' => $user_list[0]["username"])),
                                $applicationArr);


                $this->getUser()->setFlash('notice', 'User ' . $user_list[0]["username"] . ' Deactivated successfully', true);
            } else if ($case == '1') {
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_ACTIVATE,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_ACTIVATE, array('username' => $user_list[0]["username"])),
                                $applicationArr);
                $this->getUser()->setFlash('notice', 'User ' . $user_list[0]["username"] . ' Activated successfully', true);
            }
            $this->redirect('bankBranchReportUser/index');
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        } else {
            $this->getUser()->setFlash('error', 'your are not authorized user !', true);
            $this->redirect('bankBranchReportUser/index');
        }
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();
        $this->forward404Unless($user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('userId')), sprintf('Object message does not exist (%s).', $request->getParameter('id')));
        $user->delete();

        /* Audit Trail code starts */
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user->getUsername(), $user->getId()));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_DELETE,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DELETE, array('username' => $user->getUsername())),
                        $applicationArr);

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        /* Audit Trail code ends */
        $this->getUser()->setFlash('notice', 'User deleted successfully', true);
        $this->redirect('bankBranchReportUser/index');
    }

//    public function executeEdit(sfWebRequest $request) {
//        //$this->method="edit";
//        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id')), sprintf('Object bank_admin does not exist (%s).', $request->getParameter('id')));
//        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BANK);
//
//        $user = $this->getUser();
//        $sfGuardUserId = $user->getGuardUser()->getId();
//        $bankobj = $user->getGuardUser()->getBankUser()->getFirst()->getBank();
//        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();
//
//        $bank_id = $bankobj->getId();
//        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);
//
//        $this->bankname = $bankobj->getAcronym();
//        $this->bankdomain = $bankConfigObj->getfirst()->getDomain();
//
//
//        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
//        $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id, $country_id);
//
//        // $this->bank_array = $bank_obj->getBankList($request->getParameter('bank_id'));
//        $this->username = $sf_guard_user->getUsername();
//        $this->bank_branch_id = $sf_guard_user->getBankUser()->getFirst()->getBankBranchId();
//        $this->bank_id = $sf_guard_user->getBankUser()->getFirst()->getBank()->getId();
//        $this->bank_acronym = $sf_guard_user->getBankUser()->getFirst()->getBank()->getAcronym();
//        $this->bank_domain = $this->bankdomain;
//        if ($sf_guard_user->getUserDetail()->getFirst() == '') {
//            $this->name = '';
//            $this->dob = '';
//            $this->b_place = '';
//            $this->address = '';
//            $this->email = '';
//            $this->mobile_no = '';
//        } else {
//            $this->name = $sf_guard_user->getUserDetail()->getFirst()->getName();
//            $this->dob = $sf_guard_user->getUserDetail()->getFirst()->getDob();
//            //  $this->b_place = $sf_guard_user->getUserDetail()->getFirst()->getBrithPlace();
//            $this->address = $sf_guard_user->getUserDetail()->getFirst()->getAddress();
//            $this->mobile_no = $sf_guard_user->getUserDetail()->getFirst()->getMobileNo();
//            $email_address = explode('@', $sf_guard_user->getUserDetail()->getFirst()->getEmail());
//            $this->email = $email_address[0];
//        }
//        $this->userId = $request->getParameter('id');
//
//$this->form = new CustomBankUserForm(array('username' => $sf_guard_user->getUsername(), 'name' => $this->name, 'address' => $this->address, 'mobile_no' => $this->mobile_no, 'email' => $this->email), array('bank_id' => $bank_id, 'bank_name' => $this->bankname, 'edit' => true,'branch_id'=>$this->bank_branch_id), null);
//
//        $this->setTemplate('edit');
//    }

    public function executeModify(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod('post'));
        $userId = $request->getPostParameter('userId');
        $sf_guard_user_obj = Doctrine::getTable('sfGuardUser')->find($request->getParameter('userId'));
        $user = $this->getUser();
        $bankobj = $user->getGuardUser()->getBankUser()->getFirst()->getBank();
        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $bank_id = $bankobj->getId();


        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);


        // $bank_domain = $sf_guard_user_obj->getBankUser()->getFirst()->getBank()->getBankConfiguration()->getfirst()->getDomain();
        $bank_domain = $bankConfigObj->getfirst()->getDomain();
        $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $userDetailId = "";
        if ($userDetailObj->count()) {
            $userDetailId = $userDetailObj->getFirst()->getId();
        }
        if ($userDetailId != "") {
            $user_detail = Doctrine::getTable('UserDetail')->find(array($userDetailId));
        } else {
            $user_detail = new UserDetail();
        }
        $user_detail->setUserId($userId);
        $user_detail->setName(trim($request->getPostParameter('name')));
        if (($request->getPostParameter('dob')) && ($request->getPostParameter('dob') != "")) {
            $user_detail->setDob(date('Y-m-d', strtotime(trim($request->getPostParameter('dob')))));
        }
        //  $user_detail->setBrithPlace($request->getPostParameter('b_place'));
        $user_detail->setAddress(trim($request->getPostParameter('address')));
        $user_detail->setEmail(trim($request->getPostParameter('email')) . $bank_domain);
        $user_detail->setMobileNo(trim($request->getPostParameter('mobile_no')));
        $user_detail->save();

        /* Audit Trail code starts */

        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $sf_guard_user_obj->getUsername(), $sf_guard_user_obj->getId()));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_UPDATE,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_UPDATE, array('username' => $sf_guard_user_obj->getUsername())),
                        $applicationArr);

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        /* Audit Trail code ends */

        $this->getUser()->setFlash('notice', sprintf('User Profile updated successfully'));
        $this->redirect('bankBranchReportUser/index');
    }

}
