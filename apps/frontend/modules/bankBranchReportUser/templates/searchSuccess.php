<?php //use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<?php echo ePortal_legend('Search  User');      ?>

<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName().'/display','name=search id=search');?>

    <dl id='user_search_row'>
        <div class="dsTitle4Fields">Username:<sup class="cRed">*</sup></div>

        <div class="dsInfo4Fields"><input type="text" name="name" class="FieldInput" value="<?php echo $name?>"><?php echo ".".$bank_acronym;?>
            <br/><br/><div id="err_name" class="cRed"></div>
    </div> <div id="err_name" class="cRed"></div></dl>
    <div class="divBlock">
        <center id="multiFormNav">
            <?php echo button_to('Search','',array('class'=>'formSubmit', 'onClick'=>'validateSearchForm()')); ?>
        </center>
    </div>

    <div id="search_results"></div>

</div>
<script>
    //function to validate the search panel in create_voucher, vet_voucher and approve_voucher
    function validateSearchForm()
    {
        $('#search_results').html("");
        var err=0;
        var elem = document.search;
        var name  = elem.name.value;

        if(name == "")
        {
            err = err+1;
            $("#err_name").html("Please enter name");
        }
        else
        {
            $("#err_name").html("");
        }
        if(err == 0)
        {
            $.post('display',$("#search").serialize(), function(data){$("#search_results").html(data);});
        }
    }
</script>
