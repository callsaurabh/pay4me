<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
<?php //use_helper('Form');
use_helper('Pagination'); ?>

<div id="theMid">
    <?php echo ePortal_legend('Search User'); ?>
    <div class="wrapForm2">
        <?php echo form_tag($sf_context->getModuleName() . '/index', 'name=search id=search'); ?>      
     <?php include_partial('searchUser',array('form'=>$form))?>
        <div class="clear"></div></form>
    </div>
    <br/>

    <?php echo ePortal_legend('Bank Branch Report User List'); ?>

        <div class="wrapTable">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                <tr class="alternateBgColour">
                    <th width="100%" >
                        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                    </th>
                </tr>
            </table>
            <br class="pixbr" />
        </div>

        <div class="wrapTable" >
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                <thead>
                    <tr class="horizontal">
                        <th width = "2%">S.No.</th>
                        <th>Username</th>
                        <th>Bank Branch</th>
                        <th>Branch Code</th>
                        <th>Bank</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if (($pager->getNbResults()) > 0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $page = $sf_context->getRequest()->getParameter('page', 0);
                    $i = max(($page - 1), 0) * $limit;
                    foreach ($pager->getResults() as $result):
                        $i++;
                ?>
                        <tr class="alternateBgColour">
                            <td align="center"><?php echo $i ?></td>
                            <td align="center"><?php echo $result->getUserName() ?></td>
                            <td align="center"><?php echo $result->getBankBranchName() ?></td>
                            <td align="center"><?php echo $result->getBranchCode() ?></td>
                            <td align="center"><?php echo $result->getBankName() ?></td>
                            <td align="right">

                        <?php
                        if ($result->getUserStatus() == 3) {
                            echo "<font class=error>De-Activated</font>";
                        } else {
                            if ($result->getFailedAttempt() == 5) {
                                echo link_to(' ', 'create_bank_admin/unblock?id=' . $result->getId() . '&redirect_to=' . $sf_context->getModuleName(), array('method' => 'get', 'class' => 'unblockInfo', 'title' => 'Unblock User'));
                                echo "&nbsp;";
                            }
                            echo "&nbsp;";
                            if ($result['is_active'] == '1')
                                echo link_to(' ', url_for('create_bank_admin/resetPassword') . '?userId=' . $result->getId() . '&redirect_to=' . $sf_context->getModuleName(), array('method' => 'head', 'confirm' => 'Are you sure, you want to reset the password?', 'class' => 'userEditInfo', 'title' => 'Reset Password'));
                            echo "&nbsp;";
                            echo link_to(' ', 'create_bank_branch_user/edit?id=' . $result->getId(), array('confirm' => 'Are you sure, you want to edit the user?', 'class' => 'editInfo', 'title' => 'Edit'));

                            echo "&nbsp;";

                            if ($result->getUserStatus() == 1) {
                                echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=2&user_id=' . $result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to suspend the user?'), 'class' => 'deactiveInfo', 'title' => 'Suspend'));
                            }

                            if ($result->getUserStatus() == 2) {
                                echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=1&user_id=' . $result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to resume the user?'), 'class' => 'activeInfo', 'title' => 'Resume'));
                            }

                            echo "&nbsp;";
                            echo link_to(' ', 'create_bank_branch_user/setUserStatus?status=3&user_id=' . $result->getId(), array('method' => 'head', 'confirm' => __('Are you sure, you want to De-Activate the user?'), 'class' => 'deactivatedInfo', 'title' => 'De-Activate'));
                        }
                        ?>


                </tr>
<?php endforeach; ?>
                <tr><td colspan="6">
                        <div class="paging pagingFoot"><?php echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id')))  ?>

                                </div></td></tr>
<?php } else { ?>

                        <tr><td  align='center' class='error' colspan="6">No Bank Branch Report Users Found</td></tr>
<?php } ?>
                </tbody>
                <tfoot><tr><td colspan="6"></td></tr></tfoot>
            </table>
<?php echo button_to('Add New', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for('bankBranchReportUser/new') . '\'')); ?>

        </div>

<?php include_partial('create_bank_admin/message') ?></div>
