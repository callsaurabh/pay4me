<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<?php
if ($err) {

    print "<div id='flash_error' class='error_list'><span>" . $err . "</span></div>";
} else {
?>
    <div class="wrapForm2">
<?php echo form_tag('create_bank_branch_user/new', 'id="pfm_create_user_form" name="pfm_create_user_form"'); ?>


<?php
    if ($method == "new"):
        echo ePortal_legend('Create New Bank Branch Report User');
    endIf; ?>
    <?php
    if ($method == "edit"):
        echo ePortal_legend('Edit Bank Branch Report User');
    endIf; ?>
<?php include_partial('create_bank_branch_user/bankUsersForm', array('form' => $form, 'bankName' => $bankname, 'bankDomain' => $bankdomain, 'edit' => false, 'display' => false, 'function' => 'render')) ?>

<?php //echo formRowComplete('Select Branch <sup>*</sup>',select_tag('bank_branch_id', options_for_select($bank_branch_array, $bank_branch_id, array('include_custom'=>'Please select bank branch'))),'','bank','err_bank','bank_branch_row');  ?>



    <!--<dl id='username_row'>
      <div class="dsTitle4Fields"><label for="username" >Username <sup>*</sup></label></div>
      <div class="dsInfo4Fields"><?php //echo input_tag('username', $username, 'class=FieldInput maxlength=30');?>
<?php //echo input_hidden_tag('bankname', $bankname); ?>

            <span id="username_suffix"><?php //echo ".".$bankname; ?></span>
            <br /><br><a href="javascript:;" onclick="validateUserName();" >Check Availability</a>
            <div id="err_username" class="cRed"></div>
        </div> </dl>-->

    <?php //echo formRowComplete('Name <sup>*</sup>',input_tag('name', $name, 'class=FieldInput maxlength=30'),'','username','err_name','username_row'); ?>
    <?php //echo formRowComplete('Date of Birth',input_date_tag('dob', $dob, array('style' => 'width:180px', 'rich' => true,'readonly'=>'readonly','format'=>'dd-MM-yyyy')),'','username','err_dob','username_row'); ?>
    <?php //echo formRowComplete('Address',textarea_tag('address', $address, array('class'=>'FieldInput')),'<br /><br><br><br /><br><br>(upto 255 characters only)','username','err_address','username_row'); ?>
<?php //echo input_hidden_tag('grp', 'app_pfm_role_bank_branch_report_user'); ?>
    <!--  <dl id='username_row'>
        <div class="dsTitle4Fields"><label for="username">Email <sup>*</sup></label></div>
        <div class="dsInfo4Fields"><?php //echo input_tag('email', $email, 'class=FieldInput maxlength=50'); ?><span id="dns"><?php echo $bankdomain; ?></span>&nbsp;
          <div id="err_email" class="cRed"></div></div>
      </dl>-->

<?php //echo formRowComplete('Mobile Number',input_tag('mobile_no', $mobile_no, 'class=FieldInput'),'<br>(format: + [10-14] digit no, &nbsp;&nbsp;&nbsp; eg: +1234567891 )','username','err_mobile_no','username_row');  ?>

    <dl>
        <div class="dsTitle4Fields">&nbsp;</div>
        <div class="dsInfo4Fields"><?php echo button_to('Create', '', array('class' => 'formSubmit', 'onClick' => 'validateUserForm("1")')); ?>&nbsp;<?php echo button_to('Cancel', '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('bankBranchReportUser/index') . '\'')); ?></div>
    </dl>


    <div class="clear"></div>

    <div id="search_results"></div>
</form>
</div>

<?php } ?>


<script>
    function validateUserForm(submit_form)
    {

        var err  = 0;
        if($('#bank_branch_id').val() == "")
        {
            $('#err_bank').html("Please select Bank Branch");
            err = err+1;
        }
        else
        {
            $('#err_bank').html("");
        }
        if($('#username').val() == "")
        {
            $('#err_username').html("Please enter Username");
            err = err+1;
        }
        else
        {
            $('#err_username').html("");
        }
        if($('#username').val() != ""){
            if(validateString($('#username').val()))
            {
                $('#err_username').html("Please enter Valid Username");
                err = err+1;
            }
            else
            {
                $('#err_username').html("");
            }
        }
        if($('#name').val() == "")
        {
            $('#err_name').html("Please enter Name");
            err = err+1;
        }
        else
        {
            $('#err_name').html("");
        }
        if($('#email').val() == "")
        {
            $('#err_email').html("Please enter Email");
            err = err+1;
        }
        else
        {
            $('#err_email').html("");
        }
        if(validateEmail($('#email').val()))
        {
            $('#err_email').html("Please enter Valid Email");
            err = err+1;
        }
        else
        {
            $('#err_email').html("");
        }


        if($('#address').val() != "")
        {
            if(validateAddress($('#address').val()))
            {
                $('#err_address').html(invalid_addr_msg);
                err = err+1;
            }
            else
            {
                $('#err_address').html("");
            }
        }

        if($('#mobile_no').val() != "")
        {
            if(validatePhone($('#mobile_no').val()))
            {
                $('#err_mobile_no').html("Please enter Valid Mobile number");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }

        if($('#dob_date').val() != ""){

            if (compare_date($('#dob_date').val())) {

                $('#err_dob').html("");
            }
            else{
                $('#err_dob').html("Date of Birth cannot be future or today's date");
                err = err+1;
            }

        }

        if(err == 0)
        {
            if(submit_form == 1)
            {
                $('#pfm_create_user_form').submit();
            }
        }



    }
    function validateString(str) {
        var reg = /^([A-Za-z0-9])+$/;

        if(reg.test(str) == false) {
            return true;
        }

        return false;
    }
    function validateEmail(email) {
        //var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var reg = /^([A-Za-z0-9_\-\.])+$/;
        if(reg.test(email) == false) {
            return true;
        }

        return false;
    }
  
    function concat_username()
    {
        if($("#bank_id :selected").val()!="")
        {
            //   var sel_val = $("#bank_id :selected").html().toLowerCase();
            var sel_val = $("#bank_id").val();
            // var suffix_val = "."+str_replace(" ","_",sel_val);
            $('#username_suffix').html("."+sel_val);
        }
        else
        {
            $('#username_suffix').html("");
        }

    }


    function validateUserName(){
        var err = 0
        if($('#bank_branch_id').val() == "")
        {
            $('#err_bank').html("Please select Bank Branch");
            err = err+1;
        }
        else
        {
            $('#err_bank').html("");
        }
        if($('#username').val() == "")
        {
            $('#err_username').html("Please enter Username");
            err = err+1;
        }
        else
        {
            $('#err_username').html("");
        }
        if(err == 0)
        {
            ajax_call('err_username', 'ChkUserAvailability', 'pfm_create_user_form');

        }

    }
</script>