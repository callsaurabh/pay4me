<?php // use_helper('Form');
echo ePortal_pagehead(" ",array('class'=>'_form')); ?>

<div id="editBankBranchUser">  
  <?php echo ePortal_legend('Edit Bank Branch Report User'); ?>



  <!--<div id='flash_error' class='error_list' style="visibility:hidden;height:1px;overflow-y:hidden"></div>-->
  <div class="wrapForm2">
  <?php echo form_tag('create_bank_branch_user/modify','id="pfm_create_user_report_form" name="pfm_create_user_report_form"');?>
<?php include_partial('create_bank_branch_user/bankUsersForm', array('form' => $form,'bankName'=>$bankname,'bankDomain'=>$bankdomain,'edit'=>true)) ?>

  
  



  <div class="divBlock">
    <center id="multiFormNav">
      <?php echo button_to('Update','',array('class'=>'formSubmit', 'onClick'=>'validateUserForm("1")')); ?>&nbsp;<?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('bankBranchReportUser/index').'\''));?>
    </center>
  </div>
  <div id="search_results"></div>
  
  </div>
</div>
<script type="text/javascript">
  function validateUserForm(submit_form)
  {

    var err  = 0;
    if($('#name').val() == "")
    {
      $('#err_name').html("Please enter Name");
      err = err+1;
    }
    else
    {
      $('#err_name').html("");
    }
    if($('#email').val() == "")
    {
      $('#err_email').html("Please enter Email");
      err = err+1;
    }
    else
    {
      $('#err_email').html("");
    }
    if(validateEmail($('#email').val()))
    {
      $('#err_email').html("Please enter Valid Email");
      err = err+1;
    }
    else
    {
      $('#err_email').html("");
    }

        if($('#address').val() != "")
        {
          if(validateAddress($('#address').val()))
          {
            $('#err_address').html(invalid_addr_msg);
            err = err+1;
          }
          else
          {
            $('#err_address').html("");
          }
        }else
        {
            $('#err_address').html('Please enter address');
            err = err+1;
        }

    if($('#mobile_no').val() != "")
    {
      if(validatePhone($('#mobile_no').val()))
      {
        $('#err_mobile_no').html("Please enter dfsdfValid Mobile number");
        err = err+1;
      }
      else
      {
        $('#err_mobile_no').html("");
      }
    }


    if($('#dob').val() != ""){

      if (compare_date($('#dob').val())) {
        $('#err_dob').html("");
      }
      else{
        $('#err_dob').html("Date of Birth cannot be future or today's date");
        err = err+1;
      }

    }

    if(err == 0)
    {
      if(submit_form == 1)
      {
        $('#pfm_create_user_report_form').submit();
      }
    }



  }
  function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+$/;

    if(reg.test(email) == false) {
      return true;
    }

    return false;
  }

  
</script>
