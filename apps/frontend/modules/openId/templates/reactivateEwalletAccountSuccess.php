<script type="text/javascript">

    function validateSecurityCode(){

        if($("#reactivate_ewallet_code").val()==""){
            alert("Please enter the Reactivation Code.");
            return false;
            
        }else{
            return true;
        }
   }


</script>


<?php use_helper('ePortal'); ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form'));?>

<div class="wrapForm2">

    <form action="<?php echo url_for('openId/CheckReactivateSecurityCode');?>" onsubmit="return validateSecurityCode()" name="reactivate_ewallet">
  <?php echo ePortal_legend(__('Reactivate eWallet Account'));?>
  <div id="bank_user">
        <dl id='username_row'>
    <div class="dsTitle4Fields">Enter Reactivation Code</div>
    <div class="dsInfo4Fields"><input type="text" name="reactivate_ewallet_code" id="reactivate_ewallet_code" maxlength="4">
        <br><br><div id="err_name" class="cRed"></div>
    </div>

</dl>
  </div>

    <div class="divBlock">
      <center id="multiFormNav">
    <input type="submit" class="formSubmit" name="submit" class="button" value="Submit" >
      </center>

    </div>
    </form>
 </div>

