<div class="innerWrapper">
<?php
// use_helper('Form');
use_javascript('jquery-ui.min.js');
use_javascript('general.js');
use_javascript('jquery.keypad.password.js');
use_stylesheet('jquery.keypad.css');
use_helper('sfCryptoCaptcha');
?>


<?php
 echo $form->renderGlobalErrors();
include_partial('cms/leftBlock', array('title' => 'Recover eWallet Account'))
?> <?php //  echo  $form; die; ?>
<div id="wrapperInnerRight">
    <div id="innerImg">
        <?php echo image_tag('/img/new/img_about_pay4bill.jpg', array('alt' => 'About Pay4me', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
    </div>
<?php
                if ($sf_user->hasFlash('notice')) {
                    echo "<div id='flash_notice' class='error_list'><span>" . sfContext::getInstance()->getUser()->getFlash('notice') . "</span></div>";
                }
                if ($sf_user->hasFlash('error')) {
                    echo "<div id='flash_error'><span>" . sfContext::getInstance()->getUser()->getFlash('error') . "</span></div>";
                }
                ?>
    <?php // echo include_partial('signuplabel', array('applingCountry' => $applingCountry)); ?>
        <form name="signup" id="signup"  method="post" action="<?php echo url_for('openId/recoverSubmit') ?>" onSubmit="return validate_signup();" >
            <input type="hidden" name="auth_info" id="auth_info" value="<?php echo $auth_info ?>">
            
            <input type="hidden" name="merchantRequestId" id="merchantRequestId" value="<?php echo isset($merchantRequestId)?$merchantRequestId:''; ?>">
            
            <input type="hidden" name="payType" id="payType" value="<?php echo isset($payType)?$payType:''; ?>">
           
            <div class="feedbackForm">
                <div class="header">Recover eWallet Account Form</div>
                <?php $openIdDetails = sfContext::getInstance()->getUser()->getAttribute('openId');
                      if(isset($openIdDetails['attachedOpenId'])){  ?>
                        <div class="header">Your account is already authenticated with <?php echo $openIdDetails['attachedOpenId']; ?> Server. Kindly associate it with <?php echo $openIdDetails['openIdType']; ?> by filling following details.</div>
                <?php } ?>
                 <div class="fieldName"><?php echo $form['email']->renderLabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap"><label><?php echo $form['email']->render(); ?></label>
                    <div class="cRed" id="err_email"><?php echo $form['email']->renderError(); ?></div>
                </div>
                
                <div class="fieldName"><?php echo $form['ewallet']->renderLabel(); ?><span class="required"> *</span></div>
                <div class="fieldWrap">
                    <label><?php echo $form['ewallet']->render(); ?></label>
                    <div class="cRed" id="err_ewallet"><?php echo $form['ewallet']->renderError(); ?></div>
                </div>
   
                <div class="fieldName"><?php echo $form['captcha']->renderlabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap">
                    <label><?php echo $form['captcha']->render(); ?><br/><br/>
                    <?php echo captcha_image();
                    echo captcha_reload_button(); ?>
                </label>
                <div class="cRed" id="err_vocde"><?php echo $form['captcha']->renderError(); ?></div>
            </div>
            <div class="fieldWrap"> <?php echo $form['authorization_consent']->render(); ?><span class="required"></span>
                <?php echo $form['authorization_consent']->renderLabel(); ?> 
              
                <div class="cRed" id="err_authorization_consent"><?php echo $form['authorization_consent']->renderError(); ?></div>
            </div>
            <div class="fieldName">&nbsp;</div>


            <div  class="fieldWrap" id="submitdiv">
                <label>
                    <?php if ($form->isCSRFProtected()) : ?>
                    <?php echo $form['_csrf_token']->render(); ?>
                    <?php endif; ?>
                        <input type="submit" class="formSubmit" name="submit" class="button" value="Continue" >
                    </label>
                </div>
                <div class="fieldWrap" id ="progBarDiv"  style="display:none;"></div>

                <div class="fieldName">&nbsp;</div>
                <div class="fieldWrap"><span  class="message">[*] Fields marked as * are Mandatory.</span></div>
            </div>

        </form>
        <p>&nbsp;</p>
    </div>
</div>
    <script>
        function validate_signup(){
            var err  = 0;    

            if($('#email').val() == "")
            {
                $('#err_email').html("Please enter Email Id");
                err = err+1;
                $('#email').focus();
            }
            else
            {
                $('#err_email').html("");
            }
            if($('#email').val() != ""){
                if(!validateEmail($('#email').val()))
                {
                    $('#err_email').html("Please enter valid Email Id");
                    err = err+1;
                    $('#email').focus();
                }
                else
                {
                    $('#err_email').html("");
                }
            }

            if($('#ewallet').val() == "")
            { 
                $('#err_ewallet').html("Please enter eWallet Account Number");
                err = err+1;
                $('#ewallet').focus();
            }
            else
            {
                $('#err_ewallet').html("");
            }
            if($('#ewallet').val() != ""){
                if(validateNumeric($('#ewallet').val()))
                {
                    $('#err_ewallet').html("Please enter valid eWallet Account Number");
                    err = err+1;
                    $('#ewallet').focus();
                }
                else
                {
                    $('#err_ewallet').html("");
                }
            }
    

        if(jQuery.trim($('#payOptions_captcha').val()) == "")
        {
            $('#err_vocde').html("Please enter Verification Code");
            err = err+1;
            $('#payOptions_captcha').focus();
        }
        else {
            $('#err_vocde').html("");
        }
         if($('#payOptions_authorization_consent').is(':checked')){
             $('#err_authorization_consent').html("");
        }else{
            $('#err_authorization_consent').html("Please accept the authorization");
            err = err+1;
            $('#payOptions_authorization_consent').focus();
        }
       if(err == 0) {
        return true;
    } else {
        $('#payOptions_captcha').val('');
        $('#payOptions_authorization_consent').removeAttr('checked');
        document.getElementById('captcha_img').src = "<?php echo url_for('@sf_captcha') ?>?r=" + Math.random() + "&reload=1";
        return false;
    }
    }
    window.onload = function() {
        $('#payOptions_captcha').val('');
        $('#payOptions_authorization_consent').removeAttr('checked');
        
    }
    function updCaptcha(uri)
    {
        url = uri+"?r=" + Math.random() + "&reload=1";
        $('#captchaImg').attr('src', url);
    }
    function load_captcha(url){

        document.getElementById('captcha_img').src=url;
        $('#cap_ref').hide();
        setTimeout('show_reload_button()', 1000);

    }
    function show_reload_button(){


        $('#cap_ref').show();
    }

</script>