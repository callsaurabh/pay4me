<div class="innerWrapper">
<?php
// use_helper('Form');
use_javascript('jquery-ui.min.js');
use_javascript('general.js');
use_javascript('jquery.keypad.password.js');
use_stylesheet('jquery.keypad.css');
use_helper('sfCryptoCaptcha');
?>


<?php
echo $form->renderGlobalErrors();
include_partial('cms/leftBlock', array('title' => 'e-Wallet Registration'))
?> <?php //  echo  $form; die; ?>

<div id="wrapperInnerRight">
    <div id="innerImg">
        <?php echo image_tag('/img/new/img_about_pay4bill.jpg', array('alt' => 'About Pay4me', 'width' => 600, 'height' => 242, 'border' => 0)); ?>
    </div>

    <?php // echo include_partial('signuplabel', array('applingCountry' => $applingCountry));
     // [WP076] Making new account form configurable
     if(!sfConfig::get('app_disable_ewallet_registration')){
    ?>
         <form name="signup" id="signup"  method="post" action="<?php echo url_for('openId/submit') ?>" onSubmit="return validate_signup();" >
            <input type="hidden" name="auth_info" id="auth_info" value="<?php echo $auth_info ?>">
            
            <input type="hidden" name="merchantRequestId" id="merchantRequestId" value="<?php echo isset($merchantRequestId)?$merchantRequestId:''; ?>">
            
            <input type="hidden" name="payType" id="payType" value="<?php echo isset($payType)?$payType:''; ?>">
            <input type="hidden" name="payIdType" id="payIdType" value="<?php echo isset($payIdType)?$payIdType:''; ?>">
            <div class="feedbackForm">
                <div class="header">Registration Form</div>
                <div class="fieldName"><?php echo $form['name']->renderLabel(); ?><span class="required"> *</span></div>
                <div class="fieldWrap">
                    <label><?php echo $form['name']->render(); ?></label>
                    <div class="cRed" id="err_name"><?php echo $form['name']->renderError(); ?></div>
                </div>
                <div class="fieldName"><?php echo $form['lname']->renderLabel(); ?> <span class="required">*</span></div>

                <div class="fieldWrap">
                    <label><?php echo $form['lname']->render(); ?></label>
                    <div class="cRed" id="err_lastname"><?php echo $form['lname']->renderError(); ?></div>
                </div>

                <div class="fieldName"><?php echo $form['dob']->renderlabel(); ?></div>
                <div class="fieldWrap">
                    <label><?php echo $form['dob']->render(); ?>
                        <span class="desc-optional"><br/>Optional&nbsp;&nbsp;</span>
                    <div class="cRed" id="err_dob"><?php echo $form['dob']->renderError(); ?></div>
                </div>

                <div class="fieldName"><?php echo $form['email']->renderLabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap"><label><?php echo $form['email']->render(); ?></label>
                    <div class="cRed" id="err_email"><?php echo $form['email']->renderError(); ?></div>
                </div>

                <div class="clear"></div>

                <div class="fieldName"><?php echo $form['address']->renderLabel(); ?></div>
                <div class="fieldWrap"><label><?php echo $form['address']->render(); ?><span class="desc-optional"><br/>Optional&nbsp;&nbsp;</span><span class="desc">(maximum of 255 characters)</span></label>
                    <div class="cRed" id="err_address"><?php echo $form['address']->renderError(); ?></div>
                </div>

                <div class="fieldName"><?php echo $form['mobileno']->renderLabel(); ?> </div>
                <div class="fieldWrap">
                    <label><?php echo $form['mobileno']->render(); ?>
                        <span class="desc-optional"><br/>Optional&nbsp;&nbsp;</span>
                        <span class="desc">(format: + [10-14] digit no,eg: +1234567891 )</span></label>
                    <div class="cRed" id="err_mobile_no"><?php echo $form['mobileno']->renderError(); ?></div>
                </div>
                <div class="fieldName"><?php echo $form['workphone']->renderLabel(); ?></div>
                <div class="fieldWrap">
                    <label><?php echo $form['workphone']->render(); ?>
                        <span class="desc-optional"><br/>Optional&nbsp;&nbsp;</span>
                        <span class="desc">(format: + [10-14] digit no,eg: +1234567891 )</span>
                    </label>
                    <div class="cRed" id="err_work_phone"><?php echo $form['workphone']->renderError(); ?></div>
                </div>
                <div class="fieldName"><?php echo $form['captcha']->renderlabel(); ?> <span class="required">*</span></div>
                <div class="fieldWrap">
                    <label><?php echo $form['captcha']->render(); ?><br/><br/>
                    <?php echo captcha_image();
                    echo captcha_reload_button(); ?>
                </label>
                <div class="cRed" id="err_vocde"><?php echo $form['captcha']->renderError(); ?></div>
            </div>
                
            <div  class="fieldWrap"> <?php echo $form['authorization_consent']->render(); ?><span class="required"></span>
                <?php echo $form['authorization_consent']->renderLabel(); ?> 
              
                <div class="cRed" id="err_authorization_consent"><?php echo $form['authorization_consent']->renderError(); ?></div>
            </div>
            <div class="fieldName">&nbsp;</div>


            <div   class="fieldWrap" id="submitdiv">
                <label>
                    <?php if ($form->isCSRFProtected()) : ?>
                    <?php echo $form['_csrf_token']->render(); ?>
                    <?php endif; ?>
                        <input type="submit" class="formSubmit" name="submit" class="button" value="Submit" >
                    </label>
                </div>
                <div class="fieldWrap" id ="progBarDiv"  style="display:none;"></div>

                <div class="fieldName">&nbsp;</div>
                <div class="fieldWrap"><span  class="message">[*] Fields marked as * are Mandatory.</span></div>
            </div>

        </form>
        <?php }else{ ?>
        <div id='flash_error'  class='error_list_user'><span>New ewallet signup has been temporarily suspended and will be resumed shortly. We regret the inconvenience caused and expect your cooperation.</span></div>
        <?php } ?>
        <p>&nbsp;</p>
    </div>
</div>
    <script>
        function validate_signup(){
            var err  = 0;    
            if($('#name').val() == "")    
            { 
                $('#err_name').html("Please enter First Name");    
                err = err+1;    
                $('#name').focus();    
            }    
            else    
            {   
                $('#err_name').html("");    
            }    
            if($('#name').val() != ""){   
                if(validateString($('#name').val()))    
                {    
                    $('#err_name').html("Please enter valid First Name");    
                    err = err+1;    
                    $('#name').focus();    
                }    
                else    
                {    
                    $('#err_name').html("");    
                }    
            }
            if($('#lname').val() == "")
            { 
                $('#err_lastname').html("Please enter Last Name");
                err = err+1;
                $('#lname').focus();
            }
            else
            {
                $('#err_lastname').html("");
            }
            if($('#lname').val() != ""){ 
                if(validateString($('#lname').val()))
                {
                    $('#err_lastname').html("Please enter valid Last Name");
                    err = err+1;
                    $('#lname').focus();
                }
                else
                {
                    $('#err_lastname').html("");
                }
            }
            if($('#payOptions_dob_date').val() != ""){     
    
                if (compare_date($('#payOptions_dob_date').val())) {
                    $('#err_dob').html("");    
                }    
                else{    
                    $('#err_dob').html("Date of Birth can not be current or future date");    
                    err = err+1;    
                    $('#payOptions_dob_date').focus();
                }    
            }
            if(jQuery.trim($('#address').val()) != "")
            { 
                if(validateAddress($('#address').val()))
                {
                    $('#err_address').html(invalid_addr_msg);
                    err = err+1;
                    $('#address').focus();
                }
                else
                {
                    $('#err_address').html("");
                }
            }
            if($('#mobileno').val() != "")    
            {   
                if(validatePhone($('#mobileno').val()))    
                {    
                    $('#err_mobile_no').html("Please enter valid Mobile No.");    
                    err = err+1;    
                    $('#mobileno').focus();    
    
                }    
                else    
                {    
                    $('#err_mobile_no').html("");    
                }    
            }    
         
           
            if(jQuery.trim($('#workphone').val()) != "")
            {
                if(validatePhone($('#workphone').val()))
                {

                    $('#err_work_phone').html("Please enter valid Work Phone");
                    err = err+1;
                    $('#workphone').focus();
                }
                else
                {

                    $('#err_work_phone').html("");
                }
            }

        if(jQuery.trim($('#payOptions_captcha').val()) == "")
        {
            $('#err_vocde').html("Please enter Verification Code");
            err = err+1;
            $('#payOptions_captcha').focus();
        }
        else {
            $('#err_vocde').html("");
        }
         if($('#payOptions_authorization_consent').is(':checked')){
             $('#err_authorization_consent').html("");
        }else{
            $('#err_authorization_consent').html("Please accept the authorization");
            err = err+1;
            $('#payOptions_authorization_consent').focus();
        }
       if(err == 0) {
        return true;
    } else {
        $('#payOptions_captcha').val('');
        $('#payOptions_authorization_consent').removeAttr('checked');
        document.getElementById('captcha_img').src = "<?php echo url_for('@sf_captcha') ?>?r=" + Math.random() + "&reload=1";
        return false;
    }
    }

    function updCaptcha(uri)
    {
        url = uri+"?r=" + Math.random() + "&reload=1";
        $('#captchaImg').attr('src', url);
    }
    function load_captcha(url){

        document.getElementById('captcha_img').src=url;
        $('#cap_ref').hide();
        setTimeout('show_reload_button()', 1000);

    }
    function show_reload_button(){


        $('#cap_ref').show();
    }

</script>