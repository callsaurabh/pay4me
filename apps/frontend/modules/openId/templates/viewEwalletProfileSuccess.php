
<?php use_helper('ePortal'); ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form'));?>


<div class="wrapForm2">
  
  <?php echo ePortal_legend(__('User Details'));?>
    
    
    <?php echo formRowComplete(__('First Name'),$name,'','','','username_row'); ?>
    <?php echo formRowComplete(__('Last Name'),$lastname,'','','','username_row'); ?>
    <?php echo formRowComplete(__('Username'),$user_detalis[0]['username'],'','','','username_row'); ?>
    <?php echo formRowComplete(__('Date of Birth'),$b_place,'','','','username_row'); ?>
    <?php echo formRowComplete(__('Address'),nl2br($address),'','','','username_row'); ?>
    <?php echo formRowComplete(__('Email'),$email,'','','','username_row'); ?>
   

   <?php echo formRowComplete(__('Mobile No.'),$mobile_no,'','','','username_row'); ?>

   <?php echo formRowComplete(__('Work Phone'),$work_phone,'','','','username_row'); ?>
    


    <div class="divBlock">
      <center id="multiFormNav">
        <?php echo button_to(__('Edit '),'',array('class'=>'formSubmit','onClick'=>'location.href=\''.url_for('openId/editEwalletProfile').'\'')); ?>
      </center>
    </div>
 </div>
  
