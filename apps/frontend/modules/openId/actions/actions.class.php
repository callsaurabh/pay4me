<?php

/**
 * openId actions.
 *
 * @package    mysfp
 * @subpackage openId
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class openIdActions extends sfActions {
	/**
	* Executes index action
	*
	* @param sfRequest $request A request object
	*/
	public function executeAuthlogin(sfWebRequest $request) {
		$this->getContext()->getConfiguration()->loadHelpers('Url');
		$screenName = null;
		$openIdUrl = null;
		$obj = new EpOpenIDExt();
		$openid_identifier = $request->getParameter('openid_identifier');
		$identityType = $request->getParameter('identType');
		$appVars = $request->getParameter('appVars');
		$this->getUser()->getAttributeHolder()->remove('appVars');
		$this->getUser()->setAttribute('appVars', $appVars);
		if ($openid_identifier == 'facebook') {
			$this->getUser()->setAttribute('merchantRequestId', $request->getParameter('merchantRequestId'));
			$this->getUser()->setAttribute('payType', $request->getParameter('payType'));
			$openIdUrl = url_for('openId/authlogin?identType=facebookreturn', true);
		}
		if ($request->getParameter('srcname') != "") {
			$screenName = $request->getParameter('srcname');
		}
		if (!isset($_REQUEST['openid_mode']) && $identityType != 'facebookreturn') {
			EpOpenIDExt::getOpenidProvider($openid_identifier, $screenName, $openIdUrl);
		}
		try {
			if ($identityType == 'facebookreturn') {
				$openIdUrl = url_for('openId/authlogin?identType=facebookreturn', true);
				$userDetails = EpOpenIDExt::getOpenidProviderDetails($_REQUEST, 'facebook', $openIdUrl);
				if ($userDetails) {
					$this->getUser()->setAttribute('openId', $userDetails);
					return $this->renderText('<script>window.opener.location = "' . url_for('openId/authUserDetails') . '"; self.close();</script>');
				} else {
					return $this->renderText('<script>self.close();</script>');
				}
				exit;
			} else {
				$openid = new LightOpenID();
				try {
					//if user authenticated via openid
					if ($openid->validate() && isset($_REQUEST['openid_mode']) && $_REQUEST['openid_mode'] != "cancel") {
						$userDetails = EpOpenIDExt::getOpenidProviderDetails($_REQUEST, $openid_identifier);
						$this->getUser()->setAttribute('merchantRequestId', $request->getParameter('merchantRequestId'));
						$this->getUser()->setAttribute('payType', $request->getParameter('payType'));
						$this->getUser()->setAttribute('userType', $request->getParameter('userType'));
						$this->getUser()->setAttribute('openId', $userDetails);
						return $this->renderText('<script>window.opener.location = "' . url_for('openId/authUserDetails') . '"; self.close();</script>');
						exit;
					}
					//if user cancelled to authenticate
					if (isset($_REQUEST['openid_mode']) && $_REQUEST['openid_mode'] == "cancel") {
						return $this->renderText('<script>self.close();</script>');
						exit;
					}
				} catch (Exception $e) {
					$this->logMessage('OpenID:: Your authentication has been failed due to some network problem! :: ' . $e->getMessage());
					exit;
				}
			}
		} catch (Exception $e) {
			$this->logMessage('OpenID:: Your authentication has been failed due to some network problem! :: ' . $e->getMessage());
			exit;
		}
		exit;
	}
	
	/**
	*
	* @param <type> $request
	* KYC form open...
	*/
	public function executeAuthUserDetails(sfWebRequest $request) {
		$data = $this->getUser()->getAttribute('openId');
		if (empty($data)) {
			$this->redirect('welcome/index');
		}
		$payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
		$openIdAuth = $payForMeObj->openIdAuthType($data['openIdAuth']);
                $data['openIdType'] = $openIdAuth;
		$this->getUser()->setAttribute('openId', $data);
		$this->forward('openId', 'index');
	}
	
	public function executeIndex(sfWebRequest $request) {
		$openIdResArr = $this->getUser()->getAttribute('openId');
		$chkUserExisis = Doctrine::getTable('UserDetail')->findByEmail($openIdResArr['email']);
		
		$request->setParameter('merchantRequestId', $this->getUser()->getAttribute('merchantRequestId'));
		$request->setParameter('payType', $this->getUser()->getAttribute('payType'));
		$request->setParameter('userType', $this->getUser()->getAttribute('userType'));

		//Email Id is Registered with Pay4me
		if ($chkUserExisis->count()) { 
			$ewalletType = $chkUserExisis->getFirst()->getewalletType();
			if (isset($ewalletType)) {
				$userDetails = Doctrine::getTable('sfGuardUser')->find($chkUserExisis->getFirst()->getuserId());
				$userFullDetails = array_merge($chkUserExisis->getFirst()->toArray(), $userDetails->toArray());
				if ($ewalletType == 'ewallet' || $ewalletType == 'ewallet_pay4me') {
					if ($this->getUser()->getAttribute('payType') && $this->getUser()->getAttribute('userType') == 'guestUser') {
						$this->forward('openId', 'guestUserDetail');
					}
					// redirect to association page
					$this->getRequest()->setParameter('userFullDetails', $userFullDetails);
					$this->forward('openId', 'ewalletAssociation');
				} else {
					$payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
					$authValidation = $payForMeObj->authValidation($chkUserExisis->getFirst()->getuserId(), $openIdResArr['openIdAuth']);
                                        
                                        //PAYM-2032
                                        if($chkUserExisis->getFirst()->getuserId() != "" && strtolower($openIdResArr['openIdType']) == "google"){
                                            $authValidation = true;
                                        }
                                        //PAYM-2032                                        
                                        
					if ($authValidation) {
						$authenticateObj = new userAuthentication();
						$authenticateObj->setUsername($userDetails->getUserName());
						// check user is already login with limited sessions, if yes redirect to page with error msg.
						if ($authenticateObj->getUserSessionStatus() == false) {
							$errors = "User with username - '" . $userDetails->getUserName() . "' is already logged in from another terminal";
							// $errors = "Someone already logged in by username '".$uname."'.";
							$this->getUser()->setFlash('error', $errors);
							if ($this->getUser()->getAttribute('merchantRequestId'))
								$this->redirect('@ewallet_signin');
							else
								$this->redirect('@homepage');
							return false;
						} 
						//check numeber of failed attempts. if number of attempts  exceed by failed attempt limit then redirect to page with error
						if ($authenticateObj->redirectOnFailedAttempt($request->getReferer()) == false) {
							$openIdUserDetails = Doctrine::getTable('sfGuardUser')->findByUsername($userDetails->getUserName())->toArray();
							$activeStatus = $openIdUserDetails[0]['is_active'];
							if ($activeStatus != true) {
								$errors = "User is not activated.";
							} else {
								$errors = "This username is blocked. Please contact the administrator.";
							}
							$this->getUser()->setFlash('error', $errors);
							if ($this->getUser()->getAttribute('merchantRequestId'))
								$this->redirect('@ewallet_signin');
							else
								$this->redirect('@homepage');
						} 
						// check user came for payment as new user then give error else signin the user.
						if($this->getUser()->getAttribute('userType') && $this->getUser()->getAttribute('userType') == 'newUser') {
							if ($ewalletType == 'guest_user') {
								$this->forward('sfGuardAuth', 'associateOrCreateNewAccountOrRecover');
							} else {
								$this->getUser()->setFlash('error', sprintf('User already exist'));
								$this->redirect('@ewallet_signin');
							}	
						} else {
							$this->getUser()->signIn($userDetails, false, false, null, true);
						}
						if(count($this->getUser()->getAttribute('openId')) == 0){
							$this->redirect('qna/new');
						}else{
							$guser = $this->getUser();
							$requestId = $request->getParameter('merchantRequestId');
							$paymentMode = $request->getParameter('payType');
							// check user came for payment as guest user
							if ($this->getUser()->getAttribute('payType')) {
								if ($this->getUser()->getAttribute('userType') == 'guestUser') {
									$this->getUser()->setAttribute('user_type', 'guestUser');
									$this->forward('openId', 'guestUserDetail');
								}
							}
							if ($ewalletType != 'guest_user') {
								return $this->redirectToEwalletHome($guser->getUsername(),$requestId,$paymentMode);
							} else {
								$this->forward('openId', 'createNewAccount');
							}
						}
					}else {
						// to check if domains are diffrent in openid_auth
						$openIdDomain = EpOpenIDExt::getOpenidDomain($openIdResArr['openIdAuth'], $chkUserExisis->getFirst()->getopenidAuth());
						if (!$openIdDomain) {
							$this->getUser()->setFlash('error', sprintf(($openIdResArr['openIdType']?$openIdResArr['openIdType']:'Open Id').' Authentication Failed'));
							$this->redirect('@homepage');
						} else {
							if($chkUserExisis->getFirst()->getMasterAccountId() && $chkUserExisis->getFirst()->getKycStatus()==1){
								// to recover page
								$openIdResArr['recoverAccount'] = 1;
								$openIdResArr['attachedOpenId'] = $openIdDomain;
								$this->getUser()->setAttribute('openId', $openIdResArr);
								$this->forward('openId', 'recoverAccount');
							}else{
								$this->getUser()->setFlash('error', sprintf('You can\'t move further as you are already registered on Pay4me with email "'.$openIdResArr['email'].'" using '.$openIdDomain.' authentication with no eWallet account.'));
								if ($this->getUser()->getAttribute('merchantRequestId'))
									$this->redirect('@ewallet_signin');
								else
									$this->redirect('@homepage');
							}
						}
					}
				}
			} else {
				$this->getUser()->setFlash('error', sprintf('Email id is already registered with Pay4me as non-ewallet User'));
				$this->redirect('@homepage');
			}
		//Email Id is not Registered with Pay4me
		} else {
			if(!$this->getUser()->getAttribute('payType')){
				$this->forward('sfGuardAuth', 'associateOrCreateNewAccountOrRecover');
			}
			else{
				// in case of coming from payment page, if ewallet then three option to show, if card payment then divert directly to create new account page
				if(($this->getUser()->getAttribute('userType') && $this->getUser()->getAttribute('userType') == 'newUser')) {
					$this->forward('sfGuardAuth', 'associateOrCreateNewAccountOrRecover');
				} else if ($this->getUser()->getAttribute('userType') && $this->getUser()->getAttribute('userType') == 'guestUser'){
					$request->setParameter('merchantRequestId', $this->getUser()->getAttribute('merchantRequestId'));
					$request->setParameter('payType', $this->getUser()->getAttribute('payType'));
					//$this->forward('openId', 'createNewAccount');
					$this->forward('openId', 'guestUserDetail');
				}else if ($this->getUser()->getAttribute('userType') && $this->getUser()->getAttribute('userType') == 'existingEwallet'){
					$this->getUser()->setFlash('error', sprintf('User Not Exist on Pay4me with email "'.$openIdResArr['email'].'".'));
					$this->redirect('@ewallet_signin');
				}
			}
		}
	}
	
	/**
	* Function to save/update user when came for payment as guest user.
	* @param sfWebRequest $request
	*/
	public function executeGuestUserDetail(sfWebRequest $request) {
		$openIdDetail = $this->getUser()->getAttribute('openId');
		$tempArray= array(
			'merchantRequestId' => $request->getParameter('merchantRequestId'),
			'payType' => $request->getParameter('payType'),
			'userType' => $request->getParameter('userType'),
			'name' => $openIdDetail['fname'],
			'lname'=> $openIdDetail['lname'],
			'email' => $openIdDetail['email'],
			'auth_info' => $openIdDetail['openIdAuth'],
			'payOptions'=> array('authorization_consent' => 'on'),
			'openid_type' => $openIdDetail['openIdType']
		);
		$resultArray = $tempArray['payOptions'];
		$this->auth_info = $tempArray['auth_info'];
		$request->setParameter('merchantRequestId', $tempArray['merchantRequestId']);
		$request->setParameter('payType', $tempArray['payType']);
		
		$finalArr = array_merge($resultArray, $tempArray);
		$chkUserExist = Doctrine::getTable('UserDetail')->findByEmail($tempArray['email']);
		if (!$chkUserExist->count()) {
			$con = Doctrine_Manager::connection();
			try {
				$con->beginTransaction();
				$sfGuarduserObj = $this->getUser()->saveGuardUserDetails($tempArray['email']);
				if(!is_object($sfGuarduserObj)) {
					$con->rollback();
					throw new Exception('error found');
				}
				$userTypeToCreate = sfConfig::get('app_pfm_role_guest_user');
				$sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate);
				if(!is_object($sfGroupDetails->getFirst())) {
					$con->rollback();
					throw new Exception('error found');
				}
				$sfGroupId = $sfGroupDetails->getFirst()->getId();
				$sfGuardGroupObj = $this->getUser()->saveGuardGroupDetails($sfGroupId, $sfGuarduserObj->getId());
				// If first time user then save in user details
				$userdetailobj = $this->getUser()->saveUserDetail($finalArr, $sfGuarduserObj->getId());
				if(!is_object($userdetailobj)) {
					$con->rollback();
					throw new Exception('error found');
				}
				$con->commit();
			}
			catch (Exception $e) {
				$con->rollback();
				$this->getUser()->setFlash('error', sprintf('Somthing went wrong! please try again.'));
				$this->redirect('@homepage');
			}
		} else {
			$sfGuarduserObj = Doctrine::getTable('sfGuardUser')->find($chkUserExist->getFirst()->getuserId());
			$sf_guard_user = Doctrine::getTable('sfGuardUser')->find($this->userId);
		}
		$subject = "Pay4me Continue Registration";
		$partialName = 'openid_registration';
		sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
		sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
		$registration_url = url_for('openId/createNewAccount', true) . '?id=' . encrypt($sfGuarduserObj->getId());
		$taskId = EpjobsContext::getInstance()->addJob(
			'SendMailRegistration',
			'email' . "/sendRegistrationEmail", 
			array(
				'userid' => $sfGuarduserObj->getId(), 
				'subject' => $subject,
				'partialName' => $partialName,
				'registration_url' => $registration_url,
			)
		);
		$this->logMessage("sceduled mail job with id: $taskId", 'debug');
		
		$request->setParameter('userid', $sfGuarduserObj->getId());
		$this->userId = $request->getParameter('userid');
		$sf_guard_user = Doctrine::getTable('sfGuardUser')->find($this->userId);
		$sfGuardUserObj = $this->getUser()->updateGuardUserDetails($this->userId);
		// Update user details in sf_guard_user and user_detail table when came for second times.
		if ($chkUserExist->count()) {
			$sfGuardUserObj = $this->getUser()->updateGuardUserDetails($this->userId, 'guest_user');
			$userdetailobj = $this->getUser()->updateOpenIdGuestUserDetails($this->userId);
		}
		$this->getUser()->signIn($sfGuardUserObj, false, false, null, true);
		$request->setParameter('merchantRequestId', $this->getUser()->getAttribute('merchantRequestId'));
		$request->setParameter('payType', $this->getUser()->getAttribute('payType'));
		$this->logOpenidCreateAccountSuccessAuditDetails($sf_guard_user->getUserName(), $this->userId);
		// Log successfull create new account via Openid Authentication
		//$this->redirectToEwalletHome($sf_guard_user->getUserName(),$tempArray['merchantRequestId'],$tempArray['payType']);
		$this->forward('sfGuardAuth', 'signin');
	}
	
	private function redirectToEwalletHome($uname, $requestid, $paymentMode) {
		// Log successfull login Audit Trail Details
		$this->logLoginSuccessAuditDetails($uname, $this->getUser()->getGuardUser()->getId());
		//create a log; successfull login of a bank user
		$this->getUser()->log_successful_login();
		$this->redirect('@payRedirect?requestId=' . $requestid . '&paymentMode=' . $paymentMode);
	}
	
	public function logLoginSuccessAuditDetails($uname,$id){
		//Log audit Details
		//$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
		$eventHolder = new pay4meAuditEventHolder(
		EpAuditEvent::$CATEGORY_SECURITY,
		EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
		EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGIN, array('username'=>$uname)),
		null);
		$this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
	}
		
	public function executeAssociateOrCreateNewOpenAccountOrRecover(sfWebRequest $request) {
		$userDetails = $request->getPostParameters();
		$request->setParameter('merchantRequestId', $this->getUser()->getAttribute('merchantRequestId'));
		$request->setParameter('payType', $this->getUser()->getAttribute('payType'));
		if ($userDetails['type'] == 'createNewAccount') {
			$this->forward('openId', 'createNewAccount');
		} else if ($userDetails['type'] == 'ewalletAssociation') {
			$assocDetails = $this->getUser()->getAttribute('openId');
			$assocDetails['assocEmail'] = 1;
			$this->getUser()->setAttribute('openId', $assocDetails);
			$this->forward('sfGuardAuth', 'signInOnlinePayment');
		} else if ($userDetails['type'] == 'recoverEwalletAccount') {
			$assocDetails = $this->getUser()->getAttribute('openId');
			$assocDetails['recoverAccount'] = 1;
			$this->getUser()->setAttribute('openId', $assocDetails);
			$this->forward('openId', 'recoverAccount');
		}
	}
	
	public function executeEwalletAssociation(sfWebRequest $request) {
		$userFullDetails = $request->getParameter('userFullDetails');
		$this->getRequest()->setParameter('signin', $userFullDetails);
		$this->forward('sfGuardAuth', 'signin');
	}
	
	public function executeSubmit(sfWebRequest $request) {
		$this->form = new OpenIdUserSignUpForm();
		$tempArray = $request->getPostParameters();
		$resultArray = $tempArray['payOptions'];
		$this->auth_info = $request->getParameter('auth_info');
		$request->setParameter('merchantRequestId', $tempArray['merchantRequestId']);
		$request->setParameter('payType', $tempArray['payType']);
		
		$finalArr = array_merge($resultArray, $tempArray);
		$this->form->bind($finalArr);
		// Checking if user exist if yes then check the ewallet user type.
		$chkUserExist = Doctrine::getTable('UserDetail')->findByEmail($finalArr['email']);
		$ewalletType = NULL;
		if ($chkUserExist->count()) {
			$ewalletType = $chkUserExist->getFirst()->getewalletType();
		}
		// If user type is openid guest then bypass the validation rule and update the user details to openid_pay4me.
		if ($ewalletType == 'guest_user' || $this->form->isValid()) {
			if ($ewalletType) {
				// Updating user details
				$userId = $chkUserExist->getFirst()->getUserId();
				$this->userId = $userId;
				$userdetailobj = $this->getUser()->updateOpenIdGuestUserDetails($userId, $finalArr);
				$sfGuarduserObj = new sfGuardUser();
				$sfGuarduserObj->setId($userId);
				$userTypeToCreate = sfConfig::get('app_pfm_role_ewallet_user');
				$sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate);
				$sfGroupId = $sfGroupDetails->getFirst()->getId();
				$sfGuardGroupObj = $this->getUser()->updateGuardGroupDetails($sfGroupId, $sfGuarduserObj->getId());
				$sfGuardUserObj = $this->getUser()->updateGuardUserDetails($userId, 'guest_user');
			} else {
				//saving in tbl_sf_guardUser
				$sfGuarduserObj = $this->getUser()->saveGuardUserDetails($request->getParameter('email'));
				$userTypeToCreate = sfConfig::get('app_pfm_role_ewallet_user');
				$sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate);
				$sfGroupId = $sfGroupDetails->getFirst()->getId();
				$sfGuardGroupObj = $this->getUser()->saveGuardGroupDetails($sfGroupId, $sfGuarduserObj->getId());
				//save in user details
				$userdetailobj = $this->getUser()->saveUserDetail($finalArr, $sfGuarduserObj->getId());
			}
			$subject = "Pay4me User Confirmation Mail";
			$partialName = 'account_details';
			$taskId = EpjobsContext::getInstance()->addJob('SendMailConfirmation', 'email' . "/sendEmail", array('userid' => $sfGuarduserObj->getId(), 'subject' => $subject, 'partialName' => $partialName));
			$this->logMessage("sceduled mail job with id: $taskId", 'debug');
			$request->setParameter('userid', $sfGuarduserObj->getId());
			$this->userId = $request->getParameter('userid');
			$sf_guard_user = Doctrine::getTable('sfGuardUser')->find($this->userId);
			$sfGuardUserObj = $this->getUser()->updateGuardUserDetails($this->userId);
			$this->getUser()->signIn($sfGuardUserObj, false, false, null, true);
			$request->setParameter('merchantRequestId', $this->getUser()->getAttribute('merchantRequestId'));
			$request->setParameter('payType', $this->getUser()->getAttribute('payType'));
			// Log successfull create new account via Openid Authentication
			$this->logOpenidCreateAccountSuccessAuditDetails($sf_guard_user->getUserName(), $this->userId);
			/*if($ewalletType == 'guest_user') {
				logout user to referesh signin menu under home 
				$this->getUser()->setFlash('notice', sprintf("Signup Successfully. Please login again."));
				$this->redirect('sfGuardAuth/signout');
			}
			*/
			$this->forward('sfGuardAuth', 'signin');
		} else {
			$this->setTemplate('index');
		}
	}
	
	public function executeCreateNewAccount(sfWebRequest $request) {
		sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
		$templateName = 'index';
		$this->setTemplate($templateName);
		// If user came for registration from email link set default params.
		if ($request->getParameter('id')) {
			$user_id = decrypt($request->getParameter('id'));
			$user_detail = Doctrine::getTable('UserDetail')->findByUserId($user_id);
			$user_mail = $user_detail->getFirst()->getemail();
			$redirect_url = '@homepage';

			$this->name = $user_detail->getFirst()->getName();
			$this->lname = $user_detail->getFirst()->getlast_name();
			$this->email = $user_detail->getFirst()->getemail();
			$this->auth_info = $user_detail->getFirst()->getopenidAuth();
		} else {
			// If user came for registration from home link.
			$openIdDetail = $this->getUser()->getAttribute('openId');

			$user_mail = $openIdDetail['email'];
			$redirect_url = $request->getReferer();

			$this->name = $openIdDetail['fname'];
			$this->lname = $openIdDetail['lname'];
			$this->email = $openIdDetail['email'];
			$this->auth_info = $openIdDetail['openIdAuth'];
			$this->openIdType = $openIdDetail['openIdType'];
			$this->merchantRequestId = $request->getParameter('merchantRequestId');
			$this->payType = $request->getParameter('payType');
		}
		$chkUserExist = Doctrine::getTable('UserDetail')->findByEmail($user_mail);
		if (count($chkUserExist) && $chkUserExist->getFirst()->getewalletType() != 'guest_user'){
			$this->getUser()->setFlash('error', sprintf('Signup process already completed'));
			$this->redirect($redirect_url);
		} else {		
			$this->form = new OpenIdUserSignUpForm(array(), array(
				'email' => $this->email,
				'name' => $this->name, 
				'lname' => $this->lname,
				'auth_info' => $this->auth_info,
				'merchantRequestId' => $this->merchantRequestId,
				'payType' => $this->payType,
				'openIdType' => $this->openIdType
			));
		}
	}
	
	public function executeRecoverSubmit(sfWebRequest $request) {
		$this->form = new RecoverEwalletAccountForm();
		$tempArray = $request->getPostParameters();
		
		$resultArray = $tempArray['payOptions'];
		$this->auth_info = $request->getParameter('auth_info');
		$request->setParameter('merchantRequestId', $tempArray['merchantRequestId']);
		$request->setParameter('payType', $tempArray['payType']);
		$finalArr = array_merge($resultArray, $tempArray);
		
		$this->form->bind($finalArr);
		if ($this->form->isValid()) {
			$chkUserExisis = Doctrine::getTable('UserDetail')->findByEmail($tempArray['email']);
			if ($chkUserExisis->count()) {
				$userDetails = Doctrine::getTable('sfGuardUser')->find($chkUserExisis->getFirst()->getuserId());
				//$epMasterobj = Doctrine::getTable('EpMasterAccount')->findById($chkUserExisis->getFirst()->getMasterAccountId());
				$epMasterobj = Doctrine::getTable('UserDetail')->getUserDetailBetDates($tempArray['ewallet'], $tempArray['email']);
				//if($epMasterobj->getFirst()->getAccountNumber() == $tempArray['ewallet']){
				if ($epMasterobj->count()) {
					$authenticateRecover = $this->authenticateRecover($userDetails, $chkUserExisis->getFirst());
					if ($authenticateRecover) {
						$this->getUser()->signin($userDetails, false,false,null,true); // Bug:34650
						// $this->redirect('@qna');
						$this->getController()->getPresentationFor("qna", "recoverAccountProcessWithoutQuestion" );     
					} else {
						$this->setTemplate('recover');
					}
				} else {
					$errors = "Entered Email Id and eWallet Account No. are not associated with each other.";
					$this->getUser()->setFlash('error', $errors);
					$this->setTemplate('recover');
				}
			} else {
				$errors = "Entered Email Id is not registered on Pay4me.";
				$this->getUser()->setFlash('error', $errors);
				$this->setTemplate('recover');
			}
		} else {
			$this->setTemplate('recover');
		}
	}
	
	public function authenticateRecover($userDetails, $uObj) {
		if (!($uObj->getOpenidAuth()) || $uObj->getEwalletType() == 'ewallet' || $uObj->getEwalletType() == 'ewallet_pay4me') {
			sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url','Tag'));
			$assocDetails = $this->getUser()->getAttribute('openId');
			$assocDetails['assocEmail'] = 1;
			$this->getUser()->setAttribute('openId', $assocDetails);
			$errors = "You are not authorized to recover eWallet account from here. Please ".link_to('Click here', 'sfGuardAuth/signInOnlinePayment', array('title' => 'Click here to associate', 'alt' => 'Click here to associate','class'=>'recover_link')) ." to associate your Account.";
			$this->getUser()->setFlash('error', $errors);
			return false;
		}
		$authenticateObj = new userAuthentication();
		$authenticateObj->setUsername($userDetails->getUserName());
		// check user is already login with limited sessions, if yes redirect to page with error msg.
		if ($authenticateObj->getUserSessionStatus() == false) {
			$errors = "User with username - '" . $userDetails->getUserName() . "' is already logged in from another terminal";
			$this->getUser()->setFlash('error', $errors);
			return false;
		}
		//check numeber of failed attempts. if number of attempts  exceed by failed attempt limit then redirect to page with error
		if ($authenticateObj->redirectOnFailedAttempt() == false) {
			$openIdUserDetails=Doctrine::getTable('sfGuardUser')->findByUsername($userDetails->getUserName())->toArray();
			$activeStatus= $openIdUserDetails[0]['is_active'];
			if($activeStatus!=true){
				$errors = "Please enter active eWallet Account number.";
			}else{
				$errors = "You are not authorized to recover eWallet account, as your eWallet Account No. is blocked. Please contact the administrator.";
			}
			$this->getUser()->setFlash('error', $errors);
			return false;
		}
		return true;
	}
	
	public function executeRecoverAccount(sfWebRequest $request) {
		$templateName = 'recover';
		$this->setTemplate($templateName);
		$openIdDetail = $this->getUser()->getAttribute('openId');
		$this->name = $openIdDetail['fname'];
		$this->lname = $openIdDetail['lname'];
		$this->email = $openIdDetail['email'];
		$this->auth_info = $openIdDetail['openIdAuth'];
		$this->openid_type = $openIdDetail['openIdType'];
		
		$this->merchantRequestId = $request->getParameter('merchantRequestId');
		$this->payType = $request->getParameter('payType');
		$this->form = new RecoverEwalletAccountForm(array(), array(
			'email' => $this->email,
			'name' => $this->name,
			'lname' => $this->lname,
			'auth_info' => $this->auth_info,
			'merchantRequestId' => $this->merchantRequestId,
			'payType' => $this->payType,
			'openid_type' => $this->openid_type
		));
	}
	
	/**
	* Action for security question and answers
	*/
	public function executeSecurityQuestionAnswers(sfWebRequest $request) {
		$this->userId = $request->getParameter('userid');
		$sf_guard_user = Doctrine::getTable('sfGuardUser')->find($this->userId);
		$this->form = new AddQnAForm();
		if ($request->isMethod('put')) {
			$this->form->bind($request->getParameter($this->form->getName()));
			if ($this->form->isValid()) {
				$this->qnaCount = Settings::getNumberOfQuestion();
				for ($i = 0; $i < $this->qnaCount; $i++) {
					$questionAns = new SecurityQuestionsAnswers();
					$questionAns->setUserId($request->getPostParameter('userid'));
					$questionAns->setSecurityQuestionId($request->getPostParameter('sf_guard_user[qna' . $i . '][security_question_id]'));
					$questionAns->setAnswer(trim($request->getPostParameter('sf_guard_user[qna' . $i . '][answer]')));
					$questionAns->save();
				}
				$sfGuardUserObj = $this->getUser()->updateGuardUserDetails($this->userId);
				$this->getUser()->signIn($sfGuardUserObj, false, false, null, true);
				$request->setParameter('merchantRequestId', $this->getUser()->getAttribute('merchantRequestId'));
				$request->setParameter('payType', $this->getUser()->getAttribute('payType'));
				// Log successfull create new account via Openid Authentication
				$this->logOpenidCreateAccountSuccessAuditDetails($sf_guard_user->getUserName(), $this->userId);
				$this->forward('sfGuardAuth', 'signin');
				#End Code To Save SecurityQuestion Answers
			}
		}
		$this->setTemplate("QnASignUp");
	}
	
	/**
	* Function to log audit for successful create new account via OpenId authentication
	*/
	public function logOpenidCreateAccountSuccessAuditDetails($uname,$id){
		//Log audit Details
		//$applicationArr = array(new EpAuditEventAttributeHolder('',$uname,$id));
		$eventHolder = new pay4meAuditEventHolder(
		EpAuditEvent::$CATEGORY_SECURITY,
		EpAuditEvent::$SUBCATEGORY_SECURITY_CREATE_NEW_ACCOUNT_OPENID,
		EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_CREATE_ACCOUNT_OPENID, array('username'=>$uname)),
		null);
		$this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
	}
	
	/**
	* ACTION FOR RECTIVATE ACCOUNT
	*/
	public function executeReactivate(sfWebRequest $request) {
		$code = $request->getParameter('code');
		$userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
		$this->user_details = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
		if (!$code) {
			$subject = "Code for eWallet Reactivation";
			$partialName = 'reactivate_security_mail';
			$securityCode = rand(1000, 10000);
			sfContext::getInstance()->getUser()->setAttribute('securityCode', $securityCode);
			$taskId = EpjobsContext::getInstance()->addJob('SendReactivateSecurityMail', 'email' . "/sendReactivateSecurityMail", array('userid' => $userId, 'subject' => $subject, 'partialName' => $partialName, 'securityCode' => $securityCode));
			$this->logMessage("sceduled mail job with id: $taskId", 'debug');
		}
		$this->getUser()->setFlash('error', NULL);
		if ($code == "valid") {
			$this->getUser()->setAttribute('reactivate', $code);
			$this->redirect('admin/eWalletUser');
		} else {
			$this->getUser()->setFlash('notice', sprintf('A Reactivation code has been sent to your email id, Please submit the same code to reactivate your eWallet account.'));
		}
		if (count($this->user_details[0]['UserDetail']) > 0) {
			$this->userId = $this->user_details[0]['id'];
			$this->name = $this->user_details[0]['UserDetail'][0]['name'];
			$this->email = $this->user_details[0]['UserDetail'][0]['email'];
		} else {
			$this->userId = '';
			$this->name = '';
			$this->email = '';
		}
		$this->setTemplate('reactivateEwalletAccount');
	}
	
	public function executeCheckReactivateSecurityCode(sfWebRequest $request) {
		$reactivateEwalletCode = $request->getParameter('reactivate_ewallet_code');
		$sessionreactivateCode = sfContext::getInstance()->getUser()->getAttribute('securityCode');
		
		if ($reactivateEwalletCode != $sessionreactivateCode) {
			$this->getUser()->setFlash('notice', NULL);
			$this->getUser()->setFlash('error', 'The code entered does not match with the reactivation code provided in the mail.Please try again!', true);
			$this->setTemplate('reactivateEwalletAccount');
		} else {
			$user_id = $this->getUser()->getGuardUser()->getId();
			$billobj = new billManager();
			$user_detail = $billobj->getDetailsUser($user_id);
			$updated_user_type = $user_detail['0']['ewallet_type'];
			$userType = explode('_', $updated_user_type);
			try {
				// Removed new pin mail to the user after reactivate [Bug:33092]
				$this->getUser()->updateUserDetailForDeactivate($user_id, $userType[0]);
				$subject = "Re-Activate eWallet Account";
				$partialName = 'reactivate_mail';
				$taskId = EpjobsContext::getInstance()->addJob('SendReactivateSecurityMail', 'email' . "/sendReactivateSecurityMail", array('userid' => $user_id, 'subject' => $subject, 'partialName' => $partialName));
				$this->logMessage("sceduled mail job with id: $taskId", 'debug');
				$this->getUser()->setFlash('notice', 'You have successfully reactivated your eWallet Account on Pay4me.', true);
				$this->redirect('openId/reactivate?code=valid');
			} catch (Exception $e) {
				echo $this->renderText($e);
			}
			$this->getAttributeHolder()->remove('securityCode');
		}
	}
	
	/**
	* ACTION FOR DEACTIVATE ACCOUNT
	*/
	public function executeDeactivate(sfWebRequest $request) {
		$user_id = $this->getUser()->getGuardUser()->getId();
		$billobj = new billManager();
		$user_detail = $billobj->getDetailsUser($user_id);
		$ewalletType = $user_detail['0']['ewallet_type'];
		if (!strpos($ewalletType, 'pay4me')) {
			$request->setParameter('deactivate', true);
		}
		$this->forward('admin', 'eWalletUserDeactive');
	}
	
	public function executeDeactivatefinal(sfWebRequest $request) {
		$user_id = $this->getUser()->getGuardUser()->getId();
		$billobj = new billManager();
		$user_detail = $billobj->getDetailsUser($user_id);
		$updated_user_type = $user_detail['0']['ewallet_type'] . '_pay4me';
		try {
			$this->getUser()->updateUserDetailForDeactivate($user_id, $updated_user_type);
			$subject = "De-Activate eWallet Account";
			$partialName = 'deactive_mail';
		
			$taskId = EpjobsContext::getInstance()->addJob('SendDeactivateAccountMail', 'email' . "/sendDeactivateAccountMail", array('userid' => $user_id, 'subject' => $subject, 'partialName' => $partialName));
			$this->logMessage("sceduled mail job with id: $taskId", 'debug');
			return true;
		} catch (Exception $e) {
			echo $this->renderText($e);
		}
	}
	
	/**
	* ACTION FOR ACTIVATE ACCOUNT
	*/
	public function executeActivate(sfWebRequest $request) {
		$userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
		$ewalletObj = new ewallet();
		$kycStatus = $ewalletObj->getKycstatus($userId);
		if ($kycStatus == 2 || $kycStatus == 3) {
			$this->kycStatus = 2;
			$this->forward('ewallet', 'kyc');
		}
		if ($userId)
			$this->user_detalis = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
		$this->userId = $this->user_detalis[0]['id'];
		if (count($this->user_detalis[0]['UserDetail']) > 0) {
			if (trim($request->getPostParameter('name')) != "") {
			$this->name = $request->getPostParameter('name');
			} else {
				$this->name = $this->user_detalis[0]['UserDetail'][0]['name'];
			}
			if (trim($request->getPostParameter('lname')) != "") {
				$this->lastname = $request->getPostParameter('lname');
			} else {
				$this->lastname = $this->user_detalis[0]['UserDetail'][0]['last_name'];
			}
			if (trim($request->getPostParameter('dob')) != "") {
				$this->dob = date('Y-m-d', strtotime(trim($request->getPostParameter('dob_date'))));
			} else {
				$this->dob = $this->user_detalis[0]['UserDetail'][0]['dob'];
			}
			if (trim($request->getPostParameter('address')) != "") {
				$this->address = $request->getPostParameter('address');
			} else {
				$this->address = $this->user_detalis[0]['UserDetail'][0]['address'];
			}
			$this->email = $this->user_detalis[0]['UserDetail'][0]['email'];
			if (trim($request->getPostParameter('mobileno')) != "") {
				$this->mobile_no = $request->getPostParameter('mobileno');
			} else {
				$this->mobile_no = $this->user_detalis[0]['UserDetail'][0]['mobile_no'];
			}
			if (trim($request->getPostParameter('workphone')) != "") {
				$this->work_phone = $request->getPostParameter('workphone');
			} else {
				$this->work_phone = $this->user_detalis[0]['UserDetail'][0]['work_phone'];
			}
			if (trim($request->getPostParameter('sms')) != "") {
				$this->send_sms = $request->getPostParameter('sms');
			} else {
				$this->send_sms = $this->user_detalis[0]['UserDetail'][0]['send_sms'];
			}
			$this->ewProfileEditForm = new CustomEwalletEditUserForm(array('username' => $this->user_detalis[0]['username'], 'dob' => $this->dob, 'name' => $this->name, 'lname' => $this->lastname, 'address' => $this->address, 'mobileno' => $this->mobile_no, 'workphone' => $this->work_phone, 'email' => $this->email, 'sms' => $this->send_sms), array('activate' => true, 'userId' => $this->userId, 'display' => false, 'edit' => true), null);
		} else {
			$this->userId = '';
			$this->name = '';
			$this->lastname = '';
			$this->address = '';
			$this->email = '';
			$this->mobile_no = '';
			$this->work_phone = '';
			$this->send_sms = '';
		}
		$this->setTemplate('editEwalletProfile');
	}
	
	public function executeUpdate(sfWebRequest $request) {
		$this->forward404Unless($request->isMethod('post'));
		$userid = $this->getUser()->getGuardUser()->getId(); //$request->getPostParameter('userId');
		$userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userid);
		$userDetailId = "";
		if ($userDetailObj->count()) {
			$userDetailId = $userDetailObj->getFirst()->getId();
		}
		if ($userDetailId != "") {
			if (!$this->verifyEditEwalletProfile($request)) {
				$this->forward('openId', 'activate');
			}
			$currency = $request->getPostParameter('currency_id');
			$date = date('Y-m-d', strtotime(trim($request->getPostParameter('dob_date'))));
			$user_detail_arr = array(
				'fname' => trim($request->getPostParameter('name')),
				'lname' => trim($request->getPostParameter('lname')),
				'dob' => $date,
				'address' => trim($request->getPostParameter('address')),
				'mobile' => trim($request->getPostParameter('mobileno')),
				'work_phone' => trim($request->getPostParameter('workphone'))
			);
			$this->getUser()->updateUserDetailRegistrationActivate($userid, $user_detail_arr);
			$msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PROFILE_UPDATED;
			$userName = $this->getUser()->getGuardUser()->getUsername();
			$eventHolder = new pay4meAuditEventHolder(
				EpAuditEvent::$CATEGORY_TRANSACTION,
				EpAuditEvent::$SUBCATEGORY_TRANSACTION_PROFILE_UPDATED,
				EpAuditEvent::getFomattedMessage($msg, array('username' => $userName)),
				null
			);
			$this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
			$this->redirect('ewallet/kyc?currency=' . $currency);
		}
	}
	
	protected function verifyEditEwalletProfile($request) {
		$msg = array();
		if (trim($request->getPostParameter('name')) == "") {
			$msg[] = 'Please enter First Name';
		}
		if (trim($request->getPostParameter('lname')) == "") {
			$msg[] = 'Please enter Last Name';
		}
		if (trim($request->getPostParameter('username')) == "") {
			$msg[] = 'Please enter User Name ';
		}
		if (trim($request->getPostParameter('address')) != "") {
			$len = strlen(trim($request->getPostParameter('address')));
			if ($len > 255) {
				$msg[] = 'Please enter valid address';
			}
		}
		if (trim($request->getPostParameter('mobileno')) == "") {
			$msg[] = 'Please enter Mobile number';
		}
		if (trim($request->getPostParameter('sms')) == "gsmno" || trim($request->getPostParameter('sms')) == "both") {
			if (trim($request->getPostParameter('mobileno')) == "") {
				$msg[] = 'Please enter Mobile No.';
			}
			if (trim($request->getPostParameter('mobileno')) != "") {
				if (!preg_match("/^([+]{1})([0-9]{10,15})$/i", $request->getPostParameter('mobileno'))) {
					$msg[] = 'Please enter Valid Mobile No.';
				}
			}
		}
		if (trim($request->getPostParameter('sms')) == "workphone" || trim($request->getPostParameter('sms')) == "both") {
			if (trim($request->getPostParameter('workphone')) == "") {
				$msg[] = 'Please enter Work Phone.';
			}
		}
		if (trim($request->getPostParameter('workphone')) != "") {
			if (!preg_match("/^([+]{1})([0-9]{10,15})$/i", $request->getPostParameter('workphone'))) {
				$msg[] = 'Please enter Valid Work Phone.';
			}
		}
		if (trim($request->getPostParameter('captcha')) == "") {
			$msg[] = 'Please enter Verification Code';
		}
		if (trim(strtolower($request->getPostParameter('captcha'))) != "") {
			$hashAlgo = sfConfig::get('app_sf_crypto_captcha_hash_algo');
			//[wp029][CR045][Bug Id 29399]srt to lower as we make it catpcha case insensitive
			$hashedPwd = hash($hashAlgo, strtolower($request->getPostParameter('captcha')));
			$getCaptcha = $this->getUser()->getAttribute('captcha_code', null, 'captcha');
			// if(trim($request->getPostParameter('captcha')) != $this->getUser()->getAttribute('captcha')) {
			if ($hashedPwd != $getCaptcha) {
				$msg[] = 'Verification Code doesn\'t match with entered code';
			}
		}
		if (count($msg) > 0) {
			$msg = implode($msg, ', ');
			$this->getUser()->setFlash('error', sprintf($msg));
			return false;
		}
		return true;
	}
}