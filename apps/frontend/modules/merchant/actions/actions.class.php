<?php

/**
 * merchant actions.
 *
 * @package    mysfp
 * @subpackage merchant
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class merchantActions extends sfActions {
  public function executeIndex(sfWebRequest $request) {
    $this->merchant_list = Doctrine::getTable('Merchant')
        ->createQuery('a');

    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('MerchantService',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->merchant_list);
    $this->pager->setPage($this->page);
    $this->pager->init();
  }
   public function executeListInterswitchMerchant(sfWebRequest $request) {
    $this->merchant_list = Doctrine::getTable('InterswitchMerchantCategoryMapping')->getMerchantsList();

    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('InterswitchMerchantCategoryMapping',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->merchant_list);
    $this->pager->setPage($this->page);
    $this->pager->init();
  }
  public function executeEditInterswitchCatogory(sfWebRequest $request) {
      $obj=Doctrine::getTable('InterswitchMerchantCategoryMapping')->find($request->getParameter('id'));
      $this->id=$request->getParameter('id');
      $this->form = new InterswitchMerchantCategoryMappingForm($obj);
  }
  public function executeSaveInterswitchCatogory(sfWebRequest $request) {
      $obj=Doctrine::getTable('InterswitchMerchantCategoryMapping')->find($request->getParameter('id'));
      $this->form = new InterswitchMerchantCategoryMappingForm($obj);
      $this->processInterswitchMerchantCategoryMappingForm($request, $this->form);
  }
protected function processInterswitchMerchantCategoryMappingForm(sfWebRequest $request, sfForm $form)
    {
        $post_values=$request->getParameter($form->getName());
        $post_values['merchant_id']=$form->getObject()->getMerchantId();
        $form->bind($post_values, $request->getFiles($form->getName()));
        if ($form->isValid()){
            $tobj=$form->save();
            $merchantDetail = Doctrine::getTable('Merchant')->getMerchantDetailsById($tobj->getMerchantId());
            $categoryDetail = Doctrine::getTable('InterswitchCategory')->getCategoryName($tobj->getInterswitchCategoryId());
            $this->getUser()->setFlash('notice', "Merchant ".$merchantDetail['0']['name']. " with Interswitch ".$categoryDetail['0']['name']."  saved successfully",true);
            $this->redirect('merchant/listInterswitchMerchant');
        }else{
             $this->getUser()->setFlash('error', sprintf('Error in saving'));
             $this->setTemplate('editInterswitchCatogory');
        }
    }

  public function executeShow(sfWebRequest $request) {
    $this->merchant = Doctrine::getTable('Merchant')->find($request->getParameter('id'));
    $this->forward404Unless($this->merchant);
    $this->setLayout(false);
  }

  public function executeNew(sfWebRequest $request) {
    $this->form = new MerchantForm();
  }

  public function executeCreate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post'));
    
    $merchant = $request->getPostParameter('merchant');
    $getMerchantDetails = Doctrine::getTable('Merchant')->chkMerchantExists($merchant['name']);

    if($getMerchantDetails[0]['COUNT'] == '1'){
        $this->getUser()->setFlash('error', sprintf('Cannot create merchant, already deleted by administrator'));
        $this->redirect('merchant/index');
    }else{
      $this->form = new MerchantForm();
      $this->processForm($request, $this->form);
      $this->setTemplate('new');
    }
  }

  public function executeEdit(sfWebRequest $request) {
    $this->forward404Unless($merchant = Doctrine::getTable('Merchant')->find($request->getParameter('id')), sprintf('Object merchant does not exist (%s).', $request->getParameter('id')));
    $this->form = new MerchantForm($merchant);
  }

  public function executeUpdate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($merchant = Doctrine::getTable('Merchant')->find($request->getParameter('id')), sprintf('Object merchant does not exist (%s).', $request->getParameter('id')));
    $this->form = new MerchantForm($merchant);
    $params = $request->getParameter('merchant');
    $name = $params['name'];
    $getMerchantDetails = Doctrine::getTable('Merchant')->chkMerchantExists($name);

    if($getMerchantDetails[0]['COUNT'] == '1'){
        $this->getUser()->setFlash('error', sprintf('Cannot create merchant , already deleted by administrator'));
        $this->redirect('merchant/edit?id='.$request->getParameter('id'));
    }else{
         $this->processForm($request, $this->form);
    }
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request) {
    $request->checkCSRFProtection();

    $this->forward404Unless($merchant = Doctrine::getTable('Merchant')->find($request->getParameter('id')), sprintf('Object merchant does not exist (%s).', $request->getParameter('id')));
    $merchant->delete();
    $this->getUser()->setFlash('notice', sprintf('Merchant deleted successfully'));
    $this->redirect('merchant/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {


    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid()) {
      $merchant_params = $request->getParameter('merchant');
      $name = $merchant_params['name'];
      $id = $merchant_params['id'];
      $merchant_obj = merchantServiceFactory::getService(merchantServiceFactory::$TYPE_BASE);
      $already_created = $merchant_obj->getTotalMerchant($name,$id);
      if($already_created) {
        $this->getUser()->setFlash('error', sprintf('Merchant of same name already exists'));
      // $this->redirect('bank_branch/index');
      }
      else //there is no duplicacy
      {
        $merchant = $form->save();
        if(($request->getParameter('action')) == "update") {
          $this->getUser()->setFlash('notice', sprintf('Merchant updated successfully'));
        }
        else if(($request->getParameter('action')) == "create") {



            do {
              
              $merchant_code = mt_rand();
              $duplicacy_code = $merchant_obj->chkCodeDuplicacy($merchant_code);
            } while ($duplicacy_code > 0);


            $merchant_key = mt_rand();
            $auth_info = base64_encode($merchant_code .":" .$merchant_key);
            $merchant_id = $merchant->getId();
            $merchant_details = Doctrine::getTable('Merchant')->find(array($merchant_id));
            $merchant_details->setMerchantCode($merchant_code);
            $merchant_details->setMerchantKey(md5($merchant_key));
            $merchant_details->setAuthInfo(base64_encode($auth_info));
            $merchant_details->save();
            $this->getUser()->setFlash('notice', sprintf('Merchant added successfully <br> Merchant code is - '.$merchant_code.', Merchant Key is - '.md5($merchant_key)));
          }
        $this->redirect('merchant/index');

      }
    }

  }
}
