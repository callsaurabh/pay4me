<table border="1" cellpadding="3" cellspacing="3" width="75%" align="center" style="border-color:#bbbbbb; border-collapse:collapse;">
  <tbody>
    <tr><td colspan="2" align="center"><font size="3"><b>Merchant Details</b></font></td></tr>
    <tr>
      <td width="50%"><font size="2"><b>Merchant Name:</b></font></td>
      <td><font size="2"><?php echo $merchant->getName() ?></font></td>
    </tr>
    <tr>
      <td width="50%"><font size="2"><b>Merchant Code:</b></font></td>
      <td><font size="2"><?php echo $merchant->getMerchantCode() ?></font></td>
    </tr>
    <tr>
      <td width="50%"><font size="2"><b>Merchant Key:</b></font></td>
      <td><font size="2"><?php echo $merchant->getMerchantKey() ?></font></td>
    </tr>
  </tbody>
</table>
