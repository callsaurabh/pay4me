<div id='theMid'>
    <?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>
    <?php use_helper('Pagination'); ?>


    <?php echo ePortal_listinghead('Merchants with Interswitch Verve/Master Card Naira Category'); ?>
    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr class="alternateBgColour">
                <th width="100%" >
                    <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                    <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                </th>
            </tr>
        </table>
        <br class="pixbr" />
    </div>

    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <thead>
                <tr class="horizontal">
                    <th width = "2%">S.No.</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (($pager->getNbResults()) > 0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $page = $sf_context->getRequest()->getParameter('page', 0);
                    $i = max(($page - 1), 0) * $limit;
                    foreach ($pager->getResults() as $result):
                        $i++;
                ?>
                        <tr class="alternateBgColour">
                            <td><?php echo $i ?></td>
                            <td><?php echo link_to($result->getMerchant()->getName(), url_for('merchant/show?id=' . $result->getMerchant()->getId()), array('popup' => array('popupWindow', 'width=610,height=400,left=320,top=0'))) ?></td>
                           <td><?php echo $result->getInterswitchCategory()->getName(); ?></td> <td align="center"><?php echo link_to(' ', 'merchant/editInterswitchCatogory?id=' . $result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
                            </td>
                        </tr>
                <?php endforeach; ?>

                        <tr><td colspan="5">
                                <div class="paging pagingFoot"><?php echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName() . '/' . $sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id')))  ?>

                                </div></td></tr>

                <?php }else {
 ?>
                        <tr><td  align='center' class='error'>No Merchant found</td></tr>
<?php } ?>

                </tbody>

            </table>
        </div>
<?php //echo button_to('Add New', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for('merchant/newMerchantInterswitchCatMapping') . '\'')); ?>
</div>