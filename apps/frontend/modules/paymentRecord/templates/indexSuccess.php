<h1>PaymentRecord List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Payment mandate</th>
      <th>Account no</th>
      <th>Sortcode</th>
      <th>Amount</th>
      <th>Account name</th>
      <th>Payor</th>
      <th>Narration</th>
      <th>Unique</th>
      <th>Status</th>
      <th>Created at</th>
      <th>Updated at</th>
      <th>Deleted</th>
      <th>Created by</th>
      <th>Updated by</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($payment_record_list as $payment_record): ?>
    <tr>
      <td><a href="<?php echo url_for('paymentRecord/show?id='.$payment_record->getId()) ?>"><?php echo $payment_record->getId() ?></a></td>
      <td><?php echo $payment_record->getPaymentMandateId() ?></td>
      <td><?php echo $payment_record->getAccountNo() ?></td>
      <td><?php echo $payment_record->getSortcode() ?></td>
      <td><?php echo $payment_record->getAmount() ?></td>
      <td><?php echo $payment_record->getAccountName() ?></td>
      <td><?php echo $payment_record->getPayor() ?></td>
      <td><?php echo $payment_record->getNarration() ?></td>
      <td><?php echo $payment_record->getUniqueId() ?></td>
      <td><?php echo $payment_record->getStatus() ?></td>
      <td><?php echo $payment_record->getCreatedAt() ?></td>
      <td><?php echo $payment_record->getUpdatedAt() ?></td>
      <td><?php echo $payment_record->getDeleted() ?></td>
      <td><?php echo $payment_record->getCreatedBy() ?></td>
      <td><?php echo $payment_record->getUpdatedBy() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('paymentRecord/new') ?>">New</a>
