Dear Sir
<br /><br />

Please find below the Bank Account Information to carry out the attached transaction :.<br /><br />

<B>Bank Name:</B><?php if(isset($bank_name)){ echo $bank_name ; } ?><br />
<B>Account Name:</B><?php if(isset($account_name)){ echo $account_name ; } ?><br />
<B>Account No:</B><?php if(isset($account_no)){ echo $account_no ; } ?><br />
<B>Sortcode:</B><?php if(isset($bank_sortcode)){ echo $bank_sortcode ; } ?><br />
<BR><BR>
<B>** Expected Charges to be deducted for NIBSS processing for all accounts mentioned in the attached file (s) are: </B><?php if(isset($nibssChgToSend)){ echo $nibssChgToSend ; } ?>
<br />Please do not process this transaction, if the charges are incorrect.<br /><br /><br />
Regards<br /><br />

Pay4Me
