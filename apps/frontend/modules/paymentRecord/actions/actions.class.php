<?php

/**
 * paymentRecord actions.
 *
 * @package    mysfp
 * @subpackage paymentRecord
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class paymentRecordActions extends sfActions
{
  //this function to be run by cron-----------
  public function executeProcessNibssPayment(sfWebRequest $request)
  {
    
    //first get the mandate id (currently pass 1 as default)
    $paymentBatches = $this->getBatchId($request);
    
    foreach($paymentBatches as $pmtBatch)
    {
      $mandateId = $pmtBatch['id'];

      //using this mandate id, get the information from NIBSS charges report table
      $getNibssInfo = Doctrine::getTable('NibssChargesReports')->getNibssChgAmount($mandateId);
      if(isset($getNibssInfo) && count($getNibssInfo) > 0)
      {
        $nibssChgToSend = $getNibssInfo[0]['nibss_charges'];
      }
      else
      {
        $nibssChgToSend = 0;
      }
      $createObj = new paymentRecordManager();

      $records = $createObj->getPaymentRecords($mandateId);      
      $nibssSetupData = $createObj->getNibssSettings();
      $nibssSetupDataInstance = $nibssSetupData[0];

      $filenameInitial = $nibssSetupData[0]['filename'];
      $messageFrom = $nibssSetupData[0]['message_from'];
      $messageTo = $nibssSetupData[0]['message_to'];
      $massageCc = $nibssSetupData[0]['message_cc'];
      $messageSub = $nibssSetupData[0]['message_subject'];
      $payor = $nibssSetupData[0]['payor'];
      $messageFrmName = $nibssSetupData[0]['message_from_name'];
      $serverSettings = $nibssSetupData[0]['outboundserver_address'];
      $portNo = $nibssSetupData[0]['outboundserver_port'];
      $sendUsername = $nibssSetupData[0]['username'];
      $sendPassword = $nibssSetupData[0]['password'];
      //get all data from the payment record table for this mandate id

      /* Code for creating the csv file, saving it at a location and then updating the table row with the filename starts here */
      $current_page = $request->getParameter('page');
      $filename = $filenameInitial."_".date("Ymd")."_".$mandateId;

      //get nibss payee information
      $getNibssPayeeBankData = $createObj->getPayeeInfo();

      //also add this file name in the mandate table
      $forSaveFilename = $filename.".csv";
      $createObj->updateFilename($mandateId, $forSaveFilename);

      $headerNames = array('id', 'account_no', 'sortcode', 'amount', 'account_name', 'payor', 'narration') ;
      $getValue = $this->renderText(CsvHelper::getCsv($records,$headerNames, $filename));

      /* Code for creating the csv file, saving it at a location and then updating the table row with the filename ends here */
      /*------------------------------------------------XXXXXXXXXXXXXXXXXXXXXXXX-----------------------------------------------------*/

      //get the file to be attached and sent to NIBSS
      $filePathToSend=sfConfig::get('sf_upload_dir');

      //append file name to the path
      $fileToSend = $filePathToSend."/".$forSaveFilename;

      // Variable for message parts
      $partialVars = array(
         'bank_name'      => $pmtBatch['from_bank'],
         'account_name'   => $pmtBatch['account_name'],
         'account_no'     => $pmtBatch['account_no'],
         'bank_sortcode'  => '',
         'nibssChgToSend' => $nibssChgToSend
      );
      $partialName='nibbsMailSend';

      $mailToGroup = array() ;
      // TODO externalize it - it should also have the date for which we
      // are sending these instructions for - ??
      $mailSubject = "Pay4me Credit Instructions";
      $mailingOptions = array('mailFrom' => array($messageFrom => 'Pay4Me'),'mailTo' => $messageTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $mailSubject ) ;
      $mailingOptions['attachements'][0]['filepath'] = $fileToSend;
      $mailingOptions['attachements'][0]['mime'] = 'text/csv';

      // Now the account setup
      $mailServerSettings['server'] = $nibssSetupDataInstance['outboundserver_address'];
      $mailServerSettings['port'] = $nibssSetupDataInstance['outboundserver_port'];
      $mailServerSettings['encryption'] = 'ssl';
      $mailServerSettings['username'] = $nibssSetupDataInstance['username'];
      $mailServerSettings['password'] = $nibssSetupDataInstance['password'];
      $mailServerSettings['connectionType'] = 'smtp';

      $mailer = new Mailing();
      $isMailSent = $mailer->sendMail($partialName, $partialVars, $mailingOptions) ;

      if($isMailSent)
      {
        $returnArray = array();
        $this->updateNibssRows($mandateId);
        $this->getUser()->setFlash('confirm', 'Mail sent successfully');
      }
      else
      {
        $returnArray = array();
        //return $returnArray = array('returnValue'=>'1', 'returnMessage'=>'Mail could not be sent this time. please try again later');
      }
    }

    
  }

  //this method will create payment batch and
  // payment records.
  public function executeConsolidatePayment(sfWebRequest $request)
  {    
    //fetch the data from the merchant request table
    $createObj = new paymentRecordManager();
    $getMerchData = $createObj->getMerchantRequest();
    
    //get merchant service using the id
    if(isset($getMerchData) && count($getMerchData) > 0)
    {
      foreach($getMerchData as $mData){      
        //$getData = new PayForMeConsolidationManager($mData['id']);
      }
      $this->executeProcessNibssPayment($request);
    }

    return sfView::NONE ;
    
  }

  //function to get the NIbss payment batch records
  private function getBatchId(sfWebRequest $request)
  {
    $batchIds = array();
    $batchIds = Doctrine::getTable('PaymentBatch')->getPaymentBatches();
    return $batchIds;
  }

  //function to update payment_batch and payment_record table
  private function updateNibssRows($mandateId)
  {
    $createObj = new paymentRecordManager();

    //this line will update the status to 1 in payment_record
    $createObj->updateNibssMailSentData($mandateId);

    //this line will update the status to 1 in payment_batch
    $createObj->updatePaymentBatch($mandateId);
  }

  //function to read the file into an array
  public function executeFileArray(sfWebRequest $request)
  {
    //get the file to be attached
    $filePath1=sfConfig::get('sf_upload_dir');
    $filePath = $filePath1."/transaction.txt";

    $file_handle = fopen($filePath, "rb");
    $parts = array();
    $i = 0;
    while (!feof($file_handle) ) 
    {
        $line_of_text = fgets($file_handle);
        $anArray[] = trim($line_of_text);
    }    
    fclose($file_handle);
    $counterVal = count($anArray);
    for($x = 0; $x < $counterVal; $x++)
    {
      if($anArray[$x] != '----------------------')
        $newArray[$x] = $anArray[$x];
    }
    
    $getArray = array();
    $pos = 0; $twoPos = 0;
    $getArray = array_chunk($newArray, 4);
    
    //refine this array further
    $counterVal = count($getArray);
    for($x = 0; $x < $counterVal; $x++)
    {
      if(count($getArray[$x]) < 4)
        unset($getArray[$x]);      
    }
    $counterVal = count($getArray);
    for($n = 0; $n < $counterVal; $n++)
    {
      $arrCount = count($getArray[$n]);      
      for($m = 0; $m < $arrCount; $m++)
      {
        $splitCount = explode(":", $getArray[$n][$m], 2);        
        $getArray[$n][trim($splitCount[0])] = trim($splitCount[1]);
      }
    }
    $counterVal= count($getArray);
    for($n = 0; $n < $counterVal; $n++)
    {
      $arrCount = count($getArray[$n]);
      print_r($arrCount);
      for($m = 0; $m < $arrCount; $m++)
      {
        //if()
      }
    }

    echo "<pre>"; print_r($getArray); die();
    return sfView::NONE ;
  }

  public function executeSplit()
  {
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', '300');
    $createObj = new PayForMeSplitManager();    
    return sfView::NONE ;
  }
}
