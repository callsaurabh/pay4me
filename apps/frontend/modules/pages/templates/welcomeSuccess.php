
<?php
use_stylesheet('default');
//echo sfContext::getInstance()->getUser()->getFlash('error');
if($sf_user->hasFlash('error')){
  echo "<div id='flash_error' class='error_list'><span>".sfContext::getInstance()->getUser()->getFlash('error')."</span></div>";
}
if ($sf_user->hasFlash('notice')){
  echo "<div id='flash_notice' class='error_list' ><span>".sfContext::getInstance()->getUser()->getFlash('notice')."</span></div>";
}
?>
<div id="midTop">
  <div id="wrapMainIMG"><?php echo image_tag("/img/mainIMG_01.jpg",array('alt'=>"MainIMG"));?></div>
  <div id="wrapSignIn">
    <?php include_partial('sfGuardAuth/signinForm', array('bankname'=>$bankname,));?>

  </div>
  <div class="clear"></div>
</div>
<div>
  <div id="midBottom">
    <div class="blockBlue">
      <div>
        <h2>Features</h2>
      </div>
      <div class="blockBlueContainer">
        <div class="HFeatures">
          <h1>Secure &amp; Convenient Way to Pay</h1>
        </div>
        <div class="HFeatures">
          <h1>Use one single, Trusted Platform</h1>
        </div>
        <div class="HFeatures">
          <h1>Keep in touch with your Payment History</h1>
        </div>
        <div class="HFeatures">
          <h1>Find Services by just clicking a Button</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="blockRed">
    <div>
      <h4>Services</h4>
    </div>
    <div class="blockBlueContainer">
      <div class="HServices">
        <h1>Electronic Bill Presentment &amp; Payment</h1>
      </div>
      <div class="HServices">
        <h1>Generate and prepare Bills</h1>
      </div>
      <div class="HServices">
        <h1>Get Prompt Payment Confirmation</h1>
      </div>
    </div>
  </div>
  <div class="blockBlue2">
    <div>
      <h2 class="homepage_top">Frequently Asked Questions</h2>
    </div>
    <div class="blockBlueContainer">
      <div class="HFaqs">
        <h1><?php echo link_to('Why use Pay4me ?','cms/faq/#1');?></h1>
      </div>
      <div class="HFaqs">
        <h1><?php echo link_to('What Can I do with Pay4Me?','cms/faq/#2');?></h1>

      </div>
      <div class="HFaqs">
        <h1><?php echo link_to('How can I Sign into Pay4Me?','cms/faq/#3');?></h1>

      </div>
      <div class="HFaqs">
        <h1><?php echo link_to('I want to select a Service','cms/faq/#4');?></h1>

      </div>
      <div class="HFaqs">
        <h1><?php echo link_to('How can I process payment for applicant?','cms/faq/#5');?></h1>

      </div>
      <div class="HFaqs">
        <h1><?php echo link_to('I cannot sign into Pay4Me','cms/faq/#6');?></h1>
      </div>
      <p> &nbsp;</p><br/>
    </div>
  </div>
  <div class="clear"></div>
</div>

