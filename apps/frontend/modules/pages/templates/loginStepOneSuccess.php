<?php use_stylesheet('bankLogin');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Admin:</title>

    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>

    <!--[if IE 6]>
<?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
  </head>
  <body>

    <?php //use_helper('Form');?>
    <div id="wrapper-login" class="wrapLogin">
      <div class="loginLogo">
        <?php echo image_tag('/images/'.$bankname.'.jpg',array('title'=>"{$bankname}", 'alt'=>"{$bankname}",'width'=>'250px','height'=>'50px'));?>
      </div>

      <?php
      //echo sfContext::getInstance()->getUser()->getFlash('error');
      if($sf_user->hasFlash('error')) {
        echo "<div id='flash_error'><span>".sfContext::getInstance()->getUser()->getFlash('error')."</span></div>";
      }else if($sf_user->hasFlash('notice'))
      echo "<div id='flash_notice'><span>".sfContext::getInstance()->getUser()->getFlash('notice')."</span></div>";
      ?>

      <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form">
        <div>
          <input type="hidden" name="bankName" value="<?php echo $bankname; ?>" class="loginFieldInput"  />
          <input type="hidden" name="txnId" value="<?php echo $sf_request->getParameter('txnId'); ?>" class="loginFieldInput"  />

          <div class="loginField">
            <label for="username">Username</label>
            <input type="text" name="signin[username]" size="34" class="loginFieldInput"  />
          </div>

          <!--  <div class="loginField">
<label for="username">Username</label>
<input type="text" name="signin[username]" size="34" class="loginFieldInput"  />
</div>
<div class="loginField">
<label for="username2">Password</label>
<input type="password" name="signin[password]" size="34" class="loginFieldInput"  />
<input type="hidden" name="bankname" value="<?php echo $bankname;?>">

</div>
          -->
          <div class="login-ffield">
            <input type="submit" value="Continue" id="button" class="formSubmit" name="button"/>
          </div>
        </div>
      </form>

      <div class="loginPoweredby">
        <div id="poweredLogin"><a href="#">
        <?php echo image_tag("/img/poweredby.gif",array('alt'=>"Powered by pay4me")); ?></a></div>
      </div>
      <p> </p>
    </div>


  </body>

</html>