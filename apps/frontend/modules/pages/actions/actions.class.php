<?php

/**
 * Pages actions.
 *
 * @package    symfony
 * @subpackage Pages
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class PagesActions extends sfActions {
  public function executeIndex(sfWebRequest $request) {
    $this->bankname = '';
    if($request->getParameter('bankName')){
      $this->bankname = $request->getParameter('bankName');
    }
    else if ($this->getUser()->getAssociatedBankAcronym()){
      $this->bankname = $this->getUser()->getAssociatedBankAcronym() ;
    }

    $pageName = $request->getParameter('p');

    if(isset($pageName) && !empty($pageName) && $pageName !='welcome') {
      $this->setTemplate($pageName);
    }else {
      $this->setTemplate('welcome');

    }

    if(!empty($this->bankname)) {
      if($this->getUser()->isAuthenticated()) {//if user is already logged in send him to respective index page
        $group_name = $this->getUser()->getGroupNames();


        if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name)) {
          $this->logMessage("Found a valid bank user");
          return $this->redirect('@bankhome');
        }
        else {
          $this->logMessage("Found a valid user");
          return $this->redirect('@adminhome');
        }

      }
      else {

        $this->setTemplate('bankLogin');
        $this->setLayout(false);
      }
    }
    if($this->getUser()->isAuthenticated()) { 
        if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$this->getUser()->getGroupNames())){
           return $this->redirect('@bankhome');
        } if(in_array(sfConfig::get('app_pfm_role_ewallet_user'),$this->getUser()->getGroupNames())){
            //ewwallet users having credientials then redirect to ewalet home else logout user
            if(count($this->getUser()->listCredentials())>0){
                return $this->redirect('@eWallethome');
            }
            else{
                return $this->forward('sfGuardAuth','signout');
            }
        } else{
            return $this->redirect('@adminhome'); 
        }
        
        }


  }
public function executeLoginStepOne(sfWebRequest $request){
     $this->bankname = '';
    if($request->getParameter('bankName')){
      $this->bankname = $request->getParameter('bankName');
    }
    else if ($this->getUser()->getAssociatedBankAcronym()){
      $this->bankname = $this->getUser()->getAssociatedBankAcronym() ;
    }

//    if ($request->isMethod('post')) {
//        $this->forward('sfGuardAuth', 'signin');
//    }
    $this->setLayout(false);
}
   //  public function executeLogin(sfWebRequest $request) {
  //    $this->bankname = $request->getParameter('bankName');
  //    if($this->bankname) {
  //      if($this->getUser()->isAuthenticated()) {//if user is already logged in send him to respective index page
  //        $group_name = $this->getUser()->getGroupNames();
  //
  //
  //        if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name)) {
  //          $this->logMessage("Found a valid bank user");
  //          return $this->redirect('@bankhome');
  //        }
  //        else {
  //          $this->logMessage("Found a valid user");
  //          return $this->redirect('@change_password');
  //        }
  //
  //      }else {
  //
  //        $this->setTemplate('bankLogin');
  //        $this->setLayout(false);
  //      }
  //    }
  //
  //  }

  public function executePrintMe(sfWebRequest $request) {
    $this->setLayout('pay4me_print');
    $this->setTemplate('printMe');

  }

  public function executeErrorUser(sfWebRequest $request) {
    $this->setLayout('pay4me');
  }

  public function executeErrorAdmin(sfWebRequest $request) {

    $this->setLayout('layout_admin');
  }

  public function executeError404(sfWebRequest $request) {
    $this->setLayout('pay4me');
  }

  public function executeErrorInterswitch(sfWebRequest $request) {
    $this->setLayout('layout_admin');
  }

  public function executeAccessError(sfWebRequest $request) {
   
  }
}
