<?php

/**
 * svcBankConf actions.
 *
 * @package    mysfp
 * @subpackage svcBankConf
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class svcBankConfActions extends sfActions
{
  public function executeIndex(sfWebRequest $request) {
        $service_bank_configuration_obj = svcBankConfServiceFactory::getService(svcBankConfServiceFactory::$TYPE_BASE);
        $this->svc_bank_configuration_list = $service_bank_configuration_obj->getAllRecords();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('ServiceBankConfiguration',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->service_bank_configuration_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->service_bank_configuration = Doctrine::getTable('ServiceBankConfiguration')->find($request->getParameter('id'));
    $this->forward404Unless($this->service_bank_configuration);
  }
  
  public function executeNew(sfWebRequest $request)
  {
    $this->form = new ServiceBankConfigurationForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new ServiceBankConfigurationForm();

    $this->processForm($request, $this->form);
    
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($service_bank_configuration = Doctrine::getTable('ServiceBankConfiguration')->find($request->getParameter('id')), sprintf('Object service_bank_configuration does not exist (%s).', $request->getParameter('id')));
    $this->form = new ServiceBankConfigurationForm($service_bank_configuration);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($service_bank_configuration = Doctrine::getTable('ServiceBankConfiguration')->find($request->getParameter('id')), sprintf('Object service_bank_configuration does not exist (%s).', $request->getParameter('id')));
    $this->form = new ServiceBankConfigurationForm($service_bank_configuration);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($service_bank_configuration = Doctrine::getTable('ServiceBankConfiguration')->find($request->getParameter('id')), sprintf('Object service_bank_configuration does not exist (%s).', $request->getParameter('id')));
    $service_bank_configuration->delete();

    $this->redirect('svcBankConf/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
        $service_bank_configuration = $form->save();
        if(($request->getParameter('action')) == "update") {
            $this->getUser()->setFlash('notice', sprintf('Service Bank Configuration details updated successfully'));
        }else if(($request->getParameter('action')) == "create") {
                $this->getUser()->setFlash('notice', sprintf('Service Bank Configuration added successfully'));
        }

        $this->redirect('svcBankConf/index');
    }
  }
}