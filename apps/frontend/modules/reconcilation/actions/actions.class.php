<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class reconcilationActions extends sfActions {

  public function executeGetSwapAccounts(sfWebRequest $request) {
    $this->setLayout(null);
    $msg = "No data found for swap";
    $swap_time = date('Y-m-d H:i:s');
    if($request->hasParameter('schedule_id')) {
      $schedule_id = $request->getParameter('schedule_id');
      if($schedule_id!="") {


      //    $swapObj = Doctrine::getTable('MerchantAcctReconcilationSchedule')->getCurrentSwapSchedule($swap_time);
        $resultObj = Doctrine::getTable('MerchantAcctReconcilationSchedule')->find($schedule_id);//getCurrentSwapSchedule($swap_time);

        //        print "<pre>";
        //        print_r($swapObj);

        if($resultObj) { //if there are any services schedules for swap
          $con = Doctrine_Manager::connection();
          try {
            $con->beginTransaction();
            //   foreach($swapObj as $resultObj) {
            $merchant_id = $resultObj->getMerchantId();
            $merchant_service_id = $resultObj->getMerchantServiceId();
            $payment_mode_option_id = $resultObj->getPaymentModeOptionId();
            $schedule = $resultObj->getSchedule();
            $this->logMessage('Starting reconcilation of Merchant--'.$merchant_id.'--for Merchant Service--'.$merchant_service_id.'--for Payment Mode Option--'.$payment_mode_option_id.'--scheduled--'.$schedule);
            //print 'Starting reconcilation of Merchant--'.$merchant_id.'--for Merchant Service--'.$merchant_service_id.'--scheduled--'.$schedule;exit;
            $batchIds = $this->reconcile($merchant_id, $merchant_service_id,$payment_mode_option_id,$swap_time);
            if((count($batchIds)) > 0) {
              $msg = "Accounts swapped successfully";
              $this->updateSwapDate($resultObj->getId(),$schedule,$resultObj->getNextSwap());
              $batch_ids = implode(",",$batchIds);
              $this->logSwapHistory($batch_ids,$resultObj->getId(),$resultObj->getNextSwap(),$swap_time);
            }
            //  }
            $this->processNibssPayment();
            $con->commit();
            return $this->renderText($msg);
          }
          catch (Exception $e) {
            $con->rollback();
            throw $e;
          }

        }
        else {
          $this->logMessage('No Accounts to Swap -->'.date('Y-m-d H:i:s'));
          return $this->renderText("No accounts scheduled for swap");
        }
      }
    }
    else {
      return $this->renderText("Parameter Schedule Id not available");
    }


  }

  public function reconcile($merchant_id, $merchant_service_id,$payment_mode_option_id,$swap_time) {
    $merchantSerObj = Doctrine::getTable('MerchantService')->find($merchant_service_id);
    $merchant_service = $merchantSerObj->getName();
    $batchIds = array();
    $payeeObj = $this->getPayorAccountDetails($merchant_service_id,$payment_mode_option_id,$swap_time);

    //print "<pre>";
    //print_r($payeeObj);exit;
    if($payeeObj) {


      foreach($payeeObj as $payeeRecordObj) {
      //    $payment_split_array = array();
        $attrCount=0;

        //   $merchant_name = $payeeRecordObj->getMerchantRequest()->getMerchant()->getName();

        $batchObj = new PaymentBatch();
        $from_account_id = $payeeRecordObj->getFromAcct();
        $total_amount = $payeeRecordObj->getTotalAmount();
     //   $total_amount = round($total_amount,0);
        $batchObj->setAccountId($from_account_id);
        $batchObj->setAccountNo($payeeRecordObj->getFromAcctNo());
        $batchObj->setAccountName($payeeRecordObj->getFromAcctName());
        $batchObj->setSortcode($payeeRecordObj->getFromAcctSortcode());
        $fromAcctBankname = $payeeRecordObj->getFromAcctBankname();
        $batchObj->setFromBank($fromAcctBankname);
        $batchObj->setPayor($payeeRecordObj->getFromAcctName());
        $narration = $merchant_id." Collections ".date('dmY');

        $totalNibssAmount = $this->getNibssCharges($total_amount);
        $totalNibssAmount = round($totalNibssAmount,0);
       // $amount_nibbs = sfConfig::get('app_nibss_svc_charge')/100;
      //  $nibssPercent = ($amount_nibbs)/(1+$amount_nibbs);
      //  $totalNibssAmount = $total_amount * $nibssPercent;
      //  $totalNibssAmount  = ceil($totalNibssAmount*100)/100;
     //   $totalNibssAmount = round($totalNibssAmount,2);
        $amount_after_nibss_payment = $total_amount-$totalNibssAmount;

        $payment_details = $this->getPayeeAccountDetails($from_account_id, $merchant_service_id, $payment_mode_option_id,$swap_time) ;

        // populate the passed in attributes
        if($payment_details) {
          $to_payment_account = array();
          $attr_array_virtual = array();
          $attr_array_real = array();
          $i=0;
          foreach ($payment_details as $record) {
          //   print "<br>".$record->getId();
          //create array of the records being reconciled
          //  $payment_split_array[] = $record->getId();
            $amount = $record->getAmount();
            $amount = round($amount,0);
            $payment_flag = $record->getEpMasterAccountTo()->getVirtualAccountConfig()->getPaymentFlag();
            if($payment_flag == 1) {
              $amount = $amount - $totalNibssAmount;
              $nibss_payment_account_no = $record->getEpMasterAccountTo()->getVirtualAccountConfig()->getVirtualAccountId();
            }
            $physical_account_id = $record->getEpMasterAccountTo()->getVirtualAccountConfig()->getPhysicalAccountId();
            $to_account_id = $record->getToAcct();
            $to_payment_account[] = $to_account_id;
            $paymentRecord = $batchObj->PaymentRecord[$attrCount++];
            $paymentRecord->setAccountId($to_account_id);
            $paymentRecord->setAccountNo($record->getToAcctNo());
            $paymentRecord->setAccountName($record->getToAcctName());
            $paymentRecord->setAmount($amount);
            $paymentRecord->setStatus(0);
            $paymentRecord->setPayor($record->getToAcctName());
            $paymentRecord->setSortcode($record->getToAcctSortcode());
            $paymentRecord->setNarration($narration);


            //create arrays of virtual accounts that will be debited
            $attr_array_virtual[$i]['account_id'] =  $to_account_id;
            $attr_array_virtual[$i]['amount'] =  $amount;

            //create arrays of real accounts that will be credited
            $attr_array_real[$i]['account_id'] =  $physical_account_id;
            $attr_array_real[$i]['amount'] =  $amount;

            $i=$i+1;
          }
        }
        $narration_batch = $merchant_id." REMITTANCE ".date('dmY');
        $batchObj->setTotalRecord($attrCount);print $amount_after_nibss_payment;
        $batchObj->setTotalAmount(round($amount_after_nibss_payment,0));
        $batchObj->setStatus(0);
        $batchObj->setNarration($narration_batch);
        $batchObj->save();
        $batchId = $batchObj->getId();
        $this->addNibssChgs($batchId, $totalNibssAmount);

        $this->createNibssRecord($batchId, $totalNibssAmount, $narration);


        $this->updatePaymentSplitStatus($from_account_id,$to_payment_account,$swap_time,$batchId,$merchant_service_id);


        $accountingObj = new pay4meAccounting();
        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$SENT_TO_NIBSS,array('batch_id'=>$batchObj->getId()));
        $is_clear = 0;
        $attrArray = array();
        foreach($attr_array_virtual as $key=>$value) {
          $attr = $accountingObj->getAccountingHolderObj($description, $value['amount'], $value['account_id'], $is_clear, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
          $attrArray[] = $attr;
        }

        foreach($attr_array_real as $key=>$value) {
          $attr = $accountingObj->getAccountingHolderObj($description, $value['amount'], $value['account_id'], $is_clear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
          $attrArray[] = $attr;
        }


        $event = $this->dispatcher->notify(new sfEvent($attrArray, 'epUpdateAccounts'));


        //credit the nibbs virtual_acct
        $attrArrayNibss = array();
        $nibss_virtual_acctid = 1;
        $is_clear = 0;


        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$NIBSS_PAYMENT,array('batch_id'=>$batchId));
        $attr = $accountingObj->getAccountingHolderObj($description, $totalNibssAmount, $nibss_virtual_acctid, $is_clear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
        $attrArrayNibss[] = $attr;

        if(isset($nibss_payment_account_no) && ($nibss_payment_account_no!="")) {

          $attr = $accountingObj->getAccountingHolderObj($description, $totalNibssAmount, $nibss_payment_account_no, $is_clear, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_INDIRECT);
          $attrArrayNibss[] = $attr;
        }

        $event = $this->dispatcher->notify(new sfEvent($attrArrayNibss, 'epUpdateAccounts'));
        $batchIds[] = $batchId;
      // return  $payment_transaction_number = $event->getReturnValue();
      }//return $batchIds;
    }

    //    print "<pre>";
    //print_r($batchIds);exit;
    return $batchIds;
  }

  private function updatePaymentSplitStatus($from_account_id,$to_payment_account,$swap_time,$batch_id,$merchant_service_id) {
    Doctrine::getTable('PaymentSplitDetails')->updatePaymentSplitStatus($from_account_id,$to_payment_account,$swap_time,$batch_id,$merchant_service_id);
  }

  public function createNibssRecord($batchId, $totalNibssAmount, $narration) {
    $paymentRecord=new PaymentRecord();
    //getaccountinfo
    $accountManagerObj = new EpAccountingManager();
    $accountObj = $accountManagerObj->getAccount(1);
    //print "<pre>";print_r($accountObj);exit;
    $paymentRecord->setPaymentBatchId($batchId);
    $paymentRecord->setAccountId($accountObj->getId());
    $paymentRecord->setAccountNo($accountObj->getAccountNumber());
    $paymentRecord->setAccountName($accountObj->getAccountName());
    $paymentRecord->setAmount($totalNibssAmount);
    $paymentRecord->setStatus(0);
    $paymentRecord->setPayor($accountObj->getAccountName());
    $paymentRecord->setSortcode($accountObj->getSortcode());
    $paymentRecord->setNarration($narration);
    $paymentRecord->save();
  }

  public function processNibssPayment() {
  // print "vxcvcx";exit;
    $createObj = new paymentRecordManager();
    $nibssSetupData = $createObj->getNibssSettings();
    $nibssSetupDataInstance = $nibssSetupData[0];

    $filenameInitial = $nibssSetupData[0]['filename'];
    $messageFrom = $nibssSetupData[0]['message_from'];
    $messageTo = $nibssSetupData[0]['message_to'];
    $massageCc = $nibssSetupData[0]['message_cc'];
    $messageSub = $nibssSetupData[0]['message_subject'];
    $payor = $nibssSetupData[0]['payor'];
    $messageFrmName = $nibssSetupData[0]['message_from_name'];
    $serverSettings = $nibssSetupData[0]['outboundserver_address'];
    $portNo = $nibssSetupData[0]['outboundserver_port'];
    $sendUsername = $nibssSetupData[0]['username'];
    $sendPassword = $nibssSetupData[0]['password'];

    //first get the mandate id (currently pass 1 as default)
    $paymentBatches = $this->getBatchId();


    foreach($paymentBatches as $pmtBatch) {


      $mandateId = $pmtBatch['id'];

      //using this mandate id, get the information from NIBSS charges report table
      $getNibssInfo = Doctrine::getTable('NibssChargesReports')->getNibssChgAmount($mandateId);
      if(isset($getNibssInfo) && count($getNibssInfo) > 0) {
        #sfLoader::loadHelpers('ePortal');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal')); 
        $nibssChgToSend = format_amount($getNibssInfo[0]['nibss_charges'],NULL,1);
      }
      else {
        $nibssChgToSend = 0;
      }

      $records = $createObj->getPaymentRecords($mandateId);

      //get all data from the payment record table for this mandate id

      /* Code for creating the csv file, saving it at a location and then updating the table row with the filename starts here */
      //   $current_page = $request->getParameter('page');
      $filename_credit = $filenameInitial."c_".date("Ymd")."_".$mandateId;
      $filename_debit = $filenameInitial."d_".date("Ymd")."_".$mandateId;
      $forSaveFilename = $filename_credit.".csv";
      $forSaveFilenameDebit = $filename_debit.".csv";
      $createObj->updateFilename($mandateId, $forSaveFilename);

      $headerNames = array('id', 'account_no', 'sortcode', 'amount', 'payor','payee', 'narration') ;
      $getValue = $this->renderText(CsvHelper::getCsv($records,$headerNames, $filename_credit));

      /* Code for creating the csv file, saving it at a location and then updating the table row with the filename ends here */
      /*------------------------------------------------XXXXXXXXXXXXXXXXXXXXXXXX-----------------------------------------------------*/

      //Filename_debit
      $records_debit = DOCTRINE::getTable('PaymentBatch')->getDetails($mandateId);


      // $narration = "NIS REMITTANCE";
      $headerNames_debit = array('id', 'from_bank', 'account_no', 'sortcode', 'total_amount', 'narration') ;
      $getValue_debit = $this->renderText(CsvHelper::getCsv($records_debit,$headerNames_debit, $filename_debit));



      //get the file to be attached and sent to NIBSS
      $filePathToSend=sfConfig::get('sf_upload_dir');

      //append file name to the path
      $fileToSend = $filePathToSend."/".$forSaveFilename;


      //  sfLoader::loadHelpers('Asset');
      $filepath_credit = sfConfig::get('sf_web_dir').'/uploads/'.$forSaveFilename;
      $filepath_debit = sfConfig::get('sf_web_dir').'/uploads/'.$forSaveFilenameDebit;


      //      $filepath_credit = _compute_public_path($forSaveFilename, 'uploads','','true');
      //      $filepath_debit = _compute_public_path($forSaveFilenameDebit, 'uploads','','true');
      $mimetype =  mime_content_type($fileToSend);


      //create a job to send the mail to NIBSS
      $subject = "Pay4me Credit Instructions" ;
      $partialName = 'nibbsMailSend' ;
      $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$this->moduleName."/sendEmail", array('mailTo'=>$messageTo,'mailFrom'=>$messageFrom,'subject'=>$subject,'partialName'=>$partialName,'sortcode'=>$pmtBatch['sortcode'],'from_bank'=>$pmtBatch['from_bank'],'account_name'=>$pmtBatch['account_name'],'account_no'=>$pmtBatch['account_no'],'nibssChgToSend'=>$nibssChgToSend,'filepath'=>$filepath_credit,'filepath_debit'=>$filepath_debit,'mimetype'=>$mimetype));
      $this->logMessage("sceduled mail job with id: $taskId", 'debug');
      $this->updateNibssRows($mandateId);

    }

  }

  public function executeSendEmail(sfWebRequest $request) {
  //phpinfo();exit;
    $this->setLayout(null);

    //    $partialName = "nibbsMailSend";
    //    $mailTo = "nibss.switch@gmail.com";
    //    $mailFrom = "payforme.email@gmail.com";
    //    $subject = "Pay4me Credit Instructions";
    //    $nibssChgToSend = "18.22";
    //    $from_bank = "Intercontinental Bank of Nigeria";
    //    $account_name = "NIS IBPLC Bank Collection Account";
    //    $account_no = "32259023";
    //    $sortcode = "1212123";
    //    $filepath = "/var/www/payforme_ewallet/web/uploads/Pay4Mec_20100308_6.csv";
    //    $filepath_debit = "/var/www/payforme_ewallet/web/uploads/Pay4Med_20100308_6.csv";

    //$filepath = "http://localhost/payforme_ewallet/web/uploads/Pay4Mec_20100308_6.csv";
    //$filepath_debit = "http://localhost/payforme_ewallet/web/uploads/Pay4Med_20100308_6.csv";
    $mimetype = "text/plain";

    $partialName = $request->getParameter('partialName');
    $mailTo = $request->getParameter('mailTo');
    $mailFrom = $request->getParameter('mailFrom');
    $subject = $request->getParameter('subject');
    $nibssChgToSend = $request->getParameter('nibssChgToSend');
    $from_bank = $request->getParameter('from_bank');
    $account_name = $request->getParameter('account_name');
    $account_no = $request->getParameter('account_no');
    $sortcode = $request->getParameter('sortcode');
    $filepath = $request->getParameter('filepath');
    $filepath_debit = $request->getParameter('filepath_debit');
    $mimetype = $request->getParameter('mimetype');
    //$createObj = new paymentRecordManager();
    //$records = $createObj->getPaymentRecords($mandateId);

    $sendMailObj = new Mailing() ;
    $mailInfo = $sendMailObj->sendNibssEmail($mailTo,$mailFrom,$subject,$partialName,$nibssChgToSend, $from_bank, $account_name, $account_no, $sortcode, $filepath,$filepath_debit, $mimetype);
    return $this->renderText($mailInfo);

  }


  //function to get the NIbss payment batch records
  private function getBatchId() {
    $batchIds = array();
    $batchIds = Doctrine::getTable('PaymentBatch')->getPaymentBatches();
    return $batchIds;
  }

  //function to update payment_batch and payment_record table
  private function updateNibssRows($mandateId) {
    $createObj = new paymentRecordManager();

    //this line will update the status to 1 in payment_record
    $createObj->updateNibssMailSentData($mandateId);

    //this line will update the status to 1 in payment_batch
    $createObj->updatePaymentBatch($mandateId);
  }



  protected function addNibssChgs($pmtBatchId, $amount) {
  //if(len($pmtBatchId)){ echo "yes"; die; }else{ echo "no"; die; }
    $tab = new NibssChargesReports();
    $tab->setPaymentBatchId($pmtBatchId);
    $tab->setNibssCharges($amount);
    $tab->save();
  }

  public function updateSwapDate($schedule_id, $schedule, $date_from) {
    sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
    if($schedule !== "immediate") {
      $next_swap = getNextSwap($schedule, $date_from);
    }
    else {
      $next_swap = NULL;
    }
    $last_swap = $date_from;

    Doctrine::getTable('MerchantAcctReconcilationSchedule')->updateSwapSchedule($schedule_id, $last_swap, $next_swap);
  }

  public function logSwapHistory($batch_id,$schedule_id,$swap_date,$swap_time) {//print $swap_date;
    $historyObj = new MerchantAcctReconcilationHistory();

    $historyObj->setScheduleId($schedule_id);
    $historyObj->setSwapDate($swap_time);
    $historyObj->setBatchIds($batch_id);
    $historyObj->setCreatedAt($swap_time);
    $historyObj->save();
  }

  public function executeDisplaySwapDetails(sfWebRequest $request) {
    
    $this->err = '';
    $swap_time = date('Y-m-d H:i:s');
    try {
      if($request->hasParameter('id')) {
        $reconciliation_schedule_id = $request->getParameter('id');
        if($reconciliation_schedule_id!="") {
          $resultObj = Doctrine::getTable('MerchantAcctReconcilationSchedule')->find($reconciliation_schedule_id);
          if($resultObj) { //if there are any services schedules for swap
            $merchant_id = $resultObj->getMerchantId();
            $merchant_service_id = $resultObj->getMerchantServiceId();
            $payment_mode_option_id = $resultObj->getPaymentModeOptionId();
            $this->logMessage('Displaying reconcilation result of Merchant--'.$merchant_id.'--for Merchant Service--'.$merchant_service_id);
            $merchantSerObj = Doctrine::getTable('MerchantService')->find($merchant_service_id);
            $this->merchant_service = $merchantSerObj->getName();
            $this->merchant = $merchantSerObj->getMerchant()->getName();
            $paymentModeOptionObj = Doctrine::getTable('PaymentModeOption')->find($payment_mode_option_id);
            $this->payment_mode_option = $paymentModeOptionObj->getName();
            $serviceObj = Doctrine::getTable('ServicePaymentModeOption')->getMerchantServicePaymentOptions($merchant_service_id);
            //  $i = 0;
            $this->allPayorAcct = array();
            $this->allPayeeAcct = array();
            $this->amount_nibss = array();
            $this->debitAcctObj = $this->getPayorAccountDetails($merchant_service_id,$payment_mode_option_id,$swap_time);
            $this->totalNibssAmount = 0;
            if($this->debitAcctObj) {
              foreach($this->debitAcctObj as $resultObject) {
                $from_account_id = $resultObject->getFromAcct();
                $nibssAmount = $this->getNibssCharges($resultObject->getTotalAmount());
                $this->amount_nibss[$from_account_id] = $nibssAmount;
                $this->totalNibssAmount = $this->totalNibssAmount+$nibssAmount;

                $creditAcctObj = $this->getPayeeAccountDetails($from_account_id, $merchant_service_id, $payment_mode_option_id,$swap_time);
                $this->allPayeeAcct[$from_account_id] = $creditAcctObj;
              }
              $accountingManager = new EpAccountingManager();
              $this->nibss_account_details = $accountingManager->getAccount(1);
            }
          }
          else {
            throw new Exception("Reconciliation not scheduled for the requested id!!");
          }
        }
        else {
          throw new Exception("Mandatory request parameter id found empty!!");
        }
      }
      else {
        throw new Exception("Mandatory request parameter id missing!!");
      }

    }
    catch (Exception $e ) {
      echo $e->getMessage();
      $this->err = $e->getMessage();
    }

  }

  public function getPayorAccountDetails($merchant_service_id,$payment_mode_option_id,$swap_time) {
    $payeeObj = Doctrine::getTable('PaymentSplitDetails')->getPayeeAccounts($merchant_service_id, $payment_mode_option_id, $swap_time);
    if($payeeObj) {
      return $payeeObj;
    }
    else {
      throw new Exception ("No data found for Swap");
    }
  }

  public function getPayeeAccountDetails($from_account_id, $merchant_service_id, $payment_mode_option_id,$swap_time) {
    $payment_details = Doctrine::getTable('PaymentSplitDetails')->getConsolidatedPaymentAmount($from_account_id, $merchant_service_id, $payment_mode_option_id,$swap_time);

    // populate the passed in attributes
    if($payment_details) {
      return $payment_details;
    }
    else {
      throw new Exception ("No data found for Swap");
    }
  }

  public function getNibssCharges($total_amount) {
    ini_restore('precision');
    $amount_nibbs = sfConfig::get('app_nibss_svc_charge')/100;
    $nibssPercent = ($amount_nibbs)/(1+$amount_nibbs);
    $nibssAmount = $total_amount * $nibssPercent;
   // $nibssAmount  = ceil($nibssAmount*100)/100;
    return $nibssAmount = round($nibssAmount,2);
  }



 /* public function getReconcilition($merchant_id, $merchant_service_id,$swap_time) {


  //get all PaymentModeOption associated with the MerchantService

    $serviceObj = Doctrine::getTable('ServicePaymentModeOption')->getMerchantServicePaymentOptions($merchant_service_id);
    foreach($serviceObj as $paymentModeObj) {

    //get this for different payment options
      $payment_mode_option_id=$paymentModeObj->getPaymentModeOptionId(); //bank payment

      //get the different total amounts


      $payeeObj = Doctrine::getTable('PaymentSplitDetails')->getPayeeAccounts($merchant_service_id, $payment_mode_option_id, $swap_time);


      if($payeeObj) {
      //        print "<pre>";
      //        print_r($payeeObj);
        foreach($payeeObj as $payeeRecordObj) {
          $merchant_name = $payeeRecordObj->getMerchantRequest()->getMerchant()->getName();

          $from_account_id[] = $payeeRecordObj->getFromAcct();
          $total_amount = $payeeRecordObj->getAmount();
          $total_amount[] = round($total_amount,0);
          $from_acct_name[] = $payeeRecordObj->getFromAcctName();
          $from_acct_no[] = $payeeRecordObj->getFromAcctNo();
          $from_acct_sortcode[] = $payeeRecordObj->getFromAcctSortcode();
          $from_acct_bankname[] = $payeeRecordObj->getFromAcctBankname();


          $amount_nibbs = sfConfig::get('app_nibss_svc_charge')/100;
          $nibssPercent = ($amount_nibbs)/(1+$amount_nibbs);
          $totalNibssAmount = $total_amount * $nibssPercent;
          $totalNibssAmount  = ceil($totalNibssAmount*100)/100;
          $totalNibssAmount[] = round($totalNibssAmount,0);
          $amount_after_nibss_payment[] = $total_amount-$totalNibssAmount;
        }

        $payment_details = Doctrine::getTable('PaymentSplitDetails')->getConsolidatedPaymentAmount($from_account_id, $merchant_service_id, $payment_mode_option_id,$swap_time);

        // populate the passed in attributes
        if($payment_details) {
          $to_payment_account = array();
          //  $total_amount = 0 ;
          $attr_array_virtual = array();
          $attr_array_real = array();
          $i=0;
          foreach ($payment_details as $record) {
          //   print "<br>".$record->getId();
          //create array of the records being reconciled
          //  $payment_split_array[] = $record->getId();
            $amount = $record->getAmount();
            $amount = round($amount,0);
            $payment_flag = $record->getEpMasterAccountTo()->getVirtualAccountConfig()->getPaymentFlag();
            if($payment_flag == 1) {
              $amount = $amount - $totalNibssAmount;
              $nibss_payment_account_no = $record->getEpMasterAccountTo()->getVirtualAccountConfig()->getVirtualAccountId();
            }
            $physical_account_id = $record->getEpMasterAccountTo()->getVirtualAccountConfig()->getPhysicalAccountId();
            $to_account_id = $record->getToAcct();
            $to_payment_account[] = $to_account_id;
            //  $paymentRecord = $batchObj->PaymentRecord[$attrCount++];
            //   $paymentRecord->setAccountId($to_account_id);
            $to_account_no = $record->getToAcctNo();
            $to_account_name = $record->getToAcctName();
            $to_account_sortcode = $record->getToAcctSortcode();
            //$paymentRecord->setNarration($narration);


            //create arrays of virtual accounts that will be debited
            $attr_array_virtual[$i]['account_id'] =  $to_account_id;
            $attr_array_virtual[$i]['amount'] =  $amount;

            //create arrays of real accounts that will be credited
            $attr_array_real[$i]['account_id'] =  $physical_account_id;
            $attr_array_real[$i]['amount'] =  $amount;

            $i=$i+1;
          }
        }
        else {
          throw new Exception ("No data found for Swap");
        }




      }
      else {
        throw new Exception ("No data found for Swap");
      }
    }
  }*/



}

?>
