<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php //use_helper('Form'); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_listinghead('Reconciliation Details'); ?>
<?php if(!$err) {?>

 <table class="tGrid" width="100%">
  <tr>
    <td align="center">
    <div class="wrapTable">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
        <tr>
          <td class="horizontal">Merchant Name</td>
          <td ><?php echo $merchant; ?></td>
        </tr>
        <tr>
          <td class="horizontal">Merchant Service Name</td>
          <td><?php echo $merchant_service; ?></td>
        </tr>
        <tr>
          <td class="horizontal">Payment Mode Option</td>
          <td><?php echo $payment_mode_option; ?></td>
        </tr>
        <tr>
          <td class="horizontal">Total Nibss Charges</td>
          <td><?php echo format_amount($totalNibssAmount) ; ?></td>
        </tr>
      </table>
      </div>
    </td>
  </tr>


          <?php
          
              foreach ($debitAcctObj as $result):

//              print "<pre>";
//              print_r($result->getAmount());exit;
              ?>
<tr><td>&nbsp;</td></tr>
      <tr>
    <td>
      <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
          <thead>
            <tr class="horizontal">
            <th>Account Name</th>
            <th>Entry Type</th>
            <th>Account No.</th>
            <th>Amount</th>
            <th>Bank Name</th>
            <th>SortCode</th>
          </tr>
          </thead>
        <tbody>
  <?php

              $from_acct_id = $result->getFromAcct();
          ?>
          <tr>
            <td align="center"><?php print $result->getFromAcctName();?></td>
            <td align="center">Debit</td>
            <td align="center"><?php print $result->getFromAcctNo();?></td>
            <td align="right"><?php print format_amount($result->getTotalAmount(),0,1); ?></td>
            <td align="center"><?php print $result->getFromAcctBankname();?></td>
            <td align="center"><?php print $result->getFromAcctSortcode();?></td>

          </tr>
         <?php
          foreach ($allPayeeAcct[$from_acct_id] as $result):
         // print $result->getEpMasterAccountFrom();exit;
         
            $amount = format_amount($result->getAmount(),0,1);
              $payment_flag = $result->getEpMasterAccountTo()->getVirtualAccountConfig()->getPaymentFlag();
            if($payment_flag == 1) {
            //  $amount = $amount - $totalNibssAmount;
              $nibss_payment_account_no = $result->getEpMasterAccountTo()->getVirtualAccountConfig()->getVirtualAccountId();
            }

            if($result->getToAcctName() == sfConfig::get('app_pay4me_account_name')) {
              $amount = $amount - format_amount($amount_nibss[$from_acct_id],NULL,1);
            }

          ?>
          <tr>
            <td align="center"><?php print $result->getToAcctName();?></td>
            <td align="center">Credit</td>
            <td align="center"><?php print $result->getToAcctNo();?></td>
            <td align="right"><?php print $amount; ?></td>
            <td align="center"><?php print $result->getToAcctBankname();?></td>
            <td align="center"><?php print $result->getToAcctSortcode();?></td>

          </tr>
          <?php  endforeach; ?>
          
          <tr>
            <td align="center"><?php print $nibss_account_details->getAccountName();?></td>
            <td align="center">Credit</td>
            <td align="center"><?php print $nibss_account_details->getAccountNumber();?></td>
            <td align="right"><?php print format_amount($amount_nibss[$from_acct_id],NULL,1); ?></td>
            <td align="center"><?php  print $nibss_account_details->getBankname();?></td>
            <td align="center"><?php print $nibss_account_details->getSortcode();?></td>

          </tr>


           </tbody>
        <tfoot>
      </table>
      </div>
    </td>
  </tr>
      
          <?php endforeach; ?>


  <tr><td>   <div class="divBlock">
      <center id="multiFormNav">
        <?php echo button_to('Reconcile','',array('class'=>'formSubmit', 'onClick'=>'reconcile()')); ?>
            &nbsp;&nbsp;
        <?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('create_bank_admin/index').'\''));?>
      </center>
    </div></td></tr>
       


</table>
<?php echo form_tag($sf_context->getModuleName().'/getSwapAccounts',' id="pfm_reconcile_form" name="pfm_reconcile_form"');?>
<input type="hidden" name="schedule_id" value="<?php echo $sf_params->get('id');?>">
</form>


<script>

  function reconcile() {
    $('#pfm_reconcile_form').submit();
  }
  </script>

<?php } else {
echo '  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
<tr><td  align="center" class="error">'.$err."</td></tr></table>";
 }?>
</div>