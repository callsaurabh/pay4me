<?php

/**
 * SandboxPaymentActions.
 *
 * @package    symfony
 * @subpackage sandboxPayment
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class sandboxPaymentActions extends sfActions {    

    public function executeBankPayment(sfWebRequest $request){
        if(sfConfig::get('sf_environment') == "sandbox") {
            if($request->getPostParameter('paymentId')){
                $paymentSystemObject = paymentSystemServiceFactory::getService('sandbox');
                $valArray =  $paymentSystemObject->bankPay($request->getPostParameter('paymentId'),$request->getPostParameter('bank'));               
                if(is_array($valArray) && isset($valArray['var_name'])){
                    $this->redirectNotification($valArray);
                    return;
                }
                $this->valArray = $valArray;
            } else{
                $this->errorVal = "Mandatory Parameter payment id missing";
            }
        }else{
            $this->errorVal = "This URL will be open in sanbox inviroment.";
        }
    }

    public function executeAckSlip(sfWebRequest $request) {
        
        $txnArray = $request->getParameter('pfmTransactionDetails');       
        if($request->hasParameter('trans_number')) {
            $this->trans_number = $request->getParameter('trans_number');
        }
        $this->payment_receipt_header = "Payment Receipt";
        if($request->hasParameter('duplicate_payment_receipt')) {
            $this->payment_receipt_header = "Duplicate ".$this->payment_receipt_header;
        }
        $txn_no = $txnArray['pfm_transaction_number'];

        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txn_no);

        //get Param Description; as per Merchant Service
        $ValidationRulesObj = validationRulesServiceFactory::getService(validationRulesServiceFactory::$TYPE_BASE);
        $this->MerchantData = $ValidationRulesObj->getValidationRules($pfmTransactionDetails['merchant_service_id']);

        $postDataArray['txnId'] = $pfmTransactionDetails['pfm_transaction_number'];
        if($this->MerchantData) {
          foreach($this->MerchantData as $key=>$value) {
              $postDataArray[$key] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails'][$key];
          }
        }

        $postDataArray['type'] = "";
        $postDataArray['name'] = $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails']  ['name'];
        $postDataArray['appCharge'] = $pfmTransactionDetails['MerchantRequest']['item_fee'];
        $postDataArray['bankCharge'] = $pfmTransactionDetails['MerchantRequest']['bank_charge'];
        $postDataArray['serviceCharge'] = $pfmTransactionDetails['MerchantRequest']['service_charge'];
        $postDataArray['totalCharge'] = $pfmTransactionDetails['total_amount'];
        $postDataArray['serviceType'] = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
        $postDataArray['validation_number'] = $pfmTransactionDetails['MerchantRequest']['validation_number'];
        $postDataArray['payment_date'] = $pfmTransactionDetails['MerchantRequest']['paid_date'];
        $postDataArray['payment_status_code'] = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];
        $this->isClubbed = $pfmTransactionDetails['MerchantRequest']['MerchantService']['clubbed'];

        $this->postDataArray = $postDataArray;
        $this->setTemplateForReceipt($pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name']);
    }

    public function setTemplateForReceipt($mode) {

        if($mode=='Bank'){
            $this->setTemplate('ackSlip');
        }/*
        else {
            $billObj = billServiceFactory::getService();
            $walletDetails = $billObj->getEwalletAccountValues();
            $this->errorForEwalletAcount = '';
            if($walletDetails==false) {
                $this->errorForEwalletAcount = "Invalid Account Number";
            }
            else {
                $this->accountObj = $walletDetails->getFirst();
                $this->userDetailObj = $walletDetails->getFirst()->getUserDetail()->getFirst();
                $this->accountBalance =$billObj->getEwalletAccountBalance($this->accountObj->getId());
            }
            $this->setTemplate('eWalletPayOnline');
        }*/
    }

    public function redirectNotification($arrayVal){        
        $this->getUser()->setFlash($arrayVal['comments'], $arrayVal['comments_val'], false) ;
        $this->getRequest()->setParameter($arrayVal['var_name'], $arrayVal['var_value']);
        if(isset($arrayVal['var_name_trans']))
            $this->getRequest()->setParameter($arrayVal['var_name_trans'], $arrayVal['var_value_trans']);
        $this->forward('sandboxPayment', 'ackSlip');
        return ;
    }

}
