<?php //use_helper('Form') ; ?>

<div id="printSlip" <?php if(isset($trans_number) && ($trans_number!="")){echo "style='visbility:hidden;height:1px;overflow-y:hidden'"; }?>>
  <div id="paymentModeReceipt" class="onlyPrint">
    <div class="logo"><?php echo image_tag('/images/'.$bank['acronym'].'.jpg',array('alt'=>"{$bank['bank_name']}, {$bankBranch}"));?></div>
    <div class="label"><?php echo $bank['bank_name'] ; ?><br><?php echo $bankBranch ; ?></div>
  </div>

  <?php echo ePortal_pagehead($payment_receipt_header, array('id'=>'dynamicHeading')); ?>
  <div class="wrapForm2">
  <?php echo form_tag('paymentSystem/payOnline',array('name'=>'pfm_nis_details_form','id'=>'pfm_nis_details_form')) ?>


 
    <?php
    echo ePortal_legend('Application Details');
     echo formRowFormatRaw('Validation Number:',$postDataArray['validation_number']);
    echo formRowFormatRaw('Transaction Number:',$postDataArray['txnId']);
    if($MerchantData > 0) {
      foreach($MerchantData as $k=>$v) {
        echo formRowFormatRaw($v.":",$postDataArray[$k]);
      //    echo input_hidden_tag('appId', $nisServiceDetails['MerchantRequest']['MerchantRequestDetails'][$k]);

      }

    }
    echo formRowFormatRaw('Application Type:',$postDataArray['serviceType']);
    ?>
  
    <?php
    echo ePortal_legend('User Details');
    echo formRowFormatRaw('Name:',$postDataArray['name']);
    //        echo formRowFormatRaw('Mobile Number:',$postDataArray['mobNo']);
    //        echo formRowFormatRaw('Email:',$postDataArray['email']);
    ?>
  
    <?php
    echo ePortal_legend('Payment Charges');
    if($isClubbed == 'no'){
    echo formRowFormatRaw('Application Charges:',format_amount($postDataArray['appCharge'],1));
    if(!empty($postDataArray['bankCharge'])){
      echo formRowFormatRaw('Transaction Charges:',format_amount($postDataArray['bankCharge'],1));
    }
    if(!empty($postDataArray['serviceCharge'])){
      echo formRowFormatRaw('Service Charges:',format_amount($postDataArray['serviceCharge'],1));
    }
    }else{
    echo formRowFormatRaw('Application Charges:',format_amount($postDataArray['totalCharge'],1));
    }
    echo formRowFormatRaw('Total Payable Amount:',format_amount($postDataArray['totalCharge'],1));
    echo ePortal_legend('Payment Status');
    if($postDataArray['payment_status_code'] == 0){echo formRowFormatRaw('Status:','Payment Successful');
      echo formRowFormatRaw('Payment Date:',formatDate($postDataArray['payment_date']));}
    else{echo formRowFormatRaw('Status','Payment Pending');}
    
    ?>
  
  <div class="divBlock">
    <center  id="formNav">
    <input type="button" style = "cursor:pointer;" value="Print Receipt" class="formSubmit" onclick="printSlip('default')">
    </center>
  </div>
</form>
</div>
</div>




<script>
  function printSlip(mode){
    
    defHeading = $('#dynamicHeading').html();
    heading = "";
    if(mode =='user'){
      heading +="User Receipt";
    }else if (mode =='bank'){
      heading +="Bank Receipt";
    }else if (mode =='default'){
      heading +="Receipt";
    }
    $('#dynamicHeading').html(heading);
    html = '';
    html += $('#printSlip').html();    
    printMe(html,true);
    $('#dynamicHeading').html(defHeading);


  }
</script>

<?php if(isset($trans_number) && ($trans_number!="")){
   print $msg="Payment has already been made for the item against Transaction Number - ".$trans_number;
    $cancel_url = url_for('admin/bankUser');
?>

<SCRIPT language="JavaScript1.2">
  var msg = "<?php echo $msg;?>";
  var cancel_url = "<?php echo $cancel_url;?>";
function displaypop()
{
    alert(msg) ;
    document.getElementById('printSlip').style.visibility="visible";
    document.getElementById('printSlip').style.height="";
}
</SCRIPT>
<body onLoad="javascript: displaypop()" >
</body>
<?php }?>
