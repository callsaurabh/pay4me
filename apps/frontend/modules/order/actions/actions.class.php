<?php

/**
 *  order actions.
 *
 * @package    mysfp
 * @subpackage  order
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class orderActions extends sfActions {
/**
 * Executes index actionvalidated params
 *
 * @param sfRequest $request A request object
 */
    public $error_parameter = array();
    public function executePayment(sfWebRequest $request) {
        $this->setLayout(NULL);
        $version = $request->getParameter('payprocess') ;
        $pay4MeObj = pay4MeServiceFactory::getService($version) ;
        $returnVal = $pay4MeObj->processOrder($request) ;
        return $this->renderText($returnVal) ;
    }

    public function executeHistory(sfWebRequest $request) {
        $this->setLayout(NULL);
        $version = $request->getParameter('payprocess') ;
        if($version==""){
            $version = 'v1';
        }
        $pay4MeObj = pay4MeServiceFactory::getService($version) ;
        $returnVal = $pay4MeObj->processHistory($request);
        return $this->renderText($returnVal) ;
    }

    public function executeProceed(sfWebRequest $request) {
        $this->getUser()->getAttributeHolder()->remove('openId'); // to unset openId session variables
        $this->getUser()->getAttributeHolder()->remove('merchantRequestId'); // to unset merchantRequestId session variable
        $this->getUser()->getAttributeHolder()->remove('payType'); // to unset payType session variable
        $order = $request->getParameter('order');
        $request->setParameter('order', $order) ;
        $this->forward('paymentProcess', 'Mode') ;
    }

    public function executeCurrency(sfWebRequest $request) {
      $this->err = "";
      try {
        $order = base64_decode($request->getParameter('order'));
        $order_details = explode(":",$order);
        $this->requestId = $order_details[1];
        $version = $order_details[2] ;
        $pay4MeObj = pay4MeServiceFactory::getService($version) ;

        $merchantRequestObj = Doctrine::getTable('MerchantRequest')->find($this->requestId);
        $item_id = $merchantRequestObj->getMerchantItemId();

        $pay_status = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
        if(!$pay_status) {
          throw New Exception ("Payment has already been made for this item");
        }



        $this->currency_for_payment = $pay4MeObj->processRedirectOrderURL($this->requestId) ;
        if(count($this->currency_for_payment) == 1) {
          $postDataArray = array();
          $postDataArray['id'] = $this->requestId;
          foreach($this->currency_for_payment as $currency_id=>$currency_val) {
            $postDataArray['currency_id'] = $currency_id;
            $postDataArray['item_fee'] = $currency_val['amount'];
          }

          Doctrine::getTable('MerchantRequest')->updateMerchantRequest($postDataArray);
          $this->redirect('@order_pay?order='.$request->getParameter('order'));
        }
        else {
          $this->order=$request->getParameter('order');
          $this->setTemplate('currency');
        }
      }
      catch (Exception $e) {
        $this->err = $e->getMessage();
      }
    }
  
    public function executeProceedToPay(sfWebRequest $request) {
      $this->err = "";
      try {
        $postParam = $request->getParameterHolder()->getAll();
        $postDetail = $postParam['order'];        
        $this->redirect('@order_pay?order='.$postDetail);
      }
      catch(Exception $e ) {
        $this->err =  $e->getMessage();
        $this->redirect('@order_payment?order='.$postDetail);
      }
    }
    
     public function executeError(sfWebRequest $request){
         $xmlresponse = unserialize($request->getParameter('xmlresponse'));
         $this->erroCode = $xmlresponse[0];
         $this->erroMsg = base64_decode($xmlresponse[1]);
         
     }
}
