<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<script language="Javascript">

    $(document).ready(function()
    {
        // alert(document.getElementById("employee_bank_branch_id").value);
        var bank_id = "<?php echo $bank_id;?>";
        if(bank_id!="")//in case of edit, bank needs to be selected, in case of new, it will be blank
        {
            document.getElementById("bank_user_bank_id").value = bank_id;
        }
        if(document.getElementById("bank_user_bank_id").value != "")
        {
            var bank_branch_id = "<?php echo $bank_branch_id;?>";
            var bank_id = document.getElementById("bank_user_bank_id").value;
            var module = "bank_user";
            displayBankBranch(bank_id,module,bank_branch_id,'<?php echo url_for('bank_user/BankBranch'); ?>');

        }
        $('#bank_user_bank_id').change(function(){
            var bank_id = this.value;
            var module = "bank_user";
            displayBankBranch(bank_id,module,"",'<?php echo url_for('bank_user/BankBranch'); ?>');
            return false;
        }
    );
    });
</script>

<form action="<?php echo url_for('bank_user/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm multiForm" id="pfm_bank_user_form" name="pfm_bank_user_form">
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>

    <fieldset>
        <?php echo $form ?>
        <div class="XY20">
            <center>

                &nbsp;<?php  echo button_to(__('Cancel'),'',array('onClick'=>'location.href=\''.url_for('bank_user/index').'\''));?>
                <input type="submit" value="<?php echo __("Save"); ?>" />
            </center>
        </div>

    </fieldset>

</form>
