<?php //use_helper('Form'); ?>
<div id="update_msg"></div>
<?php if ($err): ?>
    print "<div id='flash_error' class='error_list'><span>" . $err . "</span></div>";
<?php else: ?>

    <div class="wrapForm2">
        <?php echo ePortal_legend('User Details'); 
        echo form_tag($sf_context->getModuleName() . '/update', 'id="pfm_edit_user_form" name="pfm_edit_user_form"');
        foreach ($user_details as $details):
            include_partial('bankUsersForm', array('form' => $buser_form, 'display' => true, 'edit' => false, 'function' => 'getValue'));
        endforeach;
        ?>
        </form>
    </div>
    <div class="divBlock">
        <center id="multiFormNav">
             <?php echo button_to('Update', '', array('class' => 'formSubmit', 'onClick' => 'validateUserForm("1")')); ?>&nbsp;<?php echo button_to('Cancel', '', array('class' => 'formSubmit', 'onClick' => 'location.href=\'' . url_for('create_bank_branch_user/search') . '\'')); ?>
        </center>
    </div>
</form>
<?php endif;  ?>
<div id="search_results"></div>
<script>
    function validateUserForm(submit_form)
    {
        var err  = 0;
        if($('#bank_branch_id').val() == ""){
            $('#err_bank').html("Please select bank branch");
            err = err+1;
        }else{
            $('#err_bank').html("");
        }
        if(err == 0)
        {
            if(submit_form == 1){
                $.post('update',$("#pfm_edit_user_form").serialize(), 
                function(data){
                   if(data == 'logout'){
                          location.reload();
                  }else{
                    $("#update_msg").html(data);
                  }
                 });
               
            }
        }
    }
</script>