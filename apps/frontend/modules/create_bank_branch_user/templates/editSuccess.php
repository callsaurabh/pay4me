<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<div id="editBankBranchUser">  
    <?php echo ePortal_legend('Edit Bank Branch User'); ?>


    <!--<div id='flash_error' class='error_list' style="visibility:hidden;height:1px;overflow-y:hidden"></div>-->
    <div class="wrapForm2">

        <?php echo form_tag('create_bank_branch_user/modify', 'id="pfm_create_user_form" name="pfm_create_user_form"'); ?>

        <?php include_partial('bankUsersForm', array('form' => $form, 'bankName' => $bankname,
            'bankDomain' => $bankdomain, 'edit' => true, 'display' => false, 'function' => 'render')); ?>




        <div id="bank_user">



        </div>

        <div class="divBlock">
            <center id="multiFormNav">
                <?php echo button_to('Update', '', array('class' => 'formSubmit', 'onClick' => 'validateUserForm("1")')); ?>&nbsp;<?php echo button_to('Cancel', '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('create_bank_branch_user/index') . '\'')); ?>
            </center>
        </div>
        <div id="search_results"></div>
        <!--  </form>-->
    </div>
</div>
<script>
    function validateUserForm(submit_form)
    {

        var err  = 0;
        $(".error").html('');
        if($('#name').val() == "")
        {
            $('#err_name').html("Please enter Name");
            err = err+1;
        }
        else
        {
            $('#err_name').html("");
        }
        if($('#email').val() == "")
        {
            $('#err_email').html("Please enter Email Address");
            err = err+1;
        }
        else
        {
            if(validateEmail($('#email').val()))
            {
               
                $('#err_email').html("Please enter Valid Email Address");
                err = err+1;
            }
            else
            {
                $('#err_email').html("");
            }
            
        }
  

        if($('#address').val() != "")
        {
            if(validateAddress($('#address').val()))
            {
                $('#err_address').html(invalid_addr_msg);
                err = err+1;
            }
            else
            {
                $('#err_address').html("");
            }
        }

        if($('#mobile_no').val() != "")
        {
            if(validatePhone($('#mobile_no').val()))
            {
                $('#err_mobile_no').html("Please enter Valid Mobile number");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }


        if($('#dob_date').val() != ""){
       

            if (compare_dateinYMD($('#dob_date').val())) {
                $('#err_dob').html("");
            }
            else{
                $('#err_dob').html("Date of Birth cannot be future or today's date");
                err = err+1;
            }

        }

        if(err == 0)
        {
            if(submit_form == 1)
            {
                $('#pfm_create_user_form').submit();
            }
        }



    }
    function validateEmail(email) {
        
        var reg = /^([A-Za-z0-9_\-\.])+$/;

        if(reg.test(email) == false) {
            return true;
        }

        return false;
    }

  
</script>
