<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<?php echo ePortal_legend('Search  User'); ?>
<!--<div id="search_err"></div>-->
<div class="wrapForm2">
    <?php echo form_tag($sf_context->getModuleName() . '/display', 'name=search id=search onsubmit=return validateSearchForm()'); ?>

    <dl id='user_search_row'>
        <div class="dsTitle4Fields">Username<font color="red">*</font></div>

        <div class="dsInfo4Fields"><input type="text" name="name" value="<?php echo $name ?>" class="FieldInput"><?php echo "." . $bank_acronym; ?><br/><br/><div id="err_name" class="cRed"></div>
        </div> </dl>

    <div class="divBlock">
        <center id="multiFormNav">
            <?php echo button_to('Search', '', array('class' => 'formSubmit', 'onClick' => 'validateSearchForm()')); ?>
        </center>
    </div>

    <div id="search_results"></div>

</form>
</div>
</div>
<script>
    //function to validate the search panel in create_voucher, vet_voucher and approve_voucher
    function validateSearchForm()
    {
        $('#search_results').html("");
        var err=0;
        var elem = document.search;
        var name  = elem.name.value;

        if(name == "")
        {
            err = err+1;
            $("#err_name").html("Please enter Username");
        }
        else
        {
            $("#err_name").html("");
        }
        if(err == 0)
        {
            $.post('display',$("#search").serialize(), function(data){

                if(data){
                    if(data=="err" || data == "logout"){
                        document.location.reload();
                    }
                    else
                    {
                        if($('.error_list')){
                            $('.error_list').html('');
                            $('.error_list').remove();
                        }
                        $("#search_results").html(data);
                    }
                }});

        }
        return false;
    }
</script>
