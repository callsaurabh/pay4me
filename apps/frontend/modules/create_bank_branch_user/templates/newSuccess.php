<?php //use_helper('Form');
echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<?php
if ($err) {

    print "<div id='flash_error' class='error_list'><span>" . $err . "</span></div>";
} else {
?>
    <div class="wrapForm2">

    <?php echo form_tag($sf_context->getModuleName() . '/new', 'id="pfm_create_user_form" name="pfm_create_user_form"'); ?>

    <?php
    if ($method == "new"):
        echo ePortal_legend(__('Create New Bank Branch User'));
    endIf; ?>
    <?php
    if ($method == "edit"):
        echo ePortal_legend(__('Edit Bank Branch User'));
    endIf; ?>
    <?php include_partial('bankUsersForm', array('form' => $form, 'bankName' => $bankname, 'bankDomain' => $bankdomain, 'edit' => false, 'display' => false, 'function' => 'render')) ?>





    <?php //echo formRowComplete(__('Date of Birth'), input_date_tag('dob', $dob, array('style' => 'width:180px', 'rich' => true, 'readonly' => 'readonly', 'format' => 'dd-MM-yyyy')), '', 'username', 'err_dob', 'username_row'); ?>



    <dl>
        <div class="dsTitle4Fields">&nbsp;</div>
        <div class="dsInfo4Fields"><?php echo button_to(__('Create'), '', array('class' => 'formSubmit', 'onClick' => 'validateUserForm("1")')); ?>&nbsp;<?php echo button_to(__('Cancel'), '', array('class' => 'formCancel', 'onClick' => 'location.href=\'' . url_for('create_bank_branch_user/index') . '\'')); ?></div>
    </dl>


    <div class="clear"></div>

    <div id="search_results"></div>
    </form>
</div>

<?php } ?>

<script>
    function validateUserForm(submit_form)
    {
        $(".error").html('');
        var err  = 0;
        if($('#bank_branch_id').val() == "")
        {
            $('#err_bank').html("Please select Bank Branch");
            err = err+1;
        }
        else
        {
            $('#err_bank').html("");
        }
        if($('#username').val() == "")
        {
            $('#err_username').html("<?php echo __("Please enter Username"); ?>");
            err = err+1;
        }
        else
        {
            $('#err_username').html("");
        }
        if($('#username').val() != ""){
            if(validateString($('#username').val()))
            {
                $('#err_username').html("Please enter Valid Username");
                err = err+1;
            }
            else
            {
                $('#err_username').html("");
            }
        }
        if($('#name').val() == "")
        {
            $('#err_name').html("Please enter Name");
            err = err+1;
        }
        else
        {
            $('#err_name').html("");
        }
        if($('#email').val() == "")
        {
            $('#err_email').html("Please enter Email Address");
            err = err+1;
        }
        else
        {
            if(validateEmail($('#email').val()))
            {
                $('#err_email').html("Please enter Valid Email Address");
                err = err+1;
            }
            else
            {
                $('#err_email').html("");
            }

        }
        


        if($('#address').val() != "")
        {
            if(validateAddress($('#address').val()))
            {
                $('#err_address').html(invalid_addr_msg);
                err = err+1;
            }
            else
            {
                $('#err_address').html("");
            }
        }
        

        if($('#mobile_no').val() != "")
        {
            if(validatePhone($('#mobile_no').val()))
            {
                $('#err_mobile_no').html("Please enter Valid Mobile number");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }

        if($('#dob_date').val() != ""){

            if (compare_date($('#dob_date').val())) {
                
                $('#err_dob').html("");
            }
            else{
                $('#err_dob').html("Date of Birth cannot be future or today's date");
                err = err+1;
            }

        }

        if(err == 0)
        {
            if(submit_form == 1)
            {
                $('#pfm_create_user_form').submit();
            }
        }



    }
    function validateString(str) {
        var reg = /^([A-Za-z0-9])+$/;

        if(reg.test(str) == false) {
            
            return true;
        }

        return false;
    }
    function validateEmail(email) {
        //var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var reg = /^([A-Za-z0-9_\-\.])+$/;
        if(reg.test(email) == false) {
            return true;
        }

        return false;
    }

    function concat_username()
    {
        if($("#bank_id :selected").val()!="")
        {
            //   var sel_val = $("#bank_id :selected").html().toLowerCase();
            var sel_val = $("#bank_id").val();
            // var suffix_val = "."+str_replace(" ","_",sel_val);
            $('#username_suffix').html("."+sel_val);
        }
        else
        {
            $('#username_suffix').html("");
        }

    }


    function validateUserName(){
        
      
        var err = 0
        if($('#bank_branch_id').val() == "")
        {
            $('#err_bank').html("<?php echo __("Please select bank branch"); ?>") ;
            err = err+1;
        }else{
            $('#err_bank').html("");
        }
        if($.trim($('#username').val())=="") {
            
            $('#err_username').html("Please enter Username");
            err = err+1;
        }else {
            $('#err_username').html("");
        }
        if(err == 0)
        {
            ajax_call('err_username', 'ChkUserAvailability', 'pfm_create_user_form');

        }

    }
    
</script>