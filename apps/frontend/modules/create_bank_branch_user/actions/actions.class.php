<?php

/**
 * create_bank_branch_user actions.
 *
 * @package    mysfp
 * @subpackage create_bank_branch_user
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class create_bank_branch_userActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->checkIsSuperAdmin();

        $user = $this->getUser();
        $bank_id = $user->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();



        $this->postDataArray = array();
        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData'] = $request->getPostParameters();
            $_SESSION['pfm']['reportData']['bank_id'] = $bank_id;
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        } else {
            unset($_SESSION['pfm']['reportData']);
            $_SESSION['pfm']['reportData']['bank_id'] = $bank_id;
            $this->postDataArray = $_SESSION['pfm']['reportData'];
        }

        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_bank_branch_user')); //get Group Id; of bank admin
        $groupid = $sfGroupDetails->getFirst()->getId();


        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id, $country_id);
        $this->bank_branch_id = $request->getParameter('bank_branch_id');
        $this->userName = $request->getParameter('user_search');
        $this->form = new searchUserForm($request->getPostParameters(), array('bank_id' => $bank_id, 'country_id' => $country_id, 'value' => $this->userName));
        $this->user_list = Doctrine::getTable('sfGuardUser')->getAllUsers($groupid, $bank_id, $this->userName, $this->bank_branch_id, $country_id);
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardAuth', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeNew(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->err = 0;
        $user = $this->getUser();
        $sfGuardUserId = $user->getGuardUser()->getId();


        $bankobj = $user->getGuardUser()->getBankUser()->getFirst()->getBank();
        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $bank_id = $bankobj->getId();

        $this->bankname = $bankobj->getAcronym();
        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);
        if (!$bankConfigObj) {
            $this->err = "Bank domain is not setup, therefore you cannot create any users";
        } else {

            $this->bankdomain = $bankConfigObj->getfirst()->getDomain();
            $this->method = "new";
            $this->bank_branch_id = "";
            $this->username = "";
            //   $this->b_place="";
            $this->address = "";
            $this->email = "";
            $this->mobile_no = "";
            $this->dob = "";
            $this->name = "";

            $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
            $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id, $country_id);
            $this->form = new CustomBankUserForm(
                            array(),
                            array('bank_id' => $bank_id,
                                'bank_name' => $this->bankname,
                                'edit' => false,
                                'display' => false, 'grp' => 'app_pfm_role_bank_branch_user')
            );
            if ($request->isMethod('post')) {


                $this->bank_branch_id = $bankbranchid = $request->getPostParameter('bank_branch_id');
                $bank_name = $request->getPostParameter('bankname');
                $this->username = $request->getPostParameter('username');

                if (trim($request->getPostParameter('name')) != "")
                    $this->name = $request->getPostParameter('name');
                if (trim($request->getPostParameter('dob_date')) != "")
                    $this->dob = date('Y-m-d', strtotime($request->getPostParameter('dob_date')));
                if (trim($request->getPostParameter('b_place')) != "")
                    $this->b_place = $request->getPostParameter('b_place');
                if (trim($request->getPostParameter('address')) != "")
                    $this->address = $request->getPostParameter('address');
                if (trim($request->getPostParameter('email')) != "")
                    $this->email = $request->getPostParameter('email');
                if (trim($request->getPostParameter('mobile_no')) != "")
                    $this->mobile_no = $request->getPostParameter('mobile_no');


                $username = $request->getPostParameter('username') . "." . $request->getPostParameter('bankname');
                $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
                if ($username_count == 0) {
                    if (!$this->userDetailValid($request)) {
                        $this->redirect('create_bank_branch_user/new');
                    }
                    //saving user data
                    $sfUser = $this->saveDataOfBankUser($request, $username,
                                    $request->getParameter('grp'),
                                    $bank_id, $country_id, $bankbranchid);


                    $this->doAuditingForBankUserCreate($sfUser->getUsername(), $sfUser->getId());

                    //sending mail to user
                    $this->sendMailToBankUser($this->bankname, $sfUser->getId(), $this->pWord);
                    $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get($request->getParameter('grp')));
                    ////hard coded as of now; is the bank admin
                    $sfGroupId = $sfGroupDetails->getFirst()->getId();

                    $groupDesc = $sfGroupDetails->getFirst()->getDescription();
                    if(sfConfig::get('app_display_password'))
                        $this->getUser()->setFlash('notice', sprintf($groupDesc) . " -" . $sfUser->getUsername() . " created successfully<br> The password is - " . $this->pWord);
                    else
                    $this->getUser()->setFlash('notice', sprintf($groupDesc) . " -" . $sfUser->getUsername() . " created successfully<br> An email has been sent to user regarding the same. ");
                    if ($sfGroupDetails->getFirst()->getName() == sfConfig::get('app_pfm_role_bank_branch_user')) {
                        $this->redirect('create_bank_branch_user/index');
                    } else {
                        $this->redirect('bankBranchReportUser/index');
                    }
                } else {


                    $this->getUser()->setFlash('error', sprintf('Username already exists'));
                    $this->setTemplate('new');
                }
            }
        }
    }

    //move data to model
    private function saveDataOfBankUser($request, $username, $grp, $bank_id, $country_id, $bankbranchid) {
        $sfUser = new sfGuardUser();
        $sfUser->setUsername($username);
        $pass_word = PasswordHelper::generatePassword();
        $this->pWord = $pass_word;
        $sfUser->setPassword($pass_word);
        $sfUser->setIsActive(true);
        $sfUser->save();
        //insert userid and password in EpPasswordPolicy table
        EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(), $sfUser->getPassword());
        $sfUserId = $sfUser->getId();

        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get($grp));
        ////hard coded as of now; is the bank admin
        $sfGroupId = $sfGroupDetails->getFirst()->getId();

        $groupDesc = $sfGroupDetails->getFirst()->getDescription();

        if ($groupDesc == 'Bank User') {

            $groupDesc = 'Bank Branch User';
        }
        $sfGuardUsrGrp = new sfGuardUserGroup();
        $sfGuardUsrGrp->setGroupId($sfGroupId);
        $sfGuardUsrGrp->setUserId($sfUserId);
        $sfGuardUsrGrp->save();

        //Insert into bank user
        $bank_user = new BankUser();
        $bank_user->setUserId($sfUserId);
        $bank_user->setBankId($bank_id);
        $bank_user->setCountryId($country_id);
        $bank_user->setBankBranchId($bankbranchid);
        $bank_user->save();

        //Save User Detail
        $this->saveUserDetail($request, $sfUserId);
        return $sfUser;
    }

    //mail firing for bank user
    private function sendMailToBankUser($bankname, $sfUserId, $pass_word) {
        /* Mail firing code starts */
        $subject = "Welcome to Pay4Me";
        $partialName = 'create_bank_admin/welcome_email';
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $login_url = url_for('@bank_login?bankName=' . $bankname, true);
        $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',
                        "create_bank_admin/sendEmail", array('userid' => $sfUserId,
                    'password' => $pass_word, 'subject' => $subject,
                    'partialName' => $partialName,
                    'login_url' => $login_url));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');

        /* Mail firing code ends */
    }

    private function doAuditingForBankUserCreate($username, $sfUserId) {
        /* Audit Trail code starts */
        $applicationArr = array(new EpAuditEventAttributeHolder(
                    EpAuditEvent::$ATTR_USERINFO,
                    $username,
                    $sfUserId)
        );
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_CREATE,
                        EpAuditEvent::getFomattedMessage(
                                EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_CREATE,
                                array('username' => $username)),
                        $applicationArr);

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        /* Audit Trail code ends */
    }

    public function executeChkUserAvailability(sfWebRequest $request) {

        $this->checkIsSuperAdmin();
        $username = $request->getPostParameter('username') . "." . $request->getPostParameter('bankname');

        //chk if username is available
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
        //  $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);
        if ($username_count == 0) {
            $this->result = "<div class='cGreen'>Username Available</div>";
        } else {
            $this->result = "<div class='cRed'>Username not Available</div>";
        }
    }

    protected function saveUserDetail($request, $userId) {
        $bankobj = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank();

        $country_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $bank_id = $bankobj->getId();

        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);
        $bankdomain = $bankConfigObj->getfirst()->getDomain();

        //$bankdomain = $bankobj->getBankConfiguration()->getfirst()->getDomain();

        $userDetail = new UserDetail();
        $userDetail->setUserId($userId);
        if (trim($request->getPostParameter('name')) != "")
            $userDetail->setName(trim($request->getPostParameter('name')));
        if ($request->getPostParameter('dob_date')) {
            $date = date('Y-m-d', strtotime($request->getPostParameter('dob_date')));
            $userDetail->setDob($date);
        }
        /*
          if(trim($request->getPostParameter('b_place')) != "")
          $userDetail->setBrithPlace(trim($request->getPostParameter('b_place')));
         */
        if (trim($request->getPostParameter('address')) != "")
            $userDetail->setAddress(trim($request->getPostParameter('address')));
        if (trim($request->getPostParameter('email')) != "")
            $userDetail->setEmail(trim($request->getPostParameter('email')) . $bankdomain);
        if (trim($request->getPostParameter('mobile_no')) != "")
            $userDetail->setMobileNo(trim($request->getPostParameter('mobile_no')));
        $userDetail->save();
    }

    protected function userDetailValid($request) {
        $msg = array();
        if (trim($request->getPostParameter('name')) == "") {
            $msg[] = 'Please enter name ';
        }
        if (trim($request->getPostParameter('email')) == "") {
            $msg[] = 'Please enter Email<br />';
        } else if (!preg_match("/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*$/", $request->getPostParameter('email'))) {
            $msg[] = 'Please enter Valid Email<br />';
        }

        if (trim($request->getPostParameter('address')) != "") {
            $len = strlen(trim($request->getPostParameter('address')));
            if ($len > 255) {
                $msg[] = 'Please enter valid address<br />';
            }
        }
        if (count($msg) > 0) {
            $msg = implode($msg, ', ');
            $this->getUser()->setFlash('error', sprintf($msg));
            return false;
        }
        return true;
    }

    public function executeSearch() {
        $this->checkIsSuperAdmin();
        $user = $this->getUser();
        $this->bank_acronym = $user->getGuardUser()->getBankUser()->getFirst()->getBank()->getAcronym();
        $this->name = "";
    }

    public function executeDisplay(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->err = 0;
        $this->bank_acronym = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBank()->getAcronym();
        $this->country_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $this->bank_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
        $this->bank_branch_id = $this->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankBranchId();

        $this->name = $request->getParameter('name');
        $username = $this->name . "." . $this->bank_acronym;
        $username_count = Doctrine::getTable('sfGuardUser')->getUserDetailByUsername($username, $this->bank_id, $this->country_id);
        if ($username_count == 0) {
            $this->getUser()->setFlash('error', 'Invalid User ');
            echo $err = "err";
            $this->renderText("err");
            die;
//            $this->err = "Invalid User";
            //   $this->redirect('create_bank_branch_user/search');
        } else {

            $this->user_details = Doctrine::getTable('sfGuardUser')->fetchUserDetails($username);
            $this->dob = $this->user_details['0']['UserDetail']['0']['dob'];

            //   $this->buser_form = new CustomBankUserForm(null, array('bank_id' => $this->bank_id, 'display' => true,'edit'=>false, 'branch_id' => $this->user_details['0']['BankUser'][0]['BankBranch']['id']), null);
            $this->buser_form = new CustomBankUserForm(array('username' => $username, 'name' => $this->name, 'address' => $this->user_details['0']['UserDetail'][0]['address'], 'mobile_no' => $this->user_details['0']['UserDetail'][0]['mobile_no'], 'email' => $this->user_details['0']['UserDetail'][0]['email'], 'dob' => $this->user_details['0']['UserDetail']['0']['dob']), array('userid' => $this->user_details[0]['id'], 'bank_id' => $this->bank_id, 'display' => true, 'edit' => false, 'branch_id' => $this->user_details['0']['BankUser'][0]['BankBranch']['id']), null);

            $user_id = $this->user_details['0']['UserDetail']['0']['user_id'];
            $user_group_obj = Doctrine::getTable('sfGuardUserGroup')->findByUserId($user_id);
            $group_id = $user_group_obj->getFirst()->getGroupId();
            $group_object = Doctrine::getTable('sfGuardGroup')->findById($group_id);
            $group_name = $group_object->getFirst()->getName();
            if (($group_name == sfConfig::get('app_pfm_role_bank_branch_user')) || ($group_name == sfConfig::get('app_pfm_role_bank_branch_report_user'))) {
                $bank_id = $this->user_details['0']['bank_id'];
                $country_id = $this->user_details['0']['country_id'];
                $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
                $this->bank_branch_array = $bankBranch_obj->getBankBranchList($this->bank_id, $this->country_id);
            } else {
                $this->getUser()->setFlash('error', 'The Bank Branch of searched user can not be changed');
                echo $err = "err";
                $this->renderText("err");
                die;
//                $this->err = "The Bank Branch of searched user can not be changed";
            }
        }
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $this->err = "";
        $userid = $request->getPostParameter('userId');
        if ($userid != "") {

            $user_count = Doctrine::getTable('sfGuardUser')->createQuery('a')->where('id=?', $userid)->count();
            if ($user_count == 1) {
                $bank_branch_id = $request->getPostParameter('bank_branch_id');
                //$this->user_details = Doctrine::getTable('BankUser')->updateUserBankBranch($userid,$bank_branch_id);
                //WP039 make versionable for bank user
                $form = Doctrine_Core::getTable('BankUser')->findOneByUserId($userid);
                $form->setBankBranchId($bank_branch_id);
                $form->save();


                /* Audit Trail code starts */
                $sfGuardUserObj = Doctrine::getTable('sfGuardUser')->find($userid);
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $sfGuardUserObj->getUsername(), $sfGuardUserObj->getId()));
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_BRANCH_CHANGED,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_BRANCH_CHANGED, array('username' => $sfGuardUserObj->getUsername())),
                                $applicationArr);

                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                /* Audit Trail code ends */

                $this->err = 0;
            } else {
                $this->err = 1;
            }
        } else {
            $this->err = 1;
        }

        if ($this->err) {
            $this->msg = "Due to some error could not save the changes.Please try again!!";
        } else {
            $user_group_obj = Doctrine::getTable('sfGuardUserGroup')->findByUserId($userid);
            $group_id = $user_group_obj->getFirst()->getGroupId();
            $group_object = Doctrine::getTable('sfGuardGroup')->findById($group_id);
            $group_name = $group_object->getFirst()->getDescription();
            $branchName = Doctrine::getTable('BankBranch')->getBankBranchNameBYID($bank_branch_id);
            $this->msg = $group_name . " '" . $sfGuardUserObj->getUsername() . "' has been linked to '" . $branchName . "' branch.";
        }
    }

    public function executeDelistUsers(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $userId = $request->getParameter('userId');
        $case = $request->getParameter('use');
        $user_list = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userId);
        $user_active_update = Doctrine::getTable('sfGuardUser')->updateUserActiveById($userId, $case);
        if ($user_active_update == '1') {
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user_list[0]["username"], $userId));

            if ($case == '0') {


                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_DEACTIVATE,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DEACTIVATE, array('username' => $user_list[0]["username"])),
                                $applicationArr);


                $this->getUser()->setFlash('notice', 'User ' . $user_list[0]["username"] . ' Deactivated successfully', true);
            } else if ($case == '1') {
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$CATEGORY_TRANSACTION,
                                EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_ACTIVATE,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_ACTIVATE, array('username' => $user_list[0]["username"])),
                                $applicationArr);
                $this->getUser()->setFlash('notice', 'User ' . $user_list[0]["username"] . ' Activated successfully', true);
            }
            $this->redirect('create_bank_branch_user/index');
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        } else {
            $this->getUser()->setFlash('error', 'your are not authorized user !', true);
            $this->redirect('create_bank_branch_user/index');
        }
    }

    public function executeDelete(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        $request->checkCSRFProtection();
        $this->forward404Unless($user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('userId')), sprintf('Object message does not exist (%s).', $request->getParameter('id')));
        $user->delete();

        /* Audit Trail code starts */
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $user->getUsername(), $user->getId()));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_DELETE,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DELETE, array('username' => $user->getUsername())),
                        $applicationArr);

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        /* Audit Trail code ends */
        $this->getUser()->setFlash('notice', 'User deleted successfully', true);
        $this->redirect('create_bank_branch_user/index');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->checkIsSuperAdmin();
        //$this->method="edit";
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('id')), sprintf('Object bank_admin does not exist (%s).', $request->getParameter('id')));
        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BANK);

        $user = $this->getUser();
        $sfGuardUserId = $user->getGuardUser()->getId();
        $bankobj = $user->getGuardUser()->getBankUser()->getFirst()->getBank();
        $bank_id = $bankobj->getId();
        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $this->bankname = $bankobj->getAcronym();
        $this->bankdomain = $bankobj->getBankConfiguration()->getfirst()->getDomain();

        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        $this->bank_branch_array = $bankBranch_obj->getBankBranchList($bank_id, $country_id);


        // $this->bank_array = $bank_obj->getBankList($request->getParameter('bank_id'));
        $this->username = $sf_guard_user->getUsername();
        $this->bank_branch_id = $sf_guard_user->getBankUser()->getFirst()->getBankBranchId();
        $this->bank_id = $sf_guard_user->getBankUser()->getFirst()->getBank()->getId();
        $this->bank_acronym = $sf_guard_user->getBankUser()->getFirst()->getBank()->getAcronym();
        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);
        $this->bank_domain = $bankConfigObj->getfirst()->getDomain(); //$sf_guard_user->getBankUser()->getFirst()->getBank()->getBankConfiguration()->getfirst()->getDomain();
        if ($sf_guard_user->getUserDetail()->getFirst() == '') {
            $this->name = '';
            $this->dob = '';
            $this->b_place = '';
            $this->address = '';
            $this->email = '';
            $this->mobile_no = '';
        } else {
            $this->name = $sf_guard_user->getUserDetail()->getFirst()->getName();
            $this->dob = $sf_guard_user->getUserDetail()->getFirst()->getDob();
            //  $this->b_place = $sf_guard_user->getUserDetail()->getFirst()->getBrithPlace();
            $this->address = $sf_guard_user->getUserDetail()->getFirst()->getAddress();
            $this->mobile_no = $sf_guard_user->getUserDetail()->getFirst()->getMobileNo();
            $email_address = explode('@', $sf_guard_user->getUserDetail()->getFirst()->getEmail());
            $this->email = $email_address[0];
        }
        $this->userId = $request->getParameter('id');

        $this->form = new CustomBankUserForm(array('username' => $sf_guard_user->getUsername(),
                    'name' => $this->name, 'address' => $this->address,
                    'mobile_no' => $this->mobile_no, 'email' => $this->email, 'dob' => $this->dob),
                        array('userid' => $this->userId, 'bank_id' => $bank_id, 'bank_name' => $this->bankname, 'edit' => true,
                            'branch_id' => $this->bank_branch_id), null);


        $this->setTemplate('edit');
    }

    public function executeModify(sfWebRequest $request) {

        $this->checkIsSuperAdmin();
        $this->forward404Unless($request->isMethod('post'));
        $userId = $request->getPostParameter('userId');


        $sf_guard_user_obj = Doctrine::getTable('sfGuardUser')->find($userId);
        $user = $this->getUser();
        $bankobj = $user->getGuardUser()->getBankUser()->getFirst()->getBank();
        $country_id = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();
        $bank_id = $bankobj->getId();
        $this->bankname = $bankobj->getAcronym();
        $this->bankdomain = $bankobj->getBankConfiguration()->getfirst()->getDomain();


        $bankConfigObj = Doctrine::getTable('BankConfiguration')->getBankCountry($bank_id, $country_id);
        $bank_domain = $bankConfigObj->getfirst()->getDomain(); //$sf_guard_user_obj->getBankUser()->getFirst()->getBank()->getBankConfiguration()->getfirst()->getDomain();

        $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $userDetailId = "";
        if ($userDetailObj->count()) {
            $userDetailId = $userDetailObj->getFirst()->getId();
        }
        if ($userDetailId != "") {
            $user_detail = Doctrine::getTable('UserDetail')->find(array($userDetailId));
        } else {
            $user_detail = new UserDetail();
        }
        $user_detail->setUserId($userId);

        $sfGuardUserGroupObj = Doctrine::getTable('sfGuardUserGroup')->findBy('user_id', $userId)->getFirst()->getGroupId();
        switch ($sfGuardUserGroupObj) {
            case 3:
                $module = 'create_bank_branch_user';
                break;
            case 9:
                $module = 'bankBranchReportUser';
                break;
        }
        $array_of_input_values = array('userId' => $userId, 'bank_branch_id' => $sf_guard_user_obj->getBankUser()->getFirst()->getBankBranchId(), '_csrf_token' => $request->getParameter('_csrf_token'), 'username' => $request->getParameter('username'),
            'name' => $request->getParameter('name'), 'address' => $request->getParameter('address'),
            'mobile_no' => $request->getParameter('mobile_no'), 'email' => $request->getParameter('email'), 'dob' => $request->getParameter('dob_date'));
        $this->form = new CustomBankUserForm($array_of_input_values,
                        array('userid' => $userId, 'country_id' => $country_id, 'bank_id' => $bank_id, 'edit' => true, 'display' => false, 'branch_id' => $sf_guard_user_obj->getBankUser()->getFirst()->getBankBranchId()), $request->getParameter('_csrf_token'));

        $this->form->bind($array_of_input_values);
        if ($this->form->isValid()) {


            $user_detail->setName(trim($request->getPostParameter('name')));
            if (($request->getPostParameter('dob_date')) && ($request->getPostParameter('dob_date') != "")) {
                $user_detail->setDob(date('Y-m-d', strtotime(trim($request->getPostParameter('dob_date')))));
            }
            //  $user_detail->setBrithPlace($request->getPostParameter('b_place'));
            $user_detail->setAddress(trim($request->getPostParameter('address')));
            $user_detail->setEmail(trim($request->getPostParameter('email')) . $bank_domain);
            $user_detail->setMobileNo(trim($request->getPostParameter('mobile_no')));
            $user_detail->save();

            /* Audit Trail code starts */

            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $sf_guard_user_obj->getUsername(), $sf_guard_user_obj->getId()));
            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_TRANSACTION,
                            EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_UPDATE,
                            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_UPDATE, array('username' => $sf_guard_user_obj->getUsername())),
                            $applicationArr);

            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            /* Audit Trail code ends */

            $this->getUser()->setFlash('notice', sprintf('User Profile updated successfully'));

            $this->redirect($module . '/index');
        } else {



            $this->setTemplate('edit');
        }
    }

    public function checkIsSuperAdmin() {
        if ($this->getUser() && $this->getUser()->isAuthenticated()) {
            $group_name = $this->getUser()->getGroupNames();
            if (in_array(sfConfig::get('app_pfm_role_admin'), $group_name)) {
                $this->redirect('@adminhome');
            }
        }
    }

    public function executeSetUserStatus(sfWebRequest $request) {


        $this->status = $request->getParameter('status');
        $this->user_id = $request->getParameter('user_id');
        $this->success = 1;

        $user_status = Doctrine::getTable('UserDetail')->updateStatus($this->status, $this->user_id);
        $userObj = Doctrine::getTable('sfGuardUser')->find($this->user_id);
        $group = Doctrine::getTable('sfGuardUserGroup')->findByUserId($this->user_id);
        $groupid = $group->getFirst()->getGroupId();
        $groupName = $group->getFirst()->getSfGuardGroup()->getName();
        $user = $userObj->getUsername();
        $username = $user;
        if ($this->status == 2) {

            $this->getUser()->setFlash('notice', 'User suspended successfully', true);

            $username = $user;
            $sessionObj = new EpDBSessionDetail();
            $user_del = array();
            $user_del = array('sigin_user_name' => $username);
            $var = $sessionObj->isSessionAllreadyCreatedForUsername($username);
            if (count($var)) {
                $sessionOut = $sessionObj->deleteSessionData(array($user_del));
            }
            $subcategory = EpAuditEvent::$SUBCATEGORY_SECURITY_SUSPENDED_USER;
            if ($groupName == sfConfig::get('app_pfm_role_bank_country_head'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_COUNTRY_HEAD_SUSPENDED;
            elseif ($groupName == sfConfig::get('app_pfm_role_country_report_user'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_REPORT_COUNTRY_ADMIN_SUSPENDED;
            elseif ($groupName == sfConfig::get('app_pfm_role_bank_branch_report_user'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_BRANCH_REPORT_USER_SUSPENDED;
            elseif ($groupName == sfConfig::get('app_pfm_role_bank_admin'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_ADMIN_SUSPENDED;
            elseif ($groupName == sfConfig::get('app_pfm_role_bank_e_auditor'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_EAUDITOR_SUSPENDED;
            elseif ($groupName == sfConfig::get('app_pfm_role_report_bank_admin'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_REPORT_BANK_ADMIN_SUSPENDED;
            else
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_SUSPENDED;
        }
        elseif ($this->status == 1) {
            $this->getUser()->setFlash('notice', 'User resumed successfully', true);
            $subcategory = EpAuditEvent::$SUBCATEGORY_SECURITY_RESUMED_USER;
            if ($groupName == sfConfig::get('app_pfm_role_bank_country_head'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_COUNTRY_HEAD_RESUMED;
            elseif ($groupName == sfConfig::get('app_pfm_role_country_report_user'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_REPORT_COUNTRY_ADMIN_RESUMED;
            elseif ($groupName == sfConfig::get('app_pfm_role_bank_branch_report_user'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_BRANCH_REPORT_USER_RESUMED;
            elseif ($groupName == sfConfig::get('app_pfm_role_bank_admin'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_ADMIN_RESUMED;
            elseif ($groupName == sfConfig::get('app_pfm_role_bank_e_auditor'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_EAUDITOR_RESUMED;
            elseif ($groupName == sfConfig::get('app_pfm_role_report_bank_admin'))
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_REPORT_BANK_ADMIN_RESUMED;
            else
                $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_RESUMED;
        }
        elseif ($this->status == 3) {
            $this->getUser()->setFlash('notice', 'User De-Activated successfully', true);

            $sessionObj = new EpDBSessionDetail();
            $user_del = array();
            $user_del = array('sigin_user_name' => $username);
            $var = $sessionObj->isSessionAllreadyCreatedForUsername($username);
            if (count($var)) {
                $sessionOut = $sessionObj->deleteSessionData(array($user_del));
                $subcategory = EpAuditEvent::$SUBCATEGORY_SECURITY_DEACTIVATED_USER;
                if ($groupName == sfConfig::get('app_pfm_role_bank_country_head'))
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_COUNTRY_HEAD_DEACTIVATED;
                elseif ($groupName == sfConfig::get('app_pfm_role_country_report_user'))
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_REPORT_COUNTRY_ADMIN_DEACTIVATED;
                elseif ($groupName == sfConfig::get('app_pfm_role_bank_branch_report_user'))
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_BRANCH_REPORT_USER_DEACTIVATED;
                elseif ($groupName == sfConfig::get('app_pfm_role_bank_admin'))
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_ADMIN_DEACTIVATED;
                elseif ($groupName == sfConfig::get('app_pfm_role_bank_e_auditor'))
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_BANK_EAUDITOR_DEACTIVATED;
                elseif ($groupName == sfConfig::get('app_pfm_role_report_bank_admin'))
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_REPORT_BANK_ADMIN_DEACTIVATED;
                else
                    $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DEACTIVATED;
            }
        }

        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $username, $this->user_id));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_SECURITY,
                        $subcategory,
                        EpAuditEvent::getFomattedMessage($msg, array('username' => $username)),
                        $applicationArr);

        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        if (($request->getParameter('redirect_to') == "bank_user") && ($request->getParameter('backurl') == 'BankUserList')) {
            $this->redirecturl = $request->getParameter('redirect_to') . '/' . $request->getParameter('backurl');
        } else {
            switch ($groupName) {
                case sfConfig::get('app_pfm_role_bank_country_head'):
                    $this->redirecturl = "create_bank_admin/listHead";
                    break;
                case sfConfig::get('app_pfm_role_country_report_user'):
                    $this->redirecturl = "create_bank_admin/listReportCountryAdmin";
                    break;
                case sfConfig::get('app_pfm_role_bank_branch_report_user'):
                    $this->redirecturl = "bankBranchReportUser/index";
                    break;
                case sfConfig::get('app_pfm_role_bank_admin'):
                    $this->redirecturl = "create_bank_admin/index";
                    break;
                case sfConfig::get('app_pfm_role_bank_e_auditor'):
                    $this->redirecturl = "create_bank_admin/listBankEAuditor";
                    break;
                case sfConfig::get('app_pfm_role_report_bank_admin'):
                    $this->redirecturl = "create_bank_admin/listReportBankUser";
                    break;
                default:
                    $this->redirecturl = "create_bank_branch_user/index";
                    break;
            }
        }


        $this->redirect($this->redirecturl);
    }

    public function executeBankBranchUserCsv(sfWebRequest $request) {
        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");
        $this->postDataArray = $_SESSION['pfm']['reportData'];

        // Creating Headers
        $headerNames = array(0 => array('S.No.', 'Username', 'Bank Branch', 'Branch Code', 'Bank'));
        $headers = array('username', 'name', 'branch_code', 'bank_name');


        $user = $this->getUser();
        $this->postDataArray['country_id'] = $user->getGuardUser()->getBankUser()->getFirst()->getCountryId();


        $records = Doctrine::getTable('sfGuardUser')->getUserDetailsForCsv($this->postDataArray);
        $i = 0;
        while ($row = mysql_fetch_array($records)) {
            $counterVal = count($headers);
            for ($index = 0; $index < $counterVal; $index++) {
                $recordsDetails[$i][$headers[$index]] = $row[$headers[$index]];
            }
            $i++;
        }
        if (count($recordsDetails) > 0) {
            $arrList = $this->exportUserList($headerNames, $headers, $recordsDetails);
            $file_array = csvSave::saveExportListFile($arrList, "/report/bankBranchUser/", "bankBranchUser");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "bank_branch_user";
            $this->setTemplate('csv');
        } else {
            $this->redirect_url('index.php/report/rechargeReport');
        }
        $this->setLayout(false);
    }

    public function exportUserList($arrHeader, $headers, $arrList) {
        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $counterVal = count($arrList);
        $counterValHeaders = count($headers);
        for ($k = 0; $k < $counterVal; $k++) {
            $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeaders; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }

}
