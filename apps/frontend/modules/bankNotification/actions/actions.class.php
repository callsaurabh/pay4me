<?php

class bankNotificationActions extends sfActions {

    public function executeTransactions(sfWebRequest $request) {

//       $xmldata = "<transaction-request  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
//   xmlns='http://www.pay4me.com/schema/bankTransactionV1'
//   xsi:schemaLocation='http://www.pay4me.com/schema/bankTransactionV1 bankTransactionV1.xsd'>
//  <start-date>2010-04-01</start-date>
//  <end-date>2010-04-10</end-date>
//  <bank>Intercontinental Bank</bank>
//  <branch-code></branch-code>
//  <user-id></user-id>
//   </transaction-request>";

        $bank_param_name = sfConfig::get('app_bank_url_param_name');
        $bank_code = $request->getParameter($bank_param_name);

        $this->getResponse()->setContentType('text/xml');
        $this->setLayout(NULL);
        $xmldata = $request->getRawBody();
        //log creation
        $this->createLog($xmldata, 'BankTransactionRequest');
        $this->logMessage("Bank Order Number : ");
        $this->logMessage("Bank Order Number : " . $bank_code);
        $bankXmlObj = new bankNotificationXml();
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        try {

            //validating request xml
            $bankXmlObj = new bankNotificationXml();
            $isValid = $bankXmlObj->validateTransRequestXml($xdoc, $xmldata);


            if (!$isValid) // is a Valid P4M XML //isValidMerchantServiceId
                return $this->error(pay4meError::$INVALID_XML); //throw new Exception('Merchant Request - Invalid XML Found');

                $bank_details = Doctrine::getTable('BankPaymentScheduler')->getBankDetails($bank_code); //BankPaymentScheduler::getBankDetails($bank_code) ;

            $isValidBank = Doctrine::getTable('BankPaymentScheduler')->isValidBank($bank_code);
            if (!$isValidBank) {
                return $this->error(pay4meError::$INVALID_BANK, $bank_details['bank_id']);   // throw new Exception ('Merchant Validation Failed');
            }
            $payForMeObj = payForMeServiceFactory::getService('payforme');
            $isBankAuthenticate = $payForMeObj->authenticateBankRequest($request);

            if (!$isBankAuthenticate)
                return $this->error(pay4meError::$AUTHENTICATION_FAILURE, $bank_details['bank_id']); //     throw new Exception("Authentication Failure");

                $requestArr = $bankXmlObj->parseTransRequestXml($xdoc);
            //echo "<pre>"; print_r($requestArr); die;
            $rexponseXml = $bankXmlObj->generateTransResponseXml($requestArr);


            $this->createLog($rexponseXml, 'BankRedirectXml');
            $bankXmlObj->setResponseHeader($bank_details['bank_id']);

            return $this->renderText($rexponseXml);
        } catch (Exception $ex) {
            $this->logMessage("Exception in processing: " . $ex->getMessage(), 'err');
            $this->getResponse()->setStatusCode(400);
            //log creation
            $this->logMessage("Exception in processing: " . $ex->getMessage(), 'err');
            $errorMessage = "Exception in processing: " . $ex->getMessage();
            $this->createLog($xmldata, 'Exception', $errorMessage);
            $bankXmlObj->setResponseHeader($bank_details['bank_id']);
            return $this->renderText($ex->getMessage());
        }
    }

    public function createLog($xmldata, $nameFormate, $exceptionMessage="") {
        /////log generation///
        //log xml
        $pay4meLog = new pay4meLog();
        if ($exceptionMessage != "") {
            $xmldata = $xmldata . "\n" . $exceptionMessage;
        }
        $pay4meLog->createLogData($xmldata, $nameFormate);
    }

    protected function error($error_code, $bank_id="") {
        $errObj = new pay4meError();
        $errorXml = $errObj->error($error_code);
        if ($errorXml) {
            $this->createLog($errorXml, 'ErrorXml');
            if ($bank_id) {
                $bankXmlObj = new bankNotificationXml();
                $bankXmlObj->setResponseHeader($bank_id);
            }
            return $this->renderText($errorXml);
        }
    }

    public function executeBankReportScheduler(sfWebRequest $request) {
        //get the array of the banks
        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BASE);

        $bank_array[''] = 'Please Select Bank';
        $this->bank_list = $bank_array + $bank_obj->getAllBanks();

        $this->time = date('H:i:s');
        $currHr = date('H:i:s');
        $this->end_time = $currHr;
        $this->form = new CustomBankReportSchedulerForm(array(), array('banks_list' => $this->bank_list), array());
    }

    public function executeDisplayBankReportSch(sfWebRequest $request) {
        $bankReport_obj = bankReportSchedulerServiceFactory::getService(bankReportSchedulerServiceFactory::$TYPE_BASE);
        $this->bank_scheduler_list = $bankReport_obj->getAllRecords();

        $this->page = 1;
        if ($request->getGetParameter('page')) {
            $this->page = $request->getGetParameter('page');
        } else {
            $this->page = $request->getPostParameter('page');
        }

        $this->pager = new sfDoctrinePager('BankReportScheduler', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->bank_scheduler_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeSaveResult(sfWebRequest $request) {
        $input_values = array('_csrf_token' => $request->getPostParameter('_csrf_token'), 'start_time' => $request->getPostParameter('start_time'), 'end_time' => $request->getPostParameter('end_time'), 'bank_id' => $request->getPostParameter('bank_id'));
        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BASE);

        $bank_array[''] = 'Please Select Bank';
        $this->bank_list = $bank_array + $bank_obj->getAllBanks();
        $this->form = new CustomBankReportSchedulerForm(array(), array('banks_list' => $this->bank_list), array());

        $this->form->bind($input_values);
        if ($this->form->isValid()) {
            $bankReport_obj = bankReportSchedulerServiceFactory::getService(bankReportSchedulerServiceFactory::$TYPE_BASE);

            //get the information and store in the proper format
            $bankId = $request->getPostParameter('bank_id');
            $stTimeArr = $request->getPostParameter('start_time');
            $enTimeArr = $request->getPostParameter('end_time');

            //format the time from manager
            $stTime = $bankReport_obj->formatTime($stTimeArr);
            $enTime = $bankReport_obj->formatTime($enTimeArr);

            do {
                $bank_code = mt_rand();
                $duplicacy_code = $bankReport_obj->checkDuplicacy($bankId);
            } while ($duplicacy_code > 0);

            $bank_key = mt_rand();
            $auth_info = base64_encode($bank_code . $bank_key);

            $bnkSch = new BankReportScheduler();
            $bnkSch->setBankId($bankId);
            $bnkSch->setStartTime($stTime);
            $bnkSch->setEndTime($enTime);
            $bnkSch->setBankKey($auth_info);
            $bnkSch->setCode($bank_code);
            $bnkSch->save();

            $this->redirect('bankNotification/displayBankReportSch');
        } else {

            $this->setTemplate('bankReportScheduler');
        }
    }

}

?>
