<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php echo ePortal_legend('Bank List'); ?>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
        <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
        <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>

<div class="wrapTable" >
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th width = "2%">S.No.</th>
      <th>Bank Name</th>
      <th>Start Time</th>
      <th>End Time</th>
      <th>Bank Key</th>
      <th>Code</th>      
    </tr>
  </thead>
  <tbody>
   <?php
     if(($pager->getNbResults())>0)
     {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
   ?>
   <tr class="alternateBgColour">
     <td align="center"><?php echo $i ?></td>
     <td><?php echo $result->getBankName() ?></td>
     <td><?php echo $result->getStartTime() ?></td>
     <td><?php echo $result->getEndTime() ?></td>
     <td><?php echo $result->getBankKey() ?></td>
     <td><?php echo $result->getCode() ?></td>     
   </tr>
   <?php endforeach;?>
   <tr>
    <td colspan="6">
      <div class="paging pagingFoot"><?php echo ajax_pager_navigation($pager,url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()),'theMid') ?></div>
    </td>
  </tr>
  <?php }else{ ?>
  <tr><td  align='center' class='error' colspan="6">No Record found</td></tr>
  <?php } ?>
  </tbody>
   <tfoot><tr><td colspan="6"></td></tr></tfoot>
</table>
</div>
<?php  echo button_to('Add New','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('bankNotification/bankReportScheduler').'\''));?>
</div>