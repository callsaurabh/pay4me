<?php //use_helper('DateForm'); ?>
<?php echo ePortal_pagehead(" ", array('class' => '_form')); ?>

<div class="wrapForm2">
    <?php //echo $sf_context->getModuleName(); die(); ?>
    <?php echo ePortal_legend('Create bank Report Scheduler'); ?>
    <?php echo form_tag('bankNotification/saveResult', 'id="report_bank_scheduler" name="report_bank_scheduler"'); ?>
    <dl id='bank_row'>
        <div class="dsTitle4Fields"><?php echo $form['bank_id']->renderLabel(); ?></div>
        <div class="dsInfo4Fields"><?php echo $form['bank_id']->render(); ?>
            
            <br><?php echo $form['bank_id']->renderError(); ?><br><div class="cRed" id="err_bank"></div>
        </div>
    </dl>
    <dl id='start_time_row'>
        <div class="dsTitle4Fields"><?php echo $form['start_time']->renderLabel(); ?></div>
        <div class="dsInfo4Fields"><?php echo $form['start_time']->render(); ?>
            <br><br><div class="cRed" id="err_start_time"></div>
        </div>
    </dl>
    <dl id='end_time_row'>
        <div class="dsTitle4Fields"><?php echo $form['end_time']->renderLabel(); ?></div>
        <div class="dsInfo4Fields"><?php echo $form['end_time']->render(); ?>
            <br><br><div class="cRed" id="err_end_time"></div>
        </div>
    </dl>
    <?php //echo formRowComplete('Select Bank <sup>*</sup>',select_tag('bank_id', options_for_select($bank_list, '', array('include_custom'=>'Please Select Bank'))),'','bank','err_bank','bank_row'); ?>
    <?php //echo formRowComplete('Start Time <sup>*</sup>',select_time_tag('start_time', $time, array('include_second' => true, '12hour_time' => false)), '', 'start_time', 'err_start_time', 'start_time_row'); ?>
    <?php //echo formRowComplete('End Time <sup>*</sup>', select_time_tag('end_time', $end_time, array('include_second' => true, '12hour_time' => false)), '', 'end_time', 'err_end_time', 'end_time_row'); ?>
<?php echo $form['_csrf_token']; ?>
    <dl>
        <div class="dsTitle4Fields">&nbsp;</div>
        <div class="dsInfo4Fields">
    <input type="button" onclick="validateUserForm('1')" value="Create" class="formSubmit">&nbsp;
    <input type="button" onclick="location.href='<?php echo url_for('bankNotification/displayBankReportSch');?>'" value="Cancel" class="formCancel">
  </div>      </dl>


<div class="clear"></div>
</div>

<script>
    function validateUserForm(submit_form)
    {

        var err  = 0;
//        if($('#bank_id').val() == "")
//        {
//            $('#err_bank').html("Please select bank");
//            err = err+1;
//        }
//        else
//        {
//            $('#err_bank').html("");
//        }
    
        if($('#start_time').val() == "")
        {
            $('#err_start_time').html("Please enter start time");
            err = err+1;
        }
        else
        {
            $('#err_start_time').html("");
        }

        if($('#end_time').val() == "")
        {
            $('#err_end_time').html("Please enter end time");
            err = err+1;
        }
        else
        {
            $('#err_end_time').html("");
        }

        if(err == 0)
        {
            if(submit_form == 1)
            {
                $('#report_bank_scheduler').submit();
            }
        }
    }
</script>