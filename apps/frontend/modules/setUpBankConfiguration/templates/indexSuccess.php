<div id="theMid">
    <?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
    <?php use_helper('Pagination');  ?>
    <?php echo ePortal_legend('Search Bank Configurations'); ?>
    <div class="wrapForm2">
        <?php echo form_tag($sf_context->getModuleName().'/index','name=search id=search method=post');?>
        <dl id='bank_row'>
            <div><br/></div>
            <div class="listreport_user_search_row"><?php echo $form?></div>
            <div class="listreport_user_search_row">
                <div id="listreport_user_search_row" class="divBlock">
                    <div class="dsTitle4Fields"></div>
                    <div class="dsInfo4Fields">
                        <input type="submit" class='formSubmit' value="Search" onclick="return userSearch();"/>
                        <div id="err_bank_branch" class="cRed"></div>
            </div>  </div></div>

        </dl>
        </form>
        <div class="clear"></div>
    </div>
    <br />
    <?php if($valid=='valid'){?>
        <?php echo ePortal_listinghead('Bank Configurations List'); ?>

    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr class="alternateBgColour">
                <th width="100%" >
                    <span class="floatLeft">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
                    <span class="floatRight">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
                </th>
            </tr>
        </table>
        <br class="pixbr" />
    </div>

    <div class="wrapTable" >
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <thead>
                <tr class="horizontal">
                    <th>S.No.</th>
                    <th>Bank</th>
                    <th>Country</th>
                    <th>Domain</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(($pager->getNbResults())>0) {
                    $limit = sfConfig::get('app_records_per_page');
                    $page = $sf_context->getRequest()->getParameter('page',0);
                    $i = max(($page-1),0)*$limit ;
                    foreach ($pager->getResults() as $bank_configuration):  $i++;?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $bank_configuration->getBank()->getBankName() ?></td>
                    <td><?php echo $bank_configuration->getCountry()->getName() ?></td>
                    <td><?php echo $bank_configuration->getDomain() ?></td>
                    <td>
                        <?php echo link_to(' ', 'setUpBankConfiguration/edit?id='.$bank_configuration->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
                        <?php echo link_to(' ', 'setUpBankConfiguration/delete?id='.$bank_configuration->getId(), array('method' => 'delete', 'confirm' => 'Are you sure, you want to delete bank domain?', 'class' => 'delInfo', 'title' => 'Delete')) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                <tr><td colspan="5">
                        <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'theMid')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>

                </div></td></tr>
                <?php
            }else { ?>

                <tr><td  align='center' class='error' colspan="5">No Bank Admin Found</td></tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php }?>
<?php  //echo button_to('Add New','',array('class'=>'formSubmit', 'onClick'=>'location.href=\''.url_for('setUpBankConfiguration/new').'\''));?>
</div>