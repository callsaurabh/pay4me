<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $bank_configuration->getId() ?></td>
    </tr>
    <tr>
      <th>Bank:</th>
      <td><?php echo $bank_configuration->getBankId() ?></td>
    </tr>
    <tr>
      <th>Country:</th>
      <td><?php echo $bank_configuration->getCountryId() ?></td>
    </tr>
    <tr>
      <th>Domain:</th>
      <td><?php echo $bank_configuration->getDomain() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $bank_configuration->getCreatedAt() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $bank_configuration->getUpdatedAt() ?></td>
    </tr>
    <tr>
      <th>Deleted at:</th>
      <td><?php echo $bank_configuration->getDeletedAt() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $bank_configuration->getCreatedBy() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $bank_configuration->getUpdatedBy() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('setUpBankConfiguration/edit?id='.$bank_configuration->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('setUpBankConfiguration/index') ?>">List</a>
