<?php

/**
 * setUpBankConfiguration actions.
 *
 * @package    mysfp
 * @subpackage setUpBankConfiguration
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class setUpBankConfigurationActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $this->form = new BankConfigurationListForm();
        $this->valid = 'valid';
        $bank_id = "";
        $params = $request->getParameter($this->form->getName());
        $bank_id = $params['bank_id'];        
        $this->form->setDefault('bank_id', $bank_id);
        $this->bank_configurations = Doctrine::getTable('BankConfiguration')->getBankConfiguration($bank_id);
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('BankConfiguration',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->bank_configurations);
        $this->pager->setPage($this->page);
        $this->pager->init();


    }
    public function executeShow(sfWebRequest $request)
    {
        $this->bank_configuration = Doctrine::getTable('BankConfiguration')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->bank_configuration);
    }

    public function executeNew(sfWebRequest $request)
    {
        $this->form = new BankConfigurationForm();
        if($request->hasParameter('id'))
        {
            $bank_id =$request->getParameter('id');
            $this->form->setDefault('bank_id', $bank_id);
        }
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new BankConfigurationForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->forward404Unless($bank_configuration = Doctrine::getTable('BankConfiguration')->find(array($request->getParameter('id'))), sprintf('Object bank_configuration does not exist (%s).', $request->getParameter('id')));
        if($bank_configuration['domain']!="") {
            $bank_configuration['domain'] = substr($bank_configuration['domain'], 1);//remove the first symbol of '@',for display
        }
        $this->form = new BankConfigurationForm($bank_configuration);
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($bank_configuration = Doctrine::getTable('BankConfiguration')->find(array($request->getParameter('id'))), sprintf('Object bank_configuration does not exist (%s).', $request->getParameter('id')));
        $this->form = new BankConfigurationForm($bank_configuration);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request)
    {
        $request->checkCSRFProtection();

        $this->forward404Unless($bank_configuration = Doctrine::getTable('BankConfiguration')->find(array($request->getParameter('id'))), sprintf('Object bank_configuration does not exist (%s).', $request->getParameter('id')));
        $bank_configuration->delete();
        $this->getUser()->setFlash('notice', sprintf('Bank domain deleted successfully'));
        $this->redirect('setUpBankConfiguration/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
            $data = $request->getParameter($form->getName());
            $domain = $data['domain'];
            $dns_details =  dns_get_record($domain);
            if(count($dns_details) == 0) {
                $this->getUser()->setFlash('error', sprintf('Please enter a valid domain'),false);
                $this->setTemplate('new');
            }else{
                $data['domain'] = '@'.$domain;//concat @ while storing in db
                $form->bind($data);
                $bank_configuration = $form->save();
                if(($request->getParameter('action')) == "update") {
                    $this->getUser()->setFlash('notice', sprintf('Bank domain updated successfully',false));
                }
                else if(($request->getParameter('action')) == "create") {
                    $this->getUser()->setFlash('notice', sprintf('Bank domain added successfully',false));
                }
                $this->redirect('setUpBankConfiguration/index');
            }

        }else{
            echo $form->renderGlobalErrors();
        }

    }
}
