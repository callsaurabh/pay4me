<div class="innerWrapper">
<div id="faqPage" class="cmspage">

    <?php
    echo ePortal_pagehead("We apologise for the inconvenience. The Pay4Me page you've requested is not available at this time. <br /> There are several possible reasons you are unable to reach the page:");
    ?>
    <div class="block-container">
        <div class="HFeatures" style="background-image:none;">
            <p>
                <ul>
                  <li>If you tried to load a bookmarked page, or followed a link from another Web site, it's possible that the URL you've requested has been moved</li>
                  <li>The web address you entered is incorrect</li>
                  <li>Technical difficulties are preventing us from display of the requested page</li>
                </ul>
            <h1>We want to help you to access what you are looking for</h1>
            <ul>
                  <li><?php echo link_to('Visit our  Homepage','@homepage') ?></li>
                  <li>Try returning to the page later</li>
                </ul>
            </p>
        </div>
</div>
</div>
</div>
</div>
