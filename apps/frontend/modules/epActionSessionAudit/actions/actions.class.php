<?php

/**
 * epActionSessonDetail actions.
 *
 * @package    pay4me
 * @subpackage epActionSessonDetail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class epActionSessionAuditActions extends sfActions {
    public function executeIndex(sfWebRequest $request) {
        $this->bank = '';
        $classObj = new auditTrailManager();
        $bankList['']='-- All Banks --';
//        $bankList['']=;
        $bankList = $classObj->getBankList($bankList);
//        array_
        $bankList[0]='-- All Banks --';
//        $bankList=array_merge(array(''=>'-- All Banks --'), $bankList);
        $this->bankList=$bankList;
        
     $this->customForm=new CustomEpActionSessionAuditForm(array('bank'=>''),array('bankList'=>$this->bankList),null);
    }



    public function executeGetBankBranches(sfWebRequest $request) {
        //create an object of the class
        $classObj = new auditTrailManager();
        //get the bank id
        $bankId = $request->getParameter('bank');
        //based on the id get the branch list
        
        $str = '<option value="">-- All Branches --</option>';
        if($bankId != "") {
            $this->branchList = $classObj->getBranchList($bankId);
            $q = $this->branchList;           

            foreach($q as $value) {
                $str .= '<option value="'.$value['id'].'">'.$value['name'].'('.$value['branch_sortcode'].')</option>';
            }
        }

        return $this->renderText($str);
    }

    public function executeDisplayReport(sfWebRequest $request) {

        $this->bank = $request->getParameter('bank');
        $this->branch = $request->getParameter('branch');
        $this->username = $request->getParameter('username');

        //get the user credendtials
        $ignoreUsers = sfConfig::get('app_ep_audit_ignore_users');
       // if(count($ignoreUsers))
        //    print_r($ignoreUsers);
        //die;
        $user = $this->getUser();
        $groupDetails = $user->getGroups()->toArray();
        $userDetails =  array();
        $userDetails['id'] = $user->getGuardUser()->getId();
        $userDetails['username'] = $user->getGuardUser()->getUsername();
        $userDetails['groupId'] = $groupDetails[0]['id'];
        $userDetails['groupName'] = $groupDetails[0]['name'];
        $sessObj = new EpDBSessionDetail();
        $logoutTime = $sessObj->getLogoutTimeFromCurrentTimestamp();
        $sessionId  = $sessObj->getSessionId();
        $classObj = new sessionAuditManager();
        $dataResult = $classObj->getDataForReport( $userDetails, $this->bank, $this->branch,  $this->username,$logoutTime,$sessionId,$ignoreUsers);

        //this below mentioned code is for executing the sql recd & get the data in paging order
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('epActionSessionAudit',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($dataResult);
        $this->pager->setPage($this->getRequestParameter('page',$this->page));
        $this->pager->init();
    }

    public function executeLogoutUserForcely(sfWebRequest $request) {
        $username = $request->getParameter('username');
        $sessionId = $request->getParameter('sessionId');
        $sessObj = new EpDBSessionDetail();
        $sessObj->doCleanUpSessionByUser($sessionId);
        $this->getUser()->setFlash('notice', 'User Session has been successfully killed.', false) ;
        $this->redirect('epActionSessionAudit/index');
    }
}
