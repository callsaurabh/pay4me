<?php //use_helper('Form') ?>

<script>
 //document.getElementById('username').value='';
  $(document).ready(function()
 {
     $("#check1").show();
     $("#check2").show();
     $("input[name='selectBy']").change(function(){

      if ($("input[name='selectBy']:checked").val() == 'bank_branch'){

          $("#check1").show();
          $("#check2").show();
          $("#user_name").hide();
          document.getElementById('username').value='';
      }else if ($("input[name='selectBy']:checked").val() == 'username'){


          $("#user_name").show();
            $("#check1").hide();
            $("#check2").hide();

      }
    });



  $("#bank").change(function()
  {
    var bank = $(this).val();

    //bank = bank.toString();
    var url = "<?php echo url_for("epActionSessionAudit/getBankBranches");?>";

    $('#loader').show();
    $("#branch").load(url, {
      bank: bank
    },function (data){
       $('#loader').hide();
    });
  });

});


</script>

<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
 <div class="wrapForm2">
 <?php echo ePortal_legend(__('Search User Session Detail')); ?>

 <?php echo form_tag('epActionSessionAudit/displayReport') ?>
<?php  include_partial('indexForm', array('form' => $customForm)); ?>
 
  <?php
    //echo formRowFormatRaw(__('Search By Bank/Branch'),radiobutton_tag('selectBy', 'bank_branch', true)); echo "<br />";
    //echo formRowFormatRaw(__('Search By Username'),radiobutton_tag('selectBy', 'username', false)); echo "<br />";
  ?>
<!--  <div style="display:none" id="check1">-->
  <?php
    //echo formRowFormatRaw(__('Bank'), select_tag('bank',options_for_select($bankList, $bank,array('include_custom' => __('-- All Banks --'),'option'=>array('All'=>'All')))));
   ?>
<!--  </div>-->
<!--  <div style="display:none" id="check2">-->
  <?php  //$Branch_array = array();
    //echo formRowFormatRaw(__('Branch'), select_tag('branch', options_for_select($Branch_array, null, array('include_custom'=>__('-- All Branches --')))));
   ?>
<!--  </div>-->
<!--  <div style="display:none" id="user_name">
  <?php
    //echo formRowFormatRaw(__('Username'),  input_tag("username", '', "class=FieldInput maxlength=20"));
   ?>
  </div>-->
  <div class="divBlock">
    <center>
      <input class="formSubmit" type="submit" value="Search" name="commit">
    </center>
  </div>
  </form>
</div>
</div>
<script language="Javascript">
  var bank= "<?php echo $bank;?>";
  if(bank != ""){display_options();}

</script> 