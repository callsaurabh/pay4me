<?php use_helper('Pagination'); ?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php echo ePortal_listinghead(__('Portal Login User Detail')); ?>


<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr class="alternateBgColour">
      <th width="100%" >
         <span class="floatLeft"><?php echo __('Found')?> <b><?php echo $pager->getNbResults(); ?></b> <?php echo __('results matching your criteria.')?></span>
         <span class="floatRight"><?php echo __('Showing')?> <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> <?php echo __('of total')?>  <b><?php echo $pager->getNbResults(); ?></b>  <?php echo __('results')?></span>
      </th>
    </tr>
  </table>
  <br class="pixbr" />
</div>

<div class="wrapTable">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
  <thead>
    <tr class="horizontal">
      <th>S.No.</th>
      <th><?php echo __('Username'); ?></th>
      <th><?php echo __('Ip address');?></th>
      <th><?php echo __('BankName') ?></th>
      <th><?php echo __('BranchName') ?></th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
     if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;

    ?>
    <tr class="alternateBgColour">
      <td align="center"><?php echo $i ?></td>
      <td align="center"><?php echo $result->getSiginUserName() ?></td>
      <td align="center"><?php echo $result->getUserIpAddress() ?></td>
      <td align="center"><?php echo ($result->getBankName()!='')?$result->getBankName():'--'?></td>
       <td align="center"><?php echo ($result->getBankBranchName()!='')?$result->getBankBranchName().'('.$result->getBankBranchSortcode().')':'--'?></td>

       <td align="center"><?php echo link_to(image_tag('/images/user_logout.jpeg',array('title'=>'Logout Forcefully', 'alt'=>'Logout Forcefully','width'=>'20px','height'=>'20px')), 'epActionSessionAudit/logoutUserForcely?username='.$result->getSiginUserName().'&sessionId='.$result->getSessId(),array( 'confirm' => 'Are you sure, you want to logout forcefully?')); ?></td>

    </tr>
    <?php endforeach; ?>
    <?php
      $url="";
      if($bank != ""){ $url .= "bank=".$bank."&"; }
      if($branch != ""){ $url .= "branch=".$branch."&"; }
      if($username != ""){ $url .= "username=".$username; }
    ?>
    <tr>
      <td colspan="10">
        <div class="paging pagingFoot"><?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?'.$url) ?></div>
      </td>
    </tr>
    <?php }else{ ?><table border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr class="alternateBgColour"><td class='error' colspan="11"><?php echo "No Record Found"; ?></td></tr></table><?php } ?>
  </tbody>
  <tfoot><tr><td colspan="10"><div align="center"><?php echo button_to(__('Back To Search'), 'epActionSessionAudit/index', array('class'=>'formSubmit')) ?></div></td></tr></tfoot>
</table>
</div>