<?php

/**
 * service_payment_mode_option actions.
 *
 * @package    mysfp
 * @subpackage service_payment_mode_option
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class service_payment_mode_optionActions extends sfActions {
  public function executeIndex(sfWebRequest $request) {
    $service_payment_mode_option_obj = servicePaymentModeOptionServiceFactory::getService(servicePaymentModeOptionServiceFactory::$TYPE_BASE);
    $this->service_payment_mode_option_list = $service_payment_mode_option_obj->getAllRecords();

    $this->page = 1;
    if($request->hasParameter('page')) {
      $this->page = $request->getParameter('page');
    }
    $this->pager = new sfDoctrinePager('ServicePaymentModeOption',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($this->service_payment_mode_option_list);
    $this->pager->setPage($this->page);
    $this->pager->init();
  }

  public function executeShow(sfWebRequest $request) {
    $this->service_payment_mode_option = Doctrine::getTable('ServicePaymentModeOption')->find($request->getParameter('id'));
    $this->forward404Unless($this->service_payment_mode_option);
  }

  public function executeNew(sfWebRequest $request) {
    $this->merchant_id="";//initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
    $this->merchant_service_id=""; //same as the reason above
    $this->payment_mode_id="";//initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
    $this->payment_mode_option_id=""; //same as the reason above
    $this->form = new ServicePaymentModeOptionForm();
    $this->form->setDefault('currency_id',0);
  }

  public function executeCreate(sfWebRequest $request) {
    $params=$request->getPostParameters('service_payment_mode_option');
    $this->merchant_service_id=$params['service_payment_mode_option']['merchant_service_id'];
    $this->merchant_id=$params['service_payment_mode_option']['merchant_id'];

    $this->payment_mode_id=$params['service_payment_mode_option']['payment_mode_id'];//initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
    $this->payment_mode_option_id=$params['service_payment_mode_option']['payment_mode_option_id']; //same as the reason above

    $this->form = new ServicePaymentModeOptionForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request) {
    $this->forward404Unless($service_payment_mode_option = Doctrine::getTable('ServicePaymentModeOption')->find($request->getParameter('id')), sprintf('Object service_payment_mode_option does not exist (%s).', $request->getParameter('id')));
    $this->service_paymant_mode_options = Doctrine::getTable('ServicePaymentModeOption')->find($request->getParameter('id'));
    $this->merchant_service_id = $this->service_paymant_mode_options->merchant_service_id;
    $this->payment_mode_option_id = $this->service_paymant_mode_options->payment_mode_option_id;

    $this->merchant_service = Doctrine::getTable('MerchantService')->find($this->merchant_service_id);//get the bank_id of the bank_branch
    $this->merchant_id = $this->merchant_service->merchant_id;

    $this->payment_option = Doctrine::getTable('PaymentModeOption')->find($this->payment_mode_option_id);
    $this->payment_mode_id = $this->payment_option->payment_mode_id;
    $interswitchCategoryId = Doctrine::getTable('InterswitchMerchantCategoryMapping')->findByMerchantId($this->merchant_id)->toArray();
    $this->interswitchCategoryId = '';
    if(!empty ($interswitchCategoryId)){
        $this->interswitchCategoryId = $interswitchCategoryId['0']['interswitch_category_id'];
        //$this->form->setDefault('interswitch_category_id', $interswitchCategoryId);
    }

    $this->form = new ServicePaymentModeOptionForm($service_payment_mode_option,array('interswitch_category_id'=>$this->interswitchCategoryId ));
  }

  public function executeUpdate(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($service_payment_mode_option = Doctrine::getTable('ServicePaymentModeOption')->find($request->getParameter('id')), sprintf('Object service_payment_mode_option does not exist (%s).', $request->getParameter('id')));
   
    $params=$request->getPostParameters('service_payment_mode_option');
    $this->merchant_service_id=$params['service_payment_mode_option']['merchant_service_id'];
    //   $merchant_id=$request->getPostParameters('merchant_id');
    $this->merchant_id=$params['service_payment_mode_option']['merchant_id'];

    $this->payment_mode_id=$params['service_payment_mode_option']['payment_mode_id'];//initializing the variable as empty; as it is being used in case of edit and both new and edit use the same partial
    $this->payment_mode_option_id=$params['service_payment_mode_option']['payment_mode_option_id']; //same as the reason above
    
    $interswitchCategoryId = Doctrine::getTable('InterswitchMerchantCategoryMapping')->findByMerchantId($this->merchant_id)->toArray();
    $this->interswitchCategoryId = '';
    if(!empty ($interswitchCategoryId)){
        $this->interswitchCategoryId = $interswitchCategoryId['0']['interswitch_category_id'];
        //$this->form->setDefault('interswitch_category_id', $interswitchCategoryId);
    }
    
    $this->form = new ServicePaymentModeOptionForm($service_payment_mode_option,array('interswitch_category_id'=>$this->interswitchCategoryId ));
    
    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request) {
    $request->checkCSRFProtection();

    $this->forward404Unless($service_payment_mode_option = Doctrine::getTable('ServicePaymentModeOption')->find($request->getParameter('id')), sprintf('Object service_payment_mode_option does not exist (%s).', $request->getParameter('id')));
    
    
    $MerchantId  = Doctrine::getTable('MerchantService')->find($service_payment_mode_option->getMerchantServiceId());
    
    Doctrine::getTable('InterswitchMerchantCategoryMapping')->findByMerchantId($MerchantId->getMerchantId())->delete();  
    
    $service_payment_mode_option->delete();
    
    $this->redirect('service_payment_mode_option/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form) {
      
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid()) {
        
      $postParameters = $request->getPostParameters();

      if($postParameters['service_payment_mode_option']['payment_mode_id'] == '4'){
          $merchantId = $postParameters['service_payment_mode_option']['merchant_id'];
          $checkMapping = $this->checkStatus($merchantId);
          if(empty($checkMapping)){
              if($postParameters['service_payment_mode_option']['interswitch_category_id']!='0'){
                    $user = new InterswitchMerchantCategoryMapping();
                    $user->set('merchant_id', $postParameters['service_payment_mode_option']['merchant_id']);  
                    $user->set('interswitch_category_id', $postParameters['service_payment_mode_option']['interswitch_category_id']);  
                    $user->save();
              }else {
                    $this->redirect('service_payment_mode_option/new');
              }
          }
      }

     $service_payment_mode_option = $form->save();
      if(($request->getParameter('action')) == "update") {
        $this->getUser()->setFlash('notice', sprintf('Service Payment Mode Option updated successfully'));
      }
      else if(($request->getParameter('action')) == "create") {
          $this->getUser()->setFlash('notice', sprintf('Service Payment Mode Option added successfully'));
        }

      $this->redirect('service_payment_mode_option/index');
    }

  }

  public function executeMerchantService(sfWebRequest $request) {
    $this->setTemplate(FALSE);
    $merchant_service_obj = merchant_serviceServiceFactory::getService(merchant_serviceServiceFactory::$TYPE_BANK);
    $str = $merchant_service_obj->getServices($request->getParameter('merchant_id'),$request->getParameter('merchant_service_id'));
    return $this->renderText($str);
  }

  public function executePaymentModeOption(sfWebRequest $request) {
    $this->setTemplate(FALSE);
    $payment_mode_option_obj = paymentModeOptionServiceFactory::getService(paymentModeOptionServiceFactory::$TYPE_BANK);
    $str = $payment_mode_option_obj->getServices($request->getParameter('payment_mode_id'),$request->getParameter('payment_mode_option_id'));
    return $this->renderText($str);
  }
  public function executeCheckPaymentcategory(sfWebRequest $request){
      $id = $request->getParameter('id');
      $data = Doctrine::getTable('InterswitchMerchantCategoryMapping')->findByMerchantId($id );
      if(count($data)>0){
          echo  "exist";
      }
      die;
  }
  public function checkStatus($id){
      $data = Doctrine::getTable('InterswitchMerchantCategoryMapping')->findByMerchantId($id );
      if(count($data)>0){
          return true;
      }
  }
}
