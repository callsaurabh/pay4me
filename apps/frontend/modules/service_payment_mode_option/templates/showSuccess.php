<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $service_payment_mode_option->getid() ?></td>
    </tr>
    <tr>
      <th>Merchant service:</th>
      <td><?php echo $service_payment_mode_option->getmerchant_service_id() ?></td>
    </tr>
    <tr>
      <th>Payment mode option:</th>
      <td><?php echo $service_payment_mode_option->getpayment_mode_option_id() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $service_payment_mode_option->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $service_payment_mode_option->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $service_payment_mode_option->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $service_payment_mode_option->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $service_payment_mode_option->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('service_payment_mode_option/edit?id='.$service_payment_mode_option->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('service_payment_mode_option/index') ?>">List</a>
