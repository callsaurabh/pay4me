<script language="Javascript">
    <?php if($interswitchCategoryId) { ?>
    $(document).ready(function()
    {
         $("#service_payment_mode_option_interswitch_category_id_row").show();
    });
    <?php  }?>
</script>

<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php echo ePortal_legend('Edit Service payment mode option'); ?>
<?php include_partial('form', array('form' => $form,'merchant_service_id'=>$merchant_service_id,'merchant_id'=>$merchant_id,'payment_mode_option_id'=>$payment_mode_option_id,'payment_mode_id'=>$payment_mode_id,'interswitchCategoryId'=>$interswitchCategoryId)) ?>
