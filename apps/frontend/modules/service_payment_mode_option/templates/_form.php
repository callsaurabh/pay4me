<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<script language="Javascript">


    $(document).ready(function()
    {
        // $("#service_payment_mode_option_interswitch_category_id_row").hide();
        // alert(document.getElementById("employee_bank_branch_id").value);
        var merchant_id = "<?php echo $merchant_id;?>";
        if(merchant_id!="")//in case of edit, bank needs to be selected, in case of new, it will be blank
        {
            document.getElementById("service_payment_mode_option_merchant_id").value = merchant_id;
        }
        if(document.getElementById("service_payment_mode_option_merchant_id").value != "")
        {
            var merchant_service_id = "<?php echo $merchant_service_id;?>";
            var merchant_id = document.getElementById("service_payment_mode_option_merchant_id").value;
            var module = "service_payment_mode_option";
            displayMerchantService(merchant_id,module,merchant_service_id,'<?php echo url_for('service_payment_mode_option/MerchantService'); ?>');

        }
        $('#service_payment_mode_option_merchant_id').change(function(){
            var merchant_id = this.value;
            var module = "service_payment_mode_option";
            displayMerchantService(merchant_id,module,"",'<?php echo url_for('service_payment_mode_option/MerchantService'); ?>');
            return false;
        });

        var payment_mode_id = "<?php echo $payment_mode_id;?>";
        if(payment_mode_id!="")//in case of edit, bank needs to be selected, in case of new, it will be blank
        {
            document.getElementById("service_payment_mode_option_payment_mode_id").value = payment_mode_id;
        }
        if(document.getElementById("service_payment_mode_option_payment_mode_id").value != "")
        {
            var payment_mode_option_id = "<?php echo $payment_mode_option_id;?>";
            var payment_mode_id = document.getElementById("service_payment_mode_option_payment_mode_id").value;
            var module = "service_payment_mode_option";
            displayPaymentModeOption(payment_mode_id,module,payment_mode_option_id,'<?php echo url_for('service_payment_mode_option/PaymentModeOption'); ?>');

        }

        $('#service_payment_mode_option_payment_mode_id').change(function(){
            var payment_mode_id = this.value;
            var module = "service_payment_mode_option";
            displayPaymentModeOption(payment_mode_id,module,"",'<?php echo url_for('service_payment_mode_option/PaymentModeOption'); ?>');
            return false;
        });
         $("#service_payment_mode_option_payment_mode_id").change(function(){
             var merchant_id = $("#service_payment_mode_option_merchant_id").val();
          
             
            
            var payment_mode_option = $("#service_payment_mode_option_payment_mode_id").val();
            if(payment_mode_option=='4'){ 
                $.ajax({
                    type: "get",
                    url: "<?php echo url_for('service_payment_mode_option/checkPaymentcategory'); ?>"+"/id/"+merchant_id,
                    success: function(msg){
                     if(msg==""){
                         $("#service_payment_mode_option_interswitch_category_id_row").show();
                     }else{
                         $("#service_payment_mode_option_interswitch_category_id_row").hide();
                     }
                    }
               });
            }else {
             $("#service_payment_mode_option_interswitch_category_id_row").hide();
            }
        });
    });
</script>

<div class="wrapForm2">
<form action="<?php echo url_for('service_payment_mode_option/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="pfm_service_payment_mode_option_form" name="pfm_service_payment_mode_option_form">
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
    
        <?php echo $form ?>
       <div class="divBlock">
    <center>  &nbsp;<?php  echo button_to('Cancel','',array('class'=>'formCancel', 'onClick'=>'location.href=\''.url_for('service_payment_mode_option/index').'\''));?>
                <input type="submit" value="Save" class="formSubmit" />
        </center></div>
    
</form>
</div>
</div>