<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $payment_mode_option->getid() ?></td>
    </tr>
    <tr>
      <th>Payment mode:</th>
      <td><?php echo $payment_mode_option->getpayment_mode_id() ?></td>
    </tr>
    <tr>
      <th>Name:</th>
      <td><?php echo $payment_mode_option->getname() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $payment_mode_option->getcreated_at() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $payment_mode_option->getupdated_at() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $payment_mode_option->getdeleted() ?></td>
    </tr>
    <tr>
      <th>Created by:</th>
      <td><?php echo $payment_mode_option->getcreated_by() ?></td>
    </tr>
    <tr>
      <th>Updated by:</th>
      <td><?php echo $payment_mode_option->getupdated_by() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('payment_mode_option/edit?id='.$payment_mode_option->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('payment_mode_option/index') ?>">List</a>
