<?php

/**
 * payment_mode_option actions.
 *
 * @package    mysfp
 * @subpackage payment_mode_option
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class payment_mode_optionActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
//    $this->payment_mode_option_list = Doctrine::getTable('PaymentModeOption')
//      ->createQuery('a')
//      ->execute();

      $payment_mode_option_obj = paymentModeOptionServiceFactory::getService(paymentModeOptionServiceFactory::$TYPE_BASE);
        $this->payment_mode_option_list = $payment_mode_option_obj->getAllRecords();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PaymentModeOption',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->payment_mode_option_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->payment_mode_option = Doctrine::getTable('PaymentModeOption')->find($request->getParameter('id'));
    $this->forward404Unless($this->payment_mode_option);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new PaymentModeOptionForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));

    $this->form = new PaymentModeOptionForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($payment_mode_option = Doctrine::getTable('PaymentModeOption')->find($request->getParameter('id')), sprintf('Object payment_mode_option does not exist (%s).', $request->getParameter('id')));
    $this->form = new PaymentModeOptionForm($payment_mode_option);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->forward404Unless($payment_mode_option = Doctrine::getTable('PaymentModeOption')->find($request->getParameter('id')), sprintf('Object payment_mode_option does not exist (%s).', $request->getParameter('id')));
    $this->form = new PaymentModeOptionForm($payment_mode_option);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($payment_mode_option = Doctrine::getTable('PaymentModeOption')->find($request->getParameter('id')), sprintf('Object payment_mode_option does not exist (%s).', $request->getParameter('id')));
    $payment_mode_option->delete();
    $this->getUser()->setFlash('notice', sprintf('Payment Mode Option deleted successfully'));

    $this->redirect('payment_mode_option/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
//    $form->bind($request->getParameter($form->getName()));
//    if ($form->isValid())
//    {
//      $payment_mode_option = $form->save();
//
//      $this->redirect('payment_mode_option/edit?id='.$payment_mode_option->getId());
//    }


    $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $payment_mode_option_params = $request->getParameter('payment_mode_option');
            $payment_mode_id = $payment_mode_option_params['payment_mode_id'];
            $name = $payment_mode_option_params['name'];
            $id = $payment_mode_option_params['id'];
            $payment_mode_option_obj = paymentModeOptionServiceFactory::getService(paymentModeOptionServiceFactory::$TYPE_BASE);
            $already_created = $payment_mode_option_obj->checkDuplicacy($payment_mode_id,$name,$id);
            if($already_created) {
                $this->getUser()->setFlash('error', sprintf('This Payment Mode Option has already been added for the selected Payment Mode'));
            // $this->redirect('bank_branch/index');
            }
            else {

                $payment_mode_option = $form->save();
                if(($request->getParameter('action')) == "update") {
                    $this->getUser()->setFlash('notice', sprintf('Payment Mode Option updated successfully'));
                }
                else if(($request->getParameter('action')) == "create") {
                        $this->getUser()->setFlash('notice', sprintf('Payment Mode Option added successfully'));
                    }
                $this->redirect('payment_mode_option/index');
            }
        }
  }
}
