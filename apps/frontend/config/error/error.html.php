<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
      <?php
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
           $webPath = public_path('',true);
           $projectUrl =url_for('@homepage',true);
        ?>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
  <meta HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
  <meta HTTP-EQUIV="Expires" CONTENT="-1"/>
    <title>Pay4Me</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
    <link rel="shortcut icon" href="/favicon.ico" />

    <script type="text/javascript" src="<?php echo $webPath.'js/jquery-1.4.2.min.js'?>"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $webPath.'css/screen.css'?>" />



</head>
<body>
<div id="body_bg">
<div id="main">
<div id="curvetop"></div>
<div id="wrapper">


<div id="wrapNav">

      <div id="thenav">
      <div id="versignLogo">
<script type="text/javascript" >

</script>
<a  href="javascript:vrsn_splash()"><img width="83" height="42" border="0" alt="Click to Verify - This site has chosen a VeriSign SSL Certificate to improve Web site security"
                                         src="<?php echo $webPath.'img/new/logo-versign.png'?>" /></a>
</div>
        <ul id="topnav">
          <li>
            <h6><a href="<?php echo $projectUrl.'page/features'?>">Features</a></h6>
          </li>
          <li>
            <h6><a href="<?php echo $projectUrl.'page/services'?>">Services</a></h6>
          </li>

          <li>
            <h6><a href="<?php echo $projectUrl.'page/pricing'?>">Pricing</a></h6>
          </li>
          <li>
            <h6><a title="Frequently Asked Questions" href="<?php echo $projectUrl.'page/Faq'?>">FAQs</a></h6>
          </li>
          <li>
            <h6><a style="border-right:none;" href="<?php echo $projectUrl.'page/feedBack'?>">Support</a></h6>
          </li>
          <li>
            <h6><a style="border-right:none;" href="<?php echo $projectUrl.'page/contactUs'?>">Contact</a></h6>
          </li>
        </ul>
      </div>
      <!-- <ul id="secnav">
        <li>
          <h6><a href="<?php // echo $projectUrl.''?>">Home</a></h6>
        </li>
        <li>

          <h6><a href="<?php // echo $projectUrl.'page/aboutUs'?>">About pay4me</a></h6>
        </li>
        <li>
          <h6><a href="<?php // echo $projectUrl.'page/contactUs'?>">Contact</a></h6>
        </li>
      </ul> -->
      <div class="clearfix"></div>
    </div>


<div id="thetop">
<div class="call-msg">For Support Call: <?php echo sfConfig::get('app_support_contact_number');?> or Email <a href="mailto:support@pay4me.com">support@pay4me.com</a></div>
      <div id="theLogo"><img alt="Pay4Me" width="261" height="33" border="0" usemap="#Map" src="<?php echo $webPath.'img/new/logo-pay4me.gif'?>" />
          <map name="Map" id="Map">

          <area shape="rect" coords="4,2,158,30" href="<?php echo $projectUrl.'/'?>" />
        </map>
      </div>
           <div class="clearfix"></div>
<div class="clear"></div>
</div>


<div class="innerWrapper">
<div id="faqPage" class="cmspage">

    <div id='loadArea'><h2>We apologise for the inconvenience. The Pay4Me page you've requested is not available at this time. <br /> There are several possible reasons you are unable to reach the page:</h2><div class='clear'></div>    <div class="block-container">
        <div class="HFeatures" style="background-image:none;">
            <p>
                <ul>
                  <li>If you tried to load a bookmarked page, or followed a link from another Web site, it's possible that the URL you've requested has been moved</li>

                  <li>The web address you entered is incorrect</li>
                  <li>Technical difficulties are preventing us from display of the requested page</li>
                </ul>
            <h1>We want to help you to access what you are looking for</h1>
            <ul>
                  <li><a href="<?php echo $projectUrl; ?>">Visit our  Homepage</a></li>
                  <li>Try returning to the page later</li>

                </ul>
            </p>
        </div>
</div>
</div></div></div>
 <div class="clearfix"></div></div></div>
<div id="wrapFoot">
<div id="main">
      <div id="copyrightInfo">Copyright © 2013 Pay4me. All rights reserved.</div>
      <ul id="footnav">
        <li><a href="<?php echo $projectUrl; ?>/">Home</a></li>

        <li><a href="<?php echo $projectUrl; ?>page/aboutUs">About pay4me</a></li>
        <li><a href="<?php echo $projectUrl; ?>page/termsOfUse">Terms of use</a></li>
        <li><a class="one" href="mailto:support@pay4me.com">Contact Us</a></li>
      </ul>

    </div>
</div>
<noscript>
  <div id="noscript">
    <div>
      <h1>Warning !</h1> <h3>This site requires Javascripts </h3><p>Your browser does not support Javascripts, or its is disabled.</p>
    </div>
  </div>

</noscript>

  </body>
</html>