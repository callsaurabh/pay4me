<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//include_once ('../lib/vendor/symfony/lib/helper/HelperHelper.php');

//use_helper('Url');

class securityQuestionRedirectFilter extends sfFilter
{
  public function execute($filterChain)
  {
      $request = $this->getContext()->getRequest();
      
      if ( $this->isFirstCall() && (!$request->isXmlHttpRequest()) ) {
          $user = "";
          if(sfContext::getInstance()->getUser()->isAuthenticated()){
                if(sfContext::getInstance()->getUser()->getGuardUser()){                 
                     $user =sfContext::getInstance()->getUser()->getGuardUser()->getId();
                }
                if(isset($user) && $user!=""){
                    $pfmHelperObj = new pfmHelper();
                    $userGroup = $pfmHelperObj->getUserGroup();
                    //do redirection logic here
                    if($this->isValidUserToWelconeQuestion($userGroup)){
                        if($this->checkUserQuestionUpdate($user)){
                            $request = $this->getContext()->getRequest();
                            $params = $request->getParameterHolder()->getAll();
                            $uri_prefix =  $request->getUriPrefix();
                            $url_end = $request->getUri();
                            $context = $this->getContext();
                            if($this->checkuserUrl($url_end)) {
                                $context->getController()->redirect('signup/choicequestion');
                            }
                          
                        }
                    }
              }
          }
      }
      $filterChain->execute();
  }


  function checkuserUrl($url){
      $arrUsers = array('/signup/choicequestion','qna/new', 'userAdmin/changeFirstPassword');
      $status = true;
      for($i=0;$i<count($arrUsers);$i++){
          if(strpos($url,$arrUsers[$i])){
            $status=false;
          }
      }

      return $status;

  }


  function isValidUserToWelconeQuestion($userGroup)
  {
   $arrayQuestionUser = array(sfConfig::get('app_pfm_role_bank_admin'),
       sfConfig::get('app_pfm_role_bank_branch_user'),
       sfConfig::get('app_pfm_role_report_bank_admin'),
       sfConfig::get('app_pfm_role_bank_country_head'),
       sfConfig::get('app_pfm_role_bank_e_auditor'),
       sfConfig::get('app_pfm_role_bank_branch_report_user'),
       sfConfig::get('app_pfm_role_ewallet_user'),
       sfConfig::get('app_pfm_role_report_admin'),
       sfConfig::get('app_pfm_role_country_report_user'))  ;

       if(in_array($userGroup, $arrayQuestionUser)){
        return true;
       }else{
        return false;
      }
    }

    private function checkUserQuestionUpdate($user_id){
        $userQuestionCount = Doctrine::getTable('SecurityQuestionsAnswers')->
                findByUserId($user_id)->Count();
        if ($userQuestionCount > Settings::getNumberOfQuestion()) {

            return true;
        } else {
            return false;
        }
    }

  
}
?>