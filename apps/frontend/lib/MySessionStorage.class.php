<?php
class MySessionStorage extends sfSessionStorage
{
  public function initialize($options = null)
  {
    // work-around for swfuploader
    if(sfContext::getInstance()->getRequest()->getParameter('z')) {
      session_id(sfContext::getInstance()->getRequest()->getParameter('z'));
    }

    parent::initialize($options);
  }
}


?>