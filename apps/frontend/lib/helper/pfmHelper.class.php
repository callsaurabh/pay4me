<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pfmHelper
 *
 * @author Manoj Kumar
 */
class pfmHelper {
    
/**
 * Description of [getUserGroup()] Function
 *
 * This function returns the current Login User's Group Name.
 *
 * @author Manoj Kumar
 */
  function getUserGroup(){
    $groups = array();
    $groups = sfContext::getInstance()->getUser()->getGroupNames();
    if(count($groups)>0){
      return $groups[0];
    }
    return $groups;
  }
  static function getUsernameByUserid($userid){
  	$username = '';
  	if($userid > 0){
    	$userDetails = Doctrine::getTable('UserDetail')->findByUserId($userid);
    //      echo "<pre>";print_r($userDetails);die;
    	$username =  $userDetails->getFirst()->getName()." ".$userDetails->getFirst()->getLastName();
    }
  	return $username;
  }
  static function getBanknameByUserid($userid){
    $userDetails = Doctrine::getTable('BankUser')->getBankIdName($userid);
    return $userDetails[0]['bank_name'];
  }


/**
 * Description of [getUserAssociatedBank()] Function
 *
 * This function returns the current Login User's Associated Bank Name.
 *
 * @author Manoj Kumar
 */
  function getUserAssociatedBank(){
    $userAssociatedBank = array();
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $userBankObj = Doctrine::getTable('BankUser')->createQuery('a')->leftJoin('a.Bank b')->where('a.user_id=?',$userId)->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(count($userBankObj)>0){
      $userAssociatedBank = $userBankObj[0]['Bank'];
      return $userAssociatedBank;
    }
    return $userAssociatedBank;
  }


/**
 * Description of [getUserAssociatedBankBranch()] Function
 *
 * This function returns the current Login User's Associated Bank Branch Name.
 *
 * @author Manoj Kumar
 */

  function getUserAssociatedBankBranch(){
    $userBankBranchDeatails = "";
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $userBankBranchObj = Doctrine::getTable('BankUser')->createQuery('a')->leftJoin('a.Bank b')->leftJoin('b.BankBranch c')->where('a.user_id=?',$userId)->andWhere('a.bank_branch_id !=?',"")->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(count($userBankBranchObj)>0){
      $userBankBranchId = $userBankBranchObj[0]['bank_branch_id'];
      $userBankBranchDeatails = Doctrine::getTable('BankBranch')->createQuery('a')->where('a.id =?',$userBankBranchId)->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(count($userBankBranchDeatails)>0){
        return $userBankBranchDeatails[0]['name'];
      }
    }
    return $userBankBranchDeatails;
  }

/**
 * Description of [getUserBankBranches()] Function
 *
 * This function returns all Branch Names for the current Login User's Associated Bank.
 *
 * @author Manoj Kumar
 */

  function getUserBankBranches(){
    $userBankBranchesDetailsArray = "";
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $userBankBranchesDetails = Doctrine::getTable('BankUser')->createQuery('a')->leftJoin('a.Bank b')->leftJoin('b.BankBranch c')->where('a.user_id=?',$userId)->orderBy('c.name')->execute(array(),Doctrine::HYDRATE_ARRAY);

    if(count($userBankBranchesDetails)>0){
      foreach($userBankBranchesDetails[0]['Bank']['BankBranch'] as $key => $val){
        $userBankBranchesDetailsArray[$val['id']] = $val['name'].' ('.$val['branch_code'].')';
      }
      return $userBankBranchesDetailsArray;
    }
    return $userBankBranchesDetailsArray;
  }

/**
 * Description of [getUserAssociatedBankDetails()] Function
 *
 * This function returns the current Login User's Associated Bank Details.
 *
 * @author Manoj Kumar
 */
  function getUserAssociatedBankDetails(){
    $userAssociatedBank = array();
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $userBankObj = Doctrine::getTable('BankUser')->createQuery('a')->leftJoin('a.Bank b')->where('a.user_id=?',$userId)->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(count($userBankObj)>0){
      $userAssociatedBank = $userBankObj[0];
      return $userAssociatedBank;
    }
    return $userAssociatedBank;
  }

/**
 * Description of [getMailData($txnId)] Function
 *
 * This function returns the data required to send a mail when payment is done successfully.
 *
 * @author Anil Kumar
 */
  static function getMailData($txnId){
    $transDetails = Doctrine::getTable('Transaction')->getMerchantRequestDetails($txnId);
    $payment_status_code = $transDetails->getFirst()->getMerchantRequest()->getPaymentStatusCode();
    $paid_date = $transDetails->getFirst()->getMerchantRequest()->getPaidDate();
    $paid_amount = $transDetails->getFirst()->getMerchantRequest()->getPaidAmount();
    $validation_number = $transDetails->getFirst()->getMerchantRequest()->getValidationNumber();
    $merchant_name = $transDetails->getFirst()->getMerchantRequest()->getMerchant()->getName();
    $currency_id = $transDetails->getFirst()->getMerchantRequest()->getCurrencyId();
    $merchant_service_name = $transDetails->getFirst()->getMerchantRequest()->getMerchantService()->getName();
    $merchant = $merchant_name."-".$merchant_service_name;
    $sendArr = array($paid_date,$merchant,$paid_amount, $currency_id, $validation_number);
    $rDetails = base64_encode(serialize($sendArr));
    $gatewayOrder = Doctrine::getTable('GatewayOrder')->findByAppId($txnId);
    $orderId = $gatewayOrder->getFirst()->getOrderId();
    $paymentModOptionId = $gatewayOrder->getFirst()->getPaymentModeOptionId();

    $partialVars = array();
    if($payment_status_code == 0) {
      $userDetailsArray = self::getUserDetailsForMail($orderId, $paymentModOptionId);

      if(is_object($userDetailsArray)){
        $firstname = $userDetailsArray->getFirst()->getSfGuardUser()->getUserDetail()->getFirst()->getName();
        $lastname = $userDetailsArray->getFirst()->getSfGuardUser()->getUserDetail()->getFirst()->getLastName();
        $email = $userDetailsArray->getFirst()->getSfGuardUser()->getUserDetail()->getFirst()->getEmail();

        $signature = 'Pay4Me Team';
        $mailFrom = sfConfig::get('app_email_local_settings_username');
        $mailingOptions['mailSubject'] = 'Pay4me - Successful Payment Notification';
        $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = $mailFrom;

        $partialVars= array('firstname'=>$firstname,'lastname'=>$lastname,'orderDetailId'=>$validation_number,'signature'=>$signature,'request_details'=>$rDetails,'mailingOptions'=>$mailingOptions);
      }
    }
    return $partialVars;
  }

/**
 * Description of [getUserDetails()] Function
 *
 * This function returns the user details required to send a mail.
 *
 * @author Anil Kumar
 */
  static function getUserDetailsForMail($orderId, $paymentModOptionId){
    $detailsArray = array();
    switch ($paymentModOptionId){
      case 2:  //Interswitch
        $detailsArray = Doctrine::getTable('EpInterswitchRequest')->getUserDetailsForMail($orderId);
        break;
      case 3:  //eTranzact
        $detailsArray = Doctrine::getTable('EpEtranzactRequest')->getUserDetailsForMail($orderId);
        break;
      case 5:  //VbV
        $detailsArray = Doctrine::getTable('EpVbvRequest')->getUserDetailsForMail($orderId);

        break;
    }
    return $detailsArray;
  }




/**
 * Description of [getMailData($txnId)] Function
 *
 * This function returns the data required to send a mail when payment is done successfully.
 *
 * @author Anil Kumar
 */
  static function getMailDataRecharge($txnId,$order_id){

  
   $gatewayOrder = Doctrine::getTable('GatewayOrder')->findByOrderId($order_id);
   
  //  $orderId = $gatewayOrder->getFirst()->getOrderId();
    $paymentModOptionId = $gatewayOrder->getFirst()->getPaymentModeOptionId();
    $totalAmountPaid = $gatewayOrder->getFirst()->getAmount();
    $serviceCharge = $gatewayOrder->getFirst()->getServicechargeKobo();
    $rechargeAmount = $totalAmountPaid - $serviceCharge;
    $validationNumber = $gatewayOrder->getFirst()->getValidationNumber();
    $currency_id = $gatewayOrder->getFirst()->getCurrencyId();
    $userId = $gatewayOrder->getFirst()->getUserId();
    //as transaction date response is updated by unkonwn date formate bug ref 34395
    $transaction_date = $gatewayOrder->getFirst()->getUpdatedAt();
    $formatted_date = date('d M, Y, g:i a', strtotime($transaction_date));

    //get account number -
    $accountManager = new UserAccountManager($userId);
    $detail = $accountManager->getAccountForCurrency($currency_id);
    foreach($detail as $val){
           $wallet_number =  $val->getEpMasterAccount()->getAccountNumber();
        }
    
    //$amount = $gatewayOrder->getFirst()->getAmount();

    $partialVars = array();
    //if($payment_status_code == 0) {
     $userDetailsArray = self::getUserDetailsForMail($order_id, $paymentModOptionId);

      if(is_object($userDetailsArray)){
        $firstname = $userDetailsArray->getFirst()->getSfGuardUser()->getUserDetail()->getFirst()->getName();
        $lastname = $userDetailsArray->getFirst()->getSfGuardUser()->getUserDetail()->getFirst()->getLastName();
        $email = $userDetailsArray->getFirst()->getSfGuardUser()->getUserDetail()->getFirst()->getEmail();

        $signature = '';
        $mailFrom = '';
        $mailingOptions['mailSubject'] = 'Pay4me - Successful Recharge Notification';//FS#30020
        if(sfConfig::get('app_host') == "production"){
          $mailFrom = sfConfig::get('app_email_production_settings_username');
          $signature = sfConfig::get('app_email_production_settings_signature');
        }
        else if(sfConfig::get('app_host') == "local"){
          $mailFrom= sfConfig::get('app_email_local_settings_username');
          $signature = sfConfig::get('app_email_local_settings_signature');
        }
        $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = array($mailFrom => 'Pay4Me');
        //$partialVars= array('firstname'=>$firstname,'lastname'=>$lastname,'orderDetailId'=>$validation_number,'signature'=>$signature,'request_details'=>$rDetails,'mailingOptions'=>$mailingOptions);
        $partialVars= array('firstname'=>$firstname,'lastname'=>$lastname,'signature'=>$signature,'mailingOptions'=>$mailingOptions,'currency_id'=>$currency_id,'date'=>$formatted_date,'total_amount'=>$totalAmountPaid,'wallet_number'=>$wallet_number,'serviceCharge'=>$serviceCharge,'rechargeAmount'=>$rechargeAmount,'validationNumber'=>$validationNumber);
      }
    //}
    return $partialVars;
  }

 /**
  * to get payment mode option ID by name set in app.yml
  * @param <type> $name
  * @return <type>
  */
 function getPMOIdByConfName($name){
    $modeName=sfConfig::get('app_payment_mode_option_'.$name);
    $paymentModeOptionDetails = Doctrine::getTable('PaymentModeOption')->findByName($modeName);
    return $paymentModeOptionDetails[0]->getId();
  }

 /**
  * to get payment mode -> Display Name by name set in app.yml
  * @param <type> $name
  * @return <type>
  */
  function getPMONameByConfName($name){
        $pmo_id=$this->getPMOIdByConfName($name);
        return $this->getPMONameByPMId($pmo_id);
  }

  /*
 * To fetch payment mode options from ewallet_recharge_acct_details
 */
    public static function getPMONameByPMId($payMentModeOption){
        $paymentModeOptionDetails = Doctrine::getTable('PaymentModeOption')->getPaymentModeOption($payMentModeOption);
        $paymentModeOptionName = $paymentModeOptionDetails->getFirst()->getPaymentMode()->getDisplayName();
        return $paymentModeOptionName;

    }

    public function getPMONameByPMOId($paymentModeOption){
        $paymentMode = Doctrine::getTable('PaymentModeOption')->findById($paymentModeOption);
        if($paymentMode->count()>0){
            return $paymentMode->getFirst()->getName();
        }
    }

    /*
     * WP058 CR099 Image in ballon button(tool tip) in cheque
     */
    public static function SortCodeTooltip($payModeType) {
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $webPath = public_path('',true);
        $tooltipBody = '';
        if(ucfirst($payModeType)==sfConfig::get('app_payment_mode_option_Cheque')){
            $tooltipText = '<img src="'.$webPath.'images/cheque.png">';}
        else{ $tooltipText = '<img src="'.$webPath.'images/draft.png">';}
        $tooltipBody = self::CreateTooltip("tsort-code", "Sample Cheque Leaflet", $tooltipText);
        echo $tooltipBody;
    }

    /*
     *  WP058 CR099 Image in ballon button(tool tip) in cheque
     */
     private static function CreateTooltip($name, $tooltipHeading, $tooltipText, $width=400) {
        $tooltipBody = '
                <span >
                    <a class="jTip" id="' . $name . '" name="' . $tooltipHeading . '" width="' . $width . '" >' .
                image_tag('question.jpeg', array('size' => '20x20')) . '
                    </a>
                </span>
                <span id = "' . $name . '-toolTip" class="formInfo" style="display:none" >' . $tooltipText . '
                </span>';
        return $tooltipBody;
    }

    /*
     * WP058 CR099
     * To fetch display name of cheque from app.yml
     */
    public static function getChequeDisplayName(){
        return sfConfig::get('app_payment_mode_option_cheque_display_name');
    }

    /*
     * WP058 CR099
     * To fetch display name of bank draft from app.yml
     */
    public static function getDraftDisplayName(){
        return sfConfig::get('app_payment_mode_option_draft_display_name');
    }
    /*
     * returns the payment_mode_id for internet bank
     */
    public function getInternetBankPayModeId() {
     $payModeName  = sfConfig::get('app_payment_mode_option_internet_bank');
     $payModeOptions = Doctrine::getTable('PaymentModeOption')->findByName($payModeName);
     $payMode = $payModeOptions->getfirst()->getId();
     return $payMode;
  }
  
}
?>