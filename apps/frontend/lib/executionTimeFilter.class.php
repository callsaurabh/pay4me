<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class executionTimeFilter extends sfFilter
{
  public function execute($filterChain)
  {


    $do_log = true ;
//    $do_log = sfConfig::get('app_log_execution_time_do_log', $default_do_log);

//    echo "do log : " . $do_log ;
    if($do_log)
    {
        $startTime = time() ;
        $moduleName = $this->getContext()->getModuleName() ;
        $actionName = $this->getContext()->getActionName() ;
        // Execute next filter
        $filterChain->execute();
        $endTime = time() ;
        $default_min_execution_time = 2 ; // default min execution time in seconds
//        $min_execution_time = sfConfig::get('app_log_execution_time_min_execution_time', $default_min_execution_time);
        $min_execution_time = 2 ;
        $execution_time = $endTime - $startTime ;
        if($execution_time > $min_execution_time)
        {
            $logString = $moduleName . "," . $actionName . "," .$startTime .",".$endTime .",".$execution_time . PHP_EOL ;
            $this->createLog($logString,'LoggingTime');
        }
    }else{
        $filterChain->execute();
    }
  }

    public function createLog($logdata,$nameFormat,$exceptionMessage="") {

        $pay4meLog = new pay4meLog();
        if($exceptionMessage!="") {
            $logdata = $logdata."\n".$exceptionMessage;
        }
        $pay4meLog->createLogData($logdata,$nameFormat, 'executionTime', false, true);
    }
}
?>