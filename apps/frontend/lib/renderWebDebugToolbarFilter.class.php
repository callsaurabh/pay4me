<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class renderWebDebugToolbarFilter extends sfFilter {
  public function execute($filterChain) {
    $environment= sfConfig::get('sf_environment');
    if(sfConfig::get('sf_environment') == "sandbox") {
      sfContext::getInstance()->getEventDispatcher()->connect('debug.web.load_panels', array('sfSandboxWebDebugPanel', 'listenToAddPanelEvent'));
    }
    $filterChain->execute();
  }
}


class sfSandboxWebDebugPanel {

  static public function checkUserPaymentAction() {
    if(sfContext::getInstance()->getModuleName()=='paymentProcess' && sfContext::getInstance()->getActionName()=='bankAcknowledgeSlip') {
      return true;
    }
  }

  static public function checkMerchantSetup($user_id) {
    $configured = Doctrine::getTable('Merchant')->isMerchantEwalletConfigured($user_id);
    if($configured) {
      return true;
    }
  }


  static public function listenToAddPanelEvent(sfEvent $event) {
    $webDebugToolbar = $event->getSubject();
    //$webDebugToolbar->removePanel();
    $webDebugToolbar->removePanel('memory');
    $webDebugToolbar->removePanel('config');
    $webDebugToolbar->removePanel('logs');
    $webDebugToolbar->removePanel('cache');
    $webDebugToolbar->removePanel('sf_debug');
    $webDebugToolbar->removePanel('time');
    $webDebugToolbar->removePanel('symfony_version');
    $webDebugToolbar->removePanel('db');



    $userObj = sfContext::getInstance()->getUser();

    if($userObj->isAuthenticated()) {
      $user_id = $userObj->getGuardUser()->getId();
      $group_name = $userObj->getGroupName();
      if($group_name == sfConfig::get('app_pfm_role_ewallet_user')) {
        $webDebugToolbar->setPanel('Recharge eWallet', new sfPanelRecharge($webDebugToolbar));

        if(self::checkMerchantSetup($user_id)) {
          $webDebugToolbar->setPanel('Get Order XML', new sfPanelOrderXML($webDebugToolbar));
        }
      }
      else if(sfConfig::get('app_pfm_role_bank_branch_user')==$group_name) {
          if(self::checkUserPaymentAction()) {
            $webDebugToolbar->setPanel('Sandbox Bank Payment', new sfWebDebugPanelBankList($webDebugToolbar));
          }
        }
    }else {
      if(self::checkUserPaymentAction()) {
        $webDebugToolbar->setPanel('Sandbox Bank Payment', new sfWebDebugPanelBankList($webDebugToolbar));
      }
    }
  }
}


class sfWebDebugPanelBankList extends sfWebDebugPanel {
  public function getTitle() {
    return 'Sandbox Bank Payment';
  }
  public function getPanelTitle() {
    return 'Select Bank for application Payment';
  }

  public function getPanelContent() {
    try {
      $request = sfContext::getInstance()->getRequest();
      if($request->getPostParameter('merchant_id')) {

        $merchantId = $request->getPostParameter('merchant_id');

        $selBankChoiceArray = array();
        $bankMerchantObj = bankMerchantServiceFactory::getService(bankMerchantServiceFactory::$TYPE_BASE);
        $bankMerchantListArray = $bankMerchantObj->findAllReleatedBanks($merchantId);
        $option = '';
        if(count($bankMerchantListArray)>0) {
          foreach($bankMerchantListArray as $key => $val) {
            $option .= '<option value="'.$val['Bank']['id'].'">'.$val['Bank']['bank_name'].'</option>';
          }
        }
        $bank  = $selBankChoiceArray;

        if($request->getPostParameter('trans_num')) {
          $paymentId = $request->getPostParameter('trans_num');
        } else {
          throw new Exception("Mandatory Parameter payment id missing");
        }
        return '<div><h2>Sandbox Payment </h2><div class="clear"></div>
<form name="pfm_search_bank_branch_form" id="pfm_search_bank_branch_form" onsubmit="return validate_search_bank_branch_form();" method="post" action="'.url_for('sandboxPayment/bankPayment').'"><div class="wrapForm2">

        <div class="formHead"> Select Bank for application Payment</div><dl><div class="dsTitle4Fields"><label>Bank  <sup>*</sup></label></div><div class="dsInfo4Fields"><select name="bank" id="bank" style="width: 250px;"><option value="">Please Select Bank</option>
'.$option.'</select></div></dl>        <center id="formNav"> <button class="formSubmit">Payment</button></center>
</div><input name="paymentId" value="'.$paymentId.'" type="hidden">
</form></div>';

      }
      else {
        throw new Exception("Mandatory Parameter merchant id missing");
      }
    }catch(Exception $e){
      echo $e->getMessage();
    }

  }
}

class sfPanelOrderXML extends sfWebDebugPanel {
  public function getTitle() {
    return 'Get Order XML';
  }

  public function getPanelTitle() {
    return 'Order XML';
  }
  public function getPanelContent() {
       $userObj = sfContext::getInstance()->getUser();
       $user_id = $userObj->getGuardUser()->getId();
       $MServiceArr=Doctrine::getTable('MerchantService')->getIdfrmEwalletUserId($user_id );
        if(count($MServiceArr)>0)
          {
                $html='<table>
                             <tr><td><b><u>Get Detail of Your order xml</u><b></td></tr>';
                foreach($MServiceArr as $merchanSerivceId)
                {
                    $serviceId=$merchanSerivceId['id'];
                    $serviceName=$merchanSerivceId['name'];
                    $pay4MeXMLV1Obj=new pay4MeXMLV1();
                    $xmlDetails=$pay4MeXMLV1Obj->generateSamlpleOrderXml($serviceId);
                    $headers='';
                    foreach($xmlDetails[1] as $val)
                        $headers=$headers.$val.'<br>';

                     $xml=htmlentities($xmlDetails[2]);
                     $midhtml = <<<EOF

    <tr><td>
        <table cellpadding=3 cellspacing=3 border=0>
            <tr>
                <td colspan=2><b>$serviceName</b><td>
            </tr>
            <tr>
                <td valign='top' width='100px' >Url :<td>
                <td valign='top'>$xmlDetails[0]<td>
            </tr>
            <tr>
                <td valign='top'>Headers :<td>
                <td valign='top'>$headers<td>
            </tr>
            <tr>
                <td valign='top'>Samlple XML :<td>
                <td valign='top'><pre>$xml</pre><td>
            </tr>
        </table>
    </td></tr>

EOF;

                    $html.=$midhtml;

                }
                  $html=$html.'</table>';
                return $html;
                //return ;
          }
       else{
           return;
         }
  }
}

class  sfPanelRecharge  extends sfWebDebugPanel {

  public function getTitle() {
    return 'Recharge eWallet';
  }

  public function getPanelTitle() {
    return 'Recharge Your Wallet';
  }

  public function getPanelContent() {
  // $response = sfContext::getInstance()->getResponse();
    $html = '';
    $url = url_for('recharge_ewallet/rechargeSandbox');
    $html = '<table border="0" width="100%">
    <form name="recharge" id="recharge" action="'.url_for('recharge_ewallet/rechargeSandbox').'" method="POST"><tr><td id="valid_amount_err"></td></tr>
    <tr><td>'; $html .= "<select name='amount'  id='amount'>
                      <option value=''>Select Amount</option>
                      <option value='20000'>20000</option>
                      <option value='40000'>40000</option>
                      <option value='60000'>60000</option>
                      <option value='80000'>80000</option>
                      <option value='100000'>100000</option></select></td></tr>";
    $html .= '<tr><td>&nbsp;</td></tr><tr><td><input type="button" value="Recharge" onClick="validate_amount_selection(\''.$url.'\')"></td></tr></form></table>';


    return $html;

  }
}

?>
