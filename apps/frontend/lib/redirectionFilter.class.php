<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class redirectionFilter extends sfFilter
{
  public function execute($filterChain)
  {
    //Execute this filter only once
    if ($this->isFirstCall())
    {
      // Filters don't have direct access to the request and user objects.
      // You will need to use the context object to get them
      $request = $this->getContext()->getRequest();
      $params = $request->getParameterHolder()->getAll();
      $uri_prefix =  $request->getUriPrefix();//http://localhost/
      $uri = $request->getUri(); //http://localhost/p4m_new/web/index.php/bank/index/id/12
      
      if(strpos($uri,'/phb/login') !== false) { //if www is not found
         $url_array = parse_url($uri_prefix); //to get if request is on http:// or https://
         $extension = explode($uri_prefix,$uri);

        $url = $url_array['scheme']."://".$request->getHost()."/kbl/login";
         header('Location: '.$url);exit;
      }
      if(strpos($uri,'/ibplc/login') !== false) { //if www is not found
         $url_array = parse_url($uri_prefix); //to get if request is on http:// or https://
         $extension = explode($uri_prefix,$uri);

        $url = $url_array['scheme']."://".$request->getHost()."/access/login";
         header('Location: '.$url);exit;
      }

    }
    // Execute next filter
    $filterChain->execute();
  }

}
?>