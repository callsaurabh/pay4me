<div class="passPolicy">
  <h3>Guidelines for Pay4Me Password</h3>
  <ul>
    <li>Password length should be between 8 – 20 characters.</li>
    <li>The Password should be alphanumeric i.e a mix of alphabets, numbers and special characters (!@#$%^&*(){}[]).</li>
    <li>Password is case sensitive.</li>
    <li>Please change your password periodically.</li>
    <li>In case the password is not changed then for security reasons every <?php echo EpPasswordPolicyManager::getNumberOfDaysInExpiry();?>  days you would be asked to compulsorily change your password.</li>
   <?php if($sf_request->getParameter('action') != 'index' && $sf_request->getParameter('module') != 'signup'){ ?><li>New password should not match the last <?php echo EpPasswordPolicyManager::getOldPasswordLimit() ?> passwords.</li> <?php }?>
  </ul>
</div>