<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Admin:</title>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_combined_stylesheets() ?>
    <?php include_combined_javascripts() ?>
    <!--[if IE 6]>
    <?php echo stylesheet_tag('admin_ie6'); ?>
    <![endif]-->
  </head>
  <body>

  <div id="adminLayout" class="pageWrapper">

    <div id="header">
      <div id="bx_toparea">
        <div id="site_logo">
          <div class="bx_nislogo"><?php echo image_tag("/images/nis.gif",array('alt'=>"Logo")); ?></div>
          <div class="bx_nis_title"><h1>Portal Administration</h1></div>
        </div>
        <div id="site_login">
          <?php if($sf_context->getUser()->isAuthenticated()){ ?>
          Welcome<span class="username"><?php echo $sf_context->getUser()->getUsername(); ?></span> [<span><?php echo link_to('Logout','@sf_guard_signout'); ?></span>]
          <?php } else{ echo "[<span>".link_to('Login','@sf_guard_signin')."</span>]" ;} ?>
        </div>
        <div id="timeclock"><?php  echo date('F dS Y H:i A, e') ?></div>
      </div>
      <div id="headLinks" class="navMenu">
        <?php if($sf_context->getUser()->isAuthenticated()){
          include_partial('global/menuAdmin');
        } else {
          echo "<ul><li>".link_to_app( 'home','cms/index','','frontend') ."</li></ul>";
        } ?>

      </div>
    </div>


    <div id="content">
      <?php echo $sf_content ?>
    </div>

    <div class="pixbr"></div>
    <?php include_partial('global/footer');?>

  </div>


  <script>
    $('document').ready(function(){
      $time = new Date();
      // alert($time);
      $('#timeclock').text($time.toLocaleString());
    })
  </script>
</html>
