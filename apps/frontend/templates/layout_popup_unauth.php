<?php
use_stylesheet('screen');
//use_stylesheet('admin_main');
//use_stylesheet('admin_theme','last');
//use_stylesheet('reset','first');
//use_stylesheet('default','first');
//use_javascript('jquery-1.2.6.js','first');
use_javascript('jquery-1.4.2.min.js','first');
use_javascript('jquery-ui.min.js','first');
use_javascript('general.js','first');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<noscript>
<meta http-equiv="refresh" content="0; URL=<?php echo url_for('@homepage');?>">
<style>
  body{display:none !important;}
</style>
</noscript>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Admin:</title>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_combined_stylesheets() ?>
    <?php include_combined_javascripts() ?>

    <!--[if IE 6]>
<?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
	
  </head>
  <body>
  
  <div id="main">
<div id="curvetop"></div>
<div id="wrapper">
<div id="thetop">
      <div id="theLogo"><?php echo image_tag('/img/new/logo-pay4me.gif',array('alt'=>'Pay4Me', 'width' => 261, 'height' => 33, 'border' => 0, 'usemap' => '#Map')); ?>
        <map name="Map" id="Map">
          <area shape="rect" coords="4,2,158,30" href="<?php echo url_for('@homepage'); ?>" />
        </map>
      </div>
      <?php echo verisign() ?>
      <div class="clearfix"></div>




    <div></div>

 <?php echo $sf_content ?></div>
<div class="clearfix"></div>
<div id="wrapFoot">
      <div id="copyrightInfo">Copyright &copy; <?php echo date('Y');?> Pay4me. All rights reserved.</div>
      
      <div class="clearfix"></div>
    </div>
</div></div>
<div id="curvebottom"></div>
</div>
  
  
  
  
  
  
  
  
  
  
  
  
  

  
  
  
  
   <!-- <div id="wrapper">
      <div id="wrapperTop">
          <div id="wrapperLogo"><?php// echo image_tag('/img/pay4me_logo.png',array('title'=>"Pay4me", 'alt'=>"Pay4me")); ?></div>
        </div>
      <div id="wrapperMid">
       
      </div>
      <div class="clear"></div>

      <?//php include_partial('global/footerAdmin');?>
    </div>-->

  </body>

</html>