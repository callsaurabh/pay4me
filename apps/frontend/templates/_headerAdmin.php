<div id="wrapperTop">
	<div id="wrapperTopNavArea">
		<div id="wrapperUserInfo">
			<?php 
			$userObj = $sf_context->getUser();
			if($userObj->isAuthenticated()){
				$userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
				$userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
			?>
				<div id="language" align="language"></div>
				<div class="flRight" id="noTextTransformation">
					<?php echo __('Hello')?>, <b><?php echo $userObj->getUsername(); ?></b>
					<?php if ($userDetails[0]['ewallet_type'] != 'guest_user') { ?>
						<b>[ <?php echo link_to(('Logout'),'@sf_guard_signout'); ?>]
						</b><br />
						<?php
						$group_name = $userObj->getGroupName();
						echo __("You're currently logged in as")?> <b><?php $groups = $userObj->getGroupDescription();
						if($group_name == sfConfig::get('app_pfm_role_ewallet_user')){
							$billObj = billServiceFactory::getService();
							$permittedPay4meType = $billObj->permittedPay4meType($userDetails[0]['ewallet_type']);
							if($permittedPay4meType) {
								$groups[0] = 'Pay4me User';
							}
						}
						echo $groups[0];
						if($group_name == sfConfig::get('app_pfm_role_bank_branch_user') || $group_name == sfConfig::get('app_pfm_role_bank_branch_report_user') ){
							$branch = $userObj->getGuardUser()->getBankUser()->getFirst()->getBankBranch()->getName();
							print "<br>Branch Name - ".$branch;
						}?>
						</b>
					<?php }?>
					<?php if ($userDetails[0]['ewallet_type'] == 'guest_user')
						echo '<br />You are Guest';
					?>
				</div><br />
			<?php } ?>
		</div>
		<div class="clear"></div>
	</div>
	<div id="wrapperLogo"><?php echo image_tag('/img/pay4me_logo.png',array('title'=>"Pay4me", 'alt'=>"Pay4me")); ?></div>
</div>

<script>
  function setLanguage(lan) {
      var url = "<?php echo url_for("report/setLanguage");?>";
       $("#language").load(url, { language:lan},function (data){                
                    location.reload();               
       });
      
  }
  function scroll_left(move){
     
      var SMPoint = document.getElementById("SMPoint").value;

      var totMenu = document.getElementById("totMenu").value;
      var totStaticMenu = document.getElementById("totStaticMenu").value;
      var work = false;
      var temp = 0;
          temp +=Number(SMPoint);
          temp +=Number(totStaticMenu);

      if("right"==move &&  (temp - 1) < totMenu){
            work = true;
            NewSMPoint = parseInt(SMPoint) + 1;
            document.getElementById("rightArrow").style.display="block";
            if( temp >= totMenu)
                document.getElementById("leftArrow").style.display="none";
          document.getElementById("SMPoint").value = NewSMPoint;
      }
      if("left"==move &&  SMPoint>1){
            work = true;
            NewSMPoint = SMPoint - 1;
            document.getElementById("leftArrow").style.display="block";
            if( NewSMPoint == 1)
                document.getElementById("rightArrow").style.display="none";
          document.getElementById("SMPoint").value = NewSMPoint;
      }

      if(work)
         {
             var url = "<?php echo url_for("globalSetting/getMenu");?>";

                $("#menuDiv").load(url, { SMPoint:NewSMPoint, TotMenu:totStaticMenu},function (data){
                if(data=='logout'){
                    location.reload();
                }

            });
         } //work
   }
   function chkValidation()
{
    var mapping= confirm("Are you sure you want to logout from Google/Yahoo/Facebook Account?");
    if (mapping== true)
    {return true;
    }else
        return false;
}
</script>
