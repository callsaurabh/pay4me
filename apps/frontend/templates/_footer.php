<div class="clearfix"></div>
<div id="wrapFoot">
<div id="main">
      <div id="copyrightInfo">Copyright © <?php echo date('Y');?> Pay4me. <?php echo __('All rights reserved.')?></div>
      <ul id="footnav">
        <li><?php echo link_to(__('Home'),'@homepage') ?></li>
        <li><?php echo link_to(__('About pay4me'),'page/aboutUs') ?></li>
        <li><?php echo link_to(__('Terms of use'),'page/termsOfUse') ?></li>
        <li><?php echo mail_to('support@pay4me.com',__('Contact Us'),array('class'=>'one')); ?></li>
      </ul>
      <div class="clearfix"></div>
      </div>
    </div>

<noscript>
  <div id="noscript">
    <div>
      <h1>Warning !</h1> <h3>This site requires Javascripts </h3><p>Your browser does not support Javascripts, or its is disabled.</p>
    </div>
  </div>
</noscript>
