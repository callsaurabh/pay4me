<?php
//use_stylesheet('master');
//use_stylesheet('admin_main');
//use_stylesheet('admin_theme','last');
//use_stylesheet('reset','first');
//use_stylesheet('default','first');
//use_stylesheet('menu','first');
use_javascript('jquery-1.4.2.min.js','first');
use_javascript('jquery-ui.min.js','first');
use_javascript('general.js','first');
use_javascript('menu.js','last');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Pay4Me</title>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_combined_stylesheets() ?>
    <?php include_combined_javascripts() ?>

    <!--[if IE 6]>
<?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">      
      <?php  include_partial('global/headerAdmin');?>
      <div id="wrapperMid">
      <div id="navArea" class="navMenu">
        <?php
           include_partial('global/menuAdmin');
        ?>
      </div>
    
      <!--<div id="loadArea">-->
        <?php echo $sf_content ?>
      <!--</div>-->
      <div class="clear"></div>



      <?php include_partial('global/footerAdmin');?>
    </div>
    </div>
  </body>

</html>
<script>
  // Print Page Script
  function printMe(contentId,raw){
    //alert('printME');
    if($('#printFrame').length){
      $('#printFrame').remove();
    }
    printFrame = "<iframe id='printFrame' width='1' height='1' src='<?php echo url_for('@printme'); ?>'></iframe>";
    $('body').append(printFrame);

    $('#printFrame').load(function(){
      if(raw === undefined || raw == false){
        printContentId  = contentId;
        html = $('#'+contentId).html();
      }else{
        html = contentId;
      }
      //  alert(html);
      $('#printFrame').contents().find("body #printBody").append(html);
      objFrame = window.frames[0];
      objFrame.focus();
      objFrame.print();
      return;
      //rmFrame = setInterval(function()clearInterval(rmFrame);},2000);

    });

  }
</script>