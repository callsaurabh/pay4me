<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Admin:</title>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php use_stylesheet('print','',array('media'=>'print'));?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_combined_stylesheets() ?>
    <?php include_combined_javascripts() ?>

<!--[if IE 6]>
    <?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
  </head>
  <body>
  <div id="wrapperPrintTop">
    <div id="wrapperLogo"><?php echo image_tag('/img/pay4me_logo.png',array('title'=>"Pay4me", 'alt'=>"Pay4me")); ?></div>
  </div>

<div id="wrapperPrint">
  <div id="wrapperMid">
    <div id="tpcurve">
      <div id="tpcurveControltop"></div>
      <div id="tpcurveLoadAreatop"></div>
    </div>
    <div id="btcurve">
      <div id="btcurveControlbottom"></div>
      <div id="btcurveLoadAreabottom"></div>

    </div>
    
    <div id="loadAreaPrint">
      <div class="descriptionArea">
        <div id="content">
            <?php echo $sf_content ?>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <?php include_partial('global/footer');?>
</div>
</html>
