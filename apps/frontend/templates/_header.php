<div id="wrapNav">
      <div id="thenav" class="navMenu"> 
       <?php echo verisign() ?>
       
        <ul id="topnav" >
          <li>
            <h6><?php echo link_to(__('Features'),'page/features') ?></h6>
          </li>
          <li >
            <h6><?php echo link_to(__('Services'),'page/services') ?></h6>
          </li>
          <!-- <li>
            <h6> --><?php //echo link_to(__('Pricing'),'page/pricing') ?><!-- </h6>
          </li> -->
          <li>
            <h6 ><?php echo link_to(__('Merchant'),'page/merchantService') ?></h6>
          </li>
          <li>
            <h6><?php echo link_to(__('FAQs'),'page/Faq', array('title'=>"Frequently Asked Questions")) ?></h6>
          </li>
          <li>
            <h6><?php echo link_to(__('Support'),'page/feedBack') ?></h6>
          </li>
          <li>
            <h6><?php echo link_to(__('Contact'),'page/contactUs', array('style'=>"border-right:none;")) ?></h6>
          </li>
<!--          <li>
            <h6><?php // echo link_to(__('Merchant'),'page/merchant') ?></h6>
          </li>
          <li>
            <h6><?php // echo link_to(__('Bank'),'page/bank') ?></h6>
          </li> -->
        </ul>
        
      </div>
      <!--<ul id="secnav">
        <li>
          <h6><?php //echo link_to(__('Home'),'@homepage') ?></h6>
        </li>
        <li>class="submen
          <h6><?php //echo link_to(__('About pay4me'),'page/aboutUs') ?></h6>
        </li>
        <li>
          <h6><?php //echo link_to(__('Contact'),'page/contactUs') ?></h6>
        </li>
      </ul>-->
      <div class="clearfix"></div>

    </div>
<div id="thetop">
<div class="call-msg">For Support Call: <?php echo sfConfig::get('app_support_contact_number');?> or Email <a href="mailto:support@pay4me.com">support@pay4me.com</a></div>
      <div id="theLogo"><?php echo image_tag('/img/new/logo-pay4me.gif',array('alt'=>'Pay4Me', 'width' => 261, 'height' => 33, 'border' => 0, 'usemap' => '#Map')); ?>
        <map name="Map" id="Map">
          <area shape="rect" coords="4,2,158,30" href="<?php echo url_for('@homepage'); ?>" />
        </map>
      </div>
      <div class="clearfix"></div>
    <div></div>
<div class="clear"></div>
</div>
<script type="text/javascript">
$(function() {
    $('.submen').hide();
    $('.list').hover( function() { 
        $('.submen').toggle();
        $(this).toggleClass("selected");
     } );
});
            </script>
