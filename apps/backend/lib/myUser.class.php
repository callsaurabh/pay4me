<?php

class myUser extends sfGuardSecurityUser
{
  /**
   *
   * @return boolean true if current logged in user is portal admin, false otherwise 
   */
  public function isPortalAdmin() {
    if (!$this->isAuthenticated()) {
      return false;
    }
    if ($this->isSuperAdmin()) {
      return true;
    }
    $currentGroups = $this->getGroupNames();
    $adminGroups = AclHelper::$ADMIN_ROLES;
    $matchingGroups = array_intersect($currentGroups, $adminGroups);
    if (count($matchingGroups)) {
      return true;
    }
    return false;
  }

  /**
   * Returns the office associated with the current logged in user
   *
   * @param array $officeType Type of office in which to search the user
   * @return IUserOffice
   */
  public function getUserOffice ($officeType = null) {
    return new UserOffice($officeType);
  }
}
