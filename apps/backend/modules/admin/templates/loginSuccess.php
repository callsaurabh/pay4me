<?php echo ePortal_pagehead('Site Administrator',array('class'=>'_form')); ?>
<div id="contentBody">
  <div id="loginForm">
    <div id="loginHead"><h1>Administrator Login</h1></div>
    <form class="dlForm XY20">
      <div style="width:60%;margin:0px auto;">
        <dl>
          <dt><label>Username <sup>*</sup>:</label></dt>
          <dd><input type="text"> </dd>
        </dl>
        <dl>
          <dt><label>Password <sup>*</sup>:</label></dt>
          <dd><input type="text"> </dd>
        </dl>
        <dl>
          <dt></dt>
          <dd><button>Submit</button> </dd>
        </dl>
        <dl>
          <dt></dt>
          <dd><a href="#">Forgotten Password</a></dd>
        </dl>
      </div>
    </form>
    
  </div>
</div>
