<?php

/**
 * passport actions.
 *
 * @package    symfony
 * @subpackage passport
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class DashboardActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $pageName = $request->getParameter('p');
    // echo "<pre>";print_r($pageName) ;
    //$this->getResponse()->setCookie('WelcomeMessage', 'newOne', 0, '/',0 , true);
    if(isset($pageName) && !empty($pageName) && $pageName !='welcome')
    {
      $this->setTemplate($pageName);
    }else{
      $this->welcomeValue = '';
      $this->getResponse()->setCookie('WelcomeMessage', 'newOne',0);
    }
    $this->pageTitle = 'Dashboard';
    $this->setTemplate('welcome');
    
  }




}
