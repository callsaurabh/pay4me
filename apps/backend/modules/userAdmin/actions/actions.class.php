<?php

/**
 * userAdmin actions.
 *
 * @package    symfony
 * @subpackage userAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class userAdminActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->sf_guard_user_list = Doctrine::getTable('sfGuardUser')
    ->createQuery('a')
    ->execute();
  }

  
  public function executeChangePassword(sfWebRequest $request)
  {
    $this->form = new ChangeUserForm($this->getUser()->getGuardUser());

    $this->setTemplate('change');
  }

    //save the new password
  public function executeSavechange(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
    #set last login time
    //$this->form->setDefault('last_login',date('Y-m-d h:i:s'));

    if ($this->processForm($request, $this->form, false)) {
      // print_r($this->getUser()->getGuardUser()->setLastLogin(date('Y-m-d h:i:s'))); die('Settings last login');
      $this->getUser()->setFlash('notice',"Password Changed Successfully",false);

      $this->forward($this->getModuleName(), 'changePassword');
    }
    $this->setTemplate('change');
  } 

  protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true)
  {

    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid())
    {
      $sf_guard_user = $form->save();
      if ($redirect)
      $this->forward('userAdmin','edit');
      return true;
    }
    //sfContext::getInstance()->getLogger()->info("Form invalid");
    return false;
  }

}
