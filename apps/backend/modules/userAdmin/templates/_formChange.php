<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form action="<?php echo url_for('userAdmin/savechange'); ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table align="center" style="width:600px;margin:auto;">
    <tfoot>
      <tr>
        <td colspan="2">
             <dl>
              <dt></dt>
              <dd> <input type="submit" value="Change Password" /></dd>
             </dl>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
      <td>
      <?php echo $form ?>
      </td>
      </tr>
    </tbody>
  </table>
</form>
