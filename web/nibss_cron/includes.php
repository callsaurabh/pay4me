<?php
require_once 'excelReader/reader.php';
require_once("database.php") ;
include('csvreader/FileReader.php' );
include( 'csvreader/CSVReader.php' );

function saveData($data, $success = 0, $fileTime)
{
	($success == 0)?$success = 1: $success = 0 ;
	if($data[1] || $data[2] || $data[3] || $data[4] || $data[5] || $data[6])
		$isUpdated = updatePayment($data[1],$data[2],$data[3],$data[4],$data[5],$data[6], $success, $fileTime  ) ;

}


function readXLSFile($fileName, $mandateId, $fileType)
{
  print "<br>sdsds";
	$data = new Spreadsheet_Excel_Reader();


// Set output Encoding.
$data->setOutputEncoding('CP1251');
$data->read($fileName);


	error_reporting(E_ALL ^ E_NOTICE);
	if($fileType == "PAYMENT")
	{
		$status = 23;  // 23 PAID
		if($data->sheets[0]['numRows'] > 0)
		{
			for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) 
			{
				$payment_record_id = $data->sheets[0]['cells'][$i][1] ;
				if(isPaymentRecord($mandateId, $payment_record_id) )
				{
					updatePaymentRecord($payment_record_id, $status, date("Ymd")) ;
				}
			}
		}
		if($data->sheets[1]['numRows'] > 0)
		{
			$status = 82;  // 82 UNPAID
			for ($i = 1; $i <= $data->sheets[1]['numRows']; $i++) 
			{
				$payment_record_id = $data->sheets[1]['cells'][$i][1] ;
				if(isPaymentRecord($mandateId, $payment_record_id ) )
				{
					updatePaymentRecord($payment_record_id, $status, date("Ymd")) ;
				}
			}
		}
	}
}

function readCSVFile($fileName, $mandateId, $fileType)
{	
	$reader =& new CSVReader( new FileReader( $fileName ) );	
	$reader->setSeparator( ',' );

	if($fileType == "VALID" || $fileType == "INVALID" )
	{
		($fileType == "VALID")? $status = 22: $status = 81 ; // 22 VALID and 81 INVALID
		$i = 0 ;
		while( false != ( $cell = $reader->next() ) )
		{
      
			if(isPaymentRecord($mandateId, $cell[0]) )
			{
				updatePaidUnpaid($cell[0], $status, date("Ymd")) ;
			}		
			$i++ ;
		}
	}
	

}

//Anurag: Attachement with the mail start	
function getAttachements($mbox,$msgno)
{

		$struct = imap_fetchstructure($mbox,$msgno);
		$contentParts = count($struct->parts);
   
		if ($contentParts >= 2) {
		   for ($i=2;$i<=$contentParts;$i++) {
				$att[$i-2] = imap_bodystruct($mbox,$msgno,$i);
			}
			for ($k=0;$k<sizeof($att);$k++) {
				if ($att[$k]->parameters[0]->value == "us-ascii" || $att[$k]->parameters[0]->value    == "US-ASCII") {
					if ($att[$k]->parameters[1]->value != "") {
						$selectBoxDisplay[$k] = $att[$k]->parameters[1]->value;
					}
				} elseif ($att[$k]->parameters[0]->value != "iso-8859-1" &&    $att[$k]->parameters[0]->value != "ISO-8859-1") {
					$selectBoxDisplay[$k] = $att[$k]->parameters[0]->value;
				}
			}
		}
   
		if (sizeof($selectBoxDisplay) > 0) 
		{			
			;
			for ($j=0;$j<sizeof($selectBoxDisplay);$j++) 
			{
				$file = $selectBoxDisplay[$j] ; 
				$strFileName = $file ; //$att[$file]->parameters[0]->value;
   				$strFileType = strrev(substr(strrev($strFileName),0,4));
   				$fileContent = imap_fetchbody($mbox,$msgno,$j+2);
				if ($strFileType == ".xls" || $strFileType == ".csv"  )
				{
          
					$name = "files/".$file ; 
					$f=fopen($name,'wb');
					fwrite($f,imap_base64($fileContent),strlen($fileContent));
					fclose($f);

					$receivedFile = explode(' ', trim($file)) ;
          $file_prefix = $receivedFile[0];
					$fileStatusType = explode('.', trim($receivedFile[1])) ; 
					$mandate_file_id = explode('_', $receivedFile[0]) ;
					if($groupDetail = getPaymentBatch($mandate_file_id[2],$file_prefix )) // $this->PaymentMandate->find(array('PaymentMandate.id' => $mandate_file_id[2] )))
					{


						if($fileStatusType[0] == 'INVALID' || $fileStatusType[0] == 'VALID' || $fileStatusType[0] == 'PAYMENT' )
						{
							if($strFileType == ".xls" && $fileStatusType[0] == 'PAYMENT')
							{
               
								readXLSFile($name, $mandate_file_id[2], $fileStatusType[0]) ;
							}
							elseif($strFileType == ".csv" && ($fileStatusType[0] == 'INVALID' || $fileStatusType[0] == 'VALID'))

								readCSVFile($name, $mandate_file_id[2], $fileStatusType[0]) ;
							
						}
					}
				}
			}
		} 	   

}   //Attachement with the mail ends



?>