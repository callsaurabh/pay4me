function validate_admin_bankUser_form(){
    if(document.getElementById('txnId').value == ""){
        alert("Please enter the Transaction Id !!!");
        return false;
    }
}

function validate_nis_index_form(){
    if(document.getElementById('txnId').value == "" && document.getElementById('appId').value == "" && document.getElementById('refNo').value == "" && document.getElementById('type').value == ""){
        //        alert("Please search through Transaction Id (OR) through all options [Application Id / Reference Number / Application Type] !!!"); #commented by iqbal at 13 Nov 2009 for bug No. 13855
        alert("Please enter Transaction Id OR Application Id / Reference Number / Application Type"); // Added by Iqbal at 13 Nov 2009 fixed bug 13855
        return false;
    }
    if(document.getElementById('txnId').value == "" && (document.getElementById('appId').value == "" || document.getElementById('refNo').value == "" || document.getElementById('type').value == "")){
        //        alert("Please search through all options [Application Id / Reference Number / Application Type] !!!"); #commented by iqbal at 13 Nov 2009 for bug No. 13855
        alert("Please enter Application Id / Reference Number / Application Type"); // Added by Iqbal at 13 Nov 2009 fixed bug 13855
        return false;
    }
}
function validate_report_mrc_form(){
    /* =================[ START ] Date Field Validations====================  */
    if(document.getElementById('from_date').value == '' || document.getElementById('to_date').value == ''){
        alert('Please Select all Date through calander.');
        document.getElementById('from_date').focus();
        return false;
    }
    if((document.getElementById('from_date').value != '') && (document.getElementById('to_date').value != '')){
        var start_date = document.getElementById('from_date').value;
        var end_date = document.getElementById('to_date').value;
        start_date = new Date(start_date.split('-')[2],start_date.split('-')[1]-1,start_date.split('-')[0]);
        end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

        if(start_date.getTime()>end_date.getTime()) {
            alert("<From date> cannot be greater than <To date>");
            document.getElementById('from_date').focus();
            return false;
        }
    }
/* ================= [ END ] Date Field Validations ====================  */
}
function validate_report_index_form(){


    var err=false;

    var today=new Date();

    var start_date,end_date;
    // end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);
    if((document.getElementById('from_date').value != '') ) {
        start_date = document.getElementById('from_date').value;

        start_date = new Date(start_date.split('-')[2],start_date.split('-')[1]-1,start_date.split('-')[0]);

        if(start_date.getTime() > today.getTime()){
            document.getElementById('err_from_date').innerHTML='From Date cannot be Future Date';
            err=true;
        }
        else
        {
            err=false;
            document.getElementById('err_from_date').innerHTML='';
        }
    }
    if((document.getElementById('to_date').value != '') ) {

        end_date = document.getElementById('to_date').value;
        end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

        if(end_date.getTime()>today.getTime()){

            document.getElementById('err_to_date').innerHTML='To Date cannot be Future Date';

            err=true;
        }
        else
        {
            //            err=!err;
            document.getElementById('err_to_date').innerHTML='';
        }
    }


    if((document.getElementById('from_date').value != '') && (document.getElementById('to_date').value != '')){
        start_date = document.getElementById('from_date').value;
        end_date = document.getElementById('to_date').value;
        start_date = new Date(start_date.split('-')[2],start_date.split('-')[1]-1,start_date.split('-')[0]);
        end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

        if(start_date.getTime()>end_date.getTime() ) {

            document.getElementById('err_to_date').innerHTML='To Date should be greater than From Date';
            err=true;;
        }


    }

    if(err)
    {
        return false;
    }
/* ================= [ END ] Date Field Validations ====================  */
}


/* ================= [ START ] Auto Chain on Servict Type ====================  */

function set_service_type(){
    var url="";
    //alert(page);
    var mId=document.getElementById('merchant').value;
    var repLabel =document.getElementById('report_label').value;
    var merchantId = "";
    if(mId == ''){
        merchantId = "";
        document.getElementById('service_type').value='';
        document.getElementById('service_type').disabled='disabled';
    }else{
        document.getElementById('service_type').disabled='';
        merchantId=mId;
    }

    /*if(document.getElementById('country_id')){
    var countryId =document.getElementById('country_id').value;
    if(countryId==''){
        document.getElementById('bank_branches').disabled='disabled';
    }else{
         document.getElementById('bank_branches').disabled='';

    }
    }*/

    var request = false;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if(repLabel == '' || repLabel == 'MerchantPerBankReport' ){
        url = "chainServiceType?id="+merchantId;
    }
    else if(repLabel == 'Merchant Support Section'){
		 url = "chainServiceType?id="+merchantId;
	}
    else{
        url = "report/chainServiceType?id="+merchantId;
    }

    request.open("GET", url, true);
    request.onreadystatechange = getServiceType;
    request.send(null);
	if(repLabel != 'MerchantPerBankReport'){
		set_payment_mode();
	}
}

    function getServiceType() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                var applicationType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var applicationTypes= applicationType[1].split('|');

                for(var i=document.pfm_report_form.service_type.options.length-1;i >=0 ;i--) {
                    document.pfm_report_form.service_type.remove(i);
                }
                var optn = document.createElement("OPTION");
                optn.text = "-- All Services --";
                optn.value = "";
                document.pfm_report_form.service_type.options.add(optn);
                if(applicationTypes.length > 1){
                    for (i=0;i<applicationTypes.length-1;i++) {
                        var arr=applicationTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.pfm_report_form.service_type.options.add(optn);

                    }
                }
            }
        }

    }







function set_service_type_bank_report(prefix){


    var url="";
    //alert(page);
    var mId=document.getElementById('bank_branch_report_merchant').value;
    //var repLabel =document.getElementById('report_label').value;
    var merchantId = "";
    if(mId == ''){
        merchantId = "";
        document.getElementById('bank_branch_report_service_type').disabled='disabled';
    }else{
        document.getElementById('bank_branch_report_service_type').disabled='';
        merchantId=mId;
    }
    var request = false;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }

    url = "chainServiceType?id="+merchantId;


    request.open("GET", url, true);
    request.onreadystatechange = getServiceType;
    request.send(null);

    function getServiceType() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                var applicationType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var applicationTypes= applicationType[1].split('|');

                for(var i=document.pfm_report_form.bank_branch_report_service_type.options.length-1;i >=0 ;i--) {
                    document.pfm_report_form.bank_branch_report_service_type.remove(i);
                }
                var optn = document.createElement("OPTION");
                optn.text = "-- All Services --";
                optn.value = "";
                document.pfm_report_form.bank_branch_report_service_type.options.add(optn);
                if(applicationTypes.length > 1){
                    for (i=0;i<applicationTypes.length-1;i++) {
                        var arr=applicationTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.pfm_report_form.bank_branch_report_service_type.options.add(optn);

                    }
                }
            }
        }
    }

    set_Currency_wthPre(prefix);

}


/* ================= [ END ] Auto Chain on Merchant Servict Type ====================  */

function set_merchant_service_type(){
    var url="";
    //alert(page);
    var mId=document.getElementById('merchant').value;
    // var repLabel =document.getElementById('report_label').value;
    var merchantId = "";
    if(mId == ''){
        merchantId = "";
        document.getElementById('service_type').disabled='disabled';
    }else{
        document.getElementById('service_type').disabled='';
        merchantId=mId;
    }
    var request = false;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //    if(repLabel == ''){
    //        url = "chainServiceType?id="+merchantId;
    //    }else{
    //        url = "report/chainServiceType?id="+merchantId;
    //    }

    url = "chainServiceType?id="+merchantId;
    request.open("GET", url, true);
    request.onreadystatechange = getMerchantServiceType;
    request.send(null);

    function getMerchantServiceType() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                var applicationType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var applicationTypes= applicationType[1].split('|');

                for(var i=document.pfm_merchant_request_form.service_type.options.length-1;i >=0 ;i--) {
                    document.pfm_merchant_request_form.service_type.remove(i);
                }
                var optn = document.createElement("OPTION");
                optn.text = "Select Merchant Services";
                optn.value = "";
                document.pfm_merchant_request_form.service_type.options.add(optn);
                if(applicationTypes.length > 1){
                    for (i=0;i<applicationTypes.length-1;i++) {
                        var arr=applicationTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.pfm_merchant_request_form.service_type.options.add(optn);

                    }
                }
            }
        }
    }
//}
}

/* ================= [ END ] Auto Chain on Servict Type ====================  */

/* ================= [ END ] Auto Chain on Payment mode Servict Type ====================  */

function set_payment_service_type(){
    var url="";
    //alert(page);
    var mId=document.getElementById('payment').value;
    // var repLabel =document.getElementById('report_label').value;
    var paymentId = "";
    if(mId == ''){
        paymentId = "";
        document.getElementById('payment_mode').disabled='disabled';
    }else{
        document.getElementById('payment_mode').disabled='';
        paymentId=mId;
    }
    var request = false;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //    if(repLabel == ''){
    //        url = "chainServiceType?id="+merchantId;
    //    }else{
    //        url = "report/chainServiceType?id="+merchantId;
    //    }

    url = "paymentServiceType?id="+paymentId;
    request.open("GET", url, true);
    request.onreadystatechange = getPaymentServiceType;
    request.send(null);

    function getPaymentServiceType() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                var applicationType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var applicationTypes= applicationType[1].split('|');

                for(var i=document.pfm_merchant_request_form.payment_mode.options.length-1;i >=0 ;i--) {
                    document.pfm_merchant_request_form.payment_mode.remove(i);
                }
                var optn = document.createElement("OPTION");
                optn.text = "Select Payment Mode Options";
                optn.value = "";
                document.pfm_merchant_request_form.payment_mode.options.add(optn);
                if(applicationTypes.length > 1){
                    for (i=0;i<applicationTypes.length-1;i++) {
                        var arr=applicationTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.pfm_merchant_request_form.payment_mode.options.add(optn);

                    }
                }
            }
        }
    }
//}
}

/* ================= [ END ] Auto Chain on Payment Type ====================  */

/* ================= [ START ] Auto Chain on Bank Branches ====================  */

function set_bank_branches(){
    var url="";
    var bId=document.getElementById('banks').value;
    var bankId = "";
    /*if(bId == ''){
        bankId = "";
        document.getElementById('bank_branches').disabled='disabled';
    }else{
        document.getElementById('bank_branches').disabled='';
        bankId=bId;
    }*/
    var request = false;
    var repLabel =document.getElementById('report_label').value;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // url = "report/chainBranchType?id="+bankId;
    if(repLabel == ''){
        url = "chainBranchType?id="+bankId;
    }else{
        url = "report/chainBranchType?id="+bankId;
    }
    request.open("GET", url, true);
    request.onreadystatechange = getBankBranches;
    request.send(null);

    function getBankBranches() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                var branchType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var branchTypes= branchType[1].split('|');

                for(var i=document.pfm_report_form.bank_branches.options.length-1;i >=0 ;i--) {
                    document.pfm_report_form.bank_branches.remove(i);
                }
                var optn = document.createElement("OPTION");
                optn.text = "-- All Branches --";
                optn.value = "";
                document.pfm_report_form.bank_branches.options.add(optn);
                if(branchTypes.length > 1){
                    for (i=0;i<branchTypes.length-1;i++) {
                        var arr=branchTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.pfm_report_form.bank_branches.options.add(optn);

                    }
                }
            }
        }
    }
//}
}

/* ================= [ END ] Auto Chain on Bank Branches ====================  */

/* ================= [ START ] Auto Chain on Bank Branches ====================  */

function set_bank_branches_report(){


    var url="";
    var bId=document.getElementById('bank_branch_report_banks').value;
    var bankId = "";
    if(bId == ''){
        bankId = "";
        if(document.getElementById('bank_branch_report_bank_branch'))
            document.getElementById('bank_branch_report_bank_branch').disabled='disabled';
    }else{
        if(document.getElementById('bank_branch_report_bank_branch'))
            document.getElementById('bank_branch_report_bank_branch').disabled='';
        bankId=bId;
    }
    var request = false;
    //var repLabel =document.getElementById('report_label').value;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // url = "report/chainBranchType?id="+bankId;
    // if(repLabel == ''){
    url = "chainBranchType?id="+bankId;
    // }else{
    //    url = "report/chainBranchType?id="+bankId;
    //}
    request.open("GET", url, true);
    request.onreadystatechange = getBankBranches;
    request.send(null);

    function getBankBranches() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                var branchType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var branchTypes= branchType[1].split('|');

                for(var i=document.pfm_report_form.bank_branch_report_bank_branch.options.length-1;i >=0 ;i--) {
                    document.pfm_report_form.bank_branch_report_bank_branch.remove(i);
                }
                var optn = document.createElement("OPTION");
                optn.text = "-- All Branches --";
                optn.value = "";
                document.pfm_report_form.bank_branch_report_bank_branch.options.add(optn);
                if(branchTypes.length > 1){
                    for (i=0;i<branchTypes.length-1;i++) {
                        var arr=branchTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.pfm_report_form.bank_branch_report_bank_branch.options.add(optn);

                    }
                }
            }
        }
    }
//}
}

/* ================= [ END ] Auto Chain on Bank Branches ====================  */




/* ================= [ START ] Show Hide Div of Bank and Branches  ====================  */

function show_hide_element(referenceId,actionId){
    if(document.getElementById(referenceId).value == 1){
        document.getElementById(actionId).style.display = 'block';
        set_bank_branches();
    }else{
        document.getElementById(actionId).style.display = 'none';
    }
}

/* ================= [ END ] Show Hide Div of Bank and Branches ====================  */
/* ================= [ START ] Show Hide Div of Bank and Branches  ====================  */

function show_hide_element_report(referenceId,actionFlg,prefix){

    var flg;
    flg=actionFlg;
    if(document.getElementById(referenceId).value == 1 || document.getElementById(referenceId).value == 14 || document.getElementById(referenceId).value == 15){
        document.getElementById("bankbrach").style.display='block';
        if(flg=='bankbranch')
            set_bank_branches_report();
    }else{

        document.getElementById("bankbrach").style.display='none';
    }
    set_Currency_wthPre(prefix);
}

/* ================= [ END ] Show Hide Div of Bank and Branches ====================  */

/* ================= [ START ] Calculate Date differece  ====================  */

/*function calculate (sDate,eDate)
    {
        $('#err_sdate').html('');
        var dateStr1 = sDate;
        dateStr1 = dateStr1.replace(/-/gi,'/');
        var dateStr2 = eDate;
        dateStr2 = dateStr2.replace(/-/gi,'/');
        var date1 = new Date(dateStr1);
        var date2 = new Date(dateStr2);

        var sec = date2.getTime() - date1.getTime();
        if (isNaN(sec))
        {
            $('#err_edate').html('Input data is incorrect!!');
            return false;
        }
        if (sec < 0)
        {
            $('#err_edate').html('The end date ocurred earlier than the start date !');
            return false;
        }
        return true;
    }*/
function calculate (sDate,eDate)
{
    $('#err_sdate').html('');
    $('#err_edate').html('');
    var sec = eDate - sDate;
    if (sec < 0)
    {
        $('#err_edate').html('Start Date should be less than End Date');
        return false;
    }
    return true;
}

/* ================= [ END ] Calculate Date differece ====================  */

/* ================= [ START ] Validate Date ====================  */

function validateDate(y, mo, d, h, mi, s)
{
    var date = new Date(y, mo - 1, d, h, mi, s, 0);
    var ny = date.getFullYear();
    var nmo = date.getMonth() + 1;
    var nd = date.getDate();
    var nh = date.getHours();
    var nmi = date.getMinutes();
    var ns = date.getSeconds();

    if(ny == y && nmo == mo && nd == d && nh == h && h != '' && nmi == mi && mi != '' && ns == s && s != '')
    {
        return date.getTime();
    }
    else
        return false;
}
/* ================= [ END ] Validate Date ====================  */



/* ================= [ START ] Auto Chain on Country Code ====================  */


/* ================= [ START ] Show Hide Div of Bank and Branches  for suumery and detail page====================  */

function show_hide_element_summery(referenceId,actionId){
    if(document.getElementById(referenceId).value == 1){
        document.getElementById(actionId).style.display = 'block';
        set_bank_branches();

    }else{
        document.getElementById(actionId).style.display = 'none';
    }

    if(document.getElementById('currency_id')){

        set_Currency();

    }
}


/* ================= [ END ] Show Hide Div of Bank and Branches ====================  */

/* ================= [ START ] Currency Code Drop Down ====================  */

function set_Currency(){


    var url="";
    if(document.getElementById('payment_mode'))
        var pId=document.getElementById('payment_mode').value;
    else
        var pId='';
    var paymentId=pId;
    var mId=document.getElementById('merchant').value;
    var merchantId=mId;
    var sId=document.getElementById('service_type').value;
    var serviceId=sId;

    var request = false;
    var repLabel =document.getElementById('report_label').value;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if(paymentId=='Bank')
        paymentId='';
    if(repLabel == ''){

        url = "CurrencyCode?mId="+merchantId + "&sId= "+serviceId+'&id='+paymentId ;
    }
    else if(repLabel == 'Merchant Support Section'){

        url = "CurrencyCode?mId="+merchantId + "&sId= "+serviceId+'&id='+paymentId ;
    }
    else{
        url = "report/CurrencyCode?mId="+merchantId + "&sId="+serviceId+'&id='+paymentId ;
    }
    request.open("GET", url, true);
    request.onreadystatechange = getCurrencyCode;
    request.send(null);


    function getCurrencyCode() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                if(response != ''){
                    var currencyType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                    var currencyTypes= currencyType[1].split('|');

                    for(var i=document.pfm_report_form.currency_id.options.length-1;i >=0 ;i--) {
                        document.pfm_report_form.currency_id.remove(i);
                    }
                    //var optn = document.createElement("OPTION");
                    //optn.text = "Naira";
                    //optn.value = "1";
                    //document.pfm_report_form.currency_id.options.add(optn);
                    if(currencyTypes.length > 1){
                        for (i=0;i<currencyTypes.length-1;i++) {
                            var arr=currencyTypes[i].split('.');
                            var optn = document.createElement("OPTION");
                            optn.text = arr[1];
                            var a=arr[1].replace(/\s+$/,"");
                            optn.value = arr[0];
                            document.pfm_report_form.currency_id.options.add(optn);

                        }
                    }
                }
            }
            set_currency_by_country();

        }
    }

// }
}

function set_currency_by_country(){
    var countryId = $("#country_id").val();
    if(countryId!=""){
        var url = "report/getSelectedCurrency";
        $.post(url,{
            country: countryId
        },function (data){
            if(data){

                $("#currency_id").val(data);
            }
        });

    }


}

function set_Currency_wthPre(prefix){


    var url="";
    if(document.getElementById(prefix+'_payment_mode'))
        var pId=document.getElementById(prefix+'_payment_mode').value;
    else
        var pId='';
    var paymentId=pId;
    var mId=document.getElementById(prefix+'_merchant').value;
    var merchantId=mId;
    var sId=document.getElementById(prefix+'_service_type').value;
    var serviceId=sId;

    var request = false;

    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if(paymentId=='Bank')
        paymentId='';
    url = "CurrencyCode?mId="+merchantId + "&sId="+serviceId+'&id='+paymentId ;
    request.open("GET", url, true);
    request.onreadystatechange = getCurrencyCode;
    request.send(null);

    function getCurrencyCode() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;

                var currencyType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var currencyTypes= currencyType[1].split('|');

                for(var i=document.getElementById(prefix+'_currency').options.length-1;i >=0 ;i--) {
                    document.getElementById(prefix+'_currency').remove(i);
                }
                //var optn = document.createElement("OPTION");
                //optn.text = "Naira";
                //optn.value = "1";
                //document.pfm_report_form.currency_id.options.add(optn);
                if(currencyTypes.length > 1){
                    for (i=0;i<currencyTypes.length-1;i++) {
                        var arr=currencyTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.getElementById(prefix+'_currency').options.add(optn);

                    }
                }
            }
        }
    }

// }
}

//===================================================*/


/* ================= [ START ] Auto Chain on Bank Branches ====================  */

function set_country_bank_branches(){
    var url="";
    var bId=document.getElementById('bank_id').value;
    var countryId =document.getElementById('country_id').value;
    if(countryId==''){
        document.getElementById('bank_branches').disabled='disabled';
    }else{
        document.getElementById('bank_branches').disabled='';
    }

    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //myparam1=" + ls_parm1 + "?myparm2=" + ls_parm2
    url = "report/ContryBranchType?id="+bId + "&countryId="+countryId;

    request.open("GET", url, true);
    request.onreadystatechange = getCountryBankBranches;
    request.send(null);

    function getCountryBankBranches() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var response = request.responseText;
                var branchType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                var branchTypes= branchType[1].split('|');

                for(var i=document.pfm_report_form.bank_branches.options.length-1;i >=0 ;i--) {
                    document.pfm_report_form.bank_branches.remove(i);
                }
                var optn = document.createElement("OPTION");
                optn.text = "-- All Branches --";
                optn.value = "";
                document.pfm_report_form.bank_branches.options.add(optn);
                if(branchTypes.length > 1){
                    for (i=0;i<branchTypes.length-1;i++) {
                        var arr=branchTypes[i].split('.');
                        var optn = document.createElement("OPTION");
                        optn.text = arr[1];
                        var a=arr[1].replace(/\s+$/,"");
                        optn.value = arr[0];
                        document.pfm_report_form.bank_branches.options.add(optn);

                    }
                }
            }
        }
    }
//}
}





/* ================= [ START ] set_payment_mode====================  */
function checkIfMerchantCounter(payment_mode_options)
{

    options_length=document.getElementById('payment_mode').options.length;
    //alert(options_length);
    for(i=0;i<=options_length-1;i++){

        if(document.getElementById('payment_mode').options[i].value=='merchant-counter')
        {
            return true;
        }

    }
    return false;
}
function set_payment_mode(){

    var url="";
    var mId=document.getElementById('merchant').value;
    var merchantId=mId;
    var sId=document.getElementById('service_type').value;
    var serviceId=sId;
    var mc=false;
    var request = false;
    var repLabel =document.getElementById('report_label').value;
    if(window.XMLHttpRequest){ // For Mozilla, Safari, ...
        var request = new XMLHttpRequest();
    }else if(window.ActiveXObject){ // For Internet Explorer
        var request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    mc = checkIfMerchantCounter(document.getElementById('payment_mode').options);
//    if(document.getElementById('payment_mode') && bankUser && !mc )
//     {
        if(repLabel == ''){

            url = "PaymentMode?id="+merchantId + "&msId="+serviceId ;
        }
        else if(repLabel == 'Merchant Support Section'){
		 url = "PaymentMode?id="+merchantId + "&sId="+serviceId ;
	}
        else{
            url = "report/PaymentMode?id="+merchantId + "&sId="+serviceId;
        }


        function getPaymentMode() {
            if (request.readyState == 4) {
                if (request.status == 200) {
                    var response = request.responseText;

                    var paymentModeType=response.split('#$*&!%&^*$#@^&*$#@#@#@');
                    var paymentModeTypes= paymentModeType[1].split('|');

                    for(var i=document.pfm_report_form.payment_mode.options.length-1;i >=0 ;i--) {
                        document.pfm_report_form.payment_mode.remove(i);
                    }
                    var optn = document.createElement("OPTION");
                    optn.text = "--All Payment Mode--";
                    optn.value = "";
                    document.pfm_report_form.payment_mode.options.add(optn);
                    if(paymentModeType.length > 1){
                        for (i=0;i<paymentModeTypes.length-1;i++) {
                            var arr=paymentModeTypes[i].split('.');
                            var optn = document.createElement("OPTION");
                            optn.text = arr[1];
                            var a=arr[1].replace(/\s+$/,"");
                            optn.value = arr[0];
                            document.pfm_report_form.payment_mode.options.add(optn);

                        }
                    }
                }
            }
        }

        request.open("GET", url, true);
        request.onreadystatechange = getPaymentMode;
        request.send(null);
//    }

    if(document.getElementById('currency_id')){
        set_Currency()
    }

// }

}

/*Check*/
/*ReviewBoard*/
