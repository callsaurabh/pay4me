
var invalid_addr_msg = "Please enter Valid Address of length less than 255";
if($.browser.mozilla){  //returns true if IE, undefined if not (ie. not true/false)
    invalid_addr_msg = "<br>Please enter Valid Address of length less than 255";
}




// Accordian Navigation Document
jQuery().ready(function(){
    $('#controlMenu').accordion({
        autoHeight: false,
        navigation: true,
        header: '.crtlItem'
    });

    $(".testMenuItem").click(function(event){
        window.location.hash=this.hash;
    });
});

function validateString(str) {
        var reg = /^([A-Za-z0-9 ])+$/;
        if(reg.test(str) == false) {
            return true;
        }
        return false;
    }

function validateEmail(email) {
        var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if(reg.test(email) == false) {
            return false;
        }

        return true;
    }
function ajax_paginator(divId, uri)
{ 
    $.post(uri,$("#search").serialize(), function(data){
       if(data == 'logout'){
           location.reload();
        }else{
        $('#'+divId).html(data);
        }
    });
    return false;
}

function displayMerchantService(merchant_id,module,merchant_service_id,url)
{
    if(module!=""){
        var target_div = module+"_merchant_service_id";
    }
    else{
        var target_div = "merchant_service_id";
    }
    //  var url = "BankBranch";
    $('#'+target_div).load(url,{
        merchant_id: merchant_id,
        merchant_service_id: merchant_service_id
    },function (data){

        if(data=='logout'){
            location.reload();
        }
    });
}


function displayMerchantServiceParams(merchant_id,module,merchant_service_id,url)
{
    var target_div = module+"_params";
    //  var url = "BankBranch";
    $('#'+target_div).load(url, {
        merchant_id: merchant_id,
        merchant_service_id: merchant_service_id
    },function (data){

        if(data=='logout'){
            location.reload();
        }
    });
}



function getMerchantDetails(merchant_service_id, url)
{
    //alert(url);
  
    //  var url = "BankBranch";
    $('#response').load(url,
    {        
        merchant_service_id: merchant_service_id
    },null
    );
}

function getBranches(bank, url)
{
    //alert(bank);

    //  var url = "BankBranch";
    $('#response').load(url,
    {
        bank: bank
    },null
    );
}

function displayBankBranch(bank_id,module,bank_branch_id,url)
{
    var target_div = module+"_bank_branch_id";
    //  var url = "BankBranch";
    $('#'+target_div).load(url,
    {
        bank_id: bank_id,
        bank_branch_id: bank_branch_id
    },null
    );
}

function displayPaymentModeOption(payment_mode_id,module,payment_mode_option_id,url)
{
    var target_div = module+"_payment_mode_option_id";
    //  var url = "BankBranch";
    $('#'+target_div).load(url,
    {
        payment_mode_id: payment_mode_id,
        payment_mode_option_id: payment_mode_option_id
    },null
    );
}

function ajax_call(divId, uri, formId)
{

    $.post(uri,$("#"+formId).serialize(), function(data){
        $('#'+divId).html(data);
    //        if(data!='Username not Available')
    //            {
    //                $('#'+divId).removeClass('cRed');
    //                    $('#'+divId).addClass('cGreen');
    //            }
    //            else
    //                {
    //$('#'+divId).addClass('cRed');
    //                    $('#'+divId).removeClass('cGreen');
    //                }
    });

    return false;


}

function displayStates(country_id,module,state_id,url)
{
    var target_div = module+"_state_id";
    //  var url = "BankBranch";
    $('#'+target_div).load(url,
    {
        country_id: country_id,
        state_id: state_id
    },null
    );
}

function displayLgas(state_id,module,lga_id,url)
{
  
    var target_div = module+"_lga_id";
    //  var url = "BankBranch";
    $('#'+target_div).load(url,
    {
        lga_id: lga_id,
        state_id: state_id
        
    },null
    );
}

//function fetch_bank_details(){
//  if($("#bank_id :selected").val()!="")
//    {
//      var sel_val = $("#bank_id").val();
//      $('#username_suffix').html("."+sel_val);
//      $.post('fetchBankDetails',{bank_acronym: sel_val}, function(data){
//        if(data!=""){
//        document.getElementById('bank_user').style.visibility="visible";
//        document.getElementById('bank_user').style.height="";
//        $('#domain_suffix').html("<br>"+data);
//        document.getElementById('flash_error').style.visibility="hidden";
//        document.getElementById('flash_error').style.height="1px";
//        document.getElementById('flash_error').innerHTML = "";
//      }else{
//        document.getElementById('bank_user').style.visibility="hidden";
//        document.getElementById('bank_user').style.height="1px";
//         $('#domain_suffix').html("");
//         document.getElementById('flash_error').style.visibility="visible";
//         document.getElementById('flash_error').style.height="";
//         document.getElementById('flash_error').innerHTML = "Bank domain for the selected bank is not setup, therefore you cannot create any users";
//      }
//      });
//
//
//
//
//    //  ;
//    }
//    else
//    {
//      $('#username_suffix').html("");
//    }
//
//}

function fetch_bank_details(){
    if($("#sf_guard_user_bankform_bank_id :selected").val()!="")
    {
        var countryId;
        if($("#sf_guard_user_bankform_country_id").val())
            countryId = $("#sf_guard_user_bankform_country_id").val();
        else
            countryId = 0;
        var sel_val = $("#sf_guard_user_bankform_bank_id").val();
        $.post('fetchBankDetails',{
            id: sel_val,
            country_id:countryId
        }, function(data){
            if(data!=""){
                strdata = data.split('~');
                if(strdata[1] && strdata[1]!='BLANK'){
                    $('#domain_suffix').html(""+strdata[1]);
                }else{
                    $('#domain_suffix').html("");
                    if(!strdata[1] || (strdata[1] && strdata[1]!='BLANK'))
                        document.getElementById('flash_error').style.visibility="visible";
                    document.getElementById('flash_error').style.height="";
                    document.getElementById('flash_error').innerHTML = "Bank domain for the selected bank is not setup, therefore you cannot create any users";
                }
               
                $('#username_suffix').html(""+'.'+strdata[0]);
                $('#temp_div').html('');
      
            }else{       
                $('#domain_suffix').html("");
            }
        });
    }
    else
    {
        $('#username_suffix').html("");
    }

}
function fetch_bank_branches(url){
    //alert(url);

    var setStatus = $("#status").val();
    var sel_val = $("#bank_id").val();
    if(sel_val!="")
    {

        $.post(url,$('#search').serialize(), function(data){
            if(data!=""){
                $('#search_results').html(data);
                if(setStatus != ''){
                    $("#flash_notice").show();
                    $("#flash_notice").html('<span>Bank Branch '+setStatus+' successfully</span>')
                }
            }else{
                $('#search_results').html("");
            }
        });
        $("#err_bank").html("");
    }
    else
    {
        $("#err_bank").html("Please select bank");
        $('#search_results').html("");
    }
    $("#action_user").val("");
    $("#id").val("");
    $('#page').val("");
    $("#status").val("");

}

function compare_date(date){
    var today = new Date;
    var entered_date = date.split("-") ;
    var dob = new Date;
    dob.setDate(entered_date[0]);
    dob.setMonth(entered_date[1]-1);
    dob.setFullYear(entered_date[2]);
    if (dob >= today) {
        return false;
    }
    else{
        return true;
    }

}

function compare_dateinYMD(date){
    var today = new Date();
    var entered_date = date.split("-") ;
  
    today.setHours(0, 0, 0, 0);
   
    var dob = new Date(entered_date[0], entered_date[1]-1, entered_date[2], 0, 0, 0, 0);
   if (dob >= today) {
        return false;
    }
    else{
        return true;
    }

}


function validatePhone(phoneNumber) {
    //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
    //var reg = /^((\+)?(\d{2}))?(\d{10-15}){1}?$/;
    var reg = /^(\+)(\d){10,14}?$/;
    if(reg.test(phoneNumber) == false) {
        return true;
    }

    return false;
}

function validateNumeric(ewalletAccount) {
    //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
    //var reg = /^((\+)?(\d{2}))?(\d{10-15}){1}?$/;
    var reg = /^[0-9]*$/;
    if(reg.test(ewalletAccount) == false) {
        return true;
    }

    return false;
}

function validatePhone1(phoneNumber) {
    //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
    var reg = /^((\+)?(\d{2}))?(\d{10}){1}?$/;
    if(reg.test(phoneNumber) == false) {
        return true;
    }

    return false;
}
function validateAlphaNum(str) {
    //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
    var reg = /^[a-zA-Z0-9]*$/;
    if(reg.test(str) == false) {
        return true;
    }
    return false;
}

function validateAddress(address) {
    // var reg = /^([A-Za-z0-9_\-\.]){1,250}$/;
    // if((reg.test(address) == false) || (address.length > 255)) {
    //    return true;
    //  }

    if((address.length > 255)) {
        return true;
    }

    return false;
}


function validateAmount(str){
    if(str.length==0 || str==0)
    {
        return false;
    }
    numdecs = 0;
    numprecision=0;
    for (i = 0; i < str.length; i++)
    {
        mychar = str.charAt(i);
        if ((mychar >= "0" && mychar <= "9") || mychar == "." ){
            if (mychar == "."){
                numdecs++;
            }
            if(numdecs==1){
                numprecision++;
            }
        }
        else return false;
    }
    
    if (numdecs > 1){
        return false;
    }
    if (numdecs == 1 && numprecision== 1 ){
        return false;
    }
    //to check if user enters value above decimal with 2 digit =3 digits
    if(numprecision>3){
        return false;
    }
    return true;
}// end validateAmount function

//trim method
function trim(str, chars) {
    return str.replace(/^s+/g,'').replace(/s+$/g,'')
//    return ltrim(rtrim(str, chars), chars); //not working properly
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

//check input field has not empty
function userSearch(){
    var str_val = trim($("#user_search").val());
    var bank_branch_id = $("#bank_branch_id").val();
    if((str_val=="") && (bank_branch_id=="")){
        alert('Please select atleast one search criteria');
        return false;
    }else{
        return true;
    }
}

function displayPaymentModeOptions(merchant_service_id,module,payment_mode_option_id,url)
{
    if(module!=""){
        var target_div = module+"_payment_mode_option_id";
    }
    else{
        var target_div = "payment_mode_option_id";
    }
    //  var url = "BankBranch";
    $('#'+target_div).load(url,{
        merchant_service: merchant_service_id,
        payment_mode_option_id: payment_mode_option_id
    },function (data){

        if(data=='logout'){
            location.reload();
        }
    });
}
function validate_search_bank_branch_form(){
    if(document.getElementById("bank").value == ""){
        alert("Please select Bank.");
        document.getElementById("bank").focus();
        return false;
    }else return true;
}


function validate_amount_selection(url){
    var amount = $("#amount").val();
    if(amount == "") {
        $("#valid_amount_err").html("<font color='red'>Please select amount to recharge</font>");
        return false;
    }
    if(isNaN(amount) || amount<=0) {
        alert("Please enter valid amount");
        return false;
    }
    //  url = 'recharge_ewallet/rechargeSandbox';
    $('#recharge').load(url,{
        amount: amount
    },function (data){
        $("#valid_amount_err").html(data);

                        
    });
//$("#recharge").submit();
  
}

function calculatePasswordStrength(divid, url) {
    $('#'+divid).keypress(function(){

                
        password=$('#'+divid).val();
        //   username=$('#username').val();//$('username').serialize();
        $("#strength").load(url, {
            password: password,
            byPass:1
        },function (data){
            if(data=='logout'){
                $("#strength").html('');
                location.reload();
            }

        });
    });
}
function calculatePasswordStrengthVirtual(divid, url) {
        password=$('#'+divid).val();
        //   username=$('#username').val();//$('username').serialize();
        $("#strength").load(url, {
            password: password,
            byPass:1
        },function (data){
            if(data=='logout'){
                $("#strength").html('');
                location.reload();
            }
        }); 
}
function fetchBankCountry(bankId,divId,postUrl,countryId){
    $("#"+divId).load(postUrl, {
        bank: bankId,
        country: countryId,
        byPass:1
    },function (data){
        if(data=='logout'){
            location.reload();
        }

    });

}



