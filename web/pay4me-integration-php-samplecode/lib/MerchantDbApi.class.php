<?php
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/

/**
* Description of MerchantDbApiclass
*
* @author spandey
*/
class MerchantDbApi {

    private function getConnection() {
        return Property::getConnection();
    }

    public function saveRequest($data){
        //create your  table with the help of this query
//          CREATE TABLE IF NOT EXISTS `ep_p4m_request` (
//          `id` int(11) NOT NULL AUTO_INCREMENT,
//          `item_number` int(11) NOT NULL,
//          `merchant_service_id` int(10) NOT NULL,
//          `transaction_number` varchar(30) NOT NULL,
//          `currency` varchar(255) NOT NULL,
//          `payment_type` int(11) NOT NULL,
//          `buyer_ip_address` varchar(255) NOT NULL,
//          `payment_description` varchar(255) NOT NULL,
//          `total_amount` float(30,2) DEFAULT NULL,
//          PRIMARY KEY (`id`)
//        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


       $checkSql = "select id from ep_p4m_request where `item_number`
                = '".$data[item_number]."' and merchant_service_id
                    = '".$data[merchant_service_id]."'";
        $result = mysql_query($checkSql,$this->getConnection());
        $num_rows = mysql_num_rows($result);
        $status = true;
        if ($num_rows==0) {
           $strsql="insert into `ep_p4m_request` (
            `item_number`,
            `transaction_number`,
            `merchant_service_id`,
            `currency`,
            `payment_type`,
            `buyer_ip_address`,
            `payment_description`,
            `total_amount`)
            values('$data[item_number]',
            '$data[transaction_number]',
            '$data[merchant_service_id]',
            '$data[currency]',
            '$data[payment_type]',
            '$data[buyer_ip_address]',
            '$data[description]',
            '$data[price]')";
            $mq = mysql_query($strsql,$this->getConnection());
            if(!$mq){
                return false;
            }
        }
        return $status;
    }


    public function saveResponse($data){
//      create your table with the help of this query
//          CREATE TABLE IF NOT EXISTS `ep_p4m_response` (
//          `id` int(11) NOT NULL AUTO_INCREMENT,
//          `request_id` int(11) NOT NULL,
//          `payment_status` enum('0','1') DEFAULT '0',
//          `payment_mode` varchar(150) NOT NULL,
//          `validation_number` varchar(255) NOT NULL,
//          `total_amount` float(30,2) DEFAULT NULL,
//          `currency` varchar(255) NOT NULL,
//          `payment_date` datetime NOT NULL,
//          `payment_description` text,
//          PRIMARY KEY (`id`),
//          KEY `request_id` (`request_id`)
//        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

        $checkSql = "select * from ep_p4m_request where `item_number`
            =$data[item_number] and merchant_service_id = $data[merchant_service_id]";
        $result = mysql_query($checkSql,$this->getConnection());
        $num_rows = mysql_num_rows($result);

        if ($num_rows) {
            $row = mysql_fetch_array($result);
            $requestId = $row['id'];
            $strsql="insert into `ep_p4m_response` (
                `request_id`,
                `payment_status`,
                `payment_mode`,
                `validation_number`,
                `total_amount`,
                `currency`,
                `payment_date`,
                `payment_description`)
            values(
                '$requestId',
                '$data[payment_status]',
                '$data[payment_mode]',
                '$data[validation_number]',
                '$data[total_amount]',
                '$data[currency]',
                '$data[payment_date]',
                '$data[description]')";
            $mq = mysql_query($strsql,$this->getConnection());

            if($mq){
                return true;
            }else {
                return false;
        }
        } else {
            return false;
        }

    }

}
?>