<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class DbApiFactory {

 private static function getDbManager($version='') {
     $MerchantDbApiManager = "MerchantDbApi".strtoupper($version);
     $MerchantDbApiObj = new $MerchantDbApiManager(Property::getMerchantCode(),
                Property::getMerchantKey(),Property::getPaymentUrl());
     return $MerchantDbApiObj;
 }

 public static function SaveRequest($version,$arrayDetails){
     $status = true;
     if(Property::$database_operations){
        $ObjManager = self::getDbManager($version);
        $status = $ObjManager->saveRequest($arrayDetails);
     }
     return $status;
 }

 public static function SaveResponse($version,$arrayDetails){
     $status = true;
     if(Property::$database_operations){
        $ObjManager = self::getDbManager($version);
        $status = $ObjManager->saveResponse($arrayDetails);
     }
     return $status;
 }
}

?>
