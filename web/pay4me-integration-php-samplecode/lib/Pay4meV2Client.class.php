<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Pay4meV2Client extends AbstractPay4meClient{

    protected $merchant_code = null;
    protected $merchant_key = null;
    protected $payment_url = null;
    protected $version = "v2";
    protected $defaultXmlNs = "http://www.pay4me.com/schema/pay4meorder/";
    protected $logger;
    protected $merchant_service_id ;
    function Pay4meV2Client($merchant_code,$merchant_key,$payment_url){
       $this->merchant_code=$merchant_code;
       $this->merchant_key=$merchant_key;
       $this->payment_url=$payment_url;
       $this->logger = new Pay4meLog("L_ALL");
    }
    

    protected function generateRequestXML($item){
        $version  = $this->version;
        $merchant_service_id  = $this->merchant_service_id ;
        if($version!=""){
            $this->defaultXmlNs = $this->defaultXmlNs.strtolower($version);
        } else {
            $this->defaultXmlNs = $this->defaultXmlNs."v2";
        }
        $xml_data = new gc_XmlBuilder();
        $xml_data->Push('order', array('xmlns' => $this->defaultXmlNs));
        $xml_data->Push('merchant-service',  array('id'=>$merchant_service_id ));
         if(!isset($item) || count($item)==0){
            return false;
        }
        
            if(isset($item['item_number']))
                $xml_data->Push('item', array('number' => $item['item_number']));
            else
                $xml_data->Push('item', array('number' => ''));
           if(isset($item['transaction_number']))
                $xml_data->Element('transaction-number', $item['transaction_number']);
           if(isset($item['name']))
            $xml_data->Element('name', $item['name']);
           if(isset($item['description']))
            $xml_data->Element('description', $item['description']);
            
            $xml_data->Push('parameters');
            if(isset($item['parameters'])){
                foreach($item['parameters'] as $paramkey=>$parameter){
                    $xml_data->Element('parameter', $parameter, array('name' => $paramkey));
                }
            }

            $xml_data->Pop('parameters');
            if(isset($item['transaction-location'])){
                $xml_data->Element('transaction-location', $item['transaction-location']);
                
            }
            $xml_data->Push('payment');
            if(isset($item['payment'])){
                foreach($item['payment'] as $paymentkey => $payment){
                    if(isset($payment['code']))
                    $xml_data->Push('currency',array('code' => $payment['code']));
                    $xml_data->Push('payment-modes');
                    if(isset($payment['payment-modes'])){
                        foreach($payment['payment-modes'] as $paymentmode){
                            $xml_data->Element('payment-mode', $paymentmode);
                        }
                    }
                    $xml_data->Pop('payment-modes');
                   if(isset($payment['deduct-at-source'])){
                        $xml_data->Element('deduct-at-source', $payment['deduct-at-source']);
                    } else {
                        $xml_data->Element('deduct-at-source', false);
                    }
                    if(isset($payment['price']))
                    $xml_data->Element('price', $payment['price']);

    //                $xml_data->Push('revenue-share',array('fixed-share' =>
    //                                                $payment['revenue-share']['fixed-share']));
    //
    //                $xml_data->Push('other-shares');
    //                foreach($payment['revenue-share']['other-shares'] as $othersharekey=>$othershare){
    //                     $xml_data->Element('other-share', $othershare,
    //                                array('merchant-account-number' => $othersharekey));
    //                }
    //                $xml_data->Pop('other-shares');
    //                $xml_data->Pop('revenue-share');
                     $xml_data->Pop('currency');
                }
            }

            $xml_data->Pop('payment');            
            $xml_data->Pop('item');
       
        $xml_data->Pop('merchant-service');
        $xml_data->Pop('order');

        return $xml_data->GetXML();
    }

//    protected function saveResponse($payment_notification){
//
//        $data = array(
//                'payment_status'=>
//                $payment_notification['item']['payment-information']['status']['code']['VALUE'],
//                'item_number'=>
//                $payment_notification['item']['number'],
//                'transaction_number'=>
//                $payment_notification['item']['transaction-number']['VALUE'],
//                'merchant_service_id'=>
//                $payment_notification['id'],
//                'validation_number'=>
//                $payment_notification['item']['validation-number']['VALUE'],
//                'payment_mode'=>
//                $payment_notification['item']['payment-information']['payment-mode']['VALUE'],
//                'total_amount'=>
//                $payment_notification['item']['payment-information']['amount']['VALUE'],
//                'currency'=>
//                $payment_notification['item']['payment-information']['currency']['code'],
//                'payment_date'=>
//                $payment_notification['item']['payment-information']['payment-date']['VALUE'],
//                'description'=>
//                $payment_notification['item']['payment-information']['status']['description']['VALUE']);
//
//        $merchantApiObj = new MerchantDbApi();
//        $merchantApiObj->saveResponse($data);
//
//    }

//    protected function savePaymentRequest($itemArray){

   

  //name
  //remove total_amount, currency
  // create new table ep_p4m_request_parameters
  /*
   *
   *
  CREATE TABLE IF NOT EXISTS `ep_p4m_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_number` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `merchant_service_id` int(10) NOT NULL,
  `transaction_number` varchar(30) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `buyer_ip_address` varchar(255) NOT NULL,
  `payment_description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
   CREATE TABLE `ep_p4m_request_parameters` (
        `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
        `request_id` INT NOT NULL ,
        `parameter_name` VARCHAR( 255 ) NOT NULL ,
        `parameter_value` VARCHAR( 255 ) NOT NULL ,
        INDEX ( `request_id` )
        ) ENGINE = InnoDB;

   CREATE TABLE `ep_p4m_request_currency` (
    `id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `request_id` INT( 10 ) NOT NULL ,
    `currency_code` INT( 4 ) NOT NULL ,
    `price` FLOAT( 30, 2 ) NOT NULL ,
    INDEX ( `request_id` )
    ) ENGINE = InnoDB;
   

    CREATE TABLE `ep_p4m_currency_payment_mode` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `request_currency_id` INT NOT NULL ,
    `payment_mode` INT NOT NULL ,
    INDEX ( `request_currency_id` )
    ) ENGINE = InnoDB;




  */
  //
  // create new table ep_p4m_request_currency
  // create new table ep_p4m_currency_payment_mode

        //configure items for version v2
//     $sampleitemV2 =
    //                array(
//                        'merchant_service_id'=>'insert your merchant service id provide by pay4me',
//                        'item_details'=>
//                                array(
//                                    'item_number'=>'item number of your application',
//                                    'transaction_number'=>'transaction number of your application',
//                                    'name'=>'name of item',
//                                    'payment_type'=>'type of payment',
//                                    'buyer_ip_address '=>'ip_address',
//                                    'description'=>'description of item',
//                                    'payment'=>
//                                        array(
//                                            'currency'=>
//                                                array(
//                                                    'code'=>'currency code',
//                                                    'payment-modes'=>'array of payment mode ids ',
//                                                    // ex array(1,2,3) etc
//                                                    'price'=>'amount'
//                                                    //ex: naira,kobo etc
//                                                    )
//                                            ),
//                                    'parameters'=>
//                                        array(
//                                            'app_id'=>'parameters of merchant validation rules specified',
//                                            'ref_num'=>'parameters of merchant validation rules specified'
//                                            )
//                                    )
//                        );
//
//
//        $merchantApiObj = new MerchantDbApiV2();
//        $merchantApiObj->saveRequest($itemArray);
//
//    }

}

?>