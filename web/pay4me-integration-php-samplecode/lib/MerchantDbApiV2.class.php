<?php
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/

/**
* Description of MerchantDbApiclass
*
* @author spandey
*/
class MerchantDbApiV2 {

    private function getConnection() {
        return Property::getConnection();
    }

    public function saveRequest($itemArray){

        //  CREATE TABLE IF NOT EXISTS `ep_p4m_request` (
        //  `id` int(11) NOT NULL AUTO_INCREMENT,
        //  `item_number` int(11) NOT NULL,
        //  `item_name` varchar(255) NOT NULL,
        //  `merchant_service_id` int(10) NOT NULL,
        //  `transaction_number` varchar(30) NOT NULL,
        //  `payment_type` int(11) NOT NULL,
        //  `buyer_ip_address` varchar(255) NOT NULL,
        //  `payment_description` varchar(255) NOT NULL
        //  PRIMARY KEY (`id`)
        //) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

        $item = $itemArray['item_details'];
        $checkRequestSql = sprintf("select id from ep_p4m_request where item_number
                = '%s' and merchant_service_id
                    = '%s'",$item['item_number'],$itemArray['merchant_service_id']);
        $result = mysql_query($checkRequestSql,$this->getConnection());
        $num_rows = mysql_num_rows($result);
        $status = true;
        if ($num_rows==0) {
           $insertRequestSql= sprintf("insert into ep_p4m_request (
                    item_number,
                    item_name,
                    transaction_number,
                    merchant_service_id,
                    payment_type,
                    buyer_ip_address,
                    payment_description
                )
                    values('%s','%s','%s','%s','%s','%s','%s')",
                    $item['item_number'],
                    (isset($item['name'])) ? $item['name']:'',
                    (isset($item['transaction_number'])) ? $item['transaction_number']:'',
                    $itemArray['merchant_service_id'],
                    (isset($item['payment_type'])) ? $item['payment_type']:'',
                    (isset($item['buyer_ip_address'])) ? $item['buyer_ip_address']:'',
                    (isset($item['description'])) ? $item['description']:''
            );
            $status = mysql_query($insertRequestSql,$this->getConnection());
            if(!$status){
                return false;
            } else {
                $requestId = mysql_insert_id($this->getConnection());
                foreach($item['parameters'] as $paramkey=>$parameter){
                    $this->saveRequestParameters($requestId,$paramkey,$parameter);
                }
                foreach($item['payment'] as $paymentkey => $payment){

                    $this->saveRequestCurrency($requestId,$payment['code'],
                                $payment['price'],(isset($payment['deduct-at-source'])) ? $payment['deduct-at-source']:false);
                    $requestCurrencyId = mysql_insert_id($this->getConnection());

                    foreach($payment['payment-modes'] as $paymentmode){
                        $this->saveRequestCurrencyPaymentMode($requestCurrencyId,$paymentmode);
                    }
                }

            }
        }
        return $status;
    }


    private function saveRequestParameters($requestId,$paramkey,$parameter){
//        CREATE TABLE `ep_p4m_request_parameters` (
//        `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//        `request_id` INT NOT NULL ,
//        `parameter_name` VARCHAR( 255 ) NOT NULL ,
//        `parameter_value` VARCHAR( 255 ) NOT NULL ,
//        INDEX ( `request_id` )
//        ) ENGINE = InnoDB;
          $insertParameterSql= sprintf("insert into ep_p4m_request_parameters (
                        request_id,
                        parameter_name,
                        parameter_value
                    )
                        values('%s','%s','%s')",
                        $requestId,
                        $paramkey,
                        $parameter
            );
          $status = mysql_query($insertParameterSql,$this->getConnection());
    }

    private function saveRequestCurrency($requestId,$currency_code,$price,$deduct_at_source){
//        CREATE TABLE `ep_p4m_request_currency` (
//    `id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//    `request_id` INT( 10 ) NOT NULL ,
//    `currency_code` INT( 4 ) NOT NULL ,
//    `price` FLOAT( 30, 2 ) NOT NULL ,
//    `deduct_at_source` boolean NOT NULL ,
//    INDEX ( `request_id` )
//    ) ENGINE = InnoDB;
        $insertCurrencySql= sprintf("insert into ep_p4m_request_currency (
                        request_id,
                        currency_code,
                        price,
                        deduct_at_source
                    )
                        values('%s','%s','%s','%s')",
                        $requestId,
                        $currency_code,
                        $price,
                        $deduct_at_source
            );
          $status = mysql_query($insertCurrencySql,$this->getConnection());

    }

    private function saveRequestCurrencyPaymentMode($requestCurrencyId,$paymentMode){
//        CREATE TABLE `ep_p4m_currency_payment_mode` (
//    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//    `request_currency_id` INT NOT NULL ,
//    `payment_mode` INT NOT NULL ,
//    INDEX ( `request_currency_id` )
//    ) ENGINE = InnoDB;

       $insertCurrencyPaymentModeSql= sprintf("insert into ep_p4m_currency_payment_mode (
                        request_currency_id,
                        payment_mode
                    )
                        values('%s','%s')",
                        $requestCurrencyId,
                        $paymentMode
                    );
          $status = mysql_query($insertCurrencyPaymentModeSql,$this->getConnection());
    }



    public function saveResponse($payment_notification){
//      create your table with the help of this query
//          CREATE TABLE IF NOT EXISTS `ep_p4m_response` (
//          `id` int(11) NOT NULL AUTO_INCREMENT,
//          `request_id` int(11) NOT NULL,
//          `payment_status` enum('0','1') DEFAULT '0',
//          `payment_mode` varchar(150) NOT NULL,
//          `validation_number` varchar(255) NOT NULL,
//          `total_amount` float(30,2) DEFAULT NULL,
//          `currency` integer(4) NOT NULL,
//          `payment_date` datetime NOT NULL,
//          `payment_description` text,
//          PRIMARY KEY (`id`),
//          KEY `request_id` (`request_id`)
//        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

        $data = array(
                'payment_status'=>
                $payment_notification['item']['payment-information']['status']['code']['VALUE'],
                'item_number'=>
                $payment_notification['item']['number'],
                'transaction_number'=>
                $payment_notification['item']['transaction-number']['VALUE'],
                'merchant_service_id'=>
                $payment_notification['id'],
                'validation_number'=>
                $payment_notification['item']['validation-number']['VALUE'],
                'payment_mode'=>
                $payment_notification['item']['payment-information']['payment-mode']['VALUE'],
                'total_amount'=>
                $payment_notification['item']['payment-information']['amount']['VALUE'],
                'currency'=>
                $payment_notification['item']['payment-information']['currency']['code'],
                'payment_date'=>
                $payment_notification['item']['payment-information']['payment-date']['VALUE'],
                'description'=>
                $payment_notification['item']['payment-information']['status']['description']['VALUE']);


        $checkSql =  sprintf("select * from ep_p4m_request where item_number
            = '%s' and merchant_service_id = '%s'",$data['item_number'],$data['merchant_service_id']);
        $result = mysql_query($checkSql,$this->getConnection());
        $num_rows = mysql_num_rows($result);

        if ($num_rows) {
            $row = mysql_fetch_array($result);
            $requestId = $row['id'];
            $strsql= sprintf("insert into ep_p4m_response(
                request_id,
                payment_status,
                payment_mode,
                validation_number,
                total_amount,
                currency,
                payment_date,
                payment_description)
            values('%s','%s','%s','%s','%s','%s','%s','%s')",
                $requestId,
                $data['payment_status'],
                $data['payment_mode'],
                $data['validation_number'],
                $data['total_amount'],
                $data['currency'],
                $data['payment_date'],
                $data['description']);
            $mq = mysql_query($strsql,$this->getConnection());

            if($mq){
                return true;
            }else {
                return false;
        }
        } else {
            return false;
        }

    }

}
?>