<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4MeServiceFactoryclass
 *
 * @author srikanth reddy
 */
function __autoload($class) {   
    if (file_exists(dirname(__FILE__).'/'.$class.'.class.php')) {        
        require_once(dirname(__FILE__).'/'.$class.'.class.php');        
    }
    else if (file_exists(dirname(__FILE__).'/../'.$class.'.class.php')) {
        require_once(dirname(__FILE__).'/../'.$class.'.class.php');      
    } else if (file_exists(dirname(__FILE__).'/xml-processing/'.$class.'.php')) {
        require_once(dirname(__FILE__).'/xml-processing/'.$class.'.php');       
    }
}


class pay4MeManagerFactory {
  
 public static function getIntegrator($version='') {
     $pay4MeManager = "Pay4me" . strtoupper($version) . "Client" ; 
     $pay4MeManagerObj = new $pay4MeManager(Property::getMerchantCode(),
                Property::getMerchantKey(),Property::getPaymentUrl());
     return $pay4MeManagerObj;     
 }
}
?>
