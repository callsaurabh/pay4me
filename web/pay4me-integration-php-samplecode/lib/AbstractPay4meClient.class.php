<?php
/**
   * AbstractPay4meClient class
   *
   * This class is responsible to do all core functionality of version v1
   * Member variable of this class are
   * @param string $merchant_code Merchant code
   * @param string $merchant_key  merchant key which will be provided by pay4me
   * @param string  $version Default configurred to v1;
   * @param string $payment_url payment url is a url which points to pay4me
   * @param string $defaultXmlNs default xmlns setting
   * @param Object $logger logger object instance
   *
   * @author srikanth reddy
   */
abstract class AbstractPay4meClient {

    /**
     *  Patment request will be started
     * this method will do all request related tasks
     * @param int $merchant_service_id  this is a merchant service id exposed with merchant
     * @param array $items  an array of items are presented.
     */
    public function PayRequest($itemArray=array()){

        $this->merchant_service_id  = $itemArray['merchant_service_id'] ;
        $return_url = $this->integratePay4Me($itemArray);
        //do logging of return/redirection url
        $this->logger->LogMessage($return_url);
        return $return_url;
    }

    public function PayResponse(){
        $xml_response =  file_get_contents('php://input');
        $logfile = "/Pay4meNotification-".date('Y-m-d_h:i:s').".txt";
        //do logging of notification response
        $this->logger->LogData($xml_response,$logfile);
        //check if magic quotes are added strip slashess
        if (get_magic_quotes_gpc()) {
           $xml_response = stripslashes($xml_response);
        }
        //create response object
        $Pay4meResponseObj = new Pay4meResponse($this->merchant_code,$this->merchant_key);
        //setting authentication failled response
        $error = false;
        //parse xml
        list($root, $data) = $Pay4meResponseObj->GetParsedXML($xml_response);
        $raw_xml = $xml_response;
        if(isset($data['payment-notification']['merchant-service'])){
            $payment_notification = $data['payment-notification']['merchant-service'];
            //do database handlings
            DbApiFactory::SaveResponse($this->version, $payment_notification);
            //$this->saveResponse($payment_notification);
        } else {
            $error = true;
        }
        if(!$error){
         $Pay4meResponseObj->SendAck(null, false);
        } else {
            $Pay4meResponseObj->SendServerErrorStatus(null, false);
        }
        return $data;
    }
    private function integratePay4Me($itemArray){
        $items = $itemArray['item_details'];
        $request_xml = $this->generateRequestXML($items);
        if(!$request_xml){
            $this->logger->LogError("Invalid Item array");
            return false;
        }
        $logfile = "/PaymentRequestXML_".$items['item_number']."_".$items['transaction_number']."_".date('Y-m-d_h:i:s').".txt";
        $this->logger->LogData($request_xml,$logfile);
        //do database saving stuff
        $requestStatus = DbApiFactory::SaveRequest($this->version,$itemArray);
        //$requestStatus = $this->savePaymentRequest($itemArray);
        if(!$requestStatus){
            $this->logger->LogError("Saving to database failled");
            return false;
        } else {
            $request_obj = new Pay4meRequest($this->merchant_code,$this->merchant_key,
                    $this->payment_url,$this->version);
            $arrResponse = $request_obj->sendRequest($request_xml);
            $ObjResponse = new Pay4meResponse($this->merchant_code,$this->merchant_key);
            return  $ObjResponse->pay4mePaymentResponse($arrResponse,$items);
        }
    }

    //protected abstract function saveResponse($payment_notification);

    //protected abstract function savePaymentRequest($items);

    protected abstract function generateRequestXML($items);

}

?>