<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php
    $message = "";
    if (isset($_REQUEST['message'])&&$_REQUEST['message']=="waitingResponse") {
        $message = "Payment has already been made for this application.";
    } else {
        $message = "<b>Oops! Page Not Found</b> <br/>
                    The server returned a 404 response.";
    }


?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="../css/default.css" media="screen" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="XY50">
            <div id="flash_error" class="error_list">
                <span>
                   <?php echo $message;?>
                </span>
            </div>
        </div>
    </body>
</html>
