<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once(dirname(__FILE__).'/../lib/Pay4meManager.class.php');
$version = strtoupper(Property::$currentversion);
$item = "sampleitem".$version;
$item = Property::$$item;
//create connection or pass your database connection
Property::createConnection();
function MakePayment($version,$item) {
    $pay4meClient = pay4MeManagerFactory::getIntegrator(Property::$currentversion);
    return $pay4meClient->PayRequest($item);
}

if(isset($_REQUEST['pay'])&&$_SERVER['REQUEST_METHOD'] === 'POST'){
    $redirectUrl = MakePayment($version,$item);
    header("Location: $redirectUrl");
}
?>
<html>
<head>
<title>Pay4Me-Sample Payment</title>
<link href="css/default.css" media="screen" type="text/css" rel="stylesheet">
</head>
<body>
    <br/>
    <br/>
<form method="post" action="">
    <table align="center" width="70%" class="XY51">
        <tr bgcolor="#EEEEEE" class="heading">
            <td colspan="2" align="center"><h1>Sample Item Payment</h1></td>
        </tr>
        <tr bgcolor="#CCCCCC" class="heading">
            <td colspan="2" align="left">Order Detail</td>
        </tr>
        <tr>
            <td>Transaction Number</td>
            <td><?php echo $item['item_details']['transaction_number']?></td>
        </tr>
        <tr>
            <td>Item Number</td>
            <td><?php echo $item['item_details']['item_number']?></td>
        </tr>
        <?php if($version=='V1'): ?>

        <tr>
            <td>Price</td>
            <td><?php echo $item['item_details']['price']?></td>
        </tr>
        <?php else : ?>
        <?php
            foreach($item['item_details']['payment'] as $paymentkey => $payment):?>
        <tr>
            <td>Currency Code</td>
            <td><?php echo $payment['code'] ?> </td>


        </tr>
        <tr>

            <td>Price</td>
            <td><?php echo $payment['price'] ?></td>

        </tr>

        <?php endforeach; ?>
        <?php endif; ?>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" name="pay" value="Make Payment" >
            </td>
        </tr>

    </table>
</form>
</body>
</html>