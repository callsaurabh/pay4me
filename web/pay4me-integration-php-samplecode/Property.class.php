<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Property {
    //please specify your log file path
    private static $logPath= "/log";
    //do database operations like save request ,save response etc.
    public static $database_operations = true;
    public static $currentversion= "v1";
    public static $merchant_code="Please insert your merchant code";
    public static $merchant_key="Please insert your merchant key";
    public static $payment_url="http://localhost/pay4me/web/frontend_dev.php/order/payment";
    //for which url/page application needs to be redirected if error occurred
    //if this url is not specified it will redirects to home/error404
    public static $error_url="http://localhost/pay4me/web/frontend_dev.php/order/payment";
    public static $log_level = "L_ALL";
    // User can override methods related to database connections depending on his application
    public static $host = "Please insert your host";
    public static $user = "Please insert your user name";
    public static $password = "Please insert your password";
    public static $database = "Please insert your database name";
    private static $connection= "";
    public static function getLogPath(){
        return dirname(__FILE__).self::$logPath."/".date('Y-m-d');
    }

    public static function createConnection($connection='') {
        if($connection==""){
            $link = mysql_connect(self::$host, self::$user, self::$password);
            if (!$link) {
                die('Could not connect: ' . mysql_error());
            }
            mysql_select_db(self::$database, $link);
            self::$connection = $link;
        } else {
            self::$connection = $connection;
        }
    }
    
    public static function getMerchantCode(){
        return self::$merchant_code;
    }
    public static function getMerchantKey(){
        return self::$merchant_key;
    }
    public static function getPaymentUrl(){
        return self::$payment_url;
    }
    public static function getHost(){
        return self::$host;
    }
    public static function getUserName(){
        return self::$user;
    }
    public static function getPassword(){
        return self::$password;
    }
    public static function getDarabase(){
        return self::$database;
    }
    public static function getConnection(){
        return self::$connection;
    }

    public static function closeConnection() {
        if(self::$connection!="")
        mysql_close(self::$connection);

    }
    //sample item details for version 1
     public static $sampleitemV1 =
                    array(
                        'merchant_service_id'=>'insert your merchant service id provide by pay4me',
                        'item_details'=>
                                array(
                                    'item_number'=>'item number of your application',
                                    'transaction_number'=>'transaction number of your application',
                                    'name'=>'name of item',
                                    'description'=>'description of item',
                                    'payment_type'=>'type of payment',
                                    'buyer_ip_address '=>'ip_address',
                                    'price'=>'item price in specified currency',
                                    'currency'=>'name of currency',
                                    //ex: naira,kobo etc
                                    'parameters'=>array(
                                        'app_id'=>'parameters of merchant validation rules specified',
                                        'ref_num'=>'parameters of merchant validation rules specified'
                                        )
                                    )
                        );
    //sample item details for version 2
   //configure items for version v2
    public static $sampleitemV2 =
                    array(
                        'merchant_service_id'=>'insert your merchant service id provide by pay4me',
                        'item_details'=>
                                array(
                                    'item_number'=>'item number of your application',
                                    'transaction_number'=>'transaction number of your application',
                                    'name'=>'name of item',
                                    'payment_type'=>'type of payment',
                                    'buyer_ip_address '=>'ip_address',
                                    'description'=>'description of item',
                                    'payment'=>
                                        array(
                                            'currency'=>
                                                array(
                                                    'code'=>'currency code',
                                                    'payment-modes'=>'array of payment mode ids ',
                                                    // ex array(1,2,3) etc
                                                    'price'=>'name of currency'
                                                    //ex: naira,kobo etc
                                                    )
                                            ),
                                    'parameters'=>
                                        array(
                                            'app_id'=>'parameters of merchant validation rules specified',
                                            'ref_num'=>'parameters of merchant validation rules specified'
                                            )
                                    )
                        );

}
?>