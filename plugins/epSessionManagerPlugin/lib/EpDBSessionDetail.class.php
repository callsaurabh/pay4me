<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpDBSessionDetail{

    //get session id
    public function getSessionId(){ 
        return session_id();
    }

    //check number of current login sessions by username
    public function isSessionActiveByUsername($userName){   
     if($this->checkIsDBSessionOn()){
     if($this->isSessionAllreadyCreatedForUsername($userName)){
          return false;
       }
       return true;
     }
     else   
        return true;
   }

   public function checkIsDBSessionOn(){         
         if(sfConfig::get('app_save_userdetails_with_sessiondb'))
            return true;
         else
            return false;
   }

    //clean login user sessions
    public function doCleanUpSessionByUser($sessionId=""){
         if($this->checkIsDBSessionOn()){
            if($sessionId=="")
                $sessionId = $this->getSessionId();
            Doctrine::getTable('AppSessions')->clearUserSessionBySessionId($sessionId);
         }
    }

    //
    public function getUserActiveSessionId($userName){
        //get timeout           
        $timeToBeGreaterThen = $this->getLastRequestedTime()-$this->getTimeOut();
        $numberOfSessions = Doctrine::getTable('AppSessions')->getSessionDataByUser($userName,$timeToBeGreaterThen);
        return $numberOfSessions;       
    }
    public function getTimeOut(){
        $user = $this->getUserContextObj();
        $userOptions = $user->getUser()->getOptions();
        return  $timeout = $userOptions["timeout"];
    }

    public function getLastRequestedTime(){
      $user = $this->getUserContextObj();
      return  $user->getUser()->getLastRequestTime();
    }

    public function getUserContextObj(){
        return sfContext::getInstance();
    }

    //get number of user login by  same username with in a session time
   public function isSessionAllreadyCreatedForUsername($userName){
       $sessionIds = $this->getUserActiveSessionId($userName);
       //number of session for a  username
       $activeSessionLimit = $this->getSessionLimit();
       
       if($sessionIds>=$activeSessionLimit)
            return true;
        else
            return false;
   }
   //get limit for active session with a username
   public function getSessionLimit(){      
        if(sfConfig::get('app_limit_of_active_session'))
            $limitOfActiveSession = sfConfig::get('app_limit_of_active_session');
       else
            $limitOfActiveSession = sfConfig::get('app_sf_session_attribute_limit_with_single_username');                      
       return $limitOfActiveSession;
   }

   public function getLogoutTimeFromCurrentTimestamp(){     
      $logoutTime =  time()-$this->getTimeOut();
      return $logoutTime;
   }
  
    public function setUserSessionData()
    {       
            $sessionId = $this->getSessionId();
            $userContext = $this->getUserContextObj();
            $user = $userContext->getUser();
            $arrayVal =  $user->getAttributeHolder()->getAll('sfGuardSecurityUser');
            $userId = $arrayVal['user_id'];
           
            $ipAddress =  $this->getIpAddress();
            $userDBSessionObj = Doctrine::getTable('AppSessions')->fetchAppSessionRecordObj($sessionId);
            $userDBSessionObj->setUsername($user->getUsername());
            $userDBSessionObj->setUserId($userId);
            $userDBSessionObj->setSessId($sessionId);
            $userDBSessionObj->setIpAddress($ipAddress);
            $userDBSessionObj->save();
    }

    /*
     * Function to get ip address from where system is getting accessed
     */
   
      protected function getIpAddress() {
        return (empty($_SERVER['HTTP_X_FORWARDED_FOR'])?(empty($_SERVER['HTTP_CLIENT_IP'])?
        $_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_CLIENT_IP']):$_SERVER['HTTP_X_FORWARDED_FOR']);
      }
 
    public function deleteSessionData($userList)
    {        
       foreach($userList as $val){
            Doctrine::getTable('AppSessions')->clearUserSessionByUsername($val['sigin_user_name']);
       }
    }
    public function cleanupIdleSessions()
   {
       $timeOut = $this->getTimeOut();
       $deleteBelow = (time() - $timeOut); // Read from factories.yml
    return    $q = Doctrine_Query::create()
           ->delete()
           ->from('AppSessions k')
           ->where('k.sess_time < ?', $deleteBelow)
           ->execute();
   }
}
?>