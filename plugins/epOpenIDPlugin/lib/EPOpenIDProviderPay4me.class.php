<?php

class EpOpenIDProviderPay4me extends EpOpenIDProviderIntf {
  public function getAuthenticationUrl($screenName=null) {
    $openIdUrl = sfConfig::get('app_openid_url_pay4me_url');
    return $this->preAuthenticate($openIdUrl);
  }

  public function getDbDetails($data) {
    $map = new DbtoOpendidAttrMap();
    $map[DbtoOpendidAttrMap::$USER_KEY] = $data['openid_identity'];
    $map[DbtoOpendidAttrMap::$FULL_NAME_KEY] = $data['firstname']." ".$data['lastname'];
    $map[DbtoOpendidAttrMap::$EMAIL_KEY]  = $data['email'];
    return $map;
  }
}