<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpOpenIDExt
{
  public static function getOpenidProvider ($identifierType,$screenName=null,$openIdUrl=null) {
    switch (strtoupper($identifierType))
    {
      case sfConfig::get('app_openid_provider_google'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$googleConfig)->getAuthenticationUrl();
        break;
      case sfConfig::get('app_openid_provider_yahoo'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$yahooConfig)->getAuthenticationUrl();
        break;
      case sfConfig::get('app_openid_provider_facebook'):
        if($openIdUrl != null){
          $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$facebookConfig)->getAuthenticationUrl($openIdUrl);
        }else{
          throw new Exception("Facebook url missing");
        }        
        break;
    }
    try{
         header('Location: ' . $openIdUrl);
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public static function getOpenidProviderDetails ($data,$identfType,$openIdUrl=null) {
    switch (strtoupper($identfType))
    {
      case sfConfig::get('app_openid_provider_google'):
        return self::fetchOpenIdDetails(EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$googleConfig)->getDbDetails($data));
        break;
      case sfConfig::get('app_openid_provider_yahoo'):
        return self::fetchOpenIdDetails(EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$yahooConfig)->getDbDetails($data));
        break;
      case sfConfig::get('app_openid_provider_facebook'):
        return self::fetchOpenIdDetails(EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$facebookConfig)->getDbDetails($data,$openIdUrl));
        break;
      case sfConfig::get('app_openid_provider_aol'):
        return self::fetchOpenIdDetails(EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$aolConfig)->getDbDetails($data));
        break;
      case sfConfig::get('app_openid_provider_pay4me'):
        return self::fetchOpenIdDetails(EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$pay4meConfig)->getDbDetails($data));
        break;
      default:
        throw new Exception("invalid Identifier type !");
        break;
    }
  }
  public static function forceLogout ($identifierType,$screenName=null,$openIdUrl=null) {
    $browser = new sfWebBrowser();
    $openid_url=self::getLogoutUrlReturn($identifierType, $screenName,$openIdUrl);
    $browser->get($openid_url);
    return $browserres = $browser->getResponseCode();
  }
  public static function getLogoutUrl ($identifierType,$screenName=null,$openIdUrl=null) {
    $openIdUrl=self::getLogoutUrlReturn($identifierType, $screenName, $openIdUrl);
    try{
      header('Location: ' . $openIdUrl);
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  public static function getLogoutUrlReturn ($identifierType,$screenName=null,$openIdUrl=null) {
    switch (strtoupper($identifierType))
    {
      case sfConfig::get('app_openid_provider_google'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$googleConfig)->getLogoutUrl();
        break;
      case sfConfig::get('app_openid_provider_yahoo'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$yahooConfig)->getLogoutUrl();
        break;
      case sfConfig::get('app_openid_provider_facebook'):
        if($openIdUrl != null){
          $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$facebookConfig)->getLogoutUrl($openIdUrl);
        }else{
          throw new Exception("Facebook url missing");
        }
        break;
      case sfConfig::get('app_openid_provider_aol'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$aolConfig)->getLogoutUrl();
        break;
    }
    return $openIdUrl;
  }
   public static function fetchOpenIdDetails($data){       
       if(!is_object($data)) return;
        $email = $data[DbtoOpendidAttrMap::$EMAIL_KEY];
        $username = $data[DbtoOpendidAttrMap::$USER_KEY];
        if(strpos($username,sfConfig::get('app_openid_url_google_url')) !== false) {
            // google case
            $fname = $data[DbtoOpendidAttrMap::$FIRST_NAME_KEY];
            $lname = $data[DbtoOpendidAttrMap::$LAST_NAME_KEY];
			$authIdArr[1]=$username;

        }else if((strpos($username,sfConfig::get('app_openid_url_yahoo_url')) !== false) || (stripos($username,'facebook') !== false)){
            // yahoo case
            $nameArr = explode(" ", $data[DbtoOpendidAttrMap::$FULL_NAME_KEY]);
            if (count($nameArr) > 1) {
                $lname = $nameArr[count($nameArr) - 1];
                unset($nameArr[count($nameArr) - 1]);
                $fname = implode(" ", $nameArr);
            } else {
                $fname = $data[DbtoOpendidAttrMap::$FULL_NAME_KEY];
                $lname = " ";
            }
        }
        if(strpos($username,sfConfig::get('app_openid_url_yahoo_url')) !== false){
			$authIdArr[1]=$username;
        }
        if(stripos($username,'facebook') !== false){
            $authIdArr[1] = $username;
        }
        $openIdResArr = array('email'=>$email,'fname'=>$fname,'lname'=>$lname,'openIdAuth'=>$authIdArr[1]);
        return $openIdResArr;
    }


    /*
     * function to check if domains of opeidAuth is diffrent from one which is in db. Mostly it will be in case of facebook.
     * if facebook login is same with email id that is of gmail id.
     */
    public static function getOpenidDomain($openidAuth,$openidAuthDb){
       // google case
       if(strpos($openidAuth,sfConfig::get('app_openid_url_google_url')) !== false){
           if(strpos($openidAuthDb,sfConfig::get('app_openid_url_google_url')) !== false){
               return false;
           }else{
               //return false;
               if(strpos($openidAuthDb,sfConfig::get('app_openid_url_facebook_url')) !== false){
                   return 'Facebook';
               }else{
                   return 'Yahoo';
               }
           }
       }
       // yahoo case
       if(strpos($openidAuth,sfConfig::get('app_openid_url_yahoo_url')) !== false){
           if(strpos($openidAuthDb,sfConfig::get('app_openid_url_yahoo_url')) !== false){
               return false;
           }else{
               //return false;
               if(strpos($openidAuthDb,sfConfig::get('app_openid_url_facebook_url')) !== false){
                   return 'Facebook';
               }else{
                   return 'Google';
               }
           }
       }
       // facebook case
       if(strpos($openidAuth,sfConfig::get('app_openid_url_facebook_url')) !== false){
           if(strpos($openidAuthDb,sfConfig::get('app_openid_url_facebook_url')) !== false){
               return false;
           }else{
               //return false;
               if(strpos($openidAuthDb,sfConfig::get('app_openid_url_yahoo_url')) !== false){
                   return 'Yahoo';
               }else{
                   return 'Google';
               }
           }
       }
    }
}

