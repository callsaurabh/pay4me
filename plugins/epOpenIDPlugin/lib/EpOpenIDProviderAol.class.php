<?php

class EpOpenIDProviderAol extends EpOpenIDProviderIntf {
  public function getAuthenticationUrl($screenName=null) {
    $openIdUrl = sfConfig::get('app_openid_url_aol_url').$screenName; 
    return $this->preAuthenticate($openIdUrl);
  }
  public function getLogoutUrl() {
    return "http://my.screenname.aol.com/_cqr/logout/mcLogout.psp";
  }

  public function getDbDetails($data) {
    $map = new DbtoOpendidAttrMap();
    $map[DbtoOpendidAttrMap::$USER_KEY] = $data['openid_identity'];
    $map[DbtoOpendidAttrMap::$FULL_NAME_KEY] = $data['openid_sreg_nickname'];
    $map[DbtoOpendidAttrMap::$EMAIL_KEY]  = $data['openid_sreg_email'];
    return $map;
  }
}