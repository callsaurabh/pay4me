<?php

class EpOpenIDProviderFacebook extends EpOpenIDProviderIntf {
  public function getAuthenticationUrl($openIdUrl=null) {
    $facebook = new FacebookApi($openIdUrl);
    return $facebookLoginUrl = $facebook->getLoginUrl(array('scope' => 'email'));
  }
  public function getLogoutUrl($openIdUrl=null) {
    $facebook = new FacebookApi($openIdUrl);
    return $facebookLoginUrl = $facebook->getLogoutUrl(array('scope' => 'email'));
  }

  public function getDbDetails($data,$openIdUrl=null) {
        ## Create our Application instance...
        $facebook = new FacebookApi($openIdUrl);
        $user = $facebook->getUser();
        if ($user) {
          try {
            // Proceed knowing you have a logged in user who's authenticated.
            $user_profile = $facebook->api('/me');
            $uid = $user_profile['id'];
            $accessToken = $facebook->getAccessToken();
            $email = $user_profile['email'];
            $fullname = $user_profile['name'];
            if($uid == '' && $email =='' && $fullname == ''){
                throw new exception ("Unable to receive user information from facebook. Please try again.");
            }else{ 
                ////got successful logged in
                //store it in object
                $map = new DbtoOpendidAttrMap();
                $map[DbtoOpendidAttrMap::$USER_KEY]  = 'https://facebook.com/uid/'.$uid;
                $map[DbtoOpendidAttrMap::$FULL_NAME_KEY] = $fullname;
                $map[DbtoOpendidAttrMap::$EMAIL_KEY]  = $email;
                ## Destroy facebook session from application after successfully logged in...
                ## Session destroy if exists...
                $facebook->destroySession();
                return $map;
            }
          } catch (FacebookApiException $e) {
              throw new exception ($e);
              $user = null;
          }
        }
        ## Session destroy if exists...
        if($facebook) $facebook->destroySession();
  }
}
