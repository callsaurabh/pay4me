<?php
/***
 * [WP:069] => CR:102
 * This class set facebook appId and secret key...
 */
class FacebookApi extends Facebook{
    
    public function __construct($returnUrl) {

        $config = array(
                  'appId'  => sfConfig::get('app_openid_url_facebook_appId'),
                  'secret' => sfConfig::get('app_openid_url_facebook_secretKey'),
                  'sitePath' => $returnUrl,
                );
        parent::__construct($config);
    }

}
//http://www.designaesthetic.com/2012/03/02/create-facebook-login-oauth-php-sdk/
?>
