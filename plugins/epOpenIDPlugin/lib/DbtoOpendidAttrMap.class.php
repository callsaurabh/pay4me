<?php

class DbtoOpendidAttrMap implements ArrayAccess {

    public static $USER_KEY = "username",
    $FULL_NAME_KEY = "fullname",
    $FIRST_NAME_KEY = "firstname",
    $LAST_NAME_KEY = "lastname",
    $EMAIL_KEY = "email";
    protected $attr = array();

    public function setUserName($user) {
        $this->attr[self::$USER_KEY] = $user;
    }

    public function getUserName() {
        $this->attr[self::$USER_KEY] = $user;
    }

    public function setFirstName($user) {
        $this->attr[self::$FIRST_NAME_KEY] = $user;
    }

    public function getFirstName() {
        $this->attr[self::$FIRST_NAME_KEY] = $user;
    }
    
    public function setLastName($user) {
        $this->attr[self::$LAST_NAME_KEY] = $user;
    }

    public function getLastName() {
        $this->attr[self::$LAST_NAME_KEY] = $user;
    }

    
    public function setEmail($user) {
        $this->attr[self::$EMAIL_KEY] = $user;
    }

    public function getEmail() {
        $this->attr[self::$EMAIL_KEY] = $user;
    }

    /**
     * @param offset
     */
    public function offsetExists($offset) {
        return in_array($offset, $this->attr);
    }

    /**
     * @param offset
     */
    public function offsetGet($offset) {
        return $this->attr[$offset];
    }

    /**
     * @param offset
     * @param value
     */
    public function offsetSet($offset, $value) {
        $this->attr[$offset] = $value;
    }

    /**
     * @param offset
     */
    public function offsetUnset($offset) {
        unset($this->attr[$offset]);
    }

}