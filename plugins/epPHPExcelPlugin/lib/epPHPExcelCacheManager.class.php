<?php

class epPHPExcelCacheManager {
  public static $APP_EXCEL_CACHE_VAR_NAME = 'plugin_epexcel_cache';

  public static function configureCache() {
    $app_yml_var_name = 'app_'.self::$APP_EXCEL_CACHE_VAR_NAME.'_method';
    if(!sfConfig::has($app_yml_var_name)) {
      self::configureDefault();
      return;
    }
    $app_cache_setting = sfConfig::get($app_yml_var_name);
    $params = self::getCacheParams();
    self::setupCache($app_cache_setting,$params);
  }

  public static function getCacheParams() {
    $app_yml_var_name = 'app_'.self::$APP_EXCEL_CACHE_VAR_NAME.'_params';
    if(!sfConfig::has($app_yml_var_name)) {
      return array();
    }
    return sfConfig::get($app_yml_var_name);
  }

  public static function configureDefault() {
    self::setupCache('cache_to_phpTemp',array('memoryCacheSize' => '8MB'));
  }

  public static function setupCache($method, $cacheSettings=array()) {
    sfContext::getInstance()->getLogger()->info('Method: '.$method.', settings: '.print_r($cacheSettings, true));
    $cacheMethod = constant(sprintf('%s::%s', 'PHPExcel_CachedObjectStorageFactory', $method));
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
  }

}