<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpNibssManager {

    //put your code here


    public $request_date_time;
    public $recharge_amt;
    public $request_email;
    public $ewallet_acc_num;
    public $tran_num;
    public $request_type;
    public $session_id;
    public $application_type;
    public $payee_name;
    public $app_charge;
    public $tran_charge;
    public $amount;
    public $service_charge;
    public $bank_acc_name;
    public $total_amount;
    public $channel_code;
    public $query_request_id;
    public $confirmation_request_id;
    public $validationNo;
    public $resp_code;
    public $total_amt;
    public $order_id;

   

    public function setConfirmationRequest() {
        $obj = new EpInternetBankConfirmationRequest();
        $obj->setOrderId($this->order_id);
        $obj->setEwalletNumber($this->ewallet_aact_num);
        $obj->setTransactionNumber($this->tran_num);
        $obj->setSessionId($this->sessionid);
        $obj->setTransactionDate($this->request_date);
        $obj->setApplicationType($this->app_type);
        $obj->setName($this->acct_name);
        $obj->setApplicationCharge($this->app_charge);
        $obj->setTransactionCharge($this->tran_charge);
        $obj->setAmount($this->amt);
        $obj->setServiceCharge($this->service_charge);
        $obj->setBankAccountName($this->bank_acct_name);
        $obj->setTotalAmount($this->total_amount);
        $obj->setChannelCode($this->channel_code);
        $obj->save();

        return $obj;
    }
/**Function  for request confiramation
 * Developed By: Deepak Bhardwaj
 */

    public function saveConfirmationRequest($values) {

        $obj = new EpInternetBankConfirmationRequest();
        if (isset($values['order_id'])) {
            $obj->setOrderId($values['order_id']);
        }
        if (isset($values['ewalletAccountNumber'])) {
            $obj->setEwalletNumber($values['ewalletAccountNumber']);
        }
        if (isset($values['tran_num'])) {
            $obj->setTransactionNumber($values['tran_num']);
        }
        if (isset($values['sessionId'])) {
            $obj->setSessionId($values['sessionId']);
        }
        if (isset($values['requestDate'])) {
            $obj->setTransactionDate($values['requestDate']);
        }
        if (isset($values['application_type'])) {
            $obj->setApplicationType($values['application_type']);
        }
        if (isset($values['accountName'])) {
            $obj->setName($values['accountName']);
        }
        if (isset($values['app_charge'])) {
            $obj->setApplicationCharge($values['app_charge']);
        }
        if (isset($values['tran_charge'])) {
            $obj->setTransactionCharge($values['tran_charge']);
        }
        if (isset($values['amount'])) {
            $obj->setAmount($values['amount']);
        }
        if (isset($values['serviceCharge'])) {
            $obj->setServiceCharge($values['serviceCharge']);
        }
        if (isset($values['bankAccountName'])) {
            $obj->setBankAccountName($values['bankAccountName']);
        }
        if (isset($values['totalAmount'])) {
            $obj->setTotalAmount($values['totalAmount']);
        }
        if (isset($values['channelCode'])) {
            $obj->setChannelCode($values['channelCode']);
        }
        $obj->save();

        return $obj;
    }
    /**Function  for request confiramation
 * Developed By: Deepak Bhardwaj
 */

    public function setNibssConfirmationRequest($query_req_arr) {
    	//echo "<pre/>";
    	//print_r($query_req_arr);die;
    	$obj = new EpInternetBankConfirmationRequest();
    	$obj->setOrderId($query_req_arr['order_id']);
    	$obj->setEwalletNumber(NULL);
    	$obj->setTransactionNumber($query_req_arr['tran_num']);
    	$obj->setSessionId($query_req_arr['session_id']);
    	$obj->setTransactionDate(NULL);
    	$obj->setApplicationType($query_req_arr['app_type']);
    	$obj->setName($query_req_arr['payee_name']);
    	$obj->setApplicationCharge($query_req_arr['app_charge']);
    	$obj->setTransactionCharge($query_req_arr['tran_charge']);
    	$obj->setAmount($this->amount);
    	$obj->setServiceCharge($query_req_arr['serv_charge']);
    	$obj->setBankAccountName($query_req_arr['bank_acc_name']);
    	$obj->setTotalAmount($query_req_arr['total_amount']);
    	$obj->setChannelCode($query_req_arr['channel_code']);
    	$obj->save();
    
    	return $obj;
    }

    public function setConfirmationResponse() {

        $obj = new EpInternetBankConfirmationResponse();
        $obj->setRequestId($this->confirmation_request_id);
        $obj->setEwalletNumber($this->ewallet_acc_num);
        $obj->setTransactionNumber($this->tran_num);
        $obj->setSessionId($this->session_id);
        $obj->setValidationNumber($this->validationNo);
        $obj->setChannelCode($this->channel_code);
        $obj->save();

        return $obj;
    }
    
    /**
	Function  for response confiramation
    */
    public function saveConfirmationResponse($values) {

        $obj = new EpInternetBankConfirmationResponse();
        $obj->setRequestId($values['confirmation_request_id']);
        $obj->setEwalletNumber($values['ewalletAccountNumber']);
        $obj->setTransactionNumber($values['tran_num']);
        $obj->setSessionId($values['sessionId']);
        $obj->setValidationNumber($values['validation_no']);
        $obj->setChannelCode($values['channelCode']);
        $obj->save();

        return $obj;
    }

/**Function  for response confiramation
     * Developed By: Deepak Bhardwaj
    */
    
    public function setNibssConfirmationResponse($query_req_arr) {
    	$obj = new EpInternetBankConfirmationResponse();
    	$obj->setRequestId($query_req_arr['confirmation_request_id']);
    	$obj->setEwalletNumber(NULL);
    	$obj->setTransactionNumber($query_req_arr['tran_num']);
    	$obj->setSessionId($query_req_arr['session_id']);
    	$obj->setValidationNumber($query_req_arr['validation_no']);
    	$obj->setChannelCode($query_req_arr['channel_code']);
    	$obj->save();
    
    	return $obj;
    }

    public function setVerifyRequest() {
        $obj = new EpInternetBankVerifyRequest();
        $obj->setOrderId($this->order_id);
        $obj->setTransactionNumber($this->tran_num);
        $obj->setEwalletNumber($this->ewallet_acc_num);
        $obj->setSessionId($this->session_id);
        $obj->save();
        return $obj;
    }

    public function saveVerifyRequest($values) {
        $obj = new EpInternetBankVerifyRequest();
        $obj->setOrderId($values['order_id']);
        $obj->setTransactionNumber($values['tran_num']);
        $obj->setEwalletNumber($values['ewallet_acc_num']);
        $obj->setSessionId($values['session_id']);
        $obj->save();
        return $obj;
    }

    /*
     * parsing of recharge query request xml
     */
    public function fetchQueryRequest($ewallet_validation_request) {

        //fetch data from xml
        $ewallet_acc_num = $ewallet_validation_request->item(0)->getElementsByTagName('eWalletAccount')->item(0)->nodeValue;
        $request_date_time = $ewallet_validation_request->item(0)->getElementsByTagName('TransactionDateTime')->item(0)->nodeValue;
        $recharge_amt = $ewallet_validation_request->item(0)->getElementsByTagName('Amount')->item(0)->nodeValue;
        return $req = array('ewalletAccountNumber'=>$ewallet_acc_num,'requestDate'=>$request_date_time,'amount'=>$recharge_amt);
    }


    public function fetchQueryRequestPayment($payment_validation_request) {

        //fetch data from xml
        $tran_num = $payment_validation_request->item(0)->getElementsByTagName('TransactionNumber')->item(0)->nodeValue;
        $request_date_time = $payment_validation_request->item(0)->getElementsByTagName('TransactionDateTime')->item(0)->nodeValue;
        $email = $payment_validation_request->item(0)->getElementsByTagName('EmailAddress')->item(0)->nodeValue;
        return $req = array('tran_num'=>$tran_num,'request_date'=>$request_date_time,'email'=>$email);
    }


    public function fetchConfirmationRequestPayment($payment_confirmation_request) {
        //fetch data from xml
        $tran_number = $payment_confirmation_request->item(0)->getElementsByTagName('TransactionNumber')->item(0)->nodeValue;
        $SessionID = $payment_confirmation_request->item(0)->getElementsByTagName('SessionID')->item(0)->nodeValue;
        $tran_dTime = $payment_confirmation_request->item(0)->getElementsByTagName('TransactionDateTime')->item(0)->nodeValue;
        //$app_id = $payment_confirmation_request->item(0)->getElementsByTagName('ApplicationIDNumber')->item(0)->nodeValue;
       // $ref_num = $payment_confirmation_request->item(0)->getElementsByTagName('ReferenceNumber')->item(0)->nodeValue;
        $app_type = $payment_confirmation_request->item(0)->getElementsByTagName('ApplicationType')->item(0)->nodeValue;
        $payee_name = $payment_confirmation_request->item(0)->getElementsByTagName('PayeeName')->item(0)->nodeValue;
        $app_charge = $payment_confirmation_request->item(0)->getElementsByTagName('ApplicationCharge')->item(0)->nodeValue;
        $tran_charge = $payment_confirmation_request->item(0)->getElementsByTagName('TransactionCharge')->item(0)->nodeValue;
        $serv_charge = $payment_confirmation_request->item(0)->getElementsByTagName('ServiceCharge')->item(0)->nodeValue;
        $bank_acc_name = $payment_confirmation_request->item(0)->getElementsByTagName('BankAccountName')->item(0)->nodeValue;
        $total_amount = $payment_confirmation_request->item(0)->getElementsByTagName('TotalPayable')->item(0)->nodeValue;
        $channel_code = $payment_confirmation_request->item(0)->getElementsByTagName('ChannelCode')->item(0)->nodeValue;
        return $req = array('tran_num'=>$tran_number,'session_id'=>$SessionID,'request_date'=>$tran_dTime,
                            //'app_id'=>$app_id,'ref_num'=>$ref_num,'app_type'=>$app_type,
                            'payee_name'=>$payee_name,'app_charge'=>$app_charge,'tran_charge'=>$tran_charge,
                            'serv_charge'=>$serv_charge,'bank_acc_name'=>$bank_acc_name,'total_amount'=>$total_amount,
                            'channel_code'=>$channel_code);
    }

    /*
     * parsing of recharge confirmation request xml
     */
    public function fetchConfirmationRequest($ewallet_validation_request) {
        
        //fetch data from xml
        $ewallet_acc_num = $ewallet_validation_request->item(0)->getElementsByTagName('eWalletAccount')->item(0)->nodeValue;
        $session_id = $ewallet_validation_request->item(0)->getElementsByTagName('SessionID')->item(0)->nodeValue;
        $request_date_time = $ewallet_validation_request->item(0)->getElementsByTagName('TransactionDateTime')->item(0)->nodeValue;
        $acct_name = $ewallet_validation_request->item(0)->getElementsByTagName('eWalletAccountName')->item(0)->nodeValue;
        $bank_acct_name = $ewallet_validation_request->item(0)->getElementsByTagName('BankAccountName')->item(0)->nodeValue;
        $amt = $ewallet_validation_request->item(0)->getElementsByTagName('Amount')->item(0)->nodeValue;
        //$service_charge= $ewallet_validation_request->item(0)->getElementsByTagName('ServiceCharge')->item(0)->nodeValue;
	$service_charge= $ewallet_validation_request->item(0)->getElementsByTagName('Nibss_Charges')->item(0)->nodeValue;
        //$total_amount = $ewallet_validation_request->item(0)->getElementsByTagName('TotalPayable')->item(0)->nodeValue;
	$total_amount = $amt + $service_charge;
        $channel_code = $ewallet_validation_request->item(0)->getElementsByTagName('ChannelCode')->item(0)->nodeValue;
        return $req = array('ewallet_aact_num'=>$ewallet_acc_num,'request_date'=>$request_date_time,'amt'=>$amt,'sessionid'=>$session_id,'acct_name'=>$acct_name,
            'bank_acct_name'=>$bank_acct_name,'channel_code'=>$channel_code,'service_charge'=>$service_charge, 'total_amount'=>$total_amount);
    }
 /*
  * recharge query response xml
  */
    public function setQueryResponseXML($query_req_arr){
        $xdoc = new DOMDocument('1.0', 'UTF-8');
        $domNode = $xdoc->createElement('eWalletValidationResponse');
        $ewallet_acc_number = $xdoc->createElement('eWalletAccount', $query_req_arr['ewalletAccountNumber']);
        $acc_name = $xdoc->createElement('eWalletAccountName', $query_req_arr['accountName']);
        $amount = $xdoc->createElement('Amount', $query_req_arr['amount']);
        $service_charge = $xdoc->createElement('ServiceCharge', $query_req_arr['serviceCharge']);
        $total_amount = $xdoc->createElement('TotalPayable', $query_req_arr['totalAmount']);
        $respcode = $xdoc->createElement('ResponseCode', $query_req_arr['responseCode']);
        $address = $xdoc->createElement('Address', $query_req_arr['address']);
        $email = $xdoc->createElement('Email', $query_req_arr['email']);
        $mobileNumber = $xdoc->createElement('MobileNumber', $query_req_arr['mobileNumber']);
        $domNode->appendChild($ewallet_acc_number);
        $domNode->appendChild($acc_name);
        $domNode->appendChild($amount);
        $domNode->appendChild($service_charge);
        $domNode->appendChild($total_amount);
        $domNode->appendChild($respcode);
        $domNode->appendChild($address);
        $domNode->appendChild($email);
        $domNode->appendChild($mobileNumber);
        $xdoc->appendChild($domNode);
        return $ewalletValidationResponse = $xdoc->saveXML();
    }

    /*
     * recharge confirmation response xml
     */
    public function setConfirmationResponseXML($confirmation_req_arr){
        $xdoc = new DOMDocument('1.0', 'UTF-8');
        $domNode = $xdoc->createElement('eWalletPaymentConfirmationResponse');
        $ewallet_acc_number = $xdoc->createElement('eWalletAccount', $confirmation_req_arr['ewallet_aact_num']);
        $session_id = $xdoc->createElement('SessionID', $confirmation_req_arr['sessionid']);
        $validation_no = $xdoc->createElement('ValidationIDNumber', $confirmation_req_arr['validation_no']);
        $channelcode = $xdoc->createElement('ChannelCode', $confirmation_req_arr['channel_code']);
        $domNode->appendChild($ewallet_acc_number);
        $domNode->appendChild($session_id);
        $domNode->appendChild($validation_no);
        $domNode->appendChild($channelcode);
        $xdoc->appendChild($domNode);
        return $ewalletConfirmationResponse = $xdoc->saveXML();
    }
    /*
  * payment query response xml
  */
 public function setQueryResponseXMLPayment($pfmTransactionDetails,$NibssObj,$responseArr = array()){
      $xdoc = new DOMDocument('1.0', 'UTF-8');
      $domNode = $xdoc->createElement('CollectionValidationResponse');
      $transactionId = $NibssObj['tran_num'];
      $tran_number = $xdoc->createElement('TransactionNumber', $transactionId);

      $app_type = $xdoc->createElement('ApplicationType', $pfmTransactionDetails['MerchantRequest']['MerchantService']['name']);
      $payee_name = $xdoc->createElement('PayeeName', $pfmTransactionDetails['MerchantRequest']['MerchantRequestDetails']['name']);
      // Bug: [19462], [21-MAR-2013] using number_format function to make integer numbers to floating numbers
      $application_charges = $pfmTransactionDetails['MerchantRequest']['item_fee']?$this->numberFormat($pfmTransactionDetails['MerchantRequest']['item_fee']):'';
     // $transaction_charges = $pfmTransactionDetails['MerchantRequest']['bank_charge']?$this->numberFormat($pfmTransactionDetails['MerchantRequest']['bank_charge']):'';
      //$service_charges = $pfmTransactionDetails['MerchantRequest']['service_charge']?$this->numberFormat($pfmTransactionDetails['MerchantRequest']['service_charge']):'';
      $transaction_charges = 100;
      $app_charge = $xdoc->createElement('ApplicationCharge', $application_charges);
	  $service =$application_charges * sfConfig::get('app_nibss_svc_charge');
      $tran_charge = $xdoc->createElement('TransactionCharge', $transaction_charges);
      $serv_charge = $xdoc->createElement('ServiceCharge', $service);
      $total_payable = $application_charges + $transaction_charges + $service ;
      $total_payable = $total_payable?$this->numberFormat($total_payable):'';
      $total_amount = $xdoc->createElement('TotalPayable', $total_payable);
      $respcode = $xdoc->createElement('ResponseCode', $responseArr['0']);
      $domNode->appendChild($tran_number);
//      foreach ($ValRule as $param => $value){
//          $domNode->appendChild($value);
//      }
      $domNode->appendChild($app_type);
      $domNode->appendChild($payee_name);
      $domNode->appendChild($app_charge);
      $domNode->appendChild($tran_charge);
      $domNode->appendChild($serv_charge);
      $domNode->appendChild($total_amount);
      $domNode->appendChild($respcode);
      $xdoc->appendChild($domNode);
      return $paymentValidationResponse = $xdoc->saveXML();
    }

     /*
     * payment confirmation response xml
     */
    public function setConfirmationResponseXMLPayment($NibssObj){
      $xdoc = new DOMDocument('1.0', 'UTF-8');
      $domNode = $xdoc->createElement('CollectionPaymentConfirmationResponse');
      $tran_number = $xdoc->createElement('TransactionNumber', $NibssObj['tran_num']);
      $session_id = $xdoc->createElement('SessionID', $NibssObj['session_id']);
      $validation_no = $xdoc->createElement('ValidationIDNumber', $NibssObj['validation_no']);
      $channelcode = $xdoc->createElement('ChannelCode', $NibssObj['channel_code']);
      $domNode->appendChild($tran_number);
      $domNode->appendChild($session_id);
      $domNode->appendChild($validation_no);
      $domNode->appendChild($channelcode);
      $xdoc->appendChild($domNode);
      return $paymentConfirmationResponse = $xdoc->saveXML();
    }

    public function setErrorXML($resp_code,$resp_msg){
      $xdoc = new DOMDocument('1.0', 'UTF-8');
      $domNode = $xdoc->createElement('ErrorXML');
      $error_code = $xdoc->createElement('error-code', $resp_code);
      $error_message = $xdoc->createElement('error-message', $resp_msg);
      $domNode->appendChild($error_code);
      $domNode->appendChild($error_message);
      $xdoc->appendChild($domNode);
      return $ErrorXml = $xdoc->saveXML();
    }

      /*
     * Verify Request Payment xml
     */
    public function setVerifyRequestXML($app_id,$session_id,$type){
      $xdoc = new DOMDocument('1.0', 'UTF-8');
      if($type == 'payment'){
        $domNode = $xdoc->createElement('CollectionTransactionQueryRequest');
        $tran_number = $xdoc->createElement('TransactionNumber', $app_id);
      }
      else{
        $domNode = $xdoc->createElement('eWalletTransactionQueryRequest');
        $tran_number = $xdoc->createElement('eWalletAccount', $app_id);
      }
      
      $session_id = $xdoc->createElement('SessionID', $session_id);
      $domNode->appendChild($tran_number);
      $domNode->appendChild($session_id);
      $xdoc->appendChild($domNode);
      return $VerifyRequest = $xdoc->saveXML();
    }

    

     public function numberFormat($amount){
      return number_format($amount,2,'.','');
  }

  public function selectAllFromRequest($trnxId) {
        try {
            $q = Doctrine_Query::create()
                            ->select('*')
                            ->from('EpInternetBankConfirmationRequest')
                            ->where('order_id  = ?', $trnxId);
            return $q->fetchArray();
        } catch (Exception $e) {

            echo("Problem found" . $e->getMessage());
            die;
        }
    }

  public function updateconfirmationRequest($order_id,$request_id) {
        try {
          $q = Doctrine_Query::create();
          $q->update('EpInternetBankConfirmationRequest');
          $q->set('order_id', '?', $order_id);
          $q->where("id=?", $request_id);
          $q->execute();
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

   public function verifyTransaction($xml, $verfiy_request_id,$type)
   {
       //$this->createLog($xml,'create_order_request_log_'.$gatewayOrderId );
       if($type == 'payment')
            $url= sfConfig::get('app_internet_bank_request_url_payment');
       else
            $url= sfConfig::get('app_internet_bank_request_url_payment');
       
       $header[] = "Content-Type: text/xml;charset=UTF-8";
       $curl = curl_init();
       curl_setopt($curl, CURLOPT_URL, $url);
       curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
       curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
       curl_setopt($curl, CURLOPT_POST, 1);
       curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
       curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
       $response = curl_exec($curl);
       
       $this->createLog($response,'verify_response');
       $internetbankResponse = $this->processResponse($response,$verfiy_request_id,$type);
       return $internetbankResponse;
   }

   public function processResponse($response,$verfiy_request_id,$type){
       $xdoc = new DomDocument;
       $internetbankResponse = '';
       if($response!='' && $xdoc->LoadXML($response))
       {
           if($type == 'payment')
              $responses = $xdoc->getElementsByTagName( "CollectionTransactionQueryResponse" );
           else
              $responses = $xdoc->getElementsByTagName( "eWalletTransactionQueryResponse" );
           foreach( $responses as $response )
           {
               $internetbankResponse = $this->processResponseXML($response,$type);
               $this->SaveVerifyResponse($internetbankResponse,$verfiy_request_id,$type);
           }
       }else{
           return EpNibssResponseManager::$RESPONSE_SYSTEM_MALFUNCTION;
       }
      return $internetbankResponse;

   }

   public function processResponseXML($response,$type){
       $status = $response->getElementsByTagName( "Status" )->item(0)->nodeValue;
       if ($status =='00'){
           $sessionId = $response->getElementsByTagName( "SessionID" )->item(0)->nodeValue;
           $transactionDate = $response->getElementsByTagName( "TransactionDateTime" )->item(0)->nodeValue;
           $serviceCharge = $response->getElementsByTagName( "ServiceCharge" )->item(0)->nodeValue;
           $totalAmount = $response->getElementsByTagName( "TotalPayable" )->item(0)->nodeValue;
           $channelCode = $response->getElementsByTagName( "ChannelCode" )->item(0)->nodeValue;
           $bankAccountName = $response->getElementsByTagName( "BankAccountName" )->item(0)->nodeValue;

           if($type == 'payment'){
               $tranNum = $response->getElementsByTagName( "TransactionNumber" )->item(0)->nodeValue;
               $applicationType = $response->getElementsByTagName( "ApplicationType" )->item(0)->nodeValue;
               $name = $response->getElementsByTagName( "PayeeName" )->item(0)->nodeValue;
               $applicationCharge = $response->getElementsByTagName( "ApplicationCharge" )->item(0)->nodeValue;
               $transactionCharge = $response->getElementsByTagName( "TransactionCharge" )->item(0)->nodeValue;

               $internetbankResponse= array('tranNum'=>$tranNum,'sessionId'=> $sessionId,
                                        'transactionDate'=>$transactionDate,'applicationType'=> $applicationType,
                                        'name'=>$name,'applicationCharge'=> $applicationCharge,
                                        'transactionCharge'=>$transactionCharge,'serviceCharge'=> $serviceCharge,
                                        'bankAccountName'=>$bankAccountName,'totalAmount'=> $totalAmount,
                                        'channelCode'=>$channelCode,'status'=> $status);
           }else{
               $ewalletNumber = $response->getElementsByTagName( "eWalletAccount" )->item(0)->nodeValue;
               $name = $response->getElementsByTagName( "eWalletAccountName" )->item(0)->nodeValue;
               $amount = $response->getElementsByTagName( "Amount" )->item(0)->nodeValue;

               $internetbankResponse= array('ewalletNumber'=>$ewalletNumber,'sessionId'=> $sessionId,
                                        'transactionDate'=>$transactionDate,'amount' => $amount,
                                        'name'=>$name,'serviceCharge'=> $serviceCharge,
                                        'bankAccountName'=>$bankAccountName,'totalAmount'=> $totalAmount,
                                        'channelCode'=>$channelCode,'status'=> $status);
           }
           
      }else{
           $internetbankResponse= array('status'=> $status);
       }
       
       return $internetbankResponse;
   }

   public function SaveVerifyResponse($internetbankResponse,$verfiy_request_id,$type){
        $obj = new EpInternetBankVerifyResponse();
        $obj->setStatus($internetbankResponse['status']);
        $obj->setRequestId($verfiy_request_id);
        if($internetbankResponse['status'] == 00){
            $obj->setSessionId($internetbankResponse['sessionId']);
            $obj->setTransactionDate($internetbankResponse['transactionDate']);
            $obj->setServiceCharge($internetbankResponse['serviceCharge']);
            $obj->setTotalAmount($internetbankResponse['totalAmount']);
            $obj->setChannelCode($internetbankResponse['channelCode']);
            $obj->setBankAccountName($internetbankResponse['bankAccountName']);
            $obj->setName($internetbankResponse['name']);
            if($type == 'payment'){
                $obj->setTransactionNumber($internetbankResponse['tranNum']);
                $obj->setApplicationType($internetbankResponse['applicationType']);
                $obj->setApplicationCharge($internetbankResponse['applicationCharge']);
                $obj->setTransactionCharge($internetbankResponse['transactionCharge']);
            }else{
                $obj->setEwalletNumber($internetbankResponse['ewalletNumber']);
                $obj->setAmount($internetbankResponse['amount']);
            }
        }
        $obj->save();
        return $obj;
       
   }
  
   public function createLog($xmldata, $type) {
    $nameFormate = $type;
    $pay4meLog = new pay4meLog();
    $pay4meLog->createLogData($xmldata,$nameFormate,'nibsslog');
    return $filename = $pay4meLog->getFileName();
  }

  /* Function to validate payment confirmation request*
 * return 0 if confirmation request details are not validated
 * return 1 if confirmation details are validated and validation number is not generated. means transaction is valid and new
 * return 2 if confirmation details are valdiated and valdiation number is also generated, means transaction is already paid
 */
    public function ValidateDetails($pfmTransactionDetails,$query_req_arr){
  	$app_name = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name']; // service name
        $validation_num = $pfmTransactionDetails['MerchantRequest']['validation_number'];// validation number
        $item_fee = $pfmTransactionDetails['MerchantRequest']['item_fee']; //application charges
        $bank_charge = $pfmTransactionDetails['MerchantRequest']['bank_charge']; // transaction charges
        $serv_charge = $pfmTransactionDetails['MerchantRequest']['service_charge']; // service charges
        $total_charge = $item_fee + $bank_charge + $serv_charge;
        if($app_name==$query_req_arr['app_type'] && (!$validation_num)){
            $validateItemFee = $this->validatecharges($item_fee,$query_req_arr['app_charge']);
            if(!$validateItemFee)
                return 0;
            $validateTranCharges = $this->validatecharges($bank_charge,$query_req_arr['tran_charge']);
            if(!$validateTranCharges)
                return 0;
            $validateServiceCharges = $this->validatecharges($serv_charge,$query_req_arr['serv_charge']);
            if(!$validateServiceCharges)
                return 0;
            $validateTotalCharges = $this->validatecharges($total_charge,$query_req_arr['total_amount']);
            if(!$validateTotalCharges)
                return 0;
            return '00';
        }
        if($app_name==$query_req_arr['app_type'] && ($validation_num)){
            $validateItemFee = $this->validatecharges($item_fee,$query_req_arr['app_charge']);
            if(!$validateItemFee)
                return 0;
            $validateTranCharges = $this->validatecharges($bank_charge,$query_req_arr['tran_charge']);
            if(!$validateTranCharges)
                return 0;
           $validateServiceCharges = $this->validatecharges($serv_charge,$query_req_arr['serv_charge']);
            if(!$validateServiceCharges)
                return 0;
            /*$validateTotalCharges = $this->validatecharges($total_charge,$query_req_arr['total_amount']);
            if(!$validateTotalCharges)
                return 0;
*/            return '00';
        }
        else
            return 0;
  }


  public function validatecharges($item_fee,$query_item_fee){
      if($item_fee==$query_item_fee)
        return true;
      else if(!$item_fee || $item_fee == 0){
          if(!$query_item_fee || $query_item_fee == 0)
            return true;
          return false;
      }
      return false;
  }
  
  

}

?>
