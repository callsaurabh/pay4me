<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpNibssResponseManager {
  //put your code here


    public static $RESPONSE_APPROVED = '00';
    public static $RESPONSE_INVALID_SENDER = '03';
    public static $RESPONSE_DO_NOT_HONOR = '05';
    public static $RESPONSE_DORMANT_ACCOUNT = '06';
    public static $RESPONSE_INVALID_ACCOUNT = '07';
    public static $RESPONSE_ACCOUNT_NAME_MISMATCH = '08';
    public static $RESPONSE_REQUEST_PROCESSING_IN_PROGRESS = '09';
    public static $RESPONSE_INVALID_TRANSACTION = '12';
    public static $RESPONSE_INVALID_AMOUNT = '13';
    public static $RESPONSE_INVALID_TRANSACTION_NUMBER = '14';
    public static $RESPONSE_INVALID_SESSION_OR_RECORD_ID = '15';
    public static $RESPONSE_UNKOWN_BANK_CODE = '16';
    public static $RESPONSE_INVALID_CHANNEL = '17';
    public static $RESPONSE_WRONG_METHOD_CALL = '18';
    public static $RESPONSE_NO_ACTION_TAKEN = '21';
    public static $RESPONSE_UNABLE_TO_LOCATE_RECORD = '25';
    public static $RESPONSE_DUPLICATE_RECORD = '26';
    public static $RESPONSE_FORMAT_ERROR = '30';
    public static $RESPONSE_SUSPECTED_FRAUD = '34';
    public static $RESPONSE_CONTACT_SENDING_BANK = '35';
    public static $RESPONSE_NO_SUFFICIENT_FUNDS = '51';
    public static $RESPONSE_TRANSACTION_NOT_PERMITTED_TO_SENDER = '57';
    public static $RESPONSE_TRANSACTION_NOT_PERMITTED_ON_CHANNEL = '58';
    public static $RESPONSE_TRANSFER_LIMIT_EXCEEDED = '61';
    public static $RESPONSE_SECURITY_VIOLATION = '63';
    public static $RESPONSE_EXCEEDS_WITHDRAWAL_FREQUENCY = '65';
    public static $RESPONSE_RESPONSE_RECEIVED_TOO_LATE = '68';
    public static $RESPONSE_BENEFICIARY_BANK_NOT_AVAILABLE = '91';
    public static $RESPONSE_ROUTING_ERROR = '92';
    public static $RESPONSE_DUPLICATE_TRANSACTION = '94';
    public static $RESPONSE_SYSTEM_MALFUNCTION = '96';
    public static $MSG_INVALID_ACCOUNT = 'Account Details Not Correct';
    public static $MSG_INVALID_AMOUNT = 'Invalid Amount';
    public static $MSG_MISMATCH_AMOUNT = 'Amount to be recharged does not match';
    public static $MSG_TAG_MISMATCH = 'Mandatory Tags Missing';
  
}
?>