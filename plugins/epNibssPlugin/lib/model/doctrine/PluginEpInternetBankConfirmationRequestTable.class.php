<?php

/**
 * PluginEpInternetBankConfirmationRequestTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginEpInternetBankConfirmationRequestTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginEpInternetBankConfirmationRequestTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginEpInternetBankConfirmationRequest');
    }
}