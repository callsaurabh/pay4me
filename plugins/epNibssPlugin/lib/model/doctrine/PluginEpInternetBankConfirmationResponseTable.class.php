<?php

/**
 * PluginEpInternetBankConfirmationResponseTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginEpInternetBankConfirmationResponseTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginEpInternetBankConfirmationResponseTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginEpInternetBankConfirmationResponse');
    }
}