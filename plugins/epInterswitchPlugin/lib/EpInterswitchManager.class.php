<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpInterswitchManager {

    public $postUrl;
    public $productId;
    public $site_redirect_url;
    public $site_name;
    public $payItemId;
    public $payItem_name;
    public $custId;
    public $custName;

    public function __construct($payItemId="", $payItemName="", $custId='', $custName='') {
      
        if ($payItemId == "") {
            $this->payItemId = sfConfig::get('app_interswitch_pay_item_id');
        } else {
            $this->payItemId = $payItemId;
        }
        if ($payItemName == "") {
            $this->payItem_name = sfConfig::get('app_interswitch_pay_item_name');
        } else {
            $this->payItem_name = $payItemName;
        }
        $this->custId = $custId;
        $this->custName = $custName;

        $this->postUrl = sfConfig::get('app_interswitch_post_url');


        $this->site_redirect_url = sfConfig::get('app_interswitch_site_redirect_url');
        $this->site_name = sfConfig::get('app_interswitch_site_name');
       
        $this->productId = sfConfig::get('app_interswitch_product_id');
    }

    public function getConfigurationDetails() {

    }

    

    public function setRequest() {
        $purchase_amt = $this->amount * 100;
        $visual_amt = $this->amount;

        if ($this->trnxId == '') {
            $this->trnxId = $this->generateTransactionId();
        }

        $obj = new EpInterswitchRequest();
        $obj->setTrnxId($this->trnxId);
        $obj->setAmountNaira($visual_amt);
        $obj->setAmountKobo($purchase_amt);
        $obj->setCurrency($this->currency);
        $obj->setPostUrl($this->postUrl);
        $obj->setUserId($this->userId);
        $obj->setCustId($this->custId);   //will be dynamic

        $obj->setCustIdDescription(sfConfig::get('app_interswitch_cust_id_desc'));   //will be dynamic
        $obj->setCustNameDescription(sfConfig::get('app_interswitch_cust_name_desc'));   //will be dynamic
        $obj->setCustName($this->custName);   //will be dynamic
        try {
            $obj->save();
            
            return $obj;
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    protected function generateTransactionId() {
        do {
            $trnxId = time() . mt_rand(10000, 99999);
            $checkTrnxId = $this->CheckDuplicacy($trnxId);
        } while ($checkTrnxId > 0);
        return $trnxId;
    }

    protected function CheckDuplicacy($trnxId) {
        try {
            $q = Doctrine_Query::create()
                            ->select('count(*)')
                            ->from('EpInterswitchRequest')
                            ->where('trnx_id = ?', $trnxId);

            return $q->count();
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function selectAllFromRequest($trnxId) {
        try {
            $q = Doctrine_Query::create()
                            ->select('*')
                            ->from('EpInterswitchRequest')
                            ->where('trnx_id  = ?', $trnxId);
            return $q->fetchArray();
        } catch (Exception $e) {

            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function saveResponse($getData) {

        $date = date('Y-m-d H:i:s');
        $getReqData = $this->selectAllFromRequest($getData['txnref']);
        $save = new EpInterswitchResponse();
        $save->setInterswitchId($getReqData[0]['id']);
        $save->setDescription($getData['desc']);
        $save->setApprAmt($getData['amount']);
        $save->setRspDate($date);
        $save->setRspCode($getData['respCode']);
        $save->setCardNum($getData['cardNum']);
        $save->setPayRef($getData['payRef']);
        $save->setRetRef($getData['retRef']);
        $save->save();

        $returArr = array();
//        $returArr['cadp_id'] = $getReqData[0]['cadp_id'];
        // $returArr['mert_id'] = $getReqData[0]['mert_id'];
        $returArr['amount_kobo'] = $getReqData[0]['amount_kobo'];
        return $returArr;
    }

    public function saveResponse_old($getData) {


        $expl = explode('/', $getData['dtime']);
        $expl1 = explode(' ', $expl[2]);
        $date = date('Y-m-d H:i:s', strtotime($expl1[0] . "-" . $expl[1] . "-" . $expl[0] . " " . $expl1[1]));
        $getReqData = $this->selectAllFromRequest($getData['txnref']);
        $save = new EpInterswitchResponse();
        $save->setInterswitchId($getReqData[0]['id']);
        $save->setRspDate($date);
        $save->setRspCode($getData['rspcode']);
        $save->setPaidAmount($getData['amt']);
        $save->setDescription($getData['rspdesc']);
        $save->save();

        $returArr = array();
        $returArr['cadp_id'] = $getReqData[0]['cadp_id'];
        $returArr['mert_id'] = $getReqData[0]['mert_id'];
        $returArr['amount_kobo'] = $getReqData[0]['amount_kobo'];
        return $returArr;
    }

    public function saveForPending($interswitchId, $rspcode, $amt, $rspdesc) {
        $transDate = date('Y-m-d H:i:s');
        $save = new EpInterswitchResponse();
        $save->setInterswitchId($interswitchId);
        $save->setRspDate($transDate);
        $save->setRspCode($rspcode);
        $save->setApprAmt($amt);
        $save->setDescription($rspdesc);
        $save->save();
    }

}

?>
