<?php

class InterswitchStatusMessages {

    public static $msgArr = array("msg00" => "Approved by Financial Institution",
        "msg01" => "Refer to Financial Institution",
        "msg02" => "Refer to Financial Institution, Special Condition",
        "msg03" => "Invalid merchant",
        "msg04" => "Pick-up card",
        "msg05" => "Do not honor",
        "msg06" => "Error",
        "msg07" => "Pick-up card, special condition",
        "msg08" => "Honor with identification",
        "msg09" => "Request in progress",
        "msg10" => "Approved by Financial Institution, Partial",
        "msg11" => "Approved by Financial Institution, VIP",
        "msg12" => "Invalid transaction",
        "msg13" => "Invalid amount",
        "msg14" => "Invalid card number",
        "msg15" => "No Such Financial Institution",
        "msg16" => "Approved by Financial Institution, Update Track 3",
        "msg17" => "Customer cancellation",
        "msg18" => "Customer dispute",
        "msg19" => "Re-enter transaction",
        "msg20" => "Invalid Response from Financial Institution",
        "msg21" => "No Action Taken by Financial Institution",
        "msg22" => "Suspected malfunction",
        "msg23" => "Unacceptable transaction fee",
        "msg24" => "File update not supported",
        "msg25" => "Unable to locate record",
        "msg26" => "Duplicate record",
        "msg27" => "File update field edit error",
        "msg28" => "File update file locked",
        "msg29" => "File update failed",
        "msg30" => "Format error",
        "msg31" => "Bank not supported",
        "msg32" => "Completed Partially by Financial Institution",
        "msg33" => "Expired card, pick-up",
        "msg34" => "Suspected fraud, pick-up",
        "msg35" => "Contact acquirer, pick-up",
        "msg36" => "Restricted card, pick-up",
        "msg37" => "Call acquirer security, pick-up",
        "msg38" => "PIN tries exceeded, pick-up",
        "msg39" => "No credit account",
        "msg40" => "Function not supported",
        "msg41" => "Lost card, pick-up",
        "msg42" => "No universal account",
        "msg43" => "Stolen card, pick-up",
        "msg44" => "No investment account",
        "msg51" => "Insufficient funds",
        "msg52" => "No check account",
        "msg53" => "No savings account",
        "msg54" => "Expired card",
        "msg55" => "Incorrect PIN",
        "msg56" => "No card record",
        "msg57" => "Transaction not permitted to CardHolder",
        "msg58" => "Transaction not permitted on terminal",
        "msg59" => "Suspected fraud",
        "msg60" => "Contact acquirer",
        "msg61" => "Exceeds withdrawal limit",
        "msg62" => "Restricted card",
        "msg63" => "Security violation",
        "msg64" => "Original amount incorrect",
        "msg65" => "Exceeds withdrawal frequency",
        "msg66" => "Call acquirer security",
        "msg67" => "Hard capture",
        "msg68" => "Response received too late",
        "msg75" => "PIN tries exceeded",
        "msg76" => "Reserved for future Postilion use",
        "msg77" => "Intervene, bank approval required",
        "msg78" => "Intervene, bank approval required for partial amount",
        "msg90" => "Cut-off in progress",
        "msg91" => "Issuer or switch inoperative",
        "msg92" => "Routing error",
        "msg93" => "Violation of law",
        "msg94" => "Duplicate transaction",
        "msg95" => "Reconcile error",
        "msg96" => "System malfunction",
        "msg98" => "Exceeds cash limit",
        "msgA0" => "Unexpected Error",
        "msgA4" => "Transaction not Permitted to Card Holders via channels",
        "msgZ0" => "Transaction Status Unconfirmed",
        "msgZ1" => "Transaction Error",
        "msgZ2" => "Bank Account Error",
        "msgZ3" => "Bank Collections Account Error",
        "msgZ4" => "Iterface Integration Error",
        "msgZ5" => "Duplicate Reference Error",
        "msgZ6" => "Incomplete Transaction",
        "msgZ7" => "Transaction Split Pre-processing Error",
        "msgZ8" => "Invalid Card Number, via Channels",
        "msgZ9" => "Transaction not permitted to card holder, via channels",
        "msgF00" => "Approved amount does not match requested amount"
    );

    /**
     * This method return the status message according to $code parameter.
     *
     * @param string $code Status code.
     * @return string Status Message
     */
    public function getStatusMessage($code) {
     
        if (array_key_exists('msg' . $code, self::$msgArr))
            return self::$msgArr['msg' . $code];
        else
            return 'No response received. Please contact your bank';
    }

}
