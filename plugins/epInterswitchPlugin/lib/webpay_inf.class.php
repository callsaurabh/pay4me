<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of webpay_infclass
 *
 * @author akumar1
 */
class webpay_inf {
    public function getRequestXML($transRef)
    {
$msg = '<?xml version="1.0" encoding="utf-8"?>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:' . sfConfig::get('app_xml_string_for_nameSpace') . '="http://tempuri.org/">
                 <soapenv:Header/>
        <soapenv:Body>
              <' . sfConfig::get('app_xml_string_for_nameSpace') . ':getTransactionData>
               <' . sfConfig::get('app_xml_string_for_nameSpace') . ':product_id>' . sfConfig::get("app_interswitch_product_id") . '</' . sfConfig::get('app_xml_string_for_nameSpace') . ':product_id>
               <' . sfConfig::get('app_xml_string_for_nameSpace') . ':trans_ref>' . $transRef . '</' . sfConfig::get('app_xml_string_for_nameSpace') . ':trans_ref>
         </' . sfConfig::get('app_xml_string_for_nameSpace') . ':getTransactionData>
         </soapenv:Body>
        </soapenv:Envelope>';
    return $msg;


    }
    function getStatusMsg($transRef) {
        $url = sfConfig::get('app_interswitch_query_url');

        $header[] = sfConfig::get('app_xml_post');
        $header[] = sfConfig::get('app_xml_host');
        $header[] = 'SOAPAction: "http://tempuri.org/getTransactionData"';
        $header[] = "Content-Type: text/xml; charset=utf-8";
       
        $msg=$this->getRequestXML($transRef);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $msg);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $response = curl_exec($curl);
      return $response;
    }

    

    private function getResponseString_As_Array($webpay_response) {
        $a = new DOMDocument('1.0');
        $a->loadXML($webpay_response);
        $nodeList = $a->getElementsByTagName('getTransactionDataResult');
         return explode(":", $nodeList->item(0)->nodeValue);
    }

    function getTransResponseCode($webpay_response) {
        $arr = $this->getResponseString_As_Array($webpay_response);
       return $arr[0];
    }

    function getTransactionDate($webpay_response) {
        $arr = $this->getResponseString_As_Array($webpay_response);
        $merhantRef = explode("|", $arr[5]);
//        echo "<pre>";
//        echo "jhjhj";
//        print_r($arr[5]);
//        print_r($merhantRef);
//        exit;
        return $merhantRef[3];
    }

//    function getPan($webpay_response) {
//        $webpay_response = str_replace("&", ";;", $webpay_response);
//        $webpay_response = str_replace("amp;", "", $webpay_response);
//        $arr_response = split(";;", $webpay_response);
//
//        $arr_count = count($arr_response);
//        if ($arr_count == 8) {
//            $str_code = $arr_response[1];
//            $arr_code = split("=", $str_code);
//            return $arr_code[1];
//        }
//        if ($arr_count == 2) {
//            return false;
//        }
//        return count($arr_response);
//    }

//    function getPayTxnCode($webpay_response) {
//       $webpay_response = str_replace("&", ";;", $webpay_response);
//        $webpay_response = str_replace("amp;", "", $webpay_response);
//        $arr_response = explode(";;", $webpay_response);
//
//        $arr_count = count($arr_response);
//        if ($arr_count == 8) {
//            $str_code = $arr_response[4];
//            $arr_code = explode("=", $str_code);
//            return $arr_code[1];
//        }
//        if ($arr_count == 2) {
//            return false;
//        }
//        return count($arr_response);
//    }

    function getTransResponseAmount($webpay_response) {
        $arr = $this->getResponseString_As_Array($webpay_response);
        return $arr[2];
    }

   function getTransResponseDescription($webpay_response) {
        $arr = $this->getResponseString_As_Array($webpay_response);
        
        return $arr[1];
    }
    function getTransResponseCardNum($webpay_response) {
        $arr = $this->getResponseString_As_Array($webpay_response);

        return $arr[3];
    }
    function getTransResponsePayRef($webpay_response) {
        $arr = $this->getResponseString_As_Array($webpay_response);

        return $arr[5];
    }
    function getTransResponseRetRef($webpay_response) {
        $arr = $this->getResponseString_As_Array($webpay_response);

        return $arr[6];
    }

}

?>
