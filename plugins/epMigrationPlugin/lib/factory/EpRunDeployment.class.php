<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpRunDeployment extends AbstractEpDeployment {

    private $taskObj = "";
    private $diff = array();
    private $origYml = array();

    private function unlinkCurrentApp() {
        //InstanceHandler::setAppInfo();
        $linkPath = EpCurrentAppInfo::getLinkPath();
        if(@unlink($linkPath)) {
            return EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_UNLINK,
                array('currentAbsolutePath' => $linkPath));
        }
        else {
            throw New EpMigrationException(EpDeployerMessages::getFomattedMessage
                    (EpDeployerMessages::$MSG_UNLINK_FAILURE, array('currentAbsolutePath'=>$linkPath)));
        }
    }

    protected function enableCustomMaintenanceMode($taskObj) {
        $cmd = "php ".EpCurrentAppInfo::getAppPath()."symfony project:disable ".sfConfig::get('sf_environment');
        system($cmd, $result);
        if ($result != 0)
        {
           throw new EpMigrationException($result);
        }
        return EpDeployerMessages::$MSG_CUSTOM_MAINTENANCE;
    }

    protected function disableCustomMaintenanceMode($targetPath) {
        $this->unlinkCurrentApp();
        if (symlink($targetPath, EpCurrentAppInfo::getLinkPath())) {
            return EpDeployerMessages::getFomattedMessage
                    (EpDeployerMessages::$MSG_DISABLE_CUSTOM_MAINTENANCE, array('targetPath' => $targetPath));
        }
    }

    protected function enablePluginsMaintenanceMode($targetPath) {
        $msgUnlink = $this->unlinkCurrentApp();
        if (symlink($targetPath, EpCurrentAppInfo::getLinkPath())) {
            return $msgUnlink.", ".EpDeployerMessages::getFomattedMessage
                    (EpDeployerMessages::$MSG_PLUGIN_CORE_MAINTENANCE, array('targetPath' => $targetPath));
        }
    }

    protected function disablePluginsMaintenanceMode($targetPath) {
        $this->unlinkCurrentApp();
        if (symlink($targetPath, EpCurrentAppInfo::getLinkPath())) {
            return EpDeployerMessages::getFomattedMessage
                    (EpDeployerMessages::$MSG_DISABLE_PLUGIN_MAINTENANCE, array('targetPath' => $targetPath));
        }
    }


    protected function confirmAndMigrate($taskObj, $configFile) {
        $this->taskObj = $taskObj;
        if ($taskObj->askConfirmation(array('Please confirm migration of - ' . $configFile . ".yml",
                    'Are you sure you want to proceed? (y/N)'), null, 'y')) {
            $this->identifyDiff($configFile);
            if(empty($this->diff)) {
                $taskObj->logSection($configFile.".yml","No diff identified");
            }
        }
        else {
            $taskObj->logSection('migrate',EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_MIGRATION_DENIED, array('fileName' => $configFile . ".yml")));
        }
    }

    private function identifyDiff($configFile) {

        $originalYaml = sfFinder::type('file')->name($configFile . '.yml')->prune('vendor', 'plugins')->
                in(array(EpCurrentAppInfo::getConfigDir().DIRECTORY_SEPARATOR,  EpCurrentAppInfo::getAppConfigDir().DIRECTORY_SEPARATOR));

        $app_config_path = sfConfig::get('sf_apps_dir').DIRECTORY_SEPARATOR.self::$APP.DIRECTORY_SEPARATOR."config";

        $newYaml = sfFinder::type('file')->name($configFile . '.yml')->prune('vendor', 'plugins')->in(array(sfConfig::get('sf_config_dir'),$app_config_path));


        foreach ($originalYaml as $originalFile) {
            $srcFileInfo =  pathinfo($originalFile);
            $srcFilePath = $srcFileInfo['dirname'] . DIRECTORY_SEPARATOR . $srcFileInfo['basename'];
            $parameters = array();
            $configHandler = new EpConfigHandler(self::$CONFIG_FILES[$configFile]);
            $origYml = $configHandler->execute(array($originalFile));
        }
        foreach ($newYaml as $newFile) {
            $destFileInfo = pathinfo($newFile);
            $destFilePath = $destFileInfo['dirname'] . DIRECTORY_SEPARATOR . $destFileInfo['basename'];
            $parameters = array();
            $configHandler = new EpConfigHandler(self::$CONFIG_FILES[$configFile]);
            $newYml = $configHandler->execute(array($newFile));
        }
        $this->taskObj->logSection("migrate",EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_YML_COMPARE,
                array('srcFile' => $srcFilePath, 'destFile' => $destFilePath)));
        //$displayObj = new epFormatConfigDiff('php://output');
        $this->displayObj->displayHeader($srcFilePath, $destFilePath);
        $diffDisplayed = $this->displayObj->displayDiff($origYml, $newYml);
        if($diffDisplayed) {
            $this->diff = $diffDisplayed;
            $this->origYml = $origYml;
            $this->mergeDiff($destFilePath, self::$CONFIG_FILES[$configFile]['prefix']);
        }
        else {
            $this->diff = "";
            return 0;
        }
     }

     private function createMergedArray($array, $prefix="") {

        foreach ($array as $k => $v) {
            if (is_array($v)) {

                if ($k == ".arrays") {
                    $key_prefix = $prefix;
                }
                else if($k == ".settings") {
                    $key_prefix = substr($prefix,0,-1);
                }
                else {
                    $key_prefix = $prefix . $k;
                }
                if (array_key_exists($key_prefix, $this->diff)) {
                    foreach ($this->diff[$key_prefix] as $ke => $va) {
                         if ($this->taskObj->askConfirmation(array('For key "'.$ke.'" the existing value is "'.$array[$k][$ke].'". Kindly confirm to replace value with "' . $va .'"',
                    'Are you sure you want to proceed? (y/N)'), null, 'y')) {
                            $array[$k][$ke] = $va;
                        }
                    }
                } else {
                    $array[$k] = $this->createMergedArray($v, $key_prefix . "_");
                }
            } else {
                $key_prefix = $prefix . $k;
                if (array_key_exists($key_prefix, $this->diff)) {
                    if(array_key_exists($key_prefix, $this->origYml)) {
                    if ($this->taskObj->askConfirmation(array('For key "'.$k.'" the existing value is "'.$v.'". Kindly confirm to replace value with "' . $this->origYml[$key_prefix] .'"',
                    'Are you sure you want to proceed? (y/N)'), null, 'y')) {
                            $v = $this->origYml[$key_prefix];
                             $array[$k] = $v;
                        }
                    }
                    else {
                        if ($this->taskObj->askConfirmation(array('Key "'.$k.'" with value "'.$v.'" not found in Original Yaml ',
                    'Would you like to remove it? (y/N)'), null, 'y')) {
                            unset($array[$k]);
                        }

                    }
                }
            }
        }
        return $array;
    }


     private function mergeDiff($newYmlPath, $prefix="") {

         $arrNewYml = sfYaml::load($newYmlPath);

         if(isset($arrNewYml['all']) && is_array($arrNewYml['all'])) {
             $arrNewYml['all'] = $this->createMergedArray($arrNewYml['all'], $prefix);
         }

         if(isset($arrNewYml['default']) && is_array($arrNewYml['default'])) {
             $arrNewYml['default'] = $this->createMergedArray($arrNewYml['default'], $prefix);
         }

         if(isset($arrNewYml[sfConfig::get('sf_environment')]) && is_array($arrNewYml[sfConfig::get('sf_environment')])) {
             $arrNewYml[sfConfig::get('sf_environment')] = $this->createMergedArray($arrNewYml[sfConfig::get('sf_environment')], $prefix);
         }
        //print "<pre>";print_r($newYmlPath);exit;
        //print "<pre>";print_r($arrNewYml);
        $file_contents = sfYaml::dump($arrNewYml,6);
        $ymlName = basename($newYmlPath);
        $pos = strripos($ymlName, ".");
        if($pos !== false) {
            $fileNameArr = str_split($ymlName, $pos);
            //$bkupFileName = $fileNameArr['0'].date('Y-m-d_H:i:s').$fileNameArr['1'];
            $bkupFileName = $ymlName.".".date('Y-m-d_H:i:s');

            $this->taskObj->getFilesystem()->touch($bkupFileName);

            $origContents = file_get_contents($newYmlPath);
            file_put_contents($bkupFileName, $origContents);
            file_put_contents($newYmlPath, $file_contents);
        }
     }

      public function migratesymboliclink($taskObj){
        $taskObj->logSection("migrate", EpDeployerMessages::$MSG_SYMLINK_MIGRATE_INITIATE );
        //get current build web path
        $newWebPath = sfConfig::get('sf_web_dir');
        sfProjectConfiguration::getActive()->loadHelpers(array("epMigration"));
        $arrNewBuildLinks = getSymboliclinks($newWebPath,true);
     //   print_r($arrNewBuildLinks);echo("\n\n");
        //get previous build web path
        $currentWebPath = EpCurrentAppInfo::getWebDir();

        $currentRootPath = EpCurrentAppInfo::getAppPath();
        $newRootPath = sfConfig::get('sf_root_dir');
        $arrCurrentBuildLinks = getSymboliclinks($currentWebPath,true);
      //  print_r($arrCurrentBuildLinks);exit;
        //$finder = sfFinder::type('any');
        foreach($arrCurrentBuildLinks as $link){
            $newBuildPath = str_replace($currentWebPath, $newWebPath, $link);
            if(!isset($arrNewBuildLinks["$newBuildPath"])){

                 $realpath = realpath($link);

                  $newSoftLink = str_replace($currentRootPath,$newRootPath."/",$link);
                
                //replace link with relative path
                //echo $currentRootPath;echo("\n\n\n\n");
                //echo $newRootPath;echo("\n\n\n");
                
                 $newrealpath = str_replace($currentRootPath,$newRootPath."/",$realpath);
                
               //print "ln -s ".$newrealpath." ".$newSoftLink;exit;
                
                 $taskObj->getFilesystem()->execute("ln -s ".$newrealpath." ".$newSoftLink);
                

               // $taskObj->getFilesystem()->symlink(
                //        $realpath,$newrealpath);
                $taskObj->logSection('migrate', EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_SYMLINK_MIGRATE));

            }


        }

      }

   protected function performDatabaseBackup($sqlFile, $dbuser, $dbpassword,$taskDump) {
       $taskDump->logSection("migrate", EpDeployerMessages::$MSG_DATABASE_BACKUP_INITIATE);
       $dbObj = new EpDatabase();
       $performBkup = 1;
       $dbObj->performDatabaseBackup($dbuser, $dbpassword, $taskDump, $performBkup);
    }

    protected function performDatabaseUpdate($sqlFile, $dbuser, $dbpassword, $taskDbUpdate, $dbfailurebreak=false) {
        $taskDbUpdate->logSection("migrate", EpDeployerMessages::$MSG_DATABASE_BACKUP_UPDATE);
        $dbObj = new EpDatabase();
        $dbUpdateFlag =$dbObj->performDatabaseUpdate($sqlFile,$taskDbUpdate,$dbfailurebreak);
    }

    /* public  function performDatabaseBackupUpdate($sqlFile, $dbuser, $dbpassword,$taskDump,$taskDbUpdate,$dbfailurebreak=false) {
            $dbObj = new EpDatabase();
            $dbObj->performDatabaseBackup($dbuser,$dbpassword,$taskDump);
            $dbUpdateFlag =$dbObj->performDatabaseUpdate($sqlFile,$taskDbUpdate,$dbfailurebreak);
     }*/

     public function copyUploadDir($taskObj) {
         $finder = sfFinder::type('any');
         $taskObj->getFilesystem()->mirror(EpCurrentAppInfo::getUploadsDir(), sfConfig::get('sf_upload_dir'),$finder);
         $taskObj->logSection('migrate',EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_UPLOAD_MIGRATE));
     }

     public function copyLogs($taskObj) {
         $finder = sfFinder::type('any');
         $taskObj->getFilesystem()->mirror(EpCurrentAppInfo::getLogDir(), sfConfig::get('sf_log_dir'),$finder);
         $taskObj->logSection('migrate',EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_LOGS_MIGRATE));
     }

}

?>