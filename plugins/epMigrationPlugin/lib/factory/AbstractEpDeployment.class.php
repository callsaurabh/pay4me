<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//abstract class AbstractEpDeployment implements IEpDeployment {
abstract class AbstractEpDeployment  {

    public static $APP = "frontend";
    protected $displayObj;
    public static $CONFIG_FILES = array(
        'databases'=>array('prefix'=>''),
        'app'=>array('prefix'=>'app_'),
        'settings'=>array('prefix'=>'sf_'),        
        'factories'=>array('prefix'=>''),
        'filters'=>array('prefix'=>''));

    public static function getDeploymentProcess($process) {
        $className = "Ep".ucfirst(strtolower($process))."Deployment";
        return $deploy = new $className();
    }

    public function setActiveApplication($application) {
        self::$APP = $application;
    }

    public function unlinkCurrent() {
        return $this->unlinkCurrentApp();
    }

    public function enableMaintenanceMode($taskObj, $maintenanceMode = "",$instanceHandlerObj='') {
       
        if($maintenanceMode == "") {
           $maintenanceMode = $instanceHandlerObj->getMaintenanceMode();
        }
        else {
            if($maintenanceMode == InstanceHandler::MAINTENANCE_CUSTOM) {
                if(!$instanceHandlerObj->checkLock()) {
                    $failureMsg = EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_CUSTOM_MAINTENANCE_CONFIGURATION_INSUFFICIENT);
                    $taskObj->logSection("migrate", $failureMsg);
                    throw New EpMigrationException($failureMsg);
                }
            }
            InstanceHandler::$MAINTENANCE_MODE = $maintenanceMode;
        }
        if ($maintenanceMode == InstanceHandler::MAINTENANCE_CUSTOM) {
            $taskObj->logSection("migrate", "enabling application's custom maintenance mode");
            return $this->enableCustomMaintenanceMode($taskObj);
        } else {         
            $taskObj->logSection("migrate", "enabling plugin's maintenance mode");
            $pathToPlugin = InstanceHandler::getPluginsMaintenancePath();
            return $this->enablePluginsMaintenanceMode($pathToPlugin);
        }
    }


    public function copyConfigFiles($taskObj) {
        $this->displayObj = new epFormatConfigDiff('php://stdout');
       // $msg = "Starting migration of config files";
        foreach (self::$CONFIG_FILES as $configFile=>$attributes) {
           $this->confirmAndMigrate($taskObj, $configFile);
        }
        return $msg;
    }

    
    public function ProcessDatabase($sqlFile, $dbuser, $dbpassword, $taskDump, $taskDbUpdate) {
        $sqlFilePath = sfConfig::get('sf_data_dir') . DIRECTORY_SEPARATOR . 'sql' . DIRECTORY_SEPARATOR . $sqlFile;
        if (file_exists($sqlFilePath)) {
            $taskDump->log(EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_FILE_EXIST, array('fileName' => $sqlFilePath)));
        } else {
            $taskDump->log(EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_FILE_NOT_EXIST, array('fileName' => $sqlFilePath)));
        }
        $this->performDatabaseBackup($sqlFile, $dbuser, $dbpassword, $taskDump);
        $this->performDatabaseUpdate($sqlFile, $dbuser, $dbpassword, $taskDbUpdate);
    }

    /*public function migratesymboliclink(){
        //get current build web path
        $webPath = sfConfig::get('sf_web_dir');
        sfProjectConfiguration::getActive()->loadHelpers(array("epMigration"));
        $arrNewBuildLinks = getSymboliclinks($webPath,true);
        //get previous build web path
        //        //error handling is required here for proper resulation


    }*/

    public function enableCurrentBuild($instanceHandlerObj) {
        $maintenanceMode = $instanceHandlerObj->getMaintenanceMode();
        if($maintenanceMode == InstanceHandler::MAINTENANCE_CUSTOM) {
            $deploymentPath = InstanceHandler::getNewBuildPath();
            return $this->disableCustomMaintenanceMode($deploymentPath);
        }
        else if($maintenanceMode == InstanceHandler::MAINTENANCE_PLUGIN) {
            $deploymentPath = InstanceHandler::getNewBuildPath();
            return $this->disablePluginsMaintenanceMode($deploymentPath);
        }
    }

    

    protected abstract function disablePluginsMaintenanceMode($deploymentPath);

    protected abstract function disableCustomMaintenanceMode($deploymentPath);

    protected abstract function confirmAndMigrate($taskObj, $configFile);

    //protected abstract function unlinkCurrentApp();

    protected abstract function enableCustomMaintenanceMode($taskObj);

    protected abstract function migratesymboliclink($taskObj);

    protected abstract function enablePluginsMaintenanceMode($path);

    protected abstract function performDatabaseBackup($sqlFile, $dbuser, $dbpassword, $taskDump);

    protected abstract function performDatabaseUpdate($sqlFile, $dbuser, $dbpassword, $taskDbUpdate);

    protected abstract function copyUploadDir($taskObj);

    protected abstract function copyLogs($taskObj);
}

?>
