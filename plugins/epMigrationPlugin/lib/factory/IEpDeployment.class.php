<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

interface IEpDeployment {
    
    public function enableMaintenanceMode();

    public function setActiveApplication($application);

    public function unlinkCurrent();

    public function copyConfigFiles($taskObj);

}

?>
