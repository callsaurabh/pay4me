<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpDryrunDeployment extends AbstractEpDeployment {

    //const APP = "frontend";

    protected function unlinkCurrentApp() {
      //  InstanceHandler::setAppInfo();
        $linkPath = EpCurrentAppInfo::getLinkPath();
        return EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_UNLINK, array('currentAbsolutePath'=>$linkPath));
    }

    protected function enableCustomMaintenanceMode($taskObj) {
        return EpDeployerMessages::$MSG_CUSTOM_MAINTENANCE;
    }

    protected function enablePluginsMaintenanceMode($targetPath) {
        $msgUnlink = $this->unlinkCurrentApp();
        return $msgUnlink.", ".EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_PLUGIN_CORE_MAINTENANCE, array('targetPath'=>$targetPath));
    }

    protected function disableCustomMaintenanceMode($deploymentPath){
        $this->unlinkCurrentApp();
        return EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_DISABLE_CUSTOM_MAINTENANCE, array('targetPath'=>$deploymentPath));
    }

    protected function disablePluginsMaintenanceMode($deploymentPath) {
        $this->unlinkCurrentApp();
        return EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_DISABLE_PLUGIN_MAINTENANCE, array('targetPath'=>$deploymentPath));
    }
    
    protected function confirmAndMigrate($taskObj, $configFile) {
        $taskObj->logSection("migrate",EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_CONFIG_MIGRATE, array('fileName'=>$configFile.".yml")));
    }

    protected function performDatabaseBackup($sqlFile, $dbuser, $dbpassword,$taskDump) {
        $taskDump->logSection("migrate", EpDeployerMessages::$MSG_DATABASE_BACKUP_INITIATE);
        $dbObj = new EpDatabase();
        $dbObj->performDatabaseBackup($dbuser,$dbpassword,$taskDump);
    }

    protected function performDatabaseUpdate($sqlFile, $dbuser, $dbpassword,$taskDbUpdate) {
        $taskDbUpdate->logSection("migrate", EpDeployerMessages::$MSG_DATABASE_BACKUP_UPDATE);
    }

    /*protected  function performDatabaseBackupUpdate($sqlFile, $dbuser, $dbpassword,$taskDump,$taskDbUpdate){
        
     }*/

     public function copyUploadDir($taskObj) {
        $taskObj->logSection('migrate', EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_UPLOAD_MIGRATE));
     }

     public function copyLogs($taskObj) {         
         $taskObj->logSection('migrate', EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_LOGS_MIGRATE));
     }

     public function migratesymboliclink($taskObj){
          $taskObj->logSection('migrate', EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_SYMLINK_MIGRATE));
     }
     
     
  
     
     
}

?>
