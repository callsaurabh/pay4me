<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function computeDiff($aArray1, $aArray2) {
    $arrayNewKeys = array();
    $diff = array();
    $diff = arrayRecursiveDiff($aArray1, $aArray2);
    $arrayNewKeys = array_diff_key($aArray2, $aArray1);
    
    if(empty($arrayNewKeys)){
        return $diff;
    } else {
        return $diff+$arrayNewKeys;
    }
}


function getSymboliclinks($directory,$recursive=true){
     $array_items = array();
     $handle = opendir($directory);
        if ($handle) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($directory. "/" . $file)) {
                            if($recursive) {
                              $array_items = array_merge($array_items, getSymboliclinks($directory. "/" . $file, $recursive));
                            }
                            $file = $directory . "/" . $file;
                            if(is_link($file)){
                              $array_items["$file"] = preg_replace("/\/\//si", "/", $file);
                            }
                    } else {
                            $file = $directory . "/" . $file;

                           if(is_link($file)){
                             $array_items["$file"] = preg_replace("/\/\//si", "/", $file);
                           }
                    }
                }
            }
            closedir($handle);
        }
        return $array_items;



}


/**
 * Compare two multi-dimensional arrays
 *
 * @param   array $aArray1
 * @param   array $aArray2
 *
 * @return  array
 */
function arrayRecursiveDiff($aArray1, $aArray2) {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }

function multi_implode($glue, $pieces, $arr)
{
   // print_r($pieces);
    $string='';

    if(is_array($pieces))
    {
        reset($pieces);
        while(list($key,$value)=each($pieces))
        {
            $string.=$glue.$key."=>".$arr[$key];
            multi_implode($glue, $value, $arr);
         
        }
    }
    else
    {
        return $pieces;
    }

    return trim($string, $glue);
}


?>
