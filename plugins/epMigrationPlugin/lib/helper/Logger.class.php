<?php

class Logger {
    private $LogFile;
    private $logParentDir;
  /**
   * SetLogFiles
   */
  public function Logger(){
    //get document root directory     
    
    $rootDir = sfConfig::get('sf_root_dir');   
    $this->logParentDir = $rootDir;
    $this->CreateLogPath($this->logParentDir);
    $this->LogFile = $this->logParentDir."/epmigration".".txt";
  }

  private function writeLogtoFile($log_message_file,$log){
    $fp = fopen($log_message_file,"a");
    $date = date('Y-m-d-H-i-s')."  ";
    fwrite($fp,$date.$log."\n");
    fclose($fp);
  }

  private  function CreateLogPath($parentLogFolder){

    $logPath =$parentLogFolder;
    if(is_dir($logPath)=='')
    {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
      chmod($dir_path, 0777);
    }
    return $logPath;

  }

  public function LogError($log){
    if($this->LogFile ){
      $this->LogErrorFile = $this->logParentDir."/pay4meErrorLog".date('Y-m-d_h:i:s').".txt";
      $this->writeLogtoFile($this->LogErrorFile,$log);
      return true;
    }
    return false;
  }

  public function LogMessage($log){
    if($this->LogFile){
      $this->writeLogtoFile($this->LogFile,$log);
      return true;
    }
    return false;
  }

  public function LogData($log,$file=''){
    if($file!=""){
        $log_message_file =  $this->logParentDir.$file;
    } else {
        $log_message_file = $this->LogFile;
    }
    $this->writeLogtoFile($log_message_file,$log);
    return false;
  }

}
?>