<?php

class EpmysqldumpTask extends sfDoctrineBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            new sfCommandOption('dbuser', null, sfCommandOption::PARAMETER_REQUIRED, 'Database Username', null),
            new sfCommandOption('dbpassword', null, sfCommandOption::PARAMETER_REQUIRED, 'Database Password', null),
            new sfCommandOption('go', null, sfCommandOption::PARAMETER_NONE, 'Do the backup, else check the connection'),
        ));

        $this->namespace = 'epmigration';
        $this->name = 'dump';
        $this->briefDescription = 'Outputs a MySQL dump';
        $this->detailedDescription = <<<EOF
The [mysqldump|INFO] task does things.
Call it with:

  [php symfony mysqldump|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
        $conn = false;
        if (isset($args['connection'])) {
            $conn = $args['connection'];
        }

        if ($options['dbuser'])
            $this->dbuser = $options['dbuser'];
        if ($options['dbpassword'])
            $this->dbassword = $options['dbpassword'];

        $this->log(EpDeployerMessages::$MSG_DATABASE_BACKUP_PROCESS_START);

        //get database and host name
        $arrParam = epDatabaseParam::getDatabaseParams($this->configuration, $conn);
        $this->logSection("migrate",  EpDeployerMessages::$MSG_DATABASE_CREDENTIALS_VERIFY);
        $conn = epDatabaseParam::checkDbConnection($arrParam, $this->dbuser, $this->dbassword);
        if ($conn==1) {
            $this->logSection("migrate",  EpDeployerMessages::$MSG_DATABASE_CREDENTIALS_VERIFIED);
            if (!$options['go']) { //just test if connection to mysql is success                
                return;
            }

            $param = epDatabaseParam::shellDatabaseParams($arrParam, $this->dbuser, $this->dbassword);
            //$this->log('mysqldump ' . $param);
            $this->logSection('migrate', EpDeployerMessages::$MSG_DATABASE_BACKUP_INITIATE);
            $bkup = @exec('mysqldump ' . $param, $output, $result);

            if ($result != 0) {
                $this->logSection("migrate", EpDeployerMessages::$MSG_DATABASE_BACKUP_FAIL);
                throw new sfException("mysqldump failed");
            } else {
                $this->logSection("migrate", EpDeployerMessages::$MSG_DATABASE_BACKUP_COMPLETE);
            }
        }
        else {
            $this->logSection("migrate", $conn);
            throw new sfException("Could not connect to Mysql");
        }
    }

}
