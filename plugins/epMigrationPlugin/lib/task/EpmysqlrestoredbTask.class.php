<?php

class EpmysqlrestoredbTask extends sfDoctrineBaseTask{
  protected function configure()
  {
    //add your own arguments here
//     $this->addArguments(array(
//         new sfCommandArgument('dbfilepath', sfCommandArgument::REQUIRED, 'My argument'),
//     ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name','frontend'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      new sfCommandOption('dbuser',null,sfCommandOption::PARAMETER_REQUIRED,'Database Username',null),
      new sfCommandOption('dbpassword',null,sfCommandOption::PARAMETER_REQUIRED,'Database Password',null),  
      new sfCommandOption('dbfilepath',null,sfCommandOption::PARAMETER_REQUIRED,'File Location',null),  
     
    ));

    $this->namespace        = 'epmigration';
    $this->name             = 'restoredb';
    $this->briefDescription = 'Restore Database through  MySQL dump file';
    $this->detailedDescription = <<<EOF
The [mysqldump|INFO] task does things.
Call it with:

  [php symfony mysqlrestoredb|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
     
  
   if($options['dbfilepath'])
   $this->dbfilepath = $options['dbfilepath'];   
 
   if (file_exists($this->dbfilepath)) {
        $this->log(EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_FILE_EXIST, array('fileName' => $this->dbfilepath)));
        if(!$this->askConfirmation(array('Please confirm that the database will restore with this file- '.$this->dbfilepath, 'Are you sure you want to proceed? (y/N)'), null, 'frontend')) {
                 $this->log("Task Aborted"."\n");
                 return 1;
         }
    } else {
       throw new sfException(EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_FILE_NOT_EXIST, array('fileName' => $this->dbfilepath)));
    }  
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
    $conn = false;
    if (isset($args['connection'])) {
       $conn = $args['connection'];
    }
    //get database and host name
    $arrParam=epDatabaseParam::getDatabaseParams($this->configuration, $conn);
    //copy current db as old db
    // 1

    //2
    //change db name in database.yml

    //just restore db by creating new db from file

    // use  (source ~/db_name.sql) to restire database



    //drop existing database
    //$this->log(EpDeployerMessages::$MSG_DATABASE_DROP);

    //$this->runTask('doctrine:drop-db');
    //$this->log(EpDeployerMessages::$MSG_DATABASE_DROP_FINISH);
    //create database from databases.yml
    //$this->log(EpDeployerMessages::$MSG_DATABASE_CREATE);
    //$this->runTask('doctrine:build-db');
    //$this->log(EpDeployerMessages::$MSG_DATABASE_CREATE_FINISH);
//    try {
//           $conn = Doctrine_Manager::connection($this->configure());
//           $this->log(EpDeployerMessages::$MSG_DATABASE_RESTORE_START);
//            foreach (explode(';', file_get_contents($this->dbfilepath)) as $query) {
//                $query = trim($query);
//                if (0 == strlen($query)) {
//                     continue;
//                }
//                $this->log($queryOutput = preg_replace('/\s+/', ' ', str_replace("\n", ' ', $query)));
//                $conn->execute($query);
//            }
//            $this->log(EpDeployerMessages::$MSG_DATABASE_RESTORE_PASS);
//
//        } catch (Exception $e) {
//            throw new sfException(EpDeployerMessages::$MSG_DATABASE_RESTORE_FAIL);
//        }
}

}
