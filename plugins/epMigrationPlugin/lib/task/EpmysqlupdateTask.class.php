<?php

class EpmysqlupdateTask extends sfDoctrineBaseTask{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name','frontend'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      new sfCommandOption('dbfile',null,sfCommandOption::PARAMETER_REQUIRED,'Do the Database Backup',null),
      new sfCommandOption('dbfailurebreak',null,sfCommandOption::PARAMETER_REQUIRED,'if data update failled break the mysql',null),
    ));

    $this->namespace        = 'epmigration';
    $this->name             = 'updatedb';
    $this->briefDescription = 'update Database from SQL file';
    $this->detailedDescription = <<<EOF
The [mysqldump|INFO] task does things.
Call it with:

  [php symfony mysqlupdatedb|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array()) {
    $this->logger = new Logger();
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
    $this->sqlFile  = $options['dbfile'];
    $sqlFilePath = sfConfig::get('sf_data_dir') .DIRECTORY_SEPARATOR. 'sql'.DIRECTORY_SEPARATOR. $this->sqlFile;   
    //Check file existant
    if (file_exists($sqlFilePath)) {
        $this->logSection("migrate",EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_FILE_EXIST, array('fileName' => $this->sqlFile)));
    } else {
            
       throw new sfException(EpDeployerMessages::getFomattedMessage(EpDeployerMessages::$MSG_FILE_NOT_EXIST, array('fileName' => $this->sqlFile)));
    }    
    
    try {
            
            $this->dataSqlPath = $options['dbfile'];
            $conn = Doctrine_Manager::connection($this->configure());
            # delimit sql file content on separate queries
            $this->dologging("Creating Table tbl_query_track");
            $strQueryTable = "CREATE TABLE IF NOT  EXISTS tbl_query_track (id INT( 11 ) NOT NULL AUTO_INCREMENT
                PRIMARY KEY , query VARCHAR( 500 ) NOT NULL,updated_dt DATETIME NOT NULL)";
            $conn->execute($strQueryTable);
            $dbfailurebreak = $options['dbfailurebreak'] ? true : false;           
            $this->dologging("Reading Sql file to String");
            $strQuery = file_get_contents($sqlFilePath);
            $this->dologging("Started Processing SQL");
            $arrSql = $this->processsql($strQuery);
            $this->dologging("Looping SQL");
            $arrStatus = $this->loopsql($arrSql, $conn,$dbfailurebreak);
            if(count($arrStatusRet['failure'])>0){
                //some of sql are failled
                //do failure handling
                //do logging
                $this->dologging("some of SQL are failed");
                throw new Exception();
                
            } 
            $this->dologging("setting status as ".EpDeployerMessages::$MSG_DATABASE_BACKUP_UPDATE);
            $this->log(EpDeployerMessages::$MSG_DATABASE_BACKUP_UPDATE);
            
        } catch (Exception $e) {           
            $this->dologging("some exception occurred db updation failled \n".$e->getMessage());
            $this->dologging("setting status as ".EpDeployerMessages::$MSG_DATABASE_UPDATE_FAIL);
            $this->log(EpDeployerMessages::$MSG_DATABASE_UPDATE_FAIL);           
            throw new sfException(EpDeployerMessages::$MSG_DATABASE_UPDATE_FAIL);
        }
   }

    private function dologging($message){
        $this->logger->LogMessage($message."\n\n");

    }

    private function processsql($sql){
       $arrSql = array();
       $sql = $this->remove_comments($sql);
       $sql = $this->remove_remarks($sql);
       $arrSql = $this->split_sql_file($sql,';[\n\r]');
       return $arrSql;
    }


    private function loopsql($arrSql,$conn,$dbfailurebreak){
        $arrFailure = array();
        $success = false;
        for($i=0;$i<count($arrSql);$i++){
            $sql = trim($arrSql[$i]);
            $boolCheck = true;
            //do logging of all sql querys for failure mark the reason
            if($sql!=""){
                $date =date('Y-m-d_h:i:s');
                $strSql = addslashes(htmlentities($sql));
                $trackQuery = "INSERT INTO tbl_query_track (id,query,updated_dt) VALUES ('NULL','".$strSql."','$date')";
                
                $this->dologging("insert query -".$trackQuery);
                $conn->execute($trackQuery);
                $this->dologging("insert sql query -".$sql);
                $boolCheck = $conn->execute($sql);
                if(!$boolCheck){
                    $this->dologging(" sql query failed -".$arrSql[$i]);
                    $arrFailure[] = $arrSql[$i];
                    if($dbfailurebreak){
                        break;
                    }
                } else {
                    $this->dologging(" sql query executed successfully -".$arrSql[$i]);
                    $success = true;
                }
            }
        }
        return array('failure'=>$arrFailure,'success'=>$success);
    }



    private function remove_comments(&$output) {
        $this->dologging("Removing all comments");
        $lines = explode("\n", $output);
        $output = "";

        // try to keep mem. use down
        $linecount = count($lines);

        $in_comment = false;
        for($i = 0; $i < $linecount; $i++) {
          if( preg_match("/^\/\*/", preg_quote($lines[$i])) ) {
             $in_comment = true;
          }

          if( !$in_comment ) {
             $output .= $lines[$i] . "\n";
          }

          if( preg_match("/\*\/$/", preg_quote($lines[$i])) ) {
             $in_comment = false;
          }
        }

        unset($lines);
        return $output;
    }

    //
    // remove_remarks will strip the sql comment lines out of an uploaded sql file
    //
    private function remove_remarks($sql){
        $this->dologging("Remove remarks");
        $lines = explode("\n", $sql);

        // try to keep mem. use down
        $sql = "";

        $linecount = count($lines);
        $output = "";

        for ($i = 0; $i < $linecount; $i++)
        {
          if (($i != ($linecount - 1)) || (strlen($lines[$i]) > 0))
          {
             if (isset($lines[$i][0]) && $lines[$i][0] != "#")
             {
                $output .= $lines[$i] . "\n";
             }
             else
             {
                $output .= "\n";
             }
             // Trading a bit of speed for lower mem. use here.
             $lines[$i] = "";
          }
        }

        return $output;

    }

    //
    // split_sql_file will split an uploaded sql file into single sql statements.
    // Note: expects trim() to have already been run on $sql.
    //
    private function split_sql_file($sql, $delimiter) {
        $this->dologging("spliting sql file");
        // Split up our string into "possible" SQL statements.
        //$tokens = explode($delimiter, $sql);
        $tokens = preg_split("/$delimiter/",$sql);
        // try to save mem.
        $sql = "";
        $output = array();

        // we don't actually care about the matches preg gives us.
        $matches = array();

        // this is faster than calling count($oktens) every time thru the loop.
        $token_count = count($tokens);
        for ($i = 0; $i < $token_count; $i++) {
           $output[] = $tokens[$i];
          // Don't wanna add an empty string as the last thing in the array.
        //      if (($i != ($token_count - 1)) || (strlen($tokens[$i] > 0)))
        //      {
        //         // This is the total number of single quotes in the token.
        //         $total_quotes = preg_match_all("/'/", $tokens[$i], $matches);
        //         // Counts single quotes that are preceded by an odd number of backslashes,
        //         // which means they're escaped quotes.
        //         $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$i], $matches);
        //
        //         $unescaped_quotes = $total_quotes - $escaped_quotes;
        //
        //         // If the number of unescaped quotes is even, then the delimiter did NOT occur inside a string literal.
        //         if (($unescaped_quotes % 2) == 0)
        //         {
        //            // It's a complete sql statement.
        //            $output[] = $tokens[$i];
        //            // save memory.
        //            $tokens[$i] = "";
        //         }
        //         else
        //         {
        //            $output[] = $tokens[$i];
                // incomplete sql statement. keep adding tokens until we have a complete one.
                // $temp will hold what we have so far.
        //            $temp = $tokens[$i] . $delimiter;
        //            // save memory..
        //            $tokens[$i] = "";
        //
        //            // Do we have a complete statement yet?
        //            $complete_stmt = false;
        //
        //            for ($j = $i + 1; (!$complete_stmt && ($j < $token_count)); $j++)
        //            {
        //               // This is the total number of single quotes in the token.
        //               $total_quotes = preg_match_all("/'/", $tokens[$j], $matches);
        //               // Counts single quotes that are preceded by an odd number of backslashes,
        //               // which means they're escaped quotes.
        //               $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$j], $matches);
        //
        //               $unescaped_quotes = $total_quotes - $escaped_quotes;
        //
        //               if (($unescaped_quotes % 2) == 1)
        //               {
        //                  // odd number of unescaped quotes. In combination with the previous incomplete
        //                  // statement(s), we now have a complete statement. (2 odds always make an even)
        //                  $output[] = $temp . $tokens[$j];
        //
        //                  // save memory.
        //                  $tokens[$j] = "";
        //                  $temp = "";
        //
        //                  // exit the loop.
        //                  $complete_stmt = true;
        //                  // make sure the outer loop continues at the right point.
        //                  $i = $j;
        //               }
        //               else
        //               {
        //                  // even number of unescaped quotes. We still don't have a complete statement.
        //                  // (1 odd and 1 even always make an odd)
        //                  $temp .= $tokens[$j] . $delimiter;
        //                  // save memory.
        //                  $tokens[$j] = "";
        //               }
        //
        //            } // for..
             //} // else
         // }
        }

       return $output;
   }
}
