<?php

class EpMigrationStartTask extends sfDoctrineBaseTask {

    public $action = false;
    protected
    $outputBuffer = '',
    $errorBuffer = '';

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name','frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'prod'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            new sfCommandOption('go', null, sfCommandOption::PARAMETER_NONE, 'Do the deployment'),
            new sfCommandOption('symlink', null, sfCommandOption::PARAMETER_REQUIRED, 
                    'Name of the soft link your deployment points to',"current"),
            new sfCommandOption('maintenance-mode', null, sfCommandOption::PARAMETER_REQUIRED,
                    'The maintenance mode to be enabled. Can be either custom or plugin',  null),
            new sfCommandOption('db-update', null, sfCommandOption::PARAMETER_NONE,
                    'Do the Database Backup and Update', null),
            new sfCommandOption('dbfile', null, sfCommandOption::PARAMETER_REQUIRED,
                    'Sql file to be executed', null),
            new sfCommandOption('dbuser', null, sfCommandOption::PARAMETER_REQUIRED,
                    'Database Username', null),
            new sfCommandOption('dbpassword', null, sfCommandOption::PARAMETER_REQUIRED,
                    'Database Password', null),
            new sfCommandOption('dbfailurebreak', null, sfCommandOption::PARAMETER_REQUIRED,
                    'if database update failed need to continue or break', null),            
            new sfCommandOption('migratelogs', null, sfCommandOption::PARAMETER_NONE,
                    'Migrate the logs', null),
            new sfCommandOption('migrateuploads', null, sfCommandOption::PARAMETER_NONE,
                    'Migrate the files/folders in web/upload directory', null),
        ));

        $this->namespace = 'epmigration';
        $this->logger = '';
        $this->name = 'start';
        $this->briefDescription = 'Start the Migration';
        $this->detailedDescription = <<<EOF
The task migrates the current production configuration to the current build in following steps:
    1. Enable Maintenence Mode
    2. Migrate the YAML files
    3. Perform database Backup
    4. Update the database
    5. Migrate Upload Folder
    6. Migrate Logs
    7. Migrate Symbolic Links
    8. Disable Maintenance Mode, point to current build

Call it with:[php symfony epmigration:start]
EOF;
    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array()) {
        $this->logger = new Logger();

         if($options['db-update']) {
            if(($options['dbuser'] == "") || ($options['dbpassword'] == "") || ($options['dbfile'] == "")) {
                $this->logSection("migrate", EpDeployerMessages::$MSG_DB_UPDATE_OPTIONS_UNAVAILABLE );
                throw new EpMigrationException(EpDeployerMessages::$MSG_DB_UPDATE_OPTIONS_UNAVAILABLE );
            }
        }
        
        if(!$this->askConfirmation(array('Please confirm that the active application is - '.$options['application'], 'Are you sure you want to proceed? (y/N)'), null, 'frontend')) {
            $this->logOutput("Task Aborted"."\n");
            return 1;
        }

        $this->logSection("migrate", "Confirmation given that active application is - ".$options['application'] );

        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        
        $process = $options['go'] ? 'run' : 'dryRun';        
        
        $this->logSection("migrate","Running with option ---> " . $process);

        
        $deployer = AbstractEpDeployment::getDeploymentProcess($process);        
        $application = $options['application'];
        
        try {
            $this->checkAppExists($application);
            $this->logSection("migrate", 'Application '.$application.' found');
        }
        catch (Exception $e) {
            $this->logSection("migrate", 'Application '.$application.' does not exist');
            $this->logSection("migrate", 'Aborting task execution');
            throw New EpMigrationException('Application '.$application.' does not exist');
        }
        
        $deployer->setActiveApplication($application);
        
        if($options['symlink']) {
            InstanceHandler::setSymbolickLink($options['symlink']);
        }
        $instanceHandlerObj = new InstanceHandler($process);
              
        //Step ------> Enable Maintenance Mode
        $this->logSection('migrate', 'Enabling maintenance mode....');
        $enableMaintenanace = $deployer->enableMaintenanceMode($this, $options['maintenance-mode'],$instanceHandlerObj);        
        $this->logSection('action', $enableMaintenanace);

        
        EpMigrationState::setState(EpMigrationState::$STATE_MAINTENANCE);
            //throw new EpMigrationException("abcd");
        //Step -------> migrate yaml files
        $this->logSection('migrate', 'Starting migration of YAML files..');
        $deployer->copyConfigFiles($this);
        EpMigrationState::setState(EpMigrationState::$STATE_CONFIG_MIGRATED);

        
        //Step -------> migrate upload folder
         if ($options['migrateuploads']) {
            $this->logSection('migrate', 'migrating the upload folder...');
            $deployer->copyUploadDir($this);
            EpMigrationState::setState(EpMigrationState::$STATE_COPY_UPLOAD);
         }
        
        //Step -------> migrate log folder
        if ($options['migratelogs']) {
            $this->logSection('migrate', 'migrating the logs folder..');
            $deployer->copyLogs($this);
        }
       
        //Step -----> migrate symbolic links
        $deployer->migratesymboliclink($this);
        //Step -----> enable new build


        //execute project permissions and cacheclear
        $this->runTask('project:permissions');
        $this->logSection("migrate", "Task ProjectPermissions executed Successfully");


        $this->runTask('cache:clear');
        $this->logSection("migrate", "Task CacheClear executed Successfully");
        
        //Step ------> Update db; 1. Perform db backup, 2. Update db
        if($options['db-update']) {
            $dbfailurebreak = $options['dbfailurebreak'] ? true : false;
            $taskDump =  $this->CreateTask('epmigration:dump');
            $taskDbUpdate =  $this->CreateTask('epmigration:updatedb');
            $deployer->ProcessDatabase($options['dbfile'],$options['dbuser'],$options['dbpassword'],$taskDump,$taskDbUpdate,$dbfailurebreak);
        }
        

        $disableMaintenance = $deployer->enableCurrentBuild($instanceHandlerObj);
        $this->logSection('action', $disableMaintenance);

    }

    private function dologging($message){
        $this->logger->LogMessage($message);
        
    }
    public function disableProject() {
        $this->runTask('project:disable', array(sfConfig::get('sf_environment')));
        $this->runTask('cache:clear');
        $this->runTask('project:permissions');
    }

    public function enableProject() {
        $tasks = array('project:permissions','cache:clear',);
        foreach($tasks as $task) {
          $this->run($task);
        }
    }

    public function logOutput($output) {
        if (false !== $pos = strpos($output, "\n")) {
            $this->outputBuffer .= substr($output, 0, $pos);
            $this->log($this->outputBuffer);
            $this->outputBuffer = substr($output, $pos + 1);
        } else {
            $this->outputBuffer .= $output;
        }
    }

    public function logErrors($output) {
        if (false !== $pos = strpos($output, "\n")) {
            $this->errorBuffer .= substr($output, 0, $pos);
            $this->log($this->formatter->format($this->errorBuffer, 'ERROR'));
            $this->errorBuffer = substr($output, $pos + 1);
        } else {
            $this->errorBuffer .= $output;
        }
    }

    protected function clearBuffers() {
        if ($this->outputBuffer) {
            $this->log($this->outputBuffer);
            $this->outputBuffer = '';
        }

        if ($this->errorBuffer) {
            $this->log($this->formatter->format($this->errorBuffer, 'ERROR'));
            $this->errorBuffer = '';
        }
    }

    public function logSection($section, $message) {
        $this->dologging($message);
        parent::logSection($section, $message);
    }
}
