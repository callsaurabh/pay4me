<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpMigrationRollback implements IEpMigrationRollback {

    public function initiateRollback() {
        $currentState = EpMigrationState::getState();
        if($currentState >=2 ) {
            $maintenanceMode = InstanceHandler::$MAINTENANCE_MODE;
            if($maintenanceMode == InstanceHandler::MAINTENANCE_CUSTOM) {
                $cmd = "php ".EpCurrentAppInfo::getAppPath()."symfony project:enable ".sfConfig::get('sf_environment');
                system($cmd, $result);
                if ($result != 0)
                {
                   throw new Exception("Rollback Failure ".$result);
                }
                //return EpDeployerMessages::$MSG_ROLLBACK;
            }
            else if($maintenanceMode == InstanceHandler::MAINTENANCE_PLUGIN) {
                $linkPath = EpCurrentAppInfo::getLinkPath();
                if(@unlink($linkPath)) {
                    $parentApp = str_replace(InstanceHandler::getDocumentRoot(),'',EpCurrentAppInfo::getWebDir());
                    if (!symlink($parentApp, $linkPath)) {
                        throw New Exception("Rollback Failure");
                    }
                }
            }
        }
        /*$reflector = new ReflectionClass(EpMigrationState);
        $staticProperties = $reflector->getProperties(ReflectionProperty::IS_STATIC);
        for ($i = $currentState; $i > 0; $i--) {
            EpMigrationState::setState($i-1);
            foreach ($staticProperties as $staticProperty) {
                if ($staticProperty->getValue() == $i) {
                    $val = $staticProperty->getName();
                    $rollback = $this->array_pop_first(explode("_", $val));
                    $func = "rollback".implode("", $rollback);
                    $this->$func();
                }
            }
        }*/
    }

    private function array_pop_first(&$array) {
        $array = array_reverse($array);
        array_pop($array);
        return $array = array_reverse($array);
    }

    private function rollbackUnlink() {
        print "\n\n\n\n\initiating rollback unlink";
        $softLink = EpCurrentAppInfo::getLinkPath();
        symlink(EpCurrentAppInfo::getWebDir().DIRECTORY_SEPARATOR, $softLink);
    }

    private function rollbackMaintenance() {
       print "\n\n\n\n\initiating rollback maintenance";
       $maintenanceMode = InstanceHandler::$MAINTENANCE_MODE;
       if($maintenanceMode == InstanceHandler::MAINTENANCE_CUSTOM) {

       }
       else {
           //unlink
       }

    }

    private function rollbackDbBackup() {
        print "\n\n\n\n\initiating rollback db backup";
    }

    private function rollbackDbUpdate() {
        print "\n\n\n\n\initiating rollback db update";
    }

    private function rollbackConfigMigration() {
        print "\n\n\n\n\initiating rollback config migration";
    }

    private function rollbackCopyUpload() {
        print "\n\n\n\n\initiating rollback config migration";
    }

}

?>
