<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class InstanceHandler {


    //appinfo
    /*the name of the symlink*/
    //const SYM_LINK = "current"; //symbolic link
    public static $SYM_LINK = "current";
  //  private static $currentAppInfo = "";

    private static $documentRoot = "";

    /*name of the build which will be made LIVE*/
//    private $_appBuildName;

    /*the name of current deployed build*/
   // private $_currentDeployedAppName;

    public static $MAINTENANCE_MODE = "";
    public static $DEPLOYMENT_MODE = "";

    const MAINTENANCE_CUSTOM = "custom";
    const MAINTENANCE_PLUGIN = "plugin";
    const DEPLOYMENT_DRYRUN = "dryRun";
    const DEPLOYMENT_RUN = "run";

    public function __construct($deploymentProcess) {
        if (self::$documentRoot == "") {
            //private constructor for Singleton
            self::$DEPLOYMENT_MODE=$deploymentProcess;
            $buildName = self::getBuildName();
            $rootDir = sfConfig::get('sf_root_dir');
            $absolutePath = strpos($rootDir, $buildName);
            $documentRoot = substr($rootDir, 0, -(strlen($rootDir)-$absolutePath));
            $this->setDocumentRoot($documentRoot);
            new EpCurrentAppInfo($documentRoot);
        }
    }

    public static function setSymbolickLink($linkName){
        self::$SYM_LINK = $linkName;
    }
    
    private static function setDocumentRoot($path) {
         self::$documentRoot = $path;
    }

    public static function getDocumentRoot() {
        return self::$documentRoot;
    }

  /*  public static function setAppInfo() {
        if (self::$documentRoot == "") {
            new InstanceHandler();
        }
        return self::$documentRoot;
    }*/

    private static function getBuildName() {
        return basename(getcwd());
    }

   /* private static function setCurrentAppInfo($softLinkPath) {
        self::$currentAppInfo = readlink($softLinkPath);
    }

    public static function getCurrentAppInfo() {
        
        return dirname(self::$currentAppInfo);
    }

    public static function getNewAppInfo() {
        
    }

    



   public static function getCurrentAppLink() {
        $buildName = self::getBuildName();
        $rootDir = sfConfig::get('sf_root_dir');
        $absolutePath = strpos($rootDir, $buildName);
        $documentRoot = substr($rootDir, 0, -(strlen($rootDir)-$absolutePath));
        self::setDocumentRoot($documentRoot.self::SYM_LINK);
        //return $documentRoot.self::SYM_LINK;
    }



    public function getCurrentDeployedAppName() {
        return $this->_currentDeployedAppName;
    }

    public function getAppBuildName() {
        return $this->_appBuildName;
    }
    
    private function unlinkCurrent() {
        unlink(self::SYM_LINK);
    }*/

    private function setMaintenanceMode() {

        //set default maintenance mode
        $maintenanceMode = self::MAINTENANCE_PLUGIN;

        if($this->checkLock()) {
            $maintenanceMode = self::MAINTENANCE_CUSTOM;
        }

        /*Check 1. if unavailable.php exists in current's - config, or in app's config*/
       /* $unavailablePagePath1 = sfConfig::get('sf_config_dir').DIRECTORY_SEPARATOR.'unavailable.php';
        $unavailablePagePath2 = sfConfig::get('sf_apps_dir').DIRECTORY_SEPARATOR.$application.'/config/unavailable.php';

         if (file_exists($unavailablePagePath1) || file_exists($unavailablePagePath2)) {
             //check if check_lock is enabled in settings.yml
             $ymlPath = sfConfig::get('sf_apps_dir').DIRECTORY_SEPARATOR.$application.'/config/settings.yml';
             $parser = sfYaml::load($ymlPath);
            // print_r($parser);exit;
             if(array_key_exists("check_lock", $parser['all']['.settings'])) { //need to replace with a better function; like array_walk_recursive
                    if($parser['all']['.settings']['check_lock']) {
                        $maintenanceMode = self::MAINTENANCE_CUSTOM;
                 }
             }
         }*/
        self::$MAINTENANCE_MODE = $maintenanceMode;
    }

    public function checkLock() {
      // we'll find the most specific unavailable page...
      $checkLock = false;
      $files = array(
        EpCurrentAppInfo::getAppConfigDir().'/unavailable.php',
        EpCurrentAppInfo::getConfigDir().'/unavailable.php',
        EpCurrentAppInfo::getWebDir().'/errors/unavailable.php',
      );
      $customMaintenancePageExist = false;
      foreach ($files as $file)
      {       
        if (is_readable($file)) {
          $customMaintenancePageExist = true;
          break;
        }
      }
      if($customMaintenancePageExist) {
          $ymlPath = EpCurrentAppInfo::getAppConfigDir().'/settings.yml';
             $parser = sfYaml::load($ymlPath);
            // print_r($parser);exit;
             if(array_key_exists("check_lock", $parser['all']['.settings'])) { //need to replace with a better function; like array_walk_recursive
                    if($parser['all']['.settings']['check_lock']) {
                        $checkLock = true;
                 }
             }
      }
      return $checkLock;
    }

    public function getMaintenanceMode() {
        if(self::$MAINTENANCE_MODE == "") {
           $this->setMaintenanceMode();
        }
        return self::$MAINTENANCE_MODE;
    }

    public static function getPluginsMaintenancePath() {
        $documentRoot = self::getDocumentRoot();
        return sfConfig::get('sf_web_dir').DIRECTORY_SEPARATOR."epMigrationPlugin".DIRECTORY_SEPARATOR;
        
    }

    public static function getNewBuildPath() {
        return sfConfig::get('sf_web_dir').DIRECTORY_SEPARATOR;
        /*$documentRoot = self::getDocumentRoot();
        return str_replace($documentRoot,'',sfConfig::get('sf_web_dir')).
                DIRECTORY_SEPARATOR;*/
    }



}

?>
