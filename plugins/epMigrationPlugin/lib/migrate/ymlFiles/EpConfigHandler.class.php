<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpConfigHandler extends sfYamlConfigHandler {

    public function execute($configFiles) {

        // get the prefix
        $prefix = strtolower($this->getParameterHolder()->get('prefix', ''));

        // add module prefix if needed
        if ($this->getParameterHolder()->get('module', false)) {
            $wildcardValues = $this->getParameterHolder()->get('wildcardValues');
            // either the module name is in wildcard values, or it needs to be inserted on runtime
            $moduleName = $wildcardValues ? strtolower($wildcardValues[0]) : "'.strtolower(\$moduleName).'";
            $prefix .= $moduleName . "_";
        }

        // parse the yaml
        $config = self::getConfiguration($configFiles);

        $values = array();
        foreach ($config as $category => $keys) {
            $values = array_merge($values, $this->getValues($prefix, $category, $keys));
        }
        return $values;
    }

    static public function getConfiguration(array $configFiles) {
        return self::replaceConstants(self::flattenConfigurationWithEnvironment(self::parseYamls($configFiles)));
    }

    protected function getValues($prefix, $category, $keys) {
        if (!is_array($keys)) {
            list($key, $value) = $this->fixCategoryValue($prefix . strtolower($category), '', $keys);

            return array($key => $value);
        }

        $values = array();

        $category = $this->fixCategoryName($category, $prefix);

        // loop through all key/value pairs
        foreach ($keys as $key => $value) {
            list($key, $value) = $this->fixCategoryValue($category, $key, $value);
            $values[$key] = $value;
        }

        return $values;
    }

    /**
     * Fixes the category name and replaces constants in the value.
     *
     * @param string $category The category name
     * @param string $key      The key name
     * @param string $value    The value
     *
     * @return string Return the new key and value
     */
    protected function fixCategoryValue($category, $key, $value) {
        return array($category . $key, $value);
    }

    /**
     * Fixes the category name.
     *
     * @param string $category The category name
     * @param string $prefix   The prefix
     *
     * @return string The fixed category name
     */
    protected function fixCategoryName($category, $prefix) {
        // categories starting without a period will be prepended to the key
        if ($category[0] != '.') {
            $category = $prefix . $category . '_';
        } else {
            $category = $prefix;
        }

        return $category;
    }

}

?>
