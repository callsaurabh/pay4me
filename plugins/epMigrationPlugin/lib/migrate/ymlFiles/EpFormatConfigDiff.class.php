<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpFormatConfigDiff extends sfAnsiColorFormatter {

    /**
     * Holds the PHP stream to log to.
     * @var null|stream
     */
    private $_stream = null;

    public $_styles = array("HEADING" => array('bold' => 'true', 'fg' => 'blue'),
        "DIFF" => array('bold' => 'true', 'fg' => 'magenta'));

    /**
     * Class Constructor
     *
     * @param  streamOrUrl     Stream or URL to open as a stream
     * @param  mode            Mode, only applicable if a URL is given
     */
    public function __construct($streamOrUrl, $mode = NULL) {

        // Setting the default
        if ($mode === NULL) {
            $mode = 'a';
        }

        foreach ($this->_styles as $key => $value) {
            $this->setStyle($key, $value);
        }
        if (!$this->_stream = @fopen($streamOrUrl, $mode, false)) {
            $msg = "\"$streamOrUrl\" cannot be opened with mode \"$mode\"";
            throw new EpMigrationException($msg);
        }
    }
    
    public function displayHeader($srcFilePath, $destFilePath) {
        $data = str_repeat('-', 160) . PHP_EOL;
        $headerSrc = $this->format($srcFilePath, "HEADING");
        $headerDest = $this->format($destFilePath, "HEADING");
        $data .= sprintf('%-80s|%-80s', $headerSrc, $headerDest);
        $data .= PHP_EOL . str_repeat('-', 160) . PHP_EOL;
        $this->write($data);
   }

   public function displayMessage($msg, $style="INFO") {
       $msg = $this->format($msg, $style);
       $this->write();
   }  

   public function displayDiff($baseArr, $newArr) {
            $diffFound = 0;
            sfProjectConfiguration::getActive()->loadHelpers(array("epMigration"));
            $diffArr  = computeDiff($baseArr, $newArr);
            if(count($diffArr)) {
                $diffFound = $diffArr;
                $data = "";
                    foreach ($diffArr as $diffKey => $diffVal) {
                    $baseFormat = "";
                    $newFormat = "";
                    $styleBase = $styleDiff = "";
                    if(array_key_exists($diffKey, $baseArr)) {
                        $baseVal = $baseArr[$diffKey];                        
                        if(is_array($baseVal)) {
                           $baseVal = "[".multi_implode(", ", $diffArr[$diffKey], $baseArr[$diffKey])."]";
                        }                        
                        $baseFormat = $diffKey . "==>" . $baseVal;                        
                        $styleBase = "INFO";
                    }
                    if(array_key_exists($diffKey, $newArr)) {
                        $changeVal = $newArr[$diffKey];
                        if(is_array($changeVal)) {
                            $changeVal = "[".multi_implode(", ", $diffArr[$diffKey], $newArr[$diffKey])."]";
                        }
                        $newFormat = $diffKey . "==>" . $changeVal;
                        $styleDiff = "INFO";
                        //$newFormat = $diffKey . "====>" . $newArr[$diffKey];
                    }                    
                    $style = "DIFF";
                    if($styleBase == $styleDiff) {$style = "INFO";}
                    $data .= $this->format(sprintf('%-80s|%-80s', $baseFormat, $newFormat). PHP_EOL, $style);
                }
                $this->write($data);
            }
            return $diffFound;        
    }

    private function writeln($message) {
        $this->write($message, true);
    }

    private function getAnswer($message, $type = "Question") {
        return $this->write($message, $type);
    }

    private function write($message, $newline=false) {

        if (false === fwrite($this->_stream, $message)) {
            throw new Exception("Unable to write to stream");
        }
    }

}

?>
