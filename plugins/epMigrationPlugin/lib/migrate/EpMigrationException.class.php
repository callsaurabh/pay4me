<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpMigrationException extends Exception {

  /**
   * Constructor.
   *
   * @param string          $code       The error code
   * @param string          $message    The error message
   */
  public function __construct($message, $code = 0)
  {
      if(InstanceHandler::$DEPLOYMENT_MODE == InstanceHandler::DEPLOYMENT_DRYRUN) { //incase of dryRun just render the Exception
          return parent::__construct($message, $code);
      }
      else {
          switch(EpMigrationState::$state) {
              case EpMigrationState::$STATE_UNLINK:
                  $this->rollback();
                  parent::__construct($message, $code);
                  break;
              case EpMigrationState::$STATE_MAINTENANCE:
                  $this->rollback();
                  parent::__construct($message, $code);
                  break;
              case EpMigrationState::$STATE_DB_BACKUP:
                  $this->rollback();
                  parent::__construct($message, $code);
                  break;
              case EpMigrationState::$STATE_DB_UPDATE:
                  $this->rollback();
                  parent::__construct($message, $code);
                  break;
              case EpMigrationState::$STATE_CONFIG_MIGRATED:
                  $this->rollback();
                  parent::__construct($message, $code);
                  break;
              case EpMigrationState::$STATE_COPY_UPLOAD:
                  $this->rollback();
                  parent::__construct($message, $code);
                  break;
              default:
                  parent::__construct($message, $code);
                  break;
          }
      }
  }

  private function rollback() {//print "test";exit;
      $rollback = new EpMigrationRollback();
      $rollback->initiateRollback();
  }
    
}

?>
