<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpDeployerMessages {

    public static $MSG_UNLINK = "Unlink Current -> '{currentAbsolutePath}'";
    public static $MSG_UNLINK_FAILURE = "Could not unlink -> '{currentAbsolutePath}'";
    public static $MSG_CUSTOM_MAINTENANCE = "Enabled Custom Maintenance Page";
    public static $MSG_PLUGIN_CORE_MAINTENANCE = "Enabled Plugin's Core Maintenance Page, by creating a soft link to '{targetPath}'";
    public static $MSG_DISABLE_CUSTOM_MAINTENANCE = "Disabled Custom Maintenance Page, and created a soft link to '{targetPath}'";
    public static $MSG_DISABLE_PLUGIN_MAINTENANCE = "Disabled Plugin's Core Maintenance Page, and created a soft link to '{targetPath}'";
    public static $MSG_CONFIG_MIGRATE = "Confirm and Migrate '{fileName}'";
    public static $MSG_MIGRATION_DENIED = "Migration denied for '{fileName}'";
    public static $MSG_FILE_EXIST = "'{fileName}' - SQL file found";
    public static $MSG_FILE_NOT_EXIST = "'{fileName}' - SQL file does not exist";
    public static $MSG_DATABASE_CREDENTIALS_VERIFY = "Validating database params --> database username and password";
    public static $MSG_DATABASE_CREDENTIALS_VERIFIED = "Successfully connected to Mysql";
    public static $MSG_DATABASE_BACKUP_PROCESS_START = "Starting Database Backup ...";
    public static $MSG_DATABASE_BACKUP_INITIATE = "Initiate Database backup";
    public static $MSG_DATABASE_BACKUP_UPDATE = "Initiate Database Update";
    public static $MSG_DATABASE_BACKUP_COMPLETE   = "Database Backup Completed";
    public static $MSG_DATABASE_BACKUP_FAIL = "Database Backup Failed";
    public static $MSG_DATABASE_UPDATE_PROCESS_START = "Database update process start";
    public static $MSG_DATABASE_UPDATE_SUCCESS   = "Database update success";
    public static $MSG_DATABASE_UPDATE_FAIL = "Database update Fail";
    public static $MSG_UPLOAD_MIGRATE = "Contents of Upload Folder Copied";
    public static $MSG_LOGS_MIGRATE = "Contents of Log Folder Copied";
    public static $MSG_SYMLINK_MIGRATE_INITIATE = "Initiating migration of Symbolic links";
    public static $MSG_SYMLINK_MIGRATE = "Symbolic links migrated";
    public static $MSG_ROLLBACK = "Symbolic links migrated";
    public static $MSG_DB_UPDATE_OPTIONS_UNAVAILABLE = "Database Username or password or sql file to migrate is not specified";
    public static $MSG_CUSTOM_MAINTENANCE_CONFIGURATION_INSUFFICIENT = "The configuration for switching to custom maintenance mode is insufficient";
    public static $MSG_YML_COMPARE = "Comparing for diff '{srcFile}', '{destFile}'";
    public static $MSG_EXECUTE_TASK = "Execute the task '{taskname}'";
    public static $MSG_TASK_EXECUTED_SUCCESS = "Task '{taskname}' executed successfully";


    
    //message for reload db
    public static $MSG_DATABASE_DROP = "Starting Database Drop";
    public static $MSG_DATABASE_DROP_FINISH = "Database Droped";
    public static $MSG_DATABASE_CREATE = "Starting Database Creat";
    public static $MSG_DATABASE_CREATE_FINISH = "Database Created";
    public static $MSG_DATABASE_RESTORE_START = "Database Restoring Start";
    public static $MSG_DATABASE_RESTORE_FAIL = "Database Restoring Fail";
    public static $MSG_DATABASE_RESTORE_PASS = "Database Restoring Pass";
    

    
      public static function getFomattedMessage($message, $replaceVars) {
        $formattedMessage = $message;
        foreach ($replaceVars as $key => $value) {
            $formattedMessage = str_replace('{' . $key . '}', $value, $formattedMessage);
        }
        return $formattedMessage;
    }
    
   }

?>
