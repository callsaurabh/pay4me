<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpMigrationState {
    public static $STATE_NONE = 0;
    public static $STATE_UNLINK = 1;
    public static $STATE_MAINTENANCE = 2;
    public static $STATE_DB_BACKUP = 3;
    public static $STATE_DB_UPDATE = 4;
    public static $STATE_CONFIG_MIGRATED = 5;
    public static $STATE_COPY_UPLOAD = 6;
    public static $state = 0;
    
    public static function setState($currentState) {
        self::$state = $currentState;
    }

    public static function getState() {
        return self::$state;
    }

}
?>
