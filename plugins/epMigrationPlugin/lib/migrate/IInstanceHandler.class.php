<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
interface IInstanceHandler {
    
    public function getCurrentDeployedAppName();

    public function getAppBuildName();

    public static function getCurrentAppInfo();
}
?>
