<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpCurrentAppInfo {

    private static $appName = "";
    private static $appPath = "";
    private static $linkPath = "";

    public function __construct($documentRoot) {
        $this->setLinkPath($documentRoot);
        $this->setAppName();
        $this->setAppPath();
    }

    public static function getLinkPath() {
        return self::$linkPath;
    }

    private function setLinkPath($documentRoot) {
        self::$linkPath = $documentRoot.InstanceHandler::$SYM_LINK;
    }

    private function setAppName() {
        $appPath = @readlink(self::$linkPath); //may or may not be till web
        $nameArr = explode("/", $appPath);
        self::$appName = $nameArr['0'];
        if(self::$appName == "") {
            throw New EpMigrationException("Could not read link => ".self::$linkPath);
        }
    }

    public static function getAppName() {
        return self::$appName;
    }

    private function setAppPath() {
        self::$appPath = InstanceHandler::getDocumentRoot().$this->getAppName();
    }

    public static function getAppPath() {
        return self::$appPath.DIRECTORY_SEPARATOR;
    }


    public static function getConfigDir() {
        return self::getAppPath()."config";
    }

    public static function getAppConfigDir() {
        $appName = sfConfig::get('sf_app');
        return self::getAppPath()."apps".DIRECTORY_SEPARATOR.$appName.DIRECTORY_SEPARATOR."config";
    }

   public static function getWebDir() {
        return self::getAppPath()."web";
    }

    public static function getUploadsDir() {
        return self::getAppPath()."web".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;
    }

}

?>
