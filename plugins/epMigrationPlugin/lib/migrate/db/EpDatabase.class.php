<?php

class EpDatabase {
    /* function : performDatabaseBackup
     * Param : $dbuser,$dbpassword,$task
     *
     */

    public function performDatabaseBackup($dbuser, $dbpassword, $task, $executeTask="") {
        try {
            $options = array('dbuser' => $dbuser, 'dbpassword' => $dbpassword);
            if($executeTask=="1") {
                $options[] = "--go";
            }
            $task->run(array(), $options);
            EpMigrationState::setState(EpMigrationState::$STATE_DB_BACKUP);
        } catch (sfException $e) {
            throw New EpMigrationException($e->getMessage(), $e->getCode());
        }
    }

    /* function : performDatabaseUpdate
     * Param :  $sqlFile,$taskDbUpdate
     *
     */

    public function performDatabaseUpdate($sqlFile, $taskDbUpdate,$dbfailurebreak=false) {
        try {
            $taskDbUpdate->run(array(), $options = array('dbfile' => $sqlFile,'dbfailurebreak'=>$dbfailurebreak));
            EpMigrationState::setState(EpMigrationState::$STATE_DB_UPDATE);
        } catch (sfException $e) {
            throw New EpMigrationException($e->getMessage(), $e->getCode());
        }
    }

    

}