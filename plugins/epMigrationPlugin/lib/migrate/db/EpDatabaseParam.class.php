<?php
class epDatabaseParam
{
  static public function getDatabaseParams($configuration, $dbname = false)
  {
    $dbManager = new sfDatabaseManager($configuration);

    $names = $dbManager->getNames();
    $db = $dbManager->getDatabase($dbname ? $dbname : $names[0]);
    if (!$db)
    {
      throw new sfException("No database connection called $db is defined");
    }
    $dsn = $db->getParameter('dsn');  //mysql:dbname=mydbtest;host=localhost because it's test config

    if (!preg_match('/^mysql:(.*)\s*$/', $dsn, $matches))
    {
      throw new sfException("I don't understand the DSN $dsn, sorry");
    }
    $pairs = explode(';', $matches[1]);
    $data = array();
    foreach ($pairs as $pair)
    {
      list($key, $val) = explode('=', $pair);
      $data[$key] = $val;
    }

    return $data;
  }


    static public function checkDbConnection($params, $dbuser, $dbPassword) {
        //$conn = mysql_connect($params['host'], $dbuser, $dbPassword) or die(mysql_error("Connection error:"));
        $link = @mysql_connect($params['host'], $dbuser, $dbPassword);
        if($link) {
            mysql_close($link);
            return true;
        }
        else {
            return "Could not connect ".mysql_error();
        }
    }


  static public function shellDatabaseParams($params,$dbuser,$dbPassword)
  {
    //file name containg dump sql file
    $fileName = sfConfig::get('app_backup_file_name')."_".date('Y-m-d_h:i:s').".sql";

    return '-u ' . escapeshellarg($dbuser) .
          ' -p' . escapeshellarg($dbPassword) .
          ' -h ' . escapeshellarg($params['host']) .
          ' ' . escapeshellarg($params['dbname']).
           ' --result-file=' . escapeshellarg($fileName);
  }

}