<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EpSocketLabsManager
 *
 * @author spandey
 */
class EpSocketLabsManager implements ISocketLabProcessor{

    private $socketLabUrl;
    private $serverId;
    private $userName;
    private $password;
    private $startDate;
    private $endDate;
    private $responseType;
    private $count;

    /**
     * constructor to initialize the variables
     *
     * @param serverId      server id of the SocketLabs
     * @param userName      SocketLabs API username
     * @param password      SocketLabs API password
     * @param startDate     formatted as yyyy-mm-ddThh:mm:ss
     * @param endDate       formatted as yyyy-mm-ddThh:mm:ss
     * @param responseType  Specifies format of the response. May be set to JSON, JSONP, XML or CSV
     * @param count         Specifies the number of records to return in the result set
     */
    public function EpSocketLabsManager($serverId, $userName, $password, $startDate, $endDate, $responseType='XML', $count="")
    {

        $this->socketLabUrl = sfConfig::get('app_socketlabs_url');
        $this->serverId     = $serverId;
        $this->userName     = $userName;
        $this->password     = $password;
        $this->startDate    = $startDate;
        $this->endDate      = $endDate;
        $this->responseType = $responseType;
        $this->count        = $count;

    }

    public function getFailedMessages()
    {
        return $response = $this->processRequest('messagesFailed');
    }

    public function getProcessedMessages()
    {
        return $response = $this->processRequest('messagesProcessed');
    }

    public function getQueuedMessages()
    {
        
        return $response = $this->processRequest('messagesQueued');
    }

    /**
     *
     * @param actionName   Describe
     *
     */
    private function processRequest($actionName)
    {
        try
        {
            define("SERVER_ID", $this->serverId);
            define("API_USER", $this->userName);
            define("API_PASSWORD", $this->password);

            $parameterString = '?serverId='.SERVER_ID.'&startDate='.$this->startDate.
                            '&endDate='.$this->endDate.'&type='.$this->responseType.($this->count ? '&count='.$this->count:'');


            $service_url = $this->socketLabUrl.$actionName.$parameterString;
            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_USERPWD, API_USER . ':' . API_PASSWORD);
            $curl_response = curl_exec($curl);
            curl_close($curl);


            if($curl_response)
                return $curl_response;
            else
                throw new Exception("Some Problem Occured ");


        }catch (Exception $e) {
            throw $e;
        }

    }


}
?>
