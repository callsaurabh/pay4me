<?php
//include('/var/www/p4me/apps/frontend/modules/userAdmin/actions/actions.class.php');
class SocketLabsMailTask extends sfBaseTask
{
    protected function configure()
    { 
        $this->addArgument('processingApi', sfCommandArgument::OPTIONAL, #
            'Requesting Api', '');
        $this->addArgument('startDate', sfCommandArgument::OPTIONAL, #
            'From (date and time)', date('Y-m-d\TH:i:s', time() - (24 * 60 * 60)));
        $this->addArgument('endDate', sfCommandArgument::OPTIONAL, #
            'To (date and time)', date('Y-m-d\TH:i:s'));
        $this->addArgument('type', sfCommandArgument::OPTIONAL, #
            'Response Type from socketLabs', '');
        $this->addArgument('count', sfCommandArgument::OPTIONAL, #
            'No Of record from socketLabs','');


        $this->addOptions(array(
                new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name','frontend'),
                new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
                new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
                // add your own options here
            ));

        $this->namespace        = '';
        $this->name             = 'SocketLabsMailTask';
        $this->briefDescription = 'Fetch the sended mail status through socketlabs API';
        $this->detailedDescription = <<<EOF
The [SocketLabsMailTask|INFO] task does things.
Call it with:

  [php symfony SocketLabsMailTask|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {


        $responseTypeArray = array('JSON','JSONP','XML','CSV');

        $serverId     = sfConfig::get('app_socketlabs_serverid');
        $username     = sfConfig::get('app_socketlabs_username');
        $password     = sfConfig::get('app_socketlabs_password');
        $actionUrl    = sfConfig::get('app_socketlabs_action_url');

     
        $startDate    = $arguments['startDate'];//'2011-12-14T';
        $endDate      = $arguments['endDate'];//'2011-12-13T00:00:00';
        
   
        
        if($arguments['type'])
        $responseType = $arguments['type'];
        else
        $responseType = sfConfig::get('app_socketlabs_response_type');

        if($arguments['count']!="")
            $count = $arguments['count'];
        else{
            $count =(string) sfConfig::get('app_socketlabs_response_count');
           
        }
       
        if($arguments['processingApi'])
        $processingApi = $arguments['processingApi'];
        else
        $processingApi = sfConfig::get('app_socketlabs_processingApi');
     
   
        try{
            $this->validateDate($startDate,$endDate);
            if(!in_array(strtoupper($responseType),$responseTypeArray))
            throw new Exception ($responseType.' is invalid responseType, Please enter correct response type');
          
            if(!$count || ! ctype_digit($count))
            throw new Exception ('Please enter valid response count');

            $this->epSocketLabsPloginObj = new EpSocketLabsManager($serverId,$username,$password,
                $startDate,$endDate,$responseType,$count);

            switch($processingApi) {
                case 'messagesFailed':
                    $result = $this->getFailedMessages();
                    break;
                case 'messagesQueued':
                    $result = $this->getQueuedMessages();
                    break;
                case 'messagesProcessed':
                    $result = $this->getProcessedMessages();
                    break;
            }
            $this->log($result);

/*            Post the response on action */
            //            $browser = new sfBrowser();
            //            $url2Fetch = $this->routeToUrl($actionUrl);
            //            $browser->post($url2Fetch,array('result'=>$result));
            //            $outText = $browser->getResponse()->getContent();


        }catch (Exception $e) {

            $errorText = "Exception during Calling Api Of socket labs: ".$e->getMessage();
         
            $this->log($errorText);
        }
    }

    protected function routeToUrl($route) {
        $this->context = sfContext::createInstance($this->configuration);
        $this->context->getConfiguration()->loadHelpers('Url');

        $this->context->getConfiguration()->loadHelpers('Url');
        $url = url_for($route);
        ## The url is ./symfony/symfony/<actual>/<route> - strip off
        ## first two symfony now to get a legitimate url
        $pos=strripos($url,'symfony');
        if ($pos === false) {
            throw new Exception ('Unable to convert route '.
                $route.' into URL, url_for returned: '.$url);
        }
        return substr($url,$pos+7);
    }

    private function getFailedMessages(){
        return $this->epSocketLabsPloginObj->getFailedMessages();
    }
    private function getProcessedMessages(){
        return $this->epSocketLabsPloginObj->getProcessedMessages();
    }
    private function getQueuedMessages(){
        return $this->epSocketLabsPloginObj->getQueuedMessages();
    }

    private function validateDate($startdate,$enddate){
       $datePattern = '/^(\d\d\d\d)-(\d\d?)-(\d\d?)T(\d\d?):(\d\d?):(\d\d?)$/';
       if (!preg_match($datePattern, $startdate) || !preg_match($datePattern, $enddate))
       throw new Exception ('Date format should be yyyy-mm-ddThh:mm:ss');
       
       $startday = strtotime($startdate);
       $endday = strtotime($enddate);
       if($startday>$endday)
       throw new Exception ('Start date '.$startdate.' should be less than End date '.$enddate);

    }
}
