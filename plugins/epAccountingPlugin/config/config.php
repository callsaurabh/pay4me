<?php

$this->dispatcher->connect('epUpdateAccounts', array('EpAccountingManager', 'processPayment'));

$this->dispatcher->connect('epConsolidatePayments', array('EpAccountingManager', 'updatePayments'));