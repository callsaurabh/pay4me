<?php

/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
abstract class PluginEpMasterAccount extends BaseEpMasterAccount {
  public function addLedgerEntry($amount, $type, $description, $isCleared = 1, $payment_transaction_number, $transaction_type, $accountant_id="") {
    $curr_date = DATE('Y-m-d H:i:s');

    if($accountant_id!="") {
      $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
      Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
    }
    $ledgerObj = new EpMasterLedger();
    $ledgerObj->setMasterAccountId($this->id);
    $ledgerObj->setAmount($amount);
    $ledgerObj->setEntryType($type);
    $ledgerObj->setDescription($description);
    $ledgerObj->setIsCleared($isCleared);
    $ledgerObj->setTransactionDate($curr_date);
    $ledgerObj->setTransactionType($transaction_type);
    $ledgerObj->setPaymentTransactionNumber($payment_transaction_number);
    $clr_balance = 0;
    $unclr_amount = 0;
    if ($type == EpAccountingConstants::$ENTRY_TYPE_DEBIT) {
      if($transaction_type == "direct") {
        if ($isCleared) {
          $clr_balance = "-".$amount;
        }
      }
      else {
        if($isCleared) {
          $unclr_amount = $amount;
          $clr_balance = "-".$amount;
        }
        else {
          $unclr_amount = "-".$amount;
        }
      }
    } else {
      // this is credit
      if($transaction_type == "direct") {
        if ($isCleared) {
          $clr_balance = $amount;
        }
        else {
          $unclr_amount = "-".$amount;
        }
      }
      else {
        if ($isCleared) {
          $unclr_amount = "-".$amount;
          $clr_balance = $amount;
        }
        else {
          $unclr_amount = $amount;
        }

      }

    }
    //get Ledger Clear Balance    
    $finalbalance = 0;
    $balanceObj = Doctrine::getTable('EpMasterAccount')->getAccountDetailsById($this->id);
    $balance = $balanceObj->getFirst()->getClearBalance();    
    $finalbalance = $balance+$clr_balance;
    //UPDATE LEDGER FINAL Balance
    $ledgerObj->setBalance($finalbalance);
    sfContext::getInstance()->getLogger()->info('will start saving');
    //$savePoint = "EpPlugin_LedgerEntry";
    try {
      Doctrine::getTable('EpMasterAccount')->updateAccounts($this->id,$clr_balance,$unclr_amount);
      if($accountant_id!="") {
        $ledgerObj->created_by = $accountant_id;
        $ledgerObj->updated_by = $accountant_id;

   
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
        $ledgerObj->save();
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);
      }
      else {
        $ledgerObj->save();
      }
    } catch (Exception $e ) {
      sfContext::getInstance()->getLogger()->err($e->getMessage());
      //      $con->rollback();
      throw $e;
    }
    sfContext::getInstance()->getLogger()->info('returning');
    return $ledgerObj->getId();
  }

  public function creditAmount($amount, $description, $isCleared=1, $payment_transaction_number,$transaction_type, $accountant_id="") {
    sfContext::getInstance()->getLogger()->debug('will credit ---'.$isCleared);
    return $this->addLedgerEntry($amount, EpAccountingConstants::$ENTRY_TYPE_CREDIT, $description, $isCleared, $payment_transaction_number,$transaction_type, $accountant_id);
  }

  public function debitAmount($amount, $description, $isCleared=1, $payment_transaction_number,$transaction_type, $accountant_id="") {
    sfContext::getInstance()->getLogger()->debug('will debit ---'.$isCleared, $payment_transaction_number);
    return $this->addLedgerEntry($amount, EpAccountingConstants::$ENTRY_TYPE_DEBIT, $description, $isCleared, $payment_transaction_number,$transaction_type, $accountant_id);
  }

  public function getUnclearedEntries() {
    return Doctrine::getTable('EpMasterLedger')->getUnclearedEntries($this->id);
  }

  public function getBalance($uncleared=false) {
    if($uncleared) { //returns unclear balance
      $balance = $this->getUnclearAmount();
    }
    else { //returns clear balance
      $balance = $this->getClearBalance();
    }
    return $balance;
  }

}