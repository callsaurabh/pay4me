<?php

/**
 * PluginEpEtranzactResponseTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginEpEtranzactResponseTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginEpEtranzactResponseTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginEpEtranzactResponse');
    }
}