<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of webpay_infclass
 *
 * @author akumar1
 */
class webpay {
  
  function getStatusMsg($transId, $terminalId,$url)
  {
     $ch = curl_init();
     $header[] = "Content-Type: application/x-www-form-urlencoded";
     curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($ch, CURLOPT_POST, 1);
     $data = "TRANSACTION_ID=".$transId."&TERMINAL_ID=".$terminalId;

     curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
     curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

     //execute post

      $result = curl_exec($ch);
      curl_close($ch);

     $dom = new DOMDocument;
     $dom->loadHTML($result);
     $bodies = $dom->getElementsByTagName('body')->item(0)->nodeValue;
     $pay4meLog = new pay4meLog();
     $nameFormate = 'response';
     $pay4meLog->createLogData($bodies, $nameFormate, 'etranzactLog');

     $bodies = htmlentities($bodies);
     // try and split the text by a double line break
     $paragraphs = split("\n",$bodies);

     $resultArray = Array();
     
     foreach($paragraphs as $key=>$val)
     {

         if ($val!="" && (strpos($val,'=')!==false))
         {

            $strArra = explode('=',$val);

            $resultArray[$strArra[0]] = $strArra[1];
         }
     }
     if (count($resultArray))
        return $resultArray;
     else
      return false;

  }

  

  

}
?>
