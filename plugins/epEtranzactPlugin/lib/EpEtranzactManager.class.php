<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpEtranzactManager {
  //put your code here
  public $trnxId ;
  public $amount ;
  public $userId ;
  public $description;
  public $terminalId;
  public $check_sum;
  public $responseUrl;
  public $logoUrl;
  public $postUrl;

  public function setRequest()
  {
    if($this->trnxId == '')
    {
      $this->trnxId = $this->generateTransactionId();
    }

    $obj = new EpEtranzactRequest();
    $obj->setTransactionId($this->trnxId);
    $obj->setTerminalId($this->terminalId);
    $obj->setAmount($this->amount);
    $obj->setDescription($this->description);
    $obj->setCheckSum($this->check_sum);
    $obj->setResponseUrl($this->responseUrl);
    $obj->setLogoUrl($this->logoUrl);
    $obj->setPostUrl ($this->postUrl);
    $obj->setUserId($this->userId);
    $obj->save();

    return $obj ;

  }

  public function updateEchodata($retObj)
  {
      $etranzactRequestId = $retObj->id;
      $retObj->setEchodata('etranzactRequestId='.$etranzactRequestId);
      $retObj->save();

      return $etranzactRequestId;
  }

  protected function generateTransactionId()
  {
    do{
      $trnxId = time().mt_rand(10000,99999);
      $checkTrnxId = $this->CheckDuplicacy($trnxId);
    }while($checkTrnxId > 0);
    return $trnxId;
  }

  protected function CheckDuplicacy($trnxId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('count(*)')
      ->from('EpEtranzactRequest')
      ->where('transaction_id = ?',$trnxId);

      return  $q->count();
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function selectAllFromRequest($trnxId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('*')
      ->from('EpEtranzactRequest')
      ->where('transaction_id = ?',$trnxId);
      return $q->fetchArray();
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function saveResponse($getData)
  {   
//    try{
          $getReqData = $this->selectAllFromRequest($getData['TRANSACTION_ID']);
//          if ($getReqData[0]['check_sum'] == $getData['CHECKSUM'])
//          {
              $obj = new EpEtranzactResponse();
              $obj->setEtranzactId($getReqData[0]['id']);
              $obj->setSwitchKey($getData['SWITCH_KEY']);
              $obj->setCard4($getData['CARD4']);
              $obj->setMerchantCode($getData['MERCHANT_CODE']);
              $obj->setAmount($getData['AMOUNT']);
              $obj->setDescription($getData['DESCRIPTION']);
              $obj->setCheckSum($getData['CHECKSUM']);
              $obj->setFinalCheckSum($getData['FINAL_CHECKSUM']);
              $obj->setCountryCode($getData['COUNTRY_CODE']);
              $obj->setNoRetry($getData['NO_RETRY']);
              $obj->setStatus($getData['SUCCESS']);
              $obj->save();

              return true;
//          }else {
//              throw new Exception('Invalid Payment');
//          }
//      }
//      catch (Exception $e) {
//         return $e->getMessage();
//      }
  }
 
}
?>
