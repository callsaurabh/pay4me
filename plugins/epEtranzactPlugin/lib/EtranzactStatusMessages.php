<?php
class EtranzactStatusMessages {

  public static $msgArr = array(
  "msg-1" => "Transaction timeout or invalid parameters or unsuccessful transaction in the case of Query History",
  "msg0" => "Transaction Successful",
  "msg1" => "Destination Card Not Found",
  "msg2" => "Card Number Not Found",
  "msg3" => "Invalid Card PIN",
  "msg4" => "Card Expiration Incorrect",
  "msg5" => "Insufficient balance",
  "msg6" => "Spending Limit Exceeded",
  "msg7" => "Internal System Error Occurred, please contact the service provider",
  "msg8" => "Financial Institution cannot authorize transaction, Please try later",
  "msg9" => "PIN tries Exceeded",
  "msg10" => "Card has been locked",
  "msg11" => "Invalid Terminal Id",
  "msg12" => "Payment Timeout",
  "msg13" => "Destination card has been locked",
  "msg14" => "Card has expired",
  "msg15" => "PIN change required",
  "msg16" => "Invalid Amount",
  "msg17" => "Card has been disabled",
  "msg18" => "Unable to credit this account immediately, credit will be done later,",
  "msg19" => "Transaction not permitted on terminal",
  "msg20" => "Exceeds withdrawal frequency",
  "msg21" => "Destination Card has expired",
  "msg22" => "Destination Card Disabled",
  "msg23" => "Source Card Disabled",
  "msg24" => "Invalid Bank Account",
  "msg25" => "Insufficient Balance",
  );
   /**
     * This method return the status message according to $code parameter.
     *
     * @param string $code Status code.
     * @return string Status Message
     */
  public function getStatusMessage($code)
  {
    if(array_key_exists('msg'.$code, self::$msgArr)){
       return self::$msgArr['msg'.$code];
    }else{
      return 'No response received. Please contact your bank';
    }
  }
}