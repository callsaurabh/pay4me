<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apundir
 */
interface IEpjobsMailer {

  /** set/overwrite list of email-to addresses */
  public function setTo($toList);

  /** set/overwrite list of email-cc addresses */
  public function setCc($ccList);

  /** set/overwrite list of email-bcc addresses */
  public function setBcc($bccList);

  /** set the from email address */
  public function setFrom($fromAddress);
  
  /** set the subject of the email address */
  public function setSubject($subject);

  /** set the body of the message*/
  public function setBody($body, $mime=null);
  
  /** add Attachment to the message */
  public function addAttachment($attachment, $mime=null);

  /** add Attachment from file to message */
  public function addAttachmentFromFile($attachmentFile, $mime=null);
  
  public function send();
}