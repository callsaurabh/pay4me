<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EpjobsMailerFactory
 *
 * @author apundir
 */
class EpjobsMailerFactory {

  /**
   * Construct appropriate mail for usage in the jobs.
   * The actual instance must confirms to IEpjobsMailer and applications
   * are encouraged (if needed) to develop their own implementation in
   * order to customize defaults. Out of the box, symfony 1.3/4 mailer
   * will be used (which uses swiftmailer internally).
   * 
   * You can always extend default EpjobsSymfonyMailer to add your custome
   * options (like default bcc and/or default from etc).
   * 
   * @return IEpjobsMailer mailer that must be used for sending emails 
   */
  public static function getMailer() {
    $className = sfConfig::get('app_epjob_mail_factory_class', 'EpjobsSymfonyMailer');
    return new $className();
  }

}