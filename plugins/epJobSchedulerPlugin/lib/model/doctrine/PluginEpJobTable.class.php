<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginEpJobTable extends Doctrine_Table
{
 /**
   * Finds out and return all the jobs URL for execution
   * right now.
   *
   * @return Doctrine_Collection collection of JobParameters objects
   */
   public function getJobsUrl($jobQueId) {
    $query = $this->createQuery()
    ->from('EpJob ej')
    ->leftJoin('ej.EpJobQueue ejq')
    ->where('ejq.id = :id', array('id' => $jobQueId));

     $result = $query->fetchOne();

    return $result;
  }
  public function getAttemptsFromJobId($id) {
  	$q = Doctrine_Query::create()
  	->select('b.execution_attempts,b.state,b.last_execution_status')
  	->from('EpJob b')
  	->where('b.id=?', $id);
  	$res = $q->fetchArray();
  	return $res;
  }

  public function getUpdateJob($jobId){

    $q = Doctrine_Query::create()
    ->update('EpJob e')
    ->set(array('e.execution_attempts' => NULL,
        'e.state' => 'active',
        'e.last_execution_status' => 'notexecuted',
    ))
    ->where('id = ?',$jobId) OR DIE(mysql_error());
    return $q->execute();
  }

}