<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EpjobsReportText
 *
 * @author apundir
 */
class EpjobsReportText extends AbstractEpjobsReportFormat {

  private $jobFormat = '%-15s|%-21.21s|%-22.22s|%-9s|%-6s|%-2s';
  private $jobParameterFormat = '%-24s|%-25.25s';

  protected function getCloseReportText($closingText=null) {
    if ($closingText) {
      return PHP_EOL . $closingText . PHP_EOL;
    }
    return '';
  }

  protected function getDetailedHeaderText(EpJobExecution $execution, $index, $total) {
    $jobName = $execution->getEpJob()->getName();
    $startTime = strtotime($execution->getStartTime());
    $endTime = strtotime($execution->getEndTime());
    $spentSec = $endTime - $startTime;
    $spentTime = $this->sec2hms($spentSec);
    $data = str_repeat('_', 80) . PHP_EOL;
    $data .= 'Jobid: ' . $execution->getJobId() . ', Execution ID: '
            . $execution->getId() . ', ' . 'Name: ' . $jobName
            . ', Exit Code: ' . $execution->getExitCode()
            . PHP_EOL . '    Started: '
            . $execution->getStartTime() . ', Finished: '
            . $execution->getEndTime() . ', Time: ' . $spentTime . PHP_EOL;
    return $data;
  }

  protected function getDetailedOutputText(EpJobData $jobData) {
    $type = "STDERR";
    if ($jobData->getOutputType() == 1) {
      $type = "STDOUT";
    }
    $data = str_repeat('_', 20) . PHP_EOL;
    $data .= $type . ' Output:' . PHP_EOL;
    $data .= $jobData->getOutputText() . PHP_EOL;
    $data = preg_replace('/^/m', '  ', $data);
    return $data;
  }

  protected function getFinishSummaryTableText() {
    return str_repeat('-', 80) . PHP_EOL;
  }

  protected function getStartReportText($reportHeader) {
    return PHP_EOL . $reportHeader . PHP_EOL . PHP_EOL;
  }

  protected function getStartSummaryTableText() {
    $data = str_repeat('-', 80) . PHP_EOL;
    $data .= sprintf($this->jobFormat, 'Job-id', 'Name', 'URL', 'CurrState', 'L_E_S', 'EA');
    $data .= PHP_EOL . str_repeat('-', 80) . PHP_EOL;
    return $data;
  }

  protected function getSummaryHeaderText($summaryHeader) {
    if (!empty($summaryHeader)) {
      return $summaryHeader . PHP_EOL;
    }
    return '';
  }

  protected function getSummaryLineText(EpJob $job) {
    $data = sprintf(
            $this->jobFormat, $job->getId(), $job->getName(), $job->getUrl(), # 
            $job->getState(), $job->getLastExecutionStatus(), $job->getExecutionAttempts());
    $data .= PHP_EOL;
    return $data;
  }
  
  protected function getParameterHeaderText($parameterHeader) {
    if (!empty($parameterHeader)) {
      return PHP_EOL . $parameterHeader . PHP_EOL;
    }
    return '';
  }

  protected function getStartParameterTableText() {
    $data = str_repeat('-', 50) . PHP_EOL;
    $data .= sprintf($this->jobParameterFormat, 'Parameter Name', 'Parameter Value');
    $data .= PHP_EOL . str_repeat('-', 50) . PHP_EOL;
    return $data;
  }

  protected function getFinishParameterTableText() {
    return str_repeat('-', 50) . PHP_EOL . PHP_EOL ;
  }

  protected function getParameterLineText(EpJobParameters $parameter) {
    $data = sprintf(
            $this->jobParameterFormat, $parameter->getName(), $parameter->getValue());
    $data .= PHP_EOL;
    return $data;
  }



}