<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbstractEbjobsReportFormat
 *
 * @author apundir
 */
abstract class AbstractEpjobsReportFormat implements IEpjobsReportFormat {

    public static $STATE_NONE = 0;
    public static $STATE_REPORT_START = 1;
    public static $STATE_SUMMARY_HEADER = 2;
    public static $STATE_SUMMARY_TBL_START = 3;
    public static $STATE_SUMMARY_TBL_FINISH = 4;
    public static $STATE_PARAMETER_HEADER = 5;
    public static $STATE_PARAMETER_TBL_START = 6;
    public static $STATE_PARAMETER_TBL_FINISH = 7;
    public static $STATE_DETAIL_HEADER = 8;
    public static $STATE_DETAIL_OUTPUT = 9;
    public static $STATE_REPORT_FINISH = 10;

    /* Initial state */
    private $state = 0;

    /* Name of the report file */
    private $reportFileName = null;

    /* report file open handle for writing */
    private $reportFileHandle = null;

    public function closeReport($closingText=null) {
        if (($this->state == self::$STATE_PARAMETER_TBL_FINISH)
                || ($this->state == self::$STATE_DETAIL_OUTPUT)
                        || (($this->state == self::$STATE_SUMMARY_TBL_FINISH))) {

            // we are in good state to proceed forward.
        } else {
            throw new EpjobsReportInvalidStateException("Report can only close "
                    . "after parameter table finish OR after detailed output!!".$this->state);
        }
        $this->write($this->getCloseReportText($closingText));
        $this->state = self::$STATE_REPORT_FINISH;
        fclose($this->getReportFileHandle());
    }

    public function finishSummaryTable() {
        if ($this->state != self::$STATE_SUMMARY_TBL_START) {
            throw new EpjobsReportInvalidStateException("summary line can be printed only "
                    . "after starting summary table");
        }
        $this->write($this->getFinishSummaryTableText());
        $this->state = self::$STATE_SUMMARY_TBL_FINISH;
    }

    public function printDetailedData(EpJobExecution $execution, $index, $total) {
        if (($this->state == self::$STATE_PARAMETER_TBL_FINISH)
                || ($this->state == self::$STATE_DETAIL_OUTPUT)) {
            // we are in valid state to proceed
        } else {
            throw new EpjobsReportInvalidStateException("You can print detailed header"
                    . " only after summary table OR after detailed output!!");
        }
        $this->write($this->getDetailedHeaderText($execution, $index, $total));
        $this->state = self::$STATE_DETAIL_HEADER;
        $errData = $execution->getStdErrData();
        if ($errData && (!$this->is_empty($errData->getOutputText()))) {
            $this->printDetailedOutput($errData);
        }
        $outData = $execution->getStdOutData();
        if ($outData && (!$this->is_empty($outData->getOutputText()))) {
            $this->printDetailedOutput($outData);
        }
        $this->state = self::$STATE_DETAIL_OUTPUT;
    }

    private function is_empty($var) {
        if (!empty($var)) {
            $var = trim($var);
            return empty($var);
        }
        return true;
    }

    public function printDetailedOutput(EpJobData $data) {
        if (($this->state == self::$STATE_DETAIL_HEADER)
                || ($this->state == self::$STATE_DETAIL_OUTPUT)) {
            // we are in good state to proceed
        } else {
            throw new EpjobsReportInvalidStateException("Detailed output must be followed "
                    . "by detailed header or another detailed output");
        }
        if ($data) {
            $this->write($this->getDetailedOutputText($data));
        }
    }

    public function printSummaryLine(EpJob $job) {
        if ($this->state != self::$STATE_SUMMARY_TBL_START) {
            throw new EpjobsReportInvalidStateException("summary line can be printed only "
                    . "after starting summary table");
        }
        $this->write($this->getSummaryLineText($job));
        // there'll be multiple lines so no need to change the state here
    }

    public function printSummaryHeader($summaryHeader) {
        if ($this->state != self::$STATE_REPORT_START) {
            throw new EpjobsReportInvalidStateException("Summary Header can be printed only "
                    . "after starting the report");
        }
        $this->write($this->getSummaryHeaderText($summaryHeader));
        $this->state = self::$STATE_SUMMARY_HEADER;
    }

    public function startReport($reportHeader) {
        if ($this->state != self::$STATE_NONE) {
            throw new EpjobsReportInvalidStateException("Report can not be started twice");
        }
        $this->write($this->getStartReportText($reportHeader));
        $this->state = self::$STATE_REPORT_START;
    }

    public function startSummaryTable($summaryHeader) {
        $this->printSummaryHeader($summaryHeader);
        if ($this->state != self::$STATE_SUMMARY_HEADER) {
            throw new EpjobsReportInvalidStateException("summary table can not start without "
                    . "starting summary header");
        }
        $this->write($this->getStartSummaryTableText());
        $this->state = self::$STATE_SUMMARY_TBL_START;
    }

    protected abstract function getStartReportText($reportHeader);

    protected abstract function getStartSummaryTableText();

    protected abstract function getSummaryHeaderText($summaryHeader);

    protected abstract function getSummaryLineText(EpJob $job);

    protected abstract function getDetailedOutputText(EpJobData $data);

    protected abstract function getDetailedHeaderText(EpJobExecution $execution, $index, $total);

    protected abstract function getFinishSummaryTableText();

    protected abstract function getCloseReportText($closingText=null);

    protected abstract function getFinishParameterTableText();

    protected abstract function getParameterHeaderText($parameterHeader);

    protected abstract function getStartParameterTableText();

    protected abstract function getParameterLineText(EpJobParameters $parameter);

    public function setReportFileName($filename) {
        if ($this->reportFileHandle) {
            throw new Exception("Changing the report filename is not allowed");
        }
        $this->reportFileName = $filename;
    }

    protected function getReportFileName() {
        return $this->reportFileName;
    }

    protected function getReportFileHandle() {
        if (!$this->reportFileHandle) {
            $this->reportFileHandle = fopen($this->reportFileName, 'a');
        }
        return $this->reportFileHandle;
    }

    protected function write($data) {
        fwrite($this->getReportFileHandle(), $data);
    }

    /**
     * Factory method for creating appropriate instance of the formatter object
     * for usage in the reporting.
     * 
     * @param type $format - format in which report is required
     * @param type $filename - filename which should have the report text
     * @return IEpjobsReportFormat formatter object responsible for formatting the
     *          text in desired format.
     */
    public static function getReportFormatter($format, $filename) {
        if ($format == EpjobsReport::$FORMAT_TXT) {
            $reporter = new EpjobsReportText();
        } elseif ($format == EpjobsReport::$FORMAT_HTML) {
            $reporter = new EpjobsReportHtml();
        } else {
            $reporter = new EpjobsReportCsv();
        }
        $reporter->setReportFileName($filename);
        return $reporter;
    }

    protected function sec2hms($sec, $padHours = false) {

        // start with a blank string
        $hms = "";

        // do the hours first: there are 3600 seconds in an hour, so if we divide
        // the total number of seconds by 3600 and throw away the remainder, we're
        // left with the number of hours in those seconds
        $hours = intval(intval($sec) / 3600);

        // add hours to $hms (with a leading 0 if asked for)
        $hms .= ( $padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" : $hours . ":";

        // dividing the total seconds by 60 will give us the number of minutes
        // in total, but we're interested in *minutes past the hour* and to get
        // this, we have to divide by 60 again and then use the remainder
        $minutes = intval(($sec / 60) % 60);

        // add minutes to $hms (with a leading 0 if needed)
        $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT) . ":";

        // seconds past the minute are found by dividing the total number of seconds
        // by 60 and using the remainder
        $seconds = intval($sec % 60);

        // add seconds to $hms (with a leading 0 if needed)
        $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

        // done!
        return $hms;
    }

    public function startParameterTable($parameterHeader) {
        $this->printParameterHeader($parameterHeader);
         if ($this->state != self::$STATE_PARAMETER_HEADER) {
          throw new EpjobsReportInvalidStateException("Parameter table can not start without "
          . "starting parameter header");
          } 
        $this->write($this->getStartParameterTableText());
        $this->state = self::$STATE_PARAMETER_TBL_START;
    }

    public function printParameterHeader($parameterHeader) {
         if (($this->state != self::$STATE_SUMMARY_TBL_FINISH) && ($this->state != self::$STATE_DETAIL_OUTPUT) && ($this->state != self::$STATE_PARAMETER_TBL_FINISH)) {
          throw new EpjobsReportInvalidStateException("Parameter Header can be printed only "
          . "after summary table has finished");
          } 
        $this->write($this->getParameterHeaderText($parameterHeader));
        $this->state = self::$STATE_PARAMETER_HEADER;
    }

    public function printParameters(EpJob $job) {
         if ($this->state != self::$STATE_PARAMETER_TBL_START) {
          throw new EpjobsReportInvalidStateException("parameters can be printed only "
          . "after starting parameter table");
          } 

        $parameters = $job->getEpJobParameters();
        foreach ($parameters as $parameter) {
            if(($parameter->getName()!="ep_int_hostname") && ($parameter->getName()!="ep_int_schema"))
             $this->write($this->getParameterLineText($parameter));
        }
    }

    public function finishParameterTable() {
        if ($this->state != self::$STATE_PARAMETER_TBL_START) {
          throw new EpjobsReportInvalidStateException("summary line can be printed only "
          . "after starting summary table");
          } 
        $this->write($this->getFinishParameterTableText());
        $this->state = self::$STATE_PARAMETER_TBL_FINISH;
    }

}