<?php
ini_set('precision',20);
require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    // for compatibility / remove and enable only the plugins you want
    //$this->enableAllPluginsExcept(array('sfDoctrinePlugin'));

    set_include_path(sfConfig::get('sf_lib_dir') .'/vendor' . PATH_SEPARATOR . get_include_path());
    $this->enableAllPluginsExcept(array('sfPropelPlugin'));

    $this->getEventDispatcher()->connect('request.method_not_found', array('sfRequestExtension', 'listenToMethodNotFound'));

      $this->enablePlugins(array('sfFormExtraPlugin'));
    //  $this->disablePlugins(array('sfPropelPlugin'));


  }

  public function configureDoctrine(Doctrine_Manager $manager)
  {
//   Using Native Enum - Use it ONLY IF you are sure you need it
//    $manager->setAttribute('use_native_enum', true);
    $manager->setAttribute(Doctrine::ATTR_USE_NATIVE_ENUM, true);
    $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);

  }
}

class sfRequestExtension
{

  static public function listenToMethodNotFound(sfEvent $event)
  {
    /**
* Method getRawBody
* retrieve raw post data
*/
    if ($event['method']=='getRawBody')
    {
      $event->setProcessed(true);
      $event->setReturnValue(file_get_contents('php://input'));
    }
  }

}
