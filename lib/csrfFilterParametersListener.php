<?php
class csrfFilterParametersListener
{
  public function connect(sfEventDispatcher $dispatcher)
  {
    $dispatcher->connect('template.filter_parameters',
      array($this, 'filterParameters'));
  }

  public function filterParameters(sfEvent $event, $parameters)
  {
    foreach ($parameters as $name => $param)
    {
      if ($param instanceof sfForm)
      {
        $form = $param; /* @var $form sfForm */

        self::changeCSRFErrorMessage($form);
      }
    }

    return $parameters;
  }

  public static function changeCSRFErrorMessage(sfForm $form)
  {
    $errors = $form->getErrorSchema()->getNamedErrors();
    if ($errors)
    {
      foreach ($errors as $i => $error)
      { /* @var $error sfValidatorError */

        if ($i == '_csrf_token')
        {
          $validator = $error->getValidator();
          /* @var $validator sfValidatorCSRFToken */
          //$validator->setMessage('session_expired', 'This session has expired. Please return to the home page and try again.');
          $validator->setMessage('csrf_attack', 'This session has expired. Please return to the home page and try again.');
        }
      }
    }
  }
}

?>
