<?php

require_once(sfConfig::get('sf_lib_dir').'/vendor/symfony/lib/vendor/swiftmailer/swift_init.php') ;//Create the Transport
//require_once(sfConfig::get('sf_lib_dir').'/vendor/swiftmailer/swift_required.php') ;
//extends Swift_Mailer
class Mailing extends Swift_Mailer  {


    public function __construct()
    {
        $server = sfConfig::get('app_email_production_settings_server');
        $port = sfConfig::get('app_email_production_settings_port');
        $connection = Swift_SmtpTransport::newInstance($server, $port);

        parent::__construct($connection) ;
        //        $this->_transport = $transport;
    }

/**
* Send a mail to user/s and/or all users of a group/s using secure mail.
*
*@author  Anurag
* @param  string                     $partialName           A Partial Name which will send an HTML email
* @param  array                      $partialVars           An Associative array with the name of variables which will be used in partial
* @param  array                      $mailingOptions        An Associative array with keys as : ['mailFrom']:string, ['mailTo']:array, ['mailToGroup']:array, ['subject']:string
*
* @return Boolean
* @example
* <pre>
* $partialVars = array('name' => 'Jhone Doe', 'age' => 27, 'marital_status' => 'Single') ;
* $mailTo = array('jhonDoe@abc.com', 'JaneDoe@xyz.com') ;
* $mailToGroup = array('admin', 'operation', 'business') ; // sfGuardUser Group
* $mailSubject = "Send the confirmation" ;
* $mailingOptions = array('mailFrom' => 'jerryWilliams@bce.com','mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
* $sendMailObj = new Mailing() ;
* $sendMailObj->sendMail('xyzModule/abcPartial', $partialVars, $mailingOptions )     *
* </pre>
* @example /examples/example.php
*/
    public function sendMail($partialName, $partialVars, $mailingOptions, $mailServerSettings=null) //$partialName, $options = array())
    {
        
        $partialVars["html"] = true ;
        $currentAction = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();

        try {
            $subject = $mailingOptions['mailSubject'] ;
            // Create the mailer and message objects
            if(isset($mailServerSettings['connectionType'])) {
                $connectionType = $mailServerSettings['connectionType'] ;
            }
            else {
                $connectionType = 'smtp';
            }
            if($connectionType == 'smtp')  // If secured connection to be used
            {
                if($mailServerSettings) {
                    $server = $mailServerSettings['server'] ;
                    $port = $mailServerSettings['port'] ;
                    $encryption = $mailServerSettings['encryption'] ;
                    $username = $mailServerSettings['username'] ;
                    $password = $mailServerSettings['password'] ;
                    $connection = Swift_SmtpTransport::newInstance($server, $port, $encryption)->setUsername($username)->setPassword($password);
                } else {
                    if(sfConfig::get('app_host') == "local"){
                        $server = sfConfig::get('app_email_local_settings_server');
                        $port = sfConfig::get('app_email_local_settings_port');
                        $encryption = sfConfig::get('app_email_local_settings_encryption');
                        $username = sfConfig::get('app_email_local_settings_username');
                        $password = sfConfig::get('app_email_local_settings_password');
                        if((sfConfig::has('app_email_local_settings_bcc'))) {
                            $bcc_email_addresses = sfConfig::get('app_email_local_settings_bcc');
                        }
                        $connection = Swift_SmtpTransport::newInstance($server, $port, $encryption)->setUsername($username)->setPassword($password);
                    }
                    else if(sfConfig::get('app_host') == "production"){
                        $server = sfConfig::get('app_email_production_settings_server');
                        $port = sfConfig::get('app_email_production_settings_port');
                        $username = sfConfig::get('app_email_production_settings_username');
                        $password = sfConfig::get('app_email_production_settings_password');
                        if((sfConfig::has('app_email_production_settings_bcc'))) {
                            $bcc_email_addresses = sfConfig::get('app_email_production_settings_bcc');
                        }
                        $connection = Swift_SmtpTransport::newInstance($server, $port)->setUsername($username)->setPassword($password);
                    }
                }


                $mailer = new Swift_Mailer($connection);

            }
            elseif($connectionType == 'native') {
                $mailer = new Swift(new Swift_Connection_NativeMail());
            } else {
                throw Exception("unsupported connection type: $connectionType");
            }

            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_from');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

           // $mailFrom = 'noreply@notification.pay4me.com' ;
            //  print_r($mailFrom);
            if(array_key_exists('mailTo', $mailingOptions)) {
               // $mailTo = $mailingOptions['mailTo'] ;
                //    print_r($mailTo);

                //          foreach($mailTo as $to)
                //          {
                $message = new Swift_Message($subject) ; //, $mailBody, 'text/html');

                $message->setFrom($mailFrom);
                //            $message->setTo(array('anuragpathak21@gmail.com' => 'Anurag')) ;
                $message->setTo($mailingOptions['mailTo']) ;


                if(isset($bcc_email_addresses) && ($bcc_email_addresses != "")) {
                    $email_addresses = explode(",",$bcc_email_addresses);
                    $message->setBcc($email_addresses) ;
                }

                if(array_key_exists('mailCc', $mailingOptions)) {
                    $mailCc = $mailingOptions['mailCc'] ;
                    $message->setCc($mailCc) ;
                }

                //$partialVars['name'] = $mailTo ;
                //   print $partialName;
                $mailBody = $currentAction->getPartial($partialName, $partialVars);

                // print $mailBody;
                $message->setBody($mailBody, 'text/html') ;
                if(array_key_exists('attachements', $mailingOptions)) {

                    //         print "<pre>";
                    //         print_r($mailingOptions['attachements']);
                    $attch_array = $mailingOptions['attachements'];
                    //           print "<pre>";
                    //         print_r($attch_array);exit;
                    foreach ($attch_array as $k=>$attachement) {
                        // print "in";

                        //         print "<pre>";
                        //         print_r($attachement);exit;

                        $attFile = $attachement['filepath'];//'http://localhost/payforme_ewallet/web/uploads/Pay4Me_20100211_1.csv';
                        $attMime = $attachement['mime'];// 'text/plain';
                        $attObj = Swift_Attachment::fromPath($attFile,$attMime);
                        $message->attach($attObj);
                    }
                }
                // print "sfsd";
                $mailer->send($message) ; //, $to, $mailFrom);

                //          }
            }




            if(array_key_exists('mailToGroup', $mailingOptions)) {
                $mailToGroup = $mailingOptions['mailToGroup'] ;
                foreach($mailToGroup as $groupName) {
                    $usersList =  $this->groupUser($groupName) ;
                    if(is_array($usersList)) {
                        foreach($usersList as $user) {
                            if(count($user['UserDetail']) > 0) {

                                $userEmail = $user['UserDetail']['0']['email'] ;

                                $userFullName = $user['UserDetail']['0']['name'];
                                if($userEmail) {
                                    $message = new Swift_Message($subject) ; //, $mailBody, 'text/html');
                                    $message->setFrom($mailFrom);
                                    $message->setTo(array($userEmail)) ;
                                    $partialVars['name'] = $userFullName ;
                                    //print_r($partialVars) ;
                                    $mailBody = $currentAction->getPartial($partialName, $partialVars);
                                    $message->setBody($mailBody, 'text/html') ;
                                    if(array_key_exists('attachements', $mailingOptions)) {
                                        $attch_array = $mailingOptions['attachements'];
                                        foreach ($attch_array as $k=>$attachement) {
                                            $attFile = $attch_array['filepath'];
                                            $attMime = $attch_array['mime'];
                                            $attObj = Swift_Attachment::fromPath($attFile,$attMime);
                                            $message->attach($attObj);
                                        }
                                    }
                                    $mailer->send($message) ;
                                    //$mailer->send($message, $userEmail, $mailFrom);
                                }
                            }

                        }

                    }
                }
            }

            //  $mailer->disconnect();
        }
        catch (Exception $e)
        {

            //echo $e;
            //print "<pre>";
            //print_r($e);
            $logger=sfContext::getInstance()->getLogger();

            $logger->info('could not send mail');
            //      $logger->log(print_r($orderRedirectXml, true)) ;
            //  echo "error" ; die ;
            //$mailer->disconnect();
            return false ;
            // handle errors here
        }
        return true ;
    }


    public function groupUser($groupName) {
        //        $user_obj = userServiceFactory::getService();
        //        $userList = $user_obj->getAllUsersOfGroup($groupName) ;

        if($groupName) {
            $userListQuery = Doctrine_Query::create()
            ->select('u.*, ud.*,g.id,ug.id')
            ->from('sfGuardUser u')
            ->leftJoin('u.sfGuardUserGroup ug')
            ->leftJoin('ug.sfGuardGroup g')
            ->leftJoin('u.UserDetail ud')
            ->where('g.name=?',$groupName);

            $userList =  $userListQuery->execute(array(),Doctrine::HYDRATE_ARRAY) ;
            return $userList ;
        }
        return false ;
    }
/**
 * Send Monitor mail
 *
 * @param $subject,$mailBody,$partialName
 * @return text
 * WP034
 */
    public function sendMonitorEmail($mailTo,$subject,$mailBody,$partialName){
      /*Mail firing code starts*/
            $mailToGroup = array() ;
            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
            $partialVars = array('mailBody' => $mailBody, 'signature' => $signature) ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";
            }else {
                return "Mail not sent";
            }
    }

    public function sendUserEmail($userid, $password, $subject, $partialName, $login_url){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];
            $bank_acronym = $userDetails[0]['bank_acronym'];
            $username = $userDetails[0]['username'];

    /*Mail firing code starts*/
            $mailTo = $email;
            $mailToGroup = array() ;
            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
            $partialVars = array('name' => $name,'username'=>$username,'password'=>$password,'url'=> $login_url,'signature' => $signature) ;

            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }

/**
 * Send Connection Monitor Mail

 * WP060
 */
    
    public function sendConnectionMonitorMail($queueDown,$topicDown,$subject,$partialName){
        if($queueDown || $topicDown) {
            $mailTo = sfConfig::get('app_connectionmonitor_cc1');

            $mailToGroup = array() ;
            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
            $partialVars = array('queueDown' => $queueDown,'topicDown' => $topicDown,'signature' => $signature) ;

            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";
            }else {
                return "Mail not sent";
            }
        }
        else{
            return "No Queues or topics down";
        }
    } 
    public function sendConfirmationEmail($userid, $subject, $partialName, $activation_url,$password=''){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];


            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }


    /*Mail firing code starts*/
            //    $signature = sfConfig::get('app_email_signature');
            if($password == ''){
                $partialVars = array('name' => $name,'url'=> $activation_url,'signature' => $signature) ;
            }else{
                $partialVars = array('name' => $name,'url'=> $activation_url,'signature' => $signature, 'password'=>$password) ;
            }
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
    public function sendConfirmationEmailToOpenIdUser($userid, $subject, $partialName, $uName){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($uName);
            $email = $userDetails[0]['UserDetail'][0]['email'];


            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }


    /*Mail firing code starts*/
            //    $signature = sfConfig::get('app_email_signature');
            $partialVars = array('name' => $name) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }

    public function sendConfirmationEmailFeedback($emailData, $subject, $partialName){
        if(count($emailData)) {
            $email = $emailData['email'];
            $emailTo = sfConfig::get('app_feedback_email');
            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
        /*Mail firing code starts*/
            //$partialVars = array('name' => $name,'url'=> $activation_url,'signature' => $signature, 'password'=>$password) ;
            $mailTo = $email;

            $partialVars = $emailData;
            $partialVars['signature']  = $signature;
            $emailData['signature']  = $signature;
            $mailToGroup = array() ;
            $subjectPayment = $emailData['payment_mode_option']?$emailData['payment_mode_option'].' | ':'';
            $subject = $emailData['merchant'].' | '.$subjectPayment.(($emailData['common_issue']=='Others')?$emailData['reason']:$emailData['common_issue']);

            $mailingOptions = array('mailFrom' => array($email => 'Pay4Me'),'mailTo' => $emailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $emailData, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }

    
    public function sendStausEmail($userid, $subject, $partialName,$status,$reason=''){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];


            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }


    /*Mail firing code starts*/
            if('' == $reason)
            $partialVars = array('name' => $name,'signature' => $signature,'status' => $status) ;
            else
            $partialVars = array('name' => $name,'signature' => $signature,'status' => $status,'reason' => $reason) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
    public function sendUpdatedProfileNotificationEmail($userid, $subject, $firstname, $lastname, $address, $send_sms, $mobileno, $workphone, $partialName, $reportUrl,$dob){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $username = $userDetails[0]['UserDetail'][0]['name'];
            $email = $userDetails[0]['UserDetail'][0]['email'];
            $firstname = ucwords($firstname);

            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

            $partialVars = array('username' => $username, 'firstname' => $firstname, 'lastname' => $lastname, 'address' => $address, 'send_sms' => $send_sms, 'mobileno'=>$mobileno, 'workphone'=>$workphone,'url'=> $reportUrl, 'signature' => $signature,'dob'=>$dob) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";
            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
    public function sendUpdatedEmailNotification($userid, $email, $preEmail, $subject, $partialName,$openIdType){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $username = $userDetails[0]['UserDetail'][0]['name'];

            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

            $partialVars = array('username' => $username, 'email' => $email, 'signature' => $signature , 'openIdType' =>$openIdType) ;
            $mailTo = $preEmail;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";
            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }

    public function sendNibssEmail($mailTo,$mailFrom,$subject,$partialName,$nibssChgToSend, $from_bank, $account_name, $account_no, $sortcode, $filepath,$filepath_debit, $mimetype){
        $createObj = new paymentRecordManager();


        $nibssSetupData = $createObj->getNibssSettings();
        $nibssSetupDataInstance = $nibssSetupData[0];



        // Now the account setup
        $mailServerSettings['server'] = $nibssSetupDataInstance['outboundserver_address'];
        $mailServerSettings['port'] = $nibssSetupDataInstance['outboundserver_port'];
        $mailServerSettings['encryption'] = 'ssl';
        $mailServerSettings['username'] = $nibssSetupDataInstance['username'];
        $mailServerSettings['password'] = $nibssSetupDataInstance['password'];
        $mailServerSettings['connectionType'] = 'smtp';


        $partialVars = array(
          'bank_name'      => $from_bank,
          'account_name'   => $account_name,
          'account_no'     => $account_no,
          'bank_sortcode'  => $sortcode,
          'nibssChgToSend' => $nibssChgToSend
        );

        $mailToGroup = array() ;
        $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject,'attachements'=>array(array('filepath'=>$filepath,'mime'=>$mimetype),array('filepath'=>$filepath_debit,'mime'=>$mimetype)),'mailCc'=>array('0'=>sfConfig::get('app_nibss_cc1'),'1'=>sfConfig::get('app_nibss_cc2')) ) ;
        $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions,$mailServerSettings) ;
        if($mailReturned) {
            return "Mail sent Successfully";
        }else {
            return "Mail not sent";
        }
    }

    public function sendeWalletUsersEmail(Array $userids, $subject, $message, $partialName){
     /* Get all recipients id to send mail */
        if(count($userids) > 0){
            foreach($userids as $ewalletUserId){
                $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($ewalletUserId);
                $recipients[$userDetails[0]['UserDetail'][0]['email']] = $userDetails[0]['UserDetail'][0]['name'];
            }
        }
        if(sfConfig::get('app_host') == "production"){
            $mailFrom = sfConfig::get('app_email_production_settings_username');
            $signature = sfConfig::get('app_email_production_settings_signature');
        }
        else if(sfConfig::get('app_host') == "local"){
            $mailFrom = sfConfig::get('app_email_local_settings_username');
            $signature = sfConfig::get('app_email_local_settings_signature');
        }


    /*Mail firing code starts*/

        $partialVars = array('body'=> $message,'signature' => $signature) ;

        $mailTo = $recipients;
        $mailToGroup = array() ;
        $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'), 'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
        //$sendMailObj = new Mailing() ;

        $mailReturned = $this->sendBulkMail($recipients, $partialName, $partialVars, $mailingOptions) ;
        if($mailReturned) {
            return "Mail sent Successfully";

        }else {
            return "Mail not sent";
        }
    }

    public function sendBulkMail(Array $mailTo, $partialName, $partialVars, $mailingOptions, $mailServerSettings=null) //$partialName, $options = array())
    {

        $partialVars["html"] = true ;
        $currentAction = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();

        try {
            $subject = $mailingOptions['mailSubject'] ;
            // Create the mailer and message objects
            if(isset($mailServerSettings['connectionType'])) {
                $connectionType = $mailServerSettings['connectionType'] ;
            }
            else {
                $connectionType = 'smtp';
            }
            if($connectionType == 'smtp')  // If secured connection to be used
            {
                if($mailServerSettings) {
                    $server = $mailServerSettings['server'] ;
                    $port = $mailServerSettings['port'] ;
                    $encryption = $mailServerSettings['encryption'] ;
                    $username = $mailServerSettings['username'] ;
                    $password = $mailServerSettings['password'] ;
                    $connection = Swift_SmtpTransport::newInstance($server, $port, $encryption)->setUsername($username)->setPassword($password);
                } else {
                    if(sfConfig::get('app_host') == "local"){
                        $server = sfConfig::get('app_email_local_settings_server');
                        $port = sfConfig::get('app_email_local_settings_port');
                        $encryption = sfConfig::get('app_email_local_settings_encryption');
                        $username = sfConfig::get('app_email_local_settings_username');
                        $password = sfConfig::get('app_email_local_settings_password');
                        if((sfConfig::has('app_email_local_settings_bcc'))) {
                            $bcc_email_addresses = sfConfig::get('app_email_local_settings_bcc');
                        }
                        $connection = Swift_SmtpTransport::newInstance($server, $port, $encryption)->setUsername($username)->setPassword($password);
                    }
                    else if(sfConfig::get('app_host') == "production"){
                        $server = sfConfig::get('app_email_production_settings_server');
                        $port = sfConfig::get('app_email_production_settings_port');
                        $username = sfConfig::get('app_email_production_settings_username');
                        $password = sfConfig::get('app_email_production_settings_password');
                        if((sfConfig::has('app_email_production_settings_bcc'))) {
                            $bcc_email_addresses = sfConfig::get('app_email_production_settings_bcc');
                        }
                        $connection = Swift_SmtpTransport::newInstance($server, $port)->setUsername($username)->setPassword($password);
                    }
                }
                $mailer = new Swift_Mailer($connection);

            }
            elseif($connectionType == 'native') {
                $mailer = new Swift(new Swift_Connection_NativeMail());
            } else {
                throw Exception("unsupported connection type: $connectionType");
            }
            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_from');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
            //$mailFrom = $mailingOptions['mailFrom'] ;
            $mailBody = $currentAction->getPartial($partialName, $partialVars);

            //Create a message
            $message = Swift_Message::newInstance($subject)
            ->setFrom($mailFrom)
            ->setTo($mailTo)
            ->setBody($mailBody, 'text/html');

            //Send the message
            //echo $numSent = $mailer->batchSend($message);
            $mailer->batchSend($message);
        /*
        if ($mailer->batchSend($message))
        {
          echo "Sent\n";
        }
        else
        {
          echo "Failed\n";
        }*/
        }
        catch (Exception $e)
        {
            $logger=sfContext::getInstance()->getLogger();
            $logger->info('could not send mail');
            $mailer->disconnect();
            return false ;
        }
        return true ;
    }

    public function batchSend(Swift_Mime_Message $message, &$failedRecipients = null,
        Swift_Mailer_RecipientIterator $it = null)
    {
        $failedRecipients = (array) $failedRecipients;

        $sent = 0;
        $to = $message->getTo();
        $cc = $message->getCc();
        $bcc = $message->getBcc();

        if (!empty($cc))
        {
            $message->setCc(array());
        }
        if (!empty($bcc))
        {
            $message->setBcc(array());
        }

        //Use an iterator if set
        if (isset($it))
        {
            while ($it->hasNext())
            {
                $message->setTo($it->nextRecipient());
                $sent += $this->send($message, $failedRecipients);
            }
        }
        else
        {
            $strBody = $message->getBody();
            foreach ($to as $address => $name)
            {
                $message->setTo(array($address => $name));

        /**** Begin Code To Replace Body ****/
                $mailBody = str_replace('[NAME]', ucfirst($name), $strBody);
                $message->setBody($mailBody);
        /**** End Code To Replace ****/

                $sent += $this->send($message, $failedRecipients);
            }
        }

        $message->setTo($to);

        if (!empty($cc))
        {
            $message->setCc($cc);
        }
        if (!empty($bcc))
        {
            $message->setBcc($bcc);
        }

        return $sent;
    }
    public function sendPinEmail($pin, $subject,$partialName,$emailAddress,$name)
    {
        //$userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);

    /*Mail firing code starts*/
        $mailTo = $emailAddress;
        $mailToGroup = array() ;
        if(sfConfig::get('app_host') == "production"){
            $mailFrom = sfConfig::get('app_email_production_settings_username');
            $signature = sfConfig::get('app_email_production_settings_signature');
        }
        else if(sfConfig::get('app_host') == "local"){
            $mailFrom = sfConfig::get('app_email_local_settings_username');
            $signature = sfConfig::get('app_email_local_settings_signature');
        }
        $partialVars = array('pin' => $pin,'signature' => $signature,'name'=>$name) ;

        $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
        //$sendMailObj = new Mailing() ;
        $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
        if($mailReturned) {
            return "Mail sent Successfully";

        }else {
            return "Mail not sent";
        }

    }
    public function sendPinBlockedEmail($subject,$partialName,$emailAddress,$name)
    {
        //$userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);

    /*Mail firing code starts*/
        $mailTo = $emailAddress;
        $mailToGroup = array() ;
        if(sfConfig::get('app_host') == "production"){
            $mailFrom = sfConfig::get('app_email_production_settings_username');
            $signature = sfConfig::get('app_email_production_settings_signature');
        }
        else if(sfConfig::get('app_host') == "local"){
            $mailFrom = sfConfig::get('app_email_local_settings_username');
            $signature = sfConfig::get('app_email_local_settings_signature');
        }
        $partialVars = array('signature' => $signature,'name'=>$name) ;

        $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
        //$sendMailObj = new Mailing() ;
        $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
        if($mailReturned) {
            return "Mail sent Successfully";

        }else {
            return "Mail not sent";
        }

    }
    public function sendPinResetEmail($subject,$partialName,$emailAddress,$name,$pin)
    {
        //$userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);

    /*Mail firing code starts*/
        $mailTo = $emailAddress;
        $mailToGroup = array() ;
        if(sfConfig::get('app_host') == "production"){
            $mailFrom = sfConfig::get('app_email_production_settings_username');
            $signature = sfConfig::get('app_email_production_settings_signature');
        }
        else if(sfConfig::get('app_host') == "local"){
            $mailFrom = sfConfig::get('app_email_local_settings_username');
            $signature = sfConfig::get('app_email_local_settings_signature');
        }
        $partialVars = array('signature' => $signature,'name'=>$name,'pin'=>$pin) ;

        $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
        //$sendMailObj = new Mailing() ;
        $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
        if($mailReturned) {
            return "Mail sent Successfully";

        }else {
            return "Mail not sent";
        }

    }

    public function sendGracePeriodMail($subject,$partialName,$emailAddress)
    {
        //$userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);

    /*Mail firing code starts*/
        $mailTo = $emailAddress;
        $mailToGroup = array() ;
        if(sfConfig::get('app_host') == "production"){
            $mailFrom = sfConfig::get('app_email_production_settings_username');
            $signature = sfConfig::get('app_email_production_settings_signature');
        }
        else if(sfConfig::get('app_host') == "local"){
            $mailFrom = sfConfig::get('app_email_local_settings_username');
            $signature = sfConfig::get('app_email_local_settings_signature');
        }
        $partialVars = array('signature' => $signature) ;

        $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
        //$sendMailObj = new Mailing() ;
        $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
        if($mailReturned) {
            return "Mail sent Successfully";

        }else {
            return "Mail not sent";
        }

    }

    public function sendAssociationEmail($userid, $subject, $partialName,$assocEmail,$openIdType){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];


            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

    /*Mail firing code starts*/
            //    $signature = sfConfig::get('app_email_signature');
            
            $partialVars = array('name' => $name,'signature' => $signature,'assocEmail' => $assocEmail,'openIdType' => $openIdType ) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
/*
 * Confirmation Mail for Openid pay4me
 */

        public function sendConfirmationEmailToOpenIdpay4me($userid, $subject, $partialName,$Vars = array()){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];


            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
            $registration_url =   $Vars['registrationUrl'];
            $merchantServiceUrl = $Vars['merchantServiceUrl'];
            $logo = $Vars['logo'];
            $partialVars = array(
            	'name' => $name,
            	'signature' => $signature,
            	'registration_url' => $registration_url,
            	'merchantServiceUrl'=>$merchantServiceUrl,
            	'logo' => $logo
            );
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";
            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
/*
 * Confirmation Mail for Deactivating account
 */

        public function sendDeactivateAccountEmail($userid, $subject, $partialName){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];

            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }
        
            $partialVars = array('name' => $name,'signature' => $signature) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }


    /*
 * sending security code Mail for Reactivating account
 */

        public function sendReactivateSecurityEmail($userid, $subject, $partialName,$securityCode){
        
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];

            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

            $partialVars = array('name' => $name,'signature' => $signature,'securityCode' => $securityCode) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
    /*
     * code for Sending mail for Recover ewallet Account
     */
    public function sendRecoverEmail($userid, $subject, $partialName,$prevEmail,$openIdType){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];


            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

    /*Mail firing code starts*/
            //    $signature = sfConfig::get('app_email_signature');

            $partialVars = array('name' => $name,'signature' => $signature,'assocEmail' => $assocEmail, 'prevEmail' => $prevEmail,'openIdType' => $openIdType ) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
    /*
 * Confirmation Mail for Activating account
 */

        public function sendActivateAccountEmail($userid, $subject, $partialName){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];

            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

            $partialVars = array('name' => $name,'signature' => $signature) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }
    
    /*
     * Mail for Custom Ewallet report generated
     * 
     * @author Ramandeep
     */
        public function sendEwalletReportCreationEmail($userid, $subject, $partialName){
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
        if(count($userDetails[0]['UserDetail'])) {
            $name = ucwords($userDetails[0]['UserDetail'][0]['name']);
            $email = $userDetails[0]['UserDetail'][0]['email'];

            if(sfConfig::get('app_host') == "production"){
                $mailFrom = sfConfig::get('app_email_production_settings_username');
                $signature = sfConfig::get('app_email_production_settings_signature');
            }
            else if(sfConfig::get('app_host') == "local"){
                $mailFrom = sfConfig::get('app_email_local_settings_username');
                $signature = sfConfig::get('app_email_local_settings_signature');
            }

            $partialVars = array('name' => $name,'signature' => $signature) ;
            $mailTo = $email;
            $mailToGroup = array() ;
            $mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
            //$sendMailObj = new Mailing() ;
            $mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
            if($mailReturned) {
                return "Mail sent Successfully";

            }else {
                return "Mail not sent";
            }

        }
        else{
            return "User details not in db";
        }

    }

	/**
	* Function for sending email containing account login details for sub merchant type user.
	*/
	public function sendAccountDetailEmailSubMerchant($userid, $subject, $partialName,$Vars = array()){
		$userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($userid);
		if(count($userDetails[0]['UserDetail'])) {
			$name = ucwords($userDetails[0]['UserDetail'][0]['name']);
			$email = $userDetails[0]['UserDetail'][0]['email'];
	
			if(sfConfig::get('app_host') == "production"){
				$mailFrom = sfConfig::get('app_email_production_settings_username');
				$signature = sfConfig::get('app_email_production_settings_signature');
			}
			else if(sfConfig::get('app_host') == "local"){
				$mailFrom = sfConfig::get('app_email_local_settings_username');
				$signature = sfConfig::get('app_email_local_settings_signature');
			}
			$username =   $Vars['username'];
			$password = $Vars['password'];
			$url = $Vars['login_url'];
			$partialVars = array(
					'name' => $name,
					'signature' => $signature,
					'username' => $userDetails[0]['username'],
					'password'=>$password,
					'login_url' => $url
			);
			$mailTo = $email;
			$mailToGroup = array() ;
			$mailingOptions = array('mailFrom' => array($mailFrom => 'Pay4Me'),'mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
			//$sendMailObj = new Mailing() ;
			$mailReturned = $this->sendMail($partialName, $partialVars, $mailingOptions) ;
			if($mailReturned) {
				return "Mail sent Successfully";
			}else {
				return "Mail not sent";
			}
		}
		else{
			return "User details not in db";
		}
	}

}
?>
