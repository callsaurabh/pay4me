<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class karafMgmtForm extends sfform {

    public function configure() {


        $arrStatus = array('' => '--Select Status--',
            '0' => 'Inactive',
            '1' => 'Active');
        if($this->getOption('checkForm')==0)
        $this->widgetSchema['status']= new sfWidgetFormChoice(array('choices' => $arrStatus));

        $this->widgetSchema['bank']= new sfWidgetFormChoice(array('choices' => $this->getOption('bankList')));
        
        if ($this->getOption('checkForm') == 0) {
            $this->widgetSchema->setLabels(array(
                'status' => 'Status',
                'bank' => 'Bank',
            ));
        }
         $this->setDefault('bank', $this->getOption('bank'));
         $this->setDefault('status', $this->getOption('status'));

          if($this->getOption('checkForm')){
              $this->validatorSchema['bank'] = new sfValidatorChoice(array('required' => true, 'choices' =>$this->getOption('bankList')));

              $this->widgetSchema->setLabels(array(
            'bank' => 'Bank <sup class="cRed">*</sup>'
        ));
          }

          
    }

}

?>
