<?php

class PaidByInternetBankReportForm extends PaymentModeOptionForm
{
  
  
 

  public function configure()
  {
    
    $optionsVals[''] = 'Please Select Status';
    // Block for showing recharge option to ewallet user and payment/recharge both to portal admin
    $guardObj = sfContext::getInstance()->getUser()->getGuardUser();
    $user_group = $guardObj->getGroupNames();
    $user_group_arr = array();
    foreach($user_group as $group){
       $user_group_arr[$group] = $group;
    }
    if((isset($user_group_arr[sfConfig::get('app_pfm_role_ewallet_user')]))){
        $typeVals = array('recharge'=>'Recharge');
    }else
        $typeVals = array('payment'=>'Payment','recharge'=>'Recharge');
    
    $this->setWidgets(array(
      'pay_type' => new sfWidgetFormSelect(array('choices' => $typeVals )),
      'from'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
      'to'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input'))  ,
      'trans_type' => new sfWidgetFormSelect(array('choices' => array('all'=>'All','failure'=>'Failure','success'=>'Success','pending'=>'Pending'))),
      'transaction_no'=>  new sfWidgetFormInputText(array(),array('maxlength'=>50,'class'=>'txt-input')),
      'account_no'=>  new sfWidgetFormInputText(array(),array('maxlength'=>50,'class'=>'txt-input')),
      
    ));

    $this->setDefault('trans_type', 'all');

    $this->widgetSchema->setNameFormat('paidwithinternetbank[%s]');
    echo $this->getValue("pay_mode");
     $this->setValidators(array(
       'pay_type'   => new sfValidatorChoice(array('choices' => array_keys($typeVals)),array('required'=>'Please select Payment Type')),
      'from' => new sfValidatorString(array('required' => false),array('required'=>'Please select from date') ),
      'to' => new sfValidatorString(array('required' => false) ,array('required'=>'Please select to date')),
      'transaction_no'=>   new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'account_no'=>   new sfValidatorString(array('max_length' => 50, 'required' => false)),
      
    ));

$this->validatorSchema->setOption('allow_extra_fields', true);
   $this->widgetSchema->setLabels(array(
           'trans_type'  => 'Transaction Status',
           'pay_type'    => 'Payment type',
           'transaction_no'    => 'Transaction Number',
           'account_no'  => 'Account No.',
           
        ));


  }
}