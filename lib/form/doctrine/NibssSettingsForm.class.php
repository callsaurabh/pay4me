<?php

/**
 * NibssSettings form.
 *
 * @package    form
 * @subpackage NibssSettings
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class NibssSettingsForm extends BaseNibssSettingsForm
{
  public function configure()
  {
      unset(
            $this['created_at'], $this['updated_at'] ,$this['created_by'], $this['updated_by'], $this['deleted_at'], $this['password']
        );

        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'filename' => new sfWidgetFormInputText(),
            'message_from' => new sfWidgetFormInputText(),
            'message_to' => new sfWidgetFormInputText(),
            'message_cc' => new sfWidgetFormInputText(),
            'payor' => new sfWidgetFormInputText(),
            'message_from_name' => new sfWidgetFormInputText(),
            'popserver' => new sfWidgetFormInputText(),
            'port' => new sfWidgetFormInputText(),
            'username' => new sfWidgetFormInputText(),
            'password' => new  sfWidgetFormInputPassword(),
        ));

        $this->setValidators(array(
            'id' => new sfValidatorDoctrineChoice(array('model' => 'NibssSettings', 'column' => 'id', 'required' => false)),
            'filename' => new sfValidatorString(array('max_length' => 150, 'required' => true)),
            'message_from' => new sfValidatorEmail(array('max_length' => 150, 'required' => true)),
            'message_to' => new sfValidatorEmail(array('max_length' => 150, 'required' => true)),
            'message_cc' => new sfValidatorEmail(array('max_length' => 150, 'required' => true)),
            'payor' => new sfValidatorString(array('max_length' => 150, 'required' => true)),
            'message_from_name' => new sfValidatorString(array('max_length' => 150, 'required' => true)),
            'popserver' => new sfValidatorString(array('max_length' => 150, 'required' => true)),
            'port' => new sfValidatorString(array('max_length' => 150, 'required' => true)),
            'username' => new sfValidatorString(array('max_length' => 150, 'required' => true)),
            'password' => new sfValidatorString(array('max_length' => 150, 'required' => true)),
        ));


        $this->widgetSchema->setLabels(array(
            'filename'    => 'File Name',
            'message_from'    => 'Message From',
            'message_to'    => 'Message To',
            'message_cc'    => 'Message CC',
            'payor'    => 'Payor',
            'message_from_name'    => 'Message From Name',
            'popserver'    => 'Pop Server',
            'port'    => 'Port',
            'username'    => 'User Name',
            'password'    => 'Password',
        ));

        $this->widgetSchema->setNameFormat('nibss_settings[%s]');

  }
}