<?php

/**
 * PaymentMode form.
 *
 * @package    form
 * @subpackage PaymentMode
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PaymentModeForm extends BasePaymentModeForm {
  public function configure() {
    unset(
        $this['created_at'], $this['updated_at'] ,$this['created_by'], $this['updated_by'], $this['deleted_at']
    );

    $this->setWidgets(array(
        'id'   => new sfWidgetFormInputHidden(),
        'name' => new sfWidgetFormInputText(array(),array('maxlength'=>50)),
    ));

    $this->setValidators(array(
        'id'   => new sfValidatorDoctrineChoice(array('model' => 'PaymentMode', 'column' => 'id', 'required' => false)),
        'name' => new sfValidatorString(array('max_length' => 50, 'required' => true, 'trim'=>false)),
    ));


    $this->widgetSchema->setLabels(array(
        'name'    => 'Payment Mode',
    ));

    $this->widgetSchema->setNameFormat('payment_mode[%s]');


       


          $this->validatorSchema->setPostValidator(
                        new sfValidatorAnd(array(
                               new sfValidatorCallback(array('callback' => array($this, 'checkDuplicacy'))),
                                  new sfValidatorDoctrineUnique(array(
                                        'model' => 'PaymentMode',
                                        'column' => 'name',
                                        'primary_key' => 'id',
                                        'required' => true
                                ),array('invalid'=>'Payment Mode already exists')),
                        ))

                );
  }


  public function checkDuplicacy($validator, $values) {
    if ($values['name'] != "" ) {
      $val = Doctrine::getTable('PaymentMode')->getDeletedDetails($values['name']);//exit;
      if($val) {
        $error = new sfValidatorError($validator,'Cannot create Payment Mode, already deleted by administrator');
        throw new sfValidatorErrorSchema($validator,array('name' => $error));


      }
    }
    return $values;
  }




}