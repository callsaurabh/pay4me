<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwalletBillRequestFormclass
 *
 * @author ryadav
 */

class EwaleetUserDetailForm extends BaseEwalletTransactionForm {
    
    public function configure()
    {

      
        $currencyArr = $this->getOption('CurrencyArray',0);
     
       
        $this->setWidgets(array(
            
            'account_no' => new sfWidgetFormInputText(array(),array('maxlength'=>30)),
            'email' => new sfWidgetFormInputText(array(),array('maxlength'=>30)),
            'from'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
            'to'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
            'activation'      => new sfWidgetFormChoice(array('choices' => array('' => '-- select--','activated' => '-- Activated --', 'notactivated' => '-- Not Activated --'))),
            'orderByBalance'  => new sfWidgetFormChoice(array('choices' => array(''=>'None','asc'=>'Ascending','desc'=>'Descending'))),
            'currency_id'  => new sfWidgetFormChoice(array('choices' => $currencyArr)),
           

            ));

        $this->widgetSchema->setNameFormat('userDetail[%s]');

        $this->setValidators(array(
            'account_no'              =>new sfValidatorString(array('required' => false)),
            'email' => new sfValidatorString(  array('required' => false)),
            'from'        => new sfValidatorString(  array('required' => false)),
            'to' => new sfValidatorString(  array('required' => false)),
            'activation' =>new sfValidatorString(  array('required' => false)),
            'orderByBalance' =>new sfValidatorString(  array('required' => false)),
            'currency_id' =>new sfValidatorString(  array('required' => false)),
            ));

           

          if(isset($_REQUEST['userDetail']['from']) && $_REQUEST['userDetail']['from']!=""  && isset($_REQUEST['userDetail']['to']) && $_REQUEST['userDetail']['to']!="" ){

          $this->validatorSchema->setPostValidator(

               new sfValidatorSchemaCompare('from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to',
                    array('throw_global_error' => false),
                    array('invalid' => 'The From date  cannot be greater than To date ')
                ));
          
           }

        $this->widgetSchema->setLabels(array(
            'account_no'    => 'eWallet account No',
            'email' => 'Email',
            'from' => 'From Date',
            'to' => 'To Date',
            'activation' => 'Activation Status',
            'orderByBalance' => 'Sort by Balance',
            'currency_id' => 'Currency',
            ));

        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
        
    }
}
?>
