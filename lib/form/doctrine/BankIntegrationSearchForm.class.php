<?php
/**
 * @author ryadav
 */
class BankIntegrationSearchForm  extends sfForm {
    public function configure()
    {
        $bankList =array();
        $arrStatus =array();
        $arrType = array();
        $arrPostValue =array();
        $this->bankList =$this->getOption('bankList');
        $arrStatus =array('' => '--Select Status--',
             "not_posted"=>'Not Processed',
             "posted_to_queue"=>'Under Processing',
             "success"=>'Success',
             "failure"=>'Failure');
        $arrType = array('Payment'=>'Payment','Recharge From Bank'=>'Recharge');
        $this->setWidgets(array(
                            'transaction_type' => new sfWidgetFormChoice(array('expanded' => true,'choices'=>$arrType)),
                            'status' => new sfWidgetFormChoice(array('choices' =>$arrStatus)),
                            'transaction_no'=>  new sfWidgetFormInputText(array(),array('maxlength'=>50,'class'=>'txt-input')),
                            'merchant'    => new sfWidgetFormDoctrineChoice(
                                    array('model' => 'Merchant', 
                                   'add_empty' => '--Select Merchant--',
                                   'order_by' => array('name', 'asc'))),
                            'bank' => new sfWidgetFormChoice(array('choices' => $this->bankList)),
                            'validation_no'=>  new sfWidgetFormInputText(array(),array('maxlength'=>50,'class'=>'txt-input')),
                            'from' =>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
                            'to' =>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input'))  ,
            ));
          $this->widgetSchema->setLabels(array('transaction_type'=>'Transaction Type:',
                                               'status'=> 'Status:',
                                               'transaction_no'=> 'Transaction No.:',
                                               'validation_no'=> 'Validation No.:',
                                               'merchant'=> 'Merchant:',
                                               'bank'=> 'Bank:',
                                               'from'=>'From Date:',
                                               'to'=>'To Date:'));
         $this->widgetSchema->setNameFormat('search[%s]');
         $this->setValidators(array(
            'transaction_type' => new sfValidatorChoice(array('required' => true, 'choices' =>array_keys($arrType))),
            'status' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($arrStatus))),
            'bank' => new sfValidatorString(array('required' => true)),
            'merchant' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => false)),
            'transaction_no'=>   new sfValidatorString(array('max_length' => 50, 'required' => false)),
            'validation_no'=>   new sfValidatorString(array('max_length' => 50, 'required' => false)),
            'from' => new sfValidatorString(array('required' => false)),
            'to' => new sfValidatorString(array('required' => false)),
               
            ));
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
        new sfValidatorCallback(array('callback' => array($this, 'isValidBank'))),)));
        
       $arrPostValue= sfContext::getInstance()->getRequest()->getParameter('search'); 
       if(!empty($arrPostValue['from']) && !empty($arrPostValue['to'])){
        $this->validatorSchema->setPostValidator(new sfValidatorSchemaCompare('from', 
        sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to', array('throw_global_error' => false),
        array('invalid' => 'From Date cannot be greater than To Date'))); 
        }
      }
      function isValidBank($validator, $values){
           if($values['bank']){
                $bankName = Doctrine::getTable('Bank')->getBankName($values['bank']);
                    if(!$bankName){
                        $error = new sfValidatorError($validator,'Invalid Bank');
                        throw new sfValidatorErrorSchema($validator,array('bank' => $error));
                    }
            }
            return $values;
       }
}
?>
