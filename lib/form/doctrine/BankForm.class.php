<?php

/**
 * Bank form.
 *
 * @package    form
 * @subpackage Bank
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BankForm extends BaseBankForm {
    public function configure() {
        unset(
            $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $this->setWidgets(array(
            'id'         => new sfWidgetFormInputHidden(),
            'bank_name'  => new sfWidgetFormInputText(array(),array('maxlength'=>30)),
            'acronym'    => new sfWidgetFormInputText(array(),array('maxlength'=>20)),
            'bank_key'         => new sfWidgetFormInputHidden(),
            'bank_code'         => new sfWidgetFormInputHidden(),
            'auth_info'         => new sfWidgetFormInputHidden(),
            

        ));

        $this->setValidators(array(
            'id'         => new sfValidatorDoctrineChoice(array('model' => 'Bank', 'column' => 'id', 'required' => false)),
            'bank_name'   => new sfValidatorRegex(array('max_length' => 30, 'required' => true,'trim' => true, 'pattern'=>'/^[a-zA-Z0-9 \. \-]*$/'),array('required'=>"Bank Name Required",'invalid'=>'Please enter a valid Bank Name.')),
            //'bank_name'  => new sfValidatorString(array('max_length' => 30, 'required' => true)),
            'acronym'    => new sfValidatorRegex(array('max_length' => 20, 'required' => true,'trim' => true, 'pattern'=>'/^[a-z_]+$/'),array('required'=>"Acronym Required",'invalid'=>'Only alphabets in small cases and underscore allowed')),
            'bank_key'         => new  sfValidatorString(array('required' => false)),
            'bank_code'         => new  sfValidatorString(array('required' => false)),
            'auth_info'         => new  sfValidatorString(array('required' => false)),
           
        ));


        $this->widgetSchema->setLabels(array(
            'bank_name'    => 'Bank Name',
            'acronym'      => 'Acronym',
          
        ));


//        $bankConfigurationObj = $this->getObject()->BankConfiguration[0];
//        $BankConfiguration = new BankConfigrationDetailForm($bankConfigurationObj);
       // $this->embedForm('bankConfiguration', $BankConfiguration);

         $this->validatorSchema->setPostValidator(
                        new sfValidatorAnd(array(
                             // new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'bankAllReadyExists'))))),
                               new sfValidatorDoctrineUnique(array(
                                        'model' => 'Bank',
                                        'column' => array('bank_name'),
                                        'primary_key' => 'id',
                                        'required' => 'Bank acronym cannot be left blank'
                                ),array('invalid'=>'The bank already exists')),
                              new sfValidatorDoctrineUnique(array(
                                        'model' => 'Bank',
                                        'column' => array('acronym'),
                                        'primary_key' => 'id',
                                        'required' => 'Bank acronym cannot be left blank'
                                ),array('invalid'=>'The acronym already exists'))

                        ))

                );
        $this->widgetSchema->setNameFormat('bank[%s]');

      
    }
    public function configureGroups() {
        $this->uiGroup = new Dlform();
        $newUserDetails = new FormBlock();
        $this->uiGroup->addElement($newUserDetails);
        $newUserDetails->addElement(array('bank_name','acronym'//,'bankConfiguration:country_id','bankConfiguration:domain'
                ));

    }
     public function bankAllReadyExists($validator,$values){
        $bankname = $values['bank_name'];
        //$countryId = $values['bankConfiguration']['country_id'];
        $bank_id = $values['id'];
        $newObj = new bankManager();
        $getBankCount = $newObj->checkBankAlreadyExists($bankname,$countryId,$bank_id);
        if($getBankCount[0]['COUNT']!= 0){
             //check if the bank has been created and deleted earlier
            $getCount = $newObj->checkBankExists($bankname,$countryId);
           
            if($getCount[0]['COUNT'] == '1')
            {
                $error = new sfValidatorError($validator, "This Bank has already been deleted by administrator");
            }
            else
            {
                $error = new sfValidatorError($validator, "The bank already exists");
            }

            throw new sfValidatorErrorSchema($validator, array('bank_name' => $error));
        }


        return $values;
    }
}
