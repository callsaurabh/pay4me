<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwaleetUserDetailForm [WP047]
 *  20-10-2011
 * @author ryadav
 */
class MerchantSplitPaymentReportForm extends MerchantForm {

    public function configure() {
        
         if(('portal_admin'==sfcontext::getInstance()->getUser()->getGroupName())
            || ('report_admin'==sfcontext::getInstance()->getUser()->getGroupName())
            || ('portal_report_admin'==sfcontext::getInstance()->getUser()->getGroupName())
                    )
            $userId='';
         else
           $userId=sfcontext::getInstance()->getUser()->getGuardUser()->getId();
         
        $merchant_id = $this->getOption('merchant_id');
        $to_date = $this->getOption('to_date'); // Vikash [WP055] Bug:36081 (07-11-2012)
         
        $this->setWidgets(array(
            'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'query' => Doctrine::getTable('Merchant')->getMerchantDetailsByEwalletId($userId), 'add_empty' => '-- All Merchants --')),
            'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'query' => Doctrine::getTable('MerchantService')->getMerchantServiceByMerchantId($merchant_id), 'add_empty' => '-- All Merchants Services --')),
            'from' => new widgetFormDateCal(array(), array('readonly' => 'true', 'class' => 'txt-input')),
            'to' => new widgetFormDateCal(array(), array('readonly' => 'true', 'class' => 'txt-input')),
        ));

        $this->widgetSchema->setNameFormat('splitDetail[%s]');

        $this->setValidators(array(
            'merchant_id' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => false),array('invalid' =>'Invalid Merchant')),
            'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => 'MerchantService','required'=>false)),
            'from' => new sfValidatorString(array('required' => false)),
            'to' => new sfValidatorString(array('required' => false)),
        ));
      // Vikash [WP055] Bug:36081 (07-11-2012)
        if($to_date){
            $this->validatorSchema->setPostValidator(

                   new sfValidatorSchemaCompare('from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to',
                        array('throw_global_error' => false),
                        array('invalid' => 'From Date cannot be greater than To Date')
             ));
        }
        $this->widgetSchema->setLabels(array(
            'merchant_id' => 'Merchant:',
            'merchant_service_id'  => 'Merchant Service:',
            'from' => 'From Date:',
            'to' => 'To Date:',
        ));
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
    }

}

?>
