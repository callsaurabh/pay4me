<?php

/**
 * BankConfiguration form.
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BankConfigurationForm extends BaseBankConfigurationForm
{
    public function configure()
    {
        unset(
            $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $this->widgetSchema['bank_id']      = new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => 'Please Select Bank'));
        $this->widgetSchema['country_id']      = new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => 'Please Select Country'));
        $this->widgetSchema['domain']      = new sfWidgetFormInputText(array(),array('maxlength'=>30));
        $this->validatorSchema['bank_id']      = new sfValidatorDoctrineChoice(array('model' => 'Bank','required'=>'true'),array('required'=>'Bank Required'));
        $this->validatorSchema['country_id']      = new sfValidatorDoctrineChoice(array('model' => 'Country','required'=>'true'),array('required'=>'Country Required'));
        $this->validatorSchema['domain']      = new sfValidatorString(array('max_length' => 255,'required'=>true),array('required'=>"Domain Required"));
        $this->widgetSchema->setHelps(array(
            'domain'       => 'If email addresses in your bank are xyz@domain.com; enter domain.com'
            ));
        $this->validatorSchema->setPostValidator(                       
                        new sfValidatorAnd(array(
                               new sfValidatorDoctrineUnique(array(
                                        'model' => 'BankConfiguration',
                                        'column' => array('bank_id','country_id'),
                                        'primary_key' => 'id',                                       
                                ),array('invalid'=>'The bank domain already exists for the selected country')),

                        ))

                );
         $this->widgetSchema->setLabels(array(
        'bank_id'    => 'Bank',
        'country_id'      => 'Country',
        'domain' => 'Domain'
    ));
    }     
}
    