<?php

/**
 * Project form base class.
 *
 * @package    form
 * @version    SVN: $Id: sfDoctrineFormBaseTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
abstract class BaseFormDoctrine extends sfFormDoctrine
{
  public function setup()
  {
    sfWidgetFormSchema::setDefaultFormFormatterName('dl');
  }
  public function configureGroups() {
    // to be overridden by the child classes
  }

  public function render($attributes = array())
  {
    //print_r($this->getFormFieldSchema());
    //    $logger = sfContext::getInstance()->getLogger();
    //    foreach ($this as $f) {
    //      $logger->info("Field Name: ".$f->getName());
    //    }
    $this->configureGroups();
    if(isset($this->uiGroup)){
      $retVal = $this->printElement($this->uiGroup);
      $retVal .= $this->renderAllHidden();
      return $retVal;
    }else{
      //echo"<pre>";print_r($this->getWidgetSchema()->getFields());die;
      foreach ($this->getWidgetSchema()->getFields() as $emName=>$elem){
        //die('yahoo');
        // echo"<pre>";print_r($elem->getOptions());echo"<hr>";
        if(isset($this->validatorSchema[$emName]) && $this->validatorSchema[$emName]->getOption('required')){
          $elemLabel = $elem->getLabel();
          if(strpos($elemLabel,'*')){$elemLabel = substr(trim($elemLabel),0,strlen($elemLabel)-1);}
          $elem->setLabel($elemLabel."<sup>*</sup>");
        }
      }
      return parent::render();
    }
  }

  public function printElement(iUIGroup $element) {
    $ffs = $this->getFormFieldSchema();
    $retVal = "";
    if ($element->getType() == iUIGroup::WEBFORM) {
      if(strlen($element->getLabel()) > 0 ) {
        $retVal .= "\n<h2>".$element->getLabel()."</h2>";
      }
    } else if ($element->getType() == iUIGroup::BLOCK) {
      //$retVal .= "\n<fieldset id=uiGroup_".str_replace(array(' ','.'),array('_',''),strip_tags($element->getLabel()))." class='multiForm'>";
      $retVal .= "\n<div id=uiGroup_".str_replace(array(' ','.'),array('_',''),strip_tags($element->getLabel()))." class='multiForm'>";
      $retVal .= (strlen($element->getLabel()) > 0 )? ePortal_legend($element->getLabel()): '';
      //$retVal .= $this->printEmbeddedFields($element);
    }
    foreach ($element->getElements() as $elem)
    {
      if (empty($elem)) {
        continue;
      }
      if ($elem instanceof iUIGroup) {
        $retVal .= $this->printElement($elem);
      } else {
        if(strpos($elem,':')) {
          // object of embedded form
          $formfield = explode (':',$elem);
          //  print_r($formfield);
          switch(count($formfield)) {
            case 2:
              $emSchema = $this->formFieldSchema[$formfield[0]]; //->getFormFieldSchema();
              $vlSchema = $this->validatorSchema[$formfield[0]] ;
              //print_r($vlSchema);
              break;
            case 3:
              // echo "Case 3: <pre>".$formfield[0]."<br>";
              //print_r(get_class($this[$formfield[0]][$formfield[1]]));
              //print_r(get_class_methods($this));
              //   die();
              $emSchema = $this[$formfield[0]][$formfield[1]]; //->getFormFieldSchema();
              $vlSchema = $this->validatorSchema[$formfield[0]][$formfield[1]];
              //print_r($vlSchema);
              break;
            default:
              $emSchema = $this->formFieldSchema[$formfield[0]]; //->getFormFieldSchema();
              $vlSchema = $this->validatorSchema[$formfield[0]] ;
              break;
          }
          $formFieldE = $formfield[count($formfield)-1];
          if($vlSchema[$formFieldE]->getOption('required')){
            $wdgt = $emSchema[$formFieldE]->getWidget();
            $clabel = $wdgt->getLabel();
            if (!strlen($clabel)) {
              // TODO this is kind of hack!! Change it with proper implementation
              $clabel = $this->widgetSchema[$formfield[0]]->getFormFormatter()->generateLabelName($formFieldE);
            }
            $wdgt->setLabel($clabel."<sup>*</sup>");
          }
          $retVal .= $emSchema[$formFieldE]->renderRow();
        } else {

          if($this->validatorSchema[$elem]->getOption('required')){
            $clabel = $this->widgetSchema[$elem]->getLabel();
            if (!strlen($clabel)) {
              // TODO this is kind of hack!! Change it with proper implementation
              $clabel = $this->widgetSchema->getFormFormatter()->generateLabelName($elem);
            }
            $this->widgetSchema[$elem]->setLabel($clabel."<sup>*</sup>") ;
          }
          $retVal .= $this->formFieldSchema[$elem]->renderRow();
        }
      }
    }

//    $retVal .= ($element->getType() == iUIGroup::BLOCK)?"</fieldset>":'';
    $retVal .= ($element->getType() == iUIGroup::BLOCK)?"</div>":'';
    return $retVal;
  }

  public function renderAllHidden() {
    $retVal = $this->renderHiddenFields();
    // TODO see how this process can be optimized
    // echo "<pre> Render: hidden:";
    //echo "<br>";print_r($this->embeddedForms);
    foreach ($this->embeddedForms as $frmName=>$frm) {
      $embedSchema = $this->formFieldSchema[$frmName];
      foreach ($embedSchema as $emFieldName=>$embedField) {
        //    echo "<br>";print_r($embedField->getName().":".get_class($embedField));
        if(get_class($embedField) =='sfFormFieldSchema'){
          foreach($embedField as $embed2FieldName=>$embed2Field){
            $embed2Schema = $this[$frmName][$embedField->getName()];
            if ($embed2Field->isHidden()) {
              $retVal .= $embed2Schema[$embed2FieldName]->render(array('type'=>'hidden'));
            }
          }
        }

        if ($embedField->isHidden()) {
          $retVal .= $embedSchema[$emFieldName]->render(array('type'=>'hidden'));
        }
      }

    }
    return $retVal;
  }
}