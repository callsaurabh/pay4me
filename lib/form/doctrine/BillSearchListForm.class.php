<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * credted by : ryadav
 * WP037 - CR057
 */


class BillSearchListForm extends BaseBillForm {

    public function configure()
    {

         $merchant =$this->getOption('merchant');
         $query= "";
         if($merchant!=""){
             $query= Doctrine::getTable('MerchantService')->getMerchantServiceByMerchantId($merchant);
         }
        if($merchant!=""){
            $this->setWidgets(array(
                'merchant' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => 'Please select Merchant')),
                'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => 'MerchantService','query'=>$query,'add_empty' => 'Please select Merchant')),
                'transaction'       => new sfWidgetFormInput(array(),array('class'=>'txt-input')),
                'from'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
                'to'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
               

            ));
         }

         else {
             $this->setWidgets(array(
                'merchant' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => 'Please select Merchant')),
                'merchant_service_id' => new sfWidgetFormChoice(array('choices' => array('' => 'Please Select Merchant Service'))),
                'transaction'       => new sfWidgetFormInput(array(),array('class'=>'txt-input')),
                'from'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
                'to'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
            ));

         }
      



        $billsType =$this->getOption('billsType');
        if($billsType=='paid'){

                    $this->widgetSchema['validation_no'] = new sfWidgetFormInput();
         }
        $this->setValidators(array(

            'from'=>new sfValidatorString(  array('required'=>false)),
            'to'=>new sfValidatorString(  array('required'=>false)),
            'merchant' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => false)),
            'merchant_service_id'     => new sfValidatorString(array('required' => false)),
            'validation_no'     => new sfValidatorString(array('required' => false)),
            'transaction'     => new sfValidatorNumber(array('required' => false))
        ));


       $arrDetails = sfContext::getInstance()->getRequest()->getParameter('bill');
      

       if(!empty($arrDetails['from']) && !empty($arrDetails['to'])){
        $this->validatorSchema->setPostValidator(

            new sfValidatorSchemaCompare('from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to',
                array('throw_global_error' => false),
                array('invalid' => 'Start date cannot be greater than End date')
            ));

       }
        $this->widgetSchema->setLabels(array(

            'merchant'    => 'Select Merchant',
            'merchant_service_id' => 'Select Merchant Service',
            'from' => 'Start Date',
            'to' => 'End Date',
            'validation_no' => 'Validation Number',
            'transaction' => 'Transaction Number'
        ));



        $this->widgetSchema->setNameFormat('bill[%s]');

       

}

}
?>
