<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MerchantWizardForm extends BaseMerchantServiceForm
{
  public function configure()
  {
  
	$categoryObj = Doctrine_Query::create()
	->from('Category')
	->orderBy('name')
	->execute();
	
	$categoryArray = array();
	$categoryArray[''] = 'Please select Category';
	
	foreach ($categoryObj as $value) {
		$categoryArray[$value->getId()] = $value->getName();
	}	
  	
	unset(
            $this['created_at'], $this['updated_at'] ,$this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'merchant_id' => new sfWidgetFormInputHidden(),
            'merchant_name' => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'class'=>'FieldInput')),
          	'category_id'=>new sfWidgetFormChoice(array ('label' => 'Merchant Category', 'choices' => $categoryArray)),
        	'name' => new sfWidgetFormInputText(array(),array('maxlength'=>50, 'class'=>'FieldInput')),
            'notification_url' => new sfWidgetFormInputText(array(),array( 'class'=>'FieldInput')),
            'merchant_home_page' => new sfWidgetFormInputText(array(),array('class'=>'FieldInput')),
            'version' => new sfWidgetFormInputText(array(),array('maxlength'=>255, 'class'=>'FieldInput')),
        ));

        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
            'merchant_id'              => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'column' => 'id', 'required' => false)),
            'merchant_name' => new sfValidatorRegex(array('pattern' =>  '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/','max_length' => 250,
           ),array('required' => 'Merchant Name can not be left blank', 'max_length' =>'Merchant Name can not be more than 30 characters.',
           'invalid' =>'Merchant Name is invalid.' )),
        	'category_id' => new sfValidatorString(array('required' => true), array('required' => 'Please select merchant category',)),
            // add validation for 0 : bug id 2887 by ryadav
            'name'        =>  new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/','max_length' => 50, 'required' => true, 'trim' => true),array('required'=>'Please enter service name','invalid' =>'Merchant Service is invalid.')),
            'notification_url' => new sfValidatorUrl(array('required' => true),array('invalid'=>'Please enter valid url')),
            'merchant_home_page' => new sfValidatorUrl(array('required' => true),array('invalid'=>'Please enter valid url')),
            'version' => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/','max_length' => 50, 'required' => false),array( 'invalid' =>'Version is invalid.')),
        ));
        
       

       //add url :bug Id 28890
        $this->widgetSchema->setLabels(array(
            'merchant_name'    => 'Merchant',
        	'category_id'    => 'Merchant Category',
            'name' => 'Merchant Service',
            'notification_url' => 'Notification Url',
            'merchant_home_page' => 'Merchant Home Page Url',
            'version' => 'Version' ,
        ));

        $this->widgetSchema->setNameFormat('merchant_wizard[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

        $this->validatorSchema->setPostValidator(
                        new sfValidatorAnd(array(
                               new sfValidatorCallback(array('callback' => array($this, 'checkDuplicacy'))),                                 
                        ))

                );

  }

    public function checkDuplicacy($validator, $values) {    
    if ($values['name'] != "" ) {
      $val = Doctrine::getTable('MerchantService')->getDeletedDetails($values['name'],$values['merchant_id']);//exit;
      if($val) {
        $error = new sfValidatorError($validator,'Cannot create Merchant Service, already deleted by administrator');
        throw new sfValidatorErrorSchema($validator,array('name' => $error));


      }
    }
    return $values;
  }

}
