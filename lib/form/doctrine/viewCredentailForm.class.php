<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of setLoggingLevelFormclass
 *
 * @author spandey
 */

class viewCredentialForm  extends sfForm {
    
    public function configure()
    {
        $groupArray =  array('' => 'Select Group',  'Admin'=>'Admin', 'Read'=>'Read', 'Write'=>'Write');
        $this->setWidgets(array(
                
                            'bank_id'     => new sfWidgetFormInputHidden(array(),array('maxlength'=>30, 'class'=>'FieldInput')),
                            'user_group' => new sfWidgetFormChoice(array('choices' => $groupArray)),
            ));

         $this->setValidators(array(
             'bank_id'     => new sfValidatorInteger(array('required' => true)),
             'user_group' => new sfValidatorChoice(array('required' => true,'choices' =>  array_keys($groupArray)),array('required'=>'Please select Group')),


            ));

          $this->widgetSchema->setLabels(array(
                  'user_group'=>"Group:"
            ));

          $this->widgetSchema->setNameFormat('credentail[%s]');

    }
}
?>
