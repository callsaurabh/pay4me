<?php

/**
 * MerchantServiceCharges form.
 *
 * @package    form
 * @subpackage MerchantServiceCharges
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MerchantServiceChargesForWiz extends BaseMerchantServiceChargesForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at'], $this['deleted_at'], $this['created_by'], $this['updated_by'], $this['merchant_service_id']);
    
    $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'service_charge_percent' => new sfWidgetFormInputText(array(),array('maxlength'=>255)),
            'upper_slab' => new sfWidgetFormInputText(array(),array('maxlength'=>255)),
           'lower_slab' => new sfWidgetFormInputText(array(),array('maxlength'=>255)),
        
        ));
    $this->setDefaults(array(
            'id'           =>"",
            'service_charge_percent' => "",
            'upper_slab' => "",
           'lower_slab' => "")

        

    );
   $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
            'service_charge_percent'  => new sfValidatorNumber( array('min'=> 0 ,'max'=> 100 ,'required' => true),array('required'=>'Please enter service charges','invalid'=>'Please enter valid service Charges','min'=>'Merchant Service Charges should be greater than 0.')),
            'upper_slab' => new sfValidatorNumber(array('min'=> 0 ,'required' => true),array('required'=>'Please enter upper slab','invalid'=>'Please enter valid upper slab','min'=>'Upper Slab should be greater than 0.')),
            'lower_slab' => new sfValidatorNumber(array('min'=> 0  , 'required' => true),array('required'=>'Please enter lower slab','invalid'=>'Please enter valid lower slab','min'=>'Lower Slab should be greater than 0.')),
            
        ));
   $this->widgetSchema->setLabels(array(
            'service_charge_percent' => 'Merchant Service Charges (%)',
            'upper_slab' => 'Upper Slab',
            'lower_slab' => 'Lower Slab',
            
        ));




    $this->widgetSchema->setNameFormat('merchant_service_charges[%s]');
    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}