<?php

/**
 * Merchant form.
 *
 * @package    form
 * @subpackage Merchant
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MerchantForm extends BaseMerchantForm {
    public function configure() {
    	
	$categoryObj = Doctrine_Query::create()
	->from('Category')
	->orderBy('name')
	->execute();    	

    	$categoryArray = array();
    	$categoryArray[''] = 'Please select Category';
    	foreach ($categoryObj as $value) {
    		$categoryArray[$value->getId()] = $value->getName();
    	}
    	
        unset(
           $this['merchant_code'], $this['merchant_key'], $this['auth_info'], $this['created_at'], $this['updated_at'] ,$this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $this->setWidgets(array(
            'id'   => new sfWidgetFormInputHidden(),
            'name' => new sfWidgetFormInputText(array(),array('maxlength'=>30)),
        ));
        
        $this->setWidget('category_id',
        		new sfWidgetFormChoice(array
        				('label' => 'Merchant Category',
        						'choices' => $categoryArray // Organization expected = 2
        				)));
        
        $this->setValidators(array(
            'id'   => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'column' => 'id', 'required' => false)),
        ));

        $this->validatorSchema['name'] = new sfValidatorAnd(array(
            new sfValidatorString(array('required' => true,'max_length' => 30,'min_length' => 3),
            array('required' => 'Merchant Name can not be left blank',
                  'max_length' =>'Merchant Name can not be more than 30 characters.',
                  'min_length' =>'Merchant Name can not be less than 3 characters.')),
            new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/'
           ),array('invalid' =>'Merchant Name is invalid.' ))
           ), array('halt_on_error' => true),array('required' => 'Merchant Name can not be left blank'));

        
        $this->validatorSchema['category_id'] = new sfValidatorAnd(array(
        		new sfValidatorString(array('required' => true),
        				array('required' => 'Please select merchant category',
        						)),
        ), array('halt_on_error' => true),array('required' => 'Please select merchant category'));
        
        
        $this->widgetSchema->setLabels(array(
            'name'    => 'Merchant Name',
        ));

        $this->widgetSchema->setNameFormat('merchant[%s]');
  }
}
