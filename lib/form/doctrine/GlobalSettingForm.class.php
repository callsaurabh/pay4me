<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class GlobalSettingtForm extends BaseGlobalMasterForm
{
  public function configure()
  {
        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'charge_amount_limit' => new sfWidgetFormInputText(),
            'charge_days_limit'   => new sfWidgetFormInputText()
        ));

        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
            'charge_amount_limit'   => new sfValidatorInteger(array('required' => true),array('required'=>'Please enter ewallet charge amount limit')),
            'charge_days_limit'     => new sfValidatorInteger(array('required' => true),array('required'=>'Please enter ewallet charge days limit')),
        ));


        $this->widgetSchema->setLabels(array(
            'charge_amount_limit'    => 'ewallet charge amount limit',
            'charge_days_limit' =>  'ewallet charge days limit'
        ));
      
        $this->widgetSchema->setNameFormat('globalsetting[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}