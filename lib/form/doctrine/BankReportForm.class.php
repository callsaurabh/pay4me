<?php

/**
 * BankBranch form.
 *
 * @package    form
 * @subpackage BankBranch
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BankReportForm extends BaseBankBranchForm
{
  public function configure()
  {
       $bank_details_id= $this->getOption('bank_details_id');
       $paymentModeResultTemp= array('' => '-- All Payment Modes --');
       $paymentModeResult=$paymentModeResultTemp+ Doctrine::getTable('PaymentModeOption')->getPaymentModeOptionArr();
       $this->widgetSchema->setLabels(array(
            'merchant'    => 'Merchant:',
            'service_type'    => 'Service Type:',
            'payment_mode' => 'Payment Mode:',
            'banks' => 'Bank:',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'disable_bank' => 'Ignore Disable Bank:',


        ));

        $this->setWidgets(array(

             'merchant'    => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant','query' => Doctrine::getTable('Merchant')->getServiceTypeOptionArr($bank_details_id,$optionQuery="yes"), 'add_empty' => '--All Merchant--'),array('style' => 'width:250px', 'onchange' => 'set_service_type_bank_report()')),
             'service_type'     => new sfWidgetFormChoice(array('choices' => array('' => '--Service Type--'),'expanded' => false),array( 'onchange' => "set_Currency_wthPre('bank_branch_report')")),
             'payment_mode'    => new sfWidgetFormChoice(array('choices' => $paymentModeResult),array('style' => 'width:250px', 'onchange' => "show_hide_element_report('bank_branch_report_payment_mode')")),
             'banks'    => new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => '--All Banks--','is_hidden'=>true),array('style' => 'width:250px')),
             'currency'    => new sfWidgetFormDoctrineChoice(array('model' => 'CurrencyCode','table_method'=>'getAllDistinctCurrencyService','method'=>'getCurrency')),
             'from_date'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
             'to_date'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
             'disable_bank' =>new sfWidgetFormInputCheckbox(),
             'report_label' => new sfWidgetFormInputHidden()

        ));



        $this->widgetSchema->setNameFormat('bank_branch_report[%s]');

         $this->setValidators(array(

            'merchant'  => new sfValidatorString(array('required' => false)),
            'service_type'  => new sfValidatorString(array('required' => false)),
            'payment_mode'  => new sfValidatorString(array('required' => false)),
            'banks'  => new sfValidatorString(array('required' => false)),
            'currency'  => new sfValidatorString(array('required' => false)),
            'from_date'    => new sfValidatorString(array('required' => false)),
            'to_date'    => new sfValidatorString(array('required' => false)),
            'disable_bank'    => new sfValidatorString(array('required' => false)),
            'report_label'    => new sfValidatorString(array('required' => false)),


        ));



         $this->widgetSchema['service_type']->setAttribute('disabled', 'disabled');
        //$this->widgetSchema['banks']-> setOption('is_hidden', 'true');
         $this->widgetSchema['banks']->setHidden(true);


  }



}
