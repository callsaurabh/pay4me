<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BillerForm extends BaseMerchantRequestForm
{
  public function configure()
  {
      /* WP032 Changes in billers for figra*/

      $merchantName = "";
      $merchantServiceId = "";
      if($this->getOption('merchantName')){
          $merchantName = $this->getOption('merchantName');
         
      }
      $merchantServiceId = $this->getOption('merchantServiceId');

        $this->setWidgets(array(
            'billerServiceId'           => new sfWidgetFormInputHidden(),
            'billerService'           => new sfWidgetFormInputHidden(),
            'biller_no' => new sfWidgetFormInputText(array(),array('maxlength'=>25)),
            'merchant_service_id' => new sfWidgetFormInputHidden(array(),array('value'=>$merchantServiceId)),
                        
        ));

      if ($merchantName == 'IFA') {
          $this->setValidators(array(
            'billerServiceId'         => new sfValidatorString(array('required' => false)),
            'billerService'           => new sfValidatorString(array('required' => false)),
            'biller_no'                  => new sfValidatorRegex(array('required'=>true,'pattern'=>'/^[A-Z](\d){7}?$/', 'required' => true),array('required'=>'Please enter Biller Number','invalid'=>'Please enter valid '.$merchantName.' Number')),
            'merchant_service_id' => new sfValidatorString(array('required' => false)),
              ));
          $this->widgetSchema->setHelps(array(
            'biller_no'       => '(must be 8 digit long, start with a capital alphabet followed by integers eg. A8765496)'
              ));
      } else {
          $this->setValidators(array(
            'billerServiceId'         => new sfValidatorString(array('required' => false)),
            'billerService'           => new sfValidatorString(array('required' => false)),
            'biller_no'     => new sfValidatorString(array('required' => true),array('required'=>'Please enter Biller Number')),
            'merchant_service_id' => new sfValidatorString(array('required' => false)),
              ));
      }

        $this->widgetSchema->setLabels(array(
            'biller_no' => ' Biller Number:'
        ));
        
        $this->widgetSchema->setNameFormat('biller[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
        $this->validatorSchema->setPostValidator(
                    new sfValidatorAnd(array(
                        //new sfValidatorCallback(array('callback' => array($this, 'checkMerchantServiceId'))),
                    ))
            );
       
  }

  public function checkMerchantServiceId($validator, $values) {

    $gUser = sfContext::getInstance()->getUser()->getGuardUser();
    $bUser = $gUser->getBankUser();
    $bankId =  $bUser->getFirst()->getBank()->getId();
    $merchant= Doctrine::getTable('MerchantService')->find($values['merchant_service_id']);
    $merchantId = $merchant->getMerchantId();
     $bankMerchantActive = Doctrine::getTable('BankMerchant')->chkBankMerchantStatus($merchantId,$bankId);
     if($bankMerchantActive == 0){
         $error = new sfValidatorError($validator,"This Merchant is de-activated for this Bank.");
        throw new sfValidatorErrorSchema($validator,array('biller_no' => $error));
      }
      return $values;
    }
    
}
