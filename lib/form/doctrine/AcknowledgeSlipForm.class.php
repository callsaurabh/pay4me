<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AcknowledgeSlipFormclass
 *
 * @author spandey
 */
class AcknowledgeSlipForm extends sfForm{
    public function configure()
    {
        $pfmHelper=new pfmHelper();
        $checkId=$pfmHelper->getPMOIdByConfName('Cheque');
        $draftId=$pfmHelper->getPMOIdByConfName('bank_draft');
        $this->setWidgets(array(
        'txnId' => new sfWidgetFormInputHidden(),
        'type' => new sfWidgetFormInputHidden(),
        'name' => new sfWidgetFormInputHidden(),
        'appCharge' => new sfWidgetFormInputHidden(),
        'bankCharge' => new sfWidgetFormInputHidden(),
        'serviceCharge' => new sfWidgetFormInputHidden(),
        'appCharge' => new sfWidgetFormInputHidden(),
        'bankCharge' => new sfWidgetFormInputHidden(),
        'serviceCharge' => new sfWidgetFormInputHidden(),
        'totalCharge' => new sfWidgetFormInputHidden(),
        'application_charge' => new sfWidgetFormInputCheckbox(),
        'bank_charges' => new sfWidgetFormInputCheckbox(),
        'service_charge' => new sfWidgetFormInputCheckbox(),
        'total_charges' => new sfWidgetFormInputCheckbox(),
        'disclaimer' => new sfWidgetFormInputCheckbox(),
        'disclaimer_cheque_details' => new sfWidgetFormInputCheckbox(),
        'disclaimer_draft_details' => new sfWidgetFormInputCheckbox(),
        //'submit'=> new sfWidgetFormInput(array('type'=>'submit'),array('name'=>'submit','value'=>'Payment','class'=>'formSubmit'))

            ));

        $this->widgetSchema->setNameFormat('acknowledge[%s]');

        $this->validatorSchema['txnId']       = new  sfValidatorString(  array('required' => false) );
        $this->validatorSchema['type']       = new sfValidatorString(  array('required' => false) );
        $this->validatorSchema['name']       = new sfValidatorString(  array('required' => false) );
        $this->validatorSchema['appCharge']       = new sfValidatorString(  array('required' => false) );

        $this->validatorSchema['bankCharge']       = new  sfValidatorString(  array('required' => false) );
        $this->validatorSchema['serviceCharge']       = new sfValidatorString(  array('required' => false) );
        $this->validatorSchema['appCharge']       = new sfValidatorString(  array('required' => false) );
        $this->validatorSchema['bankCharge']       = new sfValidatorString(  array('required' => false) );
        $this->validatorSchema['serviceCharge']       = new sfValidatorString(  array('required' => false) );
        $this->validatorSchema['totalCharge']       = new sfValidatorString(  array('required' => false) );

        $this->validatorSchema['application_charge']       = new sfValidatorBoolean(  array('required' => true) );
        if(isset($_REQUEST['acknowledge']['bankCharge']) && ($_REQUEST['clubbed']=='no')){
            $this->validatorSchema['bank_charges']       = new sfValidatorBoolean(  array('required' => true) );
        }
       if(isset($_REQUEST['acknowledge']['serviceCharge']) && ($_REQUEST['clubbed']=='no')){
         
        $this->validatorSchema['service_charge']       = new sfValidatorBoolean(  array('required' => true) );
       }
        $this->validatorSchema['total_charges']       = new sfValidatorBoolean(  array('required' => true) );
        $this->validatorSchema['disclaimer']       = new sfValidatorBoolean(  array('required' => true) );

        if(isset($_REQUEST['acknowledge']['txnId'])){
            if(Doctrine::getTable('Transaction')->findByPfmTransactionNumber($_REQUEST['acknowledge']['txnId'])->getFirst()->getPaymentModeOptionId()==$checkId){
             $this->validatorSchema['disclaimer_cheque_details']       = new sfValidatorBoolean(  array('required' => true) );
            }else if(Doctrine::getTable('Transaction')->findByPfmTransactionNumber($_REQUEST['acknowledge']['txnId'])->getFirst()->getPaymentModeOptionId()==$draftId) {
              $this->validatorSchema['disclaimer_draft_details']       = new sfValidatorBoolean(  array('required' => true) );
            }else{
                $this->validatorSchema['disclaimer_cheque_details']       = new sfValidatorBoolean(  array('required' => false) );
                 $this->validatorSchema['disclaimer_draft_details']       = new sfValidatorBoolean(  array('required' => false) );
            }
        }
        $this->widgetSchema->setLabels(array(

            'application_charge'    => 'Application Charges',
            'bank_charges'    => 'Transaction Charges',
            'service_charge'    => 'Service Charges',
            'total_charges'    => 'Total Payable Amount',
            'disclaimer'      => 'I hereby acknowledge that the information provided above is true and correct.',
            'disclaimer_cheque_details'      => 'I hereby acknowledge that the information provided above in Cheque Details is true and correct.',
            'disclaimer_draft_details'      => 'I hereby acknowledge that the information provided above in Bank Draft Details is true and correct.'
            ));
    }
}
?>
