<?php

/**
 * ServicePaymentModeOption form.
 *
 * @package    form
 * @subpackage ServicePaymentModeOption
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ServicePaymentModeOptionForm extends BaseServicePaymentModeOptionForm {

    public function configure() {
        unset(
                $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $currencyArr = array();
        $CurrencyObj = new CurrencyCode;
        $currencyArr = $CurrencyObj->getAllCountrySelect();
        $category_name = Doctrine::getTable('InterswitchCategory')->findAll();
        $categoryArray[0] = 'Please select category Name';
        $this->setDefault('interswitch_category_id', $this->getOption('interswitch_category_id'));
        foreach ($category_name as $value) {
            $categoryArray[$value->getId()] = ucwords($value->getName());
        }

        $this->setWidgets(array(
            'id' => new sfWidgetFormInputHidden(),
            'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => 'Please select Merchant')),
            'merchant_service_id' => new sfWidgetFormChoice(array('choices' => array('' => 'Please Select Merchant Service'))),
            'payment_mode_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentMode', 'add_empty' => 'Please select Payment Mode')),
            'payment_mode_option_id' => new sfWidgetFormChoice(array('choices' => array('' => 'Please Select Payment Mode Option'))),
            'currency_id' => new sfWidgetFormChoice(array('choices' => $currencyArr), array('add_empty' => 'Please select Currency')),
            'interswitch_category_id' => new sfWidgetFormChoice(array('choices' => $categoryArray), array('add_empty' => 'Please select category name')),
        ));

        $this->setValidators(array(
            'id' => new sfValidatorDoctrineChoice(array('model' => 'ServicePaymentModeOption', 'column' => 'id', 'required' => false)),
            'merchant_id' => new sfValidatorString(array('required' => true), array("required" => "Merchant Required")),
            'merchant_service_id' => new sfValidatorString(array('required' => true), array("required" => "Merchant Service Required")),
            'payment_mode_id' => new sfValidatorString(array('required' => true), array("required" => "Payment Mode Required")),
            'payment_mode_option_id' => new sfValidatorString(array('required' => true), array("required" => "Payment Mode Option Required")),
            'interswitch_category_id' => new sfValidatorString(array('required' => true), array("required" => "Category name is Required")),
                //'currency_id'        => new sfValidatorString(array('required' => true))
        ));

        $this->validatorSchema['currency_id'] = new sfValidatorNumber(array('min' => 1, 'required' => true), array('min' => "Category Name is Required", "required" => "Category Name is Required"));
        $this->widgetSchema->setLabels(array(
            'merchant_id' => 'Merchant',
            'merchant_service_id' => 'Merchant Service',
            'payment_mode_id' => 'Payment Mode',
            'payment_mode_option_id' => 'Payment Mode Option',
            'currency_id' => 'Primary Currency',
            'interswitch_category_id' => "Category Name"
        ));

        $this->widgetSchema->setNameFormat('service_payment_mode_option[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);


        $this->validatorSchema->setPostValidator(
                new sfValidatorAnd(array(
                    new sfValidatorDoctrineUnique(array(
                        'model' => 'ServicePaymentModeOption',
                        'column' => array('merchant_service_id', 'payment_mode_option_id'),
                        'primary_key' => 'id',
                        'required' => true
                            ), array('invalid' => 'The selected Merchant Service is already assigned to the selected Payment Mode Option')),
                ))
        );
        $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array('callback' => array($this, 'checkCourses')))
        );
    }

    public function checkCourses($validator, $values) {

        if (empty($values['interswitch_category_id'])) {
            $error = new sfValidatorError($validator, 'Category Name is Required');
            throw new sfValidatorErrorSchema($validator, array('interswitch_category_id' => $error));
        }
        return $values;
    }

}

