<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of transactionIdSearchFormclass
 *
 * @author spandey
 */
class transactionIdSearchForm extends BaseForm{
    public function configure()
    {
        $this->setWidgets(array(

        'txnId' => new sfWidgetFormInputText(array(),array( 'class'=>'FieldInput'))
            ));

         $this->widgetSchema->setNameFormat('search[%s]');
         $this->setValidators(array(
                  'txnId'    => new sfValidatorNumber(array('required' => true),array('required' => 'Please enter the transaction number','invalid'=>'Please enter valid transaction number')),

                ));
        $this->widgetSchema->setLabels(array(
            'txnId'    => 'Transaction Number:',

            ));
    }
}
?>
