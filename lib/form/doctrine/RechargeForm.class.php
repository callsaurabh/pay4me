<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
class RechargeForm extends BaseEwalletServiceChargesForm
{


    public function configure()
    {
        $recharge_gateway= $this->getOption('recharge_gateway');
        $this->setWidgets(array(
        'amount'  => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>15)),
            ));
        if($this->getOption('recharge_gateway')=='interswitch'){

             $this->setValidators(array(
             'amount' => new sfValidatorNumber(array('min'=>25,'required'=>true),array('invalid'=>'Please enter valid amount','required'=>'Please enter amount','min'=>'Amount can not be less than 25')),
            ));

        }else{
        $this->setValidators(array(
         'amount' => new sfValidatorNumber(array('min'=>0.01,'required'=>true),array('invalid'=>'Please enter valid amount','required'=>'Please enter amount','min'=>'Please enter valid amount ')),
            ));
        }

        $this->widgetSchema->setLabels(array(
        'amount'=> 'Amount',
            ));
//        $this->validatorSchema->setPostValidator(
//            new sfValidatorAnd(array(
//                    new sfValidatorCallback(array('callback' => array($this, 'verifyRechargeAmount')))
//
//                ))
//
//        );

        $this->widgetSchema->setNameFormat('recharge[%s]');
    }
    public function verifyRechargeAmount($validator, $values) {
        $error = '';
        $uid =  sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $rechargeAmount =  $values['amount'];
       
            if(strstr($rechargeAmount,'.')){
            $tran = explode('.',$rechargeAmount);
            if( strlen($tran[1])>=3){
                $error = new sfValidatorError($validator,'There should be two digits after decimal point');
                throw new sfValidatorErrorSchema($validator,array('amount' => $error));
            }}

        return $values;
    }
}
?>
