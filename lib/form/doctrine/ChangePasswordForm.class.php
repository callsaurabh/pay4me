<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ChangePasswordForm extends BasesfGuardUserForm {

    public function configure() {//print "<pre>";print_r($this->getOptions());
            unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list'],
      $this['is_active'],$this['username'],$this['deleted_at']
    );
           /* $userId="";
            $token="";
            if($this->getOption('userId') && $this->getOption('userId')!=""){
                $userId = $this->getOption('userId');
            }
            
            if($this->getOption('token') && ($this->getOption('token')!="")){
                $token = $this->getOption('token');
            }


        $this->widgetSchema['userId'] = new sfWidgetFormInputHidden(array(), array("value"=>$userId));
        $this->widgetSchema['token'] = new sfWidgetFormInputHidden(array(), array("value"=>$token));*/
        $this->widgetSchema['password'] = new sfWidgetFormInputPassword(array(),array('class' => 'keypad'));
        $this->widgetSchema['confirm_password'] = new sfWidgetFormInputPassword(array(),array('class' => 'keypad'));


        $this->validatorSchema['password'] = new sfValidatorAnd(array(
                    new sfValidatorString(array('max_length' => 20, 'min_length' => 8),
                            array('max_length' => 'Password can not be more than 20 characters', 'required' => 'Please Enter your New Password', 'min_length' => 'Minimum 8 characters required')),
                    new sfValidatorRegex(array('pattern' => '/[^\s+]/'), array(
                        'invalid' => 'White spaces are not allowed.'))
                ));
        $this->validatorSchema['password'] = new sfValidatorCallback(array(
                    'callback' => 'ChangeUserForm::matchNewAndConfirmPasswordCallBack'));
        $this->validatorSchema['password']->setOption('required', true);
        $this->validatorSchema['password']->setMessage('required', 'Password is required');

            $this->validatorSchema['confirm_password'] = new sfValidatorString(array('required' => true),
      array('required' => 'Please Re-enter your new password'));

             $this->widgetSchema->setLabels(
      array(    'old_password'    => 'Current Password',
      'password'    => 'New Password','confirm_password'    => 'Confirm Password'));

    $this->widgetSchema->setHelps(array(
            'password'  => '<br /> Password Strength&nbsp;&nbsp;&nbsp; <br /><span id="strength"></span>'
      ));


    }

   public function save($conn = null) {
       $userId = $this->getOption('user_id');
      $userObj = Doctrine::getTable('sfGuardUser')->find($userId);
      $userObj->setPassword($this->values['password']);
      $userObj->save();
  }


}

?>
