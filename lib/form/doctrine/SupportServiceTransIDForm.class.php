<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SupportTransID extends BaseSupportTransForm
{
  public function configure()
  {
        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'TransFormVal'    => new sfWidgetFormInputHidden(),
            'txnRef' => new sfWidgetFormInputText(array(),array('maxlength'=>25))
        ));

        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantRequest', 'column' => 'id', 'required' => false)),
            'TransFormVal'    => new sfValidatorString(array('required' => false)),
             'txnRef'          => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9 \. \-]*$/'),array('required' => 'The Transaction Number is required.','invalid' =>'The Transaction Number is invalid.' ))

        ));


        $this->widgetSchema->setLabels(array(
            'txnRef'    => 'Transaction Id.'
        ));

        $this->setDefaults(array('TransFormVal' => '1'));

        $this->widgetSchema->setNameFormat('merchant_request[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}
