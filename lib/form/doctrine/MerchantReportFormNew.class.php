<?php

/**
 * ScheduledPaymentConfig form.
 * @package    form
 * @subpackage ScheduledPaymentConfig
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MerchantReportFormNew extends sfform {

    public function configure() {
        $arrPostValue = array();
        $bank_details_id = $this->getOption('bank_details_id');
        $userGroup = $this->getOption('userGroup');
        $merchant_id = $this->getOption('merchant_id')?$this->getOption('merchant_id'):'';
        $report_label = $this->getOption('report_label');
        if ($userGroup != sfConfig::get('app_pfm_role_bank_admin') && $userGroup != sfConfig::get('app_pfm_role_bank_branch_user')) {
            $payment_status_choices = array('' => '-- All Payments List --',
                '0' => '-- Success --',
                '1' => '-- Pending --');
        } else {
            $payment_status_choices = array('0' => '-- Success --');
        }
        $selPaymentModeChoices = array('' => '-- All Payment Modes --');
        $paymentModeArray = array();
        $paymentModeArray  = Doctrine::getTable('PaymentModeOption')->getPaymentModeOptionArr();
        if (($userGroup == sfConfig::get('app_pfm_role_admin')) ||
            ($userGroup == sfConfig::get('app_pfm_role_merchant')) ||
            ($userGroup == sfConfig::get('app_pfm_role_report_admin') ||
            ($userGroup == sfConfig::get('app_pfm_role_report_portal_admin')||
            ($userGroup == sfConfig::get('app_pfm_role_ewallet_user'))))) {
            $payment_mode_choices = $selPaymentModeChoices + $paymentModeArray;
        } else {
            //get bank of logged in user
            $user_bank = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();

            $records = Doctrine::getTable('BankMerchant')->findByBankId($user_bank);
            $merchant_account = $records->getFirst()->getMerchantAccount();
            if ($merchant_account == 0) {
                $pmo_arr=array('bank','bank_draft','Cheque');
                $arrPaymentMode=Doctrine::getTable('PaymentModeOption')->getPMnPMODetailsByConfArr($pmo_arr);
                $payment_mode_choices = $selPaymentModeChoices + $arrPaymentMode;

            } else if ($merchant_account == 1) {
                $pmo_arr=array('bank','bank_draft','Cheque','merchant_counter');
                $arrPaymentMode=Doctrine::getTable('PaymentModeOption')->getPMnPMODetailsByConfArr($pmo_arr);
                $payment_mode_choices = $selPaymentModeChoices + $arrPaymentMode;
            }
        }
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $groupId = Settings::getGroupIdByGroupname(sfConfig::get('app_pfm_role_ewallet_user'));
        $this->setWidgets(array(
            'merchant' => new sfWidgetFormDoctrineChoice(
                    array('model' => 'Merchant',
                        'query' => Doctrine::getTable('Merchant')->getServiceTypeOptionArr($bank_details_id, 'yes', $userId, $groupId,$merchant_id),
                        'add_empty' => '--All Merchant--'), array('style' => 'width:250px', 'onchange' => 'set_service_type()')),
            'service_type' => new sfWidgetFormChoice(array('choices' => array('' => '-- All Services --'), 'expanded' => false), array('onchange' => "set_payment_mode()")),
            'payment_mode' => new sfWidgetFormChoice(array('choices' => $payment_mode_choices),
                    array('onchange' => 'show_hide_element_summery("payment_mode","bank_and_branch")')),
            'teller_id' => new sfWidgetFormChoice(array('choices' => array('' => '--All Bank Tellers--'), 'expanded' => false)),
            'currency_id' => new sfWidgetFormDoctrineChoice(array('model' => 'CurrencyCode', 'table_method' => 'getAllDistinctCurrencyService', 'method' => 'getCurrency')),
            'from' => new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'Date of Birth'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input')),
            'to' => new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'Date of Birth'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input')),
            'payment_status' => new sfWidgetFormChoice(array('choices' => $payment_status_choices)),
            'disable_bank' => new sfWidgetFormInputCheckbox(array(),array('checked'=>'checked')),
            'report_label' => new sfWidgetFormInputHidden(array(), array('value' => $report_label)),
        ));
        $this->widgetSchema->setLabels(array(
            'merchant' => 'Merchant:',
            'service_type' => 'Service Type:',
            'payment_mode' => 'Payment Mode:',
            'currency_id' => 'Currency:',
            'from' => 'From Date:',
            'to' => 'To Date:',
            'payment_status' => 'Payment Status:',
            'disable_bank' => 'Ignore Disable Bank:',
            'teller_id' => 'Bank Teller:',

        ));
        $this->setValidators(array(
            'merchant' =>  new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => false)),
            'service_type' => new sfValidatorString(array('required' => false)),
            'payment_mode' => new sfValidatorString(array('required' => false)),
            'currency_id' => new sfValidatorString(array('required' => false)),
            'from' => new sfValidatorString(array('required' => false)),
            'to' => new sfValidatorString(array('required' => false)),
            'disable_bank' => new sfValidatorString(array('required' => false)),
            'payment_status' => new sfValidatorString(array('required' => false)),
            'report_label' => new sfValidatorString(array('required' => false)),
        ));
        $this->widgetSchema['service_type']->setAttribute('disabled', 'disabled');
        $this->validatorSchema->setOption('allow_extra_fields', true);
    }
}