<?php

/**
 * BankBranch form.
 *
 * @package    form
 * @subpackage BankBranch
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class InputTypeHiddenForm extends sfForm
{
    public function configure()
    {


        $inputType = $this->getOption('inputType');
        $inputFiled = $this->getOption('inputFiled');



        foreach($inputFiled as $key=>$val) {
            $this->widgetSchema[$key]     = new sfWidgetFormInputHidden(array(),array('value'=>$val));
        }
    }

}