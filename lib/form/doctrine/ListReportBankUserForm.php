<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ListReportBankUserForm extends BaseUserDetailForm
{
    public function configure()
    {
        $this->setWidgets(array(
        'user_search' => new sfWidgetFormInputText(),
        ));
    
        $this->setValidators(array(
        'user_search' => new sfValidatorString(array('required'=>false))
        ));
        $this->widgetSchema->setNameFormat('listreport[%s]');

        $this->widgetSchema->setLabels(array(
        'user_search' => 'Username'
        ));
    }
}
?>
