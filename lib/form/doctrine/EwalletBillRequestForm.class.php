<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwalletBillRequestFormclass
 *
 * @author spandey
 */
class EwalletBillRequestForm extends BaseEwalletTransactionForm {
    public function configure()
    {
        $this->setWidgets(array(
            'account_name'           => new sfWidgetFormInputHidden(),
            'account_no'           => new sfWidgetFormInputHidden(),
            'bal_amount'           => new sfWidgetFormInputHidden(),
            'fromAcc' => new sfWidgetFormInputText(array(),array('add_empty' => 'Please select Merchant','maxlength'=>40)),
            'amount' => new sfWidgetFormInputText(array(),array('maxlength'=>6)),
            'description' => new sfWidgetFormTextarea(array(),array('maxlength'=>40,'rows'=>3)),

            ));

        $this->setValidators(array(
            'account_name'              =>new sfValidatorString(  array('required' => false)),
            'account_no' => new sfValidatorString(  array('required' => false)),
            'bal_amount'        => new sfValidatorString(  array('required' => true)),
            'fromAcc' => new sfValidatorString(  array('required' => true)),
            'amount' =>new sfValidatorString(  array('required' => true)),
            'description' =>new sfValidatorString(  array('required' => true)),
            ));


        $this->widgetSchema->setLabels(array(
            'fromAcc'    => 'Requesting Account Number',
            'amount' => 'Amount',
            'description' => 'Description'
            ));

    }
}
?>
