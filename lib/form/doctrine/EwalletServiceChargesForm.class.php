<?php

/**
 * EwalletServiceCharges form.
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EwalletServiceChargesForm extends BaseEwalletServiceChargesForm
{
  public function configure()
  {
      $displayName = $this->getOption('displayName');

          $this->setWidgets(array(
          'id'                     => new sfWidgetFormInputHidden(),
          'payment_mode_option_id' => new sfWidgetFormInputHidden(),
          'payment_mode'   => new  sfWidgetFormInputText(array(),array('class'=>'FieldInput', 'readonly' => true, 'value' => $displayName)),
          'charge_type'            => new sfWidgetFormChoice(array('choices' => array('percent' => 'Percent', 'flat' => 'Flat')),array('onchange'=>'showhide(this)')),
          'charge_amount'          => new sfWidgetFormInputText(array(),array('maxlength'=>10)),
          'upper_slab'             => new sfWidgetFormInputText(array(),array('maxlength'=>10)),
          'lower_slab'             => new sfWidgetFormInputText(array(),array('maxlength'=>10)),

        ));


    $arrDetails = sfContext::getInstance()->getRequest()->getParameter('ewallet_service_charges');
    

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'payment_mode_option_id' =>  new sfValidatorString(array('required' => false)),
      'charge_amount'          => new sfValidatorNumber(array('min'=>1 ,'required' => true),array('required'=>'Charge Amount / Percent Required')),
      'charge_type'            => new sfValidatorChoice(array('choices' => array(0 => 'percent', 1 => 'flat'), 'required' => false)),
      'upper_slab'             => new sfValidatorNumber(array('min'=>0,'required' => false)),
      'lower_slab'             => new sfValidatorNumber(array('min'=>0,'required' => false)),
      'payment_mode' =>  new sfValidatorString(array('required' => false))

    ));
    


   $this->validatorSchema->setPostValidator(
         new sfValidatorAnd(array(

               new sfValidatorCallback(array('callback'=>array($this,'upperValidation'),'arguments' =>array('upper_slab'=>$arrDetails['upper_slab']))),
               new sfValidatorCallback(array('callback'=>array($this,'lowerValidation'),'arguments' =>array('lower_slab'=>$arrDetails['lower_slab']))),
               new sfValidatorCallback(array('callback'=>array($this,'percentvalidation'))),
               new sfValidatorCallback(array('callback'=>array($this,'slabvalidation'))),
        ))
   );



    $this->widgetSchema->setNameFormat('ewallet_service_charges[%s]');

     $this->widgetSchema->setLabels(array(
        'payment_mode'    => 'Payment Mode Option',
        'charge_amount'    => 'Charge Amount / Percent',
        'charge_type'    => 'Charge Type',
        'upper_slab' => 'Upper Slab<sup>*</sup>',
        'lower_slab' => 'Lower Slab<sup>*</sup>',
    ));


    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);




  }




  public function getModelName()
  {
    return 'EwalletServiceCharges';
  }




  /**function upperValidation()
   *@purpose :upper validation
   *@param : $validator, $values
   *@return :  error or value
   *@author : ryadav
   *@date : 26-04-2011
   *WP034 - CR052
   */

  public function upperValidation($validator, $values)
  {

      
        $arguments = $validator->getOption('arguments');
      
        if($values['charge_type']=='percent') {

            if(empty($values['upper_slab']) && $arguments['upper_slab']==$values['upper_slab'])
            {

            $error = new sfValidatorError($validator, "Upper Slab Required");
            throw new sfValidatorErrorSchema($validator, array('upper_slab' => $error));

            }
        }
           

        return $values;


   }

  /**function lower validation
   *@purpose :lower validation
   *@param : $validator, $values
   *@return :  error or value
   *@author : ryadav
   *@date : 26-04-2011
   *WP034 - CR052
   */

  public function lowerValidation($validator, $values)
  {
        $arguments = $validator->getOption('arguments');

        if($values['charge_type']=='percent') {

            if(empty($values['lower_slab']) && $arguments['lower_slab']==$values['lower_slab'])
            {
               $error = new sfValidatorError($validator, "Lower Slab Required");
               throw new sfValidatorErrorSchema($validator, array('lower_slab' => $error));

            }
           
        }
        return $values;


   }


  /**function percentvalidation()
   *@purpose :add percent validation
   *@param : $validator, $values
   *@return :  error or value
   *@author : ryadav
   *@date : 26-04-2011
   *WP034 - CR052
   */

  public function percentvalidation($validator, $values)
  {
    if($values['charge_type']=='percent' &&  $values['charge_amount'] > 100) {


            $error = new sfValidatorError($validator, "Invalid Percent");
            throw new sfValidatorErrorSchema($validator, array('charge_amount' => $error));
        }else{
        return $values;
        }

   }


  /**function slabvalidation()
   *@purpose :add percent validation
   *@param : $validator, $values
   *@return :  error or value
   *@author : ryadav
   *@date : 26-04-2011
   *WP034 - CR052
   */

  public function slabvalidation($validator, $values)
  {

        $numeric = $this->checkNumeric($values['upper_slab'],$values['lower_slab']);
        if($values['upper_slab'] < $values['lower_slab'] && $numeric==true) {
            $error = new sfValidatorError($validator, "Upper Slab should be greater than or equal to lower slab");
            throw new sfValidatorErrorSchema($validator, array('upper_slab' => $error));
        }
        return $values;

   }


   /**function checkNumric()
   *@purpose :check array for numeric
   *@param : $upper_slab,$lower_slab
   *@return :  true false
   *@author : ryadav
   *@date : 26-04-2011
   *WP034 - CR052
   */



   function checkNumeric($upper_slab,$lower_slab){

       $tests = array($upper_slab,$lower_slab);

        foreach ($tests as $element) {
            if (!is_numeric($element)) {
                 return false;
                 break;
            } else{
                 return true;
            }
        }
   }




}

