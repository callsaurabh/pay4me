<?php

/**
 * EpInterswitchRequest form base class.
 *
 * @method EpInterswitchRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpInterswitchRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'trnx_id'      => new sfWidgetFormInputText(),
      'cadp_id'      => new sfWidgetFormInputText(),
      'mert_id'      => new sfWidgetFormInputText(),
      'amount_naira' => new sfWidgetFormInputText(),
      'amount_kobo'  => new sfWidgetFormInputText(),
      'post_url'     => new sfWidgetFormTextarea(),
      'currency'     => new sfWidgetFormInputText(),
      'user_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'trnx_id'      => new sfValidatorInteger(),
      'cadp_id'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'mert_id'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'amount_naira' => new sfValidatorNumber(array('required' => false)),
      'amount_kobo'  => new sfValidatorInteger(array('required' => false)),
      'post_url'     => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'currency'     => new sfValidatorInteger(array('required' => false)),
      'user_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpInterswitchRequest', 'column' => array('trnx_id')))
    );

    $this->widgetSchema->setNameFormat('ep_interswitch_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpInterswitchRequest';
  }

}
