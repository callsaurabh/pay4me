<?php

/**
 * EpEtranzactResponse form base class.
 *
 * @method EpEtranzactResponse getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpEtranzactResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'etranzact_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpEtranzactRequest'), 'add_empty' => false)),
      'switch_key'      => new sfWidgetFormInputText(),
      'card4'           => new sfWidgetFormInputText(),
      'merchant_code'   => new sfWidgetFormInputText(),
      'amount'          => new sfWidgetFormInputText(),
      'description'     => new sfWidgetFormInputText(),
      'check_sum'       => new sfWidgetFormInputText(),
      'final_check_sum' => new sfWidgetFormInputText(),
      'country_code'    => new sfWidgetFormInputText(),
      'no_retry'        => new sfWidgetFormInputText(),
      'status'          => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'etranzact_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpEtranzactRequest'))),
      'switch_key'      => new sfValidatorString(array('max_length' => 50)),
      'card4'           => new sfValidatorInteger(array('required' => false)),
      'merchant_code'   => new sfValidatorInteger(array('required' => false)),
      'amount'          => new sfValidatorNumber(array('required' => false)),
      'description'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'check_sum'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'final_check_sum' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'country_code'    => new sfValidatorInteger(array('required' => false)),
      'no_retry'        => new sfValidatorInteger(array('required' => false)),
      'status'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_etranzact_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpEtranzactResponse';
  }

}
