<?php

/**
 * EpEtranzactRequest form base class.
 *
 * @method EpEtranzactRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpEtranzactRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'transaction_id' => new sfWidgetFormInputText(),
      'terminal_id'    => new sfWidgetFormInputText(),
      'amount'         => new sfWidgetFormInputText(),
      'description'    => new sfWidgetFormInputText(),
      'check_sum'      => new sfWidgetFormInputText(),
      'response_url'   => new sfWidgetFormTextarea(),
      'logo_url'       => new sfWidgetFormTextarea(),
      'post_url'       => new sfWidgetFormTextarea(),
      'user_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'transaction_id' => new sfValidatorInteger(),
      'terminal_id'    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'amount'         => new sfValidatorNumber(array('required' => false)),
      'description'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'check_sum'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'response_url'   => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'logo_url'       => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'post_url'       => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'user_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpEtranzactRequest', 'column' => array('transaction_id')))
    );

    $this->widgetSchema->setNameFormat('ep_etranzact_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpEtranzactRequest';
  }

}
