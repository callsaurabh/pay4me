<?php

/**
 * Ewallet form.
 *
 * @package    form
 * @subpackage Ewallet
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EditEwalletUserForm extends BasesfGuardUserForm {
  public function configure() {

   $this->setWidgets(array(
            'email' => new sfWidgetFormInputText(array(), array('class' => 'fieldinput1')),
          ));
    $this->widgetSchema->setLabels(array(
        'email' => 'Email',
    ));
    $this->setValidators(array(
        'email' => new sfValidatorAnd(array(
                new sfValidatorEmail(array('required' => true, 'trim' => true), array('invalid' => 'Please Enter Valid Email Adrress')),
                new sfValidatorString(array('required' => true, 'max_length' => 60)),
                new sfValidatorDoctrineUnique(array('model' => 'UserDetail', 'column' => 'email'),
                        array('invalid' => 'E-mail Address already exists'))),
                    array(), array('required' => 'Please enter Email')),
    ));
    $this->setDefaults(array(
        'email' => $this->getOption('email'),
    ));
    $this->widgetSchema->setNameFormat('ewallet[%s]');
    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }

}
