<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantAccountReportFormclass
 *
 * @author spandey
 */
class MerchantAccountReportForm extends BaseMerchantForm {
    public function configure()
    {
        $Allmerchant= Doctrine::getTable('Merchant')->getAllmerchantRecords();
        $merchArr=array();
        foreach($Allmerchant as $value) {
            $merchArr[$value['id']]=$value['name'];
        }
        $merchantArr=$merchArr;
      //  $entry_type_choices = array('' => 'All Transaction List', 'debit' => '-- Debit --', 'credit' => '-- credit --');

        $this->setWidgets(array(
                  'merchant_id' => new sfWidgetFormSelect(array('choices' => $merchantArr)),
                  'from'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
                  'to'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input'))  ,
                  //'entry_type' => new sfWidgetFormSelect(array('choices' => $entry_type_choices)),

            ));
        
         $this->setValidators(array(
                'merchant_id' => new sfValidatorString(  array('required' => false) ),
                'from' => new sfValidatorString(  array('required' => false) ),
                'to' => new sfValidatorString(  array('required' => false) ),
               // 'entry_type' => new sfValidatorString(  array('required' => false) ),
            ));

        $this->widgetSchema->setLabels(array(
                                                    'merchant_id'=>'Merchant: <sup>*</sup>',
                                                    'to'=> 'To Date:',
                                                    'from'=> 'From Date:',
                                                 //   'entry_type'=>'Transaction Type:'
            ));
           $this->widgetSchema->setNameFormat('report[%s]');
      
         if(isset($_REQUEST['report']['to']) && $_REQUEST['report']['to']!=""){              
            $this->validatorSchema->setPostValidator(

                new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to',
                            array('throw_global_error' => true),
                            array('invalid' => 'The From Date ("%left_field%") cannot be greater than To Date ("%right_field%")')
                        )
                    ))

            );

        }
    }

}
?>
