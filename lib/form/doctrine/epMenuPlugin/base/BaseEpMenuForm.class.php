<?php

/**
 * EpMenu form base class.
 *
 * @method EpMenu getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpMenuForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInputText(),
      'label'       => new sfWidgetFormInputText(),
      'parent_id'   => new sfWidgetFormInputText(),
      'sfGuardPerm' => new sfWidgetFormInputText(),
      'url'         => new sfWidgetFormInputText(),
      'eval'        => new sfWidgetFormInputText(),
      'env'         => new sfWidgetFormInputText(),
      'sequence'    => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
      'deleted_at'  => new sfWidgetFormDateTime(),
      'created_by'  => new sfWidgetFormInputText(),
      'updated_by'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'label'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'parent_id'   => new sfValidatorInteger(),
      'sfGuardPerm' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'url'         => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'eval'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'env'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'sequence'    => new sfValidatorInteger(),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
      'deleted_at'  => new sfValidatorDateTime(array('required' => false)),
      'created_by'  => new sfValidatorInteger(array('required' => false)),
      'updated_by'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_menu[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpMenu';
  }

}
