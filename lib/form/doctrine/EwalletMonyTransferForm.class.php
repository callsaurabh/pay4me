<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwalletMonyTransferFormclass
 *
 * @author spandey
 */
class EwalletMonyTransferForm  extends sfForm {
    public function configure()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        $accountNumberArr = array();
        $accountObj = new UserAccountManager();
        $detail = $accountObj->getAccountForCurrency();
        foreach($detail as $val){
           $accountNumberArr[$val->getEpMasterAccount()->getId()] =  $val->getEpMasterAccount()->getAccountNumber();
        }
       
        $currency = "";
       
        if(isset($_REQUEST['monyTransfer']['from_account'])){
            $userAccountObj = new UserAccountManager();
            $currency = $userAccountObj->getCurrency($_REQUEST['monyTransfer']['from_account']);
        }
        $pinObj=new pin();
        $this->widgetSchema['account_name']     = new sfWidgetFormInputHidden();
        $this->widgetSchema['from_account'] = new sfWidgetFormChoice(array('choices' => $accountNumberArr));

        //$this->widgetSchema['from_account']       = new sfWidgetFormInputHidden();
        $this->widgetSchema['bal_amount']       = new sfWidgetFormInputHidden();

        if($pinObj->isActive())
        $this->widgetSchema['pin']              = new sfWidgetFormInputPassword(array(),array("class"=>'pin','maxlength'=>4));


        $this->widgetSchema['to_account']          = new sfWidgetFormInputText(array(),array("class"=>'pin','maxlength'=>40));
        $max_length = strlen(sfConfig::get('app_max_ewallet_transfer')/100);
        $this->widgetSchema['trans_amount']           = new sfWidgetFormInputText(array(),array('maxlength'=>$max_length));
        $this->widgetSchema['description']      = new sfWidgetFormTextarea(array(),array("class"=>'pin','rows'=>3,'cols'=>25));

        $this->validatorSchema['account_name']  = new sfValidatorString(  array('required' => false));
        $this->validatorSchema['from_account']    = new sfValidatorString(  array('required' => false));
        $this->validatorSchema['bal_amount']    = new sfValidatorString(  array('required' => false));
        if($pinObj->isActive())
        $this->validatorSchema['pin']           = new sfValidatorString(array('required' => true),array('required' => 'Pin required'));

        $this->validatorSchema['to_account']       = new sfValidatorString(array('required' => true),array('required' => 'Receiver account number required'));
        $max_amount = sfConfig::get('app_max_ewallet_transfer')/100;
        $min_amount = sfConfig::get('app_min_ewallet_transfer')/100;
        $this->validatorSchema['trans_amount']        = new sfValidatorNumber(array('min'=>$min_amount ,'max'=>$max_amount ,'required' => true),array('required' => 'Please enter amount','min'=>__('Minimum amount for transfer is ').$min_amount.' '.$currency,'max'=>__('Maximum amount for transfer is ').$max_amount.' '.$currency,'invalid'=>__('Please enter valid amount')));
        $this->validatorSchema['description']   = new sfValidatorString(array('required' => true),array('required' => 'Description required'));



        $this->widgetSchema->setLabels(array(
            'to_account'    => __('Receiver Account Number').'<font color=red><sup>*</sup></font>',
            'trans_amount' => __('Amount').'<font color=red><sup>*</sup></font>',
            'description' => 'Description<font color=red><sup>*</sup></font>',
            'pin'=>'Pin<font color=red><sup>*</sup></font>',
            'from_account'=>__('Account Number').'<font color=red><sup>*</sup></font>'
            ));
        $this->widgetSchema->setNameFormat('monyTransfer[%s]');
    }
}
?>
