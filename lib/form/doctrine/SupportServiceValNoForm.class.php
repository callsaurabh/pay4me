<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SupportValNo extends BaseSupportTransForm
{
  public function configure(){
        $this->setWidgets(array(
            'transValNo' => new sfWidgetFormInputText(array(),array('maxlength'=>25))
        ));
        $this->setValidators(array(
            'transValNo'          => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9 \. \-]*$/'),array('required' => 'The Validation Number is required.','invalid' =>'The Transaction Number is invalid.' ))
        ));
        $this->widgetSchema->setLabels(array(
            'transValNo'    => 'Validation Number'
        ));
        $this->setDefaults(array('TransFormValNo' => '1'));
        $this->widgetSchema->setNameFormat('merchant_val[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}
