<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwalletHostAccountFormclass
 *
 * @author spandey
 */
class EwalletHostAccountForm  extends BaseMerchantForm {
    public function configure()
    {
       $paymentModeOptionId = 4;
        $acctManagerObj = new EpAccountingManager;
        $masterAccounts  = $acctManagerObj->getMasterAcountDetails($paymentModeOptionId,'1');

        $masterAccountsArr = array();
        $masterAccountsArr[''] = "Please select";
        foreach($masterAccounts as $key=>$value) {
            $masterAccountsArr[$value['account_number']."_".$value['id']] =  $value['bankname']."-".$value['account_number'];
        }

        $this->setWidgets(array(
                  'master_account_id' => new sfWidgetFormSelect(array('choices' => $masterAccountsArr)),
                  'from_date'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
                  'to_date'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input'))  ,
                
            ));

         $this->setValidators(array(
                'master_account_id' => new sfValidatorString(  array('required' => true) ,array('required'=>'Please select a Bank (account) to proceed.')),
                'from_date' => new sfValidatorString(  array('required' => false) ),
                'to_date' => new sfValidatorString(  array('required' => false) ),
               
            ));

        $this->widgetSchema->setLabels(array(
                                                    'master_account_id'=>'Bank:',
                                                    'to_date'=> 'To Date:',
                                                    'from_date'=> 'From Date:',
                                                    
            ));
           $this->widgetSchema->setNameFormat('report[%s]');

         if(isset($_REQUEST['report']['to_date']) && $_REQUEST['report']['to_date']!=""){
            $this->validatorSchema->setPostValidator(

                new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from_date', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to_date',
                            array('throw_global_error' => false),
                            array('invalid' => 'The From Date ("%left_field%") cannot be greater than To Date ("%right_field%")')
                        )
                    ))

            );

        }
    }
}
?>
