<?php

/**
 * Bank form.
 *
 * @package    form
 * @subpackage Bank
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CreateSubMerchantForm extends BasesfGuardUserForm {
	public function configure() {
		$userType = $this->getOption('userType');
		$merchant_id = $this->getOption('merchant_id')?$this->getOption('merchant_id'):'';
		$merchantService = $this->getOption('merchant_service');
		$service = $this->getOption('services');
		$this->setWidgets(array(
			'merchant' => new sfWidgetFormDoctrineChoice(
				array('model' => 'Merchant',
					'query' => Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', 'yes', '', '',$merchant_id),
					'add_empty' => '--All Merchant--'), array('style' => 'width:250px', 'onchange' => 'set_service_type()')),
			'service_type' => new sfWidgetFormChoice(array('multiple' => true, 'choices' => $merchantService), array('title' => 'Merchant Service')),
			'username' => new sfWidgetFormInputText(array(),array('maxlength'=>30,'class'=>'fieldinput1')),
			'name' => new sfWidgetFormInputText(array(),array('maxlength'=>30,'class'=>'fieldinput1')),
			'dob' => new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'Date of Birth'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input')),
			'address' => new sfWidgetFormTextarea(array(),array('class'=>'txt-input')),
			'email' => new sfWidgetFormInputText(array(),array('maxlength' => 60,'class'=>'fieldinput1')),
			'mobileno' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'mobileno', 'maxlength' => '20',)),
		));
		$this->widgetSchema->setHelps(array(
			'username' => '<div id=username_suffix></div><div id="temp_div"><br></div><br/><a href="javascript:;" onclick="validateUserName();" >Check Availability</a><div id="error_name" class="error"></div>',
			'address' => '(Upto 255 characters only)',
		));
		$this->widgetSchema->setLabels(array(
			'merchant' => 'Merchant',
			'service_type' => 'Service Type',
			'name' => 'Name',
			'username' => 'User Name',	
			'address' => 'Address',
			'email' => 'Email',
			'mobileno' => 'Mobile No.',
		));
		$this->setValidators(array(
			'merchant' => new sfValidatorString(array('required' => true), array('required' => 'Please Select Merchant')),
			'name' => new sfValidatorRegex(array('max_length' => 30, 'required' =>true , 'pattern'=>'/^[a-zA-Z0-9\.\-]*$/'),array('invalid'=>'Please enter a valid Username.','required'=>'Please enter Username')),
			'service_type' => new sfValidatorString(array('required' => false)),
			'email' => new sfValidatorEmail(array('max_length' => 60, 'required' => true),array('required'=>'Please enter Email','invalid'=>'Please enter valid Email','max_length'=>'Maximum 60 characters')),
			'username' => new sfValidatorRegex(array('max_length' => 30, 'required' =>true , 'pattern'=>'/^[a-zA-Z0-9\.\-]*$/'),array('invalid'=>'Please enter a valid Username.','required'=>'Please enter Username')),
			'mobileno' => new sfValidatorRegex(array('required' => false, 'pattern' => '/^([+]{1})([0-9]{10,15})$/', 'max_length' => 20), array('max_length' => 'Maximum 20 Characters', 'invalid' => 'Please enter valid Mobile No.', 'required' => 'Please enter Mobile No.')),
			'dob' => new sfValidatorDate(array('required' => false)),
		));
		$this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'CheckDob'))))));
		$this->validatorSchema->setOption('allow_extra_fields', true);
	}
  /*  public function checkUserName($validator,$values){
        if($values['bankform']['bank_id']){
        $bankname = Doctrine::getTable('Bank')->find($values['bankform']['bank_id']);

        $user_name = $values['username'].".".$bankname->getAcronym();        
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($user_name);
        if($username_count!= 0){
             //check if the username has been created and deleted earlier
            $getUserCount = Doctrine::getTable('sfGuardUser')->chkUserDeleted($user_name);
            //echo "<pre>"; print_r($getUserCount); die();
            if($getUserCount[0]['COUNT'] == '1')
            {               
                $error = new sfValidatorError($validator, "Username already created and deleted by administrator");
            }
            else
            {                
                $error = new sfValidatorError($validator, "User name already exist");
            }
            
            throw new sfValidatorErrorSchema($validator, array('username' => $error));
        }

        
        }return $values;
    }

protected function doSave($con = null)
  {
      if($this->getOption('action') != 'edit')
      {
        $bank_id = $this->values['bankform']['bank_id'] ;
        $bankname = Doctrine::getTable('Bank')->find($bank_id);
        $this->values['username'] = $this->values['username'].".".$bankname->getAcronym();
      }
    parent::doSave($con);
  }

    public function configureGroups() {
        $userType = $this->getOption('userType');
        $this->uiGroup = new Dlform();
        //$this->uiGroup->setLabel('Portal User');

        $newUserDetails = new FormBlock();
        $this->uiGroup->addElement($newUserDetails);
        
        if(in_array($userType,CreateBankAdminForm::$groupArr))
            $newUserDetails->addElement(array('bankform:bank_id','bankform:country_id','username','details:name','details:dob','details:address',
                'details:email','details:mobile_no'));
        else
            $newUserDetails->addElement(array('bankform:bank_id','username','details:name','details:dob','details:address',
                'details:email','details:mobile_no'));

    }
*/
	public function CheckDob($validator, $values) {
	
		if (!empty($values['dob'])) {
			$dob = strtotime($values['dob']);
			$currentDate = strtotime(date('d-m-Y'));
			if ($dob >= $currentDate) {
				$error = new sfValidatorError($validator, "Date of Birth cannot be future or today's date");
				throw new sfValidatorErrorSchema($validator, array('dob' => $error));
			}
		}
		return $values;
	}
}
