<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwalletAccountStatementFormclass
 *
 * @author spandey
 */
class EwalletAccountStatementForm extends BaseEpMasterLedgerForm {
    public function configure()
    {       
        $accountNumberArr = array();
        $accountObj = new UserAccountManager();
        $detail = $accountObj->getAccountForCurrency();
        foreach($detail as $val){           
           $accountNumberArr[$val->getEpMasterAccount()->getId()] =  $val->getEpMasterAccount()->getAccountNumber();
        }

      
        $choice = array('credit' => 'Credit', 'debit' => 'Debit', 'both' => 'Both');
        $this->setWidgets(array(
                  'account_id' => new sfWidgetFormChoice(array('choices' => $accountNumberArr)),
                  'from_date'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
                  'to_date'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input'))  ,
                  'type'=>new  sfWidgetFormChoice(array('choices' => $choice ,'expanded' => true),array('class'=>''))



            ));
        $this->setDefault('type','both');
        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->setValidators(array(
                'account_id' => new sfValidatorChoice( array('choices' =>  array_keys($accountNumberArr))),
                'from_date' => new sfValidatorString(array('required' => false),array('required'=>'Please select from date') ),
                'to_date' => new sfValidatorString(array('required' => false) ,array('required'=>'Please select to date')),
                'type'=>new sfValidatorString(array('required' => false))

            ));

        $this->widgetSchema->setLabels(array(
                                                    'account_id'=>'Account No.',
                                                    'to_date'=> 'To Date:',
                                                    'from_date'=> 'From Date:',
                                                    'type'=>'Transaction Type'

            ));
        $this->widgetSchema->setNameFormat('statement[%s]');

        if(isset($_REQUEST['statement']['to_date']) && $_REQUEST['statement']['to_date']!=""){
            $this->validatorSchema->setPostValidator(

                new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from_date', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to_date',
                            array('throw_global_error' => false),
                            array('invalid' => 'From Date should be less than To Date')
                        )
                    ))

            );

        }}
}
?>
