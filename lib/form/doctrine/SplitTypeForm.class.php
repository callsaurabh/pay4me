<?php

/**
 * SplitType form.
 *
 * @package    form
 * @subpackage SplitType
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SplitTypeForm extends BaseSplitTypeForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at']);

    $this->setWidgets(array(
                            'id' => new sfWidgetFormInputHidden(),
                            'split_name' => new sfWidgetFormInputText(),
    ));

    $this->widgetSchema->setLabels(array('split_name'=>'Split Type'));

    $this->validatorSchema['split_name'] = new sfValidatorRegex(array('pattern'=> '/^[a-zA-Z0-9 \. \-]*$/', 'max_length'=> 50),
                                                                array('required'=>'Split type is required',
                                                                      'max_length' => 'The maximum length of split type should be 50 characters.',
                                                                      'invalid'=>'Invalid split type'));

    $this->widgetSchema->setNameFormat('splitType[%s]');
  }
}