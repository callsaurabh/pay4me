<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ChooseMerchantForm extends BaseChooseMerchantForm
{
  public function configure()
  {
          /*
           * param  : $strMerchantHead is used to show drop0 down heading
           * created by : ryadav
           */
  	      $merchant_id = $this->getOption('merchant_id');
          $strShowOptionalAll = $this->getOption('strShowOptionalAll');
          if($strShowOptionalAll=='strShowOptionalAll'){

               $strMerchantHead='All Merchant';
          }
          
          
          else{
          
               $strMerchantHead='Please select Merchant';

          }
        if('portal_admin'==sfcontext::getInstance()->getUser()->getGroupName())
            $userId='';
         else
             $userId=sfcontext::getInstance()->getUser()->getGuardUser()->getId();


		if($merchant_id) {
	        $this->setWidgets(array(
	            'id'           => new sfWidgetFormInputHidden(),
	            'merchantFormVal'    => new sfWidgetFormInputHidden(),
	        	'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant','query' => Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', 'yes', $userId, $groupId,$merchant_id))),
	        ));
 		 }
 		 else {
 		 	$this->setWidgets(array(
 		 			'id'           => new sfWidgetFormInputHidden(),
 		 			'merchantFormVal'    => new sfWidgetFormInputHidden(),
 		 			'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant','query' => Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', 'yes', $userId, $groupId,$merchant_id),'add_empty' => $strMerchantHead)),
 		 	));
 		 }
        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'column' => 'id', 'required' => false)),
            'merchantFormVal'    => new sfValidatorString(array('required' => false)),
          
        ));
        /*
         * last changed by  ryadav
         * Bug ID : 28926
         * here we make merchant selection optional as it use for several form show validation is change according to form
         */
        if($strShowOptionalAll=='strShowOptionalAll'){
             $this->validatorSchema['merchant_id'] =new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => false));
        }else{
             $this->validatorSchema['merchant_id'] =new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => true),array('required'=>'Please select Merchant'));
        }

        $this->widgetSchema->setLabels(array(
            'merchant_id'    => 'Merchant',            
        ));

        $this->setDefaults(array('merchantFormVal' => '1'));

        $this->widgetSchema->setNameFormat('merchant[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}