<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BankBranchListFormclass
 *
 * @author spandey
 */
class BankBranchListForm extends BaseBankBranchForm{
    public function configure()
    {
        //    ,array('onchange'=>'displayReport()')
        $bank_obj = bankServiceFactory::getService(bankServiceFactory::$TYPE_BANK);
        $bank_array = $bank_obj->getAllBanks();
        $bankArr = array();
        $bankArr[''] = "Please select bank";
        foreach($bank_array as $key=>$value) {
            $bankArr[$key] =  $value;
        }
        $this->setWidgets(array(
                 // 'bank_id' => new sfWidgetFormSelect(array('choices' => $bankArr)),
                  'branch_search'=> new sfWidgetFormInputText(array(),array('maxlength'=>30, 'class'=>'FieldInput')),

            ));
        $this->setDefault('bank_id',$this->getOption('id'));

        $this->setValidators(array(
//                'bank_id' => new sfValidatorDoctrineChoice(array('model' => 'Bank','required'=>'true'),array('required'=>'Please select bank')),
                //'bank_id' =>new sfValidatorString(  array('required' => true) ,array('required'=>'Please select bank')),
                'branch_search' => new sfValidatorString(  array('required' => false) ),


            ));

        $this->widgetSchema->setLabels(array(
                                                   // 'bank_id'=>'Select Bank',
                                                    'branch_search'=> 'Bank Branch',


            ));
        $this->widgetSchema->setNameFormat('bankBranch[%s]');

    }
    //put your code here
}
?>
