<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SupportForm extends BaseSupportForm
{
  public function configure()
  {		
  	
  		$add_empty_merchant = '--All Merchant--';
  		$add_empty_service = '--All Services--';
  		$merchant = '';
  		$merchant_id = '';
  		if($this->getOption('merchant_id')){
  			$merchant_id = $this->getOption('merchant_id');
  			$add_empty_merchant = '--Choose Merchant--';
  			$add_empty_service = '--Choose Service--';
  		}
  		//echo $merchant_id."kkk";die;
  		$userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
  		$groupId = Settings::getGroupIdByGroupname(sfConfig::get('app_pfm_role_ewallet_user'));
        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'merchant' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant','query' => Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', 'yes', $userId, $groupId,$merchant_id), 'add_empty' => $add_empty_merchant,'order_by'=>array('name','asc'))),
            'merchant_service_id' => new sfWidgetFormChoice(array('choices' => array('add_empty' => $add_empty_service)))            
        ));

        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
            'merchant' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => true),array('required'=>'Please select Merchant')),
            'merchant_service_id'     => new sfValidatorString(array('required' => true),array('required'=>'--Choose Service--')),
        ));


        $this->widgetSchema->setLabels(array(
            'merchant'    => 'Merchant',
            'merchant_service_id' => 'Service'
        ));

        $this->widgetSchema->setNameFormat('support[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}
