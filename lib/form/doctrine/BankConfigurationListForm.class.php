<?php

/**
 * BankConfiguration form.
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BankConfigurationListForm extends BaseBankConfigurationForm
{
    public function configure()
    {
         unset(
           $this['country_id'], $this['domain'],  $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );
        $this->widgetSchema['bank_id']      =  new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => 'Please select Bank', 'table_method'  => 'getBankUserList'));
        $this->validatorSchema['bank_id']      = new sfValidatorDoctrineChoice(array('model' => 'Bank','required'=>false));


        $this->widgetSchema->setLabels(array(
        'bank_id'    => 'Bank',
            ));
    }
}
