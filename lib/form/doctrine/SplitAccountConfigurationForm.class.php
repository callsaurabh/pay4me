<?php

/**
 * SplitAccountConfiguration form.
 *
 * @package    form
 * @subpackage SplitAccountConfiguration
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SplitAccountConfigurationForm extends BaseSplitAccountConfigurationForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at'], $this['deleted_at'], $this['created_by'], $this['updated_by']);

    $this->widgetSchema['account_party'] = new sfWidgetFormInputText();
    $this->widgetSchema['account_name'] = new sfWidgetFormInputText();
    $this->widgetSchema['account_number'] = new sfWidgetFormInputText();
    $this->widgetSchema['sortcode'] = new sfWidgetFormInputText();
    $this->widgetSchema['bank_name'] = new sfWidgetFormInputText();

    $this->widgetSchema->setLabels(array( 'account_party'    => 'Account Party',
                                          'account_name'     => 'Account Name',
                                          'account_number'   => 'Account No',
                                          'sortcode'         => 'Sortcode',
                                          'bank_name'        => 'Bank Name',));

    /**
     * Custom validation message
     */
    $this->validatorSchema['account_party'] = new sfValidatorRegex(array('pattern'=> '/^[a-zA-Z0-9 \. \-]*$/', 'max_length'=> 50),
                                                                array('required'=>'Account party is required',
                                                                      'max_length' => 'The maximum length of Account party should be 50 characters.',
                                                                      'invalid'=>'Invalid Account party'));
    $this->validatorSchema['account_name'] = new sfValidatorRegex(array('pattern'=> '/^[a-zA-Z0-9 \. \-]*$/', 'max_length'=> 50),
                                                                array('required'=>'Account name is required',
                                                                      'max_length' => 'The maximum length of Account name should be 50 characters.',
                                                                      'invalid'=>'Invalid Account name'));
    $this->validatorSchema['account_number'] = new sfValidatorRegex(array('pattern'=> '/^[a-zA-Z0-9 \. \-]*$/', 'max_length'=> 50),
                                                                array('required'=>'Account number is required',
                                                                      'max_length' => 'The maximum length of Account number should be 50 characters.',
                                                                      'invalid'=>'Invalid Account number'));
    $this->validatorSchema['sortcode'] = new sfValidatorRegex(array('pattern'=> '/^[a-zA-Z0-9 \. \-]*$/', 'max_length'=> 50),
                                                                array('required'=>'Sortcode is required',
                                                                      'max_length' => 'The maximum length of Sortcode should be 50 characters.',
                                                                      'invalid'=>'Invalid Sortcode'));
    $this->validatorSchema['bank_name'] = new sfValidatorRegex(array('pattern'=> '/^[a-zA-Z0-9 \. \-]*$/', 'max_length'=> 50),
                                                                array('required'=>'Bank name is required',
                                                                      'max_length' => 'The maximum length of Bank name should be 50 characters.',
                                                                      'invalid'=>'Invalid Bank name'));

    $this->widgetSchema->setNameFormat('splitAcctConf[%s]');
  }
}