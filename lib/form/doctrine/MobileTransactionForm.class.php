<?php

/**
 * MobileTransaction form.
 *
 * @package    form
 * @subpackage MobileTransaction
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MobileTransactionForm extends BaseEwalletTransactionForm
{
  public function configure()
  {

           $this->setWidgets(array(

            'account_no' => new sfWidgetFormInputText(array(),array('maxlength'=>20)),
            'from'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
            'to'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input'))
           ));
          $this->setValidators(array(
            'account_no'=>new sfValidatorNumber(array('required'=>false)),
            'from'=>new sfValidatorString(  array('required'=>false)),
            'to'=>new sfValidatorString(  array('required'=>false))
           ));
           $to_date = $this->getOption('to_date'); // Vikash [WP055] Bug:36081 (07-11-2012)

           if($to_date){
               $this->validatorSchema->setPostValidator(

                   new sfValidatorSchemaCompare('from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to',
                        array('throw_global_error' => true),
                        array('invalid' => 'From date (%left_field%) cannot be greater than To date (%right_field%)')
                    ));
           }
          $this->widgetSchema->setLabels(array(
            'account_no'    => 'eWallet Account No.',
            'from' => 'From Date',
            'to' => 'To Date'
            ));
        $this->widgetSchema->setNameFormat('transaction[%s]');
  }
}