<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ewalletAddAccountFormclass
 *
 * @author spandey
 */
class ewalletAddAccountForm extends BaseUserAccountCollectionForm{

    public function configure()
    {
        $currency =
        $currency= Doctrine::getTable('CurrencyCode')->findAll();
        $currencyArr[''] = 'Please Select Currency';
        foreach($currency as $value) {
            $currencyArr[$value['id']]=ucwords($value['currency']);
        }

        $this->setWidgets(array(
            'currency_id' => new sfWidgetFormChoice(array('choices' => $currencyArr)),
            ));

        $this->setValidators(array(
            'currency_id' => new sfValidatorChoice( array('required' => true,'choices' =>  array_keys($currencyArr)),array('required'=>'Please Select Currency')),
            ));

        
        $this->widgetSchema->setLabels(array(
            'currency_id'    => 'Currency',

            ));
        $this->setDefault('currency_id', ' ');
        $this->widgetSchema->setNameFormat('add_account[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }
}
?>
