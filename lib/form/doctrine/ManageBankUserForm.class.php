<?php
/**
 * BankUser form.
 *
 * @package    form
 * @subpackage BankUser
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ManageBankUserForm extends BaseBankUserForm {
    public function configure() {
        unset(
            $this['id'],$this['user_id'],$this['bank_id'],$this['bank_branch_id'],$this['country_id'],$this['version'],
            $this['created_at'], $this['updated_at'], $this['created_by'], 
            $this['updated_by'], $this['deleted_at']
        );
       $bankId= $this->getOption('bankId');
       $countryId= $this->getOption('countryId');
       $arrUserRole = $this->getOption('bank_user_role');
       $userRole = $this->getOption('user_role');
       $arrCountry=array();
       $arrBranch=array();
       $this->widgetSchema['user_name']  =  new sfWidgetFormInputText(array(),array('maxlength'=>30));
       $this->widgetSchema['user_role']  = new sfWidgetFormChoice(array('choices' =>$arrUserRole));
       $this->widgetSchema['bank']  =  new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => '--All Banks--', 'query'  => Doctrine::getTable('Bank')->getList('','query')));
       sfContext::getInstance()->getConfiguration()->loadHelpers(array('UserRole'));
       $objApi = new UserRoleHelper();
       $arrayGroupCountry = $objApi->getBankUserGroupCountry();
       if(empty($bankId) || !isset($arrayGroupCountry[$userRole])){
           $arrCountry = array('' => '--All Countries--');
       }else{
           $arrCountry = Doctrine::getTable('BankConfiguration')->getAllCountry($bankId);
           if(!empty($arrCountry))
           $arrCountry[''] = '--All Countries--';
           else
           $arrCountry = array('' => '--All Countries--');
       }
       $this->widgetSchema['country']  = new sfWidgetFormChoice(array('choices' => $arrCountry));
       $arrayGroupBranch=array('bank_user'=>'bank_user','bank_branch_report_user'=>'bank_branch_report_user');
       if(empty($countryId) || !isset($arrayGroupBranch[$userRole])){
         $this->widgetSchema['branch']  =new sfWidgetFormChoice(array('choices' => array('' => '--All Bank Branches--')));
       }else{
         $this->widgetSchema['branch']  =  new sfWidgetFormDoctrineChoice(array('model' => 'BankBranch', 'add_empty' => '--All Bank Branches--', 'query'  => Doctrine::getTable('BankBranch')->getAllBranches($bankId,$countryId,'','query')));
       }
       $this->setValidators(array(
            'user_name' => new sfValidatorString(array('required' => false)),
            'user_role'  => new sfValidatorChoice(array('required' => false,'choices' =>  array_keys($arrUserRole))),
            'bank' =>  new sfValidatorDoctrineChoice(array('model' => 'Bank','required'=>false)),
            'country' => new sfValidatorChoice(array('required' => false,'choices' =>array_keys($arrCountry))),
            'branch' => new sfValidatorDoctrineChoice(array('model'=>'BankBranch','required' => false)),
        ));
        $this->widgetSchema->setLabels(array(
            'user_name'=> 'Username',
            'user_role' => 'User Role',
            'bank' => 'Bank',
            'country' => 'Country',
            'branch'=> 'Bank Branch',
            ));
        $this->widgetSchema->setNameFormat('bank_user[%s]');
    }
}