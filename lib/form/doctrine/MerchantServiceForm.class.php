<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MerchantServiceForm extends BaseMerchantServiceForm
{
  public function configure()
  {
      unset(
            $this['created_at'], $this['updated_at'] ,$this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => 'Please select Merchant')),
            'name' => new sfWidgetFormInputText(array(),array('maxlength'=>50, 'class'=>'FieldInput')),
            'notification_url' => new sfWidgetFormInputText(array(),array('maxlength'=>100, 'class'=>'FieldInput')),
            'merchant_home_page' => new sfWidgetFormInputText(array(),array('maxlength'=>100, 'class'=>'FieldInput')),
        ));

        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
            'merchant_id' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => true),array('required'=>'Please select Merchant')),
            'name'        => new sfValidatorString(array('max_length' => 50, 'required' => true, 'trim' => true),array('required'=>'Please enter service name')),
            'notification_url' => new sfValidatorUrl(array('max_length' => 100, 'required' => true),array('required' => 'Please enter Notification Url','invalid'=>'Please enter valid Notification url')),
            'merchant_home_page' => new sfValidatorUrl(array('max_length' => 100, 'required' => true),array('required' => 'Please enter Merchant Home Page Url','invalid'=>'Please enter valid Merchant Home Page Url')),
        ));

        //add url :bug Id 28890
        $this->widgetSchema->setLabels(array(
            'merchant_id'    => 'Merchant',
            'name' => 'Merchant Service',
            'notification_url' => 'Notification Url',
            'merchant_home_page' => 'Merchant Home Page Url'
        ));

        $this->widgetSchema->setNameFormat('merchant_service[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

      $this->validatorSchema->setPostValidator(
                        new sfValidatorAnd(array(
                               new sfValidatorCallback(array('callback' => array($this, 'checkDuplicacy'))),
                                  new sfValidatorDoctrineUnique(array(
                                        'model' => 'MerchantService',
                                        'column' => array('name','merchant_id'),
                                        'primary_key' => 'id',
                                        'required' => true
                                ),array('invalid'=>'Merchant Service already exists')),
                        ))

                );

  }

    public function checkDuplicacy($validator, $values) {
    //    print "<pre>";
   // print_r($values);exit;
    
    if ($values['name'] != "" ) {
      $val = Doctrine::getTable('MerchantService')->getDeletedDetails($values['name'],$values['merchant_id']);//exit;
      if($val) {
        $error = new sfValidatorError($validator,'Cannot create Merchant Service, already deleted by administrator');
        throw new sfValidatorErrorSchema($validator,array('name' => $error));


      }
    }
    return $values;
  }

}