<?php

/**
 * EpJobOutData form base class.
 *
 * @method EpJobOutData getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpJobOutDataForm extends EpJobDataForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('ep_job_out_data[%s]');
  }

  public function getModelName()
  {
    return 'EpJobOutData';
  }

}
