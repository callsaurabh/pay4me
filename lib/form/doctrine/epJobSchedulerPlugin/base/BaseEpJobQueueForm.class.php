<?php

/**
 * EpJobQueue form base class.
 *
 * @method EpJobQueue getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpJobQueueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'job_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpJob'), 'add_empty' => true)),
      'scheduled_start_time' => new sfWidgetFormDateTime(),
      'start_time'           => new sfWidgetFormDateTime(),
      'current_status'       => new sfWidgetFormChoice(array('choices' => array('scheduled' => 'scheduled', 'running' => 'running'))),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'job_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpJob'), 'required' => false)),
      'scheduled_start_time' => new sfValidatorDateTime(array('required' => false)),
      'start_time'           => new sfValidatorDateTime(array('required' => false)),
      'current_status'       => new sfValidatorChoice(array('choices' => array(0 => 'scheduled', 1 => 'running'), 'required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_job_queue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobQueue';
  }

}
