<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BankVirtualAccountFormclass
 *
 * @author spandey
 */
class BankVirtualAccountForm extends BaseMerchantForm {
    public function configure()
    {
        $Allmerchant= Doctrine::getTable('Merchant')->getAllmerchantRecords();
        $merchArr=array();
        foreach($Allmerchant as $value) {
            $merchArr[$value['id']]=$value['name'];
        }
        $merchantArr=$merchArr;

        $this->setWidgets(array(
     'merchant' => new sfWidgetFormSelect(array('choices' => $merchantArr)),
            ));
        $this->validatorSchema->setOption('allow_extra_fields', true);

        $this->widgetSchema->setLabels(array(
                                                    'merchant'=>'Merchant:',
                                                    
            ));
    }
}
?>
