<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwalletUserListFormclass
 *
 * @author spandey
 */
class EwalletUserListForm extends sfForm {
    //    protected static $status_list = array(''=>'All User Type',1=>'Blocked User',2=>'Unblocked User',3=>"Blocked User's Pin");
    //    protected static $option_list = array('',1,2,3);
    public function configure()
    {
        if(Settings::isEwalletPinActive())
        {
            $status_list = array(''=>'All User Type',1=>'Blocked User',2=>'Unblocked User',3=>"Blocked User's Pin");
            $option_list = array('',1,2,3);
        }else
        {
            $status_list = array(''=>'All User Type',1=>'Blocked User',2=>'Unblocked User');
            $option_list = array('',1,2);
        }

        $this->setWidgets(array(
        'status_option' => new sfWidgetFormSelect(array('choices' => $status_list)),
        'uname' => new sfWidgetFormInputText(),
        'account_number'=>   new sfWidgetFormInputText(),
            ));
        $this->widgetSchema->setNameFormat('ewalletUserList[%s]');

        if(isset( $_REQUEST['selectBy']) && $_REQUEST['selectBy'] == 'user_name' ){

            $this->setValidators(array(
                  'account_number'=>    new sfValidatorNumber(array('required' => false)),
                  'uname'    => new sfValidatorString(array('required' => true),array('required' => 'Please enter Username')),
                  'status_option'    => new sfValidatorChoice(array('choices' => $option_list,'required' => false)),
                ));
        }
        else if(isset( $_REQUEST['selectBy']) && $_REQUEST['selectBy'] == 'account_number'){
            $this->setValidators(array(
                  'account_number'=>    new sfValidatorNumber(array('required' => true),array('required' => 'Please enter Account Number','invalid'=>'Please enter a valid Account Number')),
                  'uname'    => new sfValidatorString(array('required' => false)),
                  'status_option'    => new sfValidatorChoice(array('choices' => $option_list,'required' => false)),
                ));
        }else{
            $this->setValidators(array(
                  'account_number'=>    new sfValidatorNumber(array('required' => false)),
                  'uname'    => new sfValidatorString(array('required' => false)),
                  'status_option'    => new sfValidatorChoice(array('choices' => $option_list,'required' => false)),
                ));
        }

        $this->widgetSchema->setLabels(array(
            'status_option'    => 'User Type',
            'uname'      => 'Username <sup id=sp_uname></sup>',
            'account_number'=>'Account Number <sup id=sp_account_number></sup>'
            ));

    }

}
?>
