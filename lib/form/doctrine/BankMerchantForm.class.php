<?php

/**
 * BankMerchant form.
 *
 * @package    form
 * @subpackage BankMerchant
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BankMerchantForm extends BaseBankMerchantForm {
  public function configure() {
    unset(
        $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
    );

     $currencyArr = array();
     $CurrencyObj = new CurrencyCode;
     $currencyArr = $CurrencyObj->getAllCountrySelect();

    $this->setWidgets(array(
        'id'         => new sfWidgetFormInputHidden(),
        'bank_id'     => new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => 'Please select bank','table_method'=>'getBankList')),
        'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => 'Please select Merchant')),
        'currency_id' => new sfWidgetFormChoice(array('choices' => $currencyArr),array('add_empty' => 'Please select Currency'))

    ));

    $this->setValidators(array(
        'id'          => new sfValidatorDoctrineChoice(array('model' => 'BankMerchant', 'column' => 'id', 'required' => false)),
        'bank_id'     => new sfValidatorDoctrineChoice(array('model' => 'Bank')),
        'merchant_id' => new sfValidatorDoctrineChoice(array('model' => 'Merchant')),

    ));



  $this->validatorSchema->setPostValidator(
                        new sfValidatorAnd(array(
                                new sfValidatorDoctrineUnique(array(
                                        'model' => 'BankMerchant',
                                        'column' => array('merchant_id','bank_id'),
                                        'primary_key' => 'id',
                                        'required' => true
                                ),array('invalid'=>'The selected Bank and Merchant are already associated'))
                        ))
                );


    $this->validatorSchema['currency_id']        = new sfValidatorNumber(array('min'=>1 ,'required' => true),array('min'=>'Required'));
    $this->widgetSchema->setLabels(array(
        'bank_id'    => 'Bank',
        'merchant_id'      => 'Merchant',
        'currency_id' => 'Primary Currency'
    ));

    

    $this->widgetSchema->setNameFormat('bank_merchant[%s]');

  }
}