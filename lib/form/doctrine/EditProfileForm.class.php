<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EditProfileFormclass
 *
 * @author spandey
 */
class EditProfileForm extends BaseForm {

    public function configure() {
        $this->setWidgets(array(
            'name' => new sfWidgetFormInputText(array(), array('class' => 'FieldInput')),
            'address' => new sfWidgetFormTextarea(array(), array('rows' => '2', 'class' => 'FieldInput')),
            'email' => new sfWidgetFormInputText(array(), array('class' => 'FieldInput')),
            'dob' => new widgetFormDateCal(array('format' => '%d-%m-%Y'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input')),
            'mobile_no' => new sfWidgetFormInputText(array(), array('class' => 'FieldInput')),
            'domain' => new sfWidgetFormInputHidden(),
            'userId' => new sfWidgetFormInputHidden(),
        ));


        $this->widgetSchema->setNameFormat('edit[%s]');
        $this->setValidators(array(
            'name' => new sfValidatorString(array('required' => true), array('required' => 'Please enter Name')),
            'address' => new sfValidatorString(array('required' => false)),
            'email' => new sfValidatorString(array('required' => true), array('required' => 'Please enter Email')),
            'dob' => new sfValidatorDateTime(array('max' => strtotime("-1 day"), 'required' => true), 
                     array('max' => "Date of Birth cannot be future or today's date",'required'=>'Please enter Date of Birth')),
            'mobile_no' => new sfValidatorString(array('required' => false)),
            'domain' => new sfValidatorString(array('required' => false)),
            'userId' => new sfValidatorString(array('required' => false))
        ));
        if (isset($_REQUEST['edit']['mobile_no']) && $_REQUEST['edit']['mobile_no'] != "") {
            $this->validatorSchema['mobile_no'] = new sfValidatorRegex(array('pattern' => '/^(\+)(\d){10,14}?$/'), array(
                        'invalid' => 'Please enter Valid Mobile number'));
        }
        if (isset($_REQUEST['edit']['email']) && $_REQUEST['edit']['email'] != "") {
            $this->validatorSchema->setPostValidator(
                    new sfValidatorCallback(array('callback' => array($this, 'isValidEmail')))
            );
        }

        $this->widgetSchema->setLabels(array(
            'name' => 'Name',
            'dob' => 'Date of Birth',
            'amount' => 'Address',
            'email' => 'Email',
            'mobile_no' => 'Mobile Number',
        ));
    }

    function isValidEmail($validator, $values) {
        $err = '';
        $pattern = "/^([A-Za-z0-9_\-\.])+$/";

        if (!preg_match($pattern, $values['email'])) {
            $error = new sfValidatorError($validator, 'Please enter Valid Email');
            throw new sfValidatorErrorSchema($validator, array('email' => $error));
        }
    }

}

?>
