<?php
/**
 * BroadcastMessagePermission form.
 *
 * @package    form
 * @subpackage BroadcastMessagePermission
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BroadcastMessagePermissionForm extends BaseBroadcastMessagePermissionForm {

    public function configure() {
        unset($this['created_at'], $this['updated_at']);

        $bank_obj = new bankManager();
        $bank_array = $bank_obj->getAllBanks();

        $array1 = array();
        $array1['0'] = '-- All eWallet Users-- ';
        $array1['all_banks'] = '--All banks -- ';
        
        foreach ($bank_array as $key => $value) {
            $array1[$key] = $value;
        }

        $bank_array = $array1;

        $userRole = new UserRoleHelper();
        $userRoleArray = $userRole->getBankUserList();
        $user_role_array['all_role'] = 'All Roles';
        unset($userRoleArray['']);
        
        
        foreach($userRoleArray as $key=>$value){
            $user_role_array[$key] = $value;
        }

        // $user_role_array['ewallet_user'] = 'All eWallet Users';

        //asort($user_role_array);

        
        $userRoleWithoutEwallet =  $user_role_array;

        $bank_id = $this->getOption('bank_id');
        $selected_bank = explode(',', $bank_id);

        $user_role = $this->getOption('user_role');
        $selected_user_role = explode(',', $user_role);

        if ((in_array('ewallet_user', $selected_user_role)) || (array_intersect($selected_user_role,array_merge(sfConfig::get('app_ewallet_account_type'),sfConfig::get('app_pay4me_account_type'))))) {
            $selected_bank['0'] = '0';
            if (isset($_POST['premission_field']['bank_id'])) {
                if ($_POST['premission_field']['bank_id']['0'] == 0) {
                    $user_role_array = array();
                    $user_role_array['ewallet_user'] = 'All eWallet Users';
                    foreach(sfConfig::get('app_ewallet_account_type') as $key=>$value){
                        $user_role_array[$key] = $value;
                    }
                    foreach(sfConfig::get('app_pay4me_account_type') as $key=>$value){
                        $user_role_array[$key] = $value;
                    }
                }
            }
            if (isset($_POST['sf_method'])) {
                $user_role_array = array();
                $user_role_array['ewallet_user'] = 'All eWallet Users';
                foreach(sfConfig::get('app_ewallet_account_type') as $key=>$value){
                    $user_role_array[$key] = $value;
                }
                foreach(sfConfig::get('app_pay4me_account_type') as $key=>$value){
                    $user_role_array[$key] = $value;
                }
            } else {
                if (isset($_POST['premission_field']['specific_to'])) {
                    if ($_POST['premission_field']['specific_to']['0'] == 'ewallet_user') {
                        $user_role_array = array();
                        $user_role_array['ewallet_user'] = 'All eWallet Users';
                        foreach(sfConfig::get('app_ewallet_account_type') as $key=>$value){
                            $user_role_array[$key] = $value;
                        }
                        foreach(sfConfig::get('app_pay4me_account_type') as $key=>$value){
                            $user_role_array[$key] = $value;
                        }
                    }
                }
            }
            if (isset($_POST['premission_field']['specific_to'])) {
                if ($_POST['premission_field']['specific_to']['0'] != '0') {
                    $user_role_array = $userRole->getBankUserList();
                    $user_role_array['all_role'] = 'All Role';
                }
            }
        } else {
            if (empty($selected_bank['0'])) {
                $selected_bank['0'] = 'all_banks';
            }
        }
        if (isset($_POST['premission_field']['specific_to'])) {
            if ($_POST['premission_field']['specific_to']['0'] == 'ewallet_user') {
                $user_role_array = array();
                $user_role_array['ewallet_user'] = 'All eWallet Users';
                foreach(sfConfig::get('app_ewallet_account_type') as $key=>$value){
                    $user_role_array[$key] = $value;
                }
                foreach(sfConfig::get('app_pay4me_account_type') as $key=>$value){
                    $user_role_array[$key] = $value;
                }
            }
        }
        if (isset($_POST['premission_field']['bank_id'])) {
            if ($_POST['premission_field']['bank_id']['0'] == '0') {
                $user_role_array = array();
                $user_role_array['ewallet_user'] = 'All eWallet Users';
                foreach(sfConfig::get('app_ewallet_account_type') as $key=>$value){
                    $user_role_array[$key] = $value;
                }
                foreach(sfConfig::get('app_pay4me_account_type') as $key=>$value){
                    $user_role_array[$key] = $value;
                }
            }else {
                $user_role_array = $userRoleWithoutEwallet;
            }
        }

        $this->setWidgets(array(
            'bank_id' => new sfWidgetFormSelect(array('choices' => $bank_array, 'multiple' => 'multiple'), array('style' => 'width:250px')),
            'specific_to' => new sfWidgetFormSelect(array('choices' => $user_role_array, 'multiple' => 'multiple'), array('style' => 'width:250px')),
        ));

        $this->getObject()->setBankId($selected_bank);
        $this->getObject()->setSpecificTo($selected_user_role);
//        $this->setValidators(array(
//            'bank_id' => new sfValidatorChoiceMany(array('choices' => array_keys($bank_array), 'required' => true), array('required' => 'Please select Bank')),
//            'specific_to' => new sfValidatorChoiceMany(array('choices' => array_keys($user_role_array), 'required' => true), array('required' => 'Please select user role')),
//        ));
$this->setValidators(array(
            'bank_id' => new sfValidatorChoice(array('choices' => array_keys($bank_array), 'required' => true,'multiple'=>true), array('required' => 'Please select Bank')),
            'specific_to' => new sfValidatorChoice(array('choices' => array_keys($user_role_array), 'required' => true,'multiple'=>true), array('required' => 'Please select user role')),
        ));
        $this->widgetSchema->setLabels(array(
            'bank_id' => 'Bank Name List / eWallet',
            'specific_to' => 'User Role List'
        ));
        $this->widgetSchema->setNameFormat('premission_field[%s]');
    }
}