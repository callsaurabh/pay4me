<?php
class NewEwalletUserForm extends BaseUserDetailForm {
    public function configure()
    {
        $requestList=array('0'=>'All','2'=>'Applied','1'=>'Approved','3'=>'Rejected');
        $this->setWidgets(array(
        'request_type' => new sfWidgetFormSelect(array('choices' => $requestList)),
        'from'=>new widgetFormDateCal(array('format' => '%d-%m-%Y')),
        'to'=>new widgetFormDateCal(array('format' => '%d-%m-%Y'))
        //'to'=>new widgetFormDateTimeCal()
            ));
$this->validatorSchema->setOption('allow_extra_fields', true);

        $this->widgetSchema->setLabels(array(
                                                    'to'=> 'To Date',
                                                    'from'=> 'From Date',
            ));
    }
}
?>
