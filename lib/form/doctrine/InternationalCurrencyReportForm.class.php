<?php

/**
 * International Currency form to display fields and and to show, validate data
 */
class InternationalCurrencyReportForm extends BaseForm {

    public function configure() {
        $currencyid = 2;

        $records = Doctrine::getTable('Merchant')->getAllmerchantRecords($currencyid);

        //    die;
        $optionsVals[''] = 'Please select Merchant';
        $i = 1;
        foreach ($records as $status) {

            if ('' != $status['name'])
                $optionsVals[$status['id']] = ucwords($status['name']);
        }

        $service_choices = array('' => 'Please select Merchant Service');

        $this->setWidgets(array(
            'username' => new sfWidgetFormInputText(),
            'merchant_id' => new sfWidgetFormSelect(array('choices' => $optionsVals)),
            'merchant_service_id' => new sfWidgetFormChoice(array('choices' => $service_choices)),
        ));

        if ($this->getOption('merchant_service_choices') != '') {



            $this->getWidget('merchant_service_id')->setOption('choices', $service_choices + $this->getOption('merchant_service_choices'));
        }

        $this->setValidators(array(
            'username' => new sfValidatorString(array('required' => false)),
            'merchant_id' => new sfValidatorChoice(array('choices' => array_keys($optionsVals)), array('required' => 'Please select Merchant')),
            //new sfValidatorChoice(array('choices' => array_keys($optionsVals),'required'=>'true')),
            'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'required' => 'true'), array('required' => 'Please select Merchant Service')),
        ));


        $this->widgetSchema->setNameFormat('paidwithvisa[%s]');


        $this->widgetSchema['from_date'] = new widgetFormDateCal(array('format' => '%m/%d/%Y', 'label' => 'From Date'), array('readonly' => 'true'));

        $this->widgetSchema['to_date'] = new widgetFormDateCal(array('format' => '%m/%d/%Y', 'label' => 'To Date'), array('readonly' => 'true'));

        $this->validatorSchema['from_date'] = new sfValidatorDate(array('required' => false, 'max' => strtotime(date('d-m-Y'))), array('invalid' => 'Please Select Valid Date', 'max' => 'From Date should not be a future Date'));
        $this->validatorSchema['from_date'] = new sfValidatorDate(array('required' => false, 'max' => strtotime(date('d-m-Y'))), array('invalid' => 'Please Select Valid Date', 'max' => 'To Date should not be a future Date'));

        $this->widgetSchema->setLabels(array(
            'username' => 'UserName',
            'merchant_id' => 'Merchant<sup>*</sup>',
            'merchant_service_id' => 'Merchant Service<sup>*</sup>',
        ));
        $this->validatorSchema->setPostValidator(new sfvalidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'validateDates')), array()))));

        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function validateDates($validator, $values) {
        $values = $this->getTaintedValues();
        if ($values['from_date'] != '' && $values['to_date'] != '') {

            if (strtotime($values['from_date']) > strtotime($values['to_date'])) {
                $error['to_date'] = new sfValidatorError($validator, 'To Date should be greater than From Date');
            }
        }

        if (!empty($error)) {

            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

}