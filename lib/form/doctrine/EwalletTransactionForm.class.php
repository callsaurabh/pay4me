<?php

/**
 * EwalletTransaction form.
 *
 * @package    form
 * @subpackage EwalletTransaction
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EwalletUserTransactionForm extends BaseEwalletTransactionForm {

    public function configure() {
        $this->setWidgets(array(
            'account_no' => new sfWidgetFormInputText(array(), array('maxlength' => 20)),
            'from' => new widgetFormDateCal(array(), array('readonly' => 'true', 'class' => 'txt-input')),
            'to' => new widgetFormDateCal(array(), array('readonly' => 'true', 'class' => 'txt-input')),
            'currency_id' => new sfWidgetFormDoctrineChoice(array('model' => 'CurrencyCode')),
        ));
        $this->setValidators(array(
            'account_no' => new sfValidatorNumber(array('required' => false)),
            'from' => new sfValidatorString(array('required' => false)),
            'to' => new sfValidatorString(array('required' => false)),
            'currency_id' => new sfValidatorDoctrineChoice(array('model' => 'CurrencyCode', 'required' => false)),
        ));
        $arrDetails = sfContext::getInstance()->getRequest()->getParameter('transaction');
        if (!empty($arrDetails['from']) && !empty($arrDetails['to'])) {
            $this->validatorSchema->setPostValidator(
                    new sfValidatorSchemaCompare('from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to',
                            array('throw_global_error' => false),
                            array('invalid' => 'From date (%left_field%) cannot be greater than To date (%right_field%)')
            ));
        }
        $this->widgetSchema->setLabels(array(
            'account_no' => 'eWallet account No.',
            'from' => 'From Date',
            'to' => 'To Date',
            'currency_id' => 'Currency',
        ));
        $this->widgetSchema->setNameFormat('transaction[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
    }

}