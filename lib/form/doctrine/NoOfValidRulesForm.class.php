<?php

/**
 * NoOfValidRules form.
 *
 * @package    form
 * @subpackage ValidationRules
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class NoOfValidRulesForm extends BaseNoOfValidRulesForm
{
  public function configure()
  {

        $this->setWidgets(array(
            'no_of_rules' => new sfWidgetFormInputText(array(),array('maxlength'=>2)),
        ));

        $this->setValidators(array(
             'no_of_rules'    => new sfValidatorInteger(array('max' => 10,'min' => 0,'required' => true), array('required' => 'The No. Of Validation Rules is required.'))
        ));


        $this->widgetSchema->setLabels(array(
            'no_of_rules'    => 'No. Of Validation Rules',
        ));

        
        $this->widgetSchema->setNameFormat('noOfValidRules[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}