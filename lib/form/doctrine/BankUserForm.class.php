<?php

/**
 * BankUser form.
 *
 * @package    form
 * @subpackage BankUser
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BankUserForm extends BaseBankUserForm {
    public function configure() {
        unset(
            $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );
        $groupArr = $this->getOption('gruopArr');       
        $bankId = '';
        $userType = $this->getOption('userType');
        $bankId = $this->getOption('bankId');

        $this->widgetSchema['id']  = new sfWidgetFormInputHidden();
        $this->widgetSchema['user_id']  = new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'add_empty' => 'Please select User', 'table_method'  => 'getUnassignedUserList'));
        if($bankId){
            $this->widgetSchema['bank_id']  = new sfWidgetFormDoctrineChoice(array('model' => 'Bank'));
            $this->widgetSchema['bank_id']->setOption('query',BankTable::getSelectedBankCachedQuery($bankId));
        }else{
            $this->widgetSchema['bank_id']  = new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => 'Please select Bank'),array('onchange'=>'fetch_bank_details()'));
        }
       
        $this->widgetSchema['bank_branch_id']  = new sfWidgetFormChoice(array('choices' => array('' => 'Please select bank branch')));

        $this->setValidators(array(
            'id'             => new sfValidatorDoctrineChoice(array('model' => 'BankUser', 'column' => 'id', 'required' => false)),
            'user_id'        => new sfValidatorDoctrineChoice(array('model' => 'sfGuardUser','required'=>'true')),
            'bank_id'        => new sfValidatorDoctrineChoice(array('model' => 'Bank','required'=>'true'),array('required'=>'Please select Bank')),
            'bank_branch_id' => new sfValidatorString(array('required' => false)),      
            ));
        if(in_array($userType,$groupArr)){
           $getBankArray = Doctrine::getTable('BankConfiguration')->getAllCountry($bankId);
           if(!empty($getBankArray)){
           foreach($getBankArray as $key=>$val){                  
                 $accountNumberArr[$key] =  $val;
             }
            }{
                $accountNumberArr[""]='Please Select Country';
            }
            $this->widgetSchema['country_id']      = new sfWidgetFormChoice(array('choices' => $accountNumberArr),array('onchange'=>'fetch_bank_details()'));
            $this->validatorSchema['country_id'] =  new sfValidatorChoice( array('choices' =>  array_keys($accountNumberArr)),array('required'=>'Please Select Country'));
//            $this->widgetSchema['country_id']      = new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => 'Please select Country'));//
//            $this->validatorSchema['country_id']      = new sfValidatorDoctrineChoice(array('model' => 'Country','required'=>'true'),array('required'=>'Please select Country'));

        }
        $this->widgetSchema->setLabels(array(
            'user_id'    => 'User',
            'bank_id'    => 'Bank',
            'bank_branch' => 'Bank Branch',
            'country_id' => 'Country'
            ));

        $this->widgetSchema->setNameFormat('bank_user[%s]');
    }
}