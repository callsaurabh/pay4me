<?php

/**
 * Feedback form.
 *
 * @package    form
 * @subpackage Feedback
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class FeedbackForm extends BaseFeedbackForm
{
  public function configure()
  {
      unset($this['id'],$this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by'],$this['deleted_at']);

      $merchantObj = Doctrine::getTable('Merchant')->getAllMerchantsOrderByName();
        $merchantArray = array();
        $merchantArray[''] = 'Please select Merchant';
        foreach($merchantObj as $value) {
            $merchantArray[$value->getId()] = $value->getName();
        }
      $paymentModeArr = Doctrine::getTable('PaymentModeOption')->getPaymentModeOptionArr();
      foreach ($paymentModeArr as $key => $value) {
                if ( $value == "Merchant Counter") {
                    unset($paymentModeArr[$key]);
                }
            }
      $paymentModeArray[''] = 'Please select Payment Mode';
      $paymentModeArray = $paymentModeArray+$paymentModeArr;
      $bankObj = Doctrine::getTable('Bank')->getList();
        $bankArray = array();
        $bankArray[''] = 'Please select Bank';
        foreach($bankObj as $value) {
            $bankArray[$value['id']] = $value['bank_name'];
        }

      $this->widgetSchema['ip'] =  new sfWidgetFormInputHidden();
      $this->widgetSchema['merchant_id']= new sfWidgetFormChoice(array ('choices' => $merchantArray ));
      $this->widgetSchema['common_issue']= new sfWidgetFormChoice(array('choices' => array('' => '--Please select Common Issue--')));
      $this->widgetSchema['payment_mode_option_id'] = new sfWidgetFormChoice(array ( 'choices' => $paymentModeArray
                ));
      $this->widgetSchema['bank_id'] = new sfWidgetFormChoice(array ( 'choices' => $bankArray
                ));
      $this->widgetSchema['ewallet_username'] = new sfWidgetFormInputText(array(),array('maxlength'=>50
                ));
      $this->widgetSchema['ewallet_account_number'] = new sfWidgetFormInputText(array(),array('maxlength'=>40
                ));
      $this->widgetSchema['reason'] = new sfWidgetFormInputText(array(),array('maxlength'=>100
                ));

      $this->validatorSchema->setOption('allow_extra_fields', true);
      $this->widgetSchema->setLabels(array(
      'name'       => 'Full Name',
      'email'      => 'Email',
      'merchant_id'    => 'Subject',
      'message'    => 'Additional Comments'

      ));

$this->setValidators(array(
      'name'       => new sfValidatorString(array('max_length' => 255, 'required' => true)),
      'email'      => new sfValidatorEmail(array('required' => true),array('invalid'=>'Must be a valid email address')),
      'message'    => new sfValidatorString(array('required' => true ,'max_length' => 1000)),
      'merchant_id'    => new sfValidatorString(array( 'required' => true), array('required'=> 'Please select Merchant')),
      'bank_id'    => new sfValidatorString(array( 'required' => false)),
      'ewallet_username'    => new sfValidatorString(array( 'required' => false)),
      'ewallet_account_number'    => new sfValidatorString(array( 'required' => false)),
      'reason'    => new sfValidatorString(array( 'required' => false)),
      'payment_mode_option_id'    => new sfValidatorString(array( 'required' => false)),
      'common_issue'    => new sfValidatorString(array( 'required' => true), array('required'=>'Please select Common Issue')),
      'ip'    => new sfValidatorString(array('max_length' => 15, 'required' => true)),
    ));
  }
}