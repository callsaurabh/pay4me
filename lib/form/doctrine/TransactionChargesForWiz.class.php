<?php

/**
 * MerchantServiceCharges form.
 *
 * @package    form
 * @subpackage MerchantServiceCharges
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class TransactionChargesForWiz extends BasetransactionChargesForm
{
  public function configure()
  {

    $paymentMode = $this->getOption('paymentMode');
    $type = $this->getOption('type');
    $pfmHelperObj = new pfmHelper();
    $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
    $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
    $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');
    $reqFinancialCharge = false;
    if($type == "add") {
    foreach($paymentMode as $key=>$value){
          if($key == $paymentModeCheckId || $key == $paymentModeDraftId || $key == $paymentModebankId  ){
              $reqFinancialCharge = true;
          }
      }
    }else {
          if($paymentMode == $paymentModeCheckId || $paymentMode == $paymentModeDraftId || $paymentMode == $paymentModebankId  ){
              $reqFinancialCharge = true;
          }
    }
    unset($this['created_at'], $this['updated_at'], $this['deleted_at'], $this['created_by'], $this['updated_by'], $this['merchant_service_id']);
    $Options =  $this->getOptions();
    
    $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'PayForCharge' => new sfWidgetFormInputText(array(),array('maxlength'=>255)),
            'finInstCharge' => new sfWidgetFormInputText(array(),array('maxlength'=>255))        
        ));
    $this->setDefaults(array(
            'id'           =>"",
            'PayForCharge' => "",
            'finInstCharge' => ""
              )

        

    );
   $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
            'PayForCharge' => new sfValidatorNumber(array('min'=> 0 ,'required' => true),array('required'=>'Please enter Pay4Me charges','invalid'=>'Please enter valid Pay4Me charges')),
            'finInstCharge' => new sfValidatorNumber(array('min'=> 0  , 'required' => $reqFinancialCharge),array('required'=>'Please enter Financial Institution Charges','invalid'=>'Please enter valid Financial Institution Charges')),
            
        ));
   $this->widgetSchema->setLabels(array(
            'PayForCharge' => 'Pay4Me Charges',
            'finInstCharge' => 'Financial Institution Charges',
            
        ));



    $this->widgetSchema->setNameFormat('transaction_charges[%s]');
    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}