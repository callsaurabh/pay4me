<?php
class EwalletUserReportForm extends BaseBankForm {
  public function configure()
  {
    $user_types= array('system_user'=>'System Users','pay4me' => 'Pay4me Users');
    $pay4me_user_types= array('open_id'=>'Open-Id Users','non_open_id' => 'Non Open-ID Users' );
    $system_user_types= array('all' => '--All Users--','portal_admin' => 'Portal Admin','merchant'=>'Merchant Users','portal_support' => 'Portal Support','portal_report_admin' => 'Portal Report Admin','support' => 'Support','report_admin' => 'Report Admin' );
    $this->setWidgets(array(
        'user_type' => new sfWidgetFormSelect(array('choices' => $user_types)),
        'pay4me_user_type' => new sfWidgetFormSelect(array('choices' => $pay4me_user_types)),
    	'system_user_type' => new sfWidgetFormSelect(array('choices' => $system_user_types)),
    ));
    
    $this->validatorSchema->setOption('allow_extra_fields', true);
    $this->widgetSchema->setLabels(array(
        'user_type'=>'User Type',
        'pay4me_user_type' => 'Pay4me User Type',
    	'system_user_type' => 'System User Type',
    ));
  }
}
