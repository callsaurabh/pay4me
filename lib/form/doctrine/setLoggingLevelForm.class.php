<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of setLoggingLevelFormclass
 *
 * @author spandey
 */

class setLoggingLevelForm  extends sfForm {
    public function configure()
    {     
        $levelArray =  array('' => 'Select Logging Level',  'DEBUG'=>'Debug', 'ERROR'=>'Error', 'INFO'=>'Info','WARN' =>'Warn','FATAL'=>'Fatal' );
        $this->setWidgets(array(
                            
                            'level' => new sfWidgetFormChoice(array('choices' => $levelArray)),                           
                            'bank' =>  new sfWidgetFormDoctrineChoice(array('model' => 'bank', 'add_empty' => 'Please select bank', 'table_method'  => 'getBIEnabledBankList')),
                            

            ));

         $this->setValidators(array(
            'level' => new sfValidatorChoice(array('required' => true,'choices' =>  array_keys($levelArray)),array('required'=>'Please select logging level')),
            'bank' => new sfValidatorDoctrineChoice(array('model' => 'bank','required'=>'true'),array('required'=>'Please select Bank'))
            

            ));
         
        $this->validatorSchema['bank'] =new sfValidatorDoctrineChoice(array('model' => 'bank','required'=>'true'),array('required'=>'Please select Bank'));
         //$_Request will remove 
         if(isset($_REQUEST['loggingLevel']['bank']) && $_REQUEST['loggingLevel']['bank']!=""){
              $this->validatorSchema['level'] = new sfValidatorChoice(array('required' => true,'choices' => array_keys($levelArray)),array('required'=>'Please select logging level'));
         }else{
              $this->validatorSchema['level'] = new sfValidatorChoice(array('required' => false,'choices' => array_keys($levelArray)));
         }


          $this->widgetSchema->setLabels(array(
                                                    'level'=>'Logging Level:<sup>*</sup>',
                                                    'bank'=> 'Bank<sup>*</sup>:',
            ));

    $this->widgetSchema->setNameFormat('loggingLevel[%s]');

    }
}
?>
