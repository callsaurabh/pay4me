<?php

/**
 * BankCredential form base class.
 *
 * @method BankCredential getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBankCredentialForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'bank_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => false)),
      'user_name'      => new sfWidgetFormInputText(),
      'password'       => new sfWidgetFormInputText(),
      'user_group'     => new sfWidgetFormChoice(array('choices' => array('admin' => 'admin', 'read' => 'read', 'write' => 'write'))),
      'asymmetric_key' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bank_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'))),
      'user_name'      => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'password'       => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'user_group'     => new sfValidatorChoice(array('choices' => array(0 => 'admin', 1 => 'read', 2 => 'write'), 'required' => false)),
      'asymmetric_key' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bank_credential[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BankCredential';
  }

}
