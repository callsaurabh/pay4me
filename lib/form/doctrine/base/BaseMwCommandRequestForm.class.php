<?php

/**
 * MwCommandRequest form base class.
 *
 * @method MwCommandRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMwCommandRequestForm extends MwRequestForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('mw_command_request[%s]');
  }

  public function getModelName()
  {
    return 'MwCommandRequest';
  }

}
