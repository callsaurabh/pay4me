<?php

/**
 * MessageQueueResponse form base class.
 *
 * @method MessageQueueResponse getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMessageQueueResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'request_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MessageQueueRequest'), 'add_empty' => false)),
      'response_code'        => new sfWidgetFormInputText(),
      'response_description' => new sfWidgetFormInputText(),
      'message_response'     => new sfWidgetFormTextarea(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'request_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MessageQueueRequest'))),
      'response_code'        => new sfValidatorInteger(),
      'response_description' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'message_response'     => new sfValidatorString(array('required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('message_queue_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MessageQueueResponse';
  }

}
