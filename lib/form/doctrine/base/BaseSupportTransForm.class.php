<?php

/**
 * MerchantService form base class.
 *
 * @package    form
 * @subpackage merchant_service
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseSupportTransForm extends BaseFormDoctrine
{
  public function setup()
  {
      

    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'txnRef'        => new sfWidgetFormInputText()
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'MerchantRequest', 'column' => 'id', 'required' => false)),
      'txnRef'        => new sfValidatorDoctrineChoice(array('model' => 'MerchantRequest'))
    ));

    $this->widgetSchema->setNameFormat('merchant_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantRequest';
  }

}
