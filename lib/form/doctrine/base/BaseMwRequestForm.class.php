<?php

/**
 * MwRequest form base class.
 *
 * @method MwRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMwRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                          => new sfWidgetFormInputHidden(),
      'message_text'                => new sfWidgetFormInputText(),
      'bank_mw_mapper_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BankMwMapping'), 'add_empty' => true)),
      'transaction_bank_posting_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TransactionBankPosting'), 'add_empty' => true)),
      'message_type'                => new sfWidgetFormInputText(),
      'created_at'                  => new sfWidgetFormDateTime(),
      'updated_at'                  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'message_text'                => new sfValidatorPass(array('required' => false)),
      'bank_mw_mapper_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('BankMwMapping'), 'required' => false)),
      'transaction_bank_posting_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TransactionBankPosting'), 'required' => false)),
      'message_type'                => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'                  => new sfValidatorDateTime(),
      'updated_at'                  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('mw_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MwRequest';
  }

}
