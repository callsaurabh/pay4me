<?php

/**
 * MessageLog form base class.
 *
 * @method MessageLog getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMessageLogForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'level'          => new sfWidgetFormInputText(),
      'transaction_no' => new sfWidgetFormInputText(),
      'log_message'    => new sfWidgetFormInputText(),
      'location'       => new sfWidgetFormInputText(),
      'logger_name'    => new sfWidgetFormInputText(),
      'time_stamp'     => new sfWidgetFormInputText(),
      'thread_name'    => new sfWidgetFormInputText(),
      'class_name'     => new sfWidgetFormInputText(),
      'error_details'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'level'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'transaction_no' => new sfValidatorInteger(array('required' => false)),
      'log_message'    => new sfValidatorPass(array('required' => false)),
      'location'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'logger_name'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'time_stamp'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'thread_name'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'class_name'     => new sfValidatorPass(array('required' => false)),
      'error_details'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('message_log[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MessageLog';
  }

}
