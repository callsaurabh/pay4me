<?php

/**
 * EwalletTransactionTrack form base class.
 *
 * @method EwalletTransactionTrack getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEwalletTransactionTrackForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'transaction_code'      => new sfWidgetFormInputText(),
      'type'                  => new sfWidgetFormChoice(array('choices' => array('BillRequest' => 'BillRequest', 'BillApprove' => 'BillApprove', 'BillReject' => 'BillReject', 'eWalletFreeze' => 'eWalletFreeze', 'eWalletTransfer' => 'eWalletTransfer'))),
      'app_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EwalletTransaction'), 'add_empty' => true)),
      'wallet_account_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => false)),
      'platform'              => new sfWidgetFormChoice(array('choices' => array('Web' => 'Web', 'Mobile' => 'Mobile'))),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInputText(),
      'updated_by'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'transaction_code'      => new sfValidatorInteger(),
      'type'                  => new sfValidatorChoice(array('choices' => array(0 => 'BillRequest', 1 => 'BillApprove', 2 => 'BillReject', 3 => 'eWalletFreeze', 4 => 'eWalletTransfer'))),
      'app_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EwalletTransaction'), 'required' => false)),
      'wallet_account_number' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'))),
      'platform'              => new sfValidatorChoice(array('choices' => array(0 => 'Web', 1 => 'Mobile'))),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
      'created_by'            => new sfValidatorInteger(array('required' => false)),
      'updated_by'            => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'EwalletTransactionTrack', 'column' => array('transaction_code'))),
        new sfValidatorDoctrineUnique(array('model' => 'EwalletTransactionTrack', 'column' => array('transaction_code'))),
      ))
    );

    $this->widgetSchema->setNameFormat('ewallet_transaction_track[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EwalletTransactionTrack';
  }

}
