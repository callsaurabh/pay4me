<?php

/**
 * GatewayOrder form base class.
 *
 * @method GatewayOrder getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseGatewayOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'app_id'                 => new sfWidgetFormInputText(),
      'order_id'               => new sfWidgetFormInputText(),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => true)),
      'type'                   => new sfWidgetFormChoice(array('choices' => array('pay' => 'pay', 'recharge' => 'recharge'))),
      'validation_number'      => new sfWidgetFormInputText(),
      'user_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'currency_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => false)),
      'servicecharge_kobo'     => new sfWidgetFormInputText(),
      'status'                 => new sfWidgetFormChoice(array('choices' => array('success' => 'success', 'failure' => 'failure'))),
      'amount'                 => new sfWidgetFormInputText(),
      'pan'                    => new sfWidgetFormInputText(),
      'approvalcode'           => new sfWidgetFormInputText(),
      'response_code'          => new sfWidgetFormInputText(),
      'response_txt'           => new sfWidgetFormInputText(),
      'transaction_date'       => new sfWidgetFormDateTime(),
      'locked'                 => new sfWidgetFormInputText(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'deleted_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'app_id'                 => new sfValidatorString(array('max_length' => 100)),
      'order_id'               => new sfValidatorInteger(),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'required' => false)),
      'type'                   => new sfValidatorChoice(array('choices' => array(0 => 'pay', 1 => 'recharge'))),
      'validation_number'      => new sfValidatorInteger(array('required' => false)),
      'user_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'currency_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'required' => false)),
      'servicecharge_kobo'     => new sfValidatorInteger(array('required' => false)),
      'status'                 => new sfValidatorChoice(array('choices' => array(0 => 'success', 1 => 'failure'), 'required' => false)),
      'amount'                 => new sfValidatorInteger(array('required' => false)),
      'pan'                    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'approvalcode'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_code'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_txt'           => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'transaction_date'       => new sfValidatorDateTime(array('required' => false)),
      'locked'                 => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
      'deleted_at'             => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gateway_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GatewayOrder';
  }

}
