<?php

/**
 * MwCommandResponse form base class.
 *
 * @method MwCommandResponse getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMwCommandResponseForm extends MwResponseForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('mw_command_response[%s]');
  }

  public function getModelName()
  {
    return 'MwCommandResponse';
  }

}
