<?php

/**
 * SplitEntityConfiguration form base class.
 *
 * @method SplitEntityConfiguration getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSplitEntityConfigurationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                             => new sfWidgetFormInputHidden(),
      'merchant_service_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'split_type_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SplitType'), 'add_empty' => true)),
      'charge'                         => new sfWidgetFormInputText(),
      'split_account_configuration_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SplitAccountConfiguration'), 'add_empty' => true)),
      'merchant_item_split_column'     => new sfWidgetFormChoice(array('choices' => array('split_one' => 'split_one', 'split_two' => 'split_two', 'split_three' => 'split_three', 'split_four' => 'split_four', 'split_five' => 'split_five', 'split_six' => 'split_six', 'split_seven' => 'split_seven', 'split_eight' => 'split_eight', 'split_nine' => 'split_nine', 'split_ten' => 'split_ten', 'split_eleven' => 'split_eleven', 'split_twelve' => 'split_twelve', 'split_thirteen' => 'split_thirteen', 'split_fourteen' => 'split_fourteen', 'split_fifteen' => 'split_fifteen', 'split_sixteen' => 'split_sixteen', 'split_seventeen' => 'split_seventeen', 'split_eighteen' => 'split_eighteen', 'split_nineteen' => 'split_nineteen', 'split_twenty' => 'split_twenty'))),
      'created_at'                     => new sfWidgetFormDateTime(),
      'updated_at'                     => new sfWidgetFormDateTime(),
      'deleted_at'                     => new sfWidgetFormDateTime(),
      'created_by'                     => new sfWidgetFormInputText(),
      'updated_by'                     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_service_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'required' => false)),
      'split_type_id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SplitType'), 'required' => false)),
      'charge'                         => new sfValidatorNumber(array('required' => false)),
      'split_account_configuration_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SplitAccountConfiguration'), 'required' => false)),
      'merchant_item_split_column'     => new sfValidatorChoice(array('choices' => array(0 => 'split_one', 1 => 'split_two', 2 => 'split_three', 3 => 'split_four', 4 => 'split_five', 5 => 'split_six', 6 => 'split_seven', 7 => 'split_eight', 8 => 'split_nine', 9 => 'split_ten', 10 => 'split_eleven', 11 => 'split_twelve', 12 => 'split_thirteen', 13 => 'split_fourteen', 14 => 'split_fifteen', 15 => 'split_sixteen', 16 => 'split_seventeen', 17 => 'split_eighteen', 18 => 'split_nineteen', 19 => 'split_twenty'), 'required' => false)),
      'created_at'                     => new sfValidatorDateTime(),
      'updated_at'                     => new sfValidatorDateTime(),
      'deleted_at'                     => new sfValidatorDateTime(array('required' => false)),
      'created_by'                     => new sfValidatorInteger(array('required' => false)),
      'updated_by'                     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('split_entity_configuration[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SplitEntityConfiguration';
  }

}
