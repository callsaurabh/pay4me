<?php

/**
 * TransactionBankPosting form base class.
 *
 * @method TransactionBankPosting getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTransactionBankPostingForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => true)),
      'no_of_attempts'      => new sfWidgetFormInputText(),
      'last_mw_request_id'  => new sfWidgetFormInputText(),
      'message_txn_no'      => new sfWidgetFormInputText(),
      'txn_type'            => new sfWidgetFormChoice(array('choices' => array('payment' => 'payment', 'recharge' => 'recharge'))),
      'state'               => new sfWidgetFormChoice(array('choices' => array('not_posted' => 'not_posted', 'posted_to_queue' => 'posted_to_queue', 'success' => 'success', 'failure' => 'failure'))),
      'bank_txn_number'     => new sfWidgetFormInputText(),
      'bank_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'required' => false)),
      'no_of_attempts'      => new sfValidatorInteger(array('required' => false)),
      'last_mw_request_id'  => new sfValidatorInteger(array('required' => false)),
      'message_txn_no'      => new sfValidatorInteger(),
      'txn_type'            => new sfValidatorChoice(array('choices' => array(0 => 'payment', 1 => 'recharge'), 'required' => false)),
      'state'               => new sfValidatorChoice(array('choices' => array(0 => 'not_posted', 1 => 'posted_to_queue', 2 => 'success', 3 => 'failure'), 'required' => false)),
      'bank_txn_number'     => new sfValidatorInteger(),
      'bank_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('transaction_bank_posting[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransactionBankPosting';
  }

}
