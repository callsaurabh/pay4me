<?php

/**
 * Transaction form base class.
 *
 * @method Transaction getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTransactionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'merchant_request_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => false)),
      'pfm_transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantPaymentTransaction'), 'add_empty' => false)),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => false)),
      'payment_status_code'    => new sfWidgetFormInputText(),
      'bank_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'check_number'           => new sfWidgetFormInputText(),
      'account_number'         => new sfWidgetFormInputText(),
      'sort_code'              => new sfWidgetFormInputText(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'deleted_at'             => new sfWidgetFormDateTime(),
      'created_by'             => new sfWidgetFormInputText(),
      'updated_by'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_request_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'))),
      'pfm_transaction_number' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantPaymentTransaction'))),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'))),
      'payment_status_code'    => new sfValidatorNumber(array('required' => false)),
      'bank_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'required' => false)),
      'check_number'           => new sfValidatorPass(array('required' => false)),
      'account_number'         => new sfValidatorPass(array('required' => false)),
      'sort_code'              => new sfValidatorPass(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
      'deleted_at'             => new sfValidatorDateTime(array('required' => false)),
      'created_by'             => new sfValidatorInteger(array('required' => false)),
      'updated_by'             => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('transaction[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Transaction';
  }

}
