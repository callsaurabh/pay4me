<?php

/**
 * InternetBankRechargeReportView form base class.
 *
 * @method InternetBankRechargeReportView getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInternetBankRechargeReportViewForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'app_id'                 => new sfWidgetFormInputText(),
      'order_id'               => new sfWidgetFormInputText(),
      'payment_mode_option_id' => new sfWidgetFormInputText(),
      'type'                   => new sfWidgetFormChoice(array('choices' => array('pay' => 'pay', 'recharge' => 'recharge'))),
      'validation_number'      => new sfWidgetFormInputText(),
      'user_id'                => new sfWidgetFormInputText(),
      'currency_id'            => new sfWidgetFormInputText(),
      'servicecharge_kobo'     => new sfWidgetFormInputText(),
      'status'                 => new sfWidgetFormChoice(array('choices' => array('success' => 'success', 'failure' => 'failure'))),
      'amount'                 => new sfWidgetFormInputText(),
      'pan'                    => new sfWidgetFormInputText(),
      'approvalcode'           => new sfWidgetFormInputText(),
      'response_code'          => new sfWidgetFormInputText(),
      'response_txt'           => new sfWidgetFormInputText(),
      'transaction_date'       => new sfWidgetFormDateTime(),
      'locked'                 => new sfWidgetFormInputText(),
      'username'               => new sfWidgetFormInputText(),
      'account_number'         => new sfWidgetFormInputText(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'app_id'                 => new sfValidatorString(array('max_length' => 100)),
      'order_id'               => new sfValidatorInteger(),
      'payment_mode_option_id' => new sfValidatorInteger(array('required' => false)),
      'type'                   => new sfValidatorChoice(array('choices' => array(0 => 'pay', 1 => 'recharge'))),
      'validation_number'      => new sfValidatorInteger(array('required' => false)),
      'user_id'                => new sfValidatorInteger(),
      'currency_id'            => new sfValidatorInteger(array('required' => false)),
      'servicecharge_kobo'     => new sfValidatorInteger(array('required' => false)),
      'status'                 => new sfValidatorChoice(array('choices' => array(0 => 'success', 1 => 'failure'), 'required' => false)),
      'amount'                 => new sfValidatorInteger(array('required' => false)),
      'pan'                    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'approvalcode'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_code'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_txt'           => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'transaction_date'       => new sfValidatorDateTime(array('required' => false)),
      'locked'                 => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'username'               => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'account_number'         => new sfValidatorString(array('max_length' => 40)),
      'updated_at'             => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('internet_bank_recharge_report_view[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InternetBankRechargeReportView';
  }

}
