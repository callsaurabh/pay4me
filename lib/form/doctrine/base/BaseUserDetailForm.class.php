<?php

/**
 * UserDetail form base class.
 *
 * @method UserDetail getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseUserDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'user_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'master_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'name'              => new sfWidgetFormInputText(),
      'last_name'         => new sfWidgetFormInputText(),
      'dob'               => new sfWidgetFormDate(),
      'address'           => new sfWidgetFormInputText(),
      'email'             => new sfWidgetFormInputText(),
      'mobile_no'         => new sfWidgetFormInputText(),
      'work_phone'        => new sfWidgetFormInputText(),
      'send_sms'          => new sfWidgetFormChoice(array('choices' => array('gsmno' => 'gsmno', 'workphone' => 'workphone', 'both' => 'both', 'none' => 'none'))),
      'failed_attempt'    => new sfWidgetFormInputText(),
      'user_status'       => new sfWidgetFormInputText(),
      'kyc_status'        => new sfWidgetFormInputText(),
      'openid_auth'       => new sfWidgetFormInputText(),
      'ewallet_type'      => new sfWidgetFormChoice(array('choices' => array('ewallet' => 'ewallet', 'openid' => 'openid', 'both' => 'both', 'ewallet_pay4me' => 'ewallet_pay4me', 'openid_pay4me' => 'openid_pay4me', 'both_pay4me' => 'both_pay4me'))),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'deleted_at'        => new sfWidgetFormDateTime(),
      'created_by'        => new sfWidgetFormInputText(),
      'updated_by'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'master_account_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'required' => false)),
      'name'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'last_name'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'dob'               => new sfValidatorDate(array('required' => false)),
      'address'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'             => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'mobile_no'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'work_phone'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'send_sms'          => new sfValidatorChoice(array('choices' => array(0 => 'gsmno', 1 => 'workphone', 2 => 'both', 3 => 'none'), 'required' => false)),
      'failed_attempt'    => new sfValidatorInteger(array('required' => false)),
      'user_status'       => new sfValidatorInteger(array('required' => false)),
      'kyc_status'        => new sfValidatorInteger(array('required' => false)),
      'openid_auth'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ewallet_type'      => new sfValidatorChoice(array('choices' => array(0 => 'ewallet', 1 => 'openid', 2 => 'both', 3 => 'ewallet_pay4me', 4 => 'openid_pay4me', 5 => 'both_pay4me'), 'required' => false)),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
      'deleted_at'        => new sfValidatorDateTime(array('required' => false)),
      'created_by'        => new sfValidatorInteger(array('required' => false)),
      'updated_by'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserDetail';
  }

}
