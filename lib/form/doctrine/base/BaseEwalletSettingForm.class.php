<?php

/**
 * EwalletSetting form base class.
 *
 * @method EwalletSetting getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEwalletSettingForm extends GlobalMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('ewallet_setting[%s]');
  }

  public function getModelName()
  {
    return 'EwalletSetting';
  }

}
