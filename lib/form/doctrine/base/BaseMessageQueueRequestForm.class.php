<?php

/**
 * MessageQueueRequest form base class.
 *
 * @method MessageQueueRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMessageQueueRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => true)),
      'merchant_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => true)),
      'account_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => false)),
      'bank_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => false)),
      'teller_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'currency_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => false)),
      'amount'              => new sfWidgetFormInputText(),
      'total_amount'        => new sfWidgetFormInputText(),
      'transaction_number'  => new sfWidgetFormInputText(),
      'validation_number'   => new sfWidgetFormInputText(),
      'account_number'      => new sfWidgetFormInputText(),
      'item_name'           => new sfWidgetFormInputText(),
      'type'                => new sfWidgetFormChoice(array('choices' => array('payment' => 'payment', 'recharge' => 'recharge'))),
      'payment_type'        => new sfWidgetFormChoice(array('choices' => array('credit' => 'credit', 'debit' => 'debit'))),
      'is_processed'        => new sfWidgetFormInputText(),
      'message_request'     => new sfWidgetFormTextarea(),
      'no_of_attempt'       => new sfWidgetFormInputText(),
      'transaction_date'    => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'required' => false)),
      'merchant_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'required' => false)),
      'account_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'))),
      'bank_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'))),
      'teller_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'currency_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'))),
      'amount'              => new sfValidatorInteger(),
      'total_amount'        => new sfValidatorInteger(),
      'transaction_number'  => new sfValidatorInteger(array('required' => false)),
      'validation_number'   => new sfValidatorInteger(array('required' => false)),
      'account_number'      => new sfValidatorString(array('max_length' => 40)),
      'item_name'           => new sfValidatorString(array('max_length' => 255)),
      'type'                => new sfValidatorChoice(array('choices' => array(0 => 'payment', 1 => 'recharge'))),
      'payment_type'        => new sfValidatorChoice(array('choices' => array(0 => 'credit', 1 => 'debit'))),
      'is_processed'        => new sfValidatorInteger(array('required' => false)),
      'message_request'     => new sfValidatorString(array('required' => false)),
      'no_of_attempt'       => new sfValidatorInteger(array('required' => false)),
      'transaction_date'    => new sfValidatorPass(array('required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('message_queue_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MessageQueueRequest';
  }

}
