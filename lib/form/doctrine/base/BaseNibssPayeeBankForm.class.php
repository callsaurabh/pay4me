<?php

/**
 * NibssPayeeBank form base class.
 *
 * @method NibssPayeeBank getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseNibssPayeeBankForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'account_name'  => new sfWidgetFormInputText(),
      'account_no'    => new sfWidgetFormInputText(),
      'bank_name'     => new sfWidgetFormTextarea(),
      'bank_branch'   => new sfWidgetFormTextarea(),
      'bank_sortcode' => new sfWidgetFormTextarea(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
      'deleted_at'    => new sfWidgetFormDateTime(),
      'created_by'    => new sfWidgetFormInputText(),
      'updated_by'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'account_name'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'account_no'    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'bank_name'     => new sfValidatorString(array('required' => false)),
      'bank_branch'   => new sfValidatorString(array('required' => false)),
      'bank_sortcode' => new sfValidatorString(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
      'deleted_at'    => new sfValidatorDateTime(array('required' => false)),
      'created_by'    => new sfValidatorInteger(array('required' => false)),
      'updated_by'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('nibss_payee_bank[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'NibssPayeeBank';
  }

}
