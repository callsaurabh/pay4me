<?php

/**
 * MerchantService form base class.
 *
 * @package    form
 * @subpackage merchant_service
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseIfaSerachForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ifa_no'               => new sfWidgetFormInputText(),
      'ref_no'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'ifa_no'   => new sfValidatorString(array('required' => true)),
      'ref_no'   => new sfValidatorString(array('required' => true)),
    ));

    $this->widgetSchema->setNameFormat('ifa_search[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantService';
  }

}
