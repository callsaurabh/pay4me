<?php

/**
 * EwalletRechargeAcctDetails form base class.
 *
 * @method EwalletRechargeAcctDetails getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEwalletRechargeAcctDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => false)),
      'check_number'           => new sfWidgetFormInputText(),
      'account_number'         => new sfWidgetFormInputText(),
      'sort_code'              => new sfWidgetFormInputText(),
      'ledger_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('LedgerRecord'), 'add_empty' => false)),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'))),
      'check_number'           => new sfValidatorPass(array('required' => false)),
      'account_number'         => new sfValidatorPass(array('required' => false)),
      'sort_code'              => new sfValidatorPass(array('required' => false)),
      'ledger_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('LedgerRecord'))),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ewallet_recharge_acct_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EwalletRechargeAcctDetails';
  }

}
