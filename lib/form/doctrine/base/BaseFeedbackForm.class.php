<?php

/**
 * Feedback form base class.
 *
 * @method Feedback getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFeedbackForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'name'                   => new sfWidgetFormInputText(),
      'email'                  => new sfWidgetFormInputText(),
      'merchant_id'            => new sfWidgetFormInputText(),
      'payment_mode_option_id' => new sfWidgetFormInputText(),
      'bank_id'                => new sfWidgetFormInputText(),
      'ewallet_username'       => new sfWidgetFormInputText(),
      'ewallet_account_number' => new sfWidgetFormInputText(),
      'common_issue'           => new sfWidgetFormInputText(),
      'reason'                 => new sfWidgetFormInputText(),
      'message'                => new sfWidgetFormTextarea(),
      'ip'                     => new sfWidgetFormInputText(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'deleted_at'             => new sfWidgetFormDateTime(),
      'created_by'             => new sfWidgetFormInputText(),
      'updated_by'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'merchant_id'            => new sfValidatorInteger(),
      'payment_mode_option_id' => new sfValidatorInteger(array('required' => false)),
      'bank_id'                => new sfValidatorInteger(array('required' => false)),
      'ewallet_username'       => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'ewallet_account_number' => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'common_issue'           => new sfValidatorString(array('max_length' => 128)),
      'reason'                 => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'message'                => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'ip'                     => new sfValidatorString(array('max_length' => 15)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
      'deleted_at'             => new sfValidatorDateTime(array('required' => false)),
      'created_by'             => new sfValidatorInteger(array('required' => false)),
      'updated_by'             => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('feedback[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Feedback';
  }

}
