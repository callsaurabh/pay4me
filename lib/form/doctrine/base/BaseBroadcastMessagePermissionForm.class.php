<?php

/**
 * BroadcastMessagePermission form base class.
 *
 * @method BroadcastMessagePermission getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBroadcastMessagePermissionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'bank_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'branch_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BankBranch'), 'add_empty' => true)),
      'message_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BroadcastMessage'), 'add_empty' => true)),
      'specific_to' => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bank_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'required' => false)),
      'branch_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('BankBranch'), 'required' => false)),
      'message_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('BroadcastMessage'), 'required' => false)),
      'specific_to' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('broadcast_message_permission[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BroadcastMessagePermission';
  }

}
