<?php

/**
 * MerchantRequest form base class.
 *
 * @method MerchantRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMerchantRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'txn_ref'                  => new sfWidgetFormInputText(),
      'merchant_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
      'merchant_service_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => false)),
      'merchant_item_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'), 'add_empty' => false)),
      'item_fee'                 => new sfWidgetFormInputText(),
      'payment_mode_option_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => true)),
      'bank_name'                => new sfWidgetFormInputText(),
      'payment_status_code'      => new sfWidgetFormInputText(),
      'paid_amount'              => new sfWidgetFormInputText(),
      'paid_date'                => new sfWidgetFormDateTime(),
      'paid_by'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'bank_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'bank_branch_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BankBranch'), 'add_empty' => true)),
      'bank_charge'              => new sfWidgetFormInputText(),
      'service_charge'           => new sfWidgetFormInputText(),
      'gateway_charge'           => new sfWidgetFormInputText(),
      'bank_amount'              => new sfWidgetFormInputText(),
      'payforme_amount'          => new sfWidgetFormInputText(),
      'service_configuration_id' => new sfWidgetFormInputText(),
      'payment_mandate_id'       => new sfWidgetFormInputText(),
      'transaction_location'     => new sfWidgetFormInputText(),
      'validation_number'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterLedger'), 'add_empty' => true)),
      'created_by'               => new sfWidgetFormInputText(),
      'updated_by'               => new sfWidgetFormInputText(),
      'currency_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => false)),
      'version'                  => new sfWidgetFormInputText(),
      'platform'                 => new sfWidgetFormChoice(array('choices' => array('Web' => 'Web', 'Mobile' => 'Mobile'))),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
      'deleted_at'               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'txn_ref'                  => new sfValidatorString(array('max_length' => 20)),
      'merchant_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
      'merchant_service_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'))),
      'merchant_item_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'))),
      'item_fee'                 => new sfValidatorNumber(),
      'payment_mode_option_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'required' => false)),
      'bank_name'                => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'payment_status_code'      => new sfValidatorNumber(),
      'paid_amount'              => new sfValidatorNumber(array('required' => false)),
      'paid_date'                => new sfValidatorDateTime(array('required' => false)),
      'paid_by'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'bank_id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'required' => false)),
      'bank_branch_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('BankBranch'), 'required' => false)),
      'bank_charge'              => new sfValidatorNumber(array('required' => false)),
      'service_charge'           => new sfValidatorNumber(array('required' => false)),
      'gateway_charge'           => new sfValidatorNumber(array('required' => false)),
      'bank_amount'              => new sfValidatorNumber(array('required' => false)),
      'payforme_amount'          => new sfValidatorNumber(array('required' => false)),
      'service_configuration_id' => new sfValidatorInteger(array('required' => false)),
      'payment_mandate_id'       => new sfValidatorInteger(array('required' => false)),
      'transaction_location'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'validation_number'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterLedger'), 'required' => false)),
      'created_by'               => new sfValidatorInteger(array('required' => false)),
      'updated_by'               => new sfValidatorInteger(array('required' => false)),
      'currency_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'))),
      'version'                  => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'platform'                 => new sfValidatorChoice(array('choices' => array(0 => 'Web', 1 => 'Mobile'))),
      'created_at'               => new sfValidatorDateTime(),
      'updated_at'               => new sfValidatorDateTime(),
      'deleted_at'               => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('merchant_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantRequest';
  }

}
