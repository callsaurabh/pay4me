<?php

/**
 * EwalletUserAcctConsolidation form base class.
 *
 * @method EwalletUserAcctConsolidation getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEwalletUserAcctConsolidationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'amount'                => new sfWidgetFormInputText(),
      'ewallet_account_id'    => new sfWidgetFormInputText(),
      'collection_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => false)),
      'user_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'currency_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => false)),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInputText(),
      'updated_by'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'amount'                => new sfValidatorInteger(),
      'ewallet_account_id'    => new sfValidatorInteger(),
      'collection_account_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'))),
      'user_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'currency_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'required' => false)),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
      'created_by'            => new sfValidatorInteger(array('required' => false)),
      'updated_by'            => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ewallet_user_acct_consolidation[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EwalletUserAcctConsolidation';
  }

}
