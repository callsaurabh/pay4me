<?php

/**
 * MerchantItemSplit form base class.
 *
 * @method MerchantItemSplit getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMerchantItemSplitForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => false)),
      'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => false)),
      'split_one'           => new sfWidgetFormInputText(),
      'split_two'           => new sfWidgetFormInputText(),
      'split_three'         => new sfWidgetFormInputText(),
      'split_four'          => new sfWidgetFormInputText(),
      'split_five'          => new sfWidgetFormInputText(),
      'split_six'           => new sfWidgetFormInputText(),
      'split_seven'         => new sfWidgetFormInputText(),
      'split_eight'         => new sfWidgetFormInputText(),
      'split_nine'          => new sfWidgetFormInputText(),
      'split_ten'           => new sfWidgetFormInputText(),
      'split_eleven'        => new sfWidgetFormInputText(),
      'split_twelve'        => new sfWidgetFormInputText(),
      'split_thirteen'      => new sfWidgetFormInputText(),
      'split_fourteen'      => new sfWidgetFormInputText(),
      'split_fifteen'       => new sfWidgetFormInputText(),
      'split_sixteen'       => new sfWidgetFormInputText(),
      'split_seventeen'     => new sfWidgetFormInputText(),
      'split_eightteen'     => new sfWidgetFormInputText(),
      'split_nineteen'      => new sfWidgetFormInputText(),
      'split_twenty'        => new sfWidgetFormInputText(),
      'is_processed'        => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'deleted_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'))),
      'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'))),
      'split_one'           => new sfValidatorNumber(array('required' => false)),
      'split_two'           => new sfValidatorNumber(array('required' => false)),
      'split_three'         => new sfValidatorNumber(array('required' => false)),
      'split_four'          => new sfValidatorNumber(array('required' => false)),
      'split_five'          => new sfValidatorNumber(array('required' => false)),
      'split_six'           => new sfValidatorNumber(array('required' => false)),
      'split_seven'         => new sfValidatorNumber(array('required' => false)),
      'split_eight'         => new sfValidatorNumber(array('required' => false)),
      'split_nine'          => new sfValidatorNumber(array('required' => false)),
      'split_ten'           => new sfValidatorNumber(array('required' => false)),
      'split_eleven'        => new sfValidatorNumber(array('required' => false)),
      'split_twelve'        => new sfValidatorNumber(array('required' => false)),
      'split_thirteen'      => new sfValidatorNumber(array('required' => false)),
      'split_fourteen'      => new sfValidatorNumber(array('required' => false)),
      'split_fifteen'       => new sfValidatorNumber(array('required' => false)),
      'split_sixteen'       => new sfValidatorNumber(array('required' => false)),
      'split_seventeen'     => new sfValidatorNumber(array('required' => false)),
      'split_eightteen'     => new sfValidatorNumber(array('required' => false)),
      'split_nineteen'      => new sfValidatorNumber(array('required' => false)),
      'split_twenty'        => new sfValidatorNumber(array('required' => false)),
      'is_processed'        => new sfValidatorInteger(array('required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'deleted_at'          => new sfValidatorDateTime(array('required' => false)),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('merchant_item_split[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantItemSplit';
  }

}
