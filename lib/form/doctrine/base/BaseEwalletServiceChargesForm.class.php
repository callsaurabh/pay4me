<?php

/**
 * EwalletServiceCharges form base class.
 *
 * @method EwalletServiceCharges getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEwalletServiceChargesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => true)),
      'charge_amount'          => new sfWidgetFormInputText(),
      'charge_type'            => new sfWidgetFormChoice(array('choices' => array('percent' => 'percent', 'flat' => 'flat'))),
      'upper_slab'             => new sfWidgetFormInputText(),
      'lower_slab'             => new sfWidgetFormInputText(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'deleted_at'             => new sfWidgetFormDateTime(),
      'created_by'             => new sfWidgetFormInputText(),
      'updated_by'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'required' => false)),
      'charge_amount'          => new sfValidatorNumber(array('required' => false)),
      'charge_type'            => new sfValidatorChoice(array('choices' => array(0 => 'percent', 1 => 'flat'), 'required' => false)),
      'upper_slab'             => new sfValidatorNumber(array('required' => false)),
      'lower_slab'             => new sfValidatorNumber(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
      'deleted_at'             => new sfValidatorDateTime(array('required' => false)),
      'created_by'             => new sfValidatorInteger(array('required' => false)),
      'updated_by'             => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ewallet_service_charges[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EwalletServiceCharges';
  }

}
