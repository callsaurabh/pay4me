<?php

/**
 * MerchantAcctReconcilationHistory form base class.
 *
 * @method MerchantAcctReconcilationHistory getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMerchantAcctReconcilationHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'schedule_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantAcctReconcilationSchedule'), 'add_empty' => false)),
      'swap_date'   => new sfWidgetFormDateTime(),
      'batch_ids'   => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
      'created_by'  => new sfWidgetFormInputText(),
      'updated_by'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'schedule_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantAcctReconcilationSchedule'))),
      'swap_date'   => new sfValidatorDateTime(),
      'batch_ids'   => new sfValidatorString(array('max_length' => 100)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
      'created_by'  => new sfValidatorInteger(array('required' => false)),
      'updated_by'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('merchant_acct_reconcilation_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantAcctReconcilationHistory';
  }

}
