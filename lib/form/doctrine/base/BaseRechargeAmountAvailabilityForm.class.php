<?php

/**
 * RechargeAmountAvailability form base class.
 *
 * @method RechargeAmountAvailability getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseRechargeAmountAvailabilityForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'master_account_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => false)),
      'total_amount_paid'      => new sfWidgetFormInputText(),
      'amount_wallet'          => new sfWidgetFormInputText(),
      'service_charges'        => new sfWidgetFormInputText(),
      'date_available_on'      => new sfWidgetFormDateTime(),
      'is_credited'            => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'master_account_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'))),
      'total_amount_paid'      => new sfValidatorInteger(),
      'amount_wallet'          => new sfValidatorInteger(),
      'service_charges'        => new sfValidatorInteger(),
      'date_available_on'      => new sfValidatorDateTime(),
      'is_credited'            => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'))),
    ));

    $this->widgetSchema->setNameFormat('recharge_amount_availability[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RechargeAmountAvailability';
  }

}
