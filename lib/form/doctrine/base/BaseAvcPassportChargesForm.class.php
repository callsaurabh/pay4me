<?php

/**
 * AvcPassportCharges form base class.
 *
 * @method AvcPassportCharges getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAvcPassportChargesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'transaction_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Transaction'), 'add_empty' => false)),
      'application_id' => new sfWidgetFormInputText(),
      'merchant_request_id' => new sfWidgetFormInputText(),
      'amount'         => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
//      'deleted_at'     => new sfWidgetFormDateTime(),
//      'created_by'     => new sfWidgetFormInputText(),
//      'updated_by'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'transaction_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Transaction'))),
      'application_id' => new sfValidatorInteger(),
      'merchant_request_id' => new sfValidatorInteger(),
      'amount'         => new sfValidatorNumber(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
//      'deleted_at'     => new sfValidatorDateTime(array('required' => false)),
//      'created_by'     => new sfValidatorInteger(array('required' => false)),
//      'updated_by'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('avc_passport_charges[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AvcPassportCharges';
  }

}
