<?php

/**
 * BankPaymentScheduler form base class.
 *
 * @method BankPaymentScheduler getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBankPaymentSchedulerForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'bank_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => false)),
      'notification_schedule' => new sfWidgetFormTextarea(),
      'payment_schedule'      => new sfWidgetFormTextarea(),
      'bank_key'              => new sfWidgetFormInputText(),
      'bank_code'             => new sfWidgetFormInputText(),
      'notification_url'      => new sfWidgetFormInputText(),
      'notification_status'   => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'payment_status'        => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInputText(),
      'updated_by'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bank_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'))),
      'notification_schedule' => new sfValidatorString(array('required' => false)),
      'payment_schedule'      => new sfValidatorString(array('required' => false)),
      'bank_key'              => new sfValidatorString(array('max_length' => 255)),
      'bank_code'             => new sfValidatorInteger(),
      'notification_url'      => new sfValidatorString(array('max_length' => 255)),
      'notification_status'   => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'payment_status'        => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
      'created_by'            => new sfValidatorInteger(array('required' => false)),
      'updated_by'            => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bank_payment_scheduler[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BankPaymentScheduler';
  }

}
