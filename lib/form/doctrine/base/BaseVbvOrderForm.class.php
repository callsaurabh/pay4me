<?php

/**
 * VbvOrder form base class.
 *
 * @method VbvOrder getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVbvOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'order_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'), 'add_empty' => false)),
      'app_id'   => new sfWidgetFormInputText(),
      'type'     => new sfWidgetFormChoice(array('choices' => array('pay' => 'pay', 'recharge' => 'recharge'))),
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'order_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'))),
      'app_id'   => new sfValidatorString(array('max_length' => 100)),
      'type'     => new sfValidatorChoice(array('choices' => array(0 => 'pay', 1 => 'recharge'))),
    ));

    $this->widgetSchema->setNameFormat('vbv_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VbvOrder';
  }

}
