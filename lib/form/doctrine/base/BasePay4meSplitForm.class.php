<?php

/**
 * Pay4meSplit form base class.
 *
 * @method Pay4meSplit getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePay4meSplitForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                          => new sfWidgetFormInputHidden(),
      'merchant_request_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => false)),
      'merchant_request_payment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequestPaymentDetails'), 'add_empty' => false)),
      'account_number'              => new sfWidgetFormInputText(),
      'amount'                      => new sfWidgetFormInputText(),
      'created_at'                  => new sfWidgetFormDateTime(),
      'updated_at'                  => new sfWidgetFormDateTime(),
      'deleted_at'                  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_request_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'))),
      'merchant_request_payment_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequestPaymentDetails'))),
      'account_number'              => new sfValidatorString(array('max_length' => 40)),
      'amount'                      => new sfValidatorInteger(),
      'created_at'                  => new sfValidatorDateTime(),
      'updated_at'                  => new sfValidatorDateTime(),
      'deleted_at'                  => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('pay4me_split[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Pay4meSplit';
  }

}
