<?php

/**
 * MerchantRequestDetails form base class.
 *
 * @method MerchantRequestDetails getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMerchantRequestDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'merchant_item_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'), 'add_empty' => false)),
      'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => false)),
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => false)),
      'iparam_one'          => new sfWidgetFormInputText(),
      'iparam_two'          => new sfWidgetFormInputText(),
      'iparam_three'        => new sfWidgetFormInputText(),
      'iparam_four'         => new sfWidgetFormInputText(),
      'iparam_five'         => new sfWidgetFormInputText(),
      'iparam_six'          => new sfWidgetFormInputText(),
      'iparam_seven'        => new sfWidgetFormInputText(),
      'iparam_eight'        => new sfWidgetFormInputText(),
      'iparam_nine'         => new sfWidgetFormInputText(),
      'iparam_ten'          => new sfWidgetFormInputText(),
      'name'                => new sfWidgetFormInputText(),
      'sparam_one'          => new sfWidgetFormInputText(),
      'sparam_two'          => new sfWidgetFormInputText(),
      'sparam_three'        => new sfWidgetFormInputText(),
      'sparam_four'         => new sfWidgetFormInputText(),
      'sparam_five'         => new sfWidgetFormInputText(),
      'sparam_six'          => new sfWidgetFormInputText(),
      'sparam_seven'        => new sfWidgetFormInputText(),
      'sparam_eight'        => new sfWidgetFormInputText(),
      'sparam_nine'         => new sfWidgetFormInputText(),
      'sparam_ten'          => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'deleted_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_item_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'))),
      'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'))),
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'))),
      'iparam_one'          => new sfValidatorInteger(array('required' => false)),
      'iparam_two'          => new sfValidatorInteger(array('required' => false)),
      'iparam_three'        => new sfValidatorInteger(array('required' => false)),
      'iparam_four'         => new sfValidatorInteger(array('required' => false)),
      'iparam_five'         => new sfValidatorInteger(array('required' => false)),
      'iparam_six'          => new sfValidatorInteger(array('required' => false)),
      'iparam_seven'        => new sfValidatorInteger(array('required' => false)),
      'iparam_eight'        => new sfValidatorInteger(array('required' => false)),
      'iparam_nine'         => new sfValidatorInteger(array('required' => false)),
      'iparam_ten'          => new sfValidatorInteger(array('required' => false)),
      'name'                => new sfValidatorString(array('max_length' => 100)),
      'sparam_one'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_two'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_three'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_four'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_five'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_six'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_seven'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_eight'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_nine'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sparam_ten'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'deleted_at'          => new sfValidatorDateTime(array('required' => false)),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('merchant_request_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantRequestDetails';
  }

}
