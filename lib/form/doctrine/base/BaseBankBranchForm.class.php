<?php

/**
 * BankBranch form base class.
 *
 * @method BankBranch getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBankBranchForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'bank_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => false)),
      'name'            => new sfWidgetFormInputText(),
      'address'         => new sfWidgetFormTextarea(),
      'branch_code'     => new sfWidgetFormInputText(),
      'branch_sortcode' => new sfWidgetFormInputText(),
      'lga_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Lga'), 'add_empty' => true)),
      'country_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => false)),
      'state_name'      => new sfWidgetFormInputText(),
      'lga_name'        => new sfWidgetFormInputText(),
      'ip_address'      => new sfWidgetFormInputText(),
      'status'          => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'deleted_at'      => new sfWidgetFormDateTime(),
      'created_by'      => new sfWidgetFormInputText(),
      'updated_by'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bank_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'))),
      'name'            => new sfValidatorString(array('max_length' => 255)),
      'address'         => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'branch_code'     => new sfValidatorString(array('max_length' => 100)),
      'branch_sortcode' => new sfValidatorString(array('max_length' => 100)),
      'lga_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Lga'), 'required' => false)),
      'country_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'))),
      'state_name'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'lga_name'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'ip_address'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'status'          => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'deleted_at'      => new sfValidatorDateTime(array('required' => false)),
      'created_by'      => new sfValidatorInteger(array('required' => false)),
      'updated_by'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'BankBranch', 'column' => array('bank_id', 'branch_code')))
    );

    $this->widgetSchema->setNameFormat('bank_branch[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BankBranch';
  }

}
