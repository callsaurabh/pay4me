<?php

/**
 * ValidationRules form base class.
 *
 * @method ValidationRules getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseValidationRulesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => false)),
      'param_name'          => new sfWidgetFormInputText(),
      'param_description'   => new sfWidgetFormTextarea(),
      'is_mandatory'        => new sfWidgetFormInputCheckbox(),
      'param_type'          => new sfWidgetFormChoice(array('choices' => array('integer' => 'integer', 'string' => 'string'))),
      'mapped_to'           => new sfWidgetFormChoice(array('choices' => array('iparam_one' => 'iparam_one', 'iparam_two' => 'iparam_two', 'iparam_three' => 'iparam_three', 'iparam_four' => 'iparam_four', 'iparam_five' => 'iparam_five', 'iparam_six' => 'iparam_six', 'iparam_seven' => 'iparam_seven', 'iparam_eight' => 'iparam_eight', 'iparam_nine' => 'iparam_nine', 'iparam_ten' => 'iparam_ten', 'sparam_one' => 'sparam_one', 'sparam_two' => 'sparam_two', 'sparam_three' => 'sparam_three', 'sparam_four' => 'sparam_four', 'sparam_five' => 'sparam_five', 'sparam_six' => 'sparam_six', 'sparam_seven' => 'sparam_seven', 'sparam_eight' => 'sparam_eight', 'sparam_nine' => 'sparam_nine', 'sparam_ten' => 'sparam_ten'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'deleted_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'))),
      'param_name'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'param_description'   => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'is_mandatory'        => new sfValidatorBoolean(array('required' => false)),
      'param_type'          => new sfValidatorChoice(array('choices' => array(0 => 'integer', 1 => 'string'), 'required' => false)),
      'mapped_to'           => new sfValidatorChoice(array('choices' => array(0 => 'iparam_one', 1 => 'iparam_two', 2 => 'iparam_three', 3 => 'iparam_four', 4 => 'iparam_five', 5 => 'iparam_six', 6 => 'iparam_seven', 7 => 'iparam_eight', 8 => 'iparam_nine', 9 => 'iparam_ten', 10 => 'sparam_one', 11 => 'sparam_two', 12 => 'sparam_three', 13 => 'sparam_four', 14 => 'sparam_five', 15 => 'sparam_six', 16 => 'sparam_seven', 17 => 'sparam_eight', 18 => 'sparam_nine', 19 => 'sparam_ten'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'deleted_at'          => new sfValidatorDateTime(array('required' => false)),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('validation_rules[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ValidationRules';
  }

}
