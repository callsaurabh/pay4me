<?php

/**
 * MerchantService form base class.
 *
 * @method MerchantService getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMerchantServiceForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'merchant_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
      'name'               => new sfWidgetFormInputText(),
      'notification_url'   => new sfWidgetFormInputText(),
      'merchant_home_page' => new sfWidgetFormInputText(),
      'version'            => new sfWidgetFormInputText(),
      'clubbed'            => new sfWidgetFormChoice(array('choices' => array('no' => 'no', 'yes' => 'yes'))),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'deleted_at'         => new sfWidgetFormDateTime(),
      'created_by'         => new sfWidgetFormInputText(),
      'updated_by'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
      'name'               => new sfValidatorString(array('max_length' => 255)),
      'notification_url'   => new sfValidatorString(array('max_length' => 255)),
      'merchant_home_page' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'version'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'clubbed'            => new sfValidatorChoice(array('choices' => array(0 => 'no', 1 => 'yes'), 'required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
      'deleted_at'         => new sfValidatorDateTime(array('required' => false)),
      'created_by'         => new sfValidatorInteger(array('required' => false)),
      'updated_by'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('merchant_service[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantService';
  }

}
