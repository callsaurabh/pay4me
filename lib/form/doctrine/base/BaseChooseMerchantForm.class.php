<?php

/**
 * MerchantService form base class.
 *
 * @package    form
 * @subpackage merchant_service
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseChooseMerchantForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'merchant_id'        => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => false)),      
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'column' => 'id', 'required' => false)),
      'merchant_id'        => new sfValidatorDoctrineChoice(array('model' => 'Merchant')),      
    ));

    $this->widgetSchema->setNameFormat('merchant[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Merchant';
  }

}
