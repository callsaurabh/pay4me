<?php

/**
 * PaymentSplitDetails form base class.
 *
 * @method PaymentSplitDetails getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePaymentSplitDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'merchant_request_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => false)),
      'from_account'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountFrom'), 'add_empty' => false)),
      'to_account'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountTo'), 'add_empty' => false)),
      'amount'               => new sfWidgetFormInputText(),
      'split_date'           => new sfWidgetFormDateTime(),
      'reconciliation_state' => new sfWidgetFormChoice(array('choices' => array('new' => 'new', 'sent_to_nibss' => 'sent_to_nibss'))),
      'payment_batch_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentBatch'), 'add_empty' => true)),
      'paid_by'              => new sfWidgetFormInputText(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
      'created_by'           => new sfWidgetFormInputText(),
      'updated_by'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_request_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'))),
      'from_account'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountFrom'))),
      'to_account'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountTo'))),
      'amount'               => new sfValidatorInteger(),
      'split_date'           => new sfValidatorDateTime(),
      'reconciliation_state' => new sfValidatorChoice(array('choices' => array(0 => 'new', 1 => 'sent_to_nibss'), 'required' => false)),
      'payment_batch_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentBatch'), 'required' => false)),
      'paid_by'              => new sfValidatorInteger(array('required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
      'created_by'           => new sfValidatorInteger(array('required' => false)),
      'updated_by'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_split_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentSplitDetails';
  }

}
