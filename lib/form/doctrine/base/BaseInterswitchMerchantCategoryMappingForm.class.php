<?php

/**
 * InterswitchMerchantCategoryMapping form base class.
 *
 * @method InterswitchMerchantCategoryMapping getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInterswitchMerchantCategoryMappingForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'merchant_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
      'interswitch_category_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('InterswitchCategory'), 'add_empty' => false)),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'merchant_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
      'interswitch_category_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('InterswitchCategory'))),
      'created_at'              => new sfValidatorDateTime(),
      'updated_at'              => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'InterswitchMerchantCategoryMapping', 'column' => array('merchant_id')))
    );

    $this->widgetSchema->setNameFormat('interswitch_merchant_category_mapping[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InterswitchMerchantCategoryMapping';
  }

}
