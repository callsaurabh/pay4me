<?php

/**
 * MerchantPaymentTransaction form base class.
 *
 * @method MerchantPaymentTransaction getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMerchantPaymentTransactionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'validation_number'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterLedger'), 'add_empty' => true)),
      'merchant_request_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => false)),
      'merchant_request_unique_id' => new sfWidgetFormInputText(),
      'is_processed'               => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1', 2 => '2'))),
      'transaction_number'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Transaction'), 'add_empty' => false)),
      'created_at'                 => new sfWidgetFormDateTime(),
      'updated_at'                 => new sfWidgetFormDateTime(),
      'created_by'                 => new sfWidgetFormInputText(),
      'updated_by'                 => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'validation_number'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterLedger'), 'required' => false)),
      'merchant_request_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'))),
      'merchant_request_unique_id' => new sfValidatorInteger(),
      'is_processed'               => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1', 2 => '2'), 'required' => false)),
      'transaction_number'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Transaction'))),
      'created_at'                 => new sfValidatorDateTime(),
      'updated_at'                 => new sfValidatorDateTime(),
      'created_by'                 => new sfValidatorInteger(array('required' => false)),
      'updated_by'                 => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'MerchantPaymentTransaction', 'column' => array('merchant_request_unique_id')))
    );

    $this->widgetSchema->setNameFormat('merchant_payment_transaction[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantPaymentTransaction';
  }

}
