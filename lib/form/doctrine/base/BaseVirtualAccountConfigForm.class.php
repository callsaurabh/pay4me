<?php

/**
 * VirtualAccountConfig form base class.
 *
 * @method VirtualAccountConfig getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVirtualAccountConfigForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'virtual_account_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountVirtual'), 'add_empty' => false)),
      'physical_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountPhysical'), 'add_empty' => false)),
      'payment_flag'        => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'virtual_account_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountVirtual'))),
      'physical_account_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountPhysical'))),
      'payment_flag'        => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('virtual_account_config[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VirtualAccountConfig';
  }

}
