<?php

/**
 * VwGoStatus form base class.
 *
 * @method VwGoStatus getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVwGoStatusForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('GatewayOrder'), 'add_empty' => false)),
      'app_id' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('GatewayOrder'))),
      'app_id' => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('vw_go_status[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VwGoStatus';
  }

}
