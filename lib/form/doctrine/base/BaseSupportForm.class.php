<?php

/**
 * MerchantService form base class.
 *
 * @package    form
 * @subpackage merchant_service
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseSupportForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'merchant'        => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => false)),
      'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => 'MerchantService', 'add_empty' => false))
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
      'merchant'        => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => true)),
      'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'required' => true)),
    ));

    $this->widgetSchema->setNameFormat('support[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ServicePaymentModeOption';
  }

}
