<?php

/**
 * NoOfValidRules form base class.
 *
 * @package    form
 * @subpackage NoOfValidRules
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseNoOfValidRulesForm extends BaseFormDoctrine
{
  public function setup()
  {


    $this->setWidgets(array(
      'no_of_rules'        => new sfWidgetFormInputText()
    ));

    $this->setValidators(array(
      //'no_of_rules'   => new sfValidatorInteger(array('required' => true))
              'no_of_rules'        => new sfValidatorDoctrineChoice(array('model' => 'ValidationRules'))
    ));

    $this->widgetSchema->setNameFormat('noOfValidRules[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ValidationRules';
  }

}
