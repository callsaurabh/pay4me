<?php

/**
 * MwMessageLog form base class.
 *
 * @method MwMessageLog getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMwMessageLogForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'request_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MwRequest'), 'add_empty' => true)),
      'log_message' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'request_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MwRequest'), 'required' => false)),
      'log_message' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('mw_message_log[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MwMessageLog';
  }

}
