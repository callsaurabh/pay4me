<?php

/**
 * MwTransactionRequest form base class.
 *
 * @method MwTransactionRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMwTransactionRequestForm extends MwRequestForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('mw_transaction_request[%s]');
  }

  public function getModelName()
  {
    return 'MwTransactionRequest';
  }

}
