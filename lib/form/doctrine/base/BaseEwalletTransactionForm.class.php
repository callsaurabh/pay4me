<?php

/**
 * EwalletTransaction form base class.
 *
 * @method EwalletTransaction getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEwalletTransactionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'from_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountFrom'), 'add_empty' => false)),
      'to_account_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountTo'), 'add_empty' => false)),
      'amount'          => new sfWidgetFormInputText(),
      'requested_on'    => new sfWidgetFormDateTime(),
      'status'          => new sfWidgetFormChoice(array('choices' => array('pending' => 'pending', 'transfer' => 'transfer', 'rejected' => 'rejected'))),
      'description'     => new sfWidgetFormInputText(),
      'type'            => new sfWidgetFormChoice(array('choices' => array('direct' => 'direct', 'transfer' => 'transfer'))),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'created_by'      => new sfWidgetFormInputText(),
      'updated_by'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'from_account_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountFrom'))),
      'to_account_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountTo'))),
      'amount'          => new sfValidatorInteger(),
      'requested_on'    => new sfValidatorDateTime(),
      'status'          => new sfValidatorChoice(array('choices' => array(0 => 'pending', 1 => 'transfer', 2 => 'rejected'))),
      'description'     => new sfValidatorString(array('max_length' => 40)),
      'type'            => new sfValidatorChoice(array('choices' => array(0 => 'direct', 1 => 'transfer'))),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'created_by'      => new sfValidatorInteger(array('required' => false)),
      'updated_by'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ewallet_transaction[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EwalletTransaction';
  }

}
