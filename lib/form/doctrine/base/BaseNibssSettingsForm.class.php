<?php

/**
 * NibssSettings form base class.
 *
 * @method NibssSettings getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseNibssSettingsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'filename'               => new sfWidgetFormTextarea(),
      'message_from'           => new sfWidgetFormTextarea(),
      'message_to'             => new sfWidgetFormTextarea(),
      'message_cc'             => new sfWidgetFormTextarea(),
      'message_subject'        => new sfWidgetFormInputText(),
      'payor'                  => new sfWidgetFormTextarea(),
      'message_from_name'      => new sfWidgetFormTextarea(),
      'outboundserver_address' => new sfWidgetFormTextarea(),
      'outboundserver_port'    => new sfWidgetFormTextarea(),
      'username'               => new sfWidgetFormTextarea(),
      'password'               => new sfWidgetFormTextarea(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'deleted_at'             => new sfWidgetFormDateTime(),
      'created_by'             => new sfWidgetFormInputText(),
      'updated_by'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'filename'               => new sfValidatorString(array('required' => false)),
      'message_from'           => new sfValidatorString(array('required' => false)),
      'message_to'             => new sfValidatorString(array('required' => false)),
      'message_cc'             => new sfValidatorString(array('required' => false)),
      'message_subject'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'payor'                  => new sfValidatorString(array('required' => false)),
      'message_from_name'      => new sfValidatorString(array('required' => false)),
      'outboundserver_address' => new sfValidatorString(array('required' => false)),
      'outboundserver_port'    => new sfValidatorString(array('required' => false)),
      'username'               => new sfValidatorString(array('required' => false)),
      'password'               => new sfValidatorString(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
      'deleted_at'             => new sfValidatorDateTime(array('required' => false)),
      'created_by'             => new sfValidatorInteger(array('required' => false)),
      'updated_by'             => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('nibss_settings[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'NibssSettings';
  }

}
