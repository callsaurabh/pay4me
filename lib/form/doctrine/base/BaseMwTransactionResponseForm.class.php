<?php

/**
 * MwTransactionResponse form base class.
 *
 * @method MwTransactionResponse getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMwTransactionResponseForm extends MwResponseForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('mw_transaction_response[%s]');
  }

  public function getModelName()
  {
    return 'MwTransactionResponse';
  }

}
