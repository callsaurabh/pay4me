<?php

/**
 * PaymentRecord form base class.
 *
 * @method PaymentRecord getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePaymentRecordForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'payment_batch_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentBatch'), 'add_empty' => true)),
      'account_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'account_no'       => new sfWidgetFormInputText(),
      'sortcode'         => new sfWidgetFormInputText(),
      'amount'           => new sfWidgetFormInputText(),
      'account_name'     => new sfWidgetFormTextarea(),
      'payor'            => new sfWidgetFormTextarea(),
      'narration'        => new sfWidgetFormInputText(),
      'status'           => new sfWidgetFormInputText(),
      'paid_date'        => new sfWidgetFormInputText(),
      'validity_date'    => new sfWidgetFormInputText(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'deleted_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInputText(),
      'updated_by'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'payment_batch_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentBatch'), 'required' => false)),
      'account_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'required' => false)),
      'account_no'       => new sfValidatorString(array('max_length' => 100)),
      'sortcode'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'amount'           => new sfValidatorInteger(),
      'account_name'     => new sfValidatorString(),
      'payor'            => new sfValidatorString(array('required' => false)),
      'narration'        => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'status'           => new sfValidatorInteger(array('required' => false)),
      'paid_date'        => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'validity_date'    => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'deleted_at'       => new sfValidatorDateTime(array('required' => false)),
      'created_by'       => new sfValidatorInteger(array('required' => false)),
      'updated_by'       => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_record[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentRecord';
  }

}
