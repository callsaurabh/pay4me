<?php

/**
 * Bank form base class.
 *
 * @method Bank getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBankForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'bank_name'           => new sfWidgetFormInputText(),
      'acronym'             => new sfWidgetFormInputText(),
      'status'              => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'bank_code'           => new sfWidgetFormInputText(),
      'bank_key'            => new sfWidgetFormInputText(),
      'auth_info'           => new sfWidgetFormInputText(),
      'bi_enabled'          => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'bi_logger_level'     => new sfWidgetFormChoice(array('choices' => array('WARN' => 'WARN', 'INFO' => 'INFO', 'DEBUG' => 'DEBUG', 'FATAL' => 'FATAL', 'ERROR' => 'ERROR'))),
      'bi_protocol_version' => new sfWidgetFormChoice(array('choices' => array('V1' => 'V1', 'v2' => 'v2'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'deleted_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bank_name'           => new sfValidatorString(array('max_length' => 255)),
      'acronym'             => new sfValidatorString(array('max_length' => 100)),
      'status'              => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'bank_code'           => new sfValidatorInteger(array('required' => false)),
      'bank_key'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'auth_info'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'bi_enabled'          => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'bi_logger_level'     => new sfValidatorChoice(array('choices' => array(0 => 'WARN', 1 => 'INFO', 2 => 'DEBUG', 3 => 'FATAL', 4 => 'ERROR'), 'required' => false)),
      'bi_protocol_version' => new sfValidatorChoice(array('choices' => array(0 => 'V1', 1 => 'v2'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'deleted_at'          => new sfValidatorDateTime(array('required' => false)),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Bank', 'column' => array('acronym')))
    );

    $this->widgetSchema->setNameFormat('bank[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bank';
  }

}
