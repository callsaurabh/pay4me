<?php

/**
 * SplitAccountConfiguration form base class.
 *
 * @method SplitAccountConfiguration getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSplitAccountConfigurationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'account_party'     => new sfWidgetFormInputText(),
      'account_name'      => new sfWidgetFormInputText(),
      'account_number'    => new sfWidgetFormInputText(),
      'sortcode'          => new sfWidgetFormInputText(),
      'bank_name'         => new sfWidgetFormInputText(),
      'master_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'currency_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => false)),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'deleted_at'        => new sfWidgetFormDateTime(),
      'created_by'        => new sfWidgetFormInputText(),
      'updated_by'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'account_party'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'account_name'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'account_number'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'sortcode'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'bank_name'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'master_account_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'required' => false)),
      'currency_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'required' => false)),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
      'deleted_at'        => new sfValidatorDateTime(array('required' => false)),
      'created_by'        => new sfValidatorInteger(array('required' => false)),
      'updated_by'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('split_account_configuration[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SplitAccountConfiguration';
  }

}
