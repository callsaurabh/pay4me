<?php

/**
 * PaymentBatch form base class.
 *
 * @method PaymentBatch getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePaymentBatchForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'payment_forwarded_on' => new sfWidgetFormDate(),
      'payment_file'         => new sfWidgetFormTextarea(),
      'total_amount'         => new sfWidgetFormInputText(),
      'total_record'         => new sfWidgetFormInputText(),
      'status'               => new sfWidgetFormInputText(),
      'account_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'account_no'           => new sfWidgetFormTextarea(),
      'sortcode'             => new sfWidgetFormInputText(),
      'from_bank'            => new sfWidgetFormInputText(),
      'account_name'         => new sfWidgetFormTextarea(),
      'payor'                => new sfWidgetFormTextarea(),
      'narration'            => new sfWidgetFormInputText(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
      'deleted_at'           => new sfWidgetFormDateTime(),
      'created_by'           => new sfWidgetFormInputText(),
      'updated_by'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'payment_forwarded_on' => new sfValidatorDate(array('required' => false)),
      'payment_file'         => new sfValidatorString(array('required' => false)),
      'total_amount'         => new sfValidatorInteger(array('required' => false)),
      'total_record'         => new sfValidatorInteger(array('required' => false)),
      'status'               => new sfValidatorInteger(array('required' => false)),
      'account_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'required' => false)),
      'account_no'           => new sfValidatorString(array('required' => false)),
      'sortcode'             => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'from_bank'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'account_name'         => new sfValidatorString(array('required' => false)),
      'payor'                => new sfValidatorString(array('required' => false)),
      'narration'            => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
      'deleted_at'           => new sfValidatorDateTime(array('required' => false)),
      'created_by'           => new sfValidatorInteger(array('required' => false)),
      'updated_by'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_batch[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentBatch';
  }

}
