<?php
class changePinForm extends BaseEwalletPinForm
{

    public static function userVerifyPasswordCallBack($validator, $value, $arguments) {

        //        $userId=sfContext::getInstance()->getUser()->getGuardUser()->getId();
        //        $ewalletDetail= Doctrine::getTable('EwalletPin')->findByUserId(array($userId))->getFirst();
        $pinObj=new pin();
        $ewalletDetail=$pinObj->getEwalletDetail();
        $pin= $ewalletDetail->getPinNumber();
        if($pinObj->getEncryptedPin($value)!=$pin){
            //      throw new sfValidatorError($validator, 'gyhghnch assword can not be same');
            throw new sfValidatorError($validator, 'invalid');
        }
        return $value;
    }
    public static function matchNewAndConfirmPinCallBack($validator, $value, $arguments){
        if(!preg_match('/[^\s+]/' ,$_REQUEST['ewallet_pin']['new_pin']) ){
            throw new sfValidatorError($validator, 'White spaces are not allowed');
        }
        if(strlen($_REQUEST['ewallet_pin']['new_pin']) < 4){
            throw new sfValidatorError($validator, 'Minimum 4 digits required');
        }
        if(strlen($_REQUEST['ewallet_pin']['new_pin']) > 4){
            throw new sfValidatorError($validator, 'Pin can not be more than 4 digits');
        }
        if($_REQUEST['ewallet_pin']['new_pin'] != $_REQUEST['ewallet_pin']['confirm_pin']){
            throw new sfValidatorError($validator, 'New Pin and Confirm Pin do not match');
        }
        return $value;

    }
    public function configure()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('I18N'));
        unset(
            $this['pin_number'],$this['forced_pin_change'],$this['user_id'], $this['no_of_retries'],$this['status'],$this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );
        $pinObj=new pin();
        $detail = $pinObj->getEwalletDetail();
        $status = $detail->getStatus();
        $forceChange=$detail->getForcedPinChange();

        if(($status != "inactive") || (($status == "inactive") && ($forceChange=='1')) ) {
//
//        $old_pin_required = $pinObj->isActive();
//        if($old_pin_required) {
          $this->widgetSchema['old_pin'] = new sfWidgetFormInputPassword(array(),array('maxlength'=>4,'title'=>'Current Pin'));
            $this->validatorSchema['old_pin'] = new sfValidatorCallback(array(
                'callback'  => 'changePinForm::userVerifyPasswordCallBack'));
        $this->validatorSchema['old_pin']->setOption('required',true);
        $this->validatorSchema['old_pin']->setMessage('required','Current Pin is required');
        $this->validatorSchema['old_pin']->setMessage('invalid','Current Pin is incorrect');


       }
        $this->widgetSchema['new_pin']= new sfWidgetFormInputPassword(array(),array('maxlength'=>4,'title'=>'New Pin'));
        $this->widgetSchema['confirm_pin']= new sfWidgetFormInputPassword(array(),array('maxlength'=>4,'title'=>'Confirm Pin'));

//        $this->validatorSchema['old_pin'] = new sfValidatorCallback(array(
//                'callback'  => 'changePinForm::userVerifyPasswordCallBack'));
//        $this->validatorSchema['old_pin']->setOption('required',true);
//        $this->validatorSchema['old_pin']->setMessage('required','Current Pin is required');
//        $this->validatorSchema['old_pin']->setMessage('invalid','Current pin is incorrect');


        $this->validatorSchema['new_pin'] = new sfValidatorAnd(array(
                new sfValidatorNumber(array('max' => 9999 ,'min' => 0000,'required' => true),
                    array('max' => 'Password can not be more than 4 digits','required' => 'Please enter your new Pin','min' => 'Pin can not be less than 4 digits')),
                new sfValidatorRegex(array('pattern' => '/[^\s+]/'),array(
               'invalid' => 'White spaces are not allowed.')),
                new sfValidatorCallback(array(
        'callback'  => 'changePinForm::matchNewAndConfirmPinCallBack'))
            )
            , array('halt_on_error' => true),
            array('required' => 'Please enter your new Pin')
        );
        
        $this->validatorSchema['confirm_pin'] = new sfValidatorString(array('required' => true),
            array('required' => 'Please re-enter your new Pin'));

        //$this->validatorSchema['new_pin'] = new sfValidatorCallback(array(
        //        'callback'  => 'changePinForm::matchNewAndConfirmPinCallBack'));

        //        $this->validatorSchema->setPostValidator(
        //            new sfValidatorSchemaCompare('new_pin', sfValidatorSchemaCompare::EQUAL, 'confirm_pin',
        //                array(),
        //                array('invalid' => 'New Pin and Confirm Pin do not match, please try again')
        //            ));
        // Set Labels
        $this->widgetSchema->setLabels(
            array(    'old_pin'    => __('Current Pin'),
      'new_pin'    => __('New Pin'),'confirm_pin'    => __('Confirm Pin')));
    }

}
?>
