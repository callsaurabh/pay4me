<?php

/**
 * ServiceBankConfiguration form.
 *
 * @package    form
 * @subpackage ServiceBankConfiguration
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ServiceBankConfigurationForm extends BaseServiceBankConfigurationForm
{
  public function configure()
  {
      
      unset($this['created_at'], $this['updated_at'], $this['deleted_at'], $this['created_by'], $this['updated_by']);

      $this->widgetSchema['merchant_id']->setOption('add_empty','-- Please Select Merchant --');
      $this->widgetSchema['payment_mode_option_id']->setOption('add_empty','-- Please Select Payment Mode --');
      $this->widgetSchema['bank_branch_id']->setOption('add_empty','-- Please Select Bank Branch --');
      $this->widgetSchema['account_number'] = new sfWidgetFormInputText();

      $this->widgetSchema->setLabels(array( 'merchant_id'                        => 'Merchant',
                                                                        'payment_mode_option_id'     => 'Payment Mode Option',
                                                                        'bank_branch_id'                    => 'Bank Branch',
                                                                        'account_number'                   => 'Account Number'));

      $this->setValidators(array(
          'id'         => new sfValidatorDoctrineChoice(array('model' => 'ServiceBankConfiguration', 'column' => 'id', 'required' => false)),
          'merchant_id' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => true)),
          'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('model' => 'PaymentModeOption', 'required' => true)),
          'bank_branch_id' => new sfValidatorDoctrineChoice(array('model' => 'BankBranch', 'required' => true)),
          'account_number'  => new sfValidatorString(array('max_length' => 100, 'required' => true))
            
            
      ));

      $this->widgetSchema->setNameFormat('svcBankConf[%s]');

  }
}