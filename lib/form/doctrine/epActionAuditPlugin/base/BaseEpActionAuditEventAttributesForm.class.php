<?php

/**
 * EpActionAuditEventAttributes form base class.
 *
 * @method EpActionAuditEventAttributes getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpActionAuditEventAttributesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'audit_event_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpActionAuditEvent'), 'add_empty' => true)),
      'name'           => new sfWidgetFormInputText(),
      'svalue'         => new sfWidgetFormInputText(),
      'ivalue'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'audit_event_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpActionAuditEvent'), 'required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'svalue'         => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'ivalue'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_action_audit_event_attributes[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpActionAuditEventAttributes';
  }

}
