<?php

/**
 * PaymentModeOption form.
 *
 * @package    form
 * @subpackage PaymentModeOption
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class PaymentModeOptionForm extends BasePaymentModeOptionForm
{
  public function configure()
  {
      unset(
            $this['created_at'], $this['updated_at'] ,$this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'payment_mode_id' => new sfWidgetFormDoctrineChoice(array('model' => 'PaymentMode', 'add_empty' => 'Please select Payment Mode')),
            'name' => new sfWidgetFormInputText(array(),array('maxlength'=>255, 'class'=>'FieldInput')),
        ));

        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'PaymentModeOption', 'column' => 'id', 'required' => false)),
            'payment_mode_id' => new sfValidatorDoctrineChoice(array('model' => 'PaymentMode', 'required' => true)),
            'name'        => new sfValidatorString(array('max_length' => 255, 'required' => true, 'trim' => true)),
        ));


        $this->widgetSchema->setLabels(array(
            'payment_mode_id'    => 'Payment Mode',
            'name' => 'Payment Mode Option',
        ));

        $this->widgetSchema->setNameFormat('payment_mode_option[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

              $this->validatorSchema->setPostValidator(
                        new sfValidatorAnd(array(
                               new sfValidatorCallback(array('callback' => array($this, 'checkDuplicacy'))),
                                  new sfValidatorDoctrineUnique(array(
                                        'model' => 'PaymentModeOption',
                                        'column' => array('name','payment_mode_id'),
                                        'primary_key' => 'id',
                                        'required' => true
                                ),array('invalid'=>'Payment Mode Option already exists')),
                        ))

                );
  }


    public function checkDuplicacy($validator, $values) {
    if ($values['name'] != "" ) {
      $val = Doctrine::getTable('PaymentModeOption')->getDeletedDetails($values['name'],$values['payment_mode_id']);//exit;
      if($val) {
        $error = new sfValidatorError($validator,'Cannot create Payment Mode Option, already deleted by administrator');
        throw new sfValidatorErrorSchema($validator,array('name' => $error));


      }
    }
    return $values;
  }
}