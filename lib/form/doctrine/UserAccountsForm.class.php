<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class UserAccountsForm extends sfForm
{
  public function configure()
  {
        $user_id = $this->getOption('user_id');

        $accountNumberArr = array();
        $accountObj = new UserAccountManager($user_id);
        $detail = $accountObj->getAccountForCurrency();
        foreach($detail as $val){
           $accountNumberArr[$val->getEpMasterAccount()->getId()] =  $val->getEpMasterAccount()->getAccountNumber();
        }


        $this->widgetSchema['from_account'] = new sfWidgetFormChoice(array('choices' => $accountNumberArr));

        $this->widgetSchema->setLabels(array(
            'from_account'=>'Select Account Number<font color=red><sup>*</sup></font>'
            ));

  }
}
?>
