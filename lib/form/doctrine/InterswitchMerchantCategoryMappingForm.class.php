<?php

/**
 * InterswitchMerchantCategoryMapping form.
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InterswitchMerchantCategoryMappingForm extends BaseInterswitchMerchantCategoryMappingForm {
  public function configure()
  {
      parent::setup();
      unset( $this['created_at'], $this['updated_at']);
      $this->setWidget('merchant_id', new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'label' => "Merchant"), array('invalid',"Invalid Merchant")));
      $this->setWidget('interswitch_category_id', new sfWidgetFormDoctrineChoice(array('model' => 'InterswitchCategory', 'add_empty' => false, 'label' => "Interswitch Category"), array('invalid',"Invalid Category")));
      $this->getWidget('merchant_id')->setAttributes(array('disabled'=>'disabled'));
  }


}
