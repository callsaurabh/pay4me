<?php

/**
 * BankBranch form.
 *
 * @package    form
 * @subpackage BankBranch
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BankCountryBranchForm extends BaseBankBranchForm
{
  public function configure()
  {
    unset(
          $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );


      $bankId = $this->getOption('bank');
      $countryId = $this->getOption('country');
      $branchId = $this->getOption('branch');
      $not_needed = $this->getOption('not_needed');
      $banklistoption = $this->getOption('ewalletreport')?$this->getOption('ewalletreport'):'';

      $guardObj = sfContext::getInstance()->getUser()->getGuardUser();
      $user_group = $guardObj->getGroupNames();
      $user_group_arr = array();
      //for
      foreach($user_group as $group){
          $user_group_arr[$group] = $group;
      }



      //print_r($user_group_arr);die;
      $bankColId='';
      $countryColId = '';
      $branchColId = '';
      $groupLevel =1;
      if(!(isset($user_group_arr[sfConfig::get('app_pfm_role_admin')])
            || isset($user_group_arr[sfConfig::get('app_pfm_role_report_admin')]) || isset($user_group_arr[sfConfig::get('app_pfm_role_report_portal_admin')]) ||
            isset($user_group_arr[sfConfig::get('app_pfm_role_ewallet_user')]) || isset($user_group_arr[sfConfig::get('app_pfm_role_merchant')])))
      {
          $bankColId = $guardObj->getBankUser()->getFirst()->getBankId();
           $groupLevel =2;
           if(!(isset($user_group_arr[sfConfig::get('app_pfm_role_bank_admin')])
                || isset($user_group_arr[sfConfig::get('app_pfm_role_report_bank_admin')])
                || isset($user_group_arr[sfConfig::get('app_pfm_role_bank_e_auditor')])
                ))
            {
                $countryColId = $guardObj->getBankUser()->getFirst()->getCountryId();
                $groupLevel=3;
                if(!(isset($user_group_arr[sfConfig::get('app_pfm_role_bank_country_head')])
                || isset($user_group_arr[sfConfig::get('app_pfm_role_country_report_user')])
                ))
                {
                    $branchColId = $guardObj->getBankUser()->getFirst()->getBankBranchId();
                    $groupLevel=4;

                }
            }
       }
      $widgetsArr=array();
      if(!empty($banklistoption))
      {
        $widgetsArr[$bankId]= new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => '--All Banks--', 'query'  => Doctrine::getTable('Bank')->getListEwallet($bankColId,'query')));

      }else{
        $widgetsArr[$bankId]= new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => '--All Banks--', 'query'  => Doctrine::getTable('Bank')->getList($bankColId,'query')));
      }

if('country'!=$not_needed)
      $widgetsArr[$countryId] = new sfWidgetFormChoice(array('choices' => array('' => '--All Country--')));
 if('branch'!=$not_needed)
      $widgetsArr[$branchId] = new sfWidgetFormChoice(array('choices'=>array('' => '--All Branches--')));

      $this->setWidgets($widgetsArr);

    if($groupLevel>1)
      {
          $this->widgetSchema[$bankId]->setOption('add_empty',false);
          $this->widgetSchema[$countryId] = new sfWidgetFormDoctrineChoice(array('model'=>'Country','add_empty' => '--All Country--','query'  => Doctrine::getTable('Country')->getAllCountry($bankColId,$countryColId,'query')));

      }
   if('country'!=$not_needed)
      {
        if($groupLevel>2)
           {
            $this->widgetSchema[$countryId]->setOption('add_empty',false);
            if('branch'!=$not_needed)
                 $this->widgetSchema[$branchId] = new sfWidgetFormDoctrineChoice(array('model'=>'BankBranch','add_empty' => '--All Branches--','query'  => Doctrine::getTable('BankBranch')->getAllBranches($bankColId,$countryColId,$branchColId,'query')));
           }

        if($groupLevel>3 && 'branch'!=$not_needed)
            $this->widgetSchema[$branchId]->setOption('add_empty',false);
      }



       $this->widgetSchema->setLabel($bankId, 'Bank');
       if('country'!=$not_needed)
            $this->widgetSchema->setLabel($countryId, 'Country');
       if('branch'!=$not_needed && 'country'!=$not_needed)
            $this->widgetSchema->setLabel($branchId, 'Branch');

       $this->validatorSchema[$bankId] = new sfValidatorString(array(),array('required' => 'Bank required'));
       if('country'!=$not_needed)
            $this->validatorSchema[$countryId] =new sfValidatorString(array(),array('required' => 'Country required'));
       if('branch'!=$not_needed && 'country'!=$not_needed)
            $this->validatorSchema[$branchId] = new sfValidatorString(array(),array('required' => 'Bank required'));                                                                                           //removed validation:'pattern' => '/^[a-zA-Z0-9 \. \- \,]*$/',


     }

}