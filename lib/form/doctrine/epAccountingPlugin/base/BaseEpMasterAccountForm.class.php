<?php

/**
 * EpMasterAccount form base class.
 *
 * @method EpMasterAccount getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpMasterAccountForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'clear_balance'   => new sfWidgetFormInputText(),
      'unclear_balance' => new sfWidgetFormInputText(),
      'unclear_amount'  => new sfWidgetFormInputText(),
      'account_number'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EwalletTransactionTrack'), 'add_empty' => false)),
      'account_name'    => new sfWidgetFormInputText(),
      'sortcode'        => new sfWidgetFormInputText(),
      'bankname'        => new sfWidgetFormInputText(),
      'type'            => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'created_by'      => new sfWidgetFormInputText(),
      'updated_by'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'clear_balance'   => new sfValidatorInteger(array('required' => false)),
      'unclear_balance' => new sfValidatorInteger(array('required' => false)),
      'unclear_amount'  => new sfValidatorInteger(array('required' => false)),
      'account_number'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EwalletTransactionTrack'))),
      'account_name'    => new sfValidatorString(array('max_length' => 100)),
      'sortcode'        => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'bankname'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'type'            => new sfValidatorString(array('max_length' => 20)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'created_by'      => new sfValidatorInteger(array('required' => false)),
      'updated_by'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_master_account[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpMasterAccount';
  }

}
