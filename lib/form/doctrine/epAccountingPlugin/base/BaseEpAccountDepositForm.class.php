<?php

/**
 * EpAccountDeposit form base class.
 *
 * @package    form
 * @subpackage ep_account_deposit
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 8508 2008-04-17 17:39:15Z fabien $
 */
class BaseEpAccountDepositForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'master_account_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EpMasterAccount', 'add_empty' => true)),
      'clear_amount'      => new sfWidgetFormInputText(),
      'unclear_amount'    => new sfWidgetFormInputText(),
      'status'            => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'created_by'        => new sfWidgetFormInputText(),
      'updated_by'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => 'EpAccountDeposit', 'column' => 'id', 'required' => false)),
      'master_account_id' => new sfValidatorDoctrineChoice(array('model' => 'EpMasterAccount', 'required' => false)),
      'clear_amount'      => new sfValidatorInteger(),
      'unclear_amount'    => new sfValidatorInteger(),
      'status'            => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
      'created_by'        => new sfValidatorInteger(array('required' => false)),
      'updated_by'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_account_deposit[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpAccountDeposit';
  }

}
