<?php

/**
 * Bank form.
 *
 * @package    form
 * @subpackage Bank
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CreateBankAdminForm extends BasesfGuardUserForm {
    static $groupArr = array('bank_country_head','country_report_user');
    public function configure() {
        unset(
            $this['created_at'], $this['updated_at'], $this['is_super_admin'], $this['last_login'], $this['deleted_at'], $this['is_active'], $this['password'], $this['salt'],$this['algorithm'], $this['groups_list'],$this['permissions_list']
        );
        $userType = $this->getOption('userType');
        $countryId = $this->getOption('country_id');
        
        $readOnly = false;
        $action = '';
        $bankId = '';
        $email = '';
        if($this->getOption('action') == 'edit'){
            $readOnly = true;
            $action = 'edit';
            $email = $this->getOption('email');
        }
        $bankId = $this->getOption('bankId');
        $this->widgetSchema['username']  = new sfWidgetFormInputText(array(),array('maxlength'=>30,'readonly'=>$readOnly,'class'=>'fieldinput1'));
        $this->widgetSchema['id']  = new sfWidgetFormInputHidden();

        $this->validatorSchema['username']   = new sfValidatorRegex(array('max_length' => 30, 'required' =>true , 'pattern'=>'/^[a-zA-Z0-9\.\-]*$/'),array('invalid'=>'Please enter a valid Username.','required'=>'Please enter Username'));
        
        

        $this->widgetSchema->setLabels(array(
            'username'    => 'Username',
            ));
        if(!$readOnly)
        $this->widgetSchema->setHelps(array(
            'username'       => '<div id=username_suffix></div><div id="temp_div"><br></div><br/><a href="javascript:;" onclick="validateUserName();" >Check Availability</a><div id="error_name" class="error"></div>',

            ));
        $userDetails = $this->getObject()->UserDetail[0];
        
        $checker = new BankUserDetailsForm($userDetails,array('action'=>$action,'bankId'=>$bankId,'email'=>$email,'userType'=>$userType,'countryId'=>$countryId));

        $this->embedForm('details', $checker);
        $bankUserObj = $this->getObject()->BankUser[0];
        $bankUser = new BankUserForm($bankUserObj,array('action'=>$action,'bankId'=>$bankId,'userType'=>$userType,'gruopArr'=>CreateBankAdminForm::$groupArr));
        unset($bankUser['user_id'],$bankUser['bank_branch_id']);

        $this->embedForm('bankform', $bankUser);
        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'checkUserName'))))));
    }
    public function checkUserName($validator,$values){
        if($values['bankform']['bank_id']){
        $bankname = Doctrine::getTable('Bank')->find($values['bankform']['bank_id']);

        $user_name = $values['username'].".".$bankname->getAcronym();        
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($user_name);
        if($username_count!= 0){
             //check if the username has been created and deleted earlier
            $getUserCount = Doctrine::getTable('sfGuardUser')->chkUserDeleted($user_name);
            //echo "<pre>"; print_r($getUserCount); die();
            if($getUserCount[0]['COUNT'] == '1')
            {               
                $error = new sfValidatorError($validator, "Username already created and deleted by administrator");
            }
            else
            {                
                $error = new sfValidatorError($validator, "User name already exist");
            }
            
            throw new sfValidatorErrorSchema($validator, array('username' => $error));
        }

        
        }return $values;
    }

protected function doSave($con = null)
  {
      if($this->getOption('action') != 'edit')
      {
        $bank_id = $this->values['bankform']['bank_id'] ;
        $bankname = Doctrine::getTable('Bank')->find($bank_id);
        $this->values['username'] = $this->values['username'].".".$bankname->getAcronym();
      }
    parent::doSave($con);
  }

    public function configureGroups() {
        $userType = $this->getOption('userType');
        $this->uiGroup = new Dlform();
        //$this->uiGroup->setLabel('Portal User');

        $newUserDetails = new FormBlock();
        $this->uiGroup->addElement($newUserDetails);
        
        if(in_array($userType,CreateBankAdminForm::$groupArr))
            $newUserDetails->addElement(array('bankform:bank_id','bankform:country_id','username','details:name','details:dob','details:address',
                'details:email','details:mobile_no'));
        else
            $newUserDetails->addElement(array('bankform:bank_id','username','details:name','details:dob','details:address',
                'details:email','details:mobile_no'));

    }

}
