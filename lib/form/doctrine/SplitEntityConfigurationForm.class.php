<?php

/**
 * SplitEntityConfiguration form.
 *
 * @package    form
 * @subpackage SplitEntityConfiguration
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SplitEntityConfigurationForm extends BaseSplitEntityConfigurationForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at'], $this['deleted_at'], $this['created_by'], $this['updated_by']);


    $this->widgetSchema['merchant_service_id']->setOption('add_empty','-- Please Select Merchant Service--');
    $this->widgetSchema['split_type_id']->setOption('add_empty','-- Please Select Split Type--');
    $this->widgetSchema['charge'] = new sfWidgetFormInputText();
    $this->widgetSchema['split_account_configuration_id']->setOption('add_empty','-- Please Select Split A/c Conf--');


    $this->setValidators(array(
                  'id'         => new sfValidatorDoctrineChoice(array('model' => 'SplitEntityConfiguration', 'column' => 'id', 'required' => false)),
                  'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'required' => true)),
                  'split_type_id' => new sfValidatorDoctrineChoice(array('model' => 'SplitType', 'required' => true)),
                  'split_account_configuration_id' => new sfValidatorDoctrineChoice(array('model' => 'SplitAccountConfiguration', 'required' => false)),
                  'merchant_item_split_column'     => new sfValidatorChoice(array('choices' => array('iparam_one' => 'iparam_one', 'iparam_two' => 'iparam_two', 'iparam_three' => 'iparam_three', 'iparam_four' => 'iparam_four', 'iparam_five' => 'iparam_five', 'iparam_six' => 'iparam_six', 'iparam_seven' => 'iparam_seven', 'iparam_eight' => 'iparam_eight', 'iparam_nine' => 'iparam_nine', 'iparam_ten' => 'iparam_ten', 'iparam_eleven' => 'iparam_eleven', 'iparam_twelve' => 'iparam_twelve', 'iparam_thirteen' => 'iparam_thirteen', 'iparam_fourteen' => 'iparam_fourteen', 'iparam_fifteen' => 'iparam_fifteen', 'iparam_seventeen' => 'iparam_seventeen', 'iparam_eightteen' => 'iparam_eightteen', 'iparam_nineteen' => 'iparam_nineteen', 'iparam_twenty' => 'iparam_twenty'), 'required' => false)),
      ));

    $this->widgetSchema->setLabels(array( 'merchant_service_id'                   => 'Merchant Service',
                                          'split_type_id'                         => 'Split Type',
                                          'charge'                                => 'Charges',
                                          'split_account_configuration_id'        => 'Split Account',));

    $this->validatorSchema['charge'] = new sfValidatorRegex(array('pattern'=> '/^[a-zA-Z0-9 \. \-]*$/', 'max_length'=> 50),
                                                                array('required'=>'Charge is required',
                                                                      'max_length' => 'The maximum length of charge should be 50 characters.',
                                                                      'invalid'=>'Invalid charge'));
    $this->widgetSchema->setNameFormat('splitEntityConf[%s]');
  }
}