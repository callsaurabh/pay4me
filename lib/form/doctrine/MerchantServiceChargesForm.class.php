<?php

/**
 * MerchantServiceCharges form.
 *
 * @package    form
 * @subpackage MerchantServiceCharges
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MerchantServiceChargesForm extends BaseMerchantServiceChargesForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at'], $this['deleted_at'], $this['created_by'], $this['updated_by']);

    $this->widgetSchema['merchant_service_id']->setOption('add_empty','-- Please Select Merchant Service--');
    $this->widgetSchema['service_charge_percent'] = new sfWidgetFormInputText();
    $this->widgetSchema['upper_slab'] = new sfWidgetFormInputText();
    $this->widgetSchema['lower_slab'] = new sfWidgetFormInputText();

    $this->setValidators(array(
                  'id'         => new sfValidatorDoctrineChoice(array('model' => 'MerchantServiceCharges', 'column' => 'id', 'required' => false)),
                  'merchant_service_id' => new sfValidatorDoctrineChoice(array('model' => 'SplitEntityConfiguration', 'column' => 'id', 'required' => true))));

    $this->widgetSchema->setLabels(array( 'merchant_service_id'                 => 'Merchant Service',
                                          'service_charge_percent'              => 'Service Charges (%)',
                                          'upper_slab'                          => 'Upper Slab',
                                          'lower_slab'                          => 'Lower Slab',));

    $this->validatorSchema['service_charge_percent'] = new sfValidatorRegex(array('pattern'=> '/^[0-9 \.]*$/', 'required' => true),
                                                                array('required'=>'Service Charge is required',                                                                      
                                                                      'invalid'=>'Invalid service charge'));
    $this->validatorSchema['upper_slab'] = new sfValidatorRegex(array('pattern'=> '/^[0-9 \.]*$/', 'required' => true),
                                                                array('required'=>'Upper slab is required',
                                                                      'invalid'=>'Invalid upper slab'));
    $this->validatorSchema['lower_slab'] = new sfValidatorRegex(array('pattern'=> '/^[0-9 \.]*$/', 'required' => true),
                                                                array('required'=>'Lower slab is required',
                                                                      'invalid'=>'Invalid lower slab'));


    $this->widgetSchema->setNameFormat('merchantSvcChg[%s]');
  }
}