<?php

class PaidByVisaReportForm extends PaymentModeOptionForm
{
  
  
 

  public function configure()
  {
    
    $optionsVals[''] = 'Please Select Status';
    $GORes = Doctrine::getTable('GatewayOrder')->getDistinctPaymentMode();

    //$optionsVals = array('all'=>'All','accepted'=>'Accepted','canceled'=>'Canceled','declined'=>'Declined');
       $payModeArr[''] = 'Please Select Payment Mode';
    foreach($GORes as $paymode)
    {
        $PayModeOption = Doctrine::getTable('PaymentModeOption')->find($paymode->getPmoId());
        $payModeArr[$paymode->getPmoId()]=$PayModeOption->getPaymentMode()->getDisplayName();
    }
    $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($user_id);
    $typeVals = array('payment'=>'Payment','recharge'=>'Recharge');

    // Production issue:[38516] Recharge option was not showing to Portal admin in credit card payment Report [05-JUNE-2013]
    $groups = sfContext::getInstance()->getUser()->getGroupName()->toArray();
    if(in_array(sfConfig::get('app_pfm_role_ewallet_user'),$groups)) {
        $billObj = billServiceFactory::getService();
        $permittedType = $billObj->permittedEwalletType($userDetailObj->getFirst()->getEwalletType());
        if(!$permittedType)
            $typeVals = array('payment'=>'Payment');
        else
            $typeVals = array('payment'=>'Payment','recharge'=>'Recharge');
    }
    $this->setWidgets(array(
      'pay_mode' => new sfWidgetFormSelect(array('choices' => $payModeArr),array('onchange'=>'getStatusfrmPayMode()')),
      'pay_type' => new sfWidgetFormSelect(array('choices' => $typeVals )),
      'from'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
      'to'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input'))  ,
      'trans_type' => new sfWidgetFormSelect(array('choices' => $optionsVals)),
      'transaction_no'=>  new sfWidgetFormInputText(array(),array('maxlength'=>50,'class'=>'txt-input')),
      'account_no'=>  new sfWidgetFormInputText(array(),array('maxlength'=>50,'class'=>'txt-input')),
      'username'=>  new sfWidgetFormInputText(array(),array('maxlength'=>50,'class'=>'txt-input'))
      
    ));

    $this->setDefault('trans_type', 'all');

    $this->widgetSchema->setNameFormat('paidwithvisa[%s]');
    echo $this->getValue("pay_mode");
     $this->setValidators(array(
      'pay_mode'   => new sfValidatorChoice(array('choices' => array_keys($payModeArr),'required' => true),array('required'=>'Please select Payment Mode')),
      'pay_type'   => new sfValidatorChoice(array('choices' => array_keys($typeVals)),array('required'=>'Please select Payment Type')),
      'from' => new sfValidatorString(array('required' => false),array('required'=>'Please select from date') ),
      'to' => new sfValidatorString(array('required' => false) ,array('required'=>'Please select to date')),
      'transaction_no'=>   new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'account_no'=>   new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'username'=>   new sfValidatorString(array('max_length' => 50, 'required' => false)),
      
    ));

$this->validatorSchema->setOption('allow_extra_fields', true);
   $this->widgetSchema->setLabels(array(
           'pay_mode'    => 'Payment Mode',
           'trans_type'  => 'Transaction Status',
           'pay_type'    => 'Payment type',
           'transaction_no'    => 'Transaction Number',
           'account_no'  => 'Account No.',
           'username'    => 'Username',

        ));


  }
}