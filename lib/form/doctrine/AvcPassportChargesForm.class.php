<?php

/**
 * AvcPassportCharges form.
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AvcPassportChargesForm extends BaseAvcPassportChargesForm
{
  public function configure()
  {
      $this->widgetSchema->setLabels(array(
            'from_date' => 'From Date',
            'to_date' => 'To Date',
        ));

        $this->setWidgets(array(
             'from_date'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
             'to_date'       => new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
        ));

        $this->widgetSchema->setNameFormat('avc_report[%s]');

         $this->setValidators(array(
            'from_date'    => new sfValidatorString(array('required' => false)),
            'to_date'    => new sfValidatorString(array('required' => false)),
        ));
  }
}
