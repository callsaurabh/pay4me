<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SettlementaccountsFormclass
 *
 * @author spandey
 */
class SettlementaccountsForm  extends BasePaymentRecordForm {
    public function configure()
    {

        $this->setWidgets(array(
                 
                  'from'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
                  'to'=>new widgetFormDateCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input'))  ,
                 

            ));

         $this->setValidators(array(
               
                'from' => new sfValidatorString(array('required' => true),array('required'=>'Please select from date') ),
                'to' => new sfValidatorString(array('required' => true) ,array('required'=>'Please select to date')),
               
            ));

        $this->widgetSchema->setLabels(array(
                                                  
                                                    'to'=> 'To Date:',
                                                    'from'=> 'From Date:',
                                                  
            ));
           $this->widgetSchema->setNameFormat('report[%s]');

         if(isset($_REQUEST['report']['to']) && $_REQUEST['report']['to']!=""){
            $this->validatorSchema->setPostValidator(

                new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to',
                            array('throw_global_error' => false),
                            array('invalid' => 'The From Date cannot be greater than To Date')
                        )
                    ))

            );

         }}
}
?>
