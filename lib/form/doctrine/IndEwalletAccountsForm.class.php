<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndEwalletAccountsFormclass
 *
 * @author spandey
 */
class IndEwalletAccountsForm extends BaseEpMasterAccountForm {
    public function configure()
    {
        $entry_type_choices = array('' => 'All Transaction List', 'debit' => '-- Debit --', 'credit' => '-- credit --');
      
        $this->setWidgets(array(
        'account_no'=> new sfWidgetFormInputText(array(),array("class"=>'FieldInput','maxlength'=>30)),      
        'from'=>new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input')),
        'to'=>new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input'))  ,
         'entry_type' => new sfWidgetFormSelect(array('choices' => $entry_type_choices)),
            ));
        $this->validatorSchema->setOption('allow_extra_fields', true);

        $this->widgetSchema->setLabels(array(
                                                    'account_no'=>'eWallet account No <sup>*</sup>',
                                                    'to'=> 'To Date',
                                                    'from'=> 'From Date',
                                                    'entry_type'=>'Transaction Type'
            ));
    }
}
?>
