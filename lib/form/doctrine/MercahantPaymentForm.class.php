<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MercahantPaymentFormclass
 *
 * @author spandey
 */
class MercahantPaymentForm extends sfForm{
    public function configure()
    {
        $merchant_name = $_REQUEST['name'];
        $payForMeManagerObj = new payForMeManager() ;

        $merchant_service_array = $payForMeManagerObj->getMerchantServices($merchant_name) ;
        $optionsVals[''] = 'Please select';

        foreach($merchant_service_array as $key=>$val)
        {
            $optionsVals[$key]=$val;
        }

        $this->setWidgets(array(

        'txnId' => new sfWidgetFormInputText(array(),array('class'=>'FieldInput')),
        'type' => new sfWidgetFormSelect(array('choices' => $optionsVals),array('onchange'=>'display_options()')),
            ));
       
        $this->widgetSchema->setLabels(array(
            'txnId'    => 'Transaction Number',
            'type'    => 'Application Type',
            ));
    }

}
?>
