<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of listBankAdminFormclass
 *
 * @author spandey
 */
class listBankAdminForm  extends sfForm
{    //put your code here

    public function configure()
    {
        $countryId  = $this->getOption('country_id');

        $user = sfContext::getInstance()->getUser();
        $userId =  $user->getGuardUser()->getId();

        $country_array[''] = '--Select Country--';
        $chkBankUser = Doctrine::getTable('BankUser')->IsBankUser($userId);

        if($chkBankUser) {
            $bank_id = $user->getGuardUser()->getBankUser()->getFirst()->getBank()->getId();
            $country = Doctrine::getTable('BankConfiguration')->getAllCountry($bank_id);
            foreach($country as $key=>$val) {
                $country_array[$key]=$val;
            }
        }


        $this->widgetSchema['country_id'] = new sfWidgetFormChoice(array('choices' =>$country_array));


        $this->widgetSchema['user_search']  = new sfWidgetFormInputText(array(),array('class'=>'FieldInput','value'=>$this->getOption('value')));
        // $this->validatorSchema['country_id']      = new sfValidatorChoice(array('required'=>false,'choices' =>  array_keys($country_array)));
        $this->widgetSchema->setLabels(array(
                                                    'country_id'=>'Country',
                                                    'user_search'=> 'Username',

            ));

        if($countryId){

            $this->widgetSchema->setDefault('country_id', $countryId);

        }
    }

}
?>
