<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class collectionAccountSetupForm extends BaseBankForm
{
  public function configure()
  {

      $count = 1;
      $banks = $this->getOption('banks');

      foreach ($banks as $key=>$val){
                $Widget['BankName'.$count] = new sfWidgetFormInputText(array(),array('class'=>'invisible'));
                $Widget['accNo'.$count] =   new sfWidgetFormInputText(array(),array('class'=>'FieldInput'));
                $Widget['accName'.$count] = new sfWidgetFormInputText(array(),array('class'=>'FieldInput'));
                $Widget['sortCode'.$count] = new sfWidgetFormInputText(array(),array('class'=>'FieldInput'));
                $Widget['bank'.$count] =  new sfWidgetFormInputHidden();
                $Widget['bankId'.$count] =  new sfWidgetFormInputHidden();


                $Validator['accNo'.$count] =    new sfValidatorNumber(array('required' => true),array('required' => 'Please enter Account Number','invalid'=>'Please enter a valid Account Number'));
                $Validator['accName'.$count] =  new sfValidatorString(array('required' => true),array('required' => 'Please enter Account Name'));
                $Validator['sortCode'.$count] = new sfValidatorString(array('required' => true),array('required' => 'Please enter Sort Code'));


                $Label['BankName'.$count] = '<b>'.$val.'</b>';
                $Label['accNo'.$count]    = 'Account Number';
                $Label['accName'.$count]  = 'Account Name';
                $Label['sortCode'.$count] = 'Sort Code';
                

                $count++;
        }

                $this->setWidgets($Widget);

/*
 *
                    array(
                    'BankName'.$count       => new sfWidgetFormInputText(array(),array('class'=>'invisible')),
                    'accNo'.$count          => new sfWidgetFormInputText(array(),array('class'=>'FieldInput')),
                    'accName'.$count        => new sfWidgetFormInputText(array(),array('class'=>'FieldInput')),
                    'sortCode'.$count       => new sfWidgetFormInputText(array(),array('class'=>'FieldInput')),
                    'bank'.$count           => new sfWidgetFormInputHidden(),
                    'bankId'.$count         => new sfWidgetFormInputHidden(),

                )
            
 */

//                $this->setValidators(array(
//                    'accNo'.$count            => new sfValidatorNumber(array('required' => true),array('required' => 'Please enter Account Number','invalid'=>'Please enter a valid Account Number')),
//                    'accName'.$count          => new sfValidatorString(array('required' => true),array('required' => 'Please enter Account Name')),
//                    'sortCode'.$count         => new sfValidatorString(array('required' => true),array('required' => 'Please enter Sort Code')),
//                ));
//
//                $this->setLabels['BankName'.$count] = '<b>'.$val.'</b>';
//                $this->widgetSchema->setLabels(array(
////                    'BankName'.$count      => '<b>'.$val.'</b>',
//                    'accNo'.$count      => 'Account Number',
//                    'accName'.$count    => 'Account Name',
//                    'sortCode'.$count   => 'Sort Code'
//                ));


        $this->widgetSchema->setNameFormat('CollectionAccount[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}
