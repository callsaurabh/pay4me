<?php

/**
 * SecurityQuestion form.
 *
 * @package    form
 * @subpackage SecurityQuestion
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class SecurityQuestionForm extends BaseSecurityQuestionForm {

  public function configure() {
      unset(
          $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
      );

      $this->setWidgets(array(
          'id'         => new sfWidgetFormInputHidden(),
          'questions'  => new sfWidgetFormInputText(array(),array('maxlength'=>255))
      ));

      $this->setValidators(array(
          'id'         => new sfValidatorDoctrineChoice(array('model' => 'SecurityQuestion', 'column' => 'id', 'required' => false)),
          'questions'  => new sfValidatorString(array('max_length' => 255, 'required' => true))
      ));

      $this->widgetSchema->setLabels(array(
          'questions'    => 'Question'
      ));

      $this->widgetSchema->setNameFormat('security_question[%s]');

    }
  
}