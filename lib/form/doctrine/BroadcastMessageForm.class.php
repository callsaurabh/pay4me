<?php

/**
 * BroadcastMessage form.
 *
 * @package    form
 * @subpackage BroadcastMessage
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BroadcastMessageForm extends BaseBroadcastMessageForm
{
  public function configure()
  {
        unset(
        $this['created_at'], $this['updated_at'], $this['is_active']
    );
       $years = range(date('Y'), 2050); //Creates array of years between 1920-2000
       $years_list = array_combine($years, $years);
       $this->setWidgets(array(
            'id'         => new sfWidgetFormInputHidden(),
            'title'  => new sfWidgetFormInputText(array(),array('class'=>'FieldInput', 'maxlength'=>255)),
            'message'    => new sfWidgetFormFCKEditor(array(),
                                    array('tool' => 'Broadcast',
//                                      'config'=> 'fckconfig',
                                      'rows' => 30,                                      
//                                      'height' => 350,
//                                      'tool' => 'Basic', // name of a configured toolbar
//                                      'config'=> 'fckconfig'  // points to web/js/myfckconfig.js
                                    )),
            'exp_date'    => new sfWidgetFormDateCal(array('years' => $years_list))
        ));

     $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => 'BroadcastMessage', 'column' => 'id', 'required' => false)),
      'title'      => new sfValidatorString(array('max_length' => 50, 'required' => true, 'trim' => true),array('max_length'=>'Message title should be less than or equal to 50 characters.')),
      'message'    => new sfValidatorString(array('max_length' => 1000, 'required' => true),array('max_length'=>'Broadcast message should be less than or equal to 1000 characters.')),
      'exp_date'  => new sfValidatorDateTime(array('min'=>strtotime("-1 day"),'required' => true), array('min'=>'Expiry Date should be greater than equal to today\'s date.'))
  ));

    $this->validatorSchema->setPostValidator(
                  new sfValidatorAnd(array(
                          new sfValidatorDoctrineUnique(array(
                                  'model' => 'BroadcastMessage',
                                  'column' => 'id',
                                  'primary_key' => 'id',
                                  'required' => true
                          )),
                         new sfValidatorDoctrineUnique(array(
                                  'model' => 'BroadcastMessage',
                                  'column' => 'title',
                                  'required' => true
                          ),array('invalid'=>'The title already exixts'))
                  ))

          );

    $this->widgetSchema->setLabels(array(
        'title'    => 'Message Title',
        'message'  => 'Broadcast Message',
        'exp_date' => 'Expire Date'
    ));

    $this->widgetSchema->setNameFormat('broadcast_message[%s]');

  }
}