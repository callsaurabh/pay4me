<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class UpdateSecurityAnswerForm extends BaseSecurityQuestionsAnswersForm
{
    
    public function configure()
    {
       // do unsetting
        unset(
            $this['id'], $this['security_question_id'],$this['answer'],$this['updated_at'],$this['created_at'],
            $this['updated_by'],$this['created_by'],$this['deleted_at'],$this['user_id']); 
        $userId = $this->getOption('userId');
        $ansCount =$this->getOption('ansCount');
        if(!empty($ansCount)){
          
             $arrSecurityQuestionsAnswers = Doctrine::getTable('SecurityQuestionsAnswers')->getUserQuestionsByUserId($userId);
        }else{
            $arrSecurity = Doctrine::getTable('SecurityQuestion')->getRandomQuestion();
          
            if(!empty($arrSecurity)){
                foreach($arrSecurity as $key=>$value){
                    $arrSecurityQuestionsAnswers[$key]['quid'] = $value['id'];
                    $arrSecurityQuestionsAnswers[$key]['question'] = $value['questions'];
                }
            }
        }
        $val = 0;
        foreach($arrSecurityQuestionsAnswers as $arrQuestion){
            $val++;    
            $this->widgetSchema['security_question_text_'.$val]= new sfWidgetFormInputText();
            $this->widgetSchema['security_question_id_'.$val]= new sfWidgetFormInputHidden();
            $this->widgetSchema['answer_'.$val] = new sfWidgetFormInputText(array('type'=>'password'));
            $this->getWidget('security_question_text_'.$val)->setAttributes(array('size'=>'40','readonly'=>'readonly','class'=>'loginHintQuestion','tabindex'=>-1));
            $this->setDefault('security_question_text_'.$val, $arrQuestion['question'].' ?');
            $this->setDefault('security_question_id_'.$val, $arrQuestion['quid']);
            if(!empty($arrQuestion['answer'])){
                 $this->setDefault('answer_'.$val, $arrQuestion['answer']);
            }
            $this->validatorSchema['security_question_text_'.$val] = new sfValidatorString(array('required'=>false));
            $this->validatorSchema['answer_'.$val] = new sfValidatorString(array('required'=>true),array('required'=>'Answer is required'));
            $this->validatorSchema['security_question_id_'.$val] = new sfValidatorString(array('required'=>false));
            $attr = array('maxlength'=>'50','onpaste'=>"return false",'ondrop'=>"return false", 'ondrag'=>"return false", 'oncopy'=>"return false", 'autocomplete'=>"off");
            $this->widgetSchema['answer_'.$val]->setAttributes($attr);
            $this->widgetSchema->setLabels(
                array('security_question_text_'.$val=>'Security Question','answer_'.$val=>'Answer'));
          }
    }
}
