<?php

/**
 * BankBranch form.
 *
 * @package    form
 * @subpackage BankBranch
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BankBranchForm extends BaseBankBranchForm
{
  public function configure()
  {
       unset(
          $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );


      $countryId = $this->getOption('country_id');
      
      if($this->getOption('action') == 'edit'){

          $bankArr = BankTable::getBankName($this->getOption('bankId'));

          $this->setWidgets(array(
            'id'         => new sfWidgetFormInputHidden(),
            'bank_name'   => new sfWidgetFormInputText(array(),array('class'=>'FieldInput', 'readonly' => true, 'value' => $bankArr['bank_name'])),
            'name'       => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'class'=>'FieldInput')),
            'address'    => new sfWidgetFormTextarea(array(),array('maxlength'=>500, 'class'=>'FieldInput')),
            'branch_code' => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'class'=>'FieldInput')),
            'branch_sortcode' => new sfWidgetFormInputText(array(),array('maxlength'=>20, 'class'=>'FieldInput')),
            'country_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => 'Please select Country')),
            'state_id'     => new sfWidgetFormChoice(array('choices' => array('' => 'Please select State'))),
            'lga_id'      => new sfWidgetFormChoice(array('choices' => array('' => 'Please select City'))),
            'state_name'  => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'disabled'=>'true')),
            'lga_name'    => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'disabled'=>'true')),
            'bank_id'     => new sfWidgetFormInputHidden(array(),array('maxlength'=>30, 'class'=>'FieldInput'))
        ));

          $this->setValidators(array(
            'id'         => new sfValidatorDoctrineChoice(array('model' => 'BankBranch', 'column' => 'id', 'required' => false)),
            //'bank_id'    => new sfValidatorDoctrineChoice(array('model' => 'Bank', 'required' => true)),
            'country_id'        => new sfValidatorDoctrineChoice(array('model' => 'Country','required'=>'true')),
            'state_id' => new sfValidatorInteger(array('required' => false)),
            'lga_id' => new sfValidatorInteger(array('required' => false)),
            'state_name'  => new sfValidatorString(array('required' => false)),
            'lga_name'    => new sfValidatorString(array('required' => false)),
            'bank_id'     => new sfValidatorInteger(array('required' => true)),
            'bank_name' =>  new sfValidatorString(array('required' => false))
        ));


          $this->widgetSchema->setLabels(array(
            'bank_name'    => 'Bank',
            'name'    => 'Bank Branch Name',
            'address' => 'Address',
            'branch_code' => 'Branch Code',
            'branch_sortcode' => 'Branch SortCode',
            'country_id' => 'Country',
            'state_id' => 'State ',
            'lga_id' => 'LGA ',
            'state_name' => 'State Name <sup class="cRed">*</sup>',
            'lga_name' => 'City Name <sup class="cRed">*</sup>'
        ));


      }else{ 


        $this->setWidgets(array(
            'id'         => new sfWidgetFormInputHidden(),
            'bank_id'    => new sfWidgetFormDoctrineChoice(array('model' => 'Bank', 'add_empty' => 'Please select Bank', 'table_method'  => 'getBankUserList')),
            'name'       => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'class'=>'FieldInput')),
            'address'    => new sfWidgetFormTextarea(array(),array('maxlength'=>500, 'class'=>'FieldInput')),
            'branch_code' => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'class'=>'FieldInput')),
            'branch_sortcode' => new sfWidgetFormInputText(array(),array('maxlength'=>20, 'class'=>'FieldInput')),
           // 'country_id'   => new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => 'Please select Country')),
            'country_id'    => new sfWidgetFormChoice(array('choices' => array(''=>'Please Select Country'))),
            'state_id'     => new sfWidgetFormChoice(array('choices' => array('' => 'Please Select State'))),
            'lga_id'      => new sfWidgetFormChoice(array('choices' => array('' => 'Please Select City'))),
            'state_name'  => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'disabled'=>'true')),
            'lga_name'    => new sfWidgetFormInputText(array(),array('maxlength'=>30, 'disabled'=>'true')),
        ));


          $this->setValidators(array(
            'id'         => new sfValidatorDoctrineChoice(array('model' => 'BankBranch', 'column' => 'id', 'required' => false)),
            'bank_id'    => new sfValidatorDoctrineChoice(array('model' => 'Bank', 'required' => true)),
            //'country_id'        => new sfValidatorDoctrineChoice(array('model' => 'Country','required'=>'true')),
            'country_id'        => new sfValidatorInteger(array('required' => false)),
            'state_id' => new sfValidatorInteger(array('required' => false)),
            'lga_id' => new sfValidatorInteger(array('required' => false)),
            'state_name'  => new sfValidatorString(array('required' => false)),
            'lga_name'    => new sfValidatorString(array('required' => false)),


        ));


        $this->widgetSchema->setLabels(array(
            'bank_id'    => 'Bank',
            'name'    => 'Bank Branch Name',
            'address' => 'Address',
            'branch_code' => 'Branch Code',
            'branch_sortcode' => 'Branch SortCode',
            'country_id' => 'Country',
            'state_id' => 'State',
            'lga_id' => 'LGA',
            'state_name' => 'State Name <sup class="cRed">*</sup>',
            'lga_name' => 'City Name <sup class="cRed">*</sup>'
        ));


      }

         

      /* if($countryId)
       $this->widgetSchema['country_id']->setOption('query',CountryTable::getSelectedCountryCachedQuery($countryId));*/

        $this->validatorSchema['bank_id'] =new sfValidatorString(array(),array('required' => 'Bank required'));
        $this->validatorSchema['address'] = new sfValidatorString(array('max_length' => 500)
                    ,array('max_length'=>'Address can not  be more than 500 characters.','required' =>'Address required'));
        $this->validatorSchema['name'] = new sfValidatorString(array('max_length' => 30,
           ),array('required' => 'Bank Branch Name required', 'max_length' =>'Bank Branch Name cannot be more than 30 characters.',
           'invalid' =>'Bank Branch Name is invalid.' ));                                                                                           //removed validation:'pattern' => '/^[a-zA-Z0-9 \. \- \,]*$/',
        $this->validatorSchema['branch_code'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9 \. \-]*$/','max_length' => 30,
           ),array('required' => 'Branch Code required', 'max_length' =>'Branch Code cannot be more than 30 characters.',
           'invalid' =>'Branch Code is invalid.' ));
        $this->validatorSchema['branch_sortcode'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9 \. \-]*$/','max_length' => 20,
           ),array('required' => 'Branch SortCode required', 'max_length' =>'Branch Sortcode cannot be more than 20 characters.',
           'invalid' =>'Branch SortCode is invalid.' ));
      $this->validatorSchema['country_id'] =new sfValidatorString(array(),array('required' => 'Country required'));
      
      if(empty($_POST['bank_branch']['country_id'])){
          $this->validatorSchema['state_id'] =new sfValidatorString(array(),array('required' => 'State required'));
          $this->validatorSchema['lga_id'] =new sfValidatorString(array(),array('required' => 'LGA required'));
      }

          $this->validatorSchema->setPostValidator(
                        new sfValidatorAnd(array(
                            new sfValidatorCallback(array('callback' => array($this, 'checkCountryId'))),
                            new sfValidatorCallback(array('callback' => array($this, 'checkCountryId1'))),
                                new sfValidatorDoctrineUnique(array(
                                        'model' => 'BankBranch',
                                        'column' => 'id',
                                        'primary_key' => 'id',
                                        'required' => true
                                )),
                               new sfValidatorDoctrineUnique(array(
                                        'model' => 'BankBranch',
                                        'column' => 'branch_sortcode',
                                        'primary_key' => 'id',
                                        'required' => true
                                ),array('invalid'=>'The SortCode already exists')),
                              new sfValidatorDoctrineUnique(array(
                                        'model' => 'BankBranch',
                                        'column' => array('branch_code','bank_id'),
                                        'primary_key' => 'id',
                                        'required' => true
                                ),array('invalid'=>'The branch with same branch code already exists')),

                        ))

                );

       

        $this->widgetSchema->setNameFormat('bank_branch[%s]');
  }

  public function checkCountryId($validator, $values) {
//    print_r($values);exit;
      if ($values['country_id'] == 154 ) {
         
              if($values['state_id']=='')
              {
                $error = new sfValidatorError($validator,'State   required');
               throw new sfValidatorErrorSchema($validator,array('state_id' => $error));
              }
          
          
      }
      else if ($values['country_id'] !=''&&  $values['country_id']!=154) {
        if($values['state_name']=='')
            {
              $error = new sfValidatorError($validator,'State Name  required');
              throw new sfValidatorErrorSchema($validator,array('state_name' => $error));
            }

      }
      return $values;
    }

    public function checkCountryId1($validator, $values) {
    //print_r($values);exit;
      if ($values['country_id'] == 154 ) {          


              if($values['lga_id']=='')
              {
                $error = new sfValidatorError($validator,'LGA   required');
               throw new sfValidatorErrorSchema($validator,array('lga_id' => $error));
              }

      }
      else if ($values['country_id'] !=''&&  $values['country_id']!=154) {
        
            if($values['lga_name']=='')
            {
              $error = new sfValidatorError($validator,'City Name  required');
              throw new sfValidatorErrorSchema($validator,array('lga_name' => $error));
            }

      }
      return $values;
    }
}