<?php

/**
 * IfaSearch form.
 *
 * @package    form
 * @subpackage IfaSearch
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class IfaSearchForm extends BaseIfaSerachForm {
    public function configure() {
       
        $this->setWidgets(array(
            'ifa_no'            => new sfWidgetFormInputText(array(),array('maxlength'=>255)),
            'ref_no'            => new sfWidgetFormInputText(array(),array('maxlength'=>255))
        ));

        $this->setValidators(array(
            'ifa_no'        => new sfValidatorString(array('required' => true),array('required'=>'Please enter IFA Number')),
            'ref_no'        => new sfValidatorString(array('required' => true),array('required'=>'Please enter Reference Number'))
        ));

        $this->widgetSchema->setLabels(array(
            'ifa_no'    => 'IFA Number',
            'ref_no'    => 'Reference Number',
        ));

        $this->widgetSchema->setNameFormat('ifa_search[%s]');
    }
}