<?php

/**
 * MerchantService form.
 *
 * @package    form
 * @subpackage MerchantService
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class MServiceWizdForm extends BaseMerchantServiceForm
{
  public function configure()
  {
      unset(
            $this['created_at'], $this['updated_at'] ,$this['created_by'], $this['updated_by'], $this['deleted_at']
        );

        $this->setWidgets(array(
            'id'           => new sfWidgetFormInputHidden(),
            'merchant_id'  => new sfWidgetFormInputHidden(),
            'name' => new sfWidgetFormInputText(array(),array('maxlength'=>50)),
            'notification_url' => new sfWidgetFormInputText(array(),array()),
            'merchant_home_page' => new sfWidgetFormInputText(array(),array()),
            'version' => new sfWidgetFormInputText(array(),array('maxlength'=>255)),
        ));

        $this->setValidators(array(
            'id'              => new sfValidatorDoctrineChoice(array('model' => 'MerchantService', 'column' => 'id', 'required' => false)),
            'merchant_id' => new sfValidatorDoctrineChoice(array('model' => 'Merchant',  'column' => 'id', 'required' => false)),
            'name'        => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/','max_length' => 50, 'required' => true),array('required'=>'Please enter Merchant Service name','max_length'=>'Merchant Service name too long (50 character max.).',
           'invalid' =>'Merchant Service Name is invalid.')),
            'notification_url' => new sfValidatorUrl(array('required' => true),array('required'=>'Please enter Notification Url','invalid'=>'Please enter valid url')),
            'merchant_home_page' => new sfValidatorUrl(array('required' => true),array('required'=>'Please enter Merchant Home Page Url','invalid'=>'Please enter valid url')),
            'version' => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/','max_length' => 50, 'required' => false),array( 'invalid' =>'Version is invalid.')),
        ));

        //add url :bug Id 28890
        $this->widgetSchema->setLabels(array(
            'name' => 'Merchant Service',
            'notification_url' => 'Notification Url',
            'merchant_home_page' => 'Merchant Home Page Url',
            'version' => 'Version' ,
        ));

        $this->widgetSchema->setNameFormat('merchant_service[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}