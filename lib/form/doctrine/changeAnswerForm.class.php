<?php
class changeAnswerForm extends BaseForm {

    public function configure() {
        //unset($this['remember'], $this['deleted_at'], $this['username']);
        $this->setWidgets(array(
            'password' => new sfWidgetFormInput(array('type' => 'password')),
        ));
        $attr = array('onpaste' => "return false",'value'=>'','ondrop' => "return false",
            'ondrag' => "return false", 'oncopy' => "return false", 'autocomplete' => "off");
        $this->widgetSchema['password']->setAttributes($attr);
        $this->widgetSchema->setNameFormat('answerpassword[%s]');
        $this->validatorSchema['password'] = new sfValidatorCallback(array(
        'callback'  => 'changeAnswerForm::userVerifyPasswordCallBack'));
        $this->validatorSchema['password']->setOption('required',true);
        $this->validatorSchema['password']->setMessage('required','Password can\'t be empty');
        $this->validatorSchema['password']->setMessage('invalid','Password is incorrect');
        $this->widgetSchema->setLabels(array('password' => 'Password<sup>*</sup>'));
    }
    
    
  public static function userVerifyPasswordCallBack($validator, $value, $arguments) {
    // this is my logged in user
     $user = sfContext::getInstance()->getUser();
    // TODO verify if following usage of == is correct
     if (empty($value)) {
      // password don't match
      throw new sfValidatorError($validator, 'required');
    }
    if (!$user->checkPassword($value)) {
      // password don't match
      throw new sfValidatorError($validator, 'invalid');
    }
    return $value;
  }

}
