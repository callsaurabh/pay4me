<?php

/**
 * NoOfValidRules form.
 *
 * @package    form
 * @subpackage ValidationRules
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CurrencyChoiceForm extends BaseCurrencyCodeForm
{
  public function configure()
  {
       $curr = $this->getOption('selectedCurrency');
      if(!isset($curr))
        {
            $curr = array(1);
        }

        $this->setWidgets(array(
                'currency' => new sfWidgetFormDoctrineChoice(array('model' => 'CurrencyCode','multiple'=>true,'method'=>'getCurrency')),
//              ,'add_empty'=>'-- currnecy --'
        ));
        $this->setDefaults(array(
                'currency' => $curr 
        ));



        $this->setValidators(array(
             'currency'   => new sfValidatorDoctrineChoice(array('model' => 'CurrencyCode', 'multiple'=>true,'column' => 'id', 'required' => true),array('required'=>'Please select currency','invalid'=>'Please select currency')),
        ));


        $this->widgetSchema->setLabels(array(
            'currency'    => 'Currency',
        ));

        
        $this->widgetSchema->setNameFormat('currencyForm[%s]');
//        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
  }
}