<?php

/**
 * BankBranch form.
 *
 * @package    form
 * @subpackage BankBranch
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class CurrencyPaymentmodeForm extends BaseServicePaymentModeOptionForm
{
  public function configure()
  {
       unset(
          $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );

     $Options =  $this->getOptions();
     $this->setWidgets(array(
           
            'currency' => new sfWidgetFormChoice(array('choices'=>$Options['currency'])),
            'paymentMode' => new sfWidgetFormChoice(array('choices' => array('' => '--Select Payment Mode--'))),

        ));

     $this->setValidators(array(
            'currency'   => new sfValidatorString(array(),array('required' => 'Currency required')),
            'paymentMode'   => new sfValidatorString(array(),array('required' => 'Payment Mode required')),
        ));

       $this->widgetSchema->setLabels(array(
            'currency'    => 'Currency',
            'paymentMode'    => 'Payment Mode',

        ));


     }

}