<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of searchUserFormclass
 *
 * @author spandey
 */
class searchUserForm extends sfForm{
    public function configure()
    {

        $bankId     = $this->getOption('bank_id');
        $countryId  = $this->getOption('country_id');
        
        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        $bankBranchArray = $bankBranch_obj->getBankBranchList($bankId,$countryId);

        $bank_branch_array[''] = 'Please select bank branch';
        foreach($bankBranchArray as $key=>$val) {
            $bank_branch_array[$key]=$val;
        }
        
      
        $this->widgetSchema['bank_branch_id'] = new sfWidgetFormChoice(array('choices' =>$bank_branch_array));

        if($this->getOption('type')=='userid'){
            $this->widgetSchema['userid']  = new sfWidgetFormInputHidden(array(),array('value'=>$this->getOption('value')));
        }else{
            $this->widgetSchema['user_search']  = new sfWidgetFormInputText(array(),array('class'=>'FieldInput','value'=>$this->getOption('value')));
        }

        $this->widgetSchema->setLabels(array(
                                                    'bank_branch_id'=>'Select Bank Branch',
                                                    'user_search'=> 'Username',

            ));
        
      if($this->getOption('bank_branch_id')){
          $bankBranchId = $this->getOption('bank_branch_id');
          $this->widgetSchema->setDefault('bank_branch_id', $bankBranchId);

      }
    }
  
}
?>
