<?php

/**
 * EpInternetBankConfirmationRequest form base class.
 *
 * @method EpInternetBankConfirmationRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpInternetBankConfirmationRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'order_id'           => new sfWidgetFormInputText(),
      'ewallet_number'     => new sfWidgetFormInputText(),
      'transaction_number' => new sfWidgetFormInputText(),
      'session_id'         => new sfWidgetFormInputText(),
      'transaction_date'   => new sfWidgetFormInputText(),
      'application_type'   => new sfWidgetFormInputText(),
      'name'               => new sfWidgetFormInputText(),
      'application_charge' => new sfWidgetFormInputText(),
      'transaction_charge' => new sfWidgetFormInputText(),
      'amount'             => new sfWidgetFormInputText(),
      'service_charge'     => new sfWidgetFormInputText(),
      'bank_account_name'  => new sfWidgetFormInputText(),
      'total_amount'       => new sfWidgetFormInputText(),
      'channel_code'       => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'order_id'           => new sfValidatorInteger(),
      'ewallet_number'     => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'transaction_number' => new sfValidatorInteger(array('required' => false)),
      'session_id'         => new sfValidatorPass(array('required' => false)),
      'transaction_date'   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'application_type'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'application_charge' => new sfValidatorNumber(array('required' => false)),
      'transaction_charge' => new sfValidatorNumber(array('required' => false)),
      'amount'             => new sfValidatorNumber(array('required' => false)),
      'service_charge'     => new sfValidatorNumber(array('required' => false)),
      'bank_account_name'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'total_amount'       => new sfValidatorNumber(array('required' => false)),
      'channel_code'       => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpInternetBankConfirmationRequest', 'column' => array('order_id')))
    );

    $this->widgetSchema->setNameFormat('ep_internet_bank_confirmation_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpInternetBankConfirmationRequest';
  }

}
