<?php

/**
 * EpInternetBankVerifyRequest form base class.
 *
 * @method EpInternetBankVerifyRequest getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpInternetBankVerifyRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'order_id'           => new sfWidgetFormInputText(),
      'ewallet_number'     => new sfWidgetFormInputText(),
      'transaction_number' => new sfWidgetFormInputText(),
      'session_id'         => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'order_id'           => new sfValidatorInteger(),
      'ewallet_number'     => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'transaction_number' => new sfValidatorInteger(array('required' => false)),
      'session_id'         => new sfValidatorPass(),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpInternetBankVerifyRequest', 'column' => array('order_id')))
    );

    $this->widgetSchema->setNameFormat('ep_internet_bank_verify_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpInternetBankVerifyRequest';
  }

}
