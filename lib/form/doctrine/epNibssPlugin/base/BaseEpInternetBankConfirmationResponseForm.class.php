<?php

/**
 * EpInternetBankConfirmationResponse form base class.
 *
 * @method EpInternetBankConfirmationResponse getObject() Returns the current form's model object
 *
 * @package    mysfp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEpInternetBankConfirmationResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'request_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpInternetBankConfirmationRequest'), 'add_empty' => false)),
      'ewallet_number'     => new sfWidgetFormInputText(),
      'transaction_number' => new sfWidgetFormInputText(),
      'session_id'         => new sfWidgetFormInputText(),
      'validation_number'  => new sfWidgetFormInputText(),
      'channel_code'       => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'request_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpInternetBankConfirmationRequest'))),
      'ewallet_number'     => new sfValidatorString(array('max_length' => 40)),
      'transaction_number' => new sfValidatorInteger(array('required' => false)),
      'session_id'         => new sfValidatorPass(),
      'validation_number'  => new sfValidatorInteger(),
      'channel_code'       => new sfValidatorInteger(),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_internet_bank_confirmation_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpInternetBankConfirmationResponse';
  }

}
