<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BankConfigrationDetailFormclass
 *
 * @author spandey
 */
class BankConfigrationDetailForm extends BaseBankConfigurationForm {
     public function configure() {
        unset(
            $this['bank_id'],$this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by'], $this['deleted_at']
        );
        $this->widgetSchema['domain'] =  new sfWidgetFormInputText();
        $this->widgetSchema['country_id']      = new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => 'Please Select Country'));
        
        $this->validatorSchema['domain'] =  new sfValidatorString(array('required'=>'true'));
        $this->validatorSchema['country_id']      = new sfValidatorDoctrineChoice(array('model' => 'Country','required'=>'true'),array('required'=>'Please select Country'));

        $this->widgetSchema->setHelps(array(
            'domain'       => 'If email addresses in your bank are xyz@domain.com; enter domain.com'
        ));

}
}
?>
