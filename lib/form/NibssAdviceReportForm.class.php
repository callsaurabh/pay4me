<?php
/**
 * ScheduledPaymentConfig form.
 * @package    form
 * @subpackage ScheduledPaymentConfig
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class NibssAdviceReportForm extends sfform {
    public function configure() {
        

      $this->setWidgets(array(
             'from'       => new    widgetFormDateCal(array('format'=>'%d-%m-%Y','label'=>'Date of Birth'),array('maxlength'=>10,'readonly'=>'true','class'=>'txt-input')),
             'to'       => new   widgetFormDateCal(array('format'=>'%d-%m-%Y','label'=>'Date of Birth'),array('maxlength'=>10,'readonly'=>'true','class'=>'txt-input')),
        ));
        
        $this->widgetSchema->setLabels(array(
            'from' => 'From Date',
            'to' => 'To Date',
        ));

         $this->setValidators(array(
            'from'    => new sfValidatorString(array('required' => false)),
            'to'    => new sfValidatorString(array('required' => false)),
        ));

  
        
    }
}