<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomPaymentProcessing_collectionForm extends sfform {

    public function configure() {

     

      
       
        $add_superScipt="<sup>*</sup>";


        $currencyObj = Doctrine::getTable('CurrencyCode')->findAll();
        foreach($currencyObj as $value) {
            $currencyArray[$value->getId()] = $value->getCurrency();
        }
        $merchantObj = Doctrine::getTable('Merchant')->findAll();
        $merchantArray = array();
        $merchantArray[''] = 'Please select Merchant';
        foreach($merchantObj as $value) {
            $merchantArray[$value->getId()] = $value->getName();
        }


        $this->setWidget('merchant_id',
            new sfWidgetFormChoice(array
                ('label'=>'Merchant'.$add_superScipt,
                    'choices' => $merchantArray // Organization expected = 2
                )));
        $this->setWidget('bank',
            new sfWidgetFormChoice(array
                ('label'=>'Bank'.$add_superScipt,
                    'choices' => array(''=>'Please select Bank') // Organization expected = 2
                )));
        $this->setWidget('merchant_service',
            new sfWidgetFormChoice(array
                ('label'=>'Merchant Service'.$add_superScipt,
                    'choices' => array(''=>'Please select Merchant Service') // Organization expected = 2
                )));
        $this->setWidget('currency',
            new sfWidgetFormChoice(array
                ('label'=>'Currency'.$add_superScipt,
                     'choices' => array(''=>'Please select Currency') // Organization expected = 2
                )));

        $this->setWidget('payment_option',
            new sfWidgetFormChoice(array
                ('label'=>'Payment Option'.$add_superScipt,
                    'choices' => array(''=>'Please select Payment Option') // Organization expected = 2
                )));
     }

}

?>
