<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomEwUserComposeMail extends sfform {

    public function configure() {

        $this->widgetSchema['subject'] = new sfWidgetFormInputText(array('label' => '<sup>Subject*</sup>'), array('class' => 'FieldInput', 'maxlength' => 30));
        $this->widgetSchema['message'] = new sfWidgetFormFCKEditor(array('label' => '<sup>Body*</sup>'), array('height' => 360, 'width' => '360'));
        $this->widgetSchema['UserId'] = new sfWidgetFormInputHidden(array('default' => $this->getOption('UserId')));
        $this->validatorSchema['subject'] = new sfValidatorString(array('required'=>true,'max_length'=>30), array('required'=>'Please Enter Subject','max_length'=>'Maximum 30 Characters'));
       $this->validatorSchema['message'] = new sfValidatorString(array('required'=>true ), array('required'=>'Please Enter Message'));
       
        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

}

?>
