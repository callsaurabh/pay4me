<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomPaymentProcessing_ewalletForm extends sfform {

    public function configure() {

        if ($this->getOption('acc_type') == 'receiving') {

            $add_superScipt = null;
            $name_widget_id = 'rec_acc_name';
            $acc_widget_id = 'rec_acc_no';
        } else if ($this->getOption('acc_type') == 'ewallet') {
            $add_superScipt = "<sup>*</sup>";
            $name_widget_id = 'ewal_acc_name';
            $acc_widget_id = 'ewal_acc_no';
        }




        $this->setWidget($acc_widget_id, new sfWidgetFormInputText(array('label' => 'Account No' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 30)));
        $this->setWidget($name_widget_id, new sfWidgetFormInputText(array('label' => 'Account Name' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 30)));
    }

}

?>
