<?php

class MerchantUserActivationForm extends sfform {

    public function setup() {
        sfWidgetFormSchema::setDefaultFormFormatterName('dl');
    }

    public function configure() {

        $this->setWidgets(array(
            'id' => new sfWidgetFormInputHidden(),
            'byPass' => new sfWidgetFormInputHidden(array('default' => '1')),
            'name' => new sfWidgetFormInputHidden(array(), array('class' => 'txtfield-r', 'name' => 'name', 'maxlength' => '50', 'readonly' =>'readonly')),
            'username' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'username', 'maxlength' => 30, 'autocomplete' => 'off')),
            'password' => new sfWidgetFormInputPassword(array(), array('class' => 'keypad txtfield-r', 'name' => 'password',)),
            'cpassword' => new sfWidgetFormInputPassword(array(), array('class' => 'keypad txtfield-r', 'name' => 'cpassword',)),
            'email' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'email', 'maxlength' => '128',)),
            'captcha' => new sfWidgetFormInput(array(), array('class' => 'txtfield-r')),
        ));

        $this->setValidators(array(
            'id' => new sfValidatorString(array('required' => false)),
            'name' => new sfValidatorRegex(
                    array('pattern' => '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/', 'required' => true, 'max_length' => '50', 'min_length' => 2),
                    array('invalid' => 'Merchant name is Invalid', 'required' => 'Please enter Merchant Name', 'min_length' => 'Minimum 2 characters', 'max_length' => 'Maximum 50 characters')),
            'username' => new sfValidatorAnd(array(
                new sfValidatorString(array('max_length' => true), array('required' => 'User Name is required.', 'max_length' => 'Maximum 30 characters')),
                new sfValidatorRegex(array('pattern' => '/^[a-z\d\_\.]{5,20}$/i'), array('invalid' => 'Invalid User Name.', 'required' => 'User Name is required.')),
                new sfValidatorDoctrineUnique(array('required' => true, 'model' => 'sfGuardUser', 'column' => array('username')),
                        array('invalid' => 'User Name already Exists'))),
                    array('required' => 'Please enter Username')
            ),
            'password' => new sfValidatorString(array('required' => true), array('required' => 'Please enter Password')),
            'cpassword' => new sfValidatorString(array('required' => true), array('required' => 'Please Confirm Your Password')),
            'email' => new sfValidatorAnd(array(
                new sfValidatorEmail(array('required' => true, 'trim' => true), array('invalid' => 'Please Enter Valid Email Adrress')),
                new sfValidatorString(array('required' => true, 'max_length' => 60)),
                new sfValidatorDoctrineUnique(array('model' => 'UserDetail', 'column' => 'email'),
                        array('invalid' => 'E-mail Address already exists'))),
                    array(), array('required' => 'Please enter Email')),
            'captcha' => new sfValidatorSfCryptoCaptcha(array('required' => true, 'trim' => true), array('wrong_captcha' => 'Please enter Valid Verification Code',
                'required' => 'Please enter Verification Code.')),
        ));

        $this->validatorSchema->setPostValidator(
                new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'cpassword',
                        array('throw_global_error' => true),
                        array('invalid' => 'Your confirmed password does not match the entered password')
                )
        );
        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->widgetSchema->setLabels(array(
            'name' => 'Merchant Name',
            'username' => 'Username',
            'password' => 'Password',
            'cpassword' => 'Confirm Password',
            'email' => 'Email Address',
            'captcha' => 'Verification Code'
        ));
       // $this->widgetSchema['name']->setAttribute('disabled', 'disabled');
    }
}

?>