<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomCheckDetailsForm extends sfform {

    public function setup() {
        sfWidgetFormSchema::setDefaultFormFormatterName('dl');
    }

    public function configure() {


    
        $this->setWidgets(array(
            'bank_id' => new sfWidgetFormInputHidden(),
            'sort_code' => new sfWidgetFormInputText(array('label'=>'Sort Code'), array('class' => 'txtfield-r', 'name' => 'sort_code', 'maxlength' => '3', 'minlength'=> '3')),
            'check_number' => new sfWidgetFormInputText(array('label' => 'Cheque Number',), array('class' => 'txtfield-r', 'name' => 'check_number', 'maxlength' => '30')),
            'draft_number' => new sfWidgetFormInputText(array('label' => 'Draft Number',), array('class' => 'txtfield-r', 'name' => 'draft_number', 'maxlength' => '30')),
            'account_number' => new sfWidgetFormInputText(array('label' => 'Account Number',), array('class' => 'txtfield-r',  'name' => 'account_number', 'maxlength' => '30',)),
        ));
        $this->getWidget('bank_id')->setDefault($this->getOption('bank_id'));


        $this->setValidators(array(
            'sort_code' => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/','required' => true, 'min_length' => 2), array( 'invalid' => "Invalid Sort Code(Only Alphanumeric Charcters Accepted)",'required' => "Please enter Sort Code", 'min_length' => 'Sort code should have minimum 3 characters')),
            'check_number' => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/', 'max_length' => 30,
                    ), array('required' => '"Please enter Cheque Number', 'max_length' => 'Cheque Number cannot be more than 30 characters.', 'invalid' => "Invalid Cheque Number(Only Alphanumeric Characters Accepted)")),
            'draft_number' => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/', 'max_length' => 30,'required' => false,
                    ), array('required'=>'sdcfasdc','max_length' => 'Cheque Number cannot be more than 30 characters.', 'invalid' => "Invalid Che Number(Only Alphanumeric Characters Accepted)")),
            'account_number' => new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/', 'max_length' => 30,
                    ), array('required' => '"Please enter Payee Account Number', 'max_length' => 'Account Number cannot be more than 30 characters.', 'invalid' => "Invalid Account Number(Only Alphanumeric Characters Accepted)"))));

      $this->widgetSchema->setLabels(array(
                    'sort_code'    => 'Sort Code',
                    'check_number'    => 'Cheque Number',
                    'account_number'    => 'Payee Account Number',
                    'draft_number'    => 'Bank Draft Number',
         ));
        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'validateSortCode'))))));

        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function validateSortCode($validator, $values) {

        if ($values['sort_code'] != "") {
            
//            $first3digits = substr($values['sort_code'], 0, 3);
            $validSortCode = Doctrine::getTable('BankSortCodeMapper')->validateSortCode($this->getWidget('bank_id')->getDefault(), $values['sort_code']);

            if (!$validSortCode) {
                $error['sort_code'] = new sfValidatorError($validator, 'Invalid Sort Code For Selected Bank');
            }
        } 

        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);

        return $values;
    }

}

?>
