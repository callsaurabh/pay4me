<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomEwalletEditUserForm extends sfform {

    public function configure() {
        $add_superScipt = "<sup>*</sup>";
        if ($this->getOption('edit') === true) {
            $readonly = true;
            $disabled = true;
        } else if ($this->getOption('edit') === false) {
            $readonly = false;
            $disabled = false;
        }
        if ($this->getOption('display') == true) {

            $readonly = false;
            $disabled = false;
            $add_superScipt = null;
        }
     if ($this->getOption('activate') == true){
         if (settings::isMultiCurrencyOn()) {
            $CurrencyObj = new CurrencyCode;
            $CurrencyArray = $CurrencyObj->getAllCountrySelect();
            $this->widgetSchema['currency_id'] = new sfWidgetFormChoice(array('label' => 'Primary Currency' . $add_superScipt,'multiple' => false, 'choices' => $CurrencyArray), array('class' => 'FieldInput','add_empty' => 'false'));
            $this->validatorSchema['currency_id'] = new sfValidatorChoice(array('required'=> true,'choices' => array_keys($CurrencyArray), 'min' => 1),
                        array('required' => 'Please select Currency', 'min' => 'Please select Currency'));
        }
    }

        $this->setWidget('userId', new sfWidgetFormInputHidden(array('default'=>$this->getOption('userId'))));
        $this->setWidget('username', new sfWidgetFormInputText(array('label' => 'Username' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 30, 'readonly' => $readonly)));

        $this->setWidget('bankname', new sfWidgetFormInputHidden(array('default' => $this->getOption('bank_name'))));

        $this->setWidget('name', new sfWidgetFormInputText(array('label' => 'First Name' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 30)));



        $this->setWidget('lname', new sfWidgetFormInputText(array('label' => 'Last Name' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 30)));


        $this->setWidget('address', new sfWidgetFormTextarea(array('label' => 'Address' . $add_superScipt), array('type' => 'textarea', 'class' => 'FieldInput')));

        $this->setWidget('email', new sfWidgetFormInputText(array('label' => 'Email'), array('class' => 'FieldInput', 'maxlength' => 50, 'readonly' => $readonly)));
        $this->setWidget('mobileno', new sfWidgetFormInputText(array('label' => 'Mobile Number' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 50)));


        $this->setWidget('workphone', new sfWidgetFormInputText(array('label' => 'Work Phone'), array('class' => 'FieldInput', 'maxlength' => 50)));

        $this->widgetSchema['dob'] = new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'Date of Birth<sup>*</sup>'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));

        $this->setWidget('sms', new sfWidgetFormChoice(array(
                    'choices' => array('gsmno' => 'Mobile No.', 'workphone' => 'Work Phone', 'both' => 'Both', 'none' => 'None'),
                    'expanded' => true, 'label' => 'Send Sms'
                )));
        $this->setWidget('captcha', new sfWidgetFormInputText(array('label' => 'Verification Code' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 10)));



    }

}

?>
