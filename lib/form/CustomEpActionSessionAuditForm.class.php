<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomEpActionSessionAuditForm extends sfform {

    public function configure() {


        $this->setWidget('selectBy', new sfWidgetFormChoice(array(
                    'choices' => array('bank_branch' => 'Bank/Branch',
                        'username' => 'Username'),
                    'expanded' => true, 'label' => "Search by"
                )));

        $this->getWidget('selectBy')->setDefault('bank_branch');
        $this->setWidget('bank', new sfWidgetFormChoice(array(
                    'choices' => $this->getOption('bankList'),
                    'label' => 'Bank'
                )));

        $this->setWidget('branch', new sfWidgetFormChoice(array(
                    'choices' => array('' => '--All Branches--'),
                    'label' => 'Branch'
                )));

        $this->setWidget('username', new sfWidgetFormInput(array(
                    'label' => 'Username'
                        ), array('class' => 'FieldInput', 'maxlength' => '128')));
    }

}

?>
