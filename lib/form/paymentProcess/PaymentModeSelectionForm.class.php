<?php
/**
 * CurrencyMaster form.
 * @package    form
 */
class PaymentModeSelectionForm extends BaseFormStatic
{
  protected static $modeType = array('bank' => 'Bank', 'card' => 'card');
  protected static $modeOptions = array('interswitch' => 'Interswitch', 'etranzact' => 'Etranzact');
  public function configure()
  {
    $this->setWidgets(array(
      'merchantRequestId'  => new sfWidgetFormInputHidden(),
      'payType' => new sfWidgetFormSelectRadio(array('choices'=>self::$modeType)),
     'payMode'  => new sfWidgetFormInputHidden(),
     'userType' => new sfWidgetFormInputHidden(),
     /*  'service_type'=> new sfWidgetFormInputHidden(),
      'paymentId' => new sfWidgetFormInputHidden(),
      'txn_ref'=> new sfWidgetFormInputHidden(),      
      'application_id'=> new sfWidgetFormInputHidden(),
      'ref_no'=> new sfWidgetFormInputHidden(),
      'application_type' => new sfWidgetFormInputHidden(),
      'title'=> new sfWidgetFormInputHidden(),
      'first_name'=> new sfWidgetFormInputHidden(),
      'middle_name'=> new sfWidgetFormInputHidden(),
      'last_name'=> new sfWidgetFormInputHidden(),
      'mobile'=> new sfWidgetFormInputHidden(),
      'email'=> new sfWidgetFormInputHidden(),
      'app_charges' => new sfWidgetFormInputHidden(),
      'bank_charges' => new sfWidgetFormInputHidden(),
      */
      ));
  $this->validatorSchema['payType'] =  new sfValidatorString(array('required' => true));

        $this->widgetSchema->setLabels(array(
                    'payType'    => 'Pay With',
         ));


//    $this->widgetSchema->setLabels(array('payMode' => 'Pay Through'));
//    $this->widgetSchema->setLabels(array('payOptions' => 'Select Type'));
    $this->widgetSchema->setNameFormat('payOptions[%s]');

  }


  public function configureGroups()
  {
    $this->uiGroup = new Dlform();

    $formGroup = new FormBlock('Select Payment Mode');
    $this->uiGroup->addElement($formGroup);

    $formGroup->addElement(array('payType'));
  }
}