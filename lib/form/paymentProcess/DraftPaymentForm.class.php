<?php
/**
 * CurrencyMaster form.
 * @package    form
 */
class DraftPaymentForm extends sfform
{
  public function setup() {
        sfWidgetFormSchema::setDefaultFormFormatterName('dl');
    }
  public function configure()
  {
        $this->widgetSchema['bank_id'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['sort_code'] = new sfWidgetFormInputText(array('label'=>'Sort Code<sup>*</sup>'), array('name' => 'sort_code', 'maxlength' => '3', 'minlength'=> '3'));
        $this->widgetSchema['draft_number'] = new sfWidgetFormInputText(array('label' => 'Draft Number<sup>*</sup>'), array('maxlength' => 30));
        $this->widgetSchema['bank_id']->setDefault($this->getOption('bank_id'));
        $this->widgetSchema->setLabels(array(
            'sort_code'      => 'Sort Code',
            'draft_number'      => 'Bank Draft Number'
            ));
  }
 



  


 
}