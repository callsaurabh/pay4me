<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class RecoverEwalletAccountForm extends sfform {
    public function setup() {
        sfWidgetFormSchema::setDefaultFormFormatterName('dl');
    }
    public function configure() {
parent::configure();
         unset(
            $this['username'],
            $this['password'],
            $this['cpassword']
        );

        

        //widgets

       // $this->widgetSchema['byPass'] = new sfWidgetFormInputHidden(array('default' => '1'));

        $this->widgetSchema['ewallet'] = new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'ewallet', 'maxlength' => '30'));

        
        
        $this->widgetSchema['email'] = new sfWidgetFormInputText(array(), array( 'class' => 'txtfield-r', 'name' => 'email',
                    'maxlength' => '128'));

        $this->widgetSchema['captcha'] =  new sfWidgetFormInput(array(), array('class' => 'txtfield-r'));
        $this->widgetSchema['authorization_consent'] =  new sfWidgetFormInputCheckbox();
        

//        $this->setDefault("name", $this->getOption('name'));
//        $this->setDefault("lname", $this->getOption('lname'));
//        $this->setDefault("email", $this->getOption('email'));
//       
        //validators
//        $this->validatorSchema['ewallet'] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^([+]{1})([0-9]{10,15})$/', 'max_length' => 20),
//                        array('max_length' => 'Maximum 20 Characters', 'invalid' => 'Please enter valid eWallet Account Number', 'required' => 'Please enter eWallet Account Number'));

        $this->validatorSchema['captcha'] = new sfValidatorSfCryptoCaptcha(array('required' => true, 'trim' => true), array('wrong_captcha' => 'Please enter Valid Verification Code',
                'required' => 'Please enter Verification Code.'));
        $this->validatorSchema['authorization_consent'] = new sfValidatorBoolean( array('required' => true) );
        
        $this->validatorSchema['email'] = new sfValidatorAnd(array(
                new sfValidatorEmail(array('required' => true, 'trim' => true), array('invalid' => 'Please Enter Valid Email Id')),
                new sfValidatorString(array('required' => true, 'max_length' => 128))));
//                new sfValidatorDoctrineUnique(array('model' => 'UserDetail', 'column' => 'email'),
//                        array('invalid' => 'E-mail Address already exists'))),
//                    array(), array('required' => 'Please enter Email'));
        
        //labels
        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->widgetSchema->setLabels(array(
            'ewallet' => 'eWallet Account No.',
            'email' => 'Email Id',
            'captcha' => 'Verification Code',
            'authorization_consent' => ' I authorize pay4me to authenticate my pay4me account using '.$this->getOption('openid_type') .' credentials',
        ));
        $this->widgetSchema->setNameFormat('payOptions[%s]');
    }


}

?>
