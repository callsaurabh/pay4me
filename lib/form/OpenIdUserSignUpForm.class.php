<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class OpenIdUserSignUpForm extends CustomUserSignUpForm {

    public function configure() {
parent::configure();
         unset(
            $this['username'],
            $this['password'],
            $this['cpassword'],
            $this['currency_id']
        );

        //widgets

       // $this->widgetSchema['byPass'] = new sfWidgetFormInputHidden(array('default' => '1'));

        $this->widgetSchema['name'] = new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'name', 'maxlength' => '30'));

        $this->widgetSchema['lname'] = new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'lname', 'maxlength' => '30'));

        $this->widgetSchema['mobileno'] = new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'mobileno', 'maxlength' => '20'));

        $this->widgetSchema['email'] = new sfWidgetFormInputText(array(), array('readonly' => 'readonly',  'class' => 'readonly', 'name' => 'email',
                    'maxlength' => '128'));

//        $this->widgetSchema['currency_id'] = new sfWidgetFormChoice(array('multiple' => false, 'choices' => $CurrencyArray), array('add_empty' => 'false'));
        $this->widgetSchema['dob'] =   new widgetFormDateCal(array('format' => '%d-%m-%Y'), array('class' => 'txtfield-r', 'readonly' => 'readonly', 'name' => 'dob'));
        $this->widgetSchema['address'] =   new sfWidgetFormTextarea(array(), array('class' => 'txtfield-r', 'name' => 'address', 'maxlength' => '255',));
        $this->widgetSchema['workphone'] =   new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'workphone', 'maxlength' => '20',));
        $this->widgetSchema['captcha'] =  new sfWidgetFormInput(array(), array('class' => 'txtfield-r'));
        $this->widgetSchema['authorization_consent'] =  new sfWidgetFormInputCheckbox();
        

        $this->setDefault("name", $this->getOption('name'));
        $this->setDefault("lname", $this->getOption('lname'));
        $this->setDefault("email", $this->getOption('email'));
       
        //validators
        $this->validatorSchema['name'] = new sfValidatorRegex(
                        array('pattern' => '/[a-zA-Z]/', 'required' => true, 'max_length' => '30', 'min_length' => 2),
                        array('invalid' => 'First Name Should be characters only', 'required' => 'Please enter First Name', 'min_length' => 'Minimum 2 characters',
                            'max_length' => 'Maximum 30 characters'));

        $this->validatorSchema['lname'] = new sfValidatorRegex(
                        array('pattern' => "/[a-zA-Z]/", 'required' => true, 'max_length' => '30', 'min_length' => 2),
                        array('invalid' => 'Last Name Should be characters only', 'required' => 'Please enter Last Name', 'min_length' => 'Minimum 2 Characters',
                            'max_length' => 'Maximum 30 characters'));

        $this->validatorSchema['mobileno'] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^([+]{1})([0-9]{10,15})$/', 'max_length' => 20),
                        array('max_length' => 'Maximum 20 Characters', 'invalid' => 'Please enter valid Mobile No.'));

////        $this->validatorSchema['currency_id'] = new sfValidatorChoice(array('required'=> false,'choices' => array_keys($CurrencyArray),)
//                        );
        $this->validatorSchema['address'] = new sfValidatorString(array('required' => false));
        $this->validatorSchema['workphone'] = new sfValidatorString(array('required' => false));
        $this->validatorSchema['captcha'] = new sfValidatorSfCryptoCaptcha(array('required' => true, 'trim' => true), array('wrong_captcha' => 'Please enter Valid Verification Code',
                'required' => 'Please enter Verification Code.'));
        $this->validatorSchema['authorization_consent']       = new sfValidatorBoolean( array('required' => true) );
        
        $this->validatorSchema['email'] = new sfValidatorAnd(array(
                new sfValidatorEmail(array('required' => true, 'trim' => true), array('invalid' => 'Please Enter Valid Email Adrress')),
                new sfValidatorString(array('required' => true, 'max_length' => 128)),
                new sfValidatorDoctrineUnique(array('model' => 'UserDetail', 'column' => 'email'),
                        array('invalid' => 'E-mail Address already exists'))),
                    array(), array('required' => 'Please enter Email'));
        $this->validatorSchema['dob'] = new sfValidatorDate(array('required' => false));

        //labels
        $this->widgetSchema->setLabels(array(
            'name' => 'First Name',
            'lname' => 'Last Name',
            'mobileno' => 'Mobile No.',
            'email' => 'Email Address',
            'currency_id' => 'Primary Currency',
            'dob' => 'Date of Birth',
            'workphone' => 'Work Phone',
            'captcha' => 'Verification Code',
            'authorization_consent' => " I authorize pay4me to authenticate my pay4me account using ".$this->getOption('openid_type')." credentials",
        ));
        $this->widgetSchema->setNameFormat('payOptions[%s]');
    }


}

?>
