<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class MerchantAcctReconcilationSchForm extends sfform {

    public function configure() {


        $merchantObj = Doctrine::getTable('Merchant')->findAll();
        $merchantArray = array();
        $merchantArray[''] = 'Please select Merchant';
        foreach ($merchantObj as $value) {
            $merchantArray[$value->getId()] = $value->getName();
        }


        $this->setWidget('merchant_id',
                new sfWidgetFormChoice(array
                    ('label' => 'Merchant',
                    'choices' => $merchantArray // Organization expected = 2
                )));

        $this->setWidget('merchant_service_id',
                new sfWidgetFormChoice(array
                    ('label' => 'Merchant Service',
                    'choices' => array('' => 'Please select Merchant Service') // Organization expected = 2
                )));

        $this->setWidget('payment_mode_option_id',
                new sfWidgetFormChoice(array
                    ('label' => 'Payment Mode Option',
                    'choices' => array('' => 'Please select Payment Option') // Organization expected = 2
                )));
        $this->setWidget('reconcile', new sfWidgetFormInputHidden(array('default' => 'immediate'), array('id' => 'reconcilation_type_row')));
    }

}

?>
