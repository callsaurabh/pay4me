<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomRechargeEwalletDraftSearchForm extends CustomRechargeEwalletSearchForm {

    public function configure() {

        parent::configure();
        $this->widgetSchema['sort_code'] = new sfWidgetFormInputText(array('label'=>'Sort Code<sup>*</sup>'), array('name' => 'sort_code', 'maxlength' => '3', 'minlength'=> '3'));
        $this->widgetSchema['draft_number'] = new sfWidgetFormInputText(array('label' => 'Bank Draft Number<sup>*</sup>'), array('maxlength' => 30));
        $this->widgetSchema['disclaimer_draft_details_recharge'] =  new sfWidgetFormInputCheckbox();

        $this->validatorSchema['disclaimer_draft_details_recharge']       = new sfValidatorBoolean(  array('required' => true) );


        $this->widgetSchema->setLabels(array(
            'disclaimer_draft_details_recharge'      => 'I hereby acknowledge that the information provided above in Bank Draft Details is true and correct.'
            ));
    }

}

?>