<?php

class MerchantExistForm extends sfform {

    public function setup() {
        sfWidgetFormSchema::setDefaultFormFormatterName('dl');
    }

    public function configure() {
        $this->setWidgets(array(
            'merchantname' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'merchantname', 'maxlength' => '50')),
            'merchantcode' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'merchantcode', 'maxlength' => '30')),
            'merchantkey' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'merchantkey', 'maxlength' => '40')),

        ));

        $this->setValidators(array(
            'merchantname' => new sfValidatorRegex(
                    array('pattern' => '/^[a-zA-Z0-9\.\-]+[a-zA-Z 0-9\.\-]+[a-zA-Z0-9\.\-]*$/', 'required' => true, 'max_length' => '50', 'min_length' => 2),
                    array('invalid' => 'Merchant Name is Invalid', 'required' => 'Please enter Merchant Name', 'min_length' => 'Minimum 2 characters', 'max_length' => 'Maximum 50 characters')),


            'merchantcode' => new sfValidatorRegex(array('pattern' => '/[a-zA-Z0-9]+$/', 'required' => true, 'max_length' => 30,), array('max_length' => 'Maximum 30 Characters', 'invalid' => 'Please enter valid Merchant code')),
            'merchantkey' => new sfValidatorRegex(array('pattern' => '/[a-zA-Z0-9]+$/', 'required' => true, 'max_length' => 40,), array('max_length' => 'Maximum 40 Characters', 'invalid' => 'Please enter valid Merchant key')),
        ));

        $this->validatorSchema->setOption('allow_extra_fields', true);

        $this->widgetSchema->setLabels(array(
            'merchantname' => 'Merchant Name',
            'merchantcode' => 'Merchant Code',
            'merchantkey' => 'Merchant Key',
        ));
    }

}

?>