<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomEpActionAuditEventReportsForm extends sfform {

    public function configure() {

        $this->setWidget('category',
                new sfWidgetFormChoice(array
                    ('label' => 'Category:',
                    'choices' => $this->getOption('audit_obj') // Organization expected = 2
                )));

        $this->setWidget('subCategory',
                new sfWidgetFormChoice(array
                    ('label' => 'SubCategory:',
                    'choices' => array('' => '-- All SubCategories --') // Organization expected = 2
                )));
        if ($this->getOption('country_head') === true) {


            $choices = array('bank_branch' => 'Bank/Branch', 'username' => 'Username', 'bank_country_head' => 'Bank Country Head');
        } else {
            $choices = array('bank_branch' => 'Bank/Branch', 'username' => 'Username');
        }
        $this->setWidget('selectBy',
                new sfWidgetFormChoice(array
                    ('expanded' => 'true', 'label' => 'Search By:',
                    'choices' => $choices
                )));
        $this->getWidget('selectBy')->setDefault('bank_branch');
        $this->widgetSchema['startDate'] = new widgetFormDateCal(array('format' => '%m/%d/%Y', 'label' => 'Start Date<sup>*</sup>'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));

        $this->widgetSchema['endDate'] = new widgetFormDateCal(array('format' => '%m/%d/%Y', 'label' => 'End Date<sup>*</sup>'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));
    }

}

?>
