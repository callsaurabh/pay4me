<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomBankBranchSearchIndexForm extends sfform {

    public function configure() {
        if ($this->getOption('merchant') == '') {

            $merchantArray = $this->getOption('merchantArray');
            $this->widgetSchema['merchant_id'] = new sfWidgetFormChoice(array('label' => 'Merchant<font class=required>&nbsp;*</font>', 'choices' => $merchantArray), array('style' => 'width:250px'));
            $this->widgetSchema['search_flag'] = new sfWidgetFormInputHidden(array('default' => ''));
            $this->validatorSchema['merchant_id'] = new sfValidatorString(array('required'=>true,'min_length'=>60), array('required'=>'Please Select Merchant','min_length'=>'truejjjjjjjjjjjjjjjjjjjj'));



        } else {

           $this->widgetSchema['search_flag'] = new sfWidgetFormInputHidden(array('default' => 1));
           $this->widgetSchema['merchant_id'] = new sfWidgetFormInputHidden(array('default' => $this->getOption('merchant')));
      }
        $this->widgetSchema['bank'] =           new sfWidgetFormChoice(array('label' => 'Bank', 'choices' => $this->getOption('bank_choices'), 'default' => ''), array('style' => 'width:250px'));
        $this->widgetSchema['state'] =          new sfWidgetFormChoice(array('label' => 'State', 'choices' => array('' => 'All States')), array('style' => 'width:250px'));
        $this->widgetSchema['lga'] =            new sfWidgetFormChoice(array('label' => 'LGA', 'choices' => array('' => 'All Lgas')), array('style' => 'width:250px'));
        $this->widgetSchema['branch_code'] =    new sfWidgetFormInputText(array('label' => 'Branch Code'), array('style' => 'width:250px'));

        
    }

}

?>
