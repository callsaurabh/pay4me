<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomBankUserForm extends sfform {

    public function setup() {
        sfWidgetFormSchema::setDefaultFormFormatterName('dl');
    }

    public function configure() {
        $add_superScipt = "<sup>*</sup>";
        $add_superScipt_to_username = "<sup>*</sup>";
        if ($this->getOption('edit') === true) {
            $readonly = true;
            $disabled = false;
            $add_superScipt_to_username = null;
            $label = "Selected Bank Branch";

            $this->widgetSchema['userId'] = new sfWidgetFormInputHidden(array('default' => $this->getOption('userid')));
        } else if ($this->getOption('edit') === false) {
            $readonly = false;
            $disabled = false;
            $label = 'Select Bank Branch<sup>*</sup>';
        }

        if ($this->getOption('display') == true) {

            $readonly = false;
            $disabled = false;
            $add_superScipt = null;
            $add_superScipt_to_username = null;
            $label = 'Select Branch<sup>*</sup>';
            $this->widgetSchema['userId'] = new sfWidgetFormInputHidden(array('default' => $this->getOption('userid')));
        }

        $bankBranch_obj = bank_branchServiceFactory::getService(bank_branchServiceFactory::$TYPE_BANK);
        $choice_array_for_Branch = array('' => 'Please select Bank Branch');
        $choice_array_for_Branch = $choice_array_for_Branch + $bankBranch_obj->getBankBranchList($this->getOption('bank_id'), $this->getOption('country_id'));

        $this->widgetSchema['bank_branch_id'] = new sfWidgetFormChoice(array('label' => $label, 'choices' => $choice_array_for_Branch));
        if ($this->getOption('branch_id')) {
            $this->getWidget('bank_branch_id')->setDefault($this->getOption('branch_id'));
        }


        if ($this->getOption('edit') === true) {
            $this->widgetSchema['bank_branch_id']->setAttributes(array('disabled' => 'true'));
        }
        $this->widgetSchema['username'] = new sfWidgetFormInputText(array('label' => 'Username' . $add_superScipt_to_username), array('class' => 'FieldInput', 'maxlength' => 30, 'readonly' => $readonly));
        $this->widgetSchema['bankname'] = new sfWidgetFormInputHidden(array('default' => $this->getOption('bank_name')));
        $this->widgetSchema['address'] = new sfWidgetFormTextarea(array('label' => 'Address'), array('type' => 'textarea', 'class' => 'FieldInput', 'maxlength' => 255));
        $this->widgetSchema['name'] = new sfWidgetFormInputText(array('label' => 'Name' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 30));
        $this->widgetSchema['email'] = new sfWidgetFormInputText(array('label' => 'Email' . $add_superScipt), array('class' => 'FieldInput', 'maxlength' => 30));
        $this->widgetSchema['mobile_no'] = new sfWidgetFormInputText(array('label' => 'Mobile Number'), array('class' => 'FieldInput', 'maxlength' => 50));
        $this->widgetSchema['grp'] = new sfWidgetFormInputHidden(array('default' => $this->getOption('grp')));
        $this->widgetSchema['dob'] = new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'Date of Birth'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));

        $this->validatorSchema['name'] = new sfValidatorString(array('required' => true), array('required' => 'Please Enter Name'));
        $this->validatorSchema['username'] = new sfValidatorString(array('required' => true), array('required' => 'Please Enter Username'));
        $this->validatorSchema['email'] = new sfValidatorRegex(array('pattern' => '/^[A-Za-z0-9_\.-]+$/', 'max_length' => 30,
                    'required' => true), array('required' => 'Please enter email address', 'invalid' => 'Enter a valid email Address', 'max_length' => 'Maximum 30 Characters'));
        $this->validatorSchema['mobile_no'] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^\+{1}[0-9]{10}$/', 'max_length' => 11,
                        ), array('invalid' => 'Enter a valid Mobile Number(starting with "+")', 'max_length' => 'Maximum 11 Characters'));
        $this->validatorSchema['address'] = new sfValidatorString(array('required' => false, 'max_length' => 255), array('max_length' => 'Maximum 255 characters'));
        $this->validatorSchema['dob'] = new sfValidatorDate(array('required' => false, 'date_output' => '%d-%m-%Y', 'max' => strtotime(date('d-m-Y'))), array('bad_format' => 'Invalid Date', 'max' => "Date of Birth cannot be future or today's date"));

        $this->validatorSchema->setOption('allow_extra_fields', true);

        $this->disableCSRFProtection();
    }

}

?>
