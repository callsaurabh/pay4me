<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class customCommandForm extends sfform {

    public function configure() {


        
        $this->widgetSchema['command']= new sfWidgetFormTextarea(array(),array('maxlength'=>500, 'class'=>'FieldInput'));
            
        $this->widgetSchema->setLabels(array(
            'command' => 'Command <sup class="cRed">*</sup>',
        ));

         $this->validatorSchema['command'] = new sfValidatorstring(array('required'=>true, 'max_length'=>1000));

         
    }

}

?>
