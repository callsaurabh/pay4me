<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomKycForm extends sfform {

    public function configure() {

        $this->setWidget('image', new
                sfWidgetFormInputFile(
                        array('label' => 'Upload Passport Size Self  Image<sup>*</sup>'),
                        array('is_image' => true,
                            'class=FieldInput', 'onchange' => 'uploadImageTemp()')));
        $this->setWidget('chkImage', new sfWidgetFormInputCheckbox(array('label' => ' '), array('disabled' => true)));

        $this->setWidget('doc1', new sfWidgetFormInputFile(array('label' => 'Upload Document<sup>*</sup><br>(Issued by any government body'), array('onchange' => 'uploadDocTemp(1)')));
//$this->setWidget('file-info',new sfWidgetFormInput(array('label'=>' '),array('readonly'=>'readonly','type'=>'image','src'=>'/firstbank.jpg')));

        $this->setWidget('chkDoc1', new sfWidgetFormInputCheckbox(array('label' => ' '), array('disabled' => true)));
        $this->setWidget('confirmUpload', new sfWidgetFormInputCheckbox(array('label' => ' ')));
        $this->setWidget('label', new sfWidgetFormInput(array('type' => 'text', 'label' => "<font color='gray>I accept and confirm that the uploaded image and documents are correct</font>")));

        $this->widgetSchema['byPass'] = new sfWidgetFormInputHidden(array('default' => 1));
        $this->widgetSchema['tmpImgName'] = new sfWidgetFormInputHidden(array('default' => ''));
        $this->widgetSchema['tmpDocName'] = new sfWidgetFormInputHidden(array('default' => ''));
        $this->widgetSchema['totDocSize'] = new sfWidgetFormInputHidden(array('default' => 0));
        $this->widgetSchema['totdoc'] = new sfWidgetFormInputHidden(array('default' => '1'));
        $this->widgetSchema['totsize'] = new sfWidgetFormInputHidden(array('default' => '0'));
    }

}

?>
