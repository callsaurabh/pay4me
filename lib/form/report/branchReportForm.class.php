<?php

/**
 * ScheduledPaymentConfig form.
 * @package    form
 * @subpackage ScheduledPaymentConfig
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BranchReportForm extends sfform {

    public function configure() {
        $userGroup = $this->getOption('userGroup');
        $report_label = $this->getOption('report_label');
        $selBankChoices = array('' => '-- All Banks --');
        if ($userGroup == sfConfig::get('app_pfm_role_admin') || $userGroup == sfConfig::get('app_pfm_role_report_admin') || isset($user_group_arr[sfConfig::get('app_pfm_role_report_portal_admin')])) {
            $bankArray = array();
            $originalAttribute = Doctrine_Manager::getInstance()->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
            $bankList = Doctrine::getTable('Bank')->getAllRecords()->execute(array(), Doctrine::HYDRATE_ARRAY);
            Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $originalAttribute);
            if (count($bankList) > 0) {
                foreach ($bankList as $key => $val) {
                    $bankArray[$val['id']] = $val['bank_name'];
                }
            }
            $banks = $selBankChoices + $bankArray;
        }
        if ($userGroup == sfConfig::get('app_pfm_role_bank_admin') || $userGroup == sfConfig::get('app_pfm_role_report_bank_admin')) {
            $bank_details = $pfmHelperObj->getUserAssociatedBank();
            $banks = array($bank_details['bank_name']=>$bank_details['bank_name']);
        }
        
        $this->setWidgets(array(
             'banks' => new sfWidgetFormChoice(array('choices' => $banks)),
         
            'from' => new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'Date of Birth'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input')),
            'to' => new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'Date of Birth'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input')),
            'report_label' => new sfWidgetFormInputHidden(array(), array('value' => $report_label)),
        ));
        $this->widgetSchema->setLabels(array(
            'banks' => 'Bank:',
            'from' => 'From Date:',
            'to' => 'To Date:',
        ));
        $this->setValidators(array(
            'banks' => new sfValidatorString(array('required' => false)),
            'from' => new sfValidatorString(array('required' => false)),
            'to' => new sfValidatorString(array('required' => false)),
        ));
    }
}