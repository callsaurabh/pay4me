<?php

/**
 * ScheduledPaymentConfig form.
 * @package    form
 * @subpackage ScheduledPaymentConfig
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EWalletRechargeReportForm extends sfform {

    public function configure() {
        $entryType = array();
        $currency = array();
        $currency = $this->getOption('currency');
        $pfmHelperObj = new pfmHelper();
            $paymentModeCheckId = $pfmHelperObj->getPMOIdByConfName('Cheque');
            $paymentModeDraftId = $pfmHelperObj->getPMOIdByConfName('bank_draft');
            $paymentModebankId = $pfmHelperObj->getPMOIdByConfName('bank');
            $paymentModeCheckName = pfmHelper::getChequeDisplayName();
            $paymentModeDraftName = pfmHelper::getDraftDisplayName();
        $entryType = array('both' => 'both (credit and debit)', 'credit' => 'credit', 'debit' => 'debit');
        $paymentMode = array('' => '-- All Payment Modes --',$paymentModebankId => 'Bank',
            $paymentModeCheckId => $paymentModeCheckName, $paymentModeDraftId => $paymentModeDraftName);
        $this->widgetSchema['user_name'] = new sfWidgetFormInputText(array(), array('maxlength' => 30, 'class' => 'fieldinput1'));
        $this->widgetSchema['wallet_no'] = new sfWidgetFormInputText(array(), array('maxlength' => 30, 'class' => 'fieldinput1'));
        $this->widgetSchema['currency'] = new sfWidgetFormChoice(array('choices' => $currency), array('title' => 'Curency'));
        $this->widgetSchema['entry_type'] = new sfWidgetFormChoice(array('choices' => $entryType), array('title' => 'Curency'));
        $this->widgetSchema['payment_mode'] = new sfWidgetFormChoice(array('choices' => $paymentMode), array('title' => 'Payment Mode'));
        $this->widgetSchema['from'] = new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'From Date'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));

        $this->widgetSchema['to'] = new widgetFormDateCal(array('format' => '%d-%m-%Y', 'label' => 'To Date'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));

        $this->widgetSchema->setLabels(array(
            'user_name' => 'Search By Bank Teller (Username)',
            'wallet_no' => 'eWallet Account No',
            'currency' => 'Currency',
            'entry_type' => 'Entry Type',
            'payment_mode' => 'Payment Mode',
            'from' => 'From Date',
            'to' => 'To Date',
        ));

        $this->validatorSchema->setPostValidator(new sfvalidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'validateDates')), array()))));

        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function validateDates($validator, $values) {
        $values = $this->getTaintedValues();
        if ($values['from_date'] != '') {

            if (strtotime($values['from_date']) > strtotime(date('d-m-Y'))) {

                $error['from'] = new sfValidatorError($validator, 'From Date cannot be Future Date');
            }
        }if ($values['to_date'] != '') {

            if (strtotime($values['to_date']) > strtotime(date('d-m-Y'))) {

                $error['to'] = new sfValidatorError($validator, 'To Date cannot be Future Date');
            }
        }
        if ($values['from_date'] != '' && $values['to_date'] != '') {

            if (strtotime($values['from_date']) > strtotime($values['to_date'])) {

                $error['to'] = new sfValidatorError($validator, 'To Date should be greater than From Date');
            }
        }


        if (!empty($error)) {

            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

}