<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of ReportListForm.class
 *
 * form created for Quarterly, monthly,weekly dropdown
 * 
 * @author Deepak Bhardwaj
 */
class MerchantPerBankReportForm extends sfform {
    
    public function configure()
    {
    
       $add_empty = "--All Merchants--";
       $report_label = $this->getOption('report_label');
       $this->setWidgets(array(
            'merchant' => new sfWidgetFormDoctrineChoice(
                    array('model' => 'Merchant',
                        'query' => Doctrine::getTable('Merchant')->getServiceTypeOptionArr('', 'yes','','',''),
                        'add_empty' => $add_empty), array('style' => 'width:250px', 'onchange' => 'set_service_type()')),
            'service_type' => new sfWidgetFormChoice(array('choices' => array('' => '-- All Services --'), 'expanded' => false), array('onchange' => "")),
       		'report_label' => new sfWidgetFormInputHidden(array(), array('value' => $report_label)),
            ));
       
    }

}
?>
