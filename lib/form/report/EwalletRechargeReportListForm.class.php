<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EwalletRechargeReportListForm.class
 *
 * @author ramandeep
 */
class EwalletRechargeReportListForm extends sfform {
    
    public function configure()
    {
        $quarter_list = array(1=>'January-March',2=>'April-June',3=>"July-September",4=>"Octuber-December");
        $month_list = array('01'=>'January','02'=>'Febuary','03'=>'March','04'=>'April',
                '05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September',
                '10'=>'Octuber','11'=>'November','12'=>'December'
                );
        $week_list = array(1=>'First',2=>'Second',3=>"Third",4=>"Fourth");
        $frequency_list = array(''=>'Please Select','QUARTERLY'=>'Quarterly','WEEKLY'=>'Weekly','MONTHLY'=>'Monthly','CUSTOM'=>'Custom');
        
        
        $this->widgetSchema['quarter']   = new sfWidgetFormChoice(array('choices' => $quarter_list));
        $this->widgetSchema['month']     = new sfWidgetFormChoice(array('choices' => $month_list));
//        $this->widgetSchema['week']      = new sfWidgetFormChoice(array('choices' => $week_list));
        $this->widgetSchema['frequency'] = new sfWidgetFormChoice(array('choices' => $frequency_list),array('onchange'=>'showhide(this.value)'));
        
        
        $this->widgetSchema->setLabels(array(
            'frequency'=>'Frequency',
            'quarter'=>'Quarter',
            'month'  =>'Month',
//            'week'   =>'Week'
            ));
        
    }

}
?>
