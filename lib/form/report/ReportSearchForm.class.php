<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportListForm.class
 *
 * form created for Quarterly, monthly,weekly dropdown
 * 
 * @author ramandeep
 */
class ReportSearchForm extends sfform {
    
    public function configure()
    {
        $quarterList = array(1=>'January-March',2=>'April-June',3=>"July-September",4=>"October-December");
        $monthList = array('01'=>'January','02'=>'February','03'=>'March','04'=>'April',
                '05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September',
                '10'=>'October','11'=>'November','12'=>'December'
                );
        $weekList = array(1=>'First',2=>'Second',3=>"Third",4=>"Fourth");
        $frequencyList = array(''=>'Please Select','QUARTERLY'=>'Quarterly','WEEKLY'=>'Weekly','MONTHLY'=>'Monthly','CUSTOM'=>'Custom');
        $type_list = array('Generated'=>'Generated','Archive'=>'Archived','Pending'=>'Pending');
        
        $this->widgetSchema['quarter']   = new sfWidgetFormChoice(array('choices' => $quarterList));
        $this->widgetSchema['month']     = new sfWidgetFormChoice(array('choices' => $monthList));
//        $this->widgetSchema['week']      = new sfWidgetFormChoice(array('choices' => $weekList));
        $this->widgetSchema['frequency'] = new sfWidgetFormChoice(array('choices' => $frequencyList,'default' => $this->getOption('frequency')),array('onchange'=>'showHide(this.value)'));
        $this->widgetSchema['type']      = new sfWidgetFormChoice(array('choices' => $type_list,'default' => $this->getOption('type')));
        
        $this->widgetSchema->setLabels(array(
            'frequency'=>'Frequency',
            'quarter'=>'Quarter',
            'month'  =>'Month',
            'type'  =>'Type',
//            'week'   =>'Week'
            ));
        
    }

}
?>
