<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomBankReportSchedulerForm extends sfform {

    public function configure() {

        $current_time = date('H:i:s', time());
        $this->widgetSchema['start_time'] = new sfWidgetFormTime(array('format' => '%hour%:%minute%:%second%', 'with_seconds' => true, 'can_be_empty' => false, 'default' => $current_time, 'label' => 'Start Time<sup>*</sup>'));
        $this->widgetSchema['end_time'] = new sfWidgetFormTime(array('format' => '%hour%:%minute%:%second%', 'with_seconds' => true, 'can_be_empty' => false, 'default' => $current_time, 'label' => 'End Time<sup>*</sup>'));
        $this->widgetSchema['bank_id'] = new sfWidgetFormChoice(array('label' => 'Select Bank<sup>*</sup>', 'choices' => $this->getOption('banks_list')));
        $this->validatorSchema['bank_id'] = new sfValidatorString(array('required' => true), array('required' => 'Please Select Bank'));
        $this->validatorSchema->setOption('allow_extra_fields', true);
        
    }

}

?>
