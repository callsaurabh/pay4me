<?php
/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class AddQnAForm extends EditFirstProfileForm {
  public function configure() {
    // do unsetting
    unset(
        $this['algorithm'],$this['salt'],$this['is_super_admin'],
        $this['updated_at'],$this['created_at'],
        $this['groups_list'],$this['permissions_list'],$this['is_active'],
        $this['username'],$this['password'],$this['confirm_password'], $this['last_login']
    );

    //$listOfQuestions = Doctrine::getTable('SecurityQuestion')->findAll(Doctrine::HYDRATE_ARRAY);
   
    $questionNumber =Settings::getNumberOfQuestion();
    $listOfQuestions = Doctrine::getTable('SecurityQuestion')->getRandomQuestion($questionNumber);
    
       
   
    $this->qnaCount  = count($listOfQuestions);
    for($i=0; $i< $this->qnaCount; $i++) {
        
      //$questionAns = new QnAForm($this->object->SecurityQuestionsAnswers[$i]);
      $questionAns = new QnAForm($this->object->SecurityQuestionsAnswers[$listOfQuestions[$i]['id']]);
      $questionAns->getWidget('security_question_text')->setAttributes(array('size'=>'30','readonly'=>'readonly','class'=>'loginHintQuestion','tabindex'=>-1));
      $questionAns->setDefault('security_question_text', $listOfQuestions[$i]['questions']."?");
      $questionAns->setDefault('security_question_id', $listOfQuestions[$i]['id']);
      $this->embedForm('qna'.$i, $questionAns);
    }
  }
}
?>