<?php

/**
 * EWalletLoginForm
 *
 * @package    form
 * @subpackage EWalletLoginForm
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EWalletLoginForm extends LoginForm {
     public function configure() {
        parent::$loginType='ewallet';
        parent::configure();
        //as ewallet accoutn now will open first as read only
//        if(!$this->getDefault('readonly')){
//         $this->widgetSchema['username']->setAttributes(array('readonly'=>true));  
//        }
    }
}