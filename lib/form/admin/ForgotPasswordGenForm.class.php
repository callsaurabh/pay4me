<?php

/**
 * Forgot Password form.
 *
 * @package    form
 * @subpackage Forgot
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ForgotPasswordGenForm extends sfForm {
    
    public function configure() {
                  
         $security_question = $this->getOption('security_question');
         //if fist time clcked show security question as hidden
         $this->widgetSchema['security_question_id'] = new sfWidgetFormInputHidden(array());
         if(isset($security_question) && $security_question){
             $data = sfContext::getInstance()->getRequest()->getParameter('forgot');

             $uname = $data['username'];
             $arrSecurityQuestion = $this->getSecurityQuestion($uname);
             $securityQuestionLabel = $arrSecurityQuestion['questions'];
             $this->widgetSchema['username'] = new sfWidgetFormInput(array('label'=>'Username <span class="required">*</span>'),array('title'=>'Username','lang'=> 'blank','class'=>'text_box_new'));
             $this->widgetSchema['username']->setAttributes(array('readonly'=>'readonly','value'=>$uname,'class'=>'text_box_new'));
             $this->widgetSchema['security_question_id']->setAttributes(array('readonly'=>'readonly','value'=>$arrSecurityQuestion['id']));
             $this->widgetSchema['security_question'] = new sfWidgetFormInputPassword(
                                array('type' => 'password',
                                'label'=>$securityQuestionLabel.' ? <span class="required">*</span>'),
                                array('lang'=> 'blank','title'=>"<b>".$securityQuestionLabel."</b>",'class'=>'text_box_new'));
         } else {
             $this->widgetSchema['username'] = new sfWidgetFormInput(array('label'=>'Username <span class="required">*</span>'),array('title'=>'Username','class'=>'text_box_new'));
             $this->widgetSchema['security_question'] = new sfWidgetFormInputHidden(array());            
         }
         
         $this->validatorSchema['username'] = new sfValidatorand(
            array(
               new sfValidatorString(
                                            array('required' => true),
                                            array('required'=>'Please enter Username')),
                new sfValidatorCallback(
                    array('callback'=>array($this,'checkUser'),'arguments' => array()))
            ),
            array('halt_on_error'=>true),
            array('required'=>'Please enter Username')

        );

        if(isset($security_question) && $security_question){
            
            $data = sfContext::getInstance()->getRequest()->getParameter('forgot');
            
            $uname = $data['username'];
            $this->validatorSchema['security_question'] = new sfValidatorand(
            array(
               new sfValidatorString(
                                            array('required' => true),
                                            array('required'=>'Please enter answer to your security question')),
                new sfValidatorCallback(
                    array('callback'=>array($this,'checkSecurityQuestion'),
                        'arguments' => array('username'=>$uname,
                            'security_question_id'=>$data['security_question_id'])))
            ),
            array('halt_on_error'=>true),
            array('required'=>'Please enter answer to your security question')

            );

        } else {
            $this->validatorSchema['security_question'] = new sfValidatorString(
                                            array('required' => false));
        }
        sfValidatorBase::setDefaultMessage('csrf_attack', 'This session has expired. Please return to home page and try again');
        $this->widgetSchema->setNameFormat('forgot[%s]');
        $this->validatorSchema->setOption('allow_extra_fields', true);

    }

   public function getSecurityQuestion($userName){
       $arrDetails= Doctrine::getTable('SecurityQuestion')->getUserRandomSecurityQuestion($userName);      
       return $arrDetails[0]['SecurityQuestion'];
   }
   public function checkSecurityQuestion($validator,$securityQuestionAnswer){       
       $arguments = $validator->getOption('arguments');      
       $userName =$arguments['username'];
       $security_question_id = $arguments['security_question_id'];      
       $arrDetails = Doctrine::getTable('SecurityQuestionsAnswers')->
               getSecurityQuestionAnswer($userName,$security_question_id);
       if($arrDetails[0]['answer']==$securityQuestionAnswer){
           return $securityQuestionAnswer;
       }
       $message = 'Information you provided didn\'t match the information in your profile';
       throw new sfValidatorError($validator, $message);
       return false;       
       


   }
    public function checkUser($validator,$form_vals) {
        $status = true;
        $q = Doctrine_Query::create()->select('*')
            ->from('sfGuardUser')
            ->andWhere('username=?',$form_vals)
            ->execute()
            ->toArray(Doctrine::HYDRATE_ARRAY);
        if(!count($q)) {
           $message = 'Username is not valid';
           throw new sfValidatorError($validator, $message);
           return false;
        }else{
            if(!$q[0]['password'] && !$q[0]['salt']){
                $message = 'This user is not authorized for this action.';
                throw new sfValidatorError($validator, $message);
                return false;
            }
        }
        
        $ansCount = Doctrine::getTable('SecurityQuestionsAnswers')->
                getAnswerCount($q[0]['id']);
        if($ansCount == 0 || $ansCount=="") {
           $message = 'You are not authorized to change your password. Please contact Pay4me Support';
           throw new sfValidatorError($validator, $message);
           return false;
        }
        $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($q[0]['id']);
        $name = $arrGroup['sfGuardGroup']['name'];
        if($this->isValidUserGroup($name)){
            $message = 'You are not authorized to change your password. Please contact Pay4me Support';
           throw new sfValidatorError($validator, $message);
           return false;
        }
        return $form_vals;
        
    }

    function isValidUserGroup($user) {        
    $arrayQuestionUser = array('support','portal_admin')  ;
       if(in_array($user,$arrayQuestionUser)){
        return true;
       }else{
        return false;
      }
    }
}