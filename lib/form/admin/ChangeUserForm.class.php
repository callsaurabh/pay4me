<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ChangeUserForm extends BasesfGuardUserForm
{


  public static function userVerifyPasswordCallBack($validator, $value, $arguments) {

    // this is my logged in user
    $user = sfContext::getInstance()->getUser();
    // TODO verify if following usage of == is correct
    if ($user == null) {
      throw new sfValidatorError($validator, 'invalid');
    }
    // TODO handle not logged in user properly
    if (!$user->isAuthenticated()) {
      throw new sfValidatorError($validator, 'invalid');
    }
    if (!$user->checkPassword($value)) {
      // password don't match
      throw new sfValidatorError($validator, 'invalid');
    }
    if($value==$_REQUEST['sf_guard_user']['password']){
      throw new sfValidatorError($validator, 'Current Password and New Password can not be same');
      //         throw new sfValidatorError($validator, 'invalid');
    }
    return $value;
  }
  public static function matchNewAndConfirmPasswordCallBack($validator, $value, $arguments){
    if(!preg_match('/[^\s+]/' ,$_REQUEST['sf_guard_user']['password']) ){
      throw new sfValidatorError($validator, 'White spaces are not allowed');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) < 8){
      throw new sfValidatorError($validator, 'Minimum 8 characters required');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) > 20){
      throw new sfValidatorError($validator, 'Password can not be more than 20 characters');
    }
    if($_REQUEST['sf_guard_user']['password'] != $_REQUEST['sf_guard_user']['confirm_password']){
      throw new sfValidatorError($validator, 'New Password and Confirm Password do not match');
    }
    return $value;

  }
  public function configure()
  {
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list'],
      $this['is_active'],$this['username']
    );
    $this->widgetSchema['old_password'] = new sfWidgetFormInputPassword(array(),array('class'=>'keypad'));
    $this->validatorSchema['old_password'] = new sfValidatorCallback(array(
        'callback'  => 'ChangeUserForm::userVerifyPasswordCallBack'));
    $this->validatorSchema['old_password']->setOption('required',true);
    $this->validatorSchema['old_password']->setMessage('required','Current Password is required');
    $this->validatorSchema['old_password']->setMessage('invalid','Current password is incorrect');




    //working with changing the passwords
    $this->widgetSchema['password']= new sfWidgetFormInputPassword(array(),array('class'=>'keypad'));
    $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword(array(),array('class'=>'keypad'));
    $this->validatorSchema['password'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20 ,'min_length' => 8),
          array('max_length' => 'Password can not be more than 20 characters','required' => 'Please Enter your New Password','min_length' => 'Minimum 8 character required')),
        new sfValidatorRegex(array('pattern' => '/[^\s+]/'),array(
               'invalid' => 'White spaces are not allowed.'))
      ));
    $this->validatorSchema['password'] = new sfValidatorCallback(array(
        'callback'  => 'ChangeUserForm::matchNewAndConfirmPasswordCallBack'));
    $this->validatorSchema['password']->setOption('required',true);
    $this->validatorSchema['password']->setMessage('required','Password is required');
    
    //    $this->validatorSchema['confirm_password'] = new sfValidatorAnd(array(
    //            new sfValidatorString(array('max_length' => 20 , 'min_length' => 8),
    //            array('max_length' => 'Password can not be more than 20 characters',
    //            'required' => 'Please Re-enter your new password','min_length' => 'Minimum 8 character required')),
    //            new sfValidatorRegex(array('pattern' => '/[^\s+]/'),array(
    //               'invalid' => 'White spaces are not allowed.'))
    //          ));
    //    $this->validatorSchema['confirm_password']->setOption('required',true);
    //    $this->validatorSchema['confirm_password']->setMessage('required','Confirm Password is required');

    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('required' => true),
      array('required' => 'Please Re-enter your new password'));

    //    if(isset($_REQUEST['sf_guard_user']['old_password']) && $_REQUEST['sf_guard_user']['old_password'] &&
    //        isset($_REQUEST['sf_guard_user']['password']) && $_REQUEST['sf_guard_user']['password'] &&
    //        isset($_REQUEST['sf_guard_user']['confirm_password']) && $_REQUEST['sf_guard_user']['confirm_password']
    //        )
    //
    //    {
    //        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
    //
    //
    //
    //          new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
    //                  array(), array('invalid' => 'New Password and Confirm Password do not match')
    //          ))
    //      ));
    //    }

    // Set Labels
    $this->widgetSchema->setLabels(
      array(    'old_password'    => 'Current Password',
      'password'    => 'New Password','confirm_password'    => 'Confirm Password'));

    $this->widgetSchema->setHelps(array(
            'password'  => '<br /> Password Strength&nbsp;&nbsp;&nbsp; <br /><span id="strength"></span>'
      ));
  }

  public function save($conn = null) {
    parent::doSave($conn);
    sfContext::getInstance()->getUser()->setPassword($this->values['password']);
  }

  public function configureGroups()
  {
    $this->uiGroup = new Dlform();

    $newUserDetails = new FormBlock('Change Password');
    $this->uiGroup->addElement($newUserDetails);

    $newUserDetails->addElement(array('old_password','password','confirm_password'));
  }
}