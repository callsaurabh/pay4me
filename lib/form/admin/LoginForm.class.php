<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class LoginForm extends sfGuardFormSignin {

    protected $isFirstUser = false;
    public static  $loginType;
    
    public function configure() {
        //parent::configure();
         unset($this['remember'],$this['deleted_at']);

    $this->setWidgets(array(
      'username' => new sfWidgetFormInput(),
      'password' => new sfWidgetFormInput(array('type' => 'password')),
      //'remember' => new sfWidgetFormInputCheckbox(),
      ));

    $this->setValidators(array(
      'username' => new sfValidatorString(array(),array('required'=>'Username can\'t be empty')),
      'password' => new sfValidatorString(array(),array('required'=>'Password can\'t be empty')),
     // 'remember' => new sfValidatorBoolean(),
      ));
  $attr = array( 'onpaste'=>"return false",'ondrop'=>"return false", 'ondrag'=>"return false", 'oncopy'=>"return false", 'autocomplete'=>"off");
  $this->widgetSchema['username']->setAttributes($attr);
  $this->widgetSchema['password']->setAttributes($attr);
  $this->widgetSchema['userId']= new sfWidgetFormInputHidden();
    $this->validatorSchema->setPostValidator(new sfGuardValidatorUser());

    $this->widgetSchema->setNameFormat('signin[%s]');


        $this->validatorSchema->setOption('allow_extra_fields', true);

        //die(print_r(sfContext::getInstance()->getRequest()->getParameterHolder()));
        $data = sfContext::getInstance()->getRequest()->getParameter('signin');
        

        $uname = $data['username']; # user name
        $bname = sfContext::getInstance()->getRequest()->getParameter('bankName'); # bank name
        $this->isFirstUser = $this->checkFirstUser($uname,$bname,self::$loginType);

        $assocEmail = false;
        if(count(sfContext::getInstance()->getUser()->getAttribute('openId'))>0){
            $openidDetails = sfContext::getInstance()->getUser()->getAttribute('openId');
            if(isset($openidDetails['assocEmail']))
                $assocEmail = true;
        }


        if (count(sfContext::getInstance()->getUser()->getAttribute('openId'))==0 || $assocEmail) {
            $this->setDefaults(array('username'=>$uname));
        }
        if (count(sfContext::getInstance()->getUser()->getAttribute('openId'))>0) {
             if($data && array_key_exists('user_id', $data))
                $this->setDefaults(array('userId'=>$data['user_id']));
            else if($data && array_key_exists('userId', $data) && $data['userId'])
                $this->setDefaults(array('userId'=>$data['userId']));
        }
        
        $this->mergeQuestionForm($uname,$assocEmail);

    }

   protected function mergeQuestionForm($uname,$assocEmail) {
        if($this->isFirstUser) {
        }else {
            if (count(sfContext::getInstance()->getUser()->getAttribute('openId'))==0 || $assocEmail) {
                $this->getWidget('username')->setAttributes(array('readonly'=>'readonly'));
            }
            $questionAns = new QnAForm();

            $this->mergeForm($questionAns);
        }
    }

    public function checkFirstUser($uname,$bname,$loginType) {

        if($loginType=='bank')
        if(empty($uname) || empty($bname)) return false;
        //print_r('user: '.$uname.' : bank: '.$bname);
        # if first timeUser Skip Question validation
        $q = Doctrine_Query::create()->select('*')
            ->from('sfGuardUser')
            ->andWhere('username=?',$uname)
            ->andWhere("last_login is NULL")
            ->execute()
            ->toArray(Doctrine::HYDRATE_ARRAY);
//        print_r($q);
        //die('CheckFirstUser:'.$uname);
        if(count($q)) {
            return true;
        }
        return false;

    }
}