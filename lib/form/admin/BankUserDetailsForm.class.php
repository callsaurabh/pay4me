<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class BankUserDetailsForm extends BaseUserDetailForm
{
    public function configure()
    {
       
        unset($this['last_name'],$this['work_phone'],$this['send_sms'],$this['failed_attempt'],
            $this['user_status'],$this['kyc_status'],$this['disapprove_reason'],$this['created_at'],
            $this['updated_at'],$this['deleted_at'],$this['created_by'],$this['updated_by'],$this['user_id'],$this['master_account_id'],$this['openid_auth']);


        $this->widgetSchema['dob']                  = new widgetFormDateCal(array(),array('readonly'=>'true','class'=>'txt-input'));
        $this->widgetSchema['address']              = new sfWidgetFormTextarea(array(),array('class'=>'txt-input'));
        if($this->getOption('email')!='')
           $this->widgetSchema['email']                  =  new sfWidgetFormInputText(array(),array('value'=>$this->getOption('email'),'maxlength' => 60,'class'=>'fieldinput1'));
        else
          $this->widgetSchema['email']                  =  new sfWidgetFormInputText(array(),array('maxlength' => 60,'class'=>'fieldinput1'));
        $this->validatorSchema['name']              = new sfValidatorString(array('max_length' => 255, 'required' => true),array('required'=>'Please enter Name','max_length'=>'Name is too long (%max_length% characters max)'));
        $this->validatorSchema['dob']               = new sfValidatorString(array('required' => false));

        $this->validatorSchema['address']           = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Maximum 255 Characters'));
       /*
        * report_admin and e_auditor condiotion are added on WP029 inhousemaintainace by ryadav
        */
        if($this->getOption('userType')!='bank_admin' && $this->getOption('userType')!='report_admin' &&  $this->getOption('userType')!='e_auditor'){
         
            $this->validatorSchema['email']             = new sfValidatorRegex(array('max_length' => 60, 'required' => true, 'pattern'=>'/^[a-zA-Z0-9._-]+$/'),array('required'=>'Please enter Email','invalid'=>'Please enter a valid Email.','max_length'=>'Maximum 60 characters'));
        } else{
           
            $this->validatorSchema['email']             = new sfValidatorEmail(array('max_length' => 60, 'required' => true),array('required'=>'Please enter Email','invalid'=>'Please enter valid Email','max_length'=>'Maximum 60 characters'));
        }
        $this->validatorSchema['mobile_no']         = new sfValidatorRegex(array('pattern' => '/^(\+)(\d){10,14}?$/','required'=>false),array(
               'invalid' => 'Please enter Valid Mobile number'));


        if($this->getOption('action') == 'edit'){

             $getbankDomain = Doctrine::getTable('Bank')->getBankDomain($this->getOption('bankId'),$this->getOption('countryId'));
             $bank_domain = $getbankDomain['domain'];
         $helpArray = array(
            'email'       => $bank_domain,
            'address'       => '(Upto 255 characters only)',
            'mobile_no'       => '(format: + [10-14] digit no, eg: +1234567891 )'
                );
        if($this->getOption('userType')=='bank_admin')
            unset($helpArray['email']);

            $this->widgetSchema->setHelps($helpArray);
        }else{
            $this->widgetSchema->setHelps(array(
            'email'       => "<span id=\"domain_suffix\"></span>",
            'address'       => '(Upto 255 characters only)',
            'mobile_no'       => '(format: + [10-14] digit no, eg: +1234567891 )'
                ));}
        $this->widgetSchema->setLabels(array(
            'dob'    => 'Date of Birth',
            'mobile_no'   => 'Mobile Number'
            ));
        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback'=>array($this,'CheckDob'))))));

    }

      public function CheckDob($validator,$values){//print strtotime($values['dob']);print "<br>".strtotime(date('d-m-Y'));exit;
        if($values['dob']!="") {
        $dob = strtotime($values['dob']);
        $currentDate =  strtotime(date('Y-m-d'));
   
        if($dob >= $currentDate){ 
            $error = new sfValidatorError($validator, "Date of Birth cannot be future or today's date");
            throw new sfValidatorErrorSchema($validator, array('dob' => $error));
          }
      }
        return $values;
        
      }
}
?>
