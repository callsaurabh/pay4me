<?php

/**
 * BankLoginForm form.
 *
 * @package    form
 * @subpackage BankLogin
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BankLoginForm extends LoginForm {
    public function configure() {                   
         parent::$loginType='bank';
        parent::configure();
    }

}