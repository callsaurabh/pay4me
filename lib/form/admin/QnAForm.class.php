<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class QnAForm extends BaseSecurityQuestionsAnswersForm
{

    public function configure()
    {

       // do unsetting
        unset(
            $this['id'],$this['updated_at'],$this['created_at'],
            $this['updated_by'],$this['created_by'], $this['deleted_at'],
            $this['user_id']
        );

       $requestHolder = sfContext::getInstance()->getRequest()->getParameterHolder();
        $yesToCheck = 0;
        if($requestHolder->has('signin')){
          $siginArray = $requestHolder->get('signin');
          if($siginArray['username']){
              $yesToCheck = 1;
          }
        }


      $userId='';
      if($yesToCheck){
           $userId = $this->checkUserDetail($siginArray['username']);
      } elseif(sfContext::getInstance()->getUser()->isAuthenticated()){
            $userId=sfContext::getInstance()->getUser()->getGuardUser()->getId();
      }else{
          $userId = "";
      }

        //$listOfQuestions = Doctrine::getTable('SecurityQuestion')->findAll(Doctrine::HYDRATE_ARRAY);
        //$this->qnaCount = count($listOfQuestions);
        #User Questions
        //$randi = rand(0,$this->qnaCount-1);
      /*  $listOfQuestions = array();
          if(!empty($userId) && $requestHolder->get('action')!='changeFirstPassword'){ //condition added for fist time login update password

             $listOfQuestions = Doctrine::getTable('SecurityQuestionsAnswers')->getUserRandomQuestion($userId);
             $questionText =$listOfQuestions[0]['SecurityQuestion']['questions'];
             $questionId = $listOfQuestions[0]['SecurityQuestion']['id'];



        }else{//print "in";exit;
              $Question = Settings::getNumberOfQuestion();
              $listOfQuestions = Doctrine::getTable('SecurityQuestion')->getRandomQuestion($Question);
              $questionText =$listOfQuestions[0]['questions'];
              $questionId = $listOfQuestions[0]['id'];

        }*/

      //$userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();

      if(!empty($userId)) {
          $ansCount = Doctrine::getTable('SecurityQuestionsAnswers')->getAnswerCount($userId);
          if($ansCount == 0) {
              $userId = ""; //get any 10 random questions
          }
      }


      $listOfQuestions = Doctrine::getTable('SecurityQuestion')->getRandomQuestion(Settings::getNumberOfQuestion(), $userId);
      $questionText =$listOfQuestions[0]['questions'];
      $questionId = $listOfQuestions[0]['id'];


        //$this->widgetSchema['user_id']= new sfWidgetFormInputHidden();

        $this->widgetSchema['security_question_text']= new sfWidgetFormInputText();
        $this->widgetSchema['security_question_id']= new sfWidgetFormInputHidden();
        $this->widgetSchema['answer'] = new sfWidgetFormInputText(array('type'=>'password'));
        $this->getWidgetSchema()->moveField('security_question_text',sfWidgetFormSchema::BEFORE,'answer');

        $this->getWidget('security_question_text')->setAttributes(array('size'=>'40','readonly'=>'readonly','class'=>'loginHintQuestion','tabindex'=>-1));
        $this->setDefault('security_question_text', $questionText.' ?');
        $this->setDefault('security_question_id', $questionId);




        //$this->validatorSchema['user_id'] = new sfValidatorString(array('required'=>true));
        $this->validatorSchema['security_question_text'] = new sfValidatorString(array('required'=>false));
        $this->validatorSchema['answer'] = new sfValidatorString(array('required'=>true),array('required'=>'Answer is required'));

        $attr = array( 'onpaste'=>"return false",'ondrop'=>"return false", 'ondrag'=>"return false", 'oncopy'=>"return false", 'autocomplete'=>"off");
        $this->widgetSchema['answer']->setAttributes($attr);

        $data = sfContext::getInstance()->getUser();

        # $isAuthenticated = sfContext::getInstance()->getUser()->isAuthenticated();
        # $isFirstUser = false;
        #if ($isAuthenticated) {
        #  $isFirstUser = $this->checkFirstUser(sfContext::getInstance()->getUser()->getGuardUser()->getUsername());

        # if($isFirstUser){
        //below code for ewallet , when it comes from merchant for making payment


        //below code for ewallet user when it comes from home page for signin
        //qns crediential is added at run time for identifying ewallet user for signin( no any use of checkqns credential)
       if(sfContext::getInstance()->getUser()->isAuthenticated() && sfContext::getInstance()->getUser()->getAttribute('checkqns')){
          $yesToCheck = 1;
          $securityAnswerOnly = 'checkQuestionAnsOnSecurityPage';
        }else{
            $securityAnswerOnly = 'checkQuestionAns';
        }

        if($this->offsetExists('username') || $yesToCheck==1){
            $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array('callback' => array($this, $securityAnswerOnly)),array('invalid'=>'Invalid Answer to Security Question'))
            );
        }
        # }
        # }

        $this->widgetSchema->setLabels(
            array('security_question_text'=>'Security Question', 'answer'=>'Answer'));
    }

  public function checkQuestionAnsOnSecurityPage($validator, $values) {


        if($values['answer']!="" && $values['security_question_id']!=""){
          $isAuthenticated = sfContext::getInstance()->getUser()->isAuthenticated();
          if($isAuthenticated){
            $userName=sfContext::getInstance()->getUser()->getUsername();
          }else{
            $userName=$values['username'];
          }
          $userDetail = Doctrine::getTable('sfGuardUser')->findByUserName($userName);
          $failedAtempt=Doctrine::getTable('UserDetail')->findByUserId($userDetail->getFirst()->getId());
          $pass=false;
          if($failedAtempt->getFirst()->getFailedAttempt() < 5){
          $q = Doctrine_Query::create()->select('*')
            ->from('sfGuardUser as u')
            ->leftJoin('u.SecurityQuestionsAnswers as ua')
            ->where('u.username=?',$userName)
            ->andWhere('ua.answer=?',$values['answer'])
            ->andWhere('ua.security_question_id = ?',$values['security_question_id']);

            $pass = $q->count();
          }
          if(!$pass){
              $authenticateObj = new userAuthentication();
              $authenticateObj->setUsername($userName);
              //$pass = true;
              $returnVal = $authenticateObj->updateFailedAttempt($userName);
              if($returnVal =='user_blocked') {
                  $errors = "This username is blocked. Please contact the administrator.";
                  sfContext::getInstance()->getUser()->setFlash('error', $errors);
              }
              else if($returnVal !='continue') {
                  $errors = "you did $returnVal failed attempts out of ".sfConfig::get('app_number_of_failed_attempts_for_blocked_user').".";
                  sfContext::getInstance()->getUser()->setFlash('error', $errors);
              }
          }

         if($pass) {
            sfContext::getInstance()->getUser()->setFlash('error', NULL);
//            $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($userName);
//            $numberOfContinuousAttemptObj->setFailedAttempt(NULL);
//            $numberOfContinuousAttemptObj->save();
            return $values;
         }
         else {

            $message = "Security Question / Answer Failed";
            throw new sfValidatorErrorSchema($validator,array('answer' => new sfValidatorError($validator, 'invalid')));
            return;
         }
        }
    }


    public function checkQuestionAns($validator, $values) {


        if($values['answer']!="" && $values['security_question_id']!=""){
            $isAuthenticated = sfContext::getInstance()->getUser()->isAuthenticated();
            if($isAuthenticated){
                $userName=sfContext::getInstance()->getUser()->getUsername();
            }else{
                $userName=$values['username'];
            }
            $q = Doctrine_Query::create()->select('*')
            ->from('sfGuardUser as u')
            ->leftJoin('u.SecurityQuestionsAnswers as ua')
            ->where('u.username=?',$userName)
            ->andWhere('ua.answer=?',$values['answer'])
            ->andWhere('ua.security_question_id = ?',$values['security_question_id']);

            $pass = $q->count();

            if($pass) {

                return $values;
            }
            else {

                $message = "Security Question / Answer Failed";
                throw new sfValidatorErrorSchema($validator,array('answer' => new sfValidatorError($validator, 'invalid')));
                return;
            }
        }
    }

    public function checkFirstUser($uname) {
        if(empty($uname)) return false;
        //print_r('user: '.$uname.' : bank: '.$bname);
        # if first timeUser Skip Question validation
        $q = Doctrine_Query::create()->select('*')
        ->from('sfGuardUser')
        ->andWhere('username=?',$uname)
        ->andWhere("last_login is NULL")
        ->execute()
        ->toArray(Doctrine::HYDRATE_ARRAY);

        //        print_r($q);
        //die('CheckFirstUser:'.$uname);
        if(count($q)) {
            return true;
        }
        return false;

    }

    public function checkUserDetail($uname) {
        if(empty($uname)) return false;

        $q = Doctrine_Query::create()->select('s.id')
        ->from('sfGuardUser s')
        ->andWhere('s.username=?',$uname)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        if(count($q)) {
            return  $q[0]['id'];
        }
        return false;

    }


}
