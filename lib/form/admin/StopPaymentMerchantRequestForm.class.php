<?php

/**
 * ScheduledPaymentConfig form.
 *
 * @package    form
 * @subpackage ScheduledPaymentConfig
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class StopPaymentMerchantRequestForm extends sfform {

    public function configure() {

      
        $paymentMode = $this->getOption('payment_mode');
        $paymentModeOptions = $this->getOption('payment_mode_option');
        $merchant = $this->getOption('merchant');
        $merchantService = $this->getOption('merchant_service');
    

        //widget section
        $this->widgetSchema['payment'] = new sfWidgetFormChoice(array('choices' => $paymentMode,), array('title' => 'Payment Mode', 'onChange' => 'set_payment_service_type()'));
        $this->widgetSchema['payment_mode'] = new sfWidgetFormChoice(array('choices' => $paymentModeOptions,), array('title' => 'Payment Mode Options'));
        $this->widgetSchema['merchant'] = new sfWidgetFormChoice(array('choices' => $merchant), array('title' => 'Merchant', 'onchange' => 'set_merchant_service_type()'));
        $this->widgetSchema['service_type'] = new sfWidgetFormChoice(array('choices' => $merchantService), array('title' => 'Merchant Service'));
        $this->widgetSchema['startDate'] = new sfWidgetFormDateTime(array('date' => array('empty_values' => array('year' => 'year', 'month' => 'month', 'day' => 'day')), 'time' => array('with_seconds' => true, 'empty_values' => array('hour' => 'hour', 'minute' => 'minutes', 'second' => 'seconds'))), array());
        $this->widgetSchema['endDate'] = new sfWidgetFormDateTime(array('date' => array('empty_values' => array('year' => 'year', 'month' => 'month', 'day' => 'day')), 'time' => array('with_seconds' => true, 'empty_values' => array('hour' => 'hour', 'minute' => 'minutes', 'second' => 'seconds'))), array());
        //labels
        $this->widgetSchema->setLabels(array(
            'payment' => 'Payment Mode<sup>*</sup>',
            'payment_mode' => 'Payment Mode Option<sup>*</sup>',
            'merchant' => 'Merchant<sup>*</sup>',
            'service_type' => 'Merchant Service<sup>*</sup>',
            'startDate' => 'Start Date<sup>*</sup>',
            'endDate' => 'End Date',
        ));
        $this->validatorSchema['payment'] = new sfValidatorString(array('required' => true), array('required' => 'Please Select Payment Mode'));
        $this->validatorSchema['payment_mode'] = new sfValidatorString(array('required' => true), array('required' => 'Please Select Payment Mode Options'));
        $this->validatorSchema['merchant'] = new sfValidatorString(array('required' => true), array('required' => 'Please Select Merchant'));
        $this->validatorSchema['service_type'] = new sfValidatorString(array('required' => true), array('required' => 'Please Select Merchant Service'));
        $this->validatorSchema['startDate'] = new sfValidatorDate(array('with_time' => true, 'date_output' => '%m/%d/%Y H:i:s', 'min' => strtotime(date('d-m-Y'))), array('invalid' => 'Please Select Valid Date Time', 'bad_format' => 'Invalid Date', 'min' => 'Entered date cannot be before today\'s date', 'required' => 'Start Date is Required'));
        $this->validatorSchema['endDate'] = new sfValidatorDate(array('with_time' => true,'required' => false, 'date_output' => '%m/%d/%Y H:i:s', 'min' => strtotime(date('d-m-Y').'-1 day')), array('invalid' => 'Please Select Valid Date Time', 'bad_format' => 'Invalid Date', 'min' => 'Entered date cannot be before today\'s date'));
        $this->validatorSchema->setPostValidator(new sfvalidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'compareEndDate')), array()))));
        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function compareEndDate($validator, $values) {


        $values = $this->getTaintedValues();

        if ($values['endDate']['year'] != '' && $values['endDate']['month'] != '' && $values['endDate']['day'] != '' && $values['endDate']['hour'] != '' && $values['endDate']['minute'] != '' && $values['endDate']['second'] != '') {

            if (strtotime($values['startDate']['year'] . "-" . $values['startDate']['month'] . "-" . $values['startDate']['day'] . " " . $values['startDate']['hour'] . ":" . $values['startDate']['minute'] . ":" . $values['startDate']['second']) >= strtotime($values['endDate']['year'] . "-" . $values['endDate']['month'] . "-" . $values['endDate']['day'] . " " . $values['endDate']['hour'] . ":" . $values['endDate']['minute'] . ":" . $values['endDate']['second'])) {

                $error['endDate'] = new sfValidatorError($validator, 'End Date should be greater than Start Date');
            } 
        }

        if (!empty($error)) {

            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

}