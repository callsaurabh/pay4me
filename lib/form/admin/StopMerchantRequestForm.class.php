<?php

/**
 * ScheduledPaymentConfig form.
 *
 * @package    form
 * @subpackage ScheduledPaymentConfig
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class StopMerchantRequestForm extends sfform {

    public function configure() {
        $merchant = $this->getOption('merchant');
        $merchantService = $this->getOption('merchant_service');
        //widget section
        $this->widgetSchema['merchant'] = new sfWidgetFormChoice(array('choices' => $merchant), array('title' => 'Merchant'));
        $this->widgetSchema['service_type'] = new sfWidgetFormChoice(array('choices' => $merchantService), array('title' => 'Merchant Service'));
        $this->widgetSchema['startDate'] = new sfWidgetFormDateTime(array('date' => array('empty_values' => array('year' => 'year', 'month' => 'month', 'day' => 'day')), 'time' => array('with_seconds' => true, 'empty_values' => array('hour' => 'hour', 'minute' => 'minutes', 'second' => 'seconds'))), array());
        $this->widgetSchema['endDate'] = new sfWidgetFormDateTime(array('date' => array('empty_values' => array('year' => 'year', 'month' => 'month', 'day' => 'day')), 'time' => array('with_seconds' => true, 'empty_values' => array('hour' => 'hour', 'minute' => 'minutes', 'second' => 'seconds'))), array());
        $this->widgetSchema->setLabels(array(
            'merchant' => 'Merchant<sup>*</sup>',
            'service_type' => 'Merchant Service<sup>*</sup>',
            'startDate' => 'Start Date<sup>*</sup>',
            'endDate' => 'End Date',
        ));
        $this->validatorSchema['merchant'] = new sfValidatorString(array('required' => true), array('required' => 'Please Select Merchant'));
        $this->validatorSchema['service_type'] = new sfValidatorString(array('required' => true), array('required' => 'Please Select Merchant Service'));
        $this->validatorSchema['startDate'] = new sfValidatorDate(array('with_time' => true, 'date_output' => '%m/%d/%Y H:i:s', 'min' => strtotime(date('d-m-Y H:i:s').' -1 day')), array('invalid' => 'Please Select Valid Date Time', 'bad_format' => 'Invalid Date', 'min' => 'Entered date cannot be before today\'s date', 'required' => 'Start Date is Required'));
        $this->validatorSchema['endDate'] = new sfValidatorDate(array('with_time' => true,'required' => false, 'date_output' => '%m/%d/%Y H:i:s', 'min' => strtotime(date('d-m-Y H:i:s').' -1 day')), array('invalid' => 'Please Select Valid Date Time', 'bad_format' => 'Invalid Date', 'min' => 'Entered date cannot be before today\'s date'));
        $this->validatorSchema->setPostValidator(new sfvalidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'compareEndDate')), array()))));
        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function compareEndDate($validator, $values) {


        $values = $this->getTaintedValues();

        if ($values['endDate']['year'] != '' && $values['endDate']['month'] != '' && $values['endDate']['day'] != '' && $values['endDate']['hour'] != '' && $values['endDate']['minute'] != '' && $values['endDate']['second'] != '') {

            if (strtotime($values['startDate']['year'] . "-" . $values['startDate']['month'] . "-" . $values['startDate']['day'] . " " . $values['startDate']['hour'] . ":" . $values['startDate']['minute'] . ":" . $values['startDate']['second']) >= strtotime($values['endDate']['year'] . "-" . $values['endDate']['month'] . "-" . $values['endDate']['day'] . " " . $values['endDate']['hour'] . ":" . $values['endDate']['minute'] . ":" . $values['endDate']['second'])) {

                $error['endDate'] = new sfValidatorError($validator, 'End Date should be Greater than Start Date');
            }
        } else if (!($values['endDate']['year'] == '' && $values['endDate']['month'] == '' && $values['endDate']['day'] == '' && $values['endDate']['hour'] == '' && $values['endDate']['minute'] == '' && $values['endDate']['second'] == '')) {

            $error['endDate'] = new sfValidatorError($validator, 'Please Select Valid Date Time');
        }

        if (!empty($error)) {

            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

}