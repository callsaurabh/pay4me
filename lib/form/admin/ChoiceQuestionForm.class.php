<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ChoiceQuestionForm extends BaseSecurityQuestionForm
{
    
    public function configure()
    {
         $this->setWidgets(array(
           
            'questions' => new sfWidgetFormDoctrineChoice(array('model' => 'SecurityQuestion','multiple' => true, 'expanded' => true, )),
         
        ));

        $this->setValidators(array(
            
            'questions' => new sfValidatorDoctrineChoice(array('model' => 'SecurityQuestion',
             'multiple' => true,'min'=>3,'max'=>3,'required' => true),
              array('required'=>'Please select any three questions','min'=>'Kindly select 3 questions',
                  'max'=>'Kindly select 3 questions only')),
            
        ));


        $this->widgetSchema->setLabels(array(
            'questions'    => 'Select three Questions' ,
            
        ));

        $this->widgetSchema->setNameFormat('SQ[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
        
     
      
        
    }
 

}
