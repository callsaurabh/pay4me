<?php

/**
 * ScheduledPaymentConfig form.
 * @package    form
 * @subpackage ScheduledPaymentConfig
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class BillerPaymentModeForm extends sfform {

 // protected static $modeType = array('bank' => 'Bank', 'Check' => 'Check','Draft'=>'Draft');

  public function configure() {
  $pfmHelper=new pfmHelper();
  $bankId=$pfmHelper->getPMOIdByConfName('bank');
  $checkId=$pfmHelper->getPMOIdByConfName('Cheque');
  $bankDraftId=$pfmHelper->getPMOIdByConfName('bank_draft');
  $merchantService = $this->getOption('merchantService');
  $merchantdetails = Doctrine::getTable('MerchantService')->find($merchantService);
  $user_bank_id = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
  $merchantId = $merchantdetails->getMerchantId();
  $isMerchantBinded = Doctrine::getTable('ServiceBankConfiguration')->getPMOByMerchantId($merchantId,$user_bank_id );
  if(count($isMerchantBinded)>0){
          $paymentModeOptionDetails = Doctrine::getTable('ServicePaymentModeOption')->findByMerchantServiceId($merchantService);
          $paymentModeOptionDetailsArr = $paymentModeOptionDetails->toArray();
          $choicesArr= array();
          foreach($paymentModeOptionDetailsArr as $key=>$val){
              if($val['payment_mode_option_id'] == $bankId || $val['payment_mode_option_id']== $checkId || $val['payment_mode_option_id']== $bankDraftId ){
                  $choicesArr[]= $val['payment_mode_option_id'];
              }
          }

          $payModeChoicesArr= array();
          foreach($choicesArr as $key=>$val ){
              $payModeChoicesArr[$val] = $pfmHelper->getPMONameByPMId($val);
          }
      }
//$choices= array(0=>'asd');
  $this->widgetSchema['payType'] = new sfWidgetFormSelectRadio(
	            array(
	                'choices' => $payModeChoicesArr,
	                )
	            );
   $this->validatorSchema['payType'] =  new sfValidatorString(array('required' => true));

   $this->widgetSchema->setLabels(array(
                    'payType'    => 'Pay With<sup>*</sup>',
         ));
}}