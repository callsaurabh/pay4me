<?php

class CustomUserSignUpForm extends sfform {

    public function setup() {
        sfWidgetFormSchema::setDefaultFormFormatterName('dl');
    }

    public function configure() {
        if (settings::isMultiCurrencyOn()) {
            $CurrencyObj = new CurrencyCode;
            $CurrencyArray = $CurrencyObj->getAllCountrySelect();
            $CurrencyArray[0] = "Please Select Primary Currency";
        }

        $this->setWidgets(array(
            'id' => new sfWidgetFormInputHidden(),
            'byPass' => new sfWidgetFormInputHidden(array('default' => '1')),
            'name' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'name', 'maxlength' => '30')),
            'lname' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'lname', 'maxlength' => '30',)),
            'username' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'username', 'maxlength' => 50, 'autocomplete' => 'off')),
            'password' => new sfWidgetFormInputPassword(array(), array('class' => 'keypad txtfield-r', 'name' => 'password',)),
            'cpassword' => new sfWidgetFormInputPassword(array(), array('class' => 'keypad txtfield-r', 'name' => 'cpassword',)),
            'dob' => new widgetFormDateCal(array('format' => '%d-%m-%Y'), array('class' => 'txtfield-r', 'readonly' => 'readonly', 'name' => 'dob')),
            'email' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'email', 'maxlength' => '128',)),
            'address' => new sfWidgetFormTextarea(array(), array('class' => 'txtfield-r', 'name' => 'address', 'maxlength' => '255',)),
            'mobileno' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'mobileno', 'maxlength' => '20',)),
            'workphone' => new sfWidgetFormInputText(array(), array('class' => 'txtfield-r', 'name' => 'workphone', 'maxlength' => '20',)),
            'currency_id' => new sfWidgetFormChoice(array('multiple' => false, 'choices' => $CurrencyArray), array('add_empty' => 'false')),
            'captcha' => new sfWidgetFormInput(array(), array('class' => 'txtfield-r')),
        ));

        $this->setValidators(array(
            'id' => new sfValidatorString(array('required' => false)),
            'name' => new sfValidatorRegex(
                    array('pattern' => '/[a-zA-Z]/', 'required' => true, 'max_length' => '30', 'min_length' => 2),
                    array('invalid' => 'First Name Should be characters only', 'required' => 'Please enter First Name', 'min_length' => 'Minimum 2 characters', 'max_length' => 'Maximum 30 characters')),
            'lname' => new sfValidatorRegex(
                    array('pattern' => "/[a-zA-Z]/", 'required' => true, 'max_length' => '30', 'min_length' => 2),
                    array('invalid' => 'Last Name Should be characters only', 'required' => 'Please enter Last Name', 'min_length' => 'Minimum 2 Characters', 'max_length' => 'Maximum 30 characters')),
            'username' => new sfValidatorAnd(array(
                new sfValidatorString(array('max_length' => true), array('required' => 'User Name is required.', 'max_length' => 'Maximum 20 characters')),
                new sfValidatorRegex(array('pattern' => '/^[a-z\d\_\.]{5,20}$/i'), array('invalid' => 'Invalid User Name.', 'required' => 'User Name is required.')),
                new sfValidatorDoctrineUnique(array('required' => true, 'model' => 'sfGuardUser', 'column' => array('username')),
                        array('invalid' => 'User Name already Exists'))),
                    array('required' => 'Please enter Username')
            ),
            'password' => new sfValidatorString(array('required' => true), array('required' => 'Please enter Password')),
            'cpassword' => new sfValidatorString(array('required' => true), array('required' => 'Please Confirm Your Password')),
            'dob' => new sfValidatorDate(array('required' => true), array('required' => 'Please enter Date of Birth')),
            'email' => new sfValidatorAnd(array(
                new sfValidatorEmail(array('required' => true, 'trim' => true), array('invalid' => 'Please Enter Valid Email Adrress')),
                new sfValidatorString(array('required' => true, 'max_length' => 60)),
                new sfValidatorDoctrineUnique(array('model' => 'UserDetail', 'column' => 'email'),
                        array('invalid' => 'E-mail Address already exists'))),
                    array(), array('required' => 'Please enter Email')),
            //new sfValidatorEmail(array('max_length' => 60, 'required' => true,'trim' => true),
            //array('required'=>'Please enter Email','invalid'=>'Please enter valid Email','max_length'=>'Maximum 60 characters')),

            'address' => new sfValidatorString(array('required' => true), array('required' => 'Please enter valid address')),
            'mobileno' => new sfValidatorRegex(array('required' => true, 'pattern' => '/^([+]{1})([0-9]{10,15})$/', 'max_length' => 20), array('max_length' => 'Maximum 20 Characters', 'invalid' => 'Please enter valid Mobile No.', 'required' => 'Please enter Mobile No.')),
            'workphone' => new sfValidatorRegex(array('pattern' => '/^([+]{1})([0-9]{10,15})$/', 'required' => false, 'max_length' => 20,), array('max_length' => 'Maximum 20 Characters', 'invalid' => 'Please enter valid Work Phone')),
            'captcha' => new sfValidatorSfCryptoCaptcha(array('required' => true, 'trim' => true), array('wrong_captcha' => 'Please enter Valid Verification Code',
                'required' => 'Please enter Verification Code.')),
            'currency_id' => new sfValidatorChoice(array('choices' => array_keys($CurrencyArray), 'min' => 1), array('required' => 'Please select Currency', 'min' => 'Please select Currency'))
        ));



        $this->validatorSchema->setPostValidator(
                new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'cpassword',
                        array('throw_global_error' => true),
                        array('invalid' => 'Your confirmed password does not match the entered password')
                )
        );

        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'CheckDob'))))));

        $this->validatorSchema->setOption('allow_extra_fields', true);

        $this->widgetSchema->setLabels(array(
            'name' => 'First Name',
            'lname' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'cpassword' => 'Confirm Password',
            'dob' => 'Date of Birth',
            'address' => 'Address',
            'mobileno' => 'Mobile No.',
            'workphone' => 'Work Phone',
            'currency_id' => 'Primary Currency',
            'email' => 'Email Address',
            'captcha' => 'Verification Code'
        ));
    }

    public function CheckDob($validator, $values) {

        if (!empty($values['dob'])) {
            $dob = strtotime($values['dob']);
            $currentDate = strtotime(date('d-m-Y'));
            if ($dob >= $currentDate) {
                $error = new sfValidatorError($validator, "Date of Birth cannot be future or today's date");
                throw new sfValidatorErrorSchema($validator, array('dob' => $error));
            }
        }
        return $values;
    }

}

?>