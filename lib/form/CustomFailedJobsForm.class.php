<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomFailedJobsForm extends sfform {

    public function configure() {


        $this->widgetSchema['startDate'] = new widgetFormDateCal(array('format' => '%Y-%m-%d', 'label' => 'From Date<sup>*</sup>'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));
        $this->widgetSchema['endDate'] = new widgetFormDateCal(array('format' => '%Y-%m-%d', 'label' => 'To Date<sup>*</sup>'), array('maxlength' => 10, 'readonly' => 'true', 'class' => 'txt-input'));
        $this->validatorSchema['startDate'] = new sfValidatorDate(array('required' => true, 'date_output' => '%Y-%m-%d', 'max' => strtotime(date('Y-m-d'))), array('required' => 'Please select Start Date', 'bad_format' => 'Invalid Date', 'max' => 'Cannot Be Future Date'));
        $this->validatorSchema['endDate'] = new sfValidatorDate(array('required' => true, 'date_output' => '%Y-%m-%d', 'max' => strtotime(date('Y-m-d'))), array('required' => 'Please select End Date', 'bad_format' => 'Invalid Date', 'max' => 'Cannot Be Future Date'));

        $this->validatorSchema->setPostValidator(new sfvalidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'compareEndDate')), array()))));

        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function compareEndDate($validator, $values) {


        $values = $this->getTaintedValues();

        if ($values['startDate'] != '' && $values['endDate'] != '') {

            if ((strtotime($values['startDate']) > strtotime($values['endDate']) && (strtotime($values['endDate']) < strtotime(date('Y-m-d'))))) {

                $error['endDate'] = new sfValidatorError($validator, 'End Date should be Greater Than Start Date');
            }
        }
        if (!empty($error)) {

            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

}

?>
