<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomBankNotificationSchedulerForm extends sfform {

    public function configure() {

        $current_time = date('H:i:s', time());
        $this->widgetSchema['start_time'] = new sfWidgetFormTime(array('format' => '%hour%:%minute%:%second%', 'with_seconds' => true, 'can_be_empty' => false, 'default' => $current_time, 'label' => 'Start Time<sup>*</sup>'));
        $this->widgetSchema['end_time'] =   new sfWidgetFormTime(array('format' => '%hour%:%minute%:%second%', 'with_seconds' => true, 'can_be_empty' => false, 'default' => $current_time, 'label' => 'End Time<sup>*</sup>'));
        $this->widgetSchema['status'] =     new sfWidgetFormChoice(array('label' => 'Status<sup>*</sup>','choices' => array('' => 'Please Select Status', '0' => 'Active', '1' => 'De-active')));
        $this->widgetSchema['type'] =       new sfWidgetFormInputHidden(array('default' => 'all'));
        
    }

}

?>
