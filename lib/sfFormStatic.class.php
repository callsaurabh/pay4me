<?php

/**
 * @package    ePortal 
 */

/**
 * sfFormStatic is the base class for Static Forms (form without models) based on sfForm objects.
 *
 * @package    ePortal
 * @subpackage form
 * @author     Kamal Somani <kamal.somani@tekmindz.com>
 * @version    SVN: $Id: sfFormStatic.class.php 7845 2009-05-12 22:36:14Z ksomani $
 */
abstract class sfFormStatic extends sfForm
{
  protected
    $isNew  = true,
    $object = null;

  /**
   * Constructor.
   *
   * @param BaseObject A object used to initialize default values
   * @param array      An array of options
   * @param string     A CSRF secret (false to disable CSRF protection, null to use the global CSRF secret)
   *
   * @see sfForm
   */
  public function __construct($object = null, $options = array(), $CSRFSecret = false)
  {
   
    parent::__construct(array(), $options, $CSRFSecret);

    
  }

}
