<?php

class TestWithLogTask extends sfDoctrineBaseTask
{
    protected function configure()
    {

        $this->namespace        = 'pay4me';
        $this->name             = 'test';
        $this->briefDescription = 'Run symfony test cases and log it';
        $this->addArguments(array(
             new sfCommandArgument('testType', sfCommandArgument::OPTIONAL, 'The test cases type which should launch', 'all'),
             new sfCommandArgument('file_OR_Application_Name', sfCommandArgument::OPTIONAL, 'The test name or application name',''),
             new sfCommandArgument('functional_file_Name', sfCommandArgument::OPTIONAL, 'The test name in functional',''),
        
      ));

        

        $this->detailedDescription = <<<EOF
The [pay4me:test|INFO] task launches the test cases and the record will be log in log folder
Call it with:
[php symfony pay4me:test |INFO]
By default all test case will be launched ,
 [php symfony pay4me:test all|INFO]
In order to run particular add option
[php symfony pay4me:test unit|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
//        $context = sfContext::createInstance($this->configuration);
//        $databaseManager = new sfDatabaseManager($this->configuration);
        try{
           fwrite(STDOUT, "Test cases is goin to run ....... \n \n");
           $this->testType = $arguments['testType'];

           if($arguments['file_OR_Application_Name']!='')
                $this->testType .= ' '.$arguments['file_OR_Application_Name'];

           if($arguments['testType']=='functional' && $arguments['functional_file_Name']!='')
                $this->testType .= ' '.$arguments['functional_file_Name'];

           
           $pay4meLog = new pay4meLog();
           $path = $pay4meLog->getLogPath('TestCases');
           $file_name = $path."/".('Test:'.$arguments['testType'].'-'.date('Y_m_d_H:i:s')).".txt";

           $output = exec('php symfony test:'.$this->testType.' > '.$file_name);
           


           
        }
        catch(Exception $e){
            echo 'PROBLEM: ' . $e->getMessage() . "\n";die;
        }
    }
}