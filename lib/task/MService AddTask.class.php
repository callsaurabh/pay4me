<?php

/*
 * This file is part of the Pay4me package.
 */

/**
 * Adds bank users to database.
 *
 * @package    pay4me
 * @subpackage task
 * @author     Ashutosh Srivastava / Ashwani Kumar
 */
class MServiceAddTask extends sfDoctrineBaseTask
{

    protected $merchantId = false;
    protected $merchantName = false;
    protected $merchantCode = false;
    protected $isMerchantCodeUnique = false;
    protected $merchanKey = false;
    protected $authInfo = false;
    protected $bankId = false;
    protected $addMerchant = false;
    protected $merchantObj =false;

    protected $merchantServiceObj = false;
    protected $merchantServiceId = false;
    protected $merchantServiceName = false;
    protected $notification_url = false;
    protected $merchant_home_page = false;
    protected $serviceVersion = false;
    protected $serviceChargePercent = false;
    protected $upperSlab = false;
    protected $lowerSlab = false;
    protected $addMerchantService = false;
    protected $transactionDetail = false;
    protected $paramTypeArr=array("","integer", "string");
    protected $mappedToArr=array("","iparam_one", "iparam_two", "iparam_three", "iparam_four", "iparam_five", "iparam_six", "iparam_seven", "iparam_eight", "iparam_nine", "iparam_ten", "sparam_one", "sparam_two", "sparam_three", "sparam_four", "sparam_five", "sparam_six", "sparam_seven", "sparam_eight", "sparam_nine", "sparam_ten");
    protected $validationRulesArr=array();
    protected $ValidationRulesdetail = false;
  /**
   * @see sfTask
   */
    protected function configure()
    {

        $this->namespace = 'pay4me';
        $this->name = 'add-service';
        $this->briefDescription = 'Add merchant-service to system ';

        $this->detailedDescription = <<<EOF
The [pay4me:add-service|INFO] task adds service from the input given:

EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // process the passed in arguments and options and set class variables
        ini_set('memory_limit', '196M');
        $databaseManager = new sfDatabaseManager($this->configuration);

        echo "*** This task will create merchant-service *** \n \n" ;

        //        fwrite(STDOUT, "Please enter merchant name for which you want to enter the service \n");
        //        $merchantname = trim(fgets(STDIN));
        //        $this->merchantName=$merchantname;
        //        $merchantUname=strtoupper($merchantname);
        //        $merchantId=$this->getMerchantId($merchantUname);

        fwrite(STDOUT, "-- Select merchant from given list-- \n \n");

        $allmerchant=$this->getAllMerchant();
        $tot_merchant=count($allmerchant);
        for($i=0;$i<=$tot_merchant;++$i)
        {
            $count=$i+1;
            if($i!=$tot_merchant)
                echo $count." : ".$allmerchant[$i]['name']." \n";
            else
            echo "\n if you want to create new merchant than enter ". $count."\n";

        }


        $merchantOpt= trim(fgets(STDIN));
        $range=count($allmerchant)+1;
        $merchantOpt=$this->isValidIntRange($merchantOpt,$range);
        if($merchantOpt<=count($allmerchant))
        {
            $i=$merchantOpt-1;
            $merchantId=$allmerchant[$i]['id'];
            $merchantname=$allmerchant[$i]['name'];
            $this->merchantId= $merchantId;
            $this->merchantName= $merchantname;
        }

        if(!$merchantId)
        {
            //fwrite(STDOUT, " $merchantname does not exist in our database. Do you want to create this merchant (y/n) \n ");
            fwrite(STDOUT, "  Do you want to create new merchant (y/n) \n ");
            $opt= trim(fgets(STDIN));
            if("Y"==strtoupper($opt))
            {

                $this->getMerchantdetail();

            }
        }

        if($this->addMerchant || $merchantId)
        {
            $this->MerchantServicedetail();
            $this->ValidationRulesdetail();
            $this->getMerchantServiceCharges();
            $this->getTransactionCharges();
            if($this->addMerchant)
            {
                $this->getBankdetail();
            }
            $this->saveAll();
        }
    }
 public function createLogData($Data,$nameFormate)
  {
    $path = $this->getLogPath();

    $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
    $i=1;
    while(file_exists($file_name)) {
      $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
      $i++;
     }
     @file_put_contents($file_name, $Data);
  }

  public function getLogPath()
  {

    $logPath = sfConfig::get('sf_web_dir').'/log/merchantSetting';
    $logPath = $logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='')
    {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
      chmod($dir_path, 0777);
    }
    return $logPath;
  }
    protected function saveAll()
    {

        fwrite(STDOUT, "\n\n--- Following data have been going to save  --- \n ");
        $content="";
        echo $merchantName="Merhant Name : $this->merchantName  \n ";
        $content=$content.$merchantName;
        if($this->addMerchant)
        {
            echo $mCode="Merhant Code : $this->merchantCode  \n ";
            echo $mKey="Merhant Key : $this->merchanKey  \n ";
            echo $authInfo="Merhant auth-info : $this->authInfo  \n ";
            echo $mbank="Merhant-bank name : $this->bankName  \n ";
            $content=$content.$mCode.$mKey.$authInfo.$mbank;
        }
        echo $nn="\n  \n ";
        $content=$content.$nn;
        echo $mService="Merhant Service Name : $this->merchantServiceName  \n ";
        echo $mServiceNotiUrl="Merhant Service notification_url : $this->notification_url  \n ";
        echo $mServiceHomePage="Merhant Service home_page : $this->merchant_home_page  \n ";
        echo $mversion="Merhant Service Version : $this->serviceVersion  \n ";
        echo $mcharges="Merhant Service charges (%) : $this->serviceChargePercent  \n ";
        echo $mUlimt="Merhant Service charge upper-limit : $this->upperSlab  \n ";
        echo $mLlimit="Merhant Service charge lower-limit : $this->lowerSlab  \n ";

        $content=$content.$mService.$mServiceNotiUrl.$mServiceHomePage.$mversion.$mcharges.$mUlimt.$mLlimit;

        echo $vhead="\n Validation Rule Detail  ";
        $content=$content.$vhead;

        $tot_ValidRules=count($this->validationRulesArr);
        for($i=1;$i<=$tot_ValidRules;++$i)
        {
            echo $nn="\n  \n ";
            echo $pName=$i." : parameter-name :".$this->validationRulesArr[$i]['paramName']." \n ";
            echo $pDesc=$i." : parameter-description :".$this->validationRulesArr[$i]['paramDesc']." \n ";
            echo $pMandt=$i." : parameter- is mandatory :".$this->validationRulesArr[$i]['isMandatory']." \n ";
            echo $pType=$i." : parameter-type :".$this->validationRulesArr[$i]['paramType']." \n ";
            echo $mappedTo=$i." : mapped_to :".$this->validationRulesArr[$i]['mappedTo']." \n ";

            $content=$content.$nn.$pName.$pDesc.$pMandt.$pType.$mappedTo;
        }

        echo $thead="\n Transaction Charges detail Detail  ";
        $content=$content.$thead;

        $tot_transDetail=count($this->transactionDetail);
        for($i=1;$i<=$tot_transDetail;++$i)
        {
            echo $nn="\n  \n ";
            echo $pMode=$i." : Payment Mode :".$this->transactionDetail[$i]['payModeOptName']." \n ";
            echo $fInstCharges=$i." : financial Institution Charges :".$this->transactionDetail[$i]['finclInstCharges']." \n ";
            echo $pMcharges=$i." : Payforme Charges :".$this->transactionDetail[$i]['payformeCharges']." \n ";
            $content=$content.$nn.$pMode.$fInstCharges.$pMcharges;
        }

        fwrite(STDOUT, " Do you want to save the data (y/n) \n ");
        $opt= trim(fgets(STDIN));
        while($opt!='y' && $opt!='n')
        {
            fwrite(STDOUT, "Please enter the valid option (y/n) \n ");
            $opt= trim(fgets(STDIN));
        }
        if($opt=='y')
        {
            
            echo " Saving the data ....";
            if($this->addMerchant)
            {       $this->saveMerchant();   }

            if($this->merchantId || $this->merchantObj)
            {       $this->saveMerchantService();    }

            if($this->merchantServiceObj)
            {   $this->saveValidationRules();        }

            if($this->merchantServiceId)
            {
                $this->createLogData($content,$merchantName);
                echo "\n ";
                echo "Merchant service  is created  \n ";
            }
        }
    }


    protected function saveValidationRules()
    {
        $tot_ValidRules=count($this->validationRulesArr);
        for($i=1;$i<=$tot_ValidRules;++$i)
        {
            //unset($validationRules);
            $validationRules[$i]=$this->merchantServiceObj->ValidationRules[$i];
           // $validationRules->merchant_service_id = $this->merchantServiceId;
            $validationRules[$i]->param_name 	 =$this->validationRulesArr[$i]['paramName'] ;
            $validationRules[$i]->param_description 	 =$this->validationRulesArr[$i]['paramDesc'];
            if($this->validationRulesArr[$i]['isMandatory']=='y')
            $validationRules[$i]->is_mandatory 	 = '1';
            $validationRules[$i]->param_type 	 = $this->validationRulesArr[$i]['paramType'];
            $validationRules[$i]->mapped_to 	 = $this->validationRulesArr[$i]['mappedTo'];
           
           
        }
         if($this->addMerchant)
        {
            $this->merchantObj->save();
        }
        else
        { 
            $this->merchantServiceObj->save();
        }
         
         $this->merchantServiceId=$this->merchantServiceObj->id;
    }
    protected function saveMerchantService()
    {
        if($this->addMerchant)
        {
            $merchantServiceObj=$this->merchantObj->MerchantService[0];
        }
        else
        { 
            $merchantServiceObj=new MerchantService();
            $merchantServiceObj->setMerchant_id($this->merchantId);
        }
        $merchantServiceObj->setName($this->merchantServiceName) ;
        $merchantServiceObj->setNotification_url($this->notification_url);
        $merchantServiceObj->setMerchant_home_page($this->merchant_home_page);
        $merchantServiceObj->setVersion($this->serviceVersion);
       

        $merchantServiceChargeObj=$merchantServiceObj->MerchantServiceCharges[0];
        //$merchantServiceChargeObj->merchant_service_id = $this->merchantServiceId;
        $merchantServiceChargeObj->service_charge_percent 	 =$this->serviceChargePercent ;
        $merchantServiceChargeObj->upper_slab 	 =$this->upperSlab;
        $merchantServiceChargeObj->lower_slab 	 = $this->lowerSlab;
        
         $tot_transDetail=count($this->transactionDetail);
        for($i=1;$i<=$tot_transDetail;++$i)
        {
            //unset($transactionDetailObj);
            $transactionDetailObj[$i]=$merchantServiceObj->TransactionCharges[$i];
            //$transactionDetailObj->merchant_service_id = $this->merchantServiceId;
            $transactionDetailObj[$i]->payment_mode_option_id 	 =$this->transactionDetail[$i]['payModeOptId'] ;
            $transactionDetailObj[$i]->financial_institution_charges 	 = $this->transactionDetail[$i]['finclInstCharges'];
            $transactionDetailObj[$i]->payforme_charges 	 = $this->transactionDetail[$i]['payformeCharges'];

            $serivePayModOpt[$i]=$merchantServiceObj->ServicePaymentModeOption[$i];
            $serivePayModOpt[$i]->payment_mode_option_id =$this->transactionDetail[$i]['payModeOptId'] ;
           
        }
        $this->merchantServiceObj=$merchantServiceObj;

    }
    protected function saveMerchant()
    {
        $merchantobj=new Merchant();
        $merchantobj->setName($this->merchantName);
        $merchantobj->setMerchant_code($this->merchantCode) ;
        $merchantobj->setMerchant_key($this->merchanKey);
        $merchantobj->setAuth_info($this->authInfo);


        $bankMerchantobj=$merchantobj->BankMerchant[0];
        $bankMerchantobj->setBank_id($this->bankId);

         $this->merchantObj=$merchantobj;


    }

    protected function ValidationRulesdetail()
    {
        fwrite(STDOUT, "---Now please enter the validation rules for $this->merchantServiceName  --- \n ");
        fwrite(STDOUT, "How much validation rules you want to enter (please enter no)   \n ");
        $noOfRules= trim(fgets(STDIN));
        $noOfRules=$this->isValidIntRange($noOfRules,10);
        $validArr=array();

        for($i=1;$i<=$noOfRules;++$i)
        {
            fwrite(STDOUT, "Please enter the $i : parameter-name \n ");
            $paramName= trim(fgets(STDIN));
            $paramName=$this->isValidVal($paramName,"parameter-name");
            $validArr[$i]['paramName']=$paramName;

            fwrite(STDOUT, "Please enter the  parameter-description for $paramName  \n ");
            $paramDesc= trim(fgets(STDIN));
            $paramDesc=$this->isValidVal($paramDesc,"parameter-description");
            $validArr[$i]['paramDesc']=$paramDesc;

            fwrite(STDOUT, " $paramName is mandatory (y/n) \n ");
            $isMandatory= trim(fgets(STDIN));

            while($isMandatory!='y' && $isMandatory!='n')
            {
                fwrite(STDOUT, "Please enter a valid option (y/n) \n ");
                $isMandatory= trim(fgets(STDIN));
            }
            $validArr[$i]['isMandatory']=$isMandatory;

            fwrite(STDOUT, "Please select  the parameter-type for $paramName  \n");
            $tot_paramtype=count($this->paramTypeArr);
            for($j=1;$j<$tot_paramtype;++$j)
            {
                echo $j.": ".$this->paramTypeArr[$j]."\n";
            }
            $paramType= trim(fgets(STDIN));
            while($paramType!=1 && $paramType!=2)
            {
                fwrite(STDOUT, "Please select the valid parameter-type \n ");
                $paramType= trim(fgets(STDIN));
                $paramType=$this->isValidInt($paramType);
            }

            $validArr[$i]['paramType']=$this->paramTypeArr[$paramType];
            $count=0;
            for($j=1;$j<=count($validArr);++$j)
            {
                if($validArr[$j]['paramType']==$this->paramTypeArr[$paramType])
                    $count++;
            }

            switch($paramType)
            {
                case 1:
                    $validArr[$i]['mappedTo']=$this->mappedToArr[$count];

                    break;
                case 2:
                    $count=$count+10;
                    $validArr[$i]['mappedTo']=$this->mappedToArr[$count];
                    break;
            }




        }

        $this->validationRulesArr=$validArr;
        $this->ValidationRulesdetail=true;
    }

    protected function getTransactionCharges()
    {
        fwrite(STDOUT, "\n ---Now please enter the detail of transaction charges --- \n ");
        $allPayModOpt=$this->getAllPAymentModeOption();
        $transactionDetail=array();

        $count=1;
        do{


            unset($tmpArr);
            $i=0;
            foreach($allPayModOpt as $key => $value)
            {
                $i++;
                echo $i.":".$allPayModOpt[$key]['name']." \n";
                $tmpArr[$i]=$key;

            }
            $payModoptVal= trim(fgets(STDIN));
            $payModoptVal=$this->isValidIntRange($payModoptVal,count($tmpArr));

            $payModoptVal=$tmpArr[$payModoptVal];
            $transactionDetail[$count]["payModeOptId"]=$allPayModOpt[$payModoptVal]['id'];
            $transactionDetail[$count]["payModeOptName"]=$allPayModOpt[$payModoptVal]['name'];

            fwrite(STDOUT, "Please enter the financial institution charges  \n ");
            $finIsntCharges= trim(fgets(STDIN));
            $finIsntCharges=$this->isValidInt($finIsntCharges);
            $transactionDetail[$count]["finclInstCharges"]=$finIsntCharges;

            fwrite(STDOUT, "Please enter the payforme charges  \n ");
            $payformeCharges= trim(fgets(STDIN));
            $payformeCharges=$this->isValidInt($payformeCharges);
            $transactionDetail[$count]["payformeCharges"]=$payformeCharges;

            unset($allPayModOpt[$payModoptVal]);

            if(count($allPayModOpt)>0)
            {
                fwrite(STDOUT, "Do you want to add more charges (y/n) \n ");
                $AddPaymode= trim(fgets(STDIN));
                $AddPaymode=$this->isValidVal($AddPaymode,"option");
                while($AddPaymode!='y' && $AddPaymode!='n')
                {
                    fwrite(STDOUT, "Please enter the valid option (y/n) \n ");
                    $AddPaymode= trim(fgets(STDIN));
                }
            }

            $count++;

        } while('Y'==strtoupper($AddPaymode) && count($allPayModOpt)>0);
        $this->transactionDetail=$transactionDetail;


    }
    protected function getMerchantServiceCharges()
    {
        fwrite(STDOUT, "\n ---Now please enter the merchant-service charges --- \n ");
        fwrite(STDOUT, "Please enter the percentage of  merchant-service charges \n ");
        $serviceChargePercent= trim(fgets(STDIN));
        $serviceChargePercent=$this->isValidIntRange($serviceChargePercent,100);
        $this->serviceChargePercent=$serviceChargePercent;

        fwrite(STDOUT, "Please enter the Upper limit of  merchant-service charges \n ");
        $upperSlab= trim(fgets(STDIN));
        $upperSlab=$this->isValidInt($upperSlab);
        $this->upperSlab=$upperSlab;

        fwrite(STDOUT, "Please enter the lower limit of  merchant-service charges \n ");
        $lowerSlab= trim(fgets(STDIN));
        $lowerSlab=$this->isValidInt($lowerSlab);
        while($lowerSlab>=$upperSlab)
        {
            fwrite(STDOUT, "lower slab should be less than upper slab \n ");
            $lowerSlab= trim(fgets(STDIN));
            $lowerSlab=$this->isValidInt($lowerSlab);
        }
        $this->lowerSlab=$lowerSlab;

    }
    protected function MerchantServicedetail()
    {
        fwrite(STDOUT, "---Now please enter the merchant-service detail --- \n ");
        fwrite(STDOUT, "Please enter the merchant-service name \n ");
        $merchantServiceName= trim(fgets(STDIN));
        $merchantServiceName=$this->isValidVal($merchantServiceName,"merchant-service");


        if(!$this->addMerchant){
            $chkDuplicates=$this->isMerchantServiceExist($this->merchantId,$merchantServiceName);
            while($chkDuplicates) {
                fwrite(STDOUT, "Merchant service name $merchantServiceName already exist ! Please enter again ..  \n ");
                $merchantServiceName= trim(fgets(STDIN));
                $merchantServiceName=$this->isValidVal($merchantServiceName,"merchant-service");
                $chkDuplicates = $this->isMerchantServiceExist($this->merchantId,$merchantServiceName);
            }
        }
        $this->merchantServiceName=$merchantServiceName;
        fwrite(STDOUT, "Please enter the notification-url \n ");
        $notificationUrl= trim(fgets(STDIN));
        $notificationUrl=$this->isValidVal($notificationUrl,"notification-url");
        $this->notification_url=$notificationUrl;

        fwrite(STDOUT, "Please enter the merchant-home-page   \n ");
        $merchantHomePage= trim(fgets(STDIN));
        $merchantHomePage=$this->isValidVal($merchantHomePage,"merchant-home-page");
        $this->merchant_home_page=$merchantHomePage;

        fwrite(STDOUT, "Please enter the service-version   \n ");
        $serviceVersion= trim(fgets(STDIN));
        $serviceVersion=$this->isValidVal($serviceVersion,"service-version");
        $this->serviceVersion=$serviceVersion;


        $this->addMerchantService=true;
    }
    protected function isMerchantServiceExist($merchantId,$name)
    {

        $q =Doctrine_Query::create()
        ->select('count(id)')
        ->from('MerchantService')
        ->addWhere("merchant_id = '".$merchantId."'")
        ->addWhere("name = '".$name."'");

        return  $q->count();
    }

    protected function getMerchantdetail()
    {
        fwrite(STDOUT, "Please enter the merchant-name   \n ");
        $merchantName= trim(fgets(STDIN));
        $merchantName=$this->isValidVal($merchantName,"merchant-name");
        $merchant_obj = merchantServiceFactory::getService(merchantServiceFactory::$TYPE_BASE);

        $already_created = $merchant_obj->getTotalMerchant($merchantName,'');

        while($already_created) {
            fwrite(STDOUT, "Merchant name $merchantName already exist ! Please enter again ..  \n ");
            $merchantName= trim(fgets(STDIN));
            $merchantName=$this->isValidVal($merchantName,"merchant-name");
            $already_created = $merchant_obj->getTotalMerchant($merchantName,$id);
        }
        $this->merchantName=$this->isValidVal($merchantName,"merchant-name");

        do {

            $merchantCode = mt_rand();
            $duplicacy_code = $merchant_obj->chkCodeDuplicacy($merchantCode);
        } while ($duplicacy_code > 0);
        $merchantKey = mt_rand();
        $auth_info = base64_encode($merchantCode.$merchantKey);

        $this->merchantCode=$merchantCode;
        $this->merchanKey=md5($merchantKey);
        $this->authInfo=base64_encode($auth_info);

        unset($merchant_obj);

        /////////////////////////////////////////////////////////////
        $this->addMerchant=true;


    }

    protected function getBankdetail()
    {
        fwrite(STDOUT, " \n -- Select merchant-bank from given list-- \n \n");

        $allbank=$this->getAllBank();
        $tot_bank=count($allbank);
        for($i=0;$i<$tot_bank;++$i)
        {
            $count=$i+1;
            //             if($i!=count($allbank))
            echo $count." : ".$allbank[$i]['bank_name']." \n";
            //             else
            //                echo "\n if you want to create new Bank than enter ". $count."\n";
        }
        $bankOpt= trim(fgets(STDIN));
        $range=$tot_bank;
        $bankOpt=$this->isValidIntRange($bankOpt,$range);
        if($bankOpt<=$tot_bank)
        {
            $i=$bankOpt-1;
            $bankId=$allbank[$i]['id'];
            $this->bankId=$bankId;
            $this->bankName=$allbank[$i]['bank_name'];
        }
        //        if($bankOpt==$tot_bank)
        //         {
        //
        //             fwrite(STDOUT, "  Does your  merchant related bank exist (y/n) \n ");
        //
        //             $this->getBranchdetail();
        //             $this->getUserdetail();
        //         }

    }
    protected function isValidInt($val) {

        while(is_int($val) || $val<=0 )
        {

            fwrite(STDOUT, "Please  enter the valid integer \n ");
            $val= trim(fgets(STDIN));
        }
        return $val;
    }
    protected function isValidIntRange($val,$range) {

        while(is_int($val) || $val<=0 || $val>$range)
        {
            if($val>$range)
            {
                fwrite(STDOUT, "Please  enter less than or equal to $range \n ");
                $val= trim(fgets(STDIN));
            }
            else
            {
                fwrite(STDOUT, "Please  enter the valid option \n ");
                $val= trim(fgets(STDIN));
            }
        }
        return $val;
    }

    protected function isValidVal($val, $msg) {

        while(!$val)
        {
            fwrite(STDOUT, "Please  enter the valid $msg \n ");
            $val= trim(fgets(STDIN));
        }
        return $val;
    }

    protected function isMerchantCodeExist($mCode) {

        $qry = Doctrine_Query::create()
        ->select('id')
        ->from('Merchant')
        ->where('merchant_code = ?', $mCode);

        $res = $qry->execute(array(),Doctrine::HYDRATE_ARRAY);
        $QryCount=$qry->count();
        if($QryCount>0)
        { $this->isMerchantCodeUnique=false; return $QryCount;}
        else
        $this->isMerchantCodeUnique=true;
    }

    protected function isMerchantKeyExist($mKey) {

        $qry = Doctrine_Query::create()
        ->select('id')
        ->from('Merchant')
        ->where('merchant_key = ?', $mKey);

        $res = $qry->execute(array(),Doctrine::HYDRATE_ARRAY);
        if($qry->count()>0)
        return true;
        else
        return false;
    }

    protected function getAllPAymentModeOption() {

        $qry = Doctrine_Query::create()
        ->select('*')
        ->from('PaymentModeOption')
        ->where('deleted_at IS NULL');

        $res = $qry->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $res;
    }
    protected function getAllBank() {

        $qry = Doctrine_Query::create()
        ->select('*')
        ->from('Bank')
        ->where('deleted_at IS NULL');

        $res = $qry->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $res;
    }
    protected function getAllMerchant() {

        $qry = Doctrine_Query::create()
        ->select('*')
        ->from('Merchant')
        ->where('deleted_at IS NULL');

        $res = $qry->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $res;
    }
    protected function getMerchantId($mUname) {

        $qry = Doctrine_Query::create()
        ->select('id')
        ->from('Merchant')
        ->where('UPPER(name) = ?', $mUname);

        $res = $qry->execute(array(),Doctrine::HYDRATE_ARRAY);
        if($qry->count()>0)
        {
            $this->merchantId=$res[0]['id'];
            return $res[0]['id'];

        }
        else
        return false;
    }


}