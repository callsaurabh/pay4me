<?php

/*
 * This file is part of the Pay4me package.
 */

/**
 * Adds bank users to database.
 *
 * @package    pay4me
 * @subpackage task
 * @author     Sunil Dutt Tiwari
 */
class BulkUserAddTask extends sfDoctrineBaseTask
{
  protected $action = false;
  protected $addAcr = false;
  protected $bankObj = null;
  protected $bankAcr = null;
  protected $ignoreExisting = false;
  /** @var Doctrine_Collection  */
  protected $newUsers = null;
  /** @var sfGuardGroup */
  protected $groupObject = null;

  /** User Name Index */
  public static $UN_IDX = 0;
  /** First Name Index */
  public static $FN_IDX = 1;
  /** Last Name Index */
  public static $LN_IDX = 2;
  /** Email Address Index */
  public static $EM_IDX = 3;
  /** Bank Name Index */
  public static $BN_IDX = 4;
  /** Branch Code Index */
  public static $BC_IDX = 5;
  public static $MAX_TOKENS=6;

   /**
   * @see sfTask
   */
  protected function configure()
  {
    $this->addArguments(array(
        new sfCommandArgument('bank-acronym', sfCommandArgument::REQUIRED, 'The Bank Acronym'),
        new sfCommandArgument('user-file', sfCommandArgument::REQUIRED, 'The file having users'),
        new sfCommandArgument('hosturl', sfCommandArgument::OPTIONAL, 'Host and server name of the user'),
      ));

    $this->addOptions(array(
        new sfCommandOption('action', null, sfCommandOption::PARAMETER_OPTIONAL, 'Action to perform', 'validate'),
        new sfCommandOption('add-acronym', null, sfCommandOption::PARAMETER_NONE, 'Add the acronym to user name'),
        new sfCommandOption('ignore-existing', null, sfCommandOption::PARAMETER_NONE, 'Ignore Existing users'),
        new sfCommandOption('send-email', null, sfCommandOption::PARAMETER_NONE, 'Add email option to sent email'),
      //  


      ));
  
    $this->namespace = 'pay4me';
    $this->name = 'add-users';
    $this->briefDescription = 'Adds users to system in bulk';

    $this->detailedDescription = <<<EOF
The [pay4me:add-users|INFO] task adds users from the given csv file:

  [./symfony pay4me:add-users AFB afb-users.csv|INFO]

By default the csv file is validated only and no action is performed.
In order to actually create the users, execute it with [action=run|INFO] option

  [./symfony pay4me:add-users --action=run AFB afb-users.csv|INFO]

The bank and the branch (from csv) must exist in the database.
EOF;
  }

  protected function execute($arguments = array(), $options = array()) {
    // process the passed in arguments and options and set class variables
    ini_set('memory_limit', '196M');
    $databaseManager = new sfDatabaseManager($this->configuration);
    //$this->createConfiguration('frontend', 'prod');
    $this->newUsers = new Doctrine_Collection('sfGuardUser');
    $branchUserRoleName='bank_user';//sfConfig::get('app_pfm_role_bank_branch_user');
    $this->groupObject = Doctrine::getTable('sfGuardGroup')
    ->findOneByName($branchUserRoleName);
    if($options['action'] == 'run') {
      $this->action = true;
    }

  
    if($options['add-acronym'] == 'true') {
      $this->addAcr = true;
    }
    //define host url variable
    if($options['send-email'] == 'true') {

         if(!empty($arguments['hosturl'])){

                 $this->hosturl = $arguments['hosturl'];
         }else{
           
              throw new Exception ("Host and server name of the user should be pass as argument!");
         }

    }
    $this->bankAcr = $arguments['bank-acronym'];
    $this->bankObj = $this->getBank($this->bankAcr);
    $this->ignoreExisting = $options['ignore-existing'];
    $csvHandle = fopen($arguments['user-file'], 'r');
    $line_num = 0;
    while (!feof($csvHandle)) {
      $line_num++;
      $line = fgets($csvHandle);
      try {
        $tokens[] = $this->validateCsvLine($line);
      } catch (Exception $ex) {
        if($ex->getMessage() != 'EMPTY_LINE') {
          $this->logBlock("Error (Line $line_num): ".$ex->getMessage(), 'ERROR');
        }
      }
    }
    fclose($csvHandle);
    foreach ($tokens as $token) {
      try {
        $this->addCsvRecord($token);
      } catch (Exception $ex) {
        $this->logBlock("Error: ".$ex->getMessage(), 'ERROR');
      }
    }
    /*
    echo "Saving all users ... ";
    if ($this->action) {
      $this->newUsers->save();
    }
    echo "Done!!".PHP_EOL;
    */
  }

  protected function getBank($bankAcr) {
    $rec = Doctrine_Query::create()
    ->from('Bank')
    ->where('acronym = ?', $bankAcr)->fetchOne();
    if(!$rec) {
      throw new Exception ("Bank with acronym $bankAcr not found!!");
    }
    return $rec;
  }

  protected function addCsvRecord($tokens) {
    $branchObj = $this->getBranchForCode($this->bankObj->getId(),$tokens[self::$BC_IDX]);
    $username = $tokens[self::$UN_IDX];
    if($this->userExist($username)) {
      if ($this->ignoreExisting) {
        return;
      }
      throw new Exception("User with username $username already exists");
    }
  
    $user = new sfGuardUser();
    $user->setUsername($username);
    $user->setPassword('password.'.$this->bankAcr);
    $userDetail = $user->UserDetail[0];
    $userDetail->setName($tokens[self::$LN_IDX].', '.$tokens[self::$FN_IDX]);
    $userDetail->setEmail($tokens[self::$EM_IDX]);
    $user->sfGuardUserGroup[0]->setGroupId($this->groupObject->getId());
    $bankUser = $user->BankUser[0];//new BankUser();
    $bankUser->setBank($this->bankObj);
    $bankUser->setBankBranch($branchObj);
    echo "User $username created (save pending yet) ...".PHP_EOL;
    if($this->action) {
        $user->save();
        echo "User $username saved".PHP_EOL;
          // this code is for sending mail to user based on argument

         if(!empty($this->hosturl))
         {
            $subject = "Welcome to Pay4Me" ;
            $partialName = 'welcome_email' ;
            $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', true);
            $context = sfContext::createInstance($configuration);
           // $configuration->loadHelpers('Url');
            $last = $this->hosturl[strlen($this->hosturl)-1];
            if($last=="/"){
                $login_url = $this->hosturl.$this->bankAcr."/login";
            }else{
                 $login_url = $this->hosturl."/".$this->bankAcr."/login";
            }

            EpjobsContext::getInstance()->addJob('SendMailNotification',"create_bank_admin/sendEmail", array('userid'=>$user->getId(),'password'=>'password.'.$this->bankAcr,'subject'=>$subject,'partialName'=>$partialName,'login_url'=>$login_url));

         }

   
    }


  }

  protected function userExist($username) {
    $cnt = Doctrine_Query::create()
    ->from('sfGuardUser')
    ->where('username = ?', $username)
    ->count();
    return $cnt;
  }

  protected function getBranchForCode($bankId,$branchCode) {
    $q = Doctrine_Query::create()
    ->from('BankBranch')
    ->where('bank_id = ?', $bankId)
    ->andWhere("branch_code = ".$branchCode."");

    $obj = $q->fetchOne();
    if (!$obj) {
      throw new Exception ("Bank Branch with code: $branchCode  not found");
    }
    return $obj;
  }

  protected function validateCsvLine($line) {
    // 0 - see if this is empty line
    $line=trim($line);
    if(preg_match('/^[ ,]*$/',$line)) {
      throw new Exception ('EMPTY_LINE');
    }
    // 1 - split into tokens
    $csv_tokens = explode(',', $line);
    // 2 - validate number of token
    if(count($csv_tokens) < self::$MAX_TOKENS) {
      throw new Exception("Was expecting 6 tokens, found "
        .count($csv_tokens));
    }
    // trim all the tokens
    $csv_tokens=$this->trimTokens($csv_tokens);
    // 3 - validate if the user name confirms to the standard
    $suffix_pattern = '/'.$this->bankAcr.'$/';
    $is_suffixed = preg_match($suffix_pattern,$csv_tokens[self::$UN_IDX]);
    // 4 - add the bank acronym if the option add-acronym is true
    if(!$is_suffixed) {
      if($this->addAcr) {
        $csv_tokens[self::$UN_IDX].='.'.$this->bankAcr;
      } else {
        throw new Exception('Username '.$csv_tokens[self::$UN_IDX]
          .' does not have bank acroynum \''.$this->bankAcr.'\'');
      }
    }
    // return modified tokenized array - throw an exception if anything fails
    return $csv_tokens;
  }

  protected function trimTokens($tokens) {
    $newTokens = array();
    foreach ($tokens as $aToken) {
      $newTokens[] = trim($aToken);
    }
    return $newTokens;
  }

   
}