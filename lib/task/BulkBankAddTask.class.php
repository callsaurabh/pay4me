<?php

/*
 * This file is part of the Pay4me package.
 */

/**
 * Adds bank & bank branches to database.
 *
 * @package    pay4me
 * @subpackage task
 * @author     Amita Srivastava
 */
class BulkBankAddTask extends sfDoctrineBaseTask
{
    protected $action = false;
    protected $addbank = false;
    protected $bankObj = null;
    protected $branchObj = null;
    protected $branchCodeObj = null;
    protected $bankAcr = null;
    protected $lgaObj = null;
    protected $countryID = 154;
    protected $bankID = null;
    protected $fileString = null;
    protected $newBranch = null;

  /** Bank Name Index */
    public static $BN_IDX = 0;
  /** State Index */
    public static $ST_IDX = 1;
  /** LGA Index */
    public static $LGA_IDX = 2;
  /** Branch Location Index */
    public static $BL_IDX = 3;
  /** Branch Address Index */
    public static $BA_IDX = 4;
  /** Branch Code Index */
    public static $BC_IDX = 5;
  /** Branch Sort Code Index */
    public static $BSC_IDX = 6;
  /** Branch Source IP/Range Index */
    public static $BIP_IDX = 7;
/** Max Tokens expected*/
    public static $MAX_TOKENS=8;

   /**
   * @see sfTask
   */
    protected function configure()
    {
        $this->addArguments(array(
                new sfCommandArgument('bank-acronym', sfCommandArgument::REQUIRED, 'The Bank Acronym'),
                new sfCommandArgument('bank-file', sfCommandArgument::REQUIRED, 'The file having bank branch details'),
                new sfCommandArgument('add-bank', sfCommandArgument::OPTIONAL, 'Provide domain name(in @domainname format) to add bank & its configuration'),

            ));

        $this->addOptions(array(
                new sfCommandOption('action', null, sfCommandOption::PARAMETER_OPTIONAL, 'Action to perform', 'validate'),

            ));

        $this->namespace = 'pay4me';
        $this->name = 'bank-setup';
        $this->briefDescription = 'Set up bank to system in bulk';

        $this->detailedDescription = <<<EOF
The [pay4me:bank-setup|INFO] task set up bank from the given csv file:

  [./symfony pay4me:bank-setup BankAcr BankAcr-branches.csv @bankdomainname.com|INFO]

By default the csv file is validated only and no action is performed.
In order to actually set up bank, execute it with [action=run|INFO] option

  [./symfony pay4me:bank-setup --action=run BankAcr BankAcr-branches.csv @bankdomainname.com|INFO]

[Note: Field separator in csv file should be colon(:)|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // process the passed in arguments and options and set class variables
        ini_set('memory_limit', '196M');
        $databaseManager = new sfDatabaseManager($this->configuration);

        if($options['action'] == 'run') {
            $this->action = true;
        }

        if(!empty($arguments['add-bank'])) {
            $this->addbank = true;
        }


        //check if file exists
        if(!file_exists($arguments['bank-file']))
        throw new Exception("File: ". $arguments['bank-file']." do not exist");

        //get file contents in string
        $this->fileString = file_get_contents($arguments['bank-file']);

        //read csv file
        $csvHandle = fopen($arguments['bank-file'], 'r');
        $line_num = 0;
        while (!feof($csvHandle)) {
            $line_num++;
            $line = fgets($csvHandle);
            try {
                $tokens[] = $this->validateCsvLine($line);
            } catch (Exception $ex) {
                if($ex->getMessage() != 'EMPTY_LINE') {
                    $this->logBlock("Error (Line $line_num): ".$ex->getMessage(), 'ERROR');
                }
            }
        }
        fclose($csvHandle);
        //end reading file

        $this->bankAcr = $arguments['bank-acronym'];
        $this->bankObj = Doctrine::getTable('Bank')->getBankObjectByAcronym($this->bankAcr);
        if(!empty($this->bankObj)) //bank already exists
        {
            echo "Bank already exists.Proceeding to add bank branches ...".PHP_EOL;
            $this->bankID = $this->bankObj->getId();

        }
        else
        {
            if(!empty($tokens[0][0])){
                echo "Bank do not exists.Proceeding to create bank ...".PHP_EOL;
                //create bank
                echo "Bank created(save pending yet) ...".PHP_EOL;
                if($this->action)
                {
                   $this->bankID = Doctrine::getTable('Bank')->saveBank(
                     $tokens[0][0],
                     $this->bankAcr
                    );
                    echo "Bank saved ...".PHP_EOL;
                }
                
                //add configuration for bank
                echo "Configuration created(save pending yet) ...".PHP_EOL;
                if($this->action)
                {
                    if($this->addbank) //if domain name provided then add domainname in bankconfiguration
                    $domainname = $arguments['add-bank'];
                    else
                    $domainname = "@".$this->bankAcr.".com";

                    Doctrine::getTable('BankConfiguration')->saveBankConfig(
                     $this->bankID,
                     $this->countryID,
                     $domainname
                    );   
                    
                    echo "Configuration saved ...".PHP_EOL;
                }
            }
        }
        if(!empty($tokens)){
            foreach ($tokens as $token) {
                try {
                    $this->addCsvRecord($token);
                } catch (Exception $ex) {
                    $this->logBlock("Error: ".$ex->getMessage(), 'ERROR');
                }
            }
        }

    }

    //save bank branches
    protected function addCsvRecord($tokens){

        //get LGA ID
        $this->lgaObj = Doctrine::getTable('Lga')->getLgaDetails($tokens[self::$ST_IDX]);
        if(!$this->lgaObj)
        throw new Exception ("LGA: ".$tokens[self::$ST_IDX]."  not found");
        //check if branch exists for bank
        $this->branchObj = Doctrine::getTable('BankBranch')->branchExist($this->bankID,$tokens[self::$BL_IDX]);
        if($this->branchObj) {
            throw new Exception("Bank Branch: ". $tokens[self::$BL_IDX]." already exists for bank: ".$tokens[self::$BN_IDX]);
        }
        //check if branch code already exists
        $this->branchCodeObj = Doctrine::getTable('BankBranch')->branchCodeExist($tokens[self::$BC_IDX]);
        if($this->branchCodeObj) {
            throw new Exception("Branch Code: ".$tokens[self::$BC_IDX]." already exists");
        }


        echo "Branch: ".$tokens[self::$BL_IDX]." created (save pending yet) ...".PHP_EOL;
        if($this->action) {
            //save bank branches
            Doctrine::getTable('BankBranch')->saveBankBranch(
             $this->bankID,
             $tokens[self::$BL_IDX],
             $tokens[self::$BA_IDX],
             $tokens[self::$BC_IDX],
             $tokens[self::$BSC_IDX],
             $this->lgaObj->getId(),
             $this->countryID,
             $tokens[self::$BIP_IDX]
            );
            echo "Branch: ".$tokens[self::$BL_IDX]." saved".PHP_EOL;
        }


    }


     //validations for csv file
    protected function validateCsvLine($line) {
        // 0 - see if this is empty line
        $line=trim($line);
        if(preg_match('/^[ ,]*$/',$line)) {
            throw new Exception ('EMPTY_LINE');
        }
        // echo $line;
        // 1 - split into tokens
        $csv_tokens = array();
        $csv_tokens = explode(':', $line);

        // 2 - validate number of token
        if(count($csv_tokens) < self::$MAX_TOKENS) {
            throw new Exception("Was expecting 8 tokens, found "
                .count($csv_tokens));
        }
        // trim all the tokens
        $csv_tokens=$this->trimTokens($csv_tokens);

        if(empty($csv_tokens[self::$BN_IDX]))
        throw new Exception("Bank Name cannot be empty. ");

        if(empty($csv_tokens[self::$ST_IDX]))
        throw new Exception("State cannot be empty. ");

        if(empty($csv_tokens[self::$BL_IDX]))
        throw new Exception("Branch Location cannot be empty. ");

        if(empty($csv_tokens[self::$BC_IDX]))
        throw new Exception("Branch Code cannot be empty. ");

        //validation to find duplicate branch code in file
        $count = substr_count($this->fileString,$csv_tokens[self::$BC_IDX]);
        if($count > 1)
        throw new Exception("Branch Code: ".$csv_tokens[self::$BC_IDX]." is duplicate in the file ");

        // return modified tokenized array - throw an exception if anything fails
        return $csv_tokens;
    }

    //to trim token values
    protected function trimTokens($tokens) {
        $newTokens = array();
        foreach ($tokens as $aToken) {
            $newTokens[] = trim($aToken);
        }
        return $newTokens;
    }




}
?>
