<?php

class eWalletFinancialReportTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'project';
    $this->name             = 'eWalletFinancialReport';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [eWalletFinancialReport|INFO] task does things.
Call it with:

  [php symfony eWalletFinancialReport|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
 
      $databaseManager = new sfDatabaseManager($this->configuration);
//      $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
      
        $param = array('from'=>'', 'account_no' => '');//$_SESSION['pfm']['EwalletUserTransactionDetail'];
    	$from_date = $param['from'];
    	$to_date = '2014-09-09';//$param['to'];
    	$ewallet_account_id = $param['account_no'];
    	$currency_id = 1; //$param['currency_id'];
    	// Creating Headers
    	$headerNames = array(0 => array('S.No.', 'Account Number', 'Account Name', 'Total Credits', 'Total Debits', 'Current Balance'));
    	$headers = array('AccountNumber', 'AccountName', 'TotalCredit', 'TotalDebit', 'CurrentAccountBalance');
    	$masterAcctObj = new EpAccountingManager;
    	$statementQry = Doctrine::getTable('EpMasterLedger')->getStatementIn($from_date, $to_date, $ewallet_account_id, '', 'ewallet', $currency_id);
    	$statementObj = $statementQry->execute();
    	$clearBalance = Doctrine::getTable('EpMasterAccount')->getClearBalanceForNoTransactions('ewallet', $currency_id,$ewallet_account_id);
    	$i = 0;
    	$totalcAmount = 0;
    	$totaldAmount = 0;
    	$totalCurrentAccountBalance = 0;
    	foreach ($statementObj as $result) { 
    		###################################
    		$ledgerObj = $result->getEpMasterLedger();
    		$collectionObj = $result->getUserAccountCollection()->getFirst();
    		$ledger_entries = count($ledgerObj);
    		$resultArr[$i]['AccountNumber'] = $result->getAccountNumber();
    		$resultArr[$i]['AccountName'] = $result->getAccountName();
    		if ($ledger_entries) {
    			$crAmount = 0;
    			$drAmount = 0;
    			foreach ($ledgerObj as $ledger_result):
    			if ($ledger_result->getEntryType() == 'credit') {
    				$crAmount = $ledger_result->getAmount();
    			} else {
    				$drAmount = $ledger_result->getAmount();
    			}
    			endforeach;
    			$cAmount = ($result->getCredit()) / 100;
    			$resultArr[$i]['TotalCredit'] = html_entity_decode(number_format($cAmount, 2, '.', ''));
    			$dcAmount = ($result->getDebit()) / 100;
    			$resultArr[$i]['TotalDebit'] = html_entity_decode(number_format($dcAmount, 2, '.', ''));
    		} else {
    			$cAmount = 0;
    			$dcAmount = 0;
    			$resultArr[$i]['TotalCredit'] = $cAmount;
    			$resultArr[$i]['TotalDebit'] = $dcAmount;
    		}
    		$CurrentAccountBalance = $cAmount - $dcAmount;
    		$totalcAmount +=$cAmount;
    		$totaldAmount +=$dcAmount;
    		$totalCurrentAccountBalance +=$CurrentAccountBalance;
    		$resultArr[$i]['CurrentAccountBalance'] = html_entity_decode(number_format($result->getClearBalance() / 100, 2, '.', ''));
    		$i++;
    		###################################
    	}
    	$resultArr[$i]['AccountNumber'] = '';
    	$resultArr[$i]['AccountName'] = 'Grand Total : ';
    	$resultArr[$i]['TotalCredit'] = html_entity_decode(number_format($totalcAmount, 2, '.', ''));
    	$resultArr[$i]['TotalDebit'] = html_entity_decode(number_format($totaldAmount, 2, '.', ''));
    	$resultArr[$i]['CurrentAccountBalance'] = html_entity_decode(number_format($clearBalance / 100, 2, '.', ''));
    	$recordsDetails = $resultArr;
    	if (count($recordsDetails) > 0) {
    		$arrList = $this->exportUserList($headerNames, $headers, $recordsDetails);
    		$file_array = csvSave::saveExportListFile($arrList, "/report/eWalletFinancialReport/", "eWalletFinancialReport");
    		$file_array = explode('#$', $file_array);
    		$this->fileName = $file_array[0];
    		$this->filePath = $file_array[1];
    		$this->folder = "eWalletFinancialReport";
//    		$this->setTemplate('csv');
    	} else {
    		$this->redirect_url('index.php/report/rechargeReport');
    	}
    	//$this->setLayout(false);
        
    $this->logSection('do-nothing', 'I did nothing successfully!');
  }  
  
  public function exportUserList($arrHeader, $headers, $arrList) {
        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
//        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $counterVal = count($arrList);
        $counterValHeaders = count($headers);
        for ($k = 0; $k < $counterVal; $k++) {
            if ($k == ($counterVal - 1))
                $arrExPortList[$k][] = '';
            else
                $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeaders; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }
}
