<?php

class UserSessionTask extends sfDoctrineBaseTask
{
    protected function configure()
    {

        $this->namespace        = 'pay4me';
        $this->name             = 'clear-session';
        $this->briefDescription = 'Clears idle sessions in database';
         $this->addOptions(array(
          new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'Pay4Me', 'frontend'),
        ));

        

        $this->detailedDescription = <<<EOF
The [clear-session|INFO] task clears idle sessions in database.
Call it with:
 [php symfony pay4me:clear-session|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        $context = sfContext::createInstance($this->configuration);
        $databaseManager = new sfDatabaseManager($this->configuration);
        try{
            $sessionClass = new EpDBSessionDetail();

            //Call to session cleaning function in EpDBSessionDetail class.
            $deletedRecords = $sessionClass->cleanupIdleSessions();

            if ($deletedRecords > 0)
            {
                echo "Cleared-".$deletedRecords." sessions from database";
            }
            else
            {
                echo "There were no record for the specified criteria";
            }
        }
        catch(Exception $e){
            echo 'PROBLEM: ' . $e->getMessage() . "\n";die;
        }
    }
}