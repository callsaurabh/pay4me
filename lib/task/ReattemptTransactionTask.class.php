<?php

class ReattemptTransactionTask extends sfDoctrineBaseTask {

    protected function configure() {
        // // add your own arguments here
        $this->addArguments(array(
            new sfCommandArgument('reattempt_time', sfCommandArgument::OPTIONAL,'Reattempt Time In Minutes'),
            new sfCommandArgument('failure', sfCommandArgument::OPTIONAL,'Reattempt for failure'),
        ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
                // add your own options here
        ));
        $this->addOption('format', null, sfCommandOption::PARAMETER_REQUIRED, 'Report format', 'csv');
        $this->addOption('mailto', null, sfCommandOption::PARAMETER_REQUIRED, 'Send report in email "TO" addresses, separate by comma');
        $this->namespace = 'pay4me';
        $this->name = 'reattempt-transaction';
        $this->briefDescription = 'Post a message to Bank for a transaction for
            which response is not received/failed ';
        $this->detailedDescription = <<<EOF
        The [epjobs:report|INFO] task reattempts failled or no response received transaction.
Call it with:

  [php symfony pay4me:reattempt-transaction 1440 yes"|INFO]

[format|COMMENT] specifies the report format, possible values are:
[html|INFO]     Generate HTML report
[txt|INFO]      Generate Text report

To generate a detailed report which includes the task execution output
streams, include --detailed option:
  [php symfony pay4me:reattempt-transaction 1440 1 --format=txt
    --mailto=srikanth.reddy@tekmindz.com|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {

        //if reatempt time is specified in task get that time else fetch from app.yml
        if ($arguments['reattempt_time']) {
            $reattemptTime = $arguments['reattempt_time'];
        } else {
            $reattemptTime = sfConfig::get('app_reatttempt_time_in_min');
        }

        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $databaseManager->getDatabase($options['connection'])->getConnection();

        // Single array consisting all the requests to be re-attempted
        $arrFinal = Doctrine::getTable('TransactionBankPosting')->getReattemptedArr($reattemptTime);

        $this->bankIntegrationManager = new bankIntegration();
        $falgNoMessage = FALSE;
        //insert all request ids in this array
        $mailIds = Array();
        $i = 0;


        if (!empty($arrFinal)) {
            foreach ($arrFinal as $value) {
                @$mailIds[$i]['id'] = $value['id'];
                $status = $this->reattempt($value['id']);
                // getting all txn related values
                $mwReqObj = Doctrine::getTable('MwRequest')->find($value['id']);
                $tbpObj = $mwReqObj->getTransactionBankPosting();
                $mailIds[$i]['txn_num'] = $tbpObj->getMessageTxnNo();
                $mailIds[$i]['txn_type'] = $tbpObj->getTxnType();
                $mailIds[$i]['bank_name'] = $tbpObj->getBank()->getBankName();
                $mailIds[$i]['txn_date'] = $tbpObj->getCreatedAt();
                $mailIds[$i]['previous_state'] = $value['previous_state']; // Bug:36278
                $mailIds[$i]['attempts'] = $tbpObj->getNoOfAttempts();
                $mailIds[$i]['updated_at'] = $tbpObj->getUpdatedAt();
                $mailIds[$i]['status'] = $status;
                $mwReqObj->free(true);
                $i++;
            }
            $falgNoMessage = TRUE;
        }



        if (count($mailIds)) {
            $optionvalues = array();

            if ($options['mailto']) {
                $optionvalues['mailto'] = $options['mailto'];
            } else {
                $optionvalues['mailto'] =array(sfConfig::get('app_bankresponsefailure_ccv1'),sfConfig::get('app_bankresponsefailure_ccv2'));
            }
            if (isset($options['memory_limit']) && $options['memory_limit'] != "") {
                $optionvalues['memory_limit'] = $options['memory_limit'];
            } else {
                $optionvalues['memory_limit'] = "1024M";
            }
            //do loop and get details
            $summary = $this->generateReport($mailIds);
            $this->sendmailDetails($optionvalues, $summary);
        }

        if (!$falgNoMessage) {
            echo "No Message found to post" . PHP_EOL;
        }
    }

    /* function : reattempt()
     * $messageQueueRequestId : TablemessageQueueRequest Id
     * return : $message : String
     */

    private function reattempt($messageQueueRequestId) {
        try {
            $resArr = $this->bankIntegrationManager->QueueProcessing($messageQueueRequestId, true);
            $status = true;
            $message = "messageQueueRequestId - $messageQueueRequestId : Message posted successfully" . PHP_EOL;
        } catch (Exception $e) {
            $message = "messageQueueRequestId - $messageQueueRequestId : Due to some internal problem re-attempt failed, Please Run task again" . PHP_EOL;
            $status = false;
        }
        return $status;
    }

    protected function getStartReportText($reportHeader) {
        return PHP_EOL . $reportHeader . PHP_EOL . PHP_EOL;
    }

    private function generateReport($arrQueueId) {
        $format = '%-22s|%-20s|%-20.21s|%-20.22s|%-17s|%-18s|%-20s|%-14s';
        $Summary = "";
        $Summary .= $this->getHeaderText();
        $Summary .= $this->getStartSummaryTableText($format);
        $Summary .= $this->getSummaryLineText($format, $arrQueueId);
        return $Summary;
    }

    private function getHeaderText() {
        //$data = str_repeat('_', 120) . PHP_EOL . PHP_EOL;
        $data = 'Bank Integration Failed/Response not received Transactions  reattempt details ' . PHP_EOL . PHP_EOL;
        return $data;
    }

    protected function getStartSummaryTableText($format) {
        $data = str_repeat('-', 164) . PHP_EOL;
        $data .= sprintf($format, 'Transaction Number', 'Transaction Type', 'Bank Name', 'Transaction Date', 'Current Status', 'Number of Attempts', 'Updated at', 'Reattempt Status');      
        $data .= PHP_EOL . str_repeat('-', 164) . PHP_EOL;
        return $data;
    }

    protected function getSummaryLineText($format, $details) {
        $text = "";
        $data = "";
        $i = 0;


        foreach ($details as $records) {

            $status = "";

            if ($records['status']) {
                $status = "Success";
            } else {
                $status = "Failure";
            }
            $data = sprintf(
                    $format, $records['txn_num'], ucwords($records['txn_type']), $records['bank_name'],
                    $records['txn_date'],$records['previous_state'], $records['attempts'], $records['updated_at'], $status);
            
            $data .= PHP_EOL;
            $text .= $data;
        }
        return $text . PHP_EOL . str_repeat('-', 164) . PHP_EOL;
    }

    private function sendmailDetails($options=array(), $body) {
        sfContext::createInstance($this->configuration);
        $mime = 'text/plain';
        ini_set('memory_limit', $options['memory_limit']);
        if ($options['mailto']) {
            $mailer = EpjobsMailerFactory::getMailer();
            $mailer->setTo($options['mailto']);
            $mailer->setSubject("Bank Integration Failed/Response not received Transaction reattempt stats report");
            $mailer->setBody($body, $mime);
            $mailer->send();
            return;
        }
    }
}