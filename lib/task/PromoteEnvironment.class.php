<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class PromoteEnvironment extends sfDoctrineBaseTask {
  protected function configure() {

    $this->namespace        = 'pay4me';
    $this->name             = 'promote-env';
    $this->briefDescription = 'Promote the enviornment chosen';
    $this->addOptions(array(
        new sfCommandOption('env', null, sfCommandOption::PARAMETER_OPTIONAL, 'Environment to Set', 'dev'),
        new sfCommandOption('url', null, sfCommandOption::PARAMETER_OPTIONAL, 'Change the URL access as per the environment'),
    ));



    $this->detailedDescription = <<<EOF
        The [clear-session|INFO] task clears idle sessions in database.
Call it with:
 [php symfony pay4me:clear-session|INFO]
EOF;
  }
}

?>
