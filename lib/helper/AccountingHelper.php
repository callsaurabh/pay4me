<?php
/* @name AccountingHelper
 * @purpose Class to define the generic functions related to accounting
 * @author Ashwani *
 */


class AccountingHelper{


  /**
* Function to validate amount for (> zero), -ve, +ve.
* This function has been made to keep the amount validation as generic i.e calling same method for this purpose from anywhere.
*
*@author  Ashwani
* @param  integer                    $amount                An amount which needs to be validated.
* @param  array                      $validationOptions     An array with the name of options which you need to be validated for
*
* @return string
* @example
* <pre>
* $amount = 100
* $validationOptions = array('zeroAmtCheck', 'minimumAmtCheck', 'maximumAmtCheck') ;
* </pre> 
*/

  public static function validateAmount($amount,$validationOptions){ 
      $min_transfer_amount = sfConfig::get('app_min_ewallet_transfer');
      $max_transfer_amount = sfConfig::get('app_max_ewallet_transfer');
      $msg = '';

      foreach($validationOptions as $values){
          if($values == 'zeroAmtCheck' && $amount <= 0){ 
              $msg = "Invalid amount";
              break;
          }elseif($values == 'minimumAmtCheck' && $amount < $min_transfer_amount && $amount > 0){
              $msg = "Amount Limit for transfer (Min/Maximum) Not correct!";
              break;
          }elseif($values == 'maximumAmtCheck' && $amount > $max_transfer_amount){ 
              $msg = "Amount Limit for transfer (Min/Maximum) Not correct!";
              break;
          }
      }

      if($msg != ''){
          return $msg;
      }else{
          return true;
      }
      
    }


}


?>
