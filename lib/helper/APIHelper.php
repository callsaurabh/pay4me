<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class APIHelper {

    public function getClientIpAddress() {
       foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {                                          
                        return $ip;
                    }
                }
            }
        }
    }

    public function getCountryCodeByIp($ip){
        $country_code = "";        
        if (!empty($ip)) {
            $url = trim(sfConfig::get('app_ip_to_location_api'));
            $fetch_url = $url."?ip=".$ip;            
            $country_code = $this->url_get_contents($fetch_url);
        }
        return $country_code;
    }

    public function checkCountrycodeforEwallet($code){
        $arrList = Doctrine::getTable('Country')->getCountryDetailsByCode($code,1);
        if(count($arrList)>0){
            return true;
        }
        return false;
    }

    public function url_get_contents ($Url) {
        if (!function_exists('curl_init')){
            //curl not exists
            return "";
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
     /*
      * function : getSplitList()
      * param : $arrHeader, $headers, $arrList
      * 
      */   
      public function getCSVMergeArray($arrHeader, $headers, $arrList) {
            $counterVal = count($arrList);
            $counterValHeaders = count($headers);
            for($k=0;$k<$counterVal;$k++) {
              $arrExPortList[$k][] = $k+1;
              for($index=0; $index<$counterValHeaders;$index++) {
                  $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
              }
            }
            $arrTotalList = array_merge((array)$arrHeader, (array)$arrExPortList);
            return $arrTotalList;
      }
      
      
    public static function setFlashLogin(){   
      $flagLogin = sfContext::getInstance()->getUser()->getAttribute('lastLoginFlag');
       if($flagLogin){
        $lastLogin = sfContext::getInstance()->getUser()->getAttribute('lastLogin');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url','Tag'));
        
        sfContext::getInstance()->getUser()->setFlash('lastlogin', sprintf("Last Log In : $lastLogin"),false);
        
//        sfContext::getInstance()->getUser()->setFlash('lastlogin',
//        sprintf("<strong>Last Log In :</strong> $lastLogin"."&nbsp;&nbsp;&nbsp;&nbsp;".link_to('Report Abuse','welcome/OpenInfoBoxReportAbuse',
//        array('id'=>'reportabuse'))),false);
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('lastLoginFlag');
      }
   }
   /*function convertDbDateToXsdDate()
    * param: $dbDate
    * return :  xsd format date time
    * wp052
    */
    public function convertDbDateToXsdDate($dbDate) {
        $dtokens=explode(' ', $dbDate);
        $xdate = implode('T',$dtokens);
        return $xdate;
    }
}
?>
