<?php
/**
 * Project Template formating helper.
 * You may wonder why the helpers are named according to the underscore syntax rather than the camelCase convention,
 * used everywhere else in symfony. This is because helpers are functions, and all the core PHP functions use the
 * underscore syntax convention. *
 * @package    template objects Formating helpers
 * @path       /lib/Widget/templateHelper.php
 */

/**
 *
 * @param <string> $content
 * @param <array> $attr
 * @param <boolen> $empty
 */
function ePortal_legend($content,array $attr= array(), $empty =false, $class=false) {
  $html ='';
  $attributes ='';
  if(!$class)
    $class= 'formHead';
  if(empty($content) && !$empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  $attributes .='class=" '.$class.'"';
  $html .= "<div {$attributes}> {$content}</div>";
  return $html;
}

function ePortal_listinghead($content,array $attr= array(), $empty =false) {
  $html ='';
  $attributes ='';
  $class= 'listHead';
  if(empty($content) && !$empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  $attributes .='class=" '.$class.'"';
  $html .= "<div {$attributes}> {$content}</div>";
  return $html;
}

/**
 *
 * @param <type> $content
 * @param <type> $attr
 * @param <type> $empty
 * @return <type> page heading in admin
 */
function ePortal_pagehead($content,array $attr= array(), $empty =false) {
  $html ='';
  $attributes ='';
  $class= '';  
  if(empty($content) && !$empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  //echo "<pre>"; print_r($class); die();
  $attributes .=' class="'.$class.'"' ;
  $html .= "<div id='loadArea'><h2>{$content}</h2>";
  //echo "<pre>"; print_r($attr); echo "INR-".key_exists('showFlash',$attr)."-".str($attr['showFlash'], 'false');die;
  if(key_exists('showFlash',$attr) && (strcasecmp($attr['showFlash'], 'false') == 0) ) {
    return $html;
  }
  $sfU =sfContext::getInstance()->getUser();
  if ($sfU->hasFlash('notice')){
    $html.='<div id="flash_notice" class="error_list" ><span>';
    $html .= nl2br($sfU->getFlash('notice'));
    $html .='</span></div>';
  }
  if ($sfU->hasFlash('error')){
    $html .= '<div id="flash_error" class="error_list">
          <span>'.nl2br($sfU->getFlash('error')).
          '</span>
        </div>';
  }
  $html .="<div class='clear'></div>";
  return $html;
}


/**
 * This function is only called for the heading of pages being called without authentication.
 *
 * @param <type> $content
 * @param <type> $attr
 * @param <type> $empty
 * @return <type> page heading in admin
 */
function ePortal_pagehead_unAuth($content,array $attr= array(), $empty =false) {
  $html ='';
  $attributes ='';
  $class= '';  
  if(empty($content) && !$empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  //echo "<pre>"; print_r($class); die();
  $attributes .=' class="'.$class.'"' ;
  $html .= "<div ><h3>{$content}</h3>";
  //echo "<pre>"; print_r($attr); echo "INR-".key_exists('showFlash',$attr)."-".str($attr['showFlash'], 'false');die;
  if(key_exists('showFlash',$attr) && (strcasecmp($attr['showFlash'], 'false') == 0) ) {
    return $html;
  }
  $sfU =sfContext::getInstance()->getUser();
  if ($sfU->hasFlash('notice')){
    $html.='<div id="flash_notice" class="error_list" ><span>';
    $html .= nl2br($sfU->getFlash('notice'));
    $html .='</span></div>';
  }
  if ($sfU->hasFlash('error')){
    $html .= '<div id="flash_error" class="error_list">
          <span>'.nl2br($sfU->getFlash('error')).
          '</span>
        </div>';
  }
  $html .="<p>&nbsp;</p>
      </div>";
  return $html;
}




/**
 * Display hiligheted blocks
 * @param <type> $heading
 * @param <type> $content
 * @param <type> $attr
 * @param <type> $empty
 * @return <type>
 */
function ePortal_highlight($content, $heading='', array $attr= array(), $empty=false) {
  $html ='';
  $attributes ='';
  $class= 'highlight';
  $id ='';
  if(empty($content) && $empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      elseif($k=='id'){$id .=''.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  $attributes .='class="'.$class.'"' ;
  $attributes .='id="'.$id.'"' ;
  $html .= "<div {$attributes}>";
  $html .= "<span><b>{$heading}</b> <br \>{$content}</span></div>";
//  $html .= "<span>{$content}</span></div>";

  return $html;
}

/**
 *
 * @param <array> $thead  array of heading to be display
 * @param <array> $tbody  Recordset to be displayed
 * @param <array> $attr   settings required for the table
 * @return <type>
 * $attr['display']   <array>   ('1','5');
 * $attr['no_record'] <string>  No Records found.;
 * $attr['sno']       <boolean> true;
 */
function ePortal_tGrid(array $thead,$tbody, $attr=array()) {
  if(get_class($tbody) !=''){$tbody = $tbody->getRawValue();}
  $sno =true;
  $display = array();
  $no_record = 'No records found.';
  $empty = false;
  $sno_index = 1;
  $link = array();

  # overwrite default values with user defined values
  extract($attr,EXTR_OVERWRITE); //extract($attr, EXTR_PREFIX_SAME,'user');

  $rows = count($tbody);
  # if no record found and $empty attribute set to true return blank block;
  if(empty($rows) && $empty){return;}

  $r1 = ($rows)?$tbody[0]:0; # record #1
  if($r1 && empty($attr['display'])){
    $cols = count($r1);
    $display = array_combine(array_keys($r1),array_keys($r1));
    $headings = array_keys($r1);
  }else{
    $cols = count($thead);
    $headings = array_keys($thead);
  }
  //else{
  //    $cols = count($thead);
  //    $headings = array_keys($thead);
  //  }



  $html ="<table class='tGrid'> ";
  # add table headings
  $html .='<thead><tr>';
  if(isset($sno) && !empty($sno)) {
    $html .='<th style="width:35px;">S.No.</th>';
  }

  foreach($display as $k=>$v){
    if(in_array($k,$headings)){ //echo $v;

      $style='';
      $css = explode('|',$v);

      if(is_array($css)){ @list($align,$width) = $css ; }
      if(!empty($width) && $width!='auto'){$style .= "width: {$width};";}
      if(!empty($align) && $align!='auto'){$style .= "text-align: {$align};";}
      //$html .="<th style='{$style}'>".(@$thead[$k])." ({$k})</th>" ;
      $html .="<th style='{$style}'>".(@$thead[$k])." </th>" ;
    }
  }
  $html .='</tr></thead>' ;

  # add table body contents
  $html .='<tbody>';
  if(empty($rows)){
    $html .="<tr><td colspan='".(($sno)?count($display)+1:count($display))."'><center class='XY20'>{$no_record}</center></td></tr>" ;
  }else {

    //echo "<h1>".print_r($tbody)." <br>: ".print_r($display)."</h1>" ;
    foreach($tbody as $data ){

      $html .="<tr>";
      if(isset($sno) && !empty($sno)) {
        $html .="<td>".$sno_index++."</td>"  ;
      }

      foreach($display as $k=>$v){

        if(!empty($display) && in_array($k,$headings)){
          $style='';
          $css = explode('|',$v);
          if(is_array($css)){ @list($align,$width) = $css ; }
          //if(!empty($width) && $width!='auto'){$style .= "width: {$width};";}
          if(!empty($align) && $align!='auto'){$style .= "text-align: {$align};";}

          $html .="\r\n<td style='{$style}'>";
          //echo"<h1>".print_r($link)."</h1>" ;
          //print_r($link.":".$k."<br>");
          if(!empty($link) && array_key_exists($k,$link)){
            //echo "<h1>LINK</h1>";print_r($headings);
            //  $parse = array('%default%'=>$data[$k]);
            foreach($headings as $headKey){
              $parse["%{$headKey}%"] = @$data[$headKey];
            }
            //$context = strtr($link[$k]['context'], $parse);
            //$context = strtr(implode(';echo ',$link[$k]['context']), $parse);
            $context ='';
            foreach ($link[$k]['context'] as $linkP ){
              $context .= "\$html .= ' '.".strtr($linkP, $parse).";";
            }

            //print_r($context);echo "<br>";
            eval($context) ;
            //$html .=(!empty($link[$k]['text']))? link_to($link[$k]['text'],$link['path']):link_to($data[$k],$link[$k]['path']);
          }else{
            $html .= $data[$k];
          }
          $html .="</td>" ;

        }
      }
      $html .="</tr>" ;
    }
  }
  $html .='</tbody>';

  # add table footer
  $html .="<tfoot><tr><td colspan='".((!empty($display))?count($display)+(($sno)?1:0):count($thead))."'></td></tr></tfoot>";

  $html .= "\r\n</table>";

  return $html;
}
//Function for making http url to https url
//its,useful when you want to make forcefully your url with ssl(like http://example.com to https://example.com)
function secure_url_for($internal_uri) {
  $context = sfContext::getInstance();
  $request = $context->getRequest();
  $host = $request->getHost();
  $uri = url_for($internal_uri);
  $surl = sprintf('%s://%s%s', 'https', $host, $uri);
  return $surl;
}

/**
 * @param <type> $label
 * @param <type> $value
 * @param <type> $empty      if flase it
 * @param <type> $empty_text
 * @return <type>
 */
function ePortal_renderFormRow($label,$value,$empty=true,$empty_text=''){

  if(!$empty && empty($value)){
    if(empty($empty_text)){return;}
    $value=$empty_text;

  }
  $html = <<<EOF
<dl>
  <dt><label>{$label}</label></dt>
  <dd>{$value}</dd>
</dl>
EOF;

  return $html;
}

/**
 *
 * @param <type> $fName
 * @param <type> $mName
 * @param <type> $lName
 * @return <type> returns formatted full name as SURNAME FIRSTNAME LASTNAME
 */
function ePortal_displayName($title="", $fName="",$mName="",$lName=""){

  return ucwords (strtolower($title." ".$lName." ".$fName." ".$mName));
}

function ePortal_popup($title,$content) {
  $popBlock = <<<EOF
<div class='popWrapper'><div id='mainDiv' class='parentDisable'><table border='0' id='popup' width='400' height='400'><tr><td><div style='padding:50px;'><h2>{$title}</h2><p>{$content}</p><p><a href='#' onClick='return hide()'><b>[Close]</b></a></p></div></td></tr></table></div></div>
EOF;
  $html = <<<EOF
<style>

.pageWrapper {z-index:0}
.popWrapper {position:absolute;z-index:999;left:0px;top:0px;}
.parentDisable {
z-index:99;
width:100%;
height:100%;
display:block;
position:fixed;
top:0px;
left:0px;
background-color: #ccc;
color: #aaa;
opacity: .98;
filter: alpha(opacity=98);
}
#popup {
width:400px;
height:200px;
position:relative;
margin:0px auto;
margin-top:50px;
color: #000;
background-color: #fff;
}
</style>
<script>
showPop = false ;
function pop() {
showPop =true ;
  $('#mainDiv').css('display','block');
  return false
}
function hide() {
  $('#mainDiv').css('display','none');
  return false
}
$('body').prepend("{$popBlock}");
/*$('document').ready(function(){
  $('body').prepend("{$popBlock}");
 if(showPop){ pop();}
});*/
wHeight =$(document).height() ;
wWidth = $(document).width() ;
$('#mainDiv').css('height',wHeight).css('width',wWidth);

</script>
EOF;

  return $html;
}



/**
* link_to with additionnal param : $app
*
* @param string name of the link, i.e. string to appear between the <a> tags
* @param string 'module/action' or '@rule' of the action
* @param array additional HTML compliant <a> tag parameters
* @param string name of the application
* @return string XHTML compliant <a href> tag
*/
function link_to_app($name = '', $internal_uri = '', $options = array(),$app = SF_APP){
  //  echo "<pre>";
  //  $context = sfContext::getInstance();
  //  $request = $context->getRequest();
  //  print_r(get_class_methods(sfConfig));
  //  print_r(sfConfig::getAll());
  //  echo "</pre>";
  //  die();
  $request = sfContext::getInstance()->getRequest();
  $root = $request->getRelativeUrlRoot();

  define('SF_ENVIRONMENT',sfConfig::get('sf_environment'));

  $environment=(SF_ENVIRONMENT=='prod') ? "" : "_".SF_ENVIRONMENT;
  $protocole=(sfContext::getInstance()->getRequest()->isSecure()) ? "https" : "http";
  $url=$protocole."://".$_SERVER["SERVER_NAME"].$root."/".strtolower($app).$environment. ".php/".$internal_uri;
  return link_to($name, $url, $options);
}

function formRowFormatTable($obj){
  $html ="";
  //  echo "<pre>";
  //  echo $obj->getWidget()->getAttribute('class');
  //echo preg_match('hidden',$obj->getWidget()->getAttribute('class'));

  $hide = (ereg('hidden',$obj->getWidget()->getAttribute('class')))?'class="hidden"':'';
  if($obj->hasError()) {
    $html .= "<tr id='{$obj->renderId()}_row' class='error'><th> <label>".$obj->renderLabelName()."</label></th>";
    $html .= "<td>"."<div class='error'>".$obj->getError()."</div>".$obj->render()."</td></tr>";
  }else{
    $html .= "<tr  id='{$obj->renderId()}_row' {$hide}><th> <label>".$obj->renderLabelName()."</label></th>";
    $html .= "<td>".$obj->render()."</td></tr>";
  }
  return $html;

}

function formRowFormatRaw($key,$val){
  $html =  "<dl><div class=\"dsTitle4Fields\"><label>$key</label></div><div class=\"dsInfo4Fields\">{$val}</div></dl>";
  return $html;
}

function formRowComplete($key,$val,$addtext='',$label='',$err='',$dlid='',$renderError='',$errorClass='cRed'){
  $html =  "<dl id=$dlid><div class=\"dsTitle4Fields\"><label for=$label>$key</label></div><div class=\"dsInfo4Fields\">{$val}";
  if(!empty($addtext)){
  	$html .= "<br />$addtext<br />";
  }
  $html .= "<div id=$err class=\"$errorClass\">$renderError</div></div></dl>";
  return $html;
}

function simpleRow($key, $val,$dsinfo="dsInfo"){
  $html =  "<div class=\"dsTitle\">$key</div><div class=$dsinfo>{$val}</div>";
  return $html;
}


function formRowSetValidator($form){
  echo "<pre>";
  print_r(get_parent_class($form));
  foreach ($form as $emName=>$elem){
   echo "<br>".$emName." ::";
   print_r(get_class($elem));
    
    if($elem instanceOf sfFormFieldSchema){
    //  echo "<b>".$emName."</b>";
     // print_r(get_class_methods($elem));
     formRowSetValidator($elem);

    }
// continue;
    if($form->getValidatorSchema($emName) && $form->getValidatorSchema($emName)->getOption('required')){
      $elemLabel = $elem->renderLabelName();
      //echo " : ".$elemLabel;
      if(strpos($elemLabel,'*')){$elemLabel = substr(trim($elemLabel),0,strlen($elemLabel)-1);}
      $elem->getWidget()->setLabel($elemLabel."<sup>*</sup>");
    }
    echo "<br>";

  }
}

function verisign(){
  $html = <<<EOF
<div id="versignLogo">
<script type="text/javascript" >
<!--
dn="www.pay4me.com";
lang="en";
aff="VeriSignCACenter";
tpt="opaque";
vrsn_style="WW";
splash_url="https://trustsealinfo.verisign.com";
seal_url="https://trustsealinfo.verisign.com";
u1=splash_url+"/splash?form_file=fdf/splash.fdf&dn="+dn+"&lang="+lang;
u2=seal_url+"/getseal?at=0&&sealid=2&dn="+dn+"&aff="+aff+"&lang="+lang;
function vrsn_splash() {
    if (opener && !opener.closed){
        opener.focus();
    } else {
        tbar = "location=yes,status=yes,resizable=yes,scrollbars=yes,width=560,height=500";
        var sw = window.open(u1,'VRSN_Splash',tbar);
        if(sw) {
            sw.focus();
            opener=sw;
        }
    }
}

var ver=-1;
var v_ua=navigator.userAgent.toLowerCase();
var re=new RegExp("msie ([0-9]{1,}[\.0-9]{0,})");
if (re.exec(v_ua) != null)
 ver = parseFloat( RegExp.$1 );
var v_old_ie=(v_ua.indexOf("msie")!=-1);
if (v_old_ie) {
 v_old_ie = ver < 5;
}

function v_mact(e){
 if (document.addEventListener) {
  var s=(e.target.name=="seal");
   if (s) { vrsn_splash(); return false; }
 }else if(document.captureEvents) {
  var tgt=e.target.toString(); var s=(tgt.indexOf("splash")!=-1);
  if (s){ vrsn_splash(); return false; }
 }
 return true;
}
function v_mDown(event) {
	if (document.addEventListener) return true;
	event = event || window.event;
	if (event) {
		if (event.button == 1) {
			if (v_old_ie) { return true; }
			else { vrsn_splash(); return false; }
		} else if (event.button == 2) { vrsn_splash(); return false; }
	} else { return true; }
}
/*
document.write("<a HREF=\"javascript:vrsn_splash()\" tabindex=\"-1\"><IMG NAME=\"seal\" BORDER=\"true\" SRC=\""+u2+"\" oncontextmenu=\"return false;\" alt=\"Click to Verify - This site has chosen a VeriSign SSL Certificate to improve Web site security\"></A>");

 if((v_ua.indexOf("msie")!=-1) && (ver>=7)) {
  var plat=-1;
  var re=new RegExp("windows nt ([0-9]{1,}[\.0-9]{0,})");
  if (re.exec(v_ua) != null)
   plat = parseFloat( RegExp.$1 );

  // We might need to add "plat <= 6.x" later if we don't need to support PTP for later windows version
  // Don't go to PTP for windows 2003, plat = 5.2
  if ((plat >= 5.1) && (plat != 5.2)) {
   document.write("<div style='display:none'>");
   document.write("<img src='https://extended-validation-ssl.verisign.com/dot_clear.gif'/>");
   document.write("</div>");
  }
 }
*/
if (document.addEventListener){ document.addEventListener('mouseup', v_mact, true); } 
else {
  if (document.layers){
    document.captureEvents(Event.MOUSEDOWN); document.onmousedown=v_mact;
  }
}
function v_resized(){
  if(pageWidth!=innerWidth || pageHeight!=innerHeight){
    self.history.go(0);
  }
}
if(document.layers){
  pageWidth=innerWidth; pageHeight=innerHeight; window.onresize=v_resized;
}
// -->
</script>
<a  href="javascript:vrsn_splash()">
EOF;
$alt = "Click to Verify - This site has chosen a VeriSign SSL Certificate to improve Web site security";
$html .= image_tag('/img/new/logo-versign.png', array('width' => 92, 'height' => 46, 'border' => 0,'alt'=>$alt));

$html .= <<<EOF
</a>
</div>
EOF;
return $html;

}

function format_amount($amount, $currency="", $convert_to_kobo=0){
    if($amount =='') {
        $amount =0;
    }
  if($convert_to_kobo) {$amount=$amount/100;}
    if($currency){
       #sfLoader::loadHelpers('Asset');
       sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
       #sfLoader::loadHelpers('Tag');
       sfContext::getInstance()->getConfiguration()->loadHelpers(array('Tag'));
       if((int)$currency==2)
            $filepath_naira = image_tag('../img/dollar.gif',array('alt'=>'USD'));
       else if((int)$currency==1)
            $filepath_naira = image_tag('../img/naira.gif',array('alt'=>'NGN','class'=>'naira_symbol'));
       else if((int)$currency==3)
            $filepath_naira = image_tag('../img/cedi.gif',array('alt'=>'GHS'));
      else if((int)$currency==4)
            $filepath_naira = image_tag('../img/kenyan shilling.gif',array('alt'=>'KES'));
        return html_entity_decode($filepath_naira . number_format($amount, 2, '.', ''));
    }
    return number_format($amount, 2, '.', '');

}

function convertToKobo($nairaAmount) {
    return number_format(($nairaAmount*100), 0, '.', '');
  }

function getNextSwap($swap_schedule, $date_from="") {
    if($date_from == "") {
      $date_from=date('Y-m-d H:i:s');
    }
    $str_date_from = strtotime($date_from);
    $month = date('m', $str_date_from);
    $year = date('Y', $str_date_from);
    $day = date('d', $str_date_from);
    $hour = date('H', $str_date_from);
    $min = date('i', $str_date_from);
    $sec = date('s', $str_date_from);
    switch($swap_schedule) {
      case 'weekly':
        $next_swap = mktime($hour, $min ,$sec, $month, $day+7, $year);
        break;
      case 'monthly':
        $next_swap = mktime($hour, $min ,$sec, $month+1, $day, $year);
        break;
      case 'immediate':
        $next_swap = mktime($hour, $min ,$sec, $month, $day, $year);
        break;
    }
    return date('Y-m-d H:i:s',$next_swap);
  }

  function getRechargeDate($days) {

    if($days) {
      $date_from=date('Y-m-d H:i:s');
      $str_date_from = strtotime($date_from);
      $month = date('m', $str_date_from);
      $year = date('Y', $str_date_from);
      $day = date('d', $str_date_from);
      $hour = date('H', $str_date_from);
      $min = date('i', $str_date_from);
      $sec = date('s', $str_date_from);
      $available_timestamp = mktime($hour, $min ,$sec, $month, $day+$days, $year);
      return date('Y-m-d H:i:s',$available_timestamp);
    }
    return date('Y-m-d H:i:s');
  }

  function consolidatePayment(){
    $next_execute_mins = sfConfig::get('app_amount_consolidation_mins');
    $date_from=date('Y-m-d H:i:s');
    $str_date_from = strtotime($date_from);
    $month = date('m', $str_date_from);
    $year = date('Y', $str_date_from);
    $day = date('d', $str_date_from);
    $hour = date('H', $str_date_from);
    $min = date('i', $str_date_from);
    $sec = date('s', $str_date_from);
    $available_timestamp = mktime($hour, $min+$next_execute_mins ,$sec, $month, $day, $year);
    return date('Y-m-d H:i:s',$available_timestamp);
  }

  function formatDate($date){
    return date('d M, Y, g:i a',strtotime($date));
  }
  function getMerchantLogo($merchant_name) {
  	$filename=scandir('./logo');
  	$filename= array_slice($filename, 2);
  	foreach($filename as $key =>$value){
  		 $part[]=chop($value,'.png');
  		 
  	}
  	if(in_array($merchant_name,$part))
  	{
  		return $img = html_entity_decode(image_tag('../logo/'.$merchant_name.'.png'));
  	}
  	else {
  		return $img = html_entity_decode(image_tag('../logo/'.'logo_not_available.png'));
   	}
  } 
   function getCurrencyImage($currency_id) {
      $currencyObj = Doctrine::getTable('CurrencyCode')->find($currency_id);
      $currency_name = strtolower($currencyObj->getCurrency());
      $currency_alt =  $currencyObj->getCurrencyCode();
             sfContext::getInstance()->getConfiguration()->loadHelpers(array('Tag'));
       sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));

      return $img = html_entity_decode(image_tag('../img/'.$currency_name.'.gif',array('alt'=>''.$currency_alt.'')));
    }
  
    function encrypt($string) {
     $Len_Simbolos = strlen(sfConfig::get('app_inscrypt_key'));
     if($Len_Simbolos <26)
     {
      echo 'ERROR';
      die;
    }
    $Len_Str_Message = strlen($string);
    $Str_Encoded_Message = "";
    for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
      $Byte_To_Be_Encoded = substr($string, $Position, 1);
      $Ascii_Num_Byte_To_Encode = ord($Byte_To_Be_Encoded);
      $Rest_Modulo_Simbolos = ($Ascii_Num_Byte_To_Encode + $Len_Simbolos) % $Len_Simbolos;
      $Plus_Modulo_Simbolos = (int)($Ascii_Num_Byte_To_Encode / $Len_Simbolos);
      $Encoded_Byte = substr(sfConfig::get('app_inscrypt_key'), $Rest_Modulo_Simbolos, 1);
      if ($Plus_Modulo_Simbolos == 0) {
        $Str_Encoded_Message .= $Encoded_Byte;
      }else {
        $Str_Encoded_Message .= $Plus_Modulo_Simbolos . $Encoded_Byte;
      }
    }
    return base64_encode($Str_Encoded_Message);
  }
   function getMerchantCategoryName($category_id="") {
   	if(empty($category_id)) {
   		$categoryObj1 = Doctrine::getTable('Category')->getCategory();
   		foreach($categoryObj1 as  $key=>$value)
   		{
   			$category[]= $value['name'];
   		} 
   		return $category;
   	}
   	$categoryObj = Doctrine::getTable('Category')->find($category_id);
   	$category_name = $categoryObj->getName();
   	return $category_name;
   }	
   function decrypt($string) {
    $Len_Simbolos = strlen(sfConfig::get('app_inscrypt_key'));
    if($Len_Simbolos <26)
    {
      echo 'ERROR';
      die;
    }
    $string=base64_decode($string);
    $Len_Str_Message = strlen($string);
    $Str_Decoded_Message = "";
    for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
      $Plus_Modulo_Simbolos = 0;
      $Byte_To_Be_Decoded = substr($string, $Position, 1);
      if ($Byte_To_Be_Decoded > 0) {
        $Plus_Modulo_Simbolos = $Byte_To_Be_Decoded;
        $Position++;
        $Byte_To_Be_Decoded = substr($string, $Position, 1);
      }
      //finding the position in the string
      // $SIMBOLOS
      $Byte_Decoded = 0;
      for ($SecondPosition = 0; $SecondPosition < $Len_Simbolos; $SecondPosition++) {
        $Byte_To_Be_Compared = substr(sfConfig::get('app_inscrypt_key'), $SecondPosition, 1);
        if ($Byte_To_Be_Decoded == $Byte_To_Be_Compared) {
          $Byte_Decoded = $SecondPosition;
        }
      }
      $Byte_Decoded = ($Plus_Modulo_Simbolos * $Len_Simbolos) + $Byte_Decoded;
      $Ascii_Num_Byte_To_Decode = chr($Byte_Decoded);
      $Str_Decoded_Message .= $Ascii_Num_Byte_To_Decode;
    }
    return $Str_Decoded_Message;
  }
 