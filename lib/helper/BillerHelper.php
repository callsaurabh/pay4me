<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class billerHelper{
    public function getEncryptedUrl($ReqUrl,$ifaNo,$sceretKey="",$encryptType, $vector=""){
        if(""==$sceretKey)
        $sceretKey= "passwordDR0wSS@P6660juht";

        $secretCode=$this->getSecretCode($ifaNo,$sceretKey,$encryptType,$vector);

        $encryptUrl=$ReqUrl."?id=".$ifaNo."&code=".$secretCode;

        return $encryptUrl;
    }

    public function getSecretCode($ifaNo,$sceretKey,$encryptType,$vector){
        switch($encryptType)
        {
            case "SHA256":
                return  hash('sha256',$ifaNo);
                break;
            case "3DES":
                $code = $this->getDEScode($ifaNo,$sceretKey,$vector);
                return $code;
                break;
            default:
                return  hash('sha256',$ifaNo);
                break;
        }
    }

    public function getDEScode($ifaNo, $key,$iv=""){

        // get the amount of bytes to pad
        $extra = 8 - (strlen($ifaNo) % 8);
        // add the zero padding
        if($extra > 0) {
            for($i = 0; $i < $extra; $i++) {
                $ifaNo .= "\0";
            }
        }
        if(""==$iv)
            $iv=$this->getVector();
             
        // hex encode the return value
        return bin2hex(mcrypt_cbc(MCRYPT_3DES, $key, $ifaNo, MCRYPT_ENCRYPT, $iv));
    }
     public function getVector(){
         $iv = "password";
         return $iv;
     
     }
     public function getBillInformation($service,$billernum)
     {
         $billerObj = integrationServiceFactory::getService('biller');
         $billerServiceId = $service;
         $svcParams = array('identification_num' => $billernum, 'merchantServiceId' => $billerServiceId);

         $xmlRequest = $billerObj->getSearchXml($svcParams);

         $notifyResponse = $billerObj->notification($xmlRequest, '', $svcParams['merchantServiceId']);
         return $notifyResponse;
     }
}
?>
