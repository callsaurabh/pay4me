<?php
/* Class : UserRoleHelper
 * Purpose : get Role Base country,branch etc.
 * Date : 02-12-2011
 */
class UserRoleHelper{
/*
  * function : getBankUserList()
  * Purpose : make an array for all bak user role
  * param : N/A
  * WP047 -CR080
  * Date : 22-11-2011 
  */   
  public function getBankUserList(){
     return $bankUserArray = array(
                                  ''=>'--All User Roles--',
                                  'bank_admin'=>'Bank Admin',
                                  'bank_branch_report_user'=>'Bank Branch Report User',
                                  'bank_country_head'=>'Bank Country Admin',
                                  'bank_e_auditor'=>'Bank eAuditor Admin',
                                  'bank_user'=>'Bank User',
                                  'country_report_user'=>'Country Report User',
                                  'report_bank_admin'=>'Report Bank Admin'
                                );
  }
  /*
  * function : getBankUserGroupCountry()
  * Purpose : make an array for all bak user which have country
  * param : N/A
  * WP047 -CR080
  * Date : 02-12-2011 
  */  
  public function getBankUserGroupCountry(){
      return $arrayGroupCountry=array('bank_country_head'=>'bank_country_head',
                         'bank_user'=>'bank_user',
                         'bank_branch_report_user'=>'bank_branch_report_user',
                         'country_report_user'=>'country_report_user');
  }

  public function fetchBroadCastMessage($userId,$ewalletType=''){
      $userRole = sfcontext::getInstance()->getUser()->getGroupName();
      $userBankId = '';
      if($userRole != 'ewallet_user'){
          $userBank = Doctrine::getTable('BankUser')->getBankIdName($userId);
          if(count($userBank)){
            $userBankId = $userBank['0']['id'];
          }
      }

      $userPermission = Doctrine::getTable('BroadcastMessage')->getUserPermission($userRole,$userBankId,$ewalletType);
      if($userPermission){
          $userPermission['0']['BroadcastMessagePermission']['0']['specific_to'];
          $roleArray = array('bank_admin',"bank_branch_report_user","bank_country_head","bank_e_auditor","bank_user","country_report_user",'report_bank_admin');
          if(in_array($userRole, $roleArray)){
              return $userPermission;
          }
          if($userRole == 'ewallet_user'){
              return $userPermission;
          }
      }
  }

  // [WP061] To show legal notice to users of any Bank
      public function fetchLegalMessage($userId){
      $userRole = sfcontext::getInstance()->getUser()->getGroupName();
      $userBankId = '';
      if($userRole != 'ewallet_user'){
          $userBank = Doctrine::getTable('BankUser')->getBankIdName($userId);
          if(count($userBank)){
            $userBankId = $userBank['0']['id'];
          }
      }
      $userPermission = Doctrine::getTable('BroadcastMessage')->getLegalMessage($userRole,$userBankId);

      if($userPermission){
          return $userPermission;
      }
  }
}