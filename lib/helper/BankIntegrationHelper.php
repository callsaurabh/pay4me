<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class bankIntegrationHelper{

 public function getSerialize($type,$request){
   $dataArry = $this->generateRequestArray($type,$request);
   //serialize array
     return $serializedArray = serialize($dataArry);
}


 public function generateRequestArray($type,$request){
  if($type == 'all'){
  $bankReport_obj = bankPaymentSchedulerServiceFactory::getService(bankPaymentSchedulerServiceFactory::$TYPE_BASE);
  $stTime = $bankReport_obj->formatTime($request['start_time']);
  $enTime = $bankReport_obj->formatTime($request['end_time']);
   //make array
     $seheduleArray = array('schedule'=>'all','all'=>array());

     $seheduleArray['all']['start_time'] = $stTime;
     $seheduleArray['all']['end_time'] = $enTime;
     return $seheduleArray;
  }elseif($type == 'day'){
      unset($request['status']);
      unset($request['type']);
      $dayArray = array(1=>'mon',2=>'tues',3=>'wednes',4=>'thurs',5=>'fri',6=>'satur',7=>'sun');
      $seheduleArray = array('schedule'=>'day','day'=>array());
      foreach($dayArray as $key=>$val){
          $bankReport_obj = bankPaymentSchedulerServiceFactory::getService(bankPaymentSchedulerServiceFactory::$TYPE_BASE);
          $startTime = $bankReport_obj->formatTime($request[$val.'_start_time']);
          $endTime = $bankReport_obj->formatTime($request[$val.'_end_time']);
          $seheduleArray['day'][$val.'day']['start_time'] = $startTime;
          $seheduleArray['day'][$val.'day']['end_time'] = $endTime;
      }
      return $seheduleArray;
  }elseif($type == 'date'){      
      $noOfDayz = $request['noOfDays'];
      unset($request['status']);
      unset($request['type']);
      unset($request['noOfDays']);
      $seheduleArray = array('schedule'=>'date','date'=>array());
      for($i=1;$i<=$noOfDayz;$i++){
         $bankReport_obj = bankPaymentSchedulerServiceFactory::getService(bankPaymentSchedulerServiceFactory::$TYPE_BASE);
         $startTime = $bankReport_obj->formatTime($request[$i.'_start_time']);
         $endTime = $bankReport_obj->formatTime($request[$i.'_end_time']);
         $seheduleArray['date'][$i]['start_time'] = $startTime;
         $seheduleArray['date'][$i]['end_time'] = $endTime;
      }
  return $seheduleArray;
  }
}



 public function generatePassword($length='',$strength='') {


        if(empty($length)){
            $length = 9;
        }
        if(empty($strength)){
            $strength = 8;
        }
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;


    }
/**
 * WP060
 * check whethet update link will be visible or not
 * @param <int> $data
 * @param <int> $bank
 * @return <int>
 */
    public static function getUpdate($data, $bank) {
        $bundleversion = substr(strstr($data['1'], '('), 1, 5);
        $bundleId = $data['0'];
        $bundleName = $data['3'];
        $bankAcronym = Doctrine::getTable('Bank')->find($bank)->getAcronym();
        $bankSpecific = strpos($bundleName, $bankAcronym);
        if (!$bankSpecific) {
            $dirname = sfConfig::get('sf_web_dir') .'/'. sfConfig::get('app_bundles_soft_link_common') .'/'. $bundleName;
        } else {
            $dirname = sfConfig::get('sf_web_dir') .'/'. sfConfig::get('app_bundles_soft_link_bankspecific')  .'/'. $bankAcronym . '/' . $bundleName;
        }
        $symLink = readLink($dirname);
        $newVersion = explode('/', $symLink);
         if (!$bankSpecific) 
        $newVersion = $newVersion[5];
        else
         $newVersion = $newVersion[7];
        if($bundleversion >= $newVersion){
            return false;
        }else{
            return $newVersion;
        }
    }


   public function ReturnSoftLinksArray($directory, $bank) {
        $dh = opendir($directory);

        if ($dh = opendir($directory)) {
            $fileNames = array();
            while (($file = readdir($dh)) !== false) {
                if (!is_dir($directory . "/" . $file)) {
                    $fileNames[$file] = array('hardLink' => readlink($directory . "/" . $file), 'softLink' => $directory . "/" . $file);

                } else {

                        if ($file == $bank) {
                        $dir_for_iteration = $directory . "/" . $file;
                        $dh_bank = opendir($dir_for_iteration);
                        while (($files = readdir($dh_bank)) !== false) {
                            if ($files != "." && $files != "..") {

                                $fileNames[$files] = array('hardLink' => readlink($dir_for_iteration . "/" . $files), 'softLink' => $dir_for_iteration . "/" . $files);
                            }
                        }
                    }
                }
            }
            closedir($dh);

            return $fileNames;
        } else {
            return false;
        }
    }


    public function checkFirstResponse($requestId , $count) {
            $count--;
            if ($count > 0) {
                sleep(1);
                $commandResponseDetails = Doctrine::getTable('MwResponse')->findByRequestId($requestId);
                if($commandResponseDetails->count()){
                    return $commandResponseDetails->getFirst();
                }else
                return $this->checkFirstResponse($requestId , $count);
            }
            else
                return false;
        }



}

?>
