<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of commonHelper
 *
 * @author saurabh
 */
class commonHelper {
    //put your code here
    static function isMerchantDisableForPayment($merchant_id){
        if(isset($merchant_id) && in_array($merchant_id, sfConfig::get("app_disable_merchant_for_payment"))){
            return true;
        } else {
            return false;
        }
    }
}
