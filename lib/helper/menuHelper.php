<?php
/**
 * Project Ordered List of menu.
 * @package    Menu Render Formating
 * @path       /lib/Widget/MenuHelpers.php
 */

class menuHelper {

private $pointCount=0, $mainMCount= 0 ;
   public function setMenuSession(){
      $sessionMenu=sfContext::getInstance()->getUser()->getAttribute('menu');
     if(!isset($sessionMenu) || ''!=$sessionMenu){
       $MenuPluginObj =new menuManager();
       $menuArr = $MenuPluginObj->getUserMenu();

      $sessionMenu=sfContext::getInstance()->getUser()->setAttribute('menu', $menuArr);
    }
   }

   public function renderMenuYAML($elm,$SMPoint=0,$endPoint){
      $permissions = $_SESSION['symfony/user/sfUser/credentials']; //sfContext::getInstance()->getUser()->getAllPermissionNames();
      echo "<ul>";
      if(count($elm)){


        foreach($elm as $node)
        {
            $this->pointCount++;

            if($this->pointCount<$SMPoint )
                continue;
            if($endPoint!='' && $this->mainMCount >= $endPoint)
                continue;

             if($endPoint!='')
                $this->mainMCount++;
//            if($this->pointCount>=$totMenu)
//                break;

          //print_r($permissions);echo"<hr>";continue;
          $perm = @$node['perm'];
          if(!in_array($perm,$permissions) && !empty($perm)) continue;
          $url = @$node['url'];
          echo "<li>";

          if (isset($node['child'])&& count($node['child'])){
            echo (!empty($url) && $url !='#')? link_to($node['label'],$url):"<a href='#'>".$node['label']."</a>";
            //print_r(count($node['child']));
            $this->renderMenuYAML($node['child'],$SMPoint,'');
          }
          else{
            echo (!empty($url))? link_to($node['label'],$url):$node['label']."";
          }
          echo "</li>";
        }
      }
      echo "</ul>";
    }#end of function
     public function renderMenuYAMLPerms ($elem) {
         
        $userPerms = $_SESSION['symfony/user/sfUser/credentials'] ;// sfContext::getInstance()->getUser()->getAllPermissionNames();
        foreach ($elem as $k=>$aElem) {
    //     / echo "$k ";
          $res = $this->isValidMenuItem($aElem, $userPerms);
          // print_r ($res);
          if (!$res) {
            unset($elem[$k]);
          } else {
            $elem[$k] = $res;
          }
        }
        return $elem;
      }
     public function isValidMenuItem ($elem, $uperms) {
    if(!array_key_exists('perm', $elem)) {
      $myperm = '';
    } else {
      $myperm = $elem['perm'];
    }
    // check if my permission is there in current user permissions list
    if (!empty($myperm)){
      if (!in_array($myperm, $uperms)) {
        return false;
      }
      else{
         if(array_key_exists('eval', $elem)) {
             switch($elem['eval']){

                 case 'menu/display':
                          $valid_user = $this->chk_merchant_bank_association($elem['url']);
                           if(!$valid_user)
                                return false;
                          break;
                 case 'menu/displayBill':
                          $valid_user = $this->chk_merchant_biller_bank_association($elem['url']);
                           if(!$valid_user)
                                return false;
                          break;
                 case 'menu/recharge':
                           $valid_user = $this->chk_payment_association($elem['url']);
                           if(!$valid_user)
                                return false;
                          break;
                 case 'menu/rechargeCheck':
                           $valid_user = $this->chk_payment_association_check($elem['url']);
                           if(!$valid_user)
                                return false;
                          break;
                 case 'menu/rechargeDraft':
                           $valid_user = $this->chk_payment_association_draft($elem['url']);
                           if(!$valid_user)
                                return false;
                          break;
                 case 'menu/kyc':
                               $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                               $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
                               $kycStatus = $user_detail->getFirst()->getKycStatus();
                           if(1!=$kycStatus)
                                return false;
                          break;
                 case 'menu/changePin':
                               $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                               $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
                               $groupArray = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
                               $groupName =  array();
                               $groupName[] = $groupArray['sfGuardGroup']['name'];
                               $kycStatus = $user_detail->getFirst()->getKycStatus();
                               if(1!=Settings::isEwalletPinActive() || !in_array(sfConfig::get('app_pfm_role_ewallet_user'),$groupName))
                                return false;
                          break;
                case 'menu/splitreport':
                        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                        $groupArray = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
                        $presentGroup = $groupArray['sfGuardGroup']['name'];
                        $arrGroups = array(
                              sfConfig::get('app_pfm_role_ewallet_user')=>
                                    sfConfig::get('app_pfm_role_ewallet_user'),
                              sfConfig::get('app_pfm_role_report_admin')=>
                                    sfConfig::get('app_pfm_role_report_admin'),
                            sfConfig::get('app_pfm_role_report_portal_admin')=>
                                    sfConfig::get('app_pfm_role_report_portal_admin'),
                            sfConfig::get('app_pfm_role_admin')=>
                                    sfConfig::get('app_pfm_role_admin')
                             )  ;
                        if($this->checkUserPermissions(
                            $arrGroups,
                            $presentGroup,
                            $userId,
                            $ewalletcheck=true,
                            $bankcheck=false)){
                            return false;
                        }
                        break;

                case 'menu/summary':
                        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                        $arrGroups = array(
                              sfConfig::get('app_pfm_role_bank_admin')=>
                                    sfConfig::get('app_pfm_role_bank_admin'),
                              sfConfig::get('app_pfm_role_bank_branch_user')=>
                                    sfConfig::get('app_pfm_role_bank_branch_user'),
                              sfConfig::get('app_pfm_role_report_bank_admin')=>
                                    sfConfig::get('app_pfm_role_report_bank_admin'),
                              sfConfig::get('app_pfm_role_bank_country_head')=>
                                    sfConfig::get('app_pfm_role_bank_country_head'),
                              sfConfig::get('app_pfm_role_bank_e_auditor')=>
                                    sfConfig::get('app_pfm_role_bank_e_auditor'),
                              sfConfig::get('app_pfm_role_bank_branch_report_user')=>
                                    sfConfig::get('app_pfm_role_bank_branch_report_user'),
                              sfConfig::get('app_pfm_role_ewallet_user')=>
                                    sfConfig::get('app_pfm_role_ewallet_user'),
                              sfConfig::get('app_pfm_role_report_admin')=>
                                    sfConfig::get('app_pfm_role_report_admin'),
                             sfConfig::get('app_pfm_role_report_portal_admin')=>
                                    sfConfig::get('app_pfm_role_report_portal_admin'),
                              sfConfig::get('app_pfm_role_country_report_user')=>
                                    sfConfig::get('app_pfm_role_country_report_user'),
                               sfConfig::get('app_pfm_role_admin')=>
                                    sfConfig::get('app_pfm_role_admin') )  ;
                        $groupArray = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
                        $presentGroup = $groupArray['sfGuardGroup']['name'];
                        if($this->checkUserPermissions(
                            $arrGroups,
                            $presentGroup,
                            $userId,
                            $ewalletcheck=true,
                            $bankcheck=false)){
                            return false;
                        }
                        break;

             }

//           if($elem['eval'] == 'menu/display'){
//           $valid_user = $this->chk_merchant_bank_association($elem['url']);
//            if(!$valid_user){
//              return false;
//            }
//           }
//           else if($elem['eval'] == 'menu/recharge'){
//             $valid_user = $this->chk_payment_association($elem['url']);
//            if(!$valid_user){
//              return false;
//            }
//           }
         }

      }
    }
    // I do have permission for myself at least
    // find out if I have child
    if (!array_key_exists('child', $elem)) {
      // no child - find out if I have url
      if (array_key_exists('url', $elem) && (!empty($elem['url']))) {
        // at least I have url - I am valid entity
         if ($elem['url'] == 'userAdmin/UserQuestionAnswer') {

                    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                    $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
                    
                    $name = $arrGroup['sfGuardGroup']['name'];
                    $arrayQuestionUser = array('support', 'portal_admin','report_admin','portal_report_admin');
                    if (in_array($name, $arrayQuestionUser) ) {

                        return false;
                    } else {
                        return $elem;
                    }
                }
                //For KYC Form
                if ($elem['url'] == 'ewallet/kyc') {

                    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                    $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);

                    $name = $arrGroup['sfGuardGroup']['name'];
                    if ($name == 'ewallet_user') {

                        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
                        $ewalletTypeArr = array ('ewallet','ewallet_pay4me','both','both_pay4me');
                        if (in_array($userDetails['0']['ewallet_type'], $ewalletTypeArr) ) {
                            return $elem;
                        } else {
                            return false;
                        }
                    }
                }
                //For activate ewallet Account
                if ($elem['url'] == 'openId/activate') {
              if(sfConfig::get('app_open_id_account_show')){
                    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                    $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);

                    $name = $arrGroup['sfGuardGroup']['name'];
                    if ($name == 'ewallet_user') {

                        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
                        if ((($userDetails['0']['master_account_id'] == '') && (($userDetails['0']['ewallet_type'] == 'openid_pay4me')))
                        || (($userDetails['0']['kyc_status']) !=1  && $userDetails['0']['ewallet_type'] != 'ewallet' && $userDetails['0']['ewallet_type'] != 'both'))
                         {
                            return $elem;
                        } else {
                            return false;
                        }
                    }
                }else{
                    return false;
                }}
                //For de-activate ewallet Account
              
                if ($elem['url'] == 'openId/deactivate') {

                      if(sfConfig::get('app_open_id_account_show')){
                    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                    $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);

                    $name = $arrGroup['sfGuardGroup']['name'];
                    if ($name == 'ewallet_user') {

                        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
                        if ($userDetails['0']['master_account_id'] != '' && in_array($userDetails['0']['ewallet_type'], sfConfig::get('app_ewallet_account_type')) && ($userDetails['0']['kyc_status']) ==1 ) {
                            return $elem;
                        } else {
                            return false;
                        }
                    }
                }else{
                    return false;
                }}
                //For re-activate ewallet Account
                if ($elem['url'] == 'openId/reactivate') {

                     if(sfConfig::get('app_open_id_account_show')){
                    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                    $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);

                    $name = $arrGroup['sfGuardGroup']['name'];
                    if ($name == 'ewallet_user') {

                        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
                        if ($userDetails['0']['master_account_id'] != '' && (in_array($userDetails['0']['ewallet_type'], sfConfig::get('app_pay4me_account_type'))   && ($userDetails['0']['kyc_status']) ==1)) {
                            return $elem;
                        } else {
                            return false;
                        }
                    }
                }else{
                     return false;
                }}



                //For internet banking(Nibbs)

                if ($elem['url'] == 'report/paidByInternetBankReport') {
                     
                    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                    $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);

                    $name = $arrGroup['sfGuardGroup']['name'];
                    if ($name == 'ewallet_user' || $name =='portal_admin') {

                        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
                        $pay4me_openid_arr = sfConfig::get('app_pay4me_account_type');
                        $pay4me_openid = $pay4me_openid_arr['openid_pay4me'];
                         if(sfConfig::get('app_nibbs_id_account_show') && ($userDetails['0']['ewallet_type'] != $pay4me_openid)){
                            return $elem;
                        } else {
                            return false;
                        }
                    }
               }


                //change password
         if ($elem['url'] == 'userAdmin/changePassword') {

                    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                    $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
                    $name = $arrGroup['sfGuardGroup']['name'];
                    if($name =='ewallet_user'){

                        $openIdLogin = sfContext::getInstance()->getUser()->getAttribute('openId');
                        if(isset($openIdLogin)){
                            return false;
                        }else {
                        return $elem;
                    }
                    }else {
                        return $elem;
                    }
                   
                } else {
                    return $elem;
                }
      }
      // I don't have url
    }
    // I do have children
    if(isset($elem['child'])){
        $billObj = billServiceFactory::getService();
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $userDetails = Doctrine::getTable('UserDetail')->getDetailsUser($userId);
        $permittedEwalletType = 1;
        $arrGroup = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
        $name = $arrGroup['sfGuardGroup']['name'];
        if(($elem['label'] == 'Recharge eWallet' || $elem['label'] == 'eWallet Account Details' || $elem['label'] == 'eWallet Transfer') && ($name =='ewallet_user'))
            $permittedEwalletType = $billObj->permittedEwalletType($userDetails[0]['ewallet_type']);

        foreach ($elem['child'] as $k=>$aChild) {
            if($permittedEwalletType){
                $retVal = $this->isValidMenuItem($aChild,$uperms);
                if (!$retVal) {
                    // not a valid menu item - remove it
                    unset($elem['child'][$k]);
                } else {
                    $elem['child'][$k] = $retVal;
                }
            }else{
                unset($elem['child'][$k]);
            }
        }

        // All children validated - see if I have any valid child
        if (count($elem['child'])) {
          // I have valid child(ren)
          return $elem;
        }
     }
    // No valid child!!
    return false;
  }

    public function  chk_merchant_bank_association($url) {
        $bank_id = $this->getSfUserBank();
        if($bank_id){
          $url  = explode('=',$url);
          $suburl  = explode('&',$url[1]);
          $merchant_name = $suburl['0'];
       //   $merchantObj = Doctrine::getTable('Merchant')->findByName($merchant_name);

          return $count = Doctrine::getTable('BankMerchant')->chkAssociation($merchant_name,$bank_id);
        }
        else{
          return 0;
        }
      }

      
    public function  chk_merchant_biller_bank_association($url) {
        $bank_id = $this->getSfUserBank();
        if($bank_id){
          $url  = explode('=',$url);
          $suburl  = explode('&',$url[1]);
          $merchant_service = $suburl['0'];
          $count = Doctrine::getTable('BankMerchant')->chkServiceAssociation($merchant_service,$bank_id);
          return $count;
        }
        else{
          return 0;
        }
      }


     public function getSfUserBank() {
        $user = null;
        try {
          $user = sfContext::getInstance()->getUser();
        } catch (Exception $exe) {
          return 0;
        }
        if (!$user->isAuthenticated()) {
          return 0;
        }
        return $user->getGuardUser()->getBankUser()->getFirst()->getBankId();
       return true;
      }

     public function  chk_payment_association($url) {
        $bank_id = $this->getSfUserBank();
        if($bank_id){
         // $url  = explode('=',$url);
         // $merchant_name = $url['1'];
          return $count = Doctrine::getTable('ServiceBankConfiguration')->chkAssociation($bank_id);
        }
        else{
          return 0;
        }

      }
     public function  chk_payment_association_check($url) {
        $bank_id = $this->getSfUserBank();
        if($bank_id){
          $countSortCode = Doctrine::getTable('BankSortCodeMapper')->isvalidBankForCheckDraft($bank_id);
          $countSBC = Doctrine::getTable('ServiceBankConfiguration')->chkAssociation($bank_id);
          if($countSortCode>0 && $countSBC && $countSBC->count()>0){
              return 1;
          }
        }
        return 0;        
      }
     public function  chk_payment_association_draft($url) {
        $bank_id = $this->getSfUserBank();
        if($bank_id){
          $countSortCode = Doctrine::getTable('BankSortCodeMapper')->isvalidBankForCheckDraft($bank_id);
          $countSBC = Doctrine::getTable('ServiceBankConfiguration')->chkAssociation($bank_id);
          if($countSortCode>0 && $countSBC && $countSBC->count()>0){
              return 1;
          }
        }
        return 0;
      }

      public function checkUserPermissions($arrGroups,$presentGroup,$userId,
                            $ewalletcheck=false,
                                $bankcheck=false){

          //check user belongs to present group
          if(!isset($arrGroups[$presentGroup])){
              return true;
          }
          //check ewallet validation is enabled
          if($ewalletcheck){
              if($presentGroup==sfConfig::get('app_pfm_role_ewallet_user')){
                  if(!$this->checkeWalletUserasMerchant($userId)){
                      return true;
                  }
              }
          }
          return false;
      }

      private function checkeWalletUserasMerchant($userId){
          $merchantObj = Doctrine::getTable('Merchant')->isMerchantEwalletConfigured($userId);
          if($merchantObj){
              return true;
          }

      }

}
