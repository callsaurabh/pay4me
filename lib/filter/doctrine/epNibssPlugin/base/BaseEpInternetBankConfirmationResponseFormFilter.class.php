<?php

/**
 * EpInternetBankConfirmationResponse filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEpInternetBankConfirmationResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpInternetBankConfirmationRequest'), 'add_empty' => true)),
      'ewallet_number'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_number' => new sfWidgetFormFilterInput(),
      'session_id'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'validation_number'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'channel_code'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'request_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpInternetBankConfirmationRequest'), 'column' => 'id')),
      'ewallet_number'     => new sfValidatorPass(array('required' => false)),
      'transaction_number' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'session_id'         => new sfValidatorPass(array('required' => false)),
      'validation_number'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'channel_code'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_internet_bank_confirmation_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpInternetBankConfirmationResponse';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'request_id'         => 'ForeignKey',
      'ewallet_number'     => 'Text',
      'transaction_number' => 'Number',
      'session_id'         => 'Text',
      'validation_number'  => 'Number',
      'channel_code'       => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
