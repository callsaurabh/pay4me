<?php

/**
 * EpInternetBankVerifyResponse filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEpInternetBankVerifyResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpInternetBankVerifyRequest'), 'add_empty' => true)),
      'ewallet_number'     => new sfWidgetFormFilterInput(),
      'transaction_number' => new sfWidgetFormFilterInput(),
      'session_id'         => new sfWidgetFormFilterInput(),
      'transaction_date'   => new sfWidgetFormFilterInput(),
      'application_type'   => new sfWidgetFormFilterInput(),
      'name'               => new sfWidgetFormFilterInput(),
      'application_charge' => new sfWidgetFormFilterInput(),
      'transaction_charge' => new sfWidgetFormFilterInput(),
      'amount'             => new sfWidgetFormFilterInput(),
      'service_charge'     => new sfWidgetFormFilterInput(),
      'bank_account_name'  => new sfWidgetFormFilterInput(),
      'total_amount'       => new sfWidgetFormFilterInput(),
      'channel_code'       => new sfWidgetFormFilterInput(),
      'status'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'request_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpInternetBankVerifyRequest'), 'column' => 'id')),
      'ewallet_number'     => new sfValidatorPass(array('required' => false)),
      'transaction_number' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'session_id'         => new sfValidatorPass(array('required' => false)),
      'transaction_date'   => new sfValidatorPass(array('required' => false)),
      'application_type'   => new sfValidatorPass(array('required' => false)),
      'name'               => new sfValidatorPass(array('required' => false)),
      'application_charge' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'transaction_charge' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'service_charge'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'bank_account_name'  => new sfValidatorPass(array('required' => false)),
      'total_amount'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'channel_code'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_internet_bank_verify_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpInternetBankVerifyResponse';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'request_id'         => 'ForeignKey',
      'ewallet_number'     => 'Text',
      'transaction_number' => 'Number',
      'session_id'         => 'Text',
      'transaction_date'   => 'Text',
      'application_type'   => 'Text',
      'name'               => 'Text',
      'application_charge' => 'Number',
      'transaction_charge' => 'Number',
      'amount'             => 'Number',
      'service_charge'     => 'Number',
      'bank_account_name'  => 'Text',
      'total_amount'       => 'Number',
      'channel_code'       => 'Number',
      'status'             => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
