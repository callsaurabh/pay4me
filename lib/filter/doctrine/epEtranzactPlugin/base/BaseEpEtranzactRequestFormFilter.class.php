<?php

/**
 * EpEtranzactRequest filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEpEtranzactRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'transaction_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'terminal_id'    => new sfWidgetFormFilterInput(),
      'amount'         => new sfWidgetFormFilterInput(),
      'description'    => new sfWidgetFormFilterInput(),
      'check_sum'      => new sfWidgetFormFilterInput(),
      'response_url'   => new sfWidgetFormFilterInput(),
      'logo_url'       => new sfWidgetFormFilterInput(),
      'post_url'       => new sfWidgetFormFilterInput(),
      'user_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'transaction_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'terminal_id'    => new sfValidatorPass(array('required' => false)),
      'amount'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'description'    => new sfValidatorPass(array('required' => false)),
      'check_sum'      => new sfValidatorPass(array('required' => false)),
      'response_url'   => new sfValidatorPass(array('required' => false)),
      'logo_url'       => new sfValidatorPass(array('required' => false)),
      'post_url'       => new sfValidatorPass(array('required' => false)),
      'user_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_etranzact_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpEtranzactRequest';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'transaction_id' => 'Number',
      'terminal_id'    => 'Text',
      'amount'         => 'Number',
      'description'    => 'Text',
      'check_sum'      => 'Text',
      'response_url'   => 'Text',
      'logo_url'       => 'Text',
      'post_url'       => 'Text',
      'user_id'        => 'ForeignKey',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
