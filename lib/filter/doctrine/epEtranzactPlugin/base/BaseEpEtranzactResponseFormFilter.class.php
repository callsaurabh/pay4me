<?php

/**
 * EpEtranzactResponse filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEpEtranzactResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'etranzact_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpEtranzactRequest'), 'add_empty' => true)),
      'switch_key'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card4'           => new sfWidgetFormFilterInput(),
      'merchant_code'   => new sfWidgetFormFilterInput(),
      'amount'          => new sfWidgetFormFilterInput(),
      'description'     => new sfWidgetFormFilterInput(),
      'check_sum'       => new sfWidgetFormFilterInput(),
      'final_check_sum' => new sfWidgetFormFilterInput(),
      'country_code'    => new sfWidgetFormFilterInput(),
      'no_retry'        => new sfWidgetFormFilterInput(),
      'status'          => new sfWidgetFormFilterInput(),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'etranzact_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpEtranzactRequest'), 'column' => 'id')),
      'switch_key'      => new sfValidatorPass(array('required' => false)),
      'card4'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'merchant_code'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'amount'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'description'     => new sfValidatorPass(array('required' => false)),
      'check_sum'       => new sfValidatorPass(array('required' => false)),
      'final_check_sum' => new sfValidatorPass(array('required' => false)),
      'country_code'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'no_retry'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'          => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_etranzact_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpEtranzactResponse';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'etranzact_id'    => 'ForeignKey',
      'switch_key'      => 'Text',
      'card4'           => 'Number',
      'merchant_code'   => 'Number',
      'amount'          => 'Number',
      'description'     => 'Text',
      'check_sum'       => 'Text',
      'final_check_sum' => 'Text',
      'country_code'    => 'Number',
      'no_retry'        => 'Number',
      'status'          => 'Text',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
