<?php

/**
 * MwCommandRequest filter form.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MwCommandRequestFormFilter extends BaseMwCommandRequestFormFilter
{
  /**
   * @see MwRequestFormFilter
   */
  public function configure()
  {
    parent::configure();
  }
}
