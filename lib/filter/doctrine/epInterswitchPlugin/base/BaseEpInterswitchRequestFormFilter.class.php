<?php

/**
 * EpInterswitchRequest filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEpInterswitchRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'trnx_id'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cadp_id'      => new sfWidgetFormFilterInput(),
      'mert_id'      => new sfWidgetFormFilterInput(),
      'amount_naira' => new sfWidgetFormFilterInput(),
      'amount_kobo'  => new sfWidgetFormFilterInput(),
      'post_url'     => new sfWidgetFormFilterInput(),
      'currency'     => new sfWidgetFormFilterInput(),
      'user_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'trnx_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'cadp_id'      => new sfValidatorPass(array('required' => false)),
      'mert_id'      => new sfValidatorPass(array('required' => false)),
      'amount_naira' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount_kobo'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'post_url'     => new sfValidatorPass(array('required' => false)),
      'currency'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_interswitch_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpInterswitchRequest';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'trnx_id'      => 'Number',
      'cadp_id'      => 'Text',
      'mert_id'      => 'Text',
      'amount_naira' => 'Number',
      'amount_kobo'  => 'Number',
      'post_url'     => 'Text',
      'currency'     => 'Number',
      'user_id'      => 'ForeignKey',
      'created_at'   => 'Date',
      'updated_at'   => 'Date',
    );
  }
}
