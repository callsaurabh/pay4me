<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/doctrine/BaseFormFilterDoctrine.class.php');

/**
 * EpAccountDeposit filter form base class.
 *
 * @package    filters
 * @subpackage EpAccountDeposit *
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 11675 2008-09-19 15:21:38Z fabien $
 */
class BaseEpAccountDepositFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'master_account_id' => new sfWidgetFormDoctrineChoice(array('model' => 'EpMasterAccount', 'add_empty' => true)),
      'clear_amount'      => new sfWidgetFormFilterInput(),
      'unclear_amount'    => new sfWidgetFormFilterInput(),
      'status'            => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => '0', 1 => '1'))),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'created_by'        => new sfWidgetFormFilterInput(),
      'updated_by'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'master_account_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'EpMasterAccount', 'column' => 'id')),
      'clear_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'unclear_amount'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'            => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => '0', 1 => '1'))),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_account_deposit_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpAccountDeposit';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'master_account_id' => 'ForeignKey',
      'clear_amount'      => 'Number',
      'unclear_amount'    => 'Number',
      'status'            => 'Enum',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
      'created_by'        => 'Number',
      'updated_by'        => 'Number',
    );
  }
}