<?php

/**
 * EpMasterAccount filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEpMasterAccountFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'clear_balance'   => new sfWidgetFormFilterInput(),
      'unclear_balance' => new sfWidgetFormFilterInput(),
      'unclear_amount'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'account_number'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EwalletTransactionTrack'), 'add_empty' => true)),
      'account_name'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sortcode'        => new sfWidgetFormFilterInput(),
      'bankname'        => new sfWidgetFormFilterInput(),
      'type'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'      => new sfWidgetFormFilterInput(),
      'updated_by'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'clear_balance'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'unclear_balance' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'unclear_amount'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'account_number'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EwalletTransactionTrack'), 'column' => 'id')),
      'account_name'    => new sfValidatorPass(array('required' => false)),
      'sortcode'        => new sfValidatorPass(array('required' => false)),
      'bankname'        => new sfValidatorPass(array('required' => false)),
      'type'            => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_master_account_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpMasterAccount';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'clear_balance'   => 'Number',
      'unclear_balance' => 'Number',
      'unclear_amount'  => 'Number',
      'account_number'  => 'ForeignKey',
      'account_name'    => 'Text',
      'sortcode'        => 'Text',
      'bankname'        => 'Text',
      'type'            => 'Text',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
      'created_by'      => 'Number',
      'updated_by'      => 'Number',
    );
  }
}
