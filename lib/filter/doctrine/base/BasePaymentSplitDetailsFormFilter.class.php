<?php

/**
 * PaymentSplitDetails filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePaymentSplitDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_request_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => true)),
      'from_account'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountFrom'), 'add_empty' => true)),
      'to_account'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccountTo'), 'add_empty' => true)),
      'amount'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'split_date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'reconciliation_state' => new sfWidgetFormChoice(array('choices' => array('' => '', 'new' => 'new', 'sent_to_nibss' => 'sent_to_nibss'))),
      'payment_batch_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentBatch'), 'add_empty' => true)),
      'paid_by'              => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'           => new sfWidgetFormFilterInput(),
      'updated_by'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'merchant_request_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantRequest'), 'column' => 'id')),
      'from_account'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccountFrom'), 'column' => 'id')),
      'to_account'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccountTo'), 'column' => 'id')),
      'amount'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'split_date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'reconciliation_state' => new sfValidatorChoice(array('required' => false, 'choices' => array('new' => 'new', 'sent_to_nibss' => 'sent_to_nibss'))),
      'payment_batch_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentBatch'), 'column' => 'id')),
      'paid_by'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('payment_split_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentSplitDetails';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'merchant_request_id'  => 'ForeignKey',
      'from_account'         => 'ForeignKey',
      'to_account'           => 'ForeignKey',
      'amount'               => 'Number',
      'split_date'           => 'Date',
      'reconciliation_state' => 'Enum',
      'payment_batch_id'     => 'ForeignKey',
      'paid_by'              => 'Number',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
      'created_by'           => 'Number',
      'updated_by'           => 'Number',
    );
  }
}
