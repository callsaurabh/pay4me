<?php

/**
 * UserDetail filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseUserDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'master_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'name'              => new sfWidgetFormFilterInput(),
      'last_name'         => new sfWidgetFormFilterInput(),
      'dob'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'address'           => new sfWidgetFormFilterInput(),
      'email'             => new sfWidgetFormFilterInput(),
      'mobile_no'         => new sfWidgetFormFilterInput(),
      'work_phone'        => new sfWidgetFormFilterInput(),
      'send_sms'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'gsmno' => 'gsmno', 'workphone' => 'workphone', 'both' => 'both', 'none' => 'none'))),
      'failed_attempt'    => new sfWidgetFormFilterInput(),
      'user_status'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'kyc_status'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'openid_auth'       => new sfWidgetFormFilterInput(),
      'ewallet_type'      => new sfWidgetFormChoice(array('choices' => array('' => '', 'ewallet' => 'ewallet', 'openid' => 'openid', 'both' => 'both'))),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'        => new sfWidgetFormFilterInput(),
      'updated_by'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'user_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'master_account_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'name'              => new sfValidatorPass(array('required' => false)),
      'last_name'         => new sfValidatorPass(array('required' => false)),
      'dob'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'address'           => new sfValidatorPass(array('required' => false)),
      'email'             => new sfValidatorPass(array('required' => false)),
      'mobile_no'         => new sfValidatorPass(array('required' => false)),
      'work_phone'        => new sfValidatorPass(array('required' => false)),
      'send_sms'          => new sfValidatorChoice(array('required' => false, 'choices' => array('gsmno' => 'gsmno', 'workphone' => 'workphone', 'both' => 'both', 'none' => 'none'))),
      'failed_attempt'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_status'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'kyc_status'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'openid_auth'       => new sfValidatorPass(array('required' => false)),
      'ewallet_type'      => new sfValidatorChoice(array('required' => false, 'choices' => array('ewallet' => 'ewallet', 'openid' => 'openid', 'both' => 'both'))),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('user_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserDetail';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'user_id'           => 'ForeignKey',
      'master_account_id' => 'ForeignKey',
      'name'              => 'Text',
      'last_name'         => 'Text',
      'dob'               => 'Date',
      'address'           => 'Text',
      'email'             => 'Text',
      'mobile_no'         => 'Text',
      'work_phone'        => 'Text',
      'send_sms'          => 'Enum',
      'failed_attempt'    => 'Number',
      'user_status'       => 'Number',
      'kyc_status'        => 'Number',
      'openid_auth'       => 'Text',
      'ewallet_type'      => 'Enum',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
      'deleted_at'        => 'Date',
      'created_by'        => 'Number',
      'updated_by'        => 'Number',
    );
  }
}
