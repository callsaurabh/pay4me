<?php

/**
 * RechargeAmountAvailability filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseRechargeAmountAvailabilityFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'master_account_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'total_amount_paid'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount_wallet'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'service_charges'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_available_on'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'is_credited'            => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => '0', 1 => '1'))),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'master_account_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'total_amount_paid'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'amount_wallet'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'service_charges'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'date_available_on'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'is_credited'            => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => '0', 1 => '1'))),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentModeOption'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('recharge_amount_availability_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RechargeAmountAvailability';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'master_account_id'      => 'ForeignKey',
      'total_amount_paid'      => 'Number',
      'amount_wallet'          => 'Number',
      'service_charges'        => 'Number',
      'date_available_on'      => 'Date',
      'is_credited'            => 'Enum',
      'payment_mode_option_id' => 'ForeignKey',
    );
  }
}
