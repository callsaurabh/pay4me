<?php

/**
 * ValidationRules filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseValidationRulesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'param_name'          => new sfWidgetFormFilterInput(),
      'param_description'   => new sfWidgetFormFilterInput(),
      'is_mandatory'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'param_type'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'integer' => 'integer', 'string' => 'string'))),
      'mapped_to'           => new sfWidgetFormChoice(array('choices' => array('' => '', 'iparam_one' => 'iparam_one', 'iparam_two' => 'iparam_two', 'iparam_three' => 'iparam_three', 'iparam_four' => 'iparam_four', 'iparam_five' => 'iparam_five', 'iparam_six' => 'iparam_six', 'iparam_seven' => 'iparam_seven', 'iparam_eight' => 'iparam_eight', 'iparam_nine' => 'iparam_nine', 'iparam_ten' => 'iparam_ten', 'sparam_one' => 'sparam_one', 'sparam_two' => 'sparam_two', 'sparam_three' => 'sparam_three', 'sparam_four' => 'sparam_four', 'sparam_five' => 'sparam_five', 'sparam_six' => 'sparam_six', 'sparam_seven' => 'sparam_seven', 'sparam_eight' => 'sparam_eight', 'sparam_nine' => 'sparam_nine', 'sparam_ten' => 'sparam_ten'))),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'merchant_service_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantService'), 'column' => 'id')),
      'param_name'          => new sfValidatorPass(array('required' => false)),
      'param_description'   => new sfValidatorPass(array('required' => false)),
      'is_mandatory'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'param_type'          => new sfValidatorChoice(array('required' => false, 'choices' => array('integer' => 'integer', 'string' => 'string'))),
      'mapped_to'           => new sfValidatorChoice(array('required' => false, 'choices' => array('iparam_one' => 'iparam_one', 'iparam_two' => 'iparam_two', 'iparam_three' => 'iparam_three', 'iparam_four' => 'iparam_four', 'iparam_five' => 'iparam_five', 'iparam_six' => 'iparam_six', 'iparam_seven' => 'iparam_seven', 'iparam_eight' => 'iparam_eight', 'iparam_nine' => 'iparam_nine', 'iparam_ten' => 'iparam_ten', 'sparam_one' => 'sparam_one', 'sparam_two' => 'sparam_two', 'sparam_three' => 'sparam_three', 'sparam_four' => 'sparam_four', 'sparam_five' => 'sparam_five', 'sparam_six' => 'sparam_six', 'sparam_seven' => 'sparam_seven', 'sparam_eight' => 'sparam_eight', 'sparam_nine' => 'sparam_nine', 'sparam_ten' => 'sparam_ten'))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('validation_rules_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ValidationRules';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'merchant_service_id' => 'ForeignKey',
      'param_name'          => 'Text',
      'param_description'   => 'Text',
      'is_mandatory'        => 'Boolean',
      'param_type'          => 'Enum',
      'mapped_to'           => 'Enum',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'deleted_at'          => 'Date',
      'created_by'          => 'Number',
      'updated_by'          => 'Number',
    );
  }
}
