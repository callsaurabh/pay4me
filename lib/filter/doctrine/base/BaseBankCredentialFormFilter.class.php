<?php

/**
 * BankCredential filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBankCredentialFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bank_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'user_name'      => new sfWidgetFormFilterInput(),
      'password'       => new sfWidgetFormFilterInput(),
      'user_group'     => new sfWidgetFormChoice(array('choices' => array('' => '', 'admin' => 'admin', 'read' => 'read', 'write' => 'write'))),
      'asymmetric_key' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'bank_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bank'), 'column' => 'id')),
      'user_name'      => new sfValidatorPass(array('required' => false)),
      'password'       => new sfValidatorPass(array('required' => false)),
      'user_group'     => new sfValidatorChoice(array('required' => false, 'choices' => array('admin' => 'admin', 'read' => 'read', 'write' => 'write'))),
      'asymmetric_key' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bank_credential_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BankCredential';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'bank_id'        => 'ForeignKey',
      'user_name'      => 'Text',
      'password'       => 'Text',
      'user_group'     => 'Enum',
      'asymmetric_key' => 'Text',
    );
  }
}
