<?php

/**
 * MerchantRequestDetails filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMerchantRequestDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_item_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'), 'add_empty' => true)),
      'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => true)),
      'iparam_one'          => new sfWidgetFormFilterInput(),
      'iparam_two'          => new sfWidgetFormFilterInput(),
      'iparam_three'        => new sfWidgetFormFilterInput(),
      'iparam_four'         => new sfWidgetFormFilterInput(),
      'iparam_five'         => new sfWidgetFormFilterInput(),
      'iparam_six'          => new sfWidgetFormFilterInput(),
      'iparam_seven'        => new sfWidgetFormFilterInput(),
      'iparam_eight'        => new sfWidgetFormFilterInput(),
      'iparam_nine'         => new sfWidgetFormFilterInput(),
      'iparam_ten'          => new sfWidgetFormFilterInput(),
      'name'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sparam_one'          => new sfWidgetFormFilterInput(),
      'sparam_two'          => new sfWidgetFormFilterInput(),
      'sparam_three'        => new sfWidgetFormFilterInput(),
      'sparam_four'         => new sfWidgetFormFilterInput(),
      'sparam_five'         => new sfWidgetFormFilterInput(),
      'sparam_six'          => new sfWidgetFormFilterInput(),
      'sparam_seven'        => new sfWidgetFormFilterInput(),
      'sparam_eight'        => new sfWidgetFormFilterInput(),
      'sparam_nine'         => new sfWidgetFormFilterInput(),
      'sparam_ten'          => new sfWidgetFormFilterInput(),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'merchant_item_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantItem'), 'column' => 'id')),
      'merchant_service_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantService'), 'column' => 'id')),
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantRequest'), 'column' => 'id')),
      'iparam_one'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_two'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_three'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_four'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_five'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_six'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_seven'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_eight'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_nine'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iparam_ten'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'                => new sfValidatorPass(array('required' => false)),
      'sparam_one'          => new sfValidatorPass(array('required' => false)),
      'sparam_two'          => new sfValidatorPass(array('required' => false)),
      'sparam_three'        => new sfValidatorPass(array('required' => false)),
      'sparam_four'         => new sfValidatorPass(array('required' => false)),
      'sparam_five'         => new sfValidatorPass(array('required' => false)),
      'sparam_six'          => new sfValidatorPass(array('required' => false)),
      'sparam_seven'        => new sfValidatorPass(array('required' => false)),
      'sparam_eight'        => new sfValidatorPass(array('required' => false)),
      'sparam_nine'         => new sfValidatorPass(array('required' => false)),
      'sparam_ten'          => new sfValidatorPass(array('required' => false)),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('merchant_request_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantRequestDetails';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'merchant_item_id'    => 'ForeignKey',
      'merchant_service_id' => 'ForeignKey',
      'merchant_request_id' => 'ForeignKey',
      'iparam_one'          => 'Number',
      'iparam_two'          => 'Number',
      'iparam_three'        => 'Number',
      'iparam_four'         => 'Number',
      'iparam_five'         => 'Number',
      'iparam_six'          => 'Number',
      'iparam_seven'        => 'Number',
      'iparam_eight'        => 'Number',
      'iparam_nine'         => 'Number',
      'iparam_ten'          => 'Number',
      'name'                => 'Text',
      'sparam_one'          => 'Text',
      'sparam_two'          => 'Text',
      'sparam_three'        => 'Text',
      'sparam_four'         => 'Text',
      'sparam_five'         => 'Text',
      'sparam_six'          => 'Text',
      'sparam_seven'        => 'Text',
      'sparam_eight'        => 'Text',
      'sparam_nine'         => 'Text',
      'sparam_ten'          => 'Text',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'deleted_at'          => 'Date',
      'created_by'          => 'Number',
      'updated_by'          => 'Number',
    );
  }
}
