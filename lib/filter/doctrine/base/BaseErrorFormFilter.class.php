<?php

/**
 * Error filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseErrorFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'message'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'merchant_service_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'service_charge_percent' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'upper_slab'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'lower_slab'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'             => new sfWidgetFormFilterInput(),
      'updated_by'             => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'code'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'message'                => new sfValidatorPass(array('required' => false)),
      'merchant_service_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantService'), 'column' => 'id')),
      'service_charge_percent' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'upper_slab'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'lower_slab'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('error_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Error';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'code'                   => 'Number',
      'message'                => 'Text',
      'merchant_service_id'    => 'ForeignKey',
      'service_charge_percent' => 'Number',
      'upper_slab'             => 'Number',
      'lower_slab'             => 'Number',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'deleted_at'             => 'Date',
      'created_by'             => 'Number',
      'updated_by'             => 'Number',
    );
  }
}
