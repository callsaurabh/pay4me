<?php

/**
 * MwCommandRequest filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMwCommandRequestFormFilter extends MwRequestFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('mw_command_request_filters[%s]');
  }

  public function getModelName()
  {
    return 'MwCommandRequest';
  }
}
