<?php

/**
 * SplitEntityConfiguration filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSplitEntityConfigurationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_service_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'split_type_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SplitType'), 'add_empty' => true)),
      'charge'                         => new sfWidgetFormFilterInput(),
      'split_account_configuration_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SplitAccountConfiguration'), 'add_empty' => true)),
      'merchant_item_split_column'     => new sfWidgetFormChoice(array('choices' => array('' => '', 'split_one' => 'split_one', 'split_two' => 'split_two', 'split_three' => 'split_three', 'split_four' => 'split_four', 'split_five' => 'split_five', 'split_six' => 'split_six', 'split_seven' => 'split_seven', 'split_eight' => 'split_eight', 'split_nine' => 'split_nine', 'split_ten' => 'split_ten', 'split_eleven' => 'split_eleven', 'split_twelve' => 'split_twelve', 'split_thirteen' => 'split_thirteen', 'split_fourteen' => 'split_fourteen', 'split_fifteen' => 'split_fifteen', 'split_sixteen' => 'split_sixteen', 'split_seventeen' => 'split_seventeen', 'split_eighteen' => 'split_eighteen', 'split_nineteen' => 'split_nineteen', 'split_twenty' => 'split_twenty'))),
      'created_at'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'                     => new sfWidgetFormFilterInput(),
      'updated_by'                     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'merchant_service_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantService'), 'column' => 'id')),
      'split_type_id'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SplitType'), 'column' => 'id')),
      'charge'                         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_account_configuration_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SplitAccountConfiguration'), 'column' => 'id')),
      'merchant_item_split_column'     => new sfValidatorChoice(array('required' => false, 'choices' => array('split_one' => 'split_one', 'split_two' => 'split_two', 'split_three' => 'split_three', 'split_four' => 'split_four', 'split_five' => 'split_five', 'split_six' => 'split_six', 'split_seven' => 'split_seven', 'split_eight' => 'split_eight', 'split_nine' => 'split_nine', 'split_ten' => 'split_ten', 'split_eleven' => 'split_eleven', 'split_twelve' => 'split_twelve', 'split_thirteen' => 'split_thirteen', 'split_fourteen' => 'split_fourteen', 'split_fifteen' => 'split_fifteen', 'split_sixteen' => 'split_sixteen', 'split_seventeen' => 'split_seventeen', 'split_eighteen' => 'split_eighteen', 'split_nineteen' => 'split_nineteen', 'split_twenty' => 'split_twenty'))),
      'created_at'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('split_entity_configuration_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SplitEntityConfiguration';
  }

  public function getFields()
  {
    return array(
      'id'                             => 'Number',
      'merchant_service_id'            => 'ForeignKey',
      'split_type_id'                  => 'ForeignKey',
      'charge'                         => 'Number',
      'split_account_configuration_id' => 'ForeignKey',
      'merchant_item_split_column'     => 'Enum',
      'created_at'                     => 'Date',
      'updated_at'                     => 'Date',
      'deleted_at'                     => 'Date',
      'created_by'                     => 'Number',
      'updated_by'                     => 'Number',
    );
  }
}
