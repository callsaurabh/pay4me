<?php

/**
 * GatewayOrder filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseGatewayOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'app_id'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_id'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => true)),
      'type'                   => new sfWidgetFormChoice(array('choices' => array('' => '', 'pay' => 'pay', 'recharge' => 'recharge'))),
      'validation_number'      => new sfWidgetFormFilterInput(),
      'user_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'currency_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => true)),
      'servicecharge_kobo'     => new sfWidgetFormFilterInput(),
      'status'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'success' => 'success', 'failure' => 'failure'))),
      'amount'                 => new sfWidgetFormFilterInput(),
      'pan'                    => new sfWidgetFormFilterInput(),
      'approvalcode'           => new sfWidgetFormFilterInput(),
      'response_code'          => new sfWidgetFormFilterInput(),
      'response_txt'           => new sfWidgetFormFilterInput(),
      'transaction_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'locked'                 => new sfWidgetFormFilterInput(),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'app_id'                 => new sfValidatorPass(array('required' => false)),
      'order_id'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentModeOption'), 'column' => 'id')),
      'type'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('pay' => 'pay', 'recharge' => 'recharge'))),
      'validation_number'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'currency_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CurrencyCode'), 'column' => 'id')),
      'servicecharge_kobo'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('success' => 'success', 'failure' => 'failure'))),
      'amount'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pan'                    => new sfValidatorPass(array('required' => false)),
      'approvalcode'           => new sfValidatorPass(array('required' => false)),
      'response_code'          => new sfValidatorPass(array('required' => false)),
      'response_txt'           => new sfValidatorPass(array('required' => false)),
      'transaction_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'locked'                 => new sfValidatorPass(array('required' => false)),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('gateway_order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GatewayOrder';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'app_id'                 => 'Text',
      'order_id'               => 'Number',
      'payment_mode_option_id' => 'ForeignKey',
      'type'                   => 'Enum',
      'validation_number'      => 'Number',
      'user_id'                => 'ForeignKey',
      'currency_id'            => 'ForeignKey',
      'servicecharge_kobo'     => 'Number',
      'status'                 => 'Enum',
      'amount'                 => 'Number',
      'pan'                    => 'Text',
      'approvalcode'           => 'Text',
      'response_code'          => 'Text',
      'response_txt'           => 'Text',
      'transaction_date'       => 'Date',
      'locked'                 => 'Text',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'deleted_at'             => 'Date',
    );
  }
}
