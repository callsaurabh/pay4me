<?php

/**
 * Feedback filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFeedbackFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                   => new sfWidgetFormFilterInput(),
      'email'                  => new sfWidgetFormFilterInput(),
      'merchant_id'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_mode_option_id' => new sfWidgetFormFilterInput(),
      'bank_id'                => new sfWidgetFormFilterInput(),
      'ewallet_username'       => new sfWidgetFormFilterInput(),
      'ewallet_account_number' => new sfWidgetFormFilterInput(),
      'common_issue'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'reason'                 => new sfWidgetFormFilterInput(),
      'message'                => new sfWidgetFormFilterInput(),
      'ip'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'             => new sfWidgetFormFilterInput(),
      'updated_by'             => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'                   => new sfValidatorPass(array('required' => false)),
      'email'                  => new sfValidatorPass(array('required' => false)),
      'merchant_id'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_mode_option_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bank_id'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ewallet_username'       => new sfValidatorPass(array('required' => false)),
      'ewallet_account_number' => new sfValidatorPass(array('required' => false)),
      'common_issue'           => new sfValidatorPass(array('required' => false)),
      'reason'                 => new sfValidatorPass(array('required' => false)),
      'message'                => new sfValidatorPass(array('required' => false)),
      'ip'                     => new sfValidatorPass(array('required' => false)),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('feedback_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Feedback';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'name'                   => 'Text',
      'email'                  => 'Text',
      'merchant_id'            => 'Number',
      'payment_mode_option_id' => 'Number',
      'bank_id'                => 'Number',
      'ewallet_username'       => 'Text',
      'ewallet_account_number' => 'Text',
      'common_issue'           => 'Text',
      'reason'                 => 'Text',
      'message'                => 'Text',
      'ip'                     => 'Text',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'deleted_at'             => 'Date',
      'created_by'             => 'Number',
      'updated_by'             => 'Number',
    );
  }
}
