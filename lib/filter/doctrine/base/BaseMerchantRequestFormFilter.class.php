<?php

/**
 * MerchantRequest filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMerchantRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'txn_ref'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'merchant_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => true)),
      'merchant_service_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'merchant_item_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'), 'add_empty' => true)),
      'item_fee'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_mode_option_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => true)),
      'bank_name'                => new sfWidgetFormFilterInput(),
      'payment_status_code'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'paid_amount'              => new sfWidgetFormFilterInput(),
      'paid_date'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'paid_by'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'bank_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'bank_branch_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BankBranch'), 'add_empty' => true)),
      'bank_charge'              => new sfWidgetFormFilterInput(),
      'service_charge'           => new sfWidgetFormFilterInput(),
      'gateway_charge'           => new sfWidgetFormFilterInput(),
      'bank_amount'              => new sfWidgetFormFilterInput(),
      'payforme_amount'          => new sfWidgetFormFilterInput(),
      'service_configuration_id' => new sfWidgetFormFilterInput(),
      'payment_mandate_id'       => new sfWidgetFormFilterInput(),
      'transaction_location'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'validation_number'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterLedger'), 'add_empty' => true)),
      'created_by'               => new sfWidgetFormFilterInput(),
      'updated_by'               => new sfWidgetFormFilterInput(),
      'currency_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => true)),
      'version'                  => new sfWidgetFormFilterInput(),
      'platform'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Web' => 'Web', 'Mobile' => 'Mobile'))),
      'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'txn_ref'                  => new sfValidatorPass(array('required' => false)),
      'merchant_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Merchant'), 'column' => 'id')),
      'merchant_service_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantService'), 'column' => 'id')),
      'merchant_item_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantItem'), 'column' => 'id')),
      'item_fee'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'payment_mode_option_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentModeOption'), 'column' => 'id')),
      'bank_name'                => new sfValidatorPass(array('required' => false)),
      'payment_status_code'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_amount'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_date'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'paid_by'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'bank_id'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bank'), 'column' => 'id')),
      'bank_branch_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('BankBranch'), 'column' => 'id')),
      'bank_charge'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'service_charge'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'gateway_charge'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'bank_amount'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'payforme_amount'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'service_configuration_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_mandate_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_location'     => new sfValidatorPass(array('required' => false)),
      'validation_number'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterLedger'), 'column' => 'id')),
      'created_by'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'currency_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CurrencyCode'), 'column' => 'id')),
      'version'                  => new sfValidatorPass(array('required' => false)),
      'platform'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Web' => 'Web', 'Mobile' => 'Mobile'))),
      'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('merchant_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantRequest';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'txn_ref'                  => 'Text',
      'merchant_id'              => 'ForeignKey',
      'merchant_service_id'      => 'ForeignKey',
      'merchant_item_id'         => 'ForeignKey',
      'item_fee'                 => 'Number',
      'payment_mode_option_id'   => 'ForeignKey',
      'bank_name'                => 'Text',
      'payment_status_code'      => 'Number',
      'paid_amount'              => 'Number',
      'paid_date'                => 'Date',
      'paid_by'                  => 'ForeignKey',
      'bank_id'                  => 'ForeignKey',
      'bank_branch_id'           => 'ForeignKey',
      'bank_charge'              => 'Number',
      'service_charge'           => 'Number',
      'gateway_charge'           => 'Number',
      'bank_amount'              => 'Number',
      'payforme_amount'          => 'Number',
      'service_configuration_id' => 'Number',
      'payment_mandate_id'       => 'Number',
      'transaction_location'     => 'Text',
      'validation_number'        => 'ForeignKey',
      'created_by'               => 'Number',
      'updated_by'               => 'Number',
      'currency_id'              => 'ForeignKey',
      'version'                  => 'Text',
      'platform'                 => 'Enum',
      'created_at'               => 'Date',
      'updated_at'               => 'Date',
      'deleted_at'               => 'Date',
    );
  }
}
