<?php

/**
 * TransactionBankPosting filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTransactionBankPostingFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => true)),
      'no_of_attempts'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'last_mw_request_id'  => new sfWidgetFormFilterInput(),
      'message_txn_no'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'txn_type'            => new sfWidgetFormChoice(array('choices' => array('' => '', 'payment' => 'payment', 'recharge' => 'recharge'))),
      'state'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'not_posted' => 'not_posted', 'posted_to_queue' => 'posted_to_queue', 'success' => 'success', 'failure' => 'failure'))),
      'bank_txn_number'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bank_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantRequest'), 'column' => 'id')),
      'no_of_attempts'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'last_mw_request_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'message_txn_no'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'txn_type'            => new sfValidatorChoice(array('required' => false, 'choices' => array('payment' => 'payment', 'recharge' => 'recharge'))),
      'state'               => new sfValidatorChoice(array('required' => false, 'choices' => array('not_posted' => 'not_posted', 'posted_to_queue' => 'posted_to_queue', 'success' => 'success', 'failure' => 'failure'))),
      'bank_txn_number'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bank_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bank'), 'column' => 'id')),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('transaction_bank_posting_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransactionBankPosting';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'merchant_request_id' => 'ForeignKey',
      'no_of_attempts'      => 'Number',
      'last_mw_request_id'  => 'Number',
      'message_txn_no'      => 'Number',
      'txn_type'            => 'Enum',
      'state'               => 'Enum',
      'bank_txn_number'     => 'Number',
      'bank_id'             => 'ForeignKey',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
