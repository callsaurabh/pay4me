<?php

/**
 * VbvOrder filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseVbvOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'), 'add_empty' => true)),
      'app_id'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'type'     => new sfWidgetFormChoice(array('choices' => array('' => '', 'pay' => 'pay', 'recharge' => 'recharge'))),
    ));

    $this->setValidators(array(
      'order_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpVbvRequest'), 'column' => 'id')),
      'app_id'   => new sfValidatorPass(array('required' => false)),
      'type'     => new sfValidatorChoice(array('required' => false, 'choices' => array('pay' => 'pay', 'recharge' => 'recharge'))),
    ));

    $this->widgetSchema->setNameFormat('vbv_order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VbvOrder';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'order_id' => 'ForeignKey',
      'app_id'   => 'Text',
      'type'     => 'Enum',
    );
  }
}
