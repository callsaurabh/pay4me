<?php

/**
 * Bank filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBankFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bank_name'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'acronym'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status'              => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'bank_code'           => new sfWidgetFormFilterInput(),
      'bank_key'            => new sfWidgetFormFilterInput(),
      'auth_info'           => new sfWidgetFormFilterInput(),
      'bi_enabled'          => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'bi_logger_level'     => new sfWidgetFormChoice(array('choices' => array('' => '', 'WARN' => 'WARN', 'INFO' => 'INFO', 'DEBUG' => 'DEBUG', 'FATAL' => 'FATAL', 'ERROR' => 'ERROR'))),
      'bi_protocol_version' => new sfWidgetFormChoice(array('choices' => array('' => '', 'V1' => 'V1', 'v2' => 'v2'))),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'bank_name'           => new sfValidatorPass(array('required' => false)),
      'acronym'             => new sfValidatorPass(array('required' => false)),
      'status'              => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'bank_code'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bank_key'            => new sfValidatorPass(array('required' => false)),
      'auth_info'           => new sfValidatorPass(array('required' => false)),
      'bi_enabled'          => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'bi_logger_level'     => new sfValidatorChoice(array('required' => false, 'choices' => array('WARN' => 'WARN', 'INFO' => 'INFO', 'DEBUG' => 'DEBUG', 'FATAL' => 'FATAL', 'ERROR' => 'ERROR'))),
      'bi_protocol_version' => new sfValidatorChoice(array('required' => false, 'choices' => array('V1' => 'V1', 'v2' => 'v2'))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('bank_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bank';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'bank_name'           => 'Text',
      'acronym'             => 'Text',
      'status'              => 'Enum',
      'bank_code'           => 'Number',
      'bank_key'            => 'Text',
      'auth_info'           => 'Text',
      'bi_enabled'          => 'Enum',
      'bi_logger_level'     => 'Enum',
      'bi_protocol_version' => 'Enum',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'deleted_at'          => 'Date',
      'created_by'          => 'Number',
      'updated_by'          => 'Number',
    );
  }
}
