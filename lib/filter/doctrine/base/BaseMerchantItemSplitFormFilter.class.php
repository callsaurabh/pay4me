<?php

/**
 * MerchantItemSplit filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMerchantItemSplitFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => true)),
      'merchant_service_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'split_one'           => new sfWidgetFormFilterInput(),
      'split_two'           => new sfWidgetFormFilterInput(),
      'split_three'         => new sfWidgetFormFilterInput(),
      'split_four'          => new sfWidgetFormFilterInput(),
      'split_five'          => new sfWidgetFormFilterInput(),
      'split_six'           => new sfWidgetFormFilterInput(),
      'split_seven'         => new sfWidgetFormFilterInput(),
      'split_eight'         => new sfWidgetFormFilterInput(),
      'split_nine'          => new sfWidgetFormFilterInput(),
      'split_ten'           => new sfWidgetFormFilterInput(),
      'split_eleven'        => new sfWidgetFormFilterInput(),
      'split_twelve'        => new sfWidgetFormFilterInput(),
      'split_thirteen'      => new sfWidgetFormFilterInput(),
      'split_fourteen'      => new sfWidgetFormFilterInput(),
      'split_fifteen'       => new sfWidgetFormFilterInput(),
      'split_sixteen'       => new sfWidgetFormFilterInput(),
      'split_seventeen'     => new sfWidgetFormFilterInput(),
      'split_eightteen'     => new sfWidgetFormFilterInput(),
      'split_nineteen'      => new sfWidgetFormFilterInput(),
      'split_twenty'        => new sfWidgetFormFilterInput(),
      'is_processed'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantRequest'), 'column' => 'id')),
      'merchant_service_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantService'), 'column' => 'id')),
      'split_one'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_two'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_three'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_four'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_five'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_six'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_seven'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_eight'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_nine'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_ten'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_eleven'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_twelve'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_thirteen'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_fourteen'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_fifteen'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_sixteen'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_seventeen'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_eightteen'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_nineteen'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_twenty'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_processed'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('merchant_item_split_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantItemSplit';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'merchant_request_id' => 'ForeignKey',
      'merchant_service_id' => 'ForeignKey',
      'split_one'           => 'Number',
      'split_two'           => 'Number',
      'split_three'         => 'Number',
      'split_four'          => 'Number',
      'split_five'          => 'Number',
      'split_six'           => 'Number',
      'split_seven'         => 'Number',
      'split_eight'         => 'Number',
      'split_nine'          => 'Number',
      'split_ten'           => 'Number',
      'split_eleven'        => 'Number',
      'split_twelve'        => 'Number',
      'split_thirteen'      => 'Number',
      'split_fourteen'      => 'Number',
      'split_fifteen'       => 'Number',
      'split_sixteen'       => 'Number',
      'split_seventeen'     => 'Number',
      'split_eightteen'     => 'Number',
      'split_nineteen'      => 'Number',
      'split_twenty'        => 'Number',
      'is_processed'        => 'Number',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'deleted_at'          => 'Date',
      'created_by'          => 'Number',
      'updated_by'          => 'Number',
    );
  }
}
