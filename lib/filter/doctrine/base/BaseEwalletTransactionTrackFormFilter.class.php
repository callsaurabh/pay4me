<?php

/**
 * EwalletTransactionTrack filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEwalletTransactionTrackFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'transaction_code'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'type'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'BillRequest' => 'BillRequest', 'BillApprove' => 'BillApprove', 'BillReject' => 'BillReject', 'eWalletFreeze' => 'eWalletFreeze', 'eWalletTransfer' => 'eWalletTransfer'))),
      'app_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EwalletTransaction'), 'add_empty' => true)),
      'wallet_account_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'platform'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'Web' => 'Web', 'Mobile' => 'Mobile'))),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'transaction_code'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'type'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('BillRequest' => 'BillRequest', 'BillApprove' => 'BillApprove', 'BillReject' => 'BillReject', 'eWalletFreeze' => 'eWalletFreeze', 'eWalletTransfer' => 'eWalletTransfer'))),
      'app_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EwalletTransaction'), 'column' => 'id')),
      'wallet_account_number' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'platform'              => new sfValidatorChoice(array('required' => false, 'choices' => array('Web' => 'Web', 'Mobile' => 'Mobile'))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ewallet_transaction_track_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EwalletTransactionTrack';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'transaction_code'      => 'Number',
      'type'                  => 'Enum',
      'app_id'                => 'ForeignKey',
      'wallet_account_number' => 'ForeignKey',
      'platform'              => 'Enum',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Number',
      'updated_by'            => 'Number',
    );
  }
}
