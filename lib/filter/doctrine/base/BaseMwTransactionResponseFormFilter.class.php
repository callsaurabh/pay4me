<?php

/**
 * MwTransactionResponse filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMwTransactionResponseFormFilter extends MwResponseFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('mw_transaction_response_filters[%s]');
  }

  public function getModelName()
  {
    return 'MwTransactionResponse';
  }
}
