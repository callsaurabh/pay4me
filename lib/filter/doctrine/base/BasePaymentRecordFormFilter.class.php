<?php

/**
 * PaymentRecord filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePaymentRecordFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_batch_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentBatch'), 'add_empty' => true)),
      'account_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'account_no'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sortcode'         => new sfWidgetFormFilterInput(),
      'amount'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'account_name'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payor'            => new sfWidgetFormFilterInput(),
      'narration'        => new sfWidgetFormFilterInput(),
      'status'           => new sfWidgetFormFilterInput(),
      'paid_date'        => new sfWidgetFormFilterInput(),
      'validity_date'    => new sfWidgetFormFilterInput(),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'       => new sfWidgetFormFilterInput(),
      'updated_by'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'payment_batch_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentBatch'), 'column' => 'id')),
      'account_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'account_no'       => new sfValidatorPass(array('required' => false)),
      'sortcode'         => new sfValidatorPass(array('required' => false)),
      'amount'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'account_name'     => new sfValidatorPass(array('required' => false)),
      'payor'            => new sfValidatorPass(array('required' => false)),
      'narration'        => new sfValidatorPass(array('required' => false)),
      'status'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'paid_date'        => new sfValidatorPass(array('required' => false)),
      'validity_date'    => new sfValidatorPass(array('required' => false)),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('payment_record_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentRecord';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'payment_batch_id' => 'ForeignKey',
      'account_id'       => 'ForeignKey',
      'account_no'       => 'Text',
      'sortcode'         => 'Text',
      'amount'           => 'Number',
      'account_name'     => 'Text',
      'payor'            => 'Text',
      'narration'        => 'Text',
      'status'           => 'Number',
      'paid_date'        => 'Text',
      'validity_date'    => 'Text',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
      'deleted_at'       => 'Date',
      'created_by'       => 'Number',
      'updated_by'       => 'Number',
    );
  }
}
