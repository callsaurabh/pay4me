<?php

/**
 * BankBranch filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBankBranchFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bank_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'name'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address'         => new sfWidgetFormFilterInput(),
      'branch_code'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'branch_sortcode' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'lga_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Lga'), 'add_empty' => true)),
      'country_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'state_name'      => new sfWidgetFormFilterInput(),
      'lga_name'        => new sfWidgetFormFilterInput(),
      'ip_address'      => new sfWidgetFormFilterInput(),
      'status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'      => new sfWidgetFormFilterInput(),
      'updated_by'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'bank_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bank'), 'column' => 'id')),
      'name'            => new sfValidatorPass(array('required' => false)),
      'address'         => new sfValidatorPass(array('required' => false)),
      'branch_code'     => new sfValidatorPass(array('required' => false)),
      'branch_sortcode' => new sfValidatorPass(array('required' => false)),
      'lga_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Lga'), 'column' => 'id')),
      'country_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Country'), 'column' => 'id')),
      'state_name'      => new sfValidatorPass(array('required' => false)),
      'lga_name'        => new sfValidatorPass(array('required' => false)),
      'ip_address'      => new sfValidatorPass(array('required' => false)),
      'status'          => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('bank_branch_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BankBranch';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'bank_id'         => 'ForeignKey',
      'name'            => 'Text',
      'address'         => 'Text',
      'branch_code'     => 'Text',
      'branch_sortcode' => 'Text',
      'lga_id'          => 'ForeignKey',
      'country_id'      => 'ForeignKey',
      'state_name'      => 'Text',
      'lga_name'        => 'Text',
      'ip_address'      => 'Text',
      'status'          => 'Enum',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
      'deleted_at'      => 'Date',
      'created_by'      => 'Number',
      'updated_by'      => 'Number',
    );
  }
}
