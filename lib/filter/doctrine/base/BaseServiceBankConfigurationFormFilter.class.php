<?php

/**
 * ServiceBankConfiguration filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseServiceBankConfigurationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => true)),
      'payment_mode_option_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentModeOption'), 'add_empty' => true)),
      'bank_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'master_account_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'transfer_type'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'Payment' => 'Payment', 'Recharge From Bank' => 'Recharge From Bank', 'Recharge' => 'Recharge'))),
      'currency_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => true)),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'             => new sfWidgetFormFilterInput(),
      'updated_by'             => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'merchant_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Merchant'), 'column' => 'id')),
      'payment_mode_option_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentModeOption'), 'column' => 'id')),
      'bank_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bank'), 'column' => 'id')),
      'master_account_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'transfer_type'          => new sfValidatorChoice(array('required' => false, 'choices' => array('Payment' => 'Payment', 'Recharge From Bank' => 'Recharge From Bank', 'Recharge' => 'Recharge'))),
      'currency_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CurrencyCode'), 'column' => 'id')),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('service_bank_configuration_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ServiceBankConfiguration';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'merchant_id'            => 'ForeignKey',
      'payment_mode_option_id' => 'ForeignKey',
      'bank_id'                => 'ForeignKey',
      'master_account_id'      => 'ForeignKey',
      'transfer_type'          => 'Enum',
      'currency_id'            => 'ForeignKey',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'deleted_at'             => 'Date',
      'created_by'             => 'Number',
      'updated_by'             => 'Number',
    );
  }
}
