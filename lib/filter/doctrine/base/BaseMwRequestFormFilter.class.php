<?php

/**
 * MwRequest filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMwRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'message_text'                => new sfWidgetFormFilterInput(),
      'bank_mw_mapper_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BankMwMapping'), 'add_empty' => true)),
      'transaction_bank_posting_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TransactionBankPosting'), 'add_empty' => true)),
      'message_type'                => new sfWidgetFormFilterInput(),
      'created_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'message_text'                => new sfValidatorPass(array('required' => false)),
      'bank_mw_mapper_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('BankMwMapping'), 'column' => 'id')),
      'transaction_bank_posting_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TransactionBankPosting'), 'column' => 'id')),
      'message_type'                => new sfValidatorPass(array('required' => false)),
      'created_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('mw_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MwRequest';
  }

  public function getFields()
  {
    return array(
      'id'                          => 'Number',
      'message_text'                => 'Text',
      'bank_mw_mapper_id'           => 'ForeignKey',
      'transaction_bank_posting_id' => 'ForeignKey',
      'message_type'                => 'Text',
      'created_at'                  => 'Date',
      'updated_at'                  => 'Date',
    );
  }
}
