<?php

/**
 * NibssSettings filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseNibssSettingsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'filename'               => new sfWidgetFormFilterInput(),
      'message_from'           => new sfWidgetFormFilterInput(),
      'message_to'             => new sfWidgetFormFilterInput(),
      'message_cc'             => new sfWidgetFormFilterInput(),
      'message_subject'        => new sfWidgetFormFilterInput(),
      'payor'                  => new sfWidgetFormFilterInput(),
      'message_from_name'      => new sfWidgetFormFilterInput(),
      'outboundserver_address' => new sfWidgetFormFilterInput(),
      'outboundserver_port'    => new sfWidgetFormFilterInput(),
      'username'               => new sfWidgetFormFilterInput(),
      'password'               => new sfWidgetFormFilterInput(),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_by'             => new sfWidgetFormFilterInput(),
      'updated_by'             => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'filename'               => new sfValidatorPass(array('required' => false)),
      'message_from'           => new sfValidatorPass(array('required' => false)),
      'message_to'             => new sfValidatorPass(array('required' => false)),
      'message_cc'             => new sfValidatorPass(array('required' => false)),
      'message_subject'        => new sfValidatorPass(array('required' => false)),
      'payor'                  => new sfValidatorPass(array('required' => false)),
      'message_from_name'      => new sfValidatorPass(array('required' => false)),
      'outboundserver_address' => new sfValidatorPass(array('required' => false)),
      'outboundserver_port'    => new sfValidatorPass(array('required' => false)),
      'username'               => new sfValidatorPass(array('required' => false)),
      'password'               => new sfValidatorPass(array('required' => false)),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('nibss_settings_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'NibssSettings';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'filename'               => 'Text',
      'message_from'           => 'Text',
      'message_to'             => 'Text',
      'message_cc'             => 'Text',
      'message_subject'        => 'Text',
      'payor'                  => 'Text',
      'message_from_name'      => 'Text',
      'outboundserver_address' => 'Text',
      'outboundserver_port'    => 'Text',
      'username'               => 'Text',
      'password'               => 'Text',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'deleted_at'             => 'Date',
      'created_by'             => 'Number',
      'updated_by'             => 'Number',
    );
  }
}
