<?php

/**
 * MessageLog filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMessageLogFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'level'          => new sfWidgetFormFilterInput(),
      'transaction_no' => new sfWidgetFormFilterInput(),
      'log_message'    => new sfWidgetFormFilterInput(),
      'location'       => new sfWidgetFormFilterInput(),
      'logger_name'    => new sfWidgetFormFilterInput(),
      'time_stamp'     => new sfWidgetFormFilterInput(),
      'thread_name'    => new sfWidgetFormFilterInput(),
      'class_name'     => new sfWidgetFormFilterInput(),
      'error_details'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'level'          => new sfValidatorPass(array('required' => false)),
      'transaction_no' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'log_message'    => new sfValidatorPass(array('required' => false)),
      'location'       => new sfValidatorPass(array('required' => false)),
      'logger_name'    => new sfValidatorPass(array('required' => false)),
      'time_stamp'     => new sfValidatorPass(array('required' => false)),
      'thread_name'    => new sfValidatorPass(array('required' => false)),
      'class_name'     => new sfValidatorPass(array('required' => false)),
      'error_details'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('message_log_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MessageLog';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'level'          => 'Text',
      'transaction_no' => 'Number',
      'log_message'    => 'Text',
      'location'       => 'Text',
      'logger_name'    => 'Text',
      'time_stamp'     => 'Text',
      'thread_name'    => 'Text',
      'class_name'     => 'Text',
      'error_details'  => 'Text',
    );
  }
}
