<?php

/**
 * MwMessageLog filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMwMessageLogFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MwRequest'), 'add_empty' => true)),
      'log_message' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'request_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MwRequest'), 'column' => 'id')),
      'log_message' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('mw_message_log_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MwMessageLog';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'request_id'  => 'ForeignKey',
      'log_message' => 'Text',
    );
  }
}
