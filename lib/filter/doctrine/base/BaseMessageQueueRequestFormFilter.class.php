<?php

/**
 * MessageQueueRequest filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMessageQueueRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantRequest'), 'add_empty' => true)),
      'merchant_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => true)),
      'account_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'bank_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'teller_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'currency_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrencyCode'), 'add_empty' => true)),
      'amount'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total_amount'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_number'  => new sfWidgetFormFilterInput(),
      'validation_number'   => new sfWidgetFormFilterInput(),
      'account_number'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'item_name'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'type'                => new sfWidgetFormChoice(array('choices' => array('' => '', 'payment' => 'payment', 'recharge' => 'recharge'))),
      'payment_type'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'credit' => 'credit', 'debit' => 'debit'))),
      'is_processed'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'message_request'     => new sfWidgetFormFilterInput(),
      'no_of_attempt'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_date'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'merchant_request_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantRequest'), 'column' => 'id')),
      'merchant_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Merchant'), 'column' => 'id')),
      'account_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'bank_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bank'), 'column' => 'id')),
      'teller_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'currency_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CurrencyCode'), 'column' => 'id')),
      'amount'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'total_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_number'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'validation_number'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'account_number'      => new sfValidatorPass(array('required' => false)),
      'item_name'           => new sfValidatorPass(array('required' => false)),
      'type'                => new sfValidatorChoice(array('required' => false, 'choices' => array('payment' => 'payment', 'recharge' => 'recharge'))),
      'payment_type'        => new sfValidatorChoice(array('required' => false, 'choices' => array('credit' => 'credit', 'debit' => 'debit'))),
      'is_processed'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'message_request'     => new sfValidatorPass(array('required' => false)),
      'no_of_attempt'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_date'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('message_queue_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MessageQueueRequest';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'merchant_request_id' => 'ForeignKey',
      'merchant_id'         => 'ForeignKey',
      'account_id'          => 'ForeignKey',
      'bank_id'             => 'ForeignKey',
      'teller_id'           => 'ForeignKey',
      'currency_id'         => 'ForeignKey',
      'amount'              => 'Number',
      'total_amount'        => 'Number',
      'transaction_number'  => 'Number',
      'validation_number'   => 'Number',
      'account_number'      => 'Text',
      'item_name'           => 'Text',
      'type'                => 'Enum',
      'payment_type'        => 'Enum',
      'is_processed'        => 'Number',
      'message_request'     => 'Text',
      'no_of_attempt'       => 'Number',
      'transaction_date'    => 'Date',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
