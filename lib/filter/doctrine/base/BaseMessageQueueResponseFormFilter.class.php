<?php

/**
 * MessageQueueResponse filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMessageQueueResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MessageQueueRequest'), 'add_empty' => true)),
      'response_code'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'response_description' => new sfWidgetFormFilterInput(),
      'message_response'     => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'request_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MessageQueueRequest'), 'column' => 'id')),
      'response_code'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'response_description' => new sfValidatorPass(array('required' => false)),
      'message_response'     => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('message_queue_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MessageQueueResponse';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'request_id'           => 'ForeignKey',
      'response_code'        => 'Number',
      'response_description' => 'Text',
      'message_response'     => 'Text',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
