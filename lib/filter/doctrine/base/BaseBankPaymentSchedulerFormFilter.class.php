<?php

/**
 * BankPaymentScheduler filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBankPaymentSchedulerFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bank_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bank'), 'add_empty' => true)),
      'notification_schedule' => new sfWidgetFormFilterInput(),
      'payment_schedule'      => new sfWidgetFormFilterInput(),
      'bank_key'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bank_code'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notification_url'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notification_status'   => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'payment_status'        => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'bank_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bank'), 'column' => 'id')),
      'notification_schedule' => new sfValidatorPass(array('required' => false)),
      'payment_schedule'      => new sfValidatorPass(array('required' => false)),
      'bank_key'              => new sfValidatorPass(array('required' => false)),
      'bank_code'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'notification_url'      => new sfValidatorPass(array('required' => false)),
      'notification_status'   => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'payment_status'        => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('bank_payment_scheduler_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BankPaymentScheduler';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'bank_id'               => 'ForeignKey',
      'notification_schedule' => 'Text',
      'payment_schedule'      => 'Text',
      'bank_key'              => 'Text',
      'bank_code'             => 'Number',
      'notification_url'      => 'Text',
      'notification_status'   => 'Enum',
      'payment_status'        => 'Enum',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Number',
      'updated_by'            => 'Number',
    );
  }
}
