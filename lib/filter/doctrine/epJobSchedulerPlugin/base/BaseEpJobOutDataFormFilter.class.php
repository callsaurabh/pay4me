<?php

/**
 * EpJobOutData filter form base class.
 *
 * @package    mysfp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEpJobOutDataFormFilter extends EpJobDataFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('ep_job_out_data_filters[%s]');
  }

  public function getModelName()
  {
    return 'EpJobOutData';
  }
}
