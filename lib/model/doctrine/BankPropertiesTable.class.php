<?php


class BankPropertiesTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('BankProperties');
    }

         public function getBankProperties($bankId, $status=1)
     {
       $res =array();
        $q =  Doctrine_Query::create()
              ->select('*')
              ->from('BankProperties')
              ->where('bank_id=?',$bankId)
             ->andWhere('status=?',$status);

          $res = $q->execute();

          if($res->count()){
              return $res;
          }
        return 0;
     }

     public function getBankQueueName($bankId, $status=1)
     {
       $res =array();
        $q =  Doctrine_Query::create()
              ->select('*')
              ->from('BankProperties')
              ->where('bank_id=?',$bankId)
             ->andWhere('status=?',$status)
             ->andWhere('property_name=?','INPUT_QUEUE_NAME');

        $res = $q->execute();

          if($res->count()){
              return $res->getFirst()->getPropertyValue();
          }
        return 0;
     }
     
     public function getBankPropertryValue($bankId, $propertyName) {
     	$res =array();
     	$q =  Doctrine_Query::create()
     	->select('*')
     	->from('BankProperties')
     	->where('bank_id=?',$bankId)
     	->andWhere('status=?','1')
     	->andWhere('property_name=?', $propertyName);
     	
     	$res = $q->execute();
     	
     	if($res->count()){
     		return $res->getFirst()->getPropertyValue();
     	}
     	return 0;
     	     }
     
}