<?php


class MessageQueueRequestTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('MessageQueueRequest');
    }

    public function getMessageData($id){
        try{
            $q = Doctrine_Query::create()
            ->select('mq.*,c.currency_num,s.username')
            ->from('MessageQueueRequest mq')
            ->leftJoin('mq.CurrencyCode c')
            ->leftJoin('mq.sfGuardUser s')

            ->where('id = ?', $id);

            $result = $q->execute();

            if($result->count())
            {
                return $result;
            }else {
                return false;
            }

        }catch(Exception $e) {
            throw new Exception("Problem found". $e->getMessage());

        }
    }

    public function updateMessageRequest($id,$message,$proceed,$flagReAttemptCount='') {
       // $noofAttempt = 1;
        try {

            $q = Doctrine_Query::create()
            ->update('MessageQueueRequest');
           if($proceed)
            $q ->set('is_processed','?',$proceed);
           if(!empty($flagReAttemptCount)){        //flag is true whn it calll from epjob
             $q->set('no_of_attempt','no_of_attempt + 1');

           }
            $q->set("message_request", '?',$message)
            ->where('id=?',$id);
            $rows = $q->execute();

            if($rows)
                return true;
            else
                 return false;
        } catch(Exception $e) {

            throw new Exception("Problem found". $e->getMessage());

        }

    }
    /* [WP040]- Bank Intigration
     * Function : getRequestDetails()
     * Purpose : get request detail
     * Param :$transactionType, $transactionNo='',$validationNo='', $merchant='',$bank='',$status=''
     * Return Type : Query
     * Date : 09 -08-2010
     * Author : Ryadav
     */
     public function getRequestDetails($transactionType, $transactionNo='',
     $validationNo='', $merchant='',$bank='',$status='',$strFromDate='',$strToDate=''){
        try{
             //convert the date into the proper format
             if($strFromDate)
             $strFromDate = $strFromDate." 00:00:00";
             if($strToDate)
             $strToDate = $strToDate." 23:59:59";
            $q = Doctrine_Query::create()
            ->select('mq.merchant_id,mq.updated_at,mq.bank_id,mq.total_amount,mq.transaction_number,mq.validation_number,
                    mq.account_number,mq.is_processed,mq.no_of_attempt,mq.transaction_date,m.id,m.name,b.id,b.bank_name')
            ->from('MessageQueueRequest mq')
            ->innerJoin('mq.Merchant m')
            ->innerJoin('mq.Bank b')
            ->where('mq.type  = ?',$transactionType);
            if(!empty($transactionNo)){
              $q->andWhere('mq.transaction_number  = ?',$transactionNo);
            }
            if(!empty($validationNo)){
                $q->andWhere('mq.validation_number = ?',$validationNo);
            }
            if(!empty($merchant)){
                $q->andWhere('mq.merchant_id = ?',$merchant);
            }
            if($status!=''){

                $q->andWhere('mq.is_processed  = ?',$status);
            }
            if(!empty($bank)){
                $q->andWhere('mq.bank_id = ?',$bank);
            }
            if(!empty($strFromDate) && !empty($strToDate)){
               $q->andWhere("mq.transaction_date BETWEEN ? AND ?",array($strFromDate,$strToDate));
            }
            if(!empty($strFromDate) && empty($strToDate)){
              $q->andWhere("mq.transaction_date >= ?",$strFromDate);
            }
            if(empty($strFromDate) && !empty($strToDate)){
              $q->andWhere("mq.transaction_date  <=?",$strToDate);
            }
            $q->orderBy('mq.created_at desc');
            return $q;
        }catch(Exception $e) {
            throw new Exception("Problem found". $e->getMessage());
        }
    }

    public function checkAllreadyPosted($id){

        try {
            $q = Doctrine_Query::create()
            ->select('mq.*')
            ->from('MessageQueueRequest mq')
            ->where('mq.id=?',$id)
            ->andWhere('mq.is_processed=?',1);


            if($q->count()>0)
                return true;
            else
                return false;
        } catch(Exception $e) {

            throw new Exception("Problem found". $e->getMessage());

        }
    }




    /* [WP040]- Bank Intigration
     * Function : getMessageQueueRequestByTime()
     * Purpose : Message Queue Request by Time
     * Param :$timeInterval
     * Return Type : $res HYDRATE ARRAY
     * Date : 13 -09-2011
     * Author : Ryadav
     *
     */


     public function getMessageQueueRequestByTime($timeInterval){
        try{
            $time=date("Y-m-d H:i:s",time() -($timeInterval*60));

            return Doctrine_Query::create()
            ->select('mq.id')
            ->from('MessageQueueRequest mq')
            ->where('mq.is_processed=?',1)
            ->andWhere('updated_at <= ?',date($time))
            ->execute(array(),Doctrine::HYDRATE_ARRAY);



        }catch(Exception $e) {
            return false;
            //throw new Exception("Problem found". $e->getMessage());

        }
    }
        /**
         * to get records where last response received from bank is "No response received from Bank"
         * WP056 Bug#34897 it will get record from one day back, as there are some records on production which would be reattempted
         * @param <type> $timeInterval
         * @return <type>
         */
         public function getMessageQueueRequestForNoReponse($timeInterval){
        try{
            $time=date("Y-m-d H:i:s",time() -($timeInterval*60));

            $result=Doctrine_Manager::getInstance()->getCurrentConnection()
            ->fetchAssoc("select mqr.request_id as id from message_queue_response mqr
            where mqr.response_description = 'No response received from Bank' 
            and mqr.updated_at>='".$time."'
            and mqr.id in
            (SELECT max( mqr2.id )
            FROM `message_queue_response` mqr2
            GROUP BY mqr2.`request_id`)");
            return $result;
        }catch(Exception $e) {
            return false;
            //throw new Exception("Problem found". $e->getMessage());

        }
    }
    
     public function getMessageQueueRequestFailure(){
        try{

            return Doctrine_Query::create()
            ->select('mq.id')
            ->from('MessageQueueRequest mq')
            ->where('mq.is_processed=?',3)
            ->execute(array(),Doctrine::HYDRATE_ARRAY);

        }catch(Exception $e) {
            return false;
            //throw new Exception("Problem found". $e->getMessage());

        }
    }

   public function findMailRecord($id){
        try{
            $q = Doctrine_Query::create()
            ->select('mq.id,mq.transaction_date ,mq.no_of_attempt,')
            ->from('MessageQueueRequest mq')
            ->leftJoin('mq.MessageQueueResponse')

            ->whereIn('mq.id', $id);

            $result = $q->execute();

            if($result->count())
            {
                return $result;
            }else {
                return false;
            }

        }catch(Exception $e) {
            throw new Exception("Problem found". $e->getMessage());

        }
    }


    public function getQueueDetailsById($id){
        return $q = Doctrine_Query::create()
            ->select('mq.id,mq.transaction_date,mq.transaction_number,mq.validation_number,mq.updated_at,mq.type,mq.no_of_attempt,b.bank_name,mq.is_processed,mq.transaction_date')
            ->from('MessageQueueRequest mq')
            ->innerJoin('mq.Bank b')
            ->where('mq.id=?', $id)
            ->fetchArray();

    }

}