<?php


class MerchantRequestPaymentDetailsTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('MerchantRequestPaymentDetails');
    }

    public function getCurrenyRequests($requestId, $currency_id="") {
       $q = $this->createQuery('s')
                ->select('*')
                ->innerJoin('s.CurrencyCode c')
                ->where('s.merchant_request_id=?', $requestId);

                if($currency_id!=""){
                  $q->andWhere('s.currency_id=?', $currency_id);
                }

                $result = $q->execute();
                if($result->count()) {
                  return $result;
                }
                return 0;
    }
}