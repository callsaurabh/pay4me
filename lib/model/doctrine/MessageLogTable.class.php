<?php


class MessageLogTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('MessageLog');
    }

    public static function getLogDetail($transactionNo){

        try {
             $q =  Doctrine_Query::create()
             ->select('m.*')
             ->from('MessageLog m');
             if($transactionNo!=""){
                $q->andWhere("m.transaction_no =?",$transactionNo);
            }
            
             return $q;
        } catch(Exception $e){
           echo("Problem found". $e->getMessage());die;
       }

    }
}