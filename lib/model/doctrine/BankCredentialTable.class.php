<?php

class BankCredentialTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('BankCredential');
    }

    public function getPassword($bankId, $user_name) {
        try {
            $query = $this->createQuery('t')
                            ->select('t.password')
                            ->from('BankCredential t')
                            ->where('t.bank_id=?', $bankId)
                            ->addWhere('t.user_name=?', $user_name);
            $result = $query->execute();
            return $result;
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function chkKeyDuplicacy($key) {
        try {
            $query = $this->createQuery('t')
                            ->select('t.*')
                            ->where('t.asymmetric_key=?', $key);
            $result = $query->execute();
            return $result->count();
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            
        }
    }

    public function getBankCredentailDetail($bank_id, $group_id) {
        try {
            $query = $this->createQuery('b')
                            ->select('b.id,b.user_name as user_name,b.asymmetric_key,b.password')
                            ->where('b.bank_id=?', $bank_id)
                            ->andWhere('b.user_group=?', $group_id);
            $result = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
            if (count($result) > 0)
                return $result;
            else
                return false;
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    /*
     * function : saveBankCredential()
     * param: $bankId,$arrayGroup,$bankAcronym,$password,$key
     */

    public function saveBankCredential($bankId, $arrayGroup, $bankAcronym, $password, $key) {
        $bankCredential = new BankCredential();
        $bankCredential->setBankId($bankId);
        $bankCredential->setUserName($arrayGroup . "." . $bankAcronym);
        $bankCredential->setPassword($password);
        $bankCredential->setAsymmetricKey($key);
        $bankCredential->setUserGroup($arrayGroup);
        $bankCredential->save();
    }

}