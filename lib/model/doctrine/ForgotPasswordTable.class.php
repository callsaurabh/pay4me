<?php


class ForgotPasswordTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ForgotPassword');
    }

    public function saveForgotPassword($userId,$expiryTime){

        $forgotPassword = new ForgotPassword();
        $forgotPassword->setUserId($userId);
        //$gatewayOrder->setActivicationToken($order_id);
        $forgotPassword->setExpiryTime($expiryTime);
        $forgotPassword->save();
        return $forgotPassword;

    }

    public function UpdateForgotPassword($id,$activactionToken){
          $q= Doctrine_Query::create()
            ->update('ForgotPassword')
            ->set('activation_token','?',$activactionToken)
            ->where('id=?',$id)
            ->execute();
    }

    public function UpdateExpiredLinks($userId){
          $q= Doctrine_Query::create()
            ->update('ForgotPassword')
            ->set('status','?','expired')
            ->where('user_id=?',$userId)
            ->andwhere('status =?','active')
            ->execute();
    }

        public function validateToken($userId, $token) {       
         $sql = $this->createQuery('a')
                  ->select('a.*')
                 ->where("a.user_id =?", $userId)
                ->andWhere("a.activation_token =?", $token)
//                ->andWhere("a.expiry_time > NOW()")
//                ->andWhere("a.status =?",'active')
                ->orderBy("a.id DESC")
                ->limit('1');
         
        $result = $sql->execute()->toArray(Doctrine::HYDRATE_ARRAY);
        
        $currentdate = strtotime(date('Y-m-d H:i:s'));
        if(isset($result[0]['expiry_time']) && $result[0]['expiry_time']!="")
            $expiry = strtotime($result[0]['expiry_time']);
        else
            $expiry = $currentdate;
         if(count($result)>0 && $result[0]['status']=='active' && $expiry > $currentdate) {
             return $result;
         }
         return false;

    }

    
}