<?php


class MessageQueueResponseTable extends Doctrine_Table
{
    
    public static function getInstance()
    { 
        return Doctrine_Core::getTable('MessageQueueResponse');
    }

    public static function getHistoryDetail($id){
        
        try {
             $q =  Doctrine_Query::create()
             ->select('m.*,mqr.*')
             ->from('MessageQueueResponse m')
             ->leftJoin('m.MessageQueueRequest mqr')
             ->where('mqr.id=?',$id);
//             echo "<pre>";
//             print_r($q->toArray());
//             die;
             return $q;
        } catch(Exception $e){
           echo("Problem found". $e->getMessage());die;
       }
        
    }
}