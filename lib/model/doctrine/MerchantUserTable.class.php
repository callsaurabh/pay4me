<?php


class MerchantUserTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('MerchantUser');
    }
    public function chkMerchantCodeExist($id)
    {

       $q = Doctrine_Query::create()
      ->select('m.*')
      ->from('MerchantUser m')
      ->where('merchant_id=?', $id);

      $res = $q->execute();
      if ($res->count() > 0) {
        return true;
      }
      return false;
    }
    public function getMerchantIdFromUserId($user_id)
    {
      $q = Doctrine_Query::create()
      ->select('m.merchant_id')
      ->from('MerchantUser m')
      ->where('user_id=?', $user_id);

      $res = $q->fetchArray();
      if(count($res)>0){
      return $res['0']["merchant_id"];
      }
      else 
      	return false;
      }

}