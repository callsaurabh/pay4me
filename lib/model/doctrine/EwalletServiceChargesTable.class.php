<?php


class EwalletServiceChargesTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('EwalletServiceCharges');
  }

  public function getServiceCharge($payment_mode_option_id){
    $q = Doctrine_Query::create()
    ->select('id, charge_amount, payment_mode_option_id, charge_type, upper_slab, lower_slab')
    ->from('EwalletServiceCharges')
    ->where('payment_mode_option_id = ?', $payment_mode_option_id)
    ->execute(array(),Doctrine::HYDRATE_ARRAY);

    if(count($q) > 0)
    {
      return $q[0];
    }else {
      return 0;
    }
  }

  

  /*
   *function getAllRecords()
   *@purpose : return query to fetch all ewallet transaction
   *@param : N/A
   *@return :  query
   *@author : ryadav
   *@date : 26-04-2011
   *WP034 - CR052
   */



  public function getAllRecords(){
    $q = Doctrine_Query::create()
   // ->select('es.*,po.payment_mode_id,po.name,pm.display_name')
    ->select('es.*,po.*,pm.*')
    ->from('EwalletServiceCharges es')
    ->leftjoin('es.PaymentModeOption po')
    ->leftjoin('po.PaymentMode pm');
    return $q;
  }
}