<?php

/**
 * MwRequestTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class MwRequestTable extends Doctrine_Table {

    /**
     * Returns an instance of this class.
     *
     * @return object MwRequestTable
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('MwRequest');
    }

    public function getRequestDetails($transactionType, $transactionNo='', $validationNo='', $merchant='', $bank='', $status='', $strFromDate='', $strToDate='') {
        try {
            //convert the date into the proper format
            if ($strFromDate)
                $strFromDate = $strFromDate . " 00:00:00";
            if ($strToDate)
                $strToDate = $strToDate . " 23:59:59";
            if($transactionType =='Payment'){
            $q = Doctrine_Query::create()
                            ->select('mwr.message_text,mwr.transaction_bank_posting_id,mwr.message_type,mwr.created_at,mwr.updated_at,tbp.no_of_attempts,tbp.txn_type,tbp.state,tbp.message_txn_no ,tbp.created_at,tbp.updated_at,mr.*')
                            ->from('MwRequest mwr')
                            ->leftJoin('mwr.TransactionBankPosting tbp')
                            ->leftJoin('tbp.MerchantRequest mr')
                            ->where('tbp.txn_type  = ?', $transactionType);
            }
            else{
                $q = Doctrine_Query::create()
                            ->select('mwr.message_text,mwr.transaction_bank_posting_id,mwr.message_type,mwr.created_at,mwr.updated_at,tbp.no_of_attempts,tbp.txn_type,tbp.state,tbp.message_txn_no ,tbp.created_at,tbp.updated_at,tbp.bank_id')
                            ->from('MwRequest mwr')
                            ->leftJoin('mwr.TransactionBankPosting tbp')
                            ->where('tbp.txn_type  = ?', $transactionType);

            }
            if (!empty($transactionNo)) {
                $q->andWhere('tbp.message_txn_no   = ?', $transactionNo);
            }
            if (!empty($validationNo)) {
                $q->andWhere('tbp.message_txn_no = ?', $validationNo);
            }
            if (!empty($merchant)) {
                $q->andWhere('mr.merchant_id = ?', $merchant);
            }
            if ($status != '') {
                $q->andWhere('tbp.state  = ?', $status);
            }
            if (!empty($bank)) {
                $q->andWhere('tbp.bank_id = ?', $bank);
            }
            if (!empty($strFromDate)) {
                $q->andWhere("tbp.created_at  >= ?", $strFromDate);
            }
            if (!empty($strToDate)) {
                $q->andWhere("tbp.created_at  <=?", $strToDate);
            }
            $q->groupBy('tbp.id')
                    ->orderBy('mwr.created_at desc');
            return $q;
        } catch (Exception $e) {
            throw new Exception("Problem found" . $e->getMessage());
        }
    }
    
    public function getMessageData($id) {
        try {
            $q = Doctrine_Query::create()
                            ->select('mwr.*')
                            ->from('MwRequest mwr')
                            ->where('mwr.id = ?', $id);

            $result = $q->execute();
            if ($result->count()) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw new Exception("Problem found" . $e->getMessage());
        }
    }
}
