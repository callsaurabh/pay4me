<?php

/**
 * BaseMessageQueueResponse
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $request_id
 * @property integer $response_code
 * @property string $response_description
 * @property blob $message_response
 * @property MessageQueueRequest $MessageQueueRequest
 * 
 * @method integer              getRequestId()            Returns the current record's "request_id" value
 * @method integer              getResponseCode()         Returns the current record's "response_code" value
 * @method string               getResponseDescription()  Returns the current record's "response_description" value
 * @method blob                 getMessageResponse()      Returns the current record's "message_response" value
 * @method MessageQueueRequest  getMessageQueueRequest()  Returns the current record's "MessageQueueRequest" value
 * @method MessageQueueResponse setRequestId()            Sets the current record's "request_id" value
 * @method MessageQueueResponse setResponseCode()         Sets the current record's "response_code" value
 * @method MessageQueueResponse setResponseDescription()  Sets the current record's "response_description" value
 * @method MessageQueueResponse setMessageResponse()      Sets the current record's "message_response" value
 * @method MessageQueueResponse setMessageQueueRequest()  Sets the current record's "MessageQueueRequest" value
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseMessageQueueResponse extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('message_queue_response');
        $this->hasColumn('request_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('response_code', 'integer', 2, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 2,
             ));
        $this->hasColumn('response_description', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('message_response', 'blob', null, array(
             'type' => 'blob',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('MessageQueueRequest', array(
             'local' => 'request_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}