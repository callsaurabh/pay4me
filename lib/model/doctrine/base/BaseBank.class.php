<?php

/**
 * BaseBank
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $bank_name
 * @property string $acronym
 * @property enum $status
 * @property integer $bank_code
 * @property string $bank_key
 * @property string $auth_info
 * @property enum $bi_enabled
 * @property enum $bi_logger_level
 * @property enum $bi_protocol_version
 * @property Doctrine_Collection $BankBranch
 * @property Doctrine_Collection $MerchantRequest
 * @property Doctrine_Collection $Transaction
 * @property Doctrine_Collection $BankUser
 * @property Doctrine_Collection $ServiceBankConfiguration
 * @property Doctrine_Collection $BankMerchant
 * @property Doctrine_Collection $BroadcastMessagePermission
 * @property Doctrine_Collection $FinancialInstitutionAcctConfig
 * @property Doctrine_Collection $BankPaymentScheduler
 * @property Doctrine_Collection $BankConfiguration
 * @property Doctrine_Collection $MessageQueueRequest
 * @property Doctrine_Collection $BankCredential
 * @property Doctrine_Collection $BankProperties
 * @property Doctrine_Collection $TransactionBankPosting
 * @property Doctrine_Collection $BankMwMapping
 * @property Doctrine_Collection $SortCode
 * 
 * @method string              getBankName()                       Returns the current record's "bank_name" value
 * @method string              getAcronym()                        Returns the current record's "acronym" value
 * @method enum                getStatus()                         Returns the current record's "status" value
 * @method integer             getBankCode()                       Returns the current record's "bank_code" value
 * @method string              getBankKey()                        Returns the current record's "bank_key" value
 * @method string              getAuthInfo()                       Returns the current record's "auth_info" value
 * @method enum                getBiEnabled()                      Returns the current record's "bi_enabled" value
 * @method enum                getBiLoggerLevel()                  Returns the current record's "bi_logger_level" value
 * @method enum                getBiProtocolVersion()              Returns the current record's "bi_protocol_version" value
 * @method Doctrine_Collection getBankBranch()                     Returns the current record's "BankBranch" collection
 * @method Doctrine_Collection getMerchantRequest()                Returns the current record's "MerchantRequest" collection
 * @method Doctrine_Collection getTransaction()                    Returns the current record's "Transaction" collection
 * @method Doctrine_Collection getBankUser()                       Returns the current record's "BankUser" collection
 * @method Doctrine_Collection getServiceBankConfiguration()       Returns the current record's "ServiceBankConfiguration" collection
 * @method Doctrine_Collection getBankMerchant()                   Returns the current record's "BankMerchant" collection
 * @method Doctrine_Collection getBroadcastMessagePermission()     Returns the current record's "BroadcastMessagePermission" collection
 * @method Doctrine_Collection getFinancialInstitutionAcctConfig() Returns the current record's "FinancialInstitutionAcctConfig" collection
 * @method Doctrine_Collection getBankPaymentScheduler()           Returns the current record's "BankPaymentScheduler" collection
 * @method Doctrine_Collection getBankConfiguration()              Returns the current record's "BankConfiguration" collection
 * @method Doctrine_Collection getMessageQueueRequest()            Returns the current record's "MessageQueueRequest" collection
 * @method Doctrine_Collection getBankCredential()                 Returns the current record's "BankCredential" collection
 * @method Doctrine_Collection getBankProperties()                 Returns the current record's "BankProperties" collection
 * @method Doctrine_Collection getTransactionBankPosting()         Returns the current record's "TransactionBankPosting" collection
 * @method Doctrine_Collection getBankMwMapping()                  Returns the current record's "BankMwMapping" collection
 * @method Doctrine_Collection getSortCode()                       Returns the current record's "SortCode" collection
 * @method Bank                setBankName()                       Sets the current record's "bank_name" value
 * @method Bank                setAcronym()                        Sets the current record's "acronym" value
 * @method Bank                setStatus()                         Sets the current record's "status" value
 * @method Bank                setBankCode()                       Sets the current record's "bank_code" value
 * @method Bank                setBankKey()                        Sets the current record's "bank_key" value
 * @method Bank                setAuthInfo()                       Sets the current record's "auth_info" value
 * @method Bank                setBiEnabled()                      Sets the current record's "bi_enabled" value
 * @method Bank                setBiLoggerLevel()                  Sets the current record's "bi_logger_level" value
 * @method Bank                setBiProtocolVersion()              Sets the current record's "bi_protocol_version" value
 * @method Bank                setBankBranch()                     Sets the current record's "BankBranch" collection
 * @method Bank                setMerchantRequest()                Sets the current record's "MerchantRequest" collection
 * @method Bank                setTransaction()                    Sets the current record's "Transaction" collection
 * @method Bank                setBankUser()                       Sets the current record's "BankUser" collection
 * @method Bank                setServiceBankConfiguration()       Sets the current record's "ServiceBankConfiguration" collection
 * @method Bank                setBankMerchant()                   Sets the current record's "BankMerchant" collection
 * @method Bank                setBroadcastMessagePermission()     Sets the current record's "BroadcastMessagePermission" collection
 * @method Bank                setFinancialInstitutionAcctConfig() Sets the current record's "FinancialInstitutionAcctConfig" collection
 * @method Bank                setBankPaymentScheduler()           Sets the current record's "BankPaymentScheduler" collection
 * @method Bank                setBankConfiguration()              Sets the current record's "BankConfiguration" collection
 * @method Bank                setMessageQueueRequest()            Sets the current record's "MessageQueueRequest" collection
 * @method Bank                setBankCredential()                 Sets the current record's "BankCredential" collection
 * @method Bank                setBankProperties()                 Sets the current record's "BankProperties" collection
 * @method Bank                setTransactionBankPosting()         Sets the current record's "TransactionBankPosting" collection
 * @method Bank                setBankMwMapping()                  Sets the current record's "BankMwMapping" collection
 * @method Bank                setSortCode()                       Sets the current record's "SortCode" collection
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseBank extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('bank');
        $this->hasColumn('bank_name', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 255,
             ));
        $this->hasColumn('acronym', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'unique' => true,
             'length' => 100,
             ));
        $this->hasColumn('status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 0,
              1 => 1,
             ),
             'default' => 1,
             'notnull' => true,
             ));
        $this->hasColumn('bank_code', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('bank_key', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('auth_info', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('bi_enabled', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 0,
              1 => 1,
             ),
             'default' => 0,
             'notnull' => true,
             ));
        $this->hasColumn('bi_logger_level', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'WARN',
              1 => 'INFO',
              2 => 'DEBUG',
              3 => 'FATAL',
              4 => 'ERROR',
             ),
             'default' => 'ERROR',
             'notnull' => true,
             ));
        $this->hasColumn('bi_protocol_version', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'V1',
              1 => 'v2',
             ),
             'default' => 'V1',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('BankBranch', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('MerchantRequest', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('Transaction', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankUser', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('ServiceBankConfiguration', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankMerchant', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BroadcastMessagePermission', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('FinancialInstitutionAcctConfig', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankPaymentScheduler', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankConfiguration', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('MessageQueueRequest', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankCredential', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankProperties', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('TransactionBankPosting', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankMwMapping', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $this->hasMany('BankSortCodeMapper as SortCode', array(
             'local' => 'id',
             'foreign' => 'bank_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}