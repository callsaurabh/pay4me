<?php

/**
 * BaseNibssSettings
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $filename
 * @property string $message_from
 * @property string $message_to
 * @property string $message_cc
 * @property string $message_subject
 * @property string $payor
 * @property string $message_from_name
 * @property string $outboundserver_address
 * @property string $outboundserver_port
 * @property string $username
 * @property string $password
 * 
 * @method string        getFilename()               Returns the current record's "filename" value
 * @method string        getMessageFrom()            Returns the current record's "message_from" value
 * @method string        getMessageTo()              Returns the current record's "message_to" value
 * @method string        getMessageCc()              Returns the current record's "message_cc" value
 * @method string        getMessageSubject()         Returns the current record's "message_subject" value
 * @method string        getPayor()                  Returns the current record's "payor" value
 * @method string        getMessageFromName()        Returns the current record's "message_from_name" value
 * @method string        getOutboundserverAddress()  Returns the current record's "outboundserver_address" value
 * @method string        getOutboundserverPort()     Returns the current record's "outboundserver_port" value
 * @method string        getUsername()               Returns the current record's "username" value
 * @method string        getPassword()               Returns the current record's "password" value
 * @method NibssSettings setFilename()               Sets the current record's "filename" value
 * @method NibssSettings setMessageFrom()            Sets the current record's "message_from" value
 * @method NibssSettings setMessageTo()              Sets the current record's "message_to" value
 * @method NibssSettings setMessageCc()              Sets the current record's "message_cc" value
 * @method NibssSettings setMessageSubject()         Sets the current record's "message_subject" value
 * @method NibssSettings setPayor()                  Sets the current record's "payor" value
 * @method NibssSettings setMessageFromName()        Sets the current record's "message_from_name" value
 * @method NibssSettings setOutboundserverAddress()  Sets the current record's "outboundserver_address" value
 * @method NibssSettings setOutboundserverPort()     Sets the current record's "outboundserver_port" value
 * @method NibssSettings setUsername()               Sets the current record's "username" value
 * @method NibssSettings setPassword()               Sets the current record's "password" value
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseNibssSettings extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('nibss_settings');
        $this->hasColumn('filename', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('message_from', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('message_to', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('message_cc', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('message_subject', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('payor', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('message_from_name', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('outboundserver_address', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('outboundserver_port', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('username', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('password', 'string', null, array(
             'type' => 'string',
             ));

        $this->option('type', 'INNODB');
        $this->option('collate', 'utf8_unicode_ci');
        $this->option('charset', 'utf8');
    }

    public function setUp()
    {
        parent::setUp();
        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}