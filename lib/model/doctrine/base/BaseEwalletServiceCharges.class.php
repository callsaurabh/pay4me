<?php

/**
 * BaseEwalletServiceCharges
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $payment_mode_option_id
 * @property float $charge_amount
 * @property enum $charge_type
 * @property float $upper_slab
 * @property float $lower_slab
 * @property PaymentModeOption $PaymentModeOption
 * 
 * @method integer               getPaymentModeOptionId()    Returns the current record's "payment_mode_option_id" value
 * @method float                 getChargeAmount()           Returns the current record's "charge_amount" value
 * @method enum                  getChargeType()             Returns the current record's "charge_type" value
 * @method float                 getUpperSlab()              Returns the current record's "upper_slab" value
 * @method float                 getLowerSlab()              Returns the current record's "lower_slab" value
 * @method PaymentModeOption     getPaymentModeOption()      Returns the current record's "PaymentModeOption" value
 * @method EwalletServiceCharges setPaymentModeOptionId()    Sets the current record's "payment_mode_option_id" value
 * @method EwalletServiceCharges setChargeAmount()           Sets the current record's "charge_amount" value
 * @method EwalletServiceCharges setChargeType()             Sets the current record's "charge_type" value
 * @method EwalletServiceCharges setUpperSlab()              Sets the current record's "upper_slab" value
 * @method EwalletServiceCharges setLowerSlab()              Sets the current record's "lower_slab" value
 * @method EwalletServiceCharges setPaymentModeOption()      Sets the current record's "PaymentModeOption" value
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseEwalletServiceCharges extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('ewallet_service_charges');
        $this->hasColumn('payment_mode_option_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => false,
             ));
        $this->hasColumn('charge_amount', 'float', null, array(
             'type' => 'float',
             ));
        $this->hasColumn('charge_type', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'percent',
              1 => 'flat',
             ),
             'default' => 'percent',
             'notnull' => true,
             ));
        $this->hasColumn('upper_slab', 'float', null, array(
             'type' => 'float',
             ));
        $this->hasColumn('lower_slab', 'float', null, array(
             'type' => 'float',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('PaymentModeOption', array(
             'local' => 'payment_mode_option_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}