<?php

/**
 * BaseVirtualAccountConfig
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $virtual_account_id
 * @property integer $physical_account_id
 * @property enum $payment_flag
 * @property EpMasterAccount $EpMasterAccountVirtual
 * @property EpMasterAccount $EpMasterAccountPhysical
 * 
 * @method integer              getVirtualAccountId()        Returns the current record's "virtual_account_id" value
 * @method integer              getPhysicalAccountId()       Returns the current record's "physical_account_id" value
 * @method enum                 getPaymentFlag()             Returns the current record's "payment_flag" value
 * @method EpMasterAccount      getEpMasterAccountVirtual()  Returns the current record's "EpMasterAccountVirtual" value
 * @method EpMasterAccount      getEpMasterAccountPhysical() Returns the current record's "EpMasterAccountPhysical" value
 * @method VirtualAccountConfig setVirtualAccountId()        Sets the current record's "virtual_account_id" value
 * @method VirtualAccountConfig setPhysicalAccountId()       Sets the current record's "physical_account_id" value
 * @method VirtualAccountConfig setPaymentFlag()             Sets the current record's "payment_flag" value
 * @method VirtualAccountConfig setEpMasterAccountVirtual()  Sets the current record's "EpMasterAccountVirtual" value
 * @method VirtualAccountConfig setEpMasterAccountPhysical() Sets the current record's "EpMasterAccountPhysical" value
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVirtualAccountConfig extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('virtual_account_config');
        $this->hasColumn('virtual_account_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('physical_account_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('payment_flag', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => '0',
              1 => '1',
             ),
             'default' => 0,
             'notnull' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('EpMasterAccount as EpMasterAccountVirtual', array(
             'local' => 'virtual_account_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('EpMasterAccount as EpMasterAccountPhysical', array(
             'local' => 'physical_account_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}