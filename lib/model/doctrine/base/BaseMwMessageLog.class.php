<?php

/**
 * BaseMwMessageLog
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $request_id
 * @property text $log_message
 * @property MwRequest $MwRequest
 * 
 * @method integer      getRequestId()   Returns the current record's "request_id" value
 * @method text         getLogMessage()  Returns the current record's "log_message" value
 * @method MwRequest    getMwRequest()   Returns the current record's "MwRequest" value
 * @method MwMessageLog setRequestId()   Sets the current record's "request_id" value
 * @method MwMessageLog setLogMessage()  Sets the current record's "log_message" value
 * @method MwMessageLog setMwRequest()   Sets the current record's "MwRequest" value
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseMwMessageLog extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('mw_message_log');
        $this->hasColumn('request_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => false,
             'length' => 4,
             ));
        $this->hasColumn('log_message', 'text', null, array(
             'type' => 'text',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('MwRequest', array(
             'local' => 'request_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}