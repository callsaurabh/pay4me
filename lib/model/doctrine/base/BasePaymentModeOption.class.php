<?php

/**
 * BasePaymentModeOption
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $payment_mode_id
 * @property string $name
 * @property PaymentMode $PaymentMode
 * @property Doctrine_Collection $TransactionCharges
 * @property Doctrine_Collection $MerchantRequest
 * @property Doctrine_Collection $Transaction
 * @property Doctrine_Collection $ServicePaymentModeOption
 * @property Doctrine_Collection $ServiceBankConfiguration
 * @property Doctrine_Collection $MerchantServiceCharges
 * @property Doctrine_Collection $MerchantAcctReconcilationSchedule
 * @property Doctrine_Collection $GatewayOrder
 * @property Doctrine_Collection $RechargeAmountAvailability
 * @property Doctrine_Collection $ScheduledPaymentConfig
 * @property Doctrine_Collection $MerchantRequestPaymentMode
 * @property Doctrine_Collection $EwalletServiceCharges
 * @property Doctrine_Collection $EwalletRechargeAcctDetails
 * 
 * @method integer             getPaymentModeId()                     Returns the current record's "payment_mode_id" value
 * @method string              getName()                              Returns the current record's "name" value
 * @method PaymentMode         getPaymentMode()                       Returns the current record's "PaymentMode" value
 * @method Doctrine_Collection getTransactionCharges()                Returns the current record's "TransactionCharges" collection
 * @method Doctrine_Collection getMerchantRequest()                   Returns the current record's "MerchantRequest" collection
 * @method Doctrine_Collection getTransaction()                       Returns the current record's "Transaction" collection
 * @method Doctrine_Collection getServicePaymentModeOption()          Returns the current record's "ServicePaymentModeOption" collection
 * @method Doctrine_Collection getServiceBankConfiguration()          Returns the current record's "ServiceBankConfiguration" collection
 * @method Doctrine_Collection getMerchantServiceCharges()            Returns the current record's "MerchantServiceCharges" collection
 * @method Doctrine_Collection getMerchantAcctReconcilationSchedule() Returns the current record's "MerchantAcctReconcilationSchedule" collection
 * @method Doctrine_Collection getGatewayOrder()                      Returns the current record's "GatewayOrder" collection
 * @method Doctrine_Collection getRechargeAmountAvailability()        Returns the current record's "RechargeAmountAvailability" collection
 * @method Doctrine_Collection getScheduledPaymentConfig()            Returns the current record's "ScheduledPaymentConfig" collection
 * @method Doctrine_Collection getMerchantRequestPaymentMode()        Returns the current record's "MerchantRequestPaymentMode" collection
 * @method Doctrine_Collection getEwalletServiceCharges()             Returns the current record's "EwalletServiceCharges" collection
 * @method Doctrine_Collection getEwalletRechargeAcctDetails()        Returns the current record's "EwalletRechargeAcctDetails" collection
 * @method PaymentModeOption   setPaymentModeId()                     Sets the current record's "payment_mode_id" value
 * @method PaymentModeOption   setName()                              Sets the current record's "name" value
 * @method PaymentModeOption   setPaymentMode()                       Sets the current record's "PaymentMode" value
 * @method PaymentModeOption   setTransactionCharges()                Sets the current record's "TransactionCharges" collection
 * @method PaymentModeOption   setMerchantRequest()                   Sets the current record's "MerchantRequest" collection
 * @method PaymentModeOption   setTransaction()                       Sets the current record's "Transaction" collection
 * @method PaymentModeOption   setServicePaymentModeOption()          Sets the current record's "ServicePaymentModeOption" collection
 * @method PaymentModeOption   setServiceBankConfiguration()          Sets the current record's "ServiceBankConfiguration" collection
 * @method PaymentModeOption   setMerchantServiceCharges()            Sets the current record's "MerchantServiceCharges" collection
 * @method PaymentModeOption   setMerchantAcctReconcilationSchedule() Sets the current record's "MerchantAcctReconcilationSchedule" collection
 * @method PaymentModeOption   setGatewayOrder()                      Sets the current record's "GatewayOrder" collection
 * @method PaymentModeOption   setRechargeAmountAvailability()        Sets the current record's "RechargeAmountAvailability" collection
 * @method PaymentModeOption   setScheduledPaymentConfig()            Sets the current record's "ScheduledPaymentConfig" collection
 * @method PaymentModeOption   setMerchantRequestPaymentMode()        Sets the current record's "MerchantRequestPaymentMode" collection
 * @method PaymentModeOption   setEwalletServiceCharges()             Sets the current record's "EwalletServiceCharges" collection
 * @method PaymentModeOption   setEwalletRechargeAcctDetails()        Sets the current record's "EwalletRechargeAcctDetails" collection
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePaymentModeOption extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('payment_mode_option');
        $this->hasColumn('payment_mode_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('name', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 255,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('PaymentMode', array(
             'local' => 'payment_mode_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasMany('TransactionCharges', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('MerchantRequest', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('Transaction', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('ServicePaymentModeOption', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('ServiceBankConfiguration', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('MerchantServiceCharges', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('MerchantAcctReconcilationSchedule', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('GatewayOrder', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('RechargeAmountAvailability', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('ScheduledPaymentConfig', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('MerchantRequestPaymentMode', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('EwalletServiceCharges', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $this->hasMany('EwalletRechargeAcctDetails', array(
             'local' => 'id',
             'foreign' => 'payment_mode_option_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}