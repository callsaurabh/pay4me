<?php

/**
 * BaseError
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $code
 * @property string $message
 * @property integer $merchant_service_id
 * @property float $service_charge_percent
 * @property float $upper_slab
 * @property float $lower_slab
 * @property MerchantService $MerchantService
 * 
 * @method integer         getCode()                   Returns the current record's "code" value
 * @method string          getMessage()                Returns the current record's "message" value
 * @method integer         getMerchantServiceId()      Returns the current record's "merchant_service_id" value
 * @method float           getServiceChargePercent()   Returns the current record's "service_charge_percent" value
 * @method float           getUpperSlab()              Returns the current record's "upper_slab" value
 * @method float           getLowerSlab()              Returns the current record's "lower_slab" value
 * @method MerchantService getMerchantService()        Returns the current record's "MerchantService" value
 * @method Error           setCode()                   Sets the current record's "code" value
 * @method Error           setMessage()                Sets the current record's "message" value
 * @method Error           setMerchantServiceId()      Sets the current record's "merchant_service_id" value
 * @method Error           setServiceChargePercent()   Sets the current record's "service_charge_percent" value
 * @method Error           setUpperSlab()              Sets the current record's "upper_slab" value
 * @method Error           setLowerSlab()              Sets the current record's "lower_slab" value
 * @method Error           setMerchantService()        Sets the current record's "MerchantService" value
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseError extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('error');
        $this->hasColumn('code', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('message', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('merchant_service_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('service_charge_percent', 'float', null, array(
             'type' => 'float',
             'notnull' => true,
             'default' => 0,
             'comment' => 'This calculates the percent',
             ));
        $this->hasColumn('upper_slab', 'float', null, array(
             'type' => 'float',
             'notnull' => true,
             'default' => 0,
             'comment' => 'This is the upper limit',
             ));
        $this->hasColumn('lower_slab', 'float', null, array(
             'type' => 'float',
             'notnull' => true,
             'default' => 0,
             'comment' => 'This is the lower limit',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('MerchantService', array(
             'local' => 'merchant_service_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}