<?php

/**
 * BaseMwResponse
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property text $message_text
 * @property integer $request_id
 * @property string $message_type
 * @property MwRequest $MwRequest
 * 
 * @method text       getMessageText()  Returns the current record's "message_text" value
 * @method integer    getRequestId()    Returns the current record's "request_id" value
 * @method string     getMessageType()  Returns the current record's "message_type" value
 * @method MwRequest  getMwRequest()    Returns the current record's "MwRequest" value
 * @method MwResponse setMessageText()  Sets the current record's "message_text" value
 * @method MwResponse setRequestId()    Sets the current record's "request_id" value
 * @method MwResponse setMessageType()  Sets the current record's "message_type" value
 * @method MwResponse setMwRequest()    Sets the current record's "MwRequest" value
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseMwResponse extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('mw_response');
        $this->hasColumn('message_text', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('request_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('message_type', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));

        $this->setSubClasses(array(
             'MwTransactionResponse' => 
             array(
              'message_type' => 'transaction',
             ),
             'MwCommandResponse' => 
             array(
              'message_type' => 'command',
             ),
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('MwRequest', array(
             'local' => 'request_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}