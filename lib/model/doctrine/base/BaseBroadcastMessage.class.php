<?php

/**
 * BaseBroadcastMessage
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property string $message
 * @property enum $is_active
 * @property date $exp_date
 * @property Doctrine_Collection $BroadcastMessagePermission
 * 
 * @method integer             getId()                         Returns the current record's "id" value
 * @method string              getTitle()                      Returns the current record's "title" value
 * @method string              getMessage()                    Returns the current record's "message" value
 * @method enum                getIsActive()                   Returns the current record's "is_active" value
 * @method date                getExpDate()                    Returns the current record's "exp_date" value
 * @method Doctrine_Collection getBroadcastMessagePermission() Returns the current record's "BroadcastMessagePermission" collection
 * @method BroadcastMessage    setId()                         Sets the current record's "id" value
 * @method BroadcastMessage    setTitle()                      Sets the current record's "title" value
 * @method BroadcastMessage    setMessage()                    Sets the current record's "message" value
 * @method BroadcastMessage    setIsActive()                   Sets the current record's "is_active" value
 * @method BroadcastMessage    setExpDate()                    Sets the current record's "exp_date" value
 * @method BroadcastMessage    setBroadcastMessagePermission() Sets the current record's "BroadcastMessagePermission" collection
 * 
 * @package    mysfp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseBroadcastMessage extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('broadcast_message');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('title', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('message', 'string', 1000, array(
             'type' => 'string',
             'length' => 1000,
             ));
        $this->hasColumn('is_active', 'enum', null, array(
             'default' => '0',
             'type' => 'enum',
             'values' => 
             array(
              0 => '1',
              1 => '0',
              2 => '2',
             ),
             ));
        $this->hasColumn('exp_date', 'date', null, array(
             'type' => 'date',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('BroadcastMessagePermission', array(
             'local' => 'id',
             'foreign' => 'message_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}