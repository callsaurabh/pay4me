<?php


class BankConfigurationTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('BankConfiguration');
    }
    
     public function getAllCountry($bankId='', $country_id="",$returnVal=""){
        $q = $this->createQuery()
            ->select('bc.*,c.*')
            ->from('BankConfiguration bc')
            ->leftJoin('bc.Country c');
            if($bankId!="")
                  $q->where("bc.bank_id = ?",$bankId);
            if($country_id!="") {
              $q = $q->andWhere('c.id = ?',$country_id);
            }
            if('query'==$returnVal)
            return $q;
           $q = $q->orderby('c.name asc');
           $res = $q->execute();
           if($res->count()>0){
            $countryArray['']   ="--Select Country--";
            foreach($res as $key => $val) {
                    $countryArray[$val['country_id']] = $val['Country']['name'];
                }
            return $countryArray;
        }
        return 0;



    }
    public function getBankConfiguration($bankId = ""){
        $q = $this->createQuery()
            ->select('*')
            ->from('BankConfiguration');
       if($bankId!="")
        $q->where("bank_id = ?",$bankId);
        return $q;
    }

    public function getBankCountry($bank_id="", $country_id="") {
        $q = $this->createQuery()
            ->select('b.*')
            ->from('BankConfiguration b');
            if($bank_id!="")
                  $q->where("b.bank_id = ?",$bank_id);
            if($country_id!="") {
              $q = $q->andWhere('b.country_id = ?',$country_id);
            }
           $res = $q->execute();
           if($res->count()) {
               return $res;
           }
           return 0;
    }

      //save bank configuration
   public function saveBankConfig($bankId,$countryId,$domainName)
   {
            $bank_config = new BankConfiguration();
            $bank_config->setBank_id($bankId);
            $bank_config->setCountry_id($countryId);
            $bank_config->setDomain($domainName);
            $bank_config->save();
   }


}