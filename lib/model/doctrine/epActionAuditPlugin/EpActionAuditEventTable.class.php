<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class EpActionAuditEventTable extends PluginEpActionAuditEventTable {
//method to get the result -- this query is for portal admin
  public function getSearchData($sDate, $eDate, $bank="", $branch="", $category="", $subcategory="", $username="",$selectBy="",$userId="",$ivalue="",$allowedGroupIds=Array()) {
        $q = $this->createQuery()
        ->select("s.id, s.user_id, s.username, s.ip_address, s.category, s.subcategory, s.description, s.created_at, t.id AS ATT_ID, t.*,a.*")
        ->from('EpActionAuditEvent s')
        ->leftJoin('s.EpActionAuditEventAttributes t')
        ->leftJoin('s.EpActionAuditEventAttributes a')
        ;
     if(count($allowedGroupIds)>0){
         $q->leftJoin('s.sfGuardUser u')
            ->leftJoin('u.sfGuardUserGroup ug');
     }

    if($sDate != "") {
      $q->addWhere("s.created_at >=?",$sDate);
    }

    if($eDate != "") {
      $q->addWhere("s.created_at <=?",$eDate);
    }

    if($bank != "") {
      $q->addWhere("t.name = 'BankName' and  t.ivalue =?",$bank);
    }

    if($branch != "") {
      $q->addWhere("a.name='BranchName' and a.ivalue =?",$branch);
    }

    if($category != "") {
      $q->addWhere("s.category =?",$category);
    }

    if($subcategory != "") {
      $q->addWhere("s.subcategory =?",$subcategory);
    }

    if($username != "") {
      $q->addWhere("s.username =?",$username);
    }
    if($userId != "") {
      $q->addWhere("s.user_id =?",$userId);
    }

    if($ivalue != "") {
      $q->addWhere("t.ivalue =?",$ivalue);
    }
    
     if(count($allowedGroupIds)>0){
         $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
         $q->addWhere('ug.group_id in ? or u.id=?',array($allowedGroupIds,$userId));
     }
     $q->orderBy('s.created_at DESC');
    return $q;
  }

    
  public function getAuditDataForCSVDownload($postDataArray,$userDetails=array(),$allowedGroupIds=array()) {

    try {

      $startDate = $postDataArray['startDate_date'];
      if(isset($postDataArray['endDate_date'])) {
        $endDate = $postDataArray['endDate_date'];
      }
      if(!isset($endDate) || $endDate == '') {
        $endDate = $startDate;
      }

      //convert the date into the proper format
      $startDate = date('Y-m-d', strtotime($startDate));
      $startDate = $startDate." 00:00:00";

      $endDate = date('Y-m-d', strtotime($endDate));
      $endDate = $endDate." 23:59:59";

      // Connects to your Database
      $dbArr = DBInfo::getDSNInfo();
      mysql_connect($dbArr['host'],$dbArr['username'],$dbArr['password']) or die(mysql_error());
      mysql_select_db($dbArr['dbname']) or die(mysql_error());

      $q = $this->createQuery()
          ->select("username, ip_address, category, subcategory, description, created_at, t.id AS ATT_ID,t.id,a.id, t.*,a.*")
          ->from('EpActionAuditEvent s')
          ->leftJoin('s.EpActionAuditEventAttributes t')
          ->leftJoin('s.EpActionAuditEventAttributes a');

          if(count($allowedGroupIds)>0){
         $q->leftJoin('s.sfGuardUser u')
            ->leftJoin('u.sfGuardUserGroup ug');
         }
         $q ->where("1=1");


      if($startDate != "") {
        $q->addWhere("s.created_at >=?",$startDate);
      }
      if($endDate != "") {
        $q->addWhere("s.created_at <=?",$endDate);
      }
      if(isset($postDataArray['bank']) && ($postDataArray['bank'] != "")) {
        $bank = $postDataArray['bank'];
        $q->addWhere("t.name = 'BankName' and t.ivalue =?",$bank);
        //$sql .= " and t.name = 'BankName' and t.ivalue = '".$bank."' ";
      }
      if(isset($postDataArray['branch']) && ($postDataArray['branch'] != "")) {
        $branch = $postDataArray['branch'];
        $q->addWhere("a.name= 'BranchName' and a.ivalue=?",$branch);
      //  $sql .= " and a.name = 'BranchName' and a.ivalue = '".$branch."' ";
      }
      if($postDataArray['category'] != "") {
        $category = $postDataArray['category'];
        $q->addWhere("s.category =?",$category);
      //  $sql.= " AND s.category = '".$category."'";
      }
      if($postDataArray['subCategory'] != "") {
        $subcategory = $postDataArray['subCategory'];
        $q->addWhere("s.subcategory =?",$subcategory);
      //  $sql.= " AND s.subcategory = '".$subcategory."'";
      }
      if($postDataArray['username'] != "") {
        $username = $postDataArray['username'];
        $q->addWhere("s.username =?",$username);
       // $sql.= " AND s.username = '".$username."'";

      }

      
    if(isset($postDataArray['userId']) && $postDataArray['userId']!= "") {
      $q->addWhere("s.user_id =?",$postDataArray['userId']);
    }

    if(isset($postDataArray['ivalue']) && $postDataArray['ivalue'] != "") {
      $q->addWhere("t.ivalue =?",$postDataArray['ivalue']);
    }
    if(count($allowedGroupIds)>0){
         $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
         $q->addWhere('ug.group_id in ? or u.id=?',array($allowedGroupIds,$userId));
     }
      $q->orderBy('s.created_at DESC');
      //  print $sql;exit;
      return $q->execute(array(),Doctrine::HYDRATE_ARRAY);

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }




}
