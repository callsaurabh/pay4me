<?php


class CurrencyCodeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CurrencyCode');
    }
   public function getCurrenyForMerchantService($serviceId){

        $q = $this->createQuery('cc')
        ->select('cc.currency_num')
        ->innerJoin('cc.ServicePaymentModeOption i')
        ->where("i.merchant_service_id = ?",$serviceId)
        ->execute();
        if($q->count()>0)
            return $q;
        else
            return false;
    }
   public function getCurrenyfrmId($currId){

        $q = $this->createQuery('cc')
        ->select('cc.*')
        ->where("cc.id = ?",$currId);
        $res =   $q->execute();
        if($res->count()>0)
            {
                $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);               
                return $res[0];
            }
            
        else
            return false;
    }


   public function getAllDistinctCurrencyService($payment_mode='',$intMerchantId='',$intServiceId=''){

        
    
        $q = $this->createQuery('cc')
        ->select('cc.*,sp.*,ms.id')
        ->leftJoin('cc.ServicePaymentModeOption sp')
        ->leftJoin('sp.MerchantService ms');
        if(!empty($payment_mode)){
          $q->where("sp.payment_mode_option_id = ?",$payment_mode);
        }
        if(!empty($intMerchantId)){
          $q->andwhere("ms.merchant_id = ?",$intMerchantId);
        }
        if(!empty($intServiceId)){
          $q->andwhere("sp.merchant_service_id = ?",$intServiceId);
        }

        $q->orderBy('cc.id')
        ->groupBy('sp.currency_id');

        

         $res = $q->execute();

         if($res->count()>0){
            return $res;
        }
        return 0;


    }

     public function getSelectedCurrency($countryId){

        $q = $this->createQuery('cc')
        ->select('cc.id')
        ->where("cc.country_id = ?",$countryId);
        $res =   $q->execute();
        if($res->count()>0)
            {
                $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
                return $res[0]['id'];
            }

        else
        return sfConfig::get('app_default_currency_id');
    }
          
      /*
       * @author Ramandeep
       * @param : CurrencyId
       * @description Function will return all values related to id in CurrencyCode Table
       */
      public function getCurrencyById($currencyId) {
          $q = Doctrine_Query::create()
                  ->select('a.*')
                  ->from('CurrencyCode a')
                  ->where('id=?', $currencyId);
          return $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      }
}