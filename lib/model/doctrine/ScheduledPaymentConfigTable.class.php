<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ScheduledPaymentConfigTable extends Doctrine_Table
{
  public function getPaymentServiceStatus($merchantId,$merchantServiceId,$paymentId,$paymentServiceId){
    $q = $this->createQuery()
    ->select('*')
    ->from('ScheduledPaymentConfig s')
    ->where('s.merchant_id = ?',$merchantId)
    ->andWhere('s.merchant_service_id =?', $merchantServiceId )
    ->andWhere('s.payment_mode_id <= ?',$paymentId)
    ->andWhere('s.payment_mode_option_id =?', $paymentServiceId );
    //        ->andWhere('s.start_date <= ?',$currentDate)
    //        ->andWhere('s.end_date IS NULL OR s.end_date >? ', array($currentDate ));

    $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

    return $res;
  }
  public function getPaymentModeStatus($merchantId,$merchantServiceId){
    $currentDate = date( 'Y-m-d H:i:s', time());
    $q = $this->createQuery()
    ->select('*')
    ->from('ScheduledPaymentConfig s')
    ->where('s.merchant_id = ?',$merchantId)
    ->andWhere('s.merchant_service_id =?', $merchantServiceId )
    ->andWhere('s.start_date <= ?',$currentDate)
    ->andWhere('s.end_date IS NULL OR s.end_date >=? ', array($currentDate ));

    $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

    return $res;
  }

  public function saveTransaction($paymentId,$paymentModeId,$merchentId,$merchentServiceId,$sDate,$eDate){

    $this->deleteExistingRecord($paymentId,$paymentModeId,$merchentId,$merchentServiceId);
    $transactionObj = new ScheduledPaymentConfig();
    $transactionObj->setPaymentModeId(trim($paymentId));
    if($paymentModeId != ""){
      $transactionObj->setPaymentModeOptionId(trim($paymentModeId));
    }
    $transactionObj->setMerchantId(trim($merchentId));
    if($merchentServiceId != ""){
      $transactionObj->setMerchantServiceId(trim($merchentServiceId));
    }
    $transactionObj->setStartDate($sDate);
    if($eDate != ""){
      $transactionObj->setEndDate($eDate);
    }
    $transactionObj->save();

    if($transactionObj->getId()){
      return $transactionObj->getId();
    }else {
      return false;
    }

  }

  function deleteExistingRecord($paymentId,$paymentModeId,$merchentId,$merchentServiceId){
    $q = Doctrine_Query::create()
    ->delete('ScheduledPaymentConfig spc')
    ->Where('spc.payment_mode_id =?',$paymentId);
    if($paymentModeId != ""){
      $q = $q->andWhere('spc.payment_mode_option_id =?',$paymentModeId);
    }
    if($merchentId != ""){
      $q = $q->andWhere('spc.merchant_id =?',$merchentId);
    }
    if($merchentServiceId != ""){
      $q = $q->andWhere('spc.merchant_service_id  =?',$merchentServiceId);
    }
    $q = $q->execute();
  }

}