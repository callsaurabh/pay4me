<?php


class Pay4meSplitTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('Pay4meSplit');
    }    
     public function getRevenueSplitAccounts($merchantReqId) {
        $q =  $this->createQuery('w')
        ->select('w.*')
        ->where('merchant_request_id=?',$merchantReqId);
        $res = $q->execute();
        if($res->count()) {
            return $res;
        }
        return  false;
    }
    
   
}