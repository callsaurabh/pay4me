<?php
class KycDetailsTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('KycDetails');
    }
    
    /*
     * function : saveKycDetails()
     * purpose : save kyc details
     * param :$userId,$status=0,$reason=''
     * WP051
     */
    public function saveKycDetails($userId,$status=0,$reason=''){
        $objKycStatus = new KycDetails();
        $objKycStatus->setUserId($userId);
        $objKycStatus->setKycStatus($status);
        if(!empty($reason))
        $objKycStatus->setReason($reason);
        $objKycStatus->save();
        
    }
    /*
     * function : getDisapproveReason()
     * purpose : get disapproveReason
     * param :$user_id,$reason
     * WP051
     */
    public function getDisapproveReason($user_id,$reason=3){
       $queryResult = Doctrine_Query::create()
        ->select('k.id,k.reason')
        ->from('KycDetails k')
        ->where('k.user_id  = ?', $user_id)
        ->andwhere('k.kyc_status = ?', $reason)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        return  $queryResult;
    }
}