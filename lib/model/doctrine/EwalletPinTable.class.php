<?php


class EwalletPinTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('EwalletPin');
    }
    function setPin($pin,$userId)
    {
        if(isset($pin) && isset($userId) && !empty($pin) && !empty($userId) && is_numeric($userId) && $userId>0)
        {
            $pinObj = $this->checkPinExist($userId);
            if(!$pinObj) {
                $ob = new EwalletPin();
                $ob->set('status', 'inactive');
            }
            else {
                $id = $pinObj->getFirst()->getId();
                $ob = Doctrine::getTable('EwalletPin')->find($id);
            }
            $ob->set('user_id', $userId);
            $ob->set('pin_number', $pin);
            $ob->set('no_of_retries', '0');
            
//            print "<pre>";
//            print_r($ob);
            $ob->save();
        }else
        {
            return false;
        }

    }

    function ResetPin($userId)
    {
        try {
            $zero = '0';
            $query=Doctrine_Query::create()
            ->update('EwalletPin')
            ->set('no_of_retries','?',$zero)
            ->where('user_id=?',$userId)
            ->execute();
            return true;
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
    }
    function getInActiveEmailList()
    {
        $query=Doctrine_Query::create()
        ->select('ep.user_id')
        ->from('EwalletPin ep')
        ->where("ep.status='inactive'")
        ->execute()->toArray();
        $userList='';
        foreach($query as $user)
        {
            $userList .= $user['user_id'].',';
        }
        $userIdArray=explode(",",substr($userList,0,strlen($userList)-1));
        $userDeatil=Doctrine_Query::create()
        ->select('u.email')
        ->from('UserDetail u')
        ->whereIn('u.user_id',$userIdArray)
        ->execute()->toArray();
        $emailList='';
        foreach($userDeatil as $email)
        {
            $emailList .= $email['email'].',';
        }
        $emailList=substr($emailList,0,strlen($emailList)-1);

        return $emailList;

    }

     function checkPinExist($userId)
    {

         $query=Doctrine_Query::create()
        ->select('ep.user_id')
        ->from('EwalletPin ep')
        ->where('ep.user_id=?',$userId);

        
        
        $result = $query->execute();

        if($result->count()) {
            return $result;
        }
        return 0;
    }

}