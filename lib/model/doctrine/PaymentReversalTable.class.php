<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PaymentReversalTable extends Doctrine_Table
{
  public function isAlreadyReverse($validation_no){
    $q = $this->createQuery('e')
    ->select('e.*')
    ->where('e.payment_validation=?',$validation_no)
    ->orWhere('e.rversal_validation =?',$validation_no);
    $set = $q->execute();
    if ($set->count() == 0) return 0;
    return $set;
  }

  public function saveReversalValidationNumber($validation_no, $reverse_validation_number){
    $paymentReversal = new PaymentReversal();
    $paymentReversal->setPaymentValidation($validation_no);
    $paymentReversal->setRversalValidation($reverse_validation_number);
    $paymentReversal->save();
    return $paymentReversal;
  }
}