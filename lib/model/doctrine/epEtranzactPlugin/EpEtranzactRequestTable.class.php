<?php


class EpEtranzactRequestTable extends PluginEpEtranzactRequestTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpEtranzactRequest');
    }

    public function isFirstRequest($transactionId)
    {
        try{
          $q = Doctrine_Query::create()
          ->select('count(*)')
          ->from('EpEtranzactRequest')
          ->where('transaction_id = ?',$transactionId);

          return  $q->count();
        }catch(Exception $e) {
          echo("Problem found". $e->getMessage());die;
        }
    }

    public function getUserDetailsForMail($orderId)
    {
        $q = Doctrine_Query::create()
        ->select('e.*,u.*,d.*')
        ->from('EpEtranzactRequest e')
        ->leftJoin('e.sfGuardUser u')
        ->leftJoin('u.UserDetail d')
        ->where('e.transaction_id = ?', $orderId);

        $result = $q->execute();

        if($result->count()){
          return $result;
        }
        return 0;
    }
}