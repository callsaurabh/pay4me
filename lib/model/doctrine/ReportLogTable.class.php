<?php

/**
 * ReportLogTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ReportLogTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object ReportLogTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ReportLog');
    }
     /**
     * function will save the report parameters
     *
     * @access public
     * @param userId,report_name,file_name,frequency,banks,branch,country
     * @param $bankId,$bankBranchId,$countryId will hold band,branch and country ids respectively
     * @return Doesn't return any thing
     * @author Ramandeep Singh
     */
    public function saveReport($data){
    	$obj = new ReportLog();
        if(isset($data['user_id'])){
            $obj->setUserID($data['user_id']);
        }            
        if(isset($data['report_name'])){
            $obj->setReportName($data['report_name']);
        }            
        if(isset($data['file_name'])){
            $obj->setFileName($data['file_name']);
        }            
        if(isset($data['frequency'])){
            $obj->setfrequency($data['frequency']);
        }            
        if(isset($data['from_date'])){
            $obj->setFromDate($data['from_date']);
        }            
        if(isset($data['to_date'])){
            $obj->setToDate($data['to_date']);
        }            
        if(isset($data['ep_job_id'])){
            $obj->setEpJobId($data['ep_job_id']);
        }
        if(isset($data['banks'])){
            $obj->setBankId($data['banks']);
        }
        if(isset($data['branch'])){
            $obj->setBankBranchId($data['branch']); 
        }
        if(isset($data['country'])){
            $obj->setCountryId($data['country']);
        }
        if(count($data)){
            $obj->setParameters(json_encode($data) );
        }               
    	$obj->save();
    	return $obj;
    }
    /**
     * function will fetch report according to user access
     *
     * @access public
     * @param $userId, countryId Futre Prospective
     * @param $bankId,$bankBranchId,$countryId will hold band,branch and country ids respectively
     * @param frequency is COMPULSORY
     * @return Doesn't return any thing
     * @author Ramandeep Singh
     */
    public function fetchReportName($params){
    	 
        if (isset($params['banks'])) {
            $q = Doctrine_Query::create()
	    	->select('r.*,b.bank_name')
	    	->from('ReportLog r')
                ->leftJoin('r.Bank b')
                ->where('r.frequency = ?', $params['frequency']);
            if($params['banks'] != ""){
                $q->andWhere('r.bank_id = ?',$params['banks']);
            }            
        }else{
            $q = Doctrine_Query::create()
	    	->select('r.*')
	    	->from('ReportLog r')
                ->where('r.frequency = ?', $params['frequency']);
        }
        
        if (isset($params['type'])) {
            if ($params['type']=='Generated') {
                $q->andWhere('r.deleted_at IS NULL');
                $q->andWhere('r.file_name IS NOT NULL');
            }

            if ($params['type']=='Archive') {
                $q->andWhere('r.deleted_at IS NOT NULL');
                $q->andWhere('r.file_name IS NOT NULL');
            }

            if ($params['type']=='Pending') {
                $q->andWhere('r.deleted_at IS NULL');
                $q->andWhere('r.file_name IS NULL');
            }
        }
        
        
        if (isset($params['userBankId'])) {
            $q->andWhere('r.bank_id = ?', $params['userBankId']);
        }
        if (isset($params['report_name'])) {
            $q->andWhere('r.report_name = ?', $params['report_name']);
        }
        if($params['frequency']=='QUARTERLY' || $params['frequency']=='MONTHLY'){
            $q->andWhere('r.from_date = ?', $params['from_date'])
                ->andWhere('r.to_date = ?', $params['to_date']);
        }
        if($params['frequency']=='WEEKLY'){
            $q->andWhere('r.from_date >= ?', $params['from_date'])
                ->andWhere('r.to_date <= ?', $params['to_date']);
        }
        if (isset($params['user_id'])) {
            $q->andWhere('r.user_id = ?', $params['user_id']);
        }
        /*
         * In custom we are taking that report start date should lie in specific year
         */
        if($params['frequency']=='CUSTOM'){
            $q->andWhere('r.from_date >= ?', $params['from_date'])
              ->andWhere('r.from_date <= ?', $params['to_date']);
        }
        $q->orderBy('r.updated_at desc');
//        echo $params['frequency']."----".$params['from_date']."------".$params['to_date']."---".$params["user_id"];
//        print $q->getSqlQuery(); die;
        $result = $q->fetchArray();
//        foreach ($result as $key => $val){
//            $result[$key]['parameters'] = json_encode($this->setcorrespondingName(json_decode($val['parameters'],true)));
//        }
//        return $result;
        return $q;
//        return $q->getSqlQuery();
    }
    
    /*
     * function will add deleted date against report
     * as a result next time report will not be visible to user
     * 
     * param $ids a array containing report's ids
     * 
     * @author Ramandeep
     */
    public function deleteReportById($ids){
        $q= Doctrine_Query::create()
            ->delete('ReportLog')
            ->whereIn('id', $ids)
            ->execute();
    }
    
    /*
     * function will update record
     * 
     * Array First param value to be updated
     * Array Second param where condition
     * 
     * @author Ramandeep
     */
    public function updateReport($columnValue,$where){
        $q = Doctrine_Query::create()
            ->update('ReportLog');
        foreach($columnValue as $key => $value){
            $q->set($key, "'$value'");
        }
        $q->set("updated_at=?", "'". date("Y-m-d H:i:s")."'" );
        foreach($where as $key => $value){
            $q->where($key."=?", $value);
        }
        
        $q->execute(array(), Doctrine::HYDRATE_ARRAY);
    }
    
    /*
     * function will fetch record
     * 
     * string First param column name to be fetched concatinated with ','
     * Array Second param where condition
     * 
     * @author Ramandeep
     */
    public function fetchReport($columnsName,$where){
        $q = Doctrine_Query::create()
	    	->select($columnsName)
	    	->from('ReportLog');
        foreach($where as $key => $value){
            $q->andWhere($key."=?", $value);
        }
//        echo ($q->getSqlQuery());
        return $q->fetchArray();
    }
    
    public function removeJob($jobIds,$userId){
        $con = Doctrine_Manager::connection();
        foreach($jobIds as $key=>$val){
            $stmt = $con->prepare('CALL removeJob(\''. $val .'\', \''. $userId .'\');'); 
            $ret = $stmt->execute(array(), Doctrine::HYDRATE_ARRAY);
        }
    }
    
    public function setcorrespondingName($data){
        if(isset($data['report_name'])){
            if($data['report_name'] == "EwalletRechargeReport"){
                if(isset($data['banks'])){
                    if($data['banks'] != "" && $data['banks'] != NULL){
                        $bankResult = Doctrine::getTable("Bank")->getBankName($data['banks']);
                        if(isset($bankResult) && $bankResult != NULL){
                            $data['banks'] = $bankResult["bank_name"];
                        }            
                    }else{
                        $data['banks'] = "All";
                    }
                }
                if(isset($data['branch'])){
                    if($data['branch'] != "" && $data['branch'] != NULL){
                        $branchResult = Doctrine::getTable("BankBranch")->getBankBranchName($data['branch']);
                        if(isset($branchResult) && $branchResult != NULL){
                            $data['branch'] = $branchResult["name"];
                        }
                    }else{
                        $data['branch'] = "All";
                    }
                }
                if(isset($data['country'])){
                    if($data['country'] != "" && $data['country'] != NULL){
                        $countryResult = Doctrine::getTable("Country")->getCountryById($data['country']);
                        if(isset($countryResult) && $countryResult != NULL){
                            $data['country'] = $countryResult[0]["name"];
                        }
                    }else{
                        $data['country'] = "All";
                    }
                }
                if(isset($data['payment_mode'])){
                    if($data['payment_mode'] != "" && $data['payment_mode'] != NULL){
                        $paymentModeResult = Doctrine::getTable("PaymentModeOption")->getPaymentModeName($data['payment_mode']);    
                        if(isset($paymentModeResult) && $paymentModeResult != NULL){
                            $data['payment_mode'] = $paymentModeResult["name"];
                        }
                    }else{
                        $data['payment_mode'] = "All";
                    }
                }                        
            }
        }            	
        if(isset($data['currency'])){
            if($data['currency'] != "" && $data['currency'] != NULL){
                $currencyResult = Doctrine::getTable("CurrencyCode")->getCurrencyById($data['currency']);
                if(isset($currencyResult) && $currencyResult != NULL){
                    $data['currency'] = $currencyResult[0]["currency"];
                }
            }
        }
        return $data;
    }
    
    public function verifyDublicateReportRequest($values){
        
        $values['from_date'] = $values['from_date'].' 00:00:00';
        $values['to_date'] = $values['to_date'].' 23:59:59';
        $where = array(
            'from_date' => $values['from_date'],
            'to_date' => $values['to_date'],
            'user_id' => $values['user_id']
        );
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
        $result = $this->fetchReport('id,parameters,file_name,deleted_at', $where);
        Doctrine_Manager::getInstance()->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);
        
        foreach ($result as $key=>$val){
            $difference = array();
            $difference = array_diff($values,json_decode($val['parameters'],true));
            /*
             * == 1 because other parameters are same as file_name is extra in post values
             * so differecnce will be of one value only
             */
            if(count($difference) == 1){
                if($val["file_name"] == NULL){
                    if($val["deleted_at"] == NULL){
                        return $result[$key]['id'];
                    }                    
                }else{
                    return $result[$key]['id'];
                }                
            }
        }
        return false;
    }
}