<?php


class MerchantRequestPaymentModeTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('MerchantRequestPaymentMode');
    }

    public function getPaymentOptions($payment_detail_id){
        $q = $this->createQuery('s')
        ->select('*')
        ->where('s.merchant_request_payment_id=?', $payment_detail_id);

        $result = $q->execute();
        if($result->count()) {
            return $result;
        }
        return 0;
    }

    public function checkMerhantRequestPaymentModes($merchantRequestId){
        $q = $this->createQuery('s')
        ->select('*')
        ->where('s.merchant_request_id=?', $merchantRequestId);

        $result = $q->execute();
        if($result->count()) {
            return true;
        }
        return false;
    }


    public function getPaymentMode($merchantRequestId){
        $q = Doctrine_Query::create()
        ->select('mrp.*')
        ->from('MerchantRequestPaymentMode mrp')
        ->where('mrp.merchant_request_id=?', $merchantRequestId);
//     

        $res = $q->execute();
        
        if($res->count()) {
            return $res;
        }
        return 0;
    }

}