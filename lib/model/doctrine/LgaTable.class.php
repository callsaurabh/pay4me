<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class LgaTable extends Doctrine_Table
{
  public function getLgaList($state_id)
   {
       $q = Doctrine_Query::create()
         //->select('b.name as branch_name,bank.id as bankId, bank.name as bank_name,b.sortcode as sortcode,b.address as branch_address')
         ->select('s.id as id,m.id as state_id,s.name')
         ->from('Lga s')
         ->leftJoin("s.State m");
         if($state_id != "")
         {
           $q->andWhere("s.state_id = ".$state_id);
         }
         return $q->execute(array(),Doctrine::HYDRATE_ARRAY);

   }


  public function getLgaRelatedStates($lgaIdArray)
  {
    try {
       $res = array();

       if($lgaIdArray){
        $q =  Doctrine_Query::create()
              ->select('l.id, l.state_id as state_id, s.id,s.name as state_name')
              ->from('Lga l')
              ->leftJoin("l.State s")
              ->whereIn('l.id',$lgaIdArray)
              ->orderBy('l.state_id');

          $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

       }
         return $res;

    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }

   }

    public function getLgaName($lga_id)
   {
       $res = array();
       $q = Doctrine_Query::create()
          ->select('l.*')
          ->from('Lga l')
          ->where("id=?",$lga_id);

       $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

    if(count($res)>0){
      return $res[0];
    }
    return false ;

   }

   //to get lga id for lga name
    public function getLgaDetails($lga_name)
   {
       $q = Doctrine_Query::create()
          ->select('l.id')
          ->from('Lga l')
          ->where("name=?",$lga_name)
          ->fetchOne();

    return $q ;

   }

}