<?php


class EpInterswitchRequestTable extends PluginEpInterswitchRequestTable
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('EpInterswitchRequest');
  }

  public function isFirstRequest($trnxId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('count(*)')
      ->from('EpInterswitchRequest')
      ->where('trnx_id = ?',$trnxId);

      return  $q->count();
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
  public function postInsert($event) {
      echo "postInsert";

         }
         public function postUpdate($event) {
      echo "postUpdate";

         }
  public function getUserDetailsForMail($orderId)
  {
    $q = Doctrine_Query::create()
    ->select('v.*,u.*,d.*')
    ->from('EpInterswitchRequest v')
    ->leftJoin('v.sfGuardUser u')
    ->leftJoin('u.UserDetail d')
    ->where('v.trnx_id = ?', $orderId);

    $result = $q->execute();

    if($result->count()){
      return $result;
    }
    return 0;
  }
}