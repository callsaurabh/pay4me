<?php


class UserAccountCollectionTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('UserAccountCollection');
    }

    public function getAccounts($userId, $currency_id=NULL) {
        $q = $this->createQuery()
            ->select('*')
            ->where('user_id=?',$userId);
            if($currency_id) {
              $q->andWhere('currency_id=?',$currency_id);
            }

         $res = $q->execute();
         if($res->count()>0) {
           return $res;
         }
         return 0;

    }

/**
* Functions :  getNonEwalletAccount()
* @param :  void
* @return Object/Bool
*/
  public function getNonEwalletAccount()
  {
      $q = $this->createQuery('s')
            ->select('s.*, e.id AS ep_master_account_id')
            ->leftJoin('s.EpMasterAccount e')
            ->where('e.type != ?','ewallet');

      if($q->count()==0)
        return false;
      else
      {
          $res  = $q->execute();
          return $res ;
      }

  }
}