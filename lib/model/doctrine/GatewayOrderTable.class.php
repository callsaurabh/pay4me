<?php


class   GatewayOrderTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('GatewayOrder');
  }
  public function getDistinctPaymentMode()
  {
    try{
      $q =$this->createQuery('g')
      ->select('distinct(payment_mode_option_id) as pmo_id')
      ->where('payment_mode_option_id IS NOT NULL')
      ->andWhere('payment_mode_option_id != ?',16);
      $res = $q->execute();
      return  $res;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function saveOrder($app_id, $type, $paymentModeOption, $userId, $currencyId=1, $totalAmount=0,$serviceCharges=0)
  {
    $transaction_date = date("Y-m-d h:i:s");
    $order_id = $this->generateOrderId();
    $gatewayOrder = new GatewayOrder();
    $gatewayOrder->setAppId($app_id);
    $gatewayOrder->setOrderId($order_id);
    $gatewayOrder->setPaymentModeOptionId($paymentModeOption);
    if($paymentModeOption == 2) {
      $gatewayOrder->setStatus('Pending');
    }
    $gatewayOrder->setAmount($totalAmount);
    $gatewayOrder->setTransactionDate($transaction_date);
    $gatewayOrder->setType($type);
    $gatewayOrder->setUserId($userId);
    $gatewayOrder->setCurrencyId($currencyId);
//    echo "<pre>";
//    print_r($serviceCharges);
//    exit;
    $gatewayOrder->setServicechargeKobo($serviceCharges);
    $gatewayOrder->save();
    
    return $gatewayOrder->getOrderId();
  }

  private function generateOrderId()
  {
    do{
      $orderId = time().mt_rand(10000,99999);
      $checkOrderId = $this->checkDuplicacy($orderId);
    }while($checkOrderId > 0);
    return $orderId;
  }

  private function checkDuplicacy($orderId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('count(*)')
      ->from('GatewayOrder')
      ->where('order_id = ?',$orderId);

      return  $q->count();
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
  public function getRecordByOrderId($orderId,$paymodeOption='',$status='')
  {
    try{
      $q = Doctrine_Query::create()
      ->select('*')
      ->from('GatewayOrder')
      ->where('order_id = ?',$orderId);
      if($paymodeOption!='')
        $q->andWhere('payment_mode_option_id = ?',$paymodeOption);
      if($status!='')
        $q->andWhere('status = ?',$status);
      
     if($q->count()>0)
         {
            $res = $q->execute();
            return $res->getfirst();
        }
      return  false;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getRecordByAppId($appArr=array(),$paymodeOption='',$status='',$NotOrderId='')
  {
    try{
      $q = Doctrine_Query::create()
      ->select('*')
      ->from('GatewayOrder');
      if(count($appArr)>0)
        $q->whereIn('app_id',$appArr);
      if($paymodeOption!='')
       $q->andWhere('payment_mode_option_id = ?',$paymodeOption);
      if($status!='')
        $q->andWhere('status = ?',$status);
      if($NotOrderId!='')
         $q->andWhere('order_id = ?',$NotOrderId);

          $q->orderBy('id DESC') ;
     if($q->count()>0)
         {
            $res = $q->execute();
            return $res;
        }
      return  false;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
  public function getRecordByOrderIdAndStatus($paymodeOption,$status,$from_trans_date='')
  {
    try{
      $q = Doctrine_Query::create()
      ->select('*')
      ->from('GatewayOrder')
       ->where('payment_mode_option_id = ?',$paymodeOption)
       ->andWhere('status = ?',$status);
      if($from_trans_date!='')
        $q->andWhere('updated_at > ?',$from_trans_date);    
      $res = $q->execute();
      return  $res;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
  public function isFirstRequest($app_id, $paymentModeOption)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('*')
      ->from('GatewayOrder')
      ->where('app_id = ?', $app_id)
      ->andWhere('payment_mode_option_id = ?', $paymentModeOption);

      $result = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
      $returnArr = array();
      if(count($result) > 0)
      {
        $returnArr['order_id'] = $result[0]['order_id'];
        $returnArr['isFirst'] = 'no';
      }else {
        $returnArr['isFirst'] = 'yes';
      }
      return $returnArr;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
  public function getByOrderId($order_id){
    $q = Doctrine_Query::create()
    ->select('id, app_id, user_id, payment_mode_option_id, type, status, servicecharge_kobo,validation_number')
    ->from('GatewayOrder')
    ->where('order_id = ?', $order_id)
    ->execute(array(),Doctrine::HYDRATE_ARRAY);

    if(count($q) > 0)
    {
      return $q[0];
    }else {
      return 0;
    }
  }

  public function updateGatewayOrder($updateParamArr)
  {

    try {
      if($updateParamArr['order_id']!=""){
          $q = Doctrine_Query::create();
          $q->update('GatewayOrder');
          $q->set('status', '?', $updateParamArr['status']);
          //$q->set('amount', $updateParamArr['amount']);
          $q->set('response_code','?', $updateParamArr['code']);
          $q->set('response_txt', '?', $updateParamArr['desc']);
          if(!empty($updateParamArr['pan']))
          $q->set('pan', '?', $updateParamArr['pan']);
          $q->set('transaction_date', '?', $updateParamArr['date']);
          if(!empty($updateParamArr['approvalcode']))
          $q->set('approvalcode', '?', $updateParamArr['approvalcode']);
          $q->where("order_id=?", $updateParamArr['order_id']);
          $q->execute();
//          echo "<pre>";
//          print_r('kljhjkhjk');
//          exit;
          return true;
      } else {          
          return false;
      }    
    }catch(Exception $e){
      echo("Problem found". $e->getMessage());die;
    }
  }

  protected function convertDate($msgdate)
  {
    list ($date, $time) = explode(' ', $msgdate);
    list ($dd, $mm, $yyyy) = explode('/', $date);
    $newDate = $yyyy.'-'.$mm.'-'.$dd.' '.$time;
    $date4Db = date('Y-m-d H:i:s',strtotime($newDate));

    return $date4Db;
  }
  public function getOrderStatus($PaymentMode='')
  {
    $visa = Doctrine_Query::create()
    ->select('distinct(go.status) as status')
    ->from('GatewayOrder go');
    if(''!=$PaymentMode)
    $visa->where("payment_mode_option_id=?", $PaymentMode);


    return $visa->execute();

  }

  public function VisaStatement($from_date="",$to_date="",$status="",$user_id="",$currency,$username="",$PaymentMode='',$PaymentType='',$transactionNo='')
  {
    $visa = Doctrine_Query::create()
    ->select('go.amount as purchased_amount, gu.username as username, go.response_txt as order_desc, max(go.updated_at) as transtime, go.status as order_status,go.currency_id as  currencyid,go.app_id as app_id ,go.id, go.updated_at')
    ->from('GatewayOrder go')
    ->innerJoin('go.VwGoStatus a')
    ->leftJoin('go.sfGuardUser gu')
    ->where('a.id=go.id');
    if($PaymentMode!="")
    $visa->andWhere('go.payment_mode_option_id =? ',$PaymentMode);
    if($PaymentType!="")
    $visa->andWhere('go.type =?',$PaymentType);

    if($from_date!="")
    $visa->andWhere('go.updated_at >=?',$from_date);


    if($to_date!=""){
      $visa->andwhere('go.updated_at <=?',$to_date);
    }
    if($status!=""){
        $visa->andWhere('go.status =?',$status);
    }
/*    if($status =="success"){

    }else if($status == "failure"){
      $visa->andWhere('go.status IS NOT NULL');
    }*/

    if($user_id!=""){
      $visa->andWhere('go.user_id =?',$user_id);
    }

    if($username!=""){
      $visa->andWhere("gu.username LIKE '"."%".mysql_escape_string($username)."%'");

    }
    if($currency != ""){
      $visa->andWhere("go.currency_id =?",$currency);
    }
    if ($transactionNo != "") {
      $visa->andWhere("go.app_id =?",$transactionNo);
    }

    $visa->groupBy('go.app_id');
    if($status != "success" && $status!=''){
      $visa->having("group_concat(go.status) not like '%Success%'");
    }
    $visa->orderBy('go.updated_at DESC');
     
    return $visa;
  }


  public function InternetBankStatement($from_date="",$to_date="",$status="",$currency,$PaymentMode='',$PaymentType='',$transactionNo='')
  {
    $visa = Doctrine_Query::create()
    ->select('go.amount as purchased_amount, gu.username as username, go.response_txt as order_desc, max(go.updated_at) as transtime, go.status as order_status,go.currency_id as  currencyid,go.app_id as app_id ,go.id, go.updated_at')
    ->from('GatewayOrder go')
    ->innerJoin('go.VwGoStatus a')
    ->leftJoin('go.sfGuardUser gu')
    ->where('a.id=go.id');
    if($PaymentMode!="")
    $visa->andWhere('go.payment_mode_option_id =? ',$PaymentMode);
    if($PaymentType!="")
    $visa->andWhere('go.type =?',$PaymentType);

    if($from_date!="")
    $visa->andWhere('go.updated_at >=?',$from_date);


    if($to_date!=""){
      $visa->andwhere('go.updated_at <=?',$to_date);
    }
    if($status!=""){
        $visa->andWhere('go.status =?',$status);
    }
/*    if($status =="success"){

    }else if($status == "failure"){
      $visa->andWhere('go.status IS NOT NULL');
    }*/

    if($currency != ""){
      $visa->andWhere("go.currency_id =?",$currency);
    }
    if ($transactionNo != "") {
      $visa->andWhere("go.app_id =?",$transactionNo);
    }

    $visa->groupBy('go.app_id');
    if($status != "success" && $status!=''){
      $visa->having("group_concat(go.status) not like '%Success%'");
    }
    $visa->orderBy('go.updated_at DESC');

    return $visa;
  }
  public function getCsvVisaStatement($from_date="",$to_date="",$status="",$currency='',$username="", $PaymentMode='',$PaymentType='',$transactionNo=''){

    $dbArr =DBInfo::getDSNInfo();
    mysql_connect($dbArr['host'],$dbArr['username'],$dbArr['password']) or die(mysql_error());
    mysql_select_db($dbArr['dbname']) or die(mysql_error());


   $sql = "SELECT go.amount as purchased_amount, gu.username as username,
        go.response_txt as order_desc, go.updated_at as transtime,
        go.status as order_status,go.currency_id as currencyid,
        go.app_id as app_id ,go.payment_mode_option_id,go.type,go.transaction_date,go.status,go.user_id,gu.username,go.currency_id,
        go.pan as pan, go.approvalcode as approvalcode,
        go.id FROM gateway_order go left join sf_guard_user gu on
        go.user_id = gu.id
        inner join
        vw_go_status a on
        a.id = go.id
        where 1 ";
    if($PaymentMode!="")
    $sql .=" and go.payment_mode_option_id =".$PaymentMode;
    if($PaymentType!="")
    $sql .=" and go.type ='".$PaymentType."'";

    if($from_date!=""){
      $sql .=" and go.updated_at >="."'$from_date'";
    }

    if($to_date!=""){
      $sql .=" and go.updated_at <="."'$to_date'";
    }
    if($currency!=""){
      $sql .=" and go.currency_id ='".$currency."'";
    }
    if ($transactionNo != "") {
      $sql .=" and go.app_id ='".$transactionNo."'";
    }
    if($status!="")
    $sql .=" and go.status ='".$status."'";

    if($username!=""){
      $sql .=" and gu.username LIKE '"."%".mysql_escape_string($username)."%'";
    }
    $sql.=" GROUP BY go.app_id ";
    if($status != "success" && $status!=""){
      $sql.="having GROUP_CONCAT(go.status) NOT LIKE '%Success%'";
    }
    $sql.=" ORDER BY go.updated_at desc";

   
    return  $qry = mysql_query($sql);
  }
  public function VisaRechargeStatement($from_date="",$to_date="",$status="",$user_id="",$currency,$username="",$PaymentMode='',$PaymentType='',$accountNo='')
  {
    $visa = Doctrine_Query::create()
    ->select('go.amount as purchased_amount, gu.username as username, go.response_txt as order_desc, go.updated_at  as transtime, go.status as order_status,go.app_id as app_id, go.currency_id as currencyid, go.order_id as order_id, go.id, go.pan, go.approvalcode')
    ->from('GatewayOrder go')
    ->leftJoin('go.sfGuardUser gu')
    ->leftJoin('gu.UserAccountCollection uc')
    ->leftJoin('uc.EpMasterAccount ma');

    if($PaymentMode!="")
    $visa->andWhere('go.payment_mode_option_id =? ',$PaymentMode);
    if($PaymentType!="")
    $visa->andWhere('go.type =?',$PaymentType);

    if($from_date!="")
    $visa->andWhere('go.updated_at >=?',$from_date);


    if($to_date!=""){
      $visa->andwhere('go.updated_at <=?',$to_date);
    }

    if($status !=""){
      $visa->andWhere('go.status =?',$status)
      ->andWhere('go.status IS NOT NULL');
    }

    if($user_id!=""){
      $visa->andWhere('go.user_id =?',$user_id);
    }

    if($username!=""){
      $visa->andWhere("gu.username LIKE '"."%".mysql_escape_string($username)."%'");

    }
    if($currency != ""){
      $visa->andWhere("go.currency_id =?",$currency)
            ->andWhere("uc.currency_id =?",$currency);
    }
    if ($accountNo != "") {
      $visa->andWhere("ma.account_number =?",$accountNo);
    }

    $visa->orderBy('go.updated_at desc');
    
    return $visa;
  }

  public function getCsvRechargeStatement($from_date="",$to_date="",$status="",$currency='',$username="", $PaymentMode='',$PaymentType='',$accountNo=''){
    $dbArr =DBInfo::getDSNInfo();
    mysql_connect($dbArr['host'],$dbArr['username'],$dbArr['password']) or die(mysql_error());
    mysql_select_db($dbArr['dbname']) or die(mysql_error());
    $sql = "select 
                 go.id,go.response_txt as order_desc, go.updated_at as transtime, 
                 go.amount as purchased_amount, go.status as order_status,
                 go.app_id as app_id ,go.order_id,go.validation_number,
                 gu.username,go.type,go.pan, go.approvalcode,go.servicecharge_kobo,
                 go.amount - go.servicecharge_kobo as actual_amount,
                 e.account_number,e.account_name
             from 
                 gateway_order go 
            left join 
                 sf_guard_user gu on gu.id = go.user_id
            LEFT JOIN 
                 user_account_collection u ON gu.id = u.user_id AND (u.deleted_at IS NULL) 
            LEFT JOIN 
                 ep_master_account e ON u.master_account_id = e.id where 1  ";
    if($PaymentMode!="")
    $sql .=" and go.payment_mode_option_id =".$PaymentMode;
    if($PaymentType!="")
    $sql .=" and go.type ='".$PaymentType."'";

    if($from_date!=""){
      $sql .=" and go.updated_at >="."'$from_date'";
    }

    if($to_date!=""){
      $sql .=" and go.updated_at <="."'$to_date'";
    }
    if($currency!=""){
      $sql .=" and go.currency_id ='".$currency."'";
      $sql .=" and u.currency_id ='".$currency."'";
    }
     if ($accountNo != "") {
        $sql .=" and e.account_number ='".$accountNo."'";
    }
    if($status !=""){
      $sql .=" and go.status ="."'$status'";
      $sql .=" and go.status IS NOT NULL";
    }

    if($username!=""){
      $sql .=" and gu.username LIKE '"."%".mysql_escape_string($username)."%'";
    }

    $sql.=" ORDER BY go.updated_at desc";
    
    return  $qry = mysql_query($sql);
  }
  public function getLock($order_id)
  {
    try {
      $q = Doctrine_Query::create()
      ->update('GatewayOrder')
      ->set('locked', '?', 'locked')
      ->where("order_id=?", $order_id)
      ->execute();
      return $q;
    }catch(Exception $e){
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function viewDetail($type, $txnId, $status = '')
  {//print $status;print $txnId;
    $q = Doctrine_Query::create()
    ->select('id, app_id, validation_number,order_id, type, status, transaction_date, gu.username as username, amount, response_txt, response_code','pan','approvalcode')
    ->from('GatewayOrder go')
    ->leftJoin('go.sfGuardUser gu');
    if($type == 'recharge')
    $q->where('order_id = ?', $txnId);
    else
    $q->where('app_id = ?', $txnId);
   
    if($status!= '')
    $q->andwhere('status = ?', $status);

      $q->orderBy('go.updated_at DESC');
   // $q->andWhere('transaction_date IS NOT NULL');

//print $q->getSqlQuery();exit;
    return $q;
  }
  public function getMasterAccountId($txnNumber)
  {
    $q = Doctrine_Query::create()
    ->select('id, user_id, gu.id as guid, gud.master_account_id as master_id')
    ->from('GatewayOrder go')
    ->leftJoin('go.sfGuardUser gu')
    ->leftJoin('gu.UserDetail gud')
    ->where('app_id = ?', $txnNumber)
    ->fetchArray();
    
    return $q[0]['master_id'];
    
  }

  public function getStatus($order_id)
  {
    $q = Doctrine_Query::create()
    ->select('status')
    ->from('GatewayOrder')
    ->where('order_id = ?', $order_id)
    ->execute()->toArray();

    if(count($q) > 0)
    {
      return $q[0]['status'];
    }else {
      return '';
    }
  }

  public function getOrderDetails($app_id, $payment_status='') {
    $q = Doctrine_Query::create()
    ->select('*')
    ->from('GatewayOrder')
    ->where('app_id=?',$app_id);
    if($payment_status!='') {
      $q->andWhere('status = "'.$payment_status.'"');
    }
    $q->orderBy("id DESC limit 0,1");
    $res = $q->execute();
    if($res->count()) {
      return $res;
    }
    return 0;
  }
  public function updateValidationNo($validationNo,$orderId)
  {
    try {
      $q = Doctrine_Query::create();
      $q->update('GatewayOrder');
      $q->set('validation_number', '?', $validationNo);
      $q->where("order_id=?", $orderId);
      $q->execute();
      return true;
    }catch(Exception $e){
      echo("Problem found". $e->getMessage());die;
    }
  }


  public function updateValidationNoStatus($validationNo,$status,$orderId,$transactionDate)
  {
    try {
      $q = Doctrine_Query::create();
      $q->update('GatewayOrder');
      if($validationNo)
      $q->set('validation_number', '?', $validationNo);
      $q->set('status', '?', $status);
      if($transactionDate != '')
      	$q->set('transaction_date', '?', $transactionDate);
      $q->where("order_id=?", $orderId);
      $q->execute();
      return true;
    }catch(Exception $e){
      echo("Problem found". $e->getMessage());die;
    }
  }

  
  public function getLastOrderDetails($app_id='',$payment_status='',$limit=0) {
    $query = Doctrine_Query::create()
    ->select('*')
    ->from('GatewayOrder')
    ->where("1=1");
    if($app_id!=""){
        $query->andwhere('app_id=?',$app_id);
    }
    if($payment_status!='') {
      $query->andWhere('status = "'.$payment_status.'"');
    }
    $query->orderBy("id DESC limit $limit,1");
    return $query->execute()->toarray();
    
  }

  public function currentOrderDetails($userId ='',$payment_status='',$limit=0) {
    $query = Doctrine_Query::create()
    ->select('*')
    ->from('GatewayOrder')
    ->where("1=1");
    if($userId!=""){
        $query->andwhere('user_id=?',$userId);
    }
    if($payment_status!='') {
      $query->andWhere('status = "'.$payment_status.'"');
    }
    $query->orderBy("id DESC limit $limit,1");
    return $query->execute()->toarray();

  }
  public function deleterecords($app_id,$order_id){
    $query = Doctrine_Query::create()
            ->delete('GatewayOrder g')
            ->where('g.app_id=?',$app_id)
            ->andWhere('g.order_id=?',$order_id)
            ->execute();      
  }
  public function getDetailsByOrderId($orderId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('*')
      ->from('GatewayOrder')
      ->where('order_id = ?',$orderId);
      
      $res = $q->execute()->toarray();
      return  $res;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
}
