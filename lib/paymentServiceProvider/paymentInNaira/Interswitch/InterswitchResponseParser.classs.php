<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InterswitchResponceclasss
 *
 * @author sdutt
 */
class InterswitchResponseParser {
    //put your code here
    public $getData;
    public $postData;
    public $header;
    public $withoutSplit;
    public $splitData;
    
    public function setResponseData($getData,$postData,$header){
         $this->getData = array_change_key_case($getData, CASE_LOWER);
         $this->postData = array_change_key_case($postData, CASE_LOWER);
         $this->header = $header;
         $getDataKeys = array_keys($getData);
         $getDataValues = array_values($getData);

         //$this->setSplitData();
         $this->setWithoutSplitData();
    }

    public function setLogResponseData($trans_num)
    {
      $path = $this->getLogPath();
     
      $file_name = $path."/interswitch-".$trans_num.'-'.date('Y_m_d_H:i:s(T)').".txt";
      $current = "Log Time : ".date('Y-m-d H:i:s (T)')."\n";
      $current = "Log Type : Interswitch Payment\n";
      $current .= "Header : ".serialize(get_headers($this->header))." \n";
      $current .= "Query String : Transaction For Payments\n";
      $current .= "Post Data : ".serialize($this->postData)."\n";
      $current .= "Get Data : ".serialize($this->getData)."\n";
      $current .= "Transaction Number : ".$trans_num."\n";


      @file_put_contents($file_name, $current);      
    }    

    public function getLogPath()
    {
      $InterswitchConfigObj = new InterswitchConfig();
      $logPath = $InterswitchConfigObj->getLogFilePath();
      $logPath = $logPath.date('Y-m-d');
     
      if(is_dir($logPath)=='')
      {
        $dir_path=$logPath."/";
        mkdir($dir_path,0777,true);
      }
      return $logPath;
    }
    
/*
 *
    public function setSplitData(){

      $split = array();
      $withoutSplit['gateway_type_id'] = '2';
      $split['split'] = 't';
      $split['response'] = $this->getData['resp'];
      $split['description'] = $this->getData['desc'];
      $split['transaction_id'] = $this->getData['txnRef'];
      $split['payref'] = $this->getData['payRef'];
      $payref = $this->getData['payRef'];
      $payref_split = explode("|", $payref);
      $split['bank_name'] = $payref_split[0];
      $split['retref'] = $this->getData['retRef'];
      $split['card_num'] = $this->getData['cardNum'];
      $split['appr_amt'] = $this->getData['apprAmt'];

      $this->splitData = $split;
    }
*/
    public function setWithoutSplitData(){      

      
      $withoutSplit['response'] = $this->getData['resp'];
      $withoutSplit['description'] = $this->getData['desc'];
      $withoutSplit['transaction_id'] = $this->getData['txnref'];
      $withoutSplit['payref'] = $this->getData['payref'];
      $payref_split = explode("|", $withoutSplit['payref']);
      $withoutSplit['bank_name'] = $payref_split[0];
      $withoutSplit['retref'] = $this->getData['retref'];
      $withoutSplit['card_num'] = $this->getData['cardnum'];
      $withoutSplit['amount'] = $this->getData['appramt'];

      $this->withoutSplit = $withoutSplit;
    }

     public function getResponseData(){
       return $this->withoutSplit;
     }
/*
     public function getSplitData(){
       return $this->withoutSplit;
     }
 * 
 */
}
?>
