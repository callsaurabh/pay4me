<?php
class InterswitchConfig
{
  public $itemName;
  public $bankId;
  public $acctNum;
  public $redirectUrl;
  public $custId;
  public $custNameDesc;
  public $productId;
  public $currency;
  public $siteRedirectUrl;
  public $siteName;
  public $custIdDesc;
  public $payItemId;
  public $payItemName;
  public $paymentParams;

  public $interswitchCharge;
  public $minAmount;
  public $institution; 
  public $transactionURL;
  public $description;
  public $customerId;
  //public $serviceTypeCharges;
   
  public function getURL()
  {
    $protocal = ($this->isSSL())?'https://':'http://';
    $haystack = $protocal.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $needle = '/web/';
    $var = substr($haystack, 0, strpos($haystack, $needle));
    $webURL = $protocal.strstr($var,$_SERVER['SERVER_NAME']);

    if($var==''){
      $webURL = $protocal.$_SERVER['SERVER_NAME'];
    }else{
      $webURL = $protocal.strstr($var,$_SERVER['SERVER_NAME']).'/web';
    }
    return $webURL;
  }

  public function isSSL()
  {
    if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')
        return true;
     else
        return false;
  }

  public function getLogFilePath()
  {
    $logFolder = sfConfig::get('sf_web_dir')."/paymentServiceProvider/paymentInNaira/logs/";
    return $logFolder;
  }

 
 
  ############ Interswitch Values##################

  public function getItemName()
  {
    //$this->itemName='PassStandard';
    return $this->itemName;
  }

  public function getBankId()
  {
    //$this->bankId='0';
    return $this->bankId;
  }

  public function getAccountNum()
  {
   // $this->acctNum = 'PassStandard';
    return $this->acctNum;
  }
/*
  public function getIntSiteRedirectUrl()
  {
    //$this->intSiteRedirectUrl = $this->getURL().'/index.php/paymentProcess/restoreResponce?echo=1';
    return $this->redirectUrl;
  }
*/
  ############ InterswitchSplit Values##################
  public function getCustId()
  {
    //$this->intSplitCustId = 'CADP700000';
    return $this->custId;
  }

  public function getCustNameDesc()
  {
    //$this->intSplitCustNameDesc = 'PayForMe Service User';
    return $this->custNameDesc;
  }

  public function getProductId()
  {
    //$this->intSplitCustId = '43';
    return $this->productId;
  }

  public function getCurrency()
  {
    //$this->intSplitCurrency = '566';
    return $this->currency;
  }

  public function getSiteRedirectUrl()
  {
    //$this->intSplitSiteRedirectUrl = $this->getURL().'/index.php/paymentProcess/restoreResponce?echo=2';
    return $this->getURL().$this->siteRedirectUrl;
  }

  public function getSiteName()
  {
    //$this->intSplitSiteName = 'portal.immigratiserviceTypeChargeson.gov.ng';
    return $this->siteName;
  }

  public function getCustIdDesc()
  {
    //$this->intSplitSiteName = 'SWGLOBAL';
    return $this->custIdDesc;
  }

  public function getPayItemId()
  {
    //$this->intSplitPayItemId = '430';
    return $this->payItemId;
  }

  public function getPayItemName()
  {
    //$this->intSplitPayItemName = "PayForMe Application Payments";
    return $this->payItemName;
  }

  public function getPaymentParams()
  {
    //$this->intSplitPaymentParams = "payment_split";
    return $this->paymentParams;
  }

  ############## This is the Transuction Fees for InterswitchSplit ###########################
  
  public function getInterswitchCharge()
  {
    //$this->interswitchCharge='1.5228426';
    return $this->interswitchCharge;
  }

  ############## Minimum amount for Split Transaction #########################
 
 /* public function getMinAmount()
  {
    $this->minAmount='1500';
    return $this->minAmount;
  }
*/
  public function getInstitution()
  {
    return $this->institution;
  }

  ############ SET interswitch url ############

  public function getTransactionURL()
  {    
    return $this->transactionURL;
  }

  public function getCustomerId()
  {
   
    return $this->customerId;
  }
  
//  public function getServiceTypeCharges()
//  {
//    return $this->serviceTypeCharges;
//  }
  
}
?>