<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InterswitchResponceclasss
 *
 * @author sdutt
 */
class EtranzactResponseParser {
    //put your code here
    public $getData;
    public $postData;
    public $header;
    public $withoutSplit;
    public $splitData;
    
    public function setResponseData($getData,$postData,$header){
         $this->getData = $getData;
         $this->postData = $postData;
         $this->header = $header;
         $getDataKeys = array_keys($getData);
         $getDataValues = array_values($getData);

         //$this->setSplitData();
         $this->setWithoutSplitData();
    }

    public function setLogResponseData()
    {
      $path = $this->getLogPath();
      $file_name = $path."/".date('Y_m_d_H:i:s(T)').".txt";
      $current = "Log Time : ".date('Y-m-d H:i:s (T)')."\n";
      $current .= "Header : ".serialize(get_headers($this->header))." \n";
      $current .= "Query String : Transaction For Payments\n";
      $current .= "Post Data : ".serialize($this->postData)."\n";
      $current .= "Get Data : ".serialize($this->getData)."\n";

      file_put_contents($file_name, $current);     
    }    

    public function getLogPath()
    {
      $InterswitchConfigObj = new InterswitchConfig();
      $logPath = $InterswitchConfigObj->getLogFilePath();
      $logPath = $logPath.date('Y-m-d');
      if(is_dir($logPath)=='')
      {
        $dir_path=$logPath."/";
        mkdir($dir_path,0777,true);
      }
    }
    
    public function setWithoutSplitData(){

      $withoutSplit = array();
      $withoutSplit['terminal_id'] = $this->postData['TERMINAL_ID'];
      $withoutSplit['response'] = $this->postData['SUCCESS'];      
      $withoutSplit['description'] = $this->postData['DESCRIPTION'];      
      $withoutSplit['merchant_code'] = $this->postData['MERCHANT_CODE'];
      $withoutSplit['checksum'] = $this->postData['CHECKSUM'];
      $withoutSplit['amount'] = $this->postData['AMOUNT'];
      $withoutSplit['echo_data'] = $this->postData['ECHODATA'];
      $withoutSplit['country_code'] = $this->postData['COUNTRY_CODE'];      
      $withoutSplit['no_retry'] = $this->postData['NO_RETRY'];
      $withoutSplit['card_num'] = $this->postData['CARD4'];
      $withoutSplit['transaction_id'] = $this->postData['TRANSACTION_ID'];
      $this->withoutSplit = $withoutSplit;
    }

     public function getResponseData(){
       return $this->withoutSplit;
     }
/*
     public function getSplitData(){
       return $this->withoutSplit;
     }
 * 
 */
}
?>
