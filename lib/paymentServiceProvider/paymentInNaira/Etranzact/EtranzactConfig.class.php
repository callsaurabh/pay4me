<?php
class EtranzactConfig
{
  public static function getURL()
  {
    $protocal = (self::isSSL())?'https://':'http://';
    $haystack = $protocal.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $needle = '/web/';
    $var = substr($haystack, 0, strpos($haystack, $needle));
    $webURL = $protocal.strstr($var,$_SERVER['SERVER_NAME']);

    if($var==''){
      $webURL = $protocal.$_SERVER['SERVER_NAME'];
    }else{
      $webURL = $protocal.strstr($var,$_SERVER['SERVER_NAME']).'/web';
    }
    return $webURL;
  }

  public static function isSSL()
  {
    if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')
    return true;
    else
    return false;
  }

  public static function getLogoURL()
  {
    return self::getURL().'/img/pay4me_logo.png';
  }
}
?>