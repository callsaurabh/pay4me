<?php

class payForMeManager implements payForMeService {

    public $platform;

    public function getTransactionDetails($transactionId) {
        try {
            $txnId = TransactionTable::getTransactionDetails($transactionId);
            if (count($txnId) > 0) {

                return array('txnId' => $txnId['pfm_transaction_number']);
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function checkInterswitchCategory($payment_mode_options, $merchantId) {
        $cnt = Doctrine::getTable('InterswitchMerchantCategoryMapping')->checkInterswitchCategory($merchantId);
        if ($cnt == 0) {
            unset($payment_mode_options[0]['interswitch']);
            unset($payment_mode_options['interswitch']);
        }
        return $payment_mode_options;
    }

    public function getPaymentMode($merchantId, $merchantServiceId="", $currencyId) {
        try {
            $res = Doctrine::getTable('ServicePaymentModeOption')->getServiceDetails($merchantId, $merchantServiceId, $currencyId);
            $paymentModeConfig = paymentScheduleConfigServiceFactory::getService(paymentScheduleConfigServiceFactory::$paymentModeConfig);
            $resultData = $paymentModeConfig->isPaymentModeActive($merchantId, $merchantServiceId);
            $modeMode = array();
            $modeData = array();
//      print "<pre>";
//      print_r($res);
            //check is data not present in schedule merchant payment mode option
            if ($resultData == false) {
                foreach ($res as $result) {
                    $payment_mode_name = $result['PaymentModeOption']['PaymentMode']['payment_mode_name'];
                    $payment_mode_display_name = $result['PaymentModeOption']['PaymentMode']['payment_mode_display_name'];
                    if ($payment_mode_name != "") {
                        $pay_arr = strtolower(str_replace(" ", "_", strip_tags($payment_mode_name)));

                        if ($payment_mode_display_name != "") {
                            $modeMode[0][$pay_arr] = $payment_mode_display_name;
                        } else {
                            $modeMode[0][$pay_arr] = $payment_mode_name;
                        }

                        $payment_mode_option_name = $result['PaymentModeOption']['payment_mode_option_name'];
                        $payment_mode_arr = strtolower(str_replace(" ", "_", $payment_mode_option_name));

                        if ($payment_mode_display_name != "") {
                            $modeMode[$pay_arr][$payment_mode_arr] = $payment_mode_display_name;
                        } else {
                            $modeMode[$pay_arr][$payment_mode_arr] = $payment_mode_name;
                        }
                        ////
//            print "<pre>";
//            print_r($modeMode);print "shuchi";
                    }
                }
            } else {
                foreach ($res as $result) {
                    $get = 0;
                    foreach ($resultData as $modeData) {
                        if ($modeData['payment_mode_id'] == $result['PaymentModeOption']['PaymentMode']['paymentMode'] && $modeData['payment_mode_option_id'] == $result['PaymentModeOption']['paymentModeOption'])
                            $get = 1;
                    }
                    if ($get != 1) {
                        $payment_mode_name = $result['PaymentModeOption']['PaymentMode']['payment_mode_name'];
                        $paymeny_mode_option_display_name = $result['PaymentModeOption']['PaymentMode']['payment_mode_display_name'];
                        if ($payment_mode_name != "") {
                            $pay_arr = strtolower(str_replace(" ", "_", $payment_mode_name));

                            if ($paymeny_mode_option_display_name != "") {
                                $modeMode[0][$pay_arr] = $paymeny_mode_option_display_name;
                            } else {
                                $modeMode[0][$pay_arr] = $payment_mode_name;
                            }

                            $payment_mode_option_name = $result['PaymentModeOption']['payment_mode_option_name'];
                            $payment_mode_arr = strtolower(str_replace(" ", "_", $payment_mode_option_name));

                            if ($paymeny_mode_option_display_name != "") {
                                $modeMode[$pay_arr][$payment_mode_arr] = $paymeny_mode_option_display_name;
                            } else {
                                $modeMode[$pay_arr][$payment_mode_arr] = $payment_mode_option_name;
                            }
                        }
                    }
                }
            }
            return $modeMode;
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
        //$service='nis';
        //        switch(strtolower($service)) {
        //
        //            case 'nis':
        //                $modeMode = array(array('bank'=>'Bank','dcard'=>'Debit Card'),'dcard'=>array('interswitch' => 'Interswitch', 'etranzact' => 'eTranzact'));
        //                return $modeMode ;
        //                break;
        //            case 'crffn':
        //                $modeMode = array(array('bank'=>'Bank'));
        //                return $modeMode ;
        //                break;
        //        }
    }

    public function getService($transactionId) {
        try {
            $serviceCharge = TransactionTable::getTransactionDetails($transactionId);

            if (count($serviceCharge) > 0) {
                return array('appCharge' => $serviceCharge['payment_amount_requested']);
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    //    public function getBank($merchantServiceId) {
    //        try {
    //            $bankCharge =  BankServiceChargesTable::getBankCharge($merchantServiceId);
    //
    ////          echo "Details 555: <pre>";print_r($bankCharge);die;
    //
    //            if(count($bankCharge)>0) {
    //                return array('bankCharge' => $bankCharge['charges']);
    //            }else {
    //                return false;
    //            }
    //        }catch(Exception  $e){
    //            echo("Problem found". $e->getMessage());die;
    //        }
    //    }

    public function getTotal($appCharge, $bankCharge) {
        try {
            $totalCharge = $appCharge['appCharge'] + $bankCharge['bankCharge'];
            return array('totalCharge' => $totalCharge);
            ;
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function getMerchantServiceId($transaction_num) {
        try {
            $merchantDetails = TransactionTable::getMerchantDetails($transaction_num);
            return array('merchant_service_id' => $merchantDetails['MerchantRequest']['merchant_service_id']);
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function getMerchantRequestId($transaction_num) {
        try {
            $merchantDetails = Doctrine::getTable('Transaction')->getMerchantDetails($transaction_num);
            return array('merchant_request_id' => $merchantDetails['merchant_request_id']);
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function saveRequest($formData) {
        $merchantId = $this->getMerchantId($formData['service_type']);
        $merchantServiceId = $this->getMerchantSerId($merchantId, $formData['app_type']); // static for NIS
        $chekDuplicate = $this->checkDuplicate($formData['paymentId'], $merchantServiceId);
        if (empty($chekDuplicate)) {
            $mItem = new MerchantItem();
            $mItem->item_number = $formData['paymentId'];
            $mItem->merchant_service_id = $merchantServiceId;
            $mItem->payment_amount_requested = $formData['amount'];
            $mItem->save();
            $mItemId = $mItem->getId();
        } else {
            $mItemId = $chekDuplicate;
        }
        // echo $mItemId ;

        $mRequest = new MerchantRequest();
        $mRequest->txn_ref = $formData['txn_ref'];
        $mRequest->merchant_id = $merchantId;
        $mRequest->merchant_service_id = $merchantServiceId;
        $mRequest->merchant_item_id = $mItemId;
        $mRequest->item_fee = $formData['amount'];
        //$mRequest->payment_mode= '';
        //$mRequest->bank_name= '';
        $mRequest->payment_status_code = $this->getPaymentStatusCode();
        //$mRequest->paid_amount= '';
        $mRequest->save();
        $mRequestId = $mRequest->getId();
        //  echo $mRequestId;

        $mRequestDetails = new MerchantRequestDetails();
        $mRequestDetails->merchant_service_id = $merchantServiceId;
        $mRequestDetails->merchant_request_id = $mRequestId;
        $mRequestDetails->iparam_one = $formData['app_id'];
        if (isset($formData['reference_id'])) {
            $mRequestDetails->iparam_two = $formData['reference_id'];
        }
        $mRequestDetails->first_name = $formData['first_name'];
        $mRequestDetails->last_name = $formData['last_name'];
        $mRequestDetails->sparam_one = $formData['app_type'];
        $mRequestDetails->sparam_two = $formData['mobile_no'];
        $mRequestDetails->sparam_three = $formData['email_add'];
        $mRequestDetails->save();
        $mRequestDetailsId = $mRequest->getId();
        //echo $mRequestDetailsId;

        return $mRequestId;
    }

    static function getMerchantId($merchantName) {
        $query = Doctrine_Query::create();
        $query->from('Merchant');
        $query->andwhere('name = ?', $merchantName);
        //print_r($query->getQuery()); die();
        $rs = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
        $record = $rs[0];
        //    echo $merchantName ;
        //    print_r($rs);die();
        return $record['id'];
    }

    
    static function getFeedbackDetails($feedbackId) {
        try { 
            $feedbackRes = Doctrine::getTable('Feedback')->getFeedbackDetails($feedbackId);

            if (count($feedbackRes) > 0) {
                return $feedbackRes[0];
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }
     static function getMerchantName($merchantId) {
        $merchantName = Doctrine::getTable('Merchant')->getMerchantName($merchantId);
        return $merchantName[0]['name'];
    }

    static function getBankName($bankId) {
        $bankName = Doctrine::getTable('Bank')->getBankName($bankId);
        return $bankName['bank_name'];
    }

    static function getPaymentModeOptionName($paymentmodeoptionId) {
        $paymentModeOptionName = Doctrine::getTable('PaymentModeOption')->getPaymentModeName($paymentmodeoptionId);
        return $paymentModeOptionName['name'];
    }

    
    static function getMerchantSerId($merchantId, $serviceName) {
        $query = Doctrine_Query::create();
        $query->from('MerchantService');
        $query->andwhere('merchant_id = ?', $merchantId);
        $query->andwhere('name = ?', $serviceName);
        $rs = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
        $record = $rs[0];
        //print_r($record);die();
        return $record['id'];
    }

    public function checkDuplicate($item_num, $merchantServiceId) {

        $query = Doctrine_Query::create();
        $query->from('MerchantItem');
        $query->andwhere('item_number = ?', $item_num);
        $query->andwhere('merchant_service_id = ?', $merchantServiceId);
        $rs = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
        if ($rs) {
            $record = $rs[0]['id'];
        } else {
            $record = 0;
        }
        //print_r($record);die();
        return $record;
    }

    //search is transaction is allready paid
    public function checkMerchantRequestIsSuccessfull($id, $checkParam) {

        //fetch all records with paricular retryid that is $txnRef number where payment status code is successfull
        if ($checkParam == 'ByTxnRef') {
            $query = Doctrine_Query::create();
            $query->from('MerchantRequest');
            $query->where('txn_ref = ?', $id);
            $query->andwhere('payment_status_code = ?', '0');
            $query->andwhere('payment_mode != ?', 'NULL');
            $rs = $query->execute(array(), Doctrine::HYDRATE_ARRAY);

            if ($rs && count($rs[0]) > 0) {
                return true;
            } else {
                return false;
            }
        } else if ($checkParam == 'BYMerchantRequestId') {

            $query = Doctrine_Query::create();
            $query->from('MerchantRequest');
            $query->where('id = ?', $id);
            $rs = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
            $txn_ref = $rs[0]['txn_ref'];

            $query = Doctrine_Query::create();
            $query->from('MerchantRequest');
            $query->where('txn_ref = ?', $txn_ref);
            $query->andwhere('payment_status_code = ?', '0');
            $query->andwhere('payment_mode != ?', 'NULL');
            $rs = $query->execute(array(), Doctrine::HYDRATE_ARRAY);

            if ($rs && count($rs[0]) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            //return true if there is some parameter missing to call method, by hacking
            return true;
        }
    }

    static function getPaymentStatusCode() {
        return '01';
    }

    //    static function getBankCharges($merchant_service_id){
    //        $bank_charges_obj = Doctrine::getTable('BankServiceCharges')->findByMerchantServiceId($merchant_service_id);
    //        return $bank_charges_obj->getFirst()->getCharges();
    //    }


    static function getCharges($merchant_service_id, $payment_mode_option_id, $currencyId=1) {
        $charges = Doctrine::getTable('TransactionCharges')->getCharges($merchant_service_id, $payment_mode_option_id, $currencyId);
        
        return $charges;
    }

    static function getTransDetails($transNum) {




        $query = Doctrine_Query::create();
        $query->from('Transaction t')
                ->leftJoin('t.MerchantRequest r')
                ->leftJoin('r.MerchantRequestDetails d');
        $query->andwhere('t.pfm_transaction_number = ?', $transNum);





        $result = $query->execute(array(), Doctrine::HYDRATE_ARRAY);
        $rs = $result[0];
        $transDetails = array(
            'transaction_num' => $rs['pfm_transaction_number'],
            'service_type' => $rs['MerchantRequest']['merchant_id'],
            'txn_ref' => $rs['MerchantRequest']['txn_ref'],
            'bank_charges' => $rs['MerchantRequest']['bank_charge'],
            'service_charges' => $rs['MerchantRequest']['service_charge'],
            'application_id' => $rs['MerchantRequest']['MerchantRequestDetails']['iparam_one'],
            'ref_no' => $rs['MerchantRequest']['MerchantRequestDetails']['iparam_two'],
            'application_type' => $rs['MerchantRequest']['MerchantRequestDetails']['sparam_one'],
            'title' => '',
            'name' => $rs['MerchantRequest']['MerchantRequestDetails']['name'],
            'mobile' => $rs['MerchantRequest']['MerchantRequestDetails']['sparam_two'],
            'email' => $rs['MerchantRequest']['MerchantRequestDetails']['sparam_three'],
            'app_charges' => $rs['MerchantRequest']['item_fee'],
            'bank_charges' => $rs['MerchantRequest']['bank_charge'],
            'merchant_service_id' => $rs['MerchantRequest']['merchant_service_id'],
            'service_charge' => $rs['MerchantRequest']['service_charge']
        );

        return $transDetails;
    }

    static function getPaymentModeOptionId($optionText) {

        $query = Doctrine_Query::create();
        $query->from('PaymentModeOption');
        $query->andwhere('name = ?', $optionText);
        $rs = $query->execute(array(), Doctrine::HYDRATE_ARRAY);

        $record = $rs[0];

        return $record['id'];
    }

    public function getItemObject($merchant_service_id, $item_num) {
        $query = Doctrine_Query::create();
        $query->from('MerchantItem');
        $query->andwhere('item_number = ?', $item_num);
        $query->andwhere('merchant_service_id = ?', $merchant_service_id);
        $rs = $query->execute();
        if (count($rs)) {
            return false;
        }
        return $rs->getFirst();
    }

    public function getMerchantIdByService($merchant_service_id) {
        $query = Doctrine_Query::create();
        $query->from('Merchant a');
        $query->leftJoin('a.MerchantService b');
        $query->where('b.id = ?', $merchant_service_id);
        //$query->andwhere('merchant_service_id = ?',$merchant_service_id);
        $rs = $query->execute(array(), DOCTRINE::HYDRATE_ARRAY);
        if ($rs) {
            $record = $rs;
        } else {
            $record = false;
        }
        return $record;
    }

    public function authenticateMerchantRequest($request) {
        $merchant_code = $request->getHttpHeader('AUTH_USER', 'PHP');
        $merchant_key = $request->getHttpHeader('AUTH_PW', 'PHP');
        $merchant_param_name = sfConfig::get('app_merchant_url_param_name');
        $request_merchant_id = $request->getParameter($merchant_param_name);

        if ($request_merchant_id != $merchant_code) {
            return false;
        }

        $logger = sfContext::getInstance()->getLogger();
        $logger->debug("OBJ: $merchant_key " . print_r($merchant_code, true));

        $merchant_details = MerchantTable::getMerchantDetails($request_merchant_id);
        $logger->debug("OBJ: " . print_r($merchant_details, true));
        if (($merchant_details['merchant_code'] == $merchant_code) && ($merchant_details['merchant_key'] == $merchant_key)) {
            return true;
        }

        return false;
    }

    public function authenticateBankRequest($request) {
        $bank_code = $request->getHttpHeader('AUTH_USER', 'PHP');
        $bank_key = $request->getHttpHeader('AUTH_PW', 'PHP');
        $bank_param_name = sfConfig::get('app_bank_url_param_name');
        $request_bank_id = $request->getParameter($bank_param_name);

        if ($request_bank_id != $bank_code) {
            return false;
        }

        $logger = sfContext::getInstance()->getLogger();
        $logger->debug("OBJ: $bank_key " . print_r($bank_code, true));

        $bank_details = Doctrine::getTable('BankPaymentScheduler')->getBankDetails($request_bank_id);  //BankPaymentScheduler::getBankDetails($request_bank_id) ;
        $logger->debug("OBJ: " . print_r($bank_details, true));

        if ($bank_details) {
            if (($bank_details['bank_code'] == $bank_code) && ($bank_details['bank_key'] == $bank_key)) {
                return true;
            }

            return false;
        } else {
            return false;
        }
    }

    public function isValidMerchantServiceId($merchantCode, $serviceId) {

        $serviceCount = Doctrine_Query::create()
                        ->from('Merchant mc')
                        ->leftJoin('mc.MerchantService ms')
                        ->where('mc.merchant_code = ?', $merchantCode)
                        ->andWhere('ms.id = ?', $serviceId)
                        ->count();
        return $serviceCount;
    }

    public function isValidItemNumber($serviceId, $itemNumber) {

        $itemCount = Doctrine_Query::create()
                        ->from('MerchantItem mc')
                        ->where('mc.merchant_service_id = ?', $serviceId)
                        ->andWhere('mc.item_number = ?', $itemNumber)
                        ->count();
        return $itemCount;
    }

    static function getServiceCharges($merchant_service_id, $item_fee, $payment_mode_option_id=0, $currencyId=1) {
        //get the details of charges from the table - merchant_service_charges

        $charges = Doctrine::getTable('MerchantServiceCharges')->getCharges($merchant_service_id, $payment_mode_option_id, $currencyId);

        //$item_charge_percent = sfConfig::get('app_item_fee_charge');
        if ($charges) {
            $upperLimit = $charges[0]['upper_slab'];
            $lowerLimit = $charges[0]['lower_slab'];
            $item_charge_percent = $charges[0]['service_charge_percent'];


            $calcAmount = ($item_fee * $item_charge_percent) / 100;

            if (($upperLimit != 0)) {
                if ($calcAmount > $upperLimit) {
                    $calcAmount = $upperLimit;
                    return $calcAmount;
                }
            }
            if ($lowerLimit != 0) {
                if ($calcAmount < $lowerLimit) {
                    $calcAmount = $lowerLimit;
                    return $calcAmount;
                }
            }
            if ($calcAmount <= $upperLimit && $calcAmount >= $lowerLimit) {
                return $calcAmount;
            }
            
            return $calcAmount;
        }





        //return (($item_charge_percent*$item_fee)/100);
    }

    //get record from transaction and releated tables by transaction_number
    public function getTransactionRecord($transNum, $bankId='') {
        $returnRecordSet = Doctrine::getTable('Transaction')->getTransactionDetails($transNum, $bankId);
        return $returnRecordSet;
    }

    public function getMerchantServices($merchant_name) {
        $merchantId = $this->getMerchantId($merchant_name);
        $merchant_service_options = Doctrine::getTable('MerchantService')->findByMerchantId($merchantId);
        $merchant_service_array = array();
        foreach ($merchant_service_options as $options) {
            $merchant_service_array[$options->getId()] = $options->getName();
        }
        return $merchant_service_array;
    }

    public function getRandomNumber() {

        // return rand(1,5).''.rand('100','1000').''.rand('100','10000');
        return mt_rand(1000000, 999999999);
    }

    public function getNotificationTransactionDetails($transNum) {
        $returnRecordSet = Doctrine::getTable('Transaction')->getNotificationTransactionDetails($transNum);
        return $returnRecordSet;
    }

    public function getNotificationTransactionDetailsByDates($startDate = "", $endDate = "", $bank = "", $branchCode = "", $userId = "") {
        $returnRecordgetRandomNumberSet = Doctrine::getTable('Transaction')->getNotificationTransactionDetailsByDates($startDate, $endDate, $bank, $branchCode, $userId);
        return $returnRecordSet;
    }

    public function generateTransactionNumber() {
        do {
            $transaction_number = $this->getRandomNumber();
            $duplicacy_transaction_number = Doctrine::getTable('Transaction')->chkDuplicacy($transaction_number);
        } while ($duplicacy_transaction_number > 0);
        return $transaction_number;
    }

    public function saveTransaction($formData) {
        $merchantRequestObj = Doctrine::getTable('MerchantRequest')->findById($formData['merchantRequestId']);

        $itemId = $merchantRequestObj->getFirst()->getMerchantItemId();
        $paymentStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($itemId);
        //check if the transaction number already exists for this merchant request->in case of refresh it will exist
        $transactionObj = Doctrine::getTable('Transaction')->chkTransactionNo($formData['trans_num'], $formData['merchantRequestId']);
        if (($transactionObj) && ($paymentStatus == 1)) {
            return $trans_num = $formData['trans_num'];
        }
        if (($paymentStatus == 1) && ($transactionObj == 0)) { 
            try {
                $pfm_transaction = new Transaction();
                $pfm_transaction->pfm_transaction_number = $formData['trans_num'];
                //$pfm_transaction->pfm_transaction_number = $formData['trans_num'];
                $pfm_transaction->merchant_request_id = $formData['merchantRequestId']; //$formData['app_charges']+$bank_charges;
        
                $pfm_transaction->payment_mode_option_id = self::getPaymentModeOptionId($formData['pay_mode']);
                $pfm_transaction->save();
               $trans_num = $pfm_transaction->getPfmTransactionNumber();
            } catch (Exception $e) {
                sfContext::getInstance()->getUser()->setFlash('appError', ' Due to concurrency issue, your transaction was not successful. Please try again.');
                sfContext::getInstance()->getController()->redirect('@erroruser');
            }
            
            
            $paymentStatus = Doctrine::getTable('AvcPassportCharges')->findByMerchantRequestId($formData['merchantRequestId']);
            if(count($paymentStatus) > 0){                
                $paymentStatus->getFirst()->setTransactionId($pfm_transaction->getId());
                $paymentStatus->getFirst()->save();
            }
            
            return $trans_num;
        } else {
            return false;
        }
    }

    public function updateMerchantRequest($merchant_request_id, $payment_mode, $add_bank_charges=1, $bank_name="") {
        $pfm_merchant_request = Doctrine::getTable('MerchantRequest')->find(array($merchant_request_id));
        $merchant_service_id = $pfm_merchant_request->getMerchantServiceId();
        $currencyId = $pfm_merchant_request->getCurrencyId();
        $item_fee = $pfm_merchant_request->getItemFee();
        
        //Fetching Fees from merchant item table so as to capture it exactly what we get (in case if the fees updated in case of AVC)
        $merchant_item_id = $pfm_merchant_request->getMerchantItemId();
        $merchantItemObj = Doctrine::getTable("MerchantItem")->find($merchant_item_id);
        if(!empty($merchantItemObj)){
          $item_fee = $merchantItemObj->getPaymentAmountRequested();
        }
        //Fetching Fees from merchant item table  ....
        
        $payment_mode_option_id = self::getPaymentModeOptionId($payment_mode);
        $financial_institution_charges = 0;
        $pay4me_charges = 0;
        $service_charges = 0;
        $charges = self::getCharges($merchant_service_id, $payment_mode_option_id, $currencyId);
        if ($charges) {
            $financial_institution_charges = $charges['financial_institution_charges']; //payForMeManager::getBankAmount();
            $pay4me_charges = $charges['payforme_charges'];
        }
        $service_charges = self::getServiceCharges($merchant_service_id, $item_fee, $payment_mode_option_id, $currencyId);
        $payforme_amount = $pay4me_charges + $service_charges;
        if (isset($financial_institution_charges) && $financial_institution_charges > 0) {
            $pfm_merchant_request->setBankAmount($financial_institution_charges);
        } else {
            $pfm_merchant_request->setBankAmount(0);
        }
        $pfm_merchant_request->setBankCharge($pay4me_charges + $financial_institution_charges);
        $pfm_merchant_request->setServiceCharge($service_charges);
        $pfm_merchant_request->setPayformeAmount($payforme_amount);
        $pfm_merchant_request->setPaymentModeOptionId($payment_mode_option_id);
        if ($bank_name != "") {
            $bankId = Doctrine::getTable("Bank")->findBy('bank_name', $bank_name)->getFirst()->getId();
            $pfm_merchant_request->setBankId($bankId);
            $pfm_merchant_request->setBankName($bank_name);
        }
        $pfm_merchant_request->save();
        
        //Update item fees by adding AVC charges if applicable
        $paymentStatus = Doctrine::getTable('AvcPassportCharges')->findByMerchantRequestId($merchant_request_id);
        $avc_amount = 0;
        $errorMessage = "[AVC-PAY4ME] Merchant Request Id => ".$merchant_request_id.", Item Fee => ".$item_fee;
        sfContext::getInstance()->getLogger()->debug($errorMessage);
        if(count($paymentStatus) > 0){
           $avc_amount = $paymentStatus->getFirst()->getAmount();
           $new_item_fee = $item_fee + $avc_amount;
           sfContext::getInstance()->getLogger()->debug("[AVC-PAY4ME-Inside-Loop] :: AppID => " .$paymentStatus->getFirst()->getApplicationId(). ", Merchant Request Id => ".$merchant_request_id.", AVC Amount => ".$avc_amount.", New Item Fee => ". $new_item_fee);
           //Inserting into merchant_request table
           Doctrine::getTable("MerchantRequest")->getUpdatedItemFee($merchant_request_id, $new_item_fee);        
        }
        // End of Update item fees by adding AVC charges if applicable...
        
        
        
    }

    public function updateCheckDetailsForMerchantRequest($trans_num, $checkNumber='', $accountNumber='',$bank_name,$sort_code='') {


        try {
            $bankId = Doctrine::getTable("Bank")->findBy('bank_name', $bank_name)->getFirst()->getId();
            
            $q = Doctrine_Query::create()
                ->update('Transaction')
                ->set('bank_id', '?', $bankId)
                ->where('pfm_transaction_number = ?', $trans_num);                
                if($checkNumber!='' ){
                    $q->set('check_number', '?', $checkNumber);
                }
                if($accountNumber!=''){
                    $q->set('account_number', '?', $accountNumber);
                }
                $q->set('sort_code', '?', $sort_code);
                $q->execute();
        } catch (Exception $e) {
             echo $e->getMessage();
        }
    }
    
    public function checkSufficientBalance($transId) {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $formData = $payForMeObj->getTransactionRecord($transId);
        $currency_id = $formData['MerchantRequest']['currency_id'];
        $billObj = billServiceFactory::getService();
        $walletDetails = $billObj->getEwalletAccountValues($currency_id);
        $accountBalance = $billObj->getEwalletAccountBalance($walletDetails->getFirst()->getId());
        $arr = array();
        $arr['requestId'] = $formData['MerchantRequest']['id'];
        $total_payable_amount = $formData['total_amount'];
        #sfLoader::loadHelpers('ePortal');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $total_payable_amount = convertToKobo($total_payable_amount);
        if ($accountBalance >= $total_payable_amount) {
            $arr['flag'] = true;
        } else {
            $arr['flag'] = false;
        }
        return $arr;
    }

    //  public function getMerchantId($name)
    //  {
    //
    //    try{
    //          return $merchant_id = Doctrine::getTable('Merchant')->getMerchantId($name);
    //    }catch(Exception $e ){
    //          echo("Problem found". $e->getMessage());die;
    //    }
    //  }
    public function getUserDetailByUsername($username) {
        return $getUserDetail = Doctrine::getTable('sfGuardUser')->getUserDetailByUsername($username);
    }

    public function doPay(array $postDataArray=NULL, $txnId, $isBiller=false) {

        try {
            if ($isBiller) {
                $notificationUrl = "biller/sendBillerNotification";
            } else if ($postDataArray['version']) {
                $notificationUrl = "paymentSystem/sendNotification";
            }

            //   $bankNotificationUrl = "paymentSystem/sendBankNotification" ;




            $transactionObj = Doctrine::getTable('Transaction')->findOneByPfmTransactionNumber($txnId);

            $transactionObj->setPaymentStatusCode(0);
            $merchantRequest = $transactionObj->getMerchantRequest();
//      $merchantRequest->paid_by = sfContext::getInstance()->getUser()->getGuardUser()->getId();
            //get Validation Number from merchant_payment_transaction
            $validation_number = $this->getValidationNumber();
            $merchantRequest->validation_number = $validation_number;

            if (count($postDataArray) > 0) {
                foreach ($postDataArray as $key => $value) {
                    if ($key != "id" && $key != "userId") {

                        $merchantRequest->$key = $value;
                    }
                }
            }

            $transactionObj->save();
            $merchant_request_id = $merchantRequest->getId();


            $trackObj = new MerchantPaymentTransaction();
            $trackObj->setMerchantRequestId($merchant_request_id);
            $trackObj->setMerchantRequestUniqueId($merchant_request_id);
            $trackObj->setValidationNumber($validation_number);
            $trackObj->setTransactionNumber($txnId);
            $trackObj->save();
            $Merchant = $merchantRequest->getMerchant();
            $jobName = $Merchant->getId() . ":" . $Merchant->getName();

            // track_merchant_transtrack_merchant_transaction
            // $array_track[$merchant_request_id] = array(0=>$validation_number);
            //  $this->track_merchant_transaction($array_track);
            //        $accountingObj = new EpAccountingManager;
            //        $accountingObj->setPaymentTransactionNumber($validation_number);
            $taskId = EpjobsContext::getInstance()->addJob($jobName,
                            $notificationUrl, array('p4m_transaction_id' => $txnId));
            //  $bankTaskId = EpjobsContext::getInstance()->addJob('BankPaymentNofication',$bankNotificationUrl, array('p4m_transaction_id' => $txnId));

            sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
            //   sfContext::getInstance()->getLogger()->debug("sceduled bank job with id: $bankTaskId");
            //Log audit Details
            if ($this->platform != 'mobile') {
                $this->auditPaymentTransaction($txnId);
            }
            return true;
        } catch (Exception $e) {
            // echo $e->getMessage();die;
            if (strpos($e->getMessage(), "Duplicate entry")) {
                throw new Exception("Payment is in process");
            }
            throw new Exception($e->getMessage());
        }
    }

    public function getValidationNumber() {
        do {
            $validation_no = mt_rand(1000000, 999999999);
            $duplicacy = Doctrine::getTable('MerchantPaymentTransaction')->chkPaymentTransactionNumber($validation_no);
        } while ($duplicacy > 0);
        return $validation_no;
    }

    /* public function doPay(array $postDataArray=NULL,$txnId, $isBiller=false) {
      try {

      // $txnId = $pfmTransactionDetails['pfm_transaction_number'];

      // $merchantRequestId      =

      if($isBiller)
      {
      $notificationUrl = "biller/sendBillerNotification" ;
      }else{
      $notificationUrl = "paymentSystem/sendNotification" ;
      }

      $bankNotificationUrl = "paymentSystem/sendBankNotification" ;
      $validFlag = $this->updateRequestInfo($postDataArray);
      if($validFlag) {
      $transactionObj = transactionServiceFactory::getService(transactionServiceFactory::$TYPE_BASE);
      $transactionFlag = $transactionObj->updateTransaction($txnId);
      if($transactionFlag) {
      $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('p4m_transaction_id' => $txnId));

      //job schedule for bank xml notification sending
      $bankTaskId = EpjobsContext::getInstance()->addJob('BankPaymentNofication',$bankNotificationUrl, array('p4m_transaction_id' => $txnId));

      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
      sfContext::getInstance()->getLogger()->debug("sceduled bank job with id: $bankTaskId");
      //Log audit Details
      $this->auditPaymentTransaction($txnId);
      return true;
      }else
      return false;
      }else
      return false;
      }catch(Exception  $e) {
      sfContext::getInstance()->getUser()->setFlash('error', "There is some problem with Transaction processing. please contact to Portal Admin.", false);
      $gUser = sfContext::getInstance()->getUser()->getGuardUser();
      $group_name = $gUser->getGroupNames();
      $this->redirectTo($group_name);
      }
      } */

    public function redirectTo($group_name) {
        if (in_array(sfConfig::get('app_pfm_role_bank_branch_user'), $group_name))
            sfContext::getInstance()->getController()->redirect('admin/bankUser');
        else
        if (in_array(sfConfig::get('app_pfm_role_ewallet_user'), $group_name))
            sfContext::getInstance()->getController()->redirect('admin/eWalletUser');
    }

    public function auditPaymentTransaction($txnId) {
        //Log audit Details
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID, $txnId, $txnId));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('txnid' => $txnId)),
                        $applicationArr);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function array_sum_accountid($arr) {
        $key_array = array();
        foreach ($arr as $key => $value) {
            $account_id = $value['account_id'];
            $amount = $value['amount'];
            if (array_key_exists($value['account_id'], $key_array)) {
                $account_id . $key_array[$account_id];
                $key_array[$account_id] = $key_array[$account_id] + $amount;
            } else {
                $key_array[$account_id] = $amount;
            }
        }
        $payment_array = array();
        $i = 0;
        foreach ($key_array as $key => $value) {
            $payment_array[$i]['amount'] = $value;
            $payment_array[$i]['account_id'] = $key;
            $i++;
        }
        return $payment_array;
    }

    public function splitUsdPayment($txnId) {
        try {
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId);
            $merchant_id = $pfmTransactionDetails['MerchantRequest']['merchant_id'];
            $merchant_request_id = $pfmTransactionDetails['MerchantRequest']['id'];
            $currency_id = $pfmTransactionDetails['MerchantRequest']['currency_id'];
            $merchant = $pfmTransactionDetails['MerchantRequest']['Merchant']['name'];
            $merchant_service = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
            $payment_mode_option = $pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name'];
            if ($currency_id == 2) {
                $attrArray = array();
                $accountingObj = new pay4meAccounting();
                $is_clear = 1;
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_VBV, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));

                $splitObj = Doctrine::getTable('Pay4meSplit')->findByMerchantRequestId($merchant_request_id);
                foreach ($splitObj as $key => $value) {
                    $account_number = $value->getAccountNumber();
                    $walletObj = new EpAccountingManager;
                    $walletDetails = $walletObj->getWalletDetails($account_number);
                    $account_id = $walletDetails->getFirst()->getId();
                    $amount = $value->getAmount();
                    $attr = $accountingObj->getAccountingHolderObj($description, $amount, $account_id, $is_clear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }


                $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts'));
                $payment_transaction_number = $event->getReturnValue();
                $postDataArray['id'] = $merchant_request_id;
                $postDataArray['validation_number'] = $payment_transaction_number;
                $this->updateRequestInfo($postDataArray);
                return $payment_transaction_number;
            } else {
                throw New Exception("Cannot do this split");
                exit;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function convertToKobo($nairaAmount) {
        return number_format(($nairaAmount * 100), 0, '.', '');
    }

    public function splitPayment($txnId, $validationNo) {


        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId);
        $accountingObj = new pay4meAccounting();


        $payment_mode_option_id = $pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'];
        $bank_id = $pfmTransactionDetails['MerchantRequest']['bank_id'];
        $merchant_id = $pfmTransactionDetails['MerchantRequest']['merchant_id'];
        $merchant_request_id = $pfmTransactionDetails['MerchantRequest']['id'];


        $paymentSplitDetailsObj = Doctrine::getTable('PaymentSplitDetails')->findByMerchantRequestId($merchant_request_id);
        if (is_object($paymentSplitDetailsObj) && $paymentSplitDetailsObj->count() > 0)
            throw New Exception($merchant_request_id . "Split is already done.");
        $amount_paid = $this->convertToKobo($pfmTransactionDetails['MerchantRequest']['paid_amount']);

        $merchant = $pfmTransactionDetails['MerchantRequest']['Merchant']['name'];
        $merchant_service = $pfmTransactionDetails['MerchantRequest']['MerchantService']['name'];
        $pfm_helper_obj = new pfmHelper();
        $payment_mode_option = $pfm_helper_obj->getPMONameByPMId($pfmTransactionDetails['MerchantRequest']['payment_mode_option_id']);

        $bank_name = $pfmTransactionDetails['MerchantRequest']['bank_name'];
        $currency = $pfmTransactionDetails['MerchantRequest']['currency_id'];
        $merchant_service_id = $pfmTransactionDetails['MerchantRequest']['merchant_service_id'];
        $pay4meAmount = $pfmTransactionDetails['MerchantRequest']['payforme_amount'];

        $checkSplit = $this->checkSplit($merchant_request_id);
        
        if ($checkSplit) {
            $to_account_arr = $accountingObj->getRevenueSplitAccounts($merchant_request_id, $merchant_service_id, $pay4meAmount, $currency);
        } else {
            $to_account_arr = $accountingObj->getSplitAccounts($pfmTransactionDetails);
        }
        $userId = $pfmTransactionDetails['MerchantRequest']['paid_by'];
        if ($payment_mode_option_id == 4) {//eWallet
            //get the bank ids from which payment has to be made
//           $userObj = sfContext::getInstance()->getUser();
//           $sfGuardUserObj = $userObj->getGuardUser();
            $userId = $pfmTransactionDetails['MerchantRequest']['paid_by']; //$sfGuardUserObj->getId();
            $userAccountCollectionObj = new UserAccountManager($userId);
            $wallet_account_id = $userAccountCollectionObj->getAccountIdByCurrency($currency);
            $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_WALLET, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));

            $payment_summary = array();
            if ($to_account_arr) {
                foreach ($to_account_arr as $k => $v) {
                    $amt = $this->convertToKobo($v['amount']);
                    $to_account = $v['account_id'];
                    $split_array = $this->getWalletCollectionAccounts($amt, $wallet_account_id);

//print "<pre>";print_r($split_array);//exit;
                    $payment_summary = array_merge($payment_summary, $split_array);


                    $i = 0;
                    foreach ($split_array as $acct_split) {
                        $from_account = $acct_split['account_id'];
                        $amount = $acct_split['amount']; //$amt*$acct_split['ratio'];
                        $paymentSplitObj = new PaymentSplitDetails();
                        $paymentSplitObj->setMerchantRequestId($merchant_request_id);
                        $paymentSplitObj->setFromAccount($from_account);
                        $paymentSplitObj->setToAccount($to_account);
                        $paymentSplitObj->setAmount($amount);
                        $paymentSplitObj->setSplitDate(date('Y-m-d H:i:s'));
                        $paymentSplitObj->setReconciliationState('new');
                        $paymentSplitObj->setPaidBy($userId);
                        $paymentSplitObj->save();

                        //    $payment_summary[$from_account] = $payment_summary[$from_account]+$amount;
                        //print "<br>".$payment_summary[$i]['amt'] = $amount;
                        $i = $i++;
                    }
                    $this->updateEwalletConsolidationAcct($split_array, $wallet_account_id);
                }
            }
            $payment_summary = $this->array_sum_accountid($payment_summary);

//print "<pre>";print_r($payment_summary);exit;
            //debit the amount in collection account
            $payment_transaction_number = $this->debit_ewallet_collection_account($payment_summary, $description, $validationNo);
        } else {//bank and vbv
                   
            if ($payment_mode_option_id == 1) { //bank
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_BANK, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, $bank_id, $merchant_id, 'Payment', $currency);
            } else if ($payment_mode_option_id == 5) { //vbv
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_VBV, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $merchant_id = "";
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, '', '', 'Payment', $currency);
            } else if ($payment_mode_option_id == 2) { //interswitch
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_INTERSWITCH, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $merchant_id = "";
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, '', '', 'Payment', $currency);
            } else if ($payment_mode_option_id == 13) { //merchant-counter
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_MERCHANT_COUNTER, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $bank_id = "";
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, $bank_id, $merchant_id, 'Payment', $currency);
            } else if ($payment_mode_option_id == 3) { //etranzact
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_INTERSWITCH, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $merchant_id = "";
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, '', '', 'Payment', $currency);
            }
             else if ($payment_mode_option_id == 14) { //check
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_CHECK, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, $bank_id, $merchant_id, 'Payment', $currency);
            }

             else if ($payment_mode_option_id == 15) { //draft              
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_BANKDRAFT, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, $bank_id, $merchant_id, 'Payment', $currency);
            }
            
             //bug id 19968
             else if ($payment_mode_option_id == 16) { //internet bank
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_INTERNETBANK, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $pfmTransactionDetails['MerchantRequest']['bank_name']));
                $merchant_id = "";
                $from_account = $accountingObj->getCollectionAccount($payment_mode_option_id, '', '', 'Payment', $currency);
            }


            if ($to_account_arr) {
                foreach ($to_account_arr as $k => $v) {
                    $to_account = $v['account_id'];
                    #sfLoader::loadHelpers('ePortal');
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                    $amount = convertToKobo($v['amount']);
                    $paymentSplitObj = new PaymentSplitDetails();
                    $paymentSplitObj->setMerchantRequestId($merchant_request_id);
                    $paymentSplitObj->setFromAccount($from_account);
                    $paymentSplitObj->setToAccount($to_account);
                    $paymentSplitObj->setAmount($amount);
                    $paymentSplitObj->setSplitDate(date('Y-m-d H:i:s'));
                    $paymentSplitObj->setReconciliationState('new');
//                  if($userObj->isAuthenticated()) {
//                    $userId = $sfGuardUserObj->getId();
                    $paymentSplitObj->setPaidBy($userId);
//                  }
                    $paymentSplitObj->save();
                }
            }
            //credit the amount in collection account
            $payment_transaction_number = $this->credit_bank_collection_account($from_account, $amount_paid, $description, $validationNo);
        }



        //update the validation number in merchant_request table
        $postDataArray['id'] = $merchant_request_id;
        $postDataArray['validation_number'] = $payment_transaction_number;
        $this->updateRequestInfo($postDataArray);


        //split the amount; debit it from the collection account and credit it in the virtual_accounts
        $accountingArr = $accountingObj->getPaymentAccounts($pfmTransactionDetails, $to_account_arr); //will return the merchant's collection account
        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($accountingArr, 'epUpdateAccounts', array('validation_no' => $validationNo)));

        // $array_track[$merchant_request_id] = array(0=>$payment_transaction_number,1=>$event->getReturnValue());
//    $this->track_merchant_transaction($array_track);
        
        return $payment_transaction_number;
    }


    public function credit_bank_collection_account($account_id, $amount, $description, $validationNo) {
        $attrArray = array();
        $accountingObj = new pay4meAccounting();
        $is_clear = 1;
        
        $attr = $accountingObj->getAccountingHolderObj($description, $amount, $account_id, $is_clear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
        $attrArray[] = $attr;

        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts', array('validation_no' => $validationNo)));
        return $payment_transaction_number = $event->getReturnValue();
    }

    public function debit_ewallet_collection_account($split_array, $description, $validationNo) {
        $attrArray = array();
        $accountingObj = new pay4meAccounting();
        $is_clear = 1;
        foreach ($split_array as $k => $v) {
            $attr = $accountingObj->getAccountingHolderObj($description, $v['amount'], $v['account_id'], $is_clear, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
            $attrArray[] = $attr;
        }

        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts', array('validation_no' => $validationNo)));
        return $payment_transaction_number = $event->getReturnValue();
    }

    public function updateRequestInfo($postDataArray) {
        return Doctrine::getTable('MerchantRequest')->updateMerchantRequest($postDataArray);
    }

    public function updateEwalletConsolidationAcct($split_array, $wallet_account_id) {

        // $sfGuardUserObj = sfContext::getInstance()->getUser()->getGuardUser();
        //id = $sfGuardUserObj->getId();
        //  $ewallet_user_id = $sfGuardUserObj->getId();
        // $userObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($wallet_account_id);
        // $ewallet_user_id = $userObj->getFirst()->getUserId();
//    $userObj = Doctrine::getTable('UserDetail')->findByMasterAccountId($wallet_account_id);
//    $ewallet_user_id = $userObj->getFirst()->getUserId();
        foreach ($split_array as $payment_array) {
            $amount_paid = $payment_array['amount'];
            $collection_acct_id = $payment_array['account_id'];
            ////update the ewallet user consolidation acct
            $walletObj = Doctrine::getTable('EwalletUserAcctConsolidation')->updatePayment($wallet_account_id, $collection_acct_id, $amount_paid);
        }
    }

    public function chk_balance($total_payable_amount, $wallet_account_id) {

        $walletObj = new EpAccountingManager();
        $acctObj = $walletObj->getAccount($wallet_account_id);
        // print $total_payable_amount;exit;
//    $userObj = Doctrine::getTable('UserDetail')->findByMasterAccountId($wallet_account_id);
//    $user_id = $userObj->getFirst()->getUserId();
//    $balanceObj = Doctrine::getTable('EwalletUserAcctConsolidation')->getAccountBalance($user_id);
        if ($acctObj) {
            // print $amount_paid;exit;
            $total_deposited = $acctObj->getBalance();
            if ($total_deposited >= $total_payable_amount) {
                return 1;
            }
            return null; //Insufficient account balance
        }return null;
    }

    public function getWalletCollectionAccounts($payable_amount, $wallet_account_id) {
        //proceed for splitting
        //check if any deposit has balance greater than the amount to be paid
        //print $wallet_account_id;
        //  $userObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($wallet_account_id);
        //  $user_id = $userObj->getFirst()->getUserId();
        $payorAccountObj = Doctrine::getTable('EwalletUserAcctConsolidation')->getPayorAcct($payable_amount, $wallet_account_id);
        if ($payorAccountObj) {
            $i = 0;
            foreach ($payorAccountObj as $accountObj) {
                $account_balance = $accountObj->getAmount();
                $split_collection[$i]['amount'] = $payable_amount;
                $split_collection[$i]['account_id'] = $accountObj->getCollectionAccountId();
                $i++;
            }
        } else {
            $acctObj = Doctrine::getTable('EwalletUserAcctConsolidation')->getCollectionAccounts($wallet_account_id);
            $bal = $payable_amount;
            $i = 0;
            foreach ($acctObj as $accountObj) {
                $account_balance = $accountObj->getAmount();
                if ($bal > 0) {
                    if (($bal >= $account_balance)) {
                        $split_collection[$i]['amount'] = $account_balance;
                        $split_collection[$i]['account_id'] = $accountObj->getCollectionAccountId();
                    } else {
                        $split_collection[$i]['amount'] = $bal;
                        $split_collection[$i]['account_id'] = $accountObj->getCollectionAccountId();
                    }
                    $bal = $bal - $account_balance;
                }


                $i++;
            }
        }
//    print "<pre>";
//    print_r($split_collection);//exit;
        return $split_collection;
    }

    public function track_merchant_transaction(array $track_array) {
        //    print "<pre>";
        //    print_r($track_array);
        foreach ($track_array as $key => $value) {
            $merchant_request_id = $key;
            foreach ($value as $validation_number) {

                $trackObj = new MerchantPaymentTransaction();
                $trackObj->setMerchantRequestId($merchant_request_id);
                $trackObj->setValidationNumber($validation_number);
                //$trackObj->setIsProcessed($validation_number);
                $trackObj->save();
            }
        }
    }

    public function updateEwalletAccount($collection_account_id, $toAcct, $amount, $entry='credit', $service_charge=0) {
        if ($service_charge) {
            $amount = $amount - $service_charge;
        } 
        $userObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($toAcct);
        $user_id = $userObj->getFirst()->getUserId();
        $currency_id = $userObj->getFirst()->getCurrencyId();
        $recordObj = Doctrine::getTable('EwalletUserAcctConsolidation')->fetchDetails($toAcct, $collection_account_id, $currency_id);
        if (!$recordObj) {
            $walletObj = new EwalletUserAcctConsolidation();
            $walletObj->setEwalletAccountId($toAcct);
            $walletObj->setCollectionAccountId($collection_account_id);
            $walletObj->setCurrencyId($currency_id);
            $walletObj->setUserId($user_id);
        } else {
            $id = $recordObj->getFirst()->getId();
            $walletObj = Doctrine::getTable('EwalletUserAcctConsolidation')->find(array($id));
            $current_amount = $recordObj->getFirst()->getAmount();
            if ($entry == 'credit') {
                $amount = $current_amount + $amount;
            } else if ($entry == 'debit') {
                $amount = $current_amount - $amount;
            }
        }
        $walletObj->setAmount($amount);
        $walletObj->save();
    }

    public function createExclusivePaymentFile($txnId) {
        $logObj = new pay4meLog();
        $path = $logObj->getLogPath("PaymentTransactions");
        $fileName = "Payment-" . $txnId;
        $file_name = $path . "/" . ($fileName . '-' . date('Y_m_d_H:i:s')) . ".txt";
        $fp = fopen($file_name, "r+");
    }

    public function getRandomAccountNumber() {
        return rand(1, 5) . '' . rand('100', '1000') . '' . rand('100', '10000');
    }

    public function generateAccountNumber() {
        do {
            $account_number = $this->getRandomAccountNumber();
            $epAccObj = new EpAccountingManager;
            $duplicacy_account_number = $epAccObj->chkAccountNumber($account_number);
        } while ($duplicacy_account_number);
        return $account_number;
    }

    public function auditRecharge($ewallet_number) {
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED;
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED,
                        EpAuditEvent::getFomattedMessage($msg, array('account_number' => $ewallet_number)));

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
    public function auditRechargeCheck($ewallet_number) {
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED;
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_CHECK,
                        EpAuditEvent::getFomattedMessage($msg, array('account_number' => $ewallet_number)));
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
    public function auditRechargeDraft($ewallet_number) {
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED;
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_DRAFT,
                        EpAuditEvent::getFomattedMessage($msg, array('account_number' => $ewallet_number)));
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function getRechargeCollectionAccount($currency_id) {
        $configuration_obj = configurationServiceFactory::getService(sfConfig::get('sf_environment'));
        $bUser = $configuration_obj->getUserObj();
        $attrArray = array();
        if ($bUser) {

            $bankUser = $bUser->getFirst();

            if ($bankUser->getBankId()) {
                $bankObj = $bankUser->getBank();
                $bank_id = $bankObj->getId();
                $payment_mode_option_id = 4;
                $accountingObj = new pay4meAccounting;
                return $collection_account_id = $accountingObj->getCollectionAccount($payment_mode_option_id, $bank_id, NULL, 'Recharge From Bank', $currency_id);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function checkMerhantRequestPaymentModes($merchantServiceId) {
        return $checkPaymentMode = Doctrine::getTable('MerchantRequestPaymentMode')->checkMerhantRequestPaymentModes($merchantServiceId);
    }

    public function getMerchantRequestPaymentMode($merchantServiceId) {

        $getPaymentMode = Doctrine::getTable('MerchantRequestPaymentMode')->getPaymentMode($merchantServiceId);

        $modeMode = array();
        if ($getPaymentMode) {
            foreach ($getPaymentMode as $paymode) {
                $payment_mode_name = $paymode->getPaymentModeOption()->getPaymentMode()->getName();
                $payment_mode_display_name = $paymode->getPaymentModeOption()->getPaymentMode()->getDisplayName();
                if ($payment_mode_name != "") {
                    $pay_arr = strtolower(str_replace(" ", "_", strip_tags($payment_mode_name)));

                    if ($payment_mode_display_name != "") {
                        $modeMode[0][$pay_arr] = $payment_mode_display_name;
                    } else {
                        $modeMode[0][$pay_arr] = $payment_mode_name;
                    }

                    $payment_mode_option_name = $paymode->getPaymentModeOption()->getName();
                    $payment_mode_arr = strtolower(str_replace(" ", "_", $payment_mode_option_name));

                    if ($payment_mode_display_name != "") {
                        $modeMode[$pay_arr][$payment_mode_arr] = $payment_mode_display_name;
                    } else {
                        $modeMode[$pay_arr][$payment_mode_arr] = $payment_mode_name;
                    }
                }
            }
        }
        return $modeMode;
    }

    private function checkSplit($merchantReqId) {
        $splitObj = Doctrine::getTable('Pay4meSplit')->getRevenueSplitAccounts($merchantReqId);
        if ($splitObj)
            return true;
        else
            return false;
    }
    /*
     * Function to update ewallet_type , openidauth and email in user_detail table
     */
    public function updateEwalletType($userId,$type,$openIdAuth,$email){
        $updateUser = Doctrine::getTable('UserDetail')->updateEwalletType($userId,$type,$openIdAuth,$email);
    }

     /*
     * Function to update username in sfgaurduser table in case of recover ewallet account
     */
    public function updateUserName($id,$email){
        $updateUser = Doctrine::getTable('SfGuardUser')->updateUserName($id,$email);
    }
    
    /*
     * Function to validate open_authid sent by openid server with in our db
     */
    public function authValidation($userId,$openauthId){
        $authCount = Doctrine::getTable('UserDetail')->authValidation($userId,$openauthId);
        if($authCount)
            return true;
        return false;

    }

    /*
     * Function to check if ewallet user is valid for which openId server has verified details
     */
    public function OpenIdValidUser($userData){
        $authCount = Doctrine::getTable('sfGuardUser')->OpenIdValidUser($userData);
        if($authCount)
            return true;
        return false;
        
    }

    /*
     * Function to check ewallet_type in case of email id does not exist.
     */
    public function OpenIdAssociateValidUser($uname){
        $ewalletType = Doctrine::getTable('sfGuardUser')->OpenIdAssociateValidUser($uname);
        return $ewalletType;

    }


    /*
     * Function to fetch openIdAuth type
     */
    public function openIdAuthType($openIdAuth){
        if(strpos($openIdAuth,'google') !== false)
            $openIdAuth = 'Google';
        else if(strpos($openIdAuth,'yahoo') !== false)
            $openIdAuth = 'Yahoo';
        else if(strpos($openIdAuth,'facebook') !== false)
            $openIdAuth = 'Facebook';
        else
            $openIdAuth = 'Open Id';
        return $openIdAuth;
    }
}
?>
