<?php
class pin{
    function getPin($userId,$name,$emailAddress)
    {
        $pin = substr(rand(),0,4);
        Doctrine::getTable('EwalletPin')->setPin($this->getEncryptedPin($pin),$userId);
        $partialName='ewalletPinMail';
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $taskId = EpjobsContext::getInstance()->addJob('SendEwalletPinMail',"ewallet_pin/sendPinMail", array('pin'=>$pin,'partialName'=>$partialName,'email'=>$emailAddress,'name'=>$name));
        //$this->logMessage("sceduled mail job with id: $taskId", 'debug');
        //        echo 'done';
        //        exit;
    }
    function isActive($userId="")
    {
        if(Settings::isEwalletPinActive())
        {
            $ewalletDetail=$this->getEwalletDetail($userId);
            $status= $ewalletDetail->getStatus();
            $forced_pin_change = $ewalletDetail->getForcedPinChange();
            $endDate=Settings::getGracePeriodEndDate();
            $currentDate=date("Y-m-d");
            $gracePeriod=$this->dateDiff($currentDate, $endDate,1); 
            if(($status=='active') || ($status=='inactive' && $gracePeriod==0 && $forced_pin_change=='0') || ($status=='inactive' && $forced_pin_change=='1') || ($status=='blocked'))
            {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    function getEwalletDetail($userId="")
    {
      if($userId == "") {
        $userId=sfContext::getInstance()->getUser()->getGuardUser()->getId();
      }
      $ewalletDetail= Doctrine::getTable('EwalletPin')->findByUserId(array($userId))->getFirst();
      return $ewalletDetail;
    }
    function getEncryptedPin($pin)
    {
        return md5($pin);
    }
    function sendBlockedMail($userId)
    {
        $partialName='ewalletPinBlockedMail';
        if($userId == "") {
          $userId=sfContext::getInstance()->getUser()->getGuardUser()->getId();
        }
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        return $taskId = EpjobsContext::getInstance()->addJob('SendEwalletPinBlockedMail',"ewallet_pin/sendPinBlockedMail", array('partialName'=>$partialName,'userId'=>$userId));
    }
    public function sendGracePeriodMail()
    {
        $endDate=Settings::getGracePeriodEndDate();
        $partialName='ewalletGracePeriodMail';
        $dateArray=explode("-",$endDate);
        $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$dateArray['1'],$dateArray['2']-7,$dateArray['0']));
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        return $taskId = EpjobsContext::getInstance()->addJobForEveryDay('SendGracePeriodMail', 'ewallet_pin/sendGracePeriodMail', $endDate." 00:00:00", array('partialName'=>$partialName), null, null, $startDate,"12","00");

    }

    //Returns the time difference between two given dates.
    private function dateDiff($time1, $time2, $precision = 6)
    {
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }
        //        if ($time1 > $time2) {
        //            $ttime = $time1;
        //            $time1 = $time2;
        //            $time2 = $ttime;
        //        }
        //$intervals = array('year','month','day','hour','minute','second');
        $intervals = array('day');
        $diffs = array();
        foreach ($intervals as $interval) {
            $diffs[$interval] = 0;
            $ttime = strtotime("+1 " . $interval, $time1);
            while ($time2 >= $ttime) {
                $time1 = $ttime;
                $diffs[$interval]++;
                $ttime = strtotime("+1 " . $interval, $time1);
            }
        }
        $count = 0;
        $times = array();
        foreach ($diffs as $interval => $value) {
            if ($count >= $precision) {
                break;
            }
            if ($value > 0) {
                if ($value != 1) {
                    $interval .= "s";
                }
                //$times[] = $value . " " . $interval;
                $times[] = $value;
                $count++;
            }
        }
        if(count($times))
        return implode(", ", $times);
        return 0;
    }


  /**
   * Check if the eWallet Pin entered by the user is valid
 * @return <string> - error msg if invalid, 1 if valid
   */

    public function validatePin($pin,$user_id='') {
       // $pinObj=new pin();
       if($pin == "") {
         return "Please enter Pin";
       }
        $ewalletDetail=$this->getEwalletDetail($user_id);
        if($pin && ($ewalletDetail->getStatus()=='blocked')) {
          return "Pin has been blocked by the administrator";
        }
        else if ($ewalletDetail->getStatus()=='inactive') {
          return "Please generate new Pin";
        }
        else if(isset($pin) && !($this->getEncryptedPin($pin)==$ewalletDetail->getPinNumber()))
        {
            $ewalletDetail->set('no_of_retries',$ewalletDetail->getNoOfRetries()+1);
            if($ewalletDetail->getNoOfRetries()==Settings::getMaxNoOfRetries())
            {
                $ewalletDetail->set('status','blocked');
                //send mail to user
                $taskId= $this->sendBlockedMail($user_id);

            }
            $ewalletDetail->save();
             if($ewalletDetail->getNoOfRetries()==Settings::getMaxNoOfRetries())
             {
                    return 'Pin has been blocked by the administrator';
             }
            return "Please enter correct Pin. Post 5 unsuccessful attempts the Pin will be blocked.";
//            echo $ewalletDetail->getNoOfRetries();
           

            //$this->redirect('bill/show/trans_num/'.$request->getParameter('txnId'));
        }
        return 1;
    }

}

?>
