<?php
  class nisManager implements nisService
  {
    //function to get the SP Details
    public function getServiceDetails($transactionId)
    {
        try{
            $serviceDetails = MerchantRequestDetailsTable::getDetailsByTransaction($transactionId);

            if(count($serviceDetails)>0){
                $serviceDetailsArray = array(
                                                'appId' => $serviceDetails[0]['iparam_one'],
                                                'refNo' => $serviceDetails[0]['iparam_two'],
                                                'type' => $serviceDetails[0]['sparam_one'],
                                                'name' => $serviceDetails[0]['name'],
                                                'email' => $serviceDetails[0]['sparam_three'],
                                                'mobNo' => $serviceDetails[0]['sparam_two']
                                            );
                return $serviceDetailsArray;
            }else{
                return false;
            }
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function savePayment()
    {
        savePayForMePayment() ;

        saveNisPayment() ;
        
    }

    public function savePayForMePayment()
    {
        
    }
    public function saveNisPayment()
    {        
        $nisIntegrationObj = nisServiceIntegration::saveNisPayment() ;
        return $nisIntegrationObj->setPayment() ;
    }

    //function to get the validation rules acc to mer svc id
    public function getValidationRules($merchantSvcId)
    {
      $getData = Doctrine::getTable('ValidationRules')->getRulesById($merchantSvcId);
      return $getData;
    }

    public function checkMerchantTxnNo($transactionNo)
    {
      $getDataArr = Doctrine::getTable('Transaction')->checkTxnMerchant($transactionNo);
      return $getDataArr;
    }
    
  }
?>
