<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GatewayStatusMessages
{

 /**
   * This method return the status message for Interswitch Payment Gateway according to $code parameter.
   * This method calls getStatusMessage($code) method which exists in epInterswitchPlugin's StatusMessages class
   * @param string $code Status code.
   * @return string Status Message
  */
  public static function getInterswitchStatusMessage($code)
  {
    $statusMessagesObj = new InterswitchStatusMessages();
    return $statusMessages = $statusMessagesObj->getStatusMessage($code);
  }

 /**
   * This method return the status message for Etranzact Payment Gateway according to $code parameter.
   * This method calls getStatusMessage($code) method which exists in epEtranzactPlugin's StatusMessages class
   * @param string $code Status code.
   * @return string Status Message
  */
  public static function getEtranzactStatusMessage($code)
  {
    $statusMessagesObj = new EtranzactStatusMessages();
    return $statusMessages = $statusMessagesObj->getStatusMessage($code);
  }

  public static function getVbvStatusMessage($code)
  {
    $statusMessagesObj = new vbvStatusMessages();
    return $statusMessages = $statusMessagesObj->getStatusMessage($code);
  }
}
?>
