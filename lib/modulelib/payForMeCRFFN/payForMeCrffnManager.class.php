<?php
  class payForMeCrffnManager implements payForMeCrffnService
  {
    public function getCrffnDetails($transactionId)
    {
      $nisDetails = array();

      $payForMeManagerObj = payForMeServiceFactory::getService('crffn');
      
      $txnId = $payForMeManagerObj->getTransactionDetails($transactionId);
      
      $nisDetails = array_merge($nisDetails,$txnId);

      $appCharge = $payForMeManagerObj->getService($transactionId);
      $nisDetails = array_merge($nisDetails,$appCharge);

      $pfmTransactionDetails = PfmTransactionTable::getTransactionDetails($transactionId);

      $serviceType = ServiceDetailsTable::getServiceType($pfmTransactionDetails[0]['id']);
      
//      $bankCharge = $payForMeManagerObj->getBank($serviceType[0]['service_type_id']);
      $nisDetails = array_merge($nisDetails,$bankCharge);

      $bankCharges = $payForMeManagerObj->getTotal($appCharge, $bankCharge);
      $nisDetails = array_merge($nisDetails,$bankCharges);

      $nisManagerObj = nisServiceFactory::getService('nis');
      
      $nisServiceDetails = $nisManagerObj->getServiceDetails($transactionId);
      $nisDetails = array_merge($nisDetails,$nisServiceDetails);
      return $nisDetails;
    }
   
  }
?>
