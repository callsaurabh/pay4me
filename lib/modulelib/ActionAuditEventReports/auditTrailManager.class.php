<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of auditTrailManagerclass
 *
 * @author vrai
 */
class auditTrailManager {

    //method to get the bank list
    //below method access by session management as well as audit report
    public function getBankList($bank_list=null) {
        $getBankArray = Doctrine::getTable('Bank')->getBankUserList();
        $arr = $getBankArray->toArray();
        if ($bank_list) {
            $bank_array = $bank_list;
        } else {
            $bank_array = array();
        }
        foreach ($arr as $k => $v) {
            $bank_array[$v['id']] = $v['bank_name'];
        }
        return $bank_array;
    }

    //method to get the country list of bank
    public function getCountryList($bankId, $countryId="") {
        $getBankArray = Doctrine::getTable('BankConfiguration')->getAllCountry($bankId, $countryId);
        return $getBankArray;
    }

    //method to get the branch list
    public function getBranchList($bankId, $countryId="") {
        $getBranchArray = Doctrine::getTable('BankBranch')->getAllBranches($bankId, $countryId);
        return $getBranchArray;
    }

    //method to create the query
    public function getDataForReport($sDate, $eDate, $userDetails, $bank="", $branch="", $category="", $subcategory="", $username="", $selectBy="", $userId="", $ivalue="", $allowedGroupIds=array()) {

        //for portal admin get all data
        $getArrayPaging = '';
        $getArrayPaging = Doctrine::getTable('EpActionAuditEvent')->getSearchData($sDate, $eDate, $bank, $branch, $category, $subcategory, $username, $selectBy, $userId, $ivalue, $allowedGroupIds);

        return $getArrayPaging;
    }

    public function getAllowedGroupIds($groupName) {
        $groupIds = Array();
        $allowedGroupNames = Array();

        if ($groupName == sfConfig::get('app_pfm_role_bank_country_head') ||
                $groupName == sfConfig::get('app_pfm_role_country_report_user')) {
            $allowedGroupNames = Array(
                sfConfig::get('app_pfm_role_bank_branch_report_user'),
                sfConfig::get('app_pfm_role_bank_branch_user'));
        } elseif ($groupName == sfConfig::get('app_pfm_role_bank_admin') ||
                $groupName == sfConfig::get('app_pfm_role_bank_e_auditor') ||
                $groupName == sfConfig::get('app_pfm_role_report_bank_admin')) {


            $allowedGroupNames = Array(
                sfConfig::get('app_pfm_role_bank_branch_report_user'),
                sfConfig::get('app_pfm_role_bank_branch_user'),
                sfConfig::get('app_pfm_role_bank_country_head'),
                sfConfig::get('app_pfm_role_country_report_user'));
        }


        if (count($allowedGroupNames) > 0) {
            $groupObj = Doctrine::getTable('sfGuardGroup')->getGroupId($allowedGroupNames);
            if ($groupObj) {
                foreach ($groupObj as $val) {
                    $groupIds[] = $val->getId();
                }
            }
        }
        if (count($groupIds))
            return $groupIds;
        else
            return false;
    }

}

?>
