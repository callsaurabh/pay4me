<?php

class pay4meAccounting {

    public function isAuthenticated() {
        if ($this->sfGuardUserObj) {
            return true;
        }
        return false;
    }

    public function getUser() {
        return $this->sfGuardUserObj;
    }

    public function generateAccountNumber() {
        do {
            $account_number = $this->getRandomNumber();
            $walletObj = new EpAccountingManager;
            $duplicacy_account_number = $walletObj->chkAccountNumber($account_number);
        } while ($duplicacy_account_number);
        return $account_number;
    }

    public function getRandomNumber() {
        return rand(1, 5) . '' . rand('100', '1000') . '' . rand('100', '10000');
    }

    public function getCollectionAccount($payment_mode_option_id='', $bank_id='', $merchant_id='', $transfer_type='Payment', $currency_id=1) {
        $collectionAccountObj = Doctrine::getTable('ServiceBankConfiguration')->getMasterAcount($payment_mode_option_id, $bank_id, $merchant_id, $transfer_type, $currency_id);

        if ($collectionAccountObj) {
            if ($collectionAccountObj->getFirst()) {
                $collection_account_id = $collectionAccountObj->getFirst()->getMasterAccountId();
                if ($collection_account_id != "") {
                    return $collection_account_id;
                }
                return null;
            }else
                return null;
        }return null;
    }

    public function getAccountingHolderObj($description, $amount, $account_id, $isCleared, $entry_type, $transaction_type, $accountant_id="") {



        return $attr = new EpAccountingAttributeHolder($description, $amount, $account_id, $isCleared, $entry_type, $transaction_type, $accountant_id);
    }

    public function getAccountDetails($amount, $ewallet_account_id, $account_number, $currency_id=1,$pmo) {

        //   $description = EpAccountingDescription::$CHARGE_WALLET;
        $configuration_obj = configurationServiceFactory::getService(sfConfig::get('sf_environment'));
        $bUser = $configuration_obj->getUserObj();
        $attrArray = array();
        if ($bUser) {

            $bankUser = $bUser->getFirst();

            if ($bankUser->getBankId()) { //if the user accepting payment is valid bank_user or bank_admin
                $bankObj = $bankUser->getBank();
                $bank_name = $bankObj->getBankName();
                $bank_id = $bankObj->getId();
                $payment_mode_option_id = 4;
                $collection_account_id = $this->getCollectionAccount($payment_mode_option_id, $bank_id, NULL, 'Recharge From Bank', $currency_id);
                #sfLoader::loadHelpers('ePortal');
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
                $amount_formatted = format_amount($amount, $currency_id, 1);
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$CHARGE_WALLET, array('account_number' => $account_number, 'bank_name' => $bank_name, 'amount' => $amount_formatted,'payment_mode_option'=>$pmo));


                if ($collection_account_id) {
                    $attr = $this->getAccountingHolderObj($description, $amount, $collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }
            }
        }

        //fetch the master account id of user getting his wallet charged
        $attr = $this->getAccountingHolderObj($description, $amount, $ewallet_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
        $attrArray[] = $attr;   //    }


        if (count($attrArray)) {
            return $attrArray;
        } else {
            return null;
        }
    }

    public function updateEwalletAccount($transaction_no) {
        $transactionArr = Doctrine::getTable('Transaction')->getTransactionDetails($transaction_no);
        $merchant_request_id = $transactionArr['MerchantRequest']['id'];
        #sfLoader::loadHelpers('ePortal');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $amount_collected = convertToKobo($transactionArr['MerchantRequest']['paid_amount']);
        $fi_amount = convertToKobo($transactionArr['MerchantRequest']['bank_amount']); //financial_institution charges

        $payment_mode_option_id = $transactionArr['MerchantRequest']['payment_mode_option_id'];
        $payment_mode_option = $transactionArr['MerchantRequest']['PaymentModeOption']['name'];
        $currency = $transactionArr['MerchantRequest']['currency_id'];
        $validation_no = $transactionArr['MerchantRequest']['validation_number'];

        $attrArray = array();
        $user = sfContext::getInstance()->getUser();


        $merchant = $transactionArr['MerchantRequest']['Merchant']['name'];
        $merchant_service = $transactionArr['MerchantRequest']['MerchantService']['name'];
        $payment_mode_option = $transactionArr['MerchantRequest']['PaymentModeOption']['name'];
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_WALLET, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));

        //  $description = EpAccountingDescription::$PAY_FROM_WALLET;
        //get logged in user account_id
        if ($user->isAuthenticated()) {
            $userAccountCollectionObj = new UserAccountManager();
            $ewallet_account_id = $userAccountCollectionObj->getAccountIdByCurrency($currency);

            // $sfGuardUserObj = $user->getGuardUser();
            // $userId = $sfGuardUserObj->getId();
            // $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
            if ($ewallet_account_id) {
                //   $ewallet_account_id = $user_detail->getFirst()->getMasterAccountId();
                $attr = $this->getAccountingHolderObj($description, $amount_collected, $ewallet_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                $attrArray[] = $attr;
            }
        }
        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts', array('validation_no' => $validation_no)));
    }

    public function getPaymentAccounts($transactionArr, $toAccountArr="") {
        $merchant_request_id = $transactionArr['MerchantRequest']['id'];
        #sfLoader::loadHelpers('ePortal');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $amount_collected = convertToKobo($transactionArr['MerchantRequest']['paid_amount']);
        $fi_amount = convertToKobo($transactionArr['MerchantRequest']['bank_amount']); //financial_institution charges

        $payment_mode_option_id = $transactionArr['MerchantRequest']['payment_mode_option_id'];
        $payment_mode_option = $transactionArr['MerchantRequest']['PaymentModeOption']['name'];
        $currency = $transactionArr['MerchantRequest']['currency_id'];

        $attrArray = array();
        $user = sfContext::getInstance()->getUser();


        $merchant = $transactionArr['MerchantRequest']['Merchant']['name'];
        $merchant_service = $transactionArr['MerchantRequest']['MerchantService']['name'];
//    $payment_mode_option = $transactionArr['MerchantRequest']['PaymentModeOption']['name'];

        $pfm_helper_obj = new pfmHelper();
        $payment_mode_option = $pfm_helper_obj->getPMONameByPMId($transactionArr['MerchantRequest']['payment_mode_option_id']);
            
        switch ($payment_mode_option_id) {
            case 4:                             //eWallet
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_WALLET, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));

                //  $description = EpAccountingDescription::$PAY_FROM_WALLET;
                //get logged in user account_id
                /*  if($user->isAuthenticated()) {
                  $userAccountCollectionObj = new UserAccountManager();
                  $ewallet_account_id = $userAccountCollectionObj->getAccountIdByCurrency($currency);

                  // $sfGuardUserObj = $user->getGuardUser();
                  // $userId = $sfGuardUserObj->getId();
                  // $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
                  if($ewallet_account_id) {
                  //   $ewallet_account_id = $user_detail->getFirst()->getMasterAccountId();
                  $attr = $this->getAccountingHolderObj($description, $amount_collected, $ewallet_account_id, $isCleared=1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                  $attrArray[] = $attr;
                  }
                  } */
                break;

            case 1:

            case 14:

            case 15:  ////Bank
                //get the merchant accounts to update
                $bank_name = $transactionArr['MerchantRequest']['bank_name'];
                switch ($payment_mode_option_id) {
                    case 1:
                        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_BANK, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $bank_name));
                        break;
                    case 14:
                        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_CHECK, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $bank_name));
                        break;
                    case 15:
                        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_BANKDRAFT, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option, 'bank_name' => $bank_name));

                        break;
                }
                $bank_id = $transactionArr['MerchantRequest']['bank_id'];
                $merchant_id = $transactionArr['MerchantRequest']['merchant_id'];
                $merchant_collection_account_id = $this->getCollectionAccount($payment_mode_option_id, $bank_id, $merchant_id, 'Payment', $currency);
                if ($merchant_collection_account_id != "") {
                    if (($fi_amount > 0) && ($bank_id != '')) {
                        $accountObj = Doctrine::getTable('FinancialInstitutionAcctConfig')->getMasterAccount($bank_id, $merchant_id, $currency);
                        if ($accountObj) {
                            $bank_account_id = $accountObj->getFirst()->getAccountId();
                            $attr = $this->getAccountingHolderObj($description, $fi_amount, $bank_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                            $attrArray[] = $attr;

                            $attr = $this->getAccountingHolderObj($description, $fi_amount, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                            $attrArray[] = $attr;

                            $amount_collected = $amount_collected - $fi_amount;
                        }
                    }



                    $attr = $this->getAccountingHolderObj($description, $amount_collected, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }



                //transafer the financial institution charges to it
                //        if(($fi_amount > 0) && ($bank_id!='')) {
                //             $accountObj = Doctrine::getTable('FinancialInstitutionAcctConfig')->getMasterAccount($bank_id,$merchant_id);
                //            if($accountObj) {
                //                $bank_account_id = $accountObj->getFirst()->getAccountId();
                //                $attr = $this->getAccountingHolderObj($description, $fi_amount, $bank_account_id, $isCleared=1, EpAccountingConstants::$ENTRY_TYPE_CREDIT);
                //                $attrArray[] = $attr;
                //            }
                //        }


                break;

            case 13:                       //Bank
                //get the merchant accounts to update
                $bank_name = $transactionArr['MerchantRequest']['bank_name'];

                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_MERCHANT_COUNTER, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));
                $bank_id = $transactionArr['MerchantRequest']['bank_id'];
                $merchant_id = $transactionArr['MerchantRequest']['merchant_id'];
                $merchant_collection_account_id = $this->getCollectionAccount($payment_mode_option_id, $bank_id, $merchant_id);
                if ($merchant_collection_account_id != "") {
                    if (($fi_amount > 0) && ($bank_id != '')) {
                        $accountObj = Doctrine::getTable('FinancialInstitutionAcctConfig')->getMasterAccount($bank_id, $merchant_id);
                        if ($accountObj) {
                            $bank_account_id = $accountObj->getFirst()->getAccountId();
                            $attr = $this->getAccountingHolderObj($description, $fi_amount, $bank_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                            $attrArray[] = $attr;

                            $attr = $this->getAccountingHolderObj($description, $fi_amount, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                            $attrArray[] = $attr;

                            $amount_collected = $amount_collected - $fi_amount;
                        }
                    }

                    $attr = $this->getAccountingHolderObj($description, $amount_collected, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }

                break;


            case 5:                             //VISA
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_VBV, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));
                $merchant_collection_account_id = $this->getCollectionAccount($payment_mode_option_id);
                if ($merchant_collection_account_id != "") {
                    $attr = $this->getAccountingHolderObj($description, $amount_collected, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }
                break;


            case 2:                             //INTERSWITCH
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_INTERSWITCH, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));
                $merchant_collection_account_id = $this->getCollectionAccount($payment_mode_option_id);
                if ($merchant_collection_account_id != "") {
                    $attr = $this->getAccountingHolderObj($description, $amount_collected, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }
                break;

            case 3:                             //ETRANZACT
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_ETRANZACT, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));
                $merchant_collection_account_id = $this->getCollectionAccount($payment_mode_option_id);
                if ($merchant_collection_account_id != "") {
                    $attr = $this->getAccountingHolderObj($description, $amount_collected, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }
                break;

            //bug id 19968
            case 16:                             //Internet bank
                $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_INTERNETBANK, array('merchant' => $merchant, 'merchant_service' => $merchant_service, 'payment_mode_option' => $payment_mode_option));
                $merchant_collection_account_id = $this->getCollectionAccount($payment_mode_option_id);
                if ($merchant_collection_account_id != "") {
                    $attr = $this->getAccountingHolderObj($description, $amount_collected, $merchant_collection_account_id, $isCleared = 1, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;
                }
                break;
        }
        //get Split Account array
        //   $isLowAmount = $transactionArr['MerchantRequest']['is_lowamount'];
        //    print "<pre>";
        //    print_r($transactionArr);
        //get the split accounts
        // $bank_split_details = 1;
        if (count($toAccountArr)) {
            $split_arr = $toAccountArr;
        } else {
            $split_arr = $this->getSplitAccounts($transactionArr, 0);
        }//print "<pre>";print_r($split_arr);
        if (count($split_arr)) {
            foreach ($split_arr as $key => $value) {
                $amount = convertToKobo($value['amount']);
                $attr = $this->getAccountingHolderObj($description, $amount, $value['account_id'], $isCleared = 1, $enter_type = EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                $attrArray[] = $attr;
            }
        }
        //
        //      print "<pre>";
        //   print_r($attrArray);exit;

        return $attrArray;
    }

    public function getSplitAccounts($transactionArr, $bank_split_details_reqd=NULL) {
        $merchant_id = $transactionArr['MerchantRequest']['merchant_id'];
        $merchant_service_id = $transactionArr['MerchantRequest']['merchant_service_id'];
        $pay4meAmount = $transactionArr['MerchantRequest']['payforme_amount'];
        $fi_amount = $transactionArr['MerchantRequest']['bank_amount']; //financila_institution charges
        $amount = $transactionArr['MerchantRequest']['paid_amount'];
        $fi_id = $transactionArr['MerchantRequest']['bank_id'];
        $item_fee = $transactionArr['MerchantRequest']['item_fee'];
        $currency = $transactionArr['MerchantRequest']['currency_id'];



        $acctArray = array();
        $split_obj = Doctrine::getTable('SplitEntityConfiguration')->findByMerchantServiceId($merchant_service_id);
        foreach ($split_obj as $splitObj) {
            $SplitCurrencyId = '';
            $split_acct_config_id = $splitObj->getSplitAccountConfigurationId();
            if ($split_acct_config_id != '')
                $SplitCurrencyId = $splitObj->getSplitAccountConfiguration()->getCurrencyId();
            if (($split_acct_config_id == '') || $currency == $SplitCurrencyId) {
                $split_acct_config_id = $splitObj->getSplitAccountConfigurationId();
                //calculate the amount received by account
                $split_type_id = $splitObj->getSplitTypeId();
                $splitTypeObj = Doctrine::getTable('SplitType')->findById($split_type_id);
                $split_type_header = $splitTypeObj->getFirst()->getSplitName();

                if ($split_acct_config_id != "") {
                    //find master_account_id
                    $splitAccountObj = Doctrine::getTable('SplitAccountConfiguration')->findById($split_acct_config_id);
                    $splitAccountObj->getFirst()->getMasterAccountId();
                    if ($splitAccountObj->getFirst()->getMasterAccountId()) {
                        $receiving_account_arr[] = $splitAccountObj->getFirst()->getMasterAccountId(); //account which gets the amount
                        $split_type[] = $split_type_header;
                        $amount_arr[] = $splitObj->getCharge();
                    }
                } else if (($split_acct_config_id == "") && ($split_type_header == "Financial Institution") && ($fi_amount > 0)) {  //
                    $accountObj = Doctrine::getTable('FinancialInstitutionAcctConfig')->getMasterAccount($fi_id, $merchant_id, $currency);

                    if ($accountObj) {
                        $receiving_account_arr[] = $accountObj->getFirst()->getAccountId();
                        $split_type[] = $split_type_header;
                        $amount_arr[] = $splitObj->getCharge();
                    }
                }
            }//if of split Currency
        }
        if (!isset($receiving_account_arr))
            throw New Exception("Receiving Account Is Not Set");


        $flag = 0; //low_waletmark is not set
        if (isset($split_type) && count($split_type)) {
            $isLowAmount = 0;
            //check for low watermark
            if (in_array('Low Watermark', $split_type)) {
                $key = array_keys($split_type, 'Low Watermark');
                foreach ($key as $value) {
                    $low_amount = $amount_arr[$value];
                    if ($low_amount >= $item_fee) {
                        $account_id = $receiving_account_arr[$value];
                        $acctArray[] = array("amount" => $item_fee, "account_id" => $account_id);
                        $flag = 1; //low watermark is set
                        //return $acctArray;
                    }
                }
            }

            $total_amount_paid = 0;
            if ($flag == 0) {
                if (in_array('Flat', $split_type)) {
                    $key = array_keys($split_type, 'Flat');
                    foreach ($key as $value) {
                        $account_id = $receiving_account_arr[$value];
                        $amount_flat = $amount_arr[$value];
                        $acctArray[] = array("amount" => $amount_flat, "account_id" => $account_id);

                        //         $attr = new EpAccountingAttributeHolder($description, $amount_flat, $account_id, $isCleared, $enter_type);
                        //         $attrArray[] = $attr;
                        $total_amount_paid = $total_amount_paid + $amount_flat;
                    }
                }

                if (in_array('Percentage', $split_type)) {
                    $key = array_keys($split_type, 'Percentage');
                    foreach ($key as $value) {
                        $account_id = $receiving_account_arr[$value];
                        $amount_per = ($amount_arr[$value] * $item_fee) / 100;
                        $acctArray[] = array("amount" => $amount_per, "account_id" => $account_id);
                        $total_amount_paid = $total_amount_paid + $amount_per;
                    }
                }

                if (in_array('Ministry', $split_type)) {
                    $key = array_keys($split_type, 'Ministry');
                    foreach ($key as $value) {
                        $account_id = $receiving_account_arr[$value];
                        $amount_ministry = $item_fee - $total_amount_paid;
                        $acctArray[] = array("amount" => $amount_ministry, "account_id" => $account_id);

                        //         $attr = new EpAccountingAttributeHolder($description, $amount_ministry, $account_id, $isCleared, $enter_type);
                        //         $attrArray[] = $attr;
                    }
                }
            }

            if (in_array('Pay4Me', $split_type)) {
                $key = array_keys($split_type, 'Pay4Me');
                foreach ($key as $value) {
                    $account_id = $receiving_account_arr[$value];
                    //          $split_amt = $amount-$fi_amount;
                    //          $amount_nibbs = sfConfig::get('app_nibss_svc_charge')/100;
                    //          $nibssPercent = ($amount_nibbs)/(1+$amount_nibbs);
                    //         $totalNibssAmount = $split_amt * $nibssPercent;
                    //         //ceil this amount
                    //         $totalNibssAmount  = ceil($totalNibssAmount*100)/100;
                    //$amount_pay4me = $pay4meAmount-$totalNibssAmount;//$totalNibssAmount;
                    $amount_pay4me = $pay4meAmount;
                    if ($amount_pay4me != "" && $amount_pay4me > 0) {
                        $acctArray[] = array("amount" => $amount_pay4me, "account_id" => $account_id);
                        //  $acctArray[] = array("amount"=>$totalNibssAmount,"account_id"=>1);
                        //         $attr = new EpAccountingAttributeHolder($description, $amount_pay4me, $account_id, $isCleared, $enter_type);
                        //         $attrArray[] = $attr;
                        // $total_amount_paid = $total_amount_paid+$amount_pay4me;
                    }
                }
            }


            if ($bank_split_details_reqd) {
                if (($fi_amount > 0) && ($fi_id != '')) {
                    if (in_array('Financial Institution', $split_type)) {
                        $key = array_keys($split_type, 'Financial Institution');
                        foreach ($key as $value) {
                            $account_id = $receiving_account_arr[$value];
                            $acctArray[] = array("amount" => $fi_amount, "account_id" => $account_id);
                            // $total_amount_paid = $total_amount_paid+$fi_amount;
                        }
                    }
                }
            }



            //    print "<pre>";
            //   print_r($acctArray);
            return $acctArray;
        }
        return null;
    }

    public function getRechargeAccounts($account_number, $ewallet_account_id, $total_amount_paid, $payment_mode_option_id, $isClear=1, $transfer_mode, $service_charge) {

        $attrArray = array();
        $collection_account_id = $this->getCollectionAccount($payment_mode_option_id, NULL, NULL, 'Recharge');
        #sfLoader::loadHelpers('ePortal');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));

        $paymentModeOptionObj = Doctrine::getTable('PaymentModeOption')->find($payment_mode_option_id);
        $payment_mode_option_name = $paymentModeOptionObj->getPaymentMode()->getDisplayName();

        if ($service_charge > 0) {
            $total_amount_paid = $total_amount_paid - $service_charge;
            $amount_formatted = format_amount($total_amount_paid, 1, 1);
            $service_charge_formatted = format_amount($service_charge, 1, 1);
            $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$SERVICE_CHARGE_RECHARGE, array('account_number' => $account_number, 'recharge_amount' => $amount_formatted, 'payment_mode_option' => $payment_mode_option_name, 'amount' => $service_charge_formatted));

            $attr = $this->getAccountingHolderObj($description, $service_charge, $collection_account_id, $isClear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, $transfer_mode);
            $attrArray[] = $attr;
        } else {
            $amount_formatted = format_amount($total_amount_paid, 1, 1);
        }
        $description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$RECHARGE_EWALLET, array('account_number' => $account_number, 'amount' => $amount_formatted, 'payment_mode_option' => $payment_mode_option_name));
        if ($collection_account_id) {
            $attr = $this->getAccountingHolderObj($description, $total_amount_paid, $collection_account_id, $isClear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, $transfer_mode);
            $attrArray[] = $attr;
        }

        $attr = $this->getAccountingHolderObj($description, $total_amount_paid, $ewallet_account_id, $isClear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, $transfer_mode);
        $attrArray[] = $attr;


        if (count($attrArray)) {
            return $attrArray;
        } else {
            return null;
        }
    }

    public function eWalletTransfer($fromAcct, $toAcct, $description, $amount, $transactionCode="") {
        //    sfLoader::loadHelpers('ePortal');
        //    $amount = convertToKobo($amount);
        $managerObj = new payForMeManager();
        if ($managerObj->chk_balance($amount, $fromAcct)) {
            $split_payment_array = $managerObj->getWalletCollectionAccounts($amount, $fromAcct);
            if ($split_payment_array) {
                $attrArray = array();
                $isClear = 1;
                $con = Doctrine_Manager::connection();

                try {
                    $con->beginTransaction();
                    $attr = $this->getAccountingHolderObj($description, $amount, $fromAcct, $isClear, EpAccountingConstants::$ENTRY_TYPE_DEBIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;

                    $attr = $this->getAccountingHolderObj($description, $amount, $toAcct, $isClear, EpAccountingConstants::$ENTRY_TYPE_CREDIT, EpAccountingConstants::$TRANSACTION_TYPE_DIRECT);
                    $attrArray[] = $attr;

                    //                print "<pre>";
                    //                print_r($attrArray);
                    if ($transactionCode != "") {
                        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts', array('validation_no' => $transactionCode)));
                    } else {
                        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts'));
                    }
                    $update_acct = $managerObj->updateEwalletConsolidationAcct($split_payment_array, $fromAcct);
                    foreach ($split_payment_array as $k => $v) {
                        $amount_update = $v['amount'];
                        $fromAcct = $v['account_id'];
                        $managerObj->updateEwalletAccount($fromAcct, $toAcct, $amount_update);
                    }
                    $con->commit();
                    return $transfer_transaction_number = $event->getReturnValue();
                } catch (Exception $e) {
                    $con->rollback();
                    throw $e;
                }
            }
        } else {
            return "Insufficient Account Balance!";
        }
    }

    public function doAccounting($accountingArr) {
        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($accountingArr, 'epUpdateAccounts'));
        return $payment_transaction_number = $event->getReturnValue();
    }

    public function getRevenueSplitAccounts($merchantReqId, $merchantServiceId, $pay4meAmount, $currencyId=1) {
        $accountArray = array();
        $splitObj = Doctrine::getTable('Pay4meSplit')->getRevenueSplitAccounts($merchantReqId);

        if ($splitObj) {
            foreach ($splitObj as $val) {
                $accountNumber = $val->getAccountNumber();
                $amount = $val->getAmount();
                $epMasterAccountObj = Doctrine::getTable('EpMasterAccount')->findByAccountNumber($accountNumber);
                $accountArray[] = Array('amount' => ($amount / 100), 'account_id' => $epMasterAccountObj->getFirst()->getId());
            }
        }
        if ($pay4meAmount != "" && $pay4meAmount > 0) {
            $accountArray[] = $this->getPay4meShare($merchantServiceId, $pay4meAmount, $currencyId);
        }
        // print "<pre>";print_r($accountArray);exit;
        if (count($accountArray)) {
            return $accountArray;
        } else {
            return null;
        }
    }

    private function getPay4meShare($merchantServiceId, $pay4meAmount, $currencyId=1) {
        $accountArray = array();
        $splitObj = Doctrine::getTable('SplitEntityConfiguration')->getPay4MeAccountInfo($merchantServiceId, $currencyId);
        if ($splitObj) {
            return $accountArray[] = Array('amount' => $pay4meAmount, 'account_id' => $splitObj->getFirst()->getSplitAccountConfiguration()->getMasterAccountId());
        }
        return 0;
    }

}

?>
