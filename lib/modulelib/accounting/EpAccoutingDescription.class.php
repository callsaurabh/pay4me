<?php
class EpAccountingDescription {
  //category
  public static $CHARGE_WALLET = "eWallet - '{account_number}' recharged with '{amount}' from '{bank_name} via {payment_mode_option}'";
  public static $RECHARGE_EWALLET = "eWallet - '{account_number}' recharged with '{amount}' via {payment_mode_option}";
  public static $PAY_FROM_WALLET = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'";
  public static $PAY_FROM_BANK = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}' - '{bank_name}' ";
  public static $PAY_FROM_MERCHANT_COUNTER = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'";
  public static $PAY_FROM_VBV = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'";
  public static $PAY_FROM_INTERSWITCH = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'";
  public static $SENT_TO_NIBSS = "Sent to Nibss for Processing - '{batch_id}'";
  public static $NIBSS_PAYMENT = "NIBSS Payment for Batch Id - '{batch_id}'";
  public static $RESPONSE_FROM_NIBSS = "Response from Nibss For Batch Id - '{batch_id}'";
  public static $CHARGED_WALLET_REVERSE = "'{amount}' recharged via {payment_mode_option} is reversed in eWallet - '{account_number}' by '{teller_name}'";
  public static $PAY_FROM_ETRANZACT = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'";
  public static $SERVICE_CHARGE_RECHARGE = "Service Charge of '{amount}' collected on Recharge of '{account_number}' with '{recharge_amount}' via {payment_mode_option}";
  public static $PAY_FROM_CHECK = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'- '{bank_name}'";
  public static $PAY_FROM_BANKDRAFT = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'- '{bank_name}'";
  public static $PAY_FROM_INTERNETBANK = "Merchant Item Payment For '{merchant}' - '{merchant_service}' via '{payment_mode_option}'";

  public static function getFomattedMessage($message, array $replaceVars=NULL) {
    $formattedMessage = $message;
    if(count($replaceVars) > 0){
      foreach ($replaceVars as $key => $value) {
        $formattedMessage = str_replace('{'.$key.'}', $value, $formattedMessage);
      }
    }
    return html_entity_decode($formattedMessage);
  }
}
?>
