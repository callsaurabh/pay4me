<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Settings {

  public static function isEwalletPinActive() {
    return sfConfig::get('app_ewallet_pin_generation_flag');
  }
  public static function getGracePeriodEndDate() {
    return sfConfig::get('app_ewallet_pin_grace_period_end_date');
  }
  public static function getMaxNoOfRetries() {
    return sfConfig::get('app_ewallet_max_no_of_retries');
  }

  public static function isVbvRechargeLater() {
    return sfConfig::get('app_recharge_options_is_vbv_recharge_later');
  }

  public static function getVbvClearanceDays() {
    if(self::isVbvRechargeLater()) {
      return sfConfig::get('app_recharge_options_vbv_clearance_days');
    }
    return 0;
  }


  public static function isInterswitchRechargeLater() {
    return sfConfig::get('app_recharge_options_is_interswitch_recharge_later');
  }

  public static function getInterswitchClearanceDays() {
    if(self::isInterswitchRechargeLater()) {
      return sfConfig::get('app_recharge_options_interswitch_clearance_days');
    }
    return 0;
  }
 

  public static function isEtranzactRechargeLater() {
    return sfConfig::get('app_recharge_options_is_etranzact_recharge_later');
  }

  public static function getEtranzactClearanceDays() {
    if(self::isEtranzactRechargeLater()) {
      return sfConfig::get('app_recharge_options_etranzact_clearance_days');
    }
    return 0;
  }

  public static function isMultiCurrencyOn() {
    if(sfConfig::get('app_is_multi_currency_on') == 1 ) {
      return true;
    }
    return false;
  }


  public static function getLimitOfProcessBatch() {
    return sfConfig::get('app_limit_of_process_batch');
  }
  public static function getNextDayProcessBatchTimeInMin() {
    return sfConfig::get('app_next_day_process_batch_time_in_min');
  }

 public static function getSameDayProcessBatchTimeInMin() {
    return sfConfig::get('app_same_day_process_batch_time_in_min');
  }
  
  public static function getNextDayProcessVbvTimeInMin() {
    return sfConfig::get('app_next_day_process_vbv_time_in_min');
  }

  public static function getBillPayment(){
      $billPaymentMode =  sfConfig::get('app_bill_payment');      
      $billPaymentArray = array();      
      foreach($billPaymentMode as $key=>$val){         
       $billPaymentModeObj = Doctrine::getTable('PaymentModeOption')->getPaymentModeID($val);
       $billPaymentArray[] = $billPaymentModeObj[0]['id'];
      }     
  
      return $billPaymentArray;
  }
  public static function getVbvTimeInSecForTimeOut(){
    return sfConfig::get('app_vbv_time_in_sec_for_timeout');
  }
  
  public static function getEtranzactTimeInSecForTimeOut(){
    return sfConfig::get('app_etranzact_time_in_sec_for_timeout');
  }

 public static function getMonitorMailTo(){
     $monitorMailToArray =array();
      $monitorMailTo =  sfConfig::get('app_monitor_mail_to');
      if(!empty($monitorMailTo)){
          foreach($monitorMailTo as $key=>$val){
            $monitorMailToArray[] = $val;
           }
           
      }
      return $monitorMailToArray;
  }

  public static function getMonitorLedgerPerTxnLimit() {
    return sfConfig::get('app_monitor_ledger_per_txn_limit');
  }
  
  public static function getMonitorBeforeTimepayForNotificationAndMail() {
    return sfConfig::get('app_monitor_time_job_for_pay_notification_mail');
  }

  public static function getPayForMeMerchant() {
    return sfConfig::get('app_pay4me_merchant');
  }
  public static function getBrokerUrl() {
    return sfConfig::get('app_broker_url');
  }
  
   public static function getReatttemptTimeInMin() {
        if (sfConfig::get('app_reatttempt_time_in_min')) {
            return sfConfig::get('app_reatttempt_time_in_min');
        } else {
            return 0;
        }
    }
  
   public static function getNumberOfQuestion() {
       return sfConfig::get('app_number_of_question');
    }

  public static function getGroupIdByGroupname($name){
      $groupObj = Doctrine::getTable('sfGuardGroup')->findByName($name);
      return $groupId = $groupObj->getFirst()->getId();
  }
}

?>
