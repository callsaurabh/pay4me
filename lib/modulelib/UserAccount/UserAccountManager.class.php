<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class UserAccountManager {
  private $userId;

  public function UserAccountManager($userId=null) {
    $user = sfContext::getInstance()->getUser();
    if(isset($user) && $user->isAuthenticated()) {
      $this->sfGuardUserObj = $user->getGuardUser();
      $this->userId = $this->sfGuardUserObj->getId();
    }
    if ($userId) {
      $this->userId = $userId;
    }
  }

public function IsSuperAdmin(){
    $user = sfContext::getInstance()->getUser();
    if($user  && $user->isAuthenticated()) {
      $group_name = $user->getGroupNames();
      if(in_array(sfConfig::get('app_pfm_role_admin'),$group_name)){
        return true;
      }
    }
    return false;
  }
  public function getAccountForCurrency($currency_id=NULL) {
    return Doctrine::getTable('UserAccountCollection')->getAccounts($this->userId, $currency_id);
  }
   public function getAccountIdByCurrency($currency_id=NULL) {
       $accountObj = $this->getAccountForCurrency($currency_id);
       if(is_object($accountObj))
         {
             $wallet_account_id = $accountObj->getFirst()->getMasterAccountId();
              return $wallet_account_id;
         }
         else
         return false;

  }

  public function getCurrencyForAccount($account_id) {
    $collectionObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($account_id);
    return $collectionObj->getFirst()->getCurrencyId();
  }
   public function getCurrency($account_id) {
    $collectionObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($account_id);
    return $collectionObj->getFirst()->getCurrencyCode()->getCurrency();
  }

  public function getAccountDetails($account_id) {
    $collectionObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($account_id);
    return $collectionObj->getFirst();
  }


  public function getAccountOwner($account_id) {print $account_id;
    $collectionObj = Doctrine::getTable('UserAccountCollection')->findByMasterAccountId($account_id);
   // print "<pre>";print_r($collectionObj->toArray());
    return $collectionObj->getFirst()->getUserId();
  }
 public function IsReportAdmin(){
    $user = sfContext::getInstance()->getUser();
    if($user  && $user->isAuthenticated()) {
      $group_name = $user->getGroupNames();
      if(in_array(sfConfig::get('app_pfm_role_admin'),$group_name) || in_array(sfConfig::get('app_pfm_role_report_admin'),$group_name) || in_array(sfConfig::get('app_pfm_role_report_portal_admin'),$group_name) || in_array(sfConfig::get('app_pfm_role_portal_support'),$group_name)){
        return true;
      }
    }
    return false;
  }
  public function IsBankUser(){
    $user = sfContext::getInstance()->getUser();
    if($user  && $user->isAuthenticated()) {
      $group_name = $user->getGroupNames();
     
      if(in_array(sfConfig::get('app_pfm_role_bank_admin'),$group_name) || in_array(sfConfig::get('app_pfm_role_report_bank_admin'),$group_name)||
          in_array(sfConfig::get('app_pfm_role_bank_branch_user'),$group_name) || in_array(sfConfig::get('app_pfm_role_bank_branch_report_user'),$group_name)){
        return true;
      }
    }
    return false;
  }
  
}

?>
