<?php

class EpAuditEvent {

    //category
    public static $CATEGORY_SECURITY = 'security';
    public static $CATEGORY_TRANSACTION = 'transaction';
    // subcategory for CATEGORY_SECURITY
    public static $SUBCATEGORY_SECURITY_INCORRECT_USERNAME = 'Incorrect Username';
    public static $SUBCATEGORY_SECURITY_LOGIN = 'Login';
    public static $SUBCATEGORY_SECURITY_CREATE_NEW_ACCOUNT = 'Create New Account (Normal eWallet)';
    public static $SUBCATEGORY_SECURITY_CREATE_NEW_ACCOUNT_OPENID = 'Create New Account OpenId';
    public static $SUBCATEGORY_SECURITY_CREATE_NEW_MERCHANT_ACCOUNT = 'Create New Merchant Account';
    public static $SUBCATEGORY_SECURITY_ASSOCIATE_ACCOUNT_WITH_OPENID = 'Associate eWallet Account with OpenId';
    public static $SUBCATEGORY_SECURITY_RECOVER_ACCOUNT_WITH_OPENID = 'Recover eWallet Account With OpenId';
    public static $SUBCATEGORY_SECURITY_LOGOUT = 'Logout';
    public static $SUBCATEGORY_SECURITY_TIMEOUT = 'Timeout';
    public static $SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER = 'Incorrect Credentials';
    public static $SUBCATEGORY_SECURITY_PASSWORD_CHANGE = 'Password Change';
    public static $SUBCATEGORY_SECURITY_PASSWORD_RESET = 'Password Reset';
    public static $SUBCATEGORY_SECURITY_INVALID_IP = 'Invalid IP';
    public static $SUBCATEGORY_SECURITY_SUSPENDED_USER = 'Suspended User';
    public static $SUBCATEGORY_SECURITY_DEACTIVATED_USER = 'Deactivated User';
    public static $SUBCATEGORY_SECURITY_RESUMED_USER = 'Resumed User';
    public static $SUBCATEGORY_SECURITY_USER_QUESTION_ANSWER = "Question Answer";

    public static $SUBCATEGORY_SECURITY_EWALLET_USER_EMAIL_UPDATE = 'Email Update For ewallet User';

    // subcategory for CATEGORY_TRANSACTION
    public static $SUBCATEGORY_TRANSACTION_BANK_CREATE = 'Bank Create';
    public static $SUBCATEGORY_TRANSACTION_BANK_UPDATE = 'Bank Update';
    public static $SUBCATEGORY_TRANSACTION_BANK_DELETE = 'Bank Delete';
    public static $SUBCATEGORY_TRANSACTION_BANK_ACTIVATE = 'Bank Activate';
    public static $SUBCATEGORY_TRANSACTION_BANK_DEACTIVATE = 'Bank Deactivate';
    public static $SUBCATEGORY_TRANSACTION_BANK_BRANCH_CREATE = 'Bank Branch Create';
    public static $SUBCATEGORY_TRANSACTION_BANK_BRANCH_UPDATE = 'Bank Branch Update';
    //public static $SUBCATEGORY_TRANSACTION_BANK_BRANCH_DELETE = 'Bank Branch Delete';
    public static $SUBCATEGORY_TRANSACTION_BANK_BRANCH_ACTIVATE = 'Bank Branch Activate';
    public static $SUBCATEGORY_TRANSACTION_BANK_BRANCH_DEACTIVATE = 'Bank Branch Deactivate';
    public static $SUBCATEGORY_TRANSACTION_BANK_MERCHANT_ACTIVATE = 'Bank Merchant Activate';
    public static $SUBCATEGORY_TRANSACTION_BANK_MERCHANT_DEACTIVATE = 'Bank Merchant Deactivate';
    public static $SUBCATEGORY_TRANSACTION_USER_CREATE = 'User Create';
    public static $SUBCATEGORY_TRANSACTION_USER_UPDATE = 'User Update';
    public static $SUBCATEGORY_TRANSACTION_USER_DELETE = 'User Delete';
//  public static $SUBCATEGORY_TRANSACTION_USER_ACTIVATE = 'User Activation';
//  public static $SUBCATEGORY_TRANSACTION_USER_DEACTIVATE = 'User Deactivation';
    public static $SUBCATEGORY_TRANSACTION_USER_BLOCKED = 'User Blocked';
    public static $SUBCATEGORY_TRANSACTION_USER_BRANCH_CHANGED = 'User Branch Changed';
    public static $SUBCATEGORY_TRANSACTION_USER_UNBLOCKED = 'User Unblocked';
    public static $SUBCATEGORY_TRANSACTION_EWALLET_CHARGED = 'eWallet Charged via Bank';
    public static $SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_CHECK = 'eWallet Charged via Cheque';
    public static $SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_DRAFT = 'eWallet Charged via Bank Draft';
    public static $SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_VISA = 'eWallet Charged via Visa';
    public static $SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERNET_BANK = 'eWallet Charged via Internet Bank';
    public static $SUBCATEGORY_TRANSACTION_EWALLET_RECHARGED_PAYMENT_REVERSAL = 'eWallet recharged payment reversal';
    /* for Interswitch */
    public static $SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERSWITCH = 'eWallet Charged via Interswitch';
    /* for Etranzact */
    public static $SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_ETRANZACT = 'eWallet Charged via Etranzact';
    public static $SUBCATEGORY_SECURITY_LOGIN_MOBILE = 'Login via Mobile';
    public static $SUBCATEGORY_TRANSACTION_PAYMENT_ARCHIVED_VIA_MOBILE = 'Bill Archived via mobile';
    public static $SUBCATEGORY_TRANSACTION_PAYMENT_DISAPPROVAL_VIA_MOBILE = 'Payment Disapproval via mobile';
    public static $SUBCATEGORY_TRANSACTION_PAYMENT_APPROVAL_VIA_MOBILE = 'Payment Approval via mobile';
    public static $SUBCATEGORY_TRANSACTION_EWALLET_EWALLET_VIA_MOBILE = 'eWallet to eWallet transfer via mobile';
    public static $SUBCATEGORY_TRANSACTION_PAYMENT_REQUEST_VIA_MOBILE = 'Payment Request via mobile';
    public static $SUBCATEGORY_TRANSACTION_USER_BLOCK = "eWallet User Blocked";
    public static $SUBCATEGORY_TRANSACTION_USER_UNBLOCK = "eWallet User Unblocked";
    public static $SUBCATEGORY_TRANSACTION_USER_KYC_APPLY = "eWallet user apply for KYC";
    public static $SUBCATEGORY_TRANSACTION_USER_KYC_APPROVE = "eWallet user approved for KYC";
    public static $SUBCATEGORY_TRANSACTION_USER_KYC_REJECT = "eWallet user Rejected for KYC";
    public static $SUBCATEGORY_TRANSACTION_PAYMENT = 'Payment';
    public static $SUBCATEGORY_TRANSACTION_PROFILE_UPDATED = "Profile Updated";
    // These are attribute names
    public static $ATTR_P4MTXNID = 'TransactionId';
    public static $ATTR_USERINFO = 'UserDetail'; // User Details
    public static $ATTR_BANKINFO = 'BankDetail';
    public static $ATTR_BRANCHINFO = 'BranchDetail';  //Bank Branch Details
    public static $ATTR_EWALLETINFO = 'eWalletDetails';  //eWallet Details
    public static $ATTR_MERCHANTINFO = 'MerchantDetail';
    public static $ATTR_BANKNAME = 'BankName';
    public static $ATTR_BRANCHNAME = 'BranchName';
    public static $ATTR_KYCDETAIL = 'KycDetail';
    // These are messages templates
    public static $MSG_SUBCATEGORY_TRANSACTION_PAYMENT = "Payment made for transaction id '{txnid}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_DELETE = "Bank '{bankname}' Deleted ";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_UPDATE = "Details of Bank '{bankname}' Updated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_CREATE = "New Bank Added '{bankname}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_ACTIVATE = "Bank '{bankname}' Activated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_DEACTIVATE = "Bank '{bankname}' Deactivated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_DELETE = "Bank Branch '{branchname}' Deleted";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_UPDATE = "Detail updated for Bank Branch '{branchname}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_CREATE = "New Bank Branch '{branchname}' Added";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_ACTIVATE = "Bank Branch '{branchname}' Activated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_BRANCH_DEACTIVATE = "Bank Branch '{branchname}' Deactivated";
    public static $MSG_SUBCATEGORY_TRANSACTION_USER_BANKEAUDITOR_CREATE = "BankEAuditor '{username}' Created";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_REPORT_ADMIN_CREATE = "Bank Report Admin '{username}' Created";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_COUNTRY_HEAD_CREATE = "Bank Country Head '{username}' Created";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_REPORT_COUNTRY_ADMIN_CREATE = "Bank Report Country Admin '{username}' Created";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_ADMIN_CREATE = "Bank Admin '{username}' Created";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_USER_UPDATE = "Bank Admin '{username}' Edited";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_USER_DELETE = "Bank Admin '{username}' Deleted";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_USER_UNBLOCKED = "Bank Admin '{username}' Unblocked";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_USER_SECURITY_PASSWORD_RESET = "Password Reset for Bank Admin '{username}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_CREATE = "Bank Branch User '{username}' Created";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_SUSPENDED = "Bank Branch User '{username}' Suspended";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DEACTIVATED = "Bank Branch User '{username}' De-activated";
    public static $MSG_SUBCATEGORY_TRANSACTION_USER_BRANCH_CHANGED = "Bank Branch changed for user '{username}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_SECURITY_PASSWORD_RESET = "Password Reset for Bank Branch User '{username}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_REPORT_USER_SECURITY_PASSWORD_RESET = "Password Reset for Bank Branch ReportUser '{username}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DEACTIVATE = "Bank Branch User '{username}' Deactivated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_ACTIVATE = "Bank Branch User '{username}' Activated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_DELETE = "Bank Branch User '{username}' Deleted";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_UPDATE = "Bank Branch User '{username}' Edited";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_UNBLOCKED = "Bank Branch User '{username}' Unblocked";
    public static $MSG_SUBCATEGORY_SECURITY_LOGOUT = "User Logged out";
    public static $MSG_SUBCATEGORY_SECURITY_TIMEOUT = "User Time out";
    public static $MSG_SUBCATEGORY_TRANSACTION_USER_BLOCKED = "'{username}' blocked - exceded number of Login Attempts";
    public static $MSG_SUBCATEGORY_SECURITY_FIRST_LOGIN = "'{username}' logged in for first time";
    public static $MSG_SUBCATEGORY_SECURITY_INCORRECT_USERNAME = "Username Incorrect - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_LOGIN = "Successfull Login - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_INVALID_IP = "Invalid IP Address - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER = "Invalid Password - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_SEC_ANSWER = "Invalid Security Answer,Entered Hashed answer is '{answer}' - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_FIRSTTIME_PASSWORD_CHANGE = "First time Login - Password Changed - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_PASSWORD_CHANGE = "Password Changed for '{username}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED = "eWallet charged against Account Number '{account_number}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_MERCHANT_ACTIVATE = "Bank Merchant '{merchantname}' Activated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BANK_MERCHANT_DEACTIVATE = "Bank Merchant '{merchantname}' Deactivated";
    public static $MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_VISA = "eWallet Charged via Visa '{account_number}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERNET_BANK = "eWallet Charged via Internet Bank '{account_number}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_INTERSWITCH = "eWallet Charged via Interswitch '{account_number}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_EWALLET_RECHARGED_PAYMENT_REVERSAL = "eWallet '{account_number}' recharged payment of validation '{pvalidation}' is reversed  with validation '{rvalidation}'";
    public static $MSG_SUBCATEGORY_ACCOUNT_BLOCK = "eWallet User '{username}' Blocked";
    public static $MSG_SUBCATEGORY_ACCOUNT_UNBLOCK = "eWallet user '{username}' Unblocked";
    public static $MSG_SUBCATEGORY_TRANSACTION_EWALLET_CHARGED_VIA_ETRANZACT = "eWallet Charged via Etranzact '{account_number}'";
    public static $MSG_SUBCATEGORY_SECURITY_LOGIN_MOBILE = "Successfull Login via Mobile - '{username}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_EWALLET_EWALLET_VIA_MOBILE = "eWallet to eWallet transfer of Amount '{amount}' via mobile From Account Number  {fromaccount_number} -To Account Number  '{toaccount_number}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_PAYMENT_REQUEST_VIA_MOBILE = "Payment Request for Amount '{amount}' against Account Number '{account_number}' via mobile";
    public static $MSG_SUBCATEGORY_TRANSACTION_PAYMENT_APPROVAL_VIA_MOBILE = "Payment Approval by User '{username}' against Transaction number  '{txnid}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_PAYMENT_DISAPPROVAL_VIA_MOBILE = "Payment Disapproval by User '{username}' against Transaction number  '{txnid}'";
    public static $MSG_SUBCATEGORY_TRANSACTION_PAYMENT_ARCHIVED_VIA_MOBILE = "Bill Archived by User '{username}' against Transaction number  '{txnid}'";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_COUNTRY_HEAD_SUSPENDED = "Bank Country Head '{username}' Suspended";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_COUNTRY_HEAD_RESUMED = "Bank Country Head '{username}' Resumed";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_COUNTRY_HEAD_DEACTIVATED = "Bank Country Head '{username}' De-activated";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_REPORT_COUNTRY_ADMIN_SUSPENDED = "Bank Report Country Admin '{username}' Suspended";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_REPORT_COUNTRY_ADMIN_RESUMED = "Bank Report Country Admin '{username}' Resumed";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_REPORT_COUNTRY_ADMIN_DEACTIVATED = "Bank Report Country Admin '{username}' De-activated";
    public static $MSG_SUBCATEGORY_TRANSACTION_BRANCH_USER_RESUMED = "Bank Branch User '{username}' Resumed";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_BRANCH_REPORT_USER_SUSPENDED = "Bank Branch Report User '{username}' Suspended";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_BRANCH_REPORT_USER_RESUMED = "Bank Branch Report User '{username}' Resumed";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_BRANCH_REPORT_USER_DEACTIVATED = "Bank Branch Report User '{username}' De-activated";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_ADMIN_SUSPENDED = "Bank Admin '{username}' Suspended";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_ADMIN_RESUMED = "Bank Admin '{username}' Resumed";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_ADMIN_DEACTIVATED = "Bank Admin '{username}' De-activated";
    public static $MSG_SUBCATEGORY_SECURITY_REPORT_BANK_ADMIN_SUSPENDED = "Report Bank Admin '{username}' Suspended";
    public static $MSG_SUBCATEGORY_SECURITY_REPORT_BANK_ADMIN_RESUMED = "Report Bank Admin '{username}' Resumed";
    public static $MSG_SUBCATEGORY_SECURITY_REPORT_BANK_ADMIN_DEACTIVATED = "Report Bank Admin '{username}' De-activated";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_EAUDITOR_SUSPENDED = "Bank eAuditor '{username}' Suspended";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_EAUDITOR_RESUMED = "Bank eAuditor '{username}' Resumed";
    public static $MSG_SUBCATEGORY_SECURITY_BANK_EAUDITOR_DEACTIVATED = "Bank eAuditor '{username}' De-activated";
    public static $MSG_SUBCATEGORY_SECURITY_DEACTIVATED_USER_LOGIN_ATTEMPT = "De-activated User '{username}' Login Attempt";
    public static $MSG_SUBCATEGORY_SECURITY_SUSPENDED_USER_LOGIN_ATTEMPT = "Suspended User '{username}' Login Attempt";
    public static $MSG_SUBCATEGORY_SECURITY_ANSWER_CHANGE = "Security Answer Updated Successfully - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_PASSWORD_RESET_APPLIED = "'{username}' Applied For Reset Password";
    public static $MSG_SUBCATEGORY_TRANSACTION_USER_KYC_APPLY = "eWallet user '{username}' apply for KYC";
    public static $MSG_SUBCATEGORY_TRANSACTION_USER_KYC_APPROVE = "eWallet user '{username}' Approved for KYC";
    public static $MSG_SUBCATEGORY_TRANSACTION_USER_KYC_REJECT = "eWallet user '{username}' Rejected for KYC";
    public static $MSG_SUBCATEGORY_TRANSACTION_PROFILE_UPDATED = "User '{username}' Profile Updated Successfully";
    public static $MSG_SUBCATEGORY_SECURITY_CREATE_ACCOUNT = "Successful Create Normal eWallet New Account For Username - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_CREATE_MERCHANT_ACCOUNT = "Successful Create Merchant New Account For Username - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_CREATE_ACCOUNT_OPENID = "Successful Create New Account Using OpenId Authetication For Email - '{username}'";
    public static $MSG_SUBCATEGORY_SECURITY_ASSOCIATE_ACCOUNT_WITH_OPENID = "Successful Association of eWallet Account with OpenId Authetication For Username - '{username}'. Email Id is updated from - '{previousEmail}' to '{newEmail}'";
    public static $MSG_SUBCATEGORY_SECURITY_ASSOCIATE_ACCOUNT_WITH_OPENID_EMAIL_EXISTS = "Successfull Association of eWallet Account with OpenId Authetication For Username - '{username}' and Email Id - '{previousEmail}'";
    public static $MSG_SUBCATEGORY_SECURITY_RECOVER_ACCOUNT_WITH_OPENID = "Successful Recover of eWallet Account with OpenId Authetication For Username - '{username}'. Updated Email - '{newEmail}'. Previous email was- '{previousEmail}'";
    public static $MSG_SUBCATEGORY_SECURITY_EWALLET_USER_EMAIL_UPDATE = "ewallet User '{username}' Edited";
    public static $MSG_SUBCATEGORY_SECURITY_CREATE_SUB_MERCHANT_ACCOUNT = "Successful Create Sub Merchant New Account For Username - '{username}'";

    public static function getFomattedMessage($message, $replaceVars) {
        $formattedMessage = $message;
        foreach ($replaceVars as $key => $value) {
            $formattedMessage = str_replace('{' . $key . '}', $value, $formattedMessage);
        }
        return $formattedMessage;
    }

    public static function getAllCategories($defaultOption=null) {
        if (!is_null($defaultOption)) {
            $defaultOption['security'] = 'Security';
            $defaultOption['transaction'] = 'Transaction';
            return $defaultOption;
//return array('security'=>'Security', 'transaction'=>'Transaction');
        } else {
            return array('security' => 'Security', 'transaction' => 'Transaction');
        }
    }

    public static function getAllSubcategory($category=null) {
        $class = __CLASS__;
        $rf = new ReflectionClass($class);
        $staticProperties = $rf->getProperties(ReflectionProperty::IS_STATIC);
        $subCategories = array();
        if (!empty($category)) {
            foreach ($staticProperties as $aProp) {
//        if(empty($category)) {
//          $subCategories[] = $aProp->getValue();
//          continue;
//        }
                // see if it matches with our category
                // AUDIT_TRAIL_SUBCATEGORY_<all caps of $category>_*
                $name = $aProp->getName();
                $category_variable = "SUBCATEGORY_" . strtoupper($category);

                if ((strpos($name, $category_variable) !== false) && (strpos($name, "MSG") === FALSE)) {
                    $subCategories[] = $aProp->getValue();
                }
            }
        }
        return $subCategories;
        //   print_r($subCategories);exit;
    }

}

?>
