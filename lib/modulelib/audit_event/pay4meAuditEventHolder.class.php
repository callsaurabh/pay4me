<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class pay4meAuditEventHolder extends EpAuditEventHolder {
  private $BANK_ATTR_NAME = "BankName";
  private $BRANCH_ATTR_NAME = "BranchName";

  protected function getEventAttributes() {
    // attributes to find are
    // bank
    // branch
    if(!parent::isAuthenticated()) {
      return null;
    }

    $bUser = parent::getUser()->getBankUser();

    //var_dump($bUser->count());
    if($bUser->count()<1) {
        return null;
    }

    $attrArray = array();

    $bankUser = $bUser->getFirst();
    if ($bankUser->getBankId()) {
      $bank = $bankUser->getBank();
      $bank_id=$bank->getId();
      $bank_name = $bank->getBankName();
      $attr = new EpAuditEventAttributeHolder($this->BANK_ATTR_NAME,$bank_name,$bank_id);
      $attrArray[] = $attr;
    }
    if ($bankUser->getBankBranchId()) {
      $branch = $bankUser->getBankBranch();
      $branch_id = $branch->getId();
      $branch_name= $branch->getName();
      $attr = new EpAuditEventAttributeHolder($this->BRANCH_ATTR_NAME,$branch_name,$branch_id);
      $attrArray[] = $attr;
    }

    return $attrArray;

  }

}

?>
