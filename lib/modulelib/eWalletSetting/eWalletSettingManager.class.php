<?php
class eWalletSettingManager {
  public function chkRechargeLimit($amount,$ewallet_account_number,$ewallet_account_id) {
    #sfLoader::loadHelpers('ePortal');
    sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
    $amount = convertToKobo($amount);
    $Amt = Doctrine::getTable('EwalletSetting')->getChargeAmountLimit();
    $Days= Doctrine::getTable('EwalletSetting')->getChargeDaysLimit();

    $beforeDate=date("Y-m-d",mktime(0,0,0,date("m"),date("d")- $Days[0]['var_value'],date("Y")));
    $totAmt= Doctrine::getTable('EpMasterLedger') ->getCrdtAmtBetDate($beforeDate,$ewallet_account_number,$ewallet_account_id);
    //               print_r($Amt[0]['var_value'] + $amount);
    if( ($totAmt[0]['sum'] + $amount) > $Amt[0]['var_value'] ) {//if cheking anount limit start

      return $chargableAmt=$Amt[0]['var_value']-$totAmt[0]['sum'];

    }
    else return NULL;
  }

  public function getServiceCharge($payment_mode_option_id, $amount){
    $serviceChargeSettings = Doctrine::getTable('EwalletServiceCharges')->getServiceCharge($payment_mode_option_id);
    if($serviceChargeSettings['charge_type'] == 'flat'){
      return $serviceChargeSettings['charge_amount'] ;
    }elseif($serviceChargeSettings['charge_type'] == 'percent'){
          $tmpServiceCharge = ($serviceChargeSettings['charge_amount']/100) * $amount ;
          $upperLimit = $serviceChargeSettings['upper_slab'];
          $lowerLimit = $serviceChargeSettings['lower_slab'];
          if (($upperLimit!=0)){
              if ($tmpServiceCharge > $upperLimit) {
                  $tmpServiceCharge = $upperLimit;
                  return $tmpServiceCharge;
              }
          }
          if ($lowerLimit!=0){
              if ($tmpServiceCharge < $lowerLimit) {
                  $tmpServiceCharge = $lowerLimit;
                  return $tmpServiceCharge;
              }
          }
          if ($tmpServiceCharge <= $upperLimit && $tmpServiceCharge >= $lowerLimit) {
              return $tmpServiceCharge;
          }
          return $tmpServiceCharge;
      }
  }

}

?>
