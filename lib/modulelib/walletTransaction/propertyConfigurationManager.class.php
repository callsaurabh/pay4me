<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of service
 *
 * @author Uday
 */
class propertyConfigurationManager {
    //put your code here

    private function getListing($status){
        return Doctrine::getTable("Propery")->listing($status);
    }

   public function getPropertyAllListing(){
        $status = null;
        $this->getListing($status);
    }

   public function getPropertyActiveListing(){
        $status = "active";
        $this->getListing($status);
    }

   public function getPropertyInActiveListing(){
        $status = "inactive";
        $this->getListing($status);
    }

}
?>
