<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class walletTransactionManager {
    //assigning default value transactionPlatform
    public $transactionPlatform = 'Web';
    /*
     * Update Data into model
     */
    private function updateTransaction($walletTransactionId,$status,$eWalletAccNo) {
        $updateObj =  Doctrine::getTable('EwalletTransaction')->setUpdateTransaction($walletTransactionId,$status);
        if($status == "rejected") {
            $transactionCode = $this->createTrack($eWalletAccNo,'BillReject',$walletTransactionId);
        }else {
            $transactionCode = $this->createTrack($eWalletAccNo,'BillApprove',$walletTransactionId);
        }
        return $transactionCode;
    }

    /*
     * Insert Data into model
     */
    private function createTransaction($fromAcc,$toAcc,$amt,$desc,$type,$eWalletAccNo) {
        $saveObj = Doctrine::getTable('EwalletTransaction')->setCreateTransaction($fromAcc,$toAcc,$amt,$desc,$type);
        if($type == "direct") {
            $transactionCode = $this->createTrack($eWalletAccNo,'eWalletTransfer',$saveObj);
        }else {
            $transactionCode = $this->createTrack($eWalletAccNo,'BillRequest',$saveObj);
        }
        return $transactionCode;
    }

    /*
     * Listing Data from model
     */

    private function listTransaction($eWalletAccountId,$status,$type,$accType) {
        return Doctrine::getTable('EwalletTransaction')->getListTransaction($eWalletAccountId,$status,$type,$accType);
    }

    private function listTransactionWithTransCode($eWalletAccountId,$status,$type,$accType) {
        return Doctrine::getTable('EwalletTransaction')->getListWithTransactionCode($eWalletAccountId,$status,$type,$accType);
    }

    public function rejectTransfer($walletTransactionId,$eWalletAccNo) {
        $status = "rejected";
        return $this->updateTransaction($walletTransactionId, $status,$eWalletAccNo);
    }

    public function approveTransfer($walletTransactionId,$eWalletAccNo) {
        $status = "transfer";
        return $this->updateTransaction($walletTransactionId, $status,$eWalletAccNo);
    }

    /*
     * Call createTransaction method for insert date into table
     */
    public function requestTransfer($fromAcc,$toAcc,$amt,$desc,$eWalletAccNo) {
        $type = "transfer";
        return $this->createTransaction($fromAcc,$toAcc,$amt,$desc,$type,$eWalletAccNo);
    }
    /*
     * Call createTransaction method for insert date into table
     */
    public function directTransfer($fromAcc,$toAcc,$amt,$desc,$eWalletAccNo) {
        $type = "direct";

        //WP029 Concurrency Issue During eWallet Transfer
        $con = Doctrine_Manager::connection();
       
        try {
            $con->beginTransaction();
            $transactionCode = $this->createTransaction($fromAcc,$toAcc,$amt,$desc,$type,$eWalletAccNo);            
            if(is_numeric($transactionCode)) {
                $returnVal = $this->setAccountingInfo($fromAcc, $toAcc, $desc, $amt, $transactionCode);
                if(is_numeric($returnVal)) {
                    $con->commit();
                    return $transactionCode;
                }else {
                    return false;
                }
            }else 
                return $returnVal;
        }
        catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
    }

    public function listRejectTransferFromAcc($eWalletAccountId) {
        $status = "rejected";
        $type = "transfer";
        return $this->listTransaction($eWalletAccountId,$status,$type,'fromAcc');

    }
    public function listApproveTransferFromAcc($eWalletAccountId) {
        $status = "transfer";
        $type = "transfer";
        return $this->listTransaction($eWalletAccountId,$status,$type,'fromAcc');

    }

    public function listRequestTransferFromAcc($eWalletAccountId) {
        $status = "pending";
        $type = "transfer";
        return $this->listTransaction($eWalletAccountId,$status,$type,'fromAcc');

    }
    public function listRejectTransferToAcc($eWalletAccountId) {
        $status = "rejected";
        $type = "transfer";
        return $this->listTransaction($eWalletAccountId,$status,$type,'toAcc');

    }
    public function listApproveTransferToAcc($eWalletAccountId) {
        $status = "transfer";
        $type = "transfer";
        return $this->listTransaction($eWalletAccountId,$status,$type,'toAcc');

    }

    public function listRequestTransferToAcc($eWalletAccountId) {
        $status = "pending";
        $type = "transfer";
        return $this->listTransaction($eWalletAccountId,$status,$type,'toAcc');

    }
    public function listDirectTransfer($eWalletAccountId) {
        $status = "transfer";
        $type = "direct";
        return $this->listTransaction($eWalletAccountId,$status,$type,'fromAcc');

    }
    public function listReqFromAccWithTransCode($eWalletAccountId) {
        $status = "pending";
        $type = "transfer";
        return $this->listTransactionWithTransCode($eWalletAccountId,$status,$type,'fromAcc');

    }
    private function createTrack($wallet_account_number,$type,$app_id) {
        $trackObj = new EwalletTransactionTrack();
        $transaction_code = $this->generateTransactionCode();
        $trackObj->setTransactionCode($transaction_code);
        $trackObj->setType($type);
        $trackObj->setAppId($app_id);
        $trackObj->setWalletAccountNumber($wallet_account_number);
        $trackObj->setPlatform($this->transactionPlatform);
        $saveObj = $trackObj->save();
        if($trackObj->getId()) {
            return $transaction_code;
        }
        return null;
    }

    private function generateTransactionCode() {
        do {
            $transaction_code =  "1".mt_rand(1000000,999999999);
            $duplicacy_transaction_code = Doctrine::getTable('EwalletTransactionTrack')->chkTransactionCodeDuplicacy($transaction_code);
        } while ($duplicacy_transaction_code > 0);
        return $transaction_code;
    }

    public function getBillRequestDetailsById($eWalletTransId) {
        return  $billReqDetails = Doctrine::getTable('EwalletTransaction')->getBillRequestDetailsById($eWalletTransId);
    }

    public function isTransCodeBelongsToUser($accId,$transcode,$userId) {
    //print $transcode;exit;
    //chk if transaction code is of payment request or of a bill request
      $first_element = substr($transcode,0,1);
      if($first_element == 2) {
        $transcode = substr($transcode,1);
        $billObj = Doctrine::getTable('Bill')->validateTransaction($transcode,$userId);
        $isValid = $billObj->count();

      }
      else {
        $isValid = Doctrine::getTable('EwalletTransactionTrack')->chkTransCodeBelongsToAccId($accId,$transcode);
      }
      if($isValid) {
        return $first_element;
      }
      else {
        return 0;
      }
    }

    public function getTansId($transcode) {

        $val = Doctrine::getTable('EwalletTransactionTrack')->getTansId($transcode);
        if($val)
        return $val;
        else
        return false;
    }


    public function setAccountingInfo($fromAccId, $toAccId, $desc, $amt, $transactionCode) {
        $accountingObj = new pay4meAccounting();
        $accValidationNo = $accountingObj->eWalletTransfer($fromAccId, $toAccId, $desc, $amt, $transactionCode);
        return $accValidationNo;
    }

    public function doBillRequestAction($walletTransactionId,$approvalStatus) {
        $eWalletDetails = $this->getBillRequestDetailsById($walletTransactionId);
        $fromAcct = $eWalletDetails[0]['from_account_id'];
        $toAcct = $eWalletDetails[0]['to_account_id'];
        $description = $eWalletDetails[0]['description'];
        $amount = $eWalletDetails[0]['amount'];
        $eWalletAccNo = $eWalletDetails[0]['EpMasterAccountFrom']['account_number'];

        $epAccountDetailObj = new EpAccountingManager();;
        $balance = $epAccountDetailObj->getAccount($fromAcct)->getBalance();
        if($approvalStatus == "rejected") {
            return $this->rejectTransfer($walletTransactionId,$eWalletAccNo);
        }else {
            if($eWalletDetails[0]['status']=='transfer' && $approvalStatus == "transfer") {
                return "Payment has already been made";
            }
            else {
                if($amount <= $balance) {
                    $accountingObj = new pay4meAccounting();
                    try {
                        $con = Doctrine_Manager::connection();
                    
            $con->beginTransaction();
            $transactionCode = $this->approveTransfer($walletTransactionId,$eWalletAccNo);
            if(is_numeric($transactionCode)) {
                $returnVal = $accountingObj->eWalletTransfer($fromAcct, $toAcct, $description, $amount, $transactionCode);
                if(is_numeric($returnVal)) {
                    $con->commit();
                    return $transactionCode;
                }else {
                    return false;
                }
            }else
                return false;
        }
        catch (Exception $e) {
            $con->rollback();
            throw $e;
        }
                    /*$value = $accountingObj->eWalletTransfer($fromAcct, $toAcct, $description, $amount);
                    if(is_numeric($value)) {
                        return $this->approveTransfer($walletTransactionId,$eWalletAccNo);
                    }*/
                }else {
                    return "Insufficient Account Balance!";
                }
            }
        }
    }

    
    public function doDirectRequestTransfer($to_account,$from_account,$amt,$desc) {
      try {
        $destAccountId = $this->validateEwalletAccount($to_account);
        $sourceAccountId = $this->validateEwalletAccount($from_account);
        $this->validateAccounts($destAccountId,$sourceAccountId);

        // $fromAccId = $this->getUser()->getUserAccountId();
        $accountDetailObj = new EpAccountingManager();
        $sourceAccountObj = $accountDetailObj->getAccount($sourceAccountId);
        $balance = $sourceAccountObj->getBalance();
        //checking if the amount given is a valid amount
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Accounting'));
        $validationOptions = array('zeroAmtCheck', 'minimumAmtCheck', 'maximumAmtCheck') ;
        $amtValidMsg = AccountingHelper::validateAmount($amt,$validationOptions);        
        if(is_bool($amtValidMsg) && $amtValidMsg) {
              if($amt <= $balance) {
                // WP029 Concurrency Solution through pessimistic offline locking
                $lockingManagerObj = new lockingManager('EpMasterAccount',$sourceAccountId);
                $lockingManagerObj->getLock($sourceAccountId);
                $transferResult  = $this->directTransfer($sourceAccountId, $destAccountId, $amt, $desc, $from_account);
                $lockingManagerObj->releaseLock($sourceAccountId);
                if($transferResult) {
                  if($transferResult == 'Insufficient Account Balance') {
                    throw New Exception('Insufficient Account Balance');
                  }else {
                    return $transferResult;
                  }
                }else {
                  throw New Exception('Transaction failure');
                }
              }else {
                throw New Exception('Insufficient Account Balance');
              }
        }else {           
          throw New Exception($amtValidMsg);
       }
     }
      catch (Exception $e ) {        
        return $e->getMessage();
      }
    }

   /* public function doDirectRequestTransfer1($to_account,$from_account,$amt,$desc){


        //chk if user is transfering to his account only        
        if($this->validateAccounts($to_account,$from_account)) {
            $accountDetailObj = new EpAccountingManager();
            $result = $accountDetailObj->getWalletDetails($to_account);
            if($result) {
                $ewalletType =  $result->getFirst()->getType();
                if($ewalletType == "ewallet") {
                    $toAccId = $result->getFirst()->getId();
                    $fromAccId = $this->getAccountId($from_account);
                    // $fromAccId = $this->getUser()->getUserAccountId();
                    $balance = $accountDetailObj->getAccount($fromAccId)->getBalance();
                    //checking if the amount given is a valid amount
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Accounting'));
                    $validationOptions = array('zeroAmtCheck', 'minimumAmtCheck', 'maximumAmtCheck') ;
                    $amtValidMsg = AccountingHelper::validateAmount($amt,$validationOptions);
                    if($amtValidMsg == 'valid'){
                                if($amt <= $balance) {
                                    $transferResult  = $this->directTransfer($fromAccId, $toAccId, $amt, $desc, $from_account);
                                    if($transferResult) {
                                        if($transferResult == 'Insufficient balance to pay') {
                                            return 'Insufficient balance to pay';
                                        }else {
                                            return $transferResult;
                                        }
                                    }else {
                                        return 'Transaction failure';
                                    }
                                }else {
                                    return 'Insufficient Account Balance';
                                }
                    }else{
                        return $amtValidMsg;
                    }
                }
                else {
                    return 'Entered account number is not associated with ewallet';
                }
            }else {
                return "Please enter valid eWallet receiver's account number";
            }
        }
        else {
            return 'You can not transfer in same account';
        }
    }*/

  public function doBillAdd($to_account_no,$fromAcc_no,$amt,$description) {
    try {

      $destAccountId = $this->validateEwalletAccount($to_account_no);
      $sourceAccountId = $this->validateEwalletAccount($fromAcc_no);
      $this->validateAccounts($destAccountId,$sourceAccountId);

      sfContext::getInstance()->getConfiguration()->loadHelpers(array('Accounting'));
      $validationOptions = array('zeroAmtCheck', 'minimumAmtCheck', 'maximumAmtCheck') ;
      $amtValidMsg = AccountingHelper::validateAmount($amt,$validationOptions);
      if( is_bool($amtValidMsg) && $amtValidMsg) {
        $desc = trim($description);

        $saveObj = new walletTransactionManager();
        $result  = $this->requestTransfer($sourceAccountId, $destAccountId, $amt,$desc, $to_account_no);
        return $result;
      }else {
        throw New Exception($amtValidMsg);
      }

    }
    catch (Exception $e ) {
      return $e->getMessage();
    }
  }


   /* public function doBillAdd1($to_account_no,$fromAcc_no,$amt,$description) {
        if($to_account_no != $fromAcc_no) {
            $accountDetailObj = new EpAccountingManager();
            $result = $accountDetailObj->getWalletDetails($fromAcc_no);
            if($result) {
                $ewalletType =  $result->getFirst()->getType();
                if($ewalletType == "ewallet") {
                    $fromAccId = $result->getFirst()->getId();
                    $balance = $accountDetailObj->getAccount($fromAccId)->getBalance();
                    $toAcc = $this->getAccountId($to_account_no);
                    if($toAcc) { 
                         //checking if the amount given is a valid amount
                         sfContext::getInstance()->getConfiguration()->loadHelpers(array('Accounting'));
                         $validationOptions = array('zeroAmtCheck', 'minimumAmtCheck', 'maximumAmtCheck') ;
                         $amtValidMsg = AccountingHelper::validateAmount($amt,$validationOptions);
                         if($amtValidMsg == 'valid'){ 
                            $fromAcc = $result->getFirst()->getId();
                            $desc = trim($description);
                            $result  = $this->requestTransfer($fromAcc, $toAcc, $amt,$desc, $to_account_no);
                            return $result;
                         }else{ 
                             return $amtValidMsg;
                         }
                    }else {
                        return 'Entered account number is not associated with ewallet!';
                    }
                }else {
                    return 'Entered account number is not associated with ewallet!';
                }
            }else {
                return 'Please enter valid Requesting account number.';
            }
        }else {
            return 'You can not transfer in same account!';
        }
    }*/

    
    public function getAccountId($accNo) {
        $accountDetailObj = new EpAccountingManager();
        $result = $accountDetailObj->getWalletDetails($accNo);
        if($result) {
            $ewalletType =  $result->getFirst()->getType();
            if($ewalletType == "ewallet") {
                return $fromAcc = $result->getFirst()->getId();
            }else
            return false;
        }
    }

    public function validateEwalletAccount($account_number) {
      try {
        $accountDetailObj = new EpAccountingManager();
        $result = $accountDetailObj->getWalletDetails($account_number);
        if($result) {
          $destAcctObj = $result->getFirst();
          $ewalletType =  $destAcctObj->getType();
          if($ewalletType == "ewallet") {
            return $dest_account_id = $destAcctObj->getId();
          }
          else {
            throw New Exception('Entered account number is not associated with ewallet!');
          }
        }
        else {
          throw New Exception('Please enter valid eWallet account number');
        }

      }
      catch(Exception $e) {
        throw $e;
      }

    }
    
     //function to check if the source and dest accounts belong to the same user
     public function validateAccounts($to_account,$from_account) {
       try {
         if($to_account==$from_account) {
           throw New Exception('You can not transfer in same account!');
         }
         else {
           $collectionObj=new UserAccountManager();
            $sourceAcctObj = $collectionObj->getAccountDetails($from_account);
            $destAcctObj = $collectionObj->getAccountDetails($to_account);

            $source_acct_owner_id = $sourceAcctObj->getUserId();
            $dest_acct_owner_id = $destAcctObj->getUserId();
            if($source_acct_owner_id == $dest_acct_owner_id) {
              throw New Exception('You can not transfer from your one account to another!');
            }
            //get Currency for both accounts and chk if they are same
            $currency_source_acct = $sourceAcctObj->getCurrencyId();
            $currency_dest_acct = $destAcctObj->getCurrencyId();

            if($currency_source_acct != $currency_dest_acct) {
              throw New Exception("The currency of the destination account should be same as of source account");
            }

         }
       }
       catch (Exception $e) {
         throw $e;
       }
     }

    
}


?>
