<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of merchantConfigurationServiceclass
 *
 * @author Uday
 */
class billGenerateConfigurationManager {
    //put your code here

    private function generateIdentificationNumber(){
         return (int) "1".rand(10000,99999).rand(1000,9999);
    }

    public function generatePropertyIdentificationNumber(){
        $identNo = $this->generateIdentificationNumber();
        return "P".$identNo;
    }

    public function generateBuisnessIdentificationNumber(){
        $identNo = $this->generateIdentificationNumber();
        return "B".$identNo;
    }

    public function generatePropertyBillNumber(){
        $identNo = $this->generateIdentificationNumber();
        return "PB".$identNo;
    }

    public function generateBuisnessBillNumber(){
        $identNo = $this->generateIdentificationNumber();
        return "BB".$identNo;
    }
}
  ?>
