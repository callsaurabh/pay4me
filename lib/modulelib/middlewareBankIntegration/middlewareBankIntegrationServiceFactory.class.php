<?php
class middlewareBankIntegrationServiceFactory{
/**
  *
  * @param <type> $type
  * @return empService the service implementation
  */
 public static function getResponse($requestXml, $bankId, $requestId) {
	$bankManagerObj = new middlewareBankIntegrationManager();
	return $bankManagerObj->getBankResponse($requestXml, $bankId, $requestId);
 }
}
?>
