<?php
/*
 * To change this template, choose Tools | Templates
* and open the template in the editor.
*/
class middlewareBankIntegrationManager implements middlewareBankIntegrationService {
	public function getBankResponse($requestXml, $bankId, $requestId) {
		if ($requestXml) {
			$bankDetail = Doctrine::getTable('bank')->findById($bankId);
			$bankName = str_replace(' ', '_', $bankDetail[0]['bank_name']);
			$bankObject =  'middleware'.$bankName.'Manager';
			if (class_exists($bankObject)) {
				$middlewareObj = new $bankObject;
				return $responseXml = $middlewareObj->processRequest($requestXml,$bankId,$requestId);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
?>
