<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentReversalManager
 *
 * @author akumar1
 */
class PaymentReversalManager {

  public function isEwalletCollectionAccount($masterAccountIdArr){
    return Doctrine::getTable('ServiceBankConfiguration')->isEwalletCollectionAccount($masterAccountIdArr);
  }

  public function isAlreadyReverse($validation_no){
    return Doctrine::getTable('PaymentReversal')->isAlreadyReverse($validation_no);
  }

  public function saveReversalValidationNumber($validation_no, $reverse_validation_number){
    return Doctrine::getTable('PaymentReversal')->saveReversalValidationNumber($validation_no, $reverse_validation_number);
  }
}
?>
