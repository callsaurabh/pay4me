<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of lockingManagerclass
 *
 * @author spandey
 */
class lockingManager {

 /**
 * The lockObj that is used by the locking manager
 * The lockingManager
 * @var Doctrine_Connection object
 */
    private $lockObj;
    private $lockingManager;

   /**
     * Constructs a new locking manager object*/

    public function __construct($model,$recordId)
    {
        $this->lockObj = Doctrine_Core::getTable($model)->find($recordId);
        $this->lockingManager = new Locking_Pessimistic(Doctrine_Manager::connection());
    }

 // WP029 Function for get lock through pessimistic offline locking

    public function getLock($lockName) {
        try
        {

            $this->lockingManager->releaseAgedLocks(300);
            $isFreelock = $this->lockingManager->isLocked($this->lockObj, $lockName);
            if(!$isFreelock){
                $gotLock = $this->lockingManager->getLock($this->lockObj, $lockName);
                return true;
            } else {
                throw New Exception("Your Transaction has already been processed.<br/> <a href='accountSearch'>Click Here</a>  for to view detail");
            }


        } catch(Doctrine_Locking_Exception $e) {
            throw $e;
           
        }

    }

 // WP029 Function for release lock

    public function releaseLock($lockName) {
        try
        {
           // sleep(10);
            $freeLock = $this->lockingManager->releaseLock($this->lockObj, $lockName);
            if ($freeLock) {
                return true;
              
            } else {
                return false;
              
            }

        } catch(Doctrine_Locking_Exception $e) {
            throw $e;
            // handle the error
        }
    }
    
}
?>
