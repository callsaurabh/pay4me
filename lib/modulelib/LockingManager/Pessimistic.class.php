<?php
/* 
 * Overriding the Doctrine Locking Manager Class to Lock the record and not the table
 * Introducing a new fucntion to determine if the lock alredy exixts on a Doctrine_Record
 */

class Locking_Pessimistic extends Doctrine_Locking_Manager_Pessimistic {
/**
 * The conn that is used by the locking manager
 *
 * @var Doctrine_Connection object
 */
  private $conn;

  /**
   * The database table name for the lock tracking
   */
  private $_lockTable = 'doctrine_lock_tracking';

  /**
   * Constructs a new locking manager object
   *
   * When the CREATE_TABLES attribute of the connection on which the manager
   * is supposed to work on is set to true, the locking table is created.
   *
   * @param Doctrine_Connection $conn The database connection to use
   */
  public function __construct(Doctrine_Connection $conn) {
    $this->conn = $conn;
    parent::__construct($conn);
  }


  /**
   * Determines if a  {@link Doctrine_Record} is locked
   *
   * @param  Doctrine_Record $record     The record that has to be locked
   * @param  mixed           $userIdent  A unique identifier of the locking user
   * @return boolean  1 if the lock exists
   * @throws Doctrine_Locking_Exception  If the sql failed due to database errors
   */

  public function isLocked(Doctrine_Record $record, $userIdent) {
    $objectType = $record->getTable()->getComponentName();
    $key = $record->get($record->getTable()->getIdentifier());
    if (is_array($key)) {
    // Composite key
      $key = implode('|', $key);
    }

    $lock_free=false;

    try {
      $dbh = $this->conn->getDbh();
      $stmt = $dbh->prepare('SELECT * FROM ' . $this->_lockTable
          . ' WHERE object_type = :object_type AND object_key = :object_key AND user_ident = :user_ident');
      $stmt->bindParam(':object_type', $objectType);
      $stmt->bindParam(':object_key', $key);
      $stmt->bindParam(':user_ident', $userIdent);
      $stmt->execute();

      $count = $stmt->rowCount();
      if($count) {
          $lock_free=true;
      }
      return $lock_free;
    } catch (PDOException $pdoe) {
      throw new Doctrine_Locking_Exception($pdoe->getMessage());
    }

  }

  /**
   * Obtains a lock on a {@link Doctrine_Record}
   *
   * @param  Doctrine_Record $record     The record that has to be locked
   * @param  mixed           $userIdent  A unique identifier of the locking user
   * @return boolean  TRUE if the locking was successful, FALSE if another user
   *                  holds a lock on this record
   * @throws Doctrine_Locking_Exception  If the locking failed due to database errors
   */
  public function getLock(Doctrine_Record $record, $userIdent) {
    $objectType = $record->getTable()->getComponentName();
    $key = $record->get($record->getTable()->getIdentifier());



    $gotLock = false;
    $time = time();

    if (is_array($key)) {
    // Composite key
      $key = implode('|', $key);
    }

    try {
      $dbh = $this->conn->getDbh();
      $this->conn->beginTransaction();

      $stmt = $dbh->prepare('INSERT INTO ' . $this->_lockTable
          . ' (object_type, object_key, user_ident, timestamp_obtained)'
          . ' VALUES (:object_type, :object_key, :user_ident, :ts_obtained)');

      $stmt->bindParam(':object_type', $objectType);
      $stmt->bindParam(':object_key', $key);
      $stmt->bindParam(':user_ident', $userIdent);
      $stmt->bindParam(':ts_obtained', $time);

      try {
        $stmt->execute();
        $gotLock = true;

      // we catch an Exception here instead of PDOException since we might also be catching Doctrine_Exception
      } catch(Exception $pkviolation) {
      // PK violation occured => existing lock!
      }

      if ( ! $gotLock) {
        $lockingUserIdent = $this->_getLockingUserIdent($objectType, $key);
        if ($lockingUserIdent !== null && $lockingUserIdent == $userIdent) {
          $gotLock = true; // The requesting user already has a lock
          // Update timestamp
          $stmt = $dbh->prepare('UPDATE ' . $this->_lockTable
              . ' SET timestamp_obtained = :ts'
              . ' WHERE object_type = :object_type AND'
              . ' object_key  = :object_key  AND'
              . ' user_ident  = :user_ident');
          $stmt->bindParam(':ts', $time);
          $stmt->bindParam(':object_type', $objectType);
          $stmt->bindParam(':object_key', $key);
          $stmt->bindParam(':user_ident', $lockingUserIdent);
          $stmt->execute();
        }
      }
      $this->conn->commit();
    } catch (Exception $pdoe) {
      $this->conn->rollback();
      throw new Doctrine_Locking_Exception($pdoe->getMessage());
    }

    return $gotLock;
  }
      /**
     * Releases a lock on a {@link Doctrine_Record}
     *
     * @param  Doctrine_Record $record    The record for which the lock has to be released
     * @param  mixed           $userIdent The unique identifier of the locking user
     * @return boolean  TRUE if a lock was released, FALSE if no lock was released
     * @throws Doctrine_Locking_Exception If the release procedure failed due to database errors
     */
    public function releaseLock(Doctrine_Record $record, $userIdent)
    {
        $objectType = $record->getTable()->getComponentName();
        $key = $record->get($record->getTable()->getIdentifier());
        if (is_array($key)) {
            // Composite key
            $key = implode('|', $key);
        }

        try {
            $dbh = $this->conn->getDbh();
            $stmt = $dbh->prepare("DELETE FROM $this->_lockTable WHERE
                                        object_type = :object_type AND
                                        object_key  = :object_key  AND
                                        user_ident  = :user_ident");
            $stmt->bindParam(':object_type', $objectType);
            $stmt->bindParam(':object_key', $key);
            $stmt->bindParam(':user_ident', $userIdent);
            $stmt->execute();

            $count = $stmt->rowCount();

            return ($count > 0);
        } catch (PDOException $pdoe) {
            throw new Doctrine_Locking_Exception($pdoe->getMessage());
        }
    }

    

}

?>
