<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class pay4meException{

     //error codes
     public static $PAYMENT_SUCCESS = 0;
     public static $NO_PAYMENT_ATTEMPT = 1;

     //error messages
     public static $MSG_ERR_0 = 'Payment Successful';
     public static $MSG_ERR_1 = 'No payment attempt';


    public static function throwException($errorCode){
        $logger=sfContext::getInstance()->getLogger();
        $logger->info("Throwing the exception");
        throw new Exception(self::getExceptionDescription($errorCode));
    }


    public static function getExceptionDescription($errorCode){
        $descVar = "MSG_ERR_".$errorCode;
        $description = self::$$descVar;        
        return $description;
    }

}



?>
