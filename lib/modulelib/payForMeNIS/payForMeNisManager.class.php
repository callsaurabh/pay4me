<?php
  class payForMeNisManager implements payForMeNisService
  {
    public function getNisDetails($transactionId)
    {
    
      $nisDetails = array();

      $payForMeManagerObj = payForMeServiceFactory::getService('nis');

      $txnId = $payForMeManagerObj->getTransactionDetails($transactionId);

      $nisDetails = array_merge($nisDetails,$txnId);

      $appCharge = $payForMeManagerObj->getService($transactionId);
      $nisDetails = array_merge($nisDetails,$appCharge);


      $pfmTransactionDetails = TransactionTable::getTransactionDetails($transactionId);
      $merchantServiceId = $pfmTransactionDetails['merchant_service_id'];

   //   $bankCharge = $payForMeManagerObj->getBank($merchantServiceId);
      $nisDetails = array_merge($nisDetails,$bankCharge);

     

      $totalCharges = $payForMeManagerObj->getTotal($appCharge, $bankCharge);
      $nisDetails = array_merge($nisDetails,$totalCharges);

      $nisManagerObj = nisServiceFactory::getService('nis');

      $nisServiceDetails = $nisManagerObj->getServiceDetails($transactionId);
      $nisDetails = array_merge($nisDetails,$nisServiceDetails);

      return $nisDetails;
    }
private function checkDuplicate($item_num,$merchantServiceId){
  
    $query = Doctrine_Query::create();
    $query->from('MerchantItem');
    $query->andwhere('item_number = ?',$item_num);
    $query->andwhere('merchant_service_id = ?',$merchantServiceId);    
    $rs = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    if($rs){
      $record = $rs[0]['id'];
    }else{
      $record = 0;
    }
    //print_r($record);die();
    return $record;
}
   public function saveRequest($formData)
  {
    $merchantId = $this->getMerchantId($formData['service_type']);
    $merchantServiceId = $this->getMerchantServiceId($merchantId,$formData['app_type']);// static for NIS
    $chekDuplicate = $this->checkDuplicate($formData['paymentId'],$merchantServiceId);
    if(empty($chekDuplicate)){
      $mItem = new MerchantItem();
      $mItem->item_number = $formData['paymentId'];
      $mItem->merchant_service_id = $merchantServiceId ;
      $mItem->payment_amount_requested = $formData['amount'];
      $mItem->save();
      $mItemId = $mItem->getId();
    }else{
    $mItemId = $chekDuplicate;
    }
   // echo $mItemId ;

    $mRequest = new MerchantRequest();
    $mRequest->txn_ref = $formData['txn_ref'];
    $mRequest->merchant_id= $merchantId;
    $mRequest->merchant_service_id= $merchantServiceId;
    $mRequest->merchant_item_id= $mItemId;
    $mRequest->item_fee= $formData['amount'];
    //$mRequest->payment_mode= '';
    //$mRequest->bank_name= '';
    $mRequest->payment_status_code= $this->getPaymentStatusCode();
    //$mRequest->paid_amount= '';
    $mRequest->save();
    $mRequestId = $mRequest->getId();
  //  echo $mRequestId;

    $mRequestDetails = new MerchantRequestDetails();
    $mRequestDetails->merchant_service_id  	= $merchantServiceId;
    $mRequestDetails->merchant_request_id = $mRequestId;
    $mRequestDetails->iparam_one 	= $formData['app_id'];
    $mRequestDetails->iparam_two 	= $formData['reference_id'];
    $mRequestDetails->first_name 	= $formData['first_name'];
    $mRequestDetails->last_name 	= $formData['last_name'];
    $mRequestDetails->sparam_one= $formData['app_type'];
    $mRequestDetails->sparam_two= $formData['mobile_no'];
    $mRequestDetails->sparam_three 	= $formData['email_add'];
    $mRequestDetails->save();
    $mRequestDetailsId = $mRequest->getId();
    //echo $mRequestDetailsId;

    return $mRequestId;
  }

  static function getMerchantServiceId($merchantId,$serviceName){
    $query = Doctrine_Query::create();
    $query->from('MerchantService');
    $query->andwhere('merchant_id = ?',$merchantId);
    $query->andwhere('name = ?',$serviceName);
    $rs = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    $record = $rs[0];
    //print_r($record);die();
    return $record['id'];
  }

  static function getMerchantId($merchantName){
    $query = Doctrine_Query::create();
    $query->from('Merchant');
    $query->andwhere('name = ?',$merchantName);
    $rs = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    $record = $rs[0];
    //print_r($record);die();
    return $record['id'];
  }

  static function getPaymentModeOptionId($optionText){
    $query = Doctrine_Query::create();
    $query->from('PaymentModeOption');
    $query->andwhere('name = ?',$optionText);
    $rs = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    $record = $rs[0];
    //print_r($record);die();
    return $record['id'];
  }
  static function getPaymentStatusCode(){
    return '01';
  }

  static function getTransactionDetails($transNum){

    $query = Doctrine_Query::create();
    $query->from('Transaction t')
    ->leftJoin('t.MerchantRequest r')
   // ->leftJoin('r.MerchantItem i')
    ->leftJoin('r.MerchantRequestDetails d');
    $query->andwhere('t.pfm_transaction_number = ?',$transNum);
    //echo $query->getQuery();die($transNum);
    $result = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    $rs = $result[0];
    //print_r($rs);die($rs['MerchantRequest']['MerchantRequestDetails']);
    $transDetails = array(
      'transaction_num' => $rs['pfm_transaction_number'],
      'service_type'=> $rs['MerchantRequest']['merchant_id'],
      'txn_ref' =>$rs['MerchantRequest']['txn_ref'],
      'application_id'=> $rs['MerchantRequest']['MerchantRequestDetails'][0]['iparam_one'],
      'ref_no'=> $rs['MerchantRequest']['MerchantRequestDetails'][0]['iparam_two'],
      'application_type' => $rs['MerchantRequest']['MerchantRequestDetails'][0]['sparam_one'],
      'title'=> '',
      'name'=> $rs['MerchantRequest']['MerchantRequestDetails'][0]['name'],
      'mobile'=> $rs['MerchantRequest']['MerchantRequestDetails'][0]['sparam_two'],
      'email'=> $rs['MerchantRequest']['MerchantRequestDetails'][0]['sparam_three'],
      'app_charges' => $rs['MerchantRequest']['item_fee'],
      'bank_charges' => self::getBankCharges($rs['MerchantRequest']['merchant_id']),
      'merchant_service_id'=>$rs['MerchantRequest']['merchant_service_id']
      );
    //return $record['id'];
    return $transDetails;
  }



  }
?>
