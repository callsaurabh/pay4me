<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class csvSave {

  public static function saveExportListFile($arrList,$filePath,$fileName) {
    try {
      $download_Path   = sfConfig::get('sf_upload_dir').$filePath;
      if(!is_dir($download_Path)) {
         $dir_path=$download_Path."/";
         mkdir($dir_path,0777,true);
         chmod($dir_path, 0777);
      }
      $strFile = $fileName."_".date("Ymdhis");
      $filepath = $download_Path .$strFile.".csv";
      $strFileName = self::makeCsvFile($arrList, $filepath,",","\"");
      $arrFileInfo = pathinfo($strFileName);
      $path = "../../uploads/".$filePath.$arrFileInfo['basename'];
      return $strFile."#$".$path;
    //                $this->redirect_url($path);
    } catch(Exception $e) {
      echo 'problem found' . $e->getMessage() . "\n";die;
    }
  }

  /*
   * new function made to omit date from concatinating at the end
   * 
   * @author ramandeep
   */
    public static function saveExportListFileNew($arrList,$filePath,$fileName) {
    try {
      $download_Path   = sfConfig::get('sf_upload_dir').$filePath;
      if(!is_dir($download_Path)) {
         $dir_path=$download_Path."/";
         mkdir($dir_path,0777,true);
         chmod($dir_path, 0777);
      }
      $strFile = $fileName."_".date("Ymdhis");
      $filepath = $download_Path .$strFile.".csv";
      $strFileName = self::createCsvFile($arrList, $filepath,",","\"");
      if($strFileName){
          return array(
              "message" => "Report Generated Successfully",
              "file_name" => $strFile,
          );
      }else{
          return "Some Error";
      }
      
    } catch(Exception $e) {
      return 'problem found' . $e->getMessage() . "\n";
    }
  }
  
  /*
   * new function made so that file should not get open in append mode
   * 
   * @author ramandeep
   */
  public static function createCsvFile($dataArray,$filename,$delimiter=",",$enclosure="\"") {
  // Build the string

    $fh = fopen($filename,"w");
    // for each array element, which represents a line in the csv file...
    foreach($dataArray as $line) {
      $string = "";
      // No leading delimiter
      $writeDelimiter = FALSE;

      foreach($line as $dataElement) {
      // Replaces a double quote with two double quotes
        $dataElement=str_replace("\"", "\"\"", $dataElement);
        $dataElement=str_replace("\n", "", $dataElement);
        $dataElement=str_replace("\r", "", $dataElement);

        // Adds a delimiter before each field (except the first)
        if($writeDelimiter) $string .= $delimiter;

        // Encloses each field with $enclosure and adds it to the string
        $string .= $enclosure . $dataElement . $enclosure;

        // Delimiters are used every time except the first.
        $writeDelimiter = TRUE;
      }
      // Append new line
      $string .= "\n";

      fwrite($fh,$string);
    } // end foreach($dataArray as $line)
    fclose($fh);
    return $filename;
  }
  
  /*******************common functions for save the reports data***********************/
  public static function makeCsvFile($dataArray,$filename,$delimiter=",",$enclosure="\"") {
  // Build the string

    $fh = fopen($filename,"a");
    // for each array element, which represents a line in the csv file...
    foreach($dataArray as $line) {
      $string = "";
      // No leading delimiter
      $writeDelimiter = FALSE;

      foreach($line as $dataElement) {
      // Replaces a double quote with two double quotes
        $dataElement=str_replace("\"", "\"\"", $dataElement);
        $dataElement=str_replace("\n", "", $dataElement);
        $dataElement=str_replace("\r", "", $dataElement);

        // Adds a delimiter before each field (except the first)
        if($writeDelimiter) $string .= $delimiter;

        // Encloses each field with $enclosure and adds it to the string
        $string .= $enclosure . $dataElement . $enclosure;

        // Delimiters are used every time except the first.
        $writeDelimiter = TRUE;
      }
      // Append new line
      $string .= "\n";

      fwrite($fh,$string);
    } // end foreach($dataArray as $line)
    fclose($fh);
    return $filename;
  }
/*******************end the common function***********************/


}

?>
