<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of merchantConfigurationServiceFactoryclass
 *
 * @author sdutt
 */
class paymentScheduleConfigServiceFactory {
    //put your code here

   public static $merchantConfig = 'MERCHANT';
   public static $paymentModeConfig = 'PAYMENT_MODE';
  /**
  *
  * @param <type> $type
  * @return empService the service implementation
  */
    public static function getService($type) {
        switch($type){

            case 'MERCHANT':
                return new merchantConfigurationManager();
                break;
            case 'PAYMENT_MODE':
                return new paymentModeConfigManager();
                break;
        }        
    }
}
?>
