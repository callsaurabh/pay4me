<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of service
 *
 * @author sdutt
 */
class paymentModeConfigManager implements paymentScheduleConfigService{
  //put your code here

  public function isPaymentServiceActive($merchantId,$merchantServiceId,$paymentModeId,$paymentModeOptionId){
    $currentDate = date( 'Y-m-d H:i:s', time());
    $val = Doctrine::getTable('ScheduledPaymentConfig')->getPaymentServiceStatus($merchantId,$merchantServiceId,$paymentModeId,$paymentModeOptionId);
    if(count($val)>0){
      if($val[0]['start_date']<=$currentDate && (empty($val[0]['end_date']) || $val[0]['end_date']>=$currentDate)){
        return false;
      }else{
        return  true;
      }
    }else{
      //if data is not availiable on record set, then return true or permit the merchant payment option service
      return true ;
    }
  }

  public function isPaymentModeActive($merchantId,$merchantServiceId){
    $val = Doctrine::getTable('ScheduledPaymentConfig')->getPaymentModeStatus($merchantId,$merchantServiceId);
    if(count($val)>0){
        return $val;
    }else{
      //if data is not availiable on record set, then return true or permit the merchant payment option service
      return false ;
    }
  }
}
?>
