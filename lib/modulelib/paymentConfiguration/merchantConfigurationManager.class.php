<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of service
 *
 * @author sdutt
 */
class merchantConfigurationManager implements paymentScheduleConfigService{
  //put your code here
  public function isMerchantServiceActive($merchantCode,$merchantServiceId){
    $merArr = Doctrine::getTable('Merchant')->getMerchantDetails($merchantCode);
    $merchantId = $merArr['id'];
    return $this->getMerchantServiceStatus($merchantId,$merchantServiceId);
  }

  public function getMerchantServiceStatus($merchantId,$merchantServiceId){

    $currentDate = date( 'Y-m-d H:i:s', time());
    $val = Doctrine::getTable('ScheduledMerchantConfig')->getMerchantServiceStatus($merchantId,$merchantServiceId);
    if(count($val)>0){
      if($val[0]['start_date']<=$currentDate && (empty($val[0]['end_date']) || $val[0]['end_date']>=$currentDate)){
        return false;
      }else{
        return  true;
      }
    }else{
      //if data is not availiable on record set, then return true or permit the merchant service
      return true ;
    }

  }

  public function saveMerchantServiceConfiguration($merchentId,$merchentServiceId,$sDate,$eDate){
    $sdate = $this->calculateDate($sDate);
    $edate = $this->calculateDate($eDate);
    return Doctrine::getTable('ScheduledMerchantConfig')->saveTransaction($merchentId, $merchentServiceId, $sdate, $edate);
  }


  public function savePaymentServiceConfiguration($paymentId,$paymentModeId,$merchentId,$merchentServiceId,$sDate,$eDate){
    $sdate = $this->calculateDate($sDate);
    $edate = $this->calculateDate($eDate);
    return Doctrine::getTable('ScheduledPaymentConfig')->saveTransaction($paymentId,$paymentModeId, $merchentId, $merchentServiceId, $sdate, $edate);
  }

  function calculateDate($date){
    $retDate = "";
    if($date['year'] !='')
    {
      if($date['year'] != '')
      $retDate.= $date['year'];
      if($date['month'] != '')
      $retDate.= "-".$date['month'];
      if($date['day'] != '')
      $retDate.= "-".$date['day'];
      if($date['hour'] != '')
      $retDate.= " ".$date['hour'];
      else
      $retDate.= " 00";
      if($date['minute'] != '')
      $retDate.= ":".$date['minute'];
      else
      $retDate.= ":00";
      if($date['second'] != '')
      $retDate.= ":".$date['second'];
      else
      $retDate.= ":00";
      $retDate = date('Y-m-d H:i:s ', strtotime($retDate));
    }
    return $retDate;
  }
}
?>
