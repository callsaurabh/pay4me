<?php
class paymentSystemServiceFactory{
 /*
  *
  * @param <type> $type integer
  */
 public static function getService($environment='') {
    if($environment == "sandbox") {
      $var = ('paymentSystem'.ucfirst(strtolower($environment)).'Manager');
      return new $var;
    }
    else {
      return new paymentSystemManager();
    }
   
 }
}
?>