<?php

class Payment {
  public $txnId;
  public $pfmTransactionDetails;
  public $payment_mode_option;
  public $platform='web';
  public function __construct($txnId) {
    $this->txnId = $txnId;
    //possible values for platform is web/mobile
    $this->platform = 'web';
    $pfmTransactionDetails = Doctrine::getTable('Transaction')->getTransactionDetails($txnId);
    $this->payment_mode_option = $pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name'];
  }

  public function proceedToPay() {
    // $this->payment_mode_option="credit_card";
   $payment_option_name = ucwords(str_replace("_","",$this->payment_mode_option));

    $class_name = ($payment_option_name)."Pay";
    if(class_exists($class_name)) {
      $gUser = sfContext::getInstance()->getUser()->getGuardUser();
      if($class_name == 'BankPay' && $gUser->getBankUser()->getFirst()->getBank()->getBiEnabled()){
        $payObj = new BankPayDecorater('bank',$this->txnId);
      }else if($class_name == 'BankdraftPay' && $gUser->getBankUser()->getFirst()->getBank()->getBiEnabled()){
        $payObj = new BankPayDecorater('draft',$this->txnId);
      }else if($class_name == 'ChequePay' && $gUser->getBankUser()->getFirst()->getBank()->getBiEnabled()){
        $payObj = new BankPayDecorater('Cheque',$this->txnId);
      }else{
        $payObj = new $class_name($this->txnId);
      }
    }
     else { //for merchant-counter
            $payObj = new BankPay($this->txnId);
     }
        $payObj->platform = $this->platform;

        return $payObj->payApplication();
    }

}

class PaymentSystemManager {

    public function checkIsSuperAdmin() {
        if (sfContext::getInstance()->getUser()->isAuthenticated()) {
            $group_name = sfContext::getInstance()->getUser()->getGroupNames();
            if (in_array(sfConfig::get('app_pfm_role_admin'), $group_name)) {
                $this->redirect('@adminhome');
            }
        }
    }

    public function validateTransactionNumber($txnId, $bankId="") {
        //print $txnId;
        //print "<br>".$bankId;exit;
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId, $bankId);
        if (count($pfmTransactionDetails) == 0) {
            $arrayVal['comments'] = 'error';
            $arrayVal['comments_val'] = 'Transaction Id does not exist !!!';
            $arrayVal['var_name'] = 'pfmTransactionDetails';
            $arrayVal['var_value'] = $pfmTransactionDetails;
            $arrayVal['module'] = 'admin';
            $gUser = sfContext::getInstance()->getUser()->getGuardUser();
            $group_name = $gUser->getGroupNames();
            if (in_array(sfConfig::get('app_pfm_role_bank_branch_user'), $group_name))
                $action = 'bankUser';
            else
            if (in_array(sfConfig::get('app_pfm_role_ewallet_user'), $group_name))
                $action = 'eWalletUser';
            $arrayVal['action'] = $action;
            return $arrayVal;
        }
        return $pfmTransactionDetails;
    }

    public function chkPaymentStatus() {

        $pfmTransactionDetails = $this->pfmTransactionDetails;
        $txnId = $pfmTransactionDetails['pfm_transaction_number'];
        $item_id = $pfmTransactionDetails['MerchantRequest']['merchant_item_id'];
        $merchant_request_id = $pfmTransactionDetails['MerchantRequest']['id'];
        $this->serviceTypeStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
        if ($this->serviceTypeStatus != '' && $this->serviceTypeStatus != 0) {
            return;
        } else {

            $transactionArray = Doctrine::getTable('Transaction')->getPayDetailfrmMerchantItemId($item_id);
            $payment_transaction_number = $transactionArray['pfm_transaction_number'];
            $arrayVal['comments'] = 'error';
            $arrayVal['comments_val'] = 'Payment has already been made for this Application.';
            if ($payment_transaction_number == $txnId) {

                $arrayVal['var_name'] = 'pfmTransactionDetails';
                $arrayVal['var_value'] = $pfmTransactionDetails;
            } else {
                $pfmTransactionDetails = TransactionTable::getTransactionDetails($payment_transaction_number);
                sfContext::getInstance()->getRequest()->setParameter('trans_number', $payment_transaction_number);
                $arrayVal['var_name_trans'] = 'trans_number';
                $arrayVal['var_value_trans'] = $payment_transaction_number;
            }
            sfContext::getInstance()->getRequest()->setParameter('duplicate_payment_receipt', 'true');
            $arrayVal['var_name'] = 'pfmTransactionDetails';
            $arrayVal['var_value'] = $pfmTransactionDetails;
            $arrayVal['module'] = 'paymentSystem';
            $arrayVal['action'] = 'payOnline';
            return $arrayVal;
        }
    }

    public function checkIfPaymentActive() {
        $pfmTransactionDetails = $this->pfmTransactionDetails;
        $paymentModeConfigManager = new paymentModeConfigManager();
        if (!$paymentModeConfigManager->isPaymentServiceActive($pfmTransactionDetails['merchant_id'], $pfmTransactionDetails['merchant_service_id'], $pfmTransactionDetails['payment_mode_id'], $pfmTransactionDetails['payment_mode_option_id'])) {
            ## sfContext::getInstance()->getUser()->setFlash('error', 'You are restricted to pay for this Merchant Service', true) ;
            ## sfContext::getInstance()->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
            ## sfContext::getInstance()->getController()->forward('paymentSystem', 'payOnline');
            $arrayVal['comments'] = 'error';
            $arrayVal['comments_val'] = 'You are restricted to pay for this Merchant Service';
            $arrayVal['var_name'] = 'pfmTransactionDetails';
            $arrayVal['var_value'] = $pfmTransactionDetails;
            $arrayVal['module'] = 'paymentSystem';
            $arrayVal['action'] = 'payOnline';
            return $arrayVal;
        } else {
            return;
        }
    }

  public function doPayment(array $postDataArray=NULL,$txnId) {
    $paymentObj = new payForMeManager();
    //set platform is web or Mobile

        if (!isset($this->platform)) {
            $this->platform = 'web';
        }
        if ($this->platform == 'mobile') {
            $paymentObj->platform = 'mobile';
        } else {
            $paymentObj->platform = 'web';
        }
        $flag = $paymentObj->doPay($postDataArray, $txnId, false);
    }

    public function updatePaymentAccounts($txnId, $currencyId = 1) {
        $paymentObj = new payForMeManager();
//    if($currencyId == 1) {
        return $paymentObj->splitPayment($txnId);
//    }
//    else {
//      return $paymentObj->splitUsdPayment($txnId);
//    }
    }

    public function makePayment($postDataArray) {
        $pfmTransactionDetails = $this->pfmTransactionDetails;
        $txnId = $pfmTransactionDetails['pfm_transaction_number'];
        $returnArr = $this->chkPaymentStatus();
        if (is_array($returnArr)) {
            return $returnArr;
        }
        $returnArr = $this->checkIfPaymentActive();
        if (is_array($returnArr))
            return $returnArr;

        $postDataArray['id'] = $pfmTransactionDetails['merchant_request_id'];
        $postDataArray['paid_amount'] = $pfmTransactionDetails['total_amount'];
        $postDataArray['version'] = $pfmTransactionDetails['MerchantRequest']['version'];

        // print "<pre>";print_r($pfmTransactionDetails);exit;
        $con = Doctrine_Manager::connection();
        if(sfContext::getInstance()->getUser()->getGuardUser())
            $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId(); // not possible in case of internet bank
//    $postDataArray['userId'] = $userId;

        try {
            $con->beginTransaction();
            $postDataArray['paid_date'] = date("Y-m-d H:i:s");
            $postDataArray['payment_status_code'] = '0';
            if(isset($userId))
                $postDataArray['paid_by'] = $userId;
            $flag = $this->doPayment($postDataArray, $txnId);
            $currency_id = $pfmTransactionDetails['MerchantRequest']['currency_id'];
            $payment_mode_option_id = $pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'];
            if ($payment_mode_option_id == '4') {
                $accountingObj = new pay4meAccounting();
                $accountingObj->updateEwalletAccount($txnId);
            }

            $con->commit();
            $payment_done = 0;
        } catch (Exception $e) {
            $merchant_request_id = $pfmTransactionDetails['merchant_request_id'];
            $con->rollback();
            sfContext::getInstance()->getLogger()->info("Rollback for transaction id - " . $txnId . " - for merchant request - " . $merchant_request_id . " " . $e->getMessage());
            if ($e->getMessage() == "Payment already done") {
                $transactionObj = Doctrine::getTable('Transaction')->getPayDetailfrmMerchantReqId($merchant_request_id);
                $payment_status_code = $transactionObj['payment_status_code'];
                if ($payment_status_code == 0) {
                    $pfmTransactionDetails = $this->validateTransactionNumber($txnId);
                    $payment_done = 0;
                }
            } else if ($e->getMessage() == "Payment is in process") {
                $arrayVal = $this->createReturnArray('bill', 'paymentProcessing', 'pfmTransactionDetails', $pfmTransactionDetails, '', '');
                return $arrayVal;
            } else {
                $payment_done = 1;
            }
            // throw $e;
        }

        return $payment_done;
    }

    public function createLog($xmldata, $nameFormate) {
        /////log generation///
        //log xml
        $pay4meLog = new pay4meLog();
        $pay4meLog->createLogData($xmldata, $nameFormate);
      }

    }

abstract class Pay extends paymentSystemManager {

    public $txnId;
    public $payment_mode_option;

    protected function setTransactionId($txnId) {
        $this->txnId = $txnId;
    }

    protected function setPaymentModeOption($payment_mode_option) {
        $this->payment_mode_option = $payment_mode_option;
    }

    abstract public function getLoggedInUserDetails();

    abstract protected function validateTxnNumber();

    abstract protected function payApplication();

    protected function createReturnArray($module, $action, $param_name, $param_val, $comment, $comment_val) {
        $returnArray = array();
        $arrayVal['comments'] = $comment;
        $arrayVal['comments_val'] = $comment_val;
        $arrayVal['var_name'] = $param_name;
        $arrayVal['var_value'] = $param_val;
        $arrayVal['module'] = $module;
        $arrayVal['action'] = $action;
        return $arrayVal;
    }

    public function addJobForMail() {
        $notificationUrl = 'email/sendMail';
        $taskId = EpjobsContext::getInstance()->addJob('PaymentEmailNotification', $notificationUrl, array('p4m_transaction_id' => $this->txnId));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
    }

}

class BankPay extends Pay {

    //$pfmTransactionDetails = $this->validateTransactionNumber($txnId, $bankId);

    protected $pfmTransactionDetails;
    protected $bankId;
    public $paymentType="Bank";

  public function __construct($txnId) {
    $gUser = sfContext::getInstance()->getUser()->getGuardUser();
    $bUser = $gUser->getBankUser()->getFirst();
    $this->bankId =  $bUser->getBankId();
    parent::setTransactionId($txnId);
    parent::setPaymentModeOption(sfConfig::get('app_payment_mode_option_bank'));
  }

  public function payApplication() {
    $pfmTransactionDetails = $this->validateTxnNumber();
    $tellerDetails = $this->getLoggedInUserDetails();
    $postDataArray = array();
    $postDataArray['bank_id'] = $tellerDetails['bank_id'];
    $postDataArray['bank_branch_id'] = $tellerDetails['bank_branch_id'];
    $postDataArray['bank_name'] = $tellerDetails['bank_name'];

        $payment_done = $this->makePayment($postDataArray);

        if (is_array($payment_done)) {
            return $payment_done;
        }

        if ($payment_done == 0) {
            $arrayVal = $this->createReturnArray('paymentSystem', 'payOnline', 'pfmTransactionDetails', $this->pfmTransactionDetails, 'notice', 'Payment Successful');
            return $arrayVal;
        } else {
            $merchant_request_id = $this->pfmTransactionDetails['merchant_request_id'];
            $this->createLog($this->txnId . "-" . $merchant_request_id, 'Rollback');
            sfContext::getInstance()->getLogger()->info("Payment through bank for " . $this->txnId . "-" . $merchant_request_id);
            $arrayVal = $this->createReturnArray('admin', 'bankUser', 'txnId', $this->txnId, 'error', 'Payment UnSuccessful. Please try again');
            return $arrayVal;
        }
    }

    public function getLoggedInUserDetails() {
        $teller_details = array();
        $gUser = sfContext::getInstance()->getUser()->getGuardUser();
        $bUser = $gUser->getBankUser()->getFirst();
        $teller_details['bank_id'] = $bUser->getBankId();
        $teller_details['bank_branch_id'] = $bUser->getBankBranchId();
        $teller_details['bank_name'] = $bUser->getBank()->getBankName();
        return $teller_details;
    }

    public function validateTxnNumber() {
        return $this->pfmTransactionDetails = parent::validateTransactionNumber($this->txnId, $this->bankId);
    }

}


class InternetbankPay extends Pay {

    //$pfmTransactionDetails = $this->validateTransactionNumber($txnId, $bankId);

    protected $pfmTransactionDetails;
    protected $bankId;
    public $paymentType="Bank";

  public function __construct($txnId) {
    //$gUser = sfContext::getInstance()->getUser()->getGuardUser();
    //$bUser = $gUser->getBankUser()->getFirst();
    //$this->bankId =  $bUser->getBankId();
    parent::setTransactionId($txnId);
    parent::setPaymentModeOption(sfConfig::get('app_payment_mode_option_internet_bank'));
  }

  public function payApplication() {
    $pfmTransactionDetails = $this->validateTxnNumber();
    $tellerDetails = $this->getLoggedInUserDetails();
    $postDataArray = array();
    $postDataArray['bank_id'] = $tellerDetails['bank_id'];
    $postDataArray['bank_branch_id'] = $tellerDetails['bank_branch_id'];
    $postDataArray['bank_name'] = $tellerDetails['bank_name'];

        $payment_done = $this->makePayment($postDataArray);



        if (is_array($payment_done)) {
            return $payment_done;
        }

        if ($payment_done == 0) {
            $arrayVal = $this->createReturnArray('paymentSystem', 'payOnline', 'pfmTransactionDetails', $this->pfmTransactionDetails, 'notice', 'Payment Successful');
            return $arrayVal;
        } else {
            $merchant_request_id = $this->pfmTransactionDetails['merchant_request_id'];
            $this->createLog($this->txnId . "-" . $merchant_request_id, 'Rollback');
            sfContext::getInstance()->getLogger()->info("Payment through bank for " . $this->txnId . "-" . $merchant_request_id);
            $arrayVal = $this->createReturnArray('admin', 'bankUser', 'txnId', $this->txnId, 'error', 'Payment UnSuccessful. Please try again');
            return $arrayVal;
        }
    }

    public function getLoggedInUserDetails() {
        $teller_details = array();
        $bankObj = Doctrine::getTable('Bank')->findByBankName('Internet Bank');
        $bankBranchObj = Doctrine::getTable('BankBranch')->findByName('Internet Bank');
        $teller_details['bank_id'] = $bankObj->getFirst()->getId();
        $teller_details['bank_branch_id'] = $bankBranchObj->getFirst()->getId();
        $teller_details['bank_name'] = 'Internet Bank';
        return $teller_details;
    }

    public function validateTxnNumber() {
        return $this->pfmTransactionDetails = parent::validateTransactionNumber($this->txnId, $this->bankId);
    }

}
class BankDraftPay extends BankPay {
  //$pfmTransactionDetails = $this->validateTransactionNumber($txnId, $bankId);
  public $paymentType="Bank Draft";
}

class ChequePay extends BankPay {
  //$pfmTransactionDetails = $this->validateTransactionNumber($txnId, $bankId);
  public $paymentType="Cheque";
}

class EwalletPay extends Pay {

    protected $pfmTransactionDetails;

    public function __construct($txnId) {
        parent::setTransactionId($txnId);
        parent::setPaymentModeOption(sfConfig::get('app_payment_mode_option_eWallet'));

        //$this->pfmTransactionDetails = $this->validateTransactionNumber($txnId);
    }

    public function payApplication() {
        $balance_chk = $this->checkBalance();
        if (is_array($balance_chk)) {
            return $balance_chk;
        }
        $pfmTransactionDetails = $this->validateTxnNumber();
        $tellerDetails = $this->getLoggedInUserDetails(); //print "<pre>";
//print_R($tellerDetails);exit;
        $postDataArray = array();
        $postDataArray['bank_id'] = $tellerDetails['bank_id'];
        $postDataArray['bank_branch_id'] = $tellerDetails['bank_branch_id'];
        $postDataArray['bank_name'] = $tellerDetails['bank_name'];

        $payment_done = $this->makePayment($postDataArray);

        if (is_array($payment_done)) {
            return $payment_done;
        }

        if ($payment_done == 0) {

            $billObj = billServiceFactory::getService();
            $billObj->updateBillStatus($this->pfmTransactionDetails['merchant_request_id']);
            parent::addJobForMail();
            $arrayVal = $this->createReturnArray('paymentSystem', 'payOnline', 'pfmTransactionDetails', $this->pfmTransactionDetails, 'notice', 'Payment Successful');
            return $arrayVal;
        } else {
            $merchant_request_id = $this->pfmTransactionDetails['merchant_request_id'];
            $this->createLog($this->txnId . "-" . $merchant_request_id, 'Rollback');
            sfContext::getInstance()->getLogger()->info("Payment through ewallet for " . $this->txnId . "-" . $merchant_request_id);
            $arrayVal = $this->createReturnArray('bill', 'show', 'trans_num', $this->txnId, 'error', 'Payment UnSuccessful. Please try again');
            return $arrayVal;
        }
    }

    public function validateTxnNumber() {
        return $this->pfmTransactionDetails = parent::validateTransactionNumber($this->txnId);
    }

    public function getLoggedInUserDetails() {
        $teller_details = array();
        $bankObj = Doctrine::getTable('Bank')->findByBankName('ewallet');
        $bankBranchObj = Doctrine::getTable('BankBranch')->findByName('ewallet');
        $teller_details['bank_id'] = $bankObj->getFirst()->getId();
        $teller_details['bank_branch_id'] = $bankBranchObj->getFirst()->getId();
        $teller_details['bank_name'] = 'eWallet';
        return $teller_details;
    }

    public function checkBalance() {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        if ($this->txnId != '') {
            $valid = $payForMeObj->checkSufficientBalance($this->txnId); //print "<pre>";
//print_r($valid);exit;
            if ($valid['flag'] == false) {
                $arrayVal['comments'] = 'error';
                $arrayVal['comments_val'] = 'Insufficient account balance';
                $arrayVal['var_name'] = 'trans_num';
                $arrayVal['var_value'] = $this->txnId;
                $arrayVal['module'] = 'bill';
                $arrayVal['action'] = 'show';
                // $this->redirect('@eWallethome?requestId='.$valid['requestId']);
                return $arrayVal;
            } else {
                return;
            }
        }
    }

}

class CreditcardPay extends Pay {

    protected $pfmTransactionDetails;

    public function __construct($txnId) {
        parent::setTransactionId($txnId);
        parent::setPaymentModeOption(sfConfig::get('app_payment_mode_option_credit_card'));

        //$this->pfmTransactionDetails = $this->validateTransactionNumber($txnId);
    }

    public function payApplication() {
        $pfmTransactionDetails = $this->validateTxnNumber();
        $tellerDetails = $this->getLoggedInUserDetails();
        $postDataArray = array();
        $postDataArray['bank_id'] = $tellerDetails['bank_id'];
        $postDataArray['bank_branch_id'] = $tellerDetails['bank_branch_id'];
        $postDataArray['bank_name'] = $tellerDetails['bank_name'];

        $payment_done = $this->makePayment($postDataArray);

        if (is_array($payment_done)) {
            return $payment_done;
        }

        if ($payment_done == 0) {
            parent::addJobForMail();
            $billObj = billServiceFactory::getService();
            $billObj->updateBillStatus($this->pfmTransactionDetails['merchant_request_id']);
            $arrayVal = $this->createReturnArray('paymentSystem', 'payOnline', 'pfmTransactionDetails', $this->txnId, 'notice', 'Payment Successful');
            return $arrayVal;
        } else {
            $merchant_request_id = $this->pfmTransactionDetails['merchant_request_id'];
            $this->createLog($this->txnId . "-" . $merchant_request_id, 'Rollback');
            sfContext::getInstance()->getLogger()->info("Payment through vbv for " . $this->txnId . "-" . $merchant_request_id);
            $arrayVal = $this->createReturnArray('bill', 'show', 'trans_num', $this->txnId, 'error', 'Payment UnSuccessful. Please try again');
            return $arrayVal;
        }
    }

    public function validateTxnNumber() {
        return $this->pfmTransactionDetails = parent::validateTransactionNumber($this->txnId);
    }

    public function getLoggedInUserDetails() {
        $teller_details = array();
        $bankObj = Doctrine::getTable('Bank')->findByBankName('vbv');
        $bankBranchObj = Doctrine::getTable('BankBranch')->findByName('vbv');
        $teller_details['bank_id'] = $bankObj->getFirst()->getId();
        $teller_details['bank_branch_id'] = $bankBranchObj->getFirst()->getId();
        $teller_details['bank_name'] = 'vbv';
        return $teller_details;
    }

}

class InterswitchPay extends Pay {

    protected $pfmTransactionDetails;

    public function __construct($txnId) {
        parent::setTransactionId($txnId);
        parent::setPaymentModeOption(sfConfig::get('app_payment_mode_option_interswitch'));
    }

    public function payApplication() {
        $pfmTransactionDetails = $this->validateTxnNumber();
        $tellerDetails = $this->getLoggedInUserDetails();
        $postDataArray = array();
        $postDataArray['bank_id'] = $tellerDetails['bank_id'];
        $postDataArray['bank_branch_id'] = $tellerDetails['bank_branch_id'];
        $postDataArray['bank_name'] = $tellerDetails['bank_name'];

        $payment_done = $this->makePayment($postDataArray);

        if (is_array($payment_done)) {
            return $payment_done;
        }

        if ($payment_done == 0) {
            parent::addJobForMail();
            $billObj = billServiceFactory::getService();
            $billObj->updateBillStatus($this->pfmTransactionDetails['merchant_request_id']);
            $arrayVal = $this->createReturnArray('interswitch_configuration', 'payOnline', 'pfmTransactionDetails', $this->txnId, 'notice', 'Payment Successful');
            return $arrayVal;
        } else {

            $this->createLog($this->txnId . "-" . $merchant_request_id, 'Rollback');
            sfContext::getInstance()->getLogger()->info("Payment through vbv for " . $this->txnId . "-" . $merchant_request_id);
            $arrayVal = $this->createReturnArray('bill', 'show', 'trans_num', $this->txnId, 'error', 'Payment UnSuccessful. Please try again');
            return $arrayVal;
        }
    }

    public function validateTxnNumber() {
        return $this->pfmTransactionDetails = parent::validateTransactionNumber($this->txnId);
    }

    public function getLoggedInUserDetails() {
        $teller_details = array();
        $bankObj = Doctrine::getTable('Bank')->findByBankName('interswitch');
        $bankBranchObj = Doctrine::getTable('BankBranch')->findByName('interswitch');
        $teller_details['bank_id'] = $bankObj->getFirst()->getId();
        $teller_details['bank_branch_id'] = $bankBranchObj->getFirst()->getId();
        $teller_details['bank_name'] = 'interswitch';
        return $teller_details;
    }

}

class EtranzactPay extends Pay {

    protected $pfmTransactionDetails;

    public function __construct($txnId) {
        parent::setTransactionId($txnId);
        parent::setPaymentModeOption(sfConfig::get('app_payment_mode_option_etranzact'));
    }

    public function payApplication() {

        $pfmTransactionDetails = $this->validateTxnNumber();
        $tellerDetails = $this->getLoggedInUserDetails();
        $postDataArray = array();
        $postDataArray['bank_id'] = $tellerDetails['bank_id'];
        $postDataArray['bank_branch_id'] = $tellerDetails['bank_branch_id'];
        $postDataArray['bank_name'] = $tellerDetails['bank_name'];

        $payment_done = $this->makePayment($postDataArray);

        if (is_array($payment_done)) {
            return $payment_done;
        }

        if ($payment_done == 0) {
            parent::addJobForMail();
            $billObj = billServiceFactory::getService();
            $billObj->updateBillStatus($this->pfmTransactionDetails['merchant_request_id']);
            $arrayVal = $this->createReturnArray('etranzact_configuration', 'payOnline', 'pfmTransactionDetails', $this->txnId, 'notice', 'Payment Successful');
            return $arrayVal;
        } else {

            $this->createLog($this->txnId . "-" . $merchant_request_id, 'Rollback');
            sfContext::getInstance()->getLogger()->info("Payment through vbv for " . $this->txnId . "-" . $merchant_request_id);
            $arrayVal = $this->createReturnArray('bill', 'show', 'trans_num', $this->txnId, 'error', 'Payment UnSuccessful. Please try again');
            return $arrayVal;
        }
    }

    public function validateTxnNumber() {
        return $this->pfmTransactionDetails = parent::validateTransactionNumber($this->txnId);
    }

    public function getLoggedInUserDetails() {
        $teller_details = array();
        $bankObj = Doctrine::getTable('Bank')->findByBankName('etranzact');
        $bankBranchObj = Doctrine::getTable('BankBranch')->findByName('etranzact');
        $teller_details['bank_id'] = $bankObj->getFirst()->getId();
        $teller_details['bank_branch_id'] = $bankBranchObj->getFirst()->getId();
        $teller_details['bank_name'] = 'etranzact';
        return $teller_details;
    }

  }

class BankPayDecorater {

    public $txnId;
    public $paymode;
    public function __construct($paymode,$txnId) {
      $this->txnId = $txnId;
      $this->paymode = $paymode;
    }

    public function payApplication() {
      //$gUser = sfContext::getInstance()->getUser()->getGuardUser();
      //$bankAcronym = $gUser->getBankUser()->getFirst()->getBank()->getAcronym();
      //$bankId = $gUser->getBankUser()->getFirst()->getBank()->getId();
      $con = Doctrine_Manager::connection();
      $pay4meLog = new pay4meLog();
      try {

          $con->beginTransaction();
          switch($this->paymode){
              case 'bank':
                $payObj = new BankPay($this->txnId);
                break;
              case 'Cheque':
                $payObj = new ChequePay($this->txnId);
                break;
              case 'draft':
                $payObj = new BankDraftPay($this->txnId);
                break;
          }


          $returnObj =  $payObj->payApplication();

          if ($returnObj['comments_val'] == 'Payment Successful') {
              // Add to databes
               $bankInteObj = new bankIntegration($this->txnId);
//               echo "<pre>";
//print_r($bankInteObj->biVersionObj->getMessageProperties());
//exit;

                if ($bankInteObj) {
                      $con->commit();
                      try {
                      	$bankName = $bankInteObj->biVersionObj->getRequestObject()->getTransactionBankPosting()->getBank()->getBankName();
						$enabledBanksList = sfConfig::get('app_middleware_enabled_banks');
						if (!empty($enabledBanksList) && in_array($bankName, $enabledBanksList)) {
							$bankInteObj->QueueProcessing($bankInteObj->biVersionObj);
						} else {
                      		$taskId = EpjobsContext::getInstance()->addJob('SendBankNotification', 'middleware/BankNotificationRequestList', array('mwRequestId' => $bankInteObj->biVersionObj->getRequestObject()->getId()));
						}
                    } catch (Exception $e) {
                            //$messagerequestId = $bankInteObj->saveMessageRequest($this->txnId);
                            //vikash [WP055] Bug:36028 (08-11-2012)
                            if (!is_string($bankInteObj->biVersionObj)) {
                                $messagerequestId = $bankInteObj->biVersionObj->getRequestObject()->getId();
                            } else {
                                $messagerequestId = (Integer) $version_obj;
                            }
                      //    print "<pre>";print_r($returnObj);
                            if (!empty($enabledBanksList) && in_array($bankName, $enabledBanksList)) {
                             	$taskId=EpjobsContext::getInstance()->addJob('MessagePosterToQueue', 'bankintegration/MessagePosterToQueue',array('messageRequestId'=>$messagerequestId));
                             	sfContext::getInstance()->getLogger()->debug("sceduled update queue mail job with id: $taskId", 'debug');
                             	$taskemail=EpjobsContext::getInstance()->addJob('sendPosterMailFailure',"email/MessagePosterFailure",array('failureReason'=>$e->getMessage()));
                             	sfContext::getInstance()->getLogger()->debug("scheduled  mail job with id: $taskemail", 'debug');
                            }
                      }
                      return $returnObj;
                } else {
                      throw New Exception("Cannot commit");
                  }
            } else {
              //TBD
              return $returnObj;
          }
        } catch (Exception $e) {

          $con->rollback();
            $pay4meLog->createLogData("Rollback for transaction id - " . $this->txnId . " " . $e->getMessage(), 'Rollback');
            sfContext::getInstance()->getLogger()->err("Rollback for transaction id - " . $this->txnId . " " . $e->getMessage());
            $arrayVal = $this->createReturnArray('admin', 'bankUser', 'txnId', $this->txnId, 'error', 'Due to some internal problem payment  could  not  be completed');
            return $arrayVal;
        }
    }

    protected function createReturnArray($module, $action, $param_name, $param_val, $comment, $comment_val) {
        $returnArray = array();
        $arrayVal['comments'] = $comment;
        $arrayVal['comments_val'] = $comment_val;
        $arrayVal['var_name'] = $param_name;
        $arrayVal['var_value'] = $param_val;
        $arrayVal['module'] = $module;
        $arrayVal['action'] = $action;
        return $arrayVal;
    }

}

?>
