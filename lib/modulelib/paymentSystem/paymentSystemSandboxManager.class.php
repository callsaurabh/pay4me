<?php
class paymentSystemSandboxManager implements paymentSystemService {

    static $Bank = array(4 => array('bank_name'=>'Oceanic Bank','bank_id'=>4,'branch_id' => 1),
        7 => array('bank_name'=>'PHB Bank','bank_id'=>7,'branch_id' => 6),
        8 => array('bank_name'=>'Intercontinental Bank','bank_id'=>8,'branch_id' => 8),
        10 => array('bank_name'=>'Sterling Bank','bank_id'=>10,'branch_id' => 9),
        11 => array('bank_name'=>'Skye Bank','bank_id'=>11,'branch_id' => 10),
        12 => array('bank_name'=>'UBA Bank','bank_id'=>12,'branch_id' => 3959),
        13 => array('bank_name'=>'Afri Bank','bank_id'=>13,'branch_id' => 3174),
        14 => array('bank_name'=>'NIPOST','bank_id'=>14,'branch_id' => 4016),
        15 => array('bank_name'=>'First City Group Merchant Bank','bank_id'=>15,'branch_id' => 4294),
        16 => array('bank_name'=>'ewallet','bank_id'=>16,'branch_id' => 4415));

    public function bankPay($txnId,$bankId) {
        $pay4meLog = new pay4meLog();
        $pay4meLog->createExclusivePaymentFile($txnId,1);
        $pfmTransactionDetails = $this->validateTransactionNumber($txnId, $bankId);
        if(isset($pfmTransactionDetails['comments']))
            return $pfmTransactionDetails;
        $merchant_request_id = $pfmTransactionDetails['MerchantRequest']['id'];
        $retunVal = $this->chkPaymentStatus($pfmTransactionDetails);
        if(is_array($retunVal))
            return $retunVal;
        $paymentModeConfigManager = new paymentModeConfigManager();


        if(!$paymentModeConfigManager->isPaymentServiceActive($pfmTransactionDetails['merchant_id'], $pfmTransactionDetails['merchant_service_id'], $pfmTransactionDetails['payment_mode_id'], $pfmTransactionDetails['payment_mode_option_id'])) {

            $arrayVal['comments'] = 'error';
            $arrayVal['comments_val'] = 'You are restricted to pay for this Merchant Service';
            $arrayVal['var_name'] = 'pfmTransactionDetails';
            $arrayVal['var_value'] = $pfmTransactionDetails;
            return $arrayVal;
        }
        $this->postDataArray = array();

        $this->postDataArray['bank_id'] = $bankId;
        $this->postDataArray['bank_branch_id'] = self::$Bank[$bankId]['branch_id'];
        $this->postDataArray['bank_name'] = self::$Bank[$bankId]['bank_name'];
        $this->postDataArray['id'] = $pfmTransactionDetails['merchant_request_id'];
        $this->postDataArray['paid_amount'] = $pfmTransactionDetails['total_amount'];
        $con = Doctrine_Manager::connection();


        try {
            $con->beginTransaction();
            //  $this->postDataArray['validation_number'] = $payment_transaction_number;
            $this->postDataArray['paid_date'] = date("Y-m-d H:i:s");
            $this->postDataArray['payment_status_code'] = '0';
            $flag = $this->doPayment($this->postDataArray, $txnId);
            $payment_transaction_number = $this->updatePaymentAccounts($txnId);


            $con->commit();
            $payment_done = 0;

        }
        catch (Exception $e) {
            $con->rollback();
            if($e->getMessage() == "Payment already done") {
                $transactionObj = Doctrine::getTable('Transaction')->getPayDetailfrmMerchantReqId($merchant_request_id);
                $payment_status_code = $transactionObj['payment_status_code'];
                if($payment_status_code == 0){
                    $pfmTransactionDetails = $this->validateTransactionNumber($txnId);
                    $payment_done = 0;
                }

            }
            else {
                $payment_done = 1;
            }

        }
        $pay4meLog->createExclusivePaymentFile($txnId,0);
        if($payment_done == 0) {
            $arrayVal['comments'] = 'notice';
            $arrayVal['comments_val'] = 'Payment Successful';
            $arrayVal['var_name'] = 'pfmTransactionDetails';
            $arrayVal['var_value'] = $pfmTransactionDetails;
            return $arrayVal;
        }
        else {
            $arrayVal['comments'] = 'error';
            $arrayVal['comments_val'] = 'Payment UnSuccessful. Please try again.';
            $arrayVal['var_name'] = 'pfmTransactionDetails';
            $arrayVal['var_value'] = $pfmTransactionDetails;
            return $arrayVal;
        }

    }

    public function ewalletPay($txnId) {
        $this->checkBalance($txnId);
        $pfmTransactionDetails = $this->validateTransactionNumber($txnId);
        //    print "<pre>";
        //    print_r($pfmTransactionDetails);exit;
        $merchant_request_id = $pfmTransactionDetails['MerchantRequest']['id'];
        $this->chkPaymentStatus($pfmTransactionDetails);

        $paymentModeConfigManager = new paymentModeConfigManager();
        if(!$paymentModeConfigManager->isPaymentServiceActive($pfmTransactionDetails['merchant_id'], $pfmTransactionDetails['merchant_service_id'], $pfmTransactionDetails['payment_mode_id'], $pfmTransactionDetails['payment_mode_option_id'])) {

            sfContext::getInstance()->getUser()->setFlash('error', 'You are restricted to pay for this Merchant Service', true) ;
            sfContext::getInstance()->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
            sfContext::getInstance()->getController()->forward('paymentSystem', 'payOnline');
            die;
        }

        $this->postDataArray = array();

        $bankObj = Doctrine::getTable('Bank')->findByBankName('ewallet');
        $bankBranchObj = Doctrine::getTable('BankBranch')->findByName('ewallet');

        $this->postDataArray['bank_id'] = $bankObj->getFirst()->getId();
        $this->postDataArray['bank_branch_id'] = $bankBranchObj->getFirst()->getId();
        $this->postDataArray['bank_name'] = 'eWallet';
        $this->postDataArray['id'] = $pfmTransactionDetails['merchant_request_id'];
        $this->postDataArray['paid_amount'] = $pfmTransactionDetails['total_amount'];

        $con = Doctrine_Manager::connection();

        try {
            $con->beginTransaction();
            $this->postDataArray['paid_date'] = date("Y-m-d H:i:s");
            $this->postDataArray['payment_status_code'] = '0';
            $flag =  $this->doPayment($this->postDataArray, $txnId);
            $payment_transaction_number = $this->updatePaymentAccounts($txnId);
            $billObj = billServiceFactory::getService();
            $billObj->updateBillStatus($pfmTransactionDetails['merchant_request_id']);
            $con->commit();
            $payment_done = 0;
        }
        catch (Exception $e) {
            $con->rollback();
            $payment_done = 1;
            //throw $e;
        }
        if($payment_done == 0) {
            sfContext::getInstance()->getUser()->setFlash('notice', 'Payment Successful', false) ;
            sfContext::getInstance()->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
            sfContext::getInstance()->getController()->forward('paymentSystem', 'payOnline');
            die;

        }
        else {
            sfContext::getInstance()->getUser()->setFlash('error', 'Payment UnSuccessful. Please try again.', false) ;
            sfContext::getInstance()->getRequest()->setParameter('trans_num', $txnId);
            sfContext::getInstance()->getController()->forward('bill', 'show');
            die;

        }

    }




    public function doPayment(array $postDataArray=NULL,$txnId) {
        $paymentObj = new payForMeManager();
        $flag = $paymentObj->doPay($postDataArray, $txnId, false);
    }



    public function validateTransactionNumber($txnId, $bankId="") {

        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId,$bankId);
        if(count($pfmTransactionDetails) == 0) {
            $arrayVal['comments'] = 'error';
            $arrayVal['comments_val'] = 'Transaction Id does not exist !!!';
            return $arrayVal;
        }
        return  $pfmTransactionDetails;
    }    

     public function chkPaymentStatus($pfmTransactionDetails) {
        $txnId = $pfmTransactionDetails['pfm_transaction_number'];
        $item_id = $pfmTransactionDetails['MerchantRequest']['merchant_item_id'];
        $merchant_request_id = $pfmTransactionDetails['MerchantRequest']['id'];
        $this->serviceTypeStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
        if($this->serviceTypeStatus != '' && $this->serviceTypeStatus != 0) {
            return;
        } else {

            $transactionArray = Doctrine::getTable('Transaction')->getPayDetailfrmMerchantItemId($item_id);
            $payment_transaction_number = $transactionArray['pfm_transaction_number'];
            if($payment_transaction_number == $txnId) {

                //display the payment receipt
                #sfContext::getInstance()->getUser()->setFlash('notice', 'Payment has already been made for the selected Transaction Id', false) ;
                #sfContext::getInstance()->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);

                $arrayVal['comments'] = 'notice';
                $arrayVal['comments_val'] = 'Payment has already been made for the selected Transaction Id.';
                $arrayVal['var_name'] = 'pfmTransactionDetails';
                $arrayVal['var_value'] = $pfmTransactionDetails;

            }
            else {

                $pfmTransactionDetails = TransactionTable::getTransactionDetails($payment_transaction_number);
                sfContext::getInstance()->getRequest()->setParameter('trans_number', $payment_transaction_number);
                $arrayVal['var_name_trans'] = 'trans_number';
                $arrayVal['var_value_trans'] = $payment_transaction_number;

            }
            $pay4meLog = new pay4meLog();
            $pay4meLog->createExclusivePaymentFile($payment_transaction_number,0);
            #sfContext::getInstance()->getRequest()->setParameter('pfmTransactionDetails', $pfmTransactionDetails);
            #sfContext::getInstance()->getController()->forward('paymentSystem', 'payOnline');
            $arrayVal['var_name'] = 'pfmTransactionDetails';
            $arrayVal['var_value'] = $pfmTransactionDetails;            
            return $arrayVal;
        }
    }

    public function updatePaymentAccounts($txnId) {
        $paymentObj = new payForMeManager();
        return $paymentObj->splitPayment($txnId);
    }
    public function checkBalance($txnId) {
    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    if($txnId!='') {
      $valid = $payForMeObj->checkSufficientBalance($txnId);
      if($valid['flag']==false) {
        $this->redirect('@eWallethome?requestId='.$valid['requestId']);
        return;
      }
    }
    }

    public function createLog($xmldata,$nameFormate) {
  /////log generation///
  //log xml
    $pay4meLog = new pay4meLog();
    $pay4meLog->createLogData($xmldata,$nameFormate);
  }
}
?>
