<?php
class splitAcctConfManager implements splitAcctConfService {
//function to get the SP Details
  public function getAllRecords() {

    try {
      return $all_records = Doctrine::getTable('SplitAccountConfiguration')->getAllRecords();
    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getSplitAccountConfigurationList() {
    try {
      $split_account_configuration = Doctrine::getTable('SplitAccountConfiguration')->createQuery('a')->orderBy('account_party')->execute();
      $split_account_configuration_list = array();
      $split_account_configuration_list[''] = "Please select split_account_configuration";
      foreach ($split_account_configuration as $i => $split_account_configuration_detail):
        $split_account_configuration_list[$split_account_configuration_detail->getId()] =  $split_account_configuration_detail->getId();
      endforeach;
      return $split_account_configuration_list;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }
}
?>