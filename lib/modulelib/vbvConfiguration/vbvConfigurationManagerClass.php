<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of vbvConfigurationManagerClass
 *
 * @author ashutoshs CR051 [ WPO033 ]
 */
class vbvConfigurationManager {

    /**
     * Get the response from VbV Plugin to verify the order id
     *
     * @param int $orderId
     * @return boolean
//     */
   public function paymentVerify($orderId)
      {
        $paymentVerify = new PaymentVerify();
        $response = $paymentVerify->paymentVerifyOnBackend($orderId);
        return $response;
      }
   public function createXml($merchantDetailArr='', $gatewayOrderDetails, $userDetails)
   {
         sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
         $webPath = public_path('',true);
         if($gatewayOrderDetails->getFirst()->getType()!='recharge'){
           if($merchantDetailArr['MerchantRequest']['CurrencyCode']['currency_num'] == sfConfig::get('app_naira_currency')) {
               $currencyval = sfConfig::get('app_naira_currency') ;
               $appUrlVal= $webPath."index.php/".sfConfig::get('app_naira_ret_url_approve')."/z/".session_id() ;
               $decUrlVal= $webPath."index.php/".sfConfig::get('app_naira_ret_url_decline')."/z/".session_id() ;
               $canUrlVal =$webPath."index.php/".sfConfig::get('app_naira_ret_url_cancel')."/z/".session_id() ;
           }else{
               $currencyval = sfConfig::get('app_dollar_currency') ;
               $appUrlVal= $webPath."index.php/".sfConfig::get('app_dollar_ret_url_approve')."/z/".session_id() ;
               $decUrlVal= $webPath."index.php/".sfConfig::get('app_dollar_ret_url_decline')."/z/".session_id() ;
               $canUrlVal =$webPath."index.php/".sfConfig::get('app_dollar_ret_url_cancel')."/z/".session_id() ;
           }
       }
       else{
           $currencyval = sfConfig::get('app_naira_currency') ;
           $appUrlVal= $webPath."index.php/".sfConfig::get('app_naira_ret_url_approve')."/z/".session_id() ;
           $decUrlVal= $webPath."index.php/".sfConfig::get('app_naira_ret_url_decline')."/z/".session_id() ;
           $canUrlVal =$webPath."index.php/".sfConfig::get('app_naira_ret_url_cancel')."/z/".session_id() ;
       }
         if($gatewayOrderDetails->getFirst()->getType()=='payment'){
            $merchantServiceDetails = Doctrine::getTable('MerchantService')->find($merchantDetailArr['MerchantRequest']['merchant_service_id']);
            $descriptionTxt='Payment for '.$merchantServiceDetails->getName().' for Transaction No. '.$merchantDetailArr['pfm_transaction_number'] ; // WP059 Bug:35838
         }else{
             $acctNumber = Doctrine::getTable('UserAccountCollection')->findOneBy("user_id",$userDetails->getFirst()->getUserId())->getEpMasterAccount()->getAccountNumber();
             $descriptionTxt='Recharge for ewallet number '.$acctNumber ;
         }
        
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('TKKPG');
        $root = $doc->appendChild($root);
        $request = $doc->createElement("Request");
        $root->appendChild($request);
        $operation = $doc->createElement("Operation");
        $operation->appendChild($doc->createTextNode(sfConfig::get('app_vbv_parameter_operation')));
        $request->appendChild($operation);
        $language = $doc->createElement("Language");
        $language->appendChild($doc->createTextNode(sfConfig::get('app_vbv_parameter_language')));
        $request->appendChild($language);
        $order = $doc->createElement('Order');
        $request->appendChild($order);
        $amt = $doc->createElement('Merchant');
        $amt->appendChild($doc->createTextNode(sfConfig::get('app_vbv_parameter_merchant')));
        $order->appendChild($amt);
        $amt = $doc->createElement('Amount');
        $amt->appendChild($doc->createTextNode($gatewayOrderDetails->getFirst()->getAmount()));
        $order->appendChild($amt);
        $currency = $doc->createElement('Currency');
        $currency->appendChild($doc->createTextNode($currencyval));
        $order->appendChild($currency);
        $description= $doc->createElement('Description');
        $description->appendChild($doc->createTextNode($descriptionTxt));
        $order->appendChild($description);
        $appUrl=$doc->createElement('ApproveURL');
        $appUrl->appendChild($doc->createTextNode($appUrlVal));
        $order->appendChild($appUrl);
        $canUrl=$doc->createElement('CancelURL');
        $canUrl->appendChild($doc->createTextNode($canUrlVal));
        $order->appendChild($canUrl);
        $decUrl=$doc->createElement('DeclineURL');
        $decUrl->appendChild($doc->createTextNode($decUrlVal));
        $order->appendChild($decUrl);
        $addParams = $doc->createElement('AddParams');
        $order->appendChild($addParams);
        $email=$doc->createElement('email');
        $email->appendChild($doc->createTextNode($userDetails->getFirst()->getEmail()));
        $addParams->appendChild($email);
        $phoneNum=$doc->createElement('phone');
        $phoneNum->appendChild($doc->createTextNode($userDetails->getFirst()->getMobileNo()));
        $addParams->appendChild($phoneNum);
       
       $xmldata = $doc->saveXML();
       $result =$this->createOrder($xmldata , $gatewayOrderDetails->getFirst()->getOrderId());
       return $result;
   }
   
   public function createOrder($xml, $gatewayOrderId)
   {
       
       $this->createLog($xml,'create_order_request_log_'.$gatewayOrderId );
       $url= sfConfig::get('app_vbv_request_url');
       $header[] = "Content-Type: text/xml;charset=UTF-8";
       $curl = curl_init();
       curl_setopt($curl, CURLOPT_URL, $url);
       curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
       curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
       curl_setopt($curl, CURLOPT_POST, 1);
       curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
       curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
       $response = curl_exec($curl);
       $this->createLog($response,'create_order_response_log_'.$gatewayOrderId);
       $vbvResponse = $this->processResponse($response);
       return $vbvResponse;
   }
   public function processResponse($response){
       $xdoc = new DomDocument;
       if($response!='' && $xdoc->LoadXML($response))
       {
           $responses = $xdoc->getElementsByTagName( "Response" );
           foreach( $responses as $response )
           {
               $status = $response->getElementsByTagName( "Status" )->item(0)->nodeValue;
               if ($status =='00'){
                   $orderId = $response->getElementsByTagName( "OrderID" )->item(0)->nodeValue;
                   $sessionId = $response->getElementsByTagName( "SessionID" )->item(0)->nodeValue;
                   $url = $response->getElementsByTagName( "URL" )->item(0)->nodeValue;
                   $vbvResponse= array('order_id'=>$orderId , 'session_id'=>$sessionId,'url'=>$url, 'request_status'=> $status);
               }else{
                   $vbvResponse= array('order_id'=>'' , 'session_id'=>'','url'=>'', 'request_status'=> $status);
               }
           }
       }
      return $vbvResponse;

   }

   public function createLog($xmldata, $type) {
    $nameFormate = $type;
    $pay4meLog = new pay4meLog();
    $pay4meLog->createLogData($xmldata,$nameFormate,'vbvlog');
  }

}
?>
