<?php
class validationRulesManager implements validationRulesService {
//function to get the SP Details

  public function getValidationRules($merchant_service_id) {
    $merchantServiceValidationRules = ValidationRulesTable::getValidationRules($merchant_service_id) ;

    if($merchantServiceValidationRules) {

      foreach($merchantServiceValidationRules as $k=>$v) {

        $MerchantData[$v->getMappedTo()] = $v->getParamDescription();
      }

      return $MerchantData;
    }
    return 0;
  }

  public function getSearchValidationRules($merchant_service_id) {

    $merchantServiceValidationRules = ValidationRulesTable::getValidationRules($merchant_service_id) ;
    if($merchantServiceValidationRules) {            
      foreach($merchantServiceValidationRules as $k=>$v) {
        $MerchantData[$v->getMappedTo()]['name'] = $v->getParamName();
        $MerchantData[$v->getMappedTo()]['is_mandatory'] = $v->getIsMandatory();
      }
      
      return $MerchantData;
    }
    return 0;
  }

/* WP032 Get validation rule*/
    public function getSearchValidationRulesDetails($merchant_service_id) {

    $merchantServiceValidationRules = ValidationRulesTable::getValidationRules($merchant_service_id) ;
    if($merchantServiceValidationRules) {

      foreach($merchantServiceValidationRules as $k=>$v) {

        $MerchantData[$v->getParamDescription()] = $v->getParamName();
      }

      return $MerchantData;
    }
    return 0;
  }


  public function getRulesById($merchant_service_id) {
    $merchantServiceValidationRules = Doctrine::getTable('ValidationRules')->getRulesById($merchant_service_id);

   $mandatory ='';
   $manadotryFiled = '';
    $str = '';
    $str .= "<input type='hidden' name='TotTxt' id='TotTxt' value='".count($merchantServiceValidationRules)."' >";
    foreach($merchantServiceValidationRules as $param) {
      if(isset($param['is_mandatory']) && $param['is_mandatory']==1){
          $mandatory = "<sup>*</sup>";
           $manadotryFiled = 'mandatory';
      } else {
          $mandatory = "";
          $manadotryFiled = "nonmandatory";
      }
      $str .= "
<dl>
  <div class=\"dsTitle4Fields\"><label >".$param['param_description'].$mandatory."</label></div>

  <div class=\"dsInfo4Fields\"><input type='text' name='".$param['mapped_to']."' class='".$param['param_type']."##%%##".$param['param_description']."##%%##".$manadotryFiled."'  maxlength='25' value=''>";

      if($param['param_name'] == "mobile")
        $str .= "(format: + [10-14] digit no,     eg: +1234567891 )";

      $str .="</div>
</dl>";
    }
    return $str;

  }




}
?>
