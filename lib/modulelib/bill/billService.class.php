<?php
interface billService{
    public function updateBillStatus($merchantRequestId);
    public function addBill($merchantRequestId);
    public function updateAccountDetails($txnId);
    public function getEwalletAccountValues($currency_id);
    public function getEwalletAccountBalance($account_id) ;

    public function getItemWiseActiveBills($intMerchantId='',$intMerchantServiceId='',$strFromDate='',$strToDate='') ;
    public function getArchiveBills($intMerchantId='',$intMerchantServiceId='',$strFromDate='',$strToDate='') ;
    public function getPaidBills() ;
    public function updateBills($merchantRequestId) ;
    public function doArchiveCheckedBills($merchant_request_array) ;
    public function getRequestIdByTransaction($pfm_transaction_number) ;
    public function getItemBills($itemId,$pfm_transaction_number);

    public function isRequestBelongsToEwalletUser($id,$type) ;
    
}
?>
