<?php
class billManager implements billService {
  //function to update bill status
  public function updateBillStatus($merchantRequestId){
    if(($merchantRequestId!="") && ($merchantRequestId > 0)){
      Doctrine::getTable('Bill')->updateStatus($merchantRequestId);
    }
  }
  public function updateAccountDetails($txnId){
    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $pfmTransactionDetails = $payForMeObj->getTransactionRecord($txnId);
    $accountingObj = new pay4meAccounting();
    $accountingArr = $accountingObj->getPaymentAccounts($pfmTransactionDetails);
    return $accountingArr;
  }

  public function putBill($merchantRequestId, $paymentMode){
    $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
    $transactionNumber =  $payForMeObj->generateTransactionNumber();
    $formdata['trans_num'] = $transactionNumber;
    $formdata['merchantRequestId'] = $merchantRequestId;
    $formdata['pay_mode'] = $paymentMode;
    $pfm_trans_num = $payForMeObj->saveTransaction($formdata);
    if($pfm_trans_num!=false){
      $this->addBill($merchantRequestId,$transactionNumber);
      $payForMeObj->updateMerchantRequest($formdata['merchantRequestId'],$formdata['pay_mode']);
      return $transactionNumber;
    }
    else {
      return false;
    }
  }

  public function addBill($merchantRequestId){
    if(($merchantRequestId!="") && ($merchantRequestId > 0)){
      $bill = Doctrine::getTable('Bill')->findByMerchantRequestId($merchantRequestId);
      $bill->setMerchantRequestId($merchantRequestId);
      $bill->setStatus( 'active');
      $bill->save();
    }
  }
  public function getEwalletAccountValues($currency_id, $user_id='') {
    //$sfObject = sfContext::getInstance();
  // $user_id = $sfObject->getUser()->getGuardUser()->getId();
   // $EpId = getUserDetail()->getFirst()->getMasterAccountId();
  	    $userAccountCollectionObj = new UserAccountManager($user_id);
        $accountObj = $userAccountCollectionObj->getAccountForCurrency($currency_id);
		if ($accountObj->count() > 0){
        	$account_id = $accountObj->getFirst()->getMasterAccountId();
        	$getDetails = Doctrine::getTable('EpMasterAccount')->find($account_id);
        	$accountNumber = $getDetails->getAccountNumber();
        	$walletObj = new EpAccountingManager();
        	$walletDetails = $walletObj->getWalletDetails($accountNumber);
        	if((!is_object($walletDetails)) && $walletDetails == 0) {
          		return false;
        	}else {
        		return  $walletDetails;
        	}
        }
  }

  public function getEwalletAccountBalance($account_id) {
    $walletObj = new EpAccountingManager();
    $acctObj = $walletObj->getAccount($account_id);
    return $walletBalance = $acctObj->getBalance();
  }


  public function updateBills($merchantRequestId){
    $requestIds = array();
    if(($merchantRequestId!="") && ($merchantRequestId > 0)){
      $itemId = Doctrine::getTable('Bill')->getPaidItemId($merchantRequestId);
      $requestArray = Doctrine::getTable('Bill')->getMerchantRequestIdsByItem($itemId);
      if(count($requestArray)>0){
        foreach($requestArray as $key => $val){
          $requestIds[] = $val['merchant_request_id'];
        }
        Doctrine::getTable('Bill')->updateBills($requestIds, $merchantRequestId);
      }
      //            echo "RequestArray : <pre>";print_r($requestArray);die;
    }
  }

  public function isRequestBelongsToEwalletUser($id,$type){
    $result = false;
    if($type=='merchantRequestId')
    $result  = $this->getMerchantRequestById($id);
    else
    if($type=='transactionId')
    $result  = $this->getTransactionById($id);
    if($result){
      return true;
    }else{
      return false;
    }

  }

  public function getMerchantRequestById($id){
    $result = Doctrine::getTable('MerchantRequest')->find($id);
    if($result)
    return true;
    else
    return false;
  }
  public function getTransactionById($id){
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $result = Doctrine::getTable('Transaction')->isTransactionValid($id, $userId);
    if($result)
    return true;
    else
    return false;
  }

  public function doArchiveCheckedBills($merchant_request_array){
    $q = Doctrine::getTable('Bill')->doArchiveCheckedBills($merchant_request_array);
    return true;
  }

  public function getItemWiseActiveBills($intMerchantId='',$intMerchantServiceId='',$strFromDate='',$strToDate='',$transaction=''){
  
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $q = Doctrine::getTable('Bill')->getItemWiseActiveBills($userId,'',$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
    return $q;
  }

  public function getArchiveBills($intMerchantId='',$intMerchantServiceId='',$strFromDate='',$strToDate='',$transaction=''){
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $q = Doctrine::getTable('Bill')->getArchiveBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$transaction);
    return $q;
  }

  public function getPaidBills($intMerchantId='',$intMerchantServiceId='',$strFromDate='',$strToDate='',$intValidationNo='',$transaction='',$orderBy=""){
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $q = Doctrine::getTable('Bill')->getPaidBills($userId,$intMerchantId,$intMerchantServiceId,$strFromDate,$strToDate,$intValidationNo,$transaction,$orderBy);
    return $q;
  }


  public function getRequestIdByTransaction($pfm_transaction_number){
    $q = Doctrine::getTable('Bill')->getRequestIdByTransaction($pfm_transaction_number);
    return $q;
  }


  public function getItemBills($itemId,$pfm_transaction_number){
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $q = Doctrine::getTable('Bill')->getItemBills($itemId,$pfm_transaction_number,$userId);
    return $q;
  }
  public function getEwalletAccountValuesByTxn($txnNumber) {
    $sfObject = sfContext::getInstance();
    $walletObj = new EpAccountingManager();
    $EpId = Doctrine::getTable('GatewayOrder')->getMasterAccountId($txnNumber);
    $getDetails = Doctrine::getTable('EpMasterAccount')->find($EpId);
    $accountNumber = $getDetails->getAccountNumber();
    $walletDetails = $walletObj->getWalletDetails($accountNumber);
    if((!is_object($walletDetails)) && $walletDetails == 0) {
      return false;
    }else {
      return  $walletDetails;
    }
  }

  public function getEwalletDetails($ewallet_account_num){
      $ewallet_details = Doctrine::getTable('EpMasterAccount')->getEwalletDetails($ewallet_account_num);
      if($ewallet_details->count()){
          return $ewallet_details;

      }else return false;
      
  }

  public function getRecordByOrderIdStatusAndDate($payMode,$status,$from_trans_date){
      $allGatewayOrders = Doctrine::getTable('GatewayOrder')->getRecordByOrderIdAndStatus($payMode,$status,$from_trans_date);
      return $allGatewayOrders;

  }

  public function getRecordFrmOrderId($orderId,$payMode,$status){
      $gatewayOrder = Doctrine::getTable('GatewayOrder')->getRecordByOrderId($orderId,$payMode,$status);
      return $gatewayOrder;

  }
  public function findByOrderId($orderId){
      $order_details = Doctrine::getTable('EpInternetBankConfirmationRequest')->findByOrderId($orderId);
      return $order_details;

  }
  public function saveOrderIdInGatewayorder($master_acct_id,$type,$nibss_pay_mode,$user_id,$currency , $amount_credit,$service_charges){
       $order_id = Doctrine::getTable('GatewayOrder')->saveOrder($master_acct_id,$type,$nibss_pay_mode,$user_id,$currency,$amount_credit,$service_charges);
      return $order_id;

  }
  /*
   * Function to find user details
   */
  public function getUserDetail($master_account_id){
      $ewallet_details = Doctrine::getTable('UserDetail')->findByMasterAccountId($master_account_id);
          return $ewallet_details;

  }

  /*
   * function that will return true if ewallet_type is openid,ewallet,both means if account is ewallet account also
   */
  public function permittedEwalletType($ewalletType){
        if(in_array($ewalletType,sfConfig::get('app_ewallet_account_type')))
            return true;
        return false;
  }

  /*
   * function that will return true if ewallet_type is openid_pay4me,ewallet_pay4me,both_pay4me means if account is pay4me account only
   */
  public function permittedPay4meType($ewalletType){
        if(in_array($ewalletType,sfConfig::get('app_pay4me_account_type')))
            return true;
        return false;
  }

/*
 * function to get details of the user
 */
  public function getDetailsUser($userId){
       $userArr =  Doctrine::getTable('UserDetail')->getDetailsUser($userId);
       return $userArr;
  }
}
?>
