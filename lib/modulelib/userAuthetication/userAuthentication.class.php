<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class userAuthentication {
  private $username;
  private $is_mobile_authentication=false;

  public function setUsername($username) {
    $this->username = $username;
  }

  public function setMobileAuthentication() {
    $this->is_mobile_authentication=true;
  }

  public function checkBankUser() {

    if(empty($this->username)) return false;
    //print_r('user: '.$uname.' : bank: '.$bname);
    # if first timeUser Skip Question validation
    $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($this->username);
    $val = "";
    if(isset($userDetails[0])) {
        $val = $userDetails[0]['UserDetail'][0]['user_status'];
    }
    if($val) {
      return $val;
    }

    return false;

  }


  public function getUserSessionStatus(){
      $EpDBSessionDetailObj = new EpDBSessionDetail();
      $status = $EpDBSessionDetailObj->isSessionActiveByUsername($this->username);
      if($status==false){
        return false;
        
      }else
      return true;

    }


    function redirectOnFailedAttempt($requestReferer="") {
      $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($this->username);
      if($numberOfContinuousAttemptObj!=false)
      {
        $countFailedAttempt = $numberOfContinuousAttemptObj->getFailedAttempt();
        // TODO - have max failure attempt in app.yml
        if($countFailedAttempt ==sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
          /*if(!$this->is_mobile_authentication) {
              $errors = "This username is blocked. Please contact the administrator. ";
              $userObj = sfContext::getInstance()->getUser();
              $userObj->setFlash('error', $errors);
              $this->redirect($requestReferer);
           }*/
          return false;
        }
        return true;
      }
      else{
       /* if(!$this->is_mobile_authentication) {
          $errors = "Insufficient User detail.";
          $userObj = sfContext::getInstance()->getUser();
          $userObj->setFlash('error', $errors);
          $this->redirect($requestReferer);
        }*/
        return false;
      }
    }


       function updateFailedAttempt() {
      $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($this->username);
      $countFailedAttempt = 0;
      if($numberOfContinuousAttemptObj!=false)
      {
        $countFailedAttempt = $numberOfContinuousAttemptObj->getFailedAttempt();
        if($countFailedAttempt==sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
          //return to logout message
          //return false if number of attempts are =max_failure_attempts, false represent the user as block user for 24 hours
          return 'user_blocked';
        }
        else {
          if($numberOfContinuousAttemptObj) {
            //update failed attempts for the day
            $numberOfContinuousAttemptObj->setFailedAttempt(($countFailedAttempt+1));
            $numberOfContinuousAttemptObj->save();
            if($numberOfContinuousAttemptObj->getFailedAttempt() ==sfConfig::get('app_number_of_failed_attempts_for_blocked_user')){
              // Audit trail code
              //Log audit Details
              $this->logUserBlockAuditDetails($numberOfContinuousAttemptObj->getUserId());
              return 'user_blocked';
            }
            return $numberOfContinuousAttemptObj->getFailedAttempt();
          }
          else {
            return 'continue';
          }
        }
      }
      else
      return 'continue';
    }


    public function logUserBlockAuditDetails($id)
    {
      //Log audit Details
      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_TRANSACTION,
        EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_BLOCKED,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_BLOCKED, array('username'=>$this->username)),
        $this->userAuditDetails(),
        $id, $this->username
      );
      sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function isEwalletUser () {
      $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
      $userDetails =$payForMeObj->getUserDetailByUsername($this->username);
      $group_name = $userDetails['group_name'];
      if($group_name == sfConfig::get('app_pfm_role_ewallet_user')) { return true ;}
      return false;
    }
    
    /*
     * make entry for bank and bank user
     */
     public function userAuditDetails(){
        //Log audit Details
        $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetails($this->username);
        if(count($userDetails[0]['BankUser'])){
            $bank_id = $userDetails[0]['BankUser'][0]['bank_id'];
            $uid = $userDetails[0]['id'];
            $bank_name = $userDetails[0]['bank_name'];
            if($bank_id!=""){
                $branch_name = "";
            }
            $branch_id = $userDetails[0]['BankUser'][0]['branch_id'];
            if($branch_id!=""){
                $branch_name = $userDetails[0]['bank_branch_name'];
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) ,new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME,$branch_name,$bank_id));
            }else{
                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) );}
        }else{
            $applicationArr = NULL;
        }
        return $applicationArr;
        
      
    }




    





}

?>
