<?php
  class paymentRecordManager implements paymentRecordService
  {
    //function to get the nibss setting details
    public function getNibssSettings()
    {
      $getNibssData = Doctrine::getTable('NibssSettings')->getNibssDetails();
      return $getNibssData;
    }

    //function to get the data for the payment mandate
    public function getPaymentRecords($mandateID)
    {
      $getAllPmtRecords = Doctrine::getTable('PaymentRecord')->getDataByMandateId($mandateID);
      return $getAllPmtRecords;
    }

    //function to add the file name
    public function updateFilename($mandateID, $filename)
    {
      $getStatus = Doctrine::getTable('PaymentBatch')->updateMandateWithFilename($mandateID, $filename);
      return $getStatus;
    }

    //function t get the nibss payee information
    public function getPayeeInfo()
    {
      $getNibssData = Doctrine::getTable('PaymentRecord')->getNibssInfo();
      return $getNibssData;
    }

    //function to get the merchant request
    public function getMerchantRequest()
    {
      $getRequestData = Doctrine::getTable('PaymentRecord')->getMerchantData();
      //echo "<pre>"; print_r($getRequestData); die();
      return $getRequestData;
    }

    //function to get the merchant service data
    public function getMerchantSvcData($mID)
    {
      $getMerSvcData = Doctrine::getTable('PaymentRecord')->getMerchantServiceData($mID);      
      return $getMerSvcData;
    }

    //function to update data in payment record table
    public function updateNibssMailSentData($mandateId)
    {
      Doctrine::getTable('PaymentRecord')->updateNibssRecords($mandateId);      
    }

    //function to update status in payment_batch table
    public function updatePaymentBatch($mandateID)
    {
      Doctrine::getTable('PaymentBatch')->updateNibssStatusBatch($mandateID);
    }
  }
?>
