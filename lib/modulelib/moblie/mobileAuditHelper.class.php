<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class mobileAuditHelper {

    public static function logBalanceTransaferTransactions($toewallet_number,$fromewallet_number,$amount){
    $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_EWALLET_EWALLET_VIA_MOBILE;
    //Getting Curency Logic
    //get Master Account Id
    $AccountDetail = Doctrine::getTable('EpMasterAccount')->findBy('account_number', $fromewallet_number);
    $FromAccountId = $AccountDetail->getFirst()->getId();
    $userObj = new UserAccountManager();
    $CurrencyId = $userObj->getCurrencyForAccount($FromAccountId);
    sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
    $amount = format_amount($amount,$CurrencyId,1);
    $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_TRANSACTION,
        EpAuditEvent::$SUBCATEGORY_TRANSACTION_EWALLET_EWALLET_VIA_MOBILE,
        EpAuditEvent::getFomattedMessage($msg, array('toaccount_number'=>$toewallet_number,'fromaccount_number'=>$fromewallet_number,'amount'=>$amount)));
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
    public static function logLoginSuccessAuditDetails($uname,$id){
        $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN_MOBILE,
        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGIN_MOBILE, array('username'=>$uname)),
        null);
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));

    }
    public static function logRequestTransactions($amount,$ewallet_number){
        //Getting Curency Logic
        //get Master Account Id
        $AccountDetail = Doctrine::getTable('EpMasterAccount')->findBy('account_number', $ewallet_number);
        $FromAccountId = $AccountDetail->getFirst()->getId();
        $userObj = new UserAccountManager();
        $CurrencyId = $userObj->getCurrencyForAccount($FromAccountId);
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
        $amount = format_amount($amount,$CurrencyId,1);
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT_REQUEST_VIA_MOBILE;
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT_REQUEST_VIA_MOBILE,
            EpAuditEvent::getFomattedMessage($msg, array('amount'=>$amount,'account_number'=>$ewallet_number)));
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
    public static function logApproveTransactions($username,$transId){
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT_APPROVAL_VIA_MOBILE;
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT_APPROVAL_VIA_MOBILE,
            EpAuditEvent::getFomattedMessage($msg, array('username'=>$username,'txnid'=>$transId)));
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
    public static function logRejectTransactions($username,$transId){
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT_DISAPPROVAL_VIA_MOBILE;
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT_DISAPPROVAL_VIA_MOBILE,
            EpAuditEvent::getFomattedMessage($msg, array('username'=>$username,'txnid'=>$transId)));
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public static function logArchiveTransactions($username,$transId){
        $msg = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT_ARCHIVED_VIA_MOBILE;
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT_ARCHIVED_VIA_MOBILE,
            EpAuditEvent::getFomattedMessage($msg, array('username'=>$username,'txnid'=>$transId)));
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
}


?>
