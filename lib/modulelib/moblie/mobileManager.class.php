<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileManagerclass
 *
 * @author sdutt
 */
class mobileManager {

    public $version = 'V1';
    public function  __construct($request){       
        $version = $request->getParameter('payprocess');
        if($version!=""){
            $version = strtoUpper($request->getParameter('payprocess'));
        } else {
            $version = 'V1';
        }
        $this->version = $version;        
    }
    //put your code here


    private function getErrorCode($subcode, $msg="") {
        $var = "MSG_ERR_".$subcode;
        return mobileErrorMsg::$MESSAGE[$var];
    }

    public function parseAuthenticateReq($request){

        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->getAccountInfoXML($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        } else{
           return $this->sendErrorXML($returnVal);
        }
    }
    
    public function getVersion($version){
        if($version!=""){
            $version = strtoUpper($request->getParameter('payprocess'));
        } else {
            $version = 'V1';
        }
        $this->version = $version;
        //return $version;
    }
    /*private function errorexcellgeneration($returnVal,$payprocess){

        if($this->version=='V1'){
            if(mobileErrorMsg::$INVALID_HOST_REQUEST==$returnVal)
                return $this->sendErrorXML(mobileErrorMsg::$INVALID_HOST_REQUEST,mobileErrorMsg::$INVALID_HOST_MSG,$request->getParameter('payprocess'));
             else
                return $this->sendErrorXML(mobileErrorMsg::$INVALID_REQUEST,mobileErrorMsg::$INVALID_REQUEST_MSG,$request->getParameter('payprocess'));
        } else {
            $code = 2;
            if(mobileErrorMsg::$MESSAGE['MSG_ERR_1']['SUBCODE']==$returnVal){
                $code =1;
            }
            return $this->errorExcell($code,$payprocess);
        }
    }

    private function errorExcell($code,$payprocess){
        return $this->sendErrorXML(
                mobileErrorMsg::$MESSAGE['MSG_ERR_'.$code]['SUBCODE'],
                mobileErrorMsg::$MESSAGE['MSG_ERR_'.$code]['CODE'],
                mobileErrorMsg::$MESSAGE['MSG_ERR_'.$code]['MESSAGE'],
                mobileErrorMsg::$MESSAGE['MSG_ERR_'.$code]['DETAILS'],
                $payprocess);

    }*/

    public function parseBalanceEnquiry($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->getBalanceXML($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
           }
        }else{
           return $this->sendErrorXML($returnVal);
        }
    }

    public function parseTransferToWalletReq($request){

        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->putTransferToWallet($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        }else{
           return $this->sendErrorXML($returnVal);
        }
    }

    public function parseNewBillReq($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->putNewBill($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        }else{
           return $this->sendErrorXML($returnVal);
        }
    }

    public function parseApprovedBillReq($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->doBillApproved($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        }else{
           return $this->sendErrorXML($returnVal);
        }
    }

    public function parseBillCountReq($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->getBillCount($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        }else{
           return $this->sendErrorXML($returnVal);
        }
    }

    public function parseBillListReq($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->getBillRequestList($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        }else{
           return $this->sendErrorXML($returnVal);
        }
    }


/*
    public function parseResponseXML($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){

            $rootName = $returnVal->firstChild->nodeName;
            switch($rootName){

                case 'authenticate':
                    if($this->getUserAuthentication($returnVal)){
                        sfContext::getInstance()->getResponse()->setStatusCode(200);
                        return $this->getAccountInfoXML($returnVal,$request);
                    }
                    else
                    return $this->sendErrorXML(mobileErrorMsg::$AUTHENTICATION_FAILURE,mobileErrorMsg::$AUTHENTICATION_FAILURE_MSG);

                    break;
                case 'check-balance':
                    $val  = $this->getUserAuthentication($returnVal);
                    if($val){
                        sfContext::getInstance()->getResponse()->setStatusCode(200);
                        return $this->getBalanceXML($returnVal,$request);

                    }else
                    return $this->sendErrorXML(mobileErrorMsg::$AUTHENTICATION_FAILURE,mobileErrorMsg::$AUTHENTICATION_FAILURE_MSG);

                    break;
            }

        }else{
            //create error xml method will be in mobileErrorMsg
            return $this->sendErrorXML(mobileErrorMsg::$INVALID_REQUEST,mobileErrorMsg::$INVALID_REQUEST_MSG);
        }
    }
 *
 */
    public function getUserAuthentication($xDoc){
      $userName  = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
      $password  = $xDoc->getElementsByTagName('password')->item(0)->nodeValue;
        return $this->getAuthentication($userName,$password);
    }

    public function getAccountInfoXML($xDoc,$request){

        $userName  = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){
            $returnXML =  $apiObj->getAccountDetailXml($userName,'account-info');
            $this->createLog($returnXML,'MobileGetAccountNo');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }      
        
        return $returnXML;
    }
    public function getBalanceXML($xDoc,$request){
        $userName = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $apiObj = $this->getMobileApi($request);
        //check object exists else do error excel       
        if(is_object($apiObj)){
            $returnXML =  $apiObj->getAccountBalanceXml($userName,'balance');
            $this->createLog($returnXML,'MobileCheckBalance');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }                
        return $returnXML;
    }

    public function putTransferToWallet($xDoc,$request){
        $userName = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $destAccNum = $xDoc->getElementsByTagName('destination-account-number')->item(0)->nodeValue;
        $amount = $xDoc->getElementsByTagName('amount')->item(0)->nodeValue;
        $desc = $xDoc->getElementsByTagName('description')->item(0)->nodeValue;
        $pin = $xDoc->getElementsByTagName('pin')->item(0)->nodeValue;
        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){
            $returnXML =  $apiObj->transferToWallet($userName,$destAccNum,$amount,$desc,'attached-bills',$pin);
            $this->createLog($returnXML,'MobileTransferToWallet');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        } 
        return $returnXML;
    }

    public function putNewBill($xDoc,$request){
        $userName = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $destAccNum = $xDoc->getElementsByTagName('destination-account-number')->item(0)->nodeValue;
        $amount = $xDoc->getElementsByTagName('amount')->item(0)->nodeValue;
        $desc = $xDoc->getElementsByTagName('description')->item(0)->nodeValue;
        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){ 
            $returnXML =  $apiObj->putNewBill($userName,$destAccNum,$amount,$desc,'attached-bills');
            $this->createLog($returnXML,'MobileNewBillResponse');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }
        return $returnXML;
    }

    public function doBillApproved($xDoc,$request){
        $transCode = $xDoc->getElementsByTagName('transaction-code')->item(0)->nodeValue;
        $userName  = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $pin = $xDoc->getElementsByTagName('pin')->item(0)->nodeValue;
        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){
            $returnXML =  $apiObj->doBillApproved($transCode,$userName,'attached-bills',$pin);
            $this->createLog($returnXML,'MobileApproveBillResponse');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }
        return $returnXML;
    }


    public function getBillCount($xDoc,$request){
        $userName  = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){
            $returnXML =  $apiObj->getBillCount($userName,'response-bill-count');
            $this->createLog($returnXML,'MobileResponseBillCount');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }
        return $returnXML;
    }

    public function getBillRequestList($xDoc,$request){
        $userName  = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $index  = $xDoc->getElementsByTagName('index')->item(0)->nodeValue;
        $size  = $xDoc->getElementsByTagName('size')->item(0)->nodeValue;
        $type = "";
        if(isset($xDoc->getElementsByTagName('type')->item(0)->nodeValue)){
            $type  = $xDoc->getElementsByTagName('type')->item(0)->nodeValue;
        }
        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){
            if($type!=""){
                $returnXML =  $apiObj->getBillRequestList($userName,$index,$size,'response-bill-list',$type);
            } else {
                $returnXML =  $apiObj->getBillRequestList($userName,$index,$size,'response-bill-list');
            }
            $this->createLog($returnXML,'MobileResponseBillList');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }
        return $returnXML;
    }
    public function parsePaymentDisAuthorizeReq($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->getPaymentDisAuthorize($returnVal,$request);
            } else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        }else{
           return $this->sendErrorXML($returnVal);
        }
    }

    public function parseStatementReq($request){
        $returnVal = $this->validateMobileRequest($request);
        if(is_object($returnVal)){
            $objAuthention = $this->getUserAuthentication($returnVal);
            if($objAuthention['status']){
                return $this->getStatement($returnVal,$request);
            }else {
                $subcode = $objAuthention['error'];
                $err_arr = $this->getErrorCode($subcode);
                return $this->sendErrorXML($err_arr);
            }
        } else{
           return $this->sendErrorXML($returnVal);
        }
    }
    
   /* public function validateMobileRequest($request)
    {
        if($this->version=='V1'){
            $returnVal = $this->checkIpAddress($request->getRemoteAddress());
            if($returnVal == 1)
            {
                $obj = $this->getMobileApi($request);
                if(is_object($obj)){
                    $xmldata = $request->getRawBody();
                    $this->createLog($xmldata,'MobileRequest');
                    $xdoc = '';
                    $xdoc = $this->getXMLDom($xmldata);
                    if($xdoc){
                        if($obj->validateXMLcontent($xdoc))
                        return $xdoc;
                        else
                        return mobileErrorMsg::$INVALID_REQUEST;
                    }else
                    return mobileErrorMsg::$INVALID_REQUEST;
                }
                else{
                    return mobileErrorMsg::$INVALID_REQUEST;
                }
            }else {

                return mobileErrorMsg::$INVALID_HOST_REQUEST;
            }

        } else {
            return $this->validateMobileRequestVersion($request);
        }
    }*/


    public function validateMobileRequest($request)
    {
        $returnVal = $this->checkIpAddress($request->getRemoteAddress());
        if($returnVal == 1)
        {
            $obj = $this->getMobileApi($request);
            if(is_object($obj)){
                $xmldata = $request->getRawBody();
                $this->createLog($xmldata,'MobileRequest');
                $xdoc = '';
                $xdoc = $this->getXMLDom($xmldata);
                if($xdoc){
                    if($obj->validateXMLcontent($xdoc))
                    return $xdoc;
                    else
                    return $this->getErrorCode ('7', 'INVALID_REQUEST');
                }else
                return $this->getErrorCode ('7', 'INVALID_REQUEST');
            }
            else{
                return $this->getErrorCode ('7', 'INVALID_REQUEST');
            }
        }else {
            //invalid host
            return $this->getErrorCode ('26', 'INVALID_HOST_REQUEST');
        }
    }

    

    protected function checkIpAddress($remoteIp) {
        //To do find

        //check if ipAddress restriction flag is enabled in config
        if(sfconfig::get('app_do_check_ipaddress')){
           $server = sfConfig::get('app_pay4me_mobile_server_ip');

           if($remoteIp == $server){
                return true;
            }
            return false;
        }

        return true;

    }

    /**
     *
     * @param <String> $type
     * @return mobile api object
     */
    public function getMobileApi($request){

        if($request->getParameter('payprocess')){
            $version = $request->getParameter('payprocess');
            $obj = mobileApiServiceFactory::getService($version);
            if(is_object($obj))
            return $obj;
            else {                
                return $this->getErrorCode(7, "INVALID_REQUEST");
                //mobileErrorMsg::$MESSAGE['MSG_ERR_2']['SUBCODE'];
            }
        } else {
            return $this->getErrorCode(7, "INVALID_REQUEST");
        }
    }

    public function getXMLDom($xmldata){

        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);
        if($isLoaded)
        return $xdoc;
        else
        return false;
    }

    public function createLog($xmldata,$nameFormate,$exceptionMessage="") {

        $pay4meLog = new pay4meLog();
        if($exceptionMessage!="") {
            $xmldata = $xmldata."\n".$exceptionMessage;
        }
        $pay4meLog->createLogData($xmldata,$nameFormate,'mobile');
    }

    /*
     * Methos used for authenticate ewallet user
     * @params <String> $userName
     * @params <String> $password
     * @return Boolean
     */    
 /* public function getAuthentication($userName,$password) {
    
    if($this->version=='V1'){
        //creating a dumy variable which returns status to match v1 and v2
        $returnarray['status'] = false;
        $authenticateObj = new userAuthentication();
        $authenticateObj->setUsername($userName);
        $authenticateObj->setMobileAuthentication();

        if(!$authenticateObj->isEwalletUser()) {
          return $returnarray['status'];
        }


        //check if user_status is active
        $val = $authenticateObj->checkBankUser();
        if((!$val) || (($val) && ($val!=1))) {//print "in shuchi";exit;
          return $returnarray['status'];
        }
        //check if user is already logged in
        if($authenticateObj->getUserSessionStatus()==false) {return $returnarray['status'];}

        //check if user is blocked
        if($authenticateObj->redirectOnFailedAttempt()==false) { return $returnarray['status']; }



        $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
        $this->form = new $class(array(),array(),false);
        $this->form->bind(array('username'=>$userName,'password'=>$password));
        if($this->form->isValid()) {
          $values   = $this->form->getValues();
          $userObj = $values['user'];
          $myUserObj = sfContext::getInstance()->getUser();
          $myUserObj->signin($values['user'], false, true);


          $returnarray['status'] = true;
          return $returnarray['status'];
        }
        else {
          $returnVal = $authenticateObj->updateFailedAttempt();
          if($returnVal =='user_blocked' || $returnVal !='continue') {
            return $returnarray['status'];
          }

          return $returnarray['status'];
        }
     } else {
        return $this->getAuthenticationVersion($userName,$password);
    }
  }*/


  /*
     * Methos used for authenticate ewallet user
     * @params <String> $userName
     * @params <String> $password
     * @return Boolean
     */
  public function getAuthentication($userName,$password) {
    $authenticateObj = new userAuthentication();
    $authenticateObj->setUsername($userName);
    $authenticateObj->setMobileAuthentication();
    //return array is $arr['status'],$arr['subcode']

    //invalid  ewalet user
    //return invalid ewalet user
    if(!$authenticateObj->isEwalletUser()) {
      return array('status'=>false,'error'=>1);
    }

    //check if user_status is active
    //return User NOT Active
    $val = $authenticateObj->checkBankUser();
    if((!$val) || (($val) && ($val!=1))) {
      return array('status'=>false,'error'=>2);
    }
    //check if user is already logged in
    //return User Already Loggidin
    if($authenticateObj->getUserSessionStatus()==false) {
        return array('status'=>false,'error'=>3);
        //return false;
    }
    //Return UserBlocked
    //check if user is blocked
    if($authenticateObj->redirectOnFailedAttempt()==false) {
        //check for no of failed attempts
        $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($userName);
        if($numberOfContinuousAttemptObj!=false){
            $countFailedAttempt = $numberOfContinuousAttemptObj->getFailedAttempt();
            // TODO - have max failure attempt in app.yml
            if($countFailedAttempt == sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
                return array('status'=>false,'error'=>4);
            }
        }
        return array('status'=>false,'error'=>5);
     }
    $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
    $this->form = new $class(array(),array(),false);
    $this->form->bind(array('username'=>$userName,'password'=>$password));
    //sucessfull sign up
    if($this->form->isValid()) {
      $values   = $this->form->getValues();
      $userObj = $values['user'];
      $myUserObj = sfContext::getInstance()->getUser();
      $myUserObj->signin($values['user'], false, true);

    //To do, go for signin and remove this code from here
     /*
      $userDetailObj = $userObj->getUserDetail()->getFirst();
      if($userDetailObj->getFailedAttempt()>0)
        $userDetailObj->setFailedAttempt(0);
      $userObj->save($con);*/
      return array('status'=>true);
      //return true;
    }
    else {
       //return failed attempts
      $returnVal = $authenticateObj->updateFailedAttempt();
      if($returnVal =='user_blocked' ) {
        return array('status'=>false,'error'=>4);
        //return false;
      }
      //return authentication failurer
      return array('status'=>false,'error'=>5);
      //return false;
    }
  }

    public function checkPreAuthenticated($xmlArr){
        if(isset($xmlArr['already-authenticate'])){
            return true;
        }
        else
        return false;
    }


    public function getPaymentDisAuthorize($xDoc,$request){
        $userName  = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $transactionCode  = $xDoc->getElementsByTagName('transaction-code')->item(0)->nodeValue;
        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){
            $returnXML =  $apiObj->doPaymentDisAuthorize($transactionCode,$userName,'attached-bills');
            $this->createLog($returnXML,'MobileResponsePaymentDisAuthorize');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }
        return $returnXML;
    }
     public function getStatement($xDoc,$request){
        $userName  = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $index  = $xDoc->getElementsByTagName('index')->item(0)->nodeValue;
        $size  = $xDoc->getElementsByTagName('size')->item(0)->nodeValue;
        $type = $xDoc->getElementsByTagName('type')->item(0)->nodeValue;

        $apiObj = $this->getMobileApi($request);
        if(is_object($apiObj)){
            $returnXML =  $apiObj->getStatement($userName,$index,$size,'response-statement-list',$type);
            $this->createLog($returnXML,'MobileStatementList');
        } else {
            $returnXML = $this->sendErrorXML($apiObj);
        }
        return $returnXML;
    }


    public function sendErrorXML($error_array){
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('error');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/mobile'.$this->version );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/mobile'.$this->version.' mobile'.$this->version.'.xsd' );
        $root = $doc->appendChild($root);        
           
            $subcode = $doc->createElement("error-subcode");
            $subcode->appendChild($doc->createTextNode($error_array['SUBCODE']));
            $code = $doc->createElement("error-code");
            $code->appendChild($doc->createTextNode($error_array['CODE']));
            $message = $doc->createElement("error-message");
            $message->appendChild($doc->createTextNode($error_array['MESSAGE']));
            $description = $doc->createElement("error-details");
            $description->appendChild($doc->createTextNode($error_array['DETAILS']));
            $root->appendChild($subcode);
            $root->appendChild($code);
            $root->appendChild($message);
            $root->appendChild($description);            
           
        $xmldata = $doc->saveXML();
        $this->createLog($xmldata,'ERRR');
        return $xmldata;
    }
}
?>
