<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileErrorMsgclass
 *
 * @author sdutt
 */
class mobileErrorMsg {
    //put your code here

     public static function messageCode($message){
        //replace message with _ and do upper case of each word        
        $messagestr = strtoupper($message);
       // $messagestr = str_replace('!','',$messagestr);
        //$messagestr = str_replace('.','',$messagestr);
        //$messagestr = str_replace('(','',$messagestr);
       // $messagestr = str_replace(')','',$messagestr);
        $messagestr = preg_replace('/[!.\(\)]/','',$messagestr);       
        $messagestr = preg_replace('/[^a-zA-Z0-9_\-]/', '_', $messagestr);
        $vars = get_class_vars('mobileErrorMsg');
        $messagestr = $messagestr."_SUBCODE";       
        if(isset($vars[$messagestr]) && $vars[$messagestr]!=""){
            return $vars[$messagestr];
        } else {
            return 11;
        }
    }
    static    $AUTHENTICATION_FAILURE =1;
    static    $AUTHENTICATION_FAILURE_MSG = 'Authentication failure';
    static    $INVALID_REQUEST = 2;
    static    $INVALID_REQUEST_MSG = 'Invalid request';
    static    $XML_GENERATION_ERROR_MSG='Error at xml generation';
    static    $XML_GENERATION_ERROR  = 3;
    static    $TRANSACTION_ERROR  = 5;
    static    $INVALID_TRANSACTION_CODE  = 4;
    static    $INVALID_TRANSACTION_CODE_MSG  = 'Invalid Transaction';
    //static    $INSUFFICIENT_BALANCE_CODE  = 6;
    //static    $INSUFFICIENT_BALANCE_CODE_MSG  = 'Insufficient balance to pay!';
    static    $INVALID_HOST_REQUEST  = 7;
    static    $INVALID_HOST_MSG = 'Invalid Host Request!';
     public static $MESSAGE =
array('MSG_ERR_26'=>array('CODE'=>7,'SUBCODE'=>26,'MESSAGE'=>'INVALID_HOST_REQUEST','DETAILS'=>'Invalid Host Request!'),
      'MSG_ERR_7'=>array('CODE'=>2,'SUBCODE'=>7,'MESSAGE'=>'INVALID_REQUEST','DETAILS'=>'Invalid request!'),
      'MSG_ERR_1'=>array('CODE'=>1,'SUBCODE'=>1,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Invalid e-Wallet User!'),
      'MSG_ERR_2'=>array('CODE'=>1,'SUBCODE'=>2,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'User is Not Active!'),
      'MSG_ERR_3'=>array('CODE'=>1,'SUBCODE'=>3,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Duplicate Login attempt!'),
      'MSG_ERR_4'=>array('CODE'=>1,'SUBCODE'=>4,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'User is blocked!'),
      'MSG_ERR_5'=>array('CODE'=>1,'SUBCODE'=>5,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Authentication failure!'),      
      'MSG_ERR_20'=>array('CODE'=>5,'SUBCODE'=>20,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please obtain eWallet Secure Certificate to use this service!'),
      'MSG_ERR_10'=>array('CODE'=>5,'SUBCODE'=>10,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please enter Pin!'),
      'MSG_ERR_11'=>array('CODE'=>5,'SUBCODE'=>11,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Transaction Failed!'),
      'MSG_ERR_12'=>array('CODE'=>5,'SUBCODE'=>12,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Pin has been blocked by the administrator!'),
      'MSG_ERR_13'=>array('CODE'=>5,'SUBCODE'=>13,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please enter correct Pin. Post 5 unsuccessful attempts the Pin will be blocked!'),
      'MSG_ERR_14'=>array('CODE'=>5,'SUBCODE'=>14,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please generate new Pin!'),
      'MSG_ERR_15'=>array('CODE'=>5,'SUBCODE'=>15,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Insufficient Account Balance!'),
      'MSG_ERR_16'=>array('CODE'=>5,'SUBCODE'=>16,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Invalid amount!'),
      'MSG_ERR_17'=>array('CODE'=>5,'SUBCODE'=>17,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Amount Limit for transfer (Min/Maximum) Not correct!'),
      'MSG_ERR_18'=>array('CODE'=>5,'SUBCODE'=>18,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Payment has already been made!'),
      'MSG_ERR_19'=>array('CODE'=>5,'SUBCODE'=>19,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Payment has already been rejected!'),      
      'MSG_ERR_27'=>array('CODE'=>5,'SUBCODE'=>27,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Bill has already been Archived!'),
      //Insufficient Account Balance
      'MSG_ERR_9'=>array('CODE'=>4,'SUBCODE'=>9,'MESSAGE'=>'INVALID_TRANSACTION_CODE','DETAILS'=>'Invalid Transaction Code!'),
      'MSG_ERR_8'=>array('CODE'=>3,'SUBCODE'=>8,'MESSAGE'=>'XML_GENERATION_ERROR','DETAILS'=>'Error at xml generation!'),
      'MSG_ERR_21'=>array('CODE'=>5,'SUBCODE'=>21,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'You can not transfer in same account!'),
      'MSG_ERR_22'=>array('CODE'=>5,'SUBCODE'=>22,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'You can not transfer from your one account to another!'),
      'MSG_ERR_23'=>array('CODE'=>5,'SUBCODE'=>23,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'The currency of the destination account should be same as of source account!'),
      'MSG_ERR_24'=>array('CODE'=>5,'SUBCODE'=>24,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'please enter valid ewallet account number!'),
      'MSG_ERR_25'=>array('CODE'=>5,'SUBCODE'=>25,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Entered account number is not associated with ewallet!'),
    );
    static  $PLEASE_OBTAIN_EWALLET_SECURE_CERTIFICATE_TO_USE_THIS_SERVICE_SUBCODE = 20;
    static  $PLEASE_ENTER_PIN_SUBCODE = 10;
    static  $TRANSACTION_FAILED_SUBCODE = 11;
    static  $PIN_HAS_BEEN_BLOCKED_BY_THE_ADMINISTRATOR_SUBCODE = 12;
    static  $PLEASE_ENTER_CORRECT_PIN_POST_5_UNSUCCESSFUL_ATTEMPTS_THE_PIN_WILL_BE_BLOCKED_SUBCODE = 13;
    static  $PLEASE_GENERATE_NEW_PIN_SUBCODE = 14;
    static  $INSUFFICIENT_ACCOUNT_BALANCE_SUBCODE = 15;
    static  $INVALID_AMOUNT_SUBCODE = 16;
    static  $AMOUNT_LIMIT_FOR_TRANSFER_MIN_MAXIMUM_NOT_CORRECT_SUBCODE = 17;
    static  $PAYMENT_HAS_ALREADY_BEEN_MADE_SUBCODE = 18;
    static  $PAYMENT_HAS_ALREADY_BEEN_REJECTED_SUBCODE = 19;
    static  $INVALID_TRANSACTION_CODE_SUBCODE = 9;
    static  $ERROR_AT_XML_GENERATION_SUBCODE = 8;
    static  $YOU_CAN_NOT_TRANSFER_IN_SAME_ACCOUNT_SUBCODE = 21;
    static  $YOU_CAN_NOT_TRANSFER_FROM_YOUR_ONE_ACCOUNT_TO_ANOTHER_SUBCODE = 22;
    static  $THE_CURRENCY_OF_THE_DESTINATION_ACCOUNT_SHOULD_BE_SAME_AS_OF_SOURCE_ACCOUNT_SUBCODE =  23;
    static  $PLEASE_ENTER_VALID_EWALLET_ACCOUNT_NUMBER_SUBCODE =24;
    static  $ENTERED_ACCOUNT_NUMBER_IS_NOT_ASSOCIATED_WITH_EWALLET_SUBCODE =25;
    static  $BILL_HAS_ALREADY_BEEN_ARCHIVED_SUBCODE = 27;
}
?>
