<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileApiV1Managerclass
 *
 * @author sdutt
 */
class mobileApiV1Manager extends baseMobileApi {
//put your code here
   //initializationing walletTransactionManager Object
   protected $walletTransactionManager;

   //constructor for creating object for walletTransactionManager 
   //assign Application Platfor as Mobile for walletTransactionManager Object
   public function __construct(){
       $this->walletTransactionManager = new walletTransactionManager();
       $this->walletTransactionManager->transactionPlatform = 'Mobile';
   }

  public function validateXMLcontent($xdoc) {

    $config_dir = sfConfig::get('sf_config_dir') ;

    $xmlschema = $config_dir . '/xmlschema/mobileV1.xsd';
    try {
      if ($xdoc->schemaValidate($xmlschema)) {
        return true;
      }else {
        return false ;
      }
    }catch(Exception $ex) {
      echo $ex->getMessage();
    }
  }



  public function setXMLheader($header) {
    foreach($header as $key=>$val) {
      sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
    }
  }

  public function generateXML($arr,$rootElement,$subArrElement='') {

    $header['Content-Type'] =  "application/xml;charset=UTF-8";
    $header['Accept'] = "application/xml;charset=UTF-8";
    $this->setXMLheader($header);
    return $this->writeXML($arr,$rootElement,$subArrElement);

  }

  public function writeXML($arr,$rootElement,$subArrElement) {
    $doc = new DomDocument;
    $doc->formatOutput = true;
    $root = $doc->createElement($rootElement);
    $root->setAttribute ("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
    $root->setAttribute ("xmlns","http://www.pay4me.com/schema/mobileV1");
    $root->setAttribute ("xsi:schemaLocation","http://www.pay4me.com/schema/mobileV1 mobileV1.xsd");

    if(empty($subArrElement)) {
      $root = $doc->appendChild($root);
      foreach($arr as $key=>$value) {
        $tag = $doc->createElement($key);
        $tag->appendChild($doc->createTextNode($value));
        $root->appendChild($tag);
      }
    }else {
      $root = $doc->appendChild($root);
      foreach($arr as $valueArr) {

        $tag = $doc->createElement($subArrElement);

        foreach($valueArr as $key=>$value) {
          $tag1 = $doc->createElement($key);
          $tag1->appendChild($doc->createTextNode($value));
          $tag->appendChild( $tag1 );
          $root->appendChild($tag);
        }
      }

    }
    $xmldata = $doc->saveXML();
    $doc->LoadXML($xmldata);
    return $doc;
  }

  public function getAccountBalanceXml($userName,$rootElement) {

    $accountNumber = $this->getAccountNumber($userName);
    $val = $this->getBalance($accountNumber);
    //create account xml
    // sfLoader::loadHelpers('ePortal');
    // $val = convertToKobo($val);

    $val = array('amount'=>$val);

    $xdoc = $this->generateXML($val,$rootElement);
    if($this->validateXMLcontent($xdoc)) {
      $XMLData = $xdoc->saveXML();
      unset($xdoc);
      return $XMLData;
    }else {
      unset($xdoc);
      return $this->generateErrorXML();
    }
  }

  public function getAccountDetailXml($userName,$rootElement) {

    $val = $this->getAccountNumber($userName);
    //create account xml
    $val = array('account-number'=>$val);
    $xdoc =  $this->generateXML($val,$rootElement);
    if($this->validateXMLcontent($xdoc)) {
      $XMLData = $xdoc->saveXML();
      //do auditing here sign in
      mobileAuditHelper::logLoginSuccessAuditDetails($userName,'');
      unset($xdoc);
      return $XMLData;
    }else {
      unset($xdoc);
      return $this->generateErrorXML();
    }
  }

  public function transferToWallet($userName,$destAccNum,$amount,$desc,$rootElement,$pin) {
  //getUserId
    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $userId = $userObj->getFirst()->getId();

    //chk for kyc
    $ewalletObj = new ewallet();
    $kycStatus  = $ewalletObj->getKycstatus($userId);

    if(1!=$kycStatus) {
      $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>'Please obtain eWallet Secure Certificate to use this service');
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }

    //chk if pin is reqd
    $pinObj = new pin();
    if($pinObj->isActive($userId)) {
      $validatePin = $pinObj->validatePin($pin,$userId);
      if($validatePin!='1') {
        $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>$validatePin);
        $xdoc =  $this->generateXML($val,'error');
        return $xdoc->saveXML();
      }
    }
    if(!is_numeric($amount)){
       $code = '16';
       $val = $this->errorData('MSG_ERR_'.$code);
       $xdoc =  $this->generateXML($val,'error');
       return $xdoc->saveXML();
    }
    $sourceAccNum = $this->getAccountNumber($userName);
    //using already created wallettransactionmanager object
    $wallMang =  $this->walletTransactionManager;
   $returnVal = $wallMang->doDirectRequestTransfer($destAccNum,$sourceAccNum,$amount,$desc);
    if(ctype_digit($returnVal)) {
      $val = array('transaction-code'=>$returnVal);
      $xdoc =  $this->generateXML($val,$rootElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        //do auditing here Transaction
        mobileAuditHelper::logBalanceTransaferTransactions($destAccNum,$sourceAccNum,$amount);
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>$returnVal);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }

  public function putNewBill($userName,$destAccNum,$amount,$desc,$rootElement) {

    $sourceAccNum = $this->getAccountNumber($userName);
    //  $destAccNum = $this->getAccountIdByAccountNumber($destAccNum);
    //using already created wallettransactionmanager object
    $wallMang =  $this->walletTransactionManager;
    $returnVal = $wallMang->doBillAdd($sourceAccNum,$destAccNum,$amount,$desc);
    if(ctype_digit($returnVal)) {
      $val = array('transaction-code'=>$returnVal);
      $xdoc =  $this->generateXML($val,$rootElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        //do auditing here Transaction
        mobileAuditHelper::logRequestTransactions($amount,$destAccNum);
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>$returnVal);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }

  public function doBillApproved($transcode,$userName,$rootElement,$pin) {

    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $userId = $userObj->getFirst()->getId();

    //chk for kyc
    $ewalletObj = new ewallet();
    $kycStatus  = $ewalletObj->getKycstatus($userId);

    if(1!=$kycStatus) {
      $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>'Please obtain eWallet Secure Certificate to use this service');
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }

    //chk if pin is reqd
    $pinObj = new pin();
    if($pinObj->isActive($userId)) {
      $validatePin = $pinObj->validatePin($pin,$userId);
      if($validatePin!='1') {
        $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>$validatePin);
        $xdoc =  $this->generateXML($val,'error');
        return $xdoc->saveXML();
      }
    }
    
    $accId = $this->getAccountId($userName);
    //using already created wallettransactionmanager object
    $wallMang =  $this->walletTransactionManager;
    $isValid = $wallMang->isTransCodeBelongsToUser($accId,$transcode,$userId);    
    if($isValid) {
      if($isValid == 1) {
        $ewalletTransId = $wallMang->getTansId($transcode);
        // $returnVal = $wallMang->approveTransfer($ewalletTransId ,$accId);
        $returnVal = $wallMang->doBillRequestAction($ewalletTransId,'transfer');
      }
      else if($isValid == 2) {
          //chk payment status
        //commented the code as the session creation for mobile has been added
/*          $user = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
          $user = sfContext::getInstance()->getUser();
          $user->setAttribute('user_id', $userId, 'sfGuardSecurityUser');

          $user->setAuthenticated(true);
          $user->clearCredentials();
          $user->addCredentials($user->getAllPermissionNames());*/
          $transcode = substr($transcode,1);
          $paymentObj =  new Payment($transcode);
          $arrayVal=$paymentObj->proceedToPay();
          if($arrayVal['comments'] == "notice") { //extract the validation number from accounting and return
            $paymentVals = Doctrine::getTable('Transaction')->getTransactionDetails($transcode);
            $returnVal = $paymentVals['MerchantRequest']['validation_number'];
           //get Merchant Request Id
           $merchantRequestId = $paymentVals['MerchantRequest']['id'];
           //update merchant request with platform as Mobile
           Doctrine::getTable('MerchantRequest')->updateMerchantRequestPlatform($merchantRequestId,'Mobile');
           // $user->signout();
            //print "<pre>";
           // print_r($paymentVals);
          }
          else {
            $returnVal = $arrayVal['comments_val'];
          }
         /* else if($arrayVal['comments'] == "error") {

          }*/

        }       
      if(is_numeric($returnVal)) {
        $val = array('transaction-code'=>$returnVal);
        $xdoc =  $this->generateXML($val,$rootElement);
        if($this->validateXMLcontent($xdoc)) {
          $XMLData = $xdoc->saveXML();
          //do auditing here Transaction
          mobileAuditHelper::logApproveTransactions($userName,$transcode);
          unset($xdoc);
          return $XMLData;
        }else {
          unset($xdoc);
          return $this->generateErrorXML();
        }
      }else {       
        $val = array('error-subcode'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>$returnVal,'error-details'=>'');        
        $xdoc =  $this->generateXML($val,'error');
        return $xdoc->saveXML();
      }
    }else {
      $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$INVALID_TRANSACTION_CODE,'error-message'=>mobileErrorMsg::$INVALID_TRANSACTION_CODE_MSG);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }


  }

  public function getBillCount($userName,$rootElement) {
    $accId = $this->getAccountId($userName);
    //using already created wallettransactionmanager object
    $wallMang =  $this->walletTransactionManager;
    $wallMangQuery = $wallMang->listRequestTransferFromAcc($accId);
    $countVal = $wallMangQuery->execute()->count();

    //count unpaid bills
    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $user_id = $userObj->getFirst()->getId();
    $billObj = Doctrine::getTable('Bill')->getItemWiseActiveBills($user_id);
    //int "<pre>";
    //print_r($billObj->execute(array(),Doctrine::HYDRATE_ARRAY));
    $unpaid_bill_count = $billObj->execute()->count();

    $countVal = $countVal+$unpaid_bill_count;

    if(is_numeric($countVal)) {
      $val = array('count'=>$countVal);
      $xdoc =  $this->generateXML($val,$rootElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>$returnVal);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }


  public function getBillRequestList($userName,$index,$size,$rootElement) {
    $accId = $this->getAccountId($userName);
    //using already created wallettransactionmanager object
    $wallMang =  $this->walletTransactionManager;
    $wallMangQuery = $wallMang->listReqFromAccWithTransCode($accId);
//    $arr = $wallMangQuery->limit($size)
//        ->offset($index)
//        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        $arr = $wallMangQuery->execute(array(),Doctrine::HYDRATE_ARRAY);

    //getUnpaidBills
    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $user_id = $userObj->getFirst()->getId();
    $billObj = Doctrine::getTable('Bill')->getItemWiseActiveBills($user_id);
    //int "<pre>";
    //print_r($billObj->execute(array(),Doctrine::HYDRATE_ARRAY));
    $arr_bill = $billObj->execute(array(),Doctrine::HYDRATE_ARRAY);


    $arrToXml  = array();
    $val =  array();


    if(is_array($arr_bill)) {
      foreach($arr_bill as $key=>$value) {//echo $value;
        if($size > 0 ) {
          $val = array('transaction-code'=>"2".$value['trans_num'],'amount'=>$value['amount']*100,'description'=>"Payment for item with transaction number ".$value['trans_num']);
          $arrToXml[] = $val;
          $size = $size-1;
        }

      }
    }



    //        $arrToXml  = array();
    //      $val =  array();
    if(is_array($arr)) {
      foreach($arr as $key => $value) {
        if($size > 0 ) {
          $val = array('transaction-code'=>$value['EwalletTransactionTrack'][0]['transaction_code'],'amount'=>$value['amount'],'description'=>$value['description']);
          $arrToXml[] = $val;
          $size = $size-1;
        }
      }
    //  print "<pre>";
    //  print_r($arrToXml);exit;
      $val = $arrToXml;
      $subArrElement = 'bill-list';
      $xdoc =  $this->generateXML($val,$rootElement,$subArrElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $val = array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$TRANSACTION_ERROR,'error-message'=>$returnVal);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }


  public function generateErrorXML() {
    $xdoc = $this->generateXML(array('error-subcode'=>'','error-details'=>'','error-code'=>mobileErrorMsg::$XML_GENERATION_ERROR,'error-message'=>mobileErrorMsg::$XML_GENERATION_ERROR_MSG),'error');
    $XMLData = $xdoc->saveXML();
    unset($xdoc);
    return $XMLData;
  }
}
?>
