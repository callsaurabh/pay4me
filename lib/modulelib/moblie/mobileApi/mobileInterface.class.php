<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileInterface
 * 
 * @author sdutt
 */
interface mobileInterface {
    /*
     * Method used for set XML header
     * @params <XML> $doc XML DOM Obj
     * @return XML header tag
     */
     public function setXMLheader($doc);

     /*
     * Method used for add xml header and xml data
     * @params <class> class Obj by which xml properties will be set
     * @return XML 
     * pass $obj to writeXML() method to make xml
     */

     public function generateXML($arr,$rootElement,$subArrElement='');
     
     /*
      * Method used for set xml data
     * @params <class> class Obj by which xml properties will be set
     * @return XML DOM
     * this method add xml headres and data
     */
     public function writeXML($arr,$rootElement,$subArrElement);

     /*
     * Method used for xml validation with xsd
     * @params <XML> $xdoc XML DOM Obj
     * @return Boolean 
     */
     public function validateXMLcontent($xdoc);    

     /*
     * Method used for get ewallet user account balance
     * @params <Integer> $accountId
     * @return Decimal
     */
     public function getBalance($accountId);

     /*
     *
     * @params <Integer> $accountId
     * @return <Integer> Account Id
     */
     public function getAccountId($id);

     /*   
     * @return NULL
     */
     public function logAuditDetails($userId,$eventType);
    
}
?>
