<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of baseMobileApiclass
 *
 * @author sdutt
 */
abstract class baseMobileApi implements mobileInterface {
    //put your code here

    public function getAccountDetails($accountId){

        return EpAccountingManager::getAccount($accountId);
    }

    public function getBalance($accountId){
        $acctObj = new EpAccountingManager();
        $acct =  $acctObj->getWalletDetails($accountId);
        if($acct)
        return $acct->getFirst()->getBalance();
        else
        return 0;
    }

    public function getAccountId($userName){
        return Doctrine::getTable('UserDetail')->getAccountId($userName);
    }


    public function getAccountNumber($userName){

        $accountId =  $this->getAccountId($userName);
        $acct = $this->getAccountDetails($accountId);
        if($acct)
        return $acct->getAccountNumber();
        else
        return null;
    }

    public function getAccountIdByAccountNumber($accNum){
        $obj = new EpAccountingManager();
        $val = $obj->getWalletDetails($accNum);        
        if(is_object($val))
        return $val->getFirst()->getId();
        else
        return $val;
    }

    public function logAuditDetails($userId,$eventType){

        $applicationArr = NULL;
        $cat = '';
        $subCat = '';
        $msg = '';
        $uname = $userId;
        switch($eventType){
            case 'UserVerification':

                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) ,new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME,$branch_name,$bank_id));
                $cat = '';
                $subCat = '';
                $msg = '';
                break;
            case 'BalanceEnq':

                $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BANKNAME,$bank_name,$bank_id) ,new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_BRANCHNAME,$branch_name,$bank_id));
                $cat = '';
                $subCat = '';
                $msg = '';
                break;
        }


        $eventHolder = new pay4meAuditEventHolder(
            $cat,
            $subCat,
            EpAuditEvent::getFomattedMessage($msg, array('username'=>$uname)),
            $applicationArr,
            $userId,
            $uname
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
}
?>
