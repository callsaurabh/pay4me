<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileApiServiceFactoryclass
 *
 * @author sdutt
 */
class mobileApiServiceFactory {
 /**
  *
  * @param <type> $type version type
  * @return mobileApiVersion object
  */
    public static function getService($type) {
        try{
            $var = ('mobileApi'.strtoupper($type).'Manager');
            return new $var;
        }catch(Exception $e){
            //invalid request
            return mobileErrorMsg::$MESSAGE['MSG_ERR_7']['SUBCODE'];
        }
    }
}
?>
