<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileApiV2Managerclass
 *
 * @author sdutt
 */
    class mobileApiV2Manager extends baseMobileApi {

   protected $walletTransactionManager;
  //constructor for creating object for walletTransactionManager
  //assign Application Platfor as Mobile for walletTransactionManager Object
   public function __construct(){
       $this->walletTransactionManager =
       new walletTransactionManager();
       $this->walletTransactionManager->transactionPlatform = 'Mobile';
   }
  public function validateXMLcontent($xdoc) {

    $config_dir = sfConfig::get('sf_config_dir') ;

    $xmlschema = $config_dir . '/xmlschema/mobileV2.xsd';
    try {
      if ($xdoc->schemaValidate($xmlschema)) {
        return true;
      }else {
        return false ;
      }
    }catch(Exception $ex) {
      $ex->getMessage();
    }
  }

  private function errorData($ERR_STR,$details=''){
     return array('error-subcode'=>mobileErrorMsg::$MESSAGE[$ERR_STR]['SUBCODE'],
                   'error-code'=>mobileErrorMsg::$MESSAGE[$ERR_STR]['CODE'],
                   'error-message'=>mobileErrorMsg::$MESSAGE[$ERR_STR]['MESSAGE'],
                   'error-details'=> (($details !="") ? $details:mobileErrorMsg::$MESSAGE[$ERR_STR]['DETAILS']));

  }


  public function setXMLheader($header) {
    foreach($header as $key=>$val) {
      sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
    }
  }

  public function generateXML($arr,$rootElement,$subArrElement='') {

    $header['Content-Type'] =  "application/xml;charset=UTF-8";
    $header['Accept'] = "application/xml;charset=UTF-8";
    $this->setXMLheader($header);
    return $this->writeXML($arr,$rootElement,$subArrElement);

  }

  public function writeXML($arr,$rootElement,$subArrElement) {
    $doc = new DomDocument;
    $doc->formatOutput = true;
    $root = $doc->createElement($rootElement);
    $root->setAttribute ("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
    $root->setAttribute ("xmlns","http://www.pay4me.com/schema/mobileV2");
    $root->setAttribute ("xsi:schemaLocation","http://www.pay4me.com/schema/mobileV2 mobileV2.xsd");

    if(empty($subArrElement)) {
      $root = $doc->appendChild($root);
      foreach($arr as $key=>$value) {
        $tag = $doc->createElement($key);
        $tag->appendChild($doc->createTextNode($value));
        $root->appendChild($tag);
      }
    }else {
      $root = $doc->appendChild($root);
      foreach($arr as $valueArr) {

        $tag = $doc->createElement($subArrElement);

        foreach($valueArr as $key=>$value) {
          $tag1 = $doc->createElement($key);
          $tag1->appendChild($doc->createTextNode($value));
          $tag->appendChild( $tag1 );
          $root->appendChild($tag);
        }
      }

    }
    $xmldata = $doc->saveXML();
    $doc->LoadXML($xmldata);
    return $doc;
  }

  public function getAccountBalanceXml($userName,$rootElement) {

    $accountNumber = $this->getAccountNumber($userName);
    $val = $this->getBalance($accountNumber);
    //create account xml
    // sfLoader::loadHelpers('ePortal');
    // $val = convertToKobo($val);

    $val = array('amount'=>$val);

    $xdoc = $this->generateXML($val,$rootElement);
    if($this->validateXMLcontent($xdoc)) {
      $XMLData = $xdoc->saveXML();
      unset($xdoc);
      return $XMLData;
    }else {
      unset($xdoc);
      return $this->generateErrorXML();
    }
  }

  public function getAccountDetailXml($userName,$rootElement) {
    $accountId =  $this->getAccountId($userName);
    $val = $this->getAccountDetails($accountId);
    $account_no =$val->getAccountNumber();
    $currency = $val->getUserAccountCollection()->getFirst()->getCurrencyCode()->getCurrency();
    //create account xml
    
    $account_name =$val->getAccountName();

    $val = array('account-number'=>$account_no,'account-type'=>$currency,'name'=>$account_name);
    $xdoc =  $this->generateXML($val,$rootElement);
    if($this->validateXMLcontent($xdoc)) {
      $XMLData = $xdoc->saveXML();
      //do auditing here sign in
      mobileAuditHelper::logLoginSuccessAuditDetails($userName,'');
      unset($xdoc);
      return $XMLData;
    }else {
      unset($xdoc);
      return $this->generateErrorXML();
    }
  }

  public function transferToWallet($userName,$destAccNum,$amount,$desc,$rootElement,$pin) {
  //getUserId
    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $userId = $userObj->getFirst()->getId();

    //chk for kyc
    $ewalletObj = new ewallet();
    $kycStatus  = $ewalletObj->getKycstatus($userId);

    if(1!=$kycStatus) {
      $val = $this->errorData('MSG_ERR_20');
//      $val = array('error-subcode'=>mobileErrorMsg::$MESSAGE['MSG_ERR_9']['SUBCODE'],
//                    'error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_9']['CODE'],
//                    'error-message'=>mobileErrorMsg::$MESSAGE['MSG_ERR_9']['MESSAGE'],
//                    'error-details'=>mobileErrorMsg::$MESSAGE['MSG_ERR_9']['DETAILS']);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }

    //chk if pin is reqd
    $pinObj = new pin();
    if($pinObj->isActive($userId)) {
      $validatePin = $pinObj->validatePin($pin,$userId);
      if($validatePin!='1') {
        $code = mobileErrorMsg::messageCode($validatePin);
        $val = $this->errorData('MSG_ERR_'.$code);
//        $val = array( 'error-subcode'=>mobileErrorMsg::$MESSAGE['MSG_ERR_'.$validatePin]['SUBCODE'],
//                      'error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_'.$validatePin]['CODE'],
//                      'error-message'=>mobileErrorMsg::$MESSAGE['MSG_ERR_'.$validatePin]['MESSAGE'],
//                      'error-details'=>mobileErrorMsg::$MESSAGE['MSG_ERR_'.$validatePin]['DETAILS']
//                      );
        $xdoc =  $this->generateXML($val,'error');
        return $xdoc->saveXML();
      }
    }

    $sourceAccNum = $this->getAccountNumber($userName);
    $wallMang =  new walletTransactionManager();
    if(!is_numeric($amount)){
       $code = '16';
       $val = $this->errorData('MSG_ERR_'.$code);
       $xdoc =  $this->generateXML($val,'error');
       return $xdoc->saveXML();
    }

    $returnVal = $wallMang->doDirectRequestTransfer($destAccNum,$sourceAccNum,$amount,$desc);
    if(ctype_digit($returnVal)) {
      $val = array('transaction-code'=>$returnVal);
      $xdoc =  $this->generateXML($val,$rootElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        //do auditing here Transaction
        mobileAuditHelper::logBalanceTransaferTransactions($destAccNum,$sourceAccNum,$amount);
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    } else {
      $code = mobileErrorMsg::messageCode($returnVal);
      $val = $this->errorData('MSG_ERR_'.$code);
//      $val = array('error-subcode'=>mobileErrorMsg::$MESSAGE['MSG_ERR_'.$code]['SUBCODE'],
//                   'error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_'.$code]['CODE'],
//                   'error-message'=>mobileErrorMsg::$MESSAGE['MSG_ERR_'.$code]['MESSAGE'],
//                   'error-details'=>$details);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }

  public function putNewBill($userName,$destAccNum,$amount,$desc,$rootElement) {

    $sourceAccNum = $this->getAccountNumber($userName);
    //  $destAccNum = $this->getAccountIdByAccountNumber($destAccNum);
    $wallMang =  new walletTransactionManager();
    $returnVal = $wallMang->doBillAdd($sourceAccNum,$destAccNum,$amount,$desc);
   if(ctype_digit($returnVal)) {
      $val = array('transaction-code'=>$returnVal);
      $xdoc =  $this->generateXML($val,$rootElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        //do auditing here Transaction
        mobileAuditHelper::logRequestTransactions($amount,$destAccNum);
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $code = mobileErrorMsg::messageCode($returnVal);
      $val = $this->errorData('MSG_ERR_'.$code);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }

  public function doBillApproved($transcode,$userName,$rootElement,$pin) {

    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $userId = $userObj->getFirst()->getId();

    //chk for kyc
    $ewalletObj = new ewallet();
    $kycStatus  = $ewalletObj->getKycstatus($userId);

    if(1!=$kycStatus) {
      $val = $this->errorData('MSG_ERR_20');
      //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_9']['SUBCODE'],'error-message'=>'Please obtain eWallet Secure Certificate to use this service');
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }

    //chk if pin is reqd
    $pinObj = new pin();
    if($pinObj->isActive($userId)) {
      $validatePin = $pinObj->validatePin($pin,$userId);
      if($validatePin!='1') {
        $code = mobileErrorMsg::messageCode($validatePin);
        $val = $this->errorData('MSG_ERR_'.$code);
        $xdoc =  $this->generateXML($val,'error');
        return $xdoc->saveXML();
      }
    }

    $accId = $this->getAccountId($userName);
    $wallMang =  new walletTransactionManager();
    $isValid = $wallMang->isTransCodeBelongsToUser($accId,$transcode,$userId);
    if($isValid) {
      if($isValid == 1) {
        $ewalletTransId = $wallMang->getTansId($transcode);
        // $returnVal = $wallMang->approveTransfer($ewalletTransId ,$accId);
        $returnVal = $wallMang->doBillRequestAction($ewalletTransId,'transfer');
      }
      else if($isValid == 2) {
          //chk payment status
        //commented the code as the session creation for mobile has been added
/*          $user = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
          $user = sfContext::getInstance()->getUser();
          $user->setAttribute('user_id', $userId, 'sfGuardSecurityUser');

          $user->setAuthenticated(true);
          $user->clearCredentials();
          $user->addCredentials($user->getAllPermissionNames());*/
          $transcode = substr($transcode,1);
          $paymentObj =  new Payment($transcode);
          $paymentObj->platform = 'mobile';
          $arrayVal=$paymentObj->proceedToPay();
          if($arrayVal['comments'] == "notice") { //extract the validation number from accounting and return
            $paymentVals = Doctrine::getTable('Transaction')->getTransactionDetails($transcode);
            $returnVal = $paymentVals['MerchantRequest']['validation_number'];
           // $user->signout();
            //print "<pre>";
           // print_r($paymentVals);
          }
          else {
            $returnVal = $arrayVal['comments_val'];
          }
         /* else if($arrayVal['comments'] == "error") {

          }*/

        }
      if(is_numeric($returnVal)) {
        $val = array('transaction-code'=>$returnVal);
        $xdoc =  $this->generateXML($val,$rootElement);
        if($this->validateXMLcontent($xdoc)) {
          $XMLData = $xdoc->saveXML();
          //do auditing here Transaction
          mobileAuditHelper::logApproveTransactions($userName,$transcode);
          unset($xdoc);
          return $XMLData;
        }else {
          unset($xdoc);
          return $this->generateErrorXML();
        }
      }else {
          $code = mobileErrorMsg::messageCode($returnVal);
          $val = $this->errorData('MSG_ERR_'.$code);

        //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_11']['SUBCODE'],'error-message'=>$returnVal);
        $xdoc =  $this->generateXML($val,'error');
        return $xdoc->saveXML();
      }
    }else {
      $val = $this->errorData('MSG_ERR_9');
      //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_16']['SUBCODE'],'error-message'=>mobileErrorMsg::$MESSAGE['MSG_ERR_16']['DETAILS']);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }


  }

  public function getBillCount($userName,$rootElement) {
    $accId = $this->getAccountId($userName);
    $wallMang =  new walletTransactionManager();
    $wallMangQuery = $wallMang->listRequestTransferFromAcc($accId);
    $countVal = $wallMangQuery->execute()->count();

    //count unpaid bills
    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $user_id = $userObj->getFirst()->getId();
    $billObj = Doctrine::getTable('Bill')->getItemWiseActiveBills($user_id);
    //int "<pre>";
    //print_r($billObj->execute(array(),Doctrine::HYDRATE_ARRAY));
    $unpaid_bill_count = $billObj->execute()->count();

    $countVal = $countVal+$unpaid_bill_count;

    if(is_numeric($countVal)) {
      $val = array('count'=>$countVal);
      $xdoc =  $this->generateXML($val,$rootElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $val = $this->errorData('MSG_ERR_11');
      //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_11']['SUBCODE'],'error-message'=>$returnVal);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }


  public function getBillRequestList($userName,$index,$size,$rootElement,$type='') {
    $accId = $this->getAccountId($userName);
    $wallMang =  new walletTransactionManager();


    //getUnpaidBills
    $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
    $user_id = $userObj->getFirst()->getId();



    $arrToXml  = array();
    $val =  array();
    if($type==''){
        $type='both';
    }
    if($type=='bill' || $type=='both'){

    $billObj = Doctrine::getTable('Bill')->getItemWiseActiveBills($user_id);
    $arr_bill = $billObj->execute(array(),Doctrine::HYDRATE_ARRAY);

    if(is_array($arr_bill)) {
      foreach($arr_bill as $key=>$value) {
        $merchantObj = Doctrine::getTable('MerchantRequest')->find($value['merchant_request_id']);
        //if($size > 0 ) {
          $val = array('transaction-code'=>"2".$value['trans_num'],'amount'=>$value['amount']*100,'description'=>"Payment for item with transaction number ".$value['trans_num'],'type'=>'bill','name'=>$merchantObj->getMerchant()->getName(),'dateCreated'=>  strtotime($value['created_at']));
          $arrToXml[] = $val;
         // $size = $size-1;
       // }
      }
    }
   }
    if($type=='e2e' || $type=='both') {
        $wallMangQuery = $wallMang->listReqFromAccWithTransCode($accId);
        $arr = $wallMangQuery->execute(array(),Doctrine::HYDRATE_ARRAY);
        
        $tai = 0;
         if(is_array($arr)){
          foreach($arr as $key => $value) {
           $epMasterDetails = Doctrine::getTable('EpMasterAccount')->findById($arr[$tai]['to_account_id']); // to find the name of the master account 

           // if($size > 0 ) {
              $val = array('transaction-code'=>$value['EwalletTransactionTrack'][0]['transaction_code'],'amount'=>$value['amount'],'description'=>$value['description'],'type'=>'e2e','name'=>$epMasterDetails->getFirst()->getAccountName(),'dateCreated'=>  strtotime($value['created_at']));
              $arrToXml[] = $val;
           //   $size = $size-1;
            //}
              $tai++;
          }
          
       }
    }

    usort($arrToXml,'orderByTran');
    $arrToNewXml=array();
    
    foreach($arrToXml as $arrXml){
        if($size > 0 ) {
            array_pop($arrXml);
            $arrToNewXml[] = $arrXml ;
            $size = $size-1;
        }
    }

    
    
    if(is_array($arrToNewXml)){
      $val = $arrToNewXml;
      $subArrElement = 'bill-list';
      $xdoc =  $this->generateXML($val,$rootElement,$subArrElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $val = $this->errorData('MSG_ERR_11');
      //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_11']['SUBCODE'],'error-message'=>$returnVal);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }
 public function getStatement($userName,$index,$size,$rootElement,$type){
    if($type=="both") { $type=""; }

    $accId = $this->getAccountId($userName);
    $masterAcctObj = new EpAccountingManager;
    $walletDetailsObj = $masterAcctObj->getTransactionDetails($accId, '', '', $type);

    $walletDetailsObj->offset($index);
    $walletDetailsObj->limit($size);

    $arr_statement = $walletDetailsObj->execute(array(),Doctrine::HYDRATE_ARRAY);


    $arrToXml  = array();
    $val =  array();

    if(is_array($arr_statement)) {
      foreach($arr_statement as $key => $value) {
          $val = array('transaction-code'=>$value['payment_transaction_number'],'amount'=>$value['amount'],'description'=>$value['description'],'type'=>ucwords($value['entry_type']),'transaction-date'=>$value['transaction_date']);
          $arrToXml[] = $val;
      }
      
      usort( $arrToXml,'orderByTranStatement');

      $val = $arrToXml;
      $subArrElement = 'statement-list';
      $xdoc =  $this->generateXML($val,$rootElement,$subArrElement);
      if($this->validateXMLcontent($xdoc)) {
        $XMLData = $xdoc->saveXML();
        unset($xdoc);
        return $XMLData;
      }else {
        unset($xdoc);
        return $this->generateErrorXML();
      }
    }else {
      $val = $this->errorData('MSG_ERR_11');
      //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_11']['SUBCODE'],'error-message'=>$returnVal);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
}

  private function doarchieval($transcode){
      $arrTransDetails = Doctrine::getTable('Transaction')->getValidationNumber($transcode);
      if(isset($arrTransDetails) && $arrTransDetails['merchant_request_id']!=""){
        $intMerchantRequestId = $arrTransDetails['merchant_request_id'];
          //do archieval
          //check bill status
          $arrBillDetails = Doctrine::getTable('bill')->getBillDetailsByMerchatRequest($intMerchantRequestId);
          if($arrBillDetails[0]['status']=='archive'){
              return 'Bill has already been Archived!';
          } else if($arrBillDetails[0]['status']=='paid'){
              return  'Payment has already been made!';
          }
          $merchant_request_array[] = $intMerchantRequestId;
          $billObj = billServiceFactory::getService();
          $status = $billObj->doArchiveCheckedBills($merchant_request_array);
          if($status){
              return 1;
          } else {
              return 'Transaction Failed!';
          }
      } else {
          return 'Transaction Failed!';
      }
  }

  public function doPaymentDisAuthorize($transcode,$userName,$rootElement){
      $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($userName);
      $userId = $userObj->getFirst()->getId();

      $accId = $this->getAccountId($userName);
      $wallMang =  new walletTransactionManager();

      $isValid = $wallMang->isTransCodeBelongsToUser($accId,$transcode,$userId);
      if($isValid) {
          if($isValid == 1) {
              $ewalletTransId = $wallMang->getTansId($transcode);
              $eWalletDetails = $wallMang->getBillRequestDetailsById($ewalletTransId);
              if($eWalletDetails[0]['status']=='transfer')
                   $returnVal =  "Payment has already been made";
              elseif($eWalletDetails[0]['status']=='rejected')
                   $returnVal =  "Payment has already been rejected";
              else
                 $returnVal = $wallMang->doBillRequestAction($ewalletTransId,'rejected');
          } else if($isValid == 2){
                 $transcode = substr($transcode,1);
//              //get data from transaction table
                $returnVal = $this->doarchieval($transcode);
          }
          $retAuditVal =0;
          if(is_numeric($returnVal)) {
            if($isValid == 2 && $returnVal==1){
                $retAuditVal = 1;
                //Bill Archived Successfully
                $returnVal = '';
            }
            $val = array('transaction-code'=>$returnVal);
            $xdoc =  $this->generateXML($val,$rootElement);
            if($this->validateXMLcontent($xdoc)) {
              //do  auditing Transaction
              if($isValid == 2 && $retAuditVal==1){
                mobileAuditHelper::logArchiveTransactions($userName,$transcode);
              } else {
                mobileAuditHelper::logRejectTransactions($userName,$transcode);
              }
              $XMLData = $xdoc->saveXML();
              unset($xdoc);
              return $XMLData;
            }else {
              unset($xdoc);
              return $this->generateErrorXML();
            }
          }else {
              $code = mobileErrorMsg::messageCode($returnVal);
              $val = $this->errorData('MSG_ERR_'.$code);
            //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_11']['SUBCODE'],'error-message'=>$returnVal);
            $xdoc =  $this->generateXML($val,'error');
            return $xdoc->saveXML();
          }
    } else {

      $val = $this->errorData('MSG_ERR_9');
      //$val = array('error-code'=>mobileErrorMsg::$MESSAGE['MSG_ERR_16']['SUBCODE'],'error-message'=>mobileErrorMsg::$MESSAGE['MSG_ERR_16']['DETAILS']);
      $xdoc =  $this->generateXML($val,'error');
      return $xdoc->saveXML();
    }
  }


  public function generateErrorXML() {
    $val = $this->errorData('MSG_ERR_8');
    $xdoc = $this->generateXML($val,'error');
    $XMLData = $xdoc->saveXML();
    unset($xdoc);
    return $XMLData;
  }
}

/* For sorting the array by transaction code*/

function orderByTran($x, $y){
         if ( $x['dateCreated'] == $y['dateCreated'] )
            return 0;
         else if ( $x['dateCreated'] > $y['dateCreated'] )
                return -1;
         else
            return 1;
}

function orderByTranStatement($x, $y){
         if ( strtotime($x['transaction-date']) == strtotime($y['transaction-date']) )
            return 0;
         else if ( strtotime($x['transaction-date']) > strtotime($y['transaction-date']) )
                return -1;
         else
            return 1;
}
?>
