<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileErrorMsgclass
 *
 * @author sdutt
 */
class mobileV2ErrorMsg {
    //put your code here                 
    public static function messageCode($message){
        //replace message with _ and do upper case of each word        
        $message= strtoupper($message);
        $message = str_replace('!','',$message);
        $z = preg_replace('/[^a-zA-Z0-9_\-]/', '_', $message);
        $message = mobileV2ErrorMsg::$message."_SUBCODE";
        if(isset($message) && $message!=""){
            return mobileV2ErrorMsg::$message."_SUBCODE";
        }
    }
    public static $MESSAGE =
array('MSG_ERR_1'=>array('CODE'=>1,'SUBCODE'=>1,'MESSAGE'=>'INVALID_HOST_REQUEST','DETAILS'=>'Invalid Host Request!'),
      'MSG_ERR_2'=>array('CODE'=>2,'SUBCODE'=>2,'MESSAGE'=>'INVALID_REQUEST','DETAILS'=>'Invalid request!'),
      'MSG_ERR_3'=>array('CODE'=>3,'SUBCODE'=>3,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Invalid e-Wallet User!'),
      'MSG_ERR_4'=>array('CODE'=>3,'SUBCODE'=>4,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'User is Not Active!'),
      'MSG_ERR_5'=>array('CODE'=>3,'SUBCODE'=>5,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Duplicate Login attempt!'),
      'MSG_ERR_6'=>array('CODE'=>3,'SUBCODE'=>6,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Maximum number of failed attempts reached!'),
      'MSG_ERR_7'=>array('CODE'=>3,'SUBCODE'=>7,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Authentication failure!'),
      'MSG_ERR_8'=>array('CODE'=>3,'SUBCODE'=>8,'MESSAGE'=>'AUTHENTICATION_FAILURE','DETAILS'=>'Invalid request!'),
      'MSG_ERR_9'=>array('CODE'=>4,'SUBCODE'=>9,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please obtain eWallet Secure Certificate to use this service!'),
      'MSG_ERR_10'=>array('CODE'=>4,'SUBCODE'=>10,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please enter Pin!'),
      'MSG_ERR_11'=>array('CODE'=>4,'SUBCODE'=>11,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Transaction Failed!'),
      'MSG_ERR_12'=>array('CODE'=>4,'SUBCODE'=>12,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Pin has been blocked by the administrator!'),
      'MSG_ERR_13'=>array('CODE'=>4,'SUBCODE'=>13,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please enter correct Pin. Post 5 unsuccessful attempts the Pin will be blocked!'),
      'MSG_ERR_14'=>array('CODE'=>4,'SUBCODE'=>14,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please generate new Pin!'),
      'MSG_ERR_14'=>array('CODE'=>4,'SUBCODE'=>14,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Please generate new Pin!'),
      'MSG_ERR_15'=>array('CODE'=>4,'SUBCODE'=>15,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Insufficient Account Balance!'),
      'MSG_ERR_16'=>array('CODE'=>4,'SUBCODE'=>16,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Invalid amount!'),
      'MSG_ERR_17'=>array('CODE'=>4,'SUBCODE'=>17,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Amount Limit (Min/Maximum) Not correct!'),
      'MSG_ERR_18'=>array('CODE'=>4,'SUBCODE'=>18,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Payment has already been made!'),
      'MSG_ERR_19'=>array('CODE'=>4,'SUBCODE'=>19,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Payment has already been rejected!'),
      //Insufficient Account Balance
      'MSG_ERR_20'=>array('CODE'=>4,'SUBCODE'=>9,'MESSAGE'=>'INVALID_TRANSACTION_CODE','DETAILS'=>'Invalid Transaction Code!'),
      'MSG_ERR_8'=>array('CODE'=>3,'SUBCODE'=>8,'MESSAGE'=>'XML_GENERATION_ERROR','DETAILS'=>'Error at xml generation!'),
      'MSG_ERR_22'=>array('CODE'=>5,'SUBCODE'=>21,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'You can not transfer in same account!'),
      'MSG_ERR_23'=>array('CODE'=>5,'SUBCODE'=>22,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'You can not transfer from your one account to another!'),
      'MSG_ERR_24'=>array('CODE'=>5,'SUBCODE'=>23,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'The currency of the destination account should be same as of source account!'),
      'MSG_ERR_25'=>array('CODE'=>5,'SUBCODE'=>24,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'please enter valid ewallet account number!'),
      'MSG_ERR_26'=>array('CODE'=>5,'SUBCODE'=>25,'MESSAGE'=>'TRANSACTION_ERROR','DETAILS'=>'Entered account number is not associated with ewallet!'),
    );
    static  $PLEASE_OBTAIN_EWALLET_SECURE_CERTIFICATE_TO_USE_THIS_SERVICE_SUBCODE = 9;
    static  $PLEASE_ENTER_PIN_SUBCODE = 10;
    static  $TRANSACTION_FAILED_SUBCODE = 11;
    static  $PIN_HAS_BEEN_BLOCKED_BY_THE_ADMINISTRATOR_SUBCODE = 12;
    static  $PLEASE_ENTER_CORRECT_PIN_POST_5_UNSUCCESSFUL_ATTEMPTS_THE_PIN_WILL_BE_BLOCKED_SUBCODE = 13;
    static  $PLEASE_GENERATE_NEW_PIN = 14;
    static  $INSUFFICIENT_ACCOUNT_BALANCE_SUBCODE = 15;
    static  $INVALID_AMOUNT_SUBCODE = 16;
    static  $AMOUNT_LIMIT_MIN_MAXIMUM_NOT_CORRECT_SUBCODE = 17;
    static  $PAYMENT_HAS_ALREADY_BEEN_MADE_SUBCODE = 18;
    static  $PAYMENT_HAS_ALREADY_BEEN_REJECTED_SUBCODE = 19;
    static  $INVALID_TRANSACTION_CODE_SUBCODE = 20;
    static  $ERROR_AT_XML_GENERATION_SUBCODE = 21;
    static  $YOU_CAN_NOT_TRANSFER_IN_SAME_ACCOUNT = 22;
    static  $YOU_CAN_NOT_TRANSFER_FROM_YOUR_ONE_ACCOUNT_TO_ANOTHER = 23;
    static  $THE_CURRENCY_OF_THE_DESTINATION_ACCOUNT_SHOULD_BE_SAME_AS_OF_SOURCE_ACCOUNT =  24;
    static  $PLEASE_ENTER_VALID_EWALLET_ACCOUNT_NUMBER =25;
    static  $ENTERED_ACCOUNT_NUMBER_IS_NOT_ASSOCIATED_WITH_EWALLET =26;
    
    
    
}
?>
