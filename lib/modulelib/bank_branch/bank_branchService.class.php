<?php
interface bank_branchService
{
  public function getAllRecords();
  public function getBankRelatedLgas($bankId);
  public function getBankBranchSearchResults($bank_id, $state_id, $lga_id, $branch_code);

  
}

?>
