<?php
class bank_branchManager implements bank_branchService {
//function to get the SP Details
  public function getAllRecords($bank_id="",$bank_branch="",$country_id="") {

    try {
      return $all_records = Doctrine::getTable('BankBranch')->getAllRecords($bank_id, $bank_branch,$country_id);
    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }


  public function BankBranch($bank_id="",$bank_branch_id="") {
    if(($bank_id!="") && ($bank_id > 0)) {
      $this->branches = Doctrine::getTable('BankBranch')->getAllBranches($bank_id); {
        $str = "<option value='' selected>Please select bank branch</option>";

        if (count($this->branches)==0) {
          return $str;
        }
        else {

          foreach($this->branches as $key=>$value) {
            if($value['id'] == $bank_branch_id) {
              $selected = "selected";
            }
            else {
              $selected = "";
            }
            $str .=  "<option value='".$value['id']."' ".$selected.">".$value['name']."</option>";
          }
          return $str;

        }
      }
    }
    else {
      $str = "<option value='' selected>Please select bank branch</option>";
      return $str;
    }
  }

  public function getBankBranchList($bank_id="", $country_id="") {
    try {
      $branch_id = "";
      $bankSql = Doctrine::getTable('BankBranch')->getAllRecords($bank_id,$branch_id,$country_id);
      $bankBranch = $bankSql->execute();
      //      print "<pre>";
      //      print_r($bankBranch);exit;
      $bank_branch_list = array();
      foreach ($bankBranch as $i => $bank_branch_detail):
        $bank_branch_list[$bank_branch_detail->getId()] =  $bank_branch_detail->getName().' ('.$bank_branch_detail->getBranchCode().')';
      endforeach;
      return $bank_branch_list;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getRecordDetails($bank_branch_id) {
    try {
      $branchDetails = Doctrine::getTable('BankBranch')->getRecordDetails($bank_branch_id);
      return $branchDetails;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }

  }
  public function getBankRelatedLgas($bankId) {
    try {
      $bankRelatedLga = Doctrine::getTable('BankBranch')->getBankRelatedLgas($bankId);
      return $bankRelatedLga;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }

  }
  
  public function getBankBranchSearchResults($bank_id, $state_id, $lga_id, $branch_code) {
    try {
      $userType = sfConfig::get('app_pfm_role_bank_branch_user');
      $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userType);
      $groupId = $sfGroupDetails->getFirst()->getId();
      $bankBranchSearchResults = Doctrine::getTable('BankBranch')->getBankBranchSearchResults($bank_id, $state_id, $lga_id, $branch_code,$groupId);
      return $bankBranchSearchResults;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }



}
?>
