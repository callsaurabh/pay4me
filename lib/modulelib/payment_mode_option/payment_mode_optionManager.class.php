<?php
class paymentModeOptionManager implements paymentModeOptionService {
//function to get the SP Details
    public function getAllRecords() {

        try {
            return $all_records = Doctrine::getTable('PaymentModeOption')->getAllRecords();
        }catch(Exception $e ){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function checkDuplicacy($payment_mode_id,$name,$id="") {

        try {
            return $count  = Doctrine::getTable('PaymentModeOption')->checkDuplicacy($payment_mode_id,$name,$id);
        }catch(Exception $e ){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getServices($payment_mode_id="",$payment_mode_option_id="")
    {
     // $bank_id = $request->getParameter('bank_id');
      if(($payment_mode_id!="") && ($payment_mode_id > 0))
      {
         // $bank_branch_id = $request->getParameter('bank_branch_id');
          $this->services = Doctrine::getTable('PaymentModeOption')->getServices($payment_mode_id);
        //  print "<pre>";
        //  print_r($this->branches);exit;
        //  if ($request->isXmlHttpRequest())
              {
                 $str = "<option value='' selected>Please select Payment Mode Option</option>";
                 //print "<pre>";
                //print_r($this->branches);
                if (count($this->services)==0)
                {
                  return $str;
                }
                else
                {

                    foreach($this->services as $key=>$value)
                    {
                        if($value['id'] == $payment_mode_option_id)
                        {
                            $selected = "selected";
                        }
                        else
                        {
                            $selected = "";
                        }
                        $str .=  "<option value='".$value['id']."' ".$selected.">".$value['name']."</option>";
                    }
                    return $str;
               //   return $this->renderPartial('job/list', array('jobs' => $this->jobs));

                }
              }
      }
      else
      {
        $str = "<option value='' selected>Please select Payment Mode Option</option>";
         return $str;
      }
  }
}
?>
