<?php
class bankPaymentSchedulerManager implements bankPaymentSchedulerService
{
  //function to get the all record from the table to be displayed.
  public function getAllRecords()
  {
    try {
      return $all_records = Doctrine::getTable('BankPaymentScheduler')->getAllRecords();
    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage()); die();
    }
  }

  //method to check if the code has already used or not
  public function checkDuplicacy($bank_id)
  {
    try
    {
        $bankCodeExists = Doctrine::getTable('BankPaymentScheduler')->checkDuplicacy($bank_id);
        foreach($bankCodeExists as $bank_code)
          return $bank_code['count'];
    }catch(Exception $e ){
        echo("Problem found". $e->getMessage());die;
    }
  }

  //method to update notification scheduler
  public function updateBankNotifySchedule($bankId='',$serializedArray='',$notifyStatus='', $type=""){
    try
    {
      return Doctrine::getTable('BankPaymentScheduler')->updateBankNotifySchedule($bankId,$serializedArray,$notifyStatus,$type);
    }catch(Exception $e){
        echo ("Problem found". $e->getMessage()); die;
    }
  }


  //method to get the bank records based on bani id
  public function getBankScheduleRecords($bankId){
      try
       {
          return Doctrine::getTable('BankPaymentScheduler')->getBankScheduleRecords($bankId);
       }catch(Exception $e){
            echo ("Problem found". $e->getMessage()); die;
       }
  }


  //method to format the date
  public function formatTime($chkTime)
  {
    //since it is an array, check and correct
    if(strlen($chkTime['hour']) == 1){
      $hour = "0".$chkTime['hour'];
    }else{
      $hour = $chkTime['hour'];
    }
    if(strlen($chkTime['minute']) == 1){
      $minute = "0".$chkTime['minute'];
    }else{
      $minute = $chkTime['minute'];
    }
    if(strlen($chkTime['second']) == 1){
      $second = "0".$chkTime['second'];
    }else{
      $second = $chkTime['second'];
    }

    $makeTime = $hour.":".$minute.":".$second;
    return $makeTime;
  }
}
?>
