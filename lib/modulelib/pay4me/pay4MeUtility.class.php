<?php
/**
 * This helper class provides functionatlity
 * related to workflow
 */

final class pay4MeUtility {

  public static function setLog($xmldata,$nameFormate,$exceptionMessage="") {
    /////log generation///
    //log xml
    $Pay4meLog = new pay4meLog();
    if($exceptionMessage!="") {
      $xmldata = $xmldata."\n".$exceptionMessage;
    }
    $Pay4meLog->createLogData($xmldata,$nameFormate);
  }

  public static function error($error_msg,$xdoc,$version,$error,$xmlData="",$merchant_code="") {
    if($error_msg != "INVALID_XML" && $error_msg != "INVALID_MERCHANT" && $error_msg != "AUTHENTICATION_FAILURE") {
      $item_number = $xdoc->getElementsByTagName('item')->item(0)->getAttribute('number');
      $merchant_service_id = $xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id');
    }else {
      $item_number = "";
      $merchant_service_id = "";
    }

    if(property_exists('pay4meError', $error_msg)){
      $error_code = pay4meError::$$error_msg;  
      $errObj = new pay4meError();
      $description =  $errObj->getFormattedDescription($error_code);
      //$errorXml = $errObj->error($error_code,$error);

      $pay4MeXML = "pay4MeXML".strtoupper($version);
      $obj = new $pay4MeXML;
      $errorXml = $obj->generateErrorNotification($error_code, $description);
    }
    else {
      $error_code = 400;
      $pay4MeXML = "pay4MeXML".strtoupper($version);
      $obj = new $pay4MeXML;
      $errorXml = $obj->generateErrorNotification($error_code, $error_msg);
    }

    if($errorXml) {
      self::setLog($errorXml, 'ErrorXml');
      if($merchant_service_id) {
        $pay4MeXML = "pay4MeXML".strtoupper($version);
        $pay4MeXMLV1Obj = new $pay4MeXML;
        $pay4MeXMLV1Obj->setResponseHeader($merchant_service_id);
      }
      return $errorXml;
    }
  }

  public static function errorhistory($error_msg,$xdoc,$version,$error,$xmlData="",$merchant_code="") {

    if(property_exists('pay4meError', $error_msg)){
      $error_code = pay4meError::$$error_msg;
      $errObj = new pay4meError();
      $errorXml = $errObj->errorhistory($error_code,$error);
    }
    else {
      $error_code = 400;
      $pay4MeXML = "pay4MeXML".strtoupper($version);
      $obj = new $pay4MeXML;
      $errorXml = $obj->generateErrorNotification($error_code, $error_msg);
    }

    if($errorXml) {
      self::setLog($errorXml, 'ErrorXml');
      if($merchant_code) {
        $pay4MeXML = "pay4MeXML".strtoupper($version);
        $pay4MeXMLV1Obj = new $pay4MeXML;
        $pay4MeXMLV1Obj->setHistoryResponseHeader($merchant_code);
      }
      return $errorXml;
    }
  }

  public static function getRandomNumber(){
    return mt_rand(1000000,999999999);
  }
}

?>
