<?php

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class pay4MeXMLV3 {

    private function createRoot($doc,$rootName){
        $root = $doc->createElement($rootName);
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v3' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder/v3 payformeorderV3xsd' );
        return $root;
    }

    public function getOrderReponseXML($respArr){
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $this->createRoot($doc, 'charge-amount-details');
        $root = $doc->appendChild($root);


        $charge_amount = $doc->createElement("charge-amount");
        $charge_amount->setAttribute ( "currency", 'naira' );
        $charge_amount->appendChild($doc->createTextNode(number_format($respArr['total_charge'], 2, '.', '')));


        $item_fee = $doc->createElement("item-fee");
        $item_fee->appendChild($doc->createTextNode(number_format($respArr['item_fee'], 2, '.', '')));

        $service_charge = $doc->createElement("service-charge");
        $service_charge->appendChild($doc->createTextNode(number_format($respArr['service_charge'], 2, '.', '')));

        $transaction_charge = $doc->createElement("transaction-charge");
        $transaction_charge->appendChild($doc->createTextNode(number_format($respArr['transaction_charge'], 2, '.', '')));


        $root->appendChild($charge_amount);
        $root->appendChild($item_fee);
        $root->appendChild($service_charge);
        $root->appendChild($transaction_charge);

        $xmldata = $doc->saveXML();

        // $logger->info($xmldata);

        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        //validate the XML
        $xmlValidatorObj = new validatePay4meXML() ;
        $isValid = 1;//$xmlValidatorObj->validateXML($xdoc, "V2") ;
        if($isValid)
        return $xmldata;
        else
        return false;


    }
    public function generatePay4MeOrderRedirect($url) {


        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('order-redirect');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v3' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder payformeorderV3.xsd' );
        $root = $doc->appendChild($root);
        $redirect_url = $doc->createElement("redirect-url");
        $redirect_url->appendChild($doc->createTextNode($url));
        $root->appendChild($redirect_url);
        $xmldata = $doc->saveXML();

        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);



        //validate the XML
        $xmlValidatorObj = new validatePay4meXML() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "V3") ;
        if($isValid) {
            return $xmldata;
        }
        else {
            return false;
        }

    }
    public function setResponseHeader($merchantServiceId){
        if(trim($merchantServiceId)){
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $merchantDetail  = $payForMeObj->getMerchantIdByService($merchantServiceId);
            $header = "";
            if(count($merchantDetail)){
                $merchantCode = $merchantDetail[0]['merchant_code'];
                $merchantKey = $merchantDetail[0]['merchant_key'];
                $merchantAuthString = $merchantCode .":" .$merchantKey;
                // $logger->info("auth string: $merchantAuthString");
                $merchantAuth = base64_encode($merchantAuthString) ;

                $header['Authorization'] = "Basic $merchantAuth";
                $header['Content-Type'] =  "application/xml;charset=UTF-8";
                $header['Accept'] = "application/xml;charset=UTF-8";
            }
            $this->setHeader($header);
        }
    }
    private function setHeader($header){
        foreach($header as $key=>$val){
            sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
        }
    }

}
?>
