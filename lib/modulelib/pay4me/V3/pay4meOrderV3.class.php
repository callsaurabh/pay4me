<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderclass
 *
 * @author anurag
 */
class pay4meOrderV3{

    public $version = 'v3' ;
    public $merchant_service_id ;
    public $item_number ;
    public $transaction_number ;
    public $name ;
    public $description ;
    public $price ;
    public $currency ;
    public $parameters ;
    public $transaction_location;
    public $error_parameter = array();
    public $currencies = array();
    private $merchant_request ;
    



     public function __construct($xdoc,$version)
    {
      $this->version = $version;
      $this->merchant_service_id = $xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id') ;

      $this->item_number = $xdoc->getElementsByTagName('item')->item(0)->getAttribute('number') ;
      $this->transaction_number = $xdoc->getElementsByTagName('transaction-number')->item(0)->nodeValue ;
      $this->name = $xdoc->getElementsByTagName('name')->item(0)->nodeValue ;
      $this->description = $xdoc->getElementsByTagName('description')->item(0)->nodeValue ;
      //getting amount in case price tag is not there
      if($xdoc->getElementsByTagName('price')->item(0)){
      $this->price = $xdoc->getElementsByTagName('price')->item(0)->nodeValue ;
      }else{
         $this->price = $xdoc->getElementsByTagName('amount')->item(0)->nodeValue ;
      }
      $this->currency = $xdoc->getElementsByTagName('currency')->item(0)->nodeValue ;
      if($xdoc->getElementsByTagName("transaction-location")->length != 0) {//chk if transaction location is being passed;NIPOST passes it
         $this->transaction_location = $xdoc->getElementsByTagName('transaction-location')->item(0)->nodeValue ;
      }
      $params  = $xdoc->getElementsByTagName('parameter');
      $j=0;
      $this->parameters = array() ;
      foreach($params as $param)
      {
        $this->parameters[$params->item($j)->getAttribute('name')] = $params->item($j)->nodeValue ;
        $j++ ;
      }

      return true ;
    }

    public function validator()
    {
        $merchant_code = 12 ;
        $validatorObj = new merchantXmlValidator($merchant_code) ;
        $validatorObj->validate($this) ;
        return true;
    }

    public function getDBObjectNew($pay4MeXMLOrderObj) {

      $logger=sfContext::getInstance()->getLogger();
      $query = Doctrine_Query::create()
      ->from('MerchantItem mi')
      ->leftJoin('mi.MerchantRequest mr')
      ->leftJoin('mi.MerchantRequestDetails mrd')
      ->andwhere('item_number = ?', $pay4MeXMLOrderObj->item_number)
      ->andwhere('merchant_service_id = ?',$pay4MeXMLOrderObj->merchant_service_id);


      $item = $query->fetchOne();

      if(!$item) {
        $logger->info('Creating new item object');
        $logger->info($pay4MeXMLOrderObj->item_number);
        $logger->info($pay4MeXMLOrderObj->price);
        $logger->info($pay4MeXMLOrderObj->merchant_service_id);
        $item = new MerchantItem();
        $item->setItemNumber($pay4MeXMLOrderObj->item_number) ;
        $item->setPaymentAmountRequested($pay4MeXMLOrderObj->price) ;
        $item->setMerchantServiceId($pay4MeXMLOrderObj->merchant_service_id) ;

      }
      else {
        $logger->info('in else');
        try {
        //chk if payment has been made for this item
          $item_id = $item->getId();
          $this->paymentStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
          if($this->paymentStatus == 0) {
            $errObj = new pay4meError();
            throw New Exception("ITEM_PAYMENT_ALREADY_DONE");
          }
        }catch (Exception $e){

          $logger->err($e->getMessage()) ;
          throw $e;
        }
      }


      $payForMeManagerObj = new payForMeManager() ;
      $merchantsArr = $payForMeManagerObj->getMerchantIdByService($pay4MeXMLOrderObj->merchant_service_id) ;
      $merchant_id = $merchantsArr[0]['id'] ;


      $request = new MerchantRequest();
      $request->setTxnRef($pay4MeXMLOrderObj->transaction_number) ;
      $request->setMerchantId($merchant_id) ;
      $request->setMerchantServiceId($pay4MeXMLOrderObj->merchant_service_id) ;
      $request->setItemFee($pay4MeXMLOrderObj->price) ;
      $request->setCurrencyId(1);
      $request->setVersion($this->version);
      $request->setPaymentStatusCode(1) ;
      if($pay4MeXMLOrderObj->transaction_location) {
        $request->setTransactionLocation($pay4MeXMLOrderObj->transaction_location);
      }
      $request->setMerchantItem($item);
      $item->MerchantRequest[]=$request;
      $rdetails = new MerchantRequestDetails();
      $merchantServiceValidationRules = ValidationRulesTable::getValidationRules($pay4MeXMLOrderObj->merchant_service_id) ;

      $rdetails->setMerchantRequest($request);
      $rdetails->setMerchantServiceId($pay4MeXMLOrderObj->merchant_service_id);
      $rdetails->setName($pay4MeXMLOrderObj->name);
      $rdetails->setMerchantItem($item);
     // $rdetails->setMerchantRequestId($request);
      $item->MerchantRequestDetails[] = $rdetails;

          try{
            $populateParams = $rdetails->populateParameters($merchantServiceValidationRules, $pay4MeXMLOrderObj->parameters) ;

            if($populateParams == 1){
               $item->save();
               $this->merchant_request = $request ;
               return  $request->getId(); //return the merchant_request_id
            }
            else{
              return $populateParams;
            }
          } catch (Exception $e){
            //echo "printing" ; print_r($rdetails->parameter) ; exit ; echo "this" ;
            $this->error_parameter = $rdetails->parameter ;
            $logger->err($e->getMessage()) ;
            throw New Exception($e->getMessage());
          } ///catch



    }


    public function getRedirectObject()
    {
      $pay4meOrderRedirectV3Obj = new pay4meOrderRedirectV3($this->merchant_request) ;
      return $pay4meOrderRedirectV3Obj;
    }
}
?>
