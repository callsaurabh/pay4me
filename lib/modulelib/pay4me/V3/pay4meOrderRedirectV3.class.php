<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderRedirectV1
 *
 * @author anurag
 */
class pay4meOrderRedirectV3 {

    public $redirect_url ;
    public $redirectKey ;


    public function __construct($merchant_request,$version='')
    {    
      $transaction_num = $merchant_request->getTxnRef() ;
      $requestId = $merchant_request->getId() ;
      $redirectKeyStr = $transaction_num.":".$requestId ;

      $this->redirectKey = base64_encode($redirectKeyStr) ;
    }

    public function toXml()
    {
      $logger=sfContext::getInstance()->getLogger();
      //sfLoader::loadHelpers('Url');
      
      
    //  sfContext::getInstance()->getConfiguration()->loadHelpers(array('Helper', 'Url', 'Asset', 'Tag'));
      sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
     
//      $orderRedirectXml
       $obj = new pay4MeXMLV3();
       $logger->info(url_for("@order_pay?order=".$this->redirectKey,true));
       $orderRedirectXml = $obj->generatePay4MeOrderRedirect($this->xml_character_encode(url_for("@order_pay?order=".$this->redirectKey,true)));
      $logger->info('test2');
//
//
//      $orderRedirectXml = "<order-redirect xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder'   xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder payformeorder.xsd'>" ;
//      $orderRedirectXml .= "<redirect-url>" ;
//      $orderRedirectXml .=  $this->xml_character_encode(url_for("@order_pay?order=".$this->redirectKey,true));
//      $orderRedirectXml .=  "</redirect-url>";
//      $orderRedirectXml .=  "</order-redirect>";


      $logger->info('sending redirect XML');
      $logger->log(print_r($orderRedirectXml, true)) ;
      return $orderRedirectXml;
    }

    protected function xml_character_encode($string, $trans='') {
      $trans = (is_array($trans)) ? $trans : get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
      foreach ($trans as $k=>$v)
        $trans[$k]= "&#".ord($k).";";

      return strtr($string, $trans);
    }


}
?>
