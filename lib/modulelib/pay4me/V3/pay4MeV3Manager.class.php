<?php

/**
 * class APIPaymentProcessor
 *
 */
class pay4MeV3Manager implements pay4MeService {
    /** Aggregations: */

    /** Compositions: */
    /*     * * Attributes: ** */
    public $error_parameter = array();
    static $version = 'V3';
    static $ApiRootArray = array('charge-amount-details-request', 'charge-order');

    public function processOrder($request) {
        //order processing logic
        $xdoc = $this->getDomObject();
        $rootName = $xdoc->firstChild->nodeName;
        $merchant_param_name = sfConfig::get('app_merchant_url_param_name');
        $merchant_code = $request->getParameter($merchant_param_name);
        $this->saveLog($xdoc, $rootName);

        try {

            $this->xsdValidate($xdoc);
            $merchant_code = $request->getParameter($merchant_param_name);
            $this->isValidMerchant($merchant_code);
            $this->authenticateMerchantRequest($request);

            $payForMeObj = processorFactory::getProcessor($rootName);

            if (in_array($rootName, pay4MeV3Manager::$ApiRootArray)) {
                $this->getUserAuthentication($xdoc);
                $this->checkIp($xdoc);
            }
            $reponse = $payForMeObj->processRequest($xdoc);
            return $reponse;
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
            $logger = sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: " . $msg);
            return pay4MeUtility::error($msg, $xdoc, pay4MeV3Manager::$version, '', $xdoc, $merchant_code);
        }
    }

    /**
     *
     *


     * @return string
     * @access public
     */
    private function getDomObject() {
        sfContext::getInstance()->getResponse()->setContentType('text/xml');
        $xmldata = file_get_contents('php://input'); //$request->getRawBody() ;
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        return $xdoc;
    }

// end of member function getDomObject

    private function getUserAuthentication($xDoc) {
        $userName = $xDoc->getElementsByTagName('username')->item(0)->nodeValue;
        $password = $xDoc->getElementsByTagName('password')->item(0)->nodeValue;
        if (!$this->getAuthentication($userName, $password))
            throw New Exception("INVALID_USER");
    }

// end of member function getUser

    private function getAuthentication($userName, $password) {

        $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
        $this->form = new $class(array(), array(), false);
        $this->form->bind(array('username' => $userName, 'password' => $password));
        if ($this->form->isValid()) {
            return true;
        }
        else
            return false;
    }

    private function isValidMerchant($mechantcode) {
        if (!MerchantTable::isValidMerchant($mechantcode))
            throw New Exception("INVALID_MERCHANT");
    }

// end of member function isValidMerchant

    private function authenticateMerchantRequest($request) {
        $payForMeObj = payForMeServiceFactory::getService('payforme');
        $isMerchantAuthenticate = $payForMeObj->authenticateMerchantRequest($request);

        if (!$isMerchantAuthenticate)
            throw New Exception("AUTHENTICATION_FAILURE");
    }

    private function saveLog($xmldata, $rootName) {
        if ('charge-amount-details-request' == $rootName || 'order' == $rootName) {
            pay4MeUtility::setLog($xmldata, 'NewOrderItem');
            return;
        }

        if ('charge-order' == $rootName) {
            pay4MeUtility::setLog($xmldata, 'ChargeOrder');
            return;
        }
        pay4MeUtility::setLog($xmldata, 'ErrorRequest');
    }

    private function xsdValidate($xmldata) {

        $xmlValidatorObj = new validatePay4meXML();
        $isValid = $xmlValidatorObj->validateXML($xmldata, pay4MeV3Manager::$version);
//        if(!$isValid) // is a Valid P4M XML //isValidMerchantServiceId
//        throw New Exception("INVALID_XML");
        if (!$isValid) {
            $xmlError = $xmlValidatorObj->getXmlErrors();

            throw New Exception($xmlError);
        }
    }

// end of member function xsdValidate

    private function checkIp($xmldata) {
//        $ipAddress =  $request->getRemoteAddress();
    }

// end of member function checkIp
}

// end of APIPaymentProcessor
?>