<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of webProcesserclass
 *
 * @author ashutoshs
 */
class webProcesser {
    static $version = 'V3';
    //put your code here
    public function processRequest($request) {
        $xdoc = $this->getDomObject();
        $this->validateOrder($xdoc);
        $redirectXML =  $this->getDBObjectNew($xdoc);     
        $xml = $redirectXML->toXml() ;

        $this->saveRedirectXml($xml,'RedirectXml','pay4melog');

        $this->setResponseHeader($xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id'));
        return  $xml;


    }
    private function getDomObject() {
        sfContext::getInstance()->getResponse()->setContentType('text/xml') ;
        $xmldata = file_get_contents('php://input'); //$request->getRawBody() ;
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        return $xdoc;
    }
    private function validateOrder($xdoc){
        $order = $xdoc->getElementsByTagName('order');
        $validateObj = new validateContent();
        $validateObj->validateOrder($order);
    }

    private function getDBObjectNew($request){
        $Pay4meXMLOrderObj = pay4meXmlServiceFactory::getRequest($request, self::$version, '') ;
        $created = $Pay4meXMLOrderObj->getDBObjectNew($Pay4meXMLOrderObj) ; // try - catch
        if(!ctype_digit($created)){ //checks if the merchant_request_id is returned
            throw New Exception($created);
        }
      return  $redirectXML = $Pay4meXMLOrderObj->getRedirectObject() ;
    }
  
    public function saveRedirectXml($xmldata,$name,$folderName){
        $pay4meLog = new pay4meLog();
        $pay4meLog->createLogData($xmldata,$name,$folderName);
    }
    public function setResponseHeader($merchantServiceId){
        $pay4MeXMLV3Obj = new pay4MeXMLV3() ;
         $pay4MeXMLV3Obj->setResponseHeader($merchantServiceId);
    }
}
?>
