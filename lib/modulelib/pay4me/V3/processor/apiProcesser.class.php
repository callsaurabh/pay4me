<?php


/**
 * class API_Processer
 *
 */
class apiProcesser
{

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/

static $version = 'V3';
  /**
   *
   *
   * @param string RequestXML

   * @return string
   * @access public
   */
  public function processRequest($RequestXML) {
      $xdoc = $this->getDomObject($RequestXML);
      
      $rootName = $xdoc->firstChild->nodeName;
      if('charge-amount-details-request' == $rootName)
        $respXML = $this->processOrder($RequestXML);

      if('charge-order' == $rootName)
        $respXML = $this->processPayment($RequestXML);

        return $respXML ;
  } // end of member function processRequest


 private function processOrder($RequestXML){
     $this->validateOrder($RequestXML);
     $xdoc = $this->getDomObject($RequestXML);
     $version = self::$version;
     $Pay4meXMLOrderObj = pay4meXmlServiceFactory::getRequest($xdoc, $version , '') ;

     $respObj = $Pay4meXMLOrderObj->getOrderResponseObject();
     $pay4MeXML = "pay4MeXML".strtoupper($version);
     $writerObj = new $pay4MeXML;
     $respXML = $writerObj->getOrderReponseXML($respObj);
     return $respXML ;
     

  }

  private function processPayment($RequestXML){
     $this->validateOrder($RequestXML);
     $xdoc = $this->getDomObject($RequestXML);
     $order = $xdoc->getElementsByTagName('order');
     $Pay4meXMLOrderObj = pay4meXmlServiceFactory::getRequest($order, self::$version, '') ;

     $respObj = $Pay4meXMLOrderObj->getPaymentResponseObject();
     $writerObj = new ApiResponseWriter();
     $respXML = $writerObj->getOrderReponseXML($respObj);
  }

  
 private function validateOrder($RequestXML){
     $xdoc = $this->getDomObject($RequestXML);
     $order = $xdoc->getElementsByTagName('order');
     $validateObj = new validateContent();
     $validateObj->validateOrder($order);
 }
 private function getDomObject() {
        sfContext::getInstance()->getResponse()->setContentType('text/xml') ;
        $xmldata = file_get_contents('php://input'); //$request->getRawBody() ;
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        return $xdoc;
    }

} // end of API_Processer
?>