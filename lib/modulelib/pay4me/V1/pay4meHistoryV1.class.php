<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderclass
 *
 * @author 
 */
class pay4meHistoryV1{

    public $version = 'v1' ;     
    public $merchant_request = array();
    public $error_parameter = array();
   
    public function __construct($xdoc,$version,$merchant_code)
    {

        $this->version = $version;
        $this->merchant_code = $merchant_code;
        $arrXmlData = $this->processxmlItem($xdoc);
        unset($xdoc);
        $this->getHistoryByItems($arrXmlData);
      
        
    }






    public function getHistoryByItems($arrXmlData){


        $parameters = array();
        $arrXmlData=$arrXmlData['order-history-request']['merchant-service'];
        $payForMeObj = payForMeServiceFactory::getService('payforme');
        $i=0;
        if(isset($arrXmlData['@id'])){
            $arrNewdata = array();
            $arrNewdata[0] = $arrXmlData;
            $arrXmlData = $arrNewdata;
            unset($arrNewdata);
        }

    
        $parameters[$i]['service_error'] = false;
        $parameters[$i]['service_id'] = $arrXmlData[0]['@id'];
        $parameters[$i]['item'] = array();
        

        //loop for servicess
        $j=0;
        if(!empty($arrXmlData[0]['items']['item-number'])){

            //loop iteration for items
            foreach($arrXmlData[0]['items']['item-number'] as $item){             
                if(!is_array($item)){
                    $item != "";
                    $temp = '';
                    $temp = $item;
                    $item = array();
                    $item['#text']= $temp;
                    $temp = '';
                    
                }              
                //check for item number if success process else set error flg
                //check for item number
                if ($payForMeObj->isValidItemNumber($arrXmlData[0]['@id'],$item['#text'])){
                    //get all details from merchant request
                    $objMerchantReq = Doctrine::getTable('MerchantRequest')->getMerchantRequestDetailsByServiceItem($arrXmlData[0]['@id'],$item['#text']);
                    if($objMerchantReq && count($objMerchantReq)>0){
                        if($objMerchantReq->getFirst()->getTxnRef()!=""){
                            $parameters[$i]['item'][$j]['transaction-number'] = $objMerchantReq->getFirst()->getTxnRef();
                            $parameters[$i]['item'][$j]['validation-number'] = $objMerchantReq->getFirst()->getValidationNumber();
                            $parameters[$i]['item'][$j]['amount'] = $objMerchantReq->getFirst()->getPaidAmount();
                            $parameters[$i]['item'][$j]['currency-code'] = ($objMerchantReq->getFirst()->getCCode() !="") ? strtolower($objMerchantReq->getFirst()->getCCode()): "";
                            $parameters[$i]['item'][$j]['payment-date'] = $this->convertDbDateToXsdDate($objMerchantReq->getFirst()->getPaidDate());
                            $parameters[$i]['item'][$j]['transaction-location'] = $objMerchantReq->getFirst()->getTransactionLocation();
                            $parameters[$i]['item'][$j]['payment-mode'] = $objMerchantReq->getFirst()->getPaymentMode();
                            $parameters[$i]['item'][$j]['status-code'] = $objMerchantReq->getFirst()->getPaymentStatusCode();
                            $parameters[$i]['item'][$j]['status-description'] = "Payment Successfully Done";
                        } else {
                            $parameters[$i]['item'][$j]['status-description'] = "No Order Details Exists";
                        }
                        $parameters[$i]['item'][$j]['item_number'] = $item['#text'];
                        $parameters[$i]['item'][$j]['item_error'] = false;
                    } else {
                        $parameters[$i]['item'][$j]['status-description'] = "No Order Details Exists";
                        $parameters[$i]['item'][$j]['item_error'] = false;
                        $parameters[$i]['item'][$j]['item_number'] = $item['#text'];
                    }
                } else {
                    $parameters[$i]['item'][$j]['item_error'] = true;
                    $parameters[$i]['item'][$j]['item_number'] = $item['#text'];
                }
                $j++;
            }



        }elseif(isset($arrXmlData[0]['start-time']['#text']) && isset($arrXmlData[0]['end-time']['#text'])){

            $startTime =$this->convertXsdDateToDbDate($arrXmlData[0]['start-time']['#text']);
            $endTime =$this->convertXsdDateToDbDate($arrXmlData[0]['end-time']['#text']);
            if (isset($arrXmlData[0]['app-id']['#text']) && isset($arrXmlData[0]['ref-id']['#text'])) {
              $appId = $this->convertXsdDateToDbDate($arrXmlData[0]['app-id']['#text']);
              $refId = $this->convertXsdDateToDbDate($arrXmlData[0]['ref-id']['#text']);
              $objMerchantReq = Doctrine::getTable('MerchantRequest')->getMerchantRequestDetailsByServiceItem($arrXmlData[0]['@id'],'',NULL, NULL,$appId,$refId);
            } else {
            	$objMerchantReq = Doctrine::getTable('MerchantRequest')->getMerchantRequestDetailsByServiceItem($arrXmlData[0]['@id'],'',$startTime,$endTime);
            }
            if($objMerchantReq && count($objMerchantReq)>0){

                foreach($objMerchantReq as $value){

                    if(isset($value['txn_ref'])){
                        $parameters[$i]['item'][$j]['item_number'] =  $value['item_number'];
                        $parameters[$i]['item'][$j]['transaction-number'] = $value['txn_ref'];
                        $parameters[$i]['item'][$j]['validation-number'] = $value['validation_number'];
                        $parameters[$i]['item'][$j]['amount'] = $value['paid_amount'];
                        $parameters[$i]['item'][$j]['currency-code'] = ($value['c_code'] !="") ? strtolower($value['c_code']): "";
                        $parameters[$i]['item'][$j]['payment-date'] = $this->convertDbDateToXsdDate($value['paid_date']);
                        $parameters[$i]['item'][$j]['transaction-location'] = $value['transaction_location'];
                        $parameters[$i]['item'][$j]['payment-mode'] = $value['payment_mode'];
                        $parameters[$i]['item'][$j]['status-code'] =  $value['payment_status_code'];
                        $parameters[$i]['item'][$j]['status-description'] = "Payment Successfully Done";
                    } else {
                        $parameters[$i]['item'][$j]['status-description'] = "No Order Details Exists";
                    }
                   
                    //$parameters[$i]['item'][$j]['status-description'] = "No Order Details Exists";
                    $parameters[$i]['item'][$j]['item_error'] = false;
                    $j++;
                }
            }

        }


       
        $this->merchant_request = $parameters;

       
    }


   public function processxmlItem($node) {
      
		$occurance = array();
        $result='';
        if(count($node->childNodes)>0)
            foreach($node->childNodes as $child) {
                @$occurance[$child->nodeName]++;
            }
            if($node->nodeType == XML_TEXT_NODE) {
                $result = html_entity_decode(htmlentities($node->nodeValue, ENT_COMPAT, 'UTF-8'),
                                         ENT_COMPAT,'ISO-8859-15');
            }
            else {
                if($node->hasChildNodes()){
                    $children = $node->childNodes;
                    for($i=0; $i<$children->length; $i++) {
                        $child = $children->item($i);
                        if($child->nodeName != '#text') {
                            if($occurance[$child->nodeName] > 1) {
                                $result[$child->nodeName][] = $this->processxmlItem($child);
                            }
                            else {
                                $result[$child->nodeName] = $this->processxmlItem($child);
                            }
                        }
                        else if ($child->nodeName == '#text') {
                            $text = $this->processxmlItem($child);

                            if (trim($text) != '') {
                                $result[$child->nodeName] = $this->processxmlItem($child);
                            }
                        }
                    }
                }
			if($node->hasAttributes()) {
				$attributes = $node->attributes;
				if(!is_null($attributes)) {
					foreach ($attributes as $key => $attr) {
						$result["@".$attr->name] = $attr->value;
					}
				}
			}
		}
		return $result;
	}

//    public function validator()
//    {
//        $this->merchant_code = 12 ;
//        $validatorObj = new merchantXmlValidator($this->merchant_code) ;
//        $validatorObj->validate($this) ;
//        return true;
//    }
//
//
//    public function validateParams($merchant_service_id,$parameters) {
//      $param_names = array();
//      $merchantServiceValidationRules = ValidationRulesTable::getValidationRules($merchant_service_id) ;
//      foreach($merchantServiceValidationRules as $validationRule) {
//        $param_names[$validationRule->getParamName()] = $validationRule->getIsMandatory();
//          if($validationRule->getIsMandatory()) {
//            if(isset($parameters[$validationRule->getParamName()]) && $parameters[$validationRule->getParamName()]) {
//
//         }
//         else {
//          $this->error_parameter = array('parameter'=>$validationRule->getParamName()) ;
//          throw New Exception("MANDATORY_PARAM_MISSING");
//         }
//        }
//         if( ($validationRule->getParamType() == 'string' ) ) {
//        if(!(is_string($parameters[$validationRule->getParamName()]))) {
//          $this->error_parameter = array('parameter'=>$validationRule->getParamName()) ;
//          throw New Exception("INVALID_PARAM_INTEGER");
//        }
//      }elseif( $validationRule->getParamType() == 'integer' )
//        if(! is_numeric($parameters[$validationRule->getParamName()])) {
//          $this->error_parameter = array('parameter'=>$validationRule->getParamName()) ;
//          throw New Exception("INVALID_PARAM_STRING");
//        }
//      }
//      foreach($parameters as $key=>$value) {
//        if(!array_key_exists($key,$param_names)){
//          $this->error_parameter = array('parameter'=>$key) ;
//          throw New Exception("UNSUPPORTED_PARAM");
//        }
//      }
//    }



    protected function convertDbDateToXsdDate($dbDate) {
        if(!empty($dbDate)){
        $dtokens=explode(' ', $dbDate);
        $xdate = implode('T',$dtokens);
        return $xdate;
        }else{
        return false;
        }
    }

    protected function convertXsdDateToDbDate($dbDate) {
        if(!empty($dbDate)){
           $date =   str_replace('T', ' ', $dbDate);
           return $date;
        }else{
            return false;
        }
    }





    public function getResponseObject()
    {       
        $pay4meHistoryResponseObj = new pay4meHistoryResponseV1($this->merchant_request,$this->version) ;
        return $pay4meHistoryResponseObj;
    }
}
?>
