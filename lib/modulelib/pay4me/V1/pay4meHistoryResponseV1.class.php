<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderRedirectV2
 *
 * @author anurag
 */
class pay4meHistoryResponseV1 {

    public $response;


    public function __construct($merchant_request,$merchant_code='',$version='')
    {
        //$transaction_num = $merchant_request->getTxnRef() ;
        //$requestId = $merchant_request->getId() ;
        //$redirectKeyStr = $transaction_num.":".$requestId.":".$version;

        $this->response = $merchant_request;
    }

    public function toXml()
    {
        $logger=sfContext::getInstance()->getLogger();

        //sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        //      $orderRedirectXml
        $obj = new pay4MeXMLV1();
        //$logger->info($this->response);
      
        $orderRedirectXml = $obj->generatePay4MeHistoryResponse($this->response);
        

        $logger->info('sending redirect XML');
        $logger->log(print_r($orderRedirectXml, true)) ;
        return $orderRedirectXml;
    }

    protected function xml_character_encode($string, $trans='') {
        $trans = (is_array($trans)) ? $trans : get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
        foreach ($trans as $k=>$v)
        $trans[$k]= "&#".ord($k).";";

        return strtr($string, $trans);
    }
    

}
?>
