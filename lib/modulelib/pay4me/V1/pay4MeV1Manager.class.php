<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4MeV1Managerclass
 *
 * @author sdutt
 */
class pay4MeV1Manager implements pay4MeService {
    //put your code here
    public $error_parameter = array();
    public function processOrder($request){
        //order processing logic
        sfContext::getInstance()->getResponse()->setContentType('text/xml') ;
        $xmldata = file_get_contents('php://input'); //$request->getRawBody() ;
        $xmlString = explode('<transaction-number>',$xmldata);
        $xmlTransaction = explode('</transaction-number>',$xmlString['1']);
        $transactionNo = $xmlTransaction['0'];
        //log creation
        pay4MeUtility::setLog($xmldata,'NewOrderItem');
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        $pay4MeXMLV1Obj = new pay4MeXMLV1() ;
        $merchant_param_name = sfConfig::get('app_merchant_url_param_name') ;
        $merchant_code = $request->getParameter($merchant_param_name) ;
        $Pay4meXMLOrderObj = pay4meXmlServiceFactory::getRequest($xdoc, 'v1', $merchant_code) ;
        try {

            sfContext::getInstance()->getResponse()->setContentType('text/xml') ;
            $merchantServicePrice= $xdoc->getElementsByTagName('merchant-service');
            $merchantRequestV1XmlPrice = $merchantServicePrice->item(0)->getElementsByTagName('price')->item(0)->nodeValue;

            if(preg_match("/^[0-9.]+$/", $merchantRequestV1XmlPrice)){
                if($merchantRequestV1XmlPrice > '1000000000') {
                    throw New Exception("INVALID_PRICE_LIMIT");
                }
            }

            $xmlValidatorObj = new validatePay4meXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, 'v1') ;
           if(!$isValid)
            {
               $xmlError=$xmlValidatorObj->getXmlErrors();
                   
                  throw New Exception($xmlError);

            }
            

            if(!MerchantTable::isValidMerchant($merchant_code))
            throw New Exception("INVALID_MERCHANT");

            $payForMeObj = payForMeServiceFactory::getService('payforme') ;
            $isMerchantAuthenticate = $payForMeObj->authenticateMerchantRequest($request) ;

            if(!$isMerchantAuthenticate)
            throw New Exception("AUTHENTICATION_FAILURE");

            if (!$payForMeObj->isValidMerchantServiceId($merchant_code ,$xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id')))
            throw New Exception("INVALID_SERVICE");

            $activeMerServiceObj =  paymentScheduleConfigServiceFactory::getService(paymentScheduleConfigServiceFactory::$merchantConfig);
            if (!$activeMerServiceObj->isMerchantServiceActive($merchant_code ,$xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id')))
            throw New Exception("INACTIVE_SERVICE");


            $created = $Pay4meXMLOrderObj->getDBObjectNew($Pay4meXMLOrderObj) ; // try - catch
            if(!ctype_digit($created)){ //checks if the merchant_request_id is returned
                throw New Exception($created);
            }
		$merchantRequestId = Doctrine::getTable('MerchantRequest')->getMerchantRequestIdByTxnRef($transactionNo);
		$obj = new MerchantRequestLog();
	        $obj->setMessageText($xmldata);
        	$obj->setTransactionNumber($transactionNo);
	        $obj->setMerchantRequestId($merchantRequestId);
        	$obj->save();

            ##$this->logMessage("logging")  ;
            $redirectXML = $Pay4meXMLOrderObj->getRedirectObject() ;

            $xml = $redirectXML->toXml() ;
            //log creation
            //pay4MeUtility::setLog($xml,'RedirectXml');
            $pay4meLog = new pay4meLog();
            $pay4meLog->createLogData($xml,'RedirectXml','pay4melog');
            $pay4MeXMLV1Obj->setResponseHeader($xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id'));
            return $xml;
        } catch (Exception $ex) {
            $this->error_parameter = $Pay4meXMLOrderObj->error_parameter;
            $msg = $ex->getMessage();
            $logger=sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: ".$msg);
            return pay4MeUtility::error($msg,$xdoc,'V1',$this->error_parameter,$xmldata,$merchant_code);
        }
    }

    public function processHistory($request){

        $merchantParamName = sfConfig::get('app_merchant_url_param_name') ;
       
        $mechantCode = $request->getParameter($merchantParamName) ;
               
        //order processing logic
        sfContext::getInstance()->getResponse()->setContentType('text/xml') ;
        try {                
            $xmldata = file_get_contents('php://input'); //$request->getRawBody() ;
         
            //log creation
            pay4MeUtility::setLog($xmldata,'OrderHistory');
            $xdoc = new DomDocument;
            $xdoc->LoadXML($xmldata);           
            sfContext::getInstance()->getResponse()->setContentType('text/xml');            
             // To do validate here
             //
            $xmlValidatorObj = new validatePay4meXML();
            $isValid = $xmlValidatorObj->validateHistoryXML($xdoc, 'v1') ;
            
            if($isValid!=1) // is a Valid P4M XML //isValidMerchantServiceId 1 will be value in case of true
            throw New Exception($isValid);
                                          
            if(!MerchantTable::isValidMerchant($mechantCode))
            throw New Exception("INVALID_MERCHANT");



            $payForMeObj = payForMeServiceFactory::getService('payforme') ;
            $isMerchantAuthenticate = $payForMeObj->authenticateMerchantRequest($request) ;
            if(!$isMerchantAuthenticate)
            throw New Exception("AUTHENTICATION_FAILURE");
            
            
            if(!isset($xdoc->getElementsByTagName('merchant-service')->item(0)->nodeValue)){
                throw New Exception("INVALID_MERCHANT_SERVICE"); 
            }elseif(!$payForMeObj->isValidMerchantServiceId($mechantCode,$xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id'))) {
                throw New Exception("INVALID_MERCHANT_SERVICE"); 
            }

            if(isset($xdoc->getElementsByTagName('start-time')->item(0)->nodeValue)){
                $startDate = strtotime($xdoc->getElementsByTagName('merchant-service')->item(0)->getElementsByTagName('start-time')->item(0)->nodeValue);
                $endDate = strtotime($xdoc->getElementsByTagName('merchant-service')->item(0)->getElementsByTagName('end-time')->item(0)->nodeValue);
                $currentDate = strtotime(date('Y-m-d H:i:s'));
               
                if($startDate>$currentDate)
                throw New Exception("INVALID_START_DATE");

                if($endDate>$currentDate)
                throw New Exception("INVALID_END_DATE");

                if($startDate>$endDate)
                throw New Exception("INVALID_DATE");
            }           
            $pay4MeXMLV1Obj = new pay4MeXMLV1();
          
            $Pay4meXMLHistoryObj = pay4meXmlServiceFactory::getHistoryRequest($xdoc, 'v1', $mechantCode) ;
            
            //get response object
            $responseObj = $Pay4meXMLHistoryObj->getResponseObject() ;
          
            //generate response xml
            $xml = $responseObj->toXml() ;
            
            //log creation
            pay4MeUtility::setLog($xml,'OrderHistoryResponseXml');

            $pay4MeXMLV1Obj->setHistoryResponseHeader($mechantCode);
            return $xml;
        } catch (Exception $ex) {           
            $msg = $ex->getMessage();
            $logger=sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: ".$msg);
           
            return pay4MeUtility::errorhistory($msg,$xdoc,'V1',$this->error_parameter,$xmldata,$mechantCode);
             
        }
    }
}
?>
