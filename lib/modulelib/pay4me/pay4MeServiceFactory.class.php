<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4MeServiceFactoryclass
 *
 * @author sdutt
 */
class pay4MeServiceFactory {
   /**
  *
  * @param <type> $type
  * @return empService the service implementation
  */
 public static function getService($version='') {

     $pay4MeManager = "pay4Me" . strtoupper($version) . "Manager" ;
     $pay4MeManagerObj = new $pay4MeManager;
     return $pay4MeManagerObj;
 }
}
?>
