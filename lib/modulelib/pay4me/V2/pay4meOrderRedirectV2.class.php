<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderRedirectV2
 *
 * @author anurag
 */
class pay4meOrderRedirectV2 {

    public $redirect_url ;
    public $redirectKey ;


    public function __construct($merchant_request,$version='')
    {
        $transaction_num = $merchant_request->getTxnRef() ;
        $requestId = $merchant_request->getId() ;
        $redirectKeyStr = $transaction_num.":".$requestId.":".$version;

        $this->redirectKey = base64_encode($redirectKeyStr) ;
    }

    public function toXml()
    {
        $logger=sfContext::getInstance()->getLogger();

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        //      $orderRedirectXml
        $obj = new pay4MeXMLV2();
        $logger->info(url_for("@order_payment?order=".$this->redirectKey,true));
        $orderRedirectXml = $obj->generatePay4MeOrderRedirect($this->xml_character_encode(url_for("@order_payment?order=".$this->redirectKey,true)));

        $logger->info('sending redirect XML');
        $logger->log(print_r($orderRedirectXml, true)) ;
        return $orderRedirectXml;
    }

    protected function xml_character_encode($string, $trans='') {
        $trans = (is_array($trans)) ? $trans : get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
        foreach ($trans as $k=>$v)
        $trans[$k]= "&#".ord($k).";";

        return strtr($string, $trans);
    }


}
?>
