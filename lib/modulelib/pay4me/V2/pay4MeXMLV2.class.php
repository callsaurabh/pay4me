<?php

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class pay4MeXMLV2 {

    public function generatePay4MeOrderRedirect($url) {


        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('order-redirect');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v2' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder payformeorderV2.xsd' );
        $root = $doc->appendChild($root);
        $redirect_url = $doc->createElement("redirect-url");
        $redirect_url->appendChild($doc->createTextNode($url));
        $root->appendChild($redirect_url);
        $xmldata = $doc->saveXML();

        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);



        //validate the XML
        $xmlValidatorObj = new validatePay4meXML() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "V2") ;
        if($isValid) {
            return $xmldata;
        }
        else {
            return false;
        }
    }

    public function generatePaymentNotificationXML($transaction_no) {


        $logger=sfContext::getInstance()->getLogger();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));


        //get all the details of the transaction_no from database

        $pay4MeV2ManagerObj = new pay4MeV2Manager();
        $pfmTransactionDetails = $pay4MeV2ManagerObj->getNotificationTransactionDetailsV2($transaction_no);

//             print "<pre>";
//              print_r($pfmTransactionDetails);exit;
        if(count($pfmTransactionDetails) > 0) {
           
            $itemNumber  =  $pfmTransactionDetails['itemNumber'];
            $merchantServiceId = $pfmTransactionDetails['MerchantRequest']['MerchantService']['id'];
            $transactionNumber = $pfmTransactionDetails['MerchantRequest']['txn_ref'];
            $itemFee = $pfmTransactionDetails['MerchantRequest']['item_fee'];
            $itemFeeLowestUnit = convertToKobo($itemFee);
            $updatedAt = $this->convertDbDateToXsdDate($pfmTransactionDetails['MerchantRequest']['updated_at']);
            $paymentModeName = strtolower($pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name']);
            $bankName = $pfmTransactionDetails['MerchantRequest']['Bank']['bank_name'];
            $branchName = $pfmTransactionDetails['MerchantRequest']['BankBranch']['name'];;
            $paymentStatusCode = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];
            $validationNumber = $pfmTransactionDetails['MerchantRequest']['validation_number'];
            $transactionLocation = $pfmTransactionDetails['MerchantRequest']['transaction_location'];
            $currencyCode = $pfmTransactionDetails['MerchantRequest']['CurrencyCode']['currency_num'];
            if($paymentModeName == 'check') {
                $paymentModeName = 'cheque';
            }


            if($paymentStatusCode==2) {
                $desc = 'Unsuccessful payment';
            }
            else if($paymentStatusCode=='0') {
                $desc = 'Payment successfully done';
            }
            else if($paymentStatusCode==1) {
                $desc = 'Not any payment request';
            }


            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;
            $root = $this->createRoot($doc,"payment-notification");
            $root = $doc->appendChild($root);

            //set merchant service
            $merchant_service = $doc->createElement("merchant-service");
            $merchant_service->setAttribute ( "id", $merchantServiceId );

            //set item number
            $item_number = $doc->createElement( "item");
            $item_number->setAttribute ( "number", $itemNumber );

            // set transacction number
            $transaction_number = $doc->createElement("transaction-number");
            $transaction_number->appendChild($doc->createTextNode($transactionNumber));

            //set valication number
            $validation_number = $doc->createElement("validation-number");
            $validation_number->appendChild($doc->createTextNode($validationNumber));


            //setting payment Information
            $payment_information = $doc->createElement("payment-information");
            
            //set amount
            $amount = $doc->createElement("amount");
            $amount->appendChild($doc->createTextNode($itemFeeLowestUnit));
            
            //set currency
            $currency = $doc->createElement("currency");
            $currency->setAttribute ( "code", $currencyCode );

            //set payment date
            $payment_date = $doc->createElement("payment-date");
            $payment_date->appendChild($doc->createTextNode($updatedAt));

            //set transaction location
            $transaction_location = $doc->createElement("transaction-location");
            $transaction_location->appendChild($doc->createTextNode($transactionLocation));

            //set payment mode
            $paymentMode = $doc->createElement("payment-mode");
            $paymentMode->appendChild($doc->createTextNode($paymentModeName));

            //set payment details in case of bank and merchant-counter
            if($paymentModeName == "merchant-counter" || $paymentModeName == "bank"){
                $paymentModeDetail = $doc->createElement("bank");
                $bank_name = $doc->createElement("name");
                $bank_name->appendChild($doc->createTextNode($bankName));
                $branch_name = $doc->createElement("branch");
                $branch_name->appendChild($doc->createTextNode($branchName));
            }

            //set payment status and description
            $status = $doc->createElement("status");
            $code = $doc->createElement("code");
            $code->appendChild($doc->createTextNode($paymentStatusCode));
            $description = $doc->createElement("description");
            $description->appendChild($doc->createTextNode($desc));


            //start appending the child to parent
            $root->appendChild($merchant_service);
            $merchant_service->appendChild($item_number);
            $item_number->appendChild($transaction_number);

            if(sfconfig::get('app_send_validation_number')){
                $item_number->appendChild($validation_number);
            }
            
            $item_number->appendChild($payment_information);

            $payment_information->appendChild($amount);
            $payment_information->appendChild($currency);
            $payment_information->appendChild($payment_date);
            $payment_information->appendChild($transaction_location);
            $payment_information->appendChild($paymentMode);

            //append tags in case of bank and merchant-counter
            if($paymentModeName == "merchant-counter" || $paymentModeName == "bank"){
                $payment_information->appendChild($paymentModeDetail);
                $paymentModeDetail->appendChild($bank_name);
                $paymentModeDetail->appendChild($branch_name);
            }
            
            $payment_information->appendChild($status);
            $status->appendChild($code);
            $status->appendChild($description);

           $xmldata = $doc->saveXML();

            // $logger->info($xmldata);

            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);

            //validate the XML
            $xmlValidatorObj = new validatePay4meXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, "V2") ;
            if($isValid) {
                return $xmldata;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }


    }


    public function createRoot($doc,$rootName){
        $root = $doc->createElement($rootName);
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v2' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder/v2 payformeorderV2.xsd' );
        return $root;
    }


    public function generateErrorNotification($error_code, $desc) {
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('error');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v2' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder payformeorderV2.xsd' );
        $root = $doc->appendChild($root);
        $code = $doc->createElement("error-code");
        $code->appendChild($doc->createTextNode($error_code));
        $description = $doc->createElement("error-message");
        $description->appendChild($doc->createTextNode($desc));
        $root->appendChild($code);
        $root->appendChild($description);
        $xmldata = $doc->saveXML();

        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        //validate the XML
        $xmlValidatorObj = new validatePay4meXML() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "V2") ;
        if($isValid) {
            return $xmldata;
        }
        else {
            return false;
        }
    }

    protected function convertDbDateToXsdDate($dbDate) {
        $dtokens=explode(' ', $dbDate);
        $xdate = implode('T',$dtokens);
        return $xdate;
    }

    protected function xml_character_encode($string, $trans='') {
        $trans = (is_array($trans)) ? $trans : get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
        foreach ($trans as $k=>$v)
        $trans[$k]= "&#".ord($k).";";
        return strtr($string, $trans);
    }
    public function setResponseHeader($merchantServiceId){
        if(trim($merchantServiceId)){
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $merchantDetail  = $payForMeObj->getMerchantIdByService($merchantServiceId);
            $header = "";
            if(count($merchantDetail)){
                $merchantCode = $merchantDetail[0]['merchant_code'];
                $merchantKey = $merchantDetail[0]['merchant_key'];
                $merchantAuthString = $merchantCode .":" .$merchantKey;
                // $logger->info("auth string: $merchantAuthString");
                $merchantAuth = base64_encode($merchantAuthString) ;

                $header['Authorization'] = "Basic $merchantAuth";
                $header['Content-Type'] =  "application/xml;charset=UTF-8";
                $header['Accept'] = "application/xml;charset=UTF-8";
            }
            $this->setHeader($header);
        }
    }
    private function setHeader($header){
        foreach($header as $key=>$val){
            sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
        }
    }
    public function generateSamlpleOrderXml($merhant_service_id){

        $service_detail=Doctrine::getTable('MerchantService')->getDataforSampleXml($merhant_service_id);
        if($service_detail && count($service_detail)>0)
        {
            $service_detail=$service_detail[0];


            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;
            $root = $doc->createElement('order');
            $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v2' );
            $root = $doc->appendChild($root);

            $bodyElm=$this->getMiddleContent($service_detail,$doc);
            $root->appendChild($bodyElm);

            $xmldata = $doc->saveXML();

            ///////////////Logging   ////////////////////////////////////////////////////
            $pay4meLog = new pay4meLog();
            $pay4meLog->createLogData($xmldata,'SampleOrderXml','SampleOrder');
            //////////////////////////////////////////////////////////////////////////////


            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);

            $merchant_code = $service_detail['Merchant']['merchant_code'];
            $merchant_key =  $service_detail['Merchant']['merchant_key'];
            $merchant_auth_string = $merchant_code .":" .$merchant_key ;
            $merchantAuth = base64_encode($merchant_auth_string) ;
            $pageURL = 'http';

            if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on"){
                $pageURL.= "s";
            }
            $pageURL .= "://";

            if($_SERVER["SERVER_PORT"] != "80"){
                $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["SCRIPT_NAME"];//.$_SERVER["SCRIPT_FILENAME"];
            }else{
                $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"];//.$_SERVER["SCRIPT_FILENAME"];
            }

            //echo"<pre>";print_r($_SERVER);
            //sfConfig::set('app_pay4me_sandbox_url',$pageURL);

            //secho sfConfig::get('app_pay4me_sandbox_url');
            //$preUrl=sfConfig::get('app_pay4me_sandbox_url');
            $preUrl=$pageURL;

            $url =$preUrl."/order/payment/payprocess/v2/PID/".$merchant_code ;
            $header[] = "Authorization: Basic $merchantAuth";
            $header[] = "Content-Type: application/xml;charset=UTF-8";
            $header[] = "Accept: application/xml;charset=UTF-8 ";
            //validate the XML
            $xmlValidatorObj = new validatePay4meXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, "V2") ;
            if($isValid) {

                $xmldetail[0]=$url;
                $xmldetail[1]=$header;
                $xmldetail[2]=$xmldata;

                return $xmldetail;
            }
            else {
                return false;
            }

            // print_r($xmldata); die;
        }
        else
        { return false;}

    }

    public function getMiddleContent($seviceDetail,$doc){

        $serviceElm = $doc->createElement("merchant-service");
        $serviceElm->setAttribute("id",$seviceDetail['id']);

        $itemNumber=rand();

        $itemElm = $doc->createElement("item");
        $itemElm->setAttribute("number",$itemNumber);

        $TransNumber=rand();
        $TxnElm = $doc->createElement("transaction-number");
        $TxnElm->appendChild($doc->createTextNode($TransNumber));
        $itemElm->appendChild($TxnElm );

        $name="name";
        $fname = $doc->createElement("name");
        $fname->appendChild($doc->createTextNode($name));
        $itemElm->appendChild($fname );

        $description="description";
        $Desc = $doc->createElement("description");
        $Desc->appendChild($doc->createTextNode($description));
        $itemElm->appendChild($Desc );

        $price="25";
        $amt=$doc->createElement("price");
        $amt->appendChild($doc->createTextNode($price));
        $itemElm->appendChild($amt);

        $curr=$doc->createElement("currency");
        $curr->appendChild($doc->createTextNode('naira'));
        $itemElm->appendChild($curr);

        $payInfo=$doc->createElement("parameters");

        foreach($seviceDetail['ValidationRules'] as $validRules){

            $biller_n=$doc->createElement("parameter");
            $biller_n->setAttribute("name",$validRules['param_name']);

            if("integer"==$validRules['param_type'])
            $paramVal=rand(10,1000);
            else
            $paramVal="ABCD";
            $biller_n->appendChild($doc->createTextNode($paramVal));
            $payInfo->appendChild($biller_n);


        }

        $itemElm->appendChild($payInfo);

        $serviceElm->appendChild($itemElm);

        return $serviceElm;
    }


    public function createiPay4meOrderXml(array $orderData){

        if(count($orderData) > 0){


          //  $ipay4memerchantMappingDetails = Doctrine::getTable('ipay4meMerchantServiceMapping')->getMerchantDetailsForServiceId($orderData['merchantServiceId']);
          $ipay4memerchantMappingDetails = Doctrine::getTable('ipay4meMerchantServiceMapping')->findByMerchantServiceId($orderData['merchantServiceId']);
          $ipay4me_merchant_id=$ipay4memerchantMappingDetails->getFirst()->getIpay4meMerchantId();

            $updatedAt = date('Y-m-d H:i:s');
            $updateDateAArr = explode(" ", $updatedAt);

            $updatedAt = $updateDateAArr[0].'T'.$updateDateAArr[1];

            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;

            $root = $this->setXMLroot($doc, 'order');

            $root = $doc->appendChild($root);
            $merchant = $doc->createElement("merchant");
            $merchant->setAttribute ( "id", $ipay4me_merchant_id );

            //transaction number
            $transaction_number = $doc->createElement( "item-number");
            $transaction_number->appendChild($doc->createTextNode($orderData['transactionNumber']));

            //retry-id
            $retry_id = $doc->createElement( "retry-id");
            $retry_id->appendChild($doc->createTextNode($orderData['retry_id']));

            //merchant Information
            $name = $doc->createElement("name");
            $name->appendChild($doc->createTextNode($orderData['merchantName']));
            $description = $doc->createElement("description");
            $description->appendChild($doc->createTextNode($orderData['merchantDescription']));

            //adding currency
            $currency = $doc->createElement("currency");
            $currency->setAttribute("type", "usd");
            $currency->appendChild($doc->createTextNode($orderData['amount']));


            //start appending the child to parent
            $root->appendChild($merchant);
            $merchant->appendChild($transaction_number);
            $merchant->appendChild($retry_id);
            $merchant->appendChild($name);
            $merchant->appendChild($description);
            $merchant->appendChild($currency);

            $revenue_split = $this->getRevenueShare($ipay4me_merchant_id, $orderData['amount'], $doc, $merchant );

            $xmldata = $doc->saveXML();//print $xmldata;exit;
            return $xmldata;
        } else {
            return false;
        }

    }

    private function getRevenueShare($ipay4me_merchant_id, $totalAmt, $doc, $merchant) {
  
         $ipay4meSplitDetails = Doctrine::getTable('ipay4meSplit')->getSplitDetails($ipay4me_merchant_id);
         sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
         if ($ipay4meSplitDetails) {
           foreach($ipay4meSplitDetails as $values) {
             $source_id = $values->getipay4meMerchantSourceId();
             $destination_id = $values->getipay4meMerchantDestId();
             $pay_merchant_fee = $values->getPayMerchantFee();
             $split_value = $values->getValue();
             $splitType = $values->getSplitTypeId();
             if($splitType == 2){
                  $amountValue = ($totalAmt / 100) * $split_value;
              }else if($splitType == 1){
                $amountValue = $split_value;
              }
              
              $amountValue = convertToKobo($amountValue);
             if($source_id == $destination_id){
                $revenue_share = $doc->createElement("revenue-share");
                $revenue_share->setAttribute("fixed-share", $amountValue);
                if($pay_merchant_fee == 1) {
                  $revenue_share->setAttribute("pays-fee", "true");
                }
                else {
                  $revenue_share->setAttribute("pays-fee", "false");
                }

                $merchant->appendChild($revenue_share);
                $other_shares = $doc->createElement("other-shares");
                $revenue_share->appendChild($other_shares);

             }
             else {
               $merchant_code = $values->getiPay4meDestMerchantServiceMapping()->getipay4meMerchantCode();
               $other_share = $doc->createElement("other-share");
               $other_share->setAttribute("merchant-code", $merchant_code);
               if($pay_merchant_fee == 1) {
                  $other_share->setAttribute("pays-fee", "true");
                }
                else {
                  $other_share->setAttribute("pays-fee", "false");
                }
                $other_share->appendChild($doc->createTextNode($amountValue));
                $other_shares->appendChild($other_share);
             }

           }return $other_share;
         }
         else {
            $revenue_share = $doc->createElement("revenue-share");
            $revenue_share->setAttribute("fixed-share", convertToKobo($totalAmt));
            $revenue_share->setAttribute("pays-fee", "false");
            $other_shares = $doc->createElement("other-shares");
            $merchant->appendChild($revenue_share);
            $revenue_share->appendChild($other_shares);
            return $revenue_share;
         }
         
         
    }



    




    public function settingiPay4meHeaderInfo($merchantServiceId){
       
        $merchantDetail = Doctrine::getTable('ipay4meMerchantServiceMapping')->getMerchantDetailsForServiceId($merchantServiceId);
        
        $header = "";
        if(count($merchantDetail)){
            $merchantCode = $merchantDetail[0]['ipay4me_merchant_code'];
            $merchantKey = $merchantDetail[0]['ipay4me_merchant_key'];
            $merchantAuthString = $merchantCode .":" .$merchantKey;
            // $logger->info("auth string: $merchantAuthString");
            $merchantAuth = base64_encode($merchantAuthString) ;

            $header['Authorization'] = "Basic $merchantAuth";
            $header['Content-Type'] = "application/xml;charset=UTF-8";
            $header['Accept'] = "application/xml;charset=UTF-8";
        }
        return $header;
    }


    public function setXMLroot($doc,$rootName){
        $root = $doc->createElement($rootName);
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.ipay4me.com/schema/ipay4meorder/v1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.ipay4me.com/schema/ipay4meorder/v1 ipay4meorderV1.xsd' );

        return $root;
    }

    public function getRedirectURL($xmlResponse){ 
        $xdoc = new DomDocument('1.0');
        $isLoaded = $xdoc->LoadXML($xmlResponse);
        $rootName=$xdoc->firstChild->nodeName;

        switch($rootName) {
         case "error" :
            $errorCode = $xdoc->getElementsByTagName('error-code')->item(0)->nodeValue;
            $errorMsg = base64_encode($xdoc->getElementsByTagName('error-message')->item(0)->nodeValue);
            $errorArr=array($errorCode,$errorMsg);
            $xml = '?xmlresponse='.serialize($errorArr);
            return sfContext::getInstance()->getController()->redirect('order/error'.$xml );
            throw New Exception($errorMsg);
             break ;
         case "order-redirect" :
            return $redirectURL = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue;
             break ;        
         default;
             throw New Exception('Invalid xml returned');
             break;
        }
        
    }

}
?>
