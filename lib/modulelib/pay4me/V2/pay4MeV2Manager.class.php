<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4MeV2Managerclass
 *
 * @author sdutt
 */

class pay4MeV2Manager implements pay4MeService{
    //put your code here
    public  $browserInstance = NULL;
    public $error_parameter = array();
    public function processOrder($request){
        //order processing logic
        sfContext::getInstance()->getResponse()->setContentType('text/xml') ;
        $xmldata = file_get_contents('php://input'); //$request->getRawBody() ;
        
        //PAYM-2013
        $xmlString = explode('<transaction-number>',$xmldata);
        $xmlTransaction = explode('</transaction-number>',$xmlString['1']);
        $transactionNo = $xmlTransaction['0'];
        //PAYM-2013
        
        //log creation
        pay4MeUtility::setLog($xmldata,'NewOrderItem');
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        $pay4MeXMLV2Obj = new pay4MeXMLV2() ;
        $merchant_param_name = sfConfig::get('app_merchant_url_param_name') ;
        $merchant_code = $request->getParameter($merchant_param_name) ;
        $Pay4meXMLOrderObj = pay4meXmlServiceFactory::getRequest($xdoc, 'v2', $merchant_code) ;

        
         try {
            sfContext::getInstance()->getResponse()->setContentType('text/xml') ;

            $xmlValidatorObj = new validatePay4meXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, 'v2') ;

            $merchantServicePrice= $xdoc->getElementsByTagName('merchant-service');
            $merchantRequestV2XmlPrice = $merchantServicePrice->item(0)->getElementsByTagName('price')->item(0)->nodeValue;

            if(preg_match("/^[0-9]+$/", $merchantRequestV2XmlPrice)){
                if($merchantRequestV2XmlPrice > '100000000000') {
                    throw New Exception("INVALID_PRICE_LIMIT_V2");
                }
            }
            
           if(!$isValid)
            {
                   $xmlError=$xmlValidatorObj->getXmlErrors();

                  throw New Exception($xmlError);

            }

            if(!MerchantTable::isValidMerchant($merchant_code))
            throw New Exception("INVALID_MERCHANT");

            $payForMeObj = payForMeServiceFactory::getService('payforme') ;
            $isMerchantAuthenticate = $payForMeObj->authenticateMerchantRequest($request) ;

            if(!$isMerchantAuthenticate)
            throw New Exception("AUTHENTICATION_FAILURE");

            if (!$payForMeObj->isValidMerchantServiceId($merchant_code ,$xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id')))
            throw New Exception("INVALID_SERVICE");

            $activeMerServiceObj =  paymentScheduleConfigServiceFactory::getService(paymentScheduleConfigServiceFactory::$merchantConfig);
            if (!$activeMerServiceObj->isMerchantServiceActive($merchant_code ,$xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id')))
            throw New Exception("INACTIVE_SERVICE");

            //check
            if(!$this->checkMerchantServiceCurrency($xdoc)){
                throw New Exception("INVALID_CURRENCY");
            }
            //check
            if(!$this->checkPaymentMode($xdoc)){
                throw New Exception("INVALID_PAYMENT_MODE");
            }
            $isValidSplit = $this->isValidSplit($xdoc) ;
            if(!$isValidSplit)
                throw New Exception("INVALID_SPLIT");

            $isValidMrchntAcct = $this->isValidMerchantAccount($xdoc) ;
            if(!$isValidMrchntAcct)
                throw New Exception("INVALID_MERCHANT_ACCOUNT");

            $created = $Pay4meXMLOrderObj->getDBObjectNew($Pay4meXMLOrderObj) ; // try - catch
            if(!ctype_digit($created)){ //checks if the merchant_request_id is returned
                throw New Exception($created);
            }
            
            //PAYM-2013
            $merchantRequestId = Doctrine::getTable('MerchantRequest')->getMerchantRequestIdByTxnRef($transactionNo);
            $obj = new MerchantRequestLog();
            $obj->setMessageText($xmldata);
            $obj->setTransactionNumber($transactionNo);
            $obj->setMerchantRequestId($merchantRequestId);
            $obj->save();
            //PAYM-2013
                
            $redirectXML = $Pay4meXMLOrderObj->getRedirectObject() ;

            $xml = $redirectXML->toXml() ;
            //log creation
            //pay4MeUtility::setLog($xml,'iPay4MeRedirectXml');
            $pay4meLog = new pay4meLog();
            $pay4meLog->createLogData($xml,'RedirectXml','pay4melog');

            $pay4MeXMLV2Obj->setResponseHeader($xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id'));
            return $xml;
        } catch (Exception $ex) {
            $this->error_parameter = $Pay4meXMLOrderObj->error_parameter;
            $msg = $ex->getMessage();
            $logger=sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: ".$msg);
            return pay4MeUtility::error($msg,$xdoc,'V2',$this->error_parameter,$xmldata,$merchant_code);
        }
    }

    public function checkMerchantServiceCurrency($xdoc){

        $currencies= $xdoc->getElementsByTagName('currency');

        $recordSet = Doctrine::getTable('CurrencyCode')->getCurrenyForMerchantService($xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id'));

        if(!$recordSet)
             return false;
             
         $DbCurrency = array();
         foreach($recordSet as $record){
             $AllDbCurrency[]=$record->getCurrencyNum();
         }
         $DbCurrency = array_unique($AllDbCurrency);
         $chkDupArr = array();
         foreach($currencies as $currency){
             $currencyCode = $currency->getAttribute('code');
             if(!in_array($currencyCode,$DbCurrency))
                return false;
             else
                $chkDupArr[] = $currencyCode;
         }
         
            if(count($chkDupArr)==count(array_unique($chkDupArr)))
                return true;
             else
                return false;
    }
    public function checkPaymentMode($xdoc){
        $currencies= $xdoc->getElementsByTagName('currency');

        for ($k = 0; $k < $currencies->length; $k++) {
            $currencyCode = $currencies->item($k)->getAttribute('code');
            
            
            $payMode= $xdoc->getElementsByTagName('payment-mode');
            $paymentModes = $currencies->item($k)->getElementsByTagName('payment-modes');
            
            $recordSet = Doctrine::getTable('ServicePaymentModeOption')->getPaymentOptionsFromCurrnecyAndService($xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id'),$currencyCode);

            if(!$recordSet && $paymentModes->length>0)
                 return false;

            $DbPayMode = array();
             foreach($recordSet as $record){
                 $DbPayMode[]=$record->getPaymentModeOptionId();
             }
            for ($i = 0; $i < $paymentModes->length; $i++) {

                    for ($j = 0; $j < $paymentModes->item($i)->getElementsByTagName('payment-mode')->length;$j++){
                         $payMod =  $paymentModes->item($i)->getElementsByTagName('payment-mode')->item($j)->nodeValue;
                         if(!in_array($payMod,$DbPayMode))
                          return false;

                    }
            }
             
        }
         return true;
    }
    public function isValidSplit($xdoc) {
        $currencies= $xdoc->getElementsByTagName('currency');
        
        for ($k = 0; $k < $currencies->length; $k++) {
            $price = $currencies->item($k)->getElementsByTagName('price')->item(0)->nodeValue;
            $revenue = $currencies->item($k)->getElementsByTagName('revenue-share');
          
            for ($i = 0; $i < $revenue->length; $i++) {

                    $fixedshare = $revenue->item($i)->getAttribute('fixed-share');
          
                    $otherShare =0;
                    for ($j = 0; $j < $revenue->item($i)->getElementsByTagName('other-share')->length;$j++){
                         $otherShare = $otherShare + $revenue->item($i)->getElementsByTagName('other-share')->item($j)->nodeValue;
                         
                    }//other-share
                    $allSplit = $fixedshare + $otherShare ;
                    $TotShare = $price;
                    if($TotShare != $allSplit)
                      return false;
                }//revenue
        } //currency
       return true;
      
    }

   public function isValidMerchantAccount($xdoc) {
        $OtherShares= $xdoc->getElementsByTagName('other-share');
        $walletObj = new EpAccountingManager;
//        $chkDupArr = array();
        foreach($OtherShares as $OtherShare){
             $AcctNo = $OtherShare->getAttribute('merchant-account-number');
             $walletDetails = $walletObj->getWalletDetails($AcctNo);
             if((!is_object($walletDetails)) && $walletDetails == 0) {
                 return false;
               }
              else{
                  $arrCheck = $walletDetails->getFirst()->getUserAccountCollection()->toArray();                   
                   //check account no should be for a ewallet user
                   if(count($arrCheck)==0){
                       return false;
                   }

                   $orderCurrencyCode= $xdoc->getElementsByTagName('currency')->item(0)->getAttribute('code');                   
                   $currencyCode = $walletDetails->getFirst()->getUserAccountCollection()->getfirst()->
                                    getCurrencyCode()->getCurrencyNum();

                   if($orderCurrencyCode != $currencyCode)
                   throw New Exception("INVALID_MERCHANT_ACCOUNT_CURRENCY");
                }
 
         }
//       if(count($chkDupArr)==count(array_unique($chkDupArr)))
            return true;
//       else
//            return false;

    }
   /* public function processRedirectOrderURL($requestId){
        $resultSet = Doctrine::getTable('TempRequest')->find($requestId);
        $serviceId = $resultSet->getMerchantServiceId();
        $recordSet = Doctrine::getTable('MerchantServiceCurrency')->getCurrenyForMerchantService($serviceId);
        try{
            if(count($recordSet)>1 && !empty($resultSet['price_naira']) && !empty($resultSet['price_usd'])){
                return false;
            }else{
                if(!empty($resultSet['price_usd'])){ 
                    return $this->getUSDRedirectUrl($requestId);
                }else if(!empty($resultSet['price_naira'])){
                    return $this->getNairaRedirectUrl($requestId);
                }
                throw "Item amount not found.";
            }
        }catch(Exception $e){
            echo $e->getMessage();
            die;
        }
    }*/
  public function getRequestPaymentOptions($pay_detail_id, $currency_id, $request_id) {
      $paymentModeOptions = Doctrine::getTable('MerchantRequestPaymentMode')->getPaymentOptions($pay_detail_id);
      $payment_mode_option = array();
      if($paymentModeOptions) {
        $i = 0;
        foreach($paymentModeOptions  as $options) {
          $payment_mode_option[$currency_id][$i] = $options->getPaymentModeOptionId();
          $i++;
        }
      }
      else {
        $merchantRequestObj = Doctrine::getTable('MerchantRequest')->find($request_id);
        $merchant_service_id = $merchantRequestObj->getMerchantServiceId();
        $options = Doctrine::getTable('ServicePaymentModeOption')->getCurrencyPaymentOptions($merchant_service_id, $currency_id);
        if($options) {
          $i = 0 ;
          foreach($options as $payment_modes) {
            $payment_mode_option[$currency_id][$i] = $payment_modes->getPaymentModeOptionId();
            $i++;
          }
        }
        else {
          //throw an exception that no payment mode option set for currency - service
        }
      }
     // print "<pre>";
    //  print_r($payment_mode_option);exit;
      return $payment_mode_option;
    }

      public function processRedirectOrderURL($requestId) {
        //$requestId=171;
        $recordSet = Doctrine::getTable('MerchantRequestPaymentDetails')->getCurrenyRequests($requestId);
        $currency_id = 1; //we assume that the currency option is Naira by default; for v1 compatibily for which the currency won't be sent
        if($recordSet) { //if currency was ent by the merchant
           $total_count = $recordSet->count();
           $currency = array();
           foreach($recordSet as $val) {
              //$pay_detail_id = $val->getId();
              $currency[$val->getCurrencyId()]['name'] = $val->getCurrencyCode()->getCurrency();
              $currency[$val->getCurrencyId()]['amount'] = $val->getAmount();
              //$this->getRequestPaymentOptions($pay_detail_id, $currency_id, $requestId);
            }

            return $currency;
        }
      }

    public function moveUSDToiPay4me($merId){
        try {
            //
            $tempObject = Doctrine::getTable('TempRequest')->find($merId);
            if($tempObject) {
                $merchant_service_id = $tempObject->getMerchantServiceId();
                $item_number = $tempObject->getItemNumber();

                $amount  = $tempObject->getPriceUsd();
                $iPay4meItemObj = Doctrine::getTable('ipay4meRequest')->check_item_duplicacy($item_number,$merchant_service_id);
                if(!$iPay4meItemObj) {
                    $iPay4meItemObj = new ipay4meRequest();
                    $iPay4meItemObj->setVersion($tempObject->getVersion());
                    $iPay4meItemObj->setMerchantId($tempObject->getMerchantId());
                    $iPay4meItemObj->setMerchantServiceId($merchant_service_id);
                    $iPay4meItemObj->setItemNumber($item_number);
                    $orderNumber = $this->generateOrderNumber();
                    $iPay4meItemObj->setOrderNumber($orderNumber);
                    $iPay4meItemObj->setAmount($amount);

                }
                else {
                    try {
                        //chk if payment has been made for this item
                        $request_id = $iPay4meItemObj->getId();
                        $paymentStatus = Doctrine::getTable('ipay4meRequestDetails')->chkPaymentStatus($request_id);
                        if($paymentStatus > 0) {
                            throw New Exception("Payment has already been done");
                        }
                    }catch (Exception $e) {
                        $logger=sfContext::getInstance()->getLogger();
                        $logger->err($e->getMessage()) ;
                        throw $e;
                    }
                }
                $iPay4meRequestDetailsObj = new ipay4meRequestDetails();
                $iPay4meRequestDetailsObj->setMerchantId($tempObject->getMerchantId());
                $iPay4meRequestDetailsObj->setMerchantServiceId($merchant_service_id);
                $iPay4meRequestDetailsObj->setTxnRef($tempObject->getTxnRef());
                $iPay4meRequestDetailsObj->setName($tempObject->getName());
                $iPay4meRequestDetailsObj->setDescription($tempObject->getDescription());
                $transNumber = $this->generateTransactionNumber();
                $iPay4meRequestDetailsObj->setTransactionNumber($transNumber);
                $iPay4meRequestDetailsObj->setipay4meRequest($iPay4meItemObj);
                $iPay4meRequestDetailsObj->setAmount($amount);
                $iPay4meItemObj->ipay4meRequestDetails[] = $iPay4meRequestDetailsObj;
                try{
                    $iPay4meItemObj->save();
                    return $iPay4meRequestDetailsObj->getId();
                }catch(Exception $e){
                    throw New Exception($e->getMessage()) ;
                }
            }
            else {
                throw New Exception("Request Details not Found!!");
            }
        }
        catch(Exception $e) {
            throw New Exception($e->getMessage());
        }

        //print  Doctrine::getTable('TempRequest')->moveToiPay4me($merId, $transNumber);exit;

    }



    public function generateTransactionNumber() {
        do {
            $transaction_number =  pay4MeUtility::getRandomNumber();
            $duplicacy_transaction_number = Doctrine::getTable('ipay4meRequestDetails')->chkDuplicacy($transaction_number);
        } while ($duplicacy_transaction_number > 0);
        return $transaction_number;
    }

    public function generateOrderNumber() {
        do {
            $order_number =  pay4MeUtility::getRandomNumber();
            $duplicacy_order_number = Doctrine::getTable('ipay4meRequest')->chkDuplicacy($order_number);
        } while ($duplicacy_order_number > 0);
        return $order_number;
    }

    public function validateResponseXML(){
        $xmldata = file_get_contents('php://input');
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);

        $pay4meLog = new pay4meLog();
        $pay4meLog->createLogData($xmldata,'ipay4meResponseXML','ipay4melog');

        try{
            $xmlValidatorObj = new validatePay4meXML() ;
            //      $isValid = $xmlValidatorObj->validateXML($xdoc, 'V2') ;
            //      if(!$isValid) // is a Valid P4M XM
            //      throw New Exception("INVALID_XML");

            $merchantId = $xdoc->getElementsByTagName('merchant')->item(0)->getAttribute('id');
            $orderNumber = $xdoc->getElementsByTagName('order')->item(0)->getAttribute('number');
            $transNo = $xdoc->getElementsByTagName('transaction-number')->item(0)->nodeValue;
            $paidAmt = $xdoc->getElementsByTagName('amount')->item(0)->nodeValue;
            $paymentDate = $xdoc->getElementsByTagName('payment-date')->item(0)->nodeValue;
            $paymentMode = $xdoc->getElementsByTagName('mode')->item(0)->nodeValue;
            $statusCode = $xdoc->getElementsByTagName('code')->item(0)->nodeValue;
            $retryId = $xdoc->getElementsByTagName('retry-id')->item(0)->nodeValue;
            $paymentDate = $this->convertIntoValidDate($paymentDate);
 
//            $ipay4meRequestDetails = Doctrine::getTable('ipay4meRequestDetails')->findByTransactionNumber($transNo);
//            $ipay4meRequestId = $ipay4meRequestDetails->getFirst()->getIpay4meRequestId ();
            $saveIpay4meResponse = Doctrine::getTable('ipay4meResponse')->saveIpay4meResponse($retryId, $statusCode, $paidAmt, $paymentDate, $paymentMode, $orderNumber);

            $notificationUrl = 'ipayForMe/sendIpay4meNotification';
            $taskId = EpjobsContext::getInstance()->addJob('PaymentNotification',$notificationUrl, array('ip4m_order_id' => $orderNumber));
            sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
            //$this->auditPaymentTransaction($txnId);

        } catch (Exception $ex) {
            //$this->error_parameter = $pay4MeXMLOrderObj->error_parameter;
            $msg = $ex->getMessage();
            //$this->logMessage("Exception in processing: ".$msg, 'err');
            //return pay4MeUtility::error($msg,$xdoc,'V2',$xmldata,$merchant_code);
        }
    }

    public function  auditPaymentTransaction($txnId) {
        //Log audit Details
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID,$txnId,$txnId));
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('txnid' => $txnId)),
            $applicationArr);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    function convertIntoValidDate($paymentDate){
        $explod = explode('T', $paymentDate);
        $newDate = date('Y-m-d h:i:s',strtotime($explod[0].' '.$explod[1]));
        return $newDate;
    }

    public function transferTempToCore($formData) {

        $merchantId = $formData['merchant_id'];
        $merchantServiceId = $formData['merchant_service_id'];
        $payForMeObj = payForMeServiceFactory::getService() ;
        $mItem = Doctrine::getTable('MerchantItem')->check_item_duplicacy($formData['item_number'],$merchantServiceId);
        if(!$mItem) {
            $mItem = new MerchantItem();
            $mItem->item_number = $formData['item_number'];
            $mItem->merchant_service_id = $merchantServiceId ;
            $mItem->payment_amount_requested = $formData['price_naira'];

            // $mItemId = $mItem->getId();

        }
        else {
            try {
                //chk if payment has been made for this item
                $item_id = $mItem->getId();
                $this->paymentStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
                if($this->paymentStatus == 0) {
                  throw New Exception("Payment has already been made for this item");
                  //  $errObj = new pay4meError();
                  //  return  $errObj->error(pay4meError::$ITEM_PAYMENT_ALREADY_DONE);
                }
            }catch (Exception $e){
                $logger=sfContext::getInstance()->getLogger();
                $logger->err($e->getMessage()) ;
                throw $e;
            }
        }


        $mRequest = new MerchantRequest();
        $mRequest->txn_ref = $formData['txn_ref'];
        $mRequest->merchant_id= $merchantId;
        $mRequest->merchant_service_id= $merchantServiceId;
        $mRequest->setMerchantItem($mItem);

        $mRequest->item_fee= $formData['price_naira'];
        $mRequest->payment_status_code= $payForMeObj->getPaymentStatusCode();
        $mItem->MerchantRequest[]=$mRequest;

        $mRequestDetails = new MerchantRequestDetails();


        $mRequestDetails->setMerchantServiceId($merchantServiceId);
        $mRequestDetails->merchant_request_id = $mRequest;
        $mRequestDetails->setMerchantItem($mItem);
        $mRequestDetails->name= $formData['name'];
        foreach($formData['merchant_r_details'] as  $mrdVal)
        {
            $VaildParam= Doctrine::getTable('ValidationRules')->getRulesByParamName($merchantServiceId,$mrdVal['params_name']);
            $mRequestDetails->$VaildParam[0]['mapped_to']=$mrdVal['param_value'];
        }
        $mItem->MerchantRequest[]=$mRequest;
        $mItem->MerchantRequestDetails[]=$mRequestDetails;


        $mItem->save();
        $mRequestId=$mRequest->getId() ;
        return $mRequestId;
    }


    public function getUSDRedirectUrl($merId){
        try { 

            $iPay4meRequestId = $this->moveUSDToiPay4me($merId);
            if(ctype_digit($iPay4meRequestId)) {
                return $this->payInUSDPostXml($iPay4meRequestId);
            }
            else {
                throw new Exception ($iPay4meRequestId);
            }
        }
        catch (Exception $e) {
            throw New Exception($e->getMessage());
        }
    }

    public function getNairaRedirectUrl($merId)
    {

        $tempMRecord=Doctrine::getTable('TempRequest')->find($merId,Doctrine::HYDRATE_ARRAY);
        try{
            if(count($tempMRecord)>1)
            {
                $formDetail=$tempMRecord;
                $tempMRRecord=Doctrine::getTable('TempRequestParams')->findByTempRequestId($formDetail['id'],Doctrine::HYDRATE_ARRAY);
                $formDetail['merchant_r_details']=$tempMRRecord;

                $version =$formDetail['version'] ;
                try {
                  $mRequestId = $this->transferTempToCore($formDetail) ;
                  if(!ctype_digit($mRequestId)) {
                    throw New Exception($mRequestId);
                  }
                }
                catch (Exception $e) {
                  throw New Exception($e->getMessage());
                }

                $returnUrl= $this->builtRedirectUrl($formDetail['txn_ref'],$mRequestId);
                return $returnUrl;
            }
            else{
                throw("Merchant Transaction cannot perform due to internal error!");
            }
        }catch (Exception $e){
            $logger=sfContext::getInstance()->getLogger();
            $logger->err($e->getMessage()) ;
            throw $e;
        }


    }
    public function builtRedirectUrl($txnRef,$requestId)
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $redirectKeyStr = $txnRef.":".$requestId ;

        $redirectKey = base64_encode($redirectKeyStr) ;
        return url_for("@order_pay?order=".$redirectKey,true);
    }
    public function payInUSDPostXml($requestId){
        $pay4MeXMLV2Obj = new pay4MeXMLV2() ;

        $ipay4meRequestDetails = Doctrine::getTable('ipay4meRequestDetails')->find($requestId);

        $order_number = $ipay4meRequestDetails->getipay4meRequest()->getOrderNumber();
        $orderData = array();
        $orderData['merchantServiceId'] = $ipay4meRequestDetails->getMerchantServiceId();
        $orderData['transactionNumber'] = $order_number;
        $orderData['merchantName'] = $ipay4meRequestDetails->getName();
        $orderData['merchantDescription'] = $ipay4meRequestDetails->getDescription();
        $orderData['amount'] = $ipay4meRequestDetails->getAmount();
        $orderData['retry_id'] = $ipay4meRequestDetails->getId();


       $orderXml = $pay4MeXMLV2Obj->createiPay4meOrderXml($orderData); 

        if($orderXml){
            try{
                //log creation
                $Pay4meLog = new pay4meLog();
                $Pay4meLog->createLogData($orderXml,'ipay4meOrderXML','ipay4melog');

                //url to post xml
                $url = sfConfig::get('app_ipay4me_redirect_url');
                $merchantDetail = Doctrine::getTable('ipay4meMerchantServiceMapping')->getMerchantDetailsForServiceId($orderData['merchantServiceId']);
                if(count($merchantDetail)){
                    $merchantCode = $merchantDetail[0]['ipay4me_merchant_code'];
                    $url .='/PID/'.$merchantCode;
                }else
                throw new Exception("Merchant service not found.");

                //getting browser object
                $browser = $this->getBrowser() ;

                if(!$browser)
                throw new Exception("Unable to set request.");

                //setting header data
                $header = $pay4MeXMLV2Obj->settingiPay4meHeaderInfo($ipay4meRequestDetails->getMerchantServiceId());

                $browser->post($url, $orderXml, $header);

               $code = $browser->getResponseCode();

                if ($code != 200 ) {
                    throw new Exception("Unable to send request.");
                }else{
                    $xmlResponse = $browser->getResponseText();
                    //pass the xml for log purpose
                    $Pay4meLog->createLogData($orderXml,'ipay4meRedirectXML','ipay4melog');
                    return $pay4MeXMLV2Obj->getRedirectURL($xmlResponse);


                }
            }catch(Exception $e){
                throw New Exception($e->getMessage());
            }
        }
    }

    public function getBrowser() {
        if(!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    public function getNotificationUrl($orderNumber){
        $notificationUrl = Doctrine::getTable('ipay4meResponse')->getNotificationUrl($orderNumber);
        return $notificationUrl;
    }

    public function getNotificationTransactionDetailsV2($transNumber){
        $notificationTxnDetails  = Doctrine::getTable('Transaction')->getNotificationTransactionDetailsV2($transNumber);
        return $notificationTxnDetails;
    }


}
?>
