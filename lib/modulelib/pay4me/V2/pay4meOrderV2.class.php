<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderclass
 *
 * @author anurag
 */
class pay4meOrderV2{

    public $version = 'v2' ;
    public $merchant_service_id ;
    public $item_number ;
    public $transaction_number ;
    public $name ;
    public $description ;
    public $price ;
    public $currency ;
    public $parameters ;
    public $transaction_location;
    public $error_parameter = array();
    public $currencies = array();
    private $merchant_request ;




    public function __construct($xdoc,$version)
    {
        $this->version = $version;
        $this->merchant_service_id = $xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id') ;

        $this->item_number = $xdoc->getElementsByTagName('item')->item(0)->getAttribute('number') ;
        $this->transaction_number = $xdoc->getElementsByTagName('transaction-number')->item(0)->nodeValue ;
        $this->name = $xdoc->getElementsByTagName('name')->item(0)->nodeValue ;
        $this->description = $xdoc->getElementsByTagName('description')->item(0)->nodeValue ;
        $params  = $xdoc->getElementsByTagName('parameter');
        $j=0;
        $this->parameters = array() ;
        foreach($params as $param)
        {
            $this->parameters[$params->item($j)->getAttribute('name')] = $params->item($j)->nodeValue ;
            $j++ ;
        }

      if($xdoc->getElementsByTagName("transaction-location")->length != 0) {//chk if transaction location is being passed;NIPOST passes it
         $this->transaction_location = $xdoc->getElementsByTagName('transaction-location')->item(0)->nodeValue ;
      }
       $currencies= $xdoc->getElementsByTagName('currency');

        for ($k = 0; $k < $currencies->length; $k++) {
            $currencyCode = $currencies->item($k)->getAttribute('code');
            $this->currencies[$k]['currencyCode'] = $currencyCode;

          //////////////////////////  Payment Modes //////////////////////////////////////////
            $paymentModes = $currencies->item($k)->getElementsByTagName('payment-modes');
            for ($i = 0; $i < $paymentModes->length; $i++) {

                for ($j = 0; $j < $paymentModes->item($i)->getElementsByTagName('payment-mode')->length;$j++){
                         $payMod =  $paymentModes->item($i)->getElementsByTagName('payment-mode')->item($j)->nodeValue;
                         $this->currencies[$k]['paymentMode'][$j] = $payMod;

                  }///paymentMode
            }///paymentModes
          ////////////////////////////////////////////////////////////////////////////////////
           if($currencies->item($k)->getElementsByTagName('deduct-at-source')->length != 0){
            $DeductAtSource= $currencies->item($k)->getElementsByTagName('deduct-at-source')->item(0)->nodeValue;
            $this->currencies[$k]['deductAtSource'] = $DeductAtSource;
           }
            $price = $currencies->item($k)->getElementsByTagName('price')->item(0)->nodeValue;
            $this->currencies[$k]['price'] = $price;

            if($currencies->item($k)->getElementsByTagName('revenue-share')->length != 0){
            $revenueShare= $currencies->item($k)->getElementsByTagName('revenue-share')->item(0)->getAttribute('fixed-share');
            $this->currencies[$k]['revenueShare'] = $revenueShare;
           }

           //////////////////////////  Other Share//////////////////////////////////////////
            $otherShares = $currencies->item($k)->getElementsByTagName('other-shares');
            for ($i = 0; $i < $otherShares->length; $i++) {

                for ($j = 0; $j < $otherShares->item($i)->getElementsByTagName('other-share')->length;$j++){
                         $otherShare =  $otherShares->item($i)->getElementsByTagName('other-share')->item($j)->nodeValue;
                         $accountNo = $otherShares->item($i)->getElementsByTagName('other-share')->item($j)->getAttribute('merchant-account-number');
                         $this->currencies[$k]['otherShare'][$j]['amount'] = $otherShare;
                         $this->currencies[$k]['otherShare'][$j]['accountNumber'] = $accountNo;

                  }///Other Share
            }///Other Share
          ////////////////////////////////////////////////////////////////////////////////////

        }


        return true ;
    }

    public function validator()
    {
        $merchant_code = 12 ;
        $validatorObj = new merchantXmlValidator($merchant_code) ;
        $validatorObj->validate($this) ;
        return true;
    }

    public function getDBObjectNew($pay4MeXMLOrderObj) {
     sfContext::getInstance()->getConfiguration()->loadHelpers(array('ePortal'));
//        echo "<pre>";print_r($pay4MeXMLOrderObj->currencies);die;
     try {
        $logger=sfContext::getInstance()->getLogger();

        $item_number = $pay4MeXMLOrderObj->item_number;
        $merchant_service_id = $pay4MeXMLOrderObj->merchant_service_id;

        //finding count of currencies
        $intCurrencieycount = count($pay4MeXMLOrderObj->currencies);
        if($intCurrencieycount==1){
            $pay4MeXMLOrderObj->price = $pay4MeXMLOrderObj->currencies[0]['price'];
            //$pay4MeXMLOrderObj->currency = $pay4MeXMLOrderObj->currencies[0]['currencyCode'];
        } else {
            $pay4MeXMLOrderObj->price = 0;
            //$pay4MeXMLOrderObj->currency =0;
        }
        /*check if payment has been done for naira*/

        $item = Doctrine::getTable('MerchantItem')->check_item_duplicacy($item_number,$merchant_service_id);

        if($item) {
        //chk if payment has been made for this item
          $item_id = $item->getId();
          $this->paymentStatus = Doctrine::getTable('MerchantRequest')->getPaymentStatus($item_id);
          if($this->paymentStatus == 0) {
            $errObj = new pay4meError();
            throw New Exception("ITEM_PAYMENT_ALREADY_DONE");
          }
        }
        else{
            $logger->info('Creating new item object');
            $logger->info($pay4MeXMLOrderObj->item_number);
            $logger->info(format_amount($pay4MeXMLOrderObj->price,null,1));
            $logger->info($pay4MeXMLOrderObj->merchant_service_id);
            $item = new MerchantItem();
            $item->setItemNumber($pay4MeXMLOrderObj->item_number) ;
            $item->setPaymentAmountRequested(format_amount($pay4MeXMLOrderObj->price,null,1));
            $item->setMerchantServiceId($pay4MeXMLOrderObj->merchant_service_id) ;
            
        }



      $payForMeManagerObj = new payForMeManager() ;
      $merchantsArr = $payForMeManagerObj->getMerchantIdByService($pay4MeXMLOrderObj->merchant_service_id) ;
      $merchant_id = $merchantsArr[0]['id'] ;


      $request = new MerchantRequest();
      $request->setTxnRef($pay4MeXMLOrderObj->transaction_number) ;
      $request->setMerchantId($merchant_id) ;
      $request->setMerchantServiceId($pay4MeXMLOrderObj->merchant_service_id) ;
      $request->setItemFee(format_amount($pay4MeXMLOrderObj->price,null,1)) ;
      $request->setPaymentStatusCode(1) ;
      $request->setVersion($this->version);
      if($pay4MeXMLOrderObj->transaction_location) {
        $request->setTransactionLocation($pay4MeXMLOrderObj->transaction_location);
      }
      $request->setMerchantItem($item);
      $item->MerchantRequest[]=$request;
      $rdetails = new MerchantRequestDetails();
      $merchantServiceValidationRules = ValidationRulesTable::getValidationRules($pay4MeXMLOrderObj->merchant_service_id) ;

      $rdetails->setMerchantRequest($request);
      $rdetails->setMerchantServiceId($pay4MeXMLOrderObj->merchant_service_id);
      $rdetails->setName($pay4MeXMLOrderObj->name);
      $rdetails->setMerchantItem($item);
     // $rdetails->setMerchantRequestId($request);
      $item->MerchantRequestDetails[] = $rdetails;

          try {
            if(!$merchantServiceValidationRules && (count($pay4MeXMLOrderObj->parameters)==0)) {
              $populateParams = 1;
            }
            else {
            $populateParams = $rdetails->populateParameters($merchantServiceValidationRules, $pay4MeXMLOrderObj->parameters) ;
            }

            if($populateParams == 1){

               ////////////////////////////////// Saving to Others Table ////////////////////////////////////
               foreach($pay4MeXMLOrderObj->currencies as $currency){

                   $currObj = Doctrine::getTable('CurrencyCode')->findByCurrencyNum($currency['currencyCode']);

                   if('false' ==$pay4MeXMLOrderObj->currencies[0]['deductAtSource'])
                        $DedAtsource = 0;
                   else
                        $DedAtsource = 1;

                   $MRPaymentDetails =  new MerchantRequestPaymentDetails();
                   $MRPaymentDetails->setMerchantRequest($request);
                   $MRPaymentDetails->setCurrencyId($currObj->getfirst()->getId());
                   $MRPaymentDetails->setAmount(format_amount($currency['price'],null,1));
                   $MRPaymentDetails->setDeductAtSource($DedAtsource);
                   $request->MerchantRequestPaymentDetails[]=$MRPaymentDetails;
                   if(isset($currency['paymentMode'])){
                    foreach($currency['paymentMode'] as $payMode){
                        $MRPaymentMode =  new MerchantRequestPaymentMode();
                        $MRPaymentMode->setMerchantRequest($request);
                        $MRPaymentMode->setMerchantRequestPaymentId($MRPaymentDetails);
                        $MRPaymentMode->setPaymentModeOptionId($payMode);
                        $MRPaymentDetails->MerchantRequestPaymentMode[]=$MRPaymentMode;
                    }
                   }
                   if(isset($currency['revenueShare'])){
                        $UserId = $merchantsArr[0]['ewallet_user_id'] ;
                        $currencyId = $currObj->getfirst()->getId();
                        if($UserId == ""){
                            throw New Exception("MERCHANT_NOT_CONFIGURED_WITH_EWALLET");
                        }
                        $accountDeatils = Doctrine::getTable('UserAccountCollection')->getAccounts($UserId,$currencyId);
                        if($accountDeatils) {
                                $accountNumber = $accountDeatils->getFirst()->getEpMasterAccount()->getAccountNumber();


                                //$user = Doctrine::getTable('UserDetail')->findByUserId($UserId);
                                //$AccountId = $user->getfirst()->getMasterAccountId ();
                                //$AcctDetail = Doctrine::getTable('EpMasterAccount')->find($AccountId);

                                $PSplit =  new Pay4meSplit();
                                $PSplit->setMerchantRequest($request);
                                $PSplit->setMerchantRequestPaymentId($MRPaymentDetails);
                                $PSplit->setAccountNumber($accountNumber);
                                $PSplit->setAmount($currency['revenueShare']);
                                $request->Pay4meSplit[]=$PSplit;
                            }else{
                                throw New Exception("INVALID_MERCHANT_ACCOUNT_CURRENCY");
                            }
                   }
                   if(isset($currency['otherShare'])){
                    foreach($currency['otherShare'] as $OShares){
                        $PSplit =  new Pay4meSplit();
                        $PSplit->setMerchantRequest($request);
                        $PSplit->setMerchantRequestPaymentId($MRPaymentDetails);
                        $PSplit->setAccountNumber($OShares['accountNumber']);
                        $PSplit->setAmount($OShares['amount']);
                        $request->Pay4meSplit[]=$PSplit;
                    }
                   }

               }
               //////////////////////////////////////////////////////////////////////////////////////////////
               $item->save();               
               
            //Saving into AVC Charges table
            //Check returns avc_amount tag in XML
             if(isset($pay4MeXMLOrderObj->parameters['avc_amount']) && $pay4MeXMLOrderObj->parameters['avc_amount'] != 0){
                $avc_charges =  new AvcPassportCharges();
                $avc_charges->setMerchantRequestId($request->getId());
                $avc_charges->setApplicationId($pay4MeXMLOrderObj->parameters['app_id']);
                $avc_charges->setAmount($pay4MeXMLOrderObj->parameters['avc_amount']); 
                $avc_charges->save();                  
            }
               $this->merchant_request = $request ;
               return  $request->getId(); //return the merchant_request_id
            }
            else{
              return $populateParams;
            }
          } catch (Exception $e){
            //echo "printing" ; print_r($rdetails->parameter) ; exit ; echo "this" ;
            $this->error_parameter = $rdetails->parameter ;
            $logger->err($e->getMessage()) ;
            throw New Exception($e->getMessage());
          }


      }
      catch (Exception $e) {
        $logger->err($e->getMessage()) ;
        throw $e;
      }
    }

    public function validateParams($merchant_service_id,$parameters) {
      $param_names = array();
      $merchantServiceValidationRules = ValidationRulesTable::getValidationRules($merchant_service_id) ;
      foreach($merchantServiceValidationRules as $validationRule) {
        $param_names[$validationRule->getParamName()] = $validationRule->getIsMandatory();
        if($validationRule->getIsMandatory()) {
            if(!isset($parameters[$validationRule->getParamName()]) || $parameters[$validationRule->getParamName()]=="") {
                $this->error_parameter = array('parameter'=>$validationRule->getParamName()) ;
                throw New Exception("MANDATORY_PARAM_MISSING");
            }
         }
        if( ($validationRule->getParamType() == 'string' ) ) {
            if(isset($parameters[$validationRule->getParamName()])
                && $parameters[$validationRule->getParamName()]!=""
                &&  !(is_string($parameters[$validationRule->getParamName()]))) {
                $this->error_parameter = array('parameter'=>$validationRule->getParamName()) ;
                throw New Exception("INVALID_PARAM_INTEGER");
            }
            }elseif( $validationRule->getParamType() == 'integer' )
                if(isset($parameters[$validationRule->getParamName()])
                && $parameters[$validationRule->getParamName()]!=""
                && ! is_numeric($parameters[$validationRule->getParamName()])) {
                $this->error_parameter = array('parameter'=>$validationRule->getParamName()) ;
                throw New Exception("INVALID_PARAM_STRING");
        }
      }
      foreach($parameters as $key=>$value) {
        if(!array_key_exists($key,$param_names)){
          $this->error_parameter = array('parameter'=>$key) ;
          throw New Exception("UNSUPPORTED_PARAM");
        }
      }
    }


    public function getRedirectObject()
    {

        $pay4meOrderRedirectV2Obj = new pay4meOrderRedirectV2($this->merchant_request,$this->version) ;
        return $pay4meOrderRedirectV2Obj;
    }
}
?>
