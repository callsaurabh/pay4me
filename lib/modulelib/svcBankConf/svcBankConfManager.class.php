<?php
class svcBankConfManager implements svcBankConfService {
    // function to get the SP Details
    public function getAllRecords() {
        try {
            return $all_records = Doctrine::getTable('ServiceBankConfiguration')->getAllRecords();
        }catch(Exception $e ) {
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getServiceBankConfigurationList() {
        try {
            $service_bank_configuration = Doctrine::getTable('ServiceBankConfiguration')->createQuery('a')->orderBy('merchant_id')->execute();
            $service_bank_configuration_list = array();
            $service_bank_configuration_list[''] = "Please select service_bank_configuration";
            foreach ($service_bank_configuration as $i => $service_bank_configuration_detail):
                $service_bank_configuration_list[$service_bank_configuration_detail->getId()] =  $service_bank_configuration_detail->getId();
            endforeach;
            return $service_bank_configuration_list;

        }catch(Exception $e ) {
            echo("Problem found". $e->getMessage());die;
        }
    }


}
?>