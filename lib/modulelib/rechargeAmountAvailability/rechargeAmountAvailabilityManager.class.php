<?php
class rechargeAmountAvailabilityManager {
  public function createNew($amount,$amount_wallet,$service_charges,$appId,$recharge_date, $payment_mode_option_id) {

    $rechargeObj = new RechargeAmountAvailability();
    $rechargeObj->setMasterAccountId($appId);
    $rechargeObj->setTotalAmountPaid($amount);
    $rechargeObj->setServiceCharges($service_charges);
    $rechargeObj->setAmountWallet($amount_wallet);
    $rechargeObj->setDateAvailableOn($recharge_date);
    $rechargeObj->setPaymentModeOptionId($payment_mode_option_id);

    $rechargeObj->save();

    return $recharge_id = $rechargeObj->getId();
  }

}

?>