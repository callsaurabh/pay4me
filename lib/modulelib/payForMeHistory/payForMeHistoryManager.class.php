<?php

class payForMeHistoryManager implements payForMeHistoryService {

    public $browserInstance;

    public function setUserDetails() {

        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;

        $root = $doc->createElement("user-details");
        $root = $doc->appendChild($root);
        
        $pfmHelperObj = new pfmHelper();
        
        $userGroupPermission = $doc->createElement('user-group-permission');
        $root->appendChild($userGroupPermission);
        $userGroupPermission->appendChild($doc->createTextNode($pfmHelperObj->getUserGroup()));
        
        $temp = sfContext::getInstance()->getUser()->getGuardUser()->getAppSessions()->toArray();

        $sessionId = $doc->createElement('session-id');
        $root->appendChild($sessionId);
        $sessionId->appendChild($doc->createTextNode($temp[0]['sess_id']));
        
        $sessionTime = $doc->createElement('session-time');
        $root->appendChild($sessionTime);
        $sessionTime->appendChild($doc->createTextNode($temp[count($temp)-1]['sess_time']));

        $userId = $doc->createElement('user-id');
        $root->appendChild($userId);
        $userId->appendChild($doc->createTextNode($temp[0]['user_id']));

        $username = $doc->createElement('username');
        $root->appendChild($username);
        $username->appendChild($doc->createTextNode($temp[0]['username']));

        $temp = sfContext::getInstance()->getUser()->getGuardUser()->getUserDetail()->toArray();
       
        $userStatus = $doc->createElement('user-status');
        $root->appendChild($userStatus);
        $userStatus->appendChild($doc->createTextNode($temp[0]['user_status']));

        $email = $doc->createElement('email');
        $root->appendChild($email);
        $email->appendChild($doc->createTextNode($temp[0]['email']));
        
        $firstName = $doc->createElement('name');
        $root->appendChild($firstName);
        $firstName->appendChild($doc->createTextNode($temp[0]['name']));
        
        $lastName = $doc->createElement('lname');
        $root->appendChild($lastName);
        $lastName->appendChild($doc->createTextNode($temp[0]['last_name']));

        $kycStatus = $doc->createElement('kyc-status');
        $root->appendChild($kycStatus);
        $kycStatus->appendChild($doc->createTextNode($temp[0]['kyc_status']));

        $ewalletType = $doc->createElement('ewallet-type');
        $root->appendChild($ewalletType);
        $ewalletType->appendChild($doc->createTextNode($temp[0]['ewallet_type']));

        $openIdAuth = $doc->createElement('openid-auth');
        $root->appendChild($openIdAuth);
        $openIdAuth->appendChild($doc->createTextNode($temp[0]['openid_auth']));

        $temp = sfContext::getInstance()->getUser()->getGuardUser()->getSfGuardUserGroup()->toArray();

        $groupId = $doc->createElement('group-id');
        $root->appendChild($groupId);
        $groupId->appendChild($doc->createTextNode($temp[0]['group_id']));

        $xmldata = $doc->saveXML();

//        $xmldata = str_replace('<', '&lt;',$xmldata);
//        $xmldata = str_replace('>', '&gt;',$xmldata);
//
//        echo '<pre>';
//        print_r($xmldata);
//        echo '</pre>';
//        die;

        $browser = $this->getBrowser();

        $header ['Content-Type'] = "application/xml;charset=UTF-8";
        $header ['Accept'] = "application/xml;charset=UTF-8";
        //$url = "http://innovate1pay.local/payForMeHistory/validateUser";
        $url = "http://www.spm1.site/frontend_dev.php/payForMeHistory/validateUser";

        $browser->post($url, $xmldata, $header);

        $respText = $browser->getResponseText ();
        echo $respText;
        die;
        if ($browser->getResponseText()) {
            header("Location: http://innovate1pay.local/welcome/index");
        }
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter', array(
                'SSL_VERIFYPEER' => false,
                'SSL_VERIFYHOST' => false
                    ));
        }
        return $this->browserInstance;
    }

}
?>
