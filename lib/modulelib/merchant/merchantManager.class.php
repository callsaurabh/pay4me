<?php
class merchantManager implements merchantServiceIntf {
//function to get the SP Details
    public function getTotalMerchant($name,$id="") {

        try {
            return $merchant_count = Doctrine::getTable('Merchant')->getTotalMerchant($name,$id);
        }catch(Exception $e ){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function chkCodeDuplicacy($code) {

        try {
            return $merchant_code_count = Doctrine::getTable('Merchant')->chkCodeDuplicacy($code);
        }catch(Exception $e ){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getMerchant() {

        try {
            return $all_records = Doctrine::getTable('Merchant')->getMerchant();
        }catch(Exception $e ) {
            echo("Problem found". $e->getMessage());die;
        }
    }
    public function getMerchantCategoryWise($category_id) {

        try {
            return $all_records = Doctrine::getTable('Merchant')->getMerchantCategoryWise($category_id);
        }catch(Exception $e ) {
            echo("Problem found". $e->getMessage());die;
        }
    }
}
?>
