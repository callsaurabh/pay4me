<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of apiManagerclass
 *
 * @author ryadav
 */
class apiManager {

    //put your code here
    public $error_parameter = array();

    public function renderProperties($request) {
        $postParameters = $request->getPostParameters();


//        $param_array = explode("\n", implode("", $postParameters));
//        $system_id_string = $param_array[0];
//
//        $instanceName_string = $param_array[12];
        try {

            if ($request->hasParameter('bank_code') && !($request->hasParameter('system-id'))) { //for first bank
               $bank_code = $request->getParameter('bank_code');
                return $this->getBankPropertiesV1($bank_code);
            } elseif ($request->hasParameter('system-id') && $request->hasParameter('instance-name')) { //for v2; diamond bank
                $system_id=$request->getParameter('system-id');
                $instanceName=$request->getParameter('instance-name');
                if ($system_id != "" && $instanceName!="") {
                    $bankStatus = Doctrine::getTable('BankMwMapping')->getMwMappingStatus($system_id, $instanceName);
                    
                    //print "<pre>";
                    if ($bankStatus) {
                        //update bank Properties based on new assymetric key (last 16 digits og system_id)
                        $this->updateBankProperties($bankStatus->getFirst());

                        // Karaf Feture Discover XML generation
                        $bankDetails = Doctrine::getTable('Bank')->find($bankStatus->getFirst()->getBankId());

                        //$feature_url = "https://www.pay4me.com/bank/".$bankDetails->getBankCode()."/system-id/".$system_id."/instance-name/".$instanceName."/feature.xml";

                         sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
                         //echo $this->homepageLink = public_path('index.php', true);

                        $feature_url = public_path("/index.php/api/FeatureXMLGeneration/bank/".$bankDetails->getBankCode()."/system-id/".$system_id."/instance-name/".$instanceName."/feature.xml/",true);
                       
                        //$this->FeatureUrlXmlGeneration($bankDetails->getBankCode(),$system_id,$instanceName);
                        // ends here
                        $bankProperties = Doctrine::getTable('BankProperties')->getBankProperties($bankStatus->getFirst()->getBankId());
                        $properties = "";
                        // $encryptingObj = new Encryption();
                        foreach ($bankProperties as $middlewareProperty) {
                            // by Vikash  [WP055] Bug : 36050 (29-10-2012)
                            if ($middlewareProperty->getPropertyName() != "QUEUE_PUBLISHER_NAME" && $middlewareProperty->getPropertyName() != "QUEUE_PUBLISHER_PASSWORD" && $middlewareProperty->getPropertyName() != "TOPIC_PUBLISHER_NAME" && $middlewareProperty->getPropertyName() != "TOPIC_PUBLISHER_PASSWORD") {
                                $properties .= $middlewareProperty->getPropertyName() . "=" . $middlewareProperty->getPropertyValue() . ",";
                            } // else {
                                //$properties .= $middlewareProperty->getPropertyName() . "=" . $middlewareProperty->getPropertyValue() . ",";
//                            }
                        }
                        $properties .= "FEATURE_URL=".$feature_url.','; // Karaf Feature Discovery : feature URL
                        $properties .= "FEATURE_NAME=".ucfirst($bankDetails->getAcronym()).','; // Karaf Feature Discovery : feature URL
                        return $properties;
                    } else {

                        $ob = new BankMwMapping();
                        $ob->setSystemId($system_id);
                        $ob->setInstanceName($instanceName);
                        $ob->setSystemInformation($request->getPostParameter('system_information'));
                        $ob->setStatus(0);

                        try {
                            $ob->save();
                        } catch (Exception $e) {
                            //echo "Request Already sent for this system Id and Instance Configuration " . $request->getParameter('system_id');
                            throw New Exception("Request Already sent for this system Id and Instance Configuration " . $request->getParameter('system_id'));
                        }
//                        } else {
//                            throw New Exception("Request Already sent for this system Id " . $request->getParameter('system_id'));
//                        }
                    }
                } else {
                    throw New Exception("Invalid Request ");
                }
            } else {

                throw New Exception("Bank Code/System Id Required");
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
  
    public function getBankPropertiesV1($bank_code) { //for first bank
        try {
            $bankDetails = Doctrine::getTable('Bank')->findByBankCode($bank_code);
            if ($bankDetails->getFirst()) {
                $bankId = $bankDetails->getFirst()->getId();
                $bankProperties = Doctrine::getTable('BankProperties')->getBankProperties($bankId);
                if ($bankProperties) {
                    $properties = "";

                    foreach ($bankProperties as $middlewareProperty) {
                        $properties .= $middlewareProperty->getPropertyName() . "=" . $middlewareProperty->getPropertyValue() . "\n";
                    }
                    return $properties;
                } else {
                    throw New Exception("Properties value not found for bank code " . $bank_code);
                }
            } else {
                throw New Exception("Bank Code does not exist for bank code " . $bank_code);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function processApi($request) {
        //order processing logic

        $bank_code = $request->getParameter('Bank');
        $bank_code_auth = $request->getHttpHeader('AUTH_USER', 'PHP');
        $bank_key = $request->getHttpHeader('AUTH_PW', 'PHP');
        $xmldata = file_get_contents('php://input');
        pay4MeUtility::setLog($xmldata, 'UserDetailRequest');
        $apiXmlObj = new apiNotificationXml();
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        try {


            $isBankAuthenticate = $this->authenticateBankRequest($bank_code_auth, $bank_key, $bank_code);
            if (!$isBankAuthenticate)
                throw New Exception("AUTHENTICATION_FAILURE");


            $config_dir = sfConfig::get('sf_config_dir');
            $xmlschema = $config_dir . '/xmlschema/api.xsd';
            if (!$xdoc->schemaValidate($xmlschema)) //
                throw New Exception("INVALID_REQUEST_XML");


            $isValidBank = Doctrine::getTable('Bank')->isValidBank($bank_code);
            if (!$isValidBank) {
                throw New Exception("INVALID_BANK"); // throw new Exception ('Invalide Bank');
            }

            $bankDetails = Doctrine::getTable('Bank')->findByBankCode($bank_code);
            $bankId = $bankDetails->getFirst()->getId();

            $requestArr = $apiXmlObj->parseTransRequestXml($xdoc);



            $validateUser = $this->validateUserType($requestArr['userType']);
            if (!$validateUser) {
                throw New Exception("INVALID_USERTYPE"); // throw new Exception
            }

            //validate the branch code being sent in request XML
            if (array_key_exists("branchCode", $requestArr)) {
                if ($requestArr['userType'] == "teller" || $requestArr['userType'] == "bank_branch_report_user") {

                    $validateBranchCode = $this->validateBankBranchCode($bankId, $requestArr['branchCode']);
                    if (!$validateBranchCode) {
                        throw New Exception("INVALID_BANK_BRANCH_CODE"); // throw new Exception
                    }
                } else {
                    $requestArr['branchCode'] = "";
                }
            }

            if ($requestArr['userType'] == "teller") {
                $requestArr['userType'] = "bank_user";
            }

            $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($requestArr['userType']);
            $groupId = $sfGroupDetails->getFirst()->getId();

            $rexponseXml = $apiXmlObj->generateApiResponseXml($requestArr, $groupId, $bankId);
            $apiXmlObj->setResponseHeader($bank_code);

            $xdoc->LoadXML($rexponseXml);
            pay4MeUtility::setLog($rexponseXml, 'UserDetailResponse');


            if (!$xdoc->schemaValidate($xmlschema)) // is a Valid P4M XML
                throw New Exception("INVALID_RESPONSE_XML");



            return $rexponseXml;
        } catch (Exception $ex) {

            $msg = $ex->getMessage();
            $logger = sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: " . $msg);
            $errorXml = $apiXmlObj->sendErrorXML($msg);
            $apiXmlObj->setResponseHeader($bank_code);
            $this->createLog($errorXml, "errorLog");
            return $errorXml;
        }
    }

    private function validateBankBranchCode($bankId, $bankBranchCode) {
        $isCodeValid = Doctrine::getTable('BankBranch')->validateBranchCode($bankId, $bankBranchCode);
        if ($isCodeValid)
            return true;
        return false;
    }

    private function validateUserType($user_type) {


        $arrValidUser = array();
        //$arrValidUser =array('bank_admin','teller','report_admin','report_bank_admin','bank_e_auditor','bank_branch_report_user','bank_country_head','country_report_user');
        // Report admin should not see the data
        // bug ID 31652
        $arrValidUser = array('bank_admin', 'teller', 'report_bank_admin', 'bank_e_auditor', 'bank_branch_report_user', 'bank_country_head', 'country_report_user');
        if (in_array($user_type, $arrValidUser)) {
            return true;
        }
        return false;
    }

    public function authenticateBankRequest($request_bank_code, $bank_key, $bank_code) {

        $logger = sfContext::getInstance()->getLogger();
        $logger->debug("OBJ: $bank_key " . print_r($bank_code, true));

        if ($request_bank_code != $bank_code) {

            return false;
        }

        $bank_details = Doctrine::getTable('Bank')->getBankDetailByBankCode($bank_code);

        // $logger->debug("OBJ: ".print_r($bank_details,true));

        if ($bank_details) {
            $bankObj = $bank_details->getFirst();
            if (($bankObj->getBankCode() == $bank_code) && ($bankObj->getBankKey() == $bank_key)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function createLog($xmldata, $nameFormate, $exceptionMessage="") {

        //log xml
        $pay4meLog = new pay4meLog();
        if ($exceptionMessage != "") {
            $xmldata = $xmldata . "\n" . $exceptionMessage;
        }
        $pay4meLog->createLogData($xmldata, $nameFormate);
    }

    public function processMerchantApi($request) {
        //api processing logic
        sfContext::getInstance()->getResponse()->setContentType('text/xml');
        $xmldata = file_get_contents('php://input');
        $bankUriCode = $request->getParameter('Bank');
        $apiXmlObj = new apiNotificationXml();
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        pay4MeUtility::setLog($xmldata, 'merchantDetailRequest');

        $bank_code_auth = $request->getHttpHeader('AUTH_USER', 'PHP');
        $bank_key = $request->getHttpHeader('AUTH_PW', 'PHP');
        try {
            sfContext::getInstance()->getResponse()->setContentType('text/xml');


            $bankMeObj = bankServiceFactory::getService('payforme');

            $isBankAuthenticate = $this->authenticateBankRequest($bank_code_auth, $bank_key, $bankUriCode);
            if (!$isBankAuthenticate)
                throw New Exception("AUTHENTICATION_FAILURE");

            $config_dir = sfConfig::get('sf_config_dir');
            $xmlschema = $config_dir . '/xmlschema/merchantRequest.xsd';
            if (!$xdoc->schemaValidate($xmlschema)) // is a Valid P4M XML //isValidMerchantServiceId
                throw New Exception("INVALID_REQUEST_XML");

            if (!BANKTable::isValidBank($bankUriCode))
                throw New Exception("INVALID_BANK");

            $bankCode = $xdoc->getElementsByTagName('merchant-detail-request')->item(0)->getElementsByTagName('bank-code')->item(0)->nodeValue;
            if (!BANKTable::isValidBank($bankCode))
                throw New Exception("INVALID_BANK");

            $redirectXML = $apiXmlObj->generateMerchantResponseXml($bankCode);
            $xdoc->LoadXML($redirectXML);
            pay4MeUtility::setLog($redirectXML, 'merchantDetailResponse');


            if (!$xdoc->schemaValidate($xmlschema)) // is a Valid P4M XML
                throw New Exception("INVALID_RESPONSE_XML");

            $apiXmlObj->setResponseHeader($bankCode);

            ##$this->logMessage("logging")  ;



            return $redirectXML;
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
            $logger = sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: " . $msg);
            $errorXml = $apiXmlObj->sendErrorXML($msg, 'merchantRequest.xsd');
            $apiXmlObj->setResponseHeader($bankUriCode);
            $this->createLog($errorXml, "errorLog");
            return $errorXml;
        }
    }

    public function processQueryApi($request) {
        //api processing logic
        sfContext::getInstance()->getResponse()->setContentType('text/xml');
        $xmldata = file_get_contents('php://input');
        $apiXmlObj = new apiNotificationXml();
        $bankCode = $request->getParameter('Bank');
        $bankCodeAuth = $request->getHttpHeader('AUTH_USER', 'PHP');
        $bankKey = $request->getHttpHeader('AUTH_PW', 'PHP');
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmldata);
        pay4MeUtility::setLog($xmldata, 'transactionRequest');

        try {

            sfContext::getInstance()->getResponse()->setContentType('text/xml');
            $config_dir = sfConfig::get('sf_config_dir');
            $xmlschema = $config_dir . '/xmlschema/transactionRequest.xsd';
            if (!$xdoc->schemaValidate($xmlschema)) // valid request xml
                throw New Exception("INVALID_REQUEST_XML");

            $isBankAuthenticate = $this->authenticateBankRequest($bankCodeAuth, $bankKey, $bankCode);
            if (!$isBankAuthenticate)
                throw New Exception("AUTHENTICATION_FAILURE");

            if (!BANKTable::isValidBank($bankCode))
                throw New Exception("INVALID_BANK");

            $startDate = strtotime($xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('start-date')->item(0)->nodeValue);
            $endDate = strtotime($xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('end-date')->item(0)->nodeValue);
            $currentDate = strtotime(date('Y-m-d'));

            if ($startDate > $currentDate)
                throw New Exception("INVALID_START_DATE");

            if ($endDate > $currentDate)
                throw New Exception("INVALID_END_DATE");

            if ($startDate > $endDate)
                throw New Exception("INVALID_DATE");

            $branchCode = "";
            if (isset($xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('branch-code')->item(0)->nodeValue)) {

                $branchCode = $xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('branch-code')->item(0)->nodeValue;
                if (!Doctrine::getTable('BankBranch')->branchCodeExist($branchCode))
                    throw New Exception("INVALID_BANK_BRANCH_CODE");
            }
            if (isset($xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('user-id')->item(0)->nodeValue)) {

                $tellerName = $xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('user-id')->item(0)->nodeValue;
                if (!BANKUSERTable::isValidBankUser($tellerName, $branchCode))
                    throw New Exception("INVALID_BANK_USER");
            }

            if (isset($xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('bank')->item(0)->nodeValue)) {

                $bankName = $xdoc->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('bank')->item(0)->nodeValue;
                if (!BANKTable::isValidBank($bankCode, $bankName))
                    throw New Exception("INVALID_BANK");
            }



            $redirectXML = $apiXmlObj->generateTransactionResponseXml($xdoc, $bankCode);
            $apiXmlObj->setResponseHeader($bankCode);

            $xdoc->LoadXML($redirectXML);
            pay4MeUtility::setLog($redirectXML, 'transactionDetailResponse');

            if (!$xdoc->schemaValidate($xmlschema)) // is valid response xml
                throw New Exception("INVALID_RESPONSE_XML");

            return $redirectXML;
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
            $logger = sfContext::getInstance()->getLogger();
            $logger->info("Exception in processing: " . $msg);
            $errorXml = $apiXmlObj->sendErrorXML($msg, 'transactionRequest.xsd');
            $apiXmlObj->setResponseHeader($bankCode);
            pay4MeUtility::setLog($errorXml, 'errorLog');
            return $errorXml;
        }
    }

    private function updateBankProperties($bankMwMappingObj) {
        $encryption = new Encryption();
        $queue_publisher_password = '';
        $queue_consumer_password = '';
        $topic_publisher_password = '';
        $bankId = $bankMwMappingObj->getBankId();
        $system_id_key = substr($bankMwMappingObj->getSystemId(), -16);
        $properties = Doctrine::getTable('BankProperties')->findBy('BankId', $bankId);
        foreach ($properties as $nameValuePair) {

            switch ($nameValuePair->getPropertyName()) {
                case "QUEUE_PUBLISHER_NAME":
                    $bankCredential_record = Doctrine::getTable('BankCredential')->getPassword($bankId, $nameValuePair->getPropertyValue())->getFirst();
                    $queue_publisher_password = $encryption->generateEncyrypt($bankCredential_record->getPassword(), $system_id_key);
                   // echo $queue_publisher_password;
                    break;
                case "QUEUE_CONSUMER_NAME":
                    $bankCredential_record = Doctrine::getTable('BankCredential')->getPassword($bankId, $nameValuePair->getPropertyValue())->getFirst();
                    $queue_consumer_password = $encryption->generateEncyrypt($bankCredential_record->getPassword(), $system_id_key);
                    //echo $queue_consumer_password;
                    break;
                case "TOPIC_PUBLISHER_NAME":
                    $bankCredential_record = Doctrine::getTable('BankCredential')->getPassword($bankId, $nameValuePair->getPropertyValue())->getFirst();
                    $topic_publisher_password = $encryption->generateEncyrypt($bankCredential_record->getPassword(), $system_id_key);
                    //echo $topic_publisher_password;
                    break;
                case "QUEUE_PUBLISHER_PASSWORD":
                    $qPublisherPassOb = $nameValuePair;
                    break;
                case "QUEUE_CONSUMER_PASSWORD":
                    $qConsumerPassOb = $nameValuePair;
                    break;
                case "TOPIC_PUBLISHER_PASSWORD":
                    $topicPublisherPassOb = $nameValuePair;
                    break;
            }
        }
        $qPublisherPassOb->setPropertyValue($queue_publisher_password);
        $qPublisherPassOb->save();
        $qConsumerPassOb->setPropertyValue($queue_consumer_password);
        $qConsumerPassOb->save();

        $topicPublisherPassOb->setPropertyValue($topic_publisher_password);
        $topicPublisherPassOb->save();
    }

}

