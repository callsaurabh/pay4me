<?php
class splitEntityConfManager implements splitEntityConfService {
//function to get the SP Details
  public function getAllRecords() {

    try {
      return $all_records = Doctrine::getTable('SplitEntityConfiguration')->getAllRecords();
    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getSplitEntityConfigurationList() {
    try {
      $split_entity_configuration = Doctrine::getTable('SplitEntityConfiguration')->createQuery('a')->orderBy('split_type_id')->execute();
      $split_entity_configuration_list = array();
      $split_entity_configuration_list[''] = "Please select split_entity_configuration";
      foreach ($split_entity_configuration as $i => $split_entity_configuration_detail):
        $split_entity_configuration_list[$split_entity_configuration_detail->getId()] =  $split_entity_configuration_detail->getId();
      endforeach;
      return $split_entity_configuration_list;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }


}
?>
