<?php
interface lgaService
{
  
  public function getLgaList($state_id="",$lga_id="");
  public function getAllRecords();
  public function getLgaRelatedStates($bankRelatedLgaArray);
  public function getLgaInfo($lga_id);


  
}

?>
