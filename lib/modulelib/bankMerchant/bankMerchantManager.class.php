<?php

class bankMerchantManager implements bankMerchantService {

//function to get the SP Details
    public function getAllRecords() {

        try {
            return $all_records = Doctrine::getTable('BankMerchant')->getAllRecords();
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function findAllReleatedBanks($merchantId,$currency='') {

        try {
            return $all_related_banks = Doctrine::getTable('BankMerchant')->findAllReleatedBanks($merchantId,$currency);
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }
    // Function added by Vikash [WP058](04-09-2012) bug:35802
    public function findAllReleatedBanksBankCheque($merchantId,$currency='') {

        try {
            return $all_related_banks = Doctrine::getTable('BankMerchant')->findAllReleatedBanksBankCheque($merchantId,$currency);
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    
    public function findAllReleatedBanksForCheckPayment($merchantId, $currency='') {

        try {
            return $all_related_banks = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForCheckPayment($merchantId, $currency);
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

    public function findAllReleatedBanksForDraftPayment($merchantId, $currency='') {

        try {
            return $all_related_banks = Doctrine::getTable('BankMerchant')->findAllReleatedBanksForDraftPayment($merchantId, $currency);
        } catch (Exception $e) {
            echo("Problem found" . $e->getMessage());
            die;
        }
    }

}

?>
