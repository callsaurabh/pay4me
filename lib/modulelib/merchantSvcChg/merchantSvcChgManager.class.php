<?php
class merchantSvcChgManager implements merchantSvcChgService
{
    //function to get all merchant service charges
    public function getTotalMerchant()
    {
      try
      {
          return $merchant_count = Doctrine::getTable('MerchantServiceCharges')->getTotalMerchantChgs();
      }catch(Exception $e ){
          echo("Problem found". $e->getMessage());die;
      }
    }

    //function to get the merchant service charges according to the merchant service id
    public function getMerchantChg($merchantId)
    {
      try
      {
        return $merchant_chg = Doctrine::getTable('MerchantServiceCharges')->getCharges($merchantId);
      }catch(Exception $e ){
          echo("Problem found". $e->getMessage());die;
      }
    }
}
?>
