<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of stompManagerclass
 *
 * @author spandey
 */
require_once(dirname(__FILE__).'/../Stomp/Stomp.php');

//require_once("../Stomp.php");
class stompManager {

    //put your code here
    public function sendMessage($queue,$message,$bankId,$message_properties){
//        echo "<pre>";
//        print_r($message_properties);
//        exit;
        try
        {
            $userType = 'admin';
            $credential = $this->getCredentail($queue, $bankId, $userType);
            //base64_decode($credential['password']
           // if(extension_loaded('stomp')) {
                if(count($credential)>0) {
                    try {
                        $brokerUrl = Settings::getBrokerUrl();
                        $con = new Stomp($brokerUrl,$credential['user_name'],$credential['password']);
                        //$con = new Stomp($brokerUrl);
                        //$con->connect($credential['user_name'],$credential['password']);
                    }
                    catch (Exception $e) {
                        throw $e;
                    }
                }
                else {
                    throw new Exception("Either Broker URI is wrong or Queue credentials are invalid");
                }

          /*  }
            else {
                throw new Exception("Extension Stomp is not available");
            }*/

            try {
              //  $loggerLevel = $credential['Bank']['bi_logger_level'];
              $response = $con->send('/queue/'.strtoupper($queue), $message,$message_properties);
            }
            catch (Exception $e) {
                throw $e;
            }

            unset($con);

            if ($response)
            return $response;
            else
            return false;
        }
        catch(Exception $e)
        {
            //any exception caught here inplies the message wasn't posted to Queue
            //need to do the following -
            //1. add job which will invoke posting of this message to queue
            /*2. send a mail to support informing that either the Stomp extension
             has been disabled or the activeMQ service is */

             throw new Exception("Connection failed:" . $e->getMessage());

        }


    }
    public function sendMessage_old($queue,$message,$bankId,$message_properties){
        try
        {
            $userType = 'admin';
            $credential = $this->getCredentail($queue, $bankId, $userType);
            //base64_decode($credential['password']
           // if(extension_loaded('stomp')) {
                if(count($credential)>0) {
                    try {
                        $brokerUrl = Settings::getBrokerUrl();
                        //$stomp = new Stomp($brokerUrl,$credential['user_name'],$credential['password']);
                        $con = new Stomp($brokerUrl);
                        $con->connect($credential['user_name'],$credential['password']);
                    }
                    catch (Exception $e) {
                        throw $e;
                    }
                }
                else {
                    throw new Exception("Either Broker URI is wrong or Queue credentials are invalid");
                }

          /*  }
            else {
                throw new Exception("Extension Stomp is not available");
            }*/

            try {
                $loggerLevel = $credential['Bank']['bi_logger_level'];
                $response = $con->send('/queue/'.strtoupper($queue), $message, array('persistent' => 'true',"logger_level"=>$loggerLevel));
            }
            catch (Exception $e) {
                throw $e;
            }

            unset($con);

            if ($response)
            return $response;
            else
            return false;
        }
        catch(Exception $e)
        {
            //any exception caught here inplies the message wasn't posted to Queue
            //need to do the following -
            //1. add job which will invoke posting of this message to queue
            /*2. send a mail to support informing that either the Stomp extension
             has been disabled or the activeMQ service is */

             throw new Exception("Connection failed:" . $e->getMessage());

        }


    }

    private function getCredentail($bankAcroynm, $bankId, $userType) {
        $query = Doctrine_Query::create()
        ->select("c.password,c.user_name,b.bi_logger_level")
        ->from('BankCredential c')
        ->leftJoin('c.Bank b')
        ->where('c.bank_id = ?', $bankId);
        if(!empty($userType))
        $query->andWhere('c.user_group = ?', $userType);

        $res = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
        if(count($res)>0)
        return  $res[0];
        else
        return  false;

    }


}
?>