<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bankIntegrationclass
 *
 * @author spandey
 */
class bankIntegration {

    //public $pfmTransactionDetails;
    public $txnId;
    public $biVersionObj;

//    public static $TRANSACTION_TYPE_PAYMENT = "payment";
//    public static $TRANSACTION_TYPE_RECHARGE = "recharge";

    public function __construct($txnId="", $collection_account_id="", $currencyId="", $amount_credit="") {
        if ($txnId != "") {
            try {


                $this->txnId = $txnId;
                if ($collection_account_id == "") {

                    $biProtocolVersion = Doctrine::getTable('Transaction')->findBy('pfm_transaction_number', $txnId)->getFirst()->getMerchantRequest()->getBank()->getBiProtocolVersion();
                } else {
                    $user_id = Doctrine::getTable("EpMasterLedger")->findBy("payment_transaction_number", $this->txnId)->getFirst()->getUpdatedBy();
                    $biProtocolVersion = Doctrine::getTable('BankUser')->findBy('user_id', $user_id)->getFirst()->getBank()->getBiProtocolVersion();
                }
                $versionManager = "BankIntegration" . $biProtocolVersion . "Manager";
                $versionManagerObj = new $versionManager($this->txnId, $collection_account_id, $currencyId, $amount_credit);

                $version_Obj = $versionManagerObj->addTransactionRequest();
                $this->biVersionObj = $version_Obj;
            } catch (Exception $ex) {
                die("Check Error " . $ex->getMessage());
            }
//        $this->txnId = $txnId;
            $this->pfmTransactionDetails = $this->getDetail();
        }
    }

    public function __construct1($txnId="") {

        if ($txnId) {
            $this->txnId = $txnId;
            $this->pfmTransactionDetails = $this->getDetail();
        }
    }

    public function saveMessageRequest() {
        $pfmTransactionDetails = $this->pfmTransactionDetails;

        $merchantId = $pfmTransactionDetails['MerchantRequest']['Merchant']['id'];
        $bankId = $pfmTransactionDetails['MerchantRequest']['Bank']['id'];
        $currencyId = $pfmTransactionDetails['MerchantRequest']['currency_id'];
        $updatedAt = $pfmTransactionDetails['MerchantRequest']['updated_at'];
        $amount = $pfmTransactionDetails['MerchantRequest']['paid_amount'];
        $totalAmount = $pfmTransactionDetails['MerchantRequest']['paid_amount'];
        $paymentModeOptionId = $pfmTransactionDetails['MerchantRequest']['payment_mode_option_id'];
        $epmasterObj = $this->getAccountNo($paymentModeOptionId, $bankId, $merchantId, $currencyId);
        $accountNo = $epmasterObj->getFirst()->getAccountNumber();
        $accountId = $epmasterObj->getFirst()->getId();
        $itemName = $pfmTransactionDetails['item_name'];
        $validationNo = $pfmTransactionDetails['MerchantRequest']['validation_number'];

        $tellerId = sfContext::getInstance()->getUser()->getGuardUser()->getId();

        $messageArray = Array('merchant_request_id' => $pfmTransactionDetails['merchant_request_id'],
            'merchant_id' => $merchantId,
            'account_id' => $accountId,
            'bank_id' => $bankId,
            'teller_id' => $tellerId,
            'currency_id' => $currencyId,
            'amount' => $amount * 100, //amount converted in cobo
            'total_amount' => $totalAmount * 100, //amount converted in cobo
            'transaction_number' => $pfmTransactionDetails['pfm_transaction_number'],
            'validation_number' => $validationNo,
            'account_number' => $accountNo,
            'item_name' => $itemName,
            'type' => 'payment',
            'payment_type' => 'credit',
            'transaction_date' => $updatedAt,
        );
        return $this->saveMessage($messageArray);
    }

    public function saveMessage($messageArray) {

        $messageQueueObj = new MessageQueueRequest();
        if (array_key_exists('merchant_request_id', $messageArray))
            $messageQueueObj->setMerchantRequestId($messageArray['merchant_request_id']);

        $messageQueueObj->setMerchantId($messageArray['merchant_id']);

        $messageQueueObj->setAccountId($messageArray['account_id']);
        $messageQueueObj->setBankId($messageArray['bank_id']);
        $messageQueueObj->setTellerId($messageArray['teller_id']);
        $messageQueueObj->setCurrencyId($messageArray['currency_id']);
        $messageQueueObj->setAmount($messageArray['amount']);
        $messageQueueObj->setTotalAmount($messageArray['total_amount']);

        if (array_key_exists('transaction_number', $messageArray))
            $messageQueueObj->setTransactionNumber($messageArray['transaction_number']);

        $messageQueueObj->setValidationNumber($messageArray['validation_number']);

        $messageQueueObj->setAccountNumber($messageArray['account_number']);
        $messageQueueObj->setItemName($messageArray['item_name']);
        $messageQueueObj->setType($messageArray['type']);
        $messageQueueObj->setPaymentType($messageArray['payment_type']);
        $messageQueueObj->setTransactionDate($messageArray['transaction_date']);

        $messageQueueObj->save();
        return $messageQueueObj->getId();
    }

    private function getDetail() {
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getNotificationTransactionDetails($this->txnId);
        return $pfmTransactionDetails;
    }

    public function getJsonMessage($messageRequestId) {
//        echo "<pre>";
//        print_r($messageRequestId);
//        exit;
        return Doctrine::getTable('MwRequest')->find($messageRequestId)->getMessageText();




        //put your code here
    }

    public function generateQueueMessage($messageRequestId, $type) {
        $messageRequestObj = Doctrine::getTable('MwRequest')->getMessageData($messageRequestId);
        //get all the details


        if ($messageRequestObj) {

            $messageRequest = $messageRequestObj->getFirst();
            $itemFee = $messageRequest->getAmount($messageRequest->getMessageText());
            $itemName = $messageRequest->getItemName($messageRequest->getMessageText());
            $merchantServiceId = $messageRequest->getPay4meTransactionNumber($messageRequest->getMessageText());
            $totalamount = $messageRequest->getTotalAmount($messageRequest->getMessageText());
            $updatedAt = $messageRequest->getTransactionDate($messageRequest->getMessageText());

            $bankName = $messageRequest->getBank($messageRequest->getMessageText());
            $branchName = $messageRequest->getBranchName($messageRequest->getMessageText());
            $branchCode = $messageRequest->getBranchCode($messageRequest->getMessageText());
            $merchantName = $messageRequest->getMerchantName($messageRequest->getMessageText());
            $currencyCode = $messageRequest->getCurrencyCode($messageRequest->getMessageText());

            $accountNo = $messageRequest->getAccountNumber($messageRequest->getMessageText());

            $merchantCode = $messageRequest->getMerchantCode($messageRequest->getMessageText());
            $bankteller = $messageRequest->getTeller($messageRequest->getMessageText());
            //    $type = $messageRequest->getType();
            if ($type == 'recharge') {
                $transactionNumber = $messageRequest->getPay4meTransactionNumber($messageRequest->getMessageText());
            }
            else
                $transactionNumber = $messageRequest->getPay4meTransactionNumber($messageRequest->getMessageText());

            $bankteller = $messageRequest->getTeller($messageRequest->getMessageText());

            $messageArray = Array('merchant-code' => $merchantCode,
                'merchant-name' => $merchantName,
                'item-name' => $itemName,
                'pay4me-transaction-number' => $transactionNumber,
                'transaction-date' => $updatedAt,
                'total-amount' => $totalamount,
                'currency-code' => $currencyCode,
                'payment-type' => 'credit',
                'account-number' => $accountNo,
                'amount' => $itemFee,
                'bank' => $bankName,
                'branch' => $branchName,
                'branch-code' => $branchCode,
                'teller' => $bankteller
            );

            $message = json_encode($messageArray);

            return $message;
        }
        else {
            return false;
        }



        //put your code here
    }

//    public function updateMessageRequest($id, $message, $proceed, $flagReAttemptCount='') {
//        //   $mesgReqObj=Doctrine::getTable('MessageQueueRequest')->find($id);
//        //    $noofAttempt = $mesgReqObj->getNoOfAttempt()+1;
//
//        $mesgReqObj = Doctrine::getTable('MessageQueueRequest')->updateMessageRequest($id, $message, $proceed, $flagReAttemptCount);
//        if ($mesgReqObj)
//            return true;
//        else
//            return false;
//    }

    public function updateMwRequestDetails($mwReqOb, $checkReAttempt) {
        $conn = Doctrine::getTable(get_class($mwReqOb))->getConnection();
        $conn->beginTransaction();
//        if ($checkReAttempt != 0) {
//            $newMwReqObj = $mwReqOb->addNewRequest();
//        }
//        
        if ($mwReqOb->getMessageType() == "transaction") {

            $mesgReqObj = Doctrine::getTable('TransactionBankPosting')->updateMessageRequest($mwReqOb->getId(),$mwReqOb->getTransactionBankPostingId(), $mwReqOb->getTransactionBankPosting()->getNoOfAttempts(), $checkReAttempt);
        }
        
        $conn->commit();

        
    }

    private function getAccountNo($paymentModeOptionId, $bankId, $merchantId, $currencyId) {
        $accountInfoObj = Doctrine::getTable('ServiceBankConfiguration')->getMasterAcount($paymentModeOptionId, $bankId, $merchantId, 'Payment', $currencyId);
//        $masterAccountId = $accountInfoObj->getFirst()->getMasterAccountId();
        $masterAccountId = 3;
        $epmasterObj = Doctrine::getTable('EpMasterAccount')->getAccountDetailsById($masterAccountId);
        if (count($epmasterObj)) {
            return $epmasterObj;
        } else {
            return false;
        }
    }

//    public function QueueProcessing_old($messagerequestId, $flagReAttemptCount='') {
//        try {
//
//
//            $message = "";
//            $processId = 0;
//            // Generate json message for queue
//            if ($messagerequestId) {
//
//                $message = $this->generateQueueMessage($messagerequestId);
//                if ($message) {
//                    $pay4meLog = new pay4meLog();
//                    $pay4meLog->createLogData("Json Message to Queue - " . $message, 'Jsonmessage', 'MessageQueueLog');
//                    // send to message queue
//                    $objQueue = Doctrine::getTable('MessageQueueRequest')->find($messagerequestId);
//                    $bankId = $objQueue->getBankId();
//
//                    // get Queue name from bank properties
//                    //$objBank= Doctrine::getTable('Bank')->find($bankId);
//
//                    $bankPropertyObj = Doctrine::getTable('BankProperties')->getBankQueueName($bankId);
//                    if (!$bankPropertyObj)
//                        throw new Exception(' Queue name is not defined in bank properties');
//                    $queueName = $bankPropertyObj->getFirst()->getPropertyValue();
//
//                    // $bankAcronym = $objBank->getAcronym();
//
//                    $stopmObj = new stompManager();
//                    $stopmObj->sendMessage($queueName, $message, $bankId);
//                    $processId = 1;
//                }
//                $this->updateMessageRequest($messagerequestId, $message, $processId, $flagReAttemptCount);
//            }
//        } catch (Exception $e) {
//
//            $this->updateMessageRequest($messagerequestId, $message, $processId, $flagReAttemptCount);
//            throw $e;
//        }
//
//        return $processId;
//    }

    public function QueueProcessing($version_obj, $flagReAttemptCount='') {
        try {
            $con = Doctrine_Manager::connection()->setAttribute(Doctrine_Core::ATTR_AUTO_FREE_QUERY_OBJECTS, true);
//$insert_new_request_in_mwRequest=false;
            $message = "";
            $processId = 0;
            $reAttempt = 0;
            if (!is_string($version_obj)) {
                $messagerequestId = $version_obj->getRequestObject()->getId();
            } else {
                $reAttempt = 1;
                $messagerequestId = (Integer) $version_obj;
            }
            //vikash [WP055] Bug:36077 (08-11-2012)
            if(!$flagReAttemptCount)
                $reAttempt = 0;
            
            // Generate json message for queue
            if ($messagerequestId) {
                
                $message = $this->getJsonMessage($messagerequestId);
                if ($message) {
                    $con->beginTransaction();
                    $mwRequestObj = Doctrine::getTable('MwRequest')->find($messagerequestId);


                    $pay4meLog = new pay4meLog();
                    $pay4meLog->createLogData("Json Message to Queue - " . $message, 'Jsonmessage', 'MessageQueueLog');
                    // send to message queue
                    $transaction_number = '';
                    //$bankId = $this->getBankIdFromMwRequestObj($mwRequestObj, $message);
                    $bankObj = $mwRequestObj->getTransactionBankPosting()->getBank();
                    $bankId = $bankObj->getId();
                    // get Queue name from bank properties

                    $bi_protocol_version = $bankObj->getBiProtocolVersion();
                    $bi_protocol_version=strtoupper($bi_protocol_version);
                    $className = "BankIntegration" . $bi_protocol_version . "Manager";
                    $version_obj = new $className($transaction_number);
                    //$MWREqOj = $this->updateMwRequestDetails($mwRequestObj, $reAttempt);

                    if ($reAttempt != 0) {
                        $newMwReqObj = $mwRequestObj->addNewRequest();
                        $version_obj->setMessageProperties($newMwReqObj);
                    }else{
                        $version_obj->setMessageProperties($mwRequestObj);
                    }

                    $queueName = Doctrine::getTable('BankProperties')->getBankQueueName($bankId);

                    if (!$queueName) {
                        throw new Exception(' Queue name is not defined in bank properties');
                    }
                    $con->commit();

                    $stopmObj = new stompManager();
                    $stopmObj->sendMessage($queueName, $message, $bankId, $version_obj->getMessageProperties());

                    if ($reAttempt != 0) {
                        $this->updateMwRequestDetails($newMwReqObj, $reAttempt);
                        //$newMwReqObj->free();
                    }else{
                        $this->updateMwRequestDetails($mwRequestObj, $reAttempt);
                        //$mwRequestObj->free(true);
                    }
                    
                    $processId = 1;
                }
            }
        } catch (Exception $e) {
            // $this->updateMwRequestDetails($mwRequestObj, $reAttempt);
            throw $e;
        }

        return $processId;
    }

    private function getBankIdFromMwRequestObj($mwRequestObj, $message) {
        if (!is_null($mwRequestObj->getTransactionBankPostingId())) {
            if (!is_null($mwRequestObj->getTransactionBankPosting()->getMerchantRequestId())) {
                $transaction_number = $mwRequestObj->getTransactionBankPosting()->getMessageTxnNo();
                $bankId = $mwRequestObj->getTransactionBankPosting()->getMerchantRequest()->getBankId();
            } else {
                $decoded_msg = json_decode($message, true);
                $transaction_number = $decoded_msg['pay4me-transaction-number'];
//                        $bankName = $decoded_msg['bank'];
                $bankId = Doctrine::getTable('Bank')->findBy("bank_name", $decoded_msg['bank'])->getFirst()->getId();
            }
        } else {
            $bankId = $mwRequestObj->getBankMwMapping()->getBankId();
        }

        return $bankId;
    }

    public function getRechargeArray($validationNo, $collectionAccountId, $currencyId, $amount) {
        try {
            $merchantName = Settings::getPayForMeMerchant();
            $merchantObj = Doctrine::getTable('Merchant')->findByName($merchantName);
            $epmasterObj = Doctrine::getTable('EpmasterAccount')->getAccountDetailsById($collectionAccountId);

            $merchantId = $merchantObj->getFirst()->getId();
            $gUser = sfContext::getInstance()->getUser()->getGuardUser();
            $bankId = $gUser->getBankUser()->getFirst()->getBankId();

            $transactionDate = date('Y-m-d H:i:s');

            $totalAmount = $amount;

            $accountNo = $epmasterObj->getFirst()->getAccountNumber();
            $accountId = $collectionAccountId;
            $itemName = 'recharge';

            $tellerId = $gUser->getId();

            $messageArray = Array(
                'merchant_id' => $merchantId,
                'account_id' => $accountId,
                'bank_id' => $bankId,
                'teller_id' => $tellerId,
                'currency_id' => $currencyId,
                'amount' => $amount,
                'total_amount' => $totalAmount,
                'validation_number' => $validationNo,
                'account_number' => $accountNo,
                'item_name' => $itemName,
                'type' => 'recharge',
                'payment_type' => 'credit',
                'transaction_date' => $transactionDate,
            );

            return $messageArray;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /*
     * fucntion : generateBankTransactionXML()
     * purpose : generate request xml
     * wp052
     * Date:22-12-2011
     */

    public function generateBankTransactionXML($id, $type='') {
        $jsonMessageRequest = $this->generateQueueMessage($id, $type);
        $arrMessage = json_decode($jsonMessageRequest);
        if (!empty($arrMessage)) {
            //variable assignment
            $objAPI = new APIHelper();
            $transactionDate = $objAPI->convertDbDateToXsdDate($arrMessage->{'transaction-date'});
            $paymentType = 'cr';
            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;
            $root = $doc->createElement('transaction-request');
            $root = $doc->appendChild($root);
            $merchant_code = $doc->createElement("merchant-code");
            $merchant_code->appendChild($doc->createTextNode($arrMessage->{'merchant-code'}));
            $root->appendChild($merchant_code);
            $item = $doc->createElement("item");
            $root->appendChild($item);
            $item_name = $doc->createElement("item-name");
            $item_name->appendChild($doc->createTextNode($arrMessage->{'item-name'}));
            $item->appendChild($item_name);
            $pay4me_transaction_number = $doc->createElement("pay4me-transaction-number");
            $pay4me_transaction_number->appendChild($doc->createTextNode($arrMessage->{'pay4me-transaction-number'}));
            $item->appendChild($pay4me_transaction_number);
            $transaction_date = $doc->createElement("transaction-date");
            $transaction_date->appendChild($doc->createTextNode($transactionDate));
            $item->appendChild($transaction_date);
            $total_amount = $doc->createElement("total-amount");
            $total_amount->setAttribute("currency-code", $arrMessage->{'currency-code'});
            $total_amount->appendChild($doc->createTextNode($arrMessage->{'total-amount'}));
            $item->appendChild($total_amount);
            $payment_type = $doc->createElement("payment");
            $payment_type->setAttribute("type", $paymentType);
            $item->appendChild($payment_type);
            $account_number = $doc->createElement("account-number");
            $account_number->appendChild($doc->createTextNode($arrMessage->{'account-number'}));
            $payment_type->appendChild($account_number);
            $amount = $doc->createElement("amount");
            $amount->appendChild($doc->createTextNode($arrMessage->{'amount'}));
            $payment_type->appendChild($amount);
            $collection_details = $doc->createElement("collection-details");
            $root->appendChild($collection_details);
            $bank = $doc->createElement("bank");
            $bank->appendChild($doc->createTextNode($arrMessage->{'bank'}));
            $collection_details->appendChild($bank);
            $branch = $doc->createElement("branch");
            $branch->appendChild($doc->createTextNode($arrMessage->{'branch'}));
            $collection_details->appendChild($branch);
            $branch_code = $doc->createElement("branch-code");
            $branch_code->appendChild($doc->createTextNode($arrMessage->{'branch-code'}));
            $collection_details->appendChild($branch_code);
            $teller = $doc->createElement("teller");
            $teller->appendChild($doc->createTextNode($arrMessage->{'teller'}));
            $collection_details->appendChild($teller);
            $xmldata = $doc->saveXML();
            return $xmldata;
        } else {
            return false;
        }
    }

}

?>
