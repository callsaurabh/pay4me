<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class BankIntegrationV2Manager extends parentBiVersionManager {

    public function __construct($transactionNumber='', $collection_account_id="", $currencyId="", $amount_credit="") {

        parent::__construct($transactionNumber, $collection_account_id, $currencyId, $amount_credit);
    }

    public function setMessageProperties($mwRequestObject) {
        $this->messageProperties = array('persistent' => 'true', "mw-message-id" => $mwRequestObject->getId());
    }

    public function addCommandRequest($bank_id="", $command_text="",$param ='') {
        if ($bank_id != "" && $command_text != "") {
            $ob = new MwCommandRequest();
            $ob->setMessageText($ob->createMessageText($command_text , $param));
            $mw_mapper_id = Doctrine::getTable('BankMwMapping')->getMwMappingIdByBankId($bank_id);
            if ($mw_mapper_id) {
                $ob->setBankMwMapperId($mw_mapper_id);
            }
            
            $ob->save();
            $this->setMessageProperties($ob);
            $this->mwRequestObj = $ob;
            return $ob->toArray();

        }

    }

}

?>
