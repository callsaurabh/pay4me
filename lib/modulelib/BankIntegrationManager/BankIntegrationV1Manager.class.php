<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class BankIntegrationV1Manager extends parentBiVersionManager {

    public function __construct($transactionNumber='', $collection_account_id="", $currencyId="", $amount_credit="") {
        parent::__construct($transactionNumber, $collection_account_id, $currencyId, $amount_credit);
    }

    public function setMessageProperties($mwTransactionObject) {
        $this->messageProperties = array('persistent' => 'true', "logger_level" => $mwTransactionObject->getTransactionBankPosting()->getBank()->getBiLoggerLevel());
        
    }

    public function addCommandRequest($bank_id="",$command_text="",$param="") {
//        throw New Exception("Bank Not enabled for Command Requests");
        return false ;
    }

}

?>