<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class parentBiVersionManager implements BiManagerInterface {

    protected $transactionNumber;
    protected $messageProperties;
    protected $mwRequestObj;
    protected $collection_account_id;
    protected $currencyId;
    protected $amount_credit;
//    protected $bi_protocol_version;

    abstract public function setMessageProperties($mwRequestObject);
    abstract public function addCommandRequest($bank_id='',$command_text='');

    public function __construct($transactionNumber='', $collection_account_id="", $currencyId="", $amount_credit="") {

        $this->transactionNumber = $transactionNumber;
        $this->collection_account_id = $collection_account_id;
        $this->currencyId = $currencyId;
        $this->amount_credit = $amount_credit;
    }

    public function getCollectionAccountId() {
        return $this->collection_account_id;
    }

    public function getCurrencyId() {
        return $this->currencyId;
    }

    public function getAmountCredit() {
        return $this->amount_credit;
    }

    public function getMessageProperties() {
        return $this->messageProperties;
    }

    public function getRequestObject() {
        return $this->mwRequestObj;
    }

    public function addTransactionRequest() {

        $bankPosting_obj = $this->addToTransactionBankPosting();

        $mwTransactionObject = Doctrine::getTable('MwTransactionRequest')->getTranxMessage($bankPosting_obj, $this);
        $mwTransactionObject->save();
        $this->mwRequestObj = $mwTransactionObject;
        $this->setMessageProperties($mwTransactionObject);
        $this->updateLastMwRequest($mwTransactionObject,$bankPosting_obj); // updating last_mw_request_id
        return $this;
    }


    public function updateLastMwRequest($mwObject,$bankPosting_obj) {

        //$transaction_obj = Doctrine::getTable('TransactionBankPosting')->find($mwObject->getTransactionBankpostingId());
        $bankPosting_obj->setLastMwRequestId($mwObject->getId());
        $bankPosting_obj->save();
    }

    /*
     * WP060
     * Changes in structure of transaction Bank posting table
     */

    protected function addToTransactionBankPosting() {
        $objTxn = new TransactionBankPosting();
        $objTxn->setNoOfAttempts(0);
        $objTxn->setMessageTxnNo($this->transactionNumber);

        $transaction_obj = Doctrine::getTable('Transaction')->getMerchantDetails($this->transactionNumber);
        if (count($transaction_obj) != 0) {
            $objTxn->setMerchantRequestId($transaction_obj['MerchantRequest']['id']);
            $objTxn->setTxnType("payment");
            $objTxn->setBankId($transaction_obj['MerchantRequest']['bank_id']);
        } else {

            $objTxn->setMerchantRequestId(NULL);
            $objTxn->setTxnType("recharge");
            $bankId = sfContext::getInstance()->getUser()->getGuardUser()->getBankUser()->getFirst()->getBankId();
            $objTxn->setBankId($bankId);
        }

        try {
            $objTxn->save();
            return $objTxn;
        } catch (Exception $ex) {
            echo "Check Error " . $ex->getMessage();
        }
    }
    

}

?>
