<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *  
 * Description of merchantWizardClass
 * includes the detail of Merchant Wizard
 * CR046
 * @author ashutoshs
 */
class merchantWizard {

    // Get Private variable
    private $notRequirdField = array('deleted_at', 'created_by', 'updated_by');
    private $timestampField = array('created_at', 'updated_at');
    private $highestRow = 0;

    /**
     * function to get sql by merchant id
     * CR046
     * @author ashutoshs
     * @param integer $merchantId
     * @return query
     */
    public function getSqlForMerchant($merchantId) {
        /**
         * set the variable for initiation
         * CR046
         */
        $sqlQuery = '';
        $accountIdCount = 0;
        $splitAccountIdCount = 0;

        $table = 'merchant';
        $dqlTable = 'Merchant';
        $sqlQuery .= $this->getSqlById($table, $dqlTable, $merchantId, 'id', array('is_freezed'), array('id' => 'MERCHANT_ID'));

        $table = 'bank_merchant';
        $dqlTable = 'BankMerchant';
        $sqlQuery .= $this->getSqlById($table, $dqlTable, $merchantId, 'merchant_id', array('id'), array('merchant_id' => 'MERCHANT_ID'));

        $bankServiceResult = Doctrine::getTable('ServiceBankConfiguration')->findByMerchantId($merchantId);
        foreach ($bankServiceResult as $bankServiceRes) {

            $accountIdCount++;
            $accountId = 'EP_MASTER_ACCOUNT_ID' . $accountIdCount;

            $table = 'ep_master_account';
            $dqlTable = 'EpMasterAccount';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $bankServiceRes->getMasterAccountId(), 'id', array(), array('id' => $accountId));

            $table = 'service_bank_configuration';
            $dqlTable = 'ServiceBankConfiguration';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $bankServiceRes->getId(), 'id', array('id'), array('merchant_id' => 'MERCHANT_ID', 'master_account_id' => $accountId));
        }


        $finInstResult = Doctrine::getTable('FinancialInstitutionAcctConfig')->findByMerchantId($merchantId);

        foreach ($finInstResult as $finInstRes) {

            $accountIdCount++;
            $accountId = 'EP_MASTER_ACCOUNT_ID' . $accountIdCount;

            $table = 'financial_institution_acct_config';
            $dqlTable = 'FinancialInstitutionAcctConfig';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $finInstRes->getId(), 'id', array('id'), array('merchant_id' => 'MERCHANT_ID', 'master_account_id' => $accountId));

            $table = 'ep_master_account';
            $dqlTable = 'EpMasterAccount';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $finInstRes->getAccountId(), 'id', array(), array('id' => $accountId));
        }


        $countService = 0;
        $ServiceResult = Doctrine::getTable('MerchantService')->getServices($merchantId);
        foreach ($ServiceResult as $serviceRow) {
            $countService++;
            $table = 'merchant_service';
            $dqlTable = 'MerchantService';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $serviceRow['id'], 'id', array(), array('id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID'));

            $table = 'merchant_service_charges';
            $dqlTable = 'MerchantServiceCharges';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $serviceRow['id'], 'merchant_service_id', array('id'), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID'));

            $table = 'transaction_charges';
            $dqlTable = 'TransactionCharges';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $serviceRow['id'], 'merchant_service_id', array('id'), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID'));

            $table = 'validation_rules';
            $dqlTable = 'ValidationRules';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $serviceRow['id'], 'merchant_service_id', array('id'), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID'));

            $table = 'service_payment_mode_option';
            $dqlTable = 'ServicePaymentModeOption';
            $sqlQuery .= $this->getSqlById($table, $dqlTable, $serviceRow['id'], 'merchant_service_id', array('id'), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID'));


            $splitResult = Doctrine::getTable('SplitEntityConfiguration')->findByMerchantServiceId($serviceRow['id']);
            $splitArr = $splitResult->toArray();
            foreach ($splitArr as $splitRes) {

                if ($splitRes['split_account_configuration_id'] > 1) {

                    $splitAccountConfigResult = Doctrine::getTable('SplitAccountConfiguration')->find($splitRes['split_account_configuration_id']);



                    $virtualAcctConfigArr = Doctrine::getTable('VirtualAccountConfig')->getByVirtualAccountId($splitAccountConfigResult->getMasterAccountId());
//                 $virtualAcctConfigArr = $virtualAcctConfigResult->toArray();
                    foreach ($virtualAcctConfigArr as $virtualAccountRow) {
                        $accountIdCount++;
                        $accountId1 = 'EP_MASTER_ACCOUNT_ID' . $accountIdCount;
                        $accountIdCount++;
                        $accountId2 = 'EP_MASTER_ACCOUNT_ID' . $accountIdCount;

                        $table = 'ep_master_account';
                        $dqlTable = 'EpMasterAccount';
                        $sqlQuery .= $this->getSqlById($table, $dqlTable, $virtualAccountRow['virtual_account_id'], 'id', array(), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID', 'id' => $accountId1));
                        $sqlQuery .= $this->getSqlById($table, $dqlTable, $virtualAccountRow['physical_account_id'], 'id', array(), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID', 'id' => $accountId2));


                        $table = 'virtual_account_config';
                        $dqlTable = 'VirtualAccountConfig';
                        $sqlQuery .= $this->getSqlById($table, $dqlTable, $virtualAccountRow['id'], 'id', array('id'), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID', 'virtual_account_id' => $accountId1, 'physical_account_id' => $accountId2));
                    }
                    $splitAccountIdCount++;

                    $splitAccountId = 'SPLIT_ACCOUNT_CONIFIGURATION_ID' . $splitAccountIdCount;
                    $table = 'split_account_configuration';
                    $dqlTable = 'SplitAccountConfiguration';
                    ;
                    $sqlQuery .= $this->getSqlById($table, $dqlTable, $splitRes['split_account_configuration_id'], 'id', array(), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID', 'id' => $splitAccountId, 'master_account_id' => $accountId1));

                    $table = 'split_entity_configuration';
                    $dqlTable = 'SplitEntityConfiguration';
                    $sqlQuery .= $this->getSqlById($table, $dqlTable, $splitRes['id'], 'id', array('id'), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID', 'split_account_configuration_id' => $splitAccountId));

                    unset($virtualAcctConfigArr);
                    unset($virtualAcctConfigResult);
                } else {
                    $table = 'split_entity_configuration';
                    $dqlTable = 'SplitEntityConfiguration';
                    $sqlQuery .= $this->getSqlById($table, $dqlTable, $splitRes['id'], 'id', array('id'), array('merchant_service_id' => 'MERCHANT_SERVICE_ID' . $countService, 'merchant_id' => 'MERCHANT_ID'));
                }
            }
        }

        return $sqlQuery;
    }

    /**
     * function to get sql for table
     * CR046
     * @author ashutoshs
     * @param string  $table MYSQL Table
     * @param string  $table DQL Table
     * @param integer $id
     * @param string  $column (optional, default: id)
     * @param Array  $notRequiredColumn (optional)
     * @param Array  $defaultColumnVal (optional)
     * @return stiring
     */
    private function getSqlById($table, $dqlTable='', $id, $column='id', $notRequiredColumn=array(), $defaultColumnVal=array()) {
        /**
         * set the variable for initiation
         * CR046
         */
        $findStr = 'findBy' . ucfirst($column);
        $return = '';
        if ($dqlTable == '')
            $dqlTable = $table;
        $notRequiredColumn = array_merge($notRequiredColumn, $this->notRequirdField);
        /**
         * process the fucntion
         * CR046
         */
        $result = Doctrine::getTable($dqlTable)->$findStr($id);
        $resArr = $result->toArray();
        unset($result);
        foreach ($resArr as $row) {
            $return.= ' INSERT INTO ' . $table . ' set ';
            $countRow = 0;
            foreach ($row as $key => $value) {

                if (!in_array($key, $notRequiredColumn) && !is_array($value) && $value != '') {
                    $value = addslashes($value);
                    $value = @preg_replace("/\\n/", "\\\\n", $value);
                    if ($countRow != 0)
                        $return.=', ';
                    if (in_array($key, $this->timestampField))
                        $return.= $key . '= now() ';
                    else
                    if (array_key_exists($key, $defaultColumnVal))
                        $return.= $key . '=' . $defaultColumnVal[$key];
                    else
                        $return.= $key . '="' . $value . '" ';
                    $countRow++;
                }
            } // end of foreach for $row
            $return.= ';';
            $return.= "";
        }// end of foreach for main foreach $resArr
        $return.="";
        return $return;
    }

    /*
     *  Function for to get the highest row in excel */

    public function getHighestRow($activeSheet, $inc_value="") {
        $ascii = 65;
        $ascii_merge_column = 71;
        if ($inc_value) {
            //   $activeSheet->mergeCells(chr($ascii).($this->highestRow+1).':'.chr($ascii_merge_column).($this->highestRow+1));
            $highestRow = $this->highestRow + $inc_value;
        } else {
            $activeSheet->mergeCells(chr($ascii) . ($this->highestRow + 1) . ':' . chr($ascii_merge_column) . ($this->highestRow + 1));
            //  $activeSheet->mergeCells(chr($ascii).($this->highestRow+1).':'.chr($ascii_merge_column).($this->highestRow+1));
            $highestRow = $this->highestRow + 2;
        }
        $this->setHighestRow($highestRow);
        return $highestRow;
    }

    /* Function for to set the highest row in excel */

    public function setHighestRow($val) {
        $this->highestRow = $val;
    }

    /* Function for cretae the excel file for merchnat setUp
     * @param  integer $merchant_id Id of the Merchant
     */

    public function createSetupTemplate($merchant_id) {
        $objPHPExcel = new PHPExcel();
        $setupObj = new merchantSetupTemplate();
        $setupObj->setMerchantId($merchant_id);
        $objPHPExcel->getProperties()->setTitle("Pay4Me Merchant Setup Template"); //setting the title of the spreadsheet
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet->setTitle('Pay4me Setup Template');

        $ascii = 65;
        $highestRow = 1;
        $ascii_merge_column = 71;
        $merchantObj = Doctrine::getTable('Merchant')->find($merchant_id);
        $fileName = $merchantObj->getName();
        $highestRow = $this->getHighestRow($activeSheet);



        $activeSheet->mergeCells(chr($ascii) . $highestRow . ':' . chr($ascii_merge_column) . $highestRow);
        $activeSheet->setCellValue(chr($ascii) . $highestRow, "Pay4me Setup Template for Projects");
        $activeSheet->getStyle(chr($ascii) . $highestRow)->applyFromArray($this->getExcelHeaderHorizontalFormate());
        $highestRow = $this->getHighestRow($activeSheet, '1');
        $msg = "Setup For Merchant - " . $fileName;
        if ($merchantObj->getIsFreezed() == '1') {
            $msg .= " (Freezed) ";
        }
        $activeSheet->mergeCells(chr($ascii) . $highestRow . ':' . chr($ascii_merge_column) . $highestRow);
        $activeSheet->setCellValue(chr($ascii) . $highestRow, $msg);
        $activeSheet->getStyle(chr($ascii) . $highestRow)->applyFromArray($this->getExcelHeaderHorizontalFormate());

        for ($i = 1; $i <= 5; $i++) {

            $var = "createItem" . $i;
            $setupObj->$var();
            $activeSheet->getColumnDimension('A')->setWidth(17); //modified the width to fix FS#29415
            $activeSheet->getColumnDimension('B')->setWidth(17); //modified the width to fix FS#29415
            $activeSheet->getColumnDimension('C')->setWidth(17); //modified the width to fix FS#29415
            $activeSheet->getColumnDimension('D')->setWidth(17); //modified the width to fix FS#29415
            $activeSheet->getColumnDimension('E')->setWidth(17); //modified the width to fix FS#29415
            $activeSheet->getColumnDimension('F')->setWidth(17); //modified the width to fix FS#29415
            $activeSheet->getColumnDimension('G')->setWidth(17); //modified the width to fix FS#29415



            $this->setExcelData($setupObj, $activeSheet, $ascii, $ascii_merge_column);
        }
        $paymentModeOption = $this->getMerchantPaymentModeOption($merchant_id);
        foreach ($paymentModeOption as $val) {
            if ($val == 2) {

                $setupObj->displayInterswitchCategory($val);
                $this->setExcelData($setupObj, $activeSheet, $ascii, $ascii_merge_column);
            }
            $setupObj->getServiceCharges($val);

            $this->setExcelData($setupObj, $activeSheet, $ascii, $ascii_merge_column);
            $setupObj->getTransactionCharges($val);
            $this->setExcelData($setupObj, $activeSheet, $ascii, $ascii_merge_column);
        }


        $logPath = sfConfig::get('sf_upload_dir') . "/MerchantPdf/";
        if (is_dir($logPath) == '') {
            $dir_path = $logPath . "/";
            mkdir($dir_path, 0777, true);
            chmod($dir_path, 0777);
        }
        $path = $logPath . $fileName . ".pdf";
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
        $objWriter->save($path);
    }

    private function setExcelData($setupObj, $activeSheet, $ascii, $ascii_merge_column) {
        $heading = $setupObj->getHeading();
        $description = $setupObj->getDescription();
        $dataHeader = $setupObj->getInfoHeader();
        $item_data = $setupObj->getData();




        /*         * **heading** */
        $highestRow = $this->getHighestRow($activeSheet);



        $activeSheet->mergeCells(chr($ascii) . $highestRow . ':' . chr($ascii_merge_column) . $highestRow);
        $activeSheet->setCellValue(chr($ascii) . $highestRow, $heading);
        $activeSheet->getStyle(chr($ascii) . $highestRow)->applyFromArray($this->getExcelHeaderFormate());


        $highestRow = $this->getHighestRow($activeSheet);


        /*         * *description** */
        $activeSheet->mergeCells(chr($ascii) . $highestRow . ':' . chr($ascii_merge_column) . $highestRow);
        $activeSheet->setCellValue(chr($ascii) . $highestRow, $description);
        $highestRow = $this->getHighestRow($activeSheet);

        /*         * *header** */
        $cell = $ascii;
        foreach ($dataHeader as $key => $val) {
            $activeSheet->setCellValue(chr($cell) . $highestRow, $val);
            $activeSheet->getStyle(chr($cell) . $highestRow)->applyFromArray($this->getExcelHeaderFormate());

            $cell++;
        }
        $highestRow = $this->getHighestRow($activeSheet, '1');

        /*         * *data** */
        foreach ($item_data as $key => $value) {
            $cell = $ascii;
            foreach ($value as $data_val) {
                $activeSheet->setCellValue(chr($cell) . $highestRow, $data_val);
                $activeSheet->getStyle(chr($cell) . $highestRow)->getAlignment()->setWrapText(true);
                $cell++;
            }
            $highestRow = $this->getHighestRow($activeSheet, '1');
        }
        // $highestRow = $this->getHighestRow();
    }

    /**
     * Description of Function
     * return the all payment mode option of merchant
     * @param  integer $merchnatId Id of the Merchant
     *
     */
    private function getMerchantPaymentModeOption($merchnatId) {
        $paymentModeOption = Array();
        $paymentModeOptionObj = Doctrine::getTable('ServicePaymentModeOption')->getAllPaymentModeOption($merchnatId);
        foreach ($paymentModeOptionObj as $val) {
            $paymentModeOption[] = $val->getPaymentModeOptionId();
        }
        return $paymentModeOption;
    }

    protected function getExcelCellColorChange($color) {

        $styleArray = array(
            'font' => array(
                'color' => array('argb' => $color),
            ),
        );

        return $styleArray;
    }

    protected function getExcelHeaderFormate() {

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('argb' => '00000000'),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DASHED,
                ),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DASHED,
                ),
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DASHED,
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DASHED,
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'CFE6DA',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        return $styleArray;
    }

    protected function getExcelHeaderHorizontalFormate() {

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('argb' => '00000000'),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
        return $styleArray;
    }

}

?>
