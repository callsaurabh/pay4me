<?php

/**
 *
 * This class provides the details in the format of the Items included in the Merchant Setup Template
 * CR046
 * @author Sarita Pandey
 * @author Shuchi Sethi
 */
class merchantSetupTemplate {

    /**
     * Merchant Id whose template is required
     */
    private $merchantId;
    /**
     * Heading of the Item in the Template
     */
    private $heading;
    /**
     * Description of the Item in the Template
     */
    private $description;
    /**
     * Header of the data on the Item
     */
    private $infoHeader = array();
    /**
     * Data of the Item
     */
    private $data = array();

    /**
     * Sets the Id of the Merchant whose Template is to be generated
     *
     * @param  integer $merchantId Id of the Merchant
     */
    public function setMerchantId($merchantId) {
        $this->merchantId = $merchantId;
    }

    /**
     * Returns the Id of the merchant whose template is to be generated
     *
     * @return  integer merchantId Returns the Id of the merchant whose template is to be generated
     */
    protected function getMerchantId() {
        return $this->merchantId;
    }

    /**
     * Sets the heading of the item in the Template
     *
     * @param  string $heading Heading of the Item to be set
     */
    protected function setHeading($heading) {
        $this->heading = $heading;
    }

    /**
     * Returns the Heading of the item in the Template
     *
     * @return  string $heading Heading of the Item
     */
    public function getHeading() {
        return $this->heading;
    }

    /**
     * Sets the description of the item in the Template
     *
     * @param  string $description Description of the Item to be set
     */
    protected function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Returns the description of the item in the Template
     *
     * @return  string $description Description of the Item
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Sets the Header of the data of the item in the Template
     *
     * @param  array $infoHeader Describes the data that will be contained in the item
     */
    protected function setInfoHeader($infoHeader) {
        $this->infoHeader = $infoHeader;
    }

    /**
     * Returns the Header of data of the item in the Template
     *
     * @return  array $infoHeader Header of the data of an Item in Template
     */
    public function getInfoHeader() {
        return $this->infoHeader;
    }

    /**
     * Sets the data of the item in the Template
     *
     * @param  array $data Data of the Item to be set
     */
    protected function setData($data) {
        $this->data = $data;
    }

    /**
     * Returns the data of the item in the Template for a Merchant
     *
     * @return  array $data Data of the Item
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Sets the Heading, Description, InfoHeader and Data to be contained in Item1 of Merchant setup Template
     * Item1 contains the details of the services that are or will be configured for a Merchant
     */
    public function createItem1() {

        $this->setHeading('Item 1: Merchant Payment Type Setup:');
        $this->setDescription('Capture the various billable items associated with the merchant’s processes that form the collections handled by the service provider: ');
        $this->setInfoHeader(array("Payment Type Name", "Amount", "Payment Interval", "Other Information"));

        // $this->data = array(array('New Driver’s Licence','','',''), array('Renew Driver’s Licence','','',''));

        $serviceDetails = $this->getServiceDetails();

        $data = Array();
        foreach ($serviceDetails as $val) {
            $this->serviceId[] = $val['id'];
            $data[] = Array($val['name'], '', '', '');
        }

        $this->setData($data);
    }

    private function getServiceDetails() {
        $merchantId = $this->getMerchantId();
        return Doctrine::getTable('MerchantService')->findByMerchantId($merchantId);
    }

    /**
     * Sets the Heading, Description, InfoHeader and Data to be contained in Item1 of Merchant setup Template
     * Item2 contains the details of the details of the splitting of the various amounts
     */
    public function createItem2() {
        $this->setHeading('Item 2: Payment Splitting Entity:');
        $this->setDescription('Provide details of the splitting of the various amounts provided in the merchant payment type and the beneficiaries (who gets what).');
        $this->setInfoHeader(array("Party", "Payment Type Name", "Amount", "Percentage of Amount	", "Other Information"));
        $data = $this->getSplitDetail();
        $this->setData($data);
    }

    /**
      Function for get split account detail for merchant
     */
    private function getSplitDetail() {
        $merchantId = $this->getMerchantId();
        $merchantServiceObj = Doctrine::getTable('SplitEntityConfiguration')->getSplitDetail($merchantId);
        $dataValue = Array();
        if ($merchantServiceObj) {
            foreach ($merchantServiceObj as $data) {
                $splitConfigurationObj = $data->getSplitEntityConfiguration();
                $serviceName = $data->getName();
                $party_val = array();
                $percent_amount = array();
                $amountArray = array();
                foreach ($splitConfigurationObj as $details) {
                    $party = $details->getSplitAccountConfiguration()->getAccountParty();
                    $splitTypeId = $details->getSplitTypeId();
                    if ($splitTypeId == 1)
                        $amountArray[] = $details->getCharge();
                    else
                        $percent_amount[] = $details->getCharge();

                    $party_val[] = $party;
                }
                $amount = implode(' / ', $amountArray);
                $pAmount = implode(' / ', $percent_amount);
                $partyName = implode(' / ', $party_val);
                $dataValue[] = Array($partyName, $serviceName, $amount, $pAmount, '');
            }

            return $dataValue;
        }else {
            return $dataValue;
        }
    }

    /*
     * Sets the Heading, Description, InfoHeader and Data to be contained in Item1 of Merchant setup Template
     * Item3 contains the details of merchant consolidation account
     */

    public function createItem3() {
        $this->setHeading('Item 3: Merchant Payments Crediting (consolidation) Accounts ');
        $this->setDescription('Provide the detail of the accounts stipulated by the merchant for his payments. Add each account detail as an entity in the event that the merchant stipulated different accounts for different payment types.');
        $this->setInfoHeader(array("Entity 	Bank Name", "Crediting Account Number", "Account Name", "Branch Location Name", "Branch Sort Code", "Payment Type Category"));
        $data = $this->getConsolidationDetail();
        $this->setData($data);
    }

    /**
      Function for get consilidation account detail for merchant
     */
    private function getConsolidationDetail() {
        $merchantId = $this->getMerchantId();
        $merchantConsolidationObj = Doctrine::getTable('SplitEntityConfiguration')->getSplitDetail($merchantId);
        $value = Array();
        if ($merchantConsolidationObj) {
            foreach ($merchantConsolidationObj as $data) {
                $value = "";
                $splitConfigurationObj = $data->getSplitEntityConfiguration();
                foreach ($splitConfigurationObj as $details) {
                    $accountName = $details->getSplitAccountConfiguration()->getAccountName();
                    $accountNumber = $details->getSplitAccountConfiguration()->getAccountNumber();
                    $bankName = $details->getSplitAccountConfiguration()->getBankName();
                    $sortCode = $details->getSplitAccountConfiguration()->getSortCode();
                    $value[] = Array($bankName, $accountNumber, $accountName, '', $sortCode, '');
                }
            }
            return $value;
        } else {
            return $value;
        }
    }

    /*
     * Sets the Heading, Description, InfoHeader and Data to be contained in Item1 of Merchant setup Template
     * Item4 contains the details of the accounts stipulated by the service providers for their payments
     */

    public function createItem4() {
        $this->setHeading('Item 4: Service Provider Payment Crediting Accounts: ');
        $this->setDescription('Provide the detail of the accounts stipulated by the merchant for his payments. Add each account detail as an entity in the event that the merchant stipulated different accounts for different payment types.');
        $this->setInfoHeader(array("Party", "Crediting Bank Name", "Crediting Account No", "Account Name", "Branch Location Name", "Branch Sort Code"));
        //fething the details of the service provider to be in the data
        $splitAccountObj = Doctrine::getTable('SplitAccountConfiguration')->find(1);

        $data = Array(Array($splitAccountObj->getAccountParty(), $splitAccountObj->getBankName(), $splitAccountObj->getAccountNumber(), $splitAccountObj->getAccountName(), '', $splitAccountObj->getSortCode()));

        $this->setData($data);
    }

    /**
     * Sets the Heading, Description, InfoHeader and Data to be contained in Item5 of Merchant setup Template
     * Item5 contains the details of the collection account of the Merchant, which also informs regarding the bank to be activated for collections
     */
    public function createItem5() {

        $this->setHeading('Item 5: Payment Collection Bank Detail Merchant Type Setup:'); //setting the header
        $this->setDescription('Provide the details of the banks and corresponding account details that would be involved with the collections for the merchant fees/charges: '); //setting the description of the item
        $this->setInfoHeader(array("Party", "Crediting Account Number", "Account Name", "Branch Location Name", "Branch Sort Code")); //setting the Info Header of the Item
        $merchantId = $this->getMerchantId();

        //fething the details of the Collection Acocunts to be in the data
        $collectionAcctObj = Doctrine::getTable('ServiceBankConfiguration')->getCollectionAccounts($merchantId);

        $collection = array();
        if ($collectionAcctObj) {
            $i = 0;
            foreach ($collectionAcctObj as $key => $data) {

                $accountObj = $data->getEpMasterAccount();
                $bank_name = $accountObj->getBankname();
                $account_number = $accountObj->getAccountNumber();
                $account_name = $accountObj->getAccountName();
                $sortcode = $accountObj->getSortCode();
                $collection[$i]['0'] = $bank_name;
                $collection[$i]['1'] = $account_number;
                $collection[$i]['2'] = $account_name;
                $collection[$i]['3'] = '';
                $collection[$i]['4'] = $sortcode;
                $i++;
            }
        }
        $this->setData($collection); //sets the data that has been created for the item
    }

    public function displayInterswitchCategory() {


        $interswitch_mer_cat_obj = Doctrine::getTable('InterswitchMerchantCategoryMapping')->findBy('merchant_id', $this->getMerchantId())->getLast();
        $categoryName = $interswitch_mer_cat_obj->getInterswitchCategory()->getName();
        $this->setHeading('Interswitch Category:');
        $data[0][0] = $categoryName;
        $this->setDescription("Category Provided by the Interswitch to Merchant");
        $this->setInfoHeader(array("Category Name"));
        $this->setData($data);
    }

    /**
     * Sets the Heading, Description, InfoHeader and Data to be contained in Service Charges of Merchant setup Template
     *
     *  @param  integer $payment_mode_option_id Id of the Merchant Service Payment Mode
     *
     */
    public function getServiceCharges($payment_mode_option_id) {
        $paymentObj = Doctrine::getTable('PaymentModeOption')->find($payment_mode_option_id);


        $paymentMode = $paymentObj->getPaymentMode()->getDisplayName();
        $this->setHeading('Service Charge Details for ' . $paymentMode . ' Payments:'); //setting the header
        $this->setDescription('Provide details of charges that are included in the transaction but are not stipulated by the merchant'); //setting the description of the item
        $this->setInfoHeader(array("Charge Name", "Party Type Name", "Share(%)", "Min", "Max", "Beneficiary", "Other Information")); //setting the Info Header of the Item
        $merchantId = $this->getMerchantId();
        $chargeObj = Doctrine::getTable('MerchantServiceCharges')->getMerchantCharges($merchantId, $payment_mode_option_id);
        $collection = array();
        if ($chargeObj) {
            foreach ($chargeObj as $dataObj) {
                $serviceObj = $dataObj->getMerchantService();
                $i = 0;
                foreach ($serviceObj as $setupObj) {
                    $service_name = $setupObj->getName();
                    $chargeObj = $setupObj->getMerchantServiceCharges()->getFirst();
                    $charges = $chargeObj->getServiceChargePercent();
                    $lower_slab = $chargeObj->getLowerSlab();
                    $upper_slab = $chargeObj->getUpperSlab();
                    $beneficiary = "Pay4Me Services Ltd";
                    $collection[$i]['0'] = 'Service Charge';
                    $collection[$i]['1'] = $service_name;
                    $collection[$i]['2'] = $charges . '%';
                    $collection[$i]['3'] = $lower_slab;
                    $collection[$i]['4'] = $upper_slab;
                    $collection[$i]['5'] = $beneficiary;
                    $collection[$i]['6'] = '';
                    $i++;
                }


                //sets the data that has been created for the item
            }
        } $this->setData($collection);
    }

    /**
     * Sets the Heading, Description, InfoHeader and Data to be contained in Transaction Charges of Merchant setup Template
     *
     *  @param  integer $payment_mode_option_id Id of the Merchant Service Payment Mode
     *
     */
    public function getTransactionCharges($payment_mode_option_id) {
        $paymentObj = Doctrine::getTable('PaymentModeOption')->find($payment_mode_option_id);
        $paymentMode = $paymentObj->getPaymentMode()->getDisplayName();
        $this->setHeading('Transaction Charge Details for ' . $paymentMode . ' Payments:'); //setting the header
        $this->setDescription('Provide details of charges that are included in the transaction but are not stipulated by the merchant'); //setting the description of the item
        $this->setInfoHeader(array("Charge Name", "Party Type Name", "Amount", "Beneficiary", "Other Information")); //setting the Info Header of the Item
        $merchantId = $this->getMerchantId();
        $transactionChargeObj = Doctrine::getTable('TransactionCharges')->getTransactionCharges($merchantId, $payment_mode_option_id);
        $collection = array();
        if ($transactionChargeObj) {
            foreach ($transactionChargeObj as $dataObj) {
                $serviceObj = $dataObj->getMerchantService();

                $i = 0;
                foreach ($serviceObj as $setupObj) {
                    $service_name = $setupObj->getName();
                    $chargeObj = $setupObj->getTransactionCharges()->getFirst();

                    $bank_charges = $chargeObj->getFinancialInstitutionCharges();
                    $pay4me_charges = $chargeObj->getPayformeCharges();
                    $beneficiary = '';
                    $other_info = '';
                    if ($pay4me_charges > 0) {
                        $beneficiary.="Pay4Me";
                        $other_info .= $pay4me_charges . " to Pay4Me ";
                    }
                    if ($bank_charges > 0) {
                        $beneficiary.=" / Bank";
                        $other_info .= " / " . $bank_charges . " to Bank ";
                    }

                    $collection[$i]['0'] = 'Transaction Charge';
                    $collection[$i]['1'] = $service_name;
                    $collection[$i]['2'] = 'N' . number_format($pay4me_charges + $bank_charges, 2, '.', '');
                    $collection[$i]['3'] = $beneficiary;
                    $collection[$i]['4'] = $other_info;
                    $i++;
                }
                //sets the data that has been created for the item
            }
        } $this->setData($collection);
    }

}

