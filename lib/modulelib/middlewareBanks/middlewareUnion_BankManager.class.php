<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4MeV1Managerclass
 *
 * @author sdutt
 */
class middlewareUnion_BankManager {
	//put your code here
	public function processRequest($requestXML,$bankId,$requestId){
		return $responseXml = self::getBankResponseXml($requestXML,$bankId,$requestId);
	}

	/**
	 * Function to get bank resopnse.
	 * @param string $requestXmlObject
	 * @param integer $bankId
	 * @param integer $requestId
	 * @return boolean|unknown
	 */
	public function getBankResponseXml($requestXml,$bankId,$requestId) {
		ini_set('soap.wsdl_cache_enabled',0);
		ini_set('soap.wsdl_cache_ttl',0);
		if(!extension_loaded("soap")){
			dl("php_soap.dll");
		}
		$authenticationHeader = self::getAuthHeader($bankId);
		$context = array('http' => array(
			'header'  => $authenticationHeader)
		);
		if(version_compare(PHP_VERSION, '5.3.0') == -1){
			ini_set('user_agent', 'PHP-SOAP/' . PHP_VERSION . "\r\n" . $authenticationHeader);
		}
		$wsdl = Doctrine::getTable('BankProperties')->getBankPropertryValue($bankId, 'BANK_POST_URL');
		$level = middlewareActions::getBankLoggingLevel($bankId);
		if($level == 'DEBUG') {
			$message = 'Notification Sent to bank post URL :  '.$wsdl;
			$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
			middlewareActions::MiddlewareLog($message,$location,$requestId,$level);
		}
		try{
			//$client = new SoapClient("http://localhost/soap_test/books.wsdl");
			//$responseXml = $client->doMyBookSearch($requestXml);

			// Server link for notification.
			$client = new SoapClient($wsdl, array('trace'=> true, 'exceptions' => true));
			$responseXml = $client->invokeMessage($requestXml);
			
		} catch(Exception $e) {
			if($level == 'DEBUG') {
				$message = 'Error on SoapClient URL : '.$wsdl.'\n';
				$message .= $e->getMessage();
				$location = 'METHOD:'. __FUNCTION__ .' LINE:'.__LINE__;
				middlewareActions::MiddlewareLog($message,$location,$requestId,$level);
			}
			return false;
		}
		return $responseXml;
	}

	/**
	 * Function to generate http header.
	 * @param integer $bankId
	 * @return string
	 */
	public function getAuthHeader($bankId) {
		$bank_code = Doctrine::getTable('BankProperties')->getBankPropertryValue($bankId, 'BANK_CODE');
		$bank_key = Doctrine::getTable('BankProperties')->getBankPropertryValue($bankId, 'BANK_KEY');
	
		$bank_auth_string = trim($bank_code) .":" .trim($bank_key);
		$bankAuth = base64_encode(trim($bank_auth_string)) ;
	
		$authenticationHeader = 'Authorization: Basic ' . trim($bankAuth) . "\r\n"
			. "Accept: application/xml; charset=UTF-8";
	
		return $authenticationHeader;
	}


}
?>
