<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of monitoringToolclass
 *
 * @author ashutoshs
 */

/**
 * Check Split Account Configuration consisting right account
 *
 * @param $param: array
 * @return bool
 *         array
 *           ~ Subject
 *           ~ Mail Body
 */
class monitoringToolManager {

public $funReturnType='';
######## Subject Code ############
    private $MAIL_SUB_AccountAssociation = 'Pay4Me Monitor: ACCOUNT ASSOCIATION IS NOT VALID';
    private $MAIL_SUB_MonitorSplitingOfAmount = 'Pay4Me Monitor: SPILITING OF AMOUNT  IS NOT VALID';
    private $MAIL_SUB_MonitorMassiveTransaction = 'Pay4Me Monitor: MASSIVE TRANSACTION FOUND';
    private $MAIL_SUB_MonitorAccountBalaceOfEwallet = 'Pay4Me Monitor: EWALLET ACCOUNT DISCREPANCY';
    private $MAIL_SUB_MonitorLog = 'Pay4Me Monitor: ERROR IN Log ';
    private $MAIL_SUB_MonitorPaymentNotification = 'Pay4Me Monitor: PAYMENT NOTIFICATION NOT SEND';

##################################

######## Mail Body ###############
    private $MAIL_BODY_AccountAssociation = '<center><b>This is monitoring mail.</b></center>
<br>
At the time of process monitoring, system diagnosed following issue: <br>
Following Master Account Id associated with TABLE_NAME should be ACCOUNT_TYPE';

   private $MAIL_BODY_MonitorSplitingOfAmount = '<center><b>This is monitoring mail.</b></center>
<br>
At the time of process monitoring, system diagnosed following issue: <br>
Spliting of amount is not valid with validation no: <b>VALIDATION_NO</b>';

    private $MAIL_BODY_MonitorMassiveTransaction = '<center><b>This is monitoring mail.</b></center>
<br>
At the time of process monitoring, system diagnosed following issue: <br>
Over Limit Transaction in EpMasterLdeger ids : (limit ';
   private $MAIL_BODY_MonitorAccountBalaceOfEwallet = '<center><b>This is monitoring mail.</b></center>
<br>
At the time of process monitoring, system diagnosed following issue: <br>
Following Ewallet Account have discrepancy with account balance</b>';

   private $MAIL_BODY_MonitorLog = '<center><b>This is monitoring mail.</b></center>
<br>
At the time of process monitoring, system diagnosed following issue: <br>
Log have following issue</b>';

    private $MAIL_BODY_MonitorPaymentNotification = '<center><b>This is monitoring mail.</b></center>
<br>
At the time of process monitoring, system diagnosed following issue: <br>
Problem with exeuting "Payment Notification" job with following Job Ids </b>';
##################################

  public $verifierFunction ='';

  protected function setVerifierFunction($funtionName)
   {
       $this->verifierFunction = $funtionName;

   }
  private function getMailBody($BodyPostfix='AccountAssociation')
   {

             $BodyVar = 'MAIL_BODY_'.$BodyPostfix;
             return $this->$BodyVar;
   }
  private function getMailSubject($SubPostfix='AccountAssociation')
   {

           $SubVar = 'MAIL_SUB_'.$SubPostfix;
           return $this->$SubVar;

   }
  public function setReturnType($returnType)
   {
           return $this->funReturnType = $returnType;
   }

/**
 * Verify Split Account Configuration
 *
 * @param $param: array
 * @return bool/Array
 */
  public function verifySplitAccountConfiguration($params=array())
   {
       //Chceck the all account are recieving
       $res = Doctrine::getTable('SplitAccountConfiguration')->getNonReceivingAccount();
       //if not
       if($res)
        {
               $this->setVerifierFunction('verifySplitAccountConfiguration');
               if($this->funReturnType=='Bool')
                return true;
               else
               {
                   $subject = $this->getMailSubject();
                   $body = $this->getMailBody();
                   $body = str_replace('TABLE_NAME','SplitAccountConfiguration',$body);
                   $body = str_replace('ACCOUNT_TYPE','receiving',$body);

                   $count = 1;
                   foreach($res as $result)
                   {
                     $body .='<br>'.$count.': '.$result->getEpMasterAccountId();
                     $count++;
                   }
                     $body .='<br><br>';
                     $mailArr[0] = $subject;
                     $mailArr[1] = $body;

                 return $mailArr;
              } //if return type
        }
       else
         return false;

   }



  public function verifyServiceBankConfiguration($params=array())
   {
            //Chceck the all account are recieving
            $res = Doctrine::getTable('ServiceBankConfiguration')->getNonCollectionAccount();
            //if not
            if($res)
                {
                    $this->setVerifierFunction('verifyServiceBankConfiguration');
                   if($this->funReturnType=='Bool')
                    return true;
                   else
                   {

                        $subject = $this->getMailSubject();
                        $body = $this->getMailBody();
                        $body = str_replace('TABLE_NAME','ServiceBankConfiguration',$body);
                        $body = str_replace('ACCOUNT_TYPE','collection',$body);

                        $count = 1;
                        foreach($res as $result)
                        {
                            $body .='<br>'.$count.': '.$result->getEpMasterAccountId();
                            $count++;
                        }
                        $body .='<br><br>';
                        $mailArr[0] = $subject;
                        $mailArr[1] = $body;
                        return $mailArr;
                   } //if return type
                }
            else
                return false;

   }

/**
 * Verify User Account Collection
 *
 * @param $param: array
 * @return bool/Array
 */
  public function verifyUserAccountCollection($params=array())
   {
            //Chceck the all account are recieving
            $res = Doctrine::getTable('UserAccountCollection')->getNonEwalletAccount();
            //if not
            if($res)
                {
                    $this->setVerifierFunction('verifyUserAccountCollection');
                   if($this->funReturnType=='Bool')
                    return true;
                   else
                   {

                        $subject = $this->getMailSubject();
                        $body = $this->getMailBody();
                        $body = str_replace('TABLE_NAME','UserAccountCollection',$body);
                        $body = str_replace('ACCOUNT_TYPE','ewallet',$body);

                        $count = 1;
                        foreach($res as $result)
                        {
                            $body .='<br>'.$count.': '.$result->getEpMasterAccountId();
                            $count++;
                        }
                        $body .='<br><br>';
                        $mailArr[0] = $subject;
                        $mailArr[1] = $body;
                        return $mailArr;
                   } //if return type
                }
            else
                return false;

   }
/**
 * Verify FinacialInstutionAcctConfiguration
 *
 * @param $param: array
 * @return bool/Array
 */
  public function verifyFinancialInstutionAcctConfiguration($params=array())
   {
            //Chceck the all account are recieving
            $res = Doctrine::getTable('FinancialInstitutionAcctConfig')->getNonReceivingAccount();
            //if not
            if($res)
                {
                    $this->setVerifierFunction('verifyFinancialInstutionAcctConfiguration');
                   if($this->funReturnType=='Bool')
                    return true;
                   else
                   {

                        $subject = $this->getMailSubject();
                        $body = $this->getMailBody();
                        $body = str_replace('TABLE_NAME','FinancialInstitutionAcctConfig',$body);
                        $body = str_replace('ACCOUNT_TYPE','receiving',$body);

                        $count = 1;
                        foreach($res as $result)
                        {
                            $body .='<br>'.$count.': '.$result->getEpMasterAccountId();
                            $count++;
                        }
                        $body .='<br><br>';
                        $mailArr[0] = $subject;
                        $mailArr[1] = $body;
                        return $mailArr;
                   } //if return type
                }
            else
                return false;

   }

/**
 * Verify VirtualAccountConfig
 *
 * @param $param: array
 * @return bool/Array
 */
  public function verifyVirtualAccountConfig($params=array())
   {
        //Chceck the all account are recieving
        $res = Doctrine::getTable('VirtualAccountConfig')->getNonReceivingAccount();
        //if not
        if($res)
            {
                 $this->setVerifierFunction('verifyVirtualAccountConfig');
                   if($this->funReturnType=='Bool')
                    return true;
                   else
                   {

                        $subject = $this->getMailSubject();
                        $body = $this->getMailBody();
                        $body = str_replace('TABLE_NAME','VirtualAccountConfig',$body);
                        $body = str_replace('ACCOUNT_TYPE','receiving',$body);

                        $count = 1;
                        foreach($res as $result)
                        {
                            $body .='<br>'.$count.': Physical Account='.$result->getEpMasterAccountId1().' and Virtual Account Id= '.$result->getEpMasterAccountId2();
                            $count++;
                        }
                        $body .='<br><br>';
                        $mailArr[0] = $subject;
                        $mailArr[1] = $body;
                        return $mailArr;
                   } //if return type
             }
        else
            return false;

   }

  public function verifyMonitorSplitingOfAmount($params=array())
   {

        $subject = $this->getMailSubject('MonitorSplitingOfAmount');
        $body = $this->getMailBody('MonitorSplitingOfAmount');
        $body = str_replace('VALIDATION_NO',$params['validationNumber'],$body);

        $mailArr[0] = $subject;
        $mailArr[1] = $body;
        return $mailArr;


   }
  public function verifyMonitorMassiveTransaction($params=array())
   {

        $subject = $this->getMailSubject('MonitorMassiveTransaction');
        $body = $this->getMailBody('MonitorMassiveTransaction');

        $upperLimit = Settings::getMonitorLedgerPerTxnLimit();
        $body .= ' NGN "'.($upperLimit/100).'") ';

        $toDate = date('Y-m-d',strtotime(date('Y')."-".date('m')."-".(date('d')-1)." 23:59:59"));
        $ledgerObj =  Doctrine::getTable('EpMasterLedger')->getOverLimitTransaction($upperLimit, $toDate);
          if($ledgerObj)
          {
               $count = 1;
               foreach($ledgerObj as $ledgerRow)
                 {
                     $body .='<br>'.$count.': '.$ledgerRow->getId();
                     $count++;
                }
          }

//        $body = str_replace('Ep_Master_Ledger_Id',$params['EpMasterLedgerId'],$body);

        $mailArr[0] = $subject;
        $mailArr[1] = $body;
        return $mailArr;


   }
  public function verifyMonitorAccountBalaceOfEwallet($params=array())
   {

        $subject = $this->getMailSubject('MonitorAccountBalaceOfEwallet');
        $body = $this->getMailBody('MonitorAccountBalaceOfEwallet');
        $count = 1;
        $EpMasterAccountIds = $params['EpMasterAccountIds'];
        $EpMasterAccountIdArr = explode(',',$EpMasterAccountIds);
        foreach($EpMasterAccountIdArr as $acId)
         {
             $body .='<br>'.$count.': '.$acId;
             $count++;
        }
        $body .='<br><br>';
        $mailArr[0] = $subject;
        $mailArr[1] = $body;
        return $mailArr;


   }
 public function verifyMonitorLog($params=array())
   {

        $subject = $this->getMailSubject('MonitorLog');
        $body = $this->getMailBody('MonitorLog');

        $errorLog = unserialize($params['ErrorLog']);
        $count = 1;
        foreach($errorLog as $logRow)
           {
             $body .='<br>'.$count.': '.$logRow;
             $count++;
        }
        $body .='<br><br>';
        $mailArr[0] = $subject;
        $mailArr[1] = $body;
        return $mailArr;


   }
  public function verifyMonitorPaymentNotification($params=array())
   {

        $subject = $this->getMailSubject('MonitorPaymentNotification');
        $body = $this->getMailBody('MonitorPaymentNotification');

        $jobIds = $params['EpJobIds'];
        $count = 1;
        $jobArr =  explode(',',$jobIds);
        foreach($jobArr as $jobId)
           {
             $body .='<br>'.$count.': '.$jobId;
             $count++;
        }
        $body .='<br><br>';
        $mailArr[0] = $subject;
        $mailArr[1] = $body;
        return $mailArr;


   }
}
?>
