#!/bin/bash
#
# * Description of monitorEpJob
# *
# * @author ashutoshs
# */
####### Function Decleration Start ################
########################################################################
#### Following code heavily inspired from
#### http://stackoverflow.com/questions/7665/how-to-resolve-symbolic-links-in-a-shell-script
####
# get the absolute path of the executable
function getAbsResolvedPathAndName() {
  SELF_PATH=$(cd -P -- "$(dirname -- "$1")" && pwd -P) && SELF_PATH=$SELF_PATH/$(basename -- "$1")

  # resolve symlinks
  while [ -h $SELF_PATH ]; do
    # 1) cd to directory of the symlink
    # 2) cd to the directory of where the symlink points
    # 3) get the pwd
    # 4) append the basename
    DIR=$(dirname -- "$SELF_PATH")
    if [ "$SELF_PATH" == "" ]; then
      return
    fi
    SYM=$(readlink $SELF_PATH)
    SELF_PATH=$(cd $DIR && cd $(dirname -- "$SYM") && /bin/pwd -P)/$(basename -- "$SYM")
  done
}
########################################################################
####### Function Decleration End ################

####### Execution Start ################


invokedCmdLine=$0

## Absolutely resolve the path and links of current file
getAbsResolvedPathAndName $invokedCmdLine

myAbsPath=$SELF_PATH

BASE_DIR=$(dirname -- "$myAbsPath")

for (( c=1; c<=3; c++ ))
do
	BASE_DIR=$(dirname -- "$BASE_DIR")
done


cd $BASE_DIR

#echo "ashutosh"
SYMFONY_SCRIPT=$BASE_DIR/symfony

LOG_FILE_EXT="log"
CDATE=$(date +%d-%b-%y);
SYMFONY_ARGS="--trace pay4me:monitorEpJob"
LOG_FILE_DIR_BASE="monitorlogs"
LOG_DIR=$BASE_DIR/log/$LOG_FILE_DIR_BASE
mkdir -p $LOG_DIR
LOG_FILE_PREFIX="monitor"

## $PHP_PATH $SYMFONY_SCRIPT $SYMFONY_ARGS
#while `/bin/true`
#do
    LOG_FILE=$LOG_DIR/$LOG_FILE_PREFIX-$CDATE.$LOG_FILE_EXT
    $PHP_PATH $SYMFONY_SCRIPT $SYMFONY_ARGS >>$LOG_FILE 2>&1
#done

