<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gatewayOrderclass
 *
 * @author spandey
 */
class gatewayOrderManager{
    //put your code here

   public function updateGatewayOrderValidationNo($validationNo,$orderId)
   {
        $updated = Doctrine::getTable('GatewayOrder')->updateValidationNo($validationNo,$orderId);
        return true;
   }

   public function updateGatewayOrderValidationNoStatus($validationNo,$status,$orderId,$transactionDate = "")
   {
        $updated = Doctrine::getTable('GatewayOrder')->updateValidationNoStatus($validationNo,$status,$orderId,$transactionDate);
        return true;
   }

   public function getValidationNumber($transactionNo)
   {
        $returnRecordSet =  Doctrine::getTable('Transaction')->getValidationNumber($transactionNo);
        return $returnRecordSet;
   }

   public function checkPaymentModeOption($paymentModeOptionId)
   {
        $paymentModeArray = Array(2,3,5);
        if(in_array($paymentModeOptionId,$paymentModeArray))
        {
            return true;
        }
      return false;
        
   }
}
?>
