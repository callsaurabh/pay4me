<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of billerManagerclass
 *
 * @author vrai
 */ 
class billerManager {

   public  $browserInstance = NULL;
    //method to send ref_num and identification num, to the biller
    public function getSearchXml($svcParams)
    {
       // echo "<pre>"; print_r($svcParams); die;
      $merName = $this->getMerchantServiceDetails($svcParams['merchantServiceId']);
     
      if($merName[0]['name'])
      {       
        return $this->getBillerSearchXML($svcParams);
      }
    }
    /*WP032 function To search biller xml */
    public function getBillerSearchXML($billerArr)
    {
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $this->setXMLroot($doc, 'biller-search');
        $root = $doc->appendChild($root);

        $merchant_service = $doc->createElement("merchant-service");
        $merchant_service->setAttribute ( "id", $billerArr['merchantServiceId'] );

        $identification_num = $doc->createElement("identification-num");
        $identification_num->appendChild($doc->createTextNode(ltrim($billerArr['identification_num'])));

        //      $ref_num = $doc->createElement("ref-num");
        //      $ref_num->appendChild($doc->createTextNode($billerArr['ref_num']));

        $root->appendChild($merchant_service);
        $root->appendChild($identification_num);
        //     $root->appendChild($ref_num);

        $xmldata = $doc->saveXML();
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        $this->createLog($xmldata, "searchOrder");
        return  $xmldata;
    }

    public function getAgentSearchXml($svcParams){
        // echo "<pre>"; print_r($svcParams); die;
      $merName = $this->getMerchantServiceDetails($svcParams['merchantServiceId']);
      //echo "<pre>"; print_r($svcParams); die();
      if($merName[0]['name'] == "IFA")
      {
       $ifaManagerObj = ifaServiceFactory::getService();
       return $ifaManagerObj->getAgentSearchXml($svcParams);
      }
    }



    public function validateSearchResponse($xmlResponse,$version="v1"){
      $xdoc=$this->loadXml($xmlResponse);
      $config_dir = sfConfig::get('sf_config_dir') ;
      if($version == 'v2') {
        $priceRangePve  = '100000000000';
        $priceRangeNve  = '-100000000000';
        $xmlschema = $config_dir.'/xmlschema/billerV2.xsd';
      }else {
        $priceRangePve  = '1000000000';
        $priceRangeNve  = '-1000000000';
        $xmlschema = $config_dir.'/xmlschema/biller.xsd';
      }

      $handle = @fopen($xmlschema, "r");

      if (@$xdoc->schemaValidate($xmlschema))
      {
         $rootName=$xdoc->firstChild->nodeName;

         if($rootName=="order"){
             $merchantServicePrice= $xdoc->getElementsByTagName('merchant-service');
             $merchantRequestXmlPrice = $merchantServicePrice->item(0)->getElementsByTagName('price')->item(0)->nodeValue;
             if(!empty($merchantRequestXmlPrice)) {
                   if($merchantRequestXmlPrice > $priceRangePve || $merchantRequestXmlPrice < $priceRangeNve) {
                       $rootName = '';
                   }
             }
         }


        switch($rootName) {
         case "order" :
            return  $this->processOrderXml($xdoc,$version,$xmlResponse);
             break ;
         case "biller-agent-detail" :
            return  $this->processBillerAgentDetailsXml($xdoc);
             break ;
         case "error" :
            return  $this->processErrorXml($xdoc);
            break ;
         default;
             $dataArr = array();
             $responseArr = array(0, $dataArr);
             return $responseArr;
             break;
        }

      }else{          
             $dataArr = array();
             $responseArr = array(0, $dataArr);
             return $responseArr;
             break;
      }
 }





        public function processOrderXml($xdoc,$version="v1",$xmlResponse = ""){
            $params  = $xdoc->getElementsByTagName('parameter');
            $j=0;
            $parametersArr = array() ;
            foreach($params as $param)
            {
                $parametersArr[$params->item($j)->getAttribute('name')] = $params->item($j)->nodeValue ;
                $j++ ;
            }


            $amount = $xdoc->getElementsByTagName('price')->item(0)->nodeValue;
            $parametersArr['amount'] = $amount;
            $parametersArr['merSvcId'] = $xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id');
            $parametersArr['transaction_num'] = $xdoc->getElementsByTagName('transaction-number')->item(0)->nodeValue;
            if ($version == 'v1') {
                $parametersArr['is_auto_generated'] = $xdoc->getElementsByTagName('is-auto-generated')->item(0)->nodeValue;
                $currency = $xdoc->getElementsByTagName('currency')->item(0)->nodeValue;
                $currObj = Doctrine::getTable('CurrencyCode')->findByCurrency($currency);
                $parametersArr['currency'] = $currObj->getfirst()->getCurrencyNum();
            } else {
                $parametersArr['currency'] = $xdoc->getElementsByTagName('payment')->item(0)->getElementsByTagName('currency')->item(0)->getAttribute('code');
            }
            $parametersArr['item_num'] = $xdoc->getElementsByTagName('item')->item(0)->getAttribute('number');
            $parametersArr['name'] = $xdoc->getElementsByTagName('name')->item(0)->nodeValue;
            $parametersArr['version'] = $version;
            $parametersArr['orderxml'] = $xmlResponse;

            $responseArr = array(1, $parametersArr);
           
            return $responseArr;
            //    if($parametersArr['type'] == "IFA")
            //  {
            //   $ifaManagerObj = new ifaManager();
            //return $this->getSearchReponse($xdoc,$parametersArr);
            //}
        }


      public function getSearchReponse($xdoc,$parametersArr) {         
        $merSvcId = $xdoc->getElementsByTagName('merchant-service')->item(0)->getAttribute('id');
        $transaction_num = $xdoc->getElementsByTagName('transaction-number')->item(0)->nodeValue;
        $is_auto_generated = $xdoc->getElementsByTagName('is-auto-generated')->item(0)->nodeValue;
        $item_num = $xdoc->getElementsByTagName('item')->item(0)->getAttribute('number');
        $identificationNum = $parametersArr['identification_num'];

     

        //$refNum = $parametersArr['ref_num'];
        $type = $parametersArr['type'];
        $fName = $parametersArr['first_name'];
        $lName = $parametersArr['surname'];
        //$isAutoGenerated = $parametersArr['is_auto_generated'];
        $mobile = $parametersArr['mobile'];
        $amount = $parametersArr['amount'];

        //$dataArr = array($identificationNum,$refNum,$type,$fName,$lName,$is_auto_generated,$mobile,$amount, $transaction_num, $item_num, $merSvcId);
        $dataArr = array($identificationNum,$type,$fName,$lName,$is_auto_generated,$mobile,$amount, $transaction_num, $item_num, $merSvcId);
        $responseArr = array(1, $dataArr);
        return $responseArr;
    }
     

     public function processBillerAgentDetailsXml($xdoc){
        $params  = $xdoc->getElementsByTagName('parameter');
              $j=0;
              $parametersArr = array() ;
              foreach($params as $param)
              {
                $parametersArr[$params->item($j)->getAttribute('name')] = $params->item($j)->nodeValue ;
                $j++ ;
              }
              $amount = $xdoc->getElementsByTagName('price')->item(0)->nodeValue;
              $parametersArr['amount'] = $amount;
              if($parametersArr['type'] == "IFA")
              {
                $ifaManagerObj = new ifaManager();
                return  $ifaManagerObj->getIfaAgentSearchReponse($xdoc,$parametersArr);
              }

     }


     public function processErrorXml($xdoc){        
        $error_num = $xdoc->getElementsByTagName('error-code')->item(0)->nodeValue;
        $error_msg = $xdoc->getElementsByTagName('error-message')->item(0)->nodeValue;

        $dataArr = array($error_num, $error_msg);
        $responseArr = array(0, $dataArr);
        return $responseArr;
     }



   public function getOrderNotificationXml($dataArr){
    //if($dataArr['svc_type'] == "IFA"){
      /* WP032 Changes in billers*/
       $notificationXml = $this->generateBillerOrderNotifyXml($dataArr);

       $xdoc=$this->loadXml($notificationXml);
       $plugin_dir = sfConfig::get('sf_config_dir') ;
       $xmlschema = $plugin_dir.'/xmlschema/biller.xsd';
       $handle = @fopen($xmlschema, "r");

            if ($xdoc->schemaValidate($xmlschema)) {
               $responseArr = array(1, $notificationXml);
                return $responseArr;
            }
            else {
                $responseArr = array(0, '');
                return $responseArr;
            }
   }

   private function getValidationRules($merchant_service_id) {
       return $validationRuleObj = Doctrine::getTable('ValidationRules')->findByMerchantServiceId($merchant_service_id);
   }
   /* WP032 function for order notification xml*/
   private function generateBillerOrderNotifyXml($dataArr)
   {    
       $updatedAt = date('Y-m-d H:i:s');
       $updateDateAArr = explode(" ", $updatedAt);

       $updatedAt = $updateDateAArr[0].'T'.$updateDateAArr[1];

       $doc = new DomDocument('1.0');
       $doc->formatOutput = true;

       $root = $this->setXMLroot($doc, 'order');

       $root = $doc->appendChild($root);
       $merchant_service = $doc->createElement("merchant-service");
       $merchant_service->setAttribute ( "id", $dataArr['merchant_svc_id'] );
       $item_number = $doc->createElement( "item");
       $item_number->setAttribute ( "number", $dataArr['item_num'] );

       //transaction number
       $transaction_number = $doc->createElement("transaction-number");
       $transaction_number->appendChild($doc->createTextNode($dataArr['transaction_num']));

       //auto generated
       $auto_generated = $doc->createElement("is-auto-generated");
       $auto_generated->appendChild($doc->createTextNode(''));

       //payment Information
       $merchantServiceObj = Doctrine::getTable('MerchantService')->find($dataArr['merchant_svc_id']);       
       $name = $doc->createElement("name");
       $name->appendChild($doc->createTextNode($dataArr['name'])); //WP032  name from orderNotification
       $price = $doc->createElement("price");
       $price->appendChild($doc->createTextNode($dataArr['amount_paid']));
       $description = $doc->createElement("description");
       $description->appendChild($doc->createTextNode(''));
       $currency = $doc->createElement("currency");
       $currency->appendChild($doc->createTextNode('naira'));      
      

       // validation rule
       $params = $doc->createElement("parameters");
       $validationRuleObj = $this->getValidationRules($dataArr['merchant_svc_id']);
       foreach ($validationRuleObj as $val) {
            $nodeVal = "";
            if (array_key_exists($val->getParamName(),$dataArr))
            {
                $nodeVal = $dataArr[$val->getParamName()];
            }
            $paramName = $doc->createElement("parameter");
            $paramName->setAttribute('name',$val->getParamName());
            $paramName->appendChild($doc->createTextNode($nodeVal));
            $params->appendChild($paramName);
       }

       $transactionLocation = $doc->createElement("transaction-location");// Added after code review WP032
       $transactionLocation->appendChild($doc->createTextNode('biller'));

     
      
       //start appending the child to parent
       $root->appendChild($merchant_service);
       $merchant_service->appendChild($item_number);
       $item_number->appendChild($transaction_number);
       $item_number->appendChild($auto_generated);
       $item_number->appendChild($name);
       $item_number->appendChild($description);
       $item_number->appendChild($price);
       $item_number->appendChild($currency);



       $item_number->appendChild($params);

       $item_number->appendChild($transactionLocation);

       $xmldata = $doc->saveXML();

       $xdoc = new DomDocument;
       $isLoaded = $xdoc->LoadXML($xmldata);
       $this->createLog($xmldata, "billerOrderNotify");
       return $xmldata;


   }
   public function setXMLroot($doc,$xmlRootName)
   {
        $root = $doc->createElement($xmlRootName);
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder/v1 biller.xsd' );
        return $root;
   }
   public function getPaymentNotificationXml($dataArr,$type){
   
       $ifaManagerObj = new ifaManager();
       $notificationXml = $ifaManagerObj->generateIfaPaymentNotifyXml($dataArr);

       $xdoc=$this->loadXml($notificationXml);
       $plugin_dir = sfConfig::get('sf_config_dir') ;
       $xmlschema = $plugin_dir.'/xmlschema/biller.xsd';
       $handle = @fopen($xmlschema, "r");

       if ($xdoc->schemaValidate($xmlschema)) {
           //$responseArr = array(1, $notificationXml);
           return $notificationXml;
       }else{
           //$responseArr = array(0, '');
           return false;
       }
         
   }



    public function loadXml($xmldata)
    {
        $xdoc = new DomDocument;
        @$xdoc->LoadXML($xmldata);
        return $xdoc;
    }

    public function updateBillerMerchantRequest($merchentRequestId,$payment_mode,$add_bank_charges=1,$bank_id, $bank_branch_id, $bank_name, $paid_amount )
    {
        $pfm_merchant_request = Doctrine::getTable('MerchantRequest')->find(array($merchentRequestId));
        $merchant_service_id = $pfm_merchant_request->getMerchantServiceId();
        $item_fee = $pfm_merchant_request->getItemFee();
        $payment_mode_option_id = payForMeManager::getPMOIdByConfName($payment_mode);
        $charges = payForMeManager::getCharges($merchant_service_id, $payment_mode_option_id);
        $financial_institution_charges = $charges['financial_institution_charges'];//payForMeManager::getBankAmount();
        $pay4me_charges = $charges['payforme_charges'];
        $service_charges = payForMeManager::getServiceCharges($merchant_service_id,$item_fee);
        $payforme_amount = $pay4me_charges+$service_charges;
        $pfm_merchant_request->setBankCharge($pay4me_charges+$financial_institution_charges);
        $pfm_merchant_request->setPaidDate(date('Y-m-d H:i:s'));
        $pfm_merchant_request->setPaidAmount($paid_amount);
        $pfm_merchant_request->setPaymentStatusCode(0);
        $pfm_merchant_request->setBankId($bank_id);
        $pfm_merchant_request->setBankBranchId($bank_branch_id);
        $pfm_merchant_request->setBankName($bank_name);
        $pfm_merchant_request->setServiceCharge($service_charges);
        $pfm_merchant_request->setBankAmount($financial_institution_charges);
        $pfm_merchant_request->setPayformeAmount($payforme_amount);
        $pfm_merchant_request->setPaymentModeOptionId($payment_mode_option_id);
        $pfm_merchant_request->save();
    }

    public function createLog($xmlData,$fileName)
    {
              //pay4MeUtility::setLog($xmlData, 'orderNotification');

      $logObj = new pay4meLog();
      $logObj->createLogData($xmlData, $fileName, 'billerLog');
    }


    public function settingHeaderInfo($merchantServiceId){
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $merchantDetail  = $payForMeObj->getMerchantIdByService($merchantServiceId);
            $header = "";
            if(count($merchantDetail)){
            $merchantCode = $merchantDetail[0]['merchant_code'];
            $merchantKey = $merchantDetail[0]['merchant_key'];
            $merchantAuthString = $merchantCode .":" .$merchantKey;
            // $logger->info("auth string: $merchantAuthString");
            $merchantAuth = base64_encode($merchantAuthString) ;

            $header['Authorization'] = "Basic $merchantAuth";
            $header['Content-Type'] = "application/xml;charset=UTF-8";
            $header['Accept'] = "application/xml;charset=UTF-8";
            }
            return $header;
    }


    public function updateTransaction($transactionNum){
      Doctrine::getTable('Transaction')->updateTransaction($transactionNum);
    }

    public function getMerchantServiceDetails($merchantServiceId){
      return  Doctrine::getTable('MerchantService')->getMerchantServiceName($merchantServiceId);
    }

    public function getPaymentStatus($item_number,$merchant_service_id){
      return Doctrine::getTable('MerchantItem')->getPaymentStatus($item_number,$merchant_service_id);
    }

    public function getMerchantRequestDetails($merchentRequestId){
      return Doctrine::getTable('MerchantRequest')->findById($merchentRequestId);
    }


    public function getMerchantServiceId($merchantServiceName){
        $q = Doctrine::getTable('MerchantService')->findByName($merchantServiceName);
        return  $paymentModeOptionId = $q[0]->getId();
    }
    



     public function notification($xmlRequest, $request, $merchantServiceId) {
        $merchant_service_details = $this->getMerchantServiceDetails($merchantServiceId);
        $url = $merchant_service_details[0]['notification_url'];

        $browser = $this->getBrowser() ;

        //setting header data
        $header = $this->settingHeaderInfo($merchantServiceId);
//echo "<pre>";
//print_r($header);
//die;
        $browser->post($url, $xmlRequest, $header);

        $code = $browser->getResponseCode();

        if ($code != 200 ) {
            // $logger->info("XML data from payforme as notification:".$browser->getResponseText());
            //$respMsg = $browser->getResponseMessage();
            //$respText = $browser->getResponseText();
            //throw new Exception("Error in posting notification:".PHP_EOL.$code.' '.$respMsg.PHP_EOL.$respText);
            return "Unable to send request.";
        }else{

            $xmlResponse = $browser->getResponseText();
            //pass the xml for log purpose            
            $this->createLog($xmlResponse,'orderNotification');//print $xmlResponse;exit;
            
            $version = $this->getVersion($xmlResponse);
            $searchResponseArr = $this->validateSearchResponse($xmlResponse,$version);
          //  
           
            if($searchResponseArr[0] == 0) {
                $errMsg = $this->processErrorMsg($searchResponseArr[1]);
                return $errMsg;
            }elseif($searchResponseArr[0] == 1) {
                return $searchResponseArr[1];
            }
        }
        //$this->logMessage('notification send') ;

        //sfView::NONE;

    }


    public function processErrorMsg($errorArr){       
       if(count($errorArr)>0&&$errorArr[0] == 505){
           return $errorArr[1];
       }else{
          return "Request cannot be fulfilled.";
       }
    }



    public function getBrowser() {
        if(!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }


    public function doPayment($transactionNum,$merchentRequestId,$merchantId){
           $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
           $formData = $payForMeObj->getTransactionRecord($transactionNum);
            // Get bank user details and bank details
            $gUser = sfContext::getInstance()->getUser()->getGuardUser();
            $bUser = $gUser->getBankUser();
            $bank_id = $bUser->getFirst()->getBank()->getId();
            $bank_branch_id = $bUser->getFirst()->getBankBranch()->getId();
            $bank_name = $bUser->getFirst()->getBank()->getBankName();
            //print "<pre>";
            //print_r($formData);
            $paid_amount = $formData['total_amount'] ;

            $postDataArray = array();
            $postDataArray['bank_id'] = $bank_id;
            $postDataArray['bank_branch_id'] = $bank_branch_id;
            $postDataArray['bank_name'] = $bank_name;
            $postDataArray['paid_amount'] = $paid_amount;
            $postDataArray['payment_mode_option_id'] = 1;
            $postDataArray['payment_status_code'] = 0;
            $postDataArray['paid_date'] =date('Y-m-d H:i:s');
            $postDataArray['id'] = $merchentRequestId;
            $postDataArray['merchant_id'] = $merchantId;

            //making the payment by updating tables
            $paymentObj = new payForMeManager();
            $con = $this->getConnectionObj();

            try {
                $con->beginTransaction();
                //$paymentObj->updateMerchantRequest($merchentRequestId,'bank');
                $flag = $paymentObj->doPay($postDataArray, $transactionNum, true);

                //accounting
                if($flag) {
                    $merchantPaymentTransactionObj = Doctrine::getTable('MerchantpaymentTransaction')->findByTransactionNumber($transactionNum);
                    $validationNumber = $merchantPaymentTransactionObj->getFirst()->getValidationNumber();
                    $paymentObj->splitPayment($transactionNum,$validationNumber);
                }

                $con->commit();
            }
            catch (Exception $e)  {
                $con->rollback();
                throw $e;
            }
    }


    public function getConnectionObj(){
        return Doctrine_Manager::connection();
    }


    public function getBillerId($billerName){      
        switch($billerName) {
         case $billerName :    /* WP032 Changes in billers for figra*/
            return $billerServiceArr = $this->getMerchantServiceId($billerName);
             break ;
         default;
             echo 'Invalid Request';
             break;
        }
    }

    private function getVersion($xmlResponse){       
        $xdoc = new DomDocument;
        $xdoc->LoadXML($xmlResponse);
        $version = "";
        $rootName = $xdoc->firstChild->nodeName;
        $versionString =  $xdoc->getElementsByTagName($rootName)->item(0)->getAttribute('xmlns');
        if (strpos($versionString,"v2"))
            $version = 'v2';
        else
            $version = 'v1';

        return $version;

    }
//    private function checkBillerType($type)
//    {
//
//    }
}
?>
