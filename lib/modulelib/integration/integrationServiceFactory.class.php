<?php
class integrationServiceFactory{

/**
  *
  * @param <type> $type
  * @return integrationService the service implementation
  */
 public static function getService($type) {

     switch($type) {
         case "nis" :
             return nisIntegrationServiceFactory::getService('nis');
             break ;
         case "nipost" :
             return nipostIntegrationServiceFactory::getService('nipost');
             break ;
         case "eproc" :
             return eprocIntegrationServiceFactory::getService('eproc');
             break ;
         case "biller" :
             return billerServiceFactory::getService();
             break ;
     }   
 }
}
?>
