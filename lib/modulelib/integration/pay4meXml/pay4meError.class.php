<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class pay4meError {


  public static $PAYMENT_SUCCESS = 0;
  public static $NO_PAYMENT_ATTEMPT = 1;
  public static $ITEM_PAYMENT_ALREADY_DONE = 2;
  public static $INVALID_XML = 3;
  public static $INVALID_SERVICE = 4;
  public static $INVALID_MERCHANT = 5;
  public static $AUTHENTICATION_FAILURE = 6;
  public static $MANDATORY_PARAM_MISSING = 7;
  public static $INVALID_PARAM_INTEGER = 8;
  public static $INVALID_PARAM_STRING = 9;
  public static $UNSUPPORTED_PARAM = 10;
  public static $INVAID_ITEM_REQUEST = 11;
  public static $INACTIVE_SERVICE = 12;
  public static $INVALID_BANK = 13;
  public static $INVALID_CURRENCY = 14;
  public static $INVALID_PAYMENT_MODE = 15;
  public static $INVALID_SPLIT = 16;
  public static $INVALID_MERCHANT_ACCOUNT = 17;
  public static $INVALID_VERSION = 18;
  public static $NO_RECORD_FOUND = 19;
  public static $INVALID_MERCHANT_ACCOUNT_CURRENCY = 20;
  public static $MERCHANT_NOT_CONFIGURED_WITH_EWALLET = 21;
  public static $INVALID_TRANSACTION_NUMBER = 22;
  public static $INVALID_ITEM_NUMBER = 23;
  public static $INVALID_PRICE = 24;
  public static $INVALID_PRICE_LIMIT = 25;
  public static $INVALID_PRICE_LIMIT_V2 = 26;




  public static $MSG_ERR_0 = 'Payment Successful';
  public static $MSG_ERR_1 = 'No payment attempt';
  public static $MSG_ERR_2 = 'Merchant Request – Payment has already been made for this Item';
  public static $MSG_ERR_3 = 'Merchant Request - Invalid XML Found';
  public static $MSG_ERR_4 = 'Merchant Request - Service Not Found';
  public static $MSG_ERR_5 = 'Merchant Validation Failed';
  public static $MSG_ERR_6 = 'Authentication Failure';
  public static $MSG_ERR_7 = '<parameter> mandatory field missing';
  public static $MSG_ERR_8 = 'Type mismatch. Expecting String. Found <parameter>';
  public static $MSG_ERR_9 = 'Type mismatch. Expecting Integer. Found <parameter>';
  public static $MSG_ERR_10 ='Found Unsupported parameters :  <parameter>';
  public static $MSG_ERR_11 ='No Request has been made for this item';
  public static $MSG_ERR_12 ='Service not active';
  public static $MSG_ERR_13 ='Bank Validation Failed';
  public static $MSG_ERR_14 ='Invalid Currency Found For Merchant Service';
  public static $MSG_ERR_15 ='Invalid Payment Mode Found For Merchant Service';
  public static $MSG_ERR_16 ='Split Information is invalid';
  public static $MSG_ERR_17 ='Merchant Account Number is  invalid';
  public static $MSG_ERR_18 ='Invalid Version';
  public static $MSG_ERR_19 ='No Record Found';
  public static $MSG_ERR_20 ='Invalid Currency For Merchant Account';
  public static $MSG_ERR_21 ='Merchant is not configured with eWallet account';
  public static $MSG_ERR_22 ='Merchant Request - Invalid <transaction-number> in XML';
  public static $MSG_ERR_23 ='Merchant Request - Invalid Number for <item> in XML';
  public static $MSG_ERR_24 ='Merchant Request - Invalid <price> in XML';
  public static $MSG_ERR_25 ='Merchant Request - Amount should not be more that <1000000000>';
  public static $MSG_ERR_26 ='Merchant Request - Amount should not be more that <100000000000>';

  public function error($code, array $params=NULL) {

    $logger=sfContext::getInstance()->getLogger();
    $flag = 1;
    $description  = $this->getFormattedDescription($code, $params);
    if(!$flag) {
      $logger->info("Throwing the exception");
      throw new Exception($description);
    }
    if($flag) {
      $logger->info("Error found in XML. Sending Error Xml with Error Code - ".$code);

      $obj = new pay4MeXMLV1();
      $errorXml = $obj->generateErrorNotification($code, $description);
      if($errorXml) {
        return $errorXml;
      }
    }
  }

  public function errorhistory($code, array $params=NULL) {
    $logger=sfContext::getInstance()->getLogger();
    $flag = 1;
    $description  = $this->getFormattedDescription($code, $params);
    if(!$flag) {
      $logger->info("Throwing the exception");
      throw new Exception($description);
    }
    if($flag) {
      $logger->info("Error found in XML. Sending Error Xml with Error Code - ".$code);

      $obj = new pay4MeXMLV1();
      $errorXml = $obj->generateErrorHistoryNotification($code, $description);
      if($errorXml) {
        return $errorXml;
      }
    }
  }

  public function getFormattedDescription($code,array $params=NULL) {


    $desc_var = "MSG_ERR_".$code;
    $description = self::$$desc_var;
    if(count($params)) {
      foreach($params as $key=>$value) {
        $description = str_replace('<'.$key.'>', $value, $description);
      }
    }
    return $description;
  }


}

?>
