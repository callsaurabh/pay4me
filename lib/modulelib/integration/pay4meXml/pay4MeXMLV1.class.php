<?php

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class pay4MeXMLV1 {

    public function generatePay4MeOrderRedirect($url) {


        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('order-redirect');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder payformeorderV1.xsd' );
        $root = $doc->appendChild($root);
        $redirect_url = $doc->createElement("redirect-url");
        $redirect_url->appendChild($doc->createTextNode($url));
        $root->appendChild($redirect_url);
        $xmldata = $doc->saveXML();

        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);



        //validate the XML
        $xmlValidatorObj = new validatePay4meXML() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "V1") ;
        if($isValid) {
            return $xmldata;
        }
        else {
            return false;
        }


        //      $orderRedirectXml = "<order-redirect xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder'   xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder payformeorder.xsd'>" ;
        //
        //      $orderRedirectXml .= "<redirect-url>" ;
        //      $orderRedirectXml .=  $this->xml_character_encode(url_for("@order_pay?order=".$this->redirectKey,true));
        //      $orderRedirectXml .=  "</redirect-url>";
        //      $orderRedirectXml .=  "</order-redirect>";
        //      $logger->info('sending redirect XML');
        //      $logger->log(print_r($orderRedirectXml, true)) ;
        //      return $orderRedirectXml;

    }

    public function generatePaymentNotificationXML($transaction_no) {
		//echo $transaction_no;die;
    	//$transaction_no = '6497927';
        $logger=sfContext::getInstance()->getLogger();
        //get all the details of the transaction_no from database
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getNotificationTransactionDetails($transaction_no);
        if(count($pfmTransactionDetails) > 0) {
            $itemNumber  =  $pfmTransactionDetails['itemNumber'];
            $merchantServiceId = $pfmTransactionDetails['MerchantRequest']['MerchantService']['id'];
            $transactionNumber = $pfmTransactionDetails['MerchantRequest']['txn_ref'];
            $itemFee = $pfmTransactionDetails['MerchantRequest']['item_fee'];
            $updatedAt = $this->convertDbDateToXsdDate($pfmTransactionDetails['MerchantRequest']['updated_at']);
            $paymentMode = strtolower($pfmTransactionDetails['MerchantRequest']['PaymentModeOption']['name']);
            $bankName = $pfmTransactionDetails['MerchantRequest']['Bank']['bank_name'];
            $branchName = $pfmTransactionDetails['MerchantRequest']['BankBranch']['name'];;
            $paymentStatusCode = $pfmTransactionDetails['MerchantRequest']['payment_status_code'];
            $validationNumber = $pfmTransactionDetails['MerchantRequest']['validation_number'];
            if($paymentMode == 'check') {
                $paymentMode = 'cheque';
            }


            if($paymentStatusCode==2) {
                $desc = 'Unsuccessful payment';
            }
            else if($paymentStatusCode=='0') {
                $desc = 'Payment successfully done';
            }
            else if($paymentStatusCode==1) {
                $desc = 'Not any payment request';
            }

            // xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1'
            //       xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorder.xsd


            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;
            $root = $doc->createElement('payment-notification');
            $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
            $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v1' );
            $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder/v1 payformeorderV1.xsd' );
            $root = $doc->appendChild($root);
            $merchant_service = $doc->createElement("merchant-service");
            $merchant_service->setAttribute ( "id", $merchantServiceId );
            $item_number = $doc->createElement( "item");
            $item_number->setAttribute ( "number", $itemNumber );

            //transacction number
            $transaction_number = $doc->createElement("transaction-number");
            $transaction_number->appendChild($doc->createTextNode($transactionNumber));

            $validation_number = $doc->createElement("validation-number");
            $validation_number->appendChild($doc->createTextNode($validationNumber));


            //payment Information
            $payment_information = $doc->createElement("payment-information");
            $amount = $doc->createElement("amount");
            $amount->appendChild($doc->createTextNode($itemFee));
            $currency = $doc->createElement("currency");
            $currency->appendChild($doc->createTextNode('naira'));
            $payment_date = $doc->createElement("payment-date");
            $payment_date->appendChild($doc->createTextNode($updatedAt));
            $mode = $doc->createElement("mode");
            $mode->appendChild($doc->createTextNode($paymentMode));

            $payment_mode = $doc->createElement("bank");
            $bank_name = $doc->createElement("name");
            $bank_name->appendChild($doc->createTextNode($bankName));
            $branch_name = $doc->createElement("branch");
            $branch_name->appendChild($doc->createTextNode($branchName));

            $status = $doc->createElement("status");
            $code = $doc->createElement("code");
            $code->appendChild($doc->createTextNode($paymentStatusCode));
            $description = $doc->createElement("description");
            $description->appendChild($doc->createTextNode($desc));


            //start appending the child to parent
            $root->appendChild($merchant_service);
            $merchant_service->appendChild($item_number);
            $item_number->appendChild($transaction_number);
            if(sfconfig::get('app_send_validation_number')){
                $item_number->appendChild($validation_number);
            }
            $item_number->appendChild($payment_information);
            $payment_information->appendChild($amount);
            $payment_information->appendChild($currency);
            $payment_information->appendChild($payment_date);
            $payment_information->appendChild($mode);
            $payment_information->appendChild($payment_mode);
            $payment_mode->appendChild($bank_name);
            $payment_mode->appendChild($branch_name);
            $payment_information->appendChild($status);
            $status->appendChild($code);
            $status->appendChild($description);

            $xmldata = $doc->saveXML();
	
            // $logger->info($xmldata);
            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);
            //validate the XML
            $xmlValidatorObj = new validatePay4meXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, "V1") ;
            if($isValid) {
                return $xmldata;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }


        //   return  $doc->saveXML();

    }

    public function generateErrorNotification($error_code, $desc) {
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('error');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/pay4meorder payformeorderV1.xsd' );
        $root = $doc->appendChild($root);
        $code = $doc->createElement("error-code");
        $code->appendChild($doc->createTextNode($error_code));
        $description = $doc->createElement("error-message");
        $description->appendChild($doc->createTextNode($desc));
        $root->appendChild($code);
        $root->appendChild($description);
        $xmldata = $doc->saveXML();

        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        //validate the XML
        $xmlValidatorObj = new validatePay4meXML() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "V1") ;
        if($isValid) {
            return $xmldata;
        }
        else {
            return false;
        }
    }

    public function generateErrorHistoryNotification($error_code, $desc) {
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('error');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance');
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/orderhistory/v1');
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/orderhistory/v1 orderHistoryV1.xsd');
        $root = $doc->appendChild($root);
        $code = $doc->createElement("error-code");
        $code->appendChild($doc->createTextNode($error_code));
        $description = $doc->createElement("error-message");
        $description->appendChild($doc->createTextNode($desc));
        $root->appendChild($code);
        $root->appendChild($description);
        $xmldata = $doc->saveXML();
        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);
        //validate the XML
        $xmlValidatorObj = new validatePay4meXML() ;
        $isValid = $xmlValidatorObj->validateHistoryXML($xdoc, "V1") ;
        if($isValid) {
           
            return $xmldata;
        }
        else {
          
            return false;
        }
    }

    protected function convertDbDateToXsdDate($dbDate) {
        $dtokens=explode(' ', $dbDate);
        $xdate = implode('T',$dtokens);
        return $xdate;
    }

    protected function xml_character_encode($string, $trans='') {
        $trans = (is_array($trans)) ? $trans : get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
        foreach ($trans as $k=>$v)
        $trans[$k]= "&#".ord($k).";";
        return strtr($string, $trans);
    }
    public function setResponseHeader($merchantServiceId){
        if(trim($merchantServiceId)){
            $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
            $merchantDetail  = $payForMeObj->getMerchantIdByService($merchantServiceId);
            $header = "";
            if(count($merchantDetail)){
                $merchantCode = $merchantDetail[0]['merchant_code'];
                $merchantKey = $merchantDetail[0]['merchant_key'];
                $merchantAuthString = $merchantCode .":" .$merchantKey;
                // $logger->info("auth string: $merchantAuthString");
                $merchantAuth = base64_encode($merchantAuthString) ;

                $header['Authorization'] = "Basic $merchantAuth";
                $header['Content-Type'] =  "application/xml;charset=UTF-8";
                $header['Accept'] = "application/xml;charset=UTF-8";
            }
            $this->setHeader($header);
        }
    }
    private function setHeader($header){       
        if(isset($header) && count($header) >0 ){
            foreach($header as $key=>$val){
                sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
            }
        }
    }
        public function generateSamlpleOrderXml($merhant_service_id){

        $service_detail=Doctrine::getTable('MerchantService')->getDataforSampleXml($merhant_service_id);
        if($service_detail && count($service_detail)>0)
        {
            $service_detail=$service_detail[0];


            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;
            $root = $doc->createElement('order');
            $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/pay4meorder/v1' );
            $root = $doc->appendChild($root);

            $bodyElm=$this->getMiddleContent($service_detail,$doc);
            $root->appendChild($bodyElm);

            $xmldata = $doc->saveXML();

            ///////////////Logging   ////////////////////////////////////////////////////
            $pay4meLog = new pay4meLog();
            $pay4meLog->createLogData($xmldata,'SampleOrderXml','SampleOrder');
            //////////////////////////////////////////////////////////////////////////////


            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);

            $merchant_code = $service_detail['Merchant']['merchant_code'];
            $merchant_key =  $service_detail['Merchant']['merchant_key'];
            $merchant_auth_string = $merchant_code .":" .$merchant_key ;
            $merchantAuth = base64_encode($merchant_auth_string) ;
            $pageURL = 'http';

             if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on"){
                $pageURL.= "s";
             }
             $pageURL .= "://";

             if($_SERVER["SERVER_PORT"] != "80"){
                 $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["SCRIPT_NAME"];//.$_SERVER["SCRIPT_FILENAME"];
             }else{
                 $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"];//.$_SERVER["SCRIPT_FILENAME"];
             }

//echo"<pre>";print_r($_SERVER);
            //sfConfig::set('app_pay4me_sandbox_url',$pageURL);

            //secho sfConfig::get('app_pay4me_sandbox_url');
            //$preUrl=sfConfig::get('app_pay4me_sandbox_url');
            $preUrl=$pageURL;
            
            $url =$preUrl."/order/payment/payprocess/v1/PID/".$merchant_code ;
            $header[] = "Authorization: Basic $merchantAuth";
            $header[] = "Content-Type: application/xml;charset=UTF-8";
            $header[] = "Accept: application/xml;charset=UTF-8 ";
            //validate the XML
            $xmlValidatorObj = new validatePay4meXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, "V1") ;
            if($isValid) {

                $xmldetail[0]=$url;
                $xmldetail[1]=$header;
                $xmldetail[2]=$xmldata;

                 return $xmldetail;
             }
            else {
                  return false;
            }

           // print_r($xmldata); die;
        }
        else
        { return false;}

    }

    public function getMiddleContent($seviceDetail,$doc){
        $serviceElm = $doc->createElement("merchant-service");
        $serviceElm->setAttribute("id",$seviceDetail['id']);

        $itemNumber=rand();

        $itemElm = $doc->createElement("item");
        $itemElm->setAttribute("number",$itemNumber);

        $TransNumber=rand();
        $TxnElm = $doc->createElement("transaction-number");
        $TxnElm->appendChild($doc->createTextNode($TransNumber));
        $itemElm->appendChild($TxnElm );

        $name="name";
        $fname = $doc->createElement("name");
        $fname->appendChild($doc->createTextNode($name));
        $itemElm->appendChild($fname );

        $description="description";
        $Desc = $doc->createElement("description");
        $Desc->appendChild($doc->createTextNode($description));
        $itemElm->appendChild($Desc );

        $price="25";
        $amt=$doc->createElement("price");
        $amt->appendChild($doc->createTextNode($price));
        $itemElm->appendChild($amt);

        $curr=$doc->createElement("currency");
        $curr->appendChild($doc->createTextNode(strtolower($seviceDetail['ServicePaymentModeOption'][0]['CurrencyCode']['currency'])));
        $itemElm->appendChild($curr);

        $payInfo=$doc->createElement("parameters");

        foreach($seviceDetail['ValidationRules'] as $validRules){

            $biller_n=$doc->createElement("parameter");
            $biller_n->setAttribute("name",$validRules['param_name']);

            if("integer"==$validRules['param_type'])
                $paramVal=rand(10,1000);
            else
                $paramVal="ABCD";
            $biller_n->appendChild($doc->createTextNode($paramVal));
            $payInfo->appendChild($biller_n);


        }

        $itemElm->appendChild($payInfo);

        $serviceElm->appendChild($itemElm);

        return $serviceElm;
    }
    public function setHistoryResponseHeader($merchant_code){

        if(trim($merchant_code)){
            $merchantDetail  = Doctrine::getTable('Merchant')->getMerchantDetailsByCode($merchant_code);           
            $header = array();
            if(isset($merchantDetail[0]) &&  count($merchantDetail[0])){
                $merchantCode = $merchantDetail[0]['merchant_code'];
                $merchantKey = $merchantDetail[0]['merchant_key'];
                $merchantAuthString = $merchantCode .":" .$merchantKey;
                // $logger->info("auth string: $merchantAuthString");
                $merchantAuth = base64_encode($merchantAuthString) ;

                $header['Authorization'] = "Basic $merchantAuth";
                $header['Content-Type'] =  "application/xml;charset=UTF-8";
                $header['Accept'] = "application/xml;charset=UTF-8";
            }
            $this->setHeader($header);
        }
    }
    public function generatePay4MeHistoryResponse($response) {

        $logger=sfContext::getInstance()->getLogger();
      
        if(count($response[0]['item']) > 0) {

            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;
            $root = $doc->createElement('order-history-response');
            $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
            $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/orderhistory/v1' );
            $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/orderhistory/v1 orderHistoryV1.xsd' );
            $root = $doc->appendChild($root);
           
                foreach($response as $service){
                    $merchant_service = $doc->createElement("merchant-service");
                    $merchant_service->setAttribute ( "id", $service['service_id'] );
                    if($service['service_error']){
                        $invalidservice = $doc->createElement("invalid-merchant-service");
                        $merchant_service->appendChild($invalidservice);
                    } else {
                        foreach($service['item'] as $item){
                            $itemnode = $doc->createElement("item-number");
                            $itemnode->setAttribute("number", $item['item_number']);
                            if($item['item_error']){
                                $invaliditem = $doc->createElement("invalid-item-number");
                                $itemnode->setAttribute("number", $item['item_number']);
                                $itemnode->appendChild($invaliditem);
                            } else {
                               
                                $transaction_number = $doc->createElement("transaction-number");
                                if(isset($item['transaction-number']))
                                    $transaction_number->appendChild($doc->createTextNode($item['transaction-number']));
                                else
                                    $transaction_number->appendChild($doc->createTextNode(""));
                                $validation_number = $doc->createElement("validation-number");
                                if(isset($item['validation-number']))
                                $validation_number->appendChild($doc->createTextNode($item['validation-number']));
                                else
                                    $validation_number->appendChild($doc->createTextNode(""));
                                $payment_information = $doc->createElement("payment-information");
                                $amount = $doc->createElement("amount");
                                if(isset($item['amount']))
                                    $amount->appendChild($doc->createTextNode($item['amount']));
                                else
                                    $amount->appendChild($doc->createTextNode(""));
                                $currency_code = $doc->createElement("currency-code");
                                if(isset($item['currency-code']))
                                    $currency_code->appendChild($doc->createTextNode($item['currency-code']));
                                else
                                    $currency_code->appendChild($doc->createTextNode(""));
                                $payment_date = $doc->createElement("payment-date");
                                if(isset($item['payment-date']))
                                    $payment_date->appendChild($doc->createTextNode($item['payment-date']));
                                else
                                    $payment_date->appendChild($doc->createTextNode(""));
                                $transaction_location = $doc->createElement("transaction-location");
                                if(isset($item['transaction-location']))
                                    $transaction_location->appendChild($doc->createTextNode($item['transaction-location']));
                                else
                                    $transaction_location->appendChild($doc->createTextNode(""));
                                $payment_mode = $doc->createElement("payment-mode");
                                if(isset($item['payment-mode']))
                                    $payment_mode->appendChild($doc->createTextNode($item['payment-mode']));
                                else
                                    $payment_mode->appendChild($doc->createTextNode(""));
                                $status = $doc->createElement("status");
                                $code = $doc->createElement("code");
                                if(isset($item['status-code']))
                                    $code->appendChild($doc->createTextNode($item['status-code']));
                                else
                                    $code->appendChild($doc->createTextNode(""));
                                $description = $doc->createElement("description");
                                $description->appendChild($doc->createTextNode($item['status-description']));
                                $status->appendChild($code);
                                $status->appendChild($description);

                                $payment_information->appendChild($amount);
                                $payment_information->appendChild($currency_code);
                                $payment_information->appendChild($payment_date);
                                $payment_information->appendChild($transaction_location);
                                $payment_information->appendChild($payment_mode);
                                $payment_information->appendChild($status);

                                $itemnode->appendChild($transaction_number);
                                $itemnode->appendChild($validation_number);
                                $itemnode->appendChild($payment_information);
                            }
                            $merchant_service->appendChild($itemnode);
                        }


                    }
                    $root->appendChild($merchant_service);
                                  
                }
            
            
            $xmldata = $doc->saveXML();
            $xdoc = new DOMDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);    
            //start here need to remove once done
            $logger->info($xmldata);
       
           // validate the XML
           $xmlValidatorObj = new validatePay4meXML() ;
           $isValid = $xmlValidatorObj->validateHistoryXML($xdoc, "V1") ;
            if($isValid) {
                return $xmldata;
            }
            else {
                $msg = "INVALID_XML";
                $logger=sfContext::getInstance()->getLogger();
                $logger->info("Exception in processing: ".$msg);
                return pay4MeUtility::errorhistory($msg,"",'V1',array());
            }
            
        }
        else{
                $msg = "NO_RECORD_FOUND";
                $logger=sfContext::getInstance()->getLogger();
                $logger->info("Exception in processing: ".$msg);
                return pay4MeUtility::errorhistory($msg,"",'V1',array());
        }
    }

}
?>
