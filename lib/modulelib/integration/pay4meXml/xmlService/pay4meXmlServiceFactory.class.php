<?php
class pay4meXmlServiceFactory{
/**
  *
  * @param <type> $type
  * @return nisService the service implementation
  */
    public static function getRequest($xdoc, $version, $merchant_id) {    

        $pay4meOrder = "pay4meOrder".strtoupper($version);
        $pay4meOrderObj = new $pay4meOrder($xdoc,$version);
        return $pay4meOrderObj;
    }

    /**
  *
  * @param <type> $type
  * @return Query HistoryService the service implementation
  */
    public static function getHistoryRequest($xdoc, $version, $merchant_code) {
      
        $pay4meHistory = "pay4meHistory".strtoupper($version);
        $pay4meHistoryObj = new $pay4meHistory($xdoc,$version,$merchant_code);
        return $pay4meHistoryObj;
    }
}
?>