<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meLog
 *
 * @author sdutt
 */
class pay4meLog {
private $filename ;

  public function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false)
  {
    $path = $this->getLogPath($parentLogFolder);
    if($appendTime){
        $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
    }else{
        $file_name = $path."/".$nameFormate.".txt";
    }
    $this->filename = $file_name;
    if($append)
    {
        @file_put_contents($file_name, $xmlData, FILE_APPEND);
    }else{
    $i=1;
    while(file_exists($file_name)) {
      $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
      $i++;
     }
     @file_put_contents($file_name, $xmlData);
    }
  }

public function getFileName(){
    return $this->filename;
}
  public function getLogPath($parentLogFolder)
  {

    $logPath =$parentLogFolder==''?'/pay4melog':'/'.$parentLogFolder;   
    $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='')
    {       
      $dir_path=$logPath."/";
      //chmod(sfConfig::get('sf_log_dir'), 0777);
      mkdir($dir_path,0777,true);     
      chmod($dir_path, 0777);
    }
    return $logPath;
  }

  public function createExclusivePaymentFile($txnId,$lock=1) {
    
    $path = $this->getLogPath("PaymentTransaction");
    $fileName = "Payment-".$txnId;
    $file_name = $path."/".$fileName.".txt";
    $fp = fopen($file_name, "w");
    if($lock) {     
      flock($fp, LOCK_EX);
    }
    else {  
      flock($fp, LOCK_UN);
    }
  }
   public function saveListFile($arrList,$filePath,$fileName) {
        try {           
            $downold_Path   = $filePath;//sfConfig::get('sf_upload_dir').$filePath;
            $strFile = $fileName;
            $final_path = sfConfig::get('sf_upload_dir').$filePath;
             if(!is_dir($final_path)){
                mkdir($final_path);
                chmod($final_path, 0755);

            }
            $filepath = $downold_Path .$strFile.".csv";
            $strFileName =$this->makeCsvFile($arrList, $filepath,",","\"");
            $arrFileInfo = pathinfo($strFileName);
            $path = "../../uploads/".$filePath.$arrFileInfo['basename'];
            return $strFile."#$".$path;
            //                $this->redirect_url($path);
        } catch(Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";die;
        }
    }
    public function makeCsvFile($dataArray,$filename,$delimiter=",",$enclosure="\"") {

        // Build the string
       $file_name =  sfConfig::get('sf_web_dir').'/uploads/'.$filename;
       
      @file_put_contents($file_name, $dataArray);
        return $filename;
    }
}
?>