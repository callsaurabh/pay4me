<?php
/**
 * Description of pay4meOrderNotificationclass
 *
 * @author uday
 */
class pay4meOrderNotification {

  public function generateXml($transObj)
  {

    if($transObj['MerchantRequest']['bank_id']){
      $bankDetails =  Doctrine::getTable('Bank')->find($transObj['MerchantRequest']['bank_id'])->toArray();
      $bankName = $bankDetails['bank_name'];
    }else{
      $bankName = "";
    }
    if($transObj['MerchantRequest']['bank_branch_id']){
      $branchDetails =  Doctrine::getTable('BankBranch')->find($transObj['MerchantRequest']['bank_branch_id'])->toArray();
      $branchName = $branchDetails['name'];
    }else{
      $branchName = "";
    }

    if($transObj['MerchantRequest']['payment_status_code']==2){
      $description = 'Unsuccessful payment';
    }
    else if($transObj['MerchantRequest']['payment_status_code']=='0'){
      $description = 'Payment successfully done';
    }
    if($transObj['MerchantRequest']['payment_status_code']==1){
      $description = 'Not any payment request';
    }



    $orderRedirectXml = "<payment-notification  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder/v1'
       xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder/v1 payformeorder.xsd'>";

    $orderRedirectXml .= "<merchant-service id='".$transObj['MerchantRequest']['merchant_service_id']."'>" ;

    $orderRedirectXml .= "<item number='".$transObj['MerchantRequest']['MerchantItem']['itemNumber']."'>" ;

    $orderRedirectXml .= "<transaction-number>".$transObj['MerchantRequest']['txn_ref'];
    $orderRedirectXml .=  "</transaction-number>";

    $orderRedirectXml .= "<payment-information>" ;

    $orderRedirectXml .= "<amount>".$transObj['MerchantRequest']['item_fee'] ;
    $orderRedirectXml .=  "</amount>";

    $orderRedirectXml .= "<currency>naira" ;
    $orderRedirectXml .= "</currency>";

    $orderRedirectXml .= "<payment-date>".$this->convertDbDateToXsdDate($transObj['MerchantRequest']['updated_at']);
    $orderRedirectXml .= "</payment-date>";

    $orderRedirectXml .= "<mode>".$transObj['MerchantRequest']['PaymentModeOption']['name'];
    $orderRedirectXml .= "</mode>";

    $orderRedirectXml .= "<bank>" ;
    $orderRedirectXml .= "<name>".$bankName;
    $orderRedirectXml .= "</name>" ;
    $orderRedirectXml .= "<branch>".$branchName;
    $orderRedirectXml .= "</branch>" ;
    $orderRedirectXml .=  "</bank>";

    $orderRedirectXml .= "<status>" ;
    $orderRedirectXml .= "<code>".$transObj['MerchantRequest']['payment_status_code'] ;
    $orderRedirectXml .= "</code>" ;
    $orderRedirectXml .= "<description>".$description ;
    $orderRedirectXml .= "</description>" ;

    $orderRedirectXml .=  "</status>";

    $orderRedirectXml .=  "</payment-information>";
    $orderRedirectXml .=  "</item>";
    $orderRedirectXml .=  "</merchant-service>";
    $orderRedirectXml .=  "</payment-notification>";
    return $orderRedirectXml;
  }

  protected function convertDbDateToXsdDate($dbDate) {
    $dtokens=explode(' ', $dbDate);
    $xdate = implode('T',$dtokens);
    return $xdate;
  }

}