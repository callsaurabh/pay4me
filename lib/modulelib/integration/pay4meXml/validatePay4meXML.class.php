<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of validatePay4meXMLclass
 *
 * @author anurag
 */
class validatePay4meXML {

    public function validateXML($xdoc, $version) {

        $config_dir = sfConfig::get('sf_config_dir');

        $xmlschema = $config_dir . '/xmlschema/payformeorder' . strtoupper($version) . '.xsd';
        //  $handle = @fopen($xmlschema, "r");
        libxml_use_internal_errors(true);
        try {
            if ($xdoc->schemaValidate($xmlschema)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function getXmlErrors() {



        $errors = libxml_get_errors();

        return $this->returnErrorMsg($errors);
    }

    private function returnErrorMsg($errors) {


        $array_of_errors = array("INVALID_TRANSACTION_NUMBER" => "<transaction-number>", "INVALID_ITEM_NUMBER" => "<item>", "INVALID_PRICE" => "<price>");



        foreach ($errors as $error_no => $error_details) {


            $value_in_array = explode("'", $error_details->message);
            $tagName = substr($value_in_array[1], -(strlen($value_in_array[1]) - strrpos($value_in_array[1], "}") - 1));
            foreach ($array_of_errors as $exception_name => $msg) {

                if ("<" . $tagName . ">" === $msg) {

                    return $exception_name;
                }
            }
        }

        libxml_clear_errors();
        return "INVALID_XML";
    }

    public function validateHistoryXML($xdoc, $version) {
        $config_dir = sfConfig::get('sf_config_dir');
        //$xmlschema = $config_dir . '/xmlschema/payformehistory'.strtoupper($version).'.xsd';

        $startDate = '';

        if (isset($xdoc->getElementsByTagName('start-time')->item(0)->nodeValue)) {
            $startDate = $xdoc->getElementsByTagName('start-time')->item(0)->nodeValue;
        }

        if ($startDate != '') {
        	$xmlschema = $config_dir . '/xmlschema/orderHistory' . strtoupper($version) . '.xsd';
        } else {
        	$appId = $xdoc->getElementsByTagName('app-id')->item(0)->nodeValue;
        	$refId = $xdoc->getElementsByTagName('ref-id')->item(0)->nodeValue;
        	if ($appId != '' && $refId != '' ) {
        		$xmlschema = $config_dir . '/xmlschema/orderHistory' . strtoupper($version) . '.xsd';
        	} else {
        		$xmlschema = $config_dir . '/xmlschema/orderRequest' . strtoupper($version) . '.xsd';
        	}
        }


        try {
            libxml_use_internal_errors(true);
            if ($xdoc->schemaValidate($xmlschema)) {

                return true;
            } else {
                $errors = libxml_get_errors();
                $errorMsg = "";
                foreach ($errors as $key => $error) {
                    $errorMsg .= $error->message;
                }
                $delimiter = "Element '{http://www.pay4me.com/schema/orderhistory/v1}";
                if (!empty($errorMsg)) {
                    $arrMessage = implode("'", explode($delimiter, $errorMsg));
                }

                return $arrMessage;
            }
        } catch (Exception $ex) {

            echo $ex->getMessage();
            return false;
        }
    }

}

?>
