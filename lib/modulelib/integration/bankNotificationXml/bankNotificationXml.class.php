<?php

class bankNotificationXml {

   function generateBankNotificationXML($transaction_no){
        $logger=sfContext::getInstance()->getLogger();
        //get all the details of the transaction_no from database
        $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
        $pfmTransactionDetails = $payForMeObj->getNotificationTransactionDetails($transaction_no);

        if(count($pfmTransactionDetails) > 0) {

            $transactionAmount = $pfmTransactionDetails['total_amount'];
            $validationNumber = $pfmTransactionDetails['MerchantRequest']['validation_number'];
            $bankName = $pfmTransactionDetails['MerchantRequest']['Bank']['bank_name'];
            $branchCode = $pfmTransactionDetails['MerchantRequest']['BankBranch']['branch_code'];
            $updatedBy = $pfmTransactionDetails['MerchantRequest']['updated_by'];
            $transactionId = $pfmTransactionDetails['pfm_transaction_number'];

            $paidAt = $pfmTransactionDetails['MerchantRequest']['updated_at'];
            $paidAtArr = explode(" ", $paidAt);
            $paidAt = $paidAtArr[0].'T'.$paidAtArr[1];


            $userDetails = Doctrine::getTable('sfGuardUser')->fetchUserDetailsById($updatedBy);
            $userName = $userDetails[0]['username'];


            $doc = new DomDocument('1.0');
            $doc->formatOutput = true;

            //getting the root element of xml
            $rootElementName = "bank-transaction";
            $root = $this->setXmlRoot($doc, $rootElementName);

            $root = $doc->appendChild($root);
           
            $transaction_amount = $doc->createElement("transaction-amount");
            $transaction_amount->appendChild($doc->createTextNode($transactionAmount));

            $username = $doc->createElement("username");
            $username->appendChild($doc->createTextNode($userName));

            $bank_name = $doc->createElement("bank-name");
            $bank_name->appendChild($doc->createTextNode($bankName));

            $branch_code = $doc->createElement("branch-code");
            $branch_code->appendChild($doc->createTextNode($branchCode));

            $paid_at = $doc->createElement("paid-at");
            $paid_at->appendChild($doc->createTextNode($paidAt));

            $validation_number = $doc->createElement("validation-number");
            $validation_number->appendChild($doc->createTextNode($validationNumber));

            $transaction_id = $doc->createElement("transaction-id");
            $transaction_id->appendChild($doc->createTextNode($transactionId));

            //start appending the child to parent
            $root->appendChild($transaction_amount);
            $root->appendChild($username);
            $root->appendChild($bank_name);
            $root->appendChild($branch_code);
            $root->appendChild($paid_at);
            $root->appendChild($validation_number);
            $root->appendChild($transaction_id);

            $xmldata = $doc->saveXML();

            // $logger->info($xmldata);

            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);

            //validate the XML
            $xmlValidatorObj = new validateBankXml() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, "1");
            if($isValid) {
                return $xmldata;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
    }



    public function validateTransRequestXml($xdoc,$xmldata){
        $xmlValidatorObj = new validateBankXml() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "1");
        return $isValid;
    }


    public function parseTransRequestXml($xdoc){
        $startDate = $xdoc->getElementsByTagName('start-date')->item(0)->nodeValue;
        $endDate = $xdoc->getElementsByTagName('end-date')->item(0)->nodeValue;
        $bank = $xdoc->getElementsByTagName('bank')->item(0)->nodeValue;
        $branchCode = $xdoc->getElementsByTagName('branch-code')->item(0)->nodeValue;
        $userId = $xdoc->getElementsByTagName('user-id')->item(0)->nodeValue;
        
        $dataArr = array($startDate,$endDate,$bank,$branchCode,$userId);
        return $dataArr;
    }



    public function generateTransResponseXml($requestArr)
    {

        if(count($requestArr) > 0)
        {
          
           $payForMeObj = payForMeServiceFactory::getService(payForMeServiceFactory::$TYPE_BASE);
           $pfmTransactionDetails = $payForMeObj-> getNotificationTransactionDetailsByDates($requestArr[0], $requestArr[1], $requestArr[2], $requestArr[3], $requestArr[4]);
           $doc = new DomDocument('1.0');
           $doc->formatOutput = true;

            //getting the root element of xml
            $rootElementName = "transaction-list";
            $root = $this->setXmlRoot($doc, $rootElementName);

            $root = $doc->appendChild($root);
 
          foreach($pfmTransactionDetails as $reqArr)
          {
            //get values from the array
            $transactionAmount = $reqArr['total_amount'];

            if($transactionAmount == '')
            $transactionAmount = NULL;

            $userName = $reqArr['MerchantRequest']['user_id'];
            $bankName = $reqArr['MerchantRequest']['Bank']['bank_name'];
            $branchCode = $reqArr['MerchantRequest']['BankBranch']['branch_code'];

            $paidAt = $reqArr['paid_at'];
            $paidAtArr = explode(" ", $paidAt);
            $paidAt = $paidAtArr[0].'T'.$paidAtArr[1];

            $validationNumber = $reqArr['MerchantRequest']['validation_number'];
            $transactionId = $reqArr['transaction_id'];

           
            $transaction_detail = $doc->createElement("transaction");

            $transaction_amount = $doc->createElement("transaction-amount");
            $transaction_amount->appendChild($doc->createTextNode($transactionAmount));

            $username = $doc->createElement("username");
            $username->appendChild($doc->createTextNode($userName));

            $bank_name = $doc->createElement("bank-name");
            $bank_name->appendChild($doc->createTextNode($bankName));

            $branch_code = $doc->createElement("branch-code");
            $branch_code->appendChild($doc->createTextNode($branchCode));

            $paid_at = $doc->createElement("paid-at");
            $paid_at->appendChild($doc->createTextNode($paidAt));

            $validation_number = $doc->createElement("validation-number");
            $validation_number->appendChild($doc->createTextNode($validationNumber));

            $transaction_id = $doc->createElement("transaction-id");
            $transaction_id->appendChild($doc->createTextNode($transactionId));

            //start appending the child to parent
            $root->appendChild($transaction_detail);
            $transaction_detail->appendChild($transaction_amount);
            $transaction_detail->appendChild($username);
            $transaction_detail->appendChild($bank_name);
            $transaction_detail->appendChild($branch_code);
            $transaction_detail->appendChild($paid_at);
            $transaction_detail->appendChild($validation_number);
            $transaction_detail->appendChild($transaction_id);
            
           }

            $xmldata = $doc->saveXML();
            
            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);

            //validate the XML
            $xmlValidatorObj = new validateBankXml() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, "1");
            if($isValid) {
                return $xmldata;
            }
            else {
                return false;
            }
          
        }
        else
        {
            return false;
        }
    }


    public function setXmlRoot($doc, $rootElementName){
        $root = $doc->createElement($rootElementName);
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/bankTransactionV1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/bankTransactionV1 bankTransactionV1.xsd' );
        return $root;
    }


    public function setResponseHeader($bank_id){
        if(trim($bank_id)){
            $bankDetails = Doctrine::getTable('BankPaymentScheduler')->getBankScheduleRecords($bank_id);
            $header = "";
            if(count($bankDetails)){
                $bankCode = $bankDetails[0]['bank_code'];
                $bankKey = $bankDetails[0]['bank_key'];
                $bankAuthString = $bankCode .":" .$bankKey;
                // $logger->info("auth string: $merchantAuthString");
                $bankAuth = base64_encode($bankAuthString) ;

                $header['Authorization'] = "Basic $bankAuth";
                $header['Content-Type'] =  "application/xml;charset=UTF-8";
                $header['Accept'] = "application/xml;charset=UTF-8";
            }
            $this->setHeader($header);
        }
    }
    private function setHeader($header){
        foreach($header as $key=>$val){
            sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
        }
    }



}

?>
