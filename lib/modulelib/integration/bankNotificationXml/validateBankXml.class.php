<?php


class validateBankXml {
  public function validateXML($xdoc, $version) {

    $config_dir = sfConfig::get('sf_config_dir') ;

    $xmlschema = $config_dir . '/xmlschema/bankTransactionV1.xsd';

    $handle = @fopen($xmlschema, "r");

    try {
      if ($xdoc->schemaValidate($xmlschema)) {
        return true;
      }else {
        return false ;
      }
    }catch(Exception $ex)  {
      echo $ex->getMessage();
    }
  }
}


?>
