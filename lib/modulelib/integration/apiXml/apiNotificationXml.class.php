<?php

class apiNotificationXml {

  
    public function validateTransRequestXml($xdoc,$xmldata){
        $xmlValidatorObj = new validateApiXml() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "1");
        return $isValid;
    }


    public function parseTransRequestXml($xdoc){
        $userType = $xdoc->getElementsByTagName('user-type')->item(0)->nodeValue;
        $dataArr["userType"] = $userType;

        if(isset($xdoc->getElementsByTagName('branch-code')->item(0)->nodeValue)){
           $branchCode = $xdoc->getElementsByTagName('branch-code')->item(0)->nodeValue;
            $dataArr["branchCode"] = $branchCode;

        } 
         

      
       // $dataArr = array("userType" => $userType,"branchCode" =>$branchCode);
        return $dataArr;
    }
    
    
      protected function convertDbDateToXsdDate($dbDate) {
        $dtokens=explode(' ', $dbDate);
        $xdate = implode('T',$dtokens);
        return $xdate;
    }





    public function generateApiResponseXml($requestArr,$groupId,$bankId)
    {
        if(count($requestArr) > 0)
        {
            $branchCode = "";
            if(array_key_exists("branchCode",$requestArr)) {
                $branchCode = $requestArr['branchCode'];
            }
        // THIS LINE IS ADDED TO SHOW DELETED_AT RECORD
        // Bug Id 31655
        Doctrine_Manager::getInstance()->setAttribute(Doctrine_Core::ATTR_USE_DQL_CALLBACKS, FALSE);
        $arrDetail =  Doctrine::getTable('Bank')->getAllApiRecords($groupId,$bankId,$branchCode);
        Doctrine_Manager::getInstance()->setAttribute(Doctrine_Core::ATTR_USE_DQL_CALLBACKS, TRUE);
     
       
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;

        $root = $this->setXMLroot($doc, 'user-detail-response','api.xsd');

        $root = $doc->appendChild($root);
        $users = $doc->createElement("users");

        $root->appendChild($users);


          foreach($arrDetail as $reqArr)
          {


            $reqArr['last_login'] = $this->convertDbDateToXsdDate($reqArr['last_login']);
            $userDetail = $doc->createElement("user");
            $username = $doc->createElement("username");
            $username->appendChild($doc->createTextNode($reqArr['username']));

            $first_name = $doc->createElement("first-name");
            $first_name->appendChild($doc->createTextNode($reqArr['firstname']));

            $last_name = $doc->createElement("last-name");
            $last_name->appendChild($doc->createTextNode($reqArr['lastname']));

            $email = $doc->createElement("email");
            $email->appendChild($doc->createTextNode($reqArr['email']));

            $user_status = $doc->createElement("user-status");

          //  1=Active,2=Bank_User_Suspanded,3=Bank_User_Deactivate, 4=eWallet_wait_Admin,5=eWallet_Disapprove
          if(!empty($reqArr['deleted_at'])){
              $status = 'Deleted';
          }
           else if($reqArr['failed_attempt']==5) {
                $status = "Blocked";
           }
            else if($reqArr['user_status']==1)
                $status = "Active";
            else if($reqArr['user_status']==2)
                $status = "Suspended";
            else if($reqArr['user_status']==3)
                $status = "Deactivated";


            $user_status->appendChild($doc->createTextNode($status));

          
            $last_login = $doc->createElement("last-login");
            $last_login->appendChild($doc->createTextNode($reqArr['last_login']));

            //start appending the child to parent
            $users->appendChild($userDetail);
            $userDetail->appendChild($username);
            $userDetail->appendChild($first_name);
            $userDetail->appendChild($last_name);
            $userDetail->appendChild($email);
            $userDetail->appendChild($user_status);
            $userDetail->appendChild($last_login);


          //  if($requestArr['branchCode'] != "") {
                //create branch details elements
                $branch_details = $doc->createElement("branch-details");
                $userDetail->appendChild($branch_details);

                $branch_name = $doc->createElement("name");
                $branch_name->appendChild($doc->createTextNode($reqArr['bankbranchname']));
                $branch_details->appendChild($branch_name);

                $branch_address = $doc->createElement("address");
                $branch_address->appendChild($doc->createTextNode($reqArr['address']));
                $branch_details->appendChild($branch_address);

                $branch_code = $doc->createElement("branch-code");
                $branch_code->appendChild($doc->createTextNode($reqArr['branch_code']));
                $branch_details->appendChild($branch_code);
                

         //   }
         
           }

            $xmldata = $doc->saveXML();
            
            $xdoc = new DomDocument;
            $isLoaded = $xdoc->LoadXML($xmldata);
            return $xmldata;
        }
        else
        {
            return false;
        }
    }

  public function generateMerchantResponseXml($bankCode) {
        $bankCode;
        $merchantDeatil = Doctrine::getTable('BankMerchant')->getMerchantDetail($bankCode);
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $this->setXMLroot($doc, 'merchant-detail-response','merchantRequest.xsd');

        $root = $doc->appendChild($root);
        $merchants = $doc->createElement("merchants");
        foreach($merchantDeatil as $dataArr) {
            
                
            foreach($dataArr['ServiceBankConfiguration'] as $arrService){
             
                $merchant = $doc->createElement("merchant");
                $name = $doc->createElement("name");
                $name->appendChild($doc->createTextNode($dataArr['name']));

                $merchantCode= $doc->createElement("merchant-code");
                $merchantCode->appendChild($doc->createTextNode($dataArr['merchant_code']));

                $accountNumber = $doc->createElement("account-number");
                $accountNumber->appendChild($doc->createTextNode($arrService['EpMasterAccount']['account_number']));

                $accountName = $doc->createElement("account-name");
                $accountName->appendChild($doc->createTextNode($arrService['EpMasterAccount']['account_name']));

                $bank = $doc->createElement("bank");
                $bank->appendChild($doc->createTextNode($arrService['EpMasterAccount']['bankname']));

                $sortcode = $doc->createElement("sortcode");
                $sortcode->appendChild($doc->createTextNode($arrService['EpMasterAccount']['sortcode']));

                $merchant->appendChild($name);
                $merchant->appendChild($merchantCode);
                $merchant->appendChild($accountNumber);
                $merchant->appendChild($accountName);
                $merchant->appendChild($bank);
                $merchant->appendChild($sortcode);

                $merchants->appendChild($merchant);
            }
        }

        $root->appendChild($merchants);
        $xmldata = $doc->saveXML();
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);
        
        return $xmldata;

    }

     public function generateTransactionResponseXml($xmlData,$bankCode){
      $userId = "";
      if (isset($xmlData->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('user-id')->item(0)->nodeValue))
      {
        $userName = $xmlData->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('user-id')->item(0)->nodeValue;
        $userObj = Doctrine::getTable('sfGuardUser')->findByUserName($userName);
        $userId = $userObj->getFirst()->getId();
      }
      $startDate = $xmlData->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('start-date')->item(0)->nodeValue;
      $endDate = $xmlData->getElementsByTagName('transaction-request')->item(0)->getElementsByTagName('end-date')->item(0)->nodeValue;

      $bankObj = Doctrine::getTable('Bank')->findByBankCode($bankCode);
      $bankId = $bankObj->getFirst()->getId();

      

      $doc = new DomDocument('1.0');
      $doc->formatOutput = true;
      $root = $this->setXMLroot($doc, 'transaction-list','transactionRequest.xsd');

      $root = $doc->appendChild($root);


      $merchantPaymentObj = Doctrine::getTable('MerchantRequest')->getTransactionDetail($startDate,$endDate,$bankId,$userId);
      if($merchantPaymentObj) {
        foreach($merchantPaymentObj as  $merchantObj)  {

          $paidBy = $merchantObj->getPaidBy();

          $merchantId = $merchantObj->getMerchantId();
          $transaction = $doc->createElement("transaction");

          $merchantCode = $doc->createElement("merchant-code");
          $merchantCode->appendChild($doc->createTextNode($merchantObj->getMerchant()->getMerchantCode()));

          $item = $doc->createElement("item");
          $itemName = $doc->createElement("item-name");
          $itemName->appendChild($doc->createTextNode($merchantObj->getMerchantService()->getName()));

          $transactionNo = $doc->createElement("pay4me-transaction-number");
          $transactionNo->appendChild($doc->createTextNode($merchantObj->getTransaction()->getFirst()->getPfmTransactionNumber()));

          $transactionDate = $doc->createElement("transaction-date");
          $transactionDate->appendChild($doc->createTextNode($this->convertDbDateToXsdDate($merchantObj->getPaidDate())));

//          $currencyCode =  $doc->createElement("currency-code");
//          $currencyCode->appendChild($doc->createTextNode($merchantObj->getCurrencyCode()->getCurrencyNum()));

          $totalAmount = $doc->createElement("total-amount");
          $totalAmount->setAttribute ( "currency-code", $merchantObj->getCurrencyCode()->getCurrencyNum());
          $totalAmount->appendChild($doc->createTextNode($merchantObj->getPaidAmount()*100));

          

          

          $paymentDetails = Doctrine::getTable('ServiceBankConfiguration')->getMerchantAccount($merchantId,$bankId);
          foreach($paymentDetails as $k=>$v) {
            $account_number = $v['EpMasterAccount']['account_number'];
            $accountNo = $doc->createElement("account-number");
            $accountNo->appendChild($doc->createTextNode($account_number));


          $payment = $doc->createElement("payment");
          $payment->setAttribute ( "type", "cr");

          $amount = $doc->createElement("amount");
          $amount->appendChild($doc->createTextNode($merchantObj->getPaidAmount()*100)); // naira
          }

          

          $collectionDetail= Doctrine::getTable('BankUser')->getCollectionDetail($bankId,$paidBy);
          $collectionDetails = $doc->createElement("collection-details");



          $bank = $doc->createElement("bank");
          $bank->appendChild($doc->createTextNode($collectionDetail['Bank']['bank_name']));

          $branch = $doc->createElement("branch");
          $branch->appendChild($doc->createTextNode($collectionDetail['BankBranch']['name']));

          $branchCode = $doc->createElement("branch-code");
          $branchCode->appendChild($doc->createTextNode($collectionDetail['BankBranch']['branch_code']));

          $teller = $doc->createElement("teller");
          $teller->appendChild($doc->createTextNode($collectionDetail['sfGuardUser']['username']));


          $payment->appendChild($accountNo);
          $payment->appendChild($amount);

          $collectionDetails->appendChild($bank);
          $collectionDetails->appendChild($branch);
          $collectionDetails->appendChild($branchCode);
          $collectionDetails->appendChild($teller);

          $item->appendChild($itemName);
          $item->appendChild($transactionNo);
          $item->appendChild($transactionDate);
          //$item->appendChild($currencyCode);
          $item->appendChild($totalAmount);
          $item->appendChild($payment);


          $transaction->appendChild($merchantCode);
          $transaction->appendChild($item);
          $transaction->appendChild($collectionDetails);

          $root->appendChild($transaction);
      }

      }

        $xmldata = $doc->saveXML();
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        return $xmldata;
  }



    public function setXmlRoot($doc, $rootElementName,$fileName = "api.xsd"){
        $root = $doc->createElement($rootElementName);
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.pay4me.com/schema/2' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.pay4me.com/schema/2/' .$fileName );
        return $root;
    }


    public function sendErrorXML($error_msg,$xsdFile="") {//print $error_msg;
         if(property_exists('apiErrorMessages', $error_msg)){
              $error_code = apiErrorMessages::$$error_msg;
         }

        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $this->setXMLroot($doc, 'error',$xsdFile);

        $root = $doc->appendChild($root);
       
        $desc = apiErrorMessages::getFormattedDescription($error_code);
  
        $code = $doc->createElement("error-code");
        $code->appendChild($doc->createTextNode($error_code));
        $description = $doc->createElement("error-message");
        $description->appendChild($doc->createTextNode($desc));
        $root->appendChild($code);
        $root->appendChild($description);
        $xmldata = $doc->saveXML();
        return $xmldata;

    }
    public function setResponseHeader($bank_code){    
        if(trim($bank_code)){
         
            $bankDetails = Doctrine::getTable('Bank')->getBankDetailByBankCode($bank_code);          
            $header = "";
            if($bankDetails){
                $bankDetails->getFirst();
                $bankObj = $bankDetails->getFirst();
                $bankCode = $bankObj->getBankCode();
                $bankKey = $bankObj->getBankKey();
                $bankAuthString = $bankCode .":" .$bankKey;
                $bankAuth = base64_encode($bankAuthString) ;
                $header['Authorization'] = "Basic $bankAuth";
                $header['Content-Type'] =  "application/xml;charset=UTF-8";
                $header['Accept'] = "application/xml;charset=UTF-8";
                $this->setHeader($header);
            }
           
        }
    }
    private function setHeader($header){
        foreach($header as $key=>$val){
            sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
        }
    }
}

?>
