<?php
class apiErrorMessages {


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




  
  public static $INVALID_REQUEST_XML = 1;
  public static $INVALID_RESPONSE_XML = 2;
  public static $INVALID_BANK = 3;
  public static $AUTHENTICATION_FAILURE = 4;
  public static $INVALID_BANK_BRANCH_CODE = 5;
  public static $INVALID_BANK_USER = 6;
  public static $INVALID_USERTYPE = 7;
  public static $INVALID_START_DATE = 8;
  public static $INVALID_END_DATE = 9;
  public static $INVALID_DATE = 10;
 
 
  public static $MSG_ERR_1 = 'Bank Request - Invalid Request XML Found';
  public static $MSG_ERR_2 = 'Bank Response - Invalid Response XML Found';
  public static $MSG_ERR_3 ='Bank Validation Failed';
  public static $MSG_ERR_4 = 'Authentication Failure';
  public static $MSG_ERR_5 = 'Bank Branch Validation Failed';
  public static $MSG_ERR_6 ='Bank User Validation Failed';
  public static $MSG_ERR_7 ='User Type Validation Failed';
  public static $MSG_ERR_8 = 'Start Date Shoul Be Less Than Current Date';
  public static $MSG_ERR_9 ='End Date Shoul Be Less Than Current Date';
  public static $MSG_ERR_10 ='Start Date Shoul Be Less Than End Date';
 




  public function error($code, array $params=NULL) {
    $logger=sfContext::getInstance()->getLogger();
    $flag = 1;
    $description  = $this->getFormattedDescription($code, $params);
    if(!$flag) {
      $logger->info("Throwing the exception");
      throw new Exception($description);
    }
    if($flag) {
      $logger->info("Error found in XML. Sending Error Xml with Error Code - ".$code);

      $obj = new pay4MeXMLV1();
      $errorXml = $obj->generateErrorNotification($code, $description);
      if($errorXml) {
        return $errorXml;
      }
    }
  }

  public static function getFormattedDescription($code,array $params=NULL) {
    $desc_var = "MSG_ERR_".$code;
    $description = self::$$desc_var;
    if(count($params)) {
      foreach($params as $key=>$value) {
        $description = str_replace('<'.$key.'>', $value, $description);
      }
    }
    return $description;
  }


}

?>
