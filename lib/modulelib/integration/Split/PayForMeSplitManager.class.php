<?php

/*
 * Algorithm for splitting the amount
 * 1.  Get all merchants from merchant table
 * 2.  Get all merchant services based on merchants fetched
 * 3.  Get split conf for each merchant service
 */

class PayForMeSplitManager
{
  private $merchants = array();
  private $merchantServices = array();  
  private $flatAmount = 0;
  private $percentAmount = 0;

  /**
   * Default constructor for the class -- to get all the merchants
   */
  public function PayForMeSplitManager()
  {
    //get the merchant details where the status is 0
    $this->merchants = Doctrine::getTable('MerchantRequest')->getAllMerchantsForSplit();    
    //call the method to get all merchant services for a merchant
    if(isset($this->merchants) && count($this->merchants) > 0)
    {
      foreach($this->merchants as $merchant)
      {
        $this->merchantServices = $this->getExistingMerchantServices($merchant['merchant_id']);
        //foreach merchant service also get the split_entity configuration
        if(isset($this->merchantServices) && count($this->merchantServices) > 0)
        {
          foreach($this->merchantServices as $merSvc)
          {
            //this line gets the split entity information from the split_entity_configuration table
            $splitInfo = array();
            $splitInfo = $this->getSplitEntityData($merSvc['merchant_service_id']);

            //this line gets the item fee from merchant_request table
            $merReqItemFee = $this->getMerchantRequestData($merSvc['id']);
            //now that we have the split info and item fee
            //pass this to processSplitting method for splitting
            if(isset($splitInfo) && count($splitInfo) != 0){
              $getResult = $this->processSplitting($merSvc['id'], $splitInfo, $merReqItemFee, $merSvc['merchant_service_id']);
            }
          }
        }
      }
    }
  }

  /**
   * For each merchant get all merchant services
   */
  private function getExistingMerchantServices($merchantID)
  {
    $merSvcArr = array();
    $merSvcArr = Doctrine::getTable('MerchantRequest')->getExistingMerchantSvc($merchantID);
    return $merSvcArr;
  }

  /**
   * This function will get the split information
   * based on the merchant service id
   */
  private function getSplitEntityData($merSvcID)
  {
    $getSplitEntityData = array();
    $getSplitEntityData = Doctrine::getTable('SplitEntityConfiguration')->getSplitInfoWithSplitCol($merSvcID);
    return $getSplitEntityData;
  }

  /**
   * Function to get the item fee
   * from merchant_request table
   */
  private function getMerchantRequestData($merReqId)
  {
    $getItemFee  = Doctrine::getTable('MerchantRequest')->getItemFeeById($merReqId);
    return $getItemFee;
  }

  /**
   * This function will split the item fee according to the splitting ionformation
   * @params: $merchant_request_id -- int
   *    
   *    $splitInfo -- array
   *    $item_fee  -- double
   *    $merchantService -- int
   */
  private function processSplitting($merchant_request_id, $splitInfo, $item_fee, $merchantService)
  {
    $returnArray = array('returnValue'=> 0, 'returnString'=>'Processing Successfull');

    //first check if the item fee and splitInfo is not empty
    if(isset($item_fee) && $item_fee != 0 && isset($splitInfo) && count($splitInfo) != 0)
    {
      $getInsertId = $this->createNewRecord($merchant_request_id, $merchantService);      

      //if getInsertId is not 0 or blank then do the splitting
      if(isset($getInsertId) && $getInsertId != 0)
      {
        //check if the split type is flat or percent
        foreach($splitInfo as $splInfo)
        {
          if($splInfo['SplitType']['split_name'] == "Flat")  //This split type is: Flat
          {
            //since it is the flat type and the row would always be one
            //simply save the flat value
            $getBoolResult = $this->updateSplitRow($getInsertId, $splInfo['merchant_item_split_column'], $splInfo['charge']);            
          }
          elseif($splInfo['SplitType']['split_name'] == "Percent")  //This split type is: Percent
          {
            $percentCharge = $splInfo['charge']/100;
            $getPercentAmount = ($item_fee * $percentCharge);
            $getBoolResult = $this->updateSplitRow($getInsertId, $splInfo['merchant_item_split_column'], $getPercentAmount);            
          }
          elseif($splInfo['SplitType']['split_name'] == "Ministry")  //This split type is: Ministry(The remaining amt will go in this
          {
            //we have to subtract the remaining amount
            $remainingAmount = $this->getRemainingAmount($item_fee, $splitInfo);
            $getBoolResult = $this->updateSplitRow($getInsertId, $splInfo['merchant_item_split_column'], $remainingAmount);            
          }          
        }
      }
      else
      {
        $returnArray = array('returnValue'=> 2, 'returnString'=>'New row could not be created');
      }
    }
    else
    {
      $returnArray = array('returnValue'=> 3, 'returnString'=>'Item Fee or split info missing');
    }

    return $returnArray;
  }

  /**
   * This method create a new row in the merchant_item_split table
   */
  private function createNewRecord($merchantReqId, $merchantSvcId)
  {
    $tab = new MerchantItemSplit();
    $tab->setMerchantRequestId($merchantReqId);
    $tab->setMerchantServiceId($merchantSvcId);
    $tab->save();
    $lastInsertId = $tab->getId();

    //update the payment_status_code in the merchant_request table    
    $this->updateMerReqStatus($merchantReqId);
    
    return $lastInsertId;
  }

  /**
   * This method updates the data in the existing
   * Params:
   * $rowId -- int
   * $column_name -- string
   * $colValue -- double
   */
  private function updateSplitRow($rowId, $column_name, $colValue)
  {
    $getBoolResult = Doctrine::getTable('MerchantItemSplit')->updateRowsOnId($rowId, $column_name, $colValue);
    return $getBoolResult;
  }

  /**
   * This function gets the summation
   */
  protected function getRemainingAmount($itemFee, $splInfo)
  {
    $this->flatAmount = 0;
    $this->percentAmount = 0;    
    $remainingAmount = 0;
    $returnArray = array('returnValue'=> 0, 'returnString'=>'Processing Successfull');

    foreach($splInfo as $splitInfo)
    {
      if($splitInfo['SplitType']['split_name'] == "Flat")
      {
        $this->flatAmount = (float)$this->flatAmount + (float)$splitInfo['charge'];
      }
      elseif($splitInfo['SplitType']['split_name'] == "Percent")
      {
        $this->flatAmount = (float)$this->flatAmount + (float)$splitInfo['charge'];
      }
    }

    //Now that we have the values in both the static variables, simply deduct it from the item fee
    //First check if the percent amount is zero or not
    if($this->percentAmount != 0)
    {
      $deductPercent = (float)$this->percentAmount/100;
      $percent_amount = $itemFee * $deductPercent;
    }

    //check if the percent amount if 0 or greater
    if($this->flatAmount != 0 && $this->percentAmount == 0)
    {
      $totalDedAmount = (float)$itemFee[0]['item_fee'] - (float)$this->flatAmount;
    }
    elseif($this->flatAmount == 0 && $this->percentAmount != 0)
    {
      $totalDedAmount = $itemFee[0]['item_fee'] - $percent_amount;
    }
    elseif($this->flatAmount != 0 && $this->percentAmount != 0)
    {
      $totalAmtToBeDeducted = (float)$this->flatAmount + $percent_amount;
      $totalDedAmount = $itemFee[0]['item_fee'] - $totalAmtToBeDeducted;
    }
    else
    {
      $returnArray = array('returnValue'=> 0, 'returnString'=>'Both total: Flat ');
    }
    return $totalDedAmount;
  }

  /**
   * This method updates the status in merchant_request table
   */
  private function updateMerReqStatus($merReqId)
  {
    //first check the current status
    $getStatus = Doctrine::getTable('MerchantRequest')->getMerRequestStatus($merReqId);        
    if($getStatus[0]['payment_status_code'] == '0')
    {      
      Doctrine::getTable('MerchantRequest')->updateMerchantReqStatus($merReqId);
    }
  }
}
