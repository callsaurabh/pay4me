<?php
class bankManager implements bankService {
//function to get the SP Details
  public function getAllRecords() {

    try {
      return $all_records = Doctrine::getTable('Bank')->getAllRecords();
    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getBankList() {
    try {
      $bank = Doctrine::getTable('Bank')->createQuery('a')->where('status=?',1)->andWhere('acronym !=?','ewallet')->orderBy('bank_name')->execute();
      $bank_list = array();
      foreach ($bank as $i => $bank_detail):
        $bank_list[$bank_detail->getAcronym()] =  $bank_detail->getBankName();
      endforeach;
      return $bank_list;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  
  public function getAllBanks() {
    try {
      $bank = Doctrine::getTable('Bank')->getBankUserList();
     // $bank = Doctrine::getTable('Bank')->createQuery('a')->where('id?=',8)->orderBy('bank_name')->execute();
      $bank_list = array();
      foreach ($bank as $i => $bank_detail):
        $bank_list[$bank_detail->getId()] =  $bank_detail->getBankName();
      endforeach;
      return $bank_list;

    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getBankInfo($bank_id) {

    try {
      return $bank_info = Doctrine::getTable('Bank')->getBankName($bank_id);
    }catch(Exception $e ) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  //This method checks if the bank was previously created or not
  public function checkBankExists($bkname, $countryId="")
  {
    $getCount = Doctrine::getTable('Bank')->chkBankExists($bkname, $countryId);
    //echo "<pre>"; print_r($getCount); die();
    return $getCount;
  }

   //This method checks if the bank already exists
  public function checkBankAlreadyExists($bkname, $countryId,$bankId="")
  {
    $getCount = Doctrine::getTable('Bank')->checkBankAlreadyExists($bkname, $countryId,$bankId);
//    echo "<pre>"; print_r($getCount); die();
    return $getCount;
  }

  //This method checks if the bank was previously created or not
  public function checkBankAcronym($acronym)
  {
    $getCount = Doctrine::getTable('Bank')->chkAcronymExists($acronym); 
    return $getCount;
  }

  //Method to return banks array
  public function returnBankArr()
  {
    $getRecords = Doctrine::getTable('Bank')->getBanksArr();
    return $getRecords;
  }
// WP040 API HTTP Basic Authentication
  public function authenticateBankRequest($request)
  {
      $bank_code = $request->getHttpHeader('AUTH_USER','PHP') ;
      $bank_key = $request->getHttpHeader('AUTH_PW','PHP') ;    
      $request_bank_code = $request->getParameter('Bank') ;

      if($request_bank_code != $bank_code) {
          return false;
      }

      $logger = sfContext::getInstance()->getLogger();
      $logger->debug("OBJ: $bank_key ".print_r($bank_code,true));

      $bank_details =  BankTable::getBankDetails($request_bank_code) ;
      $logger->debug("OBJ: ".print_r($bank_details,true));
      if(($bank_details['bank_code'] == $bank_code) && ($bank_details['bank_key'] == $bank_key)){
          return true ;
      }

      return false ;
  }

}
?>
