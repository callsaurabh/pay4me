<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class supportMerchantRequestDetails {

    public $transactionId;
    public $itemId;
    public $amount;
    public $currency;
    public $paymentDate;
    public $paymentMode;
    public $bankName;
    public $bankBranch;
    public $statusCode;
    public $statusDesc;
    public $serviceId;
    public $applicationId;
    public $refId;
    public $userDetails;
    public $merchantDetails;
    public $serviceName;

    static public $currencyName = "naira";

  public function setMerchantRequestDetails($txnRefNo,$merchantItemId,$itemFee,$updatedAt,$paymentModeOptionId,$bankId,$bankBranchId,$serviceId='',$statusCode,$createdAt,$userId='',$merchantId='',$serviceId=''){

     /*
     $this->transactionId = $merchantRequestDetails->getTxn_Ref();
     $this->itemId = $this->getItemId($merchantRequestDetails->getMerchantItemId());
     $this->amount = $merchantRequestDetails->getItemFee();
     $this->currency = self::$currencyName;
     $this->paymentDate = $merchantRequestDetails->getUpdatedAt();
     $this->paymentMode = $this->getPaymentMode($merchantRequestDetails->getPaymentModeOptionId());
     $this->bankName = $this->getBankName($merchantRequestDetails->getBankId());
     $this->bankBranch = $this->getBankBranch($merchantRequestDetails->getBankBranchId());
     $this->statusCode = pay4meError::$PAYMENT_SUCCESS;
     $this->statusDesc = pay4meError::$MSG_ERR_."".$this->statusCode;
     */
     $transStatusCode = pay4meError::$PAYMENT_SUCCESS;
     $msg = "MSG_ERR_".$statusCode;
     $this->transactionId = $txnRefNo;
     $this->itemId = $this->getItemId($merchantItemId);
     $this->amount = $itemFee;
     $this->currency = self::$currencyName;
     if($statusCode == 1){
      $this->paymentDate = $createdAt;
     }else{
      $this->paymentDate = $updatedAt;
     }
     $this->paymentMode = $this->getPaymentMode($paymentModeOptionId);
     $this->bankName = $this->getBankName($bankId);
     $this->bankBranch = $this->getBankBranch($bankBranchId);
     $this->statusCode = $statusCode;
     $this->statusDesc = pay4meError::$$msg;
     $this->serviceId = $serviceId;
     //$this->applicationId = $applicationId;
     //$this->refId = $refId;
     $this->userDetails = $this->getUserName($userId);
     $this->merchantDetails = $this->getMerchantDetailsById($merchantId);
     $this->serviceName = $this->getMerchantServiceName($serviceId);
     return $this;

  }



  public function getItemId($id){
      return  Doctrine::getTable('MerchantItem')->getItemId($id);
  }

  public function getPaymentMode($paymentModeOptionId){
      return $this->getPaymentModeDetails($this->getPaymentModeId($paymentModeOptionId));
  }

  public function getPaymentModeId($paymentModeOptionId){
      return  Doctrine::getTable('PaymentModeOption')->getPaymentModeOptionDetails($paymentModeOptionId);
  }

  public function getPaymentModeDetails($paymentModeOptionId){
      return  Doctrine::getTable('PaymentMode')->getPaymentModeDetails($paymentModeOptionId);
  }

  public function getBankName($bankId){
     return  Doctrine::getTable('Bank')->getBankName($bankId);
  }

  public function getBankBranch($bankBranchId){
      return  Doctrine::getTable('BankBranch')->getBankBranchNameBYID($bankBranchId);
  }


  public function getUserName($userId){
      return  Doctrine::getTable('UserDetail')->getUserDetails($userId);
  }

  public function getMerchantDetailsById($merchantId){
      return  Doctrine::getTable('Merchant')->getMerchantDetailsById($merchantId);
  }

   public function getMerchantServiceName($serviceId){
      return  Doctrine::getTable('MerchantService')->getMerchantServiceName($serviceId);
  }

}


?>