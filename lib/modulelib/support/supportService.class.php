<?php
interface supportService{
    public function validateHeader($headerArr);
    public function getMerchantDetails($merchantCode);
    public function validateRquest($requestArr);
    public function getMerchentRequestDetails($merchentRequestId);
    public function getMerchentRequestId($itemNumber,$merchantService_id);
    public function getAllMerchentRequestsBetweenDates($serviceId,$startDate,$endDate);
    public function getPaidPaymentRequestDetails($serviceId,$startDate,$endDate);
}
?>
