<?php
class supportManager implements supportService {



    public function validateHeader($headerArr){

        $transStatusObj = new supportTransStatus();
        if(is_array($headerArr)){
            if($headerArr['merchantCode'] != ''){
                if(!Doctrine::getTable('Merchant')->isValidMerchant($headerArr['merchantCode'])){
                    $transStatus = $transStatusObj->setTransStatus(pay4meError::$INVALID_MERCHANT);
                    $returnArr =  array(false,$transStatus);
                    return  $returnArr;
                }
            }

            if($headerArr['merchantRequestId'] != $headerArr['merchantCode']) {
                $transStatus = $transStatusObj->setTransStatus(pay4meError::$AUTHENTICATION_FAILURE);
                $returnArr =  array(false,$transStatus);
                return  $returnArr;
            }

            $merchantDetails = $this->getMerchantDetails($headerArr['merchantRequestId']);

            if(($merchantDetails['merchant_code'] != $headerArr['merchantCode']) || ($merchantDetails['merchant_key'] != $headerArr['merchantKey'])){
                $transStatus = $transStatusObj->setTransStatus(pay4meError::$AUTHENTICATION_FAILURE);
                $returnArr =  array(false,$transStatus);
                return  $returnArr;
            }


            //$transStatus = $transStatusObj->setTransStatus('');
            $transStatus = '';
            $returnArr =  array(true,$transStatus);
            return  $returnArr;

        }
    }



    public function getMerchantDetails($merchantCode){
        return  Doctrine::getTable('Merchant')->getMerchantDetails($merchantCode);
    }


    public function validateRquest($requestArr){

        $transStatusObj = new supportTransStatus();
        $merchantRequestDetailsObj = new supportMerchantRequestDetails();

        if(is_array($requestArr)){

            if($requestArr['itemId'] != ''){
                $merchentRequestId = $this->getMerchentRequestId($requestArr['itemId'],$requestArr['serviceId']);


                if($merchentRequestId == false){
                    $transStatus = $transStatusObj->setTransStatus(pay4meError::$INVAID_ITEM_REQUEST);
                    $returnArr =  array(false,$transStatus);
                    return  $returnArr;
                }


                //getting payment Request Details
                $merchantRequestDetails = $this->getMerchentRequestDetails($merchentRequestId[0]['MerchantRequest'][0]['id']);

                if($merchantRequestDetails == false){
                    $transStatus = $transStatusObj->setTransStatus(pay4meError::$NO_PAYMENT_ATTEMPT);
                    $returnArr =  array(false,$transStatus);
                    return  $returnArr;
                }

                //returning merchant request details object
                $merchantDetails = $merchantRequestDetailsObj->setMerchantRequestDetails($merchantRequestDetails[0]['txn_ref'],$merchantRequestDetails[0]['merchant_item_id'],$merchantRequestDetails[0]['item_fee'],$merchantRequestDetails[0]['updated_at'],$merchantRequestDetails[0]['payment_mode_option_id'],$merchantRequestDetails[0]['bank_id'],$merchantRequestDetails[0]['bank_branch_id'],$merchantRequestDetails[0]['MerchantItem']['merchant_service_id']);
                $merchantDetailsArrayObj = array();
                $merchantDetailsArrayObj[0] = $merchantDetails;
                $returnArr =  array(true,$merchantDetailsArrayObj);
                return  $returnArr;

            }

            if($requestArr['itemId'] == ''){

                if($requestArr['requestType'] == "newOrder"){
                    $allMerchentRequests = $this->getAllMerchentRequestsBetweenDates($requestArr['serviceId'],$requestArr['startDate'], $requestArr['endDate']);

                    $merchantDetailsArrayObj = array();
                    $counterVal = count($allMerchentRequests);
                    for($i=0;$i<$counterVal;$i++){
                        $detailsObj = new supportMerchantRequestDetails();
                        $merchantDetails = $detailsObj->setMerchantRequestDetails($allMerchentRequests[$i]['txn_ref'],$allMerchentRequests[$i]['merchant_item_id'],$allMerchentRequests[$i]['item_fee'],$allMerchentRequests[$i]['updated_at'],$allMerchentRequests[$i]['payment_mode_option_id'],$allMerchentRequests[$i]['bank_id'],$allMerchentRequests[$i]['bank_branch_id'],$allMerchentRequests[$i]['MerchantItem']['merchant_service_id'],$allMerchentRequests[$i]['payment_status_code'],$allMerchentRequests[$i]['created_at']);
                        $merchantDetailsArrayObj[] = $merchantDetails;
                        unset($detailsObj);
                    }

                    $returnArr =  array(true,$merchantDetailsArrayObj);
                    return  $returnArr;

                }else{
                    $paidMerchentRequests = $this->getPaidPaymentRequestDetails($requestArr['serviceId'],$requestArr['startDate'], $requestArr['endDate']);

                    $merchantDetailsArrayObj = array();
                    $counterVal = count($paidMerchentRequests);
                    for($i=0;$i<$counterVal;$i++){
                        $detailsObj = new supportMerchantRequestDetails();
                        $merchantDetails = $detailsObj->setMerchantRequestDetails($paidMerchentRequests[$i]['txn_ref'],$paidMerchentRequests[$i]['merchant_item_id'],$paidMerchentRequests[$i]['item_fee'],$paidMerchentRequests[$i]['updated_at'],$paidMerchentRequests[$i]['payment_mode_option_id'],$paidMerchentRequests[$i]['bank_id'],$paidMerchentRequests[$i]['bank_branch_id'],$paidMerchentRequests[$i]['MerchantItem']['merchant_service_id'],$paidMerchentRequests[$i]['payment_status_code'],$paidMerchentRequests[$i]['created_at']);
                        $merchantDetailsArrayObj[] = $merchantDetails;
                        unset($detailsObj);
                    }
                    $returnArr =  array(true,$merchantDetailsArrayObj);
                    return  $returnArr;
                }
            }
        }
    }


    public function getMerchentRequestDetails($merchentRequestId){
        return  Doctrine::getTable('MerchantRequest')->getSucessPaymentRequestDetails($merchentRequestId);
    }

    public function getMerchentRequestId($itemNumber,$merchantService_id){
        return  Doctrine::getTable('MerchantItem')->getPaymentRequestStatus($itemNumber,$merchantService_id);
    }

    public function getAllMerchentRequestsBetweenDates($serviceId,$startDate,$endDate){
        return  Doctrine::getTable('MerchantRequest')->getAllPaymentRequestDetails($serviceId,$startDate,$endDate);
    }

    public function getPaidPaymentRequestDetails($serviceId,$startDate,$endDate){
        return  Doctrine::getTable('MerchantRequest')->getPaidPaymentRequestDetails($serviceId,$startDate,$endDate);
    }



    public function getRequestDetails($transactionType, $transactionNo,$validationNo,
               $merchant,$bank,$status,$fromDate,$toDate){
       return  Doctrine::getTable('MwRequest')->getRequestDetails($transactionType,
               $transactionNo,$validationNo, $merchant,$bank,$status,$fromDate,$toDate);
    }


    public function getHistoryDetail($requestId){
       return Doctrine::getTable('MwResponse')->getHistoryDetail($requestId);
    }

    public function getLogDetail($transactionNo){
//      return Doctrine::getTable('MessageLog')->getLogDetail($transactionNo);
       return Doctrine::getTable('MwMessageLog')->getLogDetail($transactionNo);
    }

    public function checkAllreadyPosted($requestId){
       return Doctrine::getTable('MessageQueueRequest')->checkAllreadyPosted($requestId);
    }

}
?>